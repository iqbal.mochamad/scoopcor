import os
from subprocess32 import check_call

from app.uploads.pdf.utils import PdfMetadata


class PdfCoverExtractorCommand(object):
    """ Generates a JPEG image from the first page of a PDF.

    Depending on the 'cover_type', the PDF will be generated in any number
    of different sizes.
    """
    """ Generates a single PNG thumbnail for a single page within a PDF.
    """
    def __init__(self, input_pdf_path, output_base_dir, page_num):
        """
        :param `str` input_pdf_path:
            The PDF to use when generating the thumbnails.
        :param `str` output_base_dir:
            The full filesystem directory path where the thumbnail will be
            stored. This folder will be created, if it doesn't exist.
        """
        self.input_pdf_path = input_pdf_path
        self.output_base_dir = output_base_dir
        self.page_num = page_num

    pixel_density = 150

    @property
    def output_file_full_path(self):
        """ Gets the location where the new thumbnail will be saved.

        :return: The full filesystem path where the thumbnail will be saved.
        :rtype: str
        """
        return os.path.join(
            self.output_base_dir,
            "{self.page_num}.jpg".format(self=self))

    @property
    def processing_command(self):
        """ The command that should be executed when *process* is called.

        :return: A single command that may be passed as an argument to
            :func:`subprocess.check_call`
        """
        return ('convert '
                '-density {self.pixel_density} '
                '{self.input_pdf_path}[{self.page_num}] '
                '{self.output_file_full_path}').format(self=self)

    def process(self):
        """ Executes the shell job.

        :return: The full path for the extracted thumbnail.
        :rtype: `str`
        """
        if not os.path.exists(os.path.dirname(self.output_file_full_path)):
            os.makedirs(os.path.dirname(self.output_file_full_path))

        check_call(self.processing_command, shell=True)

        return self.output_file_full_path


class PdfBatchCoverExtractor(object):
    """ Generates a thumbnail for every single page of a given PDF.
    """
    def __init__(self, input_pdf_path, output_base_dir):
        """
        :param `str` input_pdf_path:
            The PDF to use when generating the thumbnails.
        :param `str` output_base_dir:
            The full filesystem path where the thumbnails will be stored.
            This folder will be created, if it doesn't exist.
        """
        self.input_pdf_path = input_pdf_path
        self.output_base_dir = output_base_dir
        self.pdf_meta = PdfMetadata(self.input_pdf_path)

    def process(self):
        """ Executes shell job for every page in the input_pdf.

        :return: A list of the full path for all extracted thumbnails.
        :rtype: list
        """
        all_thumbnail_paths = []
        for page_num in range(0, self.pdf_meta.page_count):
            extractor = PdfThumbnailExtractor(self.input_pdf_path,
                                              self.output_base_dir,
                                              page_num)
            thumbnail_file_path = extractor.process()
            all_thumbnail_paths.append(thumbnail_file_path)
        return all_thumbnail_paths
