import hashlib
from datetime import datetime

from PyPDF2 import PdfFileReader


class PdfMetadata(object):
    """ Fetches metadata (such a page number count) from a single PDF.
    """
    def __init__(self, input_pdf_path):
        """
        :param `str` input_pdf_path: Absolute filesystem path of a PDF.
        """
        self.input_pdf_path = input_pdf_path

    @property
    def page_count(self):
        """ Gets the total number of pages in a PDF.

        :returns: The total number of pages in the PDF as an integer.
        :rtype: int
        """
        pdf_read = PdfFileReader(self.input_pdf_path, strict=False)
        return pdf_read.numPages


def file_version_builder(item):
    """ Helper function for building something called a 'file version'

    Anyone.. please.. better explanation on what a 'file_version' is for us.

    :param `app.items.models.Item` item:
    :return:
    """
    def version_string_builder(item, format_string):
        first_pass = format_string.format(item=item,
                                          current_time=datetime.now())
        second_pass = hashlib.md5(first_pass).hexdigest()
        return second_pass

    ver1 = version_string_builder(
        item=item,
        format_string='{item.id}{item.edition_code}'
                      '{current_time:%Y-%m-%d %H:%M:%S}{item.release_date}')

    ver2 = version_string_builder(
        item=item,
        format_string='{item.brand_id}0.99{current_time:%Y-%m-%d %H:%M:%S}')

    return "{ver1}{ver2}".format(**locals())
