import hashlib
import os

from subprocess32 import check_call

# password from GARAMZIP (zip salt)
from app.uploads.pdf.utils import file_version_builder


def zip_password_generator(item, salt, paddings=None):
    paddings = paddings or ('~#%&', '*&#@!', '&%^)', )
    file_version = file_version_builder(item)
    pass_zip = ("%(item_id)s%(padding1)s%(salt)s%(padding2)s"
                "%(brand_id)s%(padding3)s"
                "%(file_version)s") % {'item_id': item.id,
                                       'padding1': paddings[0],
                                       'salt': salt,
                                       'padding2': paddings[1],
                                       'brand_id': item.brand_id,
                                       'padding3': paddings[2],
                                       'file_version': file_version, }
    return hashlib.sha256(pass_zip).hexdigest()


class PdfPackager(object):
    """ Creates our zip package, containing the PDF for consumption by the apps
    """
    def __init__(self, input_pdf_path, output_base_dir, item, zip_password):
        self.input_pdf_path = input_pdf_path
        self.zip_password = zip_password
        self.output_base_dir = output_base_dir
        self.item = item
        # need the thumbnail generator
        # need the encryptor

    @property
    def output_file_full_path(self):
        return "%s%s%s" % (self.output_base_dir, self.item.edition_code, '.zip')

    @property
    def processing_command(self):
        return ['7za', 'a', '-p' + self.zip_password] + \
                  [self.output_file_full_path] + \
                  [os.path.join(self.output_base_dir, 'magazine.pdf'),
                   os.path.join(self.output_base_dir, 'thumbs')]

    def process(self):
        if not os.path.exists(os.path.dirname(self.output_file_full_path)):
            os.makedirs(os.path.dirname(self.output_file_full_path))

        check_call(self.processing_command, shell=True)

        return self.output_file_full_path

