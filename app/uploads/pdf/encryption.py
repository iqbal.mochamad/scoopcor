import os
import hashlib

from PyPDF2 import PdfFileReader, PdfFileWriter


class PdfEncryptor(object):

    def __init__(self, input_pdf_path, output_base_dir, salt, item):
        """
        :param `app.items.models.Item` item:
        :param `str` input_pdf_path:
        :param `str` output_base_dir:
        :param `str` salt: Comes from the GARAMPDF in app.config
        """
        self.input_pdf_path = input_pdf_path
        self.output_base_dir = output_base_dir
        self.salt = salt
        self.item = item

    @property
    def output_file_full_path(self):
        return os.path.join(self.output_base_dir, 'magazine.zip')

    @property
    def processing_command(self):
        raise NotImplementedError

    @property
    def pdf_password(self):
        pass_pdf = "%s%s%s%s%s%s%s" % (
            self.item.id, '~#%&', self.salt,
            '*&#@!', self.item.brand_id, '&%^)', file_version)
        return hashlib.sha256(pass_pdf).hexdigest()

    def process(self):
        if not os.path.exists(os.path.dirname(self.output_file_full_path)):
            os.makedirs(os.path.dirname(self.output_file_full_path))

        input = PdfFileReader(file(self.input_pdf_path, "rb"))

        output = PdfFileWriter()
        output.encrypt(self.pdf_password)

        outputStream = file(self.output_file_full_path, "wb")
        output.write(outputStream)
        outputStream.close()

        return self.output_file_full_path





