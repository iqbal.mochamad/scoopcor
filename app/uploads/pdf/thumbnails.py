"""
PDF Thumbnail Extractors
========================

For each of our PDFs, we extract a small thumbnail for each page,
which is displayed to the user within the frontend applications.
"""
import os
from subprocess32 import check_call

from app.uploads.pdf.utils import PdfMetadata





class PdfThumbnailExtractor(object):
    """ Generates a single PNG thumbnail for a single page within a PDF.
    """
    def __init__(self, input_pdf_path, output_base_dir, page_num):
        """
        :param `str` input_pdf_path:
            The PDF to use when generating the thumbnails.
        :param `str` output_base_dir:
            The full filesystem directory path where the thumbnail will be
            stored. This folder will be created, if it doesn't exist.
        """
        self.input_pdf_path = input_pdf_path
        self.output_base_dir = output_base_dir
        self.page_num = page_num

    pixel_density = 24

    @property
    def output_file_full_path(self):
        """ Gets the location where the new thumbnail will be saved.

        :return: The full filesystem path where the thumbnail will be saved.
        :rtype: str
        """
        return os.path.join(
            self.output_base_dir,
            "{}.jpg".format(self.page_num+1))  # HACK -> input is a page 'index'

    @property
    def processing_command(self):
        """ The command that should be executed when *process* is called.

        :return: A single command that may be passed as an argument to
            :func:`subprocess.check_call`
        """
        return ('convert '
                '-density {self.pixel_density} '
                '{self.input_pdf_path}[{self.page_num}] '
                '-background white '
                '{self.output_file_full_path}').format(self=self)

    def process(self):
        """ Executes the shell job.

        :return: The full path for the extracted thumbnail.
        :rtype: `str`
        """
        if not os.path.exists(os.path.dirname(self.output_file_full_path)):
            os.makedirs(os.path.dirname(self.output_file_full_path))

        check_call(self.processing_command, shell=True)

        return self.output_file_full_path


class PdfBatchThumbnailExtractor(object):
    """ Generates a thumbnail for every single page of a given PDF.
    """
    def __init__(self, input_pdf_path, output_base_dir):
        """
        :param `str` input_pdf_path:
            The PDF to use when generating the thumbnails.
        :param `str` output_base_dir:
            The full filesystem path where the thumbnails will be stored.
            This folder will be created, if it doesn't exist.
        """
        self.input_pdf_path = input_pdf_path
        self.output_base_dir = output_base_dir
        self.pdf_meta = PdfMetadata(self.input_pdf_path)

    def process(self):
        """ Executes shell job for every page in the input_pdf.

        :return: A list of the full path for all extracted thumbnails.
        :rtype: list
        """
        all_thumbnail_paths = []
        for page_num in range(0, self.pdf_meta.page_count):
            extractor = PdfThumbnailExtractor(self.input_pdf_path,
                                              self.output_base_dir,
                                              page_num)
            thumbnail_file_path = extractor.process()
            all_thumbnail_paths.append(thumbnail_file_path)
        return all_thumbnail_paths


class WebReaderPageExtractor(PdfThumbnailExtractor):
    pixel_density = 150
    # max_width_pixels = 1536
    filter = 'Cubic'

    @property
    def output_file_full_path(self):
        """ Gets the location where the new thumbnail will be saved.

        :return: The full filesystem path where the thumbnail will be saved.
        :rtype: str
        """
        return os.path.join(
            self.output_base_dir,
            "{}.jpg".format(self.page_num+1))


    @property
    def processing_command(self):
        """ The command that should be executed when *process* is called.

        :return: A single command that may be passed as an argument to
            :func:`subprocess.check_call`
        """
        return ('convert '
                '-density {self.pixel_density} '
                #'-adaptive-resize {self.max_width_pixels} '
                '-filter {self.filter} '
                '-quality 60 '
                '-background white '
                '{self.input_pdf_path}[{self.page_num}] '
                '{self.output_file_full_path}').format(self=self)


class WebReaderBatchExtractor(object):
    """ Generates a thumbnail for every single page of a given PDF.
    """
    def __init__(self, input_pdf_path, output_base_dir):
        """
        :param `str` input_pdf_path:
            The PDF to use when generating the thumbnails.
        :param `str` output_base_dir:
            The full filesystem path where the thumbnails will be stored.
            This folder will be created, if it doesn't exist.
        """
        self.input_pdf_path = input_pdf_path
        self.output_base_dir = output_base_dir
        self.pdf_meta = PdfMetadata(self.input_pdf_path)

    def process(self):
        """ Executes shell job for every page in the input_pdf.

        :return: A list of the full path for all extracted thumbnails.
        :rtype: list
        """
        all_thumbnail_paths = []
        for page_num in range(0, self.pdf_meta.page_count):
            extractor = WebReaderPageExtractor(self.input_pdf_path,
                                              self.output_base_dir,
                                              page_num)
            thumbnail_file_path = extractor.process()
            all_thumbnail_paths.append(thumbnail_file_path)
        return all_thumbnail_paths
