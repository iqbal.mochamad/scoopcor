from __future__ import unicode_literals, absolute_import

import httplib, json
from datetime import datetime
from flask import Response, request

from flask.views import MethodView
from sqlalchemy import desc
from werkzeug.exceptions import NotFound

from app import app, db
from app.auth.decorators import user_has_any_tokens
from app.helpers import internal_server_message

from app.items.choices import STATUS_TYPES
from app.items.choices.status import STATUS_UPLOADED

from app.items.models import Item, ItemUploadProcess
from app.offers.models import Offer

from . import parsers
from .choices import ImageResourceType
from .upload_epub import UploadEpubProcessing
from .upload_pdf import UploadPDFProcessing
from .upload_image import UploadImageFtp
from .upload_web_reader import UploadWebReaderProcessing


class UploadImageApi(MethodView):
    """ Upload a new image to the API.
    """
    @user_has_any_tokens('can_read_write_global_all')
    def post(self):
        req_args = parsers.UploadImageParser().parse_args()

        if req_args['resource'] == ImageResourceType.item:
            data = Item.query.get(req_args['id'])

        if req_args['resource'] == ImageResourceType.offer:
            data = Offer.query.get(req_args['id'])

        scene = UploadImageFtp(data=data, args=req_args).process()

        return scene


class ItemUploadApi(MethodView):
    """ This registers an Item's PDF or ePUB file for being ready to process.
    """
    @user_has_any_tokens('can_read_write_global_all',
                         'can_read_write_public_ext')
    def post(self):
        req_args = parsers.ItemUploadParser().parse_args(req=request)

        s = db.session()
        item = s.query(Item).get(req_args['item_id'])
        if not item:
            raise NotFound

        try:
            if not item.release_schedule:
                item.release_schedule = datetime.now()

            exist_upload2 = ItemUploadProcess.query.filter_by(item_id=item.id).all()
            for data in exist_upload2:
                db.session.delete(data)
                db.session.commit()

            exist_upload = ItemUploadProcess(**req_args)
            sv = exist_upload.save()

            if sv.get('invalid', False):
                return internal_server_message(modul="upload items", method="POST",
                    error=sv.get('message', ''), req=str(req_args))
            else:
                # Update item status (Uploaded)
                item.item_status = STATUS_TYPES[STATUS_UPLOADED]
                item.save()

            return Response(json.dumps(exist_upload.values()),
                status=httplib.OK, mimetype='application/json')

        except Exception:
            s.rollback()
            raise


def upload_process():
    try:
        data_upload = ItemUploadProcess.query.filter_by(status=1).order_by(
            ItemUploadProcess.file_size).limit(1).all()

        for item_processing in data_upload:
            # update to processings
            item_processing.start_process_time = datetime.now()
            item_processing.release_schedule = item_processing.item.release_schedule if item_processing.item.release_schedule else datetime.now()
            item_processing.status = 2
            db.session.add(item_processing)
            db.session.commit()

        for item_processing in data_upload:
            item = Item.query.filter_by(id=item_processing.item_id).first()

            if item.content_type == 'pdf':
                print 'Processing PDF START'
                UploadPDFProcessing(item, item_processing).process()

            if item.content_type == 'epub':
                print 'Processing EPUB START'
                UploadEpubProcessing(item, item_processing).process()

            #clear redis data.
            redis_key = 'items-details-{}:*'.format(item_processing.item_id)
            for data_redis in app.kvs.keys(redis_key):
                app.kvs.delete(data_redis)

            print "successfully process file", item_processing.id

    except Exception as e:
        print "upload_process Failed {}".format(e)


def upload_webreader():
    try:
        raw_upload = ItemUploadProcess.query.filter_by(status=10).order_by(ItemUploadProcess.id).limit(1).first()

        # find all related item_id, clean up soo this double record not processed again..
        all_upload = ItemUploadProcess.query.filter_by(item_id=raw_upload.item_id).all()
        for iupload in all_upload:
            iupload.status = 12
            db.session.add(iupload)
            db.session.commit()

        # find latest upload by item_id
        data_upload = ItemUploadProcess.query.filter_by(item_id=raw_upload.item_id).order_by(desc(ItemUploadProcess.id)).first()

        if data_upload:
            # update to processings
            data_upload.status = 3
            data_upload.start_process_time = datetime.now()
            data_upload.release_schedule = data_upload.item.release_schedule if data_upload.item.release_schedule else datetime.now()
            db.session.add(data_upload)
            db.session.commit()

            if data_upload.item.content_type == 'pdf':
                print 'Processing PDF WEB READER START'
                UploadWebReaderProcessing(data_upload.item, data_upload).process()

            print "successfully process web reader file", data_upload.item_id

    except Exception as e:
        print "upload_webreader Failed {}".format(e)


class ItemUploadProcessing(MethodView):
    """
        This actually executes the processing job for any currently-pending items.
    """

    def get(self):
        upload_process()
