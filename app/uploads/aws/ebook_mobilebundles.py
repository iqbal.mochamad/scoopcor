"""
    This script for copy all zip files from biznet to gramedia S3 ebook-mobilebundles bucket
"""

import boto3, os
from app import app


def check_exist_not(s3=None, bucket_name=None, key=None):
    try: file_exists = s3.head_object(Bucket=bucket_name, Key=key)
    except: file_exists = None
    return file_exists


def upload_bulk_files(bucket_name, brand_id, directory_path, s3):

    """ upload all files inside brand_id folders, no matter what files it is, and when uploaded. """

    for item in os.listdir(directory_path):
        bucket_path = '{}/{}'.format(brand_id, item)
        file_exist = check_exist_not(s3, bucket_name, bucket_path)

        if file_exist:

            # croscek size?!
            old_size = os.path.getsize(os.path.join(directory_path, item))

            if old_size != file_exist.get('ContentLength'):
                s3.upload_file(os.path.join(directory_path, item), bucket_name, bucket_path)
                print 'successfully upload new files, path bucket {} files : {}'.format(bucket_path, item)
            else:
                # File are same.. nothing changes
                pass
        else:
            # upload file, this file never upload before.
            s3.upload_file(os.path.join(directory_path, item), bucket_name, bucket_path)
            print 'successfully upload new files, path bucket {} files : {}'.format(bucket_path, item)



def upload_specified_file(bucket_name, brand_id, directory_path, s3, specified_files):
    for item in specified_files:
        bucket_path = '{}/{}'.format(brand_id, item)
        file_exist = check_exist_not(s3, bucket_name, bucket_path)

        if file_exist:
            # croscek size?!
            old_size = os.path.getsize(os.path.join(directory_path, item))
            if old_size != file_exist.get('ContentLength'):
                s3.upload_file(os.path.join(directory_path, item), bucket_name, bucket_path)
                print 'successfully upload new files, path bucket {} files : {}'.format(bucket_path, item)
            else:
                # File are same.. nothing changes
                pass
        else:
            s3.upload_file(os.path.join(directory_path, item), bucket_name, bucket_path)
            print 'successfully upload new files, path bucket {} files : {}'.format(bucket_path, item)

        # if bucket_name == app.config['BOTO3_GRAMEDIA_ORIGINAL_BUCKET']:
        #     # remove files
        #     old_files = os.path.join(directory_path, item)
        #     if os.path.exists(old_files):
        #         os.remove(old_files)
        #         print 'successfully remove old files.'


def upload_files(brand_id=None, specified_files=None, biznet_path=None, bucket_name=None):
    '''
        :param brand_id: string
        :param specified_files: list
        :return:
    '''

    BUCKET_NAME, BIZNET_PATH = bucket_name, biznet_path

    # try connecting to S3
    s3 = boto3.client('s3',
        aws_access_key_id=app.config['BOTO3_GRAMEDIA_APP_ID'],
        aws_secret_access_key=app.config['BOTO3_GRAMEDIA_APP_SECRET'])

    try:
        directory_path = os.path.join(BIZNET_PATH, brand_id)
        if os.path.exists(directory_path) and os.path.isdir(directory_path):

            # check folder if existing on Bucket?
            folder_exist = check_exist_not(s3, BUCKET_NAME, '{}/'.format(brand_id))

            if folder_exist:
                # nothing to do.. skip
                pass
            else:
                # create new folder brand_id on bucket
                s3.put_object(Bucket=BUCKET_NAME, Key='{}/'.format(brand_id))

            if specified_files:
                upload_specified_file(BUCKET_NAME, brand_id, directory_path, s3, specified_files)
            else:
                upload_bulk_files(BUCKET_NAME, brand_id, directory_path, s3)

        else:
            print 'path not exist {}'.format(directory_path)

    except Exception as e:
        print 'Error {}'.format(e)
