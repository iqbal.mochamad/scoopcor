import boto3, os

from app import app


def connect_gramedia_s3():
    s3 = boto3.client('s3',
                      aws_access_key_id=app.config['BOTO3_GRAMEDIA_APP_ID'],
                      aws_secret_access_key=app.config['BOTO3_GRAMEDIA_APP_SECRET'])
    return s3


def aws_check_files(s3, bucket_name, bucket_path):
    try:
        file_exists = s3.head_object(Bucket=bucket_name, Key=bucket_path)
        return file_exists
    except Exception as e:
        return None


def aws_generate_link(s3, bucket_name, bucket_path):
    url = s3.generate_presigned_url(
        ClientMethod='get_object',
        Params={
            'Bucket': bucket_name,
            'Key': bucket_path},
        ExpiresIn=app.config['BOTO3_LINK_EXPIRED']  # second --> 1 hours
    )
    return url


def aws_shared_link(bucket, file_bucket):
    s3_server = connect_gramedia_s3()
    file_exists = aws_check_files(s3_server, bucket, file_bucket)
    if file_exists:
        url = aws_generate_link(s3_server, bucket, file_bucket)
        return url

    return None


def aws_upload_file(bucket_name, bucket_folder, transfer_file, output_file, is_public_access=False):
    try:
        s3_server = connect_gramedia_s3()
        banner_folder = aws_check_files(s3_server, bucket_name, bucket_folder)

        if is_public_access: acl = 'public-read'
        else: acl = None

        if not banner_folder:
            s3_server.put_object(Bucket=bucket_name, Key=bucket_folder, ACL=acl)

        print transfer_file, bucket_name, os.path.join(bucket_folder, output_file)
        s3_server.upload_file(transfer_file, bucket_name, os.path.join(bucket_folder, output_file),
            ExtraArgs={'ContentType': 'image/jpeg', 'ACL': acl})
    except Exception as e:
        print e
