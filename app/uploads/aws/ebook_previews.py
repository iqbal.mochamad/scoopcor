import boto3, os
from app import app


def check_exist_not(s3=None, bucket_name=None, key=None):
    try: file_exists = s3.head_object(Bucket=bucket_name, Key=key)
    except: file_exists = None
    return file_exists


def transfer_files_previews(directory_path=None, brand_id=None, item_id=None, bucket_name=None, preview_files=[]):

    s3 = boto3.client('s3',
        aws_access_key_id=app.config['BOTO3_GRAMEDIA_APP_ID'],
        aws_secret_access_key=app.config['BOTO3_GRAMEDIA_APP_SECRET'])

    directory_path = os.path.join(directory_path, brand_id)
    s3_count, biznet_count = 0, 0

    if os.path.exists(directory_path):
        # check brand_id folders
        brand_folder = check_exist_not(s3, bucket_name, '{}/'.format(brand_id))

        if brand_folder: pass
        else: s3.put_object(Bucket=bucket_name, Key='{}/'.format(brand_id), ACL='public-read')

        sub_folder = os.path.join(directory_path, str(item_id))
        folder_exist = check_exist_not(s3, bucket_name, '{}/{}/'.format(brand_id, item_id))

        if folder_exist: pass
        else:
            s3.put_object(Bucket=bucket_name, Key='{}/{}/'.format(brand_id, item_id), ACL='public-read')

        for file_web in preview_files:
            boto_path = '{}/{}/{}'.format(brand_id, item_id, file_web)
            file_exist = check_exist_not(s3, bucket_name, boto_path)

            if file_exist:
                pass
            else:
                data_file = os.path.join(sub_folder, file_web)
                if os.path.exists(data_file):
                    s3.upload_file(data_file, bucket_name, boto_path,
                                   ExtraArgs={'ContentType': 'image/jpeg', 'ACL': 'public-read'})
                    print 'OK.. uploaded. {}/{}/{}'.format(brand_id, item_id, file_web)
                else:
                    print 'not uploaded file has not found.. {}/{}/{}'.format(brand_id, item_id, file_web)
    else:
        print 'path not exist', directory_path
