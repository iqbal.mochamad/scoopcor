import boto3, os, subprocess
from app import app


def check_exist_not(s3=None, bucket_name=None, key=None):
    try: file_exists = s3.head_object(Bucket=bucket_name, Key=key)
    except: file_exists = None
    return file_exists


def transfer_files_webreader(directory_path=None, brand_id=None, bucket_name=None, item_id_list=[]):

    s3 = boto3.client('s3',
        aws_access_key_id=app.config['BOTO3_GRAMEDIA_APP_ID'],
        aws_secret_access_key=app.config['BOTO3_GRAMEDIA_APP_SECRET'])

    directory_path = os.path.join(directory_path, brand_id)
    s3_count, biznet_count = 0, 0

    if os.path.exists(directory_path):
        # check brand_id folders
        brand_folder = check_exist_not(s3, bucket_name, '{}/'.format(brand_id))

        if brand_folder: pass
        else: s3.put_object(Bucket=bucket_name, Key='{}/'.format(brand_id))

        for item in item_id_list:
        # for item in os.listdir(directory_path):
            sub_folder = os.path.join(directory_path, item)
            folder_exist = check_exist_not(s3, bucket_name, '{}/{}/'.format(brand_id, item))

            if os.path.isdir(sub_folder):

                if folder_exist:
                    # get size
                    list_of_objects = s3.list_objects(Bucket=bucket_name, Prefix='{}/{}/'.format(brand_id, item))
                    s3_count = len(list_of_objects['Contents']) - 1
                else:
                    # create sub folder <brand_id>/<item_id>
                    s3.put_object(Bucket=bucket_name, Key='{}/{}/'.format(brand_id, item))

                biznet_count = len(os.walk(sub_folder).next()[2])

                if biznet_count != s3_count:
                    for file_web in os.listdir(sub_folder):
                        boto_path = '{}/{}/{}'.format(brand_id, item, file_web)
                        file_exist = check_exist_not(s3, bucket_name, boto_path)

                        if file_exist:
                            old_size = os.path.getsize(os.path.join(sub_folder, file_web))
                            if old_size != file_exist.get('ContentLength'):
                                s3.upload_file(os.path.join(sub_folder, file_web), bucket_name, boto_path, ExtraArgs={'ContentType': 'image/jpeg'})
                        else:
                            s3.upload_file(os.path.join(sub_folder, file_web), bucket_name, boto_path, ExtraArgs={'ContentType': 'image/jpeg'})
                            print 'OK.. uploaded. {}/{}/{}'.format(brand_id, item, file_web)
    else:
        print 'path not exist', directory_path
