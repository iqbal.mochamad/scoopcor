from flask import Blueprint


from .api import UploadImageApi, ItemUploadApi, ItemUploadProcessing


upload_blueprint = Blueprint('upload_general', __name__, url_prefix='/v1/upload-general/images')
upload_blueprint.add_url_rule('', view_func=UploadImageApi.as_view('image_upload'))


item_uploads_blueprint = Blueprint('item_uploads', __name__, url_prefix='/v1/uploads')
item_uploads_blueprint.add_url_rule('', view_func=ItemUploadApi.as_view('item_upload'))
item_uploads_blueprint.add_url_rule('/item_processing', view_func=ItemUploadProcessing.as_view('item_processing'))
