import os, shutil, subprocess, paramiko
from datetime import datetime
from app.items.choices import ItemTypes
from app import app, db
from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files
from app.items.files.pdfs import web_reader_batch_extractor_factory, thumbnail_batch_extractor_factory


class UploadWebReaderProcessing(object):

    def __init__(self, item=None, upload_data=None):
        self.item = item
        self.upload_data = upload_data
        self.original_bucket = app.config['BOTO3_GRAMEDIA_ORIGINAL_BUCKET']

        self.s3_server = connect_gramedia_s3()
        self.mod = 0777

        self.path_file = os.path.join(self.upload_data.file_dir, self.upload_data.file_name)
        self.original_brand_path = os.path.join(app.config['BOTO3_BIZNET_ORIGINAL'], str(self.item.brand_id))
        self.web_reader_path = os.path.join(app.config['BOTO3_BIZNET_WEBREADER'], str(self.item.brand_id), str(self.item.id))
        self.thumb_reader_path = os.path.join(app.config['BOTO3_BIZNET_THUMBNAIL'], str(self.item.brand_id), str(self.item.id))


    def update_upload_data(self, msg_error):
        self.upload_data.error = msg_error
        self.upload_data.status = 50
        db.session.add(self.upload_data)
        db.session.commit()

    def retrieve_files(self):
        try:
            # create folders.
            if not os.path.isdir(self.upload_data.file_dir): os.makedirs(self.upload_data.file_dir, self.mod)
            if not os.path.exists(self.original_brand_path): os.makedirs(self.original_brand_path, self.mod)

            if not os.path.exists(self.path_file):
                # download from S3
                bucket_file = '{}/{}'.format(self.item.brand_id, self.upload_data.file_name)
                check_files = aws_check_files(self.s3_server, self.original_bucket, bucket_file)

                if not check_files:
                    return False, 'Files {} not exist on bucket {}'.format(bucket_file, self.original_bucket)
                else:
                    self.s3_server.download_file(self.original_bucket, bucket_file, self.path_file)
                    file_size = os.path.getsize(self.path_file)
                    if file_size != self.upload_data.file_size:
                        return False, 'File not uploaded properly, mismatch of size'
            else:
                file_size = os.path.getsize(self.path_file)
                if file_size != self.upload_data.file_size:
                    return False, 'File not uploaded properly, mismatch of size'

            shutil.copy(self.path_file, self.original_brand_path)

            # remove files on /tmp/files
            try:
                os.remove(self.path_file)
            except:
                pass

            return True, 'OK'

        except Exception as e:
            return False, 'retrieve_files Failed {}'.format(e)


    def extract_web_reader(self):
        try:
            high_res = self.item.item_type == ItemTypes.newspaper.value
            web_reader_batch_extractor_factory(self.item, app.static_folder, high_res).process()
            return True, 'OK'

        except Exception as e:
            return False, 'extract_web_reader Failed {}'.format(e)

    def extract_thumbnails_reader(self):
        try:
            thumbnail_batch_extractor_factory(self.item, app.static_folder).process()
            return True, 'OK'
        except Exception as e:
            return False, 'extract_thumbnails_reader Failed {}'.format(e)

    def connect_biznet(self, server):

        try:
            if server == '62001':
                host, port = app.config['SERVER_62001'], app.config['SERVER_62001_PORT']

            if server == 'scoopadm':
                host, port = app.config['SERVER_SCOOPADM'], app.config['SERVER_SCOOPADM_PORT']

            transport = paramiko.Transport((host, port))
            privkey = paramiko.RSAKey.from_private_key_file(app.config['PRIVATE_KEY'], app.config['SSH_PASSWORD'])
            transport.connect(username=app.config['SSH_USER_NAME'], pkey=privkey)

            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(host, port, username=app.config['SSH_USER_NAME'], password=app.config['SSH_PASSWORD'], pkey=privkey, timeout=10)

            sftp = paramiko.SFTPClient.from_transport(transport)
            sftp.sock.settimeout(5)
            return True, sftp

        except Exception as e:
            return False, 'Failed to connect_biznet {}'.format(e)


    def transfer_web_reader_s3_and_biznet(self):
        try:
            bucket_name = app.config['BOTO3_GRAMEDIA_WEBREADER_BUCKET']

            # check brand..
            brand_folder = aws_check_files(self.s3_server, bucket_name, '{}/'.format(self.item.brand_id))
            if not brand_folder:
                self.s3_server.put_object(Bucket=bucket_name, Key='{}/'.format(self.item.brand_id))

            # check brand/item_id
            item_folder = aws_check_files(self.s3_server, bucket_name, '{}/{}/'.format(self.item.brand_id, self.item.id))
            if not item_folder:
                self.s3_server.put_object(Bucket=bucket_name, Key='{}/{}/'.format(self.item.brand_id, self.item.id))

            # # BIZNETTTTTT =================
            # brand_server_path = os.path.join(app.config['BOTO3_BIZNET_WEBREADER'], str(self.item.brand_id))
            # item_server_path = os.path.join(app.config['BOTO3_BIZNET_WEBREADER'], str(self.item.brand_id), str(self.item.id))
            #
            # status, server = self.connect_biznet('scoopadm')
            # if not status:
            #     return False, server
            #
            # try: server.stat(brand_server_path)
            # except: server.mkdir(brand_server_path)
            #
            # try: server.stat(item_server_path)
            # except: server.mkdir(item_server_path)

            for image in os.listdir(self.web_reader_path):
                files_ = os.path.join(self.web_reader_path, image)
                self.s3_server.upload_file(files_, bucket_name, '{}/{}/{}'.format(self.item.brand_id,
                    self.item.id, image), ExtraArgs={'ContentType': 'image/jpeg'})

                # destination = os.path.join(item_server_path, image)
                # server.put(files_, destination)

                os.remove(files_)

            # server.close()
            return True, 'OK'

        except Exception as e:
            return False, 'transfer_web_reader Failed {}'.format(e)


    def transfer_thumbnails_s3_and_biznet(self):
        try:
            bucket_name = app.config['BOTO3_GRAMEDIA_THUMBNAILS_BUCKET']

            # check brand..
            brand_folder = aws_check_files(self.s3_server, bucket_name, '{}/'.format(self.item.brand_id))
            if not brand_folder:
                self.s3_server.put_object(Bucket=bucket_name, Key='{}/'.format(self.item.brand_id))

            # check brand/item_id
            item_folder = aws_check_files(self.s3_server, bucket_name, '{}/{}/'.format(self.item.brand_id, self.item.id))
            if not item_folder:
                self.s3_server.put_object(Bucket=bucket_name, Key='{}/{}/'.format(self.item.brand_id, self.item.id))

            # # BIZNETTTTTT =================
            # brand_server_path = os.path.join(app.config['BOTO3_BIZNET_THUMBNAIL'], str(self.item.brand_id))
            # item_server_path = os.path.join(app.config['BOTO3_BIZNET_THUMBNAIL'], str(self.item.brand_id), str(self.item.id))

            # status, server = self.connect_biznet('scoopadm')
            # if not status:
            #     return False, server
            #
            # try: server.stat(brand_server_path)
            # except: server.mkdir(brand_server_path)
            #
            # try: server.stat(item_server_path)
            # except: server.mkdir(item_server_path)

            for image in os.listdir(self.thumb_reader_path):
                files_ = os.path.join(self.thumb_reader_path, image)
                self.s3_server.upload_file(files_, bucket_name, '{}/{}/{}'.format(self.item.brand_id,
                    self.item.id, image), ExtraArgs={'ContentType': 'image/jpeg'})

                # destination = os.path.join(item_server_path, image)
                # server.put(files_, destination)
                os.remove(files_)

            # server.close()
            return True, 'OK'

        except Exception as e:
            return False, 'transfer_thumbnails_s3_and_biznet Failed {}'.format(e)


    def process(self):
        # retrieve files..
        status, message = self.retrieve_files()
        if not status: self.update_upload_data(message)

        status, message = self.extract_web_reader()
        if not status: self.update_upload_data(message)

        status, message = self.extract_thumbnails_reader()
        if not status: self.update_upload_data(message)

        status, message = self.transfer_web_reader_s3_and_biznet()
        if not status: self.update_upload_data(message)

        status, message = self.transfer_thumbnails_s3_and_biznet()
        if not status: self.update_upload_data(message)

        #update status to complete
        if status:
            # update item processing status.
            self.upload_data.status = 12 # Complete
            self.upload_data.end_process_time = datetime.now()

            db.session.add(self.upload_data)
            db.session.commit()

        # remove files
        files_ = os.path.join(self.original_brand_path, self.upload_data.file_name)
        os.remove(files_)
