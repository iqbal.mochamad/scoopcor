from enum import IntEnum, Enum


class ImageResourceType(IntEnum):
    item = 1
    offer = 2


class CoverTypes(Enum):
    """ Represents the different types of 'cover images' for an
    :class:`app.items.models.Item`.
    """
    cover_general_small = 'general_small_covers'
    cover_general = 'general_covers'
    cover_general_big = 'general_big_covers'
    cover_big = 'big_covers'