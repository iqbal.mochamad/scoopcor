import os, shutil, subprocess, paramiko
from datetime import datetime
from app import app, db
from app.eperpus.models import WebReaderLog
from app.items.choices import ItemUploadProcessStatus
from app.items.previews import preview_rule_factory
from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files
from PyPDF2 import PdfFileReader, PdfFileWriter

from app.items.models import ItemFile
from app.logs.models import ItemProcessing

from app.items.choices.file_type import FILE_PDF, FILE_EPUB, FILE_IMAGE
from app.uploads.helpers import generate_file_version

from app.cronjobs.helpers import PostItemSubs


class UploadPDFProcessing(object):

    def __init__(self, item=None, upload_data=None):
        self.item = item
        self.upload_data = upload_data
        self.original_bucket = app.config['BOTO3_GRAMEDIA_ORIGINAL_BUCKET']
        self.s3_server = connect_gramedia_s3()

        self.custom_zip_password = None
        self.custom_pdf_password = None
        self.custom_file_version = None
        self.status = []
        self.mod = 0777

        self.biznet_sftp = None

    def update_upload_data(self, msg_error):
        self.upload_data.error = msg_error
        self.upload_data.status = ItemUploadProcessStatus.failed.value
        db.session.add(self.upload_data)
        db.session.commit()

    def validate_size_raw(self, skip_check=False):
        try:
            # first check size raw on DIR?
            path_file = os.path.join(self.upload_data.file_dir, self.upload_data.file_name)

            if not os.path.exists(self.upload_data.file_dir):
                os.makedirs(self.upload_data.file_dir)

            if os.path.isfile(path_file):
                file_size = os.path.getsize(path_file)
                if skip_check:
                    pass
                else:
                    if file_size != self.upload_data.file_size:
                        return False, 'File not uploaded properly, mismatch of size'
            else:
                # raw file not exists, get from S3
                bucket_file = '{}/{}'.format(self.item.brand_id, self.upload_data.file_name)
                check_files = aws_check_files(self.s3_server, self.original_bucket, bucket_file)

                if not check_files:
                    return False, 'Files {} not exist on bucket {}'.format(bucket_file, self.original_bucket)
                else:
                    self.s3_server.download_file(self.original_bucket, bucket_file, path_file)
                    file_size = os.path.getsize(path_file)
                    if skip_check:
                        pass
                    else:
                        if file_size != self.upload_data.file_size:
                            return False, 'File not uploaded properly, mismatch of size'

            return True, 'OK'
        except Exception as e:
            return False, 'Error validate_size_raw {}'.format(e)

    def move_process_temp_files(self):
        parent_path = os.path.join(app.config['UPLOAD_ITEM_TEMP'], self.item.edition_code)
        thumbs_path_file = os.path.join(parent_path, 'thumbs')
        if not os.path.exists(thumbs_path_file):
            os.makedirs(thumbs_path_file, self.mod)

        if not os.path.exists(os.path.join(parent_path, self.upload_data.file_name)):
            shutil.copy(os.path.join(self.upload_data.file_dir, self.upload_data.file_name), parent_path)

    def create_password(self):
        try:
            status, self.custom_zip_password, self.custom_pdf_password, self.custom_file_version = generate_file_version(self.item)
            if not status:
                return False, self.custom_zip_password
            return True, 'OK'

        except Exception as e:
            return False, 'create_password Failed {}'.format(e)

    def connect_biznet(self, server):

        try:
            if server == '62001':
                host, port = app.config['SERVER_62001'], app.config['SERVER_62001_PORT']

            if server == 'scoopadm':
                host, port = app.config['SERVER_SCOOPADM'], app.config['SERVER_SCOOPADM_PORT']

            transport = paramiko.Transport((host, port))
            privkey = paramiko.RSAKey.from_private_key_file(app.config['PRIVATE_KEY'], app.config['SSH_PASSWORD'])
            transport.connect(username=app.config['SSH_USER_NAME'], pkey=privkey)

            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(host, port, username=app.config['SSH_USER_NAME'], password=app.config['SSH_PASSWORD'], pkey=privkey, timeout=10)

            sftp = paramiko.SFTPClient.from_transport(transport)
            sftp.sock.settimeout(5)
            return True, sftp

        except Exception as e:
            return False, 'Failed to connect_biznet {}'.format(e)

    def upload_cover_s3(self, cover_path, item):
        try:
            bucket_name = app.config['BOTO3_GRAMEDIA_COVERS_BUCKET']
            brand_folder = aws_check_files(self.s3_server, bucket_name,
                '{}/{}/'.format(self.item.brand_id, cover_path))

            if not brand_folder:
                self.s3_server.put_object(
                    Bucket=bucket_name, Key='{}/{}/'.format(self.item.brand_id, cover_path), ACL='public-read')

            self.s3_server.upload_file(item, bucket_name, '{}/{}/{}.jpg'.format(self.item.brand_id,
                cover_path, self.item.edition_code), ExtraArgs={'ContentType': 'image/jpeg', 'ACL': 'public-read'})
            return True, 'OK'

        except Exception as e:
            return False, 'upload_cover_s3 Failed {}'.format(e)

    def upload_preview_s3(self, preview_path, page_num):
        try:
            bucket_name = app.config['BOTO3_GRAMEDIA_PREVIEW_BUCKET']
            brand_folder = aws_check_files(self.s3_server, bucket_name, '{}/{}/'.format(self.item.brand_id, self.item.id))

            if not brand_folder:
                self.s3_server.put_object(
                    Bucket=bucket_name, Key='{}/{}/'.format(self.item.brand_id, self.item.id), ACL='public-read')

            self.s3_server.upload_file(preview_path, bucket_name, '{}/{}/{}.jpg'.format(self.item.brand_id,
                self.item.id, page_num), ExtraArgs={'ContentType': 'image/jpeg', 'ACL': 'public-read'})

            return True, 'OK'

        except Exception as e:
            return False, 'upload_preview_s3 Failed {}'.format(e)

    def upload_cover_biznet(self, cover_path, item):
        try:
            server_path = os.path.join(app.config['SERVER_SCOOPADM_PATH'], str(self.item.brand_id))
            status, server = self.connect_biznet('scoopadm')

            if not status: return False, server
            try:
                server.stat(server_path)
            except:
                server.mkdir(server_path)

            upload_path = os.path.join(server_path, cover_path)
            try:
                server.stat(upload_path)
            except:
                server.mkdir(upload_path)

            destination = os.path.join(upload_path, '{}.jpg'.format(self.item.edition_code))
            server.put(item, destination)
            server.close()
            return True, 'OK'

        except Exception as e:
            return False, 'upload_cover_biznet Error {} {}'.format(e, cover_path)

    def upload_zip_s3(self, item):
        try:
            bucket_name = app.config['BOTO3_GRAMEDIA_MOBILE_BUCKET']
            brand_folder = aws_check_files(self.s3_server, bucket_name, '{}/'.format(self.item.brand_id))
            zip_file = '{}/{}.zip'.format(self.item.brand_id,self.item.edition_code)

            if not brand_folder:
                self.s3_server.put_object(Bucket=bucket_name, Key='{}/'.format(self.item.brand_id))

            self.s3_server.upload_file(item, bucket_name, zip_file)

            # # TODO : Uncomment this code for deliver zip file to biznet. please dont do that again.. - kiratakada -
            # try:
            #     server_path = os.path.join(app.config['SERVER_62001_PATH'], str(self.item.brand_id))
            #     destination_dir = os.path.join(server_path, '{}.zip'.format(self.item.edition_code))
            #
            #     status, server = self.connect_biznet(server='62001')
            #     if not status:
            #         return False, server
            #
            #     try:
            #         server.stat(server_path)
            #     except:
            #         server.mkdir(server_path)
            #
            #     if os.path.exists(item):
            #         server.put(item, destination_dir)
            #         server.close()
            #
            # except Exception as e:
            #     return False, 'upload zip to biznet error {}'.format(e)

            os.remove(item)

            return True, 'OK'

        except Exception as e:
            return False, 'upload_zip_s3 Failed {}'.format(e)

    def create_cover(self):
        try:
            pdf_file = os.path.join(self.upload_data.file_dir, self.upload_data.file_name)
            new_name = '%s.jpg' % (self.item.edition_code.replace(' ', '_'))

            cover_data = ['thumb_image_normal', 'thumb_image_highres', 'image_normal', 'image_highres']
            for icover in cover_data:
                cover_path = os.path.join(app.config['COVER_ITEM_BASE'], str(self.item.brand_id), icover)
                data_cover = os.path.join(cover_path, new_name)

                if not os.path.exists(cover_path): os.makedirs(cover_path, self.mod)

                if icover == 'thumb_image_normal':
                    process = 'convert -resize {}x -density {} -define pdf:use-cropbox=true -background white -flatten -colorspace RGB {}[0] {}'.format(app.config['GEN_SM_COVER'], 250,
                        pdf_file, os.path.join(cover_path, new_name))
                    self.item.thumb_image_normal = 'images/1/{}/{}/{}'.format(self.item.brand_id, icover, new_name)

                if icover == 'thumb_image_highres':
                    process = 'convert -resize {}x -density {} -define pdf:use-cropbox=true -background white -flatten -colorspace RGB {}[0] {}'.format(
                        app.config['GEN_COV'], 250, pdf_file, os.path.join(cover_path, new_name))
                    self.item.thumb_image_highres = 'images/1/{}/{}/{}'.format(self.item.brand_id, icover, new_name)

                if icover == 'image_normal':
                    process = 'convert -resize {}x -density {} -define pdf:use-cropbox=true -background white -flatten -colorspace RGB {}[0] {}'.format(
                        app.config['GEN_BIG_COV'], 250, pdf_file, os.path.join(cover_path, new_name))
                    self.item.image_normal = 'images/1/{}/{}/{}'.format(self.item.brand_id, icover, new_name)

                if icover == 'image_highres':
                    process = 'convert -resize {}x -density {} -define pdf:use-cropbox=true -background white -flatten -colorspace RGB {}[0] {}'.format(
                        app.config['BIG_COV'], 250, pdf_file, os.path.join(cover_path, new_name))
                    self.item.image_highres = 'images/1/{}/{}/{}'.format(self.item.brand_id, icover, new_name)

                # processing command line --> convert
                subprocess.call(process, shell=True)
                db.session.add(self.item)
                db.session.commit()

                status, message = self.upload_cover_s3(cover_path=icover, item=data_cover)

                # TODO : Uncomment this if cover allowed transfer to BIZNET again..
                #status, message = self.upload_cover_biznet(cover_path=icover, item=data_cover)

                try: os.remove(data_cover)
                except: pass

                if not status: return status, message

            return True, 'OK'

        except Exception as e:
            return False, 'create_cover Failed {}'.format(e)

    def create_preview(self):
        try:
            pdf_file = os.path.join(self.upload_data.file_dir, self.upload_data.file_name)
            preview_path = os.path.join(app.config['PREVIEW_ITEM_BASE'], str(self.item.brand_id), str(self.item.id))

            if not os.path.exists(preview_path): os.makedirs(preview_path, self.mod)

            # get page limit
            pdf_read = PdfFileReader(file(pdf_file, "rb"))
            total_page = pdf_read.numPages

            # update items total page..
            self.item.page_count = total_page
            db.session.add(self.item)
            db.session.commit()

            # get rule of previews pages..
            rules = preview_rule_factory(self.item)

            for page in rules.get_page_numbers():
                preview_file = os.path.join(preview_path, '{}.jpg'.format(page))
                process = 'convert -define pdf:use-cropbox=true -density 150 -depth 8 ' \
                          '-alpha on -background white -flatten -colorspace RGB {}[{}] {}'.format(pdf_file, page-1, preview_file)
                subprocess.call(process, shell=True)
                status, message = self.upload_preview_s3(preview_file, page)
                if not status:
                    return status, message

                try:
                    os.remove(preview_file)
                except:
                    pass

            return True, 'OK'

        except Exception as e:
            return False, 'create_preview Failed {}'.format(e)

    def create_thumbnails_pdf(self):
        try:
            base_temp_pdf = os.path.join(app.config['UPLOAD_ITEM_TEMP'], self.item.edition_code)
            temp_pdf_dir = os.path.join(base_temp_pdf, '{}.pdf'.format(self.item.edition_code))

            # get page limit
            pdf_read = PdfFileReader(file(temp_pdf_dir, "rb"))
            page_data = range(0, int(pdf_read.numPages))

            output = PdfFileWriter()
            input = PdfFileReader(file(temp_pdf_dir, "rb"))

            # Extract thumbnails
            for page in page_data:

                # frontend cant access 0.png, must start from 1
                data_page = int(page) + 1

                thumbs_dir = os.path.join(app.config['UPLOAD_ITEM_TEMP'], self.item.edition_code, 'thumbs')
                image_name = os.path.join(thumbs_dir, '%s.png' % data_page)
                path_call = 'convert -density {} -fill white -opaque none -define pdf:use-cropbox=true -background white -flatten {}[{}] {}'.format(
                    app.config['DENSITY_DPI'], temp_pdf_dir, page, image_name)

                subprocess.call(path_call, shell=True)
                output.addPage(input.getPage(page))

            output.encrypt(self.custom_pdf_password)
            print self.custom_pdf_password

            output_dir = os.path.join(base_temp_pdf, 'magazine.pdf')
            outputStream = file(output_dir, "wb")
            output.write(outputStream)
            outputStream.close()

            # remove temporary file magazine1.pdf
            os.remove(temp_pdf_dir)

            # update items
            self.item.page_count = len(page_data)
            db.session.add(self.item)
            db.session.commit()

            return True, 'OK'

        except Exception as e:
            return False, 'create_thumbnails_pdf Failed {}'.format(e)

    def create_zip_files(self):
        try:
            pdf_dir = os.path.join(app.config['UPLOAD_ITEM_TEMP'], self.item.edition_code)
            thumbs_dir = os.path.join(pdf_dir, 'thumbs')

            zip_path = "{}{}{}".format(app.config['UPLOAD_ITEM_TEMP'], self.item.edition_code, '.zip')

            args = ['7za', 'a', '-p' + self.custom_zip_password] + [zip_path] + [pdf_dir + '/magazine.pdf', thumbs_dir]
            subprocess.call(args)
            args_command = ['du', zip_path]

            p = subprocess.Popen(args_command, stdout=subprocess.PIPE)
            original_zip_file_sizes = p.stdout.readlines()

            for item in original_zip_file_sizes:
                item_buffer = item.split('\t')

            if os.path.exists(zip_path):
                self.item.file_size = os.path.getsize(zip_path)
                db.session.add(self.item)
                db.session.commit()

            # remove old folder
            shutil.rmtree(pdf_dir)

            # upload file zip to S3
            return self.upload_zip_s3(zip_path)

        except Exception as e:
            return False, 'create_zip_files Failed {}'.format(e)

    def create_item_files(self):
        try:
            # delete the old ItemFile
            old_itemfiles = ItemFile.query.filter_by(item_id=self.item.id).all()

            for old_data in old_itemfiles:
                old_data.delete()

            if self.item.content_type == 'pdf': file_type = FILE_PDF
            if self.item.content_type == 'epub': file_type = FILE_EPUB
            if self.item.content_type == 'image': file_type = FILE_IMAGE

            new_itemfiles = ItemFile(
                item_id = self.item.id,
                file_type = file_type,
                file_name = 'images/1/{}/{}.zip'.format(self.item.brand_id, self.item.edition_code),
                file_order = 1,
                file_status = 1,
                file_version = 1,
                is_active = True,
                key = self.custom_file_version
            )
            db.session.add(new_itemfiles)
            db.session.commit()
            return True, 'OK'

        except Exception as e:
            return False, 'create_item_files Failed {}'.format(e)

    def process(self):
        status, message = self.validate_size_raw()
        if not status: self.update_upload_data(message)
        self.status.append(status)

        self.move_process_temp_files()
        print 'Phase 1, Create password, create cover'
        status, message = self.create_password()
        if not status: self.update_upload_data(message)
        self.status.append(status)

        status, message = self.create_cover()
        if not status: self.update_upload_data(message)
        self.status.append(status)

        status, message = self.create_preview()
        if not status: self.update_upload_data(message)
        self.status.append(status)

        print 'Phase 2, Create thumbnails pdf, create zip files'
        status, message = self.create_thumbnails_pdf()
        if not status: self.update_upload_data(message)
        self.status.append(status)

        status, message = self.create_zip_files()
        if not status: self.update_upload_data(message)
        self.status.append(status)

        print 'Phase 3, Create item files, update status.'
        status, message = self.create_item_files()
        if not status: self.update_upload_data(message)
        self.status.append(status)
        print self.status

        if False not in self.status:
            # update item processing status.
            self.upload_data.status = ItemUploadProcessStatus.complete.value # Complete
            self.upload_data.end_process_time = datetime.now()

            exist_process = ItemProcessing.query.filter_by(item_id=self.item.id).all()
            for data in exist_process:
                db.session.delete(data)
                db.session.commit()

            # create item processing.
            item_processing = ItemProcessing(item_id=self.item.id, brand_id = self.item.brand_id,
                                             status = ItemUploadProcessStatus.new.value)
            db.session.add(item_processing)
            db.session.add(self.upload_data)

            # create item for webreader
            web_reader = WebReaderLog(item_id=self.item.id, is_processed=False, is_web_reader=False, error='',
                                      created=datetime.now(), modified=datetime.now())
            db.session.add(web_reader)
            db.session.commit()

            PostItemSubs(module='kiratakada', item_id=self.item.id).construct()
            print 'All Clear...'
