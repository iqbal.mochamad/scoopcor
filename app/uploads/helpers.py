import hashlib
import os
from datetime import datetime

import paramiko
from sqlalchemy.sql import desc

from app import app
from app.items.choices.item_type import ITEM_TYPES, BOOK, MAGAZINE, NEWSPAPER
from app.items.choices.text_direction import READING_DIRECTIONS
from app.items.models import Item, ItemFile
from app.offers.models import Offer, OfferPlatform
from app.send_mail import sendmail_smtp, sendmail_aws
from app.tiers.models import TierAndroid


def connect_custom_server(server=None):
    """
        connect servers via paramiko
    """

    try:
        print 'CONNECTING TO SERVER : ', server

        # split server configurations
        if server == '62001':
            host = app.config['SERVER_62001']
            port = app.config['SERVER_62001_PORT']

        if server == 'scoopadm':
            host = app.config['SERVER_SCOOPADM']
            port = app.config['SERVER_SCOOPADM_PORT']

        if server == 'static':
            host = app.config['SERVER_STATIC']
            port = app.config['SERVER_STATIC_PORT']

        print 'EXECUTE KEY', app.config['PRIVATE_KEY']
        print 'TRY CONNECTING..', host, port

        # Execute paramiko transfers files
        transport = paramiko.Transport((host, port))
        privkey = paramiko.RSAKey.from_private_key_file(
            app.config['PRIVATE_KEY'], app.config['SSH_PASSWORD'])

        transport.connect(username=app.config['SSH_USER_NAME'], pkey=privkey)

        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(host, port, username=app.config['SSH_USER_NAME'],
            password=app.config['SSH_PASSWORD'], pkey=privkey, timeout=10)

        sftp = paramiko.SFTPClient.from_transport(transport)
        sftp.sock.settimeout(5)

        return True, sftp

    except Exception, e:
        return False, 'CONNECT SERVER  %s' % e

def sftp_send_files(sftp=None, source_dir=None, destination_dir=None, server_path=None):

    try:
        # recheck folders ready, not exist create a new one
        try:
            sftp.stat(server_path)
        except:
            try:
                sftp.mkdir(server_path)
            except Exception, e:
                # close sftp if failed
                sftp.close()
                return False, "MKDIR DIRECTORY 62001 FAILED : %s" % e

        try:
            sftp.put(source_dir, destination_dir)
        except Exception, e:
            # close sftp if failed
            sftp.close()
            return False, 'PUT FILE TO SERVER 62001 FAILED : %s' % e

        # remove old pdf
        os.remove(source_dir)

        # close sftp if done
        sftp.close()

        return True, 'OK'

    except Exception, e:
        return False, 'SFTP SEND FILES FAILED %s' % e

def generate_file_version(item=None):
    try:
        custom_zip_password, custom_pdf_password, custom_file_version = '', '', ''

        # Generate File Version.
        file_version1 = '{}{}{}{}'.format(item.id, item.edition_code,
            datetime.now().strftime('%Y-%m-%d %H:%M:%S'), item.release_date)

        file_version2 = '{}{}{}'.format(item.brand_id, '0.99', datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

        version1 = hashlib.md5(file_version1).hexdigest()
        version2 = hashlib.md5(file_version2).hexdigest()
        file_version = '{}{}'.format(version1, version2)

        # This format come from SC1 formula
        pass_zip = "{}{}{}{}{}{}{}".format(item.id, '~#%&', app.config['GARAMZIP'],
            '*&#@!', item.brand_id, '&%^)', file_version)

        pass_pdf = "{}{}{}{}{}{}{}".format(item.id, '~#%&', app.config['GARAMPDF'],
            '*&#@!', item.brand_id, '&%^)', file_version)

        custom_zip_password = hashlib.sha256(pass_zip).hexdigest()
        custom_pdf_password = hashlib.sha256(pass_pdf).hexdigest()
        custom_file_version = file_version

        return (True, custom_zip_password, custom_pdf_password, custom_file_version)

    except Exception, e:
        return (False, 'generate_file_version Failed : {}'.format(e), '', '')


def upload_email(item=None, upload_data=None, error_message=None):

    subject = "ERROR: Process Item %s (item_id: %s) Error" % (item.id, item.content_type)
    sender = '%s-upload-item <no-reply@gramedia.com>' % app.config['ADMIN_SERVER']

    content = """
        There's error when processing upload files, please immediately check server logs for the details. <br><br>

        Item : [ %s ] - %s <br><br>

        Upload PIC : [ %s ] - %s <br>
        Upload time : %s <br><br>

        Release date : %s <br>
        Release schedule : %s <br>

        Detail Error : %s <br><br>

    """ % (
        item.id, item.name,
        upload_data.pic_id, upload_data.pic_name,
        datetime.strftime(upload_data.created, '%Y-%m-%d %H:%M:%S'),
        datetime.strftime(item.release_date, '%Y-%m-%d %H:%M:%S'),
        datetime.strftime(upload_data.release_schedule, '%Y-%m-%d %H:%M:%S'),
        error_message
    )

    try:
        print 'send aws..'
        sendmail_aws(subject, sender, app.config['ADMIN_EMAILS'], content)
    except:
        print 'send smtp..'
        sendmail_smtp(subject, sender, app.config['ADMIN_EMAILS'], content)

def check_size_raw(item=None, upload_data=None):

    try:
        base_upload = app.config['UPLOAD_ITEMS_BASE']

        if item.item_type == ITEM_TYPES[MAGAZINE]:
            path_base = os.path.join(base_upload, 'magazine')
        if item.item_type == ITEM_TYPES[BOOK]:
            path_base = os.path.join(base_upload, 'book')
        if item.item_type == ITEM_TYPES[NEWSPAPER]:
            path_base = os.path.join(base_upload, 'newspaper')

        file_raw = os.path.join(path_base, upload_data.file_name)
        raw_files = file_raw

        # check file raw exist.
        if os.path.exists(file_raw):
            file_size = os.path.getsize(file_raw)

            # this is for check if rsync not finished yet
            if file_size != upload_data.file_size:
                upload_data.file_check = upload_data.file_check + 1
                upload_data.save()
            else:
                # Mark status become 2 ( Processings files )
                upload_data.status = 2
                upload_data.start_process_time = datetime.now()
                upload_data.save()

                return True, 'CHECKSUM OK'

            # retry check upload file 3 times check
            if upload_data.file_check >= 3:
                mssge = ('Raw file size and checksum [item %s] - %s mismatch : %s,'
                    'rsync copy not complete' % (item.id,
                    upload_data.file_name, file_size))

                upload_data.error = mssge
                upload_data.save()
                allow_process_item = False
                return False, mssge
        else:
            return False, '%s File not exist' % file_raw

    except Exception, e:
        return False, 'CHECK SIZE RAW FAILED : ', e

def contruct_sc1_response(item=None):

    try:

        data = {}
        data['id'] = item.id
        data['brand_id'] = item.brand_id
        data['name'] = item.name
        data['issue_number'] = item.issue_number
        data['edition_code'] = item.edition_code
        data['thumb_image_normal'] = item.thumb_image_normal
        data['thumb_image_highres'] = item.thumb_image_highres
        data['image_highres'] = item.image_highres
        data['image_normal'] = item.image_normal
        data['is_active'] = item.is_active
        data['reading_direction'] = {READING_DIRECTIONS[k]: k for k in READING_DIRECTIONS}[item.reading_direction]
        data['content_type'] = item.content_type
        data['created'] = datetime.strftime(item.release_date, '%Y-%m-%d %H:%M:%S')
        data['release_date'] = datetime.strftime(item.release_date, '%Y-%m-%d')
        data['release_schedule'] = None
        data['gtin8'] = item.gtin8
        data['gtin13'] = item.gtin13
        data['gtin14'] = item.gtin14
        data['release_schedule'] = None
        data['description'] = str(item.description)

        authors = [i.id for i in item.authors]

        if authors:
            data['author_id'] = authors[0]
        else:
            data['author_id'] = None

        # item files:
        item_file = ItemFile.query.filter_by(item_id=item.id).order_by(desc(ItemFile.id)).first()

        if not item_file:
            return False, "No ItemFiles founds for item_id : %s" % item.id
        else:
            data['item_file_name'] = item_file.file_name
            data['item_file_key'] = item_file.key

        offer = Offer.query.join(Item.core_offers).filter(
            Item.id.in_([item.id])).filter_by(offer_status=7).order_by(
            desc(Offer.id)).first()

        item_type = {ITEM_TYPES[k]: k for k in ITEM_TYPES}[item.item_type]
        if offer:
            if offer.is_free:
                data['ios_tier'] = None
                data['android_tier'] = None
                data['price_usd'] = "0"
                data['price_idr'] = "0"
                data['price_point'] = 0
            else:
                android_offer = OfferPlatform.query.filter_by(offer_id=offer.id,
                    platform_id=2).order_by(desc(OfferPlatform.id)).first()

                ios_offer = OfferPlatform.query.filter_by(offer_id=offer.id,
                    platform_id=1).order_by(desc(OfferPlatform.id)).first()

                if not android_offer:
                    return False, "No Android Offer founds for item_id : %s" % item.id

                if not ios_offer:
                    return False, "No IOS Offer founds for item_id : %s" % item.id

                if item_type == MAGAZINE:
                    data['ios_tier'] = ios_offer.tier_code
                    data['android_tier'] = android_offer.tier_code
                    data['price_usd'] = '%.2f' % offer.vendor_price_usd
                    data['price_idr'] = str(int(offer.vendor_price_idr))
                    data['price_point'] = offer.vendor_price_point

                # Special case for books ( Reduced book )
                if item_type == BOOK:
                    android_tier = TierAndroid.query.filter_by(tier_price=offer.vendor_price_usd).first()

                    if not android_tier:
                        return False, "No Android Tier founds for vendor price usd: %s" % offer.vendor_price_usd
                    else:
                        data['ios_tier'] = ios_offer.tier_code
                        data['android_tier'] = android_tier.tier_code # (get original tiers before cut reduce book)

                        data['price_usd'] = '%.2f' % offer.vendor_price_usd
                        data['price_idr'] = str(int(offer.vendor_price_idr))
                        data['price_point'] = offer.vendor_price_point

        else:
            # Newspaper exclude,all offers newspaper are subs and free only
            if item_type != NEWSPAPER:
                return False, "No offers founds for item_id : %s" % item.id

        return True, data

    except Exception, e:
        return False, "Construct json sc1 FAILED : %s" % e
