

class ChecksumMismatchError(Exception):
    def __init__(self, item, actual_file_size):
        """

        :param `app.items.models.Item` item:
        :param `int` actual_file_size:
        :return:
        """
        self.item = item
        self.actual_file_size = actual_file_size

    @property
    def expected_file_size(self):
        return self.item.upload_process.file_size

    @property
    def temp_filename(self):
        return self.item.upload_process.file_name

    def __str__(self):
        pass


class ChecksumMismatchFatal(ChecksumMismatchError):
    """ Raise when the ItemFile sizes do not match, and no more retries are
    available.
    """
    def __str__(self):
        pass
        # Raw file size and checksum [item %s] - %s mismatch : %s,'
        #         'rsync copy not complete' % (self.item.id,
        #         self.upload_data.file_name, actual_file_size))
