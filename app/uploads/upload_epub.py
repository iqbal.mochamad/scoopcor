import os, shutil, zipfile, subprocess, hashlib, paramiko
from datetime import datetime

from app import app, db
from app.items.choices.file_type import FILE_PDF, FILE_EPUB, FILE_IMAGE
from app.logs.models import ItemProcessing

from app.items.models import ItemFile
from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files
from app.uploads.helpers import generate_file_version

from app.cronjobs.helpers import PostItemSubs

class UploadEpubProcessing():

    def __init__(self, item=None, upload_data=None):
        self.item = item
        self.upload_data = upload_data
        self.original_bucket = app.config['BOTO3_GRAMEDIA_ORIGINAL_BUCKET']
        self.s3_server = connect_gramedia_s3()

        self.custom_zip_password = None
        self.custom_pdf_password = None
        self.custom_file_version = None
        self.hash_key = None
        self.mod = 0777

    def update_upload_data(self, msg_error):
        self.upload_data.error = msg_error
        self.upload_data.status = 11
        db.session.add(self.upload_data)
        db.session.commit()

    def validate_size_raw(self):
        try:
            # first check size raw on DIR?
            path_file = os.path.join(self.upload_data.file_dir, self.upload_data.file_name)

            if not os.path.exists(self.upload_data.file_dir):
                os.makedirs(self.upload_data.file_dir)

            if os.path.isfile(path_file):
                file_size = os.path.getsize(path_file)
                if file_size != self.upload_data.file_size:
                    return False, 'File not uploaded properly, mismatch of size'
            else:
                # raw file not exists, get from S3
                bucket_file = '{}/{}'.format(self.item.brand_id, self.upload_data.file_name)
                check_files = aws_check_files(self.s3_server, self.original_bucket, bucket_file)

                if not check_files:
                    return False, 'Files {} not exist on bucket {}'.format(bucket_file, self.original_bucket)
                else:
                    self.s3_server.download_file(self.original_bucket, bucket_file, path_file)
                    file_size = os.path.getsize(path_file)
                    if file_size != self.upload_data.file_size:
                        return False, 'File not uploaded properly, mismatch of size'
            return True, 'validate clear'

        except Exception as e:
            return False, 'Error validate_size_raw {}'.format(e)

    def modified_file(self):
        try:
            zip_name = 'magazine.zip'

            #create path temp <tmp>/<edition_code>
            modified_path = os.path.join(app.config['UPLOAD_ITEM_TEMP'], self.item.edition_code)
            if not os.path.exists(modified_path):
                os.makedirs(modified_path, self.mod)

            temp_dir = os.path.join(modified_path, zip_name)
            if not os.path.exists(temp_dir):
                os.makedirs(temp_dir, self.mod)

            # move original file to temp
            epub_source = os.path.join(self.upload_data.file_dir, self.upload_data.file_name)
            dest_source = os.path.join(temp_dir, self.item.edition_code)
            shutil.copy(epub_source, dest_source)

            # extract zip file
            zfile = zipfile.ZipFile(dest_source)
            zfile.extractall(os.path.join(modified_path, 'magazine'))

            # remove magazine.zip
            shutil.rmtree(temp_dir)

            return True, 'modified_file Success'

        except Exception, e:
            return False, 'modified_file Failed {}'.format(e)

    def create_password(self):
        try:
            status, self.custom_zip_password, self.custom_pdf_password, self.custom_file_version = generate_file_version(self.item)
            if not status:
                return False, self.custom_zip_password
            return True, 'create_password Success'

        except Exception as e:
            return False, 'create_password Failed {}'.format(e)

    def create_hashkey(self):

        try:
            santiang, salt_key = app.config['EPUB_SANTIANG'], app.config['EPUB_SALTKEY']

            # HASH KEY : item_id + saltkey + editioncode + santiang;
            loop_case = range(1, int(str(self.item.id)[-2:]) + 3)

            key_hash = str(self.item.id) + salt_key + str(self.item.edition_code) + santiang;

            # first hash_key
            self.hash_key = hashlib.sha256(key_hash).hexdigest()

            for i in loop_case:
                self.hash_key = hashlib.sha256(self.hash_key + salt_key).hexdigest()

            # get 32 digits hash key,
            # why 32, from SC1 hash key become key AES CBC, min : 16, max 32 digits
            self.hash_key = self.hash_key[:32]
            return True, 'create_hashkey Success'

        except Exception, e:
            return False, 'create_hashkey Failed {}'.format(e)

    def connect_biznet(self, server):

        try:
            if server == '62001':
                host, port = app.config['SERVER_62001'], app.config['SERVER_62001_PORT']

            if server == 'scoopadm':
                host, port = app.config['SERVER_SCOOPADM'], app.config['SERVER_SCOOPADM_PORT']

            transport = paramiko.Transport((host, port))
            privkey = paramiko.RSAKey.from_private_key_file(app.config['PRIVATE_KEY'], app.config['SSH_PASSWORD'])
            transport.connect(username=app.config['SSH_USER_NAME'], pkey=privkey)

            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(host, port, username=app.config['SSH_USER_NAME'], password=app.config['SSH_PASSWORD'], pkey=privkey, timeout=10)

            sftp = paramiko.SFTPClient.from_transport(transport)
            sftp.sock.settimeout(5)
            return True, sftp

        except Exception as e:
            return False, 'Failed to connect_biznet {}'.format(e)

    def create_zip(self, src=None, dst=None):

        try:
            zf = zipfile.ZipFile(dst, "w", zipfile.ZIP_DEFLATED)
            abs_src = os.path.abspath(src)
            for dirname, subdirs, files in os.walk(src):
                for filename in files:
                    absname = os.path.abspath(os.path.join(dirname, filename))
                    arcname = absname[len(abs_src) + 1:]
                    zf.write(absname, arcname)
            zf.close()
            return True, "create_zip Success"

        except Exception, e:
            return False, 'create_zip Failed {}'.format(e)

    def encrypt_data(self):
        try:
            status, message = self.create_hashkey()
            if not status:
                return status, message

            modified_path = os.path.join(app.config['UPLOAD_ITEM_TEMP'], self.item.edition_code)

            path = os.path.join(modified_path, 'magazine')
            path_out = os.path.join(modified_path, 'magazine.zip')

            # encrypt for html, htm etc
            for root, dirs, files in os.walk(path):
                for file_ in files:
                    fileName, fileExtension = os.path.splitext(file_)

                    if str(fileExtension) in app.config['EPUB_EXTENSIONS']:
                        file_in = os.path.join(root, file_)
                        file_out = os.path.join(root, 'kira_' + file_)

                        # encryptions ssl
                        process = "openssl enc -e -aes-128-cbc -K %s -p -iv 0 -nosalt -in %s -out %s" % (
                            self.hash_key, file_in, file_out)
                        subprocess.call(process, shell=True)

                        shutil.copy(file_out, file_in)
                        os.remove(file_out)

            status, message = self.create_zip(path, path_out)
            if not status:
                return status, message

            # remove old folder
            shutil.rmtree(path)

            # after encrypt rename folder magazine to magazine.epub
            epub_out = os.path.join(modified_path, 'magazine.epub')
            os.rename(path_out, epub_out)

            return True, 'encrypt_data Success'

        except Exception as e:
            return False, 'encrypt_data Failed {}'.format(e)

    def upload_zip_s3(self, item):
        try:
            bucket_name = app.config['BOTO3_GRAMEDIA_MOBILE_BUCKET']
            brand_folder = aws_check_files(self.s3_server, bucket_name, '{}/'.format(self.item.brand_id))

            if not brand_folder:
                self.s3_server.put_object(Bucket=bucket_name, Key='{}/'.format(self.item.brand_id))

            self.s3_server.upload_file(item, bucket_name, '{}/{}.zip'.format(self.item.brand_id,self.item.edition_code))

            # TODO : Remove this code
            # try:
            #     server_path = os.path.join(app.config['SERVER_62001_PATH'], str(self.item.brand_id))
            #     destination_dir = os.path.join(server_path, '{}.zip'.format(self.item.edition_code))
            #
            #     status, server = self.connect_biznet(server='62001')
            #     if not status:
            #         return False, server
            #
            #     try:
            #         server.stat(server_path)
            #     except:
            #         server.mkdir(server_path)
            #
            #     if os.path.exists(item):
            #         server.put(item, destination_dir)
            #         server.close()
            #
            # except Exception as e:
            #     return False, 'upload zip to biznet error {}'.format(e)

            os.remove(item)
            return True, 'OK'

        except Exception as e:
            return False, 'upload_zip_s3 Failed {}'.format(e)


    def create_zip_files(self):
        try:
            modified_path = os.path.join(app.config['UPLOAD_ITEM_TEMP'], self.item.edition_code)
            zip_path = "{}{}{}".format(app.config['UPLOAD_ITEM_TEMP'], self.item.edition_code, '.zip')

            args = ['7za', 'a', '-p' + self.custom_zip_password] + [zip_path] + [
                modified_path + '/magazine.epub']

            subprocess.call(args)

            args_command = ['du', zip_path]
            original_zip_file_size = 0

            p = subprocess.Popen(args_command, stdout=subprocess.PIPE)
            original_zip_file_sizes = p.stdout.readlines()

            for item in original_zip_file_sizes:
                item_buffer = item.split('\t')
                original_zip_file_size = float(item_buffer[0])

            if os.path.exists(zip_path):
                self.item.file_size = os.path.getsize(zip_path)
                db.session.add(self.item)
                db.session.commit()

            status, message = self.upload_zip_s3(zip_path)
            if not status:
                shutil.rmtree(modified_path)
                return status, message

            #remove old folder
            shutil.rmtree(modified_path)
            return True, 'create_zip_files Success'

        except Exception, e:
            return False, 'create_zip_files Failed {}'.format(e)

    def create_item_files(self):
        try:
            # delete the old ItemFile
            old_itemfiles = ItemFile.query.filter_by(item_id=self.item.id).all()

            for old_data in old_itemfiles:
                old_data.delete()

            if self.item.content_type == 'pdf': file_type = FILE_PDF
            if self.item.content_type == 'epub': file_type = FILE_EPUB
            if self.item.content_type == 'image': file_type = FILE_IMAGE

            new_itemfiles = ItemFile(
                item_id=self.item.id,
                file_type=file_type,
                file_name='images/1/{}/{}.zip'.format(self.item.brand_id, self.item.edition_code),
                file_order=1,
                file_status=1,
                file_version=1,
                is_active=True,
                key=self.custom_file_version
            )
            db.session.add(new_itemfiles)
            db.session.commit()
            return True, 'OK'

        except Exception as e:
            return False, 'create_item_files Failed {}'.format(e)


    def process(self):
        status, message = self.validate_size_raw()
        if not status: self.update_upload_data(message)

        status, message = self.modified_file()
        if not status: self.update_upload_data(message)

        status, message = self.create_password()
        if not status: self.update_upload_data(message)

        status, message = self.encrypt_data()
        if not status: self.update_upload_data(message)

        status, message = self.create_zip_files()
        if not status: self.update_upload_data(message)

        status, message = self.create_item_files()
        if not status: self.update_upload_data(message)

        if status:
            # update item processing status.
            self.upload_data.status = 10 # Complete
            self.upload_data.end_process_time = datetime.now()

            # db.session.add(self.item)
            # create item processing.
            item_processing = ItemProcessing(
                item_id=self.item.id,
                brand_id=self.item.brand_id,
                status=1
            )
            db.session.add(item_processing)
            db.session.add(self.upload_data)
            db.session.commit()

            PostItemSubs('GOKIRA').construct()

            # remove raw files
            raw_file = os.path.join(self.upload_data.file_dir, self.upload_data.file_name)
            os.remove(raw_file)
