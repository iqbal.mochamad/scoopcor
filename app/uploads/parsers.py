from flask_appsfoundry.parsers import ParserBase, IntegerField, StringField

from .choices import ImageResourceType


class UploadImageParser(ParserBase):
    id = IntegerField(required=True)
    resource = IntegerField(required=True,
                            choices=(ImageResourceType.item,
                                     ImageResourceType.offer,))
    image = StringField(required=True)


class ItemUploadParser(ParserBase):
    file_dir = StringField()
    file_name = StringField()
    pic_name = StringField()

    role_id = IntegerField()
    pic_id = IntegerField()
    file_size = IntegerField()
    item_id = IntegerField()
