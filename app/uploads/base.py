import os
from abc import ABCMeta, abstractproperty, abstractmethod

from six import add_metaclass, text_type
from subprocess32 import check_call

from app.uploads.choices import CoverTypes


@add_metaclass(ABCMeta)
class UploadCommand(object):

    @abstractproperty
    def output_file_full_path(self):
        pass

    @abstractproperty
    def processing_command(self):
        pass

    @abstractmethod
    def process(self):
        pass


class CoverExtractorCommand(UploadCommand):

    __metaclass__ = ABCMeta

    def __init__(self, item, cover_type, input_file_path, output_base_dir,
                 pixel_density=300):
        """
        :param `app.items.models.Item` item:
            Item instance whom the cover image and PDFs belong to.
        :param `app.uploads.choices.CoverTypes` cover_type:
            Enum value representing the cover type that this instance
            will generate.
        :param `basestring` input_file_path:
            Absolute path to the PDF that will be processed for image data.
        :param `basestring` output_base_dir:
            Absolute path to the base directory where this cover image
            will be stored.  NOTE:  This class will automatically generate
            additional sub-folders from the base path.
        :return:
        """
        self.item = item
        self.cover_type = cover_type
        self.input_pdf_path = input_file_path
        self.output_base_dir = output_base_dir
        self.pixel_density = pixel_density

    def process(self):
        """ Converts the first page of the PDF to a JPEG.

        :rtype: `basestring`
        :return: The absolute path of the new JPEG.
        """
        if not os.path.exists(os.path.dirname(self.output_file_full_path)):
            os.makedirs(os.path.dirname(self.output_file_full_path))

        check_call(self.processing_command, shell=True)

        return self.output_file_full_path

    @property
    def output_image_width(self):
        """ The width (in pixels) of the JPEG that we're going to render.

        :rtype: int
        """
        try:
            return {
                CoverTypes.cover_general_small: 178,
                CoverTypes.cover_general: 356,
                CoverTypes.cover_general_big: 380,
                CoverTypes.cover_big: 760,
            }[self.cover_type]
        except KeyError:
            raise ValueError("Invalid Cover Type: {}".format(self.cover_type))

    @property
    def processing_command(self):
        return ('convert '
                '-resize {self.output_image_width}x '
                '-density {self.pixel_density} '
                '{self.input_pdf_path}[0] '
                '{self.output_file_full_path}').format(self=self)

    @property
    def output_file_name(self):
        return '{normalized_edition_code}.jpg'.format(
            normalized_edition_code=self.item.edition_code.replace(' ', '_'))

    @property
    def output_file_full_path(self):
        """ The absolute file path where the generated JPEG should be saved.

        This includes the filename.

        :rtype: basestring
        :return: The absolute path of the output file.
        """
        return os.path.join(
            self.output_base_dir,
            text_type(self.item.brand_id),
            self.cover_type.value,
            self.output_file_name)