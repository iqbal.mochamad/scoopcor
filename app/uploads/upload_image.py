import httplib, os, subprocess, paramiko

from flask import Response
from app import app, db
from app.helpers import internal_server_message

from flask_appsfoundry.parsers.converters import ImageFieldParser

from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files


class UploadImageFtp():

    def __init__(self, data=None, args=None):
        self.data = data
        self.args = args
        self.resource = args.get('resource', None)
        self.image = args.get('image', None)
        self.mod = 0777

        self.cover_dir = ['thumb_image_normal', 'thumb_image_highres', 'image_normal', 'image_highres']
        self.original_bucket = app.config['BOTO3_GRAMEDIA_ORIGINAL_BUCKET']
        self.s3_server = connect_gramedia_s3()
        self.cover_data = []

    def upload_image(self, cover_path=None, raw_image=None):
        try:
            p = ImageFieldParser(cover_path)
            return True, p.from_base64(raw_image)

        except Exception, e:
            return False, e

    def connect_biznet(self, server):

        try:
            if server == '62001':
                host, port = app.config['SERVER_62001'], app.config['SERVER_62001_PORT']

            if server == 'scoopadm':
                host, port = app.config['SERVER_SCOOPADM'], app.config['SERVER_SCOOPADM_PORT']

            transport = paramiko.Transport((host, port))
            privkey = paramiko.RSAKey.from_private_key_file(app.config['PRIVATE_KEY'], app.config['SSH_PASSWORD'])
            transport.connect(username=app.config['SSH_USER_NAME'], pkey=privkey)

            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(host, port, username=app.config['SSH_USER_NAME'], password=app.config['SSH_PASSWORD'], pkey=privkey, timeout=10)

            sftp = paramiko.SFTPClient.from_transport(transport)
            sftp.sock.settimeout(5)
            return True, sftp

        except Exception as e:
            return False, 'Failed to connect_biznet {}'.format(e)

    def upload_cover_s3(self, cover_path, item, extension):
        try:
            bucket_name = app.config['BOTO3_GRAMEDIA_COVERS_BUCKET']
            brand_folder = aws_check_files(self.s3_server, bucket_name, '{}/{}/'.format(self.data.brand_id, cover_path))

            if not brand_folder:
                self.s3_server.put_object(Bucket=bucket_name,
                    Key='{}/{}/'.format(self.data.brand_id, cover_path), ACL='public-read')

            self.s3_server.upload_file(item, bucket_name, '{}/{}/{}.{}'.format(self.data.brand_id,
                cover_path, self.data.edition_code, extension), ExtraArgs={'ContentType': 'image/jpeg', 'ACL': 'public-read'})

            # TODO : REMOVE THIS UPLOAD BIZNET
            # try:
            #     server_path = os.path.join(app.config['SERVER_SCOOPADM_PATH'], str(self.data.brand_id))
            #     status, server = self.connect_biznet('scoopadm')
			#
            #     if not status: return False, server
            #     try:
            #         server.stat(server_path)
            #     except:
            #         server.mkdir(server_path)
			#
            #     upload_path = os.path.join(server_path, cover_path)
            #     try:
            #         server.stat(upload_path)
            #     except:
            #         server.mkdir(upload_path)
			#
            #     destination = os.path.join(upload_path, '{}.{}'.format(self.data.edition_code, extension))
            #     server.put(item, destination)
            #     server.close()
			#
            # except Exception as e:
            #     return False, 'upload_cover_scoopadm Error {}'.format(e)

            os.remove(item)

            return True, 'OK'

        except Exception as e:
            return False, 'upload_cover_s3 Failed {}'.format(e)

    def create_covers_items(self):
        try:
            base_path = os.path.join(app.config['COVER_ITEM_BASE'], str(self.data.brand_id))
            if not os.path.exists(base_path):
                os.makedirs(base_path, self.mod)

            for icover in self.cover_dir:
                cover_path = os.path.join(base_path, icover)
                if not os.path.exists(cover_path):
                    os.makedirs(cover_path, self.mod)

                # create images in path
                status_image, image_convert = self.upload_image(cover_path, self.image)
                if not status_image:
                    return status_image, image_convert

                raw_files = os.path.join(cover_path, image_convert)

                # rename files images with raw extensions
                extension = str(image_convert).split('.')[1]
                new_name = '{}.{}'.format(self.data.edition_code, extension)
                new_file = os.path.join(cover_path, new_name)

                self.cover_data.append((icover, new_name))

                # create all cover...
                if icover == 'thumb_image_normal':
                    process = 'convert -resize {}x -density {} -define pdf:use-cropbox=true -background white -flatten {} {}'.format(app.config['GEN_SM_COVER'], 250, raw_files, new_file)
                    self.data.thumb_image_normal = 'images/1/{}/{}/{}'.format(self.data.brand_id, icover, new_name)

                if icover == 'thumb_image_highres':
                    process = 'convert -resize {}x -density {} -define pdf:use-cropbox=true -background white -flatten {} {}'.format(app.config['GEN_COV'], 250, raw_files, new_file)
                    self.data.thumb_image_highres = 'images/1/{}/{}/{}'.format(self.data.brand_id, icover, new_name)

                if icover == 'image_normal':
                    process = 'convert -resize {}x -density {} -define pdf:use-cropbox=true -background white -flatten {} {}'.format(app.config['GEN_BIG_COV'], 250, raw_files, new_file)
                    self.data.image_normal = 'images/1/{}/{}/{}'.format(self.data.brand_id, icover, new_name)

                if icover == 'image_highres':
                    process = 'convert -resize {}x -density {} -define pdf:use-cropbox=true -background white -flatten {} {}'.format(app.config['BIG_COV'], 250, raw_files, new_file)
                    self.data.image_highres = 'images/1/{}/{}/{}'.format(self.data.brand_id, icover, new_name)

                subprocess.call(process, shell=True)
                db.session.add(self.data)
                db.session.commit()

                status, message = self.upload_cover_s3(cover_path=icover, item=new_file, extension=extension)
                try:
                    # remove files
                    os.remove(raw_files)
                    os.remove(new_file)
                except:
                    pass

            return True, 'create_covers_items Successfully'

        except Exception as e:
            return False, 'create_covers_items Failed {}'.format(e)

    def process(self):

        # created covers for items.
        if self.resource == 1:
            status, data = self.create_covers_items()
            if not status:
                return internal_server_message(modul="UPLOAD IMAGE", method='POST', error=data, req=str(self.args))

        # created covers for offers
        if self.resource == 2:
            pass

        return Response(status=httplib.OK)
