import httplib, os, re, json, datetime, random, roman

from slugify import slugify as new_slugify
from unidecode import unidecode
from flask import Response, request

from app import app, db
from app.constants import *
from app.logs.models import ApiLog, ItemLog
from app.payments.models import PaymentLog

def purge_redis_cache(redis_key, detail_key):
    redis_key = '{}{}:*'.format(redis_key, detail_key)
    for data_redis in app.kvs.keys(redis_key):
        app.kvs.delete(data_redis)

def portal_metadata_changes(user_id, data_id, data_url, data_method, data_request):
    from app.items.models import ReportDataChangeModified

    try:
        data_report = ReportDataChangeModified(user_id=user_id, data_id=data_id, data_url=data_url,
                                               data_method=data_method, data_request=data_request)
        db.session.add(data_report)
        db.session.commit()
    except:
        pass


def log_api(name=None, method=None, user=None, role=None, json_request=None, response=None, code=None, msg=None, ip_address=None):
    """
        Save log file
    """
    headers = {}
    if request.headers.get("Authorization"):
        headers['Authorization'] = request.headers.get("Authorization")
    if request.headers.get("Content-Type"):
        headers['Content-Type'] = request.headers.get("Content-Type")

    # log = ApiLog(
    #     name=name,
    #     http_method=method,
    #     user_id=user,
    #     role_id=role,
    #     request=json_request,
    #     respons=response,
    #     response_code=code,
    #     err_message=msg,
    #     ip_address=ip_address,
    #     headers=str(headers)
    #     )
    #
    # db.session.add(log)
    # db.session.commit()


def log_items(item_id=0, brand_id=0, subs_id=0, useritem_id=0, status=0, error='', user_id=0):
    # log_data = ItemLog(
    #     item_id=item_id,
    #     brand_id=brand_id,
    #     subs_id=subs_id,
    #     useritem_id=useritem_id,
    #     status=status,
    #     error=error,
    #     user_id=user_id
    # )
    # log_data.save()

    # just straight log the request
    # app.logger.info(log_data.values())
    pass

def generate_point_idr(final_price=None):
    # point reward base IDR CURRENCY, RATE: 0.001
    # per 3 Oct 2017, we change the rate to 0.0005
    # per 1 Nov 2017, we change the rate to 0.0003
    # per 22 Nov 2017, we change the rate to 0 (no point reward)
    # return int(final_price * 0.0003)
    return 0


def generate_point_usd(final_price=None):
    # point reward base USD CURRENCY, RATE: 9.5
    # per 3 Oct 2017, we change the rate to 5
    # per 1 Nov 2017, we change the rate to 3
    # per 22 Nov 2017, we change the rate to 0 (no point reward)
    # return int(final_price * 3)
    return 0


def generate_invoice():
    return int(''.join([str(random.randint(1, 10)) for i in range(14)])[:14])


def payment_logs(args=None, error_message=None, error_code=None, user_message=None, status_code=None):

    try:
        paymentgateway_id = args.get('payment_gateway_id', 0)
        user_id = args.get('user_id', 0)
        order_id = args.get('order_id', 0)
        api = args.get('api', '')
        method = args.get('method', '')

        if status_code is None:
            status_code = httplib.BAD_REQUEST

        if error_code is None:
            error_code = 400101

        if user_message is None:
            user_message = 'Payment Invalid'

        log_data = PaymentLog(
            paymentgateway_id=paymentgateway_id,
            user_id=user_id,
            order_id=order_id,
            request=str(args),
            api=api,
            method=method,
            error_message=str(error_message)
        )
        log_data.save()

        return err_response(
            status=status_code,
            error_code=error_code,
            developer_message='Payment Invalid, %s' % (error_message),
            user_message=user_message)

    except Exception as e:
        pass


def conflict_message(modul=None, method=None, conflict=None, req=None):
    log_api(name=modul.upper(), method=method,
        json_request=str(req),
        response="%s already exist" % str(modul),
        code=CLIENT_409101, msg="%s already exist" % str(modul))

    return err_response(status=httplib.CONFLICT,
        error_code=CLIENT_409101,
        developer_message="%s : %s already exist" % (modul, conflict),
        user_message="%s already exist" % conflict)


def bad_request_message(modul=None, method=None, param=None, not_found=None, req=None):
    log_api(name=modul.upper(), method=method,
        json_request=str(req),
        response="%s not founds" % str(modul),
        code=CLIENT_400101, msg="%s not founds" % str(modul))

    return err_response(status=httplib.BAD_REQUEST,
        error_code=CLIENT_400101,
        developer_message="%s : %s not founds" % (param, not_found),
        user_message="Bad Request for %s" % not_found)


def internal_server_message(modul=None, method=None, error=None, req=None, message=None):

    if method == 'POST':
        user_message = "Add %s failed" % modul
    elif method == 'PUT':
        user_message = "Update %s failed" % modul
    elif method == 'CUSTOM':
        user_message = message

    if message is None:
        user_message = ''

    try:
        # try to access ScoopApiException properties if possible
        dev_message = error.developer_message
        error_code = error.code
    except Exception as e:
        dev_message = str(error)
        error_code = httplib.INTERNAL_SERVER_ERROR

    # log to log api
    log_api(name=modul.upper(), method=method,
        json_request=str(req), response=str(error),
        code=CLIENT_500101, msg=user_message)

    return err_response(status=error_code,
            error_code=CLIENT_500101, developer_message=dev_message,
            user_message=user_message)


def general_message(modul=None, method=None, req=None, mssge=None, usermsg=None, code=None, error_code=None):

    log_api(name=modul.upper(), method=method, json_request=str(req),
        response=mssge, code=code, msg=mssge)

    return err_response(status=code, error_code=error_code, developer_message=mssge,
        user_message=usermsg)


def message_json(code, message):
    rv = json.dumps({'code': code, 'message': message})
    return Response(rv, status=code, mimetype='application/json')



def assertion_error(err=None, code=None):
    """
        return response if failed insert data to DB
    """
    rv = json.dumps({'code': code, 'message': err.message}, default=dtserializer)
    return Response(rv, status=code, mimetype='application/json')


def err_response(status=0, error_code=0, developer_message='', user_message=''):
    """
        return response if failed insert data to DB

        [HOW-TO]
            return err_response(status=401,
                                error_code=14012,
                                developer_message=e,
                                user_message="You have no authority to perform this operation")

        will become like this:
            {
                "status": 401,
                "error_code": 14012,
                "developer_message": "Unauthorized. Access denied",
                "user_message": "You have no authority to perform this operation"
            }
    """
    rv = json.dumps(
        {
            'status': status,
            'error_code': error_code,
            'developer_message': developer_message,
            'user_message': user_message
        }, default=dtserializer)
    return Response(rv, status=status, mimetype='application/json')


def slugify(text, delim=u'-'):
    """
        Generate slug
    """

    _punct_re = re.compile(r'[\t !"#$%&\'()*\-/<=>?@\[\\\]^_`{|},.:;]+')
    result = []
    for word in _punct_re.split(text.lower()):
        result.extend(unidecode(word).split())
    return unicode(delim.join(result))


def date_validator(date=None, module=None):

    """
        STANDARD FORMAT : YYYY-MM-DD
    """
    temp = {}
    if module == 'custom':
        # SCOOP-2126
        data1 = date.split('T')
        if len(data1) > 1:
            data = data1[0].split('-')
            tmatch = True
        else:
            data2 = date.split(' ')
            data = data2[0].split('-')
            tmatch = False

    else:
        # SCOOP-2126
        data1 = date.split('T')
        if len(data1) > 1:
            data = data1[0].split('-')
            tmatch = True
        else:
            data2 = date.split(' ')
            data = data2[0].split('-')
            tmatch = False

    # verify month
    try:
        if int(data[1]) not in range(1, 13):
            temp['invalid'] = True
            temp['message'] = "Month invalid, not in range 1 - 12"

        elif int(data[2]) not in range(1, 32):
            temp['invalid'] = True
            temp['message'] = "Day invalid, not in range 1 - 32"
        else:
            temp['invalid'] = False
            if tmatch:
                temp['message'] = datetime.datetime.strptime(date, '%Y-%m-%dT%H:%M:%SZ')
            else:
                temp['message'] = datetime.datetime.strptime(date, '%Y-%m-%d %H:%M:%S')

    except Exception, e:
        temp['invalid'] = True
        temp['message'] = "Invalid Format date"

    return temp


# default datetime serializer for json.dumps()
dtserializer = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None


def generate_slug(entity_name, conflicting_slug_count=0):
    """
    :param `basestring` entity_name:
    :param `int` conflicting_slug_count:
    :return: returning new slug
    :rtype: `unicode`
    """
    try:
        new_slug = new_slugify(entity_name)
        if conflicting_slug_count:
            new_slug += '-{suffix}'.format(
                suffix=roman.toRoman(conflicting_slug_count))

        return new_slug.lower().decode('utf-8')
    except Exception:
        print "Error generating slug for: ".format(entity_name)
        raise


def regenerate_slug(model_class, slug, existing_item=None):
    """ Regenerate new slug if slug is exists.

    :param `class` model_class: Model name
    :param `basestring` slug: previous slug
    :return: A new slug that does not conflict with anything in the database.
    :rtype: `unicode`
    """

    slug_suffix = 0
    old_slug = slug
    while True:
        slug_suffix += 1

        if existing_item:
            slugs = model_class.query.filter(model_class.slug == slug).filter(model_class.id != existing_item.id).all()
        else:
            slugs = model_class.query.filter(model_class.slug == slug).all()

        if slugs:
            slug = generate_slug(old_slug, slug_suffix)
        else:
            break

    return slug


def create_dir_if_not_exists(dir_path, umask=0002, permission=0775):
    """ Recursively creates directory path, if it does not already exist.

    This method will take either a directory or a file path for the dir_path
    parameter.  If it is a file path, the file portion will be ignored.

    :param `str` dir_path: Path on local filesystem to create.
    :param `int` umask: Controls the permissions for newly-created files.
    :param `int` permission: Permission mask for the directory.
    :return: The path to the directory.
    :rtype: str
    """
    os.umask(umask)
    d = os.path.dirname(dir_path)

    if not os.path.exists(d):
        os.makedirs(d, permission)
    return d


def request_is_expectation_check():
    """ Indicates whether the current request is simply an expectation check.

    :return: True if current request is an expectation check, otherwise False.
    :rtype: bool
    """
    return request.headers.get('Expect', "").lower() == '100-continue'


class DateRange(object):
    def __init__(self, min_date, max_date):
        self.min_date = min_date
        self.max_date = max_date


from enum import Enum

class SchemaType(Enum):
    select = 'select'
    suggest = 'suggest'


def load_json_schema(package, name):
    """ Get a dict representing a JSON schema.

    :param `six.string_types` package:
        The name of the package where the schema can be found, sans 'app'
    :param `six.string_types` name:
        The name of the response.
    :param `SchemaType` type:
    :return:
    """
    curr_dir = os.path.abspath(os.path.dirname(__file__))

    return json.load(
        open(os.path.join(curr_dir,
                          package,
                          'schemas',
                          '{}.json'.format(name))))


