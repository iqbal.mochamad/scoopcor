from flask_appsfoundry.serializers import SerializerBase, fields

from app.utils.serializers import ImplicitRelHref, SimpleImplicitHref, SimpleImplicitHrefWithRel, \
    SimpleImplicitHrefWithId


class LayoutSerializer(SimpleImplicitHrefWithId):
    layout_type = fields.String
    title = fields.String(attribute='name')


class PriceDetailFields(SerializerBase):
    base = fields.Float
    net = fields.Float


class PriceFields(SerializerBase):
    idr = fields.Nested(PriceDetailFields())
    usd = fields.Nested(PriceDetailFields())
    point = fields.Nested(PriceDetailFields())


class LayoutCustomPickListSerializer(SimpleImplicitHrefWithId):
    subtitle = fields.String
    type = fields.String
    slug = fields.String
    price = fields.Raw
    brand = fields.Raw
    image_url = fields.String
    image_normal = fields.String
    thumb_normal = fields.String
    thumb_highres = fields.String


class LayoutCustomItemSerializer(SimpleImplicitHrefWithId):
    subtitle = fields.String
    image_url = fields.String
    type = fields.String
    display_offers = fields.Raw(attribute='display_offers')


class LayoutIntroSerializer(SerializerBase):
    id = fields.Integer
    title = fields.String
    href = fields.String

