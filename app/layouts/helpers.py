from __future__ import unicode_literals, absolute_import

from numbers import Number

from flask import current_app
from flask import url_for

from app.layouts.banners import BannerActions


def get_href_for_banners(banner_type, payload):
    if banner_type in [BannerActions.open_url, ]:
        return payload
    elif banner_type == BannerActions.static:
        # image only, not clickable
        return None
    elif banner_type == BannerActions.brand:
        # return api items, filtered by brands
        if payload:
            endpoint_url = url_for('items.list', _external=True)
            return '{}?expand=ext&brand_id={}'.format(endpoint_url, payload)
        else:
            return None
    else:
        # banner_type == BannerActions.buffet
        # banner_type == BannerActions.promo
        # banner_type == BannerActions.landing_page
        try:
            campaign_id = int(payload)
        except:
            campaign_id = None
        if payload and campaign_id:
            return url_for('campaigns.items', campaign_id=campaign_id, _external=True)
        else:
            return None


def banner_image_url(value):
    return current_app.config['BANNER_BASE_URL'] + value if value else None


def item_image_url(value):
    return current_app.config['MEDIA_BASE_URL'] + value if value else None
