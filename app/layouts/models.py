from __future__ import unicode_literals, absolute_import

from datetime import datetime
from flask_appsfoundry.models import TimestampMixin, SoftDeletableMixin
from sqlalchemy.orm import relationship

from app import db
from app.layouts.layout_type import LayoutTypes
from app.utils.models import Py3EnumType, url_mapped


@url_mapped('layouts.details', (('db_id', 'id'),))
class LayoutControl(db.Model, TimestampMixin, SoftDeletableMixin):
    __tablename__ = 'layouts'
    name = db.Column(db.Text, nullable=False, unique=True)
    sort_priority = db.Column(db.Integer, nullable=False)
    layout_type = db.Column(Py3EnumType(LayoutTypes, name = 'layout_type'),
                            nullable=False, default=LayoutTypes.latest_book)


@url_mapped('layouts.offer_details', (('db_id', 'layout_id'), ('offer_id', 'offer_id'), ))
class LayoutOffer(db.Model):
    __tablename__ = 'layout_offers'
    layout_id = db.Column(db.Integer, db.ForeignKey('layouts.id'), nullable=False, primary_key=True)
    layout = relationship('LayoutControl', backref=db.backref('offers', lazy='dynamic'))
    offer_id = db.Column(db.Integer, db.ForeignKey('core_offers.id'), nullable=False, primary_key=True)
    offer = relationship('Offer', backref=db.backref('layouts', lazy='dynamic'))
    sort_priority = db.Column(db.Integer, nullable=False)
    created = db.Column(db.DateTime, default=datetime.utcnow)


@url_mapped('layouts.banner_details', (('db_id', 'layout_id'), ('banner_id', 'banner_id'), ))
class LayoutBanner(db.Model):
    __tablename__ = 'layout_banners'
    layout_id = db.Column(db.Integer, db.ForeignKey('layouts.id'), nullable=False, primary_key=True)
    layout = relationship('LayoutControl', backref=db.backref('banners', lazy='dynamic'))
    banner_id = db.Column(db.Integer, db.ForeignKey('core_banners.id'), nullable=False, primary_key=True)
    banner = relationship('Banner')
    sort_priority = db.Column(db.Integer, nullable=False)
    created = db.Column(db.DateTime, default=datetime.utcnow)


class LayoutIntro(db.Model):
    __tablename__ = 'layout_intro'
    brand_id = db.Column(db.Integer, db.ForeignKey('core_brands.id'), nullable=False, primary_key=True)
    brand = relationship('Brand', backref=db.backref('layout_intro', lazy='dynamic'))
    sort_priority = db.Column(db.Integer, nullable=False)
    created = db.Column(db.DateTime, default=datetime.utcnow)
