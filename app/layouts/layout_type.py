from __future__ import absolute_import, unicode_literals
from enum import Enum


class LayoutTypes(Enum):
    latest_book = 'latest book'
    latest_magazine = 'latest magazine'
    latest_newspaper = 'latest newspaper'
    editor_pick_banners = 'editor pick banner'
    editor_pick_offers = 'editor pick offers'
    popular_book = 'popular book'
    popular_magazine = 'popular magazine'
    popular_newspaper = 'popular newspaper'
    latest_all = 'latest all'
    popular_all = 'popular all'

