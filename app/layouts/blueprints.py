from app.layouts import api
from app.utils.blueprint import CorBlueprint


layout_blueprint = CorBlueprint('layouts', __name__, url_prefix='/v1')

layout_blueprint.add_url_rule('/layouts', view_func=api.LayoutApi.as_view('list'))
layout_blueprint.add_url_rule('/layouts/<int:db_id>', view_func=api.LayoutDetailsApi.as_view('details'))

layout_blueprint.add_url_rule('/layouts/intro', view_func=api.LayoutIntroApi.as_view('intro'))

