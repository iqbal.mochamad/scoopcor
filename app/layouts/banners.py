from __future__ import unicode_literals, absolute_import

import os, json

from enum import Enum
from datetime import datetime
from iso3166 import countries
from six import text_type
from werkzeug.routing import BaseConverter

from marshmallow import fields as ma_fields, pre_load, validates, ValidationError
from marshmallow_sqlalchemy import ModelSchema
from marshmallow_enum import EnumField

from sqlalchemy import desc, Column, Text, Boolean, DateTime, Integer, String, or_
from sqlalchemy.dialects import postgresql

from flask import Blueprint, jsonify, request, url_for, g, current_app
from flask_appsfoundry import parsers as p
from flask_appsfoundry.models import SoftDeletableMixin
from flask_appsfoundry.parsers import FilterField, filterinputs as fi
from flask_appsfoundry.parsers.converters import ImageFieldParser
from flask_appsfoundry.exceptions import NotFound

from app import db, app
from app.auth.decorators import user_has_any_tokens
from app.auth.helpers import SCOOP_PORTAL
from app.helpers import generate_slug, regenerate_slug
from app.items.helpers import get_filter_parental_control
from app.parental_controls.choices import ParentalControlType
from app.uploads.aws.helpers import aws_upload_file
from app.utils.datetimes import get_utc
from app.utils.geo_info import get_geo_info
from app.utils.marshmallow_helpers import create_slug, update_response, create_response
from app.utils.models import Py3EnumType
from app.utils.responses import generate_weak_etag
from app.utils.shims.parsers import WildcardFilter


class BannerConverter(BaseConverter):

    def to_python(self, value):
        entity = db.session().query(Banner).get(value)
        if not entity:
            raise NotFound
        return entity

    def to_url(self, value):
        return text_type(value.id)


app.url_map.converters['banner'] = BannerConverter

blueprint = Blueprint('banners', __name__, url_prefix='/v1/banners')


def user_is_in_roles(role_types, user, *args, **kwargs):
    for user_role in user.roles:
        if user_role.id in role_types:
            return True
    return False


class BannerActions(Enum):
    landing_page = 'landing page'
    open_url = 'open url'
    static = 'static'
    brand = 'brand'
    buffet = 'buffet'
    promo = 'promo'
    banner_only = 'banner only'


class ItemPrices(Enum):
    single = 'single'
    subscription = 'subscription'
    bundle = 'bundle'


class BannerZone(Enum):
    homepage = 'homepage'
    magazine_page = 'magazine page'
    book_page = 'book page'
    newspaper_page = 'newspaper page'
    publisher_page = 'publisher page'
    promo_page = 'promo page'
    sidebar = 'sidebar'
    free_page = 'free page'


BANNER_TYPE_MAGIC_NUMBERS = {
    1: BannerActions.landing_page,
    2: BannerActions.open_url,
    3: BannerActions.static,
    4: BannerActions.brand,
    5: BannerActions.buffet,
    6: BannerActions.promo,
    7: BannerActions.banner_only,
}


BANNER_ZONE_MAGIC_OPTIONS = {
    BannerZone.homepage.value: BannerZone.homepage,
    BannerZone.magazine_page.value: BannerZone.magazine_page,
    BannerZone.book_page.value: BannerZone.book_page,
    BannerZone.newspaper_page.value: BannerZone.newspaper_page,
    BannerZone.publisher_page.value: BannerZone.publisher_page,
    BannerZone.promo_page.value: BannerZone.promo_page,
    BannerZone.sidebar.value: BannerZone.sidebar,
    BannerZone.free_page.value: BannerZone.free_page,
}

BANNER_PRICE_MAGIC_OPTIONS = {
    'single': ItemPrices.single,
    'subscription': ItemPrices.subscription,
    'bundle': ItemPrices.bundle
}


class Banner(SoftDeletableMixin, db.Model):
    """ A banner that can be displayed by getscoop.com and the mobile applications.
    """
    name = Column(Text)
    slug = Column(Text, unique=True)
    description = Column(Text)

    zone = Column(Py3EnumType(BannerZone, name='banner_display_zone'), nullable=False, default=BannerZone.homepage)
    banner_type = Column(Py3EnumType(BannerActions, name='scoop_banner_type'), nullable=False)
    payload = Column(Text)

    image_normal = Column(Text, nullable=False)
    image_highres = Column(Text, nullable=False)
    image_iphone = Column(Text)
    image_social_media = Column(Text)

    is_age_restricted = Column(Boolean, default=False, nullable=False)

    # TODO: alter core_banners table, add timezone to valid to and valid from
    valid_from = Column(DateTime(timezone=True))
    valid_to = Column(DateTime(timezone=True))

    clients = Column(postgresql.ARRAY(Integer), name='client_id')
    countries = Column(postgresql.ARRAY(String(5)), default=[])

    sort_priority = Column(Integer, nullable=False, default=0)

    term_and_condition = Column(Text)
    product_per_page = Column(Integer)
    show_title = Column(Boolean, default=True)
    show_price = Column(Py3EnumType(ItemPrices, name='scoop_item_price'), default=ItemPrices.single)

    created = db.Column(db.DateTime(timezone=True),
                        default=get_utc)

    modified = db.Column(db.DateTime(timezone=True),
                         default=get_utc,
                         onupdate=get_utc)

    __mapper_args__ = {
        'version_id_col': modified,
        'version_id_generator': lambda version: get_utc()
    }

    @property
    def api_url(self):
        return url_for('banners.details', banner_id=self.id, _external=True)


def image_from_base64(input, previous_saved_data):
    if input is None:
        return previous_saved_data
    else:
        save_directory = os.path.join(current_app.static_folder, 'banner')
        p = ImageFieldParser(save_directory)
        files = p.from_base64(input)

        raw_files = os.path.join(save_directory, files)
        aws_upload_file(bucket_name=app.config['BOTO3_GRAMEDIA_STATIC_BUCKET'],
                        bucket_folder='banners/',
                        transfer_file=raw_files,
                        output_file=files,
                        is_public_access=True)
        return files


class BannerSchema(ModelSchema):

    class Meta:
        model = Banner
        sqla_session = db.session

    media_base_url = ma_fields.Function(lambda obj: app.config['BANNER_BASE_URL'], dump_only=True)
    is_age_restricted = ma_fields.Boolean(load_only=True)

    modified = ma_fields.DateTime(dump_only=True)
    created = ma_fields.DateTime(dump_only=True)

    zone = EnumField(BannerZone, by_value=True)
    show_price = EnumField(ItemPrices, by_value=True)
    # banner_type = EnumField(BannerActions, by_value=True)
    banner_type = ma_fields.Method('get_banner_type', deserialize='load_banner_type')

    def get_banner_type(self, obj):
        return {v: k for k, v in BANNER_TYPE_MAGIC_NUMBERS.items()}[obj.banner_type]

    def load_banner_type(self, value):
        return BANNER_TYPE_MAGIC_NUMBERS[value]

    # from previous parser: countries = InputField(type=iso3166_country, action='append', default=[])
    @validates('countries')
    def validate_countries(self, value):
        for v in value:
            try:
                countries.get(v)
            except KeyError:
                raise ValidationError(
                    "{country_code} is not a valid ISO "
                    "3166 country code".format(country_code=v))

    @pre_load
    def load_image(self, data):
        # in Update process/PUT, if user didn't want to change image, set image_normal to None/Null
        #   if it's None/Null, image_from_base64 will return data from previous saved value
        data['image_normal'] = image_from_base64(
            input=data.get('image_normal', None),
            previous_saved_data=self.instance.image_normal if self.instance else None)

        # all set with single image pixels.
        data['image_highres'] = data['image_normal']
        data['image_iphone'] = data['image_normal']

        data['image_social_media'] = image_from_base64(
            input=data.get('image_social_media', None),
            previous_saved_data=self.instance.image_social_media if self.instance else None)


class EnumStringFilter(FilterField):
    def __init__(self, enum, **kwargs):
        self.__enum_type = enum
        kwargs['type'] = lambda input_value: self.__enum_type(input_value)
        super(EnumStringFilter, self).__init__(**kwargs)


class BannerListArgsParser(p.SqlAlchemyFilterParser):
    q = WildcardFilter(dest='name')
    banner_type = fi.EnumFilter(enum=BANNER_TYPE_MAGIC_NUMBERS)
    client_id = fi.IntegerFilter(dest='clients', default_operator='contains')
    show_price = EnumStringFilter(ItemPrices)
    zone = EnumStringFilter(BannerZone)
    __model__ = Banner


def get_banner(banner_id, session=None):
    session = session or db.session()
    return session.query(Banner).get(banner_id)


@blueprint.route('/<int:banner_id>')
@user_has_any_tokens(
    'can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext', 'can_read_global_banner', )
def details(banner_id):
    entity = get_banner(banner_id)
    return jsonify(BannerSchema().dump(entity).data)


@blueprint.route('', methods=('POST', ))
@user_has_any_tokens('can_read_write_global_all', 'can_read_write_global_banner', )
def create():

    def before_commit_method(entity, session, **kwargs):
        # always create slug no matter banner type.
        slug = generate_slug(entity.name)
        exist_data = Banner.query.filter(Banner.slug == slug).first()
        if not exist_data:
            entity.slug = slug
        else:
            new_slug = regenerate_slug(Banner, slug, entity)
            entity.slug = new_slug

    create_banner = create_response(BannerSchema(), before_commit_method=before_commit_method)
    try:
        data_banner = json.loads(create_banner.data).get('id', None)
        app.kvs.set('notification-banner-{}'.format(data_banner), data_banner, 604800)
    except:
        pass
    return create_banner


@blueprint.route('/<int:banner_id>', methods=('PUT', ))
@user_has_any_tokens('can_read_write_global_all', 'can_read_write_global_banner', )
def update(banner_id):
    """

    :param banner_id:
    :return:
    """
    session = db.session()
    entity = get_banner(banner_id, session)
    # load/update Banner data with data from request body (request.data) and return jsonify response
    return update_response(schema=BannerSchema(), instance=entity, session=session)


@blueprint.route('')
@user_has_any_tokens(
    'can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext', 'can_read_global_banner', )
def search():
    """ Banner List Api
    end point: v1/banners
    :return:
    """
    query = db.session().query(Banner)

    current_client_id = g.current_client.id if getattr(g, 'current_client', None) else None
    default_filters = []

    # if scoop portal (data maintenance) want to show active only banner, user query string filter "active-only=1"
    is_active = request.args.get('active-only', type=bool)

    if not current_client_id:
        user_agent = request.headers.get('user-agent', '').lower()
        if 'web' in user_agent: current_client_id = SCOOP_PORTAL

    if current_client_id != SCOOP_PORTAL or is_active:
        # filter by active only
        default_filters.extend([Banner.valid_from <= datetime.now(),
                           Banner.valid_to >= datetime.now(),])

    if current_client_id != SCOOP_PORTAL:
        parental_control_id = get_filter_parental_control(
            request.args.get('parental_control_id', type=int, default=0))

        if ((getattr(g, 'current_client', None) and not g.current_client.allow_age_restricted_content) or
                (parental_control_id and parental_control_id == ParentalControlType.parental_level_1.value)):
            # filter only all ages banner (no adult banner)
            default_filters.append(Banner.is_age_restricted == False)

    filter_exps = BannerListArgsParser().parse_args(request)['filters']

    def_filters = [defexp for defexp in default_filters
                   if defexp.left.name
                   not in [reqexp.left.name for reqexp in filter_exps]]

    filter_exps.extend(def_filters)

    if current_client_id != SCOOP_PORTAL:
        # filter by country
        geo_info = get_geo_info()
        if geo_info.country_code:
            filter_exps.append(or_(Banner.countries.any(geo_info.country_code), Banner.countries == []))

    for f in filter_exps:
        query = query.filter(f)

    query = query.order_by(desc(Banner.id))

    limit = request.args.get('limit', default=100, type=int)
    offset = request.args.get('offset', default=0, type=int)

    total_row = query.count()
    query = query.offset(offset).limit(limit)

    banners = query.all()
    response = jsonify({
        'banners': [
            BannerSchema().dump(a).data for a in banners
            ],
        'metadata': {
            'resultset': {
                'limit': limit,
                'offset': offset,
                'count': total_row
            }
        }
    })

    weak_etag = generate_weak_etag(banners)
    response.set_etag(weak_etag, weak=True)

    return response.make_conditional(request)


