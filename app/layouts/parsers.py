from flask_appsfoundry import parsers
from flask_appsfoundry.parsers import (StringField, DateTimeField, DecimalField, BooleanField, IntegerField,
                                       SqlAlchemyFilterParser, ParserBase, EnumField, InputField)
from flask_appsfoundry.parsers.filterinputs import (IntegerFilter, StringFilter, BooleanFilter, DateTimeFilter,
                                                    DecimalFilter, EnumFilter)

from app.layouts.layout_type import LayoutTypes
from . import models


class LayoutListArgsParser(SqlAlchemyFilterParser):
    __model__ = models.LayoutControl
    id = IntegerFilter()
    title = StringFilter()
    sort_priority = IntegerFilter()


class LayoutArgsParser(ParserBase):
    id = IntegerField()


class LayoutOfferArgsParser(ParserBase):
    id = IntegerField()
    sort_priority = IntegerField()


class LayoutOfferUpdateArgsParser(ParserBase):
    sort_priority = IntegerField(required=True)


class LayoutOfferListArgsParser(SqlAlchemyFilterParser):
    __model__ = models.LayoutOffer
    id = IntegerFilter()


class LayoutBannerArgsParser(ParserBase):
    id = IntegerField()
    sort_priority = IntegerField()


class LayoutBannerUpdateArgsParser(ParserBase):
    sort_priority = IntegerField(required=True)


class LayoutBannerListArgsParser(SqlAlchemyFilterParser):
    __model__ = models.LayoutBanner
    layout_id = IntegerFilter(required=True)
    banner_id = IntegerFilter(required=True)


class LayoutIntroArgsParser(ParserBase):
    id = IntegerField()
    sort_priority = IntegerField()


class LayoutIntroListArgsParser(SqlAlchemyFilterParser):
    __model__ = models.LayoutIntro
    include_age_restricted = BooleanFilter(default=False)
