from __future__ import unicode_literals, absolute_import

import json
from abc import abstractmethod

from datetime import timedelta
from flask import jsonify, current_app, request, g
from flask_appsfoundry import controllers, marshal
from flask_appsfoundry.auth.constants import TOKEN_PREFIX
from flask_appsfoundry.auth.helpers import assert_user_has_any_permission
from flask_appsfoundry.exceptions import NotFound, BadRequest, MethodNotAllowed, UnprocessableEntity
from sqlalchemy import desc
from sqlalchemy import func

from app import db
from app.auth.decorators import user_has_any_tokens
from app.auth.helpers import SCOOP_PORTAL
from app.caching import generate_redis_key
from app.items.choices import ItemTypes, STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.items.helpers import get_filter_parental_control
from app.items.models import PopularItem, Item
from app.layouts.banners import Banner
from app.layouts.layout_type import LayoutTypes
from app.layouts.models import LayoutControl
from app.layouts.parsers import LayoutArgsParser, LayoutListArgsParser, LayoutOfferArgsParser, \
    LayoutOfferListArgsParser, \
    LayoutIntroArgsParser, LayoutIntroListArgsParser
from app.layouts.serializers import LayoutSerializer, LayoutIntroSerializer, LayoutCustomPickListSerializer
from app.layouts.viewmodels import LayoutListViewModel, LayoutOffersViewModel, LayoutBannersViewModel, \
    LayoutIntroViewModel
from app.parental_controls.choices import ParentalControlType
from app.utils.controllers import CorApiErrorHandlingMixin, CorAuthMixin
from app.utils.datetimes import get_local
from app.utils.redis_cache import RedisCache, ONE_DAY
from . import models, _log, viewmodels


class ListPutApiResourceBase(CorApiErrorHandlingMixin, controllers.ListCreateApiResource, CorAuthMixin):
    allowed_options = ['OPTIONS', 'GET', 'PUT', ]
    methods = ('get', 'put')
    cache_key_name = None

    get_security_tokens = []

    put_security_tokens = ['can_read_write_global_all',
                           'can_read_write_global_banner',
                           ]

    def __init__(self):
        super(ListPutApiResourceBase, self).__init__()
        self.cache = RedisCache(current_app.kvs)

    def get(self):
        return self._get_response()

    @staticmethod
    def _bypass_cache():
        return request.headers.get('Cache-Control', '').lower() == 'no-cache'

    def _build_cache_key(self):
        return generate_redis_key(self.cache_key_name, args=request.args.to_dict())

    def _get_cached_response(self, force_update_cache):
        cached_response = None
        weak_etag = None
        try:
            if not (self._bypass_cache() or force_update_cache or not self.cache_key_name):
                cached_data = self.cache.get_hash(self._build_cache_key())
                if cached_data:
                    cached_response = jsonify(json.loads(cached_data.get('data')))
                    weak_etag = cached_data.get('etag')
        except Exception as e:
            _log.exception('Failed to read data from redis')
        return cached_response, weak_etag

    def _get_response(self, force_update_cache=False):
        response, weak_etag = self._get_cached_response(force_update_cache)
        if not response:
            response, weak_etag = self._fetch_from_db()
            # rewrite new data to cache
            try:
                if self.cache_key_name:
                    self.cache.write_hash(key=self._build_cache_key(), data=json.loads(response.data), etag=weak_etag)
            except Exception as e:
                _log.exception('Failed to write cache to redis')
                pass
        response.set_etag(weak_etag, weak=True)
        response = response.make_conditional(request)
        return response

    def _fetch_from_db(self):
        view_model = self._get_list_viewmodel(*self._get_pagination())
        response = jsonify(marshal(view_model, self._get_list_serializer()))
        weak_etag = self._generate_weak_etag(view_model)
        return response, weak_etag

    def put(self):
        assert_user_has_any_permission(self.put_security_tokens,
                                       redis=self.redis_server,
                                       app_conf=self.app_config,
                                       token_prefix=TOKEN_PREFIX,
                                       req=request)
        req_data = request.json.get(self.serialized_list_name, None)
        # update data: delete old data, and reinsert new data with the new order
        self._update_data_to_db(req_data=req_data)

        if self.cache_key_name:
            # delete old data from redis cache
            self.cache.delete_with_key_prefix(self.cache_key_name)

        return self._get_response(force_update_cache=True)

    @abstractmethod
    def _update_data_to_db(self, req_data):
        pass

    def post(self):
        raise MethodNotAllowed


class LayoutApi(ListPutApiResourceBase):

    cache_key_name = 'layout:main'

    query_set = models.LayoutControl.query.filter_by(is_active=True)

    default_ordering = (desc(models.LayoutControl.sort_priority),
                        )

    create_request_parser = LayoutArgsParser

    list_request_parser = LayoutListArgsParser

    response_serializer = LayoutSerializer

    viewmodel = LayoutListViewModel

    serialized_list_name = 'layouts'

    def __init__(self):
        super(LayoutApi, self).__init__()
        # for layout, set expiry of this api to never expired, since it will reset at put anyway
        self.cache = RedisCache(current_app.kvs, expiry=None)

    def _update_data_to_db(self, req_data):
        # validate request body
        if not req_data or len(req_data) == 0:
            raise BadRequest(developer_message="layouts not found in request body")

        session = db.session()
        try:
            sort_priority = len(req_data) * 10
            # first, mark all as not active, then change again later based on new list of data
            session.execute('UPDATE layouts SET is_active = False;')
            # update layouts based on new list of data
            for data in req_data:
                # validate request body
                if not data.get('title', None) or not data.get('layout_type', None):
                    session.rollback()
                    raise BadRequest()
                layout_id = data.get('id', 0)
                if not layout_id:
                    # new layouts
                    layout_type = LayoutTypes(data.get('layout_type', None))
                    custom_pick = models.LayoutControl(
                        name=data.get('title', None),
                        layout_type=layout_type,
                        sort_priority=sort_priority, is_active=True)
                else:
                    custom_pick = session.query(models.LayoutControl).get(layout_id)
                    # layout type update not allowed, messed up with existing layout banners/offers
                    # custom_pick.layout_type = layout_type
                    custom_pick.name = data.get('title', None)
                    custom_pick.sort_priority = sort_priority
                    custom_pick.is_active = True
                sort_priority -= 10
                session.add(custom_pick)
            # remove all inactive layouts
            inactive_layouts = session.query(models.LayoutControl).filter_by(is_active=False).all()
            for layout in inactive_layouts:
                # delete related custom pick layouts
                if layout.layout_type == LayoutTypes.editor_pick_offers:
                    session.execute('DELETE FROM layout_offers WHERE layout_id = {};'.format(layout.id))
                elif layout.layout_type == LayoutTypes.editor_pick_banners:
                    session.execute('DELETE FROM layout_banners WHERE layout_id = {};'.format(layout.id))
                # delete the layout
                session.delete(layout)
            session.commit()
        except Exception as e:
            session.rollback()
            raise BadRequest(user_message='Failed to update data',
                             developer_message=e.message)

    @user_has_any_tokens(
        'can_read_write_global_all',
        'can_read_write_public',
        'can_read_write_public_ext',
        'can_read_global_item')
    def purge(self):
        """ purge layout api and layout details api data from cache """
        redis_cache = self.cache
        redis_cache.delete_with_key_prefix(self.cache_key_name)
        redis_cache.delete_with_key_prefix('layout:details')
        return jsonify({'success_message': 'Cached data for layout api and layout details api successfully purged'})


class LayoutDetailsApi(ListPutApiResourceBase):

    query_set = models.LayoutOffer.query

    create_request_parser = LayoutOfferArgsParser

    list_request_parser = LayoutOfferListArgsParser

    response_serializer = LayoutCustomPickListSerializer

    viewmodel = LayoutOffersViewModel

    serialized_list_name = 'items'

    def __init__(self):
        super(LayoutDetailsApi, self).__init__()
        # for layout, cache will also reset at put. (default expiry = one day/8 = 3 hours)
        self.cache = RedisCache(current_app.kvs, expiry=ONE_DAY/8)
        self.layout_id = None
        self.layout_type = None

    def _get_filters(self):
        filters = super(LayoutDetailsApi, self)._get_filters()

        parental_control_id = get_filter_parental_control(
            request.args.get('parental_control_id', type=int, default=0))

        if self.layout_type == LayoutTypes.editor_pick_banners:
            filters.append(models.LayoutBanner.layout_id == self.layout_id)
            if ((g.current_client and not g.current_client.allow_age_restricted_content) or
                    (parental_control_id and parental_control_id == ParentalControlType.parental_level_1.value)):
                # filter only all ages banner (no adult banner)
                filters.append(Banner.is_age_restricted == False)
        elif self.layout_type == LayoutTypes.editor_pick_offers:
            filters.append(models.LayoutOffer.layout_id == self.layout_id)
        elif self.layout_type in [LayoutTypes.popular_all, LayoutTypes.popular_book,
                                  LayoutTypes.popular_magazine, LayoutTypes.popular_newspaper,
                                  LayoutTypes.latest_all, LayoutTypes.latest_book,
                                  LayoutTypes.latest_newspaper, LayoutTypes.latest_magazine
                                  ]:
            # more filter for latest and popular
            if self.layout_type in [LayoutTypes.popular_book, LayoutTypes.latest_book]:
                filters.append(Item.item_type == ItemTypes.book.value)
            elif self.layout_type in [LayoutTypes.popular_magazine, LayoutTypes.latest_magazine]:
                filters.append(Item.item_type == ItemTypes.magazine.value)
            elif self.layout_type in [LayoutTypes.popular_newspaper, LayoutTypes.latest_newspaper]:
                filters.append(Item.item_type == ItemTypes.newspaper.value)
            filters.append(Item.is_active == True)
            filters.append(Item.item_status == Item.Statuses.ready_for_consume.value)

            if self.layout_type in [LayoutTypes.popular_all, LayoutTypes.popular_book,
                                    LayoutTypes.popular_magazine, LayoutTypes.popular_newspaper, ]:
                # POPULAR :: only show NON FREE Item for a particular Platform
                #   Platform is in client from request headers: User-Agent or X-Scoop-Client
                platform_id = g.current_client.platform_id if getattr(g, 'current_client', None) else None
                if platform_id:
                    filters.append(PopularItem.non_free_platforms.any(platform_id))
                else:
                    filters.append(PopularItem.non_free_platforms != None)

            if parental_control_id:
                filters.append(Item.parentalcontrol_id == parental_control_id)
        return filters

    def get(self, db_id):
        self.layout_id = db_id
        self.cache_key_name = "layout:details:{}".format(self.layout_id)
        session = db.session()
        layout = session.query(LayoutControl).get(self.layout_id)
        if not layout:
            raise NotFound(user_message='Layout with id {} not found')
        self.layout_type = layout.layout_type
        session.close()
        return super(LayoutDetailsApi, self).get()

    def _get_pagination(self):
        offset = request.args.get('offset', 0, type=int)
        if g.current_client and g.current_client.id == SCOOP_PORTAL and (
                self.layout_type == LayoutTypes.editor_pick_banners
                or self.layout_type == LayoutTypes.editor_pick_offers):
            # for layout maintenance in portal
            default_limit = 500
        else:
            default_limit = 20
        limit = request.args.get('limit', default_limit, type=int)
        return limit, offset

    def _fetch_from_db(self):
        if self.layout_type == LayoutTypes.editor_pick_banners:
            self.query_set = models.LayoutBanner.query.join(models.LayoutBanner.banner)
            self.viewmodel = LayoutBannersViewModel
            self.default_ordering = (desc(models.LayoutBanner.sort_priority),)
        elif self.layout_type == LayoutTypes.editor_pick_offers:
            self.query_set = models.LayoutOffer.query
            self.viewmodel = LayoutOffersViewModel
            self.default_ordering = (desc(models.LayoutOffer.sort_priority),)
        elif self.layout_type in [LayoutTypes.latest_all, LayoutTypes.latest_book,
                                LayoutTypes.latest_newspaper, LayoutTypes.latest_magazine]:
            self.viewmodel = viewmodels.LayoutItemsViewModel
            self.query_set = Item.query
            if self.layout_type == LayoutTypes.latest_newspaper:
                # Additional default filter, to show the latest date newspapers only per brand
                one_week_duration = get_local() - timedelta(days=7)
                session = db.session()
                latest_ids = session.query(func.max(Item.id)).filter(
                    Item.release_date > one_week_duration,
                    Item.item_type == Item.Types.newspaper.value,
                    Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                    Item.is_active == True
                ).group_by(Item.brand_id).all()

                latest_ids = [id[0] for id in latest_ids]
                self.query_set = self.query_set.filter(Item.id.in_(latest_ids))
                self.default_ordering = (desc(Item.release_date), desc(Item.sort_priority), desc(Item.id))
            else:
                self.default_ordering = (desc(Item.release_date), desc(Item.id), )

        elif self.layout_type in [LayoutTypes.popular_all, LayoutTypes.popular_book,
                                  LayoutTypes.popular_magazine, LayoutTypes.popular_newspaper]:
            self.query_set = PopularItem.query.join(PopularItem.item)
            self.viewmodel = viewmodels.LayoutItemsViewModel
            self.default_ordering = (desc(PopularItem.download_count),)
        else:
            raise BadRequest(user_message='Only popular, editor pick offers and banners allowed',
                             developer_message='Only popular, editor pick offers and banners allowed')
        view_model = self._get_list_viewmodel(*self._get_pagination())
        response = jsonify(marshal(view_model, self._get_list_serializer()))
        weak_etag = self._generate_weak_etag(view_model)
        return response, weak_etag

    def put(self, db_id):
        self.layout_id = db_id
        self.cache_key_name = "layout:details:{}".format(self.layout_id)
        return super(LayoutDetailsApi, self).put()

    def _update_data_to_db(self, req_data):
        # validate request body
        req_data = req_data or []
        if req_data and len(req_data) > 0 and req_data[0].get('id', 0) == 0:
            raise UnprocessableEntity()

        session = db.session()
        layout = session.query(LayoutControl).get(self.layout_id)
        if not layout:
            raise NotFound(user_message='Layout with id {} not found')
        self.layout_type = layout.layout_type
        if self.layout_type not in [LayoutTypes.editor_pick_banners, LayoutTypes.editor_pick_offers]:
            session.close()
            raise BadRequest(user_message='Only editor pick offers and banners allowed',
                             developer_message='Only editor pick offers and banners allowed')

        try:
            # delete old data, and reinsert new data with the new order
            if self.layout_type == LayoutTypes.editor_pick_banners:
                session.execute('DELETE FROM layout_banners WHERE layout_id = {};'.format(self.layout_id))
            elif self.layout_type == LayoutTypes.editor_pick_offers:
                session.execute('DELETE FROM layout_offers WHERE layout_id = {};'.format(self.layout_id))
            sort_priority = len(req_data) * 10 if req_data else 0
            for data in req_data:
                if data.get('id', 0):
                    if self.layout_type == LayoutTypes.editor_pick_banners:
                        custom_pick = models.LayoutBanner(
                            layout_id=self.layout_id, banner_id=data.get('id'), sort_priority=sort_priority)
                    elif self.layout_type == LayoutTypes.editor_pick_offers:
                        custom_pick = models.LayoutOffer(
                            layout_id=self.layout_id, offer_id=data.get('id'), sort_priority=sort_priority)
                    sort_priority -= 10
                    session.add(custom_pick)
            session.commit()
        except Exception as e:
            session.rollback()
            raise BadRequest(user_message='Failed to update data',
                             developer_message=e.message)


class LayoutIntroApi(ListPutApiResourceBase):
    query_set = models.LayoutIntro.query

    default_ordering = (desc(models.LayoutIntro.sort_priority),
                        )

    create_request_parser = LayoutIntroArgsParser

    list_request_parser = LayoutIntroListArgsParser

    response_serializer = LayoutIntroSerializer

    viewmodel = LayoutIntroViewModel

    serialized_list_name = 'items'

    cache_key_name = "layout:intro"

    def _update_data_to_db(self, req_data):
        # validate request body
        if not req_data or len(req_data) == 0 or req_data[0].get('id', 0) == 0:
            raise BadRequest()

        session = db.session()
        try:
            # delete old data, and reinsert new data with the new order
            session.execute('DELETE FROM layout_intro;')
            sort_priority = len(req_data)
            for data in req_data:
                if data.get('id', 0):
                    layout_intro = models.LayoutIntro(brand_id=data.get('id'), sort_priority=sort_priority)
                    sort_priority -= 1
                    session.add(layout_intro)
            session.commit()
        except Exception as e:
            session.rollback()
            raise BadRequest(user_message='Failed to update data',
                             developer_message=e.message)
