from __future__ import unicode_literals
import unittest
import json

from datetime import datetime
from httplib import OK

from flask import g

from app import db, app
from app.analytics.models import UserDownload
from app.items.choices import ItemTypes
from app.items.models import PopularItem
from app.layouts.layout_type import LayoutTypes
from app.layouts.viewmodels import get_single_offer_prices
from app.master.choices import IOS
from app.master.models import Platform
from app.offers.choices.offer_type import SINGLE
from app.offers.models import offer_item, OfferType
from app.auth.models import Client
from app.utils.datetimes import datetime_to_isoformat
from app.utils.testcases import TestBase
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.items import ItemFactory
from scripts.popular_generator import main
from tests.fixtures.sqlalchemy.layouts import LayoutFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferPlatformFactory
from faker import Factory

fake = Factory.create()


class GetListItemPopularTest(TestBase):
    client = app.test_client(use_cookies=False)
    headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'X-Scoop-Client': 'scoop ios/1.0.0'
    }

    @classmethod
    def setUpClass(cls):
        db.engine.dispose()
        db.session().expunge_all()
        db.session().close_all()
        cls.drop_popular_view()
        super(GetListItemPopularTest, cls).setUpClass()
        cls.build_data(cls.session)

    @classmethod
    def build_data(cls, session):
        user = UserFactory(id=12345)
        layout = LayoutFactory(id=110, layout_type=LayoutTypes.popular_all)
        layout = LayoutFactory(id=111, layout_type=LayoutTypes.popular_book)
        layout = LayoutFactory(id=112, layout_type=LayoutTypes.popular_magazine)
        layout = LayoutFactory(id=113, layout_type=LayoutTypes.popular_newspaper)
        client = session.query(Client).get(1)
        client.allow_age_restricted_content = True
        session.add(client)
        # table item popular automatically created when db.create_all
        PopularItem.__table__.drop(db.engine, checkfirst=True)
        # db.engine.execute('DROP TABLE utility.item_popular;')

        cls.build_popular_data(session)

        main()

    @classmethod
    def drop_popular_view(cls):
        try:
            db.engine.execute('DROP MATERIALIZED VIEW utility.item_popular;')
        except:
            pass
        try:
            db.engine.execute('DROP MATERIALIZED VIEW utility.item_later_editions;')
        except:
            pass
        try:
            db.engine.execute('DROP MATERIALIZED VIEW utility.item_downloads;')
        except:
            pass
        try:
            db.engine.execute('DROP VIEW utility.non_free_item RESTRICT;')
        except:
            pass

    @classmethod
    def build_popular_data(cls, session):
        # build items
        cls.items = []
        for _ in range(3):
            item = ItemFactory(id=_+1, item_status='ready for consume', item_type='book')
            cls.items.append(item)

        for _ in range(3):
            item = ItemFactory(id=_+4, item_status='ready for consume', item_type='magazine')
            cls.items.append(item)

        for _ in range(3):
            item = ItemFactory(id=_+7, item_status='ready for consume', item_type='newspaper')
            cls.items.append(item)

        offer_type = session.query(OfferType).get(SINGLE)
        offer = OfferFactory(is_free=False, offer_type=offer_type)
        session.commit()
        platform_ios = session.query(Platform).get(IOS)
        offer_platform = OfferPlatformFactory(
            offer=offer,
            platform=platform_ios,
            price_usd=offer.price_usd,
            price_idr=offer.price_idr,
            price_point=offer.price_point,
        )
        session.commit()

        # build offer item
        for index in range(9):
            oi = offer_item.insert()
            oi.offer_id = offer.id
            oi.item_id = cls.items[index].id
            db.engine.execute(offer_item.insert(), offer_id=offer.id, item_id=cls.items[index].id)

        # build user activities
        cls.most_popular_item = cls.items[2]
        cls.more_popular_item = cls.items[1]
        cls.least_popular_item = cls.items[0]

        for _ in range(9):
            cls.generate_user_activities(session, cls.items[_], 1000 - (cls.items[_].id * 10))

        session.commit()
        for item in cls.items:
            session.expunge(item)

    @classmethod
    def generate_user_activities(cls, session, item, total_record):
        session.add(item)
        for _ in range(total_record):
            # UserActivityFactory(user_id=1, item=item, download_status=SUCCESS,
            #                     activity_type=DOWNLOAD, datetime=datetime.now())
            data = {
                "device_id": "123",
                "user_id": 1,
                "item_id": item.id,
                "session_name": "some session",
                "ip_address": "127.0.0.1",
                "download_status": "success",
                "activity_type": "download",
                "location": "64.54 32.64",
                "client_version": "1.2.3",
                "os_version": "abc",
                "device_model": "abc",
                "datetime": datetime_to_isoformat(datetime.now())
            }
            user_download = UserDownload(user_activity=data)
            session.add(user_download)

    @classmethod
    def tearDownClass(cls):

        # for testing purpose we delete view non_free_item

        db.session().close_all()
        db.session().commit()
        # remove the current SESSION from the SESSIONFACTORY
        db.session.remove()
        cls.drop_popular_view()
        db.engine.dispose()
        super(GetListItemPopularTest, cls).tearDownClass()

    def get_dict_item(self, item):
        with self.on_session(item):
            if item.item_type == ItemTypes.book.value:
                subtitle = ', '.join([author.name for author in item.authors])
            else:
                subtitle = item.issue_number
            return {
                'id': item.id,
                'title': item.name,
                'type': item.item_type,
                'href': item.api_url,
                'slug': item.slug,
                'subtitle': subtitle,
                'image_url': app.config['MEDIA_BASE_URL'] + item.image_highres if item else None,
                'image_normal': app.config['MEDIA_BASE_URL'] + item.image_normal if item else None,
                'thumb_normal': app.config['MEDIA_BASE_URL'] + item.thumb_image_normal if item else None,
                'thumb_highres': app.config['MEDIA_BASE_URL'] + item.thumb_image_highres if item else None,
                'price': get_single_offer_prices(item),
                'brand': {
                    "id": item.brand.id,
                    "title": item.brand.name,
                    "slug": item.brand.slug,
                    "href": item.brand.api_url,
                }
            }

    def test_most_popular_all(self):
        # self.resp = self.client.get('/v1/items/popular', headers=self.headers)
        self.resp = self.client.get('/v1/layouts/110', headers=self.headers)

        self.assertEqual(self.resp.status_code, OK, self.resp.data)
        self.result = json.loads(self.resp.data)
        self.assertGreater(len(self.result.get('items')), 0, "actual {}, response {}".format(
            len(self.result.get('items')),
            self.resp.data))
        # still problem with get is latest. the result is not same
        with app.test_request_context('/'):
            g.current_client = self.session.query(Client).filter_by(platform_id=IOS).first()
            for _ in range(9):
                expected = self.get_dict_item(self.items[_])
                self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_most_popular_all_getscoop(self):
        headers = self.headers.copy()
        headers['X-Scoop-Client'] = 'Scoop Web/1.0'
        self.resp = self.client.get('/v1/layouts/110', headers=headers)

        self.assertEqual(self.resp.status_code, OK, self.resp.data)
        self.result = json.loads(self.resp.data)
        self.assertGreater(len(self.result.get('items')), 0, "actual {}, response {}".format(
            len(self.result.get('items')),
            self.resp.data))
        # still problem with get is latest. the result is not same
        with app.test_request_context('/'):
            g.current_client = None
            for _ in range(9):
                expected = self.get_dict_item(self.items[_])
                # items.append(expected)
                self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_most_popular_book(self):
        # self.resp = self.client.get('/v1/items/popular', headers=self.headers)
        self.resp = self.client.get('/v1/layouts/111', headers=self.headers)

        self.assertEqual(self.resp.status_code, OK, self.resp.data)
        self.result = json.loads(self.resp.data)

        self.assertGreater(len(self.result.get('items')), 0, self.resp.data)
        # still problem with get is latest. the result is not same
        items = []
        with app.test_request_context('/'):
            g.current_client = self.session.query(Client).filter_by(platform_id=IOS).first()
            for _ in range(3):
                expected = self.get_dict_item(self.items[_])
                self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_most_popular_magazine(self):
        self.resp = self.client.get('/v1/layouts/112', headers=self.headers)

        self.assertEqual(self.resp.status_code, OK, self.resp.data)
        self.result = json.loads(self.resp.data)

        self.assertGreater(len(self.result.get('items')), 0, self.resp.data)
        with app.test_request_context('/'):
            g.current_client = self.session.query(Client).filter_by(platform_id=IOS).first()
            for _ in range(3):
                item_pos = _+3
                expected = self.get_dict_item(self.items[item_pos])
                self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_most_popular_newspaper(self):
        self.resp = self.client.get('/v1/layouts/113', headers=self.headers)

        self.assertEqual(self.resp.status_code, OK, self.resp.data)
        self.result = json.loads(self.resp.data)

        self.assertGreater(len(self.result.get('items')), 0, self.resp.data)
        with app.test_request_context('/'):
            g.current_client = self.session.query(Client).filter_by(platform_id=IOS).first()
            for _ in range(3):
                item_pos = _ + 6
                expected = self.get_dict_item(self.items[item_pos])
                self.assertDictContainsSubset(expected, self.result.get('items')[_])

