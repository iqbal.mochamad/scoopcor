from __future__ import unicode_literals
import unittest
import json
from httplib import OK

from datetime import timedelta

from sqlalchemy.exc import InvalidRequestError

from flask import g

from app import db, app
from app.auth.models import Client
from app.items.choices import ItemTypes
from app.layouts.layout_type import LayoutTypes
from app.layouts.viewmodels import get_single_offer_prices
from app.master.choices import IOS
from app.master.models import Platform
from app.utils.datetimes import get_local, get_utc_nieve
from app.utils.testcases import TestBase
from tests.fixtures.helpers import generate_static_sql_fixtures
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.items import ItemFactory
from tests.fixtures.sqlalchemy.layouts import LayoutFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferPlatformFactory, standard_offer_types
from faker import Factory

fake = Factory.create()


class GetListItemLatestTest(TestBase):
    client = app.test_client(use_cookies=False)
    headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'User-Agent': 'Scoop Web/1.0'

    }

    @classmethod
    def setUpClass(cls):
        super(GetListItemLatestTest, cls).setUpClass()
        user = UserFactory(id=12345)
        layout = LayoutFactory(id=110, layout_type=LayoutTypes.latest_all)
        layout = LayoutFactory(id=111, layout_type=LayoutTypes.latest_book)
        layout = LayoutFactory(id=112, layout_type=LayoutTypes.latest_magazine)
        layout = LayoutFactory(id=113, layout_type=LayoutTypes.latest_newspaper)
        cls.session.commit()
        cls.build_data(cls.session)

    @classmethod
    def build_data(cls, session):
        # generate non active items, should not get selected in api
        for _ in range(3):
            item = ItemFactory(id=_+1, item_status='ready for consume', item_type='book', is_active=False, slug=_+1)

        for _ in range(3):
            item = ItemFactory(id=_+4, item_status='ready for consume', item_type='magazine', is_active=False, slug=_+4)

        for _ in range(3):
            item = ItemFactory(id=_+10, item_status='ready for consume', item_type='newspaper',
                               is_active=False, slug=_+10,
                               release_date=get_local() - timedelta(days=20))

        release_data = get_utc_nieve()
        # build latest items
        cls.items = []
        for _ in range(3):
            item = ItemFactory(id=500-_, item_status='ready for consume', item_type='book',
                               release_date=release_data)
            cls.items.append(item)

        for _ in range(3):
            item = ItemFactory(id=400-_, item_status='ready for consume', item_type='magazine',
                               release_date=release_data)
            cls.items.append(item)

        for _ in range(3):
            brand = BrandFactory(id=300-_)
            item = ItemFactory(id=300-_, item_status='ready for consume', item_type='newspaper',
                               brand=brand,
                               release_date=release_data - timedelta(days=_))
            cls.items.append(item)

        offer_type = standard_offer_types()['single']
        session.commit()

        platform = session.query(Platform).get(IOS)

        # build offer item
        for index in range(9):
            offer = OfferFactory(id=index+1, is_free=False, offer_type=offer_type)
            offer_platform = OfferPlatformFactory(
                id=index+1, offer=offer, platform=platform,
                price_usd=offer.price_usd,
                price_idr=offer.price_idr,
                price_point=offer.price_point,)
            offer.items.append(cls.items[index])
            # oi = offer_item.insert()
            # oi.offer_id = offer.id
            # oi.item_id = cls.items[index].id
            # db.engine.execute(offer_item.insert(), offer_id=offer.id, item_id=cls.items[index].id)

        session.commit()
        for item in cls.items:
            session.expunge(item)

    def get_dict_item(self, item):
        with self.on_session(item):
            if item.item_type == ItemTypes.book.value:
                subtitle = ', '.join([author.name for author in item.authors])
            else:
                subtitle = item.issue_number
            return {
                'id': item.id,
                'title': item.name,
                'type': item.item_type,
                'href': item.api_url,
                'slug': item.slug,
                'subtitle': subtitle,
                'image_url': app.config['MEDIA_BASE_URL'] + item.image_highres if item else None,
                'image_normal': app.config['MEDIA_BASE_URL'] + item.image_normal if item else None,
                'thumb_normal': app.config['MEDIA_BASE_URL'] + item.thumb_image_normal if item else None,
                'thumb_highres': app.config['MEDIA_BASE_URL'] + item.thumb_image_highres if item else None,
                'price': get_single_offer_prices(item),
                'brand': {
                    "id": item.brand.id,
                    "title": item.brand.name,
                    "slug": item.brand.slug,
                    "href": item.brand.api_url,
                }
            }

    def test_latest_all(self):
        # self.resp = self.client.get('/v1/items/popular', headers=self.headers)
        self.resp = self.client.get('/v1/layouts/110', headers=self.headers)
        self.assertEqual(self.resp.status_code, OK, self.resp.data)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 9, self.resp.data)
        # still problem with get is latest. the result is not same
        items = []
        with app.test_request_context('/'):
            for _ in range(9):
                expected = self.get_dict_item(self.items[_])
                self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_all_ios(self):
        new_headers = self.headers.copy()
        new_headers['User-Agent'] = 'Scoop IOS/1.0'
        self.resp = self.client.get('/v1/layouts/110', headers=new_headers)
        self.assertEqual(self.resp.status_code, OK, self.resp.data)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 9, self.resp.data)
        # still problem with get is latest. the result is not same
        items = []
        with app.test_request_context('/'):
            g.current_client = self.session.query(Client).filter_by(platform_id=IOS).first()
            for _ in range(9):
                expected = self.get_dict_item(self.items[_])
                self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_book(self):
        # self.resp = self.client.get('/v1/items/popular', headers=self.headers)
        self.resp = self.client.get('/v1/layouts/111', headers=self.headers)
        self.assertEqual(self.resp.status_code, OK, self.resp.data)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        # still problem with get is latest. the result is not same
        items = []
        with app.test_request_context('/'):
            g.current_client = None
            for _ in range(3):
                expected = self.get_dict_item(self.items[_])
                self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_magazine(self):
        self.resp = self.client.get('/v1/layouts/112', headers=self.headers)
        self.assertEqual(self.resp.status_code, OK, self.resp.data)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        with app.test_request_context('/'):
            for _ in range(3):
                item_pos = _+3
                expected = self.get_dict_item(self.items[item_pos])
                self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_newspaper(self):
        self.resp = self.client.get('/v1/layouts/113', headers=self.headers)
        self.assertEqual(self.resp.status_code, OK, self.resp.data)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        with app.test_request_context('/'):
            for _ in range(3):
                item_pos = _ + 6
                expected = self.get_dict_item(self.items[item_pos])
                self.assertDictContainsSubset(expected, self.result.get('items')[_])
