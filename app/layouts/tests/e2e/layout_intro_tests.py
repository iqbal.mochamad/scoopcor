from __future__ import unicode_literals, absolute_import

import json
from unittest import TestCase
from flask import g

from faker import Factory
from nose.tools import istest, nottest
from sqlalchemy import desc

from app import db, set_request_id, app
from app.auth.models import UserInfo
from app.helpers import load_json_schema
from app.items.choices import STATUS_READY_FOR_CONSUME, ItemTypes
from app.items.choices import STATUS_TYPES
from app.items.models import Item
from app.layouts.models import LayoutIntro
from app.master.models import Brand
from app.users.users import Role, User
from app.utils.json_schema import validate_scoop_jsonschema
from tests.e2e.orders.order_pay_with_wallet_tests import init_g_current_user
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import ItemFactory, AuthorFactory
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.layouts import LayoutIntroFactory
from tests.fixtures.sqlalchemy.master import BrandFactory, VendorFactory
from tests import testcases


@istest
class ListTests(testcases.ListTestCase):
    maxDiff = None
    request_url = "/v1/layouts/intro"
    list_key = 'items'
    fixture_factory = LayoutIntroFactory

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache'
    }

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        s.expire_on_commit = False
        brands = s.query(Brand).order_by(Brand.id).all()
        sort_priority = 1000
        for brand in brands:
            sort_priority -= 1
            layout_intro = LayoutIntroFactory(brand=brand, sort_priority=sort_priority)
        s.commit()
        s.expunge_all()
        return s.query(LayoutIntro).order_by(desc(LayoutIntro.sort_priority)).all()

    # @patch("app.layout.app.cache", mock_redis_client)
    @classmethod
    def get_api_response(cls):
        return super(ListTests, cls).get_api_response()

    def test_response_body(self):
        response_json = json.loads(self.response.data)
        i = 0
        # make sure the priority is the same in response (sort by sort_priority desc)
        for entity_json in response_json[self.list_key]:
            entity = self.fixtures[i]
            self.assert_entity_detail_expectation(entity, entity_json)
            i += 1

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        session = db.session
        session.add(fixture)
        expected = get_expected(fixture.brand)
        session.expunge(fixture)
        self.assertDictEqual(fixture_json, expected)

    def test_response_json_schema(self):
        response_json = json.loads(self.response.data)
        validate_scoop_jsonschema(response_json, load_json_schema('layouts', 'list-layout-intro'))


@nottest
class CreateWithPutTest(testcases.UpdateTestCase):

    request_url = "/v1/layouts/intro"
    fixture_factory = LayoutIntroFactory

    @classmethod
    def setUpClass(cls):
        cls.response = cls.get_api_response()

    @classmethod
    def get_api_request_body(cls):
        s = db.session()
        s.expire_on_commit = False
        brands = s.query(Brand).filter(Brand.default_parentalcontrol_id == 1).order_by(Brand.id).all()
        items = []
        for brand in brands:
            items.append(get_expected(brand))
            s.expunge(brand)
        return {
                'items': items,
            }

    def test_response_body(self):
        # the response body should be in the same order as the request body!
        actual = json.loads(self.response.data)
        self.assertDictContainsSubset(
            expected=self.request_body,
            actual=actual)


@nottest
class UpdateTest(testcases.UpdateTestCase):

    request_url = "/v1/layouts/intro"
    fixture_factory = LayoutIntroFactory

    @classmethod
    def set_up_fixtures(cls):
        # create initial data before update!
        s = db.session()
        brands = s.query(Brand).order_by(Brand.id).all()
        sort_priority = 1000
        for brand in brands:
            sort_priority -= 1
            layout_intro = LayoutIntroFactory(brand=brand, sort_priority=sort_priority)
        s.commit()
        s.expunge(brands)
        s.expunge_all()
        return s.query(LayoutIntro).order_by(desc(LayoutIntro.sort_priority)).all()

    @classmethod
    def get_api_request_body(cls):
        s = db.session()
        brands = s.query(Brand).filter(Brand.default_parentalcontrol_id == 1).order_by(Brand.id).all()
        items = []
        for brand in brands:
            items.append(get_expected(brand))
            s.expunge(brand)
        return {
                'items': items,
            }

    def test_response_body(self):
        # the response body should be in the same order as the request body!
        actual = json.loads(self.response.data)
        self.assertDictContainsSubset(
            expected=self.request_body,
            actual=actual)


@nottest
class OptionSuperUserTest(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_super_user,
            ]
        }
        client = app.test_client(use_cookies=False)
        cls.response = client.options("/v1/layouts/intro")

    def test_allowed_options(self):
        allowed_header = [x.strip() for x in self.response.headers.get('allow', '').split(',')]
        expected = ['GET', 'HEAD', 'OPTIONS', 'PUT']
        self.assertItemsEqual(expected, allowed_header,
                              'Expected {} vs actual {}'.format(expected, allowed_header))


@nottest
class OptionAuthorizedUserTest(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_authorized_user,
            ]
        }
        client = app.test_client(use_cookies=False)
        cls.response = client.options("/v1/layouts/intro")

    def test_allowed_options(self):
        allowed_header = [x.strip() for x in self.response.headers.get('allow', '').split(',')]
        expected = ['GET', 'HEAD', 'OPTIONS', 'PUT']
        self.assertItemsEqual(expected, allowed_header,
                              'Expected {} vs actual {}'.format(expected, allowed_header))


@nottest
class OptionReadOnlyUserTest(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_read_only_user,
            ]
        }
        client = app.test_client(use_cookies=False)
        cls.response = client.options("/v1/layouts/intro")

    def test_allowed_options(self):
        allowed_header = [x.strip() for x in self.response.headers.get('allow', '').split(',')]
        expected = ['GET', 'HEAD', 'OPTIONS']
        self.assertItemsEqual(expected, allowed_header,
                              'Expected {} vs actual {}'.format(expected, allowed_header))


@nottest
class OptionNonAuthorizedUserTest(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_not_authorized_user,
            ]
        }
        client = app.test_client(use_cookies=False)
        cls.response = client.options("/v1/layouts/intro")

    def test_allowed_options(self):
        allowed_header = [x.strip() for x in self.response.headers.get('allow', '').split(',')]
        expected = ['OPTIONS', ]
        self.assertItemsEqual(expected, allowed_header,
                              'Expected {} vs actual {}'.format(expected, allowed_header))


def get_expected(brand):
    if brand.default_item_type == ItemTypes.book.value:
        item = brand.items.first()
    else:
        # for newspaper and magazine, get the latest edition!
        item = brand.items.filter(
            Item.is_active == True,
            Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME]
        ).order_by(desc(Item.id)).first()
    expected = {
            'id': brand.id,
            'title': brand.name,
            'href': '{}{}'.format(
                app.config['MEDIA_BASE_URL'],
                item.image_highres)
        }
    return expected


_fake = Factory.create()
original_before_request_funcs = None


def setup_module():
    db.session().close_all()
    db.engine.dispose()
    db.drop_all()
    db.create_all()
    global original_before_request_funcs
    original_before_request_funcs = app.before_request_funcs
    init_data()


def teardown_module():
    app.before_request_funcs = original_before_request_funcs
    db.session.close_all()
    db.session.expunge_all()
    db.drop_all()
    db.engine.dispose()


def init_data():
    s = db.session()
    generate_static_sql_fixtures(s)
    vendor = VendorFactory()
    brand_book = BrandFactory(
        id=11,
        default_item_type=ItemTypes.book.value,
        default_parentalcontrol_id=1,
        vendor=vendor
    )
    item11 = ItemFactory(id=11, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                         is_active=True, item_type=ItemTypes.book.value,
                         brand=brand_book)
    author = AuthorFactory()
    item11.authors.append(author)
    brand_book_agerestricted = BrandFactory(
        id=12,
        default_item_type=ItemTypes.book.value,
        default_parentalcontrol_id=2,
        vendor=vendor
    )
    item12 = ItemFactory(id=12, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                         is_active=True, item_type=ItemTypes.book.value,
                         brand=brand_book_agerestricted)
    brand_mag = BrandFactory(
        id=21,
        default_item_type=ItemTypes.magazine.value,
        default_parentalcontrol_id=1,
        vendor=vendor
    )
    item21 = ItemFactory(id=21, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                         is_active=True, item_type=ItemTypes.magazine.value,
                         brand=brand_mag)
    brand_mag_agerestricted = BrandFactory(
        id=22,
        default_item_type=ItemTypes.magazine.value,
        default_parentalcontrol_id=2,
        vendor=vendor
    )
    item22 = ItemFactory(id=22, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                         is_active=True, item_type=ItemTypes.magazine.value,
                         brand=brand_mag_agerestricted)
    brand_news = BrandFactory(
        id=31,
        default_item_type=ItemTypes.newspaper.value,
        default_parentalcontrol_id=1,
        vendor=vendor
    )
    item31 = ItemFactory(id=31, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                         is_active=True, item_type=ItemTypes.newspaper.value,
                         brand=brand_news)
    brand_news_agerestricted = BrandFactory(
        id=32,
        default_item_type=ItemTypes.newspaper.value,
        default_parentalcontrol_id=2,
        vendor=vendor
    )
    item32 = ItemFactory(id=32, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                         is_active=True, item_type=ItemTypes.newspaper.value,
                         brand=brand_news_agerestricted)
    super_user = UserFactory(id=12345)
    # get role admin and assign user to this roles
    role = s.query(Role).get(1)
    super_user.roles.append(role)
    authorized_user = UserFactory(id=112233)
    role_verified_user = s.query(Role).get(10)
    authorized_user.roles.append(role_verified_user)
    not_authorized_user = UserFactory(id=223344)
    role_verified_user = s.query(Role).get(10)
    not_authorized_user.roles.append(role_verified_user)
    s.commit()
    s.expunge_all()
    app.before_request_funcs = {
        None: [
            set_request_id,
            init_g_current_user,
        ]
    }


def init_g_current_super_user():
    session = db.session()
    g.current_user = session.query(User).get(12345)
    g.user = UserInfo(username=g.current_user.username,
                      user_id=g.current_user.id, token='abcd', perm=['can_read_write_global_all', ])
    session.close()


def init_g_current_authorized_user():
    session = db.session()
    g.current_user = session.query(User).get(112233)
    g.user = UserInfo(username=g.current_user.username,
                      user_id=g.current_user.id, token='abcd', perm=['can_read_write_global_banner', ])
    session.close()


def init_g_current_read_only_user():
    session = db.session()
    g.current_user = session.query(User).get(112233)
    g.user = UserInfo(username=g.current_user.username,
                      user_id=g.current_user.id, token='abcd', perm=['can_read_global_banner', ])
    session.close()


def init_g_current_not_authorized_user():
    session = db.session()
    g.current_user = session.query(User).get(223344)
    g.user = UserInfo(username=g.current_user.username,
                      user_id=g.current_user.id, token='abcd', perm=['abcde', ])
    session.close()
