from __future__ import unicode_literals, absolute_import
import json
from httplib import OK
from unittest import TestCase
from os.path import join

from dateutil.relativedelta import relativedelta

from app import app, db
from app.helpers import load_json_schema
from app.layouts.banners import BannerZone, BannerActions
from app.parental_controls.choices import ParentalControlType
from app.utils.json_schema import validate_scoop_jsonschema
from app.utils.datetimes import get_utc_nieve
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import BannerFactory


THREE_WEEKS_AGO = get_utc_nieve() - relativedelta(weeks=3)
TWO_WEEKS_AGO = get_utc_nieve() - relativedelta(weeks=2)

ONE_WEEK_FROM_NOW = get_utc_nieve() + relativedelta(weeks=1)
TWO_WEEKS_FROM_NOW = get_utc_nieve() + relativedelta(weeks=2)


class ListQueryTests(TestCase):

    @classmethod
    def setUpClass(cls):
        db.session().close_all()
        db.drop_all()
        db.create_all()
        cls.session = db.session()
        generate_static_sql_fixtures(cls.session)

        # old/inactive banner
        for _ in range(3):
            BannerFactory(valid_from=THREE_WEEKS_AGO, valid_to=TWO_WEEKS_AGO, clients=[7, ],
                          zone=BannerZone.promo_page,
                          is_active=True,
                          banner_type=BannerActions.promo)

        # current active banner -- start
        for _ in range(4):
            BannerFactory(valid_from=TWO_WEEKS_AGO, valid_to=ONE_WEEK_FROM_NOW, clients=[7],
                          zone=BannerZone.promo_page,
                          banner_type=BannerActions.promo,
                          countries=['ID', ],
                          is_active=True,
                          is_age_restricted=True)

        for _ in range(10):
            BannerFactory(valid_from=TWO_WEEKS_AGO, valid_to=ONE_WEEK_FROM_NOW, clients=[1, 2],
                          zone=BannerZone.homepage,
                          is_active=True,
                          banner_type=BannerActions.landing_page)

        for _ in range(6):
            BannerFactory(valid_from=TWO_WEEKS_AGO, valid_to=ONE_WEEK_FROM_NOW, clients=[1, ],
                          zone=BannerZone.magazine_page,
                          countries=['SG', ],
                          is_active=True,
                          banner_type=BannerActions.open_url)
        # current active banner -- end

        # future active banner
        for _ in range(7):
            BannerFactory(valid_from=ONE_WEEK_FROM_NOW, valid_to=TWO_WEEKS_FROM_NOW, clients=[1, ],
                          zone=BannerZone.magazine_page,
                          is_active=True,
                          banner_type=BannerActions.open_url)
        cls.session.commit()
        super(ListQueryTests, cls).setUpClass()

    def setUp(self):

        self.api_client = app.test_client(use_cookies=False)
        super(ListQueryTests, self).setUp()

    @classmethod
    def tearDownClass(self):
        self.session.close_all()
        db.drop_all()

    def test_response_v2_format(self):
        """ Make sure the JSON data matches our documented response format.
        """
        response = self.api_client.get('/v1/banners')
        schema = load_json_schema('items', join('v2', 'banner.list'))
        validate_scoop_jsonschema(json.loads(response.data), schema)

    def test_response_v2_is_default_format(self):
        """ Make sure v2 is our default response type, if none or if application/json is specified.
        """
        response_no_accept_type = self.api_client.get('/v1/banners')
        response_json_accept_type = self.api_client.get('/v1/banners', headers={'Accept': 'application/json'})

        self.assertDictEqual(
            json.loads(response_no_accept_type.data),
            json.loads(response_json_accept_type.data)
        )

    def test_inactive_banners_are_excluded(self):
        """ Make sure we're only getting active banners back by default (ones who are active currently).
        """
        response = self.api_client.get('/v1/banners?limit=100')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, OK, 'Status Code not OK. response {}'.format(response.data))
        self.assertEqual(response_json['metadata']['resultset']['count'], 20)

    def test_filter_by_clients(self):
        """ Make sure we're only getting banners with the same client id.
        """
        response = self.api_client.get('/v1/banners?limit=100&client_id=7')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, OK, 'Status Code not OK. response {}'.format(response.data))
        self.assertEqual(response_json['metadata']['resultset']['count'], 4)

        response = self.api_client.get('/v1/banners?limit=100&client_id=1')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, OK, 'Status Code not OK. response {}'.format(response.data))
        self.assertEqual(response_json['metadata']['resultset']['count'], 16)

    def test_banner_date_filtering(self):
        response = self.api_client.get(
            '/v1/banners?valid_from__gte=1945-01-01T00:00:00Z&valid_to__lte=2100-01-01T00:00:00Z&limit=100')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, OK, 'Status Code not OK. response {}'.format(response.data))
        self.assertEqual(response_json['metadata']['resultset']['count'], 30)

    def test_filter_by_parental_control_id(self):
        response = self.api_client.get('/v1/banners?limit=100&parental_control_id={}'.format(
            ParentalControlType.parental_level_1.value
        ))
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, OK, 'Status Code not OK. response {}'.format(response.data))
        self.assertEqual(response_json['metadata']['resultset']['count'], 16)

    def test_filter_by_zone(self):
        response = self.api_client.get('/v1/banners?limit=100&zone=promo page')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, OK, 'Status Code not OK. response {}'.format(response.data))
        self.assertEqual(response_json['metadata']['resultset']['count'], 4)

        response = self.api_client.get('/v1/banners?limit=100&zone=homepage')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, OK, 'Status Code not OK. response {}'.format(response.data))
        self.assertEqual(response_json['metadata']['resultset']['count'], 10)

    def test_filter_by_banner_type(self):
        response = self.api_client.get('/v1/banners?limit=100&banner_type=6')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, OK, 'Status Code not OK. response {}'.format(response.data))
        self.assertEqual(response_json['metadata']['resultset']['count'], 4)

        response = self.api_client.get('/v1/banners?limit=100&banner_type=1')
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, OK, 'Status Code not OK. response {}'.format(response.data))
        self.assertEqual(response_json['metadata']['resultset']['count'], 10)

    def test_filter_by_country(self):
        response = self.api_client.get('/v1/banners', headers={
            'Accept': 'application/json',
            'User-Agent': 'scoop_ios/v1.1.0',
            # simulate header by GeoIP extension in Nginx
            'GEOIP_COUNTRY_CODE': 'ID',
        })
        response_json = json.loads(response.data)
        self.assertEqual(response.status_code, OK, 'Status Code not OK. response {}'.format(response.data))
        self.assertEqual(response_json['metadata']['resultset']['count'], 14)

        # request from scoop portal, for data maintenance
        #  should not filtered by CountryID in GeoIP
        #    test filter active-only=1 too.
        response2 = self.api_client.get(
            '/v1/banners?active-only=1&limit=100',
            headers={
                'Accept': 'application/json',
                'User-Agent': 'scoop_portal/v1.1.0',
                # simulate header by GeoIP extension in Nginx
                'GEOIP_COUNTRY_CODE': 'ID',
            }
        )
        response_json = json.loads(response2.data)
        self.assertEqual(response2.status_code, OK, 'Status Code not OK. response {}'.format(response2.data))
        self.assertEqual(response_json['metadata']['resultset']['count'], 20)
