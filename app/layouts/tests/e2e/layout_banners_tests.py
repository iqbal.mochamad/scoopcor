from __future__ import unicode_literals, absolute_import

import json

from faker import Factory
from flask import g
from nose.tools import istest
from sqlalchemy import desc

from app import db, set_request_id, app
from app.auth.models import Client
from app.auth.models import UserInfo
from app.constants import MIME_TYPE_V3_JSON
from app.helpers import load_json_schema, RoleType
from app.layouts.banners import BannerActions, Banner
from app.layouts.helpers import get_href_for_banners, banner_image_url
from app.layouts.layout_type import LayoutTypes
from app.layouts.models import LayoutControl, LayoutBanner
from app.users.users import User
from app.utils.json_schema import validate_scoop_jsonschema
from tests import testcases
from tests.fixtures.helpers import create_user_and_token
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import BannerFactory
from tests.fixtures.sqlalchemy import ClientFactory
from tests.fixtures.sqlalchemy.layouts import LayoutFactory, LayoutBannerFactory


LAYOUT_ID = 102


@istest
class ListTests(testcases.ListTestCase):
    maxDiff = None
    request_url = "/v1/layouts/{}".format(LAYOUT_ID)
    list_key = 'items'
    fixture_factory = LayoutBannerFactory

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache'
    }

    @classmethod
    def setUpClass(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }
        super(ListTests, cls).setUpClass()

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        s.expire_on_commit = False
        layout = s.query(LayoutControl).get(LAYOUT_ID)
        sort_priority = 1000
        for banner in s.query(Banner).all():
            sort_priority -= 1
            layout_detail = LayoutBannerFactory(layout=layout, banner_id=banner.id, sort_priority=sort_priority)
        s.commit()
        return s.query(LayoutBanner).order_by(desc(LayoutBanner.sort_priority)).all()

    # @patch("app.layout.app.cache", mock_redis_client)
    @classmethod
    def get_api_response(cls):
        return super(ListTests, cls).get_api_response()

    def test_response_body(self):
        response_json = json.loads(self.response.data)[self.list_key]
        expected_lists = self.fixtures
        # make sure the priority is the same in response (sort by sort_priority desc)
        self.assertEqual(len(expected_lists), len(response_json),
                         'Response count is not match, expected {} vs actual {}, response: {}'.format(
                             len(expected_lists), len(response_json), response_json))
        i = 0
        for entity_json in response_json:
            entity = self.fixtures[i]
            self.assert_entity_detail_expectation(entity, entity_json)
            i += 1

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        session = db.session
        session.add(fixture)
        expected = get_expected(fixture.banner)
        self.assertDictEqual(fixture_json, expected)

    def test_response_json_schema(self):
        response_json = json.loads(self.response.data)
        validate_scoop_jsonschema(response_json, load_json_schema('layouts', 'list-layout-banners'))


@istest
class ListAgeRestrictedTests(testcases.ListTestCase):
    maxDiff = None
    request_url = "/v1/layouts/{}".format(LAYOUT_ID)
    list_key = 'items'
    fixture_factory = LayoutBannerFactory

    request_headers = {
        'Accept': MIME_TYPE_V3_JSON,
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache'
    }

    @classmethod
    def _set_before_request_funcs(cls):
        ctx = app.test_request_context('/')
        ctx.push()

        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user_client_not_age_allowed,
            ]
        }

    @classmethod
    def setUpClass(cls):
        cls._set_before_request_funcs()
        super(ListAgeRestrictedTests, cls).setUpClass()

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        s.expire_on_commit = False
        layout = s.query(LayoutControl).get(LAYOUT_ID)
        sort_priority = 1000
        for banner in s.query(Banner).all():
            sort_priority -= 1
            layout_detail = LayoutBannerFactory(layout=layout, banner_id=banner.id, sort_priority=sort_priority)
        s.commit()
        return s.query(LayoutBanner).join(LayoutBanner.banner).filter_by(
            is_age_restricted=False).order_by(desc(LayoutBanner.sort_priority)).all()

    # @patch("app.layout.app.cache", mock_redis_client)
    @classmethod
    def get_api_response(cls):
        return super(ListAgeRestrictedTests, cls).get_api_response()

    def test_response_body(self):
        response_json = json.loads(self.response.data)[self.list_key]
        expected_lists = self.fixtures
        # make sure the priority is the same in response (sort by sort_priority desc)
        self.assertEqual(len(expected_lists), len(response_json),
                         'Response count is not match, expected {} vs actual {}, response: {}'.format(
                             len(expected_lists), len(response_json), response_json))
        i = 0
        for entity_json in response_json:
            entity = self.fixtures[i]
            self.assert_entity_detail_expectation(entity, entity_json)
            i += 1

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        session = db.session
        session.add(fixture)
        expected = get_expected(fixture.banner)
        self.assertDictEqual(fixture_json, expected)

    def test_response_json_schema(self):
        response_json = json.loads(self.response.data)
        validate_scoop_jsonschema(response_json, load_json_schema('layouts', 'list-layout-banners'))


@istest
class ListAgeRestrictedViaQueryParamTests(ListAgeRestrictedTests):

    request_url = "/v1/layouts/{}?parental_control_id=1".format(LAYOUT_ID)

    @classmethod
    def _set_before_request_funcs(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }


@istest
class ListAgeRestrictedViaUserSettingsTests(ListAgeRestrictedTests):

    request_url = "/v1/layouts/{}?".format(LAYOUT_ID)

    @classmethod
    def _set_before_request_funcs(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user_not_allow_age_restricted,
            ]
        }


@istest
class CreateWithPutTest(testcases.UpdateTestCase):

    maxDiff = None
    request_url = "/v1/layouts/{}".format(LAYOUT_ID)
    list_key = 'items'
    fixture_factory = LayoutBannerFactory

    @classmethod
    def setUpClass(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }
        cls.response = cls.get_api_response()

    @classmethod
    def get_api_request_body(cls):
        s = db.session()
        s.expire_on_commit = False
        banners = s.query(Banner).filter(Banner.id > 1).order_by(Banner.id).all()
        banners_data = []
        for banner in banners:
            banners_data.append(get_expected(banner=banner))
        return {
            'items': banners_data,
        }

    def test_response_body(self):
        # the response body should be in the same order as the request body!
        actual = json.loads(self.response.data)
        self.assertDictContainsSubset(
            expected=self.request_body,
            actual=actual)


@istest
class UpdateTest(testcases.UpdateTestCase):

    maxDiff = None
    request_url = "/v1/layouts/{}".format(LAYOUT_ID)
    list_key = 'items'
    fixture_factory = LayoutBannerFactory

    @classmethod
    def setUpClass(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }
        cls.response = cls.get_api_response()

    @classmethod
    def set_up_fixtures(cls):
        # create initial data before update!
        s = db.session()
        s.expire_on_commit = False
        banners = s.query(Banner).order_by(Banner.id).all()
        sort_priority = 1000
        for banner in banners:
            sort_priority -= 1
            layout_intro = LayoutBannerFactory(
                layout_id=LAYOUT_ID, banner_id=banner.id, sort_priority=sort_priority)
        s.commit()
        return s.query(LayoutBanner).order_by(desc(LayoutBanner.sort_priority)).all()

    @classmethod
    def get_api_request_body(cls):
        s = db.session()
        s.expire_on_commit = False
        banners = s.query(Banner).filter(Banner.id > 2).order_by(Banner.id).all()
        banners_data = []
        for banner in banners:
            banners_data.append(get_expected(banner=banner))
        return {
            'items': banners_data,
        }

    def test_response_body(self):
        # the response body should be in the same order as the request body!
        actual = json.loads(self.response.data)
        self.assertDictContainsSubset(
            expected=self.request_body,
            actual=actual)


_fake = Factory.create()
original_before_request_funcs = None


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()
    s = db.session()
    global original_before_request_funcs
    original_before_request_funcs = app.before_request_funcs
    s.expire_on_commit = False
    generate_static_sql_fixtures(s)
    create_user_and_token(
        s, RoleType.super_admin,
        id=12345
    )
    layout = LayoutFactory(id=LAYOUT_ID, layout_type=LayoutTypes.editor_pick_banners)
    banner1 = BannerFactory(id=1, banner_type=BannerActions.brand, payload=445, is_age_restricted=False)
    banner2 = BannerFactory(id=2, banner_type=BannerActions.open_url, payload='http://urlshort.com/abcde',
                            is_age_restricted=False)
    banner3 = BannerFactory(id=3, banner_type=BannerActions.static, payload=None, is_age_restricted=False)
    banner4 = BannerFactory(id=4, banner_type=BannerActions.promo, payload=234, is_age_restricted=False)
    banner5 = BannerFactory(id=5, banner_type=BannerActions.landing_page, payload=532, is_age_restricted=False)
    banner6 = BannerFactory(id=6, banner_type=BannerActions.buffet, payload=235, is_age_restricted=True)
    s.commit()


def teardown_module():
    app.before_request_funcs = original_before_request_funcs
    db.session().close_all()
    db.drop_all()


def get_expected(banner):
    with app.test_request_context('/'):
        expected = {
            'id': banner.id,
            'title': banner.name,
            'type': banner.banner_type.value,
            'href': get_href_for_banners(banner_type=banner.banner_type, payload=banner.payload),
            'subtitle': None,
            'slug': banner.slug,
            'image_url': banner_image_url(banner.image_highres),
            'image_normal': banner_image_url(banner.image_normal),
            'thumb_normal': banner_image_url(banner.image_social_media),
            'thumb_highres': banner_image_url(banner.image_iphone),
            'price': None,
            'brand': None,
        }
        return expected


def init_g_current_user():
    g.user = UserInfo(username='dfdsafsdf', user_id=12345, token='abcd', perm=['can_read_write_global_all',])
    session = db.session()
    g.current_user = session.query(User).get(12345)
    g.current_user.allow_age_restricted_content = True
    g.current_client = ClientFactory(id=10000, allow_age_restricted_content=True)
    session.close()


def init_g_current_user_client_not_age_allowed():
    g.user = UserInfo(username='dfdsafsdf', user_id=12345, token='abcd', perm=['can_read_write_global_all', ])
    session = db.session()
    g.current_user = session.query(User).get(12345)
    g.current_user.allow_age_restricted_content = True
    g.current_client = ClientFactory(id=10001, allow_age_restricted_content=False)
    session.close()


def init_g_current_user_not_allow_age_restricted():
    g.user = UserInfo(username='dfdsafsdf', user_id=12345, token='abcd', perm=['can_read_write_global_all',])
    session = db.session()
    g.current_user = session.query(User).get(12345)
    g.current_user.allow_age_restricted_content = False
    g.current_client = ClientFactory(id=10000, allow_age_restricted_content=True)
    session.close()
