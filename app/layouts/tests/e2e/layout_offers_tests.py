from __future__ import unicode_literals, absolute_import

import json
from faker import Factory
from nose.tools import istest, nottest
from sqlalchemy import desc
from flask import g

from app import db, set_request_id, app
from app.auth.models import UserInfo, Client
from app.helpers import load_json_schema
from app.items.choices import STATUS_READY_FOR_CONSUME, ItemTypes
from app.items.choices import STATUS_TYPES
from app.items.models import Item
from app.layouts.layout_type import LayoutTypes
from app.layouts.models import LayoutOffer, LayoutControl
from app.master.choices import PlatformType
from app.offers.choices.offer_type import SINGLE
from app.offers.models import Offer, OfferPlatform
from app.auth.models import Client
from app.services.elevenia.entities.choices.result_status import OK
from app.users.users import User
from app.utils.json_schema import validate_scoop_jsonschema
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import ItemFactory, AuthorFactory, ClientFactory
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.layouts import LayoutFactory, LayoutOfferFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferTypeFactory, standard_offer_types
from tests import testcases


LAYOUT_ID = 101


@istest
class ListTests(testcases.ListTestCase):
    maxDiff = None
    request_url = "/v1/layouts/{}?limit=500".format(LAYOUT_ID)
    list_key = 'items'
    fixture_factory = LayoutOfferFactory

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache'
    }

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        s.expire_on_commit = False
        layout = s.query(LayoutControl).get(LAYOUT_ID)
        sort_priority = 1000
        for offer in s.query(Offer).all():
            sort_priority -= 1
            layout_offer = LayoutOfferFactory(layout=layout, offer=offer, sort_priority=sort_priority)
        s.commit()
        return s.query(LayoutOffer).order_by(desc(LayoutOffer.sort_priority)).all()

    # @patch("app.layout.app.cache", mock_redis_client)
    @classmethod
    def get_api_response(cls):
        return super(ListTests, cls).get_api_response()

    def test_response_body(self):
        response_json = json.loads(self.response.data)[self.list_key]
        expected_lists = self.fixtures
        # make sure the priority is the same in response (sort by sort_priority desc)
        self.assertEqual(len(expected_lists), len(response_json),
                         'Response count is not match, expected {} vs actual {}, response: {}'.format(
                             len(expected_lists), len(response_json), response_json))
        i = 0
        for entity_json in response_json:
            entity = self.fixtures[i]
            self.assert_entity_detail_expectation(entity, entity_json)
            i += 1

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        session = db.session
        session.add(fixture)
        with app.test_request_context('/'):
            expected = get_expected(fixture.offer)
            self.assertDictEqual(fixture_json, expected)

    def test_response_json_schema(self):
        response_json = json.loads(self.response.data)
        validate_scoop_jsonschema(response_json, load_json_schema('layouts', 'list-layout-offers'))


@istest
class CreateWithPutTest(testcases.UpdateTestCase):

    maxDiff = None
    request_url = "/v1/layouts/{}".format(LAYOUT_ID)
    list_key = 'items'
    fixture_factory = LayoutOfferFactory

    @classmethod
    def setUpClass(cls):
        # super(CreateWithPutTest, cls).setUpClass()
        cls.response = cls.get_api_response()

    @classmethod
    def get_api_request_body(cls):
        s = db.session()
        s.expire_on_commit = False
        offers = s.query(Offer).filter(Offer.id > 1).order_by(Offer.id).all()
        offers_data = []
        with app.test_request_context('/'):
            for offer in offers:
                offers_data.append(get_expected(offer=offer))
            return {
                'items': offers_data,
            }

    def test_response_body(self):
        # the response body should be in the same order as the request body!
        actual = json.loads(self.response.data)
        actual_items = actual.get('items', None)
        self.assertIsNotNone(actual_items, 'actual items is None, {}'.format(actual))
        i = 0
        while i < len(actual):
            self.assertDictEqual(self.request_body.get('items')[i],
                                 actual_items[i])
            i += 1
        # self.assertDictContainsSubset(
        #     expected=self.request_body,
        #     actual=actual)

    def test_put_with_empty_list(self):
        resp = self.client.put(
            self.request_url,
            data=json.dumps({
                'items': None,
            }),
            headers=self.request_headers)
        self.assertEqual(resp.status_code, OK, 'Status Code {} != 200 (OK). {}'.format(resp.status_code, resp.data))

        # test send list without id
        resp = self.client.put(
            self.request_url,
            data=json.dumps({
                'items': [{"abc": 123}],
            }),
            headers=self.request_headers)
        self.assertEqual(resp.status_code, 422,
                         'Status Code {} != 422 (UnprocessableEntity). {}'.format(resp.status_code, resp.data))


@istest
class UpdateTest(testcases.UpdateTestCase):

    maxDiff = None
    request_url = "/v1/layouts/{}".format(LAYOUT_ID)
    list_key = 'items'
    fixture_factory = LayoutOfferFactory

    @classmethod
    def set_up_fixtures(cls):
        # create initial data before update!
        s = db.session()
        s.expire_on_commit = False
        offers = s.query(Offer).order_by(Offer.id).all()
        sort_priority = 1000
        for offer in offers:
            sort_priority -= 1
            layout_intro = LayoutOfferFactory(
                layout_id=LAYOUT_ID, offer=offer, sort_priority=sort_priority)
        s.commit()
        return s.query(LayoutOffer).order_by(desc(LayoutOffer.sort_priority)).all()

    @classmethod
    def get_api_request_body(cls):
        s = db.session()
        s.expire_on_commit = False
        offers = s.query(Offer).filter(Offer.id > 2).order_by(Offer.id).all()
        offers_data = []
        with app.test_request_context('/'):
            for offer in offers:
                offers_data.append(get_expected(offer=offer))
            return {
                'items': offers_data,
            }

    def test_response_body(self):
        # the response body should be in the same order as the request body!
        actual = json.loads(self.response.data)
        actual_items = actual.get('items', None)
        self.assertIsNotNone(actual_items, 'actual items is None, {}'.format(actual))
        i = 0
        while i < len(actual):
            self.assertDictEqual(self.request_body.get('items')[i],
                                 actual_items[i])
            i += 1

        # self.assertDictContainsSubset(
        #     expected=self.request_body,
        #     actual=actual)


_fake = Factory.create()
original_before_request_funcs = None


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()
    init_data()
    global original_before_request_funcs
    original_before_request_funcs = app.before_request_funcs
    app.before_request_funcs = {
        None: [
            set_request_id,
            init_g_current_user,
        ]
    }


def teardown_module():
    app.before_request_funcs = original_before_request_funcs
    db.session().close_all()
    db.drop_all()


def init_g_current_user():
    session = db.session()
    g.current_user = session.query(User).get(12345)
    g.user = UserInfo(username=g.current_user.username,
                      user_id=g.current_user.id, token='abcd', perm=['can_read_write_global_banner', ])
    g.current_client = session.query(Client).get(4)
    session.close()


def init_data():
    s = db.session()
    s.expire_on_commit = False
    generate_static_sql_fixtures(s)
    user = UserFactory(id=12345)
    client = ClientFactory(id=4, platform_id=4)
    layout = LayoutFactory(id=LAYOUT_ID, layout_type=LayoutTypes.editor_pick_offers)
    offer_type = standard_offer_types()['single']
    brand = BrandFactory()
    author = AuthorFactory()
    item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
    item1.authors.append(author)
    offer1 = OfferFactory(id=1, offer_type=offer_type, price_idr=5000, price_usd=50, price_point=50)
    offer1.items.append(item1)
    item2 = ItemFactory(id=2, item_type=ItemTypes.book.value,
                        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
    author2 = AuthorFactory()
    item2.authors.append(author2)
    offer2 = OfferFactory(id=2, offer_type=offer_type, price_idr=3000, price_usd=30, price_point=30)
    offer2.items.append(item2)
    item3 = ItemFactory(id=3, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
    offer3 = OfferFactory(id=3, offer_type=offer_type, price_idr=2000, price_usd=20, price_point=20)
    offer3.items.append(item3)
    item4 = ItemFactory(id=4, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
    offer4 = OfferFactory(id=4, offer_type=offer_type, price_idr=5500, price_usd=55, price_point=55)
    offer4.items.append(item4)
    for k in range(10, 50):
        item = ItemFactory(id=k, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        offer = OfferFactory(id=k, offer_type=offer_type, price_idr=5500+k, price_usd=55+k, price_point=55+k)
        offer.items.append(item)
    s.commit()


def get_expected(offer):
    session = db.session()
    session.add(offer)
    client = session.query(Client).get(4)
    platform_type = PlatformType(client.platform_id)
    discounted = offer.display_discounts(
        idiscounts=offer.discount_id,
        values={},
        platform_id=platform_type,
        offer=offer
    )
    net_idr = float(discounted.get('discount_price_idr', '0')) \
        if float(discounted.get('discount_price_idr', '0')) \
        else float(offer.price_idr)
    net_usd = float(discounted.get('discount_price_usd', '0')) \
        if float(discounted.get('discount_price_usd', '0')) \
        else float(offer.price_usd)
    net_point = float(discounted.get('discount_price_point', '0')) \
        if float(discounted.get('discount_price_point', '0')) \
        else float(offer.price_point)
    item = offer.items.filter(
            Item.is_active == True,
            Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME]
        ).order_by(desc(Item.id)).first()
    if item.item_type == ItemTypes.book.value:
        subtitle = ', '.join([author.name for author in item.authors])
    else:
        subtitle = item.issue_number
    if platform_type != PlatformType.web:
        offer_platform = offer.offer_platforms.filter(
            OfferPlatform.platform_id == int(platform_type)
        ).order_by(desc(OfferPlatform.id)).first()
    else:
        offer_platform = None
    expected = {
        'id': offer.id,
        'title': item.name,
        'slug': item.slug,
        'type': item.item_type,
        'href': item.api_url,
        'subtitle': subtitle,
        'image_url': app.config['MEDIA_BASE_URL']+item.image_highres if item else None,
        'image_normal': app.config['MEDIA_BASE_URL'] + item.image_normal if item else None,
        'thumb_normal': app.config['MEDIA_BASE_URL'] + item.thumb_image_normal if item else None,
        'thumb_highres': app.config['MEDIA_BASE_URL'] + item.thumb_image_highres if item else None,
        'brand': {
            "id": item.brand.id,
            "title": item.brand.name,
            "slug": item.brand.slug,
            "href": item.brand.api_url,
        },
        'price': {
            'id': offer.id,
            'title': offer.long_name,
            'sub_title': offer.name,
            'offer_type_id': offer.offer_type_id,
            'is_free': offer.is_free,
            'platform_id': int(platform_type),
            'tier_id': offer_platform.tier_id if offer_platform else None,
            'tier_code': offer_platform.tier_code if offer_platform else None,
            'idr': {'base': float(offer.price_idr), 'net': net_idr},
            'point': {'base': float(offer.price_point), 'net': net_point},
            'usd': {'base': float(offer.price_usd), 'net': net_usd},
        }
    }
    return expected
