from __future__ import unicode_literals
import json

import os
import random
from datetime import datetime, timedelta
from httplib import OK, CREATED
from tempfile import mkdtemp

from dateutil.parser import parse
from faker import Factory
from marshmallow.utils import isoformat
from nose.tools import istest, nottest
from shutil import rmtree

from slugify import slugify

from app.layouts.banners import BANNER_TYPE_MAGIC_NUMBERS, BANNER_ZONE_MAGIC_OPTIONS, BANNER_PRICE_MAGIC_OPTIONS
from tests.fixtures.sqlalchemy import BannerFactory
from tests.fixtures.utility.images import base_64_encoded_images
from tests import testcases

from app import db, app

from app.utils.datetimes import get_local


@nottest
class CreateUpdateMixins(object):

    maxDiff = None

    @classmethod
    def get_api_request_body(cls, name=None):
        return {
            'banner_type': random.choice(BANNER_TYPE_MAGIC_NUMBERS.keys()),
            'slug': _fake.slug(),
            'payload': _fake.bs(),
            'name': name or _fake.bs(),
            'description': _fake.text(),
            'valid_from': isoformat(get_local()),
            'valid_to': isoformat(get_local() + timedelta(days=30)),
            'is_active': True,
            'sort_priority': _fake.pyint(),
            'clients': [1, 2, 3, ],
            'countries': ['ID', 'US', ],
            'image_normal': random.choice(base_64_encoded_images),
            'image_highres': random.choice(base_64_encoded_images),
            'zone': random.choice([zone.value for zone in BANNER_ZONE_MAGIC_OPTIONS.values()]),
            'term_and_condition': _fake.bs(),
            'image_social_media': random.choice(base_64_encoded_images),
            'product_per_page': _fake.pyint(),
            'show_title': _fake.pybool(),
            'show_price': random.choice([x.value for x in BANNER_PRICE_MAGIC_OPTIONS.values()]),
        }

    def test_response_body(self):
        """ Make sure the returned message meets our expectations.

        .. note::

            We have to do a heavy amount of data massaging because of some
            peculiarities of this API (posting base64 strings and receiving
            back filenames for images)
        """
        response_json = json.loads(self.response.data)
        request_json = self.request_body.copy()

        if 'developer_message' in response_json:
            self.fail(msg=response_json['developer_message'])

        actual_slug = response_json.get('slug', None)
        self.assertIsNotNone(actual_slug)
        if request_json.get('banner_type') == 6 and not actual_slug:
            initial_slug = slugify(request_json.get('name'))
            self.assertIn(initial_slug, actual_slug)

        try:
            for ignore_resp_field in ['created', 'modified', 'media_base_url',
                                      'id', 'image_normal', 'slug',
                                      'image_social_media', 'image_highres', 'image_iphone']:
                response_json.pop(ignore_resp_field, None)
        except KeyError as e:
            self.fail(msg="Missing response field: {0.message}".format(e))

        for ignore_req_field in ['image_normal', 'image_highres',
                                 'image_social_media', 'image_iphone', 'slug']:
            request_json.pop(ignore_req_field, None)

        # convert request and response fields to mutually-comparable types
        response_json['valid_to'] = parse(response_json['valid_to'])
        response_json['valid_from'] = parse(response_json['valid_from'])
        request_json['valid_to'] = parse(request_json['valid_to'])
        request_json['valid_from'] = parse(request_json['valid_from'])

        self.assertDictEqual(response_json, request_json)

    def test_images_exist(self):
        response_json = json.loads(self.response.data)

        for image_field in ['image_normal', 'image_highres',
                            'image_social_media']:
            try:
                file_full_path = os.path.join(os.path.join(app.static_folder, 'banner'),
                                              response_json[image_field])

                self.assertTrue(
                    os.path.exists(file_full_path),
                    msg="Image file exists for field {}".format(image_field))

            except KeyError:
                self.fail("{} missing from response".format(image_field))


@istest
class CreateTest(CreateUpdateMixins, testcases.CreateTestCase):

    request_url = '/v1/banners'
    fixture_factory = BannerFactory

    def test_response_code(self):
        self.assertEqual(CREATED, self.response.status_code,
                         'Response status code is not CREATED, response: {}'.format(self.response.data))


@istest
class CreatePromoSlugTest(CreateUpdateMixins, testcases.CreateTestCase):

    request_url = '/v1/banners'
    fixture_factory = BannerFactory

    @classmethod
    def get_api_request_body(cls):
        # set to PROMO (6), and we will check the slug should created correctly
        s = db.session()
        name = _fake.name()
        existing_banner_slug = BannerFactory(name=name + ' abc', slug=slugify(name))
        s.commit()
        request_body = super(CreatePromoSlugTest, cls).get_api_request_body(name=name)
        request_body['banner_type'] = 6
        request_body['slug'] = None
        return request_body

    def test_response_code(self):
        self.assertEqual(CREATED, self.response.status_code,
                         'Response status code is not CREATED, response: {}'.format(self.response.data))


@istest
class UpdateTest(CreateUpdateMixins, testcases.UpdateTestCase):

    request_url = '/v1/banners/{0.id}'
    fixture_factory = BannerFactory

    # @classmethod
    # def get_api_request_body(cls):
    #     req_body = super(UpdateTest, cls).get_api_request_body()
    #     req_body.pop('image_normal')
    #     return req_body


@istest
class GetDetailsTest(testcases.DetailTestCase):

    maxDiff = None

    request_url = '/v1/banners/{0.id}'
    fixture_factory = BannerFactory

    def test_response_body(self):

        self.assertDictEqual(
            json.loads(self.response.data),
            {
                'id': self.fixture.id,
                'banner_type': {v: k for k, v in BANNER_TYPE_MAGIC_NUMBERS.items()}[self.fixture.banner_type],
                'payload': self.fixture.payload,
                'name': self.fixture.name,
                'description': self.fixture.description,
                'valid_from': isoformat(self.fixture.valid_from),
                'valid_to': isoformat(self.fixture.valid_to),
                'created': isoformat(self.fixture.created),
                'modified': isoformat(self.fixture.modified),
                'is_active': self.fixture.is_active,
                'sort_priority': self.fixture.sort_priority,
                'clients': self.fixture.clients,
                'countries': self.fixture.countries,
                'image_normal': self.fixture.image_normal,
                'image_highres': self.fixture.image_highres,
                'image_iphone': self.fixture.image_iphone,
                'media_base_url': app.config['BANNER_BASE_URL'],
                'zone': self.fixture.zone.value,
                'term_and_condition': self.fixture.term_and_condition,
                'image_social_media': self.fixture.image_social_media,
                'slug': self.fixture.slug,
                'product_per_page': self.fixture.product_per_page,
                'show_title': self.fixture.show_title,
                'show_price': self.fixture.show_price.value,
            }
        )


@istest
class ListTest(testcases.ListTestCase):

    maxDiff = None
    request_url = '/v1/banners'
    list_key = 'banners'
    fixture_factory = BannerFactory

    def test_response_code(self):
        self.assertEqual(OK, self.response.status_code,
                         'Response status code is not OK, response: {}'.format(self.response.data))

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        self.assertDictEqual(
            fixture_json,
            {
                'id': fixture.id,
                'banner_type': {v: k for k, v in BANNER_TYPE_MAGIC_NUMBERS.items()}[fixture.banner_type],
                'payload': fixture.payload,
                'name': fixture.name,
                'description': fixture.description,
                'valid_from': isoformat(fixture.valid_from),
                'valid_to': isoformat(fixture.valid_to),
                'created': isoformat(fixture.created),
                'modified': isoformat(fixture.modified),
                'is_active': fixture.is_active,
                'sort_priority': fixture.sort_priority,
                'clients': fixture.clients,
                'countries': fixture.countries,
                'image_normal': fixture.image_normal,
                'image_highres': fixture.image_highres,
                'image_iphone': fixture.image_iphone,
                'media_base_url': app.config['BANNER_BASE_URL'],
                'zone': fixture.zone.value,
                'term_and_condition': fixture.term_and_condition,
                'image_social_media': fixture.image_social_media,
                'slug': fixture.slug,
                'product_per_page': fixture.product_per_page,
                'show_title': fixture.show_title,
                'show_price': fixture.show_price.value,
            }
        )


_fake = Factory.create()

TEMP_BANNER_DIR = None
ORIGINAL_SETTING = None


def setup_module():
    db.drop_all()
    db.create_all()
    global TEMP_BANNER_DIR, ORIGINAL_SETTING
    TEMP_BANNER_DIR = mkdtemp()
    ORIGINAL_SETTING = app.config.pop('BANNER_UPLOAD_DIR', None)
    app.config['BANNER_UPLOAD_DIR'] = TEMP_BANNER_DIR


def teardown_module():
    if os.path.exists(TEMP_BANNER_DIR):
        rmtree(TEMP_BANNER_DIR)
    if ORIGINAL_SETTING:
        app.config['BANNER_UPLOAD_DIR'] = ORIGINAL_SETTING

    db.session().close_all()
    db.drop_all()
