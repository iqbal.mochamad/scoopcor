from __future__ import unicode_literals, absolute_import

import json
import os
from httplib import UNAUTHORIZED
from unittest import TestCase

from faker import Factory
from flask import g
from flask import url_for
from nose.tools import istest
from sqlalchemy import desc

from app import db, set_request_id, app
from app.auth.helpers import authenticate_user_on_session, jwt_encode
from app.auth.models import UserInfo, ANONYMOUS_USER
from app.constants import RoleType
from app.layouts.banners import BannerActions
from app.layouts.layout_type import LayoutTypes
from app.layouts.models import LayoutControl
from app.offers.choices.offer_type import SINGLE
from app.offers.models import OfferType
from app.users.users import User
from tests import testcases
from tests.fixtures.helpers import create_user_and_token, generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import BannerFactory
from tests.fixtures.sqlalchemy.layouts import LayoutFactory, LayoutOfferFactory, LayoutBannerFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory


USER_ID = 12345


def get_jwt_valid_token():
    with app.test_request_context('/'):
        return jwt_encode(payload={"user_id": USER_ID})


@istest
class ListLayoutControlTests(testcases.ListTestCase):
    maxDiff = None
    request_url = "/v1/layouts"
    list_key = 'layouts'
    fixture_factory = LayoutFactory

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'Authorization': 'JWT {}'.format(get_jwt_valid_token())
    }

    @classmethod
    def setUpClass(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                authenticate_user_on_session,
            ]
        }
        super(ListLayoutControlTests, cls).setUpClass()

    @classmethod
    def set_up_fixtures(cls):
        session = db.session()
        session.expire_on_commit = False
        cls.init_layout_data(session)
        layouts = session.query(LayoutControl).order_by(desc(LayoutControl.sort_priority)).all()
        return layouts

    @classmethod
    def init_layout_data(cls, session):
        layout = LayoutFactory(id=1000, layout_type=LayoutTypes.latest_book, sort_priority=20)
        layout2 = LayoutFactory(id=1002, layout_type=LayoutTypes.popular_all, sort_priority=10)
        session.commit()

    def test_response_body(self):
        response_json = json.loads(self.response.data)
        i = 0
        # make sure the priority is the same in response (sort by sort_priority desc)
        for entity_json in response_json[self.list_key]:
            entity = self.fixtures[i]
            self.assert_entity_detail_expectation(entity, entity_json)
            i += 1

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        with app.test_request_context('/'):
            expected = {
                'id': fixture.id,
                'title': fixture.name,
                'layout_type': fixture.layout_type.value,
                'href': url_for('layouts.details', db_id=fixture.id, _external=True)
            }
        self.assertDictEqual(fixture_json, expected)


@istest
class ListWithCachedDataTests(ListLayoutControlTests):
    maxDiff = None
    request_url = "/v1/layouts"
    list_key = 'layouts'
    fixture_factory = LayoutFactory

    @classmethod
    def get_api_response(cls):
        # hit request to cached data
        request_headers = {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'Cache-Control': 'no-cache',
            'Authorization': 'JWT {}'.format(get_jwt_valid_token())
        }
        cls.client.get(cls.request_url, headers=request_headers)
        # hit request again with cached data
        request_headers = {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'Authorization': 'JWT {}'.format(get_jwt_valid_token())
        }
        return cls.client.get(cls.request_url, headers=request_headers)


@istest
class ListWithAnonymousUserTests(TestCase):

    maxDiff = None
    request_url = "/v1/layouts"
    client = app.test_client(use_cookies=False)

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'Authorization': 'JWT wrongtokeninvalidtoken'
    }

    @classmethod
    def setUpClass(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                authenticate_user_on_session,
            ]
        }
        super(ListWithAnonymousUserTests, cls).setUpClass()
        cls.response = cls.client.get(cls.request_url, headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(UNAUTHORIZED, self.response.status_code)


@istest
class CreateWithPutTest(testcases.UpdateTestCase):

    request_url = "/v1/layouts"
    fixture_factory = LayoutFactory
    list_key = 'layouts'

    @classmethod
    def setUpClass(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }
        session = db.session()
        session.execute('DELETE FROM layouts;')
        session.commit()
        cls.response = cls.get_api_response()

    @classmethod
    def get_api_request_body(cls):
        items = []
        items.append({
                'title': _fake.name(),
                'layout_type': LayoutTypes.editor_pick_banners.value,
            })
        items.append({
                'title': _fake.name(),
                'layout_type': LayoutTypes.popular_book.value,
            })
        items.append({
                'title': _fake.name(),
                'layout_type': LayoutTypes.popular_all.value,
            })
        items.append({
                'title': _fake.name(),
                'layout_type': LayoutTypes.latest_all.value,
            })
        items.append({
                'title': _fake.name(),
                'layout_type': LayoutTypes.editor_pick_offers.value,
            })
        return {
                'layouts': items,
            }

    def test_response_body(self):
        response_json = json.loads(self.response.data)[self.list_key]
        expected_lists = self.request_body.get('layouts', None)
        i = 0
        # make sure the priority is the same in response (sort by sort_priority desc)
        self.assertEqual(len(expected_lists), len(response_json),
                         'Response count is not match, expected {} vs actual {}, response: {}'.format(
                             len(expected_lists), len(response_json), response_json))
        for entity_json in response_json:
            entity = expected_lists[i]
            self.assert_entity_detail_expectation(entity, entity_json)
            i += 1

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        with app.test_request_context('/'):
            layout_type = LayoutTypes(fixture.get('layout_type'))
            expected = {
                'id': fixture_json.get('id', None),
                'title': fixture.get('title'),
                'layout_type': fixture.get('layout_type'),
                'href': url_for('layouts.details', db_id=fixture_json.get('id', None), _external=True)
            }
        self.assertDictEqual(fixture_json, expected)


@istest
class UpdateTest(testcases.UpdateTestCase):

    request_url = "/v1/layouts"
    fixture_factory = LayoutFactory
    list_key = 'layouts'

    @classmethod
    def setUpClass(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }
        session = db.session()
        session.execute('DELETE FROM layouts;')
        # create pre existing data, to test the data cleanup successfully
        layout = LayoutFactory(id=1000, layout_type=LayoutTypes.latest_book, sort_priority=20)
        layout2 = LayoutFactory(id=1002, layout_type=LayoutTypes.popular_all, sort_priority=10)
        offer_type = session.query(OfferType).get(SINGLE)
        offer1 = OfferFactory(id=1, offer_type=offer_type, price_idr=5000, price_usd=50, price_point=50)
        layout3 = LayoutFactory(id=1003, layout_type=LayoutTypes.editor_pick_offers, sort_priority=30)
        layout_offer = LayoutOfferFactory(layout=layout3, offer=offer1, sort_priority=10)
        banner1 = BannerFactory(id=3, banner_type=BannerActions.static, payload=None)
        layout4 = LayoutFactory(id=1004, layout_type=LayoutTypes.editor_pick_banners, sort_priority=40)
        layout_banner = LayoutBannerFactory(layout=layout4, banner_id=3, sort_priority=20)
        session.commit()
        cls.response = cls.get_api_response()

    @classmethod
    def get_api_request_body(cls):
        items = []
        items.append({
                'id': 1002,
                'title': _fake.name(),
                'layout_type': LayoutTypes.editor_pick_banners.value,
            })
        items.append({
                'id': 0,
                'title': _fake.name(),
                'layout_type': LayoutTypes.popular_book.value,
            })
        items.append({
                'title': _fake.name(),
                'layout_type': LayoutTypes.popular_all.value,
            })
        items.append({
                'title': _fake.name(),
                'layout_type': LayoutTypes.latest_all.value,
            })
        items.append({
                'title': _fake.name(),
                'layout_type': LayoutTypes.editor_pick_offers.value,
            })
        return {
                'layouts': items,
            }

    def test_response_body(self):
        response_json = json.loads(self.response.data)[self.list_key]
        expected_lists = self.request_body.get('layouts', None)
        # make sure the priority is the same in response (sort by sort_priority desc)
        self.assertEqual(len(expected_lists), len(response_json),
                         'Response count is not match, expected {} vs actual {}, response: {}'.format(
                             len(expected_lists), len(response_json), response_json))
        session = db.session()
        i = 0
        for entity_json in response_json:
            entity = expected_lists[i]
            self.assert_entity_detail_expectation(entity, entity_json, session)
            i += 1
        session.close_all()

    def assert_entity_detail_expectation(self, expect_entity, fixture_json, session):
        with app.test_request_context('/'):
            layout = session.query(LayoutControl).get(fixture_json.get('id', None))
            expected = {
                'id': layout.id,
                'title': expect_entity.get('title'),
                'layout_type': layout.layout_type.value,
                'href': url_for('layouts.details', db_id=fixture_json.get('id', None), _external=True)
            }
        self.assertDictEqual(fixture_json, expected)
        self.assertEqual(layout.name, expect_entity.get('title'))
        # , "actual {} vs expected {}".format(fixture_json, expected))


_fake = Factory.create()
original_before_request_funcs = None
ENV_TESTING = os.environ.get('TESTING', None)


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()
    global original_before_request_funcs
    original_before_request_funcs = app.before_request_funcs
    # remove testing from environment so we can test the method
    os.environ.pop('TESTING', None)
    session = db.session()
    generate_static_sql_fixtures(session)
    create_user_and_token(
        session, RoleType.super_admin,
        id=USER_ID
    )

    session.commit()


def teardown_module():
    app.before_request_funcs = original_before_request_funcs
    # restore the testing value in environment
    if ENV_TESTING:
        os.environ['TESTING'] = ENV_TESTING

    db.session().close_all()
    db.drop_all()


def init_g_current_user():
    g.user = UserInfo(username='dfdsafsdf', user_id=12345, token='abcd', perm=['can_read_write_global_all',])
    session = db.session()
    g.current_user = session.query(User).get(12345)
    session.close()


def init_g_anonymous_user():
    g.user = ANONYMOUS_USER
    g.current_user = None

