from flask import url_for, current_app, g
from flask_appsfoundry.viewmodels import SaViewmodelBase
from sqlalchemy import desc

from app.items.choices import ItemTypes, STATUS_TYPES, STATUS_READY_FOR_CONSUME
from app.items.models import Item
from app.layouts.helpers import get_href_for_banners, banner_image_url, item_image_url
from app.master.choices import PlatformType
from app.offers.choices.offer_type import SINGLE
from app.offers.choices.status_type import READY_FOR_SALE
from app.offers.helpers import get_discounted_price
from app.offers.models import Offer, OfferPlatform
from app.utils.viewmodels import HrefAttributeWithId

from app.items.previews import preview_s3_covers

class LayoutListViewModel(SaViewmodelBase):

    @property
    def layout_type(self):
        return self._model_instance.layout_type.value

    @property
    def href(self):
        return url_for('layouts.details', db_id=self._model_instance.id, _external=True)


class LayoutItemsViewModel(SaViewmodelBase):

    def __init__(self, model_instance):
        super(LayoutItemsViewModel, self).__init__(model_instance)
        if isinstance(self._model_instance, Item):
            self.item = self._model_instance
        else:
            self.item = self._model_instance.item
        self.slug = self.item.slug if self.item else None
        self.brand = {
                "id": self.item.brand.id,
                "title": self.item.brand.name,
                "slug": self.item.brand.slug,
                "href": self.item.brand.api_url,
            } if self.item else None

    @property
    def title(self):
        return self.item.name

    @property
    def type(self):
        return self.item.item_type

    @property
    def subtitle(self):
        if self.item.item_type == ItemTypes.book.value:
            return ', '.join([author.name for author in self.item.authors])
        else:
            return self.item.issue_number

    @property
    def href(self):
        return self.item.api_url

    @property
    def image_url(self):
        # return item_image_url(self.item.image_highres) if hasattr(self, 'item') else None
        return preview_s3_covers(self.item, 'image_highres', True) if hasattr(self, 'item') else None

    @property
    def image_normal(self):
        #return item_image_url(self.item.image_normal) if hasattr(self, 'item') else None
        return preview_s3_covers(self.item, 'image_normal', True) if hasattr(self, 'item') else None

    @property
    def thumb_normal(self):
        #return item_image_url(self.item.thumb_image_normal) if hasattr(self, 'item') else None
        return preview_s3_covers(self.item, 'thumb_image_normal', True) if hasattr(self, 'item') else None

    @property
    def thumb_highres(self):
        # return item_image_url(self.item.thumb_image_highres) if hasattr(self, 'item') else None
        return preview_s3_covers(self.item, 'thumb_image_highres', True) if hasattr(self, 'item') else None

    @property
    def price(self):
        return get_single_offer_prices(self.item, None)


def get_single_offer_prices(item, offer=None):
    offer = offer or item.core_offers.filter(
        Offer.offer_type_id == SINGLE,
        Offer.offer_status == READY_FOR_SALE,
        Offer.is_active == True).first()
    if not offer:
        return None
    else:
        net_idr, net_usd, net_point, platform_type = get_discounted_price(offer)
        if platform_type != PlatformType.web:
            offer_platform = offer.offer_platforms.filter(
                OfferPlatform.platform_id == int(platform_type)
            ).order_by(desc(OfferPlatform.id)).first()
        else:
            offer_platform = None
        return {
            'id': offer.id,
            'title': offer.long_name,
            'sub_title': offer.name,
            'offer_type_id': offer.offer_type_id,
            'is_free': offer.is_free,
            'platform_id': int(platform_type),
            'tier_id': offer_platform.tier_id if offer_platform else None,
            'tier_code': offer_platform.tier_code if offer_platform else None,
            'idr': {'base': float(offer.price_idr), 'net': net_idr},
            'point': {'base': float(offer.price_point), 'net': net_point},
            'usd': {'base': float(offer.price_usd), 'net': net_usd},
        }


class LayoutOffersViewModel(SaViewmodelBase):

    def __init__(self, model_instance):
        super(LayoutOffersViewModel, self).__init__(model_instance)
        self.offer = self._model_instance.offer
        self.item = self.offer.items.filter(
            Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            Item.is_active == True
        ).order_by(desc(Item.id)).first()
        self.slug = self.item.slug if self.item else None

    @property
    def id(self):
        return self.offer.id

    @property
    def title(self):
        return self.item.name if self.item else self.offer.long_name

    @property
    def type(self):
        return self.item.item_type if self.item else None

    @property
    def subtitle(self):
        if self.item and self.item.item_type == ItemTypes.book.value:
            return ', '.join([author.name for author in self.item.authors])
        else:
            return self.item.issue_number if self.item else None

    @property
    def href(self):
        return self.item.api_url if self.item else None

    @property
    def image_url(self):
        #return item_image_url(self.item.image_highres) if hasattr(self, 'item') else None
        return preview_s3_covers(self.item, 'image_highres', True) if hasattr(self, 'item') else None

    @property
    def image_normal(self):
        # return item_image_url(self.item.image_normal) if hasattr(self, 'item') else None
        return preview_s3_covers(self.item, 'image_normal', True) if hasattr(self, 'item') else None

    @property
    def thumb_normal(self):
        # return item_image_url(self.item.thumb_image_normal) if hasattr(self, 'item') else None
        return preview_s3_covers(self.item, 'thumb_image_normal', True) if hasattr(self, 'item') else None

    @property
    def thumb_highres(self):
        # return item_image_url(self.item.thumb_image_highres) if hasattr(self, 'item') else None
        return preview_s3_covers(self.item, 'thumb_image_highres', True) if hasattr(self, 'item') else None

    @property
    def price(self):
        return get_single_offer_prices(item=None, offer=self.offer)
        # return PriceAttribute(
        #     idr=PriceDetailAttributes(base=self.offer.price_idr, net=self.net_idr),
        #     usd=PriceDetailAttributes(base=self.offer.price_usd, net=self.net_usd),
        #     point=PriceDetailAttributes(base=self.offer.price_point, net=self.net_point),
        # )

    @property
    def brand(self):
        if self.item:
            return {
                "id": self.item.brand.id,
                "title": self.item.brand.name,
                "slug": self.item.brand.slug,
                "href": self.item.brand.api_url,
            }
        else:
            return None


class PriceAttribute(object):
    def __init__(self, idr, usd, point):
        self.idr = idr
        self.usd = usd
        self.point = point


class PriceDetailAttributes(object):
    def __init__(self, base, net):
        self.base = base
        self.net = net


class LayoutBannersViewModel(SaViewmodelBase):

    def __init__(self, model_instance):
        super(LayoutBannersViewModel, self).__init__(model_instance)
        self.banner = self._model_instance.banner
        self.slug = self.banner.slug
        self.brand = None

    @property
    def id(self):
        return self.banner.id

    @property
    def title(self):
        return self.banner.name

    @property
    def type(self):
        return self.banner.banner_type.value

    @property
    def subtitle(self):
        return None

    @property
    def href(self):
        return get_href_for_banners(banner_type=self.banner.banner_type, payload=self.banner.payload)

    @property
    def image_url(self):
        return banner_image_url(self.banner.image_highres) if hasattr(self, 'banner') else None

    @property
    def image_normal(self):
        return banner_image_url(self.banner.image_normal) if hasattr(self, 'banner') else None

    @property
    def thumb_normal(self):
        return banner_image_url(self.banner.image_social_media) if hasattr(self, 'banner') else None

    @property
    def thumb_highres(self):
        return banner_image_url(self.banner.image_iphone) if hasattr(self, 'banner') else None

    @property
    def price(self):
        return None


class SimpleImplicitAttributes(HrefAttributeWithId):
    def __init__(self, title, href, id, subtitle, image_url, base_price_idr, net_price_idr):
        super(SimpleImplicitAttributes, self).__init__(title, href, id)
        self.subtitle = subtitle
        self.image_url = image_url
        self.base_price_idr = base_price_idr
        self.net_price_idr = net_price_idr


class LayoutIntroViewModel(SaViewmodelBase):

    def __init__(self, model_instance):
        super(LayoutIntroViewModel, self).__init__(model_instance)
        if self._model_instance.brand.default_item_type == ItemTypes.book.value:
            self.item = self._model_instance.brand.items.first()
        else:
            # for newspaper and magazine, get the latest edition!
            self.item = self._model_instance.brand.items.filter(
                Item.is_active == True,
                Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME]
            ).order_by(desc(Item.id)).first()

    @property
    def title(self):
        return self._model_instance.brand.name

    @property
    def id(self):
        return self._model_instance.brand.id

    @property
    def href(self):
        if self.item:
            return '{}{}'.format(
                current_app.config['MEDIA_BASE_URL'],
                self.item.image_highres)
        else:
            return None
