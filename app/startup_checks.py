"""
Startup Checks
==============

Verifies our application configuration appears valid.

If there are any problems, Scoopcor should be terminated with an exception.
"""
import pycountry

from app.locales.choices import COUNTRIES_ENABLED, LANGUAGES_ENABLED


class ScoopConfigError(Exception):
    """ An exception that indicates our application configuration is invalid.
    """
    pass


def startup_checks(app):
    """ A minimal validation framework to try to make sure our app looks good.

    :raises: :py:class:`ScoopConfigError`
    """
    assert_enabled_languages_are_iso639_2t(LANGUAGES_ENABLED)
    assert_enabled_countries_are_iso3166_alpha2(COUNTRIES_ENABLED)


def assert_enabled_languages_are_iso639_2t(language_codes):
    """ Makes sure all language codes are in the expected format

    :param `list` language_codes: List of ISO639-2T strings
    :raises: `ScoopConfigError` If invalid language codes detected.
    """
    for lc in language_codes:
        try:
            pycountry.languages.get(iso639_2T_code=lc)
        except KeyError:
            raise ScoopConfigError("Invalid setting for LANGUAGES_ENABLED.  "
                                   "'{}' is not a valid ISO639-2/T "
                                   "code".format(lc))


def assert_enabled_countries_are_iso3166_alpha2(country_codes):
    """ Makes sure all country codes are in the expected format

    :param `list` country_codes: List of ISO3166-alpha2 strings
    :raises: `ScoopConfigError` If invalid country codes detected.
    """
    for cc in country_codes:
        try:
            # pycountry stores country codes in upper case.. cause reasons?
            pycountry.countries.get(alpha2=cc.upper())
        except KeyError:
            raise ScoopConfigError("Invalid setting for COUNTRIES_ENABLED.  "
                                   "'{}' is not a valid ISO3166-alpha2 "
                                   "code".format(cc))


def assert_no_unloaded_blueprints():
    # get the total number of blueprints
    # recursively search scoop directory and find any files named 'blueprints'
    # raise an exception if the two counts don't match
    pass
