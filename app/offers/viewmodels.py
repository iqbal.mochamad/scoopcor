from flask_appsfoundry.viewmodels import SaViewmodelBase

from app import app
from app.items.previews import PreviewUrlBuilder


class OfferBuffetViewmodel(SaViewmodelBase):

    @property
    def offer(self):
        return OfferViewmodel(self._model_instance.offer)


class OfferViewmodel(SaViewmodelBase):

    @property
    def offer_brand_ids(self):
        return [brand.id for brand in self._model_instance.brands]

    @property
    def offer_item_ids(self):
        return [item.id for item in self._model_instance.items]

    @property
    def offer_platforms_objects(self):
        return [op for op in self._model_instance.offer_platforms]


class OfferItemPreviewsViewmodel(SaViewmodelBase):

    @property
    def item_name(self):
        return self._model_instance.items[0].name

    @property
    def previews(self):
        preview_url_builder = PreviewUrlBuilder(app.config['BASE_URL'])

        previews = preview_url_builder.make_urls(self._model_instance.items[0])

        return previews
