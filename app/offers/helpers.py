import decimal, httplib, json, os

from flask import g, Response, current_app
from flask_appsfoundry.parsers.converters import ImageFieldParser
from sqlalchemy.sql import desc

from app import app
from app.constants import *
from app.discounts.models import Discount
from app.helpers import bad_request_message, internal_server_message, purge_redis_cache, err_response
from app.items.models import Item
from app.master.choices import PlatformType
from app.master.models import Brand
from app.offers.models import Offer, OfferType, OfferPlatform
from app.offers.models import OfferSingle, OfferBundle, OfferSubscription
from app.services.gramedia.gramedia_export import build_offer_from_gramedia_digital, insert_offer_to_gm_db, \
    update_product_price_to_gm_db
from app.services.gramedia.models import get_gramedia_session
from app.tiers.models import TierAndroid, TierIos, TierWp

from app.uploads.aws.helpers import aws_upload_file

def get_discounted_price(offer):
    platform_type = PlatformType.web
    try:
        if g.current_client and g.current_client.platform_id:
            platform_type = PlatformType(g.current_client.platform_id)
    except Exception as e:
        platform_type = PlatformType.web
    discounted = offer.display_discounts(
        idiscounts=offer.discount_id,
        values={},
        platform_id=platform_type,
        offer=offer
    )
    net_idr = float(discounted.get('discount_price_idr', '0')) \
        if float(discounted.get('discount_price_idr', '0')) \
        else float(offer.price_idr)
    net_usd = float(discounted.get('discount_price_usd', '0')) \
        if float(discounted.get('discount_price_usd', '0')) \
        else float(offer.price_usd)
    net_point = float(discounted.get('discount_price_point', '0')) \
        if float(discounted.get('discount_price_point', '0')) \
        else float(offer.price_point)
    return net_idr, net_usd, net_point, platform_type


class ItemConstruct(object):

    def __init__(self, args=None, method=None, old_data=None):
        self.args = args
        self.method = method
        self.old_data = old_data

        # specified master data dict
        self.offer_master = ['name', 'offer_status', 'sort_priority', 'is_active',
                             'offer_type_id', 'exclusive_clients', 'is_free', 'offer_code',
                             'item_code', 'items', 'brands', 'price_usd', 'price_idr',
                             'price_point', 'vendor_price_usd', 'vendor_price_idr', 'vendor_price_point',
                             'image_normal', 'image_highres', 'long_name']

        self.offer_master_dict = {}

        # specified master single_bundle
        self.offer_single_bundle = ['description']
        self.offer_single_bundle_dict = {}

        # specified master subscriptions
        self.offer_subscription = ['description', 'quantity',
                                   'quantity_unit', 'allow_backward', 'backward_quantity',
                                   'backward_quantity_unit']
        self.offer_subscription_dict = {}

        self.brands = args.get('brands', None)
        self.items = args.get('items', None)
        self.offer_type_id = args.get('offer_type_id', None)

        self.image_normal = args.get('image_normal', None)
        self.image_highres = args.get('image_highres', None)

        self.price_usd = args.get('price_usd', None)
        self.price_idr = args.get('price_idr', None)
        self.price_point = args.get('price_point', None)
        self.offer_put_calc = False
        self.offer_put_platform_calc = False
        self.offer_put_usd = 0

        self.offer_tiers = args.get('platforms_offers', None)
        self.is_free = args.get('is_free', None)

        self.ios_tiers = []
        self.ios_tiers_cur = []

        self.android_tiers = []
        self.android_tiers_cur = []

        self.wp_tiers = []
        self.wp_tiers_cur = []

        self.platform_input = []

        self.data_master = ''
        self.discount_master = ''

        self.is_free = args.get('is_free', False)
        self.redis_key = 'items-details-'

    def bad_request(self, bad=None, msg=None):
        return err_response(status=httplib.BAD_REQUEST,
                            error_code=CLIENT_400101,
                            developer_message="%s : %s" % (msg, bad),
                            user_message="Bad Request for %s" % bad)

    def check_offer_type(self):
        """
            CHECKING OFFER TYPE EXIST/NOT-EXIST
        """
        try:
            offer_type = OfferType.query.filter_by(id=self.offer_type_id).first()
            if not offer_type:
                return "FAILED", bad_request_message(modul="offer 1", method=self.method,
                                                     param="offer_type_id", not_found=self.offer_type_id,
                                                     req=self.args)
            return 'OK', 'OK'
        except Exception, e:
            return "FAILED", internal_server_message(modul="offer 1",
                                                     method=self.method, error=e, req=self.args)

    def check_brands(self):
        """
            CHECK BRAND EXIST/NOT EXIST
        """
        try:
            brand_data = []
            for ibrand in self.brands:
                brand = Brand.query.filter_by(id=ibrand).first()
                if not brand:
                    return "FAILED", bad_request_message(modul="offer 2", method=self.method,
                                                         param="brand", not_found=ibrand,
                                                         req=self.args)
                brand_data.append(brand)
            self.brands = brand_data
            return 'OK', 'OK'
        except Exception, e:
            return "FAILED", internal_server_message(modul="offer 2",
                                                     method=self.method, error=e, req=self.args)

    def check_items(self):
        """
            CHECK ITEMS EXIST/NOT EXIST
        """
        try:
            item_data = []
            for item in self.items:
                items = Item.query.filter_by(id=item).first()
                if not items:
                    return "FAILED", bad_request_message(modul="offer 3", method=self.method,
                                                         param="item", not_found=item,
                                                         req=self.args)
                item_data.append(items)

            self.items = item_data
            return "OK", "OK"
        except Exception, e:
            return "FAILED", internal_server_message(modul="offer 3",
                                                     method=self.method, error=e, req=self.args)

    def check_offer_item(self):
        """
            Checking item offer, can't 1 item 2 offer same type
        """
        try:
            for item in self.items:
                offer = Offer.query.join(Item.core_offers).filter(Item.id.in_([item.id])).all()
                if offer:
                    for ioffer in offer:
                        if int(ioffer.offer_type_id) == int(self.offer_type_id):
                            return 'FAILED', self.bad_request(item.name,
                                                              "Conflict, Item already have same offer types")
            return 'OK', 'OK'
        except Exception, e:
            return "FAILED", internal_server_message(modul="offer 4",
                                                     method=self.method, error=e, req=self.args)

    def check_offer_tiers(self):
        """
            CHECK OFFER TIERS INPUT
            1 PLATFORM - 1 OFFER - 1 CURRENCY TIERS

                        EXAMPLE VALID:          |       EXAMPLE INVALID
            ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                PLATFORM    : IOS               |   PLATFORM    : IOS
                OFFER       : MRA-BOOK-STORE    |   OFFER       : MRA-BOOK-STORE
                TIERS       : USD 0.99          |   TIERS       : USD 0.99
                              IDR 9000          |                 USD 5.99
            ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        """
        try:
            for tier in self.offer_tiers:
                # check platform
                platform = tier.get('platform_id', 0)
                tier_id = tier.get('tier_id', 0)

                # 1 platform 1 offer, can't double
                if platform in self.platform_input:
                    return 'FAILED', self.bad_request(platform, "Conflict, can't set tier for same two platform")
                else:
                    self.platform_input.append(platform)

                # Checking 1 platform 1 tier IOS 1 currency ONLY!!! YEAHHHH
                if platform == 1:  # IOS
                    if tier_id in self.ios_tiers:
                        return 'FAILED', self.bad_request(tier_id, 'IOS - Conflict tier_id')
                    else:
                        self.ios_tiers.append(tier_id)

                        data_tie = TierIos.query.filter_by(id=tier_id).first()
                        if data_tie:
                            if data_tie.currency_id in self.ios_tiers_cur:
                                return 'FAILED', self.bad_request(tier_id, 'IOS - Conflict tier_id CURRENCY')
                            else:
                                self.ios_tiers_cur.append(data_tie.currency_id)
                        else:
                            return 'FAILED', self.bad_request(tier_id, 'IOS - Tier id not Founds')

                # Checking 1 platform 1 tier ANDROID 1 currency ONLY!!! YEAHHHH
                if platform == 2:  # Android
                    if tier_id in self.android_tiers:
                        return 'FAILED', self.bad_request(tier_id, 'ANDROID - Conflict tier_id')
                    else:
                        self.android_tiers.append(tier_id)

                        data_tie = TierAndroid.query.filter_by(id=tier_id).first()
                        if data_tie:
                            if data_tie.currency_id in self.android_tiers_cur:
                                return 'FAILED', self.bad_request(tier_id, 'ANDROID - Conflict tier_id CURRENCY')
                            else:
                                self.android_tiers_cur.append(data_tie.currency_id)
                        else:
                            return 'FAILED', self.bad_request(tier_id, 'ANDROID - Tier id not Founds')

                # Checking 1 platform 1 tier WP 1 currency ONLY!!! YEAHHHH
                if platform == 3:  # WP
                    if tier_id in self.wp_tiers:
                        return 'FAILED', self.bad_request(tier_id, 'WP - Conflict tier_id')
                    else:
                        self.wp_tiers.append(tier_id)

                        data_tie = TierWp.query.filter_by(id=tier_id).first()
                        if data_tie:
                            if data_tie.currency_id in self.wp_tiers_cur:
                                return 'FAILED', self.bad_request(tier_id, 'WP - Conflict tier_id CURRENCY')
                            else:
                                self.wp_tiers_cur.append(data_tie.currency_id)
                        else:
                            return 'FAILED', self.bad_request(tier_id, 'WP - Tier id not Founds')
            return 'OK', 'OK'

        except Exception, e:
            return "FAILED", internal_server_message(modul="offer 5",
                                                     method=self.method, error=e, req=self.args)

    def check_subscription_data(self):
        """
            FOR SUBSCRIPTIONS OFFER,
            QUANTITY OR QUANTITY_UNIT != 0 OR NULL
        """
        try:
            quantity = self.args.get('quantity', 0)
            quantity_unit = self.args.get('quantity_unit', 0)

            if (quantity == 0 or quantity is None):
                return 'FAILED', self.bad_request(quantity,
                                                  'quantity invalid for offer subscriptions')

            elif (quantity_unit == 0 or quantity_unit is None):
                return 'FAILED', self.bad_request(quantity_unit,
                                                  'quantity_unit invalid for offer subscriptions')

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 6", method=self.method,
                                                     error=e, req=str(self.args))

    def check_tiers_not_consumable(self):
        """
            FOR OFFER SUBSCRIPTIONS
            TIER MUST TYPE 1 (NOT-CONSUMABLE)

            FOR SINGLE/BUNDLE
            TIER MUST TYPE 2 (CONSUMABLE)
        """
        try:
            for tier in self.offer_tiers:
                # check platform
                platform = tier.get('platform_id', 0)
                tier_id = tier.get('tier_id', 0)

                # SPECIAL FOR APPLE ONLY, DAMN!!
                if platform == 1:
                    tier = TierIos.query.filter_by(id=tier_id).first()

                    if not tier:
                        return 'FAILED', self.bad_request(tier_id, 'TIER NOT FOUNDS')

                # -----------------------------------------------------------
                # TODO: Remark 8 lines of code below, for migration purpose
                # -----------------------------------------------------------
                # if self.offer_type_id == 2: # SUBSCRIPTIONS
                #    if tier.tier_type != 1: #not consumable
                #        return 'FAILED', self.bad_request(
                #            tier_id, 'TIER INVALID, MUST NON-CONSUMABLE TYPE')

                # if self.offer_type_id in [1,3]: #Single Bundle
                #    if tier.tier_type != 2: #consumable
                #        return 'FAILED', self.bad_request(
                #            tier_id, 'TIER INVALID, MUST CONSUMABLE TYPE')

                # ANDROID CAN USE TIER CONSUMABLE FOR SUBSCRIPTIONS
                if platform == 2:
                    tier = TierAndroid.query.filter_by(id=tier_id).first()
                    if not tier:
                        return 'FAILED', self.bad_request(tier_id, 'TIER NOT FOUNDS')

                if platform == 3:  # WP
                    tier = TierWp.query.filter_by(id=tier_id).first()
                    if not tier:
                        return 'FAILED', self.bad_request(tier_id, 'TIER NOT FOUNDS')

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 7", method=self.method,
                                                     error=e, req=str(self.args))

    def construct_parameters(self):
        try:
            for item in self.args:
                if self.args[item] == None:
                    pass
                else:
                    if item in self.offer_master:
                        self.offer_master_dict[item] = self.args[item]

                    if item in self.offer_single_bundle:
                        self.offer_single_bundle_dict[item] = self.args[item]

                    if item in self.offer_subscription:
                        self.offer_subscription_dict[item] = self.args[item]
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 8", method=self.method,
                                                     error=e, req=str(self.args))

    def upload_image_offer(self, image=None):
        save_directory = os.path.join(current_app.static_folder, 'offers')
        p = ImageFieldParser(save_directory)
        files = p.from_base64(image)

        raw_files = os.path.join(save_directory, files)
        aws_upload_file(bucket_name=app.config['BOTO3_GRAMEDIA_STATIC_BUCKET'],
                        bucket_folder='offers/',
                        transfer_file=raw_files,
                        output_file=files,
                        is_public_access=True)

        return files

    # -======================== POST ===========================
    def post_offer_master(self):
        try:
            if self.items:
                del self.offer_master_dict['items']

            if self.brands:
                del self.offer_master_dict['brands']

            if self.image_normal:
                image_normal = self.upload_image_offer(self.image_normal)
                self.offer_master_dict['image_normal'] = image_normal

            if self.image_highres:
                image_highres = self.upload_image_offer(self.image_highres)
                self.offer_master_dict['image_highres'] = image_highres

            offer = Offer(**self.offer_master_dict)

            if self.items:
                for i in self.items:
                    offer.items.append(i)

            if self.brands:
                for x in self.brands:
                    offer.brands.append(x)

            sv = offer.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="offer 9", method=self.method,
                                                         error=sv.get('message', ''), req=str(self.args))
            else:
                # clear redis items if post.
                if self.items:
                    for data_item in self.items:
                        purge_redis_cache(self.redis_key, data_item)

                self.data_master = offer
                return 'OK', Response(json.dumps(offer.values()),
                                      status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 9", method=self.method,
                                                     error=e, req=str(self.args))

    def post_single_offers(self):
        try:
            self.offer_single_bundle_dict['offer_id'] = self.data_master.id
            offer = OfferSingle(**self.offer_single_bundle_dict)
            sv = offer.save()

            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="offer 10", method=self.method,
                                                         error=sv.get('message', ''), req=str(self.args))
            else:
                from app import db

                try:
                    # Create new record to gramedia core
                    session = db.session()
                    gramedia_session = get_gramedia_session()
                    gramedia_product = build_offer_from_gramedia_digital(offer.offer, session, gramedia_session)
                    insert_offer_to_gm_db(gramedia_product, gramedia_session)
                    gramedia_session.expunge_all()
                    gramedia_session.close_all()
                except Exception as e:
                    pass

                return 'OK', Response(json.dumps(sv.values()),
                                      status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 10", method=self.method,
                                                     error=e, req=str(self.args))

    def post_subscription_offers(self):
        try:
            self.offer_subscription_dict['offer_id'] = self.data_master.id
            offer = OfferSubscription(**self.offer_subscription_dict)
            sv = offer.save()

            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="offer 11", method=self.method,
                                                         error=sv.get('message', ''), req=str(self.args))
            else:
                return 'OK', Response(json.dumps(sv.values()),
                                      status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 11", method=self.method,
                                                     error=e, req=str(self.args))

    def post_bundle_offers(self):
        try:
            self.offer_single_bundle_dict['offer_id'] = self.data_master.id
            offer = OfferBundle(**self.offer_single_bundle_dict)
            sv = offer.save()

            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="offer 12", method=self.method,
                                                         error=sv.get('message', ''), req=str(self.args))
            else:
                return 'OK', Response(json.dumps(sv.values()),
                                      status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 12", method=self.method,
                                                     error=e, req=str(self.args))

    def post_offer_tiers(self):
        try:
            is_free = False
            for item in self.offer_tiers:
                item['offer_id'] = self.data_master.id

                # checking if free
                price = item.get('price_usd', 0)

                if float(price) <= 0:
                    is_free = True

                tiers = OfferPlatform(**item)
                if is_free:
                    tiers.currency = 'USD'
                    tiers.tier_code = ''
                else:
                    tiers.currency = tiers.get_tier_currency()
                    tiers.tier_code = tiers.get_tier_code()

                sv = tiers.save()
                if sv.get('invalid', False):
                    return 'FAILED', internal_server_message(modul="offer 13", method=self.method,
                                                             error=sv.get('message', ''), req=str(self.args))

            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 13", method=self.method,
                                                     error=e, req=str(self.args))

    # -======================== PUT ===========================
    def put_offer_discount(self):
        """
            calculating discount, if price PUT Offer changes
        """
        try:
            # get offer discount only
            discount_data = self.old_data.discount_id
            for idisc in discount_data:
                discount = Discount.query.filter_by(id=idisc).first()

                if discount:
                    if discount.discount_type == 1:
                        self.discount_master = discount

                        # calculate final price vs discount price
                        if self.price_usd:
                            usd_price = discount.calculate_offer_discount(
                                decimal.Decimal(self.price_usd), 'USD')
                            self.offer_master_dict['discount_price_usd'] = usd_price

                        if self.price_idr:
                            idr_price = discount.calculate_offer_discount(
                                decimal.Decimal(self.price_idr), 'IDR')
                            self.offer_master_dict['discount_price_idr'] = idr_price

                        if self.price_point:
                            point_price = discount.calculate_offer_discount(
                                self.price_point, 'PTS')
                            self.offer_master_dict['discount_price_point'] = point_price

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 14",
                                                     method=self.method, error=e, req=str(self.args))

    def put_offer_discount_platform(self, new_tier=None, old_tier=None):
        """
            calculating discount platform, if price PUT offer_tiers changes
        """
        try:

            new_tier, old_tier = new_tier, old_tier

            price_usd = new_tier.get('price_usd', None)
            price_idr = new_tier.get('price_idr', None)
            price_point = new_tier.get('price_point', None)
            tier_id = new_tier.get('tier_id', 0)

            # data new tiers objects
            if old_tier.platform_id == 1:
                data_new_tier = TierIos.query.filter_by(id=tier_id).first()
            if old_tier.platform_id == 2:
                data_new_tier = TierAndroid.query.filter_by(id=tier_id).first()
            if old_tier.platform_id == 3:
                data_new_tier = TierWp.query.filter_by(id=tier_id).first()

            # get discounts and calculate new price
            if old_tier.discount_id:

                # get discounts
                discount = Discount.query.filter(Discount.id.in_(
                    old_tier.discount_id)).filter_by(
                    discount_type=1).order_by(desc(Discount.id)).first()

                if discount:
                    if discount.discount_valid():
                        old_tier.discount_tag = discount.tag_name
                        old_tier.discount_name = discount.name

                        if price_usd:
                            usd_price = discount.calculate_offer_discount(
                                decimal.Decimal(price_usd), 'USD')
                            old_tier.discount_price_usd = usd_price
                            old_tier.price_usd = price_usd
                            self.offer_put_usd = usd_price

                        if price_idr:
                            idr_price = discount.calculate_offer_discount(
                                decimal.Decimal(price_idr), 'IDR')
                            old_tier.discount_price_idr = idr_price
                            old_tier.price_idr = price_idr

                        if price_point:
                            point_price = discount.calculate_offer_discount(
                                price_point, 'PTS')
                            old_tier.discount_price_point = point_price
                            old_tier.price_point = price_point
                    else:
                        # without discount
                        if price_usd:
                            old_tier.price_usd = price_usd
                            self.offer_put_usd = price_usd
                        if price_idr:
                            old_tier.price_idr = price_idr
                        if price_point:
                            old_tier.price_point = price_point
                else:
                    # without discount
                    if price_usd:
                        old_tier.price_usd = price_usd
                        self.offer_put_usd = price_usd
                    if price_idr:
                        old_tier.price_idr = price_idr
                    if price_point:
                        old_tier.price_point = price_point

            else:
                # without discount
                if price_usd:
                    old_tier.price_usd = price_usd
                    self.offer_put_usd = price_usd
                if price_idr:
                    old_tier.price_idr = price_idr
                if price_point:
                    old_tier.price_point = price_point

            # Non consumable type 2 subscriptions,
            # discount_tier_id == tier_id
            if self.old_data.offer_type_id == 2:
                old_tier.discount_tier_id = old_tier.tier_id

                # update price
                price = str(self.offer_put_usd).split('.')
                final = float('%s.%s' % (price[0], 99))
                old_tier.discount_tier_price = final

                old_tier.tier_code = old_tier.get_tier_code()
                old_tier.discount_tier_code = old_tier.get_tier_code()

            # Consumable
            # discount_tier_id != tier_id, base USD VALUE
            if self.old_data.offer_type_id in [1, 3]:
                price = str(self.offer_put_usd).split('.')
                final = float('%s.%s' % (price[0], 99))

                if old_tier.platform_id == 1:
                    data = TierIos.query.filter_by(tier_price=final, tier_type=2).first()
                if old_tier.platform_id == 2:
                    data = TierAndroid.query.filter_by(tier_price=final).first()
                if old_tier.platform_id == 3:
                    data = TierWp.query.filter_by(tier_price=final).first()

                if data:
                    old_tier.tier_id = tier_id
                    old_tier.discount_tier_id = data.id
                    old_tier.discount_tier_price = data.tier_price
                    old_tier.discount_tier_code = data.tier_code
                    old_tier.tier_code = old_tier.get_tier_code()

            # update old tier
            if data_new_tier:
                old_tier.tier_id = data_new_tier.id
                old_tier.tier_code = data_new_tier.tier_code

            old_tier.save()

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 15",
                                                     method=self.method, error=e, req=str(self.args))

    def put_offer_master(self):
        """
            PUT offer
        """
        try:
            if self.items:
                del self.offer_master_dict['items']

                for old_items in self.old_data.items:
                    self.old_data.items.remove(old_items)
                    purge_redis_cache(self.redis_key, old_items)

                for new_items in self.items:
                    self.old_data.items.append(new_items)
                    purge_redis_cache(self.redis_key, new_items)

            if self.brands:
                del self.offer_master_dict['brands']

                for old_brands in self.old_data.brands:
                    self.old_data.brands.remove(old_brands)

                for new_brands in self.brands:
                    self.old_data.brands.append(new_brands)

            # checking if offer price all changes,
            # if change, calculate again with offer discount only
            if self.old_data.price_usd:
                if float(self.old_data.price_usd) != float(self.price_usd):
                    self.offer_put_calc = True

            if self.old_data.price_idr:
                if float(self.old_data.price_idr) != float(self.price_idr):
                    self.offer_put_calc = True

            if self.old_data.price_point:
                if self.old_data.price_point != self.price_point:
                    self.offer_put_calc = True

            if self.image_normal:
                image_normal = self.upload_image_offer(self.image_normal)
                self.offer_master_dict['image_normal'] = image_normal

            if self.image_highres:
                image_highres = self.upload_image_offer(self.image_highres)
                self.offer_master_dict['image_highres'] = image_highres

            if self.offer_put_calc:
                xput = self.put_offer_discount()

            for k, v in self.offer_master_dict.iteritems():
                setattr(self.old_data, k, v)

            sv = self.old_data.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="offer 16", method=self.method,
                                                         error=sv.get('message', ''), req=str(self.args))
            else:
                # overwrite self.master_items
                self.data_master = self.old_data
                return "OK", Response(json.dumps(self.old_data.values()),
                                      status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 16", method=self.method,
                                                     error=e, req=str(self.args))

    def put_single_offers(self):
        try:
            single_offer = OfferSingle.query.filter_by(offer_id=self.old_data.id).first()
            if not single_offer:
                # create new records
                single_offer = self.post_single_offers()
                if single_offer[0] != 'OK':
                    return single_offer[1]
            else:
                for k, v in self.offer_single_bundle_dict.iteritems():
                    setattr(single_offer, k, v)

                sv = single_offer.save()

                # Update price to gramedia core
                gramedia_session = get_gramedia_session()
                update_product_price_to_gm_db(single_offer.offer, gramedia_session)
                gramedia_session.expunge_all()
                gramedia_session.close_all()

                if sv.get('invalid', False):
                    return 'FAILED', internal_server_message(modul="offer 17",
                                                             method=self.method, error=sv.get('message', ''),
                                                             req=str(self.args))
                else:
                    return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 17", method=self.method,
                                                     error=e, req=str(self.args))

    def put_subscription_offers(self):
        try:
            subs_offer = OfferSubscription.query.filter_by(offer_id=self.old_data.id).first()
            if not subs_offer:
                # create new records
                subs_offer = self.post_subscription_offers()
                if subs_offer[0] != 'OK':
                    return subs_offer[1]
            else:
                for k, v in self.offer_subscription_dict.iteritems():
                    setattr(subs_offer, k, v)

                sv = subs_offer.save()
                if sv.get('invalid', False):
                    return 'FAILED', internal_server_message(modul="offer 18",
                                                             method=self.method, error=sv.get('message', ''),
                                                             req=str(self.args))
                else:
                    return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 18", method=self.method,
                                                     error=e, req=str(self.args))

    def put_bundle_offers(self):
        try:
            bundle_offer = OfferBundle.query.filter_by(offer_id=self.old_data.id).first()
            if not bundle_offer:
                # create new records
                bundle_offer = self.post_bundle_offers()
                if bundle_offer[0] != 'OK':
                    return bundle_offer[1]
            else:
                for k, v in self.offer_single_bundle_dict.iteritems():
                    setattr(bundle_offer, k, v)

                sv = bundle_offer.save()
                if sv.get('invalid', False):
                    return 'FAILED', internal_server_message(modul="offer 19",
                                                             method=self.method, error=sv.get('message', ''),
                                                             req=str(self.args))
                else:
                    return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 19", method=self.method,
                                                     error=e, req=str(self.args))

    def put_offer_tiers(self):
        """
            PUT Offer tiers per-platform
        """

        try:
            for tier in self.offer_tiers:
                platform_id = tier.get('platform_id', 0)
                tier_id = tier.get('tiers_id', 0)

                price_usd = tier.get('price_usd', 0)
                price_idr = tier.get('price_idr', 0)
                price_point = tier.get('price_point', 0)

                # check if old tier == new tier_id
                old_tier = OfferPlatform.query.filter_by(offer_id=self.old_data.id,
                                                         platform_id=platform_id).first()

                if old_tier:
                    # Checking all input data price and tiers
                    if old_tier.tier_id != tier_id:
                        self.offer_put_platform_calc = True

                    if old_tier.price_usd:
                        if float(old_tier.price_usd) != float(price_usd):
                            self.offer_put_platform_calc = True

                    if old_tier.price_idr:
                        if float(old_tier.price_idr) != float(price_idr):
                            self.offer_put_platform_calc = True

                    if old_tier.price_point:
                        if old_tier.price_point != price_point:
                            self.offer_put_platform_calc = True

                    self.offer_put_platform_calc = True
                    if self.offer_put_platform_calc:
                        new_tier = self.put_offer_discount_platform(tier, old_tier)
                    else:
                        pass
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="offer 20", method=self.method,
                                                     error=e, req=str(self.args))

    def success_response(self):
        offer = Offer.query.filter_by(id=self.data_master.id).first()
        if offer:
            return Response(json.dumps(offer.values()),
                            status=httplib.CREATED, mimetype='application/json')
        else:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

    # -======================== PROTOCOL.. STEP BY STEP ========================
    def construct_items(self):

        # single/bundle dont have brands
        if self.offer_type_id == 2:

            # subscriptions, must have brands
            if not self.brands:
                return self.bad_request('brands', "Subscriptions empty value: ")

            brands = self.check_brands()
            if brands[0] != 'OK':
                return brands[1]

        # subscription type doen't have item_id
        if self.offer_type_id != 2:
            # single / bundle must have items
            if not self.items:
                if self.offer_type_id == 1:
                    return self.bad_request('items', "Single empty value: ")
                if self.offer_type_id == 3:
                    return self.bad_request('items', "Bundle empty value: ")

            items = self.check_items()
            if items[0] != 'OK':
                return items[1]

        if self.method == 'POST':
            if self.offer_type_id != 2:
                items_offer = self.check_offer_item()
                if items_offer[0] != 'OK':
                    return items_offer[1]

        offer_type = self.check_offer_type()
        if offer_type[0] != 'OK':
            return offer_type[1]

        if not self.is_free:
            offer_tiers = self.check_offer_tiers()
            if offer_tiers[0] != 'OK':
                return offer_tiers[1]

        # special case for subscriptions
        if self.offer_type_id == 2:
            # check data for subscriptions
            check = self.check_subscription_data()
            if check[0] != 'OK':
                return check[1]

        # check tiers non-consumable
        if not self.is_free:
            tier_nt = self.check_tiers_not_consumable()
            if tier_nt[0] != 'OK':
                return tier_nt[1]

        param = self.construct_parameters()
        if param[0] != 'OK':
            return param[1]

        if self.method == 'POST':

            master = self.post_offer_master()
            if master[0] != 'OK':
                return master[1]

            tiers = self.post_offer_tiers()
            if tiers[0] != 'OK':
                return tiers[1]

            if self.offer_type_id == 1:  # single
                xo = self.post_single_offers()
            if self.offer_type_id == 2:  # subscriptions
                xo = self.post_subscription_offers()
            if self.offer_type_id == 3:  # bundle
                xo = self.post_bundle_offers()

            if xo[0] != 'OK':
                return xo[1]

        if self.method == 'PUT':
            if self.old_data.offer_type_id != self.offer_type_id:
                return internal_server_message(modul="offer", method=self.method,
                                               error="OFFER TYPES CHANGES, PLEASE CONSULT WITH YOUR ADMIN",
                                               req=str(self.args))
            else:
                master = self.put_offer_master()
                if master[0] != 'OK':
                    return master[1]

                tiers = self.put_offer_tiers()
                if tiers[0] != 'OK':
                    return tiers[1]

                if self.offer_type_id == 1:  # single
                    xo = self.put_single_offers()
                if self.offer_type_id == 2:  # subscriptions
                    xo = self.put_subscription_offers()
                if self.offer_type_id == 3:  # bundle
                    xo = self.put_bundle_offers()

                if xo[0] != 'OK':
                    return xo[1]

        success = self.success_response()
        return success


def deserialize_buffet_related_fields(self, model, req_data):
    model.offer = Offer(
        **{k: req_data['offer'][k]
           for k in req_data['offer']
           if k not in ['items', 'brands', 'platforms_offers']}
    )

    model.offer.brands = [
        self.query_set.session.query(Brand).get(brand_id)
        for brand_id in req_data['offer']['brands']
    ]
    model.offer.items = [
        self.query_set.session.query(Item).get(item_id)
        for item_id in req_data['offer']['items']
    ]

    def get_or_create_offer_platform(op_data):
        if op_data.get('id'):
            op = self.query_set.session.query(OfferPlatform).get(op_data['id'])
            for k in op_data:
                setattr(op, k, op_data[k])
        else:
            return OfferPlatform(**op_data)

    model.offer.offer_platforms = [
        get_or_create_offer_platform(op)
        for op in req_data['offer']['platforms_offers']
    ]
