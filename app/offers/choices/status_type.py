NEW = 1
WAITING_FOR_REVIEW = 2
IN_REVIEW = 3
REJECTED = 4
APPROVED = 5
PREPARE_FOR_SALE = 6
READY_FOR_SALE = 7
NOT_FOR_SALE = 8

OFFER_STATUS = {
    NEW: 'new',
    WAITING_FOR_REVIEW: 'waiting for review',
    IN_REVIEW: 'in review',
    REJECTED: 'rejected',
    APPROVED: 'approved',
    PREPARE_FOR_SALE: 'prepare for sale',
    READY_FOR_SALE: 'ready for sale',
    NOT_FOR_SALE: 'not for sale'
}
