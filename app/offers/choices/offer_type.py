SINGLE = 1
SUBSCRIPTIONS = 2
BUNDLE = 3
BUFFET = 4

OFFER_TYPES = {SINGLE: 'single',
               SUBSCRIPTIONS: 'subscriptions',
               BUNDLE: 'bundle',
               BUFFET: 'buffet',
              }
