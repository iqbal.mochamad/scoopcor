UNIT = 1
DAILY = 2
WEEKLY = 3
MONTHLY = 4
YEARLY = 5

QUANTITY_UNIT = {
    UNIT: 'unit',
    DAILY: 'daily',
    WEEKLY: 'weekly',
    MONTHLY: 'monthly',
    YEARLY: 'yearly'
}


ID_FORMAT_QUANTITY_UNIT = {
    UNIT: 'edisi',
    DAILY: 'hari',
    WEEKLY: 'minggu',
    MONTHLY: 'bulan',
    YEARLY: 'tahun'
}

EN_FORMAT_QUANTITY_UNIT = {
    UNIT: 'edition',
    DAILY: 'day',
    WEEKLY: 'week',
    MONTHLY: 'month',
    YEARLY: 'year'
}
