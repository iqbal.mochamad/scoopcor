from decimal import Decimal

from dateutil.relativedelta import relativedelta
import re
from werkzeug.exceptions import BadRequest

from flask_appsfoundry.parsers import \
    converters, \
    StringField, DateTimeField, \
    InputField, PositiveIntegerField, ParserBase, SqlAlchemyFilterParser, \
    StringFilter, IntegerFilter, DateTimeFilter, IntegerField, BooleanField, \
    DecimalField, BooleanFilter #, NestedField
from . import models, choices
from .choices.offer_type import BUFFET

duration_regex = re.compile(
    r"^(?:P"
    r"(?:(?P<years>\d+)Y)?"
    r"(?:(?P<months>\d+)M)?"
    r"(?:(?P<days>\d+)D)?"
    r")?"
    r"(?:T"
    r"(?:(?P<hours>\d+)H)?"
    r"(?:(?P<minutes>\d+)M)?"
    r"(?:(?P<seconds>\d+)S)?"
    r")?$")


def relativedelta_from_iso8601(input):
    r = duration_regex.search(input)
    groups = r.groupdict(default=0)

    return relativedelta(
        years=int(groups['years']),
        months=int(groups['months']),
        days=int(groups['days']),
        hours=int(groups['hours']),
        minutes=int(groups['minutes']),
        seconds=int(groups['seconds']))


class OfferParser(ParserBase):
    name = StringField(required=True)
    offer_status = IntegerField(required=True) # limit choices
    sort_priority = IntegerField(required=True)
    is_active = BooleanField(required=True)
    offer_type_id = IntegerField(required=True) # limit choices

    items = PositiveIntegerField(action='append')
    brands = PositiveIntegerField(action='append')

    price_usd = DecimalField(required=True)
    price_idr = DecimalField(required=True)
    price_point = IntegerField(required=True)

    description = StringField()


def offer_field(input):
    result = {
        'name': input['name'],
        'offer_status': int(input['offer_status']),
        'sort_priority': int(input['sort_priority']),
        'is_active': converters.boolean(input['is_active']),
        'offer_type_id': int(input.get('order_type_id', BUFFET)),
        'exclusive_clients': input.get('exclusive_clients'), # Why is this not an integer array?
        'is_free': converters.boolean(input.get('is_free', False)),

        # these two are documented as not being used after scoopcore drop
        # 'offer_code': input.get('offer_code'),
        # 'item_code': input.get('item_code'),

        'price_usd': Decimal(input['price_usd']),
        'price_idr': int(input['price_idr']),
        'price_point': int(input['price_point']),

        'vendor_price_usd': Decimal(input['vendor_price_usd']) if input.get('vendor_price_usd') else None,
        'vendor_price_idr': int(input['vendor_price_idr']) if input.get('vendor_price_idr') else None,
        'vendor_price_point': int(input['vendor_price_point']) if input.get('vendor_price_point') else None,

        'discount_id': input.get('discount_id'),
        'discount_tag': input.get('discount_tag'),
        'discount_name': input.get('discount_name'),
        'is_discount': input.get('is_discount'),
        'discount_price_usd': Decimal(input['discount_price_usd']) if input.get('discount_price_usd') else None,
        'discount_price_idr': int(input['discount_price_idr']) if input.get('discount_price_idr') else None,
        'discount_price_point': int(input['discount_price_point']) if input.get('discount_price_point') else None,

        'image_normal': input.get('image_normal'),
        'image_highres': input.get('image_highres'),

        'items': [int(item_id) for item_id in input['items']],
        'brands': [int(brand_id) for brand_id in input['brands']],

        'platforms_offers': [platforms_offers_field(op) for op in input.get('platforms_offers', [])]
    }

    if result['offer_status'] not in choices.OFFER_STATUS:
        raise BadRequest(description='Invalid offer status')

    if result['offer_type_id'] != BUFFET:
        raise BadRequest(description='Invalid offer type id: must be buffet (4)')

    return result


def platforms_offers_field(input):
    result = {
        'offer_id': input.get('offer_id'),
        'platform_id': input['platform_id'],
        'tier_id': input.get('tier_id'),
        'currency': input.get('currency'),
        'price_usd': Decimal(input.get('price_usd')) if input.get('price_usd') else None,
        'price_idr': int(input.get('price_idr')) if input.get('price_idr') else None,
        'price_point': int(input.get('price_point')) if input.get('price_point') else None,

        'discount_id': int(input.get('discount_id')) if input.get('discount_id') else None,
        'discount_tier_id': int(input.get('discount_tier_id')) if input.get('discount_tier_id') else None,
        'discount_tier_code': input.get('discount_id'),
        'discount_tag': input.get('discount_tag'),
        'discount_name': input.get('discount_name'),
        'discount_tier_price': Decimal(input.get('discount_tier_price')) if input.get('discount_tier_price') else None,
        'discount_price_usd': int(input.get('discount_price_usd')) if input.get('discount_price_usd') else None,
        'discount_price_idr': int(input.get('discount_price_idr')) if input.get('discount_price_idr') else None,
        'discount_price_point': int(input.get('discount_price_point')) if input.get('discount_price_point') else None,
    }
    return result


class OfferBuffetArgsParser(ParserBase):
    available_from = DateTimeField()
    available_until = DateTimeField()
    buffet_duration = InputField(type=relativedelta_from_iso8601, required=True)
    offer = InputField(type=offer_field, required=True)


class OfferBuffetListArgsParser(SqlAlchemyFilterParser):
    __model__ = models.OfferBuffet
    id = IntegerFilter()
    name = StringFilter()
    available_from = DateTimeFilter()
    available_to = DateTimeFilter()
    # DURATION!
    # AVAILABLE BRANDS
    # AVAILABLE ITEMS


class OfferTypeArgsParser(ParserBase):
    name = StringField(required=True)
    description = StringField()
    slug = StringField(required=True)
    meta = StringField()
    sort_priority = IntegerField(required=True)
    is_active = BooleanField()


class OfferTypeListArgsParser(SqlAlchemyFilterParser):
    __model__ = models.OfferType
    id = IntegerFilter()
    name = StringFilter()
    description = StringFilter()
    slug = StringFilter()
    meta = StringFilter()
    sort_priority = IntegerFilter()
    is_active = BooleanFilter()


