from flask import Blueprint
from flask_appsfoundry import Api

from .api import OfferTypeListApi, OfferTypeApi, OfferListApi, OfferApi, \
    OfferBuffetDetailApi, OfferBuffetListApi, OfferSingleItemPreviewsApi, OfferItemApi
from .patch import MaydayOfferActivate, MaydayOfferInactivate

offer_types_blueprint = Blueprint('offers_types', __name__, url_prefix='/v1/offer_types')
offer_types_api = Api(offer_types_blueprint)
offer_types_api.add_resource(OfferTypeListApi, '')
offer_types_api.add_resource(OfferTypeApi, '/<int:offer_id>', endpoint='offer types')

offers_blueprint = Blueprint('offers', __name__, url_prefix='/v1/offers')
offers_api = Api(offers_blueprint)
offers_api.add_resource(OfferListApi, '')
offers_api.add_resource(OfferApi, '/<int:offer_id>', endpoint='offers')
offers_api.add_resource(OfferItemApi, '/items', endpoint='offers_items')
offers_api.add_resource(OfferSingleItemPreviewsApi, '/<string:db_id>/previews', endpoint='offer_previews')

offers_api.add_resource(MaydayOfferActivate, '/mayday_activate')
offers_api.add_resource(MaydayOfferInactivate, '/mayday_inactivate')

offers_api.add_resource(OfferBuffetListApi, '/buffets')
offers_api.add_resource(OfferBuffetDetailApi, '/buffets/<int:db_id>')

