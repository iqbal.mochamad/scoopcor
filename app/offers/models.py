import decimal
from unidecode import unidecode

from enum import IntEnum
from flask import url_for, g
from sqlalchemy import Column, DateTime, Integer, ForeignKey, event
from sqlalchemy.sql import desc
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import ARRAY, INTERVAL

from flask_appsfoundry import models
from app import app, db
from app.offers.event import publish_to_elevenia
from app.tiers.models import TierAndroid, TierIos, TierWp
from app.master.models import Brand
from app.discounts.models import Discount
from app.items.choices.item_type import ITEM_TYPES, MAGAZINE, BOOK, NEWSPAPER
from app.master.choices import PlatformType
from app.discounts.choices.discount_type import DISCOUNT_OFFER
from app.discounts.choices.predefined_type import ALL_MAGAZINE, ALL_BOOK, ALL_SINGLE, MAGAZINE_SINGLE, HARPER_COLLINS
from app.utils.shims import item_types
from app.utils.datetimes import get_local
from .choices.offer_type import SINGLE, SUBSCRIPTIONS, BUNDLE, BUFFET
from .choices.quantity_unit import EN_FORMAT_QUANTITY_UNIT, ID_FORMAT_QUANTITY_UNIT
from .serializers import DurationField
from sqlalchemy.dialects.postgresql import JSON

from app.helpers import slugify

"""
    LEGEND
        QUANTITY UNIT
            1. UNIT
            2. DAY
            3. WEEK
            4. MONTH

        BACKWARD QUANTITY UNIT
            1. UNIT
            2. DAY
            3. DAY
            4. MONTH
"""


offer_item = db.Table(
    'core_offers_items',
    db.Column('offer_id', db.Integer, db.ForeignKey('core_offers.id')),
    db.Column('item_id', db.Integer, db.ForeignKey('core_items.id')),
    db.PrimaryKeyConstraint('offer_id', 'item_id', name='core_offers_items_pk')
)

offer_brand = db.Table(
    'core_offers_brands',
    db.Column('offer_id', db.Integer, db.ForeignKey('core_offers.id')),
    db.Column('brand_id', db.Integer, db.ForeignKey('core_brands.id')),
    db.PrimaryKeyConstraint('offer_id', 'brand_id', name='core_offers_brands_pk')
)

offer_vendor = db.Table(
    'core_offers_vendors',
    db.Column('offer_id', db.Integer, db.ForeignKey('core_offers.id')),
    db.Column('vendor_id', db.Integer, db.ForeignKey('core_vendors.id')),
    db.PrimaryKeyConstraint('offer_id', 'vendor_id', name='core_offers_vendors_pk')
)


class OfferType(models.TimestampMixin, db.Model):
    __tablename__ = 'core_offertypes'
    name = db.Column(db.String(150))
    description = db.Column(db.Text)
    slug = db.Column(db.String(100), nullable=False)
    meta = db.Column(db.Text)
    sort_priority = db.Column(db.SmallInteger)
    is_active = db.Column(db.Boolean(), default=False)


class Offer(models.TimestampMixin, db.Model):

    __tablename__ = 'core_offers'

    class Type(IntEnum):
        single = 1
        subscription = 2
        bundle = 3
        buffet = 4
        wallet_offer = 5

    class Status(IntEnum):
        new = 1
        waiting_for_review = 2
        in_review = 3
        rejected = 4
        approved = 5
        prepare_for_sale = 6
        ready_for_sale = 7
        not_for_sale = 8

    name = db.Column(db.String(150))
    long_name = db.Column(db.String(150))
    offer_status = db.Column(db.SmallInteger, nullable=False)
    sort_priority = db.Column(db.SmallInteger, nullable=False)
    is_active = db.Column(db.Boolean(), default=False)

    offer_type = db.relationship('OfferType', backref='offers')
    offer_type_id = db.Column(db.Integer, db.ForeignKey('core_offertypes.id'), nullable=False)

    exclusive_clients = db.Column(ARRAY(db.String))
    is_free = db.Column(db.Boolean(), default=False)
    offer_code = db.Column(db.String(250)) #Note: not used after old scoopcore drop
    item_code = db.Column(db.String(250)) #Note: not used after old scoopcore drop

    price_usd = db.Column(db.Numeric(10, 2), nullable=False)
    price_idr = db.Column(db.Numeric(10, 2), nullable=False)
    price_point = db.Column(db.Integer)

    vendor_price_usd = db.Column(db.Numeric(10, 2))
    vendor_price_idr = db.Column(db.Numeric(10, 2))
    vendor_price_point = db.Column(db.Integer)

    #discounts data
    discount_id = db.Column(ARRAY(db.String))
    discount_tag = db.Column(db.String(150))
    discount_name = db.Column(db.String(150))
    discount_price_usd = db.Column(db.Numeric(10, 2))
    discount_price_idr = db.Column(db.Numeric(10, 2))
    discount_price_point = db.Column(db.Integer)
    is_discount = db.Column(db.Boolean(), default=False)

    image_normal = db.Column(db.String(150))
    image_highres = db.Column(db.String(150))
    image_banner = db.Column(JSON(db.Text), nullable=True)
    colors = db.Column(JSON(db.Text), nullable=True)

    items = db.relationship(
        'Item',
        secondary=offer_item,
        backref=db.backref('core_offers', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    brands = db.relationship(
        'Brand',
        secondary=offer_brand,
        backref=db.backref('core_offers', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    vendors = db.relationship(
        'Vendor',
        secondary=offer_vendor,
        backref=db.backref('offers', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    def __repr__(self):
        return '<name : %s >' % self.name

    @property
    def api_url(self):
        try:
            return url_for('offers.offers', offer_id=self.id, _external=True)
        except RuntimeError:
            return None

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Offer success add"}

        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Offer success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def display_calculate(self, discounts=None, platform_id=None, offer=None):

        if int(platform_id) in [PlatformType.ios, PlatformType.android, PlatformType.windows_phone]:
            usd_final = discounts.calculate_offer_discount(decimal.Decimal(offer.price_usd), 'USD')
            idr_final = discounts.calculate_offer_discount(decimal.Decimal(offer.price_idr), 'IDR')
            pts_final = discounts.calculate_offer_discount(offer.price_point, 'PTS')

            usd_price = str(usd_final).split('.')
            usd_final = float('%s.99' % usd_price[0])

        else:
            usd_final = discounts.calculate_offer_discount(decimal.Decimal(self.price_usd), 'USD')
            idr_final = discounts.calculate_offer_discount(decimal.Decimal(self.price_idr), 'IDR')
            pts_final = discounts.calculate_offer_discount(self.price_point, 'PTS')

        return usd_final, idr_final, pts_final

    def display_discounts(self, idiscounts=None, values=None, platform_id=None, offer=None):

        discount_statusx = [False]
        if idiscounts:
            discount_data = Discount.query.filter(Discount.id.in_(idiscounts)
                ).filter_by(discount_type=DISCOUNT_OFFER,
                is_active=True).order_by(desc(Discount.id)).all()

            if discount_data:
                discount_statusx = [ix.discount_valid() for ix in discount_data]

        # RECHECK NORMAL DISCOUNTS
        if True in discount_statusx:

            discount_data = Discount.query.filter(Discount.id.in_(idiscounts)
                ).filter_by(discount_type=DISCOUNT_OFFER,
                is_active=True).order_by(desc(Discount.id)).all()

            for discounts in discount_data:

                if (discounts.discount_valid() and self.id in discounts.offer_id() and
                    int(platform_id) in discounts.platform_id() and not self.is_free):

                    usd_final, idr_final, pts_final = self.display_calculate(discounts, platform_id, offer)

                    # FOR IOS, RECHECK CONSUMABLE OR NOT CONSUMABLE
                    if platform_id in [ PlatformType.ios ]:
                        tier_type = offer.get_tier_type()

                        if tier_type == 1: #NON CONSUMABLE
                            values['tier_code'] = offer.tier_code
                        else:
                            data = TierIos.query.filter_by(tier_price=usd_final, tier_type=2).first()
                            values['tier_code'] = data.tier_code

                    values['discount_price_usd'] = '%.2f' % usd_final
                    values['discount_price_idr'] = '%.2f' % idr_final
                    values['discount_price_point'] = self.price_point
                    values['discount_tag'] = discounts.tag_name
                    values['discount_name'] = discounts.name
                    values['discount_paymentgateway_id'] = discounts.paymentgateway_id()
                    values['discount_id'] = [discounts.id]
                    values['is_discount'] = True if (
                        discounts.discount_usd > 0 or discounts.discount_idr > 0
                        or discounts.discount_point) else False

                    return values

        else:
            discounts = None

            # RECHECK PREDEFINED DISCOUNTS Single & Bundle
            if self.offer_type_id in [SINGLE, BUNDLE]:
                items = [i for i in self.items]

                if items:
                    items = items[0]

                    # MAGAZINE AND OFFERS SINGLE
                    if items.item_type == ITEM_TYPES[MAGAZINE] and self.offer_type_id == SINGLE:

                        discounts = Discount.query.filter(Discount.predefined_group.in_([
                            ALL_MAGAZINE, ALL_SINGLE, MAGAZINE_SINGLE])).filter_by(
                            discount_type=DISCOUNT_OFFER, is_active=True).order_by(
                            desc(Discount.id)).first()

                    # BOOKS AND OFFERS SINGLE
                    if items.item_type == ITEM_TYPES[BOOK] and self.offer_type_id == SINGLE:

                        # HARPER COLLINS, TEMPORARY HARDCODE
                        brands = Brand.query.filter_by(id=items.brand_id).first()
                        if brands.vendor_id == HARPER_COLLINS:
                            discounts = Discount.query.filter_by(
                                predefined_group=HARPER_COLLINS,
                                discount_type=DISCOUNT_OFFER,
                                is_active=True).order_by(desc(Discount.id)).first()
                        else:
                            # REDUCE PRICE BOOKS
                            discounts = Discount.query.filter(
                                Discount.valid_to >= get_local(),
                                Discount.predefined_group.in_(
                                [ALL_BOOK, ALL_SINGLE])).filter_by(
                                discount_type=DISCOUNT_OFFER,
                                is_active=True).order_by(desc(Discount.id)).first()

            if discounts:
                if (discounts.discount_valid() and int(platform_id) in discounts.platform_id()
                    and not self.is_free):

                    usd_final, idr_final, pts_final = self.display_calculate(discounts, int(platform_id), offer)
                    values['discount_price_usd'] = '%.2f' % usd_final
                    values['discount_price_idr'] = '%.2f' % idr_final
                    values['discount_price_point'] = self.price_point
                    values['discount_tag'] = discounts.tag_name
                    values['discount_name'] = discounts.name
                    values['discount_paymentgateway_id'] = discounts.paymentgateway_id()
                    values['discount_id'] = [discounts.id]
                    values['is_discount'] = True if (discounts.discount_usd > 0 or discounts.discount_idr > 0 or
                                                     discounts.discount_point > 0) else False

        return values

    def display_offers(self, platform_id=None):

        data = []
        if PlatformType.web not in platform_id:

            #offer_platforms = OfferPlatform.query.filter_by(offer_id=self.id,
            #    platform_id=platform_id).order_by(OfferPlatform.price_usd).all()
            offer_platforms = OfferPlatform.query.filter(
                OfferPlatform.platform_id.in_(platform_id)).filter_by(
                offer_id=self.id).all()

            if offer_platforms:
                for offer in offer_platforms:

                    tmp = {}
                    tmp['offer_id'] = self.id
                    tmp['offer_name'] = self.name
                    tmp['offer_type_id'] = self.offer_type_id
                    tmp['is_free'] = self.is_free
                    tmp['platform_id'] = offer.platform_id
                    tmp['tier_id'] = offer.tier_id

                    tmp['image_highres'] = self.image_highres #preview_s3_statics(self, 'image_offers')
                    tmp['image_normal'] = self.image_normal #preview_s3_statics(self, 'image_offers')
                    tmp['media_base_url'] = app.config['OFFERS_BASE_URL']

                    tmp['tier_code'] = offer.tier_code

                    tmp['price_usd'] = self.migrate_decimal_null(offer.price_usd)
                    tmp['price_idr'] = self.migrate_decimal_null(offer.price_idr)
                    tmp['price_point'] = offer.price_point
                    tmp['colors'] = offer.colors if offer.colors else None

                    # EXCLUDE DISCOUNTS FOR WHITELABEL, FOREVER!!
                    current_client_id = g.current_client.id if getattr(g, 'current_client', None) and g.current_client else None
                    if current_client_id and current_client_id in app.config['WHITELABEL_CLIENT_ID']:
                        tmp = tmp
                    else:
                        tmp = self.display_discounts(self.discount_id,
                            tmp, offer.platform_id, offer)

                    data.append(tmp)
        else:
            tmp = {}

            # General offer data
            tmp['offer_id'] = self.id
            tmp['offer_name'] = self.name
            tmp['offer_type_id'] = self.offer_type_id
            tmp['platform_id'] = platform_id
            tmp['is_free'] = self.is_free

            tmp['image_highres'] = self.image_highres #preview_s3_statics(self, 'image_offers')
            tmp['image_normal'] = self.image_normal #preview_s3_statics(self, 'image_offers')
            tmp['media_base_url'] = app.config['OFFERS_BASE_URL']

            tmp['price_usd'] = self.migrate_decimal_null(self.price_usd)
            tmp['price_idr'] = self.migrate_decimal_null(self.price_idr)
            tmp['price_point'] = self.price_point
            tmp['colors'] = self.colors if self.colors else None

            # EXCLUDE DISCOUNTS FOR WHITELABEL, FOREVER!!
            current_client_id = g.current_client.id if getattr(g, 'current_client', None) and g.current_client else None
            if current_client_id and current_client_id in app.config['WHITELABEL_CLIENT_ID']:
                tmp = tmp
            else:
                tmp = self.display_discounts(idiscounts=self.discount_id,
                    values=tmp, platform_id=PlatformType.web)

            data.append(tmp)

        return data

    def is_restricted_offers(self, country_code=None):
        from app.items.choices.distribution_type import RESTRICTED
        for data_item in self.items:
            if data_item.item_distribution_country_group:
                return (data_item.item_distribution_country_group.group_type == RESTRICTED and
                        country_code in data_item.item_distribution_country_group.countries), unidecode(unicode(data_item.name))
        return False, ''

    def get_platform_offer(self):
        offer_platforms = OfferPlatform.query.filter_by(offer_id=self.id).order_by(OfferPlatform.price_usd).all()
        return [{
                'platform_id': data_offer.platform_id,
                'price_usd': "%.2f" % data_offer.price_usd,
                'price_idr': "%.2f" % data_offer.price_idr,
                'price_point': data_offer.price_point,
                'tier_id': data_offer.tier_id
            } for data_offer in offer_platforms]

    def get_items(self):
        return [item.id for item in self.items if self.items][:20]

    def get_item_objects(self):
        item_data = []

        for data_item in self.items:
            tmp = {}
            tmp['id'] = data_item.id
            tmp['name'] = unidecode(unicode(data_item.name))

            if data_item.brand.vendor_id == HARPER_COLLINS:
                tmp['item_type_id'] = HARPER_COLLINS
            else:
                tmp['item_type_id'] = item_types.enum_to_id(data_item.item_type)
            item_data.append(tmp)
        return item_data

    def get_item_for_orders(self):
        data = []
        for data_items in self.items:
            itemtype_id = item_types.enum_to_id(data_items.item_type)

            if itemtype_id == MAGAZINE: data_details = data_items.get_magazine()
            if itemtype_id == BOOK: data_details = data_items.get_books()
            if itemtype_id == NEWSPAPER: data_details = data_items.get_news()

            temp = {'name': unidecode(unicode(data_items.name)),
                    'id': data_items.id,
                    'media_base_url': app.config['MEDIA_BASE_URL'],
                    'item_type_id': item_types.enum_to_id(data_items.item_type),
                    'edition_code': data_items.edition_code,
                    'thumb_image_normal': data_details.get('thumb_image_highres', ''),
                    'thumb_image_highres': data_details.get('thumb_image_normal', '')
            }
            data.append(temp)
        return data

    def get_brands_id(self):
        return [data_brand.id for data_brand in self.brands][:20]

    def get_brand_objects(self):
        return [{'id': data_brand.id, 'name': unidecode(unicode(data_brand.name))} for data_brand in self.brands][:20]

    def get_brands(self, related_brand_id):
        data_brand = self.brands

        if related_brand_id:
            specified_id = related_brand_id.split(',')
            data_brand = self.brands.filter(Brand.id.in_(specified_id)).limit(20).all()
        return [{'id': data_found.id, 'name': unidecode(unicode(data_found.name))} for data_found in data_brand][:20]

    def get_subscrip_quantity(self):
        quantity, quantity_unit, allow_backward, backward_quantity, backward_quantity_unit = 0, 0, False, 0, 0
        if self.offer_type_id == SUBSCRIPTIONS:
            subs = OfferSubscription.query.filter_by(offer_id=self.id).first()
            if subs:
                quantity = subs.quantity
                quantity_unit = subs.quantity_unit
                allow_backward = subs.allow_backward

                if subs.backward_quantity:
                    backward_quantity = subs.backward_quantity
                    backward_quantity_unit = subs.backward_quantity_unit
                else:
                    backward_quantity = 0
                    backward_quantity_unit = 0

        return quantity, quantity_unit, allow_backward, backward_quantity, backward_quantity_unit

    def get_display_offer_name(self, loc='id'):
        display_name = 'Unknown Offer'
        if self.offer_type_id == SUBSCRIPTIONS:
            quantity, quantity_unit, allow_backward, backward_quantity, backward_quantity_unit = self.get_subscrip_quantity()
            formatting = EN_FORMAT_QUANTITY_UNIT if loc != 'id' else ID_FORMAT_QUANTITY_UNIT

            displays = formatting[quantity_unit]
            if loc == 'en' and quantity > 1:
                displays = displays + 's'

            display_name = '{} {}'.format(quantity, displays)

        return display_name

    def get_offertype(self):
        return {'id': self.offer_type.id, 'name': self.offer_type.name} if self.offer_type else {}

    def get_descriptions(self):

        if self.offer_type_id == SINGLE:
            data_offer = OfferSingle.query.filter_by(offer_id=self.id).first()
        if self.offer_type_id == SUBSCRIPTIONS:
            data_offer = OfferSubscription.query.filter_by(offer_id=self.id).first()
        if self.offer_type_id == BUNDLE:
            data_offer = OfferBundle.query.filter_by(offer_id=self.id).first()
        if self.offer_type_id == BUFFET:
            data_offer = None
        return data_offer.description if data_offer else ''

    def get_buffet_details(self):
        return {
            'id': self.buffet[0].id,
            'available_from': self.buffet[0].available_from.isoformat(),
            'available_until': self.buffet[0].available_until.isoformat(),
            'buffet_duration': DurationField().format(self.buffet[0].buffet_duration)
        } if self.offer_type_id == BUFFET else None

    def get_buffet_objects(self):
        return self.buffet[0] if self.offer_type_id == BUFFET else None

    def extension_values(self, extension=None, custom=None, platform_id=None):
        data_values = self.values()

        for param in extension:
            if param == 'platforms_offers':
                if PlatformType.web not in platform_id:
                    data_values['platforms_offers'] = self.display_offers(platform_id)

            if param == 'offer_type':
                data_values['offer_type'] = self.get_offertype()
                del data_values['offer_type_id'] # JIRA : SCOOP-2128

            if param == 'items':
                data_values['items'] = self.get_item_for_orders()

            if param == 'brands':
                data_values['brands'] = self.get_brand_objects()

            if param == 'buffet_details':
                data_values['buffet_details'] = self.get_buffet_details()

            if param == 'ext':
                data_values['offer_type'] = self.get_offertype()
                del data_values['offer_type_id'] # JIRA : SCOOP-2128
                data_values['items'] = self.get_item_for_orders()
                data_values['brands'] = self.get_brand_objects()
                data_values['buffet_details'] = self.get_buffet_details()

                if PlatformType.web not in platform_id:
                    data_values['platforms_offers'] = self.display_offers(platform_id)

        if custom:
            original_values = data_values

            kwargs = {}
            fields_required = custom.split(',')
            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
            return kwargs
        return data_values

    def migrate_decimal_null(self, objects=None):
        # JIRA : SCOOP-2116
        if objects is not None:
            return '%.2f' % objects
        else:
            return None

    def item_sample_id(self):
        from app.items.models import Item
        item_sample = None
        if self.offer_type_id == BUFFET:
            brand_id_sample = self.brands.first().id
            item_sample = Item.query.filter_by(brand_id=brand_id_sample).first().id
        return item_sample

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['name'] = self.name
        rv['long_name'] = self.long_name
        rv['exclusive_clients'] = self.exclusive_clients
        rv['offer_status'] = self.offer_status
        rv['sort_priority'] = self.sort_priority
        rv['is_active'] = self.is_active
        rv['offer_type_id'] = self.offer_type_id
        rv['is_free'] = self.is_free
        rv['offer_code'] = self.offer_code
        rv['item_code'] = self.item_code
        rv['price_usd'] = self.migrate_decimal_null(self.price_usd)
        rv['price_idr'] = self.migrate_decimal_null(self.price_idr)
        rv['price_point'] = self.price_point
        rv['vendor_price_usd'] = self.migrate_decimal_null(self.vendor_price_usd)
        rv['vendor_price_idr'] = self.migrate_decimal_null(self.vendor_price_idr)
        rv['vendor_price_point'] = self.vendor_price_point
        rv['description'] = self.get_descriptions()
        rv['image_highres'] = self.image_highres #preview_s3_statics(self, 'image_offers')
        rv['image_normal'] = self.image_normal #preview_s3_statics(self, 'image_offers')
        rv['media_base_url'] = app.config['OFFERS_BASE_URL']
        rv['colors'] = self.colors if self.colors else None
        rv['slug'] = slugify(self.long_name) if self.long_name else ''
        rv['image_banner'] = self.image_banner

        # this is only for GETSCOOP, getscoop cart need 1 item id, because premium is doesnt have item only brands.
        # they always hardcoded in their side.
        rv['item_sample_id'] = self.item_sample_id()

        #subscriptions only
        if self.offer_type_id == SUBSCRIPTIONS:
            data = self.get_subscrip_quantity()
            rv['quantity'], rv['quantity_unit'], rv['allow_backward'], rv['backward_quantity'], rv['backward_quantity_unit'] = data

        self.display_discounts(idiscounts=self.discount_id, values=rv, platform_id=PlatformType.web)

        return rv


class OfferSubscription(models.TimestampMixin, db.Model):

    __tablename__ = 'core_offersubscriptions'

    class QuantityUnit(IntEnum):
        # enum value for QUANTITY UNIT and BACKWARD QUANTITY UNIT
        unit = 1
        day = 2
        week = 3
        month = 4

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.Text)
    quantity = db.Column(db.Integer, nullable=False)
    quantity_unit = db.Column(db.SmallInteger, nullable=False)
    allow_backward = db.Column(db.Boolean(), default=False)
    backward_quantity = db.Column(db.Integer)
    backward_quantity_unit = db.Column(db.Integer)

    offer_id = db.Column(db.Integer, db.ForeignKey('core_offers.id'), nullable=False)
    offer = db.relationship("Offer", backref=db.backref("subscription", lazy="dynamic"))

    def __repr__(self):
        return '<name : {} >'.format(self.offer.long_name)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OfferDurationSubscription success add"}
        except Exception, e:
            db.session.rollback()
            #db.session.delete(self)
            #db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OfferDurationSubscription success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['description'] = self.description
        rv['quantity'] = self.quantity
        rv['quantity_unit'] = self.quantity_unit
        rv['allow_backward'] = self.allow_backward
        rv['backward_quantity'] = self.backward_quantity
        rv['backward_quantity_unit'] = self.backward_quantity_unit
        rv['offer_id'] = self.offer_id

        return rv


class OfferSingle(models.TimestampMixin, db.Model):

    __tablename__ = 'core_offersingles'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.Text)
    offer_id = db.Column(db.Integer, db.ForeignKey('core_offers.id'), nullable=False)
    offer = db.relationship("Offer", backref=db.backref("offer_singles", lazy="dynamic"))

    def __repr__(self):
        return '<offer_id : %s >' % self.offer_id

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OfferSingle success add"}
        except Exception, e:
            db.session.rollback()
            #db.session.delete(self)
            #db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OfferSingle success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['description'] = self.description
        rv['offer_id'] = self.offer_id
        return rv


class OfferBundle(models.TimestampMixin, db.Model):

    __tablename__ = 'core_offerbundles'

    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.Text)
    offer_id = db.Column(db.Integer, db.ForeignKey('core_offers.id'), nullable=False)
    offer = db.relationship("Offer")

    def __repr__(self):
        return '<offer_id : %s >' % self.offer_id

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OfferBundle success add"}
        except Exception, e:
            db.session.rollback()
            #db.session.delete(self)
            #db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OfferBundle success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['description'] = self.description
        rv['offer_id'] = self.offer_id
        return rv


class OfferPlatform(models.TimestampMixin, db.Model):
    __tablename__ = 'core_platforms_offers'

    id = db.Column(db.Integer, primary_key=True)

    offer_id = db.Column(db.Integer, db.ForeignKey('core_offers.id'), nullable=False)
    offer = relationship('Offer', backref=db.backref("offer_platforms", lazy='dynamic'))

    platform_id = db.Column(db.Integer, db.ForeignKey('core_platforms.id'), nullable=False)
    platform = relationship("Platform", backref=db.backref("offer_platforms", lazy='dynamic'))

    tier_id = db.Column(db.Integer)
    tier_code = db.Column(db.String(150))

    currency = db.Column(db.String(10))

    price_usd = db.Column(db.Numeric(10, 2))
    price_idr = db.Column(db.Numeric(10, 2))
    price_point = db.Column(db.Integer)

    #discount data
    discount_id = db.Column(db.Integer, db.ForeignKey('core_discounts.id'))
    discount_tier_id = db.Column(db.Integer)
    discount_tier_code = db.Column(db.String(150))
    discount_tag = db.Column(db.String(150))
    discount_name = db.Column(db.String(150))
    discount_tier_price = db.Column(db.Numeric(10, 2))

    discount_price_usd = db.Column(db.Numeric(10, 2))
    discount_price_idr = db.Column(db.Numeric(10, 2))
    discount_price_point = db.Column(db.Integer)
    colors = db.Column(JSON(db.Text), nullable=True)

    def __repr__(self):
        return '<offer_id : %s platform_id: %s >' % (self.offer_id, self.platform_id)

    def get_offer_type(self):
        offer = Offer.query.filter_by(id=self.offer_id).first()
        return offer.offer_type_id

    def get_offer(self):
        offer = Offer.query.filter_by(id=self.offer_id).first()
        return offer

    def get_item_objects(self):
        offer = Offer.query.filter_by(id=self.offer_id).first()
        if offer:
            items = offer.items
            if items:
                return [{'id': i.id,
                         'name': i.name,
                         'item_type_id': item_types.enum_to_id(i.item_type)}
                        for i in items]
        return []

    def get_tier_currency(self):
        currency = 0
        if self.platform_id == PlatformType.ios:
            data = TierIos.query.filter_by(id=self.tier_id).first()
        if self.platform_id == PlatformType.android:
            data = TierAndroid.query.filter_by(id=self.tier_id).first()
        if self.platform_id == PlatformType.windows_phone:
            data = TierWp.query.filter_by(id=self.tier_id).first()

        if data:
            currency = data.get_currency_iso()
        return currency

    def get_tier_type(self):
        tier_type = 0
        data = None
        if self.platform_id == PlatformType.ios:
            data = TierIos.query.filter_by(id=self.tier_id).first()
        if self.platform_id == PlatformType.android:
            data = TierAndroid.query.filter_by(id=self.tier_id).first()
        if self.platform_id == PlatformType.windows_phone:
            data = TierWp.query.filter_by(id=self.tier_id).first()

        if data:
            tier_type = data.tier_type
        return tier_type

    def get_tier_code(self):
        tier_code = ''
        data = None
        if self.platform_id == PlatformType.ios:
            data = TierIos.query.filter_by(id=self.tier_id).first()
        if self.platform_id == PlatformType.android:
            data = TierAndroid.query.filter_by(id=self.tier_id).first()
        if self.platform_id == PlatformType.windows_phone:
            data = TierWp.query.filter_by(id=self.tier_id).first()

        if data:
            tier_code = data.tier_code
        return tier_code

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OfferPrice success add"}
        except Exception, e:
            db.session.rollback()
            #db.session.delete(self)
            #db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OfferPrice success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['offer_id'] = self.offer_id
        rv['tier_id'] = self.tier_id
        rv['tier_code'] = self.tier_code
        rv['platform_id'] = self.platform_id
        rv['currency'] = self.currency

        if self.discount_tier_code:
            rv['discount_tier_code'] = self.discount_tier_code
        if self.discount_tier_id:
            rv['discount_tier_id'] = self.discount_tier_id
        if self.discount_tag:
            rv['discount_tag'] = self.discount_tag
        if self.discount_name:
            rv['discount_name'] = self.discount_name
        if self.discount_tier_price:
            rv['discount_tier_price'] = '%.2f' % self.discount_tier_price
        if self.price_usd:
            rv['price_usd'] = '%.2f' % self.price_usd
        if self.price_idr:
            rv['price_idr'] = '%.2f' % self.price_idr
        if self.price_point:
            rv['price_point'] = self.price_point

        if self.discount_price_usd:
            rv['discount_price_usd'] = '%.2f' % self.discount_price_usd
        if self.discount_price_idr:
            rv['discount_price_idr'] = '%.2f' % self.discount_price_idr
        if self.discount_price_point:
            rv['discount_price_point'] = self.discount_price_point

        return rv


class OfferBuffet(models.TimestampMixin, db.Model):
    """ A subscription model which the user may purchase a time-limited 'buffet'
    which will allow them to download and read items that are part of 'buffet'.
    Once the buffet expires, the user will lose access to those items.
    """
    available_from = Column(DateTime)
    available_until = Column(DateTime)
    buffet_duration = Column(INTERVAL, nullable=False)
    offer_id = Column(Integer, ForeignKey('core_offers.id'))
    offer = relationship('Offer', backref='buffet')

    def get_items(self):
        items = []
        if self.offer:
            items = self.offer.get_items()
        return items

    def get_brands_id(self):
        brands = []
        if self.offer:
            brands = self.offer.get_brands_id()
        return brands

event.listen(
    OfferSingle,
    'after_insert',
    publish_to_elevenia
)
