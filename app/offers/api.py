import httplib, os, json

from flask import Response, request, g
from flask_appsfoundry import Resource, controllers, marshal
from flask_appsfoundry.exceptions import Conflict
from flask_appsfoundry.parsers import reqparse
from flask_appsfoundry.viewmodels import SaViewmodelBase

from app.auth.decorators import token_required
from app.helpers import conflict_message, internal_server_message, portal_metadata_changes
from app.items.choices import ITEM_TYPES, ITEM_TYPES_SHIMS
from app.offers.choices.offer_type import SINGLE, SUBSCRIPTIONS, BUNDLE, BUFFET
from app.utils.controllers import CorApiErrorHandlingMixin
from . import models, viewmodels, parsers, serializers, helpers
from .custom_get import OfferCustomGet
from .helpers import ItemConstruct
from .models import Offer

from app import db, app

class OfferTypeListApi(controllers.ListCreateApiResource):
    query_set = models.OfferType.query

    create_request_parser = parsers.OfferTypeArgsParser

    list_request_parser = parsers.OfferTypeListArgsParser

    serialized_list_name = u'offer_types'

    response_serializer = serializers.OfferTypeSerializer

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_global_offer_type',
                           )

    post_security_tokens = ('can_read_write_global_all',
                            'can_read_write_global_offer_type',
                            )


class OfferTypeApi(controllers.DetailApiResource):
    query_set = models.OfferType.query

    update_request_parser = parsers.OfferTypeArgsParser

    response_serializer = serializers.OfferTypeSerializer

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_global_offer_type',
                           )

    put_security_tokens = ('can_read_write_global_all',
                           'can_read_write_global_offer_type',
                           )

    delete_security_tokens = ('can_read_write_global_all',
                              )


class OfferListApi(Resource):
    ''' Shows a list of Offer and do POST / GET '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('name', type=str, required=True, location='json')
        self.reqparser.add_argument('offer_status', type=int, required=True, location='json')
        self.reqparser.add_argument('sort_priority', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')
        self.reqparser.add_argument('offer_type_id', type=int, required=True, location='json')

        # Not required cos, subscriptions doesn't have item id
        # self.reqparser.add_argument('items', type=list, required=True, location='json')
        self.reqparser.add_argument('items', type=list, location='json')
        self.reqparser.add_argument('long_name', location='json')

        self.reqparser.add_argument('brands', type=list, location='json')
        self.reqparser.add_argument('platforms_offers', type=list, required=True, location='json')

        self.reqparser.add_argument('price_usd', type=str, required=True, location='json')
        self.reqparser.add_argument('price_idr', type=str, required=True, location='json')
        self.reqparser.add_argument('price_point', type=int, required=True, location='json')

        self.reqparser.add_argument('vendor_price_usd', type=str, location='json')
        self.reqparser.add_argument('vendor_price_idr', type=str, location='json')
        self.reqparser.add_argument('vendor_price_point', type=int, location='json')

        # optional parameters
        self.reqparser.add_argument('exclusive_clients', type=list, location='json')
        self.reqparser.add_argument('description', type=str, location='json')

        self.reqparser.add_argument('is_free', type=bool, location='json')
        self.reqparser.add_argument('offer_code', type=str, location='json')
        self.reqparser.add_argument('item_code', type=str, location='json')

        # optional for subscriptions
        self.reqparser.add_argument('quantity', type=int, location='json')
        self.reqparser.add_argument('quantity_unit', type=int, location='json')
        self.reqparser.add_argument('allow_backward', type=bool, location='json')

        self.reqparser.add_argument('backward_quantity', type=int, location='json')
        self.reqparser.add_argument('backward_quantity_unit', type=int, location='json')

        self.reqparser.add_argument('discount_price_usd', type=str, location='json')
        self.reqparser.add_argument('discount_price_idr', type=str, location='json')
        self.reqparser.add_argument('discount_price_point', type=int, location='json')

        self.reqparser.add_argument('image_highres', type=str, location='json')
        self.reqparser.add_argument('image_normal', type=str, location='json')

        super(OfferListApi, self).__init__()

    @token_required(
        'can_read_write_global_all, can_read_write_public_ext, can_read_write_public, can_read_global_offer')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = OfferCustomGet(param, 'offers').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="offer", method="POST",
                                           error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_write_global_offer')
    def post(self):
        try:
            args = self.reqparser.parse_args()
            scene = ItemConstruct(args, 'POST', '').construct_items()

            if scene.status_code in (httplib.CREATED, httplib.OK):
                portal_metadata_changes(user_id=g.current_user.id, data_id=json.loads(scene.get_data()).get('id', 0),
                                        data_url=request.base_url, data_method=request.method, data_request=args)

            return scene

        except Exception, e:
            return internal_server_message(modul="offer", method="POST",
                                           error=e, req="")


class OfferApi(Resource):
    ''' GET/PUT/DELETE Offer '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('name', type=str, required=True, location='json')
        self.reqparser.add_argument('offer_status', type=int, required=True, location='json')
        self.reqparser.add_argument('sort_priority', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')
        self.reqparser.add_argument('offer_type_id', type=int, required=True, location='json')

        # Not required cos, subscriptions doesn't have item id
        # self.reqparser.add_argument('items', type=list, required=True, location='json')
        self.reqparser.add_argument('items', type=list, location='json')
        self.reqparser.add_argument('long_name', type=str, location='json')

        self.reqparser.add_argument('brands', type=list, location='json')
        self.reqparser.add_argument('platforms_offers', type=list, required=True, location='json')

        self.reqparser.add_argument('price_usd', type=str, required=True, location='json')
        self.reqparser.add_argument('price_idr', type=str, required=True, location='json')
        self.reqparser.add_argument('price_point', type=int, required=True, location='json')

        self.reqparser.add_argument('vendor_price_usd', type=str, location='json')
        self.reqparser.add_argument('vendor_price_idr', type=str, location='json')
        self.reqparser.add_argument('vendor_price_point', type=int, location='json')

        # optional parameters
        self.reqparser.add_argument('exclusive_clients', type=list, location='json')
        self.reqparser.add_argument('description', type=str, location='json')

        self.reqparser.add_argument('is_free', type=bool, location='json')
        self.reqparser.add_argument('offer_code', type=str, location='json')
        self.reqparser.add_argument('item_code', type=str, location='json')

        # optional for subscriptions
        self.reqparser.add_argument('quantity', type=int, location='json')
        self.reqparser.add_argument('quantity_unit', type=int, location='json')
        self.reqparser.add_argument('allow_backward', type=bool, location='json')

        self.reqparser.add_argument('backward_quantity', type=int, location='json')
        self.reqparser.add_argument('backward_quantity_unit', type=int, location='json')

        self.reqparser.add_argument('discount_price_usd', type=str, location='json')
        self.reqparser.add_argument('discount_price_idr', type=str, location='json')
        self.reqparser.add_argument('discount_price_point', type=int, location='json')

        self.reqparser.add_argument('image_highres', type=str, location='json')
        self.reqparser.add_argument('image_normal', type=str, location='json')

        super(OfferApi, self).__init__()

    @token_required(
        'can_read_write_global_all, can_read_write_public_ext, can_read_write_public, can_read_global_offer')
    def get(self, offer_id=None):
        try:
            param = request.args.to_dict()
            param['offer_detail'] = offer_id
            scene = OfferCustomGet(param, 'OFFER_DETAILS').construct()
            return scene

        except Exception, e:
            return internal_server_message(modul="offer", method="POST",
                                           error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_write_global_offer')
    def put(self, offer_id=None):
        try:
            args = self.reqparser.parse_args()
            exist_offer = Offer.query.filter_by(id=offer_id).first()
            if not exist_offer:
                return Response(status=httplib.NOT_FOUND)
            else:
                if args['name'] != exist_offer.name:
                    offer_type, conflict_offer = args['offer_type_id'], None

                    if offer_type in (SINGLE, BUNDLE):
                        conflict_offer = Offer.query.filter_by(name=args['name']).first()
                    elif offer_type in (SUBSCRIPTIONS, BUFFET):
                        conflict_offer = Offer.query.filter_by(long_name=args['long_name']).first()

                    if conflict_offer:
                        return conflict_message(modul="item", method="PUT", conflict=args['name'], req=args)

                # construct items
                scene = ItemConstruct(args, 'PUT', exist_offer).construct_items()

                if scene.status_code in (httplib.CREATED, httplib.OK):
                    portal_metadata_changes(user_id=g.current_user.id, data_id=offer_id,
                                            data_url=request.base_url, data_method=request.method, data_request=args)

                return scene

        except Exception, e:
            return internal_server_message(modul="offer", method="PUT",
                                           error=e, req="")

    @token_required('can_read_write_global_all')
    def delete(self, offer_id=None):
        f = Offer.query.filter_by(id=offer_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=httplib.NOT_FOUND)


class TypedOfferListApi(controllers.ListCreateApiResource):

    @property
    def query_set(self):
        return TypedOfferListApi.query_set_factory(self.set_id)

    @staticmethod
    def query_set_factory(set_id):
        return {
            'singles': models.OfferSingle.query,
            'subscriptions': models.OfferSubscription.query,
            'bundles': models.OfferBundle.query,
            'buffets': models.OfferBuffet.query,
            None: models.Offer.query,
        }[set_id]

    @property
    def serialized_list_name(self):
        return TypedOfferListApi.serialized_list_name_factory(self.set_id)

    @staticmethod
    def serialized_list_name_factory(set_id):
        return {
            'singles': 'singles',
            'subscriptions': 'subscriptions',
            'bundles': 'bundles',
            'buffets': 'buffets',
            None: 'offers',
        }[set_id]

    @property
    def viewmodel(self):
        return TypedOfferListApi.viewmodel_factory(self.set_id)

    @staticmethod
    def viewmodel_factory(set_id):
        return {
            'singles': SaViewmodelBase,
            'subscriptions': SaViewmodelBase,
            'bundles': SaViewmodelBase,
            'buffets': viewmodels.OfferBuffetViewmodel,
        }[set_id]

    @property
    def list_request_parser(self):
        return TypedOfferListApi.list_request_parser_factory(self.set_id)

    @staticmethod
    def list_request_parser_factory(set_id):
        return {
            'singles': None,
            'subscriptions': None,
            'bundles': None,
            'buffets': parsers.OfferBuffetListArgsParser,
        }[set_id]

    @property
    def response_serializer(self):
        return TypedOfferListApi.response_serializer_factory(self.set_id)

    @staticmethod
    def response_serializer_factory(set_id):
        return {
            'singles': None,
            'subscriptions': None,
            'bundles': None,
            'buffets': serializers.BuffetSerializer,
        }[set_id]

    def __init__(self):
        self.set_id = None

    def get(self, set_id=None):
        self.set_id = set_id
        return super(TypedOfferListApi, self).get()

    def post(self, set_id=None):
        self.set_id = set_id
        return super(TypedOfferListApi, self).post()


class OfferBuffetListApi(controllers.ListCreateApiResource):
    query_set = models.OfferBuffet.query

    create_request_parser = parsers.OfferBuffetArgsParser

    viewmodel = viewmodels.OfferBuffetViewmodel

    list_request_parser = parsers.OfferBuffetListArgsParser

    response_serializer = serializers.BuffetSerializer

    serialized_list_name = u'buffets'

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext',
                           )

    post_security_tokens = ('can_read_write_global_all',
                            )

    on_create_skip_deserialization = ('offer',
                                      )

    _before_insert = helpers.deserialize_buffet_related_fields


class OfferBuffetDetailApi(controllers.DetailApiResource):
    query_set = models.OfferBuffet.query

    update_request_parser = parsers.OfferBuffetArgsParser

    viewmodel = viewmodels.OfferBuffetViewmodel

    response_serializer = serializers.BuffetSerializer

    on_update_skip_deserialization = ('offer',
                                      )

    _before_update = helpers.deserialize_buffet_related_fields

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext',
                           )

    put_security_tokens = ('can_read_write_global_all',
                           )

    delete_security_tokens = ('can_read_write_global_all',
                              )


class OfferSingleItemPreviewsApi(CorApiErrorHandlingMixin, controllers.DetailApiResource):
    """
    API to get a list of urls item preview for a single offers based on offer id or remote offer id.
    Used by remote system (like gramedia.com).

    usage example for offer id 1234
        v1/offers/1234/previews
      or (by remote offer id registered in remote system):
        v1/offers/SC00P1234/previews

    urls for item preview generated in view model OfferItemPreviewsViewmodel
        using the same method PreviewUrlBuilder that used in item API

    End to end test for this api is in file single_offer_item_preview_tests.py

    """
    query_set = models.Offer.query

    response_serializer = serializers.OfferItemPreviewsSerializer

    viewmodel = viewmodels.OfferItemPreviewsViewmodel

    @token_required('can_read_write_global_all, '
                    'can_read_write_public, '
                    'can_read_write_public_ext')
    def get(self, db_id):
        if "SC00P" in db_id:
            db_id = db_id.replace("SC00P", "")

        model = self.query_set.get_or_404(int(db_id))

        if model.offer_type_id != SINGLE:
            raise Conflict(user_message="Preview only available for single offer",
                           developer_message="Preview only available for single offer")

        return marshal(self._get_viewmodel(model),
                       self._get_serializer())


class OfferItemApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_write_public')
    def get(self):

        '''
            retrieve all items related with offers and filter
            for ebooks premium page and eperpus portals. and new mobile pages.
        '''

        param, total_count = request.args.to_dict(), 0
        limit, offset, offer_id = param.get('limit', 20), param.get('offset', 0), param.get('offer_id', 0)
        item_type = param.get('item_type', None)

        exist_offers = Offer.query.filter_by(id=offer_id, offer_status=Offer.Status.ready_for_sale.value).first()
        if not exist_offers:
            return Response(status=httplib.NOT_FOUND)

        if item_type:
            where = """
                co.id = {offer_id} and
                cb.default_item_type = '{item_type}'
            """.format(offer_id=offer_id, item_type=ITEM_TYPES[int(item_type)])
        else:
            where = """co.id = {offer_id}""".format(offer_id=offer_id)

        if exist_offers.offer_type_id in [SUBSCRIPTIONS, BUNDLE, BUFFET]:
            total_count = int(exist_offers.brands.count())
            raw_sql = """
                select 
                        ci.id, ci.name, ci.slug, ci.thumb_image_highres, ci.issue_number, ci.item_type, 
                        cb.id, cb.name
                    from
                        core_offers co
                        join core_offers_brands cob on cob.offer_id = co.id
                        join core_brands cb on cb.id = cob.brand_id
                        join core_items ci on ci.brand_id = cob.brand_id
                    where
                        {where}
                    order by ci.release_date, ci.brand_id desc
                    limit {limit}
                    offset {offset}
            """.format(limit=limit, offset=offset, where=where)
        else:
            pass

        data = db.engine.execute(raw_sql)
        response_data = [
            {'id': idata[0],
             'name': idata[1],
             'slug': idata[2],
             'images': os.path.join(app.config['AWS_PUBLIC'], app.config['BOTO3_GRAMEDIA_COVERS_BUCKET'],
                                    idata[3].replace('images/1/', '') if idata[3] else ''),
             'issue_number': idata[4],
             'item_type': {
                    'id': ITEM_TYPES_SHIMS[idata[5]],
                    'name': idata[5],
                    'href': None},
             'brands': {'id': idata[6], 'name': idata[7]}} for idata in data.fetchall()]

        metadata, resultset = {}, {}
        resultset['count'] = total_count
        resultset['limit'] = int(limit) if limit else 0
        resultset['offset'] = offset
        metadata['resultset'] = resultset

        return Response(json.dumps({'data': response_data, 'metadata': metadata}), status=httplib.OK,
                        mimetype='application/json')
