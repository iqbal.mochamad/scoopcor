from httplib import NO_CONTENT

from app.items.models import Item
from app.services.elevenia.entities.choices.result_status import OK
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory


class OfferSearchTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(OfferSearchTests, cls).setUpClass()
        for i in range(1, 11):
            OfferFactory(long_name='Kompas Waktu {}'.format(i), is_active=True, offer_status=7)
            OfferFactory(long_name='Wrong {}'.format(i), is_active=True, offer_status=7)
        item1 = ItemFactory(item_type=Item.Types.newspaper.value)
        offer1 = OfferFactory(long_name='Kompas Jun 2017', is_active=True, offer_status=7)
        offer1.items.append(item1)
        item2 = ItemFactory(item_type=Item.Types.newspaper.value)
        offer2 = OfferFactory(long_name='Kompas Jul 2017', is_active=True, offer_status=7)
        offer2.items.append(item2)
        cls.session.commit()
        cls.headers = cls.build_headers().copy()
        cls.headers.pop('Accept', None)

    def test_search_no_content(self):
        resp = self.api_client.get(
            '/v1/offers?is_active=true&offer_status=7&q=bin', headers=self.headers)
        self.assert_status_code(resp, NO_CONTENT)

    def test_search(self):
        # https://scoopadm.apps-foundry.com/scoopcor/api/v1/offers?is_active=true&offer_status=7&q=bin
        resp = self.api_client.get(
            '/v1/offers?is_active=true&offer_status=7&q=kompas', headers=self.headers)
        self.assert_status_code(resp, OK)
        result = self._get_response_json(resp)
        actual = result.get('offers', [])
        self.assertIsNotNone(actual)
        self.assertEqual(12, len(actual))

    def test_search_with_item_type(self):
        # search with keyword + item type
        # for CMS Portal Layout maintenance
        resp = self.api_client.get(
            '/v1/offers?is_active=true&offer_status=7&q=kompas&item_type=newspaper', headers=self.headers)
        self.assert_status_code(resp, OK)
        result = self._get_response_json(resp)
        actual = result.get('offers', [])
        self.assertIsNotNone(actual)
        self.assertEqual(2, len(actual))

