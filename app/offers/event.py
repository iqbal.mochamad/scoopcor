# import datetime
# from requests.exceptions import BaseHTTPError
#
# from flask import current_app
#
# from app.offers.models import Offer
# from app.remote_publishing.choices import ELEVENIA
# from app.remote_publishing.models import RemoteOffer
# from app.services.elevenia.entities.exceptions import ApiOperationFailure
# from scripts.elevenia_items_publish import SINGLE_OFFER
#
# from app.services.elevenia import Elevenia, Product, DeliveryTemplate
# from app.services.elevenia.entities.choices.sale_types import READY_STOCK
# from app.services.elevenia.entities.choices.item_conditions import NEW
# from app.services.elevenia.entities.choices.tax_codes import NOT_TAXED

def publish_to_elevenia(mapper, connection, target):
    pass
    #
    # gateway = Elevenia(current_app.config["ELEVENIA_API_KEY"])
    # delivery_template = get_delivery_template(gateway)
    #
    # try:
    #
    #
    #         # first time we're uploading this offer to elevenia
    #     elevenia_product = build_elevenia_product_from_scoop_offer(
    #         target.offer, delivery_template, gateway
    #     )
    #
    #     elevenia_product.save()
    #
    #     record_remote_publishing(target.offer, elevenia_product)
    #
    # except (ApiOperationFailure) as e:
    #     pass
    # except (NoMappableCategoriesError, BaseHTTPError, ValueError, Exception) as e:
    #     pass


def update_elevenia(mapper, connection, target):
    pass


def get_delivery_template(gateway):
    # """ Gets the delivery template that will be used for items.
    #
    # Since we aren't actually delivering products, we're just going to grab the
    # first delivery template and assume it's the only one.
    #
    # :param `app.services.elevenia.gateway.Elevenia` gateway: The gateway object
    #     that should be used to communicate with the Elevenia API.
    # :return: The single delivery template that should be used with all items.
    # :rtype: `app.services.elevenia.entities.delivery_template.DeliveryTemplate`
    # """
    # delivery_templates = gateway.resource(DeliveryTemplate).list()
    # if len(delivery_templates):
    #     return delivery_templates[0]
    # else:
    #     raise ValueError("No delivery templates uploaded yet!")
    pass

def get_is_sellable_to_minors(item):
    pass
    # """ Returns True if this item is avaialble for sale to anyone under 17
    # years of age.
    #
    # :param `app.items.model.Item` item: The item to check.
    # :return: Boolean indication of whether or not this items is sellable
    #     to anyone under 17 years of age.
    # """
    # if item.parentalcontrol is None:
    #     return True
    # else:
    #     # This is VERY hacky.  It would be cool if parental controls grew
    #     # a minimum age int field, or this should be an enum.
    #     return item.parentalcontrol.name != "17+"


def get_elevenia_category_id(item):
    pass
    # """ Iterates a given Item's categories, and returns the first category
    # that is found to have a remote mapping.
    #
    # :param `app.items.models.Item` item: The item to search for valid
    #     remote Elevenia category mappings.
    # :return: The first valid remote Elevenia category.
    # :rtype: `app.remote_publishing.models.RemoteCategory`
    # :raises `~NoMappableCategoriesError`: If no valid remote categories could
    #     be found.
    # """
    # cat_list = [item.brand.primary_category,] if item.brand.primary_category else item.categories.all()
    #
    #
    # for category in cat_list:
    #     remote_category = category.remote_categories\
    #         .filter(RemoteCategory.remote_service == ELEVENIA)\
    #         .first()
    #
    #     if remote_category:
    #         # we store these as string values in the database, because we don't
    #         # care about their actual type in order to be as flexible as
    #         # possible to future remote stores.
    #         return int(remote_category.remote_id)
    #
    # raise NoMappableCategoriesError(
    #     u"""Item ID: {} does not have any known categories that map to
    #     valid Elevenia categories.  Cannot post to Elevenia""".format(item.id))


def build_elevenia_product_from_scoop_offer(offer, delivery_template, gateway):
    pass
    # item = offer.items.first()
    # if offer.offer_type_id == SINGLE_OFFER:
    #     name = u"[SCOOP Digital] {} by {}".format(item.name,
    #                                               item.authors.first().name)
    #     nickname = offer.items.first().name
    #     scoop_id = u"SC00P{}".format(offer.id)
    #     sellable_to_minors = get_is_sellable_to_minors(item)
    #     description = item.description or " "
    #     image_highres = item.image_highres
    #     price = int(offer.price_idr)
    #     category = get_elevenia_category_id(item)
    #     if not all([price, image_highres, category]):
    #         raise ValueError("Offer {0.id} is missing a required field"
    #             ":\nprice={0.price_idr}\nimage_highres={1.image_highres}\ncategory={2}".format(offer, item, category))
    # else:
    #     raise ValueError("Only single offers are currently supported.")
    #
    # # create and publish the new item to elevenia
    # p = gateway.resource(Product).new(
    #     name=name,
    #     nickname=nickname,
    #     scoop_item_id=scoop_id,
    #     sale_type=READY_STOCK,
    #     weight_kilograms=1,  # BS required field
    #     quantity_in_stock=99999,  # BS required field..
    #     item_condition=NEW,
    #     vat_tax_code=NOT_TAXED,
    #     is_sellable_to_minors=sellable_to_minors,
    #     uses_sales_period=False,
    #     after_service_details="Contact person for scoop: "
    #                           "customercare@gramedia.com",
    #     return_or_exchange_details="Refunds/exchanges are not permitted for "
    #                                "digital content.",
    #     has_delivery_guarantee=True,
    #     primary_image=u"{base}{img}".format(base=current_app.config['MEDIA_BASE_URL'],
    #                                        img=image_highres),
    #     html_detail=u"<img src='http://ebooks.gramedia.com/ext/img/sites/promo/elevenia-purchase-process.jpg' alt='To download your purchase, please sign into the scoop android or iphone app' />",
    #
    #     delivery_template_id=delivery_template.id,
    #     display_category_id=category,
    #
    #     price=price
    # )
    #
    # return p


def record_remote_publishing(scoop_offer, elevenia_product, remote_offer=None):
    # if not remote_offer:
    #     scoop_offer.remote_offers.append(RemoteOffer(
    #         remote_service=ELEVENIA,
    #         remote_id=elevenia_product.id,
    #         last_checked=datetime.utcnow(),
    #         last_posted=datetime.utcnow()
    #     ))
    # else:
    #     remote_offer.last_checked=datetime.utcnow()
    #     remote_offer.last_posted=datetime.utcnow()
    #
    # Offer.query.session.commit()
    pass


class NoMappableCategoriesError(Exception):
    pass
