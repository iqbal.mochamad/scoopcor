import httplib

from flask_restful import Resource
from flask import Response

from .models import Offer
from .choices.status_type import READY_FOR_SALE, NOT_FOR_SALE


class MaydayOfferActivate(Resource):

    # TO DO : delete this function
    # This for activate offers ( PROMO MAYDAY ) 1 april 10:00
    # why? frontend still not used publish until offers

    def get(self):
        offer_ids = [41949, 41948, 41947, 41946, 41945, 41944, 41943, 41942,
            41941, 41940, 41939, 41938, 41937, 41936, 41935, 41934, 41933,
            41932, 41931, 41930, 41927, 41926, 41925, 41924, 41923, 41922,
            41921, 41920, 41919, 41918, 41917, 41916, 41915, 41914, 41913,
            41912, 41911, 41910, 41909, 41908, 41907, 41906, 41905, 41903,
            41902, 41901, 41900, 41899, 41898, 41897, 41896, 41895, 41894,
            41893, 41892, 41891, 41890, 41889, 41888, 41887, 41886, 41885,
            41884, 41882, 41881]
        for ioffer in offer_ids:
            offer = Offer.query.filter_by(id=ioffer).first()
            if offer:
                offer.is_active = True
                offer.offer_status = READY_FOR_SALE
                offer.save()
        return Response(None, status=httplib.OK, mimetype='application/json')


class MaydayOfferInactivate(Resource):

    # TO DO : delete this function
    # This for inactivate offers ( PROMO MAYDAY ) 7 april 23:59 PM
    # why? frontend still not used publish until offers

    def get(self):
        offer_ids = [41949, 41948, 41947, 41946, 41945, 41944, 41943, 41942,
            41941, 41940, 41939, 41938, 41937, 41936, 41935, 41934, 41933,
            41932, 41931, 41930, 41927, 41926, 41925, 41924, 41923, 41922,
            41921, 41920, 41919, 41918, 41917, 41916, 41915, 41914, 41913,
            41912, 41911, 41910, 41909, 41908, 41907, 41906, 41905, 41903,
            41902, 41901, 41900, 41899, 41898, 41897, 41896, 41895, 41894,
            41893, 41892, 41891, 41890, 41889, 41888, 41887, 41886, 41885,
            41884, 41882, 41881]

        for ioffer in offer_ids:
            offer = Offer.query.filter_by(id=ioffer).first()
            if offer:
                offer.is_active = False
                offer.offer_status = NOT_FOR_SALE
                offer.save()
        return Response(None, status=httplib.OK, mimetype='application/json')
