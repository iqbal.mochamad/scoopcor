import json
import httplib
from sqlalchemy.sql import desc

from flask import Response
from flask_appsfoundry.parsers import converters

from app.items.models import Item
from app.master.models import Brand
from app.offers.choices.status_type import READY_FOR_SALE
from app.offers.models import Offer


class OfferCustomGet():

    def __init__(self, param=None, module=None):
        self.param = param
        self.module = module

        self.q = param.get('q', None)
        self.limit = param.get('limit', 20)
        self.offset = param.get('offset', 0)
        self.fields = param.get('fields', None)
        self.related_item_id = param.get('related_item_id', None)
        self.related_brand_id = param.get('related_brand_id', None)
        self.offer_type_id = param.get('offer_type_id', None)
        self.offer_ids = param.get('offer_id', None)
        self.offer_detail = param.get('offer_detail', None)

        self.order = param.get('order', None)
        self.offer_status = param.get('offer_status', None)
        self.platform_id = param.get('platform_id', None)
        self.extend = param.get('expand', None)
        self.price = param.get('price', None)

        self.is_active = param.get('is_active', False)

        self.master = ''
        self.total = 0

    def json_success(self, data_items=None, offset=None, limit=None):

        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            if self.platform_id:
                platform = self.platform_id.split(",")
            else:
                platform = [4]

            if self.fields and self.extend:
                extends = self.extend.split(',')
                cur_values = [u.extension_values(extension=extends, custom=self.fields, platform_id=platform) for u in data_items]

            elif self.extend:
                extends = self.extend.split(',')
                cur_values = [u.extension_values(extension=extends, platform_id=platform) for u in data_items]

            elif self.fields:
                cur_values = [u.custom_values(self.fields) for u in data_items]
            else:
                cur_values = [u.values() for u in data_items]

            if self.related_brand_id:
                tmp = []
                for u in data_items:
                    x = u.values()
                    x['brands'] = u.get_brands(self.related_brand_id)
                    tmp.append(x)
                cur_values = tmp

            if self.related_item_id:
                tmp = []
                for u in data_items:
                    x = u.values()
                    x['items'] = u.get_items()
                    tmp.append(x)
                cur_values = tmp

            temp={}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            resultset = {'resultset': temp}

            if self.module == 'OFFER_DETAILS':
                values = cur_values[0]
                rv = json.dumps(values)
            else:
                rv = json.dumps({'offers': cur_values, "metadata": resultset})

            return Response(rv, status=httplib.OK, mimetype='application/json')

    def offer_master(self):
        self.total = Offer.query.count()

        # dont change this order_by
        # front end use first array as their display price!
        if self.offer_type_id == '3':
            return Offer.query.filter_by(offer_status=READY_FOR_SALE).order_by(desc(Offer.id))
        else:
            return Offer.query.filter_by(offer_status=READY_FOR_SALE).order_by(Offer.price_usd)

    def construct(self):
        self.master = self.offer_master()

        if self.order:
            order_data = self.order.split(',')
            for orderx in order_data:
                if orderx[0] == '-': #descending
                    order_param = '%s %s' % (orderx[1:], 'desc')
                    self.master = self.master.order_by(order_param)
                else:
                    self.master = self.master.order_by(orderx)

        if self.q:
            self.q = "%s%s%s" % ('%', self.q, '%')
            self.master = self.master.filter(Offer.long_name.ilike(self.q))

        if self.offer_ids:
            offers = self.offer_ids.split(',')
            self.master = self.master.filter(Offer.id.in_(offers))

        if self.offer_type_id:
            self.master = self.master.filter(Offer.offer_type_id == self.offer_type_id)

        if self.is_active:
            self.master = self.master.filter_by(is_active=self.is_active)

        if self.offer_status:
            self.master = self.master.filter(Offer.offer_status == self.offer_status)

        if self.offer_detail:
            self.master = self.master.filter_by(id=self.offer_detail)

        if self.price:
            stat = str(self.price[:1])
            price = float(self.price[1:])

            if stat == '-':
                self.master = self.master.filter(Offer.price_usd <= price)
            else:
                self.master = self.master.filter(Offer.price_usd >= price)

        if self.related_brand_id:
            related_brand_id = self.related_brand_id.split(',')
            self.master = self.master.join(Brand.core_offers).filter(Brand.id.in_(related_brand_id))

        item_type = self.param.get('item_type', None)
        if self.related_item_id or item_type:
            # self.master = self.master.join(Item.core_offers)
            self.master = self.master.join(Offer.items)
            if item_type:
                self.master = self.master.filter(Item.item_type == item_type)
            if self.related_item_id:
                related_item_id = self.related_item_id.split(',')
                self.master = self.master.filter(Item.id.in_(related_item_id))

        self.total = self.master.count()
        self.master = self.master.limit(self.limit).offset(self.offset).all()
        return self.json_success(self.master, self.offset, self.limit)
