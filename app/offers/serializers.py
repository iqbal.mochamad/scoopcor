from flask_appsfoundry.serializers import fields, SerializerBase


class DurationField(fields.Raw):

    def format(self, value):
        return "P{years}Y{months}M{days}DT{hours}H{minutes}M{seconds}S".format(**vars(value))


class PlatformOfferSerializer(SerializerBase):
    id = fields.Integer
    offer_id = fields.Integer
    platform_id = fields.Integer
    tier_id = fields.Integer
    tier_code = fields.String
    currency = fields.String
    price_usd = fields.Float
    price_idr = fields.Integer
    price_point = fields.Integer
    discount_id = fields.List(fields.String)
    discount_tier_id = fields.Integer
    discount_tier_code = fields.String
    discount_tag = fields.String
    discount_name = fields.String
    discount_tier_price = fields.Float
    discount_price_usd = fields.Float
    discount_price_idr = fields.Integer
    discount_price_point = fields.Integer


class OfferSerializer(SerializerBase):
    id = fields.Integer
    name = fields.String
    offer_status = fields.Integer
    sort_priority = fields.Integer
    is_active = fields.Boolean
    offer_type_id = fields.Integer
    exclusive_clients = fields.List(fields.String)
    is_free = fields.Boolean
    offer_code = fields.String
    item_code = fields.String
    price_usd = fields.Float
    price_idr = fields.Integer
    price_point = fields.Integer
    vendor_price_usd = fields.Float
    vendor_price_idr = fields.Integer
    vendor_price_point = fields.Integer
    discount_id = fields.List(fields.String)
    discount_tag = fields.String
    discount_name = fields.String
    discount_price_usd = fields.Float
    discount_price_idr = fields.Integer
    discount_price_point = fields.Integer
    is_discount = fields.Boolean
    image_normal = fields.String
    image_highres = fields.String
    brands = fields.List(fields.Integer, attribute='offer_brand_ids')
    items = fields.List(fields.Integer, attribute='offer_item_ids')
    platforms_offers = fields.List(fields.Nested(PlatformOfferSerializer()), attribute='offer_platforms_objects')


class BuffetSerializer(SerializerBase):
    id = fields.Integer
    available_from = fields.DateTime('iso8601')
    available_until = fields.DateTime('iso8601')
    buffet_duration = DurationField
    offer = fields.Nested(OfferSerializer())


class OfferTypeSerializer(SerializerBase):
    id = fields.Integer
    name = fields.String
    description = fields.String
    slug = fields.String
    meta = fields.String
    sort_priority = fields.Integer
    is_active = fields.Integer


class OfferItemPreviewsSerializer(SerializerBase):
    id = fields.Integer
    item_name = fields.String
    previews = fields.List(fields.String)
