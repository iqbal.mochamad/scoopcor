{% if not item_count %}
There's no active newspaper today, please immediately check server logs for the details.
{%  endif %}

Level : {{ level }}
Active newspaper count : {{ item_count }}
Release date : {{ today }}
