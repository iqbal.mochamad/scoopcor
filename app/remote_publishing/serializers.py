from flask_appsfoundry.serializers import SerializerBase, fields


class RemoteCategorySerializer(SerializerBase):
    id = fields.Integer()
    category_id = fields.Integer()
    remote_service = fields.String()
    remote_id = fields.String()
