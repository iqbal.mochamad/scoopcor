"""
Models
======

SQLAlchemy ORM model classes used for this package.

.. note::

    Currently this is only implemented for the Elevenia service.
    For detailed information, see `app.services.elevenia`

"""
from sqlalchemy import Column, Integer, ForeignKey, String, DateTime
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.orm import relationship, backref

from app import db

from .choices import REMOTE_DATA_LOCATIONS


class RemoteOffer(db.Model):
    """ Records posting of :class:`app.offers.models.Offer`s to a remote data store.

    This is necessary for remote checkout APIs that maintain their own
    product databases.  We need to record a 'mapping' of our offers to
    their identifiers for our offers.
    """
    __tablename__ = 'core_remote_offers'

    offer_id = Column(
        Integer,
        ForeignKey('core_offers.id'),
        primary_key=True,
        doc="Foreign key mapping to an Offer in COR.")
    offer = relationship('Offer', backref=backref('remote_offers'))

    remote_service = Column(
        ENUM(*REMOTE_DATA_LOCATIONS, name='remote_data_location'),
        primary_key=True,
        doc="Identifier for a remote service that this offer is valid for.  "
            "Valid choices: {}.".format(",".join(REMOTE_DATA_LOCATIONS)))

    remote_id = Column(
        String,
        nullable=False,
        doc="Some type of uniquely identifier from the remote service.  "
            "These may be numeric or otherwise, so we use strings (ie. these"
            "may require additional processing, such as if they're integers)")

    last_checked = Column(
        DateTime,
        doc="Date/time this offer was last retrieved from the remote service "
            "and checked if it was out of date with our internal data.")

    last_posted = Column(
        DateTime,
        doc="Date/time this offer was last posted to the remote service.")


class RemoteCategory(db.Model):
    """ Maps a local :class:`app.master.models.Category`'s id, to an identifier
    in another system.

    .. note::
        This is similar to a RemoteOffer but is a special use-case for Elevenia
        currently, because they also maintain their own internal product
        categories, which we must use when publishing our offers.
    """
    __tablename__ = 'core_remote_categories'

    category_id = Column(
        Integer,
        ForeignKey('core_categories.id'),
        primary_key=True,
        doc="Foreign Key mapping to a category in COR.")

    category = relationship('Category', backref=backref('remote_categories',
                                                        uselist=True,
                                                        lazy='dynamic'))
    remote_service = Column(
        ENUM(*REMOTE_DATA_LOCATIONS, name='remote_data_location'),
        primary_key=True,
        doc="Identifier for a remote service that this category mapping is "
            "valid for.  Valid choices: {}.".format(
                ",".join(REMOTE_DATA_LOCATIONS)))

    remote_id = Column(
        String,
        nullable=False,
        doc="Some unique identifier from the remote service.  For flexibility, "
            "we store this as a string, though it may represent other types "
            "(ie, an integer), and require additional processing for use.")
