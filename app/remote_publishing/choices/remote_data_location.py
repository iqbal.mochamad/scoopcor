""" Enum type definition for remote data locations.
"""
ELEVENIA = u'elevenia'
GRAMEDIA = u'gramedia'


REMOTE_DATA_LOCATIONS = (
    ELEVENIA,
    GRAMEDIA,
)

