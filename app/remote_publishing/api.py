"""
API
===

HTTP Api endpoints for this package.
"""
from flask_appsfoundry import controllers

from .models import RemoteCategory
from .parsers import RemoteCategoryArgsParser, RemoteCategoryListArgsParser
from .serializers import RemoteCategorySerializer


class RemoteCategoryListApi(controllers.ListCreateApiResource):
    """ List/Create `app.remote_publishing.models.RemoteCategory`
    """
    get_security_tokens = ('can_read_write_global_all', )
    post_security_tokens = ('can_read_write_global_all', )
    query_set = RemoteCategory.query
    list_request_parser = RemoteCategoryListArgsParser
    create_request_parser = RemoteCategoryArgsParser
    response_serializer = RemoteCategorySerializer
    serialized_list_name = 'remote_categories'


class RemoteCategoryDetailApi(controllers.DetailApiResource):
    """ Get/Update/Delete single `app.remote_publishing.models.RemoteCategory`
    """
    get_security_tokens = ('can_read_write_global_all', )
    put_security_tokens = ('can_read_write_global_all', )
    delete_security_tokens = ('can_read_write_global_all', )
    query_set = RemoteCategory.query
    update_request_parser = RemoteCategoryArgsParser
    response_serializer = RemoteCategorySerializer
