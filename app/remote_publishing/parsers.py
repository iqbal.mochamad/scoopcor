from flask_appsfoundry.parsers import ParserBase, IntegerField, StringField, \
    SqlAlchemyFilterParser, IntegerFilter, StringFilter

from .choices import REMOTE_DATA_LOCATIONS
from .models import RemoteCategory


class RemoteCategoryArgsParser(ParserBase):
    category_id = IntegerField(required=True)
    remote_service = StringField(required=True, choices=REMOTE_DATA_LOCATIONS)
    remote_id = StringField(required=True)


class RemoteCategoryListArgsParser(SqlAlchemyFilterParser):
    __model__ = RemoteCategory
    category_id = IntegerFilter()
    remote_service = StringFilter(choices=REMOTE_DATA_LOCATIONS)
    remote_id = StringFilter()
