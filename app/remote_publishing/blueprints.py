from flask import Blueprint
from flask_appsfoundry import Api

from .api import RemoteCategoryListApi, RemoteCategoryDetailApi

blueprint = Blueprint('remote_publishing', __name__, url_prefix='/v1/remote-publishing')

api = Api(blueprint)

api.add_resource(RemoteCategoryListApi, '/categories')
api.add_resource(RemoteCategoryDetailApi, '/categories/<int:db_id>')

