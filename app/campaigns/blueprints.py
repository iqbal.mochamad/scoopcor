from .api import CampaignListPostApi, CampaignApi, CampaignItemsApi
from flask import Blueprint


blueprint = Blueprint('campaigns', __name__, url_prefix='/v1/campaigns')

blueprint.add_url_rule('', view_func=CampaignListPostApi.as_view('list'))
blueprint.add_url_rule('/<int:entity_id>', view_func=CampaignApi.as_view('details'))
blueprint.add_url_rule('/<int:campaign_id>/items', view_func=CampaignItemsApi.as_view('items'))


