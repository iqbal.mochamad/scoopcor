from flask import url_for

from app import db

from flask_appsfoundry.models import TimestampMixin, SoftDeletableMixin
from sqlalchemy import String, DateTime, Numeric, Column

from app.utils.models import TimestampMixinWithTimezone


class Campaign(TimestampMixinWithTimezone, SoftDeletableMixin, db.Model):
    """ Marketing campaign
    """
    name = Column(String, nullable=False, unique=True)
    description = Column(String)
    start_date = Column(DateTime(timezone=True), nullable=False)
    end_date = Column(DateTime(timezone=True), nullable=False)
    total_cost = Column(Numeric(precision=13, scale=4))

    @property
    def api_url(self):
        return url_for('campaigns.details', entity_id=self.id, _external=True)

