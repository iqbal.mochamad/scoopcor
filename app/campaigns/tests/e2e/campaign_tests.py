from __future__ import unicode_literals, absolute_import

import httplib
import json
from datetime import timedelta

from marshmallow.utils import isoformat
from nose.tools import istest
from sqlalchemy import desc

from app.campaigns.models import Campaign
from app.items.models import Item
from app.utils.datetimes import get_local
from app.utils.testcases import CrudBase
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.discounts import CampaignFactory, DiscountFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory


@istest
class CampaignCRUDTests(CrudBase):

    request_url = '/v1/campaigns'
    fixture_factory = CampaignFactory
    model = Campaign
    list_key = 'campaigns'

    def get_request_body(self):
        request_body = {
            "name": self.fake.name(),
            "description": self.fake.bs(),
            'start_date': isoformat(get_local() - timedelta(days=2)),
            'end_date': isoformat(get_local() + timedelta(days=3)),
            'total_cost': self.fake.pyfloat(left_digits=8, right_digits=4),
            'is_active': self.fake.pybool(),
        }
        return request_body

    def get_expected_response(self, entity):
        expected = {
            "id": entity.id,
            "name": entity.name,
            "description": entity.description,
            'start_date': isoformat(entity.start_date),
            'end_date': isoformat(entity.end_date),
            'total_cost': float(entity.total_cost),
            'is_active': entity.is_active,
        }
        return expected

    def test_list_api_filter_active_only(self):
        # setup some more data
        self.set_up_list_fixtures()
        # setup some false data
        c1 = CampaignFactory(id=500001, is_active=False)
        # campaign in the future, not selected
        c2 = CampaignFactory(
            id=500002, is_active=True, start_date=get_local() + timedelta(days=2),
            end_date=get_local() + timedelta(days=12))
        # campaign already ended, not selected
        c3 = CampaignFactory(
            id=500003, is_active=True, start_date=get_local() - timedelta(days=12),
            end_date=get_local() - timedelta(days=2))
        self.session.add(c1)
        self.session.add(c2)
        self.session.add(c3)
        self.session.commit()

        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '{url}?is_active=1&limit=20&order=-id'.format(url=self.request_url),
            headers=self.build_headers())

        self.assert_status_code(response, httplib.OK)

        response_json = self._get_response_json(response)
        list_campaign = response_json.get(self.list_key, [])
        list_ids = [e.get('id') for e in list_campaign]
        self.assertNotIn(500001, list_ids)
        self.assertNotIn(500002, list_ids)
        self.assertNotIn(500003, list_ids)

        expected_entities = self.session.query(self.model).filter(
            Campaign.is_active == True,
            Campaign.start_date <= get_local(),
            Campaign.end_date >= get_local()
        ).order_by(desc(Campaign.id)).limit(20).all()
        self.assert_list_response(response_json, expected_entities)

    def test_list_api_with_filters(self):
        # setup some more data
        self.set_up_list_fixtures()
        c1 = CampaignFactory(id=500010, is_active=False)
        self.session.add(c1)
        self.session.commit()

        # test filter list by other on query string (based on filter parser definition)
        self.set_api_authorized_user(self.regular_user)
        response = self.api_client.get(
            '{url}?id=500010'.format(url=self.request_url),
            headers=self.build_headers())

        self.assert_status_code(response, httplib.OK)
        response_json = self._get_response_json(response)
        self.assert_list_response_metadata(response_json, expected_count=1)

        self.set_api_authorized_user(self.regular_user)
        response = self.api_client.get(
            '{url}?name__ilike=%a%&limit=20&order=-id'.format(url=self.request_url),
            headers=self.build_headers())

        self.assert_status_code(response, httplib.OK)
        response_json = self._get_response_json(response)
        expected_entities = self.session.query(self.model).filter(
            Campaign.name.ilike('%a%')
        ).order_by(desc(Campaign.id)).limit(20).all()
        self.assert_list_response(response_json, expected_entities)

        self.set_api_authorized_user(self.regular_user)
        response = self.api_client.get(
            '{url}?q=a&limit=20&order=-id'.format(url=self.request_url),
            headers=self.build_headers())

        response_json = self._get_response_json(response)
        self.assert_list_response(response_json, expected_entities)

    def test_create_api_with_no_timezone(self):
        self.set_api_authorized_user(self.super_admin)
        pass
        # # on demand test:
        # # execute api and get response
        # request_body = self.get_request_body()
        # request_body['start_date'] = '2016-12-29T23:00:00'
        # request_body['end_date'] = '2016-12-31T15:00:00'
        # response = self.api_client.post(
        #     self.request_url,
        #     data=json.dumps(request_body),
        #     headers=self.build_headers(has_body=True))
        #
        # self.assert_status_code(response, httplib.CREATED)
        #
        # response_json = json.loads(response.data)
        # saved_entity = self.session.query(self.model).get(response_json.get('id'))
        # expected = self.get_expected_response(saved_entity)
        # # assert saved data vs request body
        # self.assert_saved_data(saved_entity, request_body)
        # # assert response body vs expected response
        # response_json = self._get_response_json(response)
        # self.assert_response_body(response_json, expected, saved_entity,
        #                           msg="\nFail on Test Create API."
        #                               "\nExpected {expected}\nActual {actual}\n".format(
        #                                 expected=expected, actual=response_json
        #                                 ))

    def test_campaign_items(self):
        self.set_api_authorized_user(self.regular_user)
        campaign = CampaignFactory(id=90001, is_active=True)
        campaign2 = CampaignFactory(id=90002, is_active=True)
        discount = DiscountFactory(campaign=campaign2, is_active=True)
        offer1 = OfferFactory()
        item1 = ItemFactory(item_type=Item.Types.book.value,
                            item_status=Item.Statuses.ready_for_consume.value, is_active=True)
        offer1.items.append(item1)
        discount.offers.append(offer1)
        self.session.add(campaign)
        self.session.add(campaign2)
        self.session.add(discount)
        self.session.add(item1)
        self.session.add(offer1)
        self.session.commit()

        response = self.api_client.get(
            '{url}/{id}/items?platform_id=4'.format(url=self.request_url, id=campaign.id),
            headers=self.build_headers())

        self.assert_status_code(response, httplib.NO_CONTENT)

        response = self.api_client.get(
            '{url}/{id}/items?platform_id=4'.format(url=self.request_url, id=campaign2.id),
            headers=self.build_headers())

        self.assert_status_code(response, httplib.OK)

        response_json = self._get_response_json(response)
        self.assert_list_response_metadata(response_json, expected_count=1)

    def test_invalid_date(self):
        self.set_api_authorized_user(self.super_admin)

        # execute api and get response
        request_body = self.get_request_body()
        request_body['start_date'] = '2016-12-29T23:00:00+00:00'
        request_body['end_date'] = '2016-12-21T15:00:00+00:00'
        response = self.api_client.post(
            self.request_url,
            data=json.dumps(request_body),
            headers=self.build_headers(has_body=True))

        self.assert_status_code(response, 422)
