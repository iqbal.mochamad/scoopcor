import json, httplib

from flask import request, Response, jsonify
from flask_appsfoundry.parsers import SqlAlchemyFilterParser
from flask_restful import Resource
from marshmallow import ValidationError
from marshmallow import fields
from marshmallow import validates_schema
from marshmallow_sqlalchemy import ModelSchema

from app import db, app
from app.auth.decorators import user_has_any_tokens
from app.master.choices import GETSCOOP
from app.master.custom_get import CampaignItems
from app.utils.datetimes import get_local
from app.utils.marshmallow_helpers import MaListResponse, MaGetEditBase
from app.utils.shims.parsers import WildcardFilter
from .models import Campaign


class CampaignSchema(ModelSchema):

    class Meta:
        model = Campaign
        sqla_session = db.session

    total_cost = fields.Float(allow_none=True)

    @validates_schema
    def validate_request_data(self, data):
        if data['start_date'] >= data['end_date']:
            raise ValidationError('Field start date must be less than end date', field_names='start_date')


LIST_TOKENS = ['can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext',
               'can_read_write_global_campaign', 'can_read_global_campaign']
MODIFY_TOKENS = ['can_read_write_global_all', 'can_read_write_global_campaign']


class DefinitionMixin(object):
    model = Campaign
    schema = CampaignSchema()

    get_security_tokens = LIST_TOKENS
    edit_security_tokens = MODIFY_TOKENS


class CampaignApi(DefinitionMixin, MaGetEditBase):
    """ GET API (Get single), PUT API (Update), DELETE API
    """


class CampaignArgsParserLists(SqlAlchemyFilterParser):
    __model__ = Campaign
    q = WildcardFilter(dest='name')


class CampaignListPostApi(DefinitionMixin, MaListResponse):
    """ LIST API (search), POST API (Create)
    """
    key_name = "campaigns"
    filter_parsers = CampaignArgsParserLists()

    def _get_filters(self):
        filters = super(CampaignListPostApi, self)._get_filters()

        is_active = request.args.get('is_active', type=bool)
        if is_active:
            filters.append(Campaign.start_date <= get_local())
            filters.append(Campaign.end_date >= get_local())
        return filters


def dict_to_string(args):
    args = json.dumps(args)
    return str(hash(args))


class CampaignItemsApi(Resource):

    @user_has_any_tokens(*LIST_TOKENS)
    def get(self, campaign_id):
        param = request.args.to_dict()

        key_dict = str(hash(json.dumps(param))).replace('-', '')
        key_name = 'campaign-details-{}'.format(campaign_id)
        redis_key = '{}:{}'.format(key_name, key_dict)

        # IF REDIS CACHE AVAILABLE, RETURN DATA!!
        # data_redis = app.kvs.get(redis_key)
        # if data_redis:
        #     return Response(data_redis, status=httplib.OK, mimetype='application/json')

        exist_campaign = Campaign.query.filter_by(id=campaign_id).first()
        if not exist_campaign:
            return Response(None, status=httplib.NO_CONTENT)

        total_count, campaign_item = CampaignItems(
            campaign=exist_campaign,
            limit=param.get('limit', 20),
            offset=param.get('offset', 0),
            platform_id=param.get('platform_id', GETSCOOP),
            fields=param.get('fields', None),
            extends=param.get('expand').split(',') if param.get('expand') else '').get_campaign_items()

        data_response = {
            'items': campaign_item,
            'name': exist_campaign.name,
            'description': exist_campaign.description,
            'metadata': {'resultset': {"count": total_count, "limit": param.get('limit', 20), "offset": param.get('offset', 0)}}
        }

        # STORE TO REDIS CACHE
        app.kvs.set(redis_key, json.dumps(data_response), app.config['ITEM_DETAILS_REDIS_TIME'])

        resp = jsonify(data_response)
        resp.status_code = httplib.OK
        return resp
