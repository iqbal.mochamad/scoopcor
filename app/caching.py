import json

from flask import current_app, request


def generate_cache_key_default(req=None):
    """ Generates a hash of all the http request arguments.

    :param `flask.request` req: Current flask request object.
    :return: A string-encoded hash suitable for using as a redis key.
    :rtype: `str`
    """
    req = req or request
    hash_input = json.dumps(req.args)
    return str(hash(hash_input))


def cached_response(key_prefix, expires_after, key_generator_func=None):
    """ Decorator for caching API response to redis

    :param `str` key_prefix: prefix key for redis
    :param `timedelta` expires_after: redis will delete the data after expires
    :param key_generator_func:
    :return:
    :rtype: `tuple`
    """
    key_generator_func = key_generator_func or generate_cache_key_default

    def decorated(func):

        def inner(*args, **kwargs):
            if current_app.testing:
                # if it's TESTING, then ignore redis cache, just run the function normally
                return func(*args, **kwargs)

            key = "{prefix}:{key}".format(
                prefix=key_prefix,
                key=key_generator_func()
            )
            cached_response = current_app.kvs.get(key)

            if not cached_response:
                api_response = func(*args, **kwargs)

                if api_response[1] == 200:
                    current_app.kvs.setex(key, int(expires_after.total_seconds()), json.dumps(api_response))
            else:
                api_response = tuple(json.loads(cached_response))

            return api_response

        return inner

    return decorated


def generate_cache_key(args=None):
    """ Generates a hash of all the http request arguments.

    :param `dict` args: args from request.args.
    :return: A string-encoded hash suitable for using as a redis key.
    :rtype: str
    """
    args = args or request.args
    hash_input = json.dumps(args, sort_keys=True)
    return str(hash(hash_input))


def generate_redis_key(key_prefix, key_generator_func=generate_cache_key, args=None):
    """ Generate key for redis with args parameter

    :param `six.string_types` key_prefix: A prefix to be used along with our cache key.
    :param `func` key_generator_func: A function capiable of generating a unique cache key (sans prefix).
    :param `dict` args: args from request.args
    :return: A string that can be used as our redis key.
    :rtype: six.string_types
    """
    key = "{prefix}:{key}".format(
        prefix=key_prefix,
        key=key_generator_func(args)
    )

    return key

