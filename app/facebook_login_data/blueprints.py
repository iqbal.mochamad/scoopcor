from flask import Blueprint
from flask_appsfoundry import Api

from .api import FacebookLoginDataListApi, FacebookLoginDetailApi

blueprint = Blueprint('facebook_login_data', __name__, url_prefix='/v1/facebook_login_data')

api = Api(blueprint)

api.add_resource(FacebookLoginDataListApi, '')
api.add_resource(FacebookLoginDetailApi, '/<int:db_id>', endpoint='facebook_login_data')
