from geoalchemy2 import Geometry
from sqlalchemy import Column
from datetime import datetime
from sqlalchemy.dialects import postgresql

from flask_appsfoundry.models import SoftDeletableMixin

from app import db


class FacebookLoginData(SoftDeletableMixin, db.Model):

    __tablename__ = 'core_logs_facebooklogindata'

    facebook_data = db.Column(postgresql.JSON, nullable=False)
    location = db.Column(Geometry('POINT'))
    os_version = db.Column(db.Text)
    device_model = db.Column(db.Text)
    client_id = db.Column(db.Integer)
    ip_address = db.Column(db.Text)
    created = Column(db.DateTime, default=datetime.utcnow, nullable=False)
    datetime = db.Column(db.DateTime)
    client_version = db.Column(db.Text)
    device_id = db.Column(db.Text)
