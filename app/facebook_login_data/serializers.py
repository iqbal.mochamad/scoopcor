import re
from flask_restful import fields
from flask_appsfoundry import serializers

from app import db
from app.analytics.serializers import GeometryPoint


class FacebookData(serializers.fields.Raw):
    def format(self, value):
        return value



class FacebookLoginDataSerializer(serializers.SerializerBase):
    id = fields.Integer
    facebook_data = FacebookData

    location = GeometryPoint
    client_version = fields.String
    device_id = fields.String
    datetime = fields.DateTime('iso8601')
    os_version = fields.String
    device_model = fields.String
    client_id = fields.Integer
    ip_address = fields.String
    created = fields.DateTime('iso8601')
    is_active = fields.Boolean

