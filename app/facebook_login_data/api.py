from flask_appsfoundry import controllers
from sqlalchemy import desc


from . import models, parsers, serializers


class FacebookLoginDataListApi(controllers.ListCreateApiResource):

    query_set = models.FacebookLoginData.query

    default_ordering = (desc(models.FacebookLoginData.id),
                        )
    create_request_parser = parsers.FacebookLoginDataArgsParser

    list_request_parser = parsers.FacebookLoginDataListArgsParser

    response_serializer = serializers.FacebookLoginDataSerializer

    serialized_list_name = u'facebook_login_data'

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext',
                           )

    post_security_tokens = ('can_read_write_global_all',
                            )


class FacebookLoginDetailApi(controllers.DetailApiResource):

    query_set = models.FacebookLoginData.query

    update_request_parser = parsers.FacebookLoginDataArgsParser

    response_serializer = serializers.FacebookLoginDataSerializer

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext',
                           )

    put_security_tokens = ('can_read_write_global_all',
                           )

    delete_security_tokens = ('can_read_write_global_all',
                              )

    def put(self):
        raise NotImplemented
