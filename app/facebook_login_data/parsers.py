from datetime import datetime
from flask_appsfoundry.parsers import converters as inputs
from flask_appsfoundry.parsers import (
    ParserBase, InputField, SqlAlchemyFilterParser, IntegerField)
from flask_appsfoundry.parsers.filterinputs import (
    IntegerFilter, StringFilter, DateTimeFilter)

from . import models


class FacebookLoginDataArgsParser(ParserBase):
    facebook_data = InputField(type=lambda x: x)
    location = InputField(type=lambda e: u"POINT({})".format(e) if e else None)
    client_version = InputField()
    device_id = InputField()
    datetime = InputField(type=inputs.naive_datetime_from_iso8601, default=datetime.now)
    os_version = InputField()
    device_model = InputField()
    client_id = IntegerField()
    ip_address = InputField()
    is_active = InputField(default=True)
class FacebookLoginDataListArgsParser(SqlAlchemyFilterParser):
    __model__ = models.FacebookLoginData
    facebook_data = StringFilter()
    location = StringFilter()
    client_version = StringFilter()
    device_id = StringFilter()
    datetime = DateTimeFilter()
    os_version = StringFilter()
    device_model = StringFilter()
    client_id = IntegerFilter()
    ip_address = StringFilter()
    is_active = StringFilter()
