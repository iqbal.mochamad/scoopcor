import os
import csv
import sys

from datetime import datetime

from app.master.models import Brand
from app.items.models import Item, UserSubscription
from app.offers.models import Offer

from dateutil import relativedelta

from app.utils.shims import item_types
from app.items.choices.item_type import BOOK, MAGAZINE, NEWSPAPER

from sqlalchemy.sql import and_, or_, desc, func
from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME

def csv_data():
    file = open('/Users/kira/Desktop/migrate_edition_offset_0_limit_1000.txt', 'rb').read()
    data = file.split('\n')

    try:
        file_name = '/Users/kira/Desktop/report.csv'
        writer = csv.writer(open(file_name, 'wb'))

        writer.writerow([
            'BRAND ID', 'BRAND NAME', 'LAST ITEM', 'ITEM RELEASE DATE',
            'ITEM EDITION CODE', 'SUBSCRIPTION ID', 'SUBS VALID FROM',
            'BUY YEAR', 'BUY MONTH', 'USER ID', 'GAP TIME (MONTH)'
        ])

        fileb = open('/Users/kira/Desktop/report_id.txt', 'wb')

        data_id = []
        for item in data:
            datax = item.split(',')

            subs = datax[0].split(':')

            subs_valid_from = str(datax[2]).strip().split(' ')[2]
            subs_valid_from = datetime.strptime(subs_valid_from, '%Y-%m-%d')

            release_date = str(datax[3]).strip().split(' ')[1]
            release_date = datetime.strptime(release_date, '%Y-%m-%d')

            subs_id = int(str(subs[1].strip()).split(' ')[0])

            brand_id = int(subs[2].strip())
            item_id = int(datax[1].split(':')[1].strip())

            brands = Brand.query.filter_by(id=brand_id).first()
            items = Item.query.filter_by(id=item_id).first()
            subscrip = UserSubscription.query.filter_by(id=subs_id).first()

            data_id.append(subs_id)

            r = relativedelta.relativedelta(subs_valid_from, release_date)
            if r.months == 0:
                month = 1
            else:
                month = r.months

            writer.writerow([
                brands.id, brands.name, items.id, items.release_date,
                items.edition_code, subscrip.id, subscrip.valid_from,
                subscrip.valid_from.year, subscrip.valid_from.month,
                subscrip.user_id, month
            ])

        fileb.write(str(data_id))
        fileb.close()
        writer.writerow(['END OF FILE']) # END

    except Exception, e:
        pass


def check_item(offset=None, limit=None, item_type=None):

    item_data = Item.query\
        .filter_by(
            item_type=item_types.id_to_enum(item_type) if item_type else None,
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            is_active=True).order_by(
        desc(Item.release_date)).offset(offset).limit(limit).all()

    path = '/tmp/'
    if item_type == MAGAZINE:
        file_name = 'magazine_lost_off_%s.txt' % offset

    if item_type == BOOK:
        file_name = 'book_lost_off_%s.txt' % offset

    if item_type == NEWSPAPER:
        file_name = 'newspapers_lost_off_%s.txt' % offset

    files = '%s%s' % (path, file_name)
    fileb = open(files, 'wb')

    for item in item_data:
        offer_sing = Offer.query.join(Item.core_offers).filter(Item.id.in_([item.id])).all()

        # TODO: is this ready by a machine or is this a log?  If it's machine read, item_status needs converted back to int value
        if not offer_sing:
            data = "Item_id : %s, Edition_code : %s, Release_date : %s, Status : %s - %s \n" % (
                item.id, item.edition_code, item.release_date, item.item_status, item.is_active
            )
            fileb.write(data)
    fileb.close()
