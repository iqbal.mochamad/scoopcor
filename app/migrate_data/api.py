import json, requests, httplib, base64

from flask_restful import Resource, Api, reqparse
from flask import Response, request

from app import app, db
from app.constants import *
from app.auth.decorators import token_required

from app.migrate_data.helpers import MigrateDiscountCodeConstruct
from app.migrate_data.custom_get import CustomGet

from app.users.buffets import UserBuffet
from app.cronjobs.helpers import PostItemSubs, PostMonitorSubs
from app.items.models import Item, ItemFile, ItemUploadProcess
from app.helpers import internal_server_message, bad_request_message, err_response
from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME

from datetime import datetime, timedelta

from sqlalchemy.sql import desc
from app.send_mail import sendmail_smtp, sendmail_aws
from app.uploads.helpers import upload_email


class MigrateDiscountCodeApi(Resource):

    ''' This for migrate data discount SC1 --> SC2 '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('id', type=int, required=True, location='json')
        self.reqparser.add_argument('name', type=str, required=True, location='json')
        self.reqparser.add_argument('tag_name', type=str, required=True, location='json')
        self.reqparser.add_argument('campaign_id', type=int, required=True, location='json')
        self.reqparser.add_argument('valid_to', type=str, required=True, location='json')
        self.reqparser.add_argument('valid_from', type=str, required=True, location='json')
        self.reqparser.add_argument('discount_rule', type=int, required=True, location='json')
        self.reqparser.add_argument('discount_type', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')
        self.reqparser.add_argument('discount_usd', type=str, required=True, location='json')
        self.reqparser.add_argument('discount_idr', type=str, required=True, location='json')
        self.reqparser.add_argument('platforms', type=list, required=True, location='json')
        self.reqparser.add_argument('paymentgateways', type=list, required=True, location='json')
        self.reqparser.add_argument('discount_status', type=int, required=True, location='json')
        self.reqparser.add_argument('discount_schedule_type', type=int, required=True, location='json')

        self.reqparser.add_argument('offers', type=list, location='json')
        self.reqparser.add_argument('discount_point', type=int, location='json')
        self.reqparser.add_argument('min_usd_order_price', type=str, location='json')
        self.reqparser.add_argument('max_usd_order_price', type=str, location='json')
        self.reqparser.add_argument('min_idr_order_price', type=str, location='json')
        self.reqparser.add_argument('max_idr_order_price', type=str, location='json')
        self.reqparser.add_argument('description', type=str, location='json')

        super(MigrateDiscountCodeApi, self).__init__()

    @token_required('can_read_write_global_all')
    def post(self):
        args = self.reqparser.parse_args()
        scene = MigrateDiscountCodeConstruct(args, "POST").construct()
        return scene

class MigrateItemsSubs(Resource):

    def get(self):
        data = PostItemSubs('GO').construct()
        return 'OK'

class MonitorItemSubs(Resource):

    def get(self):
        try:
            data = PostMonitorSubs('GO').construct()
            return 'OK'
        except Exception, e:
            return str(e)

class MonitorItemOffers(Resource):

    def get(self):
        param = request.args.to_dict()
        scene = CustomGet(param, 'MONITOR_ITEM_OFFERS').filter_custom()
        return scene

class MonitorBuffets(Resource):

    """
        monitor user buffets restore, restore > max limit send an emails

    """
    def __init__(self):
        self.master_original = []
        self.master_content = []

    def html_content(self):
        content = """
            <div style="font-family: Arial; padding: 10px;">
                <div style=""> Parent Restore : %s - %s </div></br>
                <div style=""> Original Transaction Id : %s </div></br></br>
                <table border="1" style="border-collapse:collapse;width: 500px;" cellpadding="3" cellspacing="2">
                    <tr>
                        <th>Restore Date</th>
                        <th>SCOOPID </th>
                        <th>Username</th>
                    </tr>
                    %s
                </table>
            </div>
        """
        return content

    def child_content(self):
        content = """
            <tr>
                <td>%s</td>
                <td>%s</td>
                <td>%s</td>
            </tr>
        """
        return content

    def get(self):

        try:
            limit_date = datetime.now() - timedelta(weeks=1)
            restore_count = {}

            # get buffet data for pass 1 weeks
            data = UserBuffet.query.filter(
                UserBuffet.created >= limit_date,
                UserBuffet.original_transaction_id != None).filter_by(
                is_restore=True, is_trial=False).all()
            original_transactions = [d.original_transaction_id for d in data]

            # count original transaction id, store in dict
            for i in set(original_transactions): restore_count[i] = original_transactions.count(i)

            # recheck count > max buffet limit
            for j in restore_count.iteritems():
                if j[1] >= app.config['RESTORE_BUFFET_LIMIT']: self.master_original.append(j[0])

            if self.master_original:
                for data_p in self.master_original:

                    # find parents
                    parent_data = UserBuffet.query.filter_by(
                        original_transaction_id = data_p, is_restore=False).first()

                    if parent_data:
                        email_parent = parent_data.user.email

                    # find child restore
                    child_data = UserBuffet.query.filter(
                        UserBuffet.created >= limit_date).filter_by(
                        original_transaction_id = data_p,
                        is_restore = True, is_trial = False).order_by(
                        desc(UserBuffet.created)).all()

                    if child_data:
                        data_child = []
                        for data_c in child_data:
                            email_child = data_c.user.email
                            data_content = self.child_content()

                            data_content = data_content % (
                                data_c.created.date(),
                                data_c.user_id,
                                email_child
                            )
                            data_child.append(data_content)

                    content = self.html_content()
                    content = content % (
                        parent_data.id, email_parent,
                        parent_data.original_transaction_id, ''.join(data_child))

                    self.master_content.append(content)

            if self.master_content:
                # send mails
                subject = '%s [WARNING] Unusual Activity Restore Buffet' % app.config['ADMIN_SERVER']
                admin_emails = app.config['ADMIN_EMAILS']
                try:
                    sendmail_aws(subject, 'no-reply@gramedia.com', admin_emails, ''.join(self.master_content))
                except Exception, e:
                    try:
                        sendmail_smtp(subject, 'no-reply@gramedia.com',
                            admin_emails, ''.join(self.master_content))
                    except Exception, e:
                        pass

            return "OK"

        except Exception, e:
            print "Error : ", e

class ActivateSC1(Resource):
    """
        This api used for operator, send data items SC2 to SC1
        send, file_version key, image after uploader
    """

    def get(self):
        try:
            json_data = []
            items = ItemUploadProcess.query.filter_by(status=11).limit(10).all()

            for data_item in items:
                tmp = {}

                # get item objects
                item = Item.query.filter_by(id=data_item.item_id).first()
                if not item:
                    tmp['item_id'] = item.id
                    tmp['error'] = "Migrate data failed : item not founds"

                    data_item.status = 12
                    data_item.error = "Migrate data failed : item not founds"

                # check this item if already on sale?
                if item.item_status != STATUS_TYPES[STATUS_READY_FOR_CONSUME]:
                    tmp['item_id'] = item.id
                    tmp['error'] = "Migrate data failed : item status Not for consume"

                    data_item.status = 12
                    data_item.error = "Migrate data failed : item status Not for consume"

                data = {}
                data['id'] = item.id
                data['image_highres'] = item.image_highres
                data['image_normal'] = item.image_normal
                data['thumb_image_highres'] = item.thumb_image_highres
                data['thumb_image_normal'] = item.thumb_image_normal

                # here check for item type, mapping on sc1:
                # PDF = str(1)
                # Epub = str(4)
                if str(item.content_type) == 'pdf':
                    is_active = 1
                if str(item.content_type) == 'epub':
                    is_active = 4

                data['is_active'] = is_active

                ## get item file key
                item_file = ItemFile.query.filter_by(
                    item_id=item.id).order_by(desc(ItemFile.id)).first()

                if not item_file:
                    tmp['item_id'] = item.id
                    tmp['error'] = "Migrate data failed : item file not founds"
                    data_item.status = 12
                    data_item.error = "Migrate data failed : item file not founds"
                else:
                    data['item_file_name'] = item_file.file_name
                    data['item_file_key'] = item_file.key

                ##authorization signature
                data_signature = '%s%s' % (item.id, app.config['SC1_SALT'])
                headers = {'Authorization': base64.b64encode(data_signature)}

                data_json = json.dumps(data)
                send_files = requests.put(app.config['SC1_FILE_METADATA'], data=data_json, headers=headers)

                if send_files.status_code not in [httplib.OK, httplib.CREATED]:
                    tmp['item_id'] = item.id
                    tmp['error'] = "Migrate data failed : status : %s error : %s" % (
                        send_files.status_code, send_files.text)
                    data_item.status = 12
                    data_item.error = "Migrate data failed : status : %s error : %s" % (
                        send_files.status_code, send_files.text)
                else:
                    data_item.status = 14
                data_item.save()

                if data_item.status == 12:
                    upload_email(item, data_item, data_item.error)

                json_data.append(tmp)

            rv = json.dumps(json_data)
            return Response(rv, status=httplib.MULTI_STATUS, mimetype='application/json')

        except Exception, e:
            return internal_server_message(
                modul="ActivateSC1 ERROR", method="PUT",
                error="Error : %s" % e, req="")
