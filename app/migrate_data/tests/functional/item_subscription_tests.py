from datetime import datetime, timedelta
from app.items.models import Item
from app.migrate_data.items_subscriptions import DurationMigrate
from app.offers.models import OfferSubscription

from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import ItemFactory, UserSubscriptionFactory


class ItemSubscriptionTest(TestBase):

    @classmethod
    def setUpClass(cls):
        super(ItemSubscriptionTest, cls).setUpClass()
        cls.item1 = ItemFactory(
            release_date=datetime.now().date(),
            item_status=Item.Statuses.ready_for_consume.value,
            is_active=True
        )
        # cls.session.commit()

        cls.item2 = ItemFactory(
            brand=cls.item1.brand,
            release_date=datetime.now() - timedelta(weeks=1),
            item_status=Item.Statuses.ready_for_consume.value,
            is_active=True
        )
        # cls.session.commit()

        cls.user_subs1 = UserSubscriptionFactory(
            brand=cls.item1.brand,
            quantity=1,
            quantity_unit=OfferSubscription.QuantityUnit.week.value,
            current_quantity=1,
            valid_from=datetime.now(),
            valid_to=datetime.now() + timedelta(weeks=1),
            is_active=True,
            orderline_id=None
        )

        cls.user_subs2 = UserSubscriptionFactory(
            brand=cls.item2.brand,
            quantity=1,
            quantity_unit=OfferSubscription.QuantityUnit.week.value,
            current_quantity=1,
            valid_from=datetime.now() - timedelta(weeks=2),
            valid_to=datetime.now() - timedelta(weeks=1),
            is_active=True,
            orderline_id=None
        )
        cls.session.commit()

    def test_get_usersubscription(self):

        dm_data = DurationMigrate(item=self.item1).get_usersubscription()
        self.assertEqual(len(dm_data), 1)
        self.assertEqual(dm_data[0].id, self.user_subs1.id)

        dm_data2 = DurationMigrate(item=self.item2).get_usersubscription()
        self.assertEqual(len(dm_data2), 1)
        self.assertEqual(dm_data2[0].id, self.user_subs2.id)
