import os

from sqlalchemy import cast, Date

from app.items.models import Item, UserItem, UserSubscription, UserSubscriptionItem
from app.items.choices.status import *

from sqlalchemy.sql import desc

from app.offers.models import OfferSubscription

from app.utils.shims import item_types
from app import db


class DurationMigrate(object):
    """
        GET FROM LATEST ITEMS, SEARCH SUBSCRIPTIONS WITH BRAND_ID ACTIVE/NOT
    """

    def __init__(self, item=None):
        self.item = item

    def migrate_duration_subs(self):
        """
            THIS IS FOR MIGRATE DATA NEWSPAPER VS USER_SUBSCRIPTION
        """
        try:

            if self.item:
                # Why force again to is_active?, cos from SC1 sometimes set is_active=False
                # frontend will crash if item consumable but is_active False.
                if not self.item.is_active:
                    self.item.is_active = True
                    db.session.add(self.item)
                    db.session.commit()

                subscription = self.get_usersubscription()

                for isubs in subscription:
                    # Recheck date ( item release date vs range (subs valid_from - valid_to ))
                    if (self.item.release_date.date() >= isubs.valid_from.date()
                        and self.item.release_date.date() <= isubs.valid_to.date()):

                        # Deliver items durations..
                        self.save_duration_item(self.item, isubs)

            return True, 'OK'

        except Exception as e:
            return False, 'migrate_duration_subs Error {}'.format(str(e))

    def get_usersubscription(self):
        subscription = UserSubscription.query.filter(
            UserSubscription.brand_id == self.item.brand_id,
            UserSubscription.is_active == True,
            UserSubscription.quantity_unit.in_([
                OfferSubscription.QuantityUnit.day.value,
                OfferSubscription.QuantityUnit.week.value,
                OfferSubscription.QuantityUnit.month.value
            ]),
            cast(UserSubscription.valid_to, Date) >= self.item.release_date.date(),
            cast(UserSubscription.valid_from, Date) <= self.item.release_date.date()
        ).order_by(desc(UserSubscription.valid_to)).all()

        return subscription

    def save_duration_item(self, item=None, item_subs=None):
        try:
            # check if already exists not buffets
            item_data = UserItem.query.filter(
                UserItem.user_buffet_id == None,
                UserItem.user_id == item_subs.user_id,
                UserItem.item_id == item.id).first()

            if item_data:
                # Force is_active True if False..
                # Why force again to is_active?, this is bug if user buyed item from SC1, migrate to user item with is_active False
                # frontend will crash if item consumable but is_active False.
                if not item_data.is_active:
                    item_data.is_active = True
                    db.session.add(item_data)
                    db.session.commit()
            else:
                vendor_id = item.brand.vendor_id if item.brand.vendor_id else 0
                user_item = UserItem(
                    user_id = item_subs.user_id,
                    item_id = item.id,
                    edition_code = item.edition_code,
                    orderline_id = item_subs.orderline_id,
                    is_active = True,
                    brand_id = item.brand_id,
                    itemtype_id = item_types.enum_to_id(item.item_type),
                    vendor_id = vendor_id
                )
                db.session.add(user_item)
                db.session.commit()
                print 'success deliver DURATION items {} for user : {}'.format(item.id, item_subs.user_id)

            return True, 'OK'

        except Exception as e:
            return False, 'Error save_duration_item {}'.format(str(e))

    def construct(self):
        mg = self.migrate_duration_subs()
        return mg


class EditionMigrate():

    def __init__(self, item=None):
        self.item = item

    def migrate_edition(self):
        try:
            subs_data = UserSubscription.query.filter(
                UserSubscription.current_quantity > 0).filter_by(
                quantity_unit=OfferSubscription.QuantityUnit.unit.value,
                brand_id=self.item.brand_id, is_active=True).order_by(UserSubscription.id).all()

            for subs in subs_data:

                # find all item with range valid_from subs
                valid_from = subs.valid_from.replace(hour=0, minute=0, second=0, microsecond=0)
                all_items = Item.query.filter(
                    Item.release_date >= valid_from,
                    Item.brand_id == self.item.brand_id,
                    Item.item_status.in_([
                        STATUS_TYPES[STATUS_UPLOADED],
                        STATUS_TYPES[STATUS_PREPARE_FOR_CONSUME],
                        STATUS_TYPES[STATUS_READY_FOR_CONSUME]]),
                    Item.is_active == True
                ).order_by(Item.release_date).limit(subs.quantity).all()

                for item_found in all_items:
                    self.save_lib_item_edition(item_found, subs)

            return True, 'OK'

        except Exception as e:
            return False, 'Error self.migrate_edition {}'.format(str(e))


    def save_lib_item_edition(self, item=None, item_subs=None):
        try:
            # check if already exists not buffets
            item_data = UserItem.query.filter(
                UserItem.user_buffet_id == None,
                UserItem.user_id == item_subs.user_id,
                UserItem.item_id == item.id).first()

            if item_data:
                # Force is_active True if False..
                # Why force again to is_active?, this is bug if user buyed item from SC1, migrate to user item with is_active False
                # frontend will crash if item consumable but is_active False.
                if not item_data.is_active:
                    item_data.is_active = True
                    db.session.add(item_data)
                    db.session.commit()
            else:
                # calculate current quantity..
                if item_subs.current_quantity - 1 <= 0:
                    item_subs.current_quantity = 0
                else:
                    item_subs.current_quantity = item_subs.current_quantity - 1

                db.session.add(item_subs)
                db.session.commit()

                vendor_id = item.brand.vendor_id if item.brand.vendor_id else 0
                user_item = UserItem(
                    user_id = item_subs.user_id,
                    item_id = item.id,
                    edition_code = item.edition_code,
                    orderline_id = item_subs.orderline_id,
                    is_active = True,
                    brand_id = item.brand_id,
                    itemtype_id = item_types.enum_to_id(item.item_type),
                    vendor_id = vendor_id
                )
                db.session.add(user_item)
                db.session.commit()
                print 'success deliver EDITION items {} for user : {}'.format(item.id, item_subs.user_id)

            return True, 'OK'

        except Exception as e:
            return False, 'Error save_lib_item_edition {}'.format(str(e))

    def construct(self):
        mg = self.migrate_edition()
        return mg

class AdditionalMigrate():

    def __init__(self, subs_id=None):
        self.subs_id = subs_id
        self.meot = 0

    def migrate_subs(self):
        try:
            if self.subs_id:
                subs_data = UserSubscription.query.filter(UserSubscription.id > self.subs_id).filter_by(quantity_unit=4).order_by(
                    desc(UserSubscription.id)).all()
                file_name = 'migrate_add_offset_0_id_%s.txt' % self.subs_id

            fileb = open(os.path.join('/tmp/', file_name), 'wb')
            length = len(subs_data)

            self.meot = 0
            for subs in subs_data:
                self.meot = self.meot + 1

                item = Item.query.filter(Item.release_date >= subs.valid_from, Item.release_date <= subs.valid_to).filter_by(
                    brand_id=subs.brand_id, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                    is_active=True).order_by(Item.release_date).limit(subs.quantity).all()

                for item_data in item:
                    self.save_lib_item(item_data, subs)
                    print 'subs_valid_from (%s --> %s) item (%s)' % (subs.valid_from, subs.valid_to, item_data.release_date)

        except Exception, e:
            print e

    def save_lib_item(self, item=None, subs=None):
        try:
            #check if already exists
            item_data = UserItem.query.filter_by(user_id=subs.user_id, item_id=item.id).first()
            if item_data:
                print 'Data CONFLICT....'
                pass
                # junc_subs = UserSubscriptionItem(
                #     usersubscription_id = subs.id,
                #     useritem_id = item_data.id,
                #     item_id = item.id,
                #     user_id = subs.user_id,
                #     note = 'FIXED DATA'
                # )
                # mv = junc_subs.save()
            else:

                try:
                    vendors = item.get_vendor()
                    vendor_id = vendors.get('id', 0)
                except:
                    vendor_id = 0

                user_item = UserItem(
                    user_id = subs.user_id,
                    item_id = item.id,
                    edition_code = item.edition_code,
                    orderline_id = 0,
                    is_active = True,
                    brand_id = item.brand_id,
                    itemtype_id = item_types.enum_to_id(item.item_type),
                    vendor_id = vendor_id
                )
                sv = user_item.save()
                if sv.get('invalid', False):
                    return 'INVALID1', 'USER ITEMS FAILED WRITE ITEM_ID : %s SUBS_ID : %s \n' % (item.id, subs.id)
                else:
                    pass
                    # junc_subs = UserSubscriptionItem(
                    #     usersubscription_id = subs.id,
                    #     useritem_id = user_item.id,
                    #     item_id = item.id,
                    #     user_id = subs.user_id,
                    #     note = 'MIGRATE SUBS MIGRATE'
                    # )
                    # mv = junc_subs.save()
                    # if mv.get('invalid', False):
                    #     return 'INVALID2', 'JUNK TABLE FAILED WRITE ITEM_ID : %s SUBS_ID : %s \n' % (item.id, subs.id)
            return 'OK', ''

        except Exception, e:
            return 'INVALID3', '%s \n' % str(e)

    def construct(self):
        self.migrate_subs()

class CheckData():

    def __init__(self, offset=None, limit=None, subs_id=None):
        self.offset = offset
        self.limit = limit

        self.subs_id = subs_id

    def check_edition(self):
        " Checking edition who lost junc table "
        subs_data = UserSubscription.query.filter(
            UserSubscription.current_quantity < UserSubscription.quantity).filter_by(
            quantity_unit=1).order_by(desc(UserSubscription.id)).offset(
            self.offset).limit(self.limit).all()

        if subs_data:
            for subs in subs_data:
                junc = UserSubscriptionItem.query.filter_by(usersubscription_id=subs.id).first()
                if not junc:

                    item = Item.query.filter(Item.release_date >= subs.valid_from).filter_by(brand_id=subs.brand_id, is_active=True).order_by(
                        Item.release_date).limit(subs.quantity).all()

                    for data_item in item:
                        user_item = UserItem.query.filter_by(user_id=subs.user_id, item_id=data_item.id).first()

                        if user_item:
                            junc2 = UserSubscriptionItem.query.filter_by(usersubscription_id=subs.id, useritem_id=user_item.id).first()
                            if not junc2:
                                jc_tb = UserSubscriptionItem(
                                    usersubscription_id = subs.id,
                                    useritem_id = user_item.id,
                                    item_id = data_item.id,
                                    user_id = subs.user_id,
                                    note = 'FIXED DATA'
                                )
                                jc_tb.save()
                                print '----- FIXED subs_id : %s user_item : %s junc : %s' % (subs.id, user_item.id, jc_tb.id)


    def check_duration(self):
        " Checking duration who lost junc table "

        if self.subs_id:
            subs_data = UserSubscription.query.filter(
                UserSubscription.id > self.subs_id).filter_by(
                quantity_unit=4).order_by(desc(UserSubscription.id)).all()
        else:
            subs_data = UserSubscription.query.filter_by(quantity_unit = 4).order_by(
                desc(UserSubscription.id)).offset(
                self.offset).limit(self.limit).all()

        if subs_data:
            for subs in subs_data:
                junc = UserSubscriptionItem.query.filter_by(usersubscription_id=subs.id).first()
                if not junc:

                    item = Item.query.filter(Item.release_date >= subs.valid_from, Item.release_date <= subs.valid_to).filter_by(
                        brand_id=subs.brand_id, is_active=True).order_by(Item.release_date).all()

                    for data_item in item:
                        user_item = UserItem.query.filter_by(user_id=subs.user_id, item_id=data_item.id).first()

                        if user_item:
                            junc2 = UserSubscriptionItem.query.filter_by(usersubscription_id=subs.id, useritem_id=user_item.id).first()
                            if not junc2:
                                jc_tb = UserSubscriptionItem(
                                    usersubscription_id = subs.id,
                                    useritem_id = user_item.id,
                                    item_id = data_item.id,
                                    user_id = subs.user_id,
                                    note = 'FIXED DATA'
                                )
                                jc_tb.save()
                                print '----- FIXED subs_id : %s valid_from : %s release_date : %s' % (subs.id, subs.valid_from, data_item.release_date)

