import json
import httplib

from flask_restful import Resource, Api, reqparse
from flask_restful import Api, reqparse
from flask import abort, Response, jsonify

from app import app, db

from app.offers.models import Offer, OfferPlatform
from app.items.models import Item
from app.master.models import Brand

from sqlalchemy.sql import and_, or_, desc
from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME

class CustomGet():

    def __init__(self, param=None, module=None):
        self.module = module

        self.item_id = param.get('item_id', None)
        self.q = param.get('q', None)
        self.brand_id = param.get('brand_id', None)
        self.platform = param.get('platform_id', None)

        self.limit = param.get('limit', 100)
        self.offset = param.get('offset', 0)

        self.master = ''
        self.total = 0
        self.cur_values = []

    def item_master(self):
        self.total = Item.query.count()
        return Item.query.filter_by(is_active=True, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME])

    def json_success(self, data_items=None, offset=None, limit=None):

        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            if self.platform:
                for u in data_items:
                    tmp = {}
                    offer = u.get_offer_platform(self.platform)

                    for z in offer:
                        if z.get('Error', None):
                            tmp['item_id'] = u.id
                            tmp['offer_id'] = z.get('offer_id', None)
                            tmp['platform_id'] = self.platform
                            tmp['Error'] = z.get('Error', '')
                            self.cur_values.append(tmp)

            rv = json.dumps({self.module: self.cur_values})
            return Response(rv, status=httplib.OK, mimetype='application/json')

    def filter_custom(self):
        self.master = self.item_master()

        if self.item_id:
            self.master = self.master.filter_by(id=self.item_id)

        if self.brand_id:
            self.master = self.master.filter_by(brand_id=self.brand_id)

        if self.q:
            self.q = "%s%s" % (self.q, '%')
            self.master = self.master.filter(Item.name.ilike(self.q))

        self.master = self.master.limit(self.limit).offset(self.offset).all()
        return self.json_success(self.master, self.offset, self.limit)



