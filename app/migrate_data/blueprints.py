from flask import Blueprint
from flask.ext import restful

from .api import MigrateDiscountCodeApi, MigrateItemsSubs
from .api import MonitorItemSubs, MonitorItemOffers, MonitorBuffets
from .api import ActivateSC1

migrate_data_blueprint = Blueprint('migrate_data', __name__, url_prefix='/v1')

migrate_data_api = restful.Api(migrate_data_blueprint)
migrate_data_api.add_resource(MigrateDiscountCodeApi, '/migrate-discounts')
migrate_data_api.add_resource(MigrateItemsSubs, '/items/deliver_subscriptions')
migrate_data_api.add_resource(MonitorItemSubs, '/monitors/items/deliver_subscriptions')
migrate_data_api.add_resource(MonitorItemOffers, '/monitors/offer_items')

migrate_data_api.add_resource(MonitorBuffets, '/monitors/buffet_restore')
migrate_data_api.add_resource(ActivateSC1, '/sc1deliver_files')
