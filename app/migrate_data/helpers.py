import ast
import json
import httplib

from datetime import datetime

from flask_restful import Resource, Api, reqparse
from flask_restful import Api, reqparse
from flask import abort, Response, jsonify, request

from app import app, db
from app.constants import *
from app.auth.decorators import token_required

from app.helpers import log_api, assertion_error, message_json
from app.helpers import dtserializer, err_response, date_validator
from app.helpers import conflict_message, bad_request_message, internal_server_message

from sqlalchemy.sql import and_, or_

from app.items.models import Item
from app.master.models import Platform
from app.campaigns.models import Campaign

from app.offers.models import Offer, OfferPlatform
from app.discounts.models import Discount
from app.payment_gateways.models import PaymentGateway


class MigrateDiscountCodeConstruct():

    def __init__(self, args=None, module=None):
        self.args = args
        self.method = module

        self.campaign_id = args.get('campaign_id', None)
        self.valid_from = args.get('valid_from', '')
        self.valid_to = args.get('valid_to', '')

        self.discount_type = args.get('discount_type', 0)
        self.discount_schedule_type = args.get('discount_schedule_type', None)

        self.discount_usd = args.get('discount_usd', None)
        self.discount_idr = args.get('discount_idr', None)

        self.min_usd_order_price = args.get('min_usd_order_price', None)
        self.max_usd_order_price = args.get('max_usd_order_price', None)
        self.min_idr_order_price = args.get('min_idr_order_price', None)
        self.max_idr_order_price = args.get('max_idr_order_price', None)

        self.offers = args.get('offers', None)
        self.platforms = args.get('platforms', None)
        self.paymentgateways = args.get('paymentgateways', None)

        self.discount_idx = args.get('id', 0)

        self.master_offers = []
        self.discount_plat = []

    def check_discount_type(self):
        try:
            if int(self.discount_type) != 5:
                return 'FAILED', bad_request_message(modul="migdiscount 1", method=self.method,
                    param="discount_type", not_found="discount_type must DISCOUNT CODE TYPES", req=self.args)

            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="migdiscount 1", method=self.method,
                error=e, req=str(self.args))

    def check_campaign(self):
        try:
            campaign = Campaign.query.filter_by(id=self.campaign_id).first()
            if not campaign:
                return 'FAILED', bad_request_message(modul="migdiscount 2", method=self.method,
                    param="campaign_id", not_found=self.campaign_id, req=self.args)
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="migdiscount 2", method=self.method,
                error=e, req=str(self.args))

    def check_date(self):
        """
            CHECKING DATE FORMAT YYYY-MM-DD
            INVALID IF VALID_TO < VALID_FROM
        """
        try:
            valid_from = date_validator(self.valid_from, 'custom')
            if valid_from.get('invalid', False):
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    error_code=httplib.BAD_REQUEST,
                    developer_message="Invalid valid from",
                    user_message="valid from invalid")

            valid_to = date_validator(self.valid_to, 'custom')
            if valid_to.get('invalid', False):
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    error_code=httplib.BAD_REQUEST,
                    developer_message="Invalid valid to",
                    user_message="valid to invalid")

            if self.valid_to <= self.valid_from:
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    error_code=httplib.BAD_REQUEST,
                    developer_message="Invalid valid to must greater than valid from",
                    user_message="valid to invalid")

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="migdiscount 3", method=self.method,
                error=e, req=str(self.args))

    def check_offers_exist(self):
        try:
            offer_data = []
            for ioffer in self.offers:
                offer = Offer.query.filter_by(offer_code=ioffer).first()

                if not offer:
                    return 'FAILED', bad_request_message(modul="migdiscount 4", method=self.method,
                        param="offer code", not_found='invalid Offer code : %s' % ioffer, req=self.args)

                if offer.is_free:
                    return 'FAILED', bad_request_message(modul="migdiscount 4", method=self.method,
                        param="offer code", not_found='Offer code : %s is FREE!!!' % ioffer, req=self.args)
                offer_data.append(offer)

            self.master_offers = offer_data
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="migdiscount 4", method=self.method,
                error=e, req=str(self.args))

    def check_platforms(self):
        """
            CHECK PLATFORM EXIST/NOT EXIST
        """
        try:
            data_platform = []
            for iplat in self.platforms:
                plat = Platform.query.filter_by(id=iplat).first()
                if not plat:
                    return 'FAILED', bad_request_message(modul="migdiscount 5", method=self.method,
                        param="platforms", not_found=iplat, req=self.args)
                data_platform.append(plat)

            self.platforms = data_platform
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="migdiscount 5", method=self.method,
                error=e, req=str(self.args))

    def check_payment_gateways(self):
        """
            CHECK PAYMENT GATEWAY EXIST/NOT EXIST
        """
        try:
            data_payment = []
            for ipay in self.paymentgateways:
                pay = PaymentGateway.query.filter_by(id=ipay).first()
                if not pay:
                    return 'FAILED', bad_request_message(modul="migdiscount 6", method=self.method,
                        param="paymentgateways", not_found=ipay, req=self.args)
                data_payment.append(pay)

            self.paymentgateways = data_payment
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="migdiscount 6", method=self.method,
                error=e, req=str(self.args))

    def post_discounts(self):

        try:
            kwargs = {}
            for item in self.args:
                if self.args[item] is None:
                    pass
                else:
                    kwargs[item] = self.args[item]

            if self.offers:
                del kwargs['offers']

            if self.platforms:
                del kwargs['platforms']

            if self.paymentgateways:
                del kwargs['paymentgateways']

            #ALL OFFERS
            if not self.master_offers:
                kwargs['predefined_group'] = 4

            m = Discount(**kwargs)

            if self.master_offers:
                for i in self.master_offers:
                    m.offers.append(i)

            if self.platforms:
                for z in self.platforms:
                    m.platforms.append(z)

            if self.paymentgateways:
                for x in self.paymentgateways:
                    m.paymentgateways.append(x)

            sv = m.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="migdiscount 8",
                    method=self.method, error=sv.get('message', ''), req=str(self.args))
            else:
                rv = m.values()
                self.master_discounts = m

                # if platform and has offers ( NOT ALL OFFER )
                if self.master_offers:
                    offer_id = [i.id for i in self.master_offers]
                    for i in self.master_offers:
                        self.update_offers(i, self.platforms)

                return 'OK', Response(json.dumps(rv), status=httplib.CREATED,
                    mimetype='application/json')

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="migdiscount 8", method=self.method,
                error=e, req=str(self.args))

        fileb.close()

    def update_offers(self, offer_data=None, platform=None):
        try:
            temp_discount = []

            if len(platform) > 0:

                platforms = [i.id for i in platform]

                for plt in platforms:
                    plt = int(plt)
                    ##update offers platform + base offer
                    ##IOS ONLY
                    offplatform = OfferPlatform.query.filter_by(offer_id=offer_data.id, platform_id=plt).all()

                    if offplatform:
                        for dsc in offplatform:
                            if dsc.discount_id:
                                self.discount_plat = [i for i in dsc.discount_id]
                                for i in dsc.discount_id:
                                    if self.master_discounts.id not in self.discount_plat:
                                        self.discount_plat.append(self.master_discounts.id)
                                dsc.discount_id = self.discount_plat
                            else:
                                dsc.discount_id = [self.master_discounts.id]
                            dsc.save()

            if offer_data.is_discount:
                for ix in offer_data.discount_id:
                    temp_discount.append(ix)
                temp_discount.append(self.master_discounts.id)
                offer_data.discount_id = temp_discount
            else:
                offer_data.discount_id = [self.master_discounts.id]

            offer_data.is_discount = True
            offer_data.save()

        except Exception, e:
            return 'FAILED', internal_server_message(modul="migdiscount 9", method=self.method,
                error=e, req=str(self.args))

    def construct(self):
        typs = self.check_discount_type()
        if typs[0] != 'OK':
            return typs[1]

        cps = self.check_campaign()
        if cps[0] != 'OK':
            return cps[1]

        chk_date = self.check_date()
        if chk_date[0] != 'OK':
            return chk_date[1]

        if len(self.platforms) > 0:
            check_platforms = self.check_platforms()
            if check_platforms[0] != 'OK':
                return check_platforms[1]

        payment = self.check_payment_gateways()
        if payment[0] != 'OK':
            return payment[1]

        if len(self.offers) > 1:
            #CHECK OFFERS_ID
            chk_offer = self.check_offers_exist()
            if chk_offer[0] != 'OK':
                return chk_offer[1]

        pst_discount = self.post_discounts()
        if pst_discount[0] != 'OK':
            return pst_discount[1]

        return pst_discount[1]

