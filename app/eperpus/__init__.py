"""
Shared Libraries (EPerpus)
==========================

Allows a group of users to access a shared pool of Items for a limited amount of time.   The items themselves are
owned by a Shared Library Organization (eperpus), and are accessable to their members for a limited amount of time.
"""
