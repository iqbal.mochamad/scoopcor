from __future__ import unicode_literals, absolute_import

import os, requests
from httplib import CREATED, NO_CONTENT, OK

from flask import g, current_app, make_response, url_for, request, jsonify, Blueprint
from flask_appsfoundry import viewmodels, parsers, marshal
from flask_appsfoundry.exceptions import NotFound, BadRequest, UnprocessableEntity, Forbidden
from flask_appsfoundry.serializers import SerializerBase, fields
from humanize import naturalsize
from marshmallow import fields as ma_fields
from marshmallow_sqlalchemy import ModelSchema
from six import text_type
from sqlalchemy import desc
from werkzeug.datastructures import ImmutableMultiDict
from werkzeug.routing import BaseConverter

from app import app, db, ma
from app.eperpus.helpers import get_stock_holding_org, get_rating_metadata, organization_list_parent

from app.eperpus.models import WatchList, Catalog, CatalogItem
from app.eperpus.solr import get_catalog_items_search, get_brand_name_suggestion
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.items.helpers import get_avg_and_total_rating
from app.items.models import Item, Review
from app.items.previews import preview_s3_covers, preview_s3_urls
from app.items.helpers import ItemQueryBuilder

from app.users.organizations import is_organization_member, is_organization_manager, get_org_and_child_org_ids, Organization
from app.utils.dicts import CorDefaultDict
from app.utils.marshmallow_helpers import MaListResponse, SimpleHRefSchema, schema_load, update_response
from app.utils.models import get_url_for_model
from app.utils.serializers import SimpleImplicitHref, SimpleImplicitHrefWithId, RatingSerializer
from app.utils.shims.parsers import WildcardFilter
from app.utils.strings import FORMAT_ISO8601_DURATION
from app.utils.viewmodels import HrefAttribute, HrefAttributeWithId, RatingAttribute
from app.services.solr import SolrGateway

from .libraries import SharedLibrary, SharedLibraryItem

blueprint = Blueprint('eperpus_catalogs', __name__, url_prefix='/v1/organizations')


class CatalogConverter(BaseConverter):

    def to_python(self, value):
        session = db.session()
        catalog = session.query(Catalog).get(value)
        if not catalog:
            raise NotFound
        return catalog

    def to_url(self, value):
        return text_type(value.id)


app.url_map.converters['catalog'] = CatalogConverter


class CatalogSchema(ModelSchema):
    class Meta:
        model = Catalog
        sqla_session = db.session()
        exclude = ('created', 'modified', 'name', 'catalog_watch_list', 'catalog_items')

    title = ma_fields.Str(attribute='name', required=True, allow_none=False)
    href = ma_fields.Str(attribute='api_url', dump_only=True)
    shared_library = ma_fields.Nested(SimpleHRefSchema, dump_only=True)


@blueprint.route('/<shared_library:entity>/shared-catalogs')
@is_organization_member
def search_catalogs(entity):
    """
    :param `SharedLibrary` entity:
    """
    include_child = request.args.get('include-child', type=bool, default=False)
    offset = request.args.get('offset', type=int, default=0)
    limit = request.args.get('limit', type=int, default=20)

    if include_child:
        ord_ids = get_org_and_child_org_ids(entity)
        query = Catalog.query.filter(Catalog.organization_id.in_(ord_ids)).offset(offset).limit(limit)
    else:
        query = entity.catalogs.offset(offset).limit(limit)

    return MaListResponse(
        query=query,
        schema=CatalogSchema(),
        key_name="catalogs",
        filter_parsers=SharedCatalogArgsParser()
    ).response()


# @only_child_organizations
@blueprint.route('/<shared_library:entity>/shared-catalogs', methods=['POST', ])
@is_organization_manager
def create_catalog(entity):
    """ Creates a new `Catalog` for a given organization.

    :param `SharedLibrary` entity:
    """
    session = db.session()
    catalog = schema_load(schema=CatalogSchema(), session=session)
    if not catalog.shared_library:
        entity.catalogs.append(catalog)
    session.commit()
    session.refresh(catalog)

    resp = jsonify(CatalogSchema().dump(catalog).data)
    resp.headers['Location'] = catalog.api_url
    resp.status_code = CREATED
    return resp


@blueprint.route('/<shared_library:entity>/shared-catalogs/<catalog:catalog>', methods=['PUT', ])
@is_organization_manager
def update_catalog(entity, catalog):
    """
    :param `SharedLibrary` entity:
    :param `Catalog` catalog:
    """
    return update_response(schema=CatalogSchema(), instance=catalog)


def catalog_organization_permissions(catalog, entity):

    # Validate organization can access specific catalog, related to organization.
    if entity.parent_organization_id:
        # child organization.
        data_org = organization_list_parent(entity.parent_organization)
    else:
        data_org = organization_list_parent(entity)

    organization_ids = [org.id for org in data_org]
    return False if catalog.organization_id not in organization_ids else True


@blueprint.route('/<shared_library:entity>/shared-catalogs/<catalog:catalog>')
# @is_organization_member
def search_catalog_items(entity, catalog):
    """ Details of a catalog or search item in a catalog

    :param entity:
    :param catalog:
    :return:
    """
    session = db.session()
    direct_db = request.args.get('direct-db', type=int, default=0)
    offset = request.args.get('offset', type=int, default=0)
    limit = request.args.get('limit', type=int, default=20)
    header_user_agent = request.headers.get('User-Agent', None)
    api_version = request.headers.get('version', '1.0')

    valid_access = catalog_organization_permissions(catalog, entity)
    if not valid_access:
        raise Forbidden(developer_message='Invalid access catalog permissions!')

    if direct_db in [None, 1]:
        query = session.query(CatalogItem).filter_by(catalog_id=catalog.id, is_active=True)
        count = query.count()
        items = query.offset(offset).limit(limit).all()
        facet_values = None
        spell_check = None
    else:
        # Api version 2.0 only for T-eperpus, this eperpus allowed to display items from scoop-content SOLR.
        if api_version == '2.0':
            solr_gateway = SolrGateway(current_app.config['SOLR_BASE_URL'], 'scoop-content', requests)
            request_arg_ =  request.args.to_dict()

            qsearch = request_arg_.get('q', None)
            if qsearch:
                request.args = ImmutableMultiDict([('q', qsearch), ('eperpus-id', entity.id), ('exclude-free-item', 'true')])
            else:
                request.args = ImmutableMultiDict([('eperpus-id', entity.id), ('exclude-free-item', 'true')])
            item_query = ItemQueryBuilder(flask_req=request, facet_list=["author_names", "category_names", "languages",], version='2.0')
            solr_query = item_query.build()

            facets = request_arg_.get('facets', None)
            if facets:
                fq = solr_query.get('fq')
                fq.append(facets)

            solr_query['start'] = offset
            solr_query['rows'] =limit
            response_json = solr_gateway.search(solr_query)
        else:
            user_agent = header_user_agent.split('/')[0].replace(' ', '_').strip()
            response_json = get_catalog_items_search(req=request, catalog_id=catalog.id, user_agent=user_agent)

        count, facet_values, spell_check, items = extract_solr_response(catalog, response_json, session, api_version, entity)

    # session.add(catalog)
    if api_version == '2.0':
        items_response = items
    else:
        items_response = [marshal(SharedCatalogItemsViewModel(item), SharedCatalogListItemsApiSerializer()) for item in items]

    return jsonify({
        'id': catalog.id,
        'name': catalog.name,
        'items': items_response,
        "organization": {
            "id": catalog.shared_library.id,
            "name": catalog.shared_library.name,
            "href": catalog.shared_library.api_url
        },
        'metadata': {
            'resultset': {
                'offset': offset,
                'limit': limit,
                'count': count
            },
            "facets": facet_values,
            "spelling_suggestions": spell_check
        }
    })


def extract_solr_response(catalog, response_json, session, api_version, organization):

    count = response_json.get('response', {}).get('numFound', 0)
    if count <= 0:
        q_param = '{}*'.format(request.args.get('q', '*'))
        response_json = get_catalog_items_search(req=request, catalog_id=catalog.id, q_param=q_param)
        count = response_json.get('response', {}).get('numFound', 0)

    facet_name_mapper = CorDefaultDict(category_names='item_categories', author_names='item_authors', languages='item_languages')
    facet_items = response_json['facet_counts']['facet_fields'].items() if 'facet_counts' in response_json else []

    facet_values = []
    if facet_items:
        facet_values = [{ "field_name": facet_name_mapper[key],
            "values": [{"value": t[0], "count": t[1]} for t in zip(facet[0::2], facet[1::2])]} for key, facet in facet_items]

    spell_check = []
    if response_json.get('spellcheck', {'correctlySpelled': True})['correctlySpelled'] == 'false':
        suggestions = response_json['spellcheck']['suggestions'][1]['suggestion']
        spell_check = [{"value": data['word'], "count": data['freq']} for data in suggestions]

    items = []
    if api_version == '2.0':
        for item in response_json.get('response', {}).get('docs', {}):
            item = session.query(Item).filter_by(id=item["id"], item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).first()

            if item:
                data_response = V2CatalogItem(item, catalog, organization)
                items.append({
                    "is_owned_by_organization": data_response.is_owned_by_organization,
                    "borrow_item": {"href": data_response.href_borrowed_item().href, "title": data_response.href_borrowed_item().title},
                    "cover_image": {"href": data_response.href_cover_image().href, "title": data_response.href_cover_image().title},
                    "currently_available": data_response.currently_available(),
                    "details": {"id": data_response.href_details().id, "href": data_response.href_details().href,
                                "title": data_response.href_details().title},
                    "file_size": data_response.file_size(),
                    "href": data_response.href_catalog_item_detail().href,
                    "id": item.id,
                    "item_type": item.item_type,
                    "max_borrow_time": data_response.max_borrowed_time(),
                    "new_subtitle": item.issue_number,
                    "new_title": item.brand.name,
                    "rating": data_response.avg_rating(),
                    "subtitle": data_response.subtitle(),
                    "title": data_response.item_title(),
                    "total_in_collection": data_response.total_in_collection(),
                    "vendor": {"href": data_response.href_vendor().href, "title": data_response.href_vendor().title}
                })
    else:
        for doc in response_json.get('response', {}).get('docs', {}):
            item = session.query(CatalogItem).filter_by(catalog_id=catalog.id,shared_library_item_item_id=doc.get('item_id')).first()
            if item: items.append(item)
    return count, facet_values, spell_check, items

@blueprint.route('/<shared_library:entity>/shared-catalogs/<catalog:catalog>/suggest')
@is_organization_member
def catalog_item_suggestions_by_brand(entity, catalog):
    return jsonify(get_brand_name_suggestion(request, catalog.id))


@blueprint.route('/<shared_library:entity>/shared-catalogs/<catalog:catalog>', methods=['LINK', ])
@is_organization_manager
def add_item_to_catalog(entity, catalog):
    """ Adds an existing SCOOP item to a catalog.

    :param `Organization` entity:
    :param `Catalog` catalog:
    """
    s = db.session()
    item_id = request.json.get('id')
    item_to_add = s.query(Item).get(item_id)
    if not item_to_add:
        raise UnprocessableEntity(developer_message='Item with id {} not found!'.format(item_id))

    stock_org = get_stock_holding_org(entity)

    previous_catalog_item = s.query(CatalogItem).filter_by(
        catalog_id=catalog.id,
        shared_library_item_org_id=stock_org.id,
        shared_library_item_item_id=item_id
    ).first()
    if not previous_catalog_item:
        # if not yet exists, add new catalog item
        catalog_item = CatalogItem(
            catalog=catalog, shared_library_item_org_id=stock_org.id, shared_library_item_item_id=item_id
        )
        s.add(catalog_item)
    elif not previous_catalog_item.is_active:
        previous_catalog_item.is_active = True
        s.add(previous_catalog_item)

    # change modified for solr update
    update_organization_item_modified(item_id, stock_org.id, session=s)

    # try:
    #     commented out, portal/cms will execute it manually after ALL link/unlink done!
    #     update_solr_library_catalog_item()
    # except:
    #     pass

    return make_response('{"success_message": "Added items"}', CREATED)


def update_organization_item_modified(item_id, organization_id, session):
    db.engine.execute(
        "update core_organization_items set modified=now() where "
        "organization_id={organization_id} and "
        "item_id={item_id}".format(
            organization_id=organization_id,
            item_id=item_id
        ))
    session.commit()


@blueprint.route('/<shared_library:entity>/shared-catalogs/<catalog:catalog>', methods=['UNLINK', ])
@is_organization_manager
def remove_item_from_catalog(entity, catalog):
    """ Removes an item from a Catalog.

    :param `Organization` entity:
    :param `Catalog` catalog:
    """
    s = db.session()
    item_id = request.json.get('id')
    stock_org = get_stock_holding_org(entity)
    if item_id:
        db.engine.execute("UPDATE core_catalog_items SET is_active = False where catalog_id={catalog_id} and "
                          "shared_library_item_org_id={shared_library_item_org_id} and "
                          "shared_library_item_item_id={item_id}".format(
            catalog_id=catalog.id,
            shared_library_item_org_id=stock_org.id,
            item_id=item_id))

    # change modified for solr update
    update_organization_item_modified(item_id, stock_org.id, session=s)

    return make_response('', NO_CONTENT)


@blueprint.route('/<shared_library:entity>/shared-catalogs/<catalog:catalog>/<item:item>')
# @is_organization_member
def catalog_item_details(entity, catalog, item):

    api_version = request.headers.get('version', '1.0')

    valid_access = catalog_organization_permissions(catalog, entity)
    if not valid_access:
        raise Forbidden(developer_message='Invalid access catalog permissions!')

    if api_version == '2.0':
        data_response = V2CatalogItem(item=item, catalog=catalog, organization=entity)
        response_json = {
            "authors": data_response.href_author(),
            "borrow_item": {"href": data_response.href_borrowed_item().href, "title": data_response.href_borrowed_item().title},
            "brand": {"href": data_response.href_brand().href, "id": data_response.href_brand().id,
                      "title": data_response.href_brand().title},
            "catalog": {"href": data_response.href_catalog().href, "id": data_response.href_catalog().id,
                        "title": data_response.href_catalog().title},
            "categories": data_response.categories_eperpus(),
            "cover_image": {"href": data_response.href_cover_image().href, "title": data_response.href_cover_image().title},
            "currently_available": data_response.currently_available(),
            "description": item.description if item.description else "",
            "details": {"href": data_response.href_details().href, "id": data_response.href_details().id,
                        "title": data_response.href_details().title},
            "edition_code": item.edition_code,
            "file_size": data_response.file_size(),
            "high_res_image": {"href": data_response.href_high_res_image().href, "title": data_response.href_high_res_image().title},
            "href": data_response.href_catalog_item_detail().href,
            "id": item.id,
            "is_owned_by_organization": data_response.is_owned_by_organization,
            "item_type": item.item_type,
            "max_borrow_time": data_response.max_borrowed_time(),
            "new_subtitle": item.issue_number,
            "new_title": item.brand.name,
            "next_available": data_response.next_available_time(),
            "organization_id": entity.id,
            "page_count": item.page_count if item.page_count else 0,
            "previews": data_response.href_previews(),
            "rating": data_response.avg_rating(),
            "review_status": data_response.review_status(),
            "subtitle": data_response.subtitle(),
            "title": data_response.item_title(),
            "total_in_collection": data_response.total_in_collection(),
            "vendor": {"href": data_response.href_vendor().href, "title": data_response.href_vendor().title},
            "watch_list": data_response.href_watchlist()}
        return make_response(jsonify(response_json), OK)
    else:
        catalog_item = CatalogItem.query.filter(CatalogItem.catalog_id == catalog.id,
            CatalogItem.shared_library_item_item_id == item.id, CatalogItem.is_active == True).first()

        if not catalog_item: raise NotFound
        return jsonify(marshal(SharedCatalogItemDetailsViewModel(catalog_item),
                               SharedCatalogListItemDetailsApiSerializer()))


class CatalogBrand(db.Model):
    """ Mapping table for OrganizationBrands that are available on a Brand.
    """
    __tablename__ = 'core_catalog_brands'

    catalog_id = db.Column(db.Integer, db.ForeignKey("core_catalogs.id"), primary_key=True)
    catalog = db.relationship(Catalog, backref='brands')

    shared_library_brand_org_id = db.Column(db.Integer, nullable=False, primary_key=True)
    shared_library_brand_brand_id = db.Column(db.Integer, nullable=False, primary_key=True)

    shared_library_brand = db.relationship("SharedLibraryBrand", backref='brands')

    __table_args__ = (
        db.ForeignKeyConstraint(
            ['shared_library_brand_org_id', 'shared_library_brand_brand_id'],
            ['core_organization_brands.organization_id', 'core_organization_brands.brand_id'],
            name='core_catalog_brands_organization_brand_fkey'
        ),
    )


def get_estimated_next_available_time(session, catalog_item):
    """ Makes a best guess at the next time a catalog item will be available.

    :param `sqlalchemy.orm.Session` session:
    :param `CatalogItem` catalog_item:
    :rtype: datetime
    :return:
    """
    if catalog_item.shared_library_item.quantity_available > 0:
        # the item is currently available, so there's no need for next available date
        return None

    # having problems with relationships.  I have something else screwed up.
    shared_lib = session.query(SharedLibrary).get(catalog_item.shared_library_item_org_id)
    max_borrowing_time = shared_lib.borrowing_time_limit
    if not max_borrowing_time:
        return None

    from app.eperpus.members import UserBorrowedItem
    earliest_borrowed_item = (
        session.query(UserBorrowedItem)
            .filter_by(
            catalog_item=catalog_item,
            returned_time=None
        )
            .order_by(db.asc(UserBorrowedItem.id))
            .first()
    )
    if not earliest_borrowed_item:
        return None

    return earliest_borrowed_item.borrowing_start_time + max_borrowing_time


class SharedCatalogListItemsApiSerializer(SerializerBase):
    id = fields.Integer
    title = fields.String
    subtitle = fields.String
    item_type = fields.String
    href = fields.String
    currently_available = fields.Integer
    total_in_collection = fields.Integer
    max_borrow_time = fields.String
    details = fields.Nested(SimpleImplicitHrefWithId())
    cover_image = fields.Nested(SimpleImplicitHref())
    borrow_item = fields.Nested(SimpleImplicitHref())
    vendor = fields.Nested(SimpleImplicitHref())
    file_size = fields.String
    rating = fields.Nested(RatingSerializer())
    new_title = fields.String
    new_subtitle = fields.String
    is_owned_by_organization = fields.Boolean


class SharedCatalogListItemDetailsApiSerializer(SharedCatalogListItemsApiSerializer):
    catalog = fields.Nested(SimpleImplicitHrefWithId())
    next_available = fields.DateTime('iso8601')
    brand = fields.Nested(SimpleImplicitHrefWithId())
    authors = fields.List(fields.Nested(SimpleImplicitHrefWithId()))
    description = fields.String
    watch_list = fields.Raw
    high_res_image = fields.Nested(SimpleImplicitHref())
    previews = fields.Raw
    organization_id = fields.Integer
    edition_code = fields.String
    page_count = fields.Integer
    review_status = fields.Boolean
    categories = fields.List(fields.Nested(SimpleImplicitHrefWithId()))
    is_owned_by_organization = fields.Boolean

class SharedCatalogListItemsApiArgsParser(parsers.SqlAlchemyFilterParser):
    __model__ = CatalogItem
    name = parsers.StringFilter()

class SharedCatalogArgsParser(parsers.SqlAlchemyFilterParser):
    __model__ = Catalog
    q = WildcardFilter(dest='name')
    name = parsers.StringFilter()
    organization_id = parsers.IntegerFilter()

class SharedCatalogItemsViewModel(viewmodels.SaViewmodelBase):

    def __init__(self, model_instance):
        super(SharedCatalogItemsViewModel, self).__init__(model_instance)
        org_item = self._model_instance.shared_library_item
        self.href = self._model_instance.api_url
        self.item = org_item.item
        self.currently_available = org_item.quantity_available
        self.total_in_collection = org_item.quantity
        self.max_borrow_time = FORMAT_ISO8601_DURATION.format(org_item.shared_library.borrowing_time_limit)
        self.id = self.item.id
        # details -> id will be used for download api
        self.details = HrefAttributeWithId(title='Detil Item', href=self.item.api_url, id=self.item.id)
        self.borrow_item = HrefAttribute(
            title='Pinjam Item',
            href=url_for('shared_library_users.update_user_current_borrowed_items', _external=True))
        self.cover_image = HrefAttribute(
            title='Gambar Sampul',
            href=preview_s3_covers(self.item, 'image_normal', True)
        )
        self.vendor = HrefAttribute(
            title=self.item.brand.vendor.name,
            href=get_url_for_model(self.item.brand.vendor)
        )
        self.rating = RatingAttribute(
            **self.rating_result
        )
        self.item_type = self.item.item_type

    @property
    def is_owned_by_organization(self):
        return True

    @property
    def subtitle(self):
        if self.item and self.item.item_type == self.item.Types.book.value:
            return self.item.brand.vendor.name
        else:
            return self.item.issue_number if self.item else None

    @property
    def title(self):
        if self._model_instance.shared_library_item.item_name_override:
            return self._model_instance.shared_library_item.item_name_override
        else:
            return self.item.name

    @property
    def file_size(self):
        file_size = self.item.file_size if self.item.file_size else 0
        return naturalsize(file_size) if file_size > 0 else '- MB'

    @property
    def rating_result(self):
        average, count = get_avg_and_total_rating(self.item.id)
        return {'average': average, 'total': count}

    @property
    def new_title(self):
        return self.item.brand.name

    @property
    def new_subtitle(self):
        return self.item.issue_number


class SharedCatalogItemDetailsViewModel(SharedCatalogItemsViewModel):

    def __init__(self, model_instance):
        super(SharedCatalogItemDetailsViewModel, self).__init__(model_instance)
        self.next_available = get_estimated_next_available_time(db.session(), self._model_instance)
        self.brand = HrefAttributeWithId(id=self.item.brand.id, title=self.item.brand.name, href=self.item.brand.api_url)
        self.authors = [HrefAttributeWithId(id=a.id, title=a.name, href=get_url_for_model(a)) for a in self.item.authors.all()]
        self.description = self.item.description

        watch_list = db.session.query(WatchList).filter(
            WatchList.user_id == g.current_user.id,
            WatchList.item_id == self.item.id,
            WatchList.catalog_id == self._model_instance.catalog_id,
            WatchList.end_date == None,
            WatchList.is_borrowed == False
        ).first()
        self.watch_list = {"id": watch_list.id, "title": "watch list",
                           "href": watch_list.api_url} if watch_list else None

        self.catalog = HrefAttributeWithId(id=self._model_instance.catalog.id, title=self._model_instance.catalog.name,
                                           href=self._model_instance.catalog.api_url)
        self.high_res_image = HrefAttribute(title='Gambar Sampul High Res',
                                            href=preview_s3_covers(self.item, 'image_highres', True))
        data_preview = preview_s3_urls(self.item)
        if data_preview: preview = [{'href': i, 'title': os.path.basename(i).split('.')[0]} for i in data_preview]
        else:preview = [{'href': preview_s3_covers(self.item, 'image_highres', True), 'title': "1"}]
        self.previews = preview

        self.organization_id = self._model_instance.catalog.organization_id
        self.edition_code = self.item.edition_code
        self.page_count = self.item.page_count if self.item.page_count else 0
        self.categories = self.eperpus_categories()

    def eperpus_categories(self):
        from app.eperpus.categories import CategoryEperpusItem, CategoryEperpus

        cat_eperpus_item = CategoryEperpusItem.query.filter_by(organization_id=self._model_instance.catalog.organization_id,
            item_id=self.item.id).all()

        data_categories = []
        for icat in cat_eperpus_item:
            category_eperpus = CategoryEperpus.query.filter_by(organization_id=icat.organization_id,
                                                               category_id=icat.category_id).first()
            if category_eperpus:
                data_categories.append({'id': category_eperpus.category_id, 'title': category_eperpus.name,
                                        'href': get_url_for_model(category_eperpus.category)})
        return data_categories

    @property
    def review_status(self):
        return Review.query.filter_by(user_id=g.user.user_id, item_id=self.item.id).count() > 0

def assert_catalog_item(session, catalog, item_id, stock_org):
    catalog_item = session.query(CatalogItem).filter(
        CatalogItem.catalog_id == catalog.id,
        CatalogItem.shared_library_item_org_id == stock_org.id,
        CatalogItem.shared_library_item_item_id == item_id,
        CatalogItem.is_active == True
    ).first()

    if not catalog_item:
        msg = 'Catalog Item for catalog id {}, shared library {}, and item {} Not Found!'.format(
            catalog.id, stock_org.id, item_id
        )
        raise BadRequest(user_message=msg, developer_message=msg)
    return catalog_item


def assert_qty_available(session, stock_org_id, item_id):
    shared_lib_item = session.query(SharedLibraryItem).filter(
        SharedLibraryItem.organization_id == stock_org_id,
        SharedLibraryItem.item_id == item_id,
        SharedLibraryItem.is_active == True
    ).first()

    if not shared_lib_item or shared_lib_item.quantity_available <= 0:
        msg = 'Item {item_id} in shared library {org_id}: Quantity Available = {qty}'.format(
            item_id=item_id,
            org_id=stock_org_id,
            qty=shared_lib_item.quantity_available if shared_lib_item else 0)
        raise BadRequest(user_message='Stok kosong.',
                         developer_message=msg)


class ListCatalogSchema(ma.Schema):
    from marshmallow import fields

    name = fields.String(required=True)
    group = fields.Integer(required=False)
    is_internal_catalog = fields.Boolean(required=False, default=False)


@blueprint.route('/<organization:entity>/catalogs', methods=['GET', 'POST'])
@is_organization_manager
def organization_catalogs(entity):
    if request.method == 'GET':

        def get_stock(organization_id, items_id):
            if items_id is not None:
                sql = """
                    select sum(quantity) from core_organization_items where organization_id={org_id} and item_id in ('{items_id}')
                """.format(org_id=organization_id, items_id="','".join(str(o) for o in items_id))
                data = db.engine.execute(sql)
                return data.fetchone()[0]
            return 0

        args = request.args.to_dict()
        q = args.get('q', None)
        limit, offset, search, data = args.get('limit', 20), args.get('offset', 0), '', []

        child = Organization.query.filter_by(parent_organization_id=entity.id).all()
        data_organization = [entity.id] + [i.id for i in child]
        list_catalog = Catalog.query.filter(Catalog.organization_id.in_(data_organization)).order_by(desc(Catalog.id))

        total_count = len(list_catalog.all())
        if q:
            list_catalog = list_catalog.filter(Catalog.name.ilike('%{}%'.format(q)))

        if limit != u'-1':
            list_catalog = list_catalog.limit(limit).offset(offset)

        data_catalog = []
        for idata in list_catalog:
            total_item = CatalogItem.query.filter_by(catalog_id=idata.id, is_active=True).all()
            items_id = [i.shared_library_item_item_id for i in total_item]
            total_stock = get_stock(entity.id, items_id if len(items_id) > 0 else None)

            data_catalog.append({'name': idata.name, 'catalog_id': idata.id, 'organization_id': idata.organization_id,
                                 'total_item': len(total_item), 'total_stock': total_stock if total_stock else 0,
                                 'is_internal_catalog': idata.is_internal_catalog})

        metadata = {"resultset": {'count': total_count, 'limit': int(limit), 'offset': int(offset)}}
        return make_response(jsonify({'items': data_catalog, 'metadata': metadata}), OK)

    if request.method == 'POST':
        catalog_schema = ListCatalogSchema()
        data, errors = catalog_schema.load(request.get_json())
        if errors:
            raise BadRequest(user_message='Bad request format.', developer_message='Bad request format.')

        valid_organization = validate_catalog_request(data, entity)
        new_catalogs = Catalog(
            name=data.get('name', ""),
            organization_id=valid_organization.id,
            is_internal_catalog=data.get("is_internal_catalog", False))

        db.session.add(new_catalogs)
        db.session.commit()

        return make_response(jsonify(new_catalogs.values()), CREATED)


@blueprint.route('/<organization:entity>/catalogs/<catalog_id>', methods=['GET', 'PUT'])
@is_organization_manager
def organization_catalogs_detail(entity, catalog_id):
    if request.method == 'GET':
        data_catalog = Catalog.query.filter_by(id=catalog_id).first()
        if not data_catalog:
            raise NotFound
        return make_response(jsonify(data_catalog.values()), OK)

    if request.method == 'PUT':
        from app.users.organizations import Organization
        catalog_schema = ListCatalogSchema()
        data, errors = catalog_schema.load(request.get_json())

        if errors:
            raise BadRequest(user_message='Bad request format.', developer_message='Bad request format.')

        exist_catalog = Catalog.query.filter_by(id=catalog_id).first()
        if not exist_catalog: raise NotFound

        update_name = data.get('name', '')
        if update_name != exist_catalog.name:
            duplicate_catalog = check_duplicate_catalog(entity,update_name)
            if duplicate_catalog:
                raise BadRequest(user_message='Invalid catalog name, already exist!',
                                developer_message='Invalid catalog name, already exist!')
        # #validate name...
        exist_catalog.name = update_name
        db.session.add(exist_catalog)
        db.session.commit()
        return make_response(jsonify(exist_catalog.values()), OK)


def validate_catalog_request(data, entity):
    from app.users.organizations import Organization

    # check if organizations related with entity
    organization = Organization.query.filter_by(id=data.get('group', 0)).first()
    data_organization = [entity]
    if not organization:
        raise BadRequest(user_message='Invalid group, Not found!.', developer_message='Invalid group, Not found!.')

    if not organization.parent_organization_id:
        data_organization.append(organization)
        if organization.id != entity.id:  # hacked!!
            raise BadRequest(user_message='Invalid group, Not related with organization!.',
                             developer_message='Invalid group, Not related with organization!.')
    else:
        all_child = Organization.query.filter_by(parent_organization_id=organization.parent_organization_id).all()
        data_organization = data_organization + all_child

        # this is mean child organization
        if organization.parent_organization_id != entity.id:
            raise BadRequest(user_message='Invalid group, Not related with organization!.',
                             developer_message='Invalid group, Not related with organization!.')

    catalog_name = data.get('name', '')
    duplicate_catalog = check_duplicate_catalog(entity, catalog_name)
    if duplicate_catalog:
        raise BadRequest(user_message='Invalid catalog name, already exist!',
                         developer_message='Invalid catalog name, already exist!')
    return organization


def check_duplicate_catalog(entity, catalog_name):
    from app.eperpus.members import get_all_child_orgs
    all_related_org = get_all_child_orgs([entity, ])
    return Catalog.query.filter(Catalog.name == catalog_name, Catalog.organization_id.in_(all_related_org)).first()



@blueprint.route('/<shared_library:entity>/shared-catalogs/<catalog:catalog>/<item:item>/reviews', methods=['GET'])
# @is_organization_member
def catalog_item_reviews(entity, catalog, item):

    valid_access = catalog_organization_permissions(catalog, entity)
    if not valid_access:
        raise Forbidden(developer_message='Invalid access catalog permissions!')

    reviews = Review.query.filter(Review.item_id == item.id, Review.is_active).order_by(desc(Review.created))
    if not reviews:
        raise NotFound
    result = []
    limit = request.args.get('limit', 3, type=int)
    offset = request.args.get('offset', 0, type=int)

    for review in reviews.limit(limit).offset(offset):
        email = "{email}***".format(email=review.user.email[0:4])
        result.append(
            {'review': review.review, 'rating': review.rating, 'email': email,
             'review_date': review.created.isoformat(), 'id': review.id,
             'report_href': review.report_review_url})

    metadata = get_rating_metadata(item.id)
    average, total = get_avg_and_total_rating(item.id)
    meta = {
        'one': metadata[0],
        'two': metadata[1],
        'three': metadata[2],
        'four': metadata[3],
        'five': metadata[4],
        'average': average,
        'total': total
    }
    return make_response(jsonify(dict(data=result, metadata=meta)), OK)


class V2CatalogItem(object):
    """
        This class prepare for display all ebook items on EPERPUS!!
        Fucking Trouble!
    """

    def __init__(self, item, catalog, organization):

        self.catalog_item = CatalogItem.query.filter_by(shared_library_item_item_id=item.id, catalog_id=catalog.id).first()
        self.is_owned_by_organization = True if self.catalog_item else False
        self.item = self.catalog_item.shared_library_item.item if self.catalog_item else item
        self.organization = organization
        self.catalog = catalog

    def is_owned_by_organization(self):
        return self.is_owned_by_organization

    def subtitle(self):
        if self.item.item_type == self.item.Types.book.value: subtitle = self.item.brand.vendor.name
        else: subtitle = self.item.issue_number
        return subtitle

    def href_catalog_item_detail(self):
        return HrefAttribute(title='Catalog Item Detail', href=url_for(
            'eperpus_catalogs.catalog_item_details', entity=self.organization, catalog=self.catalog, item=self.item, _external=True))

    def href_borrowed_item(self):
        return HrefAttribute(title='Pinjam Item', href=url_for(
            'shared_library_users.update_user_current_borrowed_items', _external=True))

    def href_vendor(self):
        return HrefAttribute(title=self.item.brand.vendor.name, href=get_url_for_model(self.item.brand.vendor))

    def href_cover_image(self):
        return HrefAttribute(title='Gambar Sampul', href=preview_s3_covers(self.item, 'image_normal', True))

    def href_high_res_image(self):
        return HrefAttribute(title='Gambar Sampul High Res', href=preview_s3_covers(self.item, 'image_highres', True))

    def href_details(self):
        return HrefAttributeWithId(title='Detil Item', href=self.item.api_url, id=self.item.id)

    def href_brand(self):
        return HrefAttributeWithId(id=self.item.brand.id, title=self.item.brand.name, href=self.item.brand.api_url)

    def href_author(self):
        author_list = []

        for author in self.item.authors.all():
            author_href = HrefAttributeWithId(id=author.id, title=author.name, href=get_url_for_model(author))
            author_list.append({'id': author_href.id, 'title': author_href.title, 'href': author_href.href})
        return author_list

    def href_catalog(self):
        return HrefAttributeWithId(id=self.catalog.id, title=self.catalog.name, href=self.catalog.api_url)

    def href_previews(self):
        data_preview = preview_s3_urls(self.item)
        if data_preview:
            preview = [{'href': i, 'title': os.path.basename(i).split('.')[0]} for i in data_preview]
        else:
            preview = [{'href': preview_s3_covers(self.item, 'image_highres', True), 'title': "1"}]
        return preview

    def href_watchlist(self):
        watch_list = db.session.query(WatchList).filter(WatchList.user_id == g.current_user.id,
            WatchList.item_id == self.item.id, WatchList.catalog_id == self.catalog.id,
            WatchList.end_date == None, WatchList.is_borrowed == False).first()
        return {'id': watch_list.id, 'title': "watch list", "href": watch_list.api_url} if watch_list else None

    def file_size(self):
        return naturalsize(self.item.file_size if self.item.file_size else 0) if self.item.file_size > 0 else '- MB'

    def avg_rating(self):
        rating_average, rating_count = get_avg_and_total_rating(self.item.id)
        return {"average": rating_average, "total": rating_count}

    def item_title(self):
        if self.is_owned_by_organization:
            return self.catalog_item.shared_library_item.item_name_override \
                if self.catalog_item.shared_library_item.item_name_override else self.item.name
        else:
            return self.item.name

    def max_borrowed_time(self):
        return FORMAT_ISO8601_DURATION.format(self.catalog_item.shared_library_item.shared_library.borrowing_time_limit) \
            if self.is_owned_by_organization else ''

    def currently_available(self):
        return self.catalog_item.shared_library_item.quantity_available if self.is_owned_by_organization else 0

    def total_in_collection(self):
        return self.catalog_item.shared_library_item.quantity if self.is_owned_by_organization else 0

    def next_available_time(self):
        if self.is_owned_by_organization:
            data_time = get_estimated_next_available_time(db.session(), self.catalog_item)
            return data_time if data_time else ""
        return ""

    def review_status(self):
        return Review.query.filter_by(user_id=g.user.user_id, item_id=self.item.id).count() > 0

    def categories_eperpus(self):
        return [{'id': cat_.id, 'title': cat_.name, 'href': None} for cat_ in self.item.categories.all()]
