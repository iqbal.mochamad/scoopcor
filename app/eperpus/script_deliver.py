from sqlalchemy import desc

from app.eperpus.helpers import update_eperpus_categories
from app.items.choices import STATUS_TYPES, STATUS_READY_FOR_CONSUME
from app.items.models import Item
from app.eperpus.catalogs import CatalogBrand
from app.eperpus.libraries import SharedLibraryBrand, SharedLibraryItem
from app.eperpus.models import CatalogItem
from datetime import datetime, timedelta
from app import app, db

class DeliverSubsEperpus(object):

    def __init__(self):
        self.data_item = []

    def create_organization_items(self, shared_data, items=[], type_ed=''):
        edition_data, data_item = [], []
        for exist_items in items:
            exist_org_item = SharedLibraryItem.query.filter_by(
                organization_id= shared_data.organization_id, item_id=exist_items.id).first()

            if not exist_org_item:
                org_item = SharedLibraryItem(
                    organization_id=shared_data.organization_id,
                    item_id=exist_items.id,
                    quantity=shared_data.quantity,
                    quantity_available=shared_data.quantity,
                    created=datetime.now(),
                    modified=datetime.now() + timedelta(hours=7))

                if type_ed == 'edition': edition_data.append(org_item)
                data_item.append(exist_items.id)
                db.session.add(org_item)
                db.session.commit()

            catalog = CatalogBrand.query.filter_by(
                shared_library_brand_org_id=shared_data.organization_id,
                shared_library_brand_brand_id=shared_data.brand_id).first()

            if catalog:
                exist_catalog_item = CatalogItem.query.filter_by(
                    catalog_id=catalog.catalog_id,
                    shared_library_item_org_id=shared_data.organization_id,
                    shared_library_item_item_id=exist_items.id).first()

                if not exist_catalog_item:
                    catalog_item = CatalogItem(
                        catalog_id=catalog.catalog_id,
                        shared_library_item_org_id=shared_data.organization_id,
                        shared_library_item_item_id=exist_items.id)
                    db.session.add(catalog_item)
                    db.session.commit()

        # update categories eperpus
        update_eperpus_categories(data_item=data_item, organization_id=shared_data.organization_id)

        if type_ed == 'edition':
            next_stock = shared_data.current_offer_stock - len(edition_data)
            if next_stock <= 0: next_stock = 0
            shared_data.current_offer_stock = next_stock
            db.session.add(shared_data)
            db.session.commit()

    def deliver_duration_eperpus(self):
        duration_data = SharedLibraryBrand.query.filter(
            SharedLibraryBrand.valid_to >= datetime.now(),
            SharedLibraryBrand.current_offer_type == 'duration'
        ).order_by(desc(SharedLibraryBrand.valid_to)).all()

        # # get items by old duration data.
        for data in duration_data:
            if data.latest_added_item_id:
                # get data item, filter from latest deliver ID
                data_item = Item.query.filter(
                    Item.release_date >= data.valid_from,
                    Item.release_date <= data.valid_to,
                    Item.brand_id == data.brand_id,
                    Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                    Item.id > data.latest_added_item_id
                ).order_by(desc(Item.id)).all()
            else:
                # get data Item, this case next items.( No latest delivery ID )
                data_item = Item.query.filter(
                    Item.release_date >= data.valid_from,
                    Item.release_date <= data.valid_to,
                    Item.brand_id == data.brand_id,
                    Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME]
                ).order_by(desc(Item.id)).all()

            self.create_organization_items(shared_data=data, items=data_item, type_ed='duration')
            latest_item = data_item[0] if data_item else None
            if latest_item:
                data.latest_added_item_id = latest_item.id
                db.session.add(data)
                db.session.commit()

    def deliver_edition_eperpus(self):
        edition_data = SharedLibraryBrand.query.filter(
            SharedLibraryBrand.current_offer_stock > 0,
            SharedLibraryBrand.current_offer_type == 'edition'
        ).order_by(desc(SharedLibraryBrand.valid_to)).all()

        for data_ed in edition_data:
            data_item = Item.query.filter(
                Item.release_date >= data_ed.valid_from,
                Item.brand_id == data_ed.brand_id,
                Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME]
            ).order_by(desc(Item.id)).limit(data_ed.offer_stock).all()

            self.create_organization_items(shared_data=data_ed, items=data_item, type_ed='edition')
            latest_item = data_item[0] if data_item else None

            if latest_item:
                data_ed.latest_added_item_id = latest_item.id
                db.session.add(data_ed)
                db.session.commit()

    def deliver(self):
        self.deliver_duration_eperpus()
        self.deliver_edition_eperpus()
