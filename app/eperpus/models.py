from flask import url_for
from flask.ext.appsfoundry.models import TimestampMixin
from humanize import naturalsize
import operator
from app import db
from app.eperpus.libraries import SharedLibraryItem, SharedLibrary
from app.items.models import Item
from app.users.organizations import Organization
from app.users.users import User

from sqlalchemy.dialects.postgresql import JSONB


class Catalog(TimestampMixin, db.Model):
    """ A user-defined collection of `Item`s that can be borrowed from a `SharedLibrary`
    """
    __tablename__ = 'core_catalogs'
    name = db.Column(db.Text, nullable=False)
    organization_id = db.Column(db.Integer, db.ForeignKey("cas_organizations.id"), nullable=False)
    shared_library = db.relationship(SharedLibrary, backref=db.backref('catalogs', lazy='dynamic'))
    is_internal_catalog = db.Column(db.Boolean, default=False)

    @property
    def api_url(self):
        return url_for('eperpus_catalogs.search_catalog_items', entity=self.shared_library, catalog=self,
                       _external=True)

    def values(self):
        return {
            'id': self.id,
            'name': self.name,
            'organization_id': self.organization_id,
            'is_internal_catalog': self.is_internal_catalog
        }


class CatalogItem(db.Model):
    """ Mapping table for OrganizationItems that are available on an Item.
    """
    __tablename__ = 'core_catalog_items'

    catalog_id = db.Column(db.Integer, db.ForeignKey("core_catalogs.id"), primary_key=True)
    catalog = db.relationship(Catalog, backref='catalog_items')

    shared_library_item_org_id = db.Column(db.Integer, nullable=False, primary_key=True)
    shared_library_item_item_id = db.Column(db.Integer, nullable=False, primary_key=True)

    shared_library_item = db.relationship(SharedLibraryItem, backref=db.backref('catalog_items', lazy='dynamic'))

    is_active = db.Column(db.Boolean, nullable=False, default=True)

    __table_args__ = (
        db.ForeignKeyConstraint(
            ['shared_library_item_org_id', 'shared_library_item_item_id'],
            ['core_organization_items.organization_id', 'core_organization_items.item_id'],
            name='core_catalog_items_organization_item_fkey'),
    )

    @property
    def api_url(self):
        return url_for('eperpus_catalogs.catalog_item_details',
                       entity=self.shared_library_item.shared_library,
                       catalog=self.catalog,
                       item=self.shared_library_item.item,
                       _external=True)


class WatchList(db.Model):
    """ Mapping table for Items that in user's watch list
    """
    __tablename__ = 'watch_list'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("cas_users.id"))
    user = db.relationship(User, backref='watch_list')

    stock_org_id = db.Column(db.Integer, nullable=False)

    item_id = db.Column(db.Integer, db.ForeignKey("core_items.id"), nullable=False)
    item = db.relationship(Item, backref='watch_list', viewonly=True)

    shared_library_item = db.relationship(SharedLibraryItem, backref=db.backref('library_watch_list', lazy='dynamic'))

    catalog_id = db.Column(db.Integer, db.ForeignKey("core_catalogs.id"))
    catalog = db.relationship(Catalog, backref='catalog_watch_list')

    catalog_item = db.relationship(
        CatalogItem, backref=db.backref('watch_list', lazy='dynamic'), viewonly=True)

    start_date = db.Column(db.DateTime(timezone=True))
    end_date = db.Column(db.DateTime(timezone=True))
    is_borrowed = db.Column(db.Boolean, default=False)

    __table_args__ = (
        db.ForeignKeyConstraint(
            ['stock_org_id', 'item_id'],
            ['core_organization_items.organization_id', 'core_organization_items.item_id'],
            name='watch_list_organization_item_fkey'),
        db.ForeignKeyConstraint(
            ['catalog_id', 'stock_org_id', 'item_id'],
            ['core_catalog_items.catalog_id',
             'core_catalog_items.shared_library_item_org_id',
             'core_catalog_items.shared_library_item_item_id'],
            name='watch_list_catalog_items_fkey'),
    )

    @property
    def api_url(self):
        return url_for('watch_list.details', entity_id=self.id, _external=True)

class WebReaderLog(TimestampMixin, db.Model):

    __tablename__ = 'web_reader_processed'

    id = db.Column(db.Integer, primary_key=True)
    item_id = db.Column(db.Integer, db.ForeignKey("core_items.id"), nullable=False)
    item = db.relationship(Item, backref='web_reader', viewonly=True)
    is_processed = db.Column(db.Boolean, default=False)
    is_web_reader = db.Column(db.Boolean, default=False)
    error = db.Column(db.String(250))

class LandingPage(TimestampMixin, db.Model):

    __tablename__ = 'core_landingpage'

    id = db.Column(db.Integer, primary_key=True)
    slug = db.Column(db.String(250))
    data = db.Column(JSONB)
    organization_id = db.Column(db.Integer, db.ForeignKey('cas_organizations.id'), nullable=False)
    organization = db.relationship(Organization, backref=db.backref('landing_pages', lazy='dynamic'))

    def values(self):
        response = {"id": self.id, "app_slug": self.slug, "organization_id": self.organization_id}
        response.update(self.data)
        return response

    def custom_values(self):
        exist_values = self.values()
        new_list = []
        default_latest_version = {
            "app_name_version": "",
            "app_size": "",
            "app_whats_new": "",
            "app_date": ""
        }

        version_list = exist_values.pop('app_versions', None)

        if version_list:
            new_list = list(reversed(version_list))

        exist_values['latest_version'] = new_list[0] if new_list else default_latest_version
        exist_values['app_versions'] = new_list
        return exist_values



