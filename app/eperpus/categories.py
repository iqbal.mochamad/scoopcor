import requests

from httplib import CREATED, NOT_FOUND, OK
from datetime import datetime
from marshmallow import fields

from flask import Blueprint, jsonify, request, make_response, Response
from flask_appsfoundry.exceptions import Conflict, InternalServerError, BadRequest
from sqlalchemy import desc

from app import db, ma, app
from app.eperpus.libraries import SharedLibraryItem
from app.helpers import slugify
from app.items.models import Item
from app.master.models import Category
from app.users.organizations import Organization, is_organization_manager


blueprint = Blueprint('eperpus_categories_organizations', __name__, url_prefix='/v1')


class CategoryEperpus(db.Model):
    __tablename__ = 'core_eperpus_categories_organizations'

    id = db.Column(db.Integer, primary_key=True)

    organization_id = db.Column(db.Integer, db.ForeignKey('cas_organizations.id'))
    organization = db.relationship(Organization)

    category_id = db.Column(db.Integer, db.ForeignKey('core_categories.id'))
    category = db.relationship(Category)

    name = db.Column(db.String(250))
    is_active = db.Column(db.Boolean(), default=True)

    def values(self):
        return {'id': self.id, 'name': self.name, 'category_id': self.category_id, 'is_active': self.is_active}


class CategoryEperpusItem(db.Model):
    __tablename__ = 'core_eperpus_items_categories'

    id = db.Column(db.Integer, primary_key=True)

    organization_id = db.Column(db.Integer, db.ForeignKey('cas_organizations.id'))
    organization = db.relationship(Organization)

    category_id = db.Column(db.Integer, db.ForeignKey('core_categories.id'))
    category = db.relationship(Category)

    item_id = db.Column(db.Integer, db.ForeignKey('core_items.id'))
    item = db.relationship(Item)


class CategoryEperpusSchema(ma.Schema):
    name = fields.String()
    # is_active = fields.Boolean()

class CategoryEperpusUpdate(ma.Schema):
    category_id = fields.Integer()
    item_id = fields.Integer()
    is_added = fields.Boolean()

@blueprint.route('/organizations/<organization:entity>/categories', methods=['GET', 'POST'])
@is_organization_manager
def categories_eperpus_list(entity):

    if request.method == 'GET':

        args = request.args.to_dict()
        limit, offset, q = args.get('limit', 20), args.get('offset', 0), args.get('q', None)

        child_org = Organization.query.filter_by(parent_organization_id=entity.id).all()
        all_org_id = [i.id for i in child_org + [entity]]

        categories = CategoryEperpus.query.filter(CategoryEperpus.organization_id.in_(all_org_id)).order_by(desc(
            CategoryEperpus.id))
        data_count = categories.count() # total_count for pagination

        if q: categories = categories.filter(CategoryEperpus.name.ilike('%{}%'.format(q)))
        if limit:
            categories = categories.limit(limit).offset(offset)

        data = [i.values() for i in categories.all()]

        metadata, resultset = {}, {}
        resultset['count'] = data_count
        resultset['limit'] = int(limit) if limit else 0
        resultset['offset'] = int(offset) if offset else 0
        metadata['resultset'] = resultset

        return jsonify({'data': data, 'metadata': metadata})

    if request.method == 'POST':
        from app.utils.marshmallow_helpers import schema_load

        category_schema = CategoryEperpusSchema()
        data = schema_load(schema=category_schema)
        new_name = data.get('name', '')

        # check category parent
        current_categories = Category.query.filter_by(name=new_name).first()
        if not current_categories:
            current_categories = Category(
                name=new_name,
                description = new_name,
                slug = slugify(new_name),
                sort_priority = 1,
                item_type='book',
                is_active=False # always set false soo not displayed on SCOOP
            )
            db.session.add(current_categories)
            db.session.commit()

        child_org = Organization.query.filter_by(parent_organization_id=entity.id).all()
        all_org_id = [i.id for i in child_org + [entity]]
        exist_categories = CategoryEperpus.query.filter(
            CategoryEperpus.organization_id.in_(all_org_id), CategoryEperpus.name == new_name).count()

        if exist_categories:
            raise Conflict(user_message='Kategori {} sudah ada.!'.format(new_name),
                           developer_message='Kategori {} sudah ada.!'.format(new_name))

        # check if category id already exist on eperpus categories
        eperpus_categories = CategoryEperpus.query.filter(
            CategoryEperpus.organization_id.in_(all_org_id),
            CategoryEperpus.category_id == current_categories.id).first()

        if not eperpus_categories:
            eperpus_categories = CategoryEperpus(
                organization_id=entity.id,
                category_id = current_categories.id,
                name = current_categories.name,
                is_active = True
            )
            db.session.add(eperpus_categories)
            db.session.commit()

        return make_response(jsonify(eperpus_categories.values()), CREATED)


@blueprint.route('/organizations/<organization:entity>/categories/<category_id>', methods=['GET', 'PUT'])
@is_organization_manager
def categories_eperpus_detail_list(entity, category_id):

    if request.method == 'GET':
        child_org = Organization.query.filter_by(parent_organization_id=entity.id).all()
        all_org_id = [i.id for i in child_org + [entity]]

        current_category = CategoryEperpus.query.filter_by(id=category_id).first()
        if not current_category: return Response(status=NOT_FOUND)

        item_categories = CategoryEperpusItem.query.filter(
            CategoryEperpusItem.category_id == current_category.category.id,
            CategoryEperpusItem.organization_id.in_(all_org_id)
        ).order_by(desc(CategoryEperpusItem.item_id)).all()

        temp = {
            'name': current_category.name,
            'category_id': current_category.category_id,
            'items': [
                {'id': data.item.id,
                 'name': data.item.name} for data in item_categories
            ]
        }
        return jsonify(temp)

    if request.method == 'PUT':
        try:
            from app.utils.marshmallow_helpers import schema_load

            # check data if exist or not?
            current_category = CategoryEperpus.query.filter_by(id=category_id).first()
            if not current_category: return Response(status=NOT_FOUND)

            # check if name is already have it.
            category_schema = CategoryEperpusSchema()
            data = schema_load(schema=category_schema)
            new_name = data.get('name', '')

            # no category double with same name in 1 organizations.
            if current_category.name != new_name:
                child_org = Organization.query.filter_by(parent_organization_id=entity.id).all()
                all_org_id = [i.id for i in child_org + [entity]]

                exist_categories = CategoryEperpus.query.filter(
                    CategoryEperpus.organization_id.in_(all_org_id), CategoryEperpus.name == new_name).count()

                if exist_categories:
                    raise Conflict(user_message='Kategori {} sudah ada.!'.format(new_name),
                                     developer_message='Kategori {} sudah ada.!'.format(new_name))

                # at here.. if category name changes, all modified on item and shared must updated
                # soo solr can reindex.

                #1. find all item related with this categories.
                all_items = CategoryEperpusItem.query.filter(CategoryEperpusItem.organization_id.in_(all_org_id),
                                                             CategoryEperpusItem.category_id == current_category.category_id).all()
                for idata in all_items:
                    # update modified items
                    idata.item.modified = datetime.now()
                    db.session.add(idata)

                    # update modified shared lib.
                    shared_lib = SharedLibraryItem.query.filter(
                        SharedLibraryItem.organization_id.in_(all_org_id),
                        SharedLibraryItem.item_id == idata.item.id
                    ).all()
                    for ishare in shared_lib:
                        ishare.modified = datetime.now()
                        db.session.add(ishare)
                    db.session.commit()

            try:
                # always update shared lib, no matter what!
                shared_lib_url = '{}/scoop-sharedlib-all/dataimport'.format(app.config['SOLR_BASE_URL'])
                requests.post(shared_lib_url, data={'command': 'delta-import', 'wt': 'json'})
            except Exception as e:
                pass

            current_category.name = new_name
            db.session.add(current_category)
            db.session.commit()

            return make_response(jsonify(current_category.values()), OK)
        except Exception as e:
            raise InternalServerError(user_message='Error {}'.format(e), developer_message='Error {}'.format(e))


@blueprint.route('/organizations/<organization:entity>/categories/update', methods=['PUT'])
@is_organization_manager
def categories_eperpus_update(entity):
    from app.utils.marshmallow_helpers import schema_load

    category_schema = CategoryEperpusUpdate()
    data = schema_load(schema=category_schema)

    child_org = Organization.query.filter_by(parent_organization_id=entity.id).all()
    all_org_id = [i.id for i in child_org + [entity]]

    try:
        status_update = data.get('is_added')

        exist_item = Item.query.filter_by(id=data.get('item_id', 0)).first()
        if not exist_item: return Response(status=NOT_FOUND)

        exist_category = Category.query.filter_by(id=data.get('category_id', 0)).first()
        if not exist_category:
            raise BadRequest(user_message='Kategori tidak ada.!',
                developer_message='Kategori tidak ada.!')

        exist_category_eperpus = CategoryEperpus.query.filter(
            CategoryEperpus.organization_id.in_(all_org_id),
            CategoryEperpus.category_id == exist_category.id).first()

        # added from eperpus categories
        if not exist_category_eperpus and status_update is True:
            eperpus_categories = CategoryEperpus(
                organization_id=entity.id,
                category_id=exist_category.id,
                name=exist_category.name,
                is_active=True)
            db.session.add(eperpus_categories)
            db.session.commit()

        exist_eperpus_item_cat = CategoryEperpusItem.query.filter_by(
            organization_id = entity.id,
            category_id = exist_category.id,
            item_id = exist_item.id
        ).first()

        # added to new eperpus Category
        if not exist_eperpus_item_cat and status_update is True:
            new_item_cat = CategoryEperpusItem(
                organization_id=entity.id,
                category_id=exist_category.id,
                item_id=exist_item.id
            )
            db.session.add(new_item_cat)
            db.session.commit()

        # if category already 1 left, and takeout.
        if exist_eperpus_item_cat and status_update is False:
            db.session.delete(exist_eperpus_item_cat)
            db.session.commit()

        # # takeout from eperpus categories..
        # if exist_category_eperpus and len([exist_eperpus_item_cat]) <=1 and status_update is False:
        #     db.session.delete(exist_category_eperpus)
        #     db.session.commit()

        # update shared lib and modified items
        exist_item.modified = datetime.now()
        db.session.add(exist_item)

        # shared lib
        shared_lib = SharedLibraryItem.query.filter(
            SharedLibraryItem.organization_id.in_(all_org_id),
            SharedLibraryItem.item_id == exist_item.id
        ).all()
        for ishare in shared_lib:
            ishare.modified = datetime.now()
            db.session.add(ishare)

        db.session.commit()

        # update SOLR
        try:
            # always update shared lib, no matter what!
            shared_lib_url = '{}/scoop-sharedlib-all/dataimport'.format(app.config['SOLR_BASE_URL'])
            requests.post(shared_lib_url, data={'command': 'delta-import', 'wt': 'json'})
        except Exception as e:
            print 'Error {}'.format(e)

        return make_response(jsonify({'message': 'succesfully updated'}), OK)

    except Exception as e:
        raise InternalServerError(user_message='Error {}'.format(e), developer_message='Error {}'.format(e))
