from datetime import datetime

from sqlalchemy import desc

from app import db
from app.eperpus.categories import CategoryEperpusItem, CategoryEperpus
from app.eperpus.libraries import SharedLibraryItem
from app.eperpus.models import CatalogItem
from app.items.choices import STATUS_TYPES, STATUS_READY_FOR_CONSUME
from app.items.models import Item
from app.offers.choices.offer_type import SINGLE, SUBSCRIPTIONS
from app.users.organizations import OrganizationUser, Organization


def get_stock_holding_org(current_org):
    # search current org and all parent org, which org is the stock keeping org
    prev_org = current_org
    while prev_org:
        parent_org = prev_org.parent_organization
        if not parent_org:
            # previous org doesn't have parent org, this is the main organization, return it
            return prev_org
        else:
            prev_org = parent_org


def update_qty_available(session, current_org, item_id, qty):
    stock_org = get_stock_holding_org(current_org)
    # beware of 2/more user updating the same data at the same time
    # should update with the latest value from database
    #  update OrganizationItem set OrganizationItem = OrganizationItem + qty_borrowed where ...
    from app.eperpus.libraries import SharedLibraryItem
    session.query(SharedLibraryItem).filter(
        SharedLibraryItem.organization_id == stock_org.id,
        SharedLibraryItem.item_id == item_id
    ).update({'quantity_available': SharedLibraryItem.quantity_available + qty})


def update_shared_library_item_from_order(session, shared_library, offer, quantity_buy, catalog_id=None):
    # add inventory to shared library/organization - item/brand
    # only used in order_complete.py
    if offer.offer_type_id == SINGLE:
        item = offer.items.first()
        # update shared library/org item in database
        update_shared_library_item(session, shared_library, item, quantity_buy, catalog_id)

        # import delta data to SOLR (update shared library item information in SOLR)
        #  is in order_complete.order_change_status() (after all item in order line updated to db

    elif offer.offer_type_id == SUBSCRIPTIONS:
        brand = offer.brands.first()
        from app.eperpus.libraries import SharedLibraryBrand
        org_brand = session.query(SharedLibraryBrand).filter(
            SharedLibraryBrand.organization_id == shared_library.id,
            SharedLibraryBrand.brand_id == brand.id
        ).first()
        if not org_brand:
            org_brand = SharedLibraryBrand(
                shared_library=shared_library,
                brand=brand,
                quantity=quantity_buy,
            )
        else:
            org_brand.quantity = org_brand.quantity + quantity_buy

        session.add(org_brand)

        # add latest edition of the brand into shared library item
        from app.items.models import Item
        item = brand.items.filter(
            Item.is_active == True,
            Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME]
        ).order_by(desc(Item.id)).first()
        if item:
            update_shared_library_item(session, shared_library, item, quantity_buy, catalog_id)
            org_brand.latest_added_item = item

    else:
        # not implemented yet
        return


def update_shared_library_item(session, shared_library, item, quantity_buy, catalog_id):
    from app.eperpus.libraries import SharedLibraryItem
    from app.users.organizations import Organization
    from app.eperpus.categories import CategoryEperpus, CategoryEperpusItem

    org_item = session.query(SharedLibraryItem).filter(
        SharedLibraryItem.organization_id == shared_library.id,
        SharedLibraryItem.item_id == item.id
    ).first()
    if not org_item:
        org_item = SharedLibraryItem(
            shared_library=shared_library,
            item=item,
            quantity=quantity_buy,
            quantity_available=quantity_buy,
            created=datetime.now(),
            modified=datetime.now()
        )
    else:
        org_item.quantity = org_item.quantity + quantity_buy
        org_item.quantity_available = org_item.quantity_available + quantity_buy
        org_item.modified = datetime.now()

    session.add(org_item)

    if catalog_id:
        catalog_item = session.query(CatalogItem).filter(
            CatalogItem.catalog_id == catalog_id,
            CatalogItem.shared_library_item_item_id == item.id
        ).first()
        if not catalog_item:
            catalog_item = CatalogItem(
                catalog_id=catalog_id,
                shared_library_item=org_item,
                is_active=True
            )
        elif not catalog_item.is_active:
            catalog_item.is_active = True

        session.add(catalog_item)

    # added eperpus categories
    child_org = Organization.query.filter_by(parent_organization_id=shared_library.id).all()
    all_org_id = [i.id for i in child_org] + [shared_library.id]

    category_items = [{'id': i.id, 'name': i.name} for i in item.categories]

    for icat in category_items:

        # new eperpus categories
        exist_category_eperpus = CategoryEperpus.query.filter(
            CategoryEperpus.organization_id.in_(all_org_id),
            CategoryEperpus.category_id == icat.get('id', None)).first()

        if not exist_category_eperpus:
            new_category = CategoryEperpus(
                organization_id=shared_library.id,
                category_id=icat.get('id', None),
                name=icat.get('name', ''),
                is_active=True
            )
            db.session.add(new_category)
            db.session.commit()

        # new eperpus items
        exist_category_eperpus_item = CategoryEperpusItem.query.filter_by(
            organization_id=shared_library.id,
            category_id=icat.get('id'),
            item_id=item.id
        ).first()

        if not exist_category_eperpus_item:
            new_category_item = CategoryEperpusItem(
                organization_id=shared_library.id,
                category_id=icat.get('id'),
                item_id=item.id
            )
            db.session.add(new_category_item)
            db.session.commit()


def assert_user_organization(user, orgs_id):
    return OrganizationUser.query.filter_by(
        organization_id=orgs_id,
        user=user
    ).first()


def eperpus_organization_admins(org_id):
    # avoid looping import!
    from app.users.users import User

    exist_admin = OrganizationUser.query.filter_by(organization_id=org_id, is_manager=True).all()
    user_ids = [data.user_id for data in exist_admin]
    return User.query.filter(User.id.in_(user_ids)).all()


def organization_list_parent(entity):
    all_organizations = Organization.query.filter_by(parent_organization_id=entity.id, is_active=True).order_by(
        desc(Organization.id)).all()

    return [entity] + [i for i in all_organizations]


def current_internal_content_size(entity):
    sql = """select sum(file_size) from core_organization_items join core_items ci on 
                core_organization_items.item_id = ci.id where organization_id = {org_id} 
                and is_internal_content = true""".format(
        org_id=entity.id)
    resultset = db.engine.execute(sql)
    data = resultset.fetchone()[0]
    return data


def total_content_count(entity, is_internal=False):
    sql = """select count(*) from core_organization_items join core_items ci on 
                core_organization_items.item_id = ci.id where organization_id = {org_id} 
                and is_internal_content = {is_internal}""".format(
        org_id=entity.id, is_internal=is_internal)
    resultset = db.engine.execute(sql)
    data = resultset.fetchone()[0]
    return data


def get_rating_metadata(item_id):
    sql = """select sum(case when rating = 1 then 1 else 0 end) as one,
                    sum(case when rating = 2 then 1 else 0 end) as two,
                    sum(case when rating = 3 then 1 else 0 end) as three,
                    sum(case when rating = 4 then 1 else 0 end) as four,
                    sum(case when rating = 5 then 1 else 0 end) as five
                from core_reviews where item_id={item_id} and is_active;""".format(item_id=item_id)
    resultset = db.engine.execute(sql)
    result = resultset.fetchone()
    return result


def update_eperpus_categories(data_item=None, organization_id=None):
    '''
        update category eperpus by specified item id and organization_id
    '''

    data_category_data = []
    for items_ in data_item:
        exist_item = Item.query.filter_by(id=items_).first()

        if exist_item:
            for data_category in exist_item.categories:
                item_eperpus = CategoryEperpusItem.query.filter_by(organization_id=organization_id,
                        item_id=exist_item.id, category_id=data_category.id).first()

                exist_category_eperpus = CategoryEperpus.query.filter_by(organization_id=organization_id,
                        category_id=data_category.id).first()

                if not exist_category_eperpus:
                    new_category_eperpus = CategoryEperpus(organization_id=organization_id,
                        category_id=data_category.id, name=data_category.name, is_active=True)

                    db.session.add(new_category_eperpus)
                    db.session.commit()
                    data_category_data.append(exist_item.id)

                if not item_eperpus:
                    new_item_eperpus = CategoryEperpusItem(organization_id=organization_id, item_id=exist_item.id,
                        category_id=data_category.id)

                    db.session.add(new_item_eperpus)
                    db.session.commit()
                    data_category_data.append(exist_item.id)

    return data_category_data


def update_eperpus_category_by_organization(organization_id=None):
    items = SharedLibraryItem.query.filter_by(organization_id=organization_id).all()
    item_ids = [data.item_id for data in items]
    update_eperpus_categories(item_ids, organization_id)


def build_item_reponse(item):
    from app.items.viewmodels import get_image_from_item
    offers, price = item.get_offers(platform_id=4)
    if item and item.item_type == Item.Types.book.value:
        subtitle = ', '.join([author.name for author in item.authors])
        gtin14 = item.gtin14
    else:
        subtitle = item.issue_number if item else None
        gtin14 = ''
    rv = {
        "brand": {
            "title": item.brand.name,
            "href": item.brand.api_url,
            "slug": item.brand.slug
        },
        "categories": [{
            "title": category.name,
            "href": category.api_url
        } for category in item.categories],
        "countries": item.countries,
        "highlighting": {},
        "id": item.id,
        "title": item.name,
        "subtitle": subtitle,
        "href": item.api_url,
        "type": item.item_type,
        "slug": item.slug,
        "is_active": item.is_active,
        "status": item.item_status,
        "images": get_image_from_item(item),
        "languages": item.languages,
        "offers": offers,
        "gtin14": gtin14,
        # "release_date": item.release_date,
        "vendor": {
            "title": item.brand.vendor.name,
            "href": item.brand.vendor.api_url
        }
    }

    return rv
