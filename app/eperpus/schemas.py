from marshmallow import fields
from marshmallow.validate import Length, Range, OneOf

from app import ma
from app.items.choices.review import REPORT_REASON


class UserOrganizationSchema(ma.Schema):
    email = fields.String()


class UserReviewSchema(ma.Schema):
    rating = fields.Integer(required=True, validate=Range(1, 5))
    borrowing_id = fields.Integer(required=True)
    review = fields.String(required=False, validate=Length(20, 500))


class ReportReviewSchema(ma.Schema):
    reason = fields.String(required=True, validate=OneOf(choices=REPORT_REASON.values()))
    notes = fields.String(required=False, validate=Length(20, 50))
