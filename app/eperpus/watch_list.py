from __future__ import unicode_literals

import httplib, json
from httplib import NO_CONTENT, CREATED

import requests
from flask import Blueprint, jsonify, request, Response
from flask import current_app
from flask import g
from flask import url_for
from flask_appsfoundry.exceptions import NotFound, Unauthorized, UnprocessableEntity
from flask_appsfoundry import parsers
from marshmallow import fields
from marshmallow_sqlalchemy import ModelSchema
from sqlalchemy import desc

from app import app, db, ma
from app.auth.decorators import user_is_authenticated
from app.eperpus.catalogs import Catalog, get_estimated_next_available_time, CatalogItem, assert_catalog_item
from app.eperpus.helpers import get_stock_holding_org
from app.eperpus.libraries import SharedLibraryItem
from app.eperpus.members import get_allowed_orgs, assert_user_vs_organization
from app.eperpus.models import WatchList
from app.items.previews import preview_s3_covers
from app.utils.datetimes import get_local
from app.utils.exceptions import BadGateway
from app.utils.marshmallow_helpers import MaListResponse, schema_load
from app.utils.strings import FORMAT_ISO8601_DURATION

from marshmallow.utils import isoformat, from_datestring
from urllib import quote_plus


blueprint = Blueprint('watch_list', __name__, url_prefix='/v1')

class TitleHrefSchema(ma.Schema):

    title = fields.Str(attribute='name')
    href = fields.Url(attribute='api_url')


class WatchListSchema(ModelSchema):

    class Meta:
        model = WatchList
        sqla_session = db.session
        exclude = ('name', 'user', 'item', 'catalog_item', 'shared_library_item', 'unlink_api_url')

    title = fields.Function(
        lambda obj: obj.shared_library_item.item_name_override if
        obj.shared_library_item and obj.shared_library_item.item_name_override else obj.item.name)

    # href to post in request body for borrowing the item
    href = fields.Url(attribute='catalog_item.api_url')

    # href for remove item!
    delete = fields.Function(lambda obj: url_for('watch_list.remove', entity_id=obj.id, _external=True))

    currently_available = fields.Integer(dump_only=True, attribute='shared_library_item.quantity_available')
    total_in_collection = fields.Integer(dump_only=True, attribute='shared_library_item.quantity')

    max_borrow_time = fields.Function(
        lambda obj: FORMAT_ISO8601_DURATION.format(obj.shared_library_item.shared_library.borrowing_time_limit))

    next_available = fields.Function(
        lambda obj: get_estimated_next_available_time(db.session, catalog_item=obj.catalog_item))

    details = fields.Function(lambda obj: {
        "title": "Detil Item",
        "href": obj.item.api_url,
        "id": obj.item.id,
    })

    cover_image = fields.Function(lambda obj: {
        "title": "Gambar Sampul",
        "href": preview_s3_covers(obj.item, 'image_normal', True)
    })
    borrow_item = fields.Function(lambda obj: {
        "title": "Pinjam Item",
        "href": url_for('shared_library_users.update_user_current_borrowed_items', _external=True)
    })
    catalog = fields.Nested(TitleHrefSchema(), attribute='catalog')
    vendor = fields.Function(lambda obj: {
        "title": obj.item.brand.vendor.name,
        "href": obj.item.brand.vendor.api_url,
    })

    # authors = fields.Function(lambda obj: [{
    #     "id": a.id,
    #     "title": a.name,
    #     "href": a.api_url
    # } for a in obj.item.authors.all()])


class WatchListResponse(MaListResponse):

    def _generate_weak_etag(self, entities, list_result):
        # create unique etag based on id + currently_available info
        from hashlib import md5
        if len(list_result):
            weak_etag = ";".join(["{}-{}".format(e.get('id'), e.get('currently_available')) for e in list_result])
        else:
            weak_etag = ""
        return md5(weak_etag).hexdigest()


@blueprint.route('/users/current-user/watch-list')
@user_is_authenticated
def get_list():
    session = db.session
    session.add(g.current_user)
    allowed_orgs = get_allowed_orgs(session, user=g.current_user)
    query_set = session.query(WatchList).filter(
        WatchList.user_id == g.current_user.id,
        WatchList.stock_org_id.in_(allowed_orgs),
        WatchList.item_id > 0,
        WatchList.end_date == None,
        WatchList.is_borrowed == False
    ).join(WatchList.shared_library_item).filter(
        SharedLibraryItem.is_active == True
    ).join(WatchList.catalog_item).filter(
        CatalogItem.is_active == True
    )

    return WatchListResponse(
        query=query_set,
        schema=WatchListSchema(),
        key_name="items",
        filter_parsers=SharedCatalogArgsParser(),
        ordering=[desc(SharedLibraryItem.quantity_available), desc(WatchList.start_date)]
    ).response()


class SharedCatalogArgsParser(parsers.SqlAlchemyFilterParser):
    __model__ = WatchList

    item_id = parsers.IntegerFilter()
    stock_org_id = parsers.IntegerFilter()


@blueprint.route('/users/current-user/watch-list/<int:entity_id>', )
@user_is_authenticated
def details(entity_id):
    s = db.session
    watch_list = get_watch_list(entity_id, s)
    resp = jsonify(WatchListSchema().dump(watch_list).data)
    return resp


class AddWatchListSchema(ma.Schema):
    # required request body:
    #  a link from catalog item,
    #  example: http://localhost/v1/organizations/{org_id}/shared-catalogs/{cat_id}/{item_id}
    href = fields.Url(required=True, load_only=True)

    # for response
    id = fields.Int(dump_only=True)
    # href for remove item!
    delete = fields.Function(lambda obj: url_for('watch_list.remove', entity_id=obj.id, _external=True), dump_only=True)


@blueprint.route('/users/current-user/watch-list', methods=['POST'])
@user_is_authenticated
def add():
    """ add an item from a catalog to user watch list

    :return:
    """
    data = schema_load(AddWatchListSchema())
    # href in request body example:
    # https://api.apps-foundry.com/organizations/1/shared-catalogs/12/9998
    # 9998 = item id
    # 12 = catalog id
    href = data.get('href')
    data = href.split('/') if href else None
    catalog_id = None
    item_id = None
    if data and data[len(data) - 3] == 'shared-catalogs':
        item_id = data[len(data) - 1]
        catalog_id = data[len(data) - 2]

    if not item_id or not catalog_id:
        raise UnprocessableEntity()

    session = db.session()

    catalog = session.query(Catalog).get(catalog_id)
    if not catalog:
        raise NotFound()

    stock_org = get_stock_holding_org(catalog.shared_library)

    current_user_id = g.current_user.id

    assert_user_vs_organization(
        session=session, org_id=catalog.organization_id, user_id=current_user_id)

    assert_catalog_item(session, catalog, item_id, stock_org)

    watch_list = session.query(WatchList).filter(
        WatchList.user_id == g.current_user.id,
        WatchList.stock_org_id == stock_org.id,
        WatchList.item_id == item_id,
        WatchList.end_date == None,
        WatchList.is_borrowed == False,
    ).first()
    if not watch_list:
        watch_list = WatchList(
            user_id=current_user_id,
            catalog_id=catalog_id,
            stock_org_id=stock_org.id,
            item_id=item_id,
            start_date=get_local(),
            end_date=None,
            is_borrowed=False
        )
        session.add(watch_list)
        session.commit()

    resp = jsonify(AddWatchListSchema().dump(watch_list).data)
    resp.status_code = CREATED
    return resp


def get_watch_list(entity_id, session=None):
    session = session or db.session
    watch_list = session.query(WatchList).get(entity_id)
    if watch_list:
        return watch_list
    else:
        raise NotFound()


@blueprint.route('/users/current-user/watch-list/<int:entity_id>', methods=['DELETE', ])
@user_is_authenticated
def remove(entity_id):
    """ remove / stop from watch list

    parameter required: id: watch list unique id
    :return:
    """
    s = db.session
    watch_list = get_watch_list(entity_id, s)

    if watch_list.user_id != g.current_user.id:
        raise Unauthorized(
            user_message="Ini bukan item di dalam watch list anda",
            developer_message="This watch list item belong to another user"
        )

    if not watch_list.end_date:
        watch_list.end_date = get_local()
        s.add(watch_list)
        s.commit()

    resp = jsonify({})
    resp.status_code = NO_CONTENT
    return resp

def encode_date(value):
    if isinstance(value, str):
        # make sure it's a valid date
        value = from_datestring(value)
    return quote_plus(isoformat(value, localtime=True))

@blueprint.route('/organizations/<int:organization_id>/watch-list-data', methods=['GET', ])
@user_is_authenticated
def reports(organization_id):

    args = request.args.to_dict()
    start_date_for_dw = args.get('start_date', '').split('T')[0].replace('"', '')
    end_date_for_dw = args.get('end_date', '').split('T')[0].replace('"', '')
    limit, offset = args.get('limit', None), args.get('offset', 25)
    catalog_id = args.get('catalog_id', 0)

    if args.get('org_id', None): organization_id = args.get('org_id', 0)
    else: organization_id = organization_id

    base_url = '{}/kettle/executeTrans/?rep=scoopdwext_etl&trans={}&start_date={}&end_date={}&organization_id={}' \
        '&limit={}&offset={}&catalog_id={}'.format(app.config['DW_API_BASE_URL'], 'eperpus_API_watch_list', start_date_for_dw,
        end_date_for_dw, organization_id, limit, offset, catalog_id)

    response = requests.get(base_url, auth=(app.config['DW_API_USER'], app.config['DW_API_PASSWORD']))
    if response.status_code == httplib.OK and response.text:
        data_response = response.json().get('data', [])

        # DW api return index [0] only for total_row.. DAM
        total_count = data_response[0].get('total_row', 0)
        del data_response[0]
        data_meta = {"resultset": {"count": total_count, "limit": int(limit), "offset": int(offset)}}

        return Response(json.dumps({"items": data_response, "metadata": data_meta}), status=httplib.OK)
    else:
        raise BadGateway(
            user_message="Failed to get data from data warehouse API",
            developer_message="DW Response: {status_code} - response body: {body}.\n Url: {url}".format(
                status_code=response.status_code,
                body=response.text if response.text else "Empty Response",
                url=base_url)
        )
