from __future__ import unicode_literals, absolute_import

import json
from httplib import OK, FORBIDDEN, METHOD_NOT_ALLOWED

from app.users.organizations import OrganizationType
from app.users.tests.e2e.base import OrganizationE2EBase


class SharedLibraryTests(OrganizationE2EBase):

    force_organization_fixture_type = OrganizationType.shared_library

    def api_lock_organization(self, organization, message):
        return self.api_client.open(
            '/v1/organizations/{org.id}'.format(org=organization),
            data=json.dumps({'locked_message': message}),
            method='LOCK',
            headers=self.build_headers(has_body=True)
        )

    def api_unlock_organization(self, organization):
        return self.api_client.open(
            '/v1/organizations/{org.id}'.format(org=organization),
            method='UNLOCK',
            headers=self.build_headers()
        )

    def test_lock_child(self):
        """ BASIC: Child organizations can be flagged as 'locked' with a user-provided message.
        """
        self.set_api_authorized_user(self.organization_manager)

        response = self.api_lock_organization(
            self.child_organization,
            'Access is disabled during Exam Time'
        )

        self.assertEqual(response.status_code, OK)

        self.reattach_model(self.child_organization)

        self.assertEqual(self.child_organization.locked_message, 'Access is disabled during Exam Time')

    def test_unlock_child(self):
        """ BASIC: Child organizations can be flagged as 'unlocked'.
        """
        self.set_api_authorized_user(self.organization_manager)

        with self.on_session(self.child_organization):
            self.child_organization.locked_message = 'Because I said so'

        response = self.api_unlock_organization(self.child_organization)

        self.assertEqual(response.status_code, OK)

        self.reattach_model(self.child_organization)

        self.assertIsNone(self.child_organization.locked_message)

    def test_only_managers_can_lock_children(self):
        """ SECURITY: Only organization managers should have privileges to lock an organization.
        """
        self.set_api_authorized_user(self.organization_member)

        response = self.api_lock_organization(
            self.child_organization,
            'I should not be allowed to do this'
        )

        self.assertEqual(response.status_code, FORBIDDEN)

        self.reattach_model(self.child_organization)

        self.assertIsNone(self.child_organization.locked_message)

    def test_only_managers_can_unlock_child(self):
        """ SECURITY: Only organization managers should have privileges to unlock an organization.
        """
        self.set_api_authorized_user(self.organization_member)

        with self.on_session(self.child_organization):
            self.child_organization.locked_message = 'Because I said so'

        response = self.api_unlock_organization(
            self.child_organization
        )

        self.assertEqual(response.status_code, FORBIDDEN)

        self.reattach_model(self.child_organization)

        self.assertEqual(self.child_organization.locked_message, 'Because I said so')

    def test_parent_organizations_cannot_be_locked(self):
        """ BUSINESS RULE: Only child organizations can be locked/unlocked.
        """
        self.set_api_authorized_user(self.organization_manager)

        response = self.api_lock_organization(
            self.organization,
            'Access is disabled during Exam Time'
        )

        self.assertEqual(response.status_code, METHOD_NOT_ALLOWED)

        self.reattach_model(self.organization)

        self.assertIsNone(self.organization.locked_message)
