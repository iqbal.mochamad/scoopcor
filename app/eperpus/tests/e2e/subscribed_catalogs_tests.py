from __future__ import unicode_literals

import json
from httplib import OK, UNAUTHORIZED
from unittest import TestCase

from dateutil.relativedelta import relativedelta

from nose.tools import istest, nottest

from app import app, db
from app.auth.helpers import jwt_encode_from_user
from app.auth.models import Client
from app.eperpus.catalogs import Catalog, CatalogItem
from app.eperpus.tests.fixtures import SharedLibraryFactory, SharedLibraryItemFactory, CatalogFactory, \
    CatalogItemFactory
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import ItemFactory, AuthorFactory


USER_ID = 1234
USER_UNAUTHORIZED = 2234
PARENT_ORG_ID = 11
CHILD_ORG_ID = 12


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    # if args[0] == '/v1/organizations/11/users'':
    # return MockResponse({"key1": "value1"}, 200)
    return MockResponse(mock_response_catalog_items_search(), 200)
    # else:
    # return MockResponse({}, 404)


def mock_response_catalog_items_search():
    items = []
    session = db.session()
    for cat_item in session.query(CatalogItem).all():
        items.append({'item_id': cat_item.shared_library_item_item_id})

    response = {
        "response": {
            "docs": items,
            "numFound": len(items),
        },
        "facet_counts": {
            "facet_queries": {},
            ""
            "facet_fields": {
                "item_categories":[
                    "Automotive", 2,
                    "Crime & Thrillers", 1
                ],
                "author_names": [
                    "ramdani", 2
                ],
                "item_languages":[
                    "id", 2,
                    "en", 0
                ]
            }
        },
        "spellcheck": {
            "suggestions": [
                "sedekha", {
                    "numFound": 2,
                    "startOffset": 0,
                    "endOffset": 7,
                    "origFreq": 0,
                    "suggestion": [
                        {
                            "word": "sedekah",
                            "freq": 8
                        },
                        {
                            "word": "selekta",
                            "freq": 1
                        }
                    ]
                }
            ],
            "correctlySpelled": 'false'
        }
    }

    session.close()
    return response


@nottest
class SubscribedCatalogBase(TestCase):

    maxDiff = None
    url = '/v1/users/current-user/subscribed-catalogs'
    user_id = USER_ID

    @classmethod
    def setUpClass(cls):
        session = db.session()
        user = session.query(User).get(cls.user_id)
        # in generate_static_sql_fixtures --> kg_eperpus_ios is for client id 85
        cls.headers = {
            'Accept': 'application/vnd.scoop.v3+json',
            'User-Agent': 'kg eperpus ios/v2.0.0'
        }
        with app.test_request_context('/'):
            cls.headers['Authorization'] = 'JWT {}'.format(
                jwt_encode_from_user(user)
                )
            client = app.test_client(use_cookies=False)
            cls.response = client.get(cls.url, headers=cls.headers)

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()

    def test_subscribed_catalog(self):

        expected = self.get_expected_response()

        if self.response.status_code == OK:
            actual = json.loads(self.response.data)
            self.assertDictEqual(actual, expected)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, OK, self.response.data)

    @staticmethod
    def get_expected_response():
        s = db.session()
        catalog_data = s.query(Catalog).filter(Catalog.organization_id.in_([CHILD_ORG_ID, ])).all()
        catalogs = []
        for c in catalog_data:
            catalogs.append(
                {
                    "id": c.id,
                    "title": c.name,
                    "href": "http://localhost/v1/organizations/{org_id}/shared-catalogs/{cat_id}".format(
                        org_id=c.organization_id,
                        cat_id=c.id,
                    ),
                    "is_internal_catalog": False
                }
            )
        expected = {
            "catalogs": catalogs,
            "metadata": {
                "resultset": {
                    "count": len(catalogs),
                    "limit": 20,
                    "offset": 0
                }
            }
        }
        return expected


@istest
class CurrentUserSubscribedCatalogTests(SubscribedCatalogBase):

    user_id = USER_ID


@istest
class SubscribedCatalogWithInValidOrgQueryTests(SubscribedCatalogBase):

    user_id = USER_UNAUTHORIZED

    def test_subscribed_catalog(self):
        pass

    def test_status_code(self):
        self.assertEqual(self.response.status_code, UNAUTHORIZED,
                         'Status Code is not UNAUTHORIZED. response: {}'.format(self.response.data))


def setup_module():
    db.drop_all()
    db.create_all()

    init_data()


def teardown_module():
    db.session().close_all()
    db.drop_all()


def init_data():
    s = db.session()
    s.expire_on_commit = False
    generate_static_sql_fixtures(session=s)
    client = s.query(Client).get(85)
    library_org = SharedLibraryFactory(
        id=PARENT_ORG_ID,
        user_concurrent_borrowing_limit=10,
        reborrowing_cooldown=relativedelta(days=0),
        borrowing_time_limit=relativedelta(days=7))
    child_org = SharedLibraryFactory(
        id=CHILD_ORG_ID,
        parent_organization=library_org)
    client.organizations.append(library_org)
    client.organizations.append(child_org)

    user = UserFactory(id=USER_ID)
    child_org.users.append(user)

    other_org = SharedLibraryFactory(id=99123)
    other_user = UserFactory(id=USER_UNAUTHORIZED)
    other_org.users.append(other_user)

    author = AuthorFactory()
    item1 = ItemFactory(id=1)
    item1.authors.append(author)
    item2 = ItemFactory(id=2)

    shared_lib_item1 = SharedLibraryItemFactory(
        shared_library=library_org,
        item=item1,
        quantity=10,
        quantity_available=0,
        item_name_override='{} additional title'.format(item1.name)
    )
    shared_lib_item2 = SharedLibraryItemFactory(
        shared_library=library_org,
        item=item2,
        quantity=20,
        quantity_available=20,
        item_name_override='{} additional title'.format(item2.name)
    )
    catalog = CatalogFactory(
        id=1,
        shared_library=library_org,
    )
    catalog2 = CatalogFactory(
        id=2,
        shared_library=child_org,
    )
    catalog_item1 = CatalogItemFactory(
        catalog=catalog2,
        shared_library_item=shared_lib_item1,
    )
    catalog_item2 = CatalogItemFactory(
        catalog=catalog2,
        shared_library_item=shared_lib_item2,
    )
    s.add(client)
    s.add(other_org)
    s.add(user)
    s.commit()
