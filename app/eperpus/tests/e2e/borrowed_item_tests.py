from __future__ import unicode_literals

import json
from httplib import OK, CREATED, NO_CONTENT, UNAUTHORIZED, FORBIDDEN
from unittest import TestCase

from datetime import timedelta

from dateutil.relativedelta import relativedelta
from flask import current_app
from marshmallow.utils import isoformat
from mock import patch
from nose.tools import istest, nottest
from sqlalchemy import desc

from app import app, db
from app.auth.helpers import jwt_encode_from_user
from app.auth.models import Client
from app.constants import RoleType
from app.eperpus.libraries import SharedLibraryItem
from app.eperpus.members import UserBorrowedItem
from app.eperpus.models import WatchList
from app.eperpus.tests.fixtures import SharedLibraryFactory, SharedLibraryItemFactory, CatalogFactory, \
    CatalogItemFactory, UserBorrowedItemFactory
from app.items.helpers import ItemOwnershipStatus, get_current_borrow_status
from app.items.previews import PreviewUrlBuilder
from app.users.organizations import OrganizationUser
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from app.utils.datetimes import get_utc, datetime_to_isoformat
from app.utils.testcases import E2EBase, TestBase
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import AuthorFactory
from tests.fixtures.sqlalchemy import ItemFactory


USER_ID = 12345
CLIENT_ID = 85
UNAUTHORIZED_USER_ID = 991122


@istest
class CurrentUserBorrowItemTests(TestBase):
    """ test for for api: /v1/users/current-user/borrowed-items, and
     /v1/users/current-user/borrowed-items/history

     also for: borrow an item, return borrowed item
    """

    maxDiff = None

    max_borrowed_time = relativedelta(days=7)

    @classmethod
    def setUpClass(cls):
        super(CurrentUserBorrowItemTests, cls).setUpClass()
        cls.init_data(cls.session)

        cls.client = app.test_client(use_cookies=False)

        cls.headers = {
            'Accept': 'application/vnd.scoop.v3+json',
            'User-Agent': 'kg eperpus ios/v2.0.0'
        }
        with app.test_request_context('/'):
            cls.headers['Authorization'] = 'JWT {}'.format(
                jwt_encode_from_user(cls.user)
            )

    @classmethod
    def init_data(cls, s):
        cls.library_org = SharedLibraryFactory(
            id=11,
            user_concurrent_borrowing_limit=10,
            reborrowing_cooldown=relativedelta(days=0),
            borrowing_time_limit=cls.max_borrowed_time)
        cls.child_org = SharedLibraryFactory(
            id=12,
            parent_organization=cls.library_org)
        client = s.query(Client).get(CLIENT_ID)
        client.organizations.append(cls.library_org)

        cls.user = UserFactory(id=USER_ID)
        unauthorized_user = UserFactory(id=UNAUTHORIZED_USER_ID)
        unauthorized_user.organizations.append(SharedLibraryFactory(id=99911))

        # cls.child_org.users.append(cls.user)
        org_user = OrganizationUser(organization_id=11,
                                    user_id=USER_ID,
                                    is_manager=True)
        s.add(org_user)

        author = AuthorFactory()
        cls.item1 = ItemFactory(id=1)
        cls.item1.authors.append(author)
        cls.item2 = ItemFactory(id=2)
        # item 3, item not borrowed yet
        cls.item3 = ItemFactory(id=3)
        # item 4, item is borrowed and already returned, should not shown in list of borrowed items
        cls.item4 = ItemFactory(id=4)
        cls.item5 = ItemFactory(id=5)

        shared_lib_item1 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item1,
            quantity=10,
            quantity_available=10,
            item_name_override='{} additional title'.format(cls.item1.name)
        )
        shared_lib_item2 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item2,
            quantity=10,
            quantity_available=7,
            item_name_override='{} additional title'.format(cls.item2.name)
        )
        shared_lib_item3 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item3,
            quantity=10,
            quantity_available=8,
            item_name_override='{} additional title'.format(cls.item3.name)
        )
        shared_lib_item4 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item4,
            quantity=10,
            quantity_available=4,
            item_name_override='{} additional title'.format(cls.item4.name)
        )
        cls.catalog = CatalogFactory(id=1, shared_library=cls.library_org, )
        catalog_item1 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item1, )
        catalog_item2 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item2, )
        catalog_item3 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item3, )
        catalog_item4 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item4, )
        cls.user_borrowed_item1 = UserBorrowedItemFactory(
            id=1000,
            user=cls.user,
            catalog_item=catalog_item1,
            borrowing_start_time=get_utc() - timedelta(days=5),
            returned_time=None,
        )
        cls.user_borrowed_item2 = UserBorrowedItemFactory(
            id=1001,
            user=cls.user,
            catalog_item=catalog_item2,
            borrowing_start_time=get_utc() - timedelta(days=4),
            returned_time=None,
        )
        cls.user_borrowed_item4 = UserBorrowedItemFactory(
            id=1004,
            user=cls.user,
            catalog_item=catalog_item4,
            borrowing_start_time=get_utc() - timedelta(days=10),
            returned_time=get_utc() - timedelta(days=7),
        )
        s.commit()

    def test_borrowed_items_list(self):
        user_items = UserBorrowedItem.query.filter(
            UserBorrowedItem.user_id == USER_ID,
            UserBorrowedItem.returned_time == None
        ).limit(20).all()
        items = []
        with app.app_context():
            for ubw in user_items:
                items.append(self.get_expected_response_list(ubw))

        expected = {
            "items": items,
            "metadata": {
                "resultset": {
                    "offset": 0,
                    "limit": 20,
                    "count": len(user_items)
                }
            }
        }

        response = self.client.get('/v1/users/current-user/borrowed-items',
                                   headers=self.headers)
        if response.status_code == OK:
            actual = json.loads(response.data)
            self.assertDictEqual(actual, expected)
        self.assertEqual(response.status_code, OK, response.data)

    def test_borrowed_items_list_unauthorized(self):
        # test unauthorized user (g.current_user.id don't have access to eperpus)
        with app.test_request_context('/'):
            current_headers = self.headers.copy()
            current_headers['Authorization'] = 'JWT {}'.format(
                jwt_encode_from_user(User.query.get(UNAUTHORIZED_USER_ID))
            )

        response = self.client.get('/v1/users/current-user/borrowed-items',
                                   headers=current_headers)
        self.assertEqual(response.status_code, UNAUTHORIZED)

    @nottest
    def get_expected_response_list(self, ubw):
        expire_time = datetime_to_isoformat(ubw.borrowing_start_time + self.max_borrowed_time)
        org_item = ubw.catalog_item.shared_library_item
        item = org_item.item
        authors = []
        for author in item.authors.all():
            authors.append({
                "id": author.id,
                "title": author.name,
                "href": "http://localhost/v1/authors/{}".format(author.id)
            })
        return {
            "id": ubw.id,
            "title": org_item.item_name_override if org_item.item_name_override else item.name,
            "href": "http://localhost/v1/users/current-user/borrowed-items/{}".format(ubw.id),
            "details": {
                "href": "http://localhost/v1/items/{}".format(item.id),
                "title": "Detil Item",
                "id": item.id
            },
            "catalog_item": {
                "href": "http://localhost/v1/organizations/{org_id}/"
                        "shared-catalogs/{cat_id}/{item_id}".format(
                            org_id=ubw.catalog_item_org_id,
                            cat_id=ubw.catalog_item_catalog_id,
                            item_id=item.id
                        ),
                "title": "Detil Item di Katalog",
            },
            "cover_image": {
                "href": current_app.config['MEDIA_BASE_URL'] + item.image_normal,
                "title": "Gambar Sampul"
            },
            "borrowing_start_time": datetime_to_isoformat(ubw.borrowing_start_time),
            "expires": expire_time,
            "vendor": {
                "href": "http://localhost/v1/vendors/{}".format(item.brand.vendor.id),
                "title": item.brand.vendor.name
            },
            "authors": authors,
            "download": {
                "href": "http://localhost/v1/items/{}/download".format(item.id),
                "title": "Download buat Baca Offline"
            },
            "currently_available": org_item.quantity_available,
            # "last_read_time": "2016-10-10T01:23:45Z",
            # "last_read_page": 12
            "catalog": {
                "href": "http://localhost/v1/organizations/{org_id}/"
                        "shared-catalogs/{cat_id}".format(
                            org_id=ubw.catalog_item_org_id,
                            cat_id=ubw.catalog_item_catalog_id
                        ),
                "rel": "catalog",
                "title": ubw.catalog_item.catalog.name,
                "id": ubw.catalog_item.catalog.id,
            },
            "returned_time": datetime_to_isoformat(ubw.returned_time) if ubw.returned_time else None
        }

    def get_expected_response(self, user_borrow_item):
        with app.test_request_context('/'):
            expire_time = datetime_to_isoformat(user_borrow_item.borrowing_start_time + self.max_borrowed_time)
            org_item = user_borrow_item.catalog_item.shared_library_item
            item = org_item.item
            catalog = user_borrow_item.catalog_item.catalog
            shared_library_org = user_borrow_item.catalog_item.shared_library_item.shared_library
            expected = {
                "id": user_borrow_item.id,
                "title": org_item.item_name_override if org_item.item_name_override else item.name,
                "details": {
                    "href": "http://localhost/v1/items/{}".format(item.id),
                    "title": "Detil Item",
                    "id": item.id,
                },
                "catalog_item": {
                    "href": "http://localhost/v1/organizations/{org_id}/"
                            "shared-catalogs/{cat_id}/{item_id}".format(
                                org_id=user_borrow_item.catalog_item_org_id,
                                cat_id=user_borrow_item.catalog_item_catalog_id,
                                item_id=item.id
                            ),
                    "title": "Detil Item di Katalog",
                },
                "owner": {
                    "href": "http://localhost/v1/organizations/{}".format(shared_library_org.id),
                    "rel": shared_library_org.type,
                    "title": shared_library_org.name
                },
                "catalog": {
                    "href": "http://localhost/v1/organizations/{org_id}/"
                            "shared-catalogs/{cat_id}".format(
                                org_id=shared_library_org.id,
                                cat_id=catalog.id
                            ),
                    "rel": "catalog",
                    "title": catalog.name,
                    "id": catalog.id,
                },
                "cover_image": {
                    "href": current_app.config['MEDIA_BASE_URL'] + item.image_normal,
                    "title": "Gambar Sampul"
                },
                "download": {
                    "href": "http://localhost/v1/items/{}/download".format(item.id),
                    "title": "Download buat Baca Offline"
                },
                "streaming": {
                    "href": "http://localhost/v1/items/{}/web-reader".format(item.id),
                    "title": "Baca Sekarang"
                },
                "borrowing_start_time": datetime_to_isoformat(user_borrow_item.borrowing_start_time),
                "expires": expire_time,
                "last_read_time": None,
                "last_read_page": None,
                "vendor": {
                    "href": "http://localhost/v1/vendors/{}".format(item.brand.vendor.id),
                    "title": item.brand.vendor.name
                },
                "currently_available": org_item.quantity_available,
            }
            return expected

    def get_expected_details_response(self, user_borrow_item):
        expected = self.get_expected_response(user_borrow_item)
        org_item = user_borrow_item.catalog_item.shared_library_item
        item = org_item.item
        catalog = user_borrow_item.catalog_item.catalog
        watch_list = db.session.query(WatchList).filter_by(
            user_id=USER_ID,
            item_id=item.id,
            catalog_id=catalog.id
        ).first()
        # add more info specific to catalog-item details
        expected.update({
            # "next_available": next_available,
            "description": item.description,
            "brand": {
                "id": item.brand.id,
                "title": item.brand.name,
                "href": item.brand.api_url},
            "watch_list": {
                "id": watch_list.id,
                "title": 'watch list',
                "href": watch_list.api_url} if watch_list else None,
            "high_res_image": {
                "href": current_app.config['MEDIA_BASE_URL'] + item.image_highres,
                "title": "Gambar Sampul High Res"
            },
            "previews": PreviewUrlBuilder(app.config['BASE_URL']).make_urls(item, True),
        })
        return expected

    def test_borrowed_items_history_list(self):
        """ test for current user borrowed item history
        """
        user_items = UserBorrowedItem.query.filter(
            UserBorrowedItem.user_id == USER_ID
        ).order_by(desc(UserBorrowedItem.id)).limit(20).all()
        items = []
        with app.app_context():
            for ubw in user_items:
                items.append(self.get_expected_response_list(ubw))

        expected = {
            "items": items,
            "metadata": {
                "resultset": {
                    "offset": 0,
                    "limit": 20,
                    "count": len(user_items)
                }
            }
        }

        response = self.client.get('/v1/users/current-user/borrowed-items-history',
                                   headers=self.headers)
        if response.status_code == OK:
            actual = json.loads(response.data)
            self.assertDictEqual(actual, expected)
        self.assertEqual(response.status_code, OK, response.data)

        # test alternate url
        response = self.client.get('/v1/users/{}/borrowed-items-history'.format(USER_ID),
                                   headers=self.headers)
        if response.status_code == OK:
            actual = json.loads(response.data)
            self.assertDictEqual(actual, expected)
        self.assertEqual(response.status_code, OK, response.data)

        # test unauthorized user (user id different with g.current_user.id)
        response = self.client.get('/v1/users/{}/borrowed-items-history'.format(UNAUTHORIZED_USER_ID),
                                   headers=self.headers)
        self.assertEqual(response.status_code, UNAUTHORIZED)

    def test_borrowed_items_history_unauthorized(self):
        # test unauthorized user (g.current_user.id don't have access to eperpus)
        with app.test_request_context('/'):
            current_headers = self.headers.copy()
            current_headers['Authorization'] = 'JWT {}'.format(
                jwt_encode_from_user(User.query.get(UNAUTHORIZED_USER_ID))
            )
        response = self.client.get('/v1/users/current-user/borrowed-items-history',
                                   headers=current_headers)
        self.assertEqual(response.status_code, UNAUTHORIZED)

    def test_borrowed_items_detail(self):
        with self.on_session(self.user_borrowed_item1):
            ubw_id = self.user_borrowed_item1.id
        ubw1 = self.session.query(UserBorrowedItem).get(ubw_id)
        expected = self.get_expected_details_response(ubw1)
        response = self.client.get('/v1/users/current-user/borrowed-items/{}'.format(ubw_id),
                                   headers=self.headers)
        self.assertEqual(response.status_code, OK, response.data)
        actual = json.loads(response.data)
        self.assertDictEqual(actual, expected)

    @patch('app.eperpus.members.update_stock_solr')
    def test_borrow_an_item(self, stock_solr):
        with self.on_session(self.catalog, self.item3, self.child_org, ):
            item_id = self.item3.id
            org_id = self.catalog.organization_id
            catalog_id = self.catalog.id
            child_org_id = self.child_org.id

        org_item = self.session.query(SharedLibraryItem).filter(
            SharedLibraryItem.organization_id == org_id,
            SharedLibraryItem.item_id == item_id
        ).first()
        available_qty_before_borrowed = org_item.quantity_available
        # self.session.close_all()
        # -- when
        body = {'href': 'http://localhost//organizations/{org_id}'
                        '/shared-catalogs/{catalog_id}/{item_id}'.format(
                            org_id=child_org_id,
                            catalog_id=catalog_id,
                            item_id=item_id, )}

        response = self.client.post('/v1/users/current-user/borrowed-items',
                                    data=json.dumps(body),
                                    headers=self.headers)

        # -- then
        self.assertEqual(response.status_code, CREATED, response.data)
        # get qty available now after borrowed:
        # self.session = db.session()
        # org_item = self.session.query(SharedLibraryItem).filter(
        #     SharedLibraryItem.organization_id == org_id,
        #     SharedLibraryItem.item_id == item_id
        # ).first()
        self.session.add(org_item)
        actual_qty = org_item.quantity_available if org_item else -1

        ubw = UserBorrowedItem.query.filter(
            UserBorrowedItem.user_id == USER_ID,
            UserBorrowedItem.catalog_item_item_id == item_id,
            UserBorrowedItem.catalog_item_catalog_id == catalog_id
        ).first()
        if ubw:
            expected_body = self.get_expected_response(ubw)
        else:
            expected_body = None
        actual = json.loads(response.data)
        self.assertDictEqual(actual, expected_body)
        self.assertEqual(actual_qty, available_qty_before_borrowed - 1)

        self.assertTrue(stock_solr.called)

    @patch('app.eperpus.members.update_stock_solr')
    def test_return_item(self, stock_solr):
        with self.on_session(self.catalog, self.item2, self.child_org, self.user_borrowed_item2):
            item_id = self.item2.id
            org_id = self.catalog.organization_id
            catalog_id = self.catalog.id
            child_org_id = self.child_org.id
            borrow_id = self.user_borrowed_item2.id

        body = {'href': 'http://localhost//organizations/{org_id}'
                        '/shared-catalogs/{catalog_id}/{item_id}'.format(
                            org_id=child_org_id,
                            catalog_id=catalog_id,
                            item_id=item_id, ), }

        response = self.client.delete('/v1/users/current-user/borrowed-items/{}'.format(borrow_id),
                                      data=json.dumps(body),
                                      headers=self.headers)

        self.assertEqual(response.status_code, NO_CONTENT, response.data)
        current_ubw_data = UserBorrowedItem.query.get(borrow_id)
        actual_returned_time = current_ubw_data.returned_time
        self.assertIsNotNone(actual_returned_time)
        self.assertTrue(stock_solr.called)

    def test_extend_borrowing_time(self):
        with self.on_session(self.catalog, self.item2, self.child_org, self.user_borrowed_item2):
            item_id = self.item2.id
            org_id = self.catalog.organization_id
            catalog_id = self.catalog.id
            child_org_id = self.child_org.id
            borrow_id = self.user_borrowed_item2.id

        current_ubw_data = self.session.query(UserBorrowedItem).get(borrow_id)
        current_ubw_data.returned_time = None
        self.session.add(current_ubw_data)
        self.session.commit()

        body = {'href': 'http://localhost//organizations/{org_id}'
                        '/shared-catalogs/{catalog_id}/{item_id}'.format(
                            org_id=child_org_id,
                            catalog_id=catalog_id,
                            item_id=item_id, ), }

        response = self.client.put('/v1/users/current-user/borrowed-items/{}'.format(borrow_id),
                                   data=json.dumps(body),
                                   headers=self.headers)

        # if response.status_code == OK:
        #     current_ubw_data = UserBorrowedItem.query.get(self.user_borrowed_item2.id)
        #     expected_time = get_utc().replace(
        #         minute=0, second=0, microsecond=0)
        #     actual_time = current_ubw_data.borrowing_start_time.replace(minute=0, second=0, microsecond=0)
        #     self.assertEqual(actual_time, expected_time)

        self.assertEqual(response.status_code, OK, response.data)

    def test_borrowed_item_ownership(self):
        # test get_current_borrow_status method for Item Download API
        with self.on_session(self.item1, self.item4, self.item5):
            ownership_status = get_current_borrow_status(user_id=USER_ID, item_id=self.item1.id)
            self.assertEqual(ownership_status, ItemOwnershipStatus.borrowed_item)

            # test returned item, status should be not owend
            ownership_status = get_current_borrow_status(user_id=USER_ID, item_id=self.item4.id)
            self.assertEqual(ownership_status, ItemOwnershipStatus.does_not_own)

            ownership_status2 = get_current_borrow_status(user_id=USER_ID, item_id=self.item5.id)
            self.assertEqual(ownership_status2, ItemOwnershipStatus.does_not_own)


class BorrowedItemListTests(E2EBase):
    """ API test for the search endpoint: /v1/users/<some-user>/borrowed-items
    """
    def setUp(self):
        super(BorrowedItemListTests, self).setUp()
        self.eperpus = SharedLibraryFactory()
        self.catalog = CatalogFactory(shared_library=self.eperpus)
        self.eperpus_manager = self.create_user(RoleType.verified_user)
        self.session.add(
            OrganizationUser(user=self.eperpus_manager, organization=self.eperpus, is_manager=True)
        )

        self.eperpus_member = self.create_user(RoleType.verified_user)
        self.session.add(
            OrganizationUser(user=self.eperpus_member, organization=self.eperpus)
        )

        self.session.commit()

    def create_and_borrow_item(self, catalog, user):
        """ Generates a new CatalogItem in the specified catalog, and then automatically borrows it by the 'user'.

        :return: Tuple containing the catalog_item and the newly-borrowed item for the 'user' param
        """
        catalog_item = CatalogItemFactory(
            catalog=catalog,
            shared_library_item=SharedLibraryItemFactory(
                shared_library=catalog.shared_library,
                quantity=10,
                quantity_available=9
            )
        )
        borrowed_item = UserBorrowedItem(
            user=user,
            catalog_item=catalog_item,
        )
        self.session.add(borrowed_item)
        self.session.commit()
        return catalog_item, borrowed_item

    def test_can_search_with_current_user_shortcut(self):
        """ BASIC: Users can search their own borrowed items using the 'current-user' shortcut.
        """
        self.session.add(self.eperpus_member)
        self.create_and_borrow_item(self.catalog, self.eperpus_member)

        self.set_api_authorized_user(self.eperpus_member)

        response = self.api_client.get('/v1/users/current-user/borrowed-items', headers=self.build_headers())

        self.assertEqual(response.status_code, OK)

        self.assertEqual(len(json.loads(response.data).get('items', [])), 1)

    def test_can_search_own_borrowed_items(self):
        """ BASIC: Users can search their own borrowed items.
        """
        self.session.add(self.eperpus_member)
        self.create_and_borrow_item(self.catalog, self.eperpus_member)

        self.set_api_authorized_user(self.eperpus_member)

        response = self.api_client.get(
            '/v1/users/{0.id}/borrowed-items'.format(self.eperpus_member),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, OK)

        self.assertEqual(len(json.loads(response.data).get('items', [])), 1)

    def test_admin_can_search_members_borrowed_items(self):
        """ BASIC: Admins can search borrowed items on other users
        """
        self.session.add(self.eperpus_member)
        self.session.add(self.eperpus_manager)
        self.create_and_borrow_item(self.catalog, self.eperpus_member)

        self.set_api_authorized_user(self.eperpus_manager)

        response = self.api_client.get(
            '/v1/users/{0.id}/borrowed-items'.format(self.eperpus_member),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, OK)

        self.assertEqual(len(json.loads(response.data).get('items', [])), 1)

    def test_user_cannot_search_other_users(self):
        """ SECURITY: Users cannot search borrowed items on other users.
        """
        self.create_and_borrow_item(self.catalog, self.eperpus_manager)

        self.set_api_authorized_user(self.eperpus_member)

        response = self.api_client.get(
            '/v1/users/{0.id}/borrowed-items'.format(self.eperpus_manager),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, FORBIDDEN)


class AdministratorForceReturnTests(E2EBase):

    def setUp(self):
        # make an eperpus.
        super(AdministratorForceReturnTests, self).setUp()
        self.eperpus = SharedLibraryFactory()
        self.catalog = CatalogFactory(shared_library=self.eperpus)
        self.eperpus_manager = self.create_user(RoleType.verified_user)
        self.session.add(
            OrganizationUser(user=self.eperpus_manager, organization=self.eperpus, is_manager=True)
        )

        self.eperpus_member = self.create_user(RoleType.verified_user)
        self.session.add(
            OrganizationUser(user=self.eperpus_member, organization=self.eperpus)
        )

        self.session.commit()

    def create_and_borrow_item(self, catalog, user):
        """ Generates a new CatalogItem in the specified catalog, and then automatically borrows it by the 'user'.

        :return: Tuple containing the catalog_item and the newly-borrowed item for the 'user' param
        """
        catalog_item = CatalogItemFactory(
            catalog=catalog,
            shared_library_item=SharedLibraryItemFactory(
                quantity=10,
                quantity_available=9
            )
        )
        borrowed_item = UserBorrowedItem(
            user=user,
            catalog_item=catalog_item,
        )
        self.session.add(borrowed_item)
        self.session.commit()
        return catalog_item, borrowed_item

    @patch('app.eperpus.members.update_stock_solr')
    def test_admin_force_return(self, mock_solr_update):
        """ BASIC: Organization admin can force book return for other users.
        """
        self.session.add(self.eperpus_member)
        catalog_item, borrowed_item = self.create_and_borrow_item(self.catalog, self.eperpus_member)

        self.set_api_authorized_user(self.eperpus_manager)

        response = self.api_client.delete(
            '/v1/users/{0.id}/borrowed-items/{1.id}'.format(self.eperpus_member, borrowed_item),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, NO_CONTENT)

        with self.on_session(catalog_item, borrowed_item):
            self.assertEqual(catalog_item.shared_library_item.quantity_available, 10)
            self.assertIsNotNone(borrowed_item.returned_time)
            self.assertTrue(mock_solr_update.called)

    @patch('app.eperpus.members.update_stock_solr')
    def test_current_user_return(self, mock_solr_update):
        """ BASIC: Organization member can return their own books with the shortcut url 'current-user'
        """
        self.session.add(self.eperpus_member)
        catalog_item, borrowed_item = self.create_and_borrow_item(self.catalog, self.eperpus_member)

        self.set_api_authorized_user(self.eperpus_member)

        response = self.api_client.delete(
            '/v1/users/current-user/borrowed-items/{0.id}'.format(borrowed_item),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, NO_CONTENT)

        with self.on_session(catalog_item, borrowed_item):
            self.assertEqual(catalog_item.shared_library_item.quantity_available, 10)
            self.assertIsNotNone(borrowed_item.returned_time)
            self.assertTrue(mock_solr_update.called)

    @patch('app.eperpus.members.update_stock_solr')
    def test_security(self, mock_solr_update):
        """ SECURITY: Non-managers cannot force return items for other members.
        """
        self.session.add(self.eperpus_member)
        catalog_item, borrowed_item = self.create_and_borrow_item(self.catalog, self.eperpus_manager)

        self.set_api_authorized_user(self.eperpus_member)

        response = self.api_client.delete(
            '/v1/users/{0.id}/borrowed-items/{1.id}'.format(self.eperpus_manager, borrowed_item),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, FORBIDDEN)

        with self.on_session(catalog_item, borrowed_item):
            self.assertEqual(catalog_item.shared_library_item.quantity_available, 9)
            self.assertIsNone(borrowed_item.returned_time)
            self.assertFalse(mock_solr_update.called)


@istest
class EperpusBorrowedItemHistoryTests(TestCase):
    # test for for api: /v1/organizations/<organization_id>/borrowed-items-history

    maxDiff = None

    max_borrowed_time = relativedelta(days=7)

    @classmethod
    def setUpClass(cls):
        db.session().close_all()
        db.drop_all()
        db.create_all()

        cls.init_data()
        cls.client = app.test_client(use_cookies=False)
        cls.headers = {
            'Accept': 'application/vnd.scoop.v3+json',
            'User-Agent': 'kg eperpus ios/v2.0.0'
        }
        with app.test_request_context('/'):
            cls.headers['Authorization'] = 'JWT {}'.format(
                jwt_encode_from_user(cls.user)
            )

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.drop_all()

    @classmethod
    def init_data(cls):
        s = db.session()
        s.expire_on_commit = False
        cls.library_org = SharedLibraryFactory(
            id=11,
            user_concurrent_borrowing_limit=10,
            reborrowing_cooldown=relativedelta(days=0),
            borrowing_time_limit=cls.max_borrowed_time)
        cls.child_org = SharedLibraryFactory(
            id=12,
            parent_organization=cls.library_org)
        cls.user = UserFactory(id=USER_ID)
        user2 = UserFactory(id=2114)

        org_user = OrganizationUser(organization_id=11,
                                    user_id=USER_ID,
                                    is_manager=True)
        s.add(org_user)

        author = AuthorFactory()
        cls.item1 = ItemFactory(id=1)
        cls.item1.authors.append(author)
        cls.item2 = ItemFactory(id=2)
        # item 3, item not borrowed yet
        cls.item3 = ItemFactory(id=3)
        # item 4, item is borrowed and already returned, should not shown in list of borrowed items
        cls.item4 = ItemFactory(id=4)

        shared_lib_item1 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item1,
            quantity=10,
            quantity_available=10,
            item_name_override='{} additional title'.format(cls.item1.name)
        )
        shared_lib_item2 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item2,
            quantity=10,
            quantity_available=7,
            item_name_override='{} additional title'.format(cls.item2.name)
        )
        shared_lib_item3 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item3,
            quantity=10,
            quantity_available=8,
            item_name_override='{} additional title'.format(cls.item3.name)
        )
        shared_lib_item4 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item4,
            quantity=10,
            quantity_available=4,
            item_name_override='{} additional title'.format(cls.item4.name)
        )
        cls.catalog = CatalogFactory(id=1, shared_library=cls.library_org, )
        catalog_item1 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item1, )
        catalog_item2 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item2, )
        catalog_item3 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item3, )
        catalog_item4 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item4, )
        cls.user_borrowed_item1 = UserBorrowedItemFactory(
            id=1000,
            user=cls.user,
            catalog_item=catalog_item1,
            borrowing_start_time=get_utc() - timedelta(days=5),
            returned_time=None,
        )
        cls.user_borrowed_item2 = UserBorrowedItemFactory(
            id=1001,
            user=cls.user,
            catalog_item=catalog_item2,
            borrowing_start_time=get_utc() - timedelta(days=4),
            returned_time=None,
        )
        cls.user_borrowed_item4 = UserBorrowedItemFactory(
            id=1004,
            user=cls.user,
            catalog_item=catalog_item4,
            borrowing_start_time=get_utc() - timedelta(days=10),
            returned_time=get_utc() - timedelta(days=7),
        )
        user_borrowed_item5 = UserBorrowedItemFactory(
            id=1005,
            user=user2,
            catalog_item=catalog_item4,
            borrowing_start_time=get_utc() - timedelta(days=10),
            returned_time=get_utc() - timedelta(days=7),
        )
        s.commit()

    def test_get_list_all(self):
        # test: history of borrowed item per organizations
        user_items = UserBorrowedItem.query.filter(
            UserBorrowedItem.catalog_item_org_id.in_([11, 12])
        ).order_by(desc(UserBorrowedItem.id)).limit(20).all()
        items = []
        with app.app_context():
            for ubw in user_items:
                items.append(self.get_expected_response_list(ubw))

        expected = {
            "items": items,
            "metadata": {
                "resultset": {
                    "offset": 0,
                    "limit": 20,
                    "count": len(user_items)
                }
            }
        }

        response = self.client.get('/v1/organizations/11/borrowed-items-history',
                                   headers=self.headers)
        if response.status_code == OK:
            actual = json.loads(response.data)
            self.assertDictEqual(actual, expected)
        self.assertEqual(response.status_code, OK, response.data)

    def test_get_list_filter_by_item(self):
        # test: history of borrowed item per organizations per item
        user_items = UserBorrowedItem.query.filter(
            UserBorrowedItem.catalog_item_org_id.in_([11, 12]),
            UserBorrowedItem.catalog_item_item_id == self.item1.id
        ).order_by(desc(UserBorrowedItem.id)).limit(20).all()
        items = []
        with app.app_context():
            for ubw in user_items:
                items.append(self.get_expected_response_list(ubw))

        expected = {
            "items": items,
            "metadata": {
                "resultset": {
                    "offset": 0,
                    "limit": 20,
                    "count": len(user_items)
                }
            }
        }

        response = self.client.get('/v1/organizations/11/borrowed-items-history?item_id={}'.format(self.item1.id),
                                   headers=self.headers)
        if response.status_code == OK:
            actual = json.loads(response.data)
            self.assertDictEqual(actual, expected)
        self.assertEqual(response.status_code, OK, response.data)

    def test_get_list_filter_by_user(self):
        # test: history of borrowed item per organizations per item
        user_items = UserBorrowedItem.query.filter(
            UserBorrowedItem.catalog_item_org_id.in_([11, 12]),
            UserBorrowedItem.user_id == self.user.id
        ).order_by(desc(UserBorrowedItem.id)).limit(20).all()
        items = []
        with app.app_context():
            for ubw in user_items:
                items.append(self.get_expected_response_list(ubw))

        expected = {
            "items": items,
            "metadata": {
                "resultset": {
                    "offset": 0,
                    "limit": 20,
                    "count": len(user_items)
                }
            }
        }

        response = self.client.get('/v1/organizations/11/borrowed-items-history?user_id={}'.format(self.user.id),
                                   headers=self.headers)
        if response.status_code == OK:
            actual = json.loads(response.data)
            self.assertDictEqual(actual, expected)
        self.assertEqual(response.status_code, OK, response.data)

    @nottest
    def get_expected_response_list(self, ubw):
        expire_time = datetime_to_isoformat(ubw.borrowing_start_time + self.max_borrowed_time)
        org_item = ubw.catalog_item.shared_library_item
        item = org_item.item
        authors = []
        for author in item.authors.all():
            authors.append({
                "id": author.id,
                "title": author.name,
                "href": "http://localhost/v1/authors/{}".format(author.id)
            })
        return {
            "id": ubw.id,
            "title": org_item.item_name_override if org_item.item_name_override else item.name,
            "href": "http://localhost/v1/users/current-user/borrowed-items/{}".format(ubw.id),
            "details": {
                "href": "http://localhost/v1/items/{}".format(item.id),
                "title": "Detil Item",
                "id": item.id
            },
            "catalog_item": {
                "href": "http://localhost/v1/organizations/{org_id}/"
                        "shared-catalogs/{cat_id}/{item_id}".format(
                            org_id=ubw.catalog_item_org_id,
                            cat_id=ubw.catalog_item_catalog_id,
                            item_id=item.id
                        ),
                "title": "Detil Item di Katalog",
            },
            "cover_image": {
                "href": current_app.config['MEDIA_BASE_URL'] + item.image_normal,
                "title": "Gambar Sampul"
            },
            "borrowing_start_time": datetime_to_isoformat(ubw.borrowing_start_time),
            "expires": expire_time,
            "vendor": {
                "href": "http://localhost/v1/vendors/{}".format(item.brand.vendor.id),
                "title": item.brand.vendor.name
            },
            "authors": authors,
            "download": {
                "href": "http://localhost/v1/items/{}/download".format(item.id),
                "title": "Download buat Baca Offline"
            },
            "currently_available": org_item.quantity_available,
            # "last_read_time": "2016-10-10T01:23:45Z",
            # "last_read_page": 12
            "catalog": {
                "href": "http://localhost/v1/organizations/{org_id}/"
                        "shared-catalogs/{cat_id}".format(
                    org_id=ubw.catalog_item_org_id,
                    cat_id=ubw.catalog_item_catalog_id
                ),
                "rel": "catalog",
                "title": ubw.catalog_item.catalog.name,
                "id": ubw.catalog_item.catalog.id,
            },
            "returned_time": datetime_to_isoformat(ubw.returned_time) if ubw.returned_time else None
        }

    @nottest
    def get_expected_response(self, user_borrow_item):
        with app.app_context():
            expire_time = datetime_to_isoformat(user_borrow_item.borrowing_start_time + self.max_borrowed_time)
            org_item = user_borrow_item.catalog_item.shared_library_item
            item = org_item.item
            catalog = user_borrow_item.catalog_item.catalog
            shared_library_org = user_borrow_item.catalog_item.shared_library_item.shared_library
            expected = {
                "id": user_borrow_item.id,
                "title": org_item.item_name_override if org_item.item_name_override else item.name,
                "details": {
                    "href": "http://localhost/v1/items/{}".format(item.id),
                    "title": "Detil Item",
                    "id": item.id
                },
                "catalog_item": {
                    "href": "http://localhost/v1/organizations/{org_id}/"
                            "shared-catalogs/{cat_id}/{item_id}".format(
                                org_id=user_borrow_item.catalog_item_org_id,
                                cat_id=user_borrow_item.catalog_item_catalog_id,
                                item_id=item.id
                            ),
                    "title": "Detil Item di Katalog",
                },
                "owner": {
                    "href": "http://localhost/v1/organizations/{}".format(shared_library_org.id),
                    "rel": shared_library_org.type,
                    "title": shared_library_org.name
                },
                "catalog": {
                    "href": "http://localhost/v1/organizations/{org_id}/"
                            "shared-catalogs/{cat_id}".format(
                        org_id=shared_library_org.id,
                        cat_id=catalog.id
                    ),
                    "rel": "catalog",
                    "title": catalog.name
                },
                "cover_image": {
                    "href": current_app.config['MEDIA_BASE_URL'] + item.image_normal,
                    "title": "Gambar Sampul"
                },
                "download": {
                    "href": "http://localhost/v1/items/{}/download".format(item.id),
                    "title": "Download buat Baca Offline"
                },
                "streaming": {
                    "href": "http://localhost/v1/items/{}/web-reader".format(item.id),
                    "title": "Baca Sekarang"
                },
                "borrowing_start_time": datetime_to_isoformat(user_borrow_item.borrowing_start_time),
                "expires": expire_time,
                "last_read_time": None,
                "last_read_page": None,
                "vendor": {
                    "href": "http://localhost/v1/vendors/{}".format(item.brand.vendor.id),
                    "title": item.brand.vendor.name
                },
                "currently_available": org_item.quantity_available,
            }
            return expected
