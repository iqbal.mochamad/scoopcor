from __future__ import unicode_literals, absolute_import

import json
from httplib import CREATED, FORBIDDEN, METHOD_NOT_ALLOWED, OK

from flask import url_for
from sqlalchemy import desc

from app import app
from app.eperpus.tests.base import EperpusE2EBase
from app.users.organizations import OrganizationType
from app.eperpus.catalogs import Catalog
from app.eperpus.tests.fixtures import CatalogFactory, CatalogItemFactory


class SharedLibraryCatalogTests(EperpusE2EBase):

    # screw this and just make an EPERPUS BASE .. this is confusing and I'll forget how it works eventually.
    force_organization_fixture_type = OrganizationType.shared_library

    def api_create_catalog(self, organization, data):
        return self.api_client.post(
            '/v1/organizations/{shared_lib.id}/shared-catalogs'.format(shared_lib=organization),
            data=json.dumps(data),
            headers=self.build_headers(has_body=True)
        )

    def api_search_catalogs(self, organization, include_child=False):
        url = '/v1/organizations/{shared_lib.id}/shared-catalogs'.format(shared_lib=organization)
        if include_child:
            url = '{}?include-child=1'.format(url)
        return self.api_client.get(
            url,
            headers=self.build_headers()
        )

    def get_catalog_url(self, catalog):
        with app.test_request_context('/'):
            self.session.add(catalog)
            # self.session.add(catalog.shared_library)
            url = catalog.api_url
        return url

    def test_create(self):
        """ BASIC:  Organization managers can create catalogs.
        """
        self.set_api_authorized_user(self.organization_manager)

        self.session.add(self.child_organization)
        child_id = self.child_organization.id
        original_catalog_count = self.child_organization.catalogs.count()
        self.session.expunge(self.child_organization)
        self.session.expunge_all()

        response = self.api_create_catalog(self.child_organization, {'title': 'Some catalog'})

        self.assertEqual(response.status_code, CREATED)

        catalog_count = self.session.query(Catalog).filter_by(organization_id=child_id).count()
        self.assertEqual(original_catalog_count + 1, catalog_count)

        newest_catalog = self.session.query(Catalog).filter_by(
            organization_id=child_id).order_by(desc(Catalog.created)).first()
        with app.test_request_context():
            catalog_url = newest_catalog.api_url

        self.assertEqual(response.headers['LOCATION'], catalog_url)
        self.assertEqual(newest_catalog.name, 'Some catalog')
        self.assertEqual(newest_catalog.shared_library.id, self.child_organization.id)

    def test_only_managers_can_create(self):
        """ SECURITY:  Non-managers cannot create catalogs.
        """
        self.set_api_authorized_user(self.organization_member)

        response = self.api_create_catalog(self.child_organization, {'title': 'Some catalog'})

        self.assertEqual(response.status_code, FORBIDDEN)

        self.assertScoopErrorFormat(response)

    def test_search(self):
        """ BASIC:  Can search catalogs within an organization.
        """
        catalog_to_generate = 5

        self.session.add(self.child_organization)

        prior_catalogs_count = self.child_organization.catalogs.count()

        [CatalogFactory(shared_library=self.child_organization) for _ in range(catalog_to_generate)]
        self.session.commit()

        self.assign_user_to_organization(self.organization_member, self.child_organization)

        self.set_api_authorized_user(self.organization_member)
        response = self.api_search_catalogs(self.child_organization)
        self.assert_catalog_list_response(response, catalog_to_generate, prior_catalogs_count)

        # test search by parent org id (?include-child=true
        # self.session.add(self.child_organization)
        self.set_api_authorized_user(self.organization_member)
        self.session.add(self.child_organization)
        response = self.api_search_catalogs(self.child_organization.parent_organization, True)
        self.assert_catalog_list_response(response, catalog_to_generate, prior_catalogs_count)

    def assert_catalog_list_response(self, response, catalog_to_generate, prior_catalogs_count):
        self.assertEqual(response.status_code, OK)

        self.session.add(self.child_organization)
        self.session.refresh(self.child_organization)

        ctx = app.test_request_context('/')
        ctx.push()

        expected_json = {
            'catalogs': [
                {
                    'id': c.id,
                    'title': c.name,
                    'href': c.api_url,
                    'shared_library': {
                        'id': c.shared_library.id,
                        'title': c.shared_library.name,
                        'href': c.shared_library.api_url,
                    }
                } for c in self.child_organization.catalogs
            ],
            'metadata': {
                'resultset': {
                    'offset': 0,
                    'limit': 20,
                    'count': catalog_to_generate + prior_catalogs_count
                }
            }
        }

        ctx.pop()

        self.assertUnorderedJsonEqual(response, expected_json)

    def test_only_members_can_search(self):
        """ SECURITY: User must be a direct member of a catalog, in order to search against it.
        """
        self.set_api_authorized_user(self.organization_member)
        response = self.api_search_catalogs(self.child_organization)
        self.assertEqual(response.status_code, FORBIDDEN)
        self.assertScoopErrorFormat(response)

    def test_search_items(self):
        """ BASIC:  Can search on the items available within a catalog.

        :return:
        """
        ITEMS_TO_CREATE = 5

        catalog = CatalogFactory(shared_library=self.child_organization)
        for _ in range(ITEMS_TO_CREATE):
            CatalogItemFactory(catalog=catalog)
        self.session.commit()

        response = self.api_client.get(
            '/v1/organizations/{shared_lib.id}/{catalog.id}'.format(shared_lib=self.child_organization, catalog=catalog),
            headers=self.build_headers()
        )

