from __future__ import unicode_literals

import json
from httplib import OK
from unittest import TestCase

import mock
from dateutil.relativedelta import relativedelta
from flask import current_app

from nose.tools import istest, nottest

from app import app, db
from app.auth.helpers import jwt_encode_from_user
from app.eperpus.catalogs import CatalogItem, get_estimated_next_available_time, Catalog
from app.eperpus.models import WatchList
from app.eperpus.tests.fixtures import SharedLibraryFactory, SharedLibraryItemFactory, CatalogFactory, \
    CatalogItemFactory
from app.items.previews import PreviewUrlBuilder
from app.users.users import User
from app.users.tests.fixtures import UserFactory
from app.utils.datetimes import datetime_to_isoformat
from tests.fixtures.sqlalchemy import ItemFactory, AuthorFactory


# test for shared catalog list api : /v1/organizations/12/shared-catalogs/1

USER_ID = 1234


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    # if args[0] == '/v1/organizations/11/users'':
    # return MockResponse({"key1": "value1"}, 200)
    return MockResponse(mock_response_catalog_items_search(), 200)
    # else:
    # return MockResponse({}, 404)


def mock_response_catalog_items_search():
    items = []
    session = db.session()
    for cat_item in session.query(CatalogItem).all():
        items.append({'item_id': cat_item.shared_library_item_item_id})

    response = {
        "response": {
            "docs": items,
            "numFound": len(items),
        },
        "facet_counts": {
            "facet_queries": {},
            ""
            "facet_fields": {
                "item_categories":[
                    "Automotive", 2,
                    "Crime & Thrillers", 1
                ],
                "author_names": [
                    "ramdani", 2
                ],
                "item_languages":[
                    "id", 2,
                    "en", 0
                ]
            }
        },
        "spellcheck": {
            "suggestions": [
                "sedekha", {
                    "numFound": 2,
                    "startOffset": 0,
                    "endOffset": 7,
                    "origFreq": 0,
                    "suggestion": [
                        {
                            "word": "sedekah",
                            "freq": 8
                        },
                        {
                            "word": "selekta",
                            "freq": 1
                        }
                    ]
                }
            ],
            "correctlySpelled": 'false'
        }
    }

    session.close()
    return response


@istest
class SharedLibraryItemTestBase(TestCase):

    maxDiff = None

    @classmethod
    def setUpClass(cls):
        session = db.session
        cls.user = session.query(User).get(USER_ID)
        cls.headers = {
            'Accept': 'application/vnd.scoop.v3+json',
        }
        with app.test_request_context('/'):
            cls.headers['Authorization'] = 'JWT {}'.format(
                    jwt_encode_from_user(cls.user)
                )
        cls.ctx = app.test_request_context('/')
        cls.ctx.push()

    @classmethod
    def tearDownClass(cls):
        cls.ctx.pop()
        db.session().close_all()

    def get_item_response(self, app1, item, next_available, org_item, catalog_id):
        return {
            "id": item.id,
            "title": org_item.item_name_override if org_item.item_name_override else item.name,
            "subtitle": item.issue_number if item.item_type != item.Types.book.value else item.brand.vendor.name,
            "item_type": item.item_type,
            "href": "http://localhost/v1/organizations/{org_id}/"
                    "shared-catalogs/{cat_id}/{item_id}".format(
                org_id=org_item.organization_id,
                cat_id=catalog_id,
                item_id=item.id
            ),
            "details": {
                "id": item.id,
                "href": "http://localhost/v1/items/{}".format(item.id),
                "title": "Detil Item",
            },
            "borrow_item": {
                "href": "http://localhost/v1/users/current-user/borrowed-items",
                "title": "Pinjam Item",
            },
            "cover_image": {
                "href": app1.config['MEDIA_BASE_URL'] + item.image_normal,
                "title": "Gambar Sampul"
            },
            "currently_available": org_item.quantity_available,
            "total_in_collection": org_item.quantity,
            "max_borrow_time": "P0Y0M7DT0H0M0S",
            # "max_borrow_time": "P{0.years}Y{0.months}M{0.days}DT{0.hours}H{0.minutes}M{0.seconds}S".format(
            #     org_item.shared_library.borrowing_time_limit),
            "vendor": {
                "href": "http://localhost/v1/vendors/{}".format(item.brand.vendor.id),
                "title": item.brand.vendor.name
            },
        }


@istest
class SharedCatalogItemDetailsApiTests(SharedLibraryItemTestBase):

    catalog_id = 2
    item_id = 1
    url = '/v1/organizations/12/shared-catalogs/{catalog_id}/{item_id}' \
          '?facets=author_names:Novita Tandry&facets=category_names:Parenting'.format(
                catalog_id=catalog_id, item_id=item_id)

    def get_item_response(self, app1, item, next_available, org_item, catalog_id):
        expected = super(SharedCatalogItemDetailsApiTests, self).get_item_response(
            app1, item, next_available, org_item, catalog_id)
        authors = []
        for author in item.authors.all():
            authors.append({
                "id": author.id,
                "title": author.name,
                "href": "http://localhost/v1/authors/{}".format(author.id)
            })
        watch_list = db.session.query(WatchList).filter_by(
            user_id=USER_ID,
            item_id=item.id,
            catalog_id=catalog_id
        ).first()
        catalog = db.session.query(Catalog).get(catalog_id)

        # add more info specific to catalog-item details
        expected.update({
            "next_available": next_available,
            "authors": authors,
            "description": item.description,
            "brand": {
                "id": item.brand.id,
                "title": item.brand.name,
                "href": item.brand.api_url},
            "watch_list": {
                "id": watch_list.id,
                "title": 'watch list',
                "href": watch_list.api_url} if watch_list else None,
            "catalog": {
                "id": catalog.id,
                "title": catalog.name,
                "href": catalog.api_url},
            "high_res_image": {
                "href": app1.config['MEDIA_BASE_URL'] + item.image_highres,
                "title": "Gambar Sampul High Res"
            },
            "previews": PreviewUrlBuilder(app.config['BASE_URL']).make_urls(item, True),
        })
        return expected

    def test_subscribed_catalog_list_items(self):
        with app.app_context():

            expected = self.get_expected_response()

            client = app.test_client(use_cookies=False)
            response = client.get(self.url, headers=self.headers)
            if response.status_code == OK:
                actual = json.loads(response.data)
                self.assertDictEqual(actual, expected)
            self.assertEqual(response.status_code, OK, response)

    def get_expected_response(self):
        items = []
        session = db.session()
        cat_item = session.query(CatalogItem).filter_by(
            catalog_id=self.catalog_id,
            shared_library_item_item_id=self.item_id).first()

        org_item = cat_item.shared_library_item
        item = org_item.item
        next_available = get_estimated_next_available_time(session, cat_item)
        next_available = datetime_to_isoformat(next_available) if next_available else None
        session.add(item)

        expected = self.get_item_response(current_app, item, next_available,
                                          org_item, catalog_id=self.catalog_id)

        session.close_all()
        return expected


@istest
class SharedCatalogItemListApiTests(SharedLibraryItemTestBase):

    catalog_id = 2
    url = '/v1/organizations/12/shared-catalogs/{}' \
          '?facets=author_names:Novita Tandry&facets=category_names:Parenting'.format(catalog_id)

    @mock.patch('app.users.helpers.requests.get', mock.Mock(side_effect=mocked_requests_get))
    def test_subscribed_catalog_list_items(self):
        # Test get list of catalog items from SOLR (with mocked solr response)
        with app.test_request_context('/'):

            expected = self.get_expected_response()

            client = app.test_client(use_cookies=False)
            response = client.get(self.url, headers=self.headers)
            if response.status_code == OK:
                actual = json.loads(response.data)

                actual_facets = actual['metadata'].pop('facets')
                expected_facets = expected['metadata'].pop('facets')
                self.assertItemsEqual(actual_facets, expected_facets)

                self.assertDictEqual(actual, expected)
            self.assertEqual(response.status_code, OK, response)

    def test_subscribed_catalog_list_items_direct_db(self):
        # Test get list of catalog items direct query to db
        with app.test_request_context('/'):

            expected = self.get_expected_response(direct_db=True)

            client = app.test_client(use_cookies=False)
            response = client.get(self.url + "&direct-db=1", headers=self.headers)
            if response.status_code == OK:
                actual = json.loads(response.data)

                # actual_facets = actual['metadata'].pop('facets')
                # expected_facets = expected['metadata'].pop('facets')

                self.assertDictEqual(actual, expected)
            self.assertEqual(response.status_code, OK, response)

    def get_expected_response(self, direct_db=None):
        items = []
        session = db.session()
        self.catalog = session.query(Catalog).get(self.catalog_id)

        for cat_item in self.catalog.catalog_items:
            org_item = cat_item.shared_library_item
            item = org_item.item
            next_available = get_estimated_next_available_time(session, cat_item)
            next_available = datetime_to_isoformat(next_available) if next_available else None
            session.add(item)
            items.append(self.get_item_response(
                current_app, item, next_available, org_item, catalog_id=self.catalog.id))
        expected = {
            'id': self.catalog.id,
            'name': self.catalog.name,
            "items": items,
            "organization":  {
                "id": self.catalog.shared_library.id,
                "name": self.catalog.shared_library.name,
                "href": self.catalog.shared_library.api_url
            },
            "metadata": {
                "resultset": {
                    "count": 2,
                    "limit": 20,
                    "offset": 0
                },
                "facets": None if direct_db else [
                    {
                        "field_name": "item_categories",
                        "values": [
                            {
                                "value": "Automotive",
                                "count": 2,
                            },
                            {
                                "value": "Crime & Thrillers",
                                "count": 1,
                            },
                        ]
                    },
                    {
                        "field_name": "item_authors",
                        "values": [
                            {
                                "value": "ramdani",
                                "count": 2,
                            }
                        ]
                    },
                    {
                        "field_name": "item_languages",
                        "values": [
                            {
                                "value": "id",
                                "count": 2,
                            },
                            {
                                "value": "en",
                                "count": 0,
                            }
                        ]
                    }
                ],

                "spelling_suggestions": None if direct_db else [
                    {
                        "value": "sedekah",
                        "count": 8,
                    },
                    {
                        "value": "selekta",
                        "count": 1,
                    }
                ]
            }
        }
        session.close_all()
        return expected


PARENT_ORG_ID = 11
CHILD_ORG_ID = 12


def setup_module():
    db.drop_all()
    db.create_all()

    init_data()


def teardown_module():
    db.session().close_all()
    db.drop_all()


def init_data():
    s = db.session()
    s.expire_on_commit = False
    library_org = SharedLibraryFactory(
        id=PARENT_ORG_ID,
        user_concurrent_borrowing_limit=10,
        reborrowing_cooldown=relativedelta(days=0),
        borrowing_time_limit=relativedelta(days=7))
    child_org = SharedLibraryFactory(
        id=CHILD_ORG_ID,
        parent_organization=library_org)
    user = UserFactory(id=USER_ID)

    child_org.users.append(user)

    author = AuthorFactory()
    item1 = ItemFactory(id=1)
    item1.authors.append(author)
    item2 = ItemFactory(id=2)

    shared_lib_item1 = SharedLibraryItemFactory(
        shared_library=library_org,
        item=item1,
        quantity=10,
        quantity_available=0,
        item_name_override='{} additional title'.format(item1.name)
    )
    shared_lib_item2 = SharedLibraryItemFactory(
        shared_library=library_org,
        item=item2,
        quantity=20,
        quantity_available=20,
        item_name_override='{} additional title'.format(item2.name)
    )
    catalog = CatalogFactory(
        id=1,
        shared_library=library_org,
    )
    catalog2 = CatalogFactory(
        id=2,
        shared_library=child_org,
    )
    catalog_item1 = CatalogItemFactory(
        catalog=catalog2,
        shared_library_item=shared_lib_item1,
    )
    catalog_item2 = CatalogItemFactory(
        catalog=catalog2,
        shared_library_item=shared_lib_item2,
    )
    s.add(user)
    s.commit()
