from __future__ import unicode_literals, absolute_import

import json
from httplib import CREATED, OK, NO_CONTENT
from unittest import TestCase

from dateutil.relativedelta import relativedelta
from faker import Factory
from flask import g
from nose.tools import istest, nottest

from app import db, app, set_request_id
from app.auth.models import UserInfo
from app.eperpus.catalogs import CatalogItem, Catalog
from app.eperpus.libraries import SharedLibrary
from app.eperpus.tests.fixtures import CatalogFactory, SharedLibraryItemFactory, CatalogItemFactory, \
    SharedLibraryFactory
from app.users.tests.fixtures import OrganizationUserFactory
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from tests import testcases
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import ItemFactory


USER_ID = 1234
PARENT_ORG_ID = 711
CHILD_ORG_ID = 12
CATALOG_ID_UPDATE = 100


class CreateTests(testcases.CreateTestCase):
    """ Test Create a catalog

    """
    request_url = '/v1/organizations/{}/shared-catalogs'.format(CHILD_ORG_ID)
    fixture_factory = CatalogFactory
    maxDiff = None

    @classmethod
    def get_api_request_body(cls):
        with app.test_request_context("/"):
            return {
               'title': _fake.name()
            }

    def test_response_code(self):
        self.assertEqual(CREATED, self.response.status_code,
                         msg="Creating new objects returns HTTP 201.  "
                             "Got {0.status_code} \n {0.data}".format(self.response))

    def test_response_body(self):
        self.assertIsNotNone(json.loads(self.response.data)['id'], 'id is none, response {}'.format(self.response.data))
        self.assertEqual(self.request_body['title'], json.loads(self.response.data)['title'])


class UpdateTests(testcases.UpdateTestCase):
    """ Test Update a catalog

    """

    request_url = '/v1/organizations/{}/shared-catalogs/{}'.format(CHILD_ORG_ID, CATALOG_ID_UPDATE)
    fixture_factory = CatalogItemFactory

    @classmethod
    def setUpClass(cls):
        cls.items = []
        cls.catalog_name = _fake.name()
        super(UpdateTests, cls).setUpClass()

    @classmethod
    def set_up_fixtures(cls):
        # create initial data before update!
        session = db.session()
        # session.expire_on_commit = False
        parent_org = session.query(SharedLibrary).get(PARENT_ORG_ID)
        child_org = session.query(SharedLibrary).get(CHILD_ORG_ID)
        catalog = CatalogFactory(id=CATALOG_ID_UPDATE, shared_library=child_org)
        session.commit()
        return catalog

    @classmethod
    def get_api_request_body(cls):
        with app.test_request_context('/'):
            return {
                'id': CATALOG_ID_UPDATE,
                'title': cls.catalog_name
            }
    #
    # def test_catalog_item_exists(self):
    #     session = db.session()
    #     catalog_item = session.query(CatalogItem).filter_by(catalog_id=CATALOG_ID_UPDATE).all()
    #     # we add by 1 because 1 is already exists before
    #     self.assertEqual(len(catalog_item), len(self.items)+1)

    def test_response_body(self):
        self.assertIsNotNone(json.loads(self.response.data)['id'], 'id is none, response {}'.format(self.response.data))
        self.assertEqual(self.request_body['title'], json.loads(self.response.data)['title'])
        session = db.session()
        updated_catalog = session.query(Catalog).get(CATALOG_ID_UPDATE)
        actual_name = updated_catalog.name
        session.expunge(updated_catalog)
        self.assertEqual(self.request_body['title'], actual_name)


class LinkUnlinkTests(TestCase):
    """ Test add item to a catalog (LINK)
    and remove item from a catalog (UNLINK)

    """
    CATALOG_ID = 9910
    request_url = '/v1/organizations/{}/shared-catalogs/{}'.format(CHILD_ORG_ID, CATALOG_ID)

    headers = {
        'Accept': 'application/vnd.scoop.v3+json',
        'Content-Type': 'application/json'
    }

    @classmethod
    def setUpClass(cls):
        # create initial data before update!
        session = db.session()
        # session.expire_on_commit = False
        parent_org = session.query(SharedLibrary).get(PARENT_ORG_ID)
        child_org = session.query(SharedLibrary).get(CHILD_ORG_ID)
        catalog = CatalogFactory(id=cls.CATALOG_ID, shared_library=child_org)

        # prepare some new item for tests insert to catalog item
        cls.items = []
        for _ in xrange(5):
            item = ItemFactory()
            shared_library_item = SharedLibraryItemFactory(
                shared_library=parent_org,
                item=item)
            cls.items.append(item)

        # add item that will be remove for remove testing
        cls.item_remove = ItemFactory()
        shared_library_item_remove = SharedLibraryItemFactory(
            shared_library=parent_org,
            item=cls.item_remove)
        catalog_item_remove = CatalogItemFactory(catalog=catalog, shared_library_item=shared_library_item_remove)

        # create an item that already exists in catalog item
        item = ItemFactory()
        shared_library_item = SharedLibraryItemFactory(
            shared_library=parent_org,
            item=item)
        catalog_item = CatalogItemFactory(catalog=catalog, shared_library_item=shared_library_item)
        session.commit()

        cls.client = app.test_client(use_cookies=False)

    def test_link(self):
        session = db.session()
        session.add(self.items[0])
        new_item_id = self.items[0].id
        response = self.client.open(
            self.request_url,
            method='LINK',
            data=json.dumps({'id': new_item_id}),
            headers=self.headers)
        self.assertEqual(response.status_code, CREATED,
                         "response status code != 200(OK). Response {}".format(response.data))
        new_catalog_item = session.query(CatalogItem).filter_by(
            catalog_id=self.CATALOG_ID,
            shared_library_item_item_id=new_item_id
        ).first()
        self.assertIsNotNone(new_catalog_item)

    def test_unlink(self):
        session = db.session()
        session.add(self.item_remove)
        remove_item_id = self.item_remove.id
        response = self.client.open(
            self.request_url,
            method='UNLINK',
            data=json.dumps({'id': remove_item_id}),
            headers=self.headers)
        self.assertEqual(response.status_code, NO_CONTENT,
                         "response status code != 204(NO_CONTENT). Response {}".format(response.data))
        updated_catalog_item = session.query(CatalogItem).filter_by(
            catalog_id=self.CATALOG_ID,
            shared_library_item_item_id=remove_item_id,
        ).first()
        self.assertFalse(updated_catalog_item.is_active)

_fake = Factory.create()


def init_g_current_user():
    session = db.session()
    g.current_user = session.query(User).get(USER_ID)
    g.user = UserInfo(username=g.current_user.username,
                      user_id=USER_ID,
                      token='abcd',
                      perm=['can_read_write_global_all', ])
    session.close()


original_before_request_funcs = app.before_request_funcs


def setup_module():
    db.engine.dispose()
    db.drop_all()
    db.create_all()
    init_data()
    app.before_request_funcs = {
        None: [
            set_request_id,
            init_g_current_user,
        ]
    }


def teardown_module():
    app.before_request_funcs = original_before_request_funcs
    db.session().close_all()
    db.engine.dispose()
    db.drop_all()


def init_data():
    s = db.session()
    generate_static_sql_fixtures(s)
    library_org = SharedLibraryFactory(
        id=PARENT_ORG_ID,
        user_concurrent_borrowing_limit=_fake.pyint(),
        reborrowing_cooldown=relativedelta(days=0),
        borrowing_time_limit=relativedelta(days=7))
    child_org = SharedLibraryFactory(
        id=CHILD_ORG_ID,
        parent_organization=library_org)

    user = UserFactory(id=USER_ID)

    org_user = OrganizationUserFactory(user_id=USER_ID,
                                       organization_id=PARENT_ORG_ID,
                                       is_manager=True)
    # child_org.users.append(user)
    # s.add(child_org)
    # s.add(library_org)
    s.commit()

