from __future__ import absolute_import, unicode_literals

from httplib import OK, FORBIDDEN

import mock
from dateutil.relativedelta import relativedelta

from app import app, db
from app.users.organizations import OrganizationType
from app.eperpus.libraries import SharedLibraryItem
from app.users.tests.e2e.base import OrganizationE2EBase
from tests.fixtures.sqlalchemy import ItemFactory, AuthorFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from app.eperpus.tests.fixtures import SharedLibraryItemFactory, SharedLibraryBrandFactory, CatalogFactory, \
    CatalogItemFactory, CatalogBrandFactory


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    return MockResponse(mock_response_catalog_items_search(), 200)


def mock_response_catalog_items_search():
    items = []
    session = db.session()
    for org_item in session.query(SharedLibraryItem).all():
        items.append({'item_id': org_item.item_id})

    response = {
        "response": {
            "docs": items,
            "numFound": len(items),
        },
        "facet_counts": {
            "facet_queries": {},
            ""
            "facet_fields": {
                "item_categories": [
                    "Automotive", 2,
                    "Crime & Thrillers", 1
                ],
                "author_names": [
                    "ramdani", 2
                ],
                "item_languages": [
                    "id", 2,
                    "en", 0
                ]
            }
        },
        "spellcheck": {
            "suggestions": [
                "sedekha", {
                    "numFound": 2,
                    "startOffset": 0,
                    "endOffset": 7,
                    "origFreq": 0,
                    "suggestion": [
                        {
                            "word": "sedekah",
                            "freq": 8
                        },
                        {
                            "word": "selekta",
                            "freq": 1
                        }
                    ]
                }
            ],
            "correctlySpelled": 'false'
        }
    }

    session.close()
    return response


class SharedLibraryItemTests(OrganizationE2EBase):
    """Test for Shared Library Item Details API (organization-item details api)
    """
    force_organization_fixture_type = OrganizationType.shared_library

    def setUp(self):
        super(SharedLibraryItemTests, self).setUp()

        self.session.add(self.organization)
        self.organization.user_concurrent_borrowing_limit = 10
        self.organization.reborrowing_cooldown = relativedelta(days=0)
        self.organization.borrowing_time_limit = relativedelta(days=7)

        self.assign_user_to_organization(self.organization_member, self.child_organization)

        author = AuthorFactory()

        item1 = ItemFactory(id=1, authors=[author, ])
        item1.authors.append(author)
        self.item1 = item1

        item2 = ItemFactory(id=2)

        shared_lib_item1 = SharedLibraryItemFactory(
            shared_library=self.organization,
            item=item1,
            quantity=10,
            quantity_available=0,
            item_name_override='{} additional title'.format(item1.name)
        )
        shared_lib_item2 = SharedLibraryItemFactory(
            shared_library=self.organization,
            item=item2,
            quantity=20,
            quantity_available=20,
            item_name_override='{} additional title'.format(item2.name)
        )

        catalog = CatalogFactory(
            id=1,
            shared_library=self.organization,
        )

        catalog2 = CatalogFactory(
            id=2,
            shared_library=self.child_organization,
        )

        catalog_item1 = CatalogItemFactory(
            catalog=catalog,
            shared_library_item=shared_lib_item1,
        )
        catalog_item2 = CatalogItemFactory(
            catalog=catalog,
            shared_library_item=shared_lib_item2,
        )

        brand1 = BrandFactory(id=1001)
        brand2 = BrandFactory(id=1002)
        shared_lib_brand1 = SharedLibraryBrandFactory(shared_library=self.organization, brand=brand1, quantity=20)
        shared_lib_brand2 = SharedLibraryBrandFactory(shared_library=self.organization, brand=brand2, quantity=22)
        catalog_brand1 = CatalogBrandFactory(catalog=catalog, shared_library_brand=shared_lib_brand1)
        catalog_brand2 = CatalogBrandFactory(catalog=catalog, shared_library_brand=shared_lib_brand2)

        self.session.commit()

        self.session.add(self.organization)
        self.session.add(self.item1)

    def tearDown(self):
        self.session.expunge_all()
        super(SharedLibraryItemTests, self).tearDown()

    @mock.patch('app.users.helpers.requests.get', mock.Mock(side_effect=mocked_requests_get))
    def test_get_details(self):
        """ BASIC:  Members can retrieve details about items owned by  SharedLibrary.
        """
        self.set_api_authorized_user(self.organization_member)

        response = self.api_client.get(
            '/v1/organizations/{shared_lib.id}/items/{item.id}'.format(shared_lib=self.organization, item=self.item1),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, OK)

        self.session.add(self.item1)

        org_item = self.session.query(SharedLibraryItem).filter(
            SharedLibraryItem.organization_id == self.organization.id,
            SharedLibraryItem.item_id == self.item1.id
        ).first()

        self.assertUnorderedJsonEqual(response, {
            "id": self.item1.id,
            "title": org_item.item_name_override if org_item.item_name_override else self.item1.name,
            "currently_available": org_item.quantity_available,
            "total_in_collection": org_item.quantity,
            "details": {
                "id": self.item1.id,
                "href": "http://localhost/v1/items/{}".format(self.item1.id),
                "title": "Detil Item"
            },
            "cover_image": {
                "href": app.config['MEDIA_BASE_URL'] + self.item1.image_normal,
                "title": "Gambar Sampul"
            },
            "max_borrow_time": "P0Y0M7DT0H0M0S",
            "vendor": {
                "href": "http://localhost/v1/vendors/{}".format(self.item1.brand.vendor.id),
                "title": self.item1.brand.vendor.name
            }
        })

    def test_only_members_can_get_details(self):
        """ SECURITY: Non-members cannot get Shared Library item details.
        """
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/organizations/{shared_lib.id}/items/{item.id}'.format(shared_lib=self.organization, item=self.item1),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, FORBIDDEN)

        self.assertScoopErrorFormat(response)

    @mock.patch('app.users.helpers.requests.get', mock.Mock(side_effect=mocked_requests_get))
    def test_search(self):
        """ BASIC: Members can list all the Items owned by an Organization.
        """
        self.set_api_authorized_user(self.organization_member)

        response = self.api_client.get(
            '/v1/organizations/{shared_lib.id}/items'.format(shared_lib=self.organization),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, OK)

        org_items = (
            self.session
                .query(SharedLibraryItem)
                .filter(SharedLibraryItem.organization_id == self.organization.id)
                .all()
        )

        items = []
        for org_item in org_items:
            item = org_item.item
            items.append({
                "id": item.id,
                "title": org_item.item_name_override if org_item.item_name_override else item.name,
                "currently_available": org_item.quantity_available,
                "total_in_collection": org_item.quantity,
                "details": {
                    "id": item.id,
                    "href": "http://localhost/v1/items/{}".format(item.id),
                    "title": "Detil Item"
                },
                "cover_image": {
                    "href": app.config['MEDIA_BASE_URL'] + item.image_normal,
                    "title": "Gambar Sampul"
                },
                "max_borrow_time": "P0Y0M7DT0H0M0S",
                "vendor": {
                    "href": "http://localhost/v1/vendors/{}".format(item.brand.vendor.id),
                    "title": item.brand.vendor.name
                }
            })
        self.assertUnorderedJsonEqual(
            response,
            {
                "items": items,
                "metadata": {
                    "resultset": {
                        "count": 2,
                        "limit": 20,
                        "offset": 0
                    },
                    "facets": [
                        {
                            "field_name": "item_categories",
                            "values": [
                                {
                                    "value": "Automotive",
                                    "count": 2,
                                },
                                {
                                    "value": "Crime & Thrillers",
                                    "count": 1,
                                },
                            ]
                        },
                        {
                            "field_name": "item_authors",
                            "values": [
                                {
                                    "value": "ramdani",
                                    "count": 2,
                                }
                            ]
                        },
                        {
                            "field_name": "item_languages",
                            "values": [
                                {
                                    "value": "id",
                                    "count": 2,
                                },
                                {
                                    "value": "en",
                                    "count": 0,
                                }
                            ]
                        }
                    ],

                    "spelling_suggestions": [
                        {
                            "value": "sedekah",
                            "count": 8,
                        },
                        {
                            "value": "selekta",
                            "count": 1,
                        }
                    ]
                }
            })

    def test_only_members_can_search(self):
        """ SECURITY: Non-members cannot search the SharedLibrary's items.
        """
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/organizations/{shared_lib.id}/items'.format(shared_lib=self.organization),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, FORBIDDEN)

        self.assertScoopErrorFormat(response)
