from __future__ import absolute_import, unicode_literals

import json
from httplib import OK, UNAUTHORIZED, NO_CONTENT, NOT_FOUND, CREATED

from marshmallow.utils import isoformat

from app import app
from app.eperpus.catalogs import get_estimated_next_available_time
from app.eperpus.tests.fixtures import SharedLibraryFactory, CatalogFactory, SharedLibraryItemFactory, \
    CatalogItemFactory, WatchListFactory
from app.eperpus.watch_list import WatchList
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import ItemFactory


class WatchListTests(TestBase):

    request_url = "/v1/users/current-user/watch-list"
    model = WatchList
    fixture_factory = WatchListFactory

    list_key = "items"

    maxDiff = None
    longMessage = True

    resultset_max_size = 19
    resultset_min_size = 3
    max_response_size = 20

    @classmethod
    def setUpClass(cls):
        super(WatchListTests, cls).setUpClass()
        org = SharedLibraryFactory()
        cls.catalog = CatalogFactory(shared_library=org)
        cls.session.add(cls.regular_user)
        cls.session.add(cls.super_admin)
        cls.regular_user.organizations.append(org)
        cls.super_admin.organizations.append(org)
        cls.item1 = ItemFactory()
        cls.s_item1 = SharedLibraryItemFactory(
            shared_library=org, item=cls.item1, quantity=20, quantity_available=20
        )
        cls.s_item2 = SharedLibraryItemFactory(
            shared_library=org, quantity=20, quantity_available=20
        )
        cls.catalog_item1 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=cls.s_item1)
        catalog_item2 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=cls.s_item2)
        cls.session.commit()

        # data for testing unlink/remove item from watch list
        cls.watch_item2 = WatchListFactory(user=cls.super_admin, catalog=cls.catalog,
                                           shared_library_item=cls.s_item2)

        cls.session.commit()

    def set_up_list_fixtures(self):
        with self.on_session(self.regular_user, self.catalog, self.s_item1, self.s_item2):
            watch_item1 = WatchListFactory(user=self.regular_user, catalog=self.catalog,
                                           shared_library_item=self.s_item1)
            watch_item2 = WatchListFactory(user=self.regular_user, catalog=self.catalog,
                                           shared_library_item=self.s_item2)
            self.session.commit()
        self.session.add(watch_item1)
        self.session.add(watch_item2)
        return [watch_item1, watch_item2]

    def test_add_item_to_watch_list(self):
        with self.on_session(self.super_admin, self.catalog_item1):
            href = self.catalog_item1.api_url
            item_id = self.catalog_item1.shared_library_item_item_id
            catalog_id = self.catalog_item1.catalog_id

            self.set_api_authorized_user(self.super_admin)

            response = self.api_client.post(
                self.request_url,
                data=json.dumps({'href': href}),
                headers=self.build_headers(has_body=True))

            self.assert_status_code(response, CREATED)
            actual = json.loads(response.data)
            self.assertIsNotNone(actual.get("id", None))
            id = actual.get('id', None)
            watch_list1 = self.session.query(WatchList).get(id)
            self.assertEqual(watch_list1.api_url, actual.get('delete', None))

    def test_remove_item_from_watch_list(self):
        with self.on_session(self.watch_item2):
            watch_list_id = self.watch_item2.id

        self.set_api_authorized_user(self.super_admin)

        response = self.api_client.delete(
            '{url}/{id}'.format(url=self.request_url, id=watch_list_id),
            headers=self.build_headers(has_body=True))

        self.assert_status_code(response, NO_CONTENT)

        # test watch list id not found
        response = self.api_client.delete(
            '{url}/{id}'.format(url=self.request_url, id=12345),
            headers=self.build_headers(has_body=True))

        self.assert_status_code(response, NOT_FOUND)

    def test_unauthenticated_request(self):
        """ BASIC: Unauthenticated requests return the expected error message.
        """
        self.set_api_authorized_user(None)
        response = self.api_client.get(self.request_url, headers=self.build_headers())

        self.assertEqual(response.status_code, UNAUTHORIZED)

        self.assertScoopErrorFormat(
            response,
            user_message='Unauthorized, You cannot access this resource with the provided credentials.',
            developer_message='You cannot access this resource with the provided credentials.',
            status=401,
            error_code=401
        )

    def test_list_api(self):
        # setup some
        expected_entities = self.set_up_list_fixtures()

        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(self.request_url,
                                       headers=self.build_headers())

        self.assert_status_code(response, OK)
        response_json = self._get_response_json(response)
        self.assert_list_response(response_json, expected_entities)

    def assert_list_response(self, response_json, expected_entities):
        # for each response in list, compare with expected response
        self.assertIsNotNone(response_json.get(self.list_key),
                             '\nData with key [{}] not found in response. Response: {}'.format(
                                 self.list_key, response_json
                             ))
        # test each data in response body
        self.assert_list_main_data(entities=expected_entities, response_json=response_json,
                                   msg="\nFail on Test List API."
                                       "\nActual {actual}\n".format(actual=response_json))
        # test meta data
        self.assert_list_response_metadata(response_json, expected_count=len(expected_entities))

    def assert_list_main_data(self, entities, response_json, msg=None):
        for entity_json in response_json.get(self.list_key, []):
            entity_id = entity_json['id']
            # get entity with the same id from expected entities
            match_entities = filter(lambda e: e.id == entity_id, entities)
            if not match_entities:
                self.fail(msg='Cannot find entity with id {id} in Expected Data. '
                              'Expected {actual}\n{msg}'.format(
                                id=entity_id, actual=entities, msg=msg))

            expected = self.get_expected_list_response(match_entities[0])
            self.assert_response_body(entity_json, expected, match_entities[0], msg=msg)

        actual_count = len(response_json.get(self.list_key, []))
        self.assertEqual(len(entities), actual_count,
                         msg='\nExpected count {expected_count} != actual count {actual_count} '
                             'in list api\n{msg}'.format(
                             expected_count=len(expected), actual_count=actual_count, msg=msg))

    def get_expected_list_response(self, entity):
        self.session.add(entity)
        next_available = get_estimated_next_available_time(self.session, entity.catalog_item)
        next_available = isoformat(next_available) if next_available else None
        item = entity.item
        org_item = entity.shared_library_item
        authors = []
        for author in item.authors.all():
            authors.append({
                "id": author.id,
                "title": author.name,
                "href": "http://localhost/v1/authors/{}".format(author.id)
            })
        return {
            "id": entity.id,
            "title": org_item.item_name_override if org_item.item_name_override else item.name,
            "href": "http://localhost/v1/organizations/{org_id}/"
                    "shared-catalogs/{cat_id}/{item_id}".format(
                        org_id=org_item.organization_id,
                        cat_id=entity.catalog_id,
                        item_id=item.id
                    ),
            "catalog": {
                "title": entity.catalog.name,
                "href": entity.catalog.api_url,
            },
            "details": {
                "id": item.id,
                "href": "http://localhost/v1/items/{}".format(item.id),
                "title": "Detil Item"
                },
            "borrow_item": {
                "href": "http://localhost/v1/users/current-user/borrowed-items",
                "title": "Pinjam Item",
                },
            "cover_image": {
                "href": app.config['MEDIA_BASE_URL'] + item.image_normal,
                "title": "Gambar Sampul"
                },
            "currently_available": org_item.quantity_available,
            "total_in_collection": org_item.quantity,
            "max_borrow_time": "P0Y0M7DT0H0M0S",
            # "max_borrow_time": "P{0.years}Y{0.months}M{0.days}DT{0.hours}H{0.minutes}M{0.seconds}S".format(
            #     org_item.shared_library.borrowing_time_limit),
            "next_available": next_available,
            "vendor": {
                "href": "http://localhost/v1/vendors/{}".format(item.brand.vendor.id),
                "title": item.brand.vendor.name
                },
            # "authors": authors
        }

    def assert_response_body(self, response_json, expected, entity=None, msg=None):
        # override this method if needed
        self._assert_dict_contain_subset(expected, actual=response_json, msg=msg)
