from __future__ import absolute_import, unicode_literals

from httplib import FORBIDDEN

from dateutil.relativedelta import relativedelta

from app.eperpus.tests.base import EperpusE2EBase
from app.users.organizations import OrganizationType
from app.eperpus.libraries import SharedLibraryBrand
from tests.fixtures.sqlalchemy import AuthorFactory, ItemFactory
from app.eperpus.tests.fixtures import SharedLibraryItemFactory, SharedLibraryBrandFactory, CatalogFactory, \
    CatalogItemFactory, CatalogBrandFactory
from tests.fixtures.sqlalchemy.master import BrandFactory


class SharedLibraryBrandListApiTests(EperpusE2EBase):
    """Test for Shared Library Brands list API (organization-brands list api)
    """
    force_organization_fixture_type = OrganizationType.shared_library

    def setUp(self):
        super(SharedLibraryBrandListApiTests, self).setUp()

        self.session.add(self.organization)
        self.organization.user_concurrent_borrowing_limit = 10
        self.organization.reborrowing_cooldown = relativedelta(days=0)
        self.organization.borrowing_time_limit = relativedelta(days=7)

        self.assign_user_to_organization(self.organization_member, self.child_organization)

        author = AuthorFactory()

        item1 = ItemFactory(id=1, authors=[author, ])
        item1.authors.append(author)
        self.item1 = item1

        item2 = ItemFactory(id=2)

        shared_lib_item1 = SharedLibraryItemFactory(
            shared_library=self.organization,
            item=item1,
            quantity=10,
            quantity_available=0,
            item_name_override='{} additional title'.format(item1.name)
        )
        shared_lib_item2 = SharedLibraryItemFactory(
            shared_library=self.organization,
            item=item2,
            quantity=20,
            quantity_available=20,
            item_name_override='{} additional title'.format(item2.name)
        )

        catalog = CatalogFactory(
            id=1,
            shared_library=self.organization,
        )

        catalog2 = CatalogFactory(
            id=2,
            shared_library=self.child_organization,
        )

        catalog_item1 = CatalogItemFactory(
            catalog=catalog,
            shared_library_item=shared_lib_item1,
        )
        catalog_item2 = CatalogItemFactory(
            catalog=catalog,
            shared_library_item=shared_lib_item2,
        )

        brand1 = BrandFactory(id=1001)
        brand2 = BrandFactory(id=1002)
        shared_lib_brand1 = SharedLibraryBrandFactory(shared_library=self.organization, brand=brand1, quantity=20)
        shared_lib_brand2 = SharedLibraryBrandFactory(shared_library=self.organization, brand=brand2, quantity=22)
        catalog_brand1 = CatalogBrandFactory(catalog=catalog, shared_library_brand=shared_lib_brand1)
        catalog_brand2 = CatalogBrandFactory(catalog=catalog, shared_library_brand=shared_lib_brand2)

        self.session.commit()

        self.session.refresh(self.organization)
        self.session.refresh(self.item1)
        self.session.expunge_all()

    def test_search(self):
        """ BASIC: can fetch a list of 'brands' that are owned by a SharedLibrary
        """
        self.set_api_authorized_user(self.organization_member)

        response = self.api_client.get(
            '/v1/organizations/{}/brands'.format(self.organization.id),
            headers=self.build_headers()
        )

        org_brands = self.session.query(SharedLibraryBrand).filter(
            SharedLibraryBrand.organization_id == self.organization.id
        ).all()

        self.assertUnorderedJsonEqual(
            response,
            {
                "brands": [{
                    "id": org_brand.brand.id,
                    "title": org_brand.brand.name,
                    "total_in_collection": org_brand.quantity,
                    "details": {
                        "href": "http://localhost/v1/brands/{}".format(org_brand.brand.id),
                        "title": "Detil Brand"
                    },
                    "max_borrow_time": "P0Y0M7DT0H0M0S",
                    "vendor": {
                        "href": "http://localhost/v1/vendors/{}".format(org_brand.brand.vendor.id),
                        "title": org_brand.brand.vendor.name
                    }
                } for org_brand in org_brands],
                "metadata": {
                    "resultset": {
                        "count": len(org_brands),
                        "limit": 20,
                        "offset": 0
                    }
                }
            }
        )

    def test_only_members_can_search(self):
        """ SECURITY: Searching is restricted to organization members.
        """
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/organizations/{}/brands'.format(self.organization.id),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, FORBIDDEN)

        self.assertScoopErrorFormat(response)
