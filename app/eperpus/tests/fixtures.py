from datetime import timedelta
from factory import alchemy, Faker, SubFactory

from app import db
from app.eperpus.catalogs import Catalog, CatalogItem, CatalogBrand
from app.eperpus.libraries import SharedLibrary, SharedLibraryItem, SharedLibraryBrand
from app.eperpus.members import UserBorrowedItem
from app.eperpus.watch_list import WatchList
from app.users.organizations import OrganizationStatus, OrganizationType
from tests.fixtures.sqlalchemy import ItemFactory
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.master import BrandFactory


class SharedLibraryFactory(alchemy.SQLAlchemyModelFactory):
    name = Faker('bs')
    status = OrganizationStatus.clear.value
    slug = Faker('slug')
    type = OrganizationType.shared_library.value
    user_concurrent_borrowing_limit = 5
    borrowing_time_limit = timedelta(days=7)
    public_library = False
    logo_url = Faker('url')

    class Meta:
        sqlalchemy_session = db.session
        model = SharedLibrary


class SharedLibraryItemFactory(alchemy.SQLAlchemyModelFactory):
    shared_library = SubFactory(SharedLibraryFactory)
    item = SubFactory(ItemFactory)
    quantity = Faker('random_number')
    quantity_available = Faker('random_number')
    is_active = True

    class Meta:
        sqlalchemy_session = db.session
        model = SharedLibraryItem


class SharedLibraryBrandFactory(alchemy.SQLAlchemyModelFactory):
    shared_library = SubFactory(SharedLibraryFactory)
    brand = SubFactory(BrandFactory)
    quantity = Faker('random_number')

    class Meta:
        sqlalchemy_session = db.session
        model = SharedLibraryBrand


class CatalogFactory(alchemy.SQLAlchemyModelFactory):
    name = Faker('bs')
    shared_library = SubFactory(SharedLibraryFactory)

    class Meta:
        sqlalchemy_session = db.session
        model = Catalog


class CatalogItemFactory(alchemy.SQLAlchemyModelFactory):
    catalog = SubFactory(CatalogFactory)
    shared_library_item = SubFactory(SharedLibraryItemFactory)
    is_active = True

    class Meta:
        sqlalchemy_session = db.session
        model = CatalogItem


class CatalogBrandFactory(alchemy.SQLAlchemyModelFactory):
    catalog = SubFactory(CatalogFactory)
    shared_library_brand = SubFactory(SharedLibraryBrandFactory)

    class Meta:
        sqlalchemy_session = db.session
        model = CatalogBrand


class UserBorrowedItemFactory(alchemy.SQLAlchemyModelFactory):

    user = SubFactory(UserFactory)
    catalog_item = SubFactory(CatalogItemFactory)

    class Meta:
        sqlalchemy_session = db.session
        model = UserBorrowedItem


class WatchListFactory(alchemy.SQLAlchemyModelFactory):
    user = SubFactory(UserFactory)
    catalog = SubFactory(CatalogFactory)
    shared_library_item = SubFactory(SharedLibraryItemFactory)

    class Meta:
        sqlalchemy_session = db.session
        model = WatchList
