from unittest import TestCase

from flask import Flask
from app.eperpus.solr import SolrSuggester, BrandNameSuggester


fake_app = Flask(__name__)


class SolrSuggesterTests(TestCase):

    def test_suggest(self):
        with fake_app.test_request_context('/suggest?q=jakarta') as ctx:
            self.assertDictEqual(
                SolrSuggester(ctx.request).build(),
                {'q': 'jakarta'}
            )

    def test_suggest_without_query(self):
        with fake_app.test_request_context('/suggest') as ctx:
            self.assertDictEqual(
                SolrSuggester(ctx.request).build(),
                {'q': '*'}
            )


class BrandNameSuggesterTests(TestCase):

    def test_suggest_catalog_ids(self):

        with fake_app.test_request_context('/suggest?q=jakarta') as ctx:

            catalog_id = 1
            self.assertDictEqual(
                BrandNameSuggester(flask_req=ctx.request, catalog_id=catalog_id).build(),
                {'q': 'jakarta', 'suggest.cfq': 1}
            )


