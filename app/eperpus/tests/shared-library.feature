Feature: Shared Library

    @e2e
    Scenario: Manager temporally locks Shared Library
        Given a shared library with a child group
            And the child is currently "unlocked"
        When a "manager" tries to lock the "child_group" with the message "Because I said so"
        Then HTTP response status is "OK"
            And the "child_group" locked_message in the database is "Because I said so"

    @e2e
    Scenario: Manager unlocks a Shared Library
        Given a shared library with a child group
            And the child is currently "locked"
        When a "manager" sends an unlock request
        Then HTTP response status is "OK"
            And the "child_group" locked_message in the database is NULL

    @security @e2e
    Scenario: Non-managers cannot lock a Shared Library
        Given a shared library with a child group
        When a "member" tries to lock the "child_group" with the message "Because I said so"
        Then HTTP response status is "FORBIDDEN"
            And scoop standard error format is returned
            And the "child_group" locked_message in the database is NULL


    @security @e2e
    Scenario: Non-manager cannot unlock a Shared Library
        Given a shared library with a child group
            And the child is currently "locked"
        When a "member" sends an unlock request
        Then HTTP response status is "FORBIDDEN"
            And scoop standard error format is returned
            And the "child_group" locked_message in the database is "Because I said so"

    @business_rule @e2e
    Scenario: Parent Organizations can't be locked
        Given a shared library with a child group
        When a "manager" tries to lock the "parent_group" with the message "Because I said so"
        Then HTTP response status is "METHOD_NOT_ALLOWED"
            And scoop standard error format is returned
            And the "parent_group" locked_message in the database is NULL
