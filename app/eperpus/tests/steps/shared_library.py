from __future__ import absolute_import, unicode_literals

import json

from behave import given, when, then

from app.constants import RoleType
from app.utils.behave import create_user, assign_user_to_organization, set_api_user, build_headers
from app.eperpus.tests.fixtures import SharedLibraryFactory


@given('a shared library with a child group')
def _(context):
    parent = SharedLibraryFactory()
    child = SharedLibraryFactory(parent_organization=parent)
    context.parent_group = parent
    context.child_group = child
    context.session.commit()

    manager = create_user(context, RoleType.verified_user)
    context.manager = manager
    assign_user_to_organization(context, manager, child, is_manager=True)

    member = create_user(context, RoleType.verified_user)
    context.member = member
    assign_user_to_organization(context, member, child)

    context.session.commit()


@given('the child is currently "{locked_type}"')
def _(context, locked_type):
    if locked_type == "locked":
        context.session.add(context.child_group)
        context.child_group.locked_message = "Because I said so"
        context.session.commit()


@when('a "{user_type}" tries to lock the "{group_type}" with the message "{message_text}"')
def _(context, user_type, group_type, message_text):

    set_api_user(context, getattr(context, user_type))

    group = getattr(context, group_type)

    context.response = context.api_client.open(
        '/v1/organizations/{org.id}'.format(org=group),
        data=json.dumps({'locked_message': message_text}),
        method='LOCK',
        headers=build_headers(context, has_body=True)
    )


@when('a "{user_type}" sends an unlock request')
def _(context, user_type):

    set_api_user(context, getattr(context, user_type))

    context.response = context.api_client.open(
        '/v1/organizations/{org.id}'.format(org=context.child_group),
        method='UNLOCK',
        headers=build_headers(context)
    )


@then('the "{group_type}" locked_message in the database is "{message_text}"')
def _(context, group_type, message_text):

    group = getattr(context, group_type)

    context.session.add(group)
    context.session.refresh(group)

    assert group.locked_message == message_text


@then('the "{group_type}" locked_message in the database is NULL')
def _(context, group_type):

    group = getattr(context, group_type)

    context.session.add(group)
    context.session.refresh(group)

    assert group.locked_message is None
