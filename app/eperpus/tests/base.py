from __future__ import absolute_import, unicode_literals

from app.users.organizations import OrganizationType
from app.users.tests.e2e.base import OrganizationE2EBase


class EperpusE2EBase(OrganizationE2EBase):

    # set this to use a specific organization type for all tests
    force_organization_fixture_type = OrganizationType.shared_library

    def setUp(self):
        super(EperpusE2EBase, self).setUp()
