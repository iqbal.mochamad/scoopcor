"""
Eperpus Members
===============

Data models and URL endpoints that are related to a member of an eperpus.
"""
from __future__ import unicode_literals, absolute_import

import csv
import httplib
import json
import os
from datetime import datetime, timedelta
from httplib import CREATED, NO_CONTENT, OK

from dateutil.relativedelta import relativedelta
from flask import Blueprint, g, current_app, request, jsonify, url_for, Response
from flask_appsfoundry import parsers, viewmodels, marshal
from flask_appsfoundry.exceptions import NotFound, Conflict, BadRequest, InternalServerError, Forbidden, Unauthorized
from flask_appsfoundry.serializers import SerializerBase, fields
from sqlalchemy import desc

from app import db, app
from app.auth.decorators import user_is_authenticated
from app.eperpus.models import WatchList
from app.items.choices import ITEM_TYPES, MAGAZINE, BOOK, NEWSPAPER
from app.items.models import Review
from app.items.previews import PreviewUrlBuilder, preview_s3_covers
from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files
from app.users.organizations import Organization, is_organization_manager, get_org_and_child_org_ids, \
    is_organization_manager_by_id, OrganizationCatalog, OrganizationUser
from app.users.users import User, UserConverter
from app.utils.datetimes import get_local, get_local_nieve
from app.utils.responses import get_list_response
from app.utils.serializers import SimpleImplicitHrefWithId, SimpleImplicitHref, SimpleImplicitHrefWithRel, \
    SimpleImplicitHrefWithRelId
from app.utils.viewmodels import HrefAttributeWithId, HrefAttribute, HrefAttributeWithRel, HrefAttributeWithRelId
from .catalogs import Catalog, assert_catalog_item, assert_qty_available, CatalogItem
from .helpers import get_stock_holding_org, update_qty_available, organization_list_parent
from .libraries import SharedLibraryConverter, SharedLibrary, SharedLibraryBorrowingItem, SharedLibraryItem
from .solr import update_stock_solr

blueprint = Blueprint('shared_library_users', __name__, url_prefix='/v1')

app.url_map.converters['shared_library'] = SharedLibraryConverter


def get_current_client_org_ids(session):
    # get list of organization id from g.current_client.organizations, included it's child organizations
    if getattr(g, 'current_client', None):
        session.add(g.current_client)
        return get_all_child_orgs(g.current_client.organizations)
    else:
        return []


def get_all_child_orgs(organizations):
    list_client_org_ids = []
    for org in organizations:
        list_client_org_ids.append(org.id)
        for org_child in org.child_organizations:
            list_client_org_ids.append(org_child.id)
            for org_child2 in org_child.child_organizations:
                list_client_org_ids.append(org_child2.id)
    return list(set(list_client_org_ids))


def get_allowed_orgs(session, user=None, custom_org=None):
    # compare current user - organizations vs current client - organizations and return list of allowed orgs
    user = user or g.current_user
    session.add(user)

    if custom_org:
        eperpus_orgs = [custom_org]
    else:
        eperpus_orgs = [org.id for org in user.organizations.filter(Organization.type == 'shared library').all()]

    current_client_orgs = get_current_client_org_ids(session)
    client_id = g.current_client.id if getattr(g, 'current_client', None) else None
    if current_client_orgs:  # and client_id not in PUBLIC_LIBRARY_CLIENT_ID:
        # if a client has limit list of organizations, crosscheck it with current user - organization
        allowed_orgs = list(set(current_client_orgs).intersection(eperpus_orgs))
    else:
        # client don't have organization limit.. return all user orgs
        allowed_orgs = eperpus_orgs

    if allowed_orgs:
        return allowed_orgs
    else:
        raise Unauthorized(developer_message="User didn't have access to any organization {}".format(
            current_client_orgs))


@blueprint.route('/users/current-user/subscribed-catalogs')
@user_is_authenticated
def user_subscribed_catalogs():
    """ Search all available catalog for current user based on its organizations
    """
    session = db.session()
    query_set = session.query(Catalog)

    allowed_orgs = get_allowed_orgs(session, user=g.current_user)

    exist_catalog = OrganizationCatalog.query.filter(OrganizationCatalog.organization_id.in_(allowed_orgs)).all()
    catalog_ids = list(set([cat.catalog_id for cat in exist_catalog]))
    query_set = query_set.filter(Catalog.id.in_(catalog_ids)).order_by(Catalog.name)

    # DO NOT REMOVE THIS LINE!, AVOID LOST LOGIC!
    # catalog already move to cas_organization_catalogs;
    # query_set = query_set.filter(Catalog.organization_id.in_(allowed_orgs)).order_by(Catalog.name)

    return get_list_response(
        query_set=query_set,
        serializer_name='catalogs',
        serializer=UserSubscribedCatalogSerializer(),
        element_viewmodel=viewmodels.SaViewmodelBase,
        filtering=[]
    )


@blueprint.route('/users/<user:user>/borrowed-items')
@user_is_authenticated
def search_user_current_borrowed_items(user):
    """ get list of items that current user borrowed.

    Please note:  This doesn't descriminate, by default, on the user's organizations.  It might
        be a good idea to enforce this as a manditory query-string parameter.  Edge case would be
        if the user and the manager belong to multiple, separate eperpus's.
    """
    args = request.args.to_dict()
    custom_org = args.get('organization_id', None)

    session = db.session()
    if user == 'current-user':
        user = g.current_user
    else:
        if user.id != g.current_user.id:
            # only needed if a user is trying to return an item for a different user
            # little bit of a hack, but no father (tapi, nga papa)
            # warning: edge case .. this isn't a really good check.
            is_organization_manager(lambda *args, **kwargs: True)(
                user.organizations.filter_by(type='shared library').first()
            )

    query_set = session.query(UserBorrowedItem).filter(
        UserBorrowedItem.user_id == user.id,
        UserBorrowedItem.returned_time == None
    )
    allowed_orgs = get_allowed_orgs(session, user=user, custom_org=custom_org)
    if allowed_orgs:
        query_set = query_set.filter(
            UserBorrowedItem.catalog_item_org_id.in_(allowed_orgs)
        )

    return get_list_response(
        query_set=query_set,
        serializer_name='items',
        serializer=UserBorrowedItemListSerializer(),
        element_viewmodel=UserBorrowedItemViewModel,
        filtering=[]
    )


@blueprint.route('/users/current-user/borrowed-items', methods=['POST', ])
@user_is_authenticated
def update_user_current_borrowed_items():
    """ User borrow an item """

    # parse data from request body
    req_data = json.loads(request.data)
    # href in request body example:
    # https://api.apps-foundry.com/organizations/1/shared-catalogs/12/9998
    # 9998 = item id
    # 12 = catalog id
    href = req_data.get("href", None) if req_data else None
    data = href.split('/') if href else None
    catalog_id = None
    item_id = None
    if data and data[len(data) - 3] == 'shared-catalogs':
        item_id = data[len(data) - 1]
        catalog_id = data[len(data) - 2]

    if not item_id or not catalog_id:
        raise BadRequest()

    session = db.session()

    catalog = session.query(Catalog).get(catalog_id)

    stock_org = get_stock_holding_org(catalog.shared_library)

    current_user_id = g.current_user.id

    assert_user_vs_organization(
        session=session, org_id=catalog.organization_id, user_id=current_user_id)

    assert_user_has_not_reached_maximum_allowed(
        session=session,
        stock_org=stock_org,
        user_id=current_user_id,
    )

    assert_borrowed_cooldown(session, stock_org, current_user_id, item_id, catalog.id)

    assert_catalog_item(session, catalog, item_id, stock_org)

    assert_user_already_borrowed_the_item(
        session=session,
        stock_org=stock_org, current_user_id=current_user_id,
        item_id=item_id, catalog_id=catalog_id
    )

    assert_qty_available(session, stock_org.id, item_id)

    req_data = {
        'user_id': current_user_id,
        'catalog_item_catalog_id': catalog.id,
        'catalog_item_org_id': stock_org.id,
        'catalog_item_item_id': item_id,
        'borrowing_start_time': get_local(),
        'returned_time': None
    }

    model = UserBorrowedItem(**{k: req_data[k] for k in req_data if k != 'href'})

    try:
        # add user borrow item transaction
        session.add(model)

        # reduce available quantity in org-item
        update_qty_available(
            session=session,
            current_org=stock_org,
            item_id=item_id,
            qty=-1
        )
        session.commit()

        # update watch list, already borrowed
        db.engine.execute(
            'UPDATE watch_list SET end_date = now(), is_borrowed = true '
            'WHERE user_id={user_id} AND stock_org_id={stock_org_id} AND item_id={item_id}'
            ' AND end_date is null'.format(
                user_id=current_user_id,
                stock_org_id=stock_org.id,
                item_id=item_id))

        update_stock_solr(item_id=item_id, catalog_id=catalog.id, qty=-1)
    except Exception as e:
        session.rollback()
        raise InternalServerError(user_message='Failed to borrow the item, please try again.',
                                  developer_message=e.message)

    response = jsonify(marshal(UserBorrowedItemViewModel(model), UserBorrowedItemSerializer()))
    response.status_code = CREATED
    return response


@blueprint.route('/users/current-user/borrowed-items/<int:db_id>', methods=['PUT', ])
@user_is_authenticated
def extend_borrowed_item_lease(db_id):
    """ Extend borrowing time

    :param db_id: primary key of `app.users.models.UserBorrowedItem`
    """
    session = db.session()
    model = session.query(UserBorrowedItem).get(db_id)
    if not model:
        raise NotFound

    stock_org = get_stock_holding_org(model.catalog_item.shared_library_item.shared_library)

    assert_cooldown_for_reborrowing_item(shared_library_org=stock_org)

    # extend borrowing time via updating borrowing_start_time
    # DJC: This concerns me a little bit, because we may want to know the real original
    # start time for portal reporting.
    model.borrowing_start_time = get_local()
    session.commit()
    return jsonify(marshal(UserBorrowedItemViewModel(model), UserBorrowedItemSerializer()))


@blueprint.route('/users/current-user/borrowed-items/<int:id>')
@user_is_authenticated
def borrowed_item_details(id):
    """ Get details info of an item that user borrowed

    :param id: primary key of `app.users.models.UserBorrowedItem`
    :return:
    """
    model = UserBorrowedItem.query.get(id)

    return jsonify(marshal(UserBorrowedItemDetailsViewModel(model), UserBorrowedItemDetailsSerializer()))


@blueprint.route('/users/<user_id>/borrowed-items/<int:id>', methods=['DELETE', ])
@user_is_authenticated
def return_borrowed_item(user_id, id):
    """ Returns an Item that the user is currently borrowing to stock.

    :param `int` db_id: primary key of `app.users.models.UserBorrowedItem`
    :return:
    """
    session = db.session()

    user_borrowed_item = session.query(UserBorrowedItem).get(id)
    if not user_borrowed_item:
        return NotFound

    if user_id == 'current-user':
        user = getattr(g, 'current_user', None)
    else:
        user = session.query(User).get(user_id)
        authed_user = getattr(g, 'current_user', None)
        if not user:
            raise NotFound

        if user.id != authed_user.id:
            # only needed if a user is trying to return an item for a different user
            # little bit of a hack, but no father (tapi, nga papa)
            is_organization_manager(lambda *args, **kwargs: True)(
                user_borrowed_item.catalog_item.catalog.shared_library
            )

    try:
        if not user_borrowed_item.returned_time:
            # flag as returned
            user_borrowed_item.returned_time = get_local()
            # increase available qty
            update_qty_available(
                session=session,
                current_org=user_borrowed_item.catalog_item.shared_library_item.shared_library,
                item_id=user_borrowed_item.catalog_item.shared_library_item.item.id,
                qty=1
            )
            session.commit()
    except Exception as e:
        session.rollback()
        raise Conflict(
            user_message='Gagal mengembalikan item, pastikan anda hanya mengklik sekali saja, '
                         'silahkan refresh dan cek lagi data peminjaman anda.',
            developer_message=e.message
        )

    try:
        update_stock_solr(item_id=user_borrowed_item.catalog_item_item_id,
                          catalog_id=user_borrowed_item.catalog_item_catalog_id, qty=1)
    except:

        pass

    return jsonify({}), NO_CONTENT


app.url_map.converters['user'] = UserConverter


@blueprint.route('/users/<user:user>/borrowed-items-history')
@user_is_authenticated
def get_user_borrowing_history(user):
    """ get borrowing history of a user/current user
    valid end point for this api:
        /v1/users/current-user/borrowed-items-history
        /v1/users/{user_id}/borrowed-items-history

    :param `app.users.users.User` user:
    :return:
    """
    session = db.session()
    if user == 'current-user':
        user = g.current_user
        session.add(user)
    elif user and user.id != g.current_user.id:
        raise Unauthorized()

    allowed_orgs = get_allowed_orgs(session, user=user)
    return user_borrowing_history(session, user, allowed_orgs)


@blueprint.route('/organizations/<int:organization_id>/<user:user>/borrowed-items-history')
@user_is_authenticated
@is_organization_manager_by_id
def get_user_organization_borrowing_history(organization_id, user):
    """ get borrowing history of a user/current user
    valid end point for this api:
        /v1/users/current-user/borrowed-items-history
        /v1/users/{user_id}/borrowed-items-history

    :param `app.users.users.User` user:
    :return:
    """
    session = db.session()
    if organization_id not in [org.id for org in user.organizations]:
        raise Unauthorized()

    # allowed_orgs = get_allowed_orgs(session, user=user)
    return user_borrowing_history(session, user, allowed_orgs=[organization_id, ])


def user_borrowing_history(session, user, allowed_orgs):
    query_set = session.query(UserBorrowedItem).filter(UserBorrowedItem.user_id == user.id)
    if allowed_orgs:
        query_set = query_set.filter(
            UserBorrowedItem.catalog_item_org_id.in_(allowed_orgs)
        )
    query_set = query_set.order_by(desc(UserBorrowedItem.id))
    return get_list_response(
        query_set=query_set,
        serializer_name='items',
        serializer=UserBorrowedItemListSerializer(),
        element_viewmodel=UserBorrowedItemViewModel,
        filtering=[]
    )


@blueprint.route('/organizations/<shared_library:entity>/borrowed-items-history')
@is_organization_manager
def search_user_borrowing_history_by_organization(entity):
    """ get: get list history of borrowed items in an organizations
    can filtered by item id and or user id also
    """
    user_id = request.args.get('user_id', None)
    item_id = request.args.get('item_id', None)
    filter_org_ids = get_org_and_child_org_ids(entity)

    filters = []
    # its for indexing.. first by user id, then org id then by item id
    if user_id:
        filters.append(UserBorrowedItem.user_id == user_id)
    filters.append(UserBorrowedItem.catalog_item_org_id.in_(filter_org_ids))
    if item_id:
        filters.append(UserBorrowedItem.catalog_item_item_id == item_id)
    query_set = UserBorrowedItem.query.filter(*filters).order_by(desc(UserBorrowedItem.id))
    return get_list_response(
        query_set=query_set,
        serializer_name='items',
        serializer=UserBorrowedItemListSerializer(),
        element_viewmodel=UserBorrowedItemViewModel,
        filtering=[]
    )


class UserBorrowedItem(db.Model):
    """ A single `Item` borrowed by a single `User` from a `SharedLibrary`'s `Catalog`
    """
    __tablename__ = 'core_user_borrowed_items'

    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    user = db.relationship('User', backref=db.backref('borrowed_items', lazy='dynamic'))

    catalog_item_catalog_id = db.Column(db.Integer, nullable=False)
    catalog_item_org_id = db.Column(db.Integer, nullable=False)

    # catalog_item_item_id = db.Column(db.Integer, nullable=False)
    catalog_item_item_id = db.Column(db.Integer, db.ForeignKey('core_items.id'), nullable=False)
    catalog_item_item = db.relationship('Item')

    catalog_item = db.relationship(CatalogItem, backref=db.backref('borrowed_items', lazy='dynamic'))

    borrowing_start_time = db.Column(db.DateTime(timezone=True), nullable=False, default=get_local)
    returned_time = db.Column(db.DateTime(timezone=True))

    modified = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    __table_args__ = (
        db.ForeignKeyConstraint(
            ['catalog_item_catalog_id', 'catalog_item_org_id', 'catalog_item_item_id'],
            ['core_catalog_items.catalog_id',
             'core_catalog_items.shared_library_item_org_id',
             'core_catalog_items.shared_library_item_item_id'],
            name='core_user_borrowed_items_catalog_items_organization_item_fkey'),
    )

    __mapper_args__ = {
        "version_id_col": modified,
        "version_id_generator": lambda version: get_local_nieve(),
    }

    @property
    def api_url(self):
        return url_for('shared_library_users.borrowed_item_details', id=self.id, _external=True)


class UserBorrowedItemListSerializer(SerializerBase):
    id = fields.Integer
    title = fields.String
    href = fields.String
    details = fields.Nested(SimpleImplicitHrefWithId())
    catalog_item = fields.Nested(SimpleImplicitHref())
    catalog = fields.Nested(SimpleImplicitHrefWithRelId(), attribute='catalog_response')
    cover_image = fields.Nested(SimpleImplicitHref())
    expires = fields.DateTime('iso8601', attribute='expires')
    vendor = fields.Nested(SimpleImplicitHref())
    borrowing_start_time = fields.DateTime('iso8601')
    returned_time = fields.DateTime('iso8601')
    authors = fields.List(fields.Nested(SimpleImplicitHrefWithId()))
    download = fields.Nested(SimpleImplicitHref())
    currently_available = fields.Integer
    page_count = fields.Integer
    is_webreader = fields.Boolean
    categories = fields.List(fields.Nested(SimpleImplicitHrefWithId()))
    username = fields.String(attribute='user.username')
    first_name = fields.String(attribute='user.first_name')
    last_name = fields.String(attribute='user.last_name')
    review_status = fields.Boolean
    file_size = fields.String
    new_subtitle = fields.String
    new_title = fields.String


class UserBorrowedItemSerializer(SerializerBase):
    id = fields.Integer
    href = fields.String
    title = fields.String
    vendor = fields.Nested(SimpleImplicitHref())
    details = fields.Nested(SimpleImplicitHrefWithId())
    catalog_item = fields.Nested(SimpleImplicitHref())
    catalog = fields.Nested(SimpleImplicitHrefWithRelId(), attribute='catalog_response')
    cover_image = fields.Nested(SimpleImplicitHref())
    expires = fields.DateTime('iso8601', attribute='expires')
    owner = fields.Nested(SimpleImplicitHrefWithRel(), attribute='owner')
    download = fields.Nested(SimpleImplicitHref())
    streaming = fields.Nested(SimpleImplicitHref())
    last_read_time = fields.String(attribute='last_read_time')
    last_read_page = fields.String(attribute='last_read_page')
    borrowing_start_time = fields.DateTime('iso8601')
    currently_available = fields.Integer
    username = fields.String(attribute='user.username')
    first_name = fields.String(attribute='user.first_name')
    last_name = fields.String(attribute='user.last_name')
    file_size = fields.String
    new_subtitle = fields.String
    new_title = fields.String


class UserBorrowedItemDetailsSerializer(UserBorrowedItemSerializer):
    # next_available = fields.DateTime('iso8601')
    brand = fields.Nested(SimpleImplicitHrefWithId())
    description = fields.String
    watch_list = fields.Raw
    high_res_image = fields.Nested(SimpleImplicitHref())
    previews = fields.Raw


class UserSubscribedCatalogSerializer(SerializerBase):
    id = fields.Integer
    title = fields.String(attribute='name')
    href = fields.String(attribute='api_url')
    is_internal_catalog = fields.Boolean


class EperpusUserParser(parsers.ParserBase):
    username = parsers.StringField(required=True)
    email = parsers.StringField(required=True)
    first_name = parsers.StringField(required=True)
    last_name = parsers.StringField()
    birthdate = parsers.DateTimeField()
    gender = parsers.StringField()


class UserBorrowedItemViewModel(viewmodels.SaViewmodelBase):

    def __init__(self, model_instance):
        super(UserBorrowedItemViewModel, self).__init__(model_instance)
        self.currently_available = self._model_instance.catalog_item.shared_library_item.quantity_available
        self.item = self._model_instance.catalog_item.shared_library_item.item
        self.catalog = self._model_instance.catalog_item.catalog
        self.shared_library_org = self._model_instance.catalog_item.shared_library_item.shared_library
        self.new_subtitle = self.item.issue_number if self.item.issue_number else None
        self.new_title = self.item.brand.name

    @property
    def title(self):
        if self._model_instance.catalog_item.shared_library_item.item_name_override:
            return self._model_instance.catalog_item.shared_library_item.item_name_override
        else:
            return self.item.name

    @property
    def catalog_item(self):
        return HrefAttribute(
            title='Detil Item di Katalog',
            href=self._model_instance.catalog_item.api_url,
        )

    @property
    def details(self):
        return HrefAttributeWithId(
            title='Detil Item',
            href=self.item.api_url,
            id=self.item.id
        )

    @property
    def cover_image(self):
        return HrefAttribute(
            title='Gambar Sampul',
            href=preview_s3_covers(self.item, 'image_normal', True),
        )

    @property
    def expires(self):
        item_conf = SharedLibraryBorrowingItem.query.filter_by(
            organization_id=self._model_instance.catalog_item_org_id,
            catalog_id=self._model_instance.catalog_item_catalog_id,
            item_id=self._model_instance.catalog_item_item_id
        ).first()

        if item_conf:
            # first checking if this item already registered to.. SharedLibItem
            return self._model_instance.borrowing_start_time + item_conf.borrowing_time_limit
        else:
            # used general settings for content types
            if self._model_instance.catalog_item_item.item_type == ITEM_TYPES[MAGAZINE]:
                return self._model_instance.borrowing_start_time + self.shared_library_org.magazine_borrowing_time_limit

            if self._model_instance.catalog_item_item.item_type == ITEM_TYPES[BOOK]:
                return self._model_instance.borrowing_start_time + self.shared_library_org.book_borrowing_time_limit

            if self._model_instance.catalog_item_item.item_type == ITEM_TYPES[NEWSPAPER]:
                return self._model_instance.borrowing_start_time + self.shared_library_org.newspaper_borrowing_time_limit

    @property
    def owner(self):
        return HrefAttributeWithRel(
            title=self.shared_library_org.name,
            href=self.shared_library_org.api_url,
            rel=self.shared_library_org.type,
        )

    @property
    def catalog_response(self):
        return HrefAttributeWithRelId(
            title=self.catalog.name,
            href=self.catalog.api_url,
            rel="catalog",
            id=self.catalog.id,
        )

    @property
    def href(self):
        return self._model_instance.api_url

    @property
    def download(self):
        return HrefAttribute(
            title='Download buat Baca Offline',
            href=self.item.download_url,
        )

    @property
    def streaming(self):
        return HrefAttribute(
            title='Baca Sekarang',
            href=self.item.web_reader_url,
        )

    @property
    def last_read_time(self):
        return None

    @property
    def last_read_page(self):
        return None

    @property
    def vendor(self):
        return HrefAttribute(
            title=self.item.brand.vendor.name,
            href=self.item.brand.vendor.api_url
        )

    @property
    def authors(self):
        authors = []
        for author in self.item.authors.all():
            authors.append(HrefAttributeWithId(
                id=author.id,
                title=author.name,
                href=author.api_url
            ))
        return authors

    @property
    def page_count(self):
        return self.item.page_count if self.item.page_count else 0

    @property
    def categories(self):
        from app.eperpus.categories import CategoryEperpus, CategoryEperpusItem
        categories_eperpus = CategoryEperpusItem.query.filter_by(
            organization_id=self._model_instance.catalog_item_org_id,
            item_id=self._model_instance.catalog_item_item_id
        ).all()

        data = []
        for idata in categories_eperpus:
            cat_eperpus = CategoryEperpus.query.filter_by(
                organization_id=self._model_instance.catalog_item_org_id,
                category_id=idata.category_id
            ).first()

            if cat_eperpus:
                data.append({
                    'id': cat_eperpus.category_id,
                    'title': cat_eperpus.name
                })
        return data

    @property
    def is_webreader(self):
        return self.item.eperpus_web_reader_files_ready()

    @property
    def review_status(self):
        return Review.query.filter_by(user_id=self._model_instance.user.id, item_id=self.item.id).count() > 0

    @property
    def file_size(self):
        from humanize import naturalsize
        return naturalsize(self.item.file_size) if self.item.file_size and self.item.file_size > 0 else '- MB'


class UserBorrowedItemDetailsViewModel(UserBorrowedItemViewModel):

    def __init__(self, model_instance):
        super(UserBorrowedItemDetailsViewModel, self).__init__(model_instance)
        # self.next_available = get_estimated_next_available_time(db.session(), self._model_instance)
        self.brand = HrefAttributeWithId(id=self.item.brand.id, title=self.item.brand.name,
                                         href=self.item.brand.api_url)
        self.description = self.item.description
        watch_list = db.session.query(WatchList).filter(
            WatchList.user_id == g.current_user.id,
            WatchList.item_id == self.item.id,
            WatchList.catalog_id == self.catalog.id,
            WatchList.end_date == None,
            WatchList.is_borrowed == False
        ).first()
        self.watch_list = {
            "id": watch_list.id,
            "title": "watch list",
            "href": watch_list.api_url} if watch_list else None
        self.high_res_image = HrefAttribute(
            title='Gambar Sampul High Res',
            href=preview_s3_covers(self.item, 'image_highres', True)
        )
        self.previews = PreviewUrlBuilder(current_app.config['BASE_URL']).make_urls(self.item, True)
        self.new_subtitle = self.item.issue_number if self.item.issue_number else None
        self.new_title = self.item.brand.name


def assert_user_already_borrowed_the_item(session, stock_org, current_user_id, item_id, catalog_id):
    user_borrowed_item = session.query(UserBorrowedItem).filter(
        UserBorrowedItem.user_id == current_user_id,
        UserBorrowedItem.catalog_item_catalog_id == catalog_id,
        UserBorrowedItem.catalog_item_org_id == stock_org.id,
        UserBorrowedItem.catalog_item_item_id == item_id,
        UserBorrowedItem.returned_time == None
    ).order_by(UserBorrowedItem.id.desc()).first()

    if user_borrowed_item:
        msg = 'You already borrowed this item'
        raise Conflict(user_message='Buku ini telah anda pinjam', developer_message=msg)


def assert_user_has_not_reached_maximum_allowed(session, stock_org, user_id):
    list_of_org_id_and_child_ids = get_org_and_child_org_ids(stock_org)
    borrowed_item_count = session.query(UserBorrowedItem).filter(
        UserBorrowedItem.user_id == user_id,
        UserBorrowedItem.returned_time == None,
        UserBorrowedItem.catalog_item_org_id.in_(list_of_org_id_and_child_ids)
    ).count()

    if 0 < stock_org.user_concurrent_borrowing_limit <= borrowed_item_count:
        msg = 'Maximum borrowed item reached (maximum item {})'.format(stock_org.user_concurrent_borrowing_limit)
        raise Conflict(
            user_message='Maksimal kuota peminjaman {} buku.'.format(
                stock_org.user_concurrent_borrowing_limit)
            , developer_message=msg)


def assert_borrowed_cooldown(session, stock_org, current_user_id, item_id, catalog_id):
    """ when user borrow an item, assert reborrowing_cooldown from last trx history

    :param `db.session` session:
    :param `app.users.shared_library.SharedLibrary` stock_org:
    :param `int` current_user_id: from g.current_user.id
    :param `int` item_id:
    :param `int` catalog_id:
    :return:
    """
    date_now = datetime.now()
    cooldown_date = date_now + stock_org.reborrowing_cooldown
    if cooldown_date.replace(tzinfo=None) > date_now.replace(tzinfo=None):
        last_borrow = session.query(UserBorrowedItem).filter(
            UserBorrowedItem.user_id == current_user_id,
            UserBorrowedItem.catalog_item_catalog_id == catalog_id,
            UserBorrowedItem.catalog_item_org_id == stock_org.id,
            UserBorrowedItem.catalog_item_item_id == item_id,
            UserBorrowedItem.returned_time != None
        ).order_by(UserBorrowedItem.id.desc()).first()

        if last_borrow:
            check_returned_time = (last_borrow.returned_time + stock_org.reborrowing_cooldown).replace(tzinfo=None)

            if cooldown_date.replace(tzinfo=None) > check_returned_time:
                wait_time = relativedelta(cooldown_date, check_returned_time)
                user_message = 'Anda belum diizinkan untuk meminjam buku ini, ' \
                               'mohon menunggu selama {0.days} hari'.format(wait_time)
                msg = "Cooldown, you're not allowed to borrow this item now, please wait for " \
                      "{0.days} days.".format(wait_time)
                raise Conflict(user_message=user_message, developer_message=msg)


def assert_cooldown_for_reborrowing_item(shared_library_org):
    """ When user extend borrowing time, assert reborrowing_cooldown.

    :param `app.users.shared_library.SharedLibrary` shared_library_org:
    :return:
    """
    date_now = datetime.now()
    cooldown_date = date_now + shared_library_org.reborrowing_cooldown
    if cooldown_date > date_now:
        user_message = 'Anda belum diizinkan untuk meminjam buku ini, ' \
                       'mohon menunggu selama {} hari'.format(
            shared_library_org.reborrowing_cooldown
        )
        msg = "Cooldown, you're not allowed to borrow this item now, please wait for " \
              "{0.days} days.".format(shared_library_org.reborrowing_cooldown)
        raise Conflict(user_message=user_message, developer_message=msg)


def assert_user_vs_organization(session, org_id, user_id):
    # cant import this on top of files, make loop import!
    from app.users.organizations import OrganizationCatalog
    from app.eperpus.catalogs import Catalog

    user = session.query(User).get(user_id)

    # all relation now store on cas_organization_catalogs.
    # get all related users organizations.
    org_user = [org_.id for org_ in user.organizations.all()]

    # get all related catalogs organizations.
    data_catalog = OrganizationCatalog.query.filter(OrganizationCatalog.organization_id.in_(org_user)).all()
    catalog_ids = [catalog.catalog_id for catalog in data_catalog]

    exist_catalog = Catalog.query.filter(Catalog.id.in_(catalog_ids)).all()
    list_of_user_org = [cat_org.organization_id for cat_org in exist_catalog]

    # for user_org in user.organizations.all():
    #     list_of_user_org.append(user_org.id)
    #     # get the parent org also -- up to 2 level
    #     if user_org.parent_organization:
    #         list_of_user_org.append(user_org.parent_organization.id)
    #         if user_org.parent_organization.parent_organization:
    #             list_of_user_org.append(user_org.parent_organization.parent_organization.id)

    if org_id not in list_of_user_org:
        shared_lib = session.query(SharedLibrary).get(org_id)
        raise Forbidden(
            user_message='Data ini hanya bisa diakses oleh anggota {}'.format(shared_lib.name),
            developer_message='Catalog organization not match with user organization')


@blueprint.route('/organizations/<int:organization_id>/loan-book-history')
@user_is_authenticated
@is_organization_manager_by_id
def get_organization_loan_book(organization_id):
    args = request.args.to_dict()
    start_date = args.get('start_date', (datetime.now() - timedelta(days=14)).isoformat())
    end_date = args.get('end_date', datetime.now().isoformat())
    customize_org_id = args.get('org_id', None)
    limit, offset = args.get('limit', None), args.get('offset', 0)

    if start_date and end_date:
        date1, date2 = start_date.split('T'), end_date.split('T')
        start_date = '{} 00:00:00'.format(date1[0])
        end_date = '{} 23:59:59'.format(date2[0])

    if customize_org_id:
        organization_id = customize_org_id

    organizations = Organization.query.filter_by(id=organization_id).first()
    user_ids = [iuser.id for iuser in organizations.users]

    sql = '''
        select
            cas_users.username as "username",
            cas_users.first_name as "first_name",
            cas_users.last_name as "last_name",
            core_user_borrowed_items.borrowing_start_time as "borrowing_start_time",
            core_user_borrowed_items.returned_time as "returned_time",
            core_items.name as "title"
        from
            core_user_borrowed_items
            LEFT JOIN cas_users ON core_user_borrowed_items.user_id = cas_users.id
            LEFT JOIN core_items ON core_user_borrowed_items.catalog_item_item_id = core_items.id
        where
            borrowing_start_time >= '%s' and
            borrowing_start_time <= '%s'
    '''

    sql = sql % (start_date, end_date)

    if not organizations.parent_organization_id:
        # to find all borrowed item (ALL = organization_id + child org)
        child = Organization.query.filter_by(parent_organization_id=organization_id).all()
        data_organization = [organizations.id] + [i.id for i in child]

        # need to validate if catalog only one
        if len(data_organization) > 1:
            sql = sql + ' and catalog_item_org_id in {}'.format(tuple(data_organization))
        else:
            sql = sql + ' and catalog_item_org_id in (%s)' % data_organization[0]

    else:
        # to find specific organization borrowed item (child org)
        exist_user = OrganizationUser.query.filter_by(organization_id=customize_org_id).all()
        exist_user_ids = [i.user_id for i in exist_user]
        if len(exist_user_ids) > 1:
            sql = sql + ' and catalog_item_org_id in ({}) and user_id in {}'.format(organizations.parent_organization_id,
                                                                                    tuple(exist_user_ids))
        else:
            sql = sql + ' and catalog_item_org_id in ({}) and user_id in ({})'.format(organizations.parent_organization_id,
                                                                                      exist_user_ids[0])

    if user_ids:
        if len(user_ids) > 1:
            sql = sql + ' and user_id in {}'.format(tuple(user_ids))
        else:
            sql = sql + ' and user_id in (%s)' % user_ids[0]

    real_data = db.engine.execute(sql)
    data_count = len(real_data.fetchall())

    if limit:
        sql = sql + ' order by borrowing_start_time desc'
        sql = sql + ' limit %s offset %s' % (limit, offset)
    else:
        sql = sql + ' order by borrowing_start_time desc'

    data = db.engine.execute(sql)
    resp_ = []

    for row in data.fetchall():
        dict_resp_ = {}
        dict_resp_['username'] = row[0]
        dict_resp_['first_name'] = row[1]
        dict_resp_['last_name'] = row[2]
        dict_resp_['borrowing_start_time'] = row[3].isoformat()
        if row[4]:
            dict_resp_['returned_time'] = row[4].isoformat()
        else:
            dict_resp_['returned_time'] = None
        dict_resp_['title'] = row[5]
        resp_.append(dict_resp_)

    metadata, resultset = {}, {}
    resultset['count'] = data_count
    resultset['limit'] = int(limit) if limit else 0
    resultset['offset'] = int(offset) if offset else 0
    metadata['resultset'] = resultset

    return Response(json.dumps({'items': resp_, 'metadata': metadata}), status=httplib.OK, mimetype='application/json')


@blueprint.route('/organizations/<int:organization_id>/item-history/<int:item_id>/users')
@user_is_authenticated
@is_organization_manager_by_id
def get_organization_user_book(organization_id, item_id):
    args = request.args.to_dict()
    catalog_id = args.get('catalog_id', None)
    limit, offset = args.get('limit', 20), args.get('offset', 0)
    start_date = args.get('start_date', None)
    end_date = args.get('end_date', None)
    q = args.get('q', None)

    # find related item_id vs organization_id
    related_item = SharedLibraryItem.query.filter_by(
        organization_id=organization_id, item_id=item_id).first()

    if not related_item:
        return Response(status=httplib.NOT_FOUND, mimetype='application/json')
    else:
        if catalog_id:
            borrowed = UserBorrowedItem.query.filter_by(
                catalog_item_org_id=organization_id,
                catalog_item_item_id=item_id,
                catalog_item_catalog_id=catalog_id
            )
        else:
            borrowed = UserBorrowedItem.query.filter_by(
                catalog_item_org_id=organization_id,
                catalog_item_item_id=item_id
            )

        if start_date and end_date:
            borrowed = borrowed.filter(
                UserBorrowedItem.borrowing_start_time >= start_date,
                UserBorrowedItem.borrowing_start_time <= end_date
            )

        if q:
            q = '%{}%'.format(q)
            user = User.query.filter(User.username.ilike(q)).all()
            user_id = [i.id for i in user]
            borrowed = borrowed.filter(UserBorrowedItem.user_id.in_(user_id))

        total_count = len(borrowed.all())
        borrowed = borrowed.order_by(desc(UserBorrowedItem.borrowing_start_time)).limit(limit).offset(offset).all()

        data_borrowed = []
        for idata in borrowed:
            temp = {}
            temp['id'] = idata.id
            temp['organization_id'] = idata.catalog_item_org_id
            temp['catalog_id'] = idata.catalog_item_catalog_id
            temp['item_id'] = idata.catalog_item_item_id
            temp['user_id'] = idata.user_id
            temp['username'] = "{} {} ({})".format(
                idata.user.first_name if idata.user.first_name else '',
                idata.user.last_name if idata.user.last_name else '',
                idata.user.username).strip()
            temp['borrowing_start_time'] = idata.borrowing_start_time.isoformat()
            if idata.returned_time:
                temp['borrowing_end_time'] = idata.returned_time.isoformat()
            else:
                temp['borrowing_end_time'] = None
            data_borrowed.append(temp)

        metadata, resultset = {}, {}
        resultset['count'] = total_count
        resultset['limit'] = int(limit) if limit else 0
        resultset['offset'] = offset
        metadata['resultset'] = resultset

        return Response(json.dumps({'data': data_borrowed, 'metadata': metadata}), status=httplib.OK,
                        mimetype='application/json')


class UserBulkUpload(db.Model):
    __tablename__ = 'core_eperpus_upload'

    id = db.Column(db.Integer, primary_key=True)
    upload_file = db.Column(db.String(250))
    organization_id = db.Column(db.Integer, db.ForeignKey("cas_organizations.id"))
    organization = db.relationship(Organization)
    upload_type = db.Column(db.String(20))
    upload_date = db.Column(db.DateTime(timezone=True))

    complete_file = db.Column(db.String(250))
    status = db.Column(db.Integer)
    error_status = db.Column(db.String(250))
    created_by = db.Column(db.Integer, db.ForeignKey('cas_users.id'))
    created = db.relationship(User, backref='core_eperpus_upload')
    is_all_group = db.Column(db.Boolean())

    def values(self):
        aws_bucket = app.config['AWS_BUCKET']
        if self.complete_file:
            path_complete = os.path.join(aws_bucket, self.upload_type, self.complete_file if self.complete_file else '')
        else:
            path_complete = None

        return {
            'id': self.id,
            'upload_file': self.upload_file,
            'organization': {'id': self.organization.id, 'name': self.organization.name},
            'complete_file': self.complete_file,
            'upload_date': self.upload_date.isoformat(),
            'download_complete_file': path_complete,
            'created_by': self.created.email if self.created_by else '-',
            'download_upload_file': os.path.join(aws_bucket, self.upload_type,
                                                 self.upload_file if self.upload_file else '')
        }


def get_total_user(entity=None):
    if entity.parent_organization:
        max_user = entity.parent_organization.maximum_user_allowed
    else:
        max_user = entity.maximum_user_allowed

    org_id = [i.id for i in organization_list_parent(entity)] + [1]
    sql = """
        select
            count(distinct user_id)
        from
            cas_users_organizations where organization_id in {}
    """.format(tuple(org_id))

    data = db.engine.execute(sql)
    data_user = data.fetchone()[0]
    return data_user, max_user


@blueprint.route('/organizations/<organization:entity>/<type>', methods=['GET', 'POST', ])
@is_organization_manager
def upload_member_files(entity, type=None):
    EPERPUS_MEMBER_DIR, ALLOWED_EXTENSIONS, MAX_COUNT = '/tmp/members', ['csv'], 1000

    if request.method == 'GET':
        args = request.args.to_dict()
        limit, offset = args.get('limit', 20), args.get('offset', 0)

        total_user, max_user = get_total_user(entity)
        organization_id = [i.id for i in organization_list_parent(entity)]

        data_upload = UserBulkUpload.query.filter(UserBulkUpload.organization_id.in_(organization_id),
                                                  UserBulkUpload.upload_type == type).order_by(desc(UserBulkUpload.id))

        total_count = len(data_upload.all())
        if limit:
            data_upload = data_upload.limit(limit).offset(offset)

        data = [i.values() for i in data_upload.all()]

        return jsonify({
            'data': data,
            'metadata': {
                'resulset': {
                    'count': total_count,
                    'limit': limit,
                    'offset': offset
                }
            },
            'info': {
                "total_user": total_user,
                "max_user": max_user
            }
        }), OK

    if request.method == 'POST':
        form_request = request.form.to_dict()

        total_user, max_user = get_total_user(entity)

        if type not in ['upload-add', 'upload-delete']:
            raise NotFound
        if type == 'upload-add':
            csv_headers = app.config['BULK_ADD_CSV_HEADER']
            upload_organization = Organization.query.filter_by(id=form_request.get('organization_id', ''),
                                                               type='shared library').first()
        else:
            csv_headers = app.config['BULK_DELETE_CSV_HEADER']
            MAX_COUNT = 2000
            upload_organization = entity

        if not upload_organization:
            raise BadRequest(user_message='Invalid Organization!', developer_message='Invalid Organization')

        # validate if files exist or not
        if 'file' not in request.files:
            raise BadRequest(user_message='Missing file, Please upload a csv file.',
                             developer_message='Missing file, Please upload a csv file.')

        # validate extensions
        file_upload = request.files['file']
        extension = os.path.basename(file_upload.filename).split('.')[-1:][0].lower()

        if extension not in ALLOWED_EXTENSIONS:
            raise BadRequest(user_message='Upload failed! Please upload a csv file.',
                             developer_message='Upload failed! Please upload a csv file.')

        # create directory & save file on drive
        if not os.path.exists(EPERPUS_MEMBER_DIR): os.makedirs(EPERPUS_MEMBER_DIR, 0777)
        new_filename = 'import-pengguna-{}-{}.csv'.format(
            datetime.strftime(datetime.now(), '%Y%m%d'), datetime.now().microsecond).lower()
        csv_files = os.path.join(EPERPUS_MEMBER_DIR, new_filename)
        file_upload.save(csv_files)

        # check file formating header if same.
        label_valid = []
        if os.path.isfile(csv_files):
            reader = csv.reader(open(csv_files, 'rb'), delimiter=str(u',').encode('utf-8'))
            raw_label = reader.next()

            if len(raw_label) != len(csv_headers):
                reader = csv.reader(open(csv_files, 'rb'), delimiter=str(u';').encode('utf-8'))
                raw_label = reader.next()

            row_count = sum(1 for row in reader)
            if max_user <= 0 or max_user is None:
                raise BadRequest(
                    user_message='Upload failed! Maximum user allowed has not been set.',
                    developer_message='Upload failed! Maximum user allowed has not been set.')

            if total_user + row_count > max_user and type in ['upload-add']:
                raise BadRequest(
                    user_message='Upload failed! The maximum number of users allowed is {}'.format(max_user),
                    developer_message='Upload failed! The maximum number of users allowed is {}'.format(max_user))

            if row_count > MAX_COUNT:
                raise BadRequest(
                    user_message='Upload failed! The maximum number of rows allowed in csv file is {}'.format(
                        MAX_COUNT),
                    developer_message='Upload failed! The maximum number of rows allowed in csv file is {}'.format(
                        MAX_COUNT))

            for ilabel in csv_headers:
                if str(ilabel) not in raw_label:
                    label_valid.append(False)

            if False in label_valid:
                os.remove(csv_files)
                raise BadRequest(
                    user_message='Upload failed! Please use the csv template provided (windows comma separated (.csv)).',
                    developer_message='Upload failed! Please use the csv template provided (windows comma separated (.csv)).')
            else:
                new_data = UserBulkUpload(
                    upload_file=new_filename,
                    organization_id=upload_organization.id,
                    upload_type=type,
                    upload_date=datetime.now(),
                    status=1,
                    created_by=g.current_user.id
                )
                db.session.add(new_data)
                db.session.commit()

                # send to aws static.
                s3_server = connect_gramedia_s3()
                bucket_name = app.config['BOTO3_GRAMEDIA_STATIC_BUCKET']
                aws_folder = aws_check_files(s3_server, bucket_name, '{}/{}/'.format('eperpus', type))

                if not aws_folder:
                    s3_server.put_object(Bucket=bucket_name, Key='{}/{}/'.format('eperpus', type), ACL='public-read')
                s3_server.upload_file(csv_files, bucket_name, '{}/{}/{}'.format('eperpus', type, new_filename),
                                      ExtraArgs={'ContentType': 'binary/octet-stream', 'ACL': 'public-read'})

        return jsonify({'message': 'upload file successfully..'}), OK


@blueprint.route('/organizations/<organization:entity>/groups', methods=['GET', ])
@is_organization_manager
def list_organization_child(entity):
    data = []
    list_of_groups = organization_list_parent(entity)
    for idata in list_of_groups:
        data.append({'id': idata.id, 'is_default': True if idata.id == entity.id else False, 'name': idata.name,
                     'href': idata.api_url})

    return jsonify({'data': data}), OK


@blueprint.route('/organizations/<organization:entity>/groups/<organization:entity_del>', methods=['DELETE', ])
@is_organization_manager
def remove_organization_child(entity, entity_del):
    message_error = None

    if entity_del.parent_organization_id is None:
        message_error = "Failed, This is parent Organizations"
    if entity_del.parent_organization_id != entity.id:
        message_error = 'Failed, This grup are not match with parent organization.'
    if entity_del.users.count():
        message_error = 'Hapus Grup gagal, silakan hapus pengguna di dalam Grup terlebih dulu'

    if message_error:
        raise BadRequest(user_message=message_error, developer_message=message_error)

    entity_del.is_active = False
    db.session.add(entity_del)
    db.session.commit()

    return jsonify({'message': "Grup successfully inactive"}), NO_CONTENT
