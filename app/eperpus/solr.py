import requests
from flask import current_app
from flask_appsfoundry.exceptions import BadRequest

from app.services.solr import SolrQueryBuilder, SolrGateway
from app.utils.dicts import CorDefaultDict


def facet_field_mapper(facet_field):
    value_map = {
                "item_categories": "category_names",
                "item_authors": "author_names",
                "item_languages": "item_languages",
                "category_names": "category_names",
                "author_names": "author_names"
            }

    return value_map[facet_field]


class CatalogItemQueryBuilder(SolrQueryBuilder):

    def __init__(self, catalog_id, organization_id=None, q_param=None, user_agent=None, *args, **kwargs):
        super(CatalogItemQueryBuilder, self).__init__(*args, **kwargs)
        self.catalog_id = catalog_id
        self.organization_id = organization_id
        self.q_param = q_param
        self.user_agent = user_agent

    def _build_query(self):
        self.q_param = self.q_param or self.flask_req.args.get('q', '*')

        if self.user_agent and self.q_param == '*':
            args = {'q': '{}, quantity_available:[1 TO *]^5, quantity_available:[0 TO 0]^2'.format(self.q_param), }
        else:
            args = {'q': self.q_param}

        if self.q_param == '*':
            args['sort'] = "score desc, brand_name_sortable asc, item_release_date desc"
        else:
            args['df'] = 'item_name'

        return args

    def _build_filter_queries(self):
        query = super(CatalogItemQueryBuilder, self)._build_filter_queries()
        query['fq'] = ['{}:"{}"'.format(facet_field_mapper(f.split(":")[0]), f.split(":")[1]) for f in
                       self.flask_req.args.getlist('facets')]
        if self.catalog_id:
            query['fq'].append('catalog_ids:{}'.format(self.catalog_id))
        if self.organization_id:
            query['fq'].append('org_id:{}'.format(self.organization_id))
        return query

    def build(self):
        """ Additional filter by available stock
        :return: returning query for solr
        :rtype: dict
        """
        query = super(CatalogItemQueryBuilder, self).build()
        if self.flask_req.args.get('available', False) == 'true':
            query['fq'].append('quantity_available:[1 TO *]')
        return query

    def _build_order(self):
        # order = self.flask_req.args.get('order')

        # if order:
        #     if order in ['brand_name', '-brand_name']:
        #         return {'sort': "{}".format(
        #             "brand_name_sortable asc"
        #          if not order.startswith("-") else "brand_name_sortable desc")}
        #     else:
        #         raise BadRequest(developer_message='Support brand_name Only')
        # else:
        #     return {}
        return {'sort': "{}".format("score desc, brand_name_sortable asc, item_release_date desc")}


def get_catalog_items_search(req, catalog_id, organization_id=None, q_param=None, user_agent=None):
    solr_url = current_app.config['SOLR_BASE_URL'] + '/' + current_app.config['SOLR_SHAREDLIB_CORE'] + '/select'

    solr_query_builder = CatalogItemQueryBuilder(
        catalog_id=catalog_id,
        organization_id=organization_id,
        q_param=q_param,
        flask_req=req,
        user_agent=user_agent,
        facet_list=["author_names", "category_names", "item_languages", ],
    )

    #print solr_query_builder.build()
    response_json = requests.get(solr_url, params=solr_query_builder.build()).json()
    return response_json


def format_catalog_items_response(session, solr_response):
    facet_name_mapper = CorDefaultDict(category_names='item_categories', author_names='item_authors')
    return {
      "items": [ ],
      "metadata": {
        "resultset": {
          "count": solr_response['response']['numFound'],
          "limit": len(solr_response['response']['docs']),
          "offset": solr_response['response']['start']
        },
        "facets": [{"field_name": facet_name_mapper[key],
                    "values": [
                                {"value": t[0], "count": t[1]} for t in zip(facet[0::2], facet[1::2])
                                ]
                    } for key, facet in solr_response.get('facet_counts', {}).get('facet_fields', {}).items()],
        "spelling_suggestions": [{'value': d['word'],
                                  'count': d['freq']} for d in solr_response
            .get("spellcheck", {})
            .get("suggestions", [None, {}])[1]
            .get("suggestion", [])
         ]

      }
    }


class StockSolr(object):

    def __init__(self, solr):
        self.solr = solr

    def select_item(self, item_id, catalog_id, fields='*', **kwargs):
        """ query item from solr by item id
        :param `int` item_id: an item id `app.items.models.Item`
        :param `int` catalog_id: an catalog id`
        :param `list` fields: fields selection
        :return: returning item from solr
        :rtype: `dict`
        """
        response = self.solr.search(
            self._set_params(item_id, catalog_id, fields, **kwargs)
        )

        item = {}
        if response.get('response'):
            if response['response'].get('docs'):
                item = response['response']['docs'][0]

        return item

    def _set_params(self, item_id, catalog_Id, fields, **kwargs):
        """
            set parameter for updating document to solr
        :param `int` item_id: an item id `app.items.models.Item`
        :param `item` catalog_id: organization id
        :param `list` fields: default can be '*'
        :param `dict` kwargs:
        :return: returning parameter for solr
        :rtype: `dict`
        """
        return {
            'q': 'item_id:{}'.format(item_id),
            'fq': [
                'catalog_ids:{}'.format(catalog_Id)
            ],
            "fl": ",".join(fields)
        }

    def increment_stock(self, item, value=1):
        """
            Update stock with increment value
        :param `dict` item: an item from StockSolr.select_item
        :param `int` value: increment of value ex 1 or -1
        :return: returning response from solr
        :rtype: `dict`
        """
        return self.solr.update([self._update_stock(item, value)])

    def decrement_stock(self, item, value=-1):
        """
            Update stock with decerement value
        :param `dict` item: an item from StockSolr.select_item
        :param `int` value: increment of value ex 1 or -1
        :return: returning response from solr
        :rtype: `dict`
        """
        return self.solr.update([self._update_stock(item, value)])

    def _update_stock(self, item, value):
        """
            update stock. for detail documentation
            `https://cwiki.apache.org/confluence/display/solr/Updating+Parts+of+Documents`

        :param `dict` item: an item from StockSolr.select_item
        :param `int` value: increment of value ex 1 or -1
        :return: returning item with new value of stock
        :rtype: `dict`
        """
        stock = item.get('quantity_available', None)
        if stock:
            item['quantity_available'] = {'inc': value}
        else:
            item.update({'quantity_available': value})
        return item


class SolrSuggester(object):

    def __init__(self, flask_req):
        self.flask_req = flask_req

    def build(self):
        return {
            'q': self.flask_req.args.get('q', '*')
        }


class BrandNameSuggester(SolrSuggester):

    def __init__(self, catalog_id, *args, **kwargs):
        super(BrandNameSuggester, self).__init__(*args, **kwargs)
        self.catalog_id = catalog_id

    def build(self):
        params = super(BrandNameSuggester, self).build()
        params.update({
            'suggest.cfq': self.catalog_id
        })
        return params


def get_brand_name_suggestion(req, catalog_id):
    solr_url = current_app.config['SOLR_BASE_URL'] + '/' + current_app.config['SOLR_SHAREDLIB_CORE'] + '/suggest'
    brand_name_suggester = BrandNameSuggester(flask_req=req, catalog_id=catalog_id)
    response_json = requests.get(solr_url, params=brand_name_suggester.build()).json()

    result = [suggest.get('term') for suggest in response_json.get('suggest').get(
                                                                    'brandNameSuggester').get(
                                                                     req.args.get('q','*'))['suggestions']]

    data_result = list(set(result))
    return data_result


def update_stock_solr(item_id, catalog_id, qty):
    """ Update stock in solr

    :param item_id:
    :param catalog_id:
    :param `integer` qty:
    :return:
    """

    solr = SolrGateway(current_app.config['SOLR_BASE_URL'], current_app.config['SOLR_SHAREDLIB_CORE'])
    stock_solr = StockSolr(solr)
    item = stock_solr.select_item(item_id=item_id, catalog_id=catalog_id)
    if qty == -1:
        stock_solr.decrement_stock(item)
    elif qty == 1:
        stock_solr.increment_stock(item)
