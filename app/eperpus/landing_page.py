import json, httplib, base64
from flask import Response, request, jsonify, Blueprint
from flask_appsfoundry.exceptions import BadRequest
from app.helpers import generate_invoice
from app.uploads.aws.helpers import connect_gramedia_s3
from app import db, ma, app
from app.eperpus.models import LandingPage
from app.utils.marshmallow_helpers import schema_load
from marshmallow import fields

blueprint = Blueprint('landing_page', __name__, url_prefix='/v1/organizations')


class VersionSchema(ma.Schema):
    app_name_version = fields.Str(required=True)
    app_size = fields.Str(required=True)
    app_whats_new = fields.Str(required=True)
    app_date = fields.Str(required=False)


class LandingPageSchema(ma.Schema):
    app_title = fields.Str(required=True)
    app_name = fields.Str(required=True)
    app_slug = fields.Str(required=True)
    app_versions = fields.List(fields.Nested(VersionSchema))
    app_description = fields.Str(required=True)
    app_icon = fields.Str(required=False)
    app_link_android = fields.Str(required=False)
    app_link_ios = fields.Str(required=False)
    app_previews = fields.List(fields.String())


def upload_image(entity, landing_schema):
    app_icon = landing_schema.pop("app_icon", None)
    app_previews = landing_schema.pop("app_previews", [])
    if app_icon:
        if len(app_icon) > 150:
            icon_url = upload_logo_to_s3(app_icon, entity.id)
        else:
            icon_url = app_icon
        landing_schema["app_icon"] = icon_url

    new_app_previews = []
    for preview_ in app_previews:
        if len(preview_) > 150:
            url = upload_logo_to_s3(preview_, entity.id)
            new_app_previews.append(url)
        else:
            new_app_previews.append(preview_)
    landing_schema['app_previews'] = new_app_previews


def create_landing_page(entity, landing_schema):
    slug = landing_schema.get('app_slug', '')
    exist_slug = LandingPage.query.filter_by(slug=slug).first()
    if exist_slug:
        raise BadRequest(user_message='Slug already exists!',
                         developer_message='Slug already exists!')

    landing_schema.pop("app_slug", None)
    landing_page_obj = LandingPage.query.filter_by(organization_id=entity.id).first()
    if landing_page_obj:
        landing_page_obj.data = landing_schema
        landing_page_obj.slug = slug

        db.session.add(landing_page_obj)
        db.session.commit()
    else:
        landing_page_obj = LandingPage(
            organization_id=entity.id,
            slug=slug,
            data=landing_schema,
        )

        db.session.add(landing_page_obj)
        db.session.commit()
    return landing_page_obj


def upload_logo_to_s3(data, orgs_id):
    dec = base64.b64decode(data)
    s3_server = connect_gramedia_s3()
    bucket_name = app.config['BOTO3_GRAMEDIA_STATIC_BUCKET']
    location = '{folder}/{orgs_id}/{name}.png'.format(folder='eperpus-landing-page', orgs_id=orgs_id,
                                                      name=generate_invoice())
    s3_server.put_object(Bucket=bucket_name, Key=location, Body=dec, ContentType='image/png', ACL='public-read',
                         ContentEncoding='base64')

    url = "{base}{bucket}/{location}".format(base=app.config['AWS_PUBLIC'], bucket=bucket_name, location=location)
    return url


# landing page, checking slug exist or not
@blueprint.route('/landing-page/<app_slug>', methods=['GET'])
def get_landing_page(app_slug):
    if request.method == 'GET':
        exist_slug = LandingPage.query.filter_by(slug=app_slug).first()
        if not exist_slug:
            return Response(None, status=httplib.NO_CONTENT)

        response = jsonify(exist_slug.custom_values())
        response.status_code = httplib.OK
        return response


# create new landing page & get list of landing page
@blueprint.route('/<organization:entity>/landing-page', methods=['POST', 'GET', 'PUT'])
def landing_page(entity):
    if request.method == "POST":
        landing_schema = schema_load(LandingPageSchema())
        upload_image(entity, landing_schema)
        new_data = create_landing_page(entity, landing_schema)

        response = jsonify(new_data.values())
        response.status_code = httplib.CREATED
        return response

    # get current landing page
    if request.method == "GET":
        exist_landing_page = LandingPage.query.filter_by(organization_id=entity.id).first()

        if not exist_landing_page:
            return Response(None, status=httplib.NO_CONTENT)

        response = jsonify(exist_landing_page.values())
        response.status_code = httplib.OK
        return response

    # update landing page
    if request.method == 'PUT':
        landing_schema = schema_load(LandingPageSchema())
        exist_data = LandingPage.query.filter_by(organization_id=entity.id).first()
        if not exist_data: return Response(status=httplib.NOT_FOUND)

        # check duplicate slug
        update_slug = landing_schema.get('app_slug', '')
        if update_slug != exist_data.slug:
            duplicate_slug = LandingPage.query.filter(LandingPage.slug == update_slug).first()
            if duplicate_slug:
                return BadRequest(user_message='Slug already exists!',
                                 developer_message='Slug already exists!')

        upload_image(entity, landing_schema)
        landing_schema.pop("app_slug", None)
        exist_data.organization_id = entity.id
        exist_data.slug = update_slug
        exist_data.data = landing_schema

        db.session.add(exist_data)
        db.session.commit()

        response = jsonify(exist_data.values())
        response.status_code = httplib.OK
        return response
