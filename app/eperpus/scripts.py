import csv
import os
import re
from datetime import datetime, timedelta, time

import requests
from dateutil.relativedelta import relativedelta
from flask import Blueprint
from sqlalchemy import desc
from unidecode import unidecode
from validate_email import validate_email

from app import app, db
from app.eperpus.catalogs import CatalogBrand
from app.eperpus.categories import CategoryEperpusItem, CategoryEperpus
from app.eperpus.helpers import update_qty_available
from app.eperpus.libraries import SharedLibraryItem, SharedLibraryBrand, SharedLibrary
from app.eperpus.models import CatalogItem
from app.eperpus.transactions import TransactionBulkUpload
from app.helpers import generate_invoice
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.items.models import Item
from app.master.models import Brand
from app.offers.choices.offer_type import SUBSCRIPTIONS
from app.offers.models import Offer, OfferSubscription
from app.orders.models import Order, OrderLine
from app.payments.choices import PAYMENT_BILLED
from app.payments.models import Payment
from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files
from app.users.organizations import Organization, OrganizationUser
from app.users.users import User, Role
from app.utils.datetimes import get_local
from members import UserBulkUpload, UserBorrowedItem

blueprint = Blueprint('shared_library_scripts', __name__, url_prefix='/v1')
blueprint_transaction = Blueprint('shared_library_transaction', __name__, url_prefix='/v1')


@blueprint_transaction.route('/transactions')
def user_subscribed_catalogs():
    TransactionBulkAddProcessing().process()
    return 'OK'


class ProcessingFiles(object):

    def __init__(self):
        self.NEW, self.PROCESS, self.ERROR, self.COMPLETE = 1, 2, 3, 4

        self.data_execute = []
        self.data_csv_report = []
        self.master_organization = None
        self.general_message_delete = 'User not found.'
        self.path_tmp = app.config['BULK_DIR']
        self.general_success = 'Success'
        self.general_success_eperpus = 'Berhasil'
        self.invalid_input = ['', None]
        self.s3_server = connect_gramedia_s3()

    def delete_user(self, data):
        try:
            self.log_files(data, self.PROCESS, None)
            csv_file = os.path.join(self.path_tmp, data.upload_file)

            # get all organization.
            organization_parent = [data.organization.id, 0]

            if data.organization.parent_organization_id:
                all_child = Organization.query.filter_by(parent_organization_id=data.organization.parent_organization_id).all()
            else:
                # this mean organization on db is parent
                all_child = Organization.query.filter_by(parent_organization_id=data.organization.id).all()

            organization_delete = list(set(organization_parent + [i.id for i in all_child]))

            if os.path.isfile(csv_file):
                reader = csv.reader(open(csv_file, 'rb'), delimiter=str(u',').encode('utf-8'))
                label_name = reader.next()

                if len(label_name) != len(app.config['BULK_DELETE_CSV_HEADER']):
                    reader = csv.reader(open(csv_file, 'rb'), delimiter=str(u';').encode('utf-8'))
                    label_name = reader.next()

                for b in reader:
                    data_label = [i.split(',') for i in b]
                    rw = zip(label_name, data_label[0])
                    self.data_execute.append(dict(rw))

                for idata in self.data_execute:
                    # check username inside DB
                    username = idata.get('username', '').strip()
                    exist_users = User.query.filter_by(username=username).first()

                    if not exist_users:
                        idata['status'] = self.general_message_delete
                    else:
                        idata['status'] = 'Berhasil'

                    if exist_users:
                        # check user related with organization.
                        related = bool(
                            OrganizationUser.query.filter(
                                OrganizationUser.organization_id.in_(organization_delete),
                                OrganizationUser.user_id == exist_users.id
                            ).count()
                        )
                        if not related:
                            idata['status'] = self.general_message_delete
                        else:
                            self.remove_user_org(exist_users, organization_delete)
                            self.auto_return_borrowed(exist_users, organization_delete)

                    extract_data = dict((k, v) for k, v in idata.iteritems() if v not in ['', ' '])
                    self.data_csv_report.append(extract_data)

                self.export_to_csv(data, app.config['BULK_DELETE_CSV_HEADER'], 'report-delete')
                self.log_files(data, self.COMPLETE, None)
                self.update_solr()

            else:
                return data, 'File Not Exists'
            return data, 'OK'

        except Exception as e:
            return data, 'Error {}'.format(e)

    def update_solr(self):
        try:
            shared_lib_url = '{}/scoop-sharedlib-all/dataimport'.format(app.config['SOLR_BASE_URL'])
            requests.post(shared_lib_url, data={'command': 'delta-import', 'wt': 'json'})

            user_lib_url = '{}/scoop-users/dataimport'.format(app.config['SOLR_BASE_URL'])
            requests.post(user_lib_url, data={'command': 'delta-import', 'wt': 'json'})

        except Exception as e:
            pass

    def auto_return_borrowed(self, exist_users, organization_delete):
        session = db.session()

        borrowed_data = UserBorrowedItem.query.filter(
            UserBorrowedItem.user_id == exist_users.id,
            UserBorrowedItem.catalog_item_org_id.in_(organization_delete),
            UserBorrowedItem.returned_time == None
        ).all()

        for user_borrowed_item in borrowed_data:
            user_borrowed_item.returned_time = get_local()

            # increase available qty
            update_qty_available(
                session=session,
                current_org=user_borrowed_item.catalog_item.shared_library_item.shared_library,
                item_id=user_borrowed_item.catalog_item.shared_library_item.item.id,
                qty=1
            )
            session.commit()

    def remove_user_org(self, exist_users, organization_delete):
        # remove user by sql
        remove_org = """
            delete from cas_users_organizations WHERE
            user_id = {} AND
            organization_id in {}
        """.format(exist_users.id, tuple(organization_delete))
        db.engine.execute(remove_org)

        # update cas_users set modified now so solr can reindex.
        exist_users.modified = datetime.now()
        db.session.add(exist_users)
        db.session.commit()

    def send_file_aws(self, data, raw_files, new_filename):
        # send to aws static.
        bucket_name = app.config['BOTO3_GRAMEDIA_STATIC_BUCKET']
        aws_folder = aws_check_files(self.s3_server, bucket_name, '{}/{}/'.format('eperpus', data.upload_type))

        if not aws_folder:
            self.s3_server.put_object(Bucket=bucket_name, Key='{}/{}/'.format('eperpus', data.upload_type), ACL='public-read')

        self.s3_server.upload_file(raw_files, bucket_name, '{}/{}/{}'.format('eperpus', data.upload_type, new_filename),
                              ExtraArgs={'ContentType': 'binary/octet-stream', 'ACL': 'public-read'})

    def export_to_csv(self, data, fieldnames, title):
        new_filename = '{}-{}-{}.csv'.format(title, datetime.strftime(datetime.now(), '%Y%m%d'),
                                             datetime.now().microsecond).lower()
        new_files = os.path.join(self.path_tmp, new_filename)
        raw_files = os.path.join(self.path_tmp, data.upload_file)

        try:
            fieldnames.append('status')
            with open(new_files, 'wb') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter=';')
                writer.writeheader()

                for idata in self.data_csv_report:
                    writer.writerow(idata)
            csvfile.close()
            self.send_file_aws(data, new_files, new_filename)

            data.complete_file = new_filename
            db.session.add(data)
            db.session.commit()

            os.remove(new_files)
            os.remove(raw_files)

        except Exception as e:
            print e

    def validate_character(self, data):
        # unidecode need unicode string
        try:
            unistring = u'{}'.format(data)
            return unidecode(unistring) == data
        except UnicodeDecodeError:
            return False

    def add_user(self, data):
        try:
            self.log_files(data, self.PROCESS, None)
            csv_file = os.path.join(self.path_tmp, data.upload_file)

            # get organizations for added.
            if data.is_all_group:
                # this add to all group of parents organization id
                parent_id = data.organization.parent_organization_id
                if parent_id:
                    all_organizations = Organization.query.filter_by(parent_organization_id=parent_id).all()
                else:
                    all_organizations = Organization.query.filter_by(parent_organization_id=data.organization.id).all()
                organization_data = [data.organization] + all_organizations
            else:
                # always automaticly insert to parents.
                if data.organization.parent_organization_id:
                    organization_data = [data.organization, data.organization.parent_organization]
                else:
                    organization_data = [data.organization]

            if os.path.isfile(csv_file):
                reader = csv.reader(open(csv_file, 'rb'), delimiter=str(u',').encode('utf-8'))
                label_name = reader.next()

                if len(label_name) != len(app.config['BULK_ADD_CSV_HEADER']):
                    reader = csv.reader(open(csv_file, 'rb'), delimiter=str(u';').encode('utf-8'))
                    label_name = reader.next()

                for b in reader:
                    data_label = [i for i in b]
                    rw = zip(label_name, data_label)
                    self.data_execute.append(dict(rw))

                for idata in self.data_execute:
                    idata['status'] = 'Berhasil'

                    email = idata.get('email', '').strip().lower()
                    username = idata.get('username', '').strip().lower()
                    password = idata.get('password', '').strip().lower()
                    job = idata.get('job', '').strip().lower()

                    if password in self.invalid_input:
                        idata['status'] = "Password harus diisi"
                    if username in self.invalid_input:
                        idata['status'] = "Username harus diisi"
                    if not validate_email(email):
                        idata['status'] = "Format email salah"

                    if (not self.validate_character(username) or not self.validate_character(password) or
                        not self.validate_character(job) or not self.validate_character(idata.get('name'))):
                        idata['status'] = "Ada karakter yang tidak dikenal"

                    if idata['status'] == self.general_success_eperpus:
                        exist_users = User.query.filter_by(email=email).first()
                        if exist_users:
                            self.update_existing_users(idata, exist_users, organization_data)
                        else:
                            idata = self.create_new_users(idata, organization_data, data.organization.username_prefix)

                    extract_data = dict((k, v) for k, v in idata.iteritems() if v not in ['', ' '])
                    self.data_csv_report.append(extract_data)

                self.export_to_csv(data, app.config['BULK_ADD_CSV_HEADER'], 'report-add')
                self.log_files(data, self.COMPLETE, None)
                self.update_solr()
            else:
                return data, 'File Not Exists'

            return data, 'OK'

        except Exception as e:
            return data, 'Error {}'.format(e)

    def update_role_user(self, exist_users=None):
        from app.constants import RoleType

        if not exist_users.is_verified:
            banned_role = None
            for role in exist_users.roles:
                if role.id == RoleType.banned_user.value:
                    banned_role = role

            if banned_role:
                pass
            else:
                verified_role = Role.query.filter_by(id=RoleType.verified_user.value).first()
                # need to clean up previous role
                for role in exist_users.roles:
                    exist_users.roles.remove(role)

                exist_users.is_verified = True
                exist_users.roles.append(verified_role)

                db.session.add(exist_users)
                db.session.commit()

    def update_existing_users(self, idata, exist_users, organization_data):
        username = idata.get('username', None).lower().strip()
        email = idata.get('email', '').lower().strip()
        job = idata.get('job', '').strip().lower()

        exist_users_by_email = User.query.filter_by(email=email).first()
        if exist_users_by_email and exist_users_by_email.username != username:
            exist_users_by_username = User.query.filter_by(username=username).first()

            if exist_users_by_username:
                idata['status'] = 'Gagal, Username {} sudah digunakan User lain, silahkan ganti.'.format(username)
                return idata

        password = idata.get('password').strip()
        if not re.search('^[a-zA-Z0-9]*$', password) or not len(password) >= 6:
            idata['status'] = 'Password harus alphanumeric & min. 6 karakter'
            return idata

        exist_org = [o.id for o in exist_users.organizations.all()]

        for iorg in organization_data:
            if iorg.id not in exist_org:
                exist_users.organizations.append(iorg)
                exist_users.modified = datetime.now()

        fullname = idata.get('name', '').strip().title().split(' ')

        exist_users.first_name = fullname[0]
        exist_users.last_name = " ".join(fullname[1:])
        exist_users.username = idata.get('username').strip().lower()
        exist_users.level = job

        idata['status'] = 'Berhasil. Email {exist_email} pernah didaftarkan pada sistem kami sebelumnya, mohon informasikan ke user untuk menggunakan password lama ' \
                          'klik forget password dari aplikasi untuk buat password baru'.format(exist_email=exist_users.email)

        self.update_role_user(exist_users=exist_users)
        return idata

    def create_new_users(self, idata, organization_data, username_prefix):
        from hashlib import sha1
        from app.constants import RoleType

        try:
            username = str(idata.get('username', 'unknown').lower().strip())
            email = str(idata.get('email', 'unknown').lower().strip())
        except:
            idata['status'] = 'User memiliki karakter yang tidak dikenal'
            return idata

        exist_users_by_username = User.query.filter_by(username=username).first()
        if exist_users_by_username and exist_users_by_username.email != email:
            idata['status'] = 'Gagal, Username {} sudah digunakan User lain, silahkan ganti.'.format(username)
            return idata

        exist_users_by_email = User.query.filter_by(email=email).first()
        if exist_users_by_email and not exist_users_by_username:
            idata['status'] = self.general_success_eperpus

            exist_users_by_email.username = username
            for iorg in organization_data:
                exist_users_by_email.organizations.append(iorg)

            db.session.add(exist_users_by_email)
            db.session.commit()

        salt_password = app.config['PASSWORD_HASH']
        role = Role.query.filter_by(id=RoleType.verified_user.value).first()

        if username_prefix and username_prefix.lower() != username[:len(username_prefix)]:
            idata['status'] = 'Prefix username tidak valid'

        password = idata.get('password').strip()
        if not re.search('^[a-zA-Z0-9]*$', password) or not len(password) >= 6:
            idata['status'] = 'Password harus alphanumeric & min. 6 karakter'
            return idata

        if idata['status'] == self.general_success_eperpus:
            fullname = idata.get('name', '').strip().title().split(' ')
            user = User(
                username=username.strip().lower(),
                email=email.strip().lower(),
                first_name=fullname[0],
                last_name=" ".join(fullname[1:]),
                last_login=datetime.now(),
                is_verified=True,
                excluded_from_reporting=False,
                level=idata.get('job', '').strip().title(),
                created=datetime.now(),
                modified=datetime.now()
            )

            password_sha = sha1(salt_password + idata.get('password').strip()).hexdigest()
            user.set_password(password_sha)
            user.roles.append(role)

            # append organizations
            for iorg in organization_data:
                user.organizations.append(iorg)

            db.session.add(user)
            db.session.commit()

        return idata

    def log_files(self, data, state, error_status):
        data.error_status = error_status
        data.status = state
        db.session.add(data)
        db.session.commit()

    def retrieve_data_and_process(self):
        data_bulk = UserBulkUpload.query.filter_by(status=self.NEW).all()


        if data_bulk:
            for data in data_bulk:
                data_file = os.path.join(app.config['BULK_DIR'], data.upload_file)
                if not os.path.exists(app.config['BULK_DIR']):
                    os.makedirs(app.config['BULK_DIR'])

                if not os.path.isfile(data_file):
                    bucket_file = 'eperpus/{upload_type}/{upload_file}'.format(upload_type=data.upload_type,
                                                                               upload_file=data.upload_file)
                    # bucket_file = os.path.join('eperpus', data.upload_type, data.upload_file)
                    self.s3_server.download_file(app.config['BOTO3_GRAMEDIA_STATIC_BUCKET'], bucket_file, data_file)
                else:
                    if data.upload_type == 'upload-add':
                        return self.add_user(data)

                    if data.upload_type == 'upload-delete':
                        return self.delete_user(data)

        return 'OK', 'OK'

    def process(self):
        data, status = self.retrieve_data_and_process()
        if not status == 'OK':
            self.log_files(data, self.ERROR, status)


class TransactionBulkAddProcessing(object):

    def __init__(self):
        self.NEW, self.PROCESS, self.ERROR, self.COMPLETE = 1, 2, 3, 4

        self.data_execute = []
        self.data_csv_report = []
        self.data_report = []
        self.master_organization = None
        self.general_message_delete = 'Transaksi Gagal.!'
        self.path_tmp = app.config['BULK_TRANSACTION']
        self.general_message = 'Success'
        self.invalid_input = ['', None, 'N/A', '#N/A']
        self.threshold_days = 30  # days
        self.s3_server = connect_gramedia_s3()

        self.total_price = []

    def log_files(self, data, state, error_status):
        data.error_status = error_status
        data.status = state
        db.session.add(data)
        db.session.commit()

    def update_solr(self):
        try:
            shared_lib_url = '{}/scoop-sharedlib-all/dataimport'.format(app.config['SOLR_BASE_URL'])
            requests.post(shared_lib_url, data={'command': 'delta-import', 'wt': 'json'})

        except Exception as e:
            pass

    def export_to_csv(self, data, fieldnames, title, transaction='single'):
        new_filename = '{}-{}-{}.csv'.format(
            title,
            datetime.strftime(datetime.now(), '%Y%m%d'),
            datetime.now().microsecond).lower()

        new_files = os.path.join(self.path_tmp, new_filename)
        raw_files = os.path.join(self.path_tmp, data.upload_file)

        try:
            if transaction == 'single':
                for idata in self.data_report:
                    if idata.get('offer_id', None): del idata['offer_id']
                    if idata.get('item_name', None): del idata['item_name']
                fieldnames.append('status')

            else:
                for idata in self.data_report:
                    if idata.get('offer_id', None): del idata['offer_id']
                    if idata.get('total harga', None): del idata['total harga']
                fieldnames.append('item_name')
                fieldnames.append('harga')
                fieldnames.append('status')

            with open(new_files, 'wb') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter=';')
                writer.writeheader()

                for idata in self.data_report:
                    writer.writerow(idata)

            csvfile.close()
            self.send_file_aws(data, new_files, new_filename)

            data.complete_file = new_filename
            db.session.add(data)
            db.session.commit()

            os.remove(new_files)
            os.remove(raw_files)

        except Exception as e:
            print e

    def send_file_aws(self, data, raw_files, new_filename):
        # send to aws static.
        bucket_name = app.config['BOTO3_GRAMEDIA_STATIC_BUCKET']
        aws_folder = aws_check_files(self.s3_server, bucket_name, '{}/{}/'.format('eperpus', data.upload_type))

        if not aws_folder:
            self.s3_server.put_object(Bucket=bucket_name, Key='{}/{}/'.format('eperpus', data.upload_type), ACL='public-read')

        self.s3_server.upload_file(raw_files, bucket_name, '{}/{}/{}'.format('eperpus', data.upload_type, new_filename),
                              ExtraArgs={'ContentType': 'binary/octet-stream', 'ACL': 'public-read'})

    def integer_format(self, data, idata, module):
        try:
            if '.' in data:
                data.replace('.', 'm') #invalidate
            int(data)
        except:
            idata['status'] = 'Invalid Format {}'.format(module)
        return idata

    def add_single_transaction(self, data):
        try:
            self.log_files(data, self.PROCESS, None)
            csv_file = os.path.join(self.path_tmp, data.upload_file)

            if os.path.isfile(csv_file):
                reader = csv.reader(open(csv_file, 'rb'), delimiter=str(u',').encode('utf-8'))
                label_name = reader.next()

                if len(label_name) != len(app.config['BULK_ADD_CSV_TRANSACTION_SINGLE_ISSUE']):
                    reader = csv.reader(open(csv_file, 'rb'), delimiter=str(u';').encode('utf-8'))
                    label_name = reader.next()

                for b in reader:
                    data_label = [i for i in b]
                    rw = zip(label_name, data_label)
                    self.data_execute.append(dict(rw))

                for idata in self.data_execute:
                    idata['status'] = self.general_message

                    # check if..
                    eksemplar = idata.get('eksemplar', '')
                    price = idata.get('harga', '')
                    sum_price = idata.get('total harga', '')
                    item_id = idata.get('item id', '')

                    if item_id in self.invalid_input: idata['status'] = 'Invalid Data Item ID'
                    idata = self.integer_format(sum_price, idata, 'item id')

                    if eksemplar in self.invalid_input: idata['status'] = 'Invalid Data Eksemplar'
                    idata = self.integer_format(eksemplar, idata, 'eksemplar')

                    if price in self.invalid_input: idata['status'] = 'Invalid Data Harga'
                    idata = self.integer_format(price, idata, 'harga')

                    if sum_price in self.invalid_input: idata['status'] = 'Invalid Data Total Harga'
                    idata = self.integer_format(sum_price, idata, 'total harga')

                    if idata.get('status', '') == self.general_message:
                        exist_items = Item.query.filter_by(id=idata.get('item id', 0)).first()
                        if not exist_items:
                            idata['status'] = 'Invalid Data Item Not exist'

                        if exist_items:
                            if exist_items.item_type in ['book', 'magazine']:
                                offer_single = Offer.query.join(Item.core_offers).filter(
                                    Item.id.in_([exist_items.id])).filter_by(offer_type_id=1).order_by(Offer.id).first()
                            else:
                                offer_single = Offer.query.join(Brand.core_offers).filter(
                                    Brand.id.in_([exist_items.brand_id])).filter_by(
                                    is_active=True, offer_type_id=2).order_by(Offer.price_usd).first()

                            idata['offer_id'] = offer_single.id if offer_single else None
                            idata['item_name'] = exist_items.name

                            # cek ke core_organization_items... query org_id & item_id klo ada? quantity lu tambah.
                            exist_org_item = SharedLibraryItem.query.filter_by(
                                organization_id=data.organization_id,
                                item_id=int(idata['item id'])
                            ).all()

                            if exist_org_item:
                                self.update_quota_shared_lib(data, idata)
                            else:
                                self.create_new_shared_lib(data, idata)
                                self.update_catalog(data, idata)

                            self.add_new_categories(exist_items, data)
                            self.total_price.append(int(idata['total harga']))

                            extract_data = dict((k, v) for k, v in idata.iteritems() if v not in ['', ' '])
                            self.data_csv_report.append(extract_data)

                    extract_data = dict((k, v) for k, v in idata.iteritems() if v not in ['', ' '])
                    self.data_report.append(extract_data)

                order = self.create_new_order(data)
                for idata in self.data_csv_report:
                    self.create_new_orderlines(data, idata, order)

                self.create_new_payment(data, order)
                self.export_to_csv(data, app.config['BULK_ADD_CSV_TRANSACTION_SINGLE_ISSUE'], 'report-single')
                self.log_files(data, self.COMPLETE, None)
                self.update_solr()
            else:
                return data, 'File Not Exists'
            return data, 'OK'

        except Exception as e:
            pass

    def add_new_categories(self, exist_items, data):

        # add new categories on CategoryEperpusItem
        item_categories = [{'name': i.name, 'id': i.id} for i in exist_items.categories]

        for icat in item_categories:
            exist_category_items = CategoryEperpusItem.query.filter_by(
                category_id=icat['id'],
                organization_id=data.organization_id,
                item_id=exist_items.id).first()

            if not exist_category_items:
                new_categories = CategoryEperpusItem(
                    category_id=icat['id'],
                    organization_id=data.organization_id,
                    item_id=exist_items.id
                )
                db.session.add(new_categories)
                db.session.commit()

            child_org = Organization.query.filter_by(parent_organization_id=data.organization_id).all()
            all_org_id = [i.id for i in child_org + [data.organization]]

            categories = CategoryEperpus.query.filter(CategoryEperpus.organization_id.in_(all_org_id),
                                                      CategoryEperpus.category_id == icat['id']).first()

            if not categories:
                new_cat = CategoryEperpus(
                    organization_id=data.organization.id,
                    category_id=icat['id'],
                    name=icat['name'],
                    is_active=True
                )
                db.session.add(new_cat)
                db.session.commit()

        return 'OK'

    def create_new_payment(self, data, order):
        payment = Payment(
            order_id=order.id,
            user_id=data.organization_id,
            paymentgateway_id=order.paymentgateway_id,
            currency_code=order.currency_code,
            amount=order.final_amount,
            payment_status=PAYMENT_BILLED,
            is_active=True,
            payment_datetime=datetime.now()
        )
        db.session.add(payment)
        db.session.commit()

    def create_new_orderlines(self, data, idata, order):
        order_line = OrderLine(
            name=idata['item_name'],
            offer_id=idata['offer_id'],
            is_active=True,
            user_id=data.organization_id,
            quantity=int(idata['eksemplar']),
            orderline_status=OrderLine.Status.complete.value,
            currency_code='IDR',
            price=int(idata['harga']),
            final_price=int(idata['total harga']),
            order_id=order.id,
            localized_currency_code='IDR',
            localized_final_price=int(idata['total harga'])
        )
        db.session.add(order_line)
        db.session.commit()

    def create_new_order(self, data):
        order = Order(
            user_id=data.organization_id,
            order_number=generate_invoice(),
            client_id=67,
            is_active=True,
            paymentgateway_id=24,
            currency_code='IDR',
            platform_id=4,
            order_status=Order.Status.complete.value,
            final_amount=sum(self.total_price),
            total_amount=sum(self.total_price),
            point_reward=0
        )
        db.session.add(order)
        db.session.commit()
        return order

    def update_quota_shared_lib(self, data, idata):
        org_item = SharedLibraryItem.query.filter_by(
            organization_id=data.organization_id,
            item_id=int(idata['item id'])
        ).first()
        org_item.quantity += int(idata.get('eksemplar', 0))
        org_item.quantity_available += int(idata.get('eksemplar', 0))
        org_item.modified = datetime.now()
        db.session.add(org_item)
        db.session.commit()

    def create_new_shared_lib(self, data, idata, item_id=None):
        if not item_id: item_id = int(idata['item id'])

        exist_org_item = SharedLibraryItem.query.filter_by(organization_id=data.organization_id, item_id=item_id).first()
        if not exist_org_item:
            org_item = SharedLibraryItem(
                organization_id=data.organization_id,
                item_id=item_id,
                quantity=int(idata['eksemplar']),
                quantity_available=int(idata['eksemplar']),
                created=datetime.now(),
                modified=datetime.now()
            )
            db.session.add(org_item)
            db.session.commit()

    def find_items_subs(self, brand_id, csv_valid_from, csv_valid_to, offer_subs):

        if offer_subs.quantity_unit != OfferSubscription.QuantityUnit.unit.value:
            exist_items = Item.query.filter(
                Item.brand_id == brand_id,
                Item.release_date >= csv_valid_from,
                Item.release_date <= csv_valid_to,
                Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            ).order_by(desc(Item.id)).all()
            current_offer_stock, offer_stock, current_offer_type = 0, 0, 'duration'
        else:
            exist_items = Item.query.filter(
                Item.brand_id == brand_id,
                Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                Item.release_date >= csv_valid_from
            ).order_by(desc(Item.id)).limit(offer_subs.quantity).all()

            offer_stock, current_offer_type, current_offer_stock = offer_subs.quantity, 'edition', offer_subs.quantity - len(exist_items)
            if current_offer_stock <= 0: current_offer_stock = 0

        latest_added_item = exist_items[0].id if exist_items else None
        exist_items = [item.id for item in exist_items]

        return latest_added_item, offer_stock, current_offer_stock, exist_items, current_offer_type

    def check_organization_subs(self, brand_id, csv_valid_from, data, offer_subs):
        # HATE THIS SHIT,... OLD RECORD NOT HAVE OFFER ID FROM MARKETING
        old_old_record = SharedLibraryBrand.query.filter(
            SharedLibraryBrand.brand_id == brand_id,
            SharedLibraryBrand.valid_to > datetime.now(),
            SharedLibraryBrand.organization_id == data.organization_id,
            SharedLibraryBrand.note is not None
        ).all()

        if offer_subs.quantity_unit != OfferSubscription.QuantityUnit.unit.value:
            if offer_subs.quantity_unit == OfferSubscription.QuantityUnit.day.value:
                csv_valid_to = csv_valid_from + timedelta(days=offer_subs.quantity)
            if offer_subs.quantity_unit == OfferSubscription.QuantityUnit.week.value:
                csv_valid_to = csv_valid_from + timedelta(weeks=offer_subs.quantity)
            if offer_subs.quantity_unit == OfferSubscription.QuantityUnit.month.value:
                csv_valid_to = csv_valid_from + relativedelta(months=offer_subs.quantity)

            if not old_old_record:
                old_old_record = SharedLibraryBrand.query.filter(
                    SharedLibraryBrand.brand_id == brand_id,
                    SharedLibraryBrand.valid_to > csv_valid_from,
                    SharedLibraryBrand.organization_id == data.organization_id,
                    SharedLibraryBrand.note == None
                ).all()
        else:
            csv_valid_to = csv_valid_from
            if not old_old_record:
                old_old_record = SharedLibraryBrand.query.filter(
                    SharedLibraryBrand.brand_id == brand_id,
                    SharedLibraryBrand.current_offer_stock > 0,
                    SharedLibraryBrand.organization_id == data.organization_id,
                    SharedLibraryBrand.note == None
                ).all()

        return old_old_record, csv_valid_to

    def create_organization_brands(self, brand_id, csv_valid_from, csv_valid_to, current_offer_stock,
                                   current_offer_type, data, eksemplar, exist_offers, latest_added_item,
                                   offer_stock, shared_library):

        # core_organization_brands must accept multiple records.
        library_brand = SharedLibraryBrand(
            organization_id=data.organization_id,
            shared_library=shared_library,
            brand_id=brand_id,
            quantity=eksemplar,
            latest_added_item_id=latest_added_item,
            valid_from=csv_valid_from,
            valid_to=csv_valid_to,
            created=datetime.now(),
            modified=datetime.now(),
            offer_id=exist_offers.id,
            offer_stock=offer_stock,
            current_offer_stock=current_offer_stock,
            current_offer_type=current_offer_type
        )
        db.session.add(library_brand)
        db.session.commit()

        exist_catalog_brand = CatalogBrand.query.filter_by(
            catalog_id=data.catalog_id,
            shared_library_brand_org_id=data.organization_id,
            shared_library_brand_brand_id=brand_id
        ).first()

        if not exist_catalog_brand:
            # core_catalog_brands must record 1 only
            catalog_brand = CatalogBrand(
                catalog_id=data.catalog_id,
                shared_library_brand_org_id=data.organization_id,
                shared_library_brand_brand_id=brand_id)

            db.session.add(catalog_brand)
            db.session.commit()

    def update_catalog(self, data, idata, item_id=None):
        if not item_id: item_id = int(idata['item id'])
        exist_catalog_item = CatalogItem.query.filter_by(
            catalog_id=data.catalog.id,
            shared_library_item_org_id=data.organization.id,
            shared_library_item_item_id=item_id
        ).first()

        if not exist_catalog_item:
            catalog_item = CatalogItem(
                catalog_id=data.catalog.id,
                shared_library_item_org_id=data.organization.id,
                shared_library_item_item_id=item_id
            )
            db.session.add(catalog_item)
            db.session.commit()

    def add_subscription_transaction(self, data):
        try:
            self.log_files(data, self.PROCESS, None)
            csv_file = os.path.join(self.path_tmp, data.upload_file)

            if os.path.isfile(csv_file):
                reader = csv.reader(open(csv_file, 'rb'), delimiter=str(u',').encode('utf-8'))
                label_name = reader.next()

                if len(label_name) != len(app.config['BULK_ADD_CSV_TRANSACTION_SUBSCRIPTION_PURCHASE']):
                    reader = csv.reader(open(csv_file, 'rb'), delimiter=str(u';').encode('utf-8'))
                    label_name = reader.next()

                for b in reader:
                    data_label = [i for i in b]
                    rw = zip(label_name, data_label)
                    self.data_execute.append(dict(rw))

                for idata in self.data_execute:
                    idata['status'] = self.general_message

                    # check if..
                    offer_id = idata.get('offer id', '')
                    eksemplar = idata.get('eksemplar', '')
                    csv_valid_from = idata.get('tanggal mulai (yyyy-mm-dd hh:mm:ss)', '')

                    if offer_id in self.invalid_input: idata['status'] = 'Invalid Data Offer id'
                    idata = self.integer_format(offer_id, idata, 'offer id')

                    if eksemplar in self.invalid_input: idata['status'] = 'Invalid Data Eksemplar'
                    idata = self.integer_format(eksemplar, idata, 'eksemplar')

                    if csv_valid_from in self.invalid_input: idata['status'] = 'Invalid Data Tanggal mulai'

                    idata['harga'], idata['total harga'], idata['item_name'], idata['offer_id'] = 0, 1, 'unknown offer', offer_id
                    if idata.get('status', '') == self.general_message:
                        try:
                            reformat_date = datetime.strptime(csv_valid_from, '%Y-%m-%d %H:%M:%S')
                            csv_valid_from = reformat_date
                            date_threhold = datetime.now() - timedelta(days=self.threshold_days)
                            min_pub = datetime.combine(date_threhold, time.min)
                            if min_pub > reformat_date:
                                idata['status'] = 'Tanggal mulai tidak valid. Paling lambat 30 hari yang lalu'
                        except:
                            idata['status'] = 'Invalid Format Tanggal mulai format : YYYY-MM-DD HH:MM:SS'

                        exist_offers = Offer.query.filter_by(id=offer_id).first()
                        if not exist_offers:
                            idata['status'] = 'Invalid Offer ID not Founds'
                        else:
                            idata['item_name'] = exist_offers.long_name
                            if exist_offers.offer_type_id != SUBSCRIPTIONS:
                                idata['status'] = 'Offer tidak valid karena bukan offer langganan.'
                            # if exist_offers.offer_status != READY_FOR_SALE:
                            #     idata['status'] = 'Offer tidak valid status offer Tidak di jual.'

                        if exist_offers and idata.get('status', '') == self.general_message:
                            offer_subs = OfferSubscription.query.filter_by(offer_id=exist_offers.id).first()
                            brand_id = [ibrand.id for ibrand in exist_offers.brands][0] if exist_offers.brands else 0

                            old_old_record, csv_valid_to = self.check_organization_subs(brand_id, csv_valid_from, data, offer_subs)
                            if old_old_record: idata['status'] = 'Subscription masih Active'

                            if idata.get('status', '') == self.general_message:
                                final_price = exist_offers.price_idr * int(idata['eksemplar'])
                                idata['harga'] = int('%.2d' % final_price)
                                idata['total harga'] = int('%.2d' % final_price)
                                self.total_price.append(int('%.2d' % final_price))

                                shared_library = SharedLibrary.query.filter_by(id=data.organization_id).first()

                                # get all data related with brands.
                                latest_added_item, offer_stock, current_offer_stock, deliver_item, current_offer_type = \
                                    self.find_items_subs(brand_id, csv_valid_from, csv_valid_to, offer_subs)

                                # create core_organization_brands
                                self.create_organization_brands(brand_id, csv_valid_from, csv_valid_to,
                                    current_offer_stock, current_offer_type, data, eksemplar, exist_offers,
                                    latest_added_item, offer_stock, shared_library)

                                if deliver_item:
                                    for item_trans in deliver_item:
                                        exist_items = Item.query.filter_by(id=item_trans).first()
                                        self.create_new_shared_lib(data, idata, item_trans)
                                        self.update_catalog(data, idata, item_trans)
                                        self.add_new_categories(exist_items, data)

                                extract_data = dict((k, v) for k, v in idata.iteritems() if v not in ['', ' '])
                                self.data_csv_report.append(extract_data)

                    extract_data = dict((k, v) for k, v in idata.iteritems() if v not in ['', ' '])
                    self.data_report.append(extract_data)

                if self.data_csv_report:
                    order = self.create_new_order(data)
                    for idata in self.data_csv_report:
                        self.create_new_orderlines(data, idata, order)

                    self.create_new_payment(data, order)
                self.export_to_csv(data, app.config['BULK_ADD_CSV_TRANSACTION_SUBSCRIPTION_PURCHASE'],
                                   'report-subscription', 'subscription')
                self.log_files(data, self.COMPLETE, None)
                self.update_solr()
            else:
                return data, 'File Not Exists'
            return data, 'OK'

        except Exception as e:
            print 'Error', e

    def process(self):
        data_bulk = TransactionBulkUpload.query.filter_by(status=self.NEW).order_by(
            desc(TransactionBulkUpload.id)).limit(1).all()

        for data in data_bulk:
            data_file = os.path.join(app.config['BULK_TRANSACTION'], data.upload_file)
            if not os.path.exists(app.config['BULK_TRANSACTION']):
                os.makedirs(app.config['BULK_TRANSACTION'])

            if not os.path.isfile(data_file):
                bucket_file = os.path.join('eperpus', data.upload_type, data.upload_file)
                self.s3_server.download_file(app.config['BOTO3_GRAMEDIA_STATIC_BUCKET'], bucket_file, data_file)
            else:
                if data.upload_type == 'single':
                    self.add_single_transaction(data)
                elif data.upload_type == 'subscription':
                    self.add_subscription_transaction(data)
