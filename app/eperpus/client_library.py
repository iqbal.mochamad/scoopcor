import httplib, iso639

from flask import Blueprint, request, jsonify, url_for
from flask_appsfoundry.exceptions import BadRequest, InternalServerError, NotFound
from flask_restful import marshal

from app.auth.decorators import user_has_any_tokens
from app.auth.models import Client
from app.eperpus.catalogs import SharedCatalogItemDetailsViewModel, SharedCatalogListItemDetailsApiSerializer
from app.eperpus.helpers import organization_list_parent
from app.eperpus.libraries import SharedLibraryItem
from app.eperpus.models import CatalogItem, Catalog
from app.items.models import Item
from app.items.previews import preview_s3_covers
from app.users.organizations import Organization


blueprint = Blueprint('client-library', __name__, url_prefix='/v1')

# Settings for Kemendikbud!
KEMENDIKBUD_ACCESS = ['eperpusdikbud_client', 'eperpusdikbud_android', 'eperpusdikbud_ios']
KEMENDIKBUD_AGENT = 'eperpusdikbud_client'

# Settings for Acc.
ACC_DIGILIB_ACCESS = ['acc_resource_client', 'eperpus_acc_android', 'eperpus_acc_ios']
ACC_DIGILIB_AGENT = 'acc_resource_client'

# Add more deeplink here!!!
DEEPLINK_USERAGENT = {
    KEMENDIKBUD_AGENT: "kemendikbud://eperpus-item-detail?href=",
    ACC_DIGILIB_AGENT: "acc://eperpus-item-detail?href="
}

# Allowed Access
ALLOWED_ACCESS = KEMENDIKBUD_ACCESS + ACC_DIGILIB_ACCESS


def get_org_ids_by_user_agent(request):
    user_agent = request.headers.get('User-Agent').split('/')[0].lower().strip().replace(" ", "_")
    if user_agent not in ALLOWED_ACCESS:
        return user_agent, None
    else:
        # find existing organization by client user agent.
        exist_client = Client.query.filter_by(app_name=user_agent).first()
        exist_organization_by_client = Organization.query.filter(Organization.clients.any(id=exist_client.id)).first()
        all_organization_child = organization_list_parent(exist_organization_by_client)
        org_ids = [org.id for org in all_organization_child]
        return user_agent, org_ids


def validate_data_slug(slug):
    error_message = None
    user_agent, org_ids = get_org_ids_by_user_agent(request)
    if not org_ids: error_message = "Invalid UserAgent, {} not registered".format(user_agent)

    exist_items = Item.query.filter_by(slug=slug).first()
    if not exist_items: error_message = "Invalid Slug Identifier, Item not exist!."
    return error_message, exist_items, org_ids, user_agent


def shared_response(data_item, item, user_agent):
    # Just add another deeplink for another agent client if this api are used!
    deeplink_url = DEEPLINK_USERAGENT.get(user_agent)

    response_json = {
        "name": data_item.item.name,
        "url_deeplink": "{}{}".format(deeplink_url, url_for('.detail_client_items', slug=data_item.item.slug, _external=True)),
        "url_detail": url_for('.detail_client_library', slug=data_item.item.slug, _external=True),
        "authors": [author_.name for author_ in item.authors] if item.authors else [],
        "cover": preview_s3_covers(item, 'image_highres', is_full_path=True),
        "isbn": item.gtin13 if item.gtin13 else "-",
        "description": item.description if item.description else "-",
        "total_page": item.page_count if item.page_count else 0,
        "quantity_available": data_item.quantity_available,
        "languages": [iso639.find(lang_)['name'] for lang_ in item.languages] if item.languages else [],
        "vendor": item.brand.vendor.name if item.brand.vendor else "-",
        "slug": item.slug
    }
    return response_json


@blueprint.route('/eperpus/shared-library')
@user_has_any_tokens('can_read_write_global_all', 'can_read_eperpus_lib')
def search_client_library():
    '''
        this api for allowing publisher/client get our eperpus data item.
        for now only kemendikbud used, later will expand to other clients.

        For security purpose : do not display any ID (whatever id.) in response!!
    '''
    try:
        args = request.args.to_dict()
        keyword_search, limit, offset = args.get('search', None), int(args.get('limit', 20)), int(args.get('offset', 0))

        error_message = None
        user_agent, org_ids = get_org_ids_by_user_agent(request)
        if not org_ids: error_message = "Invalid UserAgent, {} not registered".format(user_agent)

        if error_message:
            return BadRequest(user_message=error_message, developer_message=error_message)

        # Get all item related by catalog share, by search or limit offset
        if keyword_search:
            item_catalog = SharedLibraryItem.query.filter(SharedLibraryItem.organization_id.in_(org_ids)).join(
                Item).filter(Item.name.ilike('%{}%'.format(keyword_search)))
        else:
            item_catalog = SharedLibraryItem.query.filter(SharedLibraryItem.organization_id.in_(org_ids))

        # Get all shared library catalog items
        item_catalog = item_catalog.order_by(SharedLibraryItem.quantity_available.desc(), SharedLibraryItem.item_id.desc())
        total_count = item_catalog.count()
        if limit:
            item_catalog = item_catalog.limit(limit).offset(offset).all()

        # build only response data needed by clients.
        response_data = []
        for data_item in item_catalog:
            response_json = shared_response(data_item=data_item, item=data_item.item, user_agent=user_agent)
            response_data.append(response_json)

        response = jsonify({
            'data': response_data,
            'metadata': {'resultset': {'offset': offset, 'limit': limit, 'count': total_count}}})

        response.status_code = httplib.OK
        return response

    except Exception as e:
        msg = "Oops, We are sorry our server is currently not available for now, please try again later!"
        return InternalServerError(user_message=msg, developer_message=msg)


@blueprint.route('/eperpus/shared-library/<slug>')
@user_has_any_tokens('can_read_write_global_all', 'can_read_eperpus_lib')
def detail_client_library(slug):

    error_message, exist_items, org_ids, user_agent = validate_data_slug(slug)
    if error_message: return BadRequest(user_message=error_message, developer_message=error_message)

    item_catalog = SharedLibraryItem.query.filter(SharedLibraryItem.organization_id.in_(org_ids),
                                                  SharedLibraryItem.item_id == exist_items.id).first()
    response_json = shared_response(data_item=item_catalog, item=exist_items, user_agent=user_agent)
    response = jsonify(response_json)
    response.status_code = httplib.OK
    return response


@blueprint.route('/eperpus/shared-item/<slug>')
@user_has_any_tokens('can_read_write_global_all', 'can_read_eperpus_lib', 'can_read_write_public', 'can_read_write_public_ext')
# @is_organization_member
def detail_client_items(slug):
    error_message, exist_items, org_ids, user_agent = validate_data_slug(slug)
    if error_message: return BadRequest(user_message=error_message, developer_message=error_message)

    # Retrieve exist catalog!
    exist_catalog = Catalog.query.filter(Catalog.organization_id.in_(org_ids), Catalog.is_internal_catalog==False).first()

    catalog_item = CatalogItem.query.filter(CatalogItem.catalog_id == exist_catalog.id,
        CatalogItem.shared_library_item_item_id == exist_items.id, CatalogItem.is_active == True).first()

    if not catalog_item: raise NotFound
    return jsonify(marshal(SharedCatalogItemDetailsViewModel(catalog_item), SharedCatalogListItemDetailsApiSerializer()))
