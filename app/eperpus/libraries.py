from __future__ import unicode_literals, absolute_import

import httplib
import json
import requests
from datetime import datetime, timedelta
from httplib import NO_CONTENT

from enum import Enum
from flask import Blueprint, request, jsonify, current_app, Response, g
from flask import make_response
from flask_appsfoundry import viewmodels, parsers, marshal
from flask_appsfoundry.exceptions import NotFound
from flask_appsfoundry.serializers import PaginatedSerializer, SerializerBase, fields
from humanize import naturalsize
from six import text_type
from sqlalchemy import desc
from sqlalchemy.dialects.postgresql import ENUM, JSONB
from marshmallow import fields as ma_fields
from werkzeug.routing import BaseConverter

from app import app, db, ma
from app.auth.decorators import user_is_authenticated
from app.auth.models import Client
from app.eperpus.schemas import ReportReviewSchema
from app.eperpus.schemas import UserReviewSchema
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.items.choices.review import REPORT_REASON_OPTION
from app.items.models import Item, Review, ReportReview
from app.items.previews import preview_s3_covers
from app.orders.deliver_items import send_auto_renewal_email
from app.orders.models import Order, OrderLine
from app.orders.choices import PAYMENT_IN_PROCESS
from app.helpers import generate_invoice
from app.payments.models import Payment
from app.users.organizations import Organization, is_organization_manager, only_child_organizations, \
    is_organization_member
from app.users.users import User
from app.utils.dicts import CorDefaultDict
from app.utils.models import get_url_for_model
from app.utils.serializers import SimpleImplicitHref, SimpleImplicitHrefWithId, RatingSerializer
from app.utils.strings import format_iso8601_duration
from app.utils.viewmodels import HrefAttribute, HrefAttributeWithId, RatingAttribute
from app.utils.marshmallow_helpers import schema_load

review_blueprint = Blueprint('eperpus_review', __name__, url_prefix='/v1')
blueprint = Blueprint('shared_library', __name__, url_prefix='/v1/organizations')


class SharedLibraryConverter(BaseConverter):

    def to_python(self, value):
        session = db.session()
        shared_library = session.query(SharedLibrary).get(value)
        if not shared_library:
            raise NotFound
        return shared_library

    def to_url(self, value):
        return text_type(value.id)


class ReviewConverter(BaseConverter):
    def to_python(self, value):
        session = db.session()
        review = session.query(Review).get(value)
        if not review:
            raise NotFound
        return review

    def to_url(self, value):
        return text_type(value.id)


app.url_map.converters['shared_library'] = SharedLibraryConverter
app.url_map.converters['review'] = ReviewConverter


class SharedLibrary(Organization):
    """ A type of `Organization` that `Users` can join and access `Item`s that are actually owned by an `Organization`.
    """
    __tablename__ = 'cas_shared_libraries'
    id = db.Column(db.Integer, db.ForeignKey('cas_organizations.id'), primary_key=True)
    user_concurrent_borrowing_limit = db.Column(db.Integer)
    reborrowing_cooldown = db.Column(db.Interval)
    borrowing_time_limit = db.Column(db.Interval)
    locked_message = db.Column(db.Text)
    public_library = db.Column(db.Boolean, default=False, nullable=False)
    book_borrowing_time_limit = db.Column(db.Interval)
    magazine_borrowing_time_limit = db.Column(db.Interval)
    newspaper_borrowing_time_limit = db.Column(db.Interval)
    disk_space = db.Column(db.Integer, nullable=True)

    __mapper_args__ = {
        'polymorphic_identity': 'shared library'
    }


class SharedLibraryBorrowingItem(db.Model):
    __tablename__ = 'cas_shared_libraries_borrowing_items'

    id = db.Column(db.Integer, primary_key=True)
    organization_id = db.Column(db.Integer, db.ForeignKey("cas_organizations.id"))
    catalog_id = db.Column(db.Integer, db.ForeignKey("core_catalogs.id"))
    item_id = db.Column(db.Integer, db.ForeignKey("core_items.id"))
    borrowing_time_limit = db.Column(db.Interval)


@blueprint.route('/items/update-solr', methods=['POST', ])
def update_solr_library_catalog_item():
    """ Delta import data Organization-items + catalog-items info to SOLR

    /v1/organizations/items/update-solr

    :return:
    """
    url = '{}/{}/dataimport'.format(
        app.config['SOLR_BASE_URL'],
        current_app.config['SOLR_SHAREDLIB_CORE'])
    requests.post(url, data={'command': 'delta-import', 'wt': 'json'})
    return make_response('', NO_CONTENT)


@blueprint.route('/<shared_library:entity>', methods=['LOCK', ])
@is_organization_manager
@only_child_organizations
def lock_child(entity):
    """ Temporarily disables a child `SharedLibrary`.

    :param `SharedLibrary` entity:
    """
    entity.locked_message = request.json['locked_message']

    return jsonify({'success_message': 'Organization locked'})


@blueprint.route('/<shared_library:entity>', methods=['UNLOCK', ])
@is_organization_manager
@only_child_organizations
def unlock_child(entity):
    """ Removes a temporary lock on a child `SharedLibrary`.

    :param `SharedLibrary` entity:
    """
    entity.locked_message = None

    return jsonify({'success_message': 'Organization unlocked'})


class SharedLibraryItem(db.Model):
    """ A record of `Item`s which are owned by a 'shared library' type `Organization`
    """
    __tablename__ = 'core_organization_items'

    organization_id = db.Column(db.Integer, db.ForeignKey("cas_organizations.id"), primary_key=True)
    shared_library = db.relationship(SharedLibrary, backref='items')
    item_id = db.Column(db.Integer, db.ForeignKey("core_items.id"), primary_key=True)
    item = db.relationship("Item")

    item_name_override = db.Column(db.String(250))

    quantity = db.Column(db.Integer, nullable=False)
    quantity_available = db.Column(db.Integer, nullable=False)

    is_active = db.Column(db.Boolean, nullable=False, default=True)

    created = db.Column(db.DateTime, default=datetime.utcnow)
    modified = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)


@blueprint.route('/<shared_library:entity>/items')
@is_organization_member
def search_items(entity):
    from app.eperpus.helpers import get_stock_holding_org

    stock_holding_org = get_stock_holding_org(current_org=entity)

    from .catalogs import get_catalog_items_search
    params = {'{query}, is_internal_content:false'.format(query=request.args.get('q'))}
    response_json = get_catalog_items_search(req=request, catalog_id=None, organization_id=stock_holding_org.id,
                                             q_param=params)

    facet_name_mapper = CorDefaultDict(category_names='item_categories', author_names='item_authors')

    facet_items = response_json['facet_counts']['facet_fields'].items() if 'facet_counts' in response_json \
        else []
    facet_values = []
    if facet_items:
        facet_values = [{"field_name": facet_name_mapper[key], "values": [
            {"value": t[0], "count": t[1]} for t in zip(facet[0::2], facet[1::2])
        ]} for key, facet in facet_items]

    spell_check = []
    if response_json.get('spellcheck', {'correctlySpelled': True})['correctlySpelled'] == 'false':
        suggestions = response_json['spellcheck']['suggestions'][1]['suggestion']
        spell_check = [{"value": data['word'], "count": data['freq']} for data in suggestions]

    items = []
    session = db.session()

    for doc in response_json.get('response', {}).get('docs', {}):
        item = session.query(SharedLibraryItem) \
            .filter_by(
            organization_id=stock_holding_org.id,
            item_id=doc.get('item_id')) \
            .first()
        if item:
            items.append(marshal(SharedLibraryListItemsViewModel(item), SharedLibraryItemListApiSerializer()))

    results = {
        'items': items,
        'metadata': {
            'resultset': {
                'offset': request.args.get('offset', type=int, default=0),
                'limit': request.args.get('limit', type=int, default=20),
                'count': response_json.get('response', {}).get('numFound', {})
            },
            "facets": facet_values,
            "spelling_suggestions": spell_check
        }
    }

    return jsonify(results)


@blueprint.route('/<shared_library:entity>/internal-items')
@is_organization_member
def search_internal_items(entity):
    from app.eperpus.helpers import get_stock_holding_org

    stock_holding_org = get_stock_holding_org(current_org=entity)

    q = request.args.get('q', None)
    limit = request.args.get('limit', type=int, default=25)
    offset = request.args.get('offset', type=int, default=0)

    session = db.session()

    all_items = session.query(SharedLibraryItem).join(Item).filter(Item.is_internal_content == True,
                                                                   SharedLibraryItem.organization_id == stock_holding_org.id)

    if q:
        q = "%s%s%s" % ('%', q, '%')
        all_items = all_items.filter(Item.name.ilike(q))

    items = []
    for item in all_items.limit(limit).offset(offset).all():
        items.append(marshal(SharedLibraryListItemsViewModel(item), SharedLibraryItemListApiSerializer()))

    results = {
        'items': items,
        'metadata': {
            'resultset': {
                'offset': offset,
                'limit': limit,
                'count': len(all_items.all()),
            }
        }
    }

    return jsonify(results)


@blueprint.route('/<shared_library:entity>/items/<item:item>')
@is_organization_member
def item_details(entity, item):
    """

    :param `SharedLibrary` entity:
    :param `app.items.models.Item` item:
    """
    # todo: simplify this by querying straight into the entity.items relationship
    s = db.session()

    model = s.query(SharedLibraryItem).filter_by(shared_library=entity, item=item, is_active=True).first()

    if not model:
        raise NotFound

    return jsonify(
        marshal(SharedLibraryListItemsViewModel(model), SharedLibraryItemListApiSerializer())
    )


class SharedLibraryBrand(db.Model):
    """ A record of `Brand`s which are owned by a 'shared library' type `Organization`
    """
    __tablename__ = 'core_organization_brands'

    class SharedOfferTypes(Enum):
        edition = 'edition'
        duration = 'duration'

    CURRENT_OFFER_TYPE = {
        'edition': 'edition',
        'duration': 'duration'
    }

    RENEWAL_STATUSES = {
        'new': 'new',
        'complete': 'complete',
        'fail': 'fail',
        'cancel': 'cancel'
    }

    id = db.Column(db.Integer, primary_key=True)

    organization_id = db.Column(db.Integer, db.ForeignKey("cas_shared_libraries.id"), nullable=False)
    shared_library = db.relationship(SharedLibrary, backref=db.backref('brands', lazy='dynamic'))

    brand_id = db.Column(db.Integer, db.ForeignKey("core_brands.id"), nullable=False)
    brand = db.relationship("Brand")

    quantity = db.Column(db.Integer, nullable=False)
    latest_added_item_id = db.Column(db.Integer, db.ForeignKey("core_items.id"), nullable=True)
    latest_added_item = db.relationship("Item")

    valid_from = db.Column(db.DateTime(timezone=True))
    valid_to = db.Column(db.DateTime(timezone=True))

    created = db.Column(db.DateTime, default=datetime.utcnow)
    modified = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    offer_id = db.Column(db.Integer, db.ForeignKey("core_offers.id"), nullable=True)
    offer = db.relationship("Offer")

    offer_stock = db.Column(db.Integer, nullable=False, default=0)
    current_offer_stock = db.Column(db.Integer, nullable=False, default=0)
    current_offer_type = db.Column(ENUM(*CURRENT_OFFER_TYPE.values(), name='current_offer_type'), nullable=False)
    note = db.Column(db.String(250))

    order_id = db.Column(db.Integer, db.ForeignKey("core_orders.id"), nullable=True)
    order = db.relationship("Order")

    is_renewal = db.Column(db.Boolean(), default=False)
    is_active = db.Column(db.Boolean(), default=True)
    renewal_note = db.Column(JSONB)
    renewal_status = db.Column(ENUM(*RENEWAL_STATUSES.values(), name='renewal_status_eperpus'), nullable=False)

    def get_edition_text(self):
        edition_text = self.offer.long_name.split('/')
        return edition_text[0]

    def get_display_name(self, loc='id'):
        display_offer = '{} {} {} {}'.format(self.brand.name,
                                             self.offer.get_display_offer_name(loc=loc),
                                             self.quantity,
                                             'copies' if loc != 'id' else 'eksemplar')
        return display_offer


@blueprint.route('/<shared_library:entity>/brands')
@is_organization_member
def search_brands(entity):
    """ Convenience endpoint:  Shows all the brands available through search_items.

    :param `SharedLibrary` entity:
    """
    vm = viewmodels.ListViewmodel(
        entity.brands,
        element_viewmodel=SharedLibraryListBrandsViewModel,
        limit=request.args.get('limit', 20),
        offset=request.args.get('offset', 0),
        ordering=parsers.SqlAlchemyOrderingParser().parse_args()['order'] or [SharedLibraryBrand.brand_id, ],
        filtering=SharedLibraryListBrandsApiArgsParser().parse_args(req=request)['filters']
    )

    serializer = PaginatedSerializer(SharedLibraryBrandListApiSerializer(), 'brands')

    return jsonify(marshal(vm, serializer))


class SharedLibraryRestrictedVendor(db.Model):
    """ in Item Purchase ePerpus Portal, certain ePerpus can only purchase from a list of specific vendors (restricted).
    This table stored list of vendor that allowed for that Library.

    used in : item search v3 API,
        for certain ePerpus (ex KG Smart), they only allowed to search item from specific vendors only
    """
    __tablename__ = 'cas_shared_libraries_restricted_vendors'

    organization_id = db.Column(db.Integer, db.ForeignKey("cas_shared_libraries.id"), primary_key=True, nullable=False)
    shared_library = db.relationship("SharedLibrary", backref='restricted_vendors')
    vendor_id = db.Column(db.Integer, db.ForeignKey("core_vendors.id"), primary_key=True, nullable=False)
    vendor = db.relationship("Vendor", backref='restricted_shared_libraries')


class SharedLibraryItemListApiSerializer(SerializerBase):
    id = fields.Integer
    title = fields.String
    currently_available = fields.Integer
    total_in_collection = fields.Integer
    details = fields.Nested(SimpleImplicitHrefWithId())
    cover_image = fields.Nested(SimpleImplicitHref())
    vendor = fields.Nested(SimpleImplicitHref())
    max_borrow_time = fields.String

    vendor_name = fields.String  # used for reporting eperpus portals
    category_name = fields.String  # used for reporting eperpus portals
    category_ids = fields.List(fields.Integer)
    isbn = fields.String  # used for reporting eperpus portals
    author_name = fields.String  # used for reporting eperpus portals
    is_internal_content = fields.Boolean
    file_size = fields.String
    rating = fields.Nested(RatingSerializer())


class SharedLibraryBrandListApiSerializer(SerializerBase):
    id = fields.Integer
    title = fields.String
    total_in_collection = fields.Integer
    details = fields.Nested(SimpleImplicitHref())
    vendor = fields.Nested(SimpleImplicitHref())
    max_borrow_time = fields.String


class SharedLibraryListItemsApiArgsParser(parsers.SqlAlchemyFilterParser):
    __model__ = SharedLibraryItem
    name = parsers.StringFilter()


class SharedLibraryListBrandsApiArgsParser(parsers.SqlAlchemyFilterParser):
    __model__ = SharedLibraryBrand
    name = parsers.StringFilter()


class SharedLibraryListItemsViewModel(viewmodels.SaViewmodelBase):

    def __init__(self, model_instance):
        super(SharedLibraryListItemsViewModel, self).__init__(model_instance)
        self.currently_available = self._model_instance.quantity_available
        self.total_in_collection = self._model_instance.quantity
        self.max_borrow_time = format_iso8601_duration(self._model_instance.shared_library.borrowing_time_limit)
        self.item = self.item
        self.rating = RatingAttribute(
            **self.avg_rating
        )

    @property
    def title(self):
        if self._model_instance.item_name_override:
            return self._model_instance.item_name_override
        else:
            return self._model_instance.item.name

    @property
    def id(self):
        # need to define like this since table CatalogItem didn't have Field ID
        return self._model_instance.item.id

    @property
    def details(self):
        return HrefAttributeWithId(
            id=self._model_instance.item.id,
            title='Detil Item',
            href=self._model_instance.item.api_url,
        )

    @property
    def cover_image(self):
        return HrefAttribute(
            title='Gambar Sampul',
            href=preview_s3_covers(self._model_instance.item, 'image_normal', True),
        )

    @property
    def vendor(self):
        return HrefAttribute(
            title=self._model_instance.item.brand.vendor.name,
            href=get_url_for_model(self._model_instance.item.brand.vendor)
        )

    @property
    def vendor_name(self):
        return self._model_instance.item.brand.vendor.name

    @property
    def isbn(self):
        return self._model_instance.item.gtin14 if self._model_instance.item.gtin14 else ' '

    @property
    def author_name(self):
        authors = self._model_instance.item.authors
        return '; '.join([i.name for i in authors])

    def search_categories_eperpus(self):
        from app.eperpus.categories import CategoryEperpus, CategoryEperpusItem

        category_item_eperpus = CategoryEperpusItem.query.filter_by(
            organization_id=self._model_instance.organization_id,
            item_id=self._model_instance.item.id).all()

        item_categories = [i.category_id for i in category_item_eperpus]

        categories_name = []
        categories_id = []

        for icat in item_categories:
            eperpus_categories = CategoryEperpus.query.filter_by(
                organization_id=self._model_instance.organization_id,
                category_id=icat
            ).first()
            if eperpus_categories:
                categories_id.append(eperpus_categories.category_id)
                categories_name.append(eperpus_categories.name)

        return categories_name, categories_id

    @property
    def category_name(self):
        categories_name, categories_id = self.search_categories_eperpus()
        return '; '.join(categories_name)

    @property
    def category_ids(self):
        categories_name, categories_id = self.search_categories_eperpus()
        return categories_id

    @property
    def is_internal_content(self):
        return self._model_instance.item.is_internal_content

    @property
    def file_size(self):
        file_size = self._model_instance.item.file_size if self._model_instance.item.file_size else 0
        return naturalsize(file_size) if file_size > 0 else '- MB'

    @property
    def avg_rating(self):
        from app.items.helpers import get_avg_and_total_rating
        rating_average, rating_count = get_avg_and_total_rating(self.item.id)
        return {"average": rating_average, "total": rating_count}

class SharedLibraryListBrandsViewModel(viewmodels.SaViewmodelBase):

    def __init__(self, model_instance):
        super(SharedLibraryListBrandsViewModel, self).__init__(model_instance)
        self.total_in_collection = self._model_instance.quantity
        self.max_borrow_time = format_iso8601_duration(self._model_instance.shared_library.borrowing_time_limit)

    title = property(lambda self: self._model_instance.brand.name)
    id = property(lambda self: self._model_instance.brand.id)

    @property
    def details(self):
        return HrefAttribute(
            title='Detil Brand',
            href=self._model_instance.brand.api_url,
        )

    @property
    def vendor(self):
        return HrefAttribute(
            title=self._model_instance.brand.vendor.name,
            href=get_url_for_model(self._model_instance.brand.vendor)
        )


def parse_timedelta(data, language="en"):
    day, month, year, msg = data.days, data.months, data.years, ''
    seconds, minutes, hours = data.seconds, data.minutes, data.hours

    loc_id = True if language == 'id' else False

    if hours and not (minutes or seconds or day):
        msg = '{} {}'.format(hours, 'jam' if loc_id else 'hours')
    elif hours and day:
        msg = '{} {}'.format(day * 24 + hours, 'jam' if loc_id else 'hours')
    elif minutes and hours:
        msg = '{} {}'.format(hours * 60 + minutes, 'menit' if loc_id else 'minutes')
    elif day and not (year or month):
        msg = '{} {}'.format(day, 'hari' if loc_id else 'days')
    elif month and year and not day:
        msg = '{} {}'.format(12 * year + month, 'bulan' if loc_id else 'months')
    elif month and not (year or day):
        msg = '{} {}'.format(month, 'bulan' if loc_id else 'months')
    elif year and not (month or day):
        msg = '{} {}'.format(year, 'tahun' if loc_id else 'years')

    return msg


@blueprint.route('/<shared_library:entity>/config', methods=['GET', ])
@is_organization_manager
def shared_library_config(entity):
    """
        retrive config of shared library relate with organizations
    """
    current_shared = SharedLibrary.query.filter_by(id=entity.id).first()
    return get_response_config(current_shared)


@blueprint.route('/mobile-config', methods=['GET', ])
def shared_library_mobile_config():
    user_agent = request.headers.get('user-agent', '').lower().split('/')[0].replace(' ', '_').strip()
    exist_user_agent = Client.query.filter_by(app_name=user_agent).first()

    if exist_user_agent:
        exist_organization_by_client = Organization.query.filter(Organization.clients.any(id=exist_user_agent.id)).first()
        current_shared = SharedLibrary.query.filter_by(id=exist_organization_by_client.id).first()
        return get_response_config(current_shared)
    else:
        response = jsonify({"error_message": "Organization not Found"})
        response.status_code = httplib.NOT_FOUND
        return response


def get_response_config(current_shared):
    from app.eperpus.helpers import current_internal_content_size, total_content_count
    language = request.headers.get('Accept-Language', 'en').lower()

    if language != ("id" or "en"):
        language = "en"

    if not current_shared:
        return NotFound

    current_disk_space_usage = current_internal_content_size(current_shared)
    disk_space = current_shared.disk_space if current_shared.disk_space else 0
    percentage = float(current_disk_space_usage) / float(
        disk_space) * 100 if current_disk_space_usage and disk_space > 0 else 0

    results = {
        "id": current_shared.id,
        "name": current_shared.name,
        "user_borrowing_limit": current_shared.user_concurrent_borrowing_limit,
        "general_borrowing_time": parse_timedelta(current_shared.borrowing_time_limit, language),
        "book_borrowing_time": parse_timedelta(current_shared.book_borrowing_time_limit, language),
        "magazine_borrowing_time": parse_timedelta(current_shared.magazine_borrowing_time_limit, language),
        "newspaper_borrowing_time": parse_timedelta(current_shared.newspaper_borrowing_time_limit, language),
        "disk_space": naturalsize(disk_space) if disk_space > 0 else '- MB',
        "current_disk_space_usage": naturalsize(current_disk_space_usage) if current_disk_space_usage else '- MB',
        "disk_space_percentage": round(percentage, 2),
        "content_count": total_content_count(current_shared),
        "internal_content_count": total_content_count(current_shared, is_internal=True)
    }
    return jsonify(results)


class SharedLibConfigSchema(ma.Schema):
    from marshmallow import fields

    user_borrowing_limit = fields.Integer(required=True)
    book_borrowing_time = fields.Str(required=True)
    magazine_borrowing_time = fields.Str(required=True)
    newspaper_borrowing_time = fields.Str(required=True)


@blueprint.route('/<shared_library:entity>/config', methods=['PUT', ])
@is_organization_manager
def shared_library_config_update(entity):
    from app.utils.marshmallow_helpers import schema_load

    """
        Update config of shared library relate with organizations
    """
    default_data = "7 days"
    current_shared = SharedLibrary.query.filter_by(id=entity.id).first()
    if not current_shared: return NotFound

    config_schema = SharedLibConfigSchema()
    data_schema = schema_load(schema=config_schema)

    # update data..
    current_shared.user_concurrent_borrowing_limit = data_schema.get('user_borrowing_limit', 1)
    current_shared.borrowing_time_limit = data_schema.get("general_borrowing_time", default_data)
    current_shared.book_borrowing_time_limit = data_schema.get('book_borrowing_time', default_data)
    current_shared.magazine_borrowing_time_limit = data_schema.get('magazine_borrowing_time', default_data)
    current_shared.newspaper_borrowing_time_limit = data_schema.get('newspaper_borrowing_time', default_data)
    db.session.add(current_shared)
    db.session.commit()

    results = {
        "id": current_shared.id,
        "name": current_shared.name,
        "user_borrowing_limit": current_shared.user_concurrent_borrowing_limit,
        "general_borrowing_time": parse_timedelta(current_shared.borrowing_time_limit),
        "book_borrowing_time": parse_timedelta(current_shared.book_borrowing_time_limit),
        "magazine_borrowing_time": parse_timedelta(current_shared.magazine_borrowing_time_limit),
        "newspaper_borrowing_time": parse_timedelta(current_shared.newspaper_borrowing_time_limit)
    }
    return jsonify(results)


@blueprint.route('/<organization:entity>/subscriptions', methods=['GET', ])
@is_organization_manager
def shared_library_subs(entity):
    from app.eperpus.helpers import organization_list_parent
    from app.master.models import Brand

    args = request.args.to_dict()
    q, limit, offset = args.get('q', None), args.get('limit', 25), args.get('offset', 0)

    organization_id = [i.id for i in organization_list_parent(entity)]
    shared_lib = SharedLibraryBrand.query.filter(
        SharedLibraryBrand.organization_id.in_(organization_id),
        SharedLibraryBrand.is_active == True
    ).order_by(desc(SharedLibraryBrand.valid_to))
    total_count = len(shared_lib.all())

    if q:
        q = "%s%s%s" % ('%', q, '%')
        brands = Brand.query.filter(Brand.name.ilike(q)).all()
        brand_id = [i.id for i in brands]
        shared_lib = shared_lib.filter(SharedLibraryBrand.brand_id.in_(brand_id))

    if limit:
        shared_lib = shared_lib.limit(limit).offset(offset)

    metadata, resultset = {}, {}
    resultset['count'] = total_count
    resultset['limit'] = int(limit) if limit else 0
    resultset['offset'] = offset
    metadata['resultset'] = resultset

    data_borrowed = [
        {
            "id": idata.id,
            "name": idata.brand.name,
            "quantity": idata.quantity,
            "valid_to": idata.valid_to.isoformat() if idata.valid_to else "",
            "is_active": idata.valid_to > datetime.now(
                tz=idata.valid_to.tzinfo) if idata.current_offer_type == 'duration'
            else idata.current_offer_stock > 0,
            "brand_type": idata.brand.default_item_type.title() if idata.brand.default_item_type else "",
            "subscription_type": idata.current_offer_type.title(),
            "current_stock": idata.current_offer_stock,
            "is_renewal":  idata.is_renewal,
            "display_offer_name": idata.get_display_name()
        } for idata in shared_lib
    ]

    data_borrowed = sorted(data_borrowed, key=lambda i: i['is_active'], reverse=True)
    return Response(json.dumps({'data': data_borrowed, 'metadata': metadata}), status=httplib.OK,
                    mimetype='application/json')


@review_blueprint.route('/reviews', methods=['POST', ])
@user_is_authenticated
def add_review():
    from app.utils.marshmallow_helpers import schema_load
    from app.eperpus.members import UserBorrowedItem
    review_schema = UserReviewSchema()
    request_data = schema_load(review_schema)
    user_id = g.user.user_id
    current_borrow = UserBorrowedItem.query.filter_by(id=request_data.pop('borrowing_id'), user_id=user_id).first()
    if current_borrow is not None:
        past_review = Review.query.filter_by(user_id=user_id, item_id=current_borrow.catalog_item_item_id).first()
        if past_review is not None:
            return Response(json.dumps({'message': 'Anda sudah pernah memberikan ulasan.'}),
                            status=httplib.CONFLICT, mimetype='application/json')
        review = Review(user_id=user_id, item_id=current_borrow.catalog_item_item_id, **request_data)
        db.session.add(review)
        db.session.commit()
        return Response(json.dumps({'message': 'Ulasan berhasil dikirim'}),
                        status=httplib.CREATED, mimetype='application/json')
    return Response(json.dumps({'message': 'Ulasan tidak berhasil dikirim. Silakan coba beberapa saat lagi.'}),
                    status=httplib.BAD_REQUEST, mimetype='application/json')


@review_blueprint.route('/report-reason', methods=['GET'])
@user_is_authenticated
def report_reason():
    response = []
    for item, value in REPORT_REASON_OPTION.iteritems():
        response.append(dict(value=item, name=value))
    return Response(json.dumps(response), status=httplib.OK, mimetype='application/json')


@review_blueprint.route('/reviews/<review:review>/report', methods=['POST'])
@user_is_authenticated
def report_review(review):
    from app.utils.marshmallow_helpers import schema_load
    report_schema = ReportReviewSchema()
    request_data = schema_load(report_schema)
    user_id = g.user.user_id
    past_report = ReportReview.query.filter_by(user_id=user_id, review_id=review.id).first()
    if past_report is not None:
        return Response(json.dumps({'message': 'Anda sudah pernah melaporkan ulasan ini.'}),
                        status=httplib.CONFLICT, mimetype='application/json')
    report = ReportReview(user_id=user_id, review_id=review.id, **request_data)
    db.session.add(report)
    db.session.commit()
    return Response(json.dumps({'message': 'Laporan berhasil dikirim'}),
                    status=httplib.CREATED, mimetype='application/json')


@blueprint.route('/items/subscriptions', methods=['GET'])
@user_is_authenticated
def subscription_items_list():
    from app.eperpus.helpers import build_item_reponse
    from app.utils.redis_cache import RedisCache

    redis_key = 'eperpus:items:subscription'

    from_cache = request.headers.get('Cache-Control', None)

    item_type = request.args.get('item_type', None)

    extra_query = ''

    if item_type in ['newspaper', 'magazine']:
        redis_key = '{redis_key}:{item_type}'.format(redis_key=redis_key, item_type=item_type)
        extra_query = "and core_items.item_type = '{item_type}'".format(item_type=item_type)

    sql_query = """
        select distinct on (core_offers_brands.brand_id) core_items.id as latest_item_id, core_items.name
        from core_offers_brands
            join core_offers on core_offers_brands.offer_id = core_offers.id
            join core_items on core_items.brand_id = core_offers_brands.brand_id
        where core_offers.offer_type_id = 2
            and core_offers.offer_status = 7
            and core_items.item_status = 'ready for consume'
            {extra_query}
        order by core_offers_brands.brand_id desc, core_items.id desc;
    """.format(extra_query=extra_query)

    cache = RedisCache(current_app.kvs)

    cache_response = cache.get(redis_key)

    if cache_response and not from_cache == 'no-cache':
        return Response(json.dumps(json.loads(cache_response)), status=httplib.OK, mimetype='application/json')

    cache.delete(redis_key)

    result = []
    subs_items = db.engine.execute(sql_query)
    data = subs_items.fetchall()
    for item in data:
        item_id, item_name = item
        item = Item.query.get(item_id)
        result.append(build_item_reponse(item))
    cache.write(redis_key, result)
    return Response(json.dumps(result), status=httplib.OK, mimetype='application/json')


class ReportLogEperpus(db.Model):
    __tablename__ = 'core_report_data_eperpus'
    id = db.Column(db.Integer, primary_key=True)

    organization_id = db.Column(db.Integer, db.ForeignKey("cas_organizations.id"), nullable=False)

    item_id = db.Column(db.Integer, db.ForeignKey("core_items.id"))
    item = db.relationship("Item")

    latest_quantity_available = db.Column(db.Integer, nullable=False)

    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'))
    user = db.relationship(User, backref='core_report_data_eperpus')

    created = db.Column(db.DateTime, default=datetime.utcnow)
    modified = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def response_data(self):
        return {
            "id": self.id,
            "item_name": self.item.name,
            "latest_quantity": self.latest_quantity_available,
            "pic": self.user.username
        }


@blueprint.route('/<organization:entity>/delete/<item_id>', methods=['DELETE'])
@is_organization_manager
def remove_item_eperpus(entity, item_id):
    from app.eperpus.models import CatalogItem
    from app.eperpus.members import UserBorrowedItem
    from app.eperpus.categories import CategoryEperpusItem
    from app.eperpus.models import WatchList

    # delete item in eperpus
    def delete_item(data):
        try:
            db.session.delete(data)
            db.session.commit()
        except:
            db.session.rollback()

    # check from core_organization_items
    exist_org_items = SharedLibraryItem.query.filter_by(item_id=item_id, organization_id=entity.id).all()

    # check from core_catalog_items
    exist_catalog_items = CatalogItem.query.filter_by(shared_library_item_item_id=item_id,
                                                      shared_library_item_org_id=entity.id).all()

    # check from core_user_borrowed_items
    exist_user_borrowed_items = UserBorrowedItem.query.filter_by(catalog_item_item_id=item_id,
                                                                 catalog_item_org_id=entity.id).all()

    # check from core_eperpus_items_categories
    exist_item_category = CategoryEperpusItem.query.filter_by(item_id=item_id, organization_id=entity.id).all()

    # check watchlist
    exist_watchlist = WatchList.query.filter_by(item_id=item_id, stock_org_id=entity.id).all()

    if not exist_org_items:
        raise NotFound

    for exist_item in exist_item_category:
        delete_item(data=exist_item)

    for exist_item in exist_user_borrowed_items:
        delete_item(data=exist_item)

    for exist_item in exist_catalog_items:
        delete_item(data=exist_item)

    for exist_watch in exist_watchlist:
        delete_item(exist_watch)

    item_name = 'Unknown'
    for exist_item in exist_org_items:
        item_name = exist_item.item.name
        # record pic who deleted records
        log_eperpus = ReportLogEperpus(organization_id=entity.id, user_id=g.current_user.id,
                                       item_id=item_id, latest_quantity_available=exist_item.quantity)
        db.session.add(log_eperpus)
        db.session.commit()
        delete_item(data=exist_item)

    return Response(json.dumps({'message': 'Produk {} berhasil dihapus'.format(item_name)}),
                    status=httplib.OK, mimetype='application/json')


@blueprint.route('/<organization:entity>/remove-history', methods=['GET'])
@is_organization_manager
def list_deleted_item(entity):
    args = request.args.to_dict()
    q, limit, offset = args.get('q', None), args.get('limit', 25), args.get('offset', 0)
    exist_history = ReportLogEperpus.query.filter_by(organization_id=entity.id)

    total_count = exist_history.count()
    data_history = exist_history.order_by(desc(ReportLogEperpus.created)).limit(limit).offset(offset).all()

    rebuild = {
        "history": [data.response_data() for data in data_history],
        'metadata': {
            'resultset': {
                'offset': offset,
                'limit': limit,
                'count': total_count
            },
        }
    }
    data = jsonify(rebuild)
    data.status_code = httplib.OK
    return data


@blueprint.route('/<organization:entity>/detail/<db_id>', methods=['GET'])
@is_organization_manager
def detail_deleted_item(entity, db_id):
    exist_history = ReportLogEperpus.query.filter_by(organization_id=entity.id,id=db_id).first()
    data = jsonify(exist_history.response_data())
    data.status_code = httplib.OK
    return data


class RenewalSchema(ma.Schema):
    id = ma_fields.Integer(required=True)
    is_renewal = ma_fields.Bool(load_only=True, required=True)


@blueprint.route('/<organization:entity>/renewal', methods=['POST', ])
@is_organization_manager
def auto_renewal_eperpus(entity):
    renewal_schema = schema_load(schema=RenewalSchema())

    org_brand_id = renewal_schema.get('id', None)
    is_renewal_status = renewal_schema.get('is_renewal', False)

    exist_org_brand = SharedLibraryBrand.query.filter_by(id=org_brand_id, organization_id=entity.id).first()
    if not exist_org_brand:
        raise NotFound

    if exist_org_brand:
        user_data = g.current_user if g.current_user else None

        pic_record = {}
        if exist_org_brand.renewal_note:
            for keys, values in exist_org_brand.renewal_note.iteritems():
                pic_record[keys] = values

        if is_renewal_status:
            pic_record.update({'active': 'Activate by {} at {}'.format(user_data.email, datetime.now().isoformat()),
                               'user_id_activate': user_data.id})
        else:
            pic_record.update({'inactive': 'Inactivate by {} at {}'.format(user_data.email, datetime.now().isoformat()),
                               'user_id_inactivate': user_data.id})

        exist_org_brand.is_renewal = is_renewal_status
        exist_org_brand.renewal_status = 'new' if is_renewal_status else 'cancel'
        exist_org_brand.renewal_note = pic_record
        db.session.add(exist_org_brand)
        db.session.commit()

    try:
        send_auto_renewal_email(exist_org_brand=exist_org_brand, is_renewal=is_renewal_status)
    except:
        pass

    response = jsonify({"status": "Renewal Updated"})
    response.status_code = httplib.CREATED
    return response


def process_renewal_eperpus(exist_renewal_eperpus):
    new_order = Order(
        order_number=generate_invoice(),
        total_amount=exist_renewal_eperpus.offer.price_idr,
        final_amount=exist_renewal_eperpus.offer.price_idr,
        user_id=exist_renewal_eperpus.organization_id,
        client_id=67,
        order_status=PAYMENT_IN_PROCESS,
        is_active=True,
        point_reward=0,
        currency_code="IDR",
        paymentgateway_id=24,
        platform_id=4,
        is_renewal=True
    )
    db.session.add(new_order)
    db.session.commit()

    new_orderlines = OrderLine(
        name=exist_renewal_eperpus.offer.long_name,
        offer_id=exist_renewal_eperpus.offer.id,
        is_active=True,
        is_free=False,
        is_discount=False,
        user_id=new_order.user_id,
        order_id=new_order.id,
        quantity=1,
        orderline_status=PAYMENT_IN_PROCESS,
        currency_code=new_order.currency_code,
        price=new_order.final_amount,
        final_price=new_order.final_amount,
        localized_currency_code='IDR',
        localized_final_price=new_order.final_amount,
        is_trial=False
    )
    db.session.add(new_orderlines)
    db.session.commit()

    new_payment = Payment(
        order_id=new_order.id,
        user_id=new_order.user_id,
        paymentgateway_id=new_order.paymentgateway_id,
        currency_code=new_order.currency_code,
        amount=new_order.final_amount,
        payment_status=PAYMENT_IN_PROCESS,
        is_active=True,
        is_test_payment=False,
        is_trial=False,
        payment_datetime=datetime.now(),
        financial_archive_date=None,
        merchant_params=None
    )
    db.session.add(new_payment)
    db.session.commit()
    return new_order, new_orderlines, new_payment


def process_wallet_eperpus(exist_renewal_eperpus, new_order):
    from app.users.wallet import Wallet, WalletTransaction
    from decimal import Decimal

    # cut wallet
    amount_update = -1 * new_order.final_amount
    exist_wallet = Wallet.query.filter_by(party_id=exist_renewal_eperpus.organization_id).first()
    exist_wallet.balance = exist_wallet.balance - Decimal(new_order.final_amount)

    # create transaction wallet di wallet_transactions tabel , CREATED BY IS USER WHO ENABLE AND DISABLE
    wallet_trx = WalletTransaction(wallet=exist_wallet,
                                   amount=amount_update,
                                   created_by=exist_renewal_eperpus.renewal_note['user_id_activate'],
                                   order_id=new_order.id)
    return exist_wallet, wallet_trx


def create_new_org_brand_renewal(exist_renewal_eperpus, new_order):
    from dateutil.relativedelta import relativedelta
    from app.offers.models import OfferSubscription

    # find shared library and offer subs
    shared_library = SharedLibrary.query.filter_by(id=exist_renewal_eperpus.organization_id).first()
    offer_subs = OfferSubscription.query.filter_by(offer_id=exist_renewal_eperpus.offer_id).first()

    valid_from = datetime.now().replace(microsecond=0, hour=0, minute=0, second=0)
    new_valid_from = (exist_renewal_eperpus.valid_to + timedelta(days=1)).replace(
        microsecond=0, hour=0, minute=0, second=0)
    valid_to = datetime.now().replace(microsecond=0, hour=0, minute=0, second=0)

    if offer_subs.quantity_unit == OfferSubscription.QuantityUnit.unit.value:
        valid_to = valid_to
    if offer_subs.quantity_unit == OfferSubscription.QuantityUnit.day.value:
        valid_to = new_valid_from + timedelta(days=offer_subs.quantity)
    if offer_subs.quantity_unit == OfferSubscription.QuantityUnit.week.value:
        valid_to = new_valid_from + timedelta(weeks=offer_subs.quantity)
    if offer_subs.quantity_unit == OfferSubscription.QuantityUnit.month.value:
        valid_to = new_valid_from + relativedelta(months=offer_subs.quantity)

    offer_stock = offer_subs.quantity if offer_subs.quantity_unit == OfferSubscription.QuantityUnit.unit.value else 0
    offer_type = 'edition' if offer_subs.quantity_unit == OfferSubscription.QuantityUnit.unit.value else 'duration'

    # CREATE NEW ORGANIZATION BRAND
    new_lib_brand = SharedLibraryBrand(
        organization_id=exist_renewal_eperpus.organization_id,
        shared_library=shared_library,
        brand_id=exist_renewal_eperpus.brand_id,
        quantity=exist_renewal_eperpus.quantity,
        latest_added_item_id=exist_renewal_eperpus.latest_added_item_id,
        valid_from=valid_from,
        valid_to=valid_to,
        created=datetime.now(),
        modified=datetime.now(),
        offer_id=exist_renewal_eperpus.offer_id,
        offer_stock=offer_stock,
        current_offer_stock=offer_stock,
        current_offer_type=offer_type,
        order_id=new_order.id,
        is_renewal=True,
        renewal_note=exist_renewal_eperpus.renewal_note,
        is_active=True,
        renewal_status='new'
    )
    return new_lib_brand


def transaction_renewal_note(exist_renewal_eperpus, renewal_note, renewal_status,is_active):
    if exist_renewal_eperpus.renewal_note:
        tmp = {}
        for keys, values in exist_renewal_eperpus.renewal_note.iteritems():
            tmp[keys] = values
        tmp.update(renewal_note)
        renewal_note = tmp

    exist_renewal_eperpus.is_renewal = False
    exist_renewal_eperpus.is_active = is_active
    exist_renewal_eperpus.renewal_status = renewal_status
    exist_renewal_eperpus.renewal_note = renewal_note
    db.session.add(exist_renewal_eperpus)
    db.session.commit()
