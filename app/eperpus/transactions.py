import csv
from datetime import datetime, timedelta, date

from flask import Blueprint, jsonify, request, send_file, g
from flask_appsfoundry.exceptions import BadRequest, NotFound
from httplib import OK
import requests
import io
import os

from sqlalchemy import desc
from unidecode import unidecode

from app import db, app, ma
from app.eperpus.libraries import SharedLibraryBrand
from app.eperpus.models import Catalog
from app.orders.choices import COMPLETE
from app.orders.models import Order, OrderLine
from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files
from app.users.organizations import is_organization_manager, Organization
from app.users.wallet import WalletTransaction
from app.users.users import User
from app.utils.marshmallow_helpers import schema_load

blueprint = Blueprint('eperpus_record_transactions', __name__, url_prefix='/v1')


class TransactionBulkUpload(db.Model):
    __tablename__ = 'core_eperpus_transactions'
    id = db.Column(db.Integer, primary_key=True)

    organization_id = db.Column(db.Integer, db.ForeignKey('cas_organizations.id'))
    organization = db.relationship(Organization)

    catalog_id = db.Column(db.Integer, db.ForeignKey('core_catalogs.id'))
    catalog = db.relationship(Catalog)

    upload_date = db.Column(db.DateTime(timezone=True))
    upload_file = db.Column(db.String(250))

    complete_file = db.Column(db.String(250))
    status = db.Column(db.Integer)
    error_status = db.Column(db.String(250))
    upload_type = db.Column(db.String(100))

    created_by = db.Column(db.Integer, db.ForeignKey('cas_users.id'))
    created = db.relationship(User, backref='core_eperpus_transactions')

    def values(self):
        aws_bucket = 'https://s3-ap-southeast-1.amazonaws.com/ebook-statics/eperpus'
        if self.complete_file:
            path_complete = os.path.join(aws_bucket, self.upload_type, self.complete_file if self.complete_file else '')
        else:
            path_complete = None

        return {
            'id': self.id,
            'upload_file': self.upload_file,
            'organization': {'id': self.organization.id, 'name': self.organization.name},
            'catalog': {'id': self.catalog.id, 'name': self.catalog.name},
            'complete_file': self.complete_file,
            'upload_date': self.upload_date.isoformat(),
            'download_complete_file': path_complete,
            'created_by': self.created.email if self.created_by else '-',
            'download_upload_file': os.path.join(aws_bucket, self.upload_type, self.upload_file if self.upload_file else '')
        }


@blueprint.route('/organizations/<organization:entity>/transactions/<type>', methods=['GET', 'POST',])
@is_organization_manager
def upload_single_purchase(entity, type):

    if request.method == 'GET':
        args = request.args.to_dict()
        limit = args.get('limit', 20)
        offset = args.get('offset', 0)

        data_upload = TransactionBulkUpload.query.filter_by(organization_id=entity.id, upload_type=type).order_by(
            desc(TransactionBulkUpload.upload_date))
        total_count = len(data_upload.all())
        if limit:
            data_upload = data_upload.limit(limit).offset(offset)

        data = [i.values() for i in data_upload.all()]

        return jsonify({
            'data': data,
            'metadata': {
                'resultset': {
                    'count': total_count,
                    'limit': limit,
                    'offset': offset
                }
            }
        })

    if request.method == 'POST':
        EPERPUS_PURCHASE_DIR, ALLOWED_EXTENSIONS = app.config['BULK_TRANSACTION'], ['csv']

        if type == 'single':
            csv_headers = app.config['BULK_ADD_CSV_TRANSACTION_SINGLE_ISSUE']
        elif type == 'subscription':
            csv_headers = app.config['BULK_ADD_CSV_TRANSACTION_SUBSCRIPTION_PURCHASE']

        catalog_id = request.form.get('catalog_id', type=int)

        # Validasi apakah file ada atau tidak
        if 'file' not in request.files:
            raise BadRequest(user_message='Upload file is required!',
                             developer_message='Upload file is required!, missing file')

        # Validasi apakah ekstensi file sudah sesuai
        file = request.files['file']
        extension = os.path.basename(file.filename).split('.')[-1:][0].lower()
        if extension not in ALLOWED_EXTENSIONS:
            raise BadRequest(user_message='Upload failed. Please re-upload a valid csv file.',
                             developer_message='Upload failed. Please re-upload a valid csv file.')

        # Membuat direktori dan simpan file ke drive
        if not os.path.exists(EPERPUS_PURCHASE_DIR):
            os.makedirs(EPERPUS_PURCHASE_DIR, 0777)

        new_filename = 'transaction-{}-{}.csv'.format(
            datetime.strftime(datetime.now(), '%Y%m%d'), datetime.now().microsecond).lower()

        csv_files = os.path.join(EPERPUS_PURCHASE_DIR, new_filename)
        file.save(csv_files)

        # Check file formating header if same
        label_valid = []
        if os.path.isfile(csv_files):
            reader = csv.reader(open(csv_files, 'rb'), delimiter=str(u',').encode('utf-8'))
            raw_label = reader.next()

            if len(raw_label) != len(csv_headers):
                reader = csv.reader(open(csv_files, 'rb'), delimiter=str(u';').encode('utf-8'))
                raw_label = reader.next()

            for ilabel in csv_headers:
                if str(ilabel) not in raw_label:
                    label_valid.append(False)

            if False in label_valid:
                os.remove(csv_files)
                raise BadRequest(
                    user_message='Upload failed. Please use the csv template provided.',
                    developer_message='Upload failed. Please use the csv template provided.')
            else:
                new_data = TransactionBulkUpload(
                    upload_file=new_filename,
                    organization_id=entity.id,
                    catalog_id=int(catalog_id),
                    upload_date=datetime.now(),
                    created_by=g.current_user.id,
                    status=1,
                    upload_type=type
                )

                db.session.add(new_data)
                db.session.commit()

                # Send to aws static
                s3_server = connect_gramedia_s3()
                bucket_name = app.config['BOTO3_GRAMEDIA_STATIC_BUCKET']

                aws_folder = aws_check_files(s3_server, bucket_name, '{}/{}/'.format('eperpus', type))
                if not aws_folder:
                    s3_server.put_object(Bucket=bucket_name, Key='{}/{}/'.format('eperpus', type), ACL='public-read')

                s3_server.upload_file(csv_files, bucket_name, '{}/{}/{}'.format('eperpus', type, new_filename),
                                      ExtraArgs={'ContentType': 'binary/octet-stream', 'ACL': 'public-read'})

        return jsonify({'message': 'upload file successfully..'}), OK


@blueprint.route('/organizations/<organization:entity>/transactions', methods=['GET', ])
@is_organization_manager
def list_history_transaction(entity):
    args = request.args.to_dict()
    limit, offset, q, data = args.get('limit', 20), args.get('offset', 0), args.get('q', None),[]

    start_date = args.get('start_date', date.today() - timedelta(days=90))
    end_date = args.get('end_date', date.today())

    data_transaction = Order.query.filter(Order.user_id == entity.id, Order.order_status == COMPLETE,
                                          Order.created >= start_date, Order.created <= end_date).order_by(desc('id'))

    total_count = data_transaction.count()
    if limit:data_transaction = data_transaction.limit(limit).offset(offset)
    data_transaction = data_transaction.all()

    for transaction in data_transaction:
        temp, total_orderlines = tmp_response(transaction)
        data.append(temp)

    metadata = {"resultset": {'count': total_count, 'limit': int(limit), 'offset': int(offset)}}
    data_respond = jsonify({'data': data, 'metadata': metadata})
    data_respond.status_code = OK
    return data_respond


@blueprint.route('/organizations/<organization:entity>/transactions/<int:order_id>', methods=['GET', ])
@is_organization_manager
def detail_history_transaction(entity, order_id):
    args = request.args.to_dict()
    limit, offset, q,data = args.get('limit', 20), args.get('offset', 0), args.get('q', None), {}
    data_transaction = Order.query.filter_by(user_id=entity.id, id=order_id, order_status=COMPLETE).all()
    exist_transaction = OrderLine.query.filter_by(user_id=entity.id, order_id=order_id, orderline_status=COMPLETE).all()

    if data_transaction and exist_transaction:
        for transaction in data_transaction:
            temp, total_orderlines = tmp_response(transaction)
            data = temp

        data['details'] = [{'item_name': unidecode(unicode(detail.name)), 'item_price': '%.2f' % detail.price,
                            'item_quantity': detail.quantity,'sub_total': '%.2f' % detail.final_price
                            } for detail in exist_transaction]
    else:
        raise NotFound(
            user_message="Halaman tidak ditemukan",
            developer_message="Halaman tidak ditemukan"
        )

    data_respond = jsonify(data)
    data_respond.status_code = OK
    return data_respond


@blueprint.route('/organizations/<organization:entity>/download-transactions-history', methods=['GET', ])
def download_history_transaction(entity):
    report_name = 'history-transaction'
    args = request.args.to_dict()

    start_date = args.get('start_date', date.today() - timedelta(days=90))
    end_date = args.get('end_date', date.today())

    start_date = datetime.date(datetime.strptime(start_date.replace('\"', '').split('T')[0], '%Y-%m-%d'))
    end_date = datetime.date(datetime.strptime(end_date.replace('\"', '').split('T')[0], '%Y-%m-%d'))

    identity = '&start_date={start_date}&end_date={end_date}&organization_id={org_id}&j_username={user}&' \
               'j_password={password}&output=xlsx'.format(start_date=start_date, end_date=end_date,
                                                          org_id=entity.id, user=app.config['DW_JASPER_USER'],
                                                          password=app.config['DW_JASPER_PASS'])

    query_params = '_flowId=viewReportFlow&_flowId=viewReportFlow&&reportUnit=/EPerpus/finance_report_eperpus&'

    download_url = "{base_url}{query_params}{identity}".format(
        base_url=app.config['DW_JASPER_URL'], query_params=query_params, identity=identity)

    output = io.BytesIO()
    resp = requests.get(download_url, stream=True)

    output.write(resp.content)
    output.seek(0)

    filename = "{filename}-{org_id}-{date}.xlsx".format(
        filename=report_name, org_id=entity.id, date=date.today().strftime('%Y-%m-%d'))

    return send_file(output, attachment_filename=filename, as_attachment=True)


def tmp_response(transaction):
    exist_buyer = WalletTransaction.query.filter_by(order_id=transaction.id).first()
    if exist_buyer: buyer = exist_buyer.created_by_user.email
    else: buyer = '-'

    temp = {}
    total_orderlines, total_quantity = transaction.get_quantity()
    temp['order_id'] = transaction.id
    temp['order_number'] = transaction.order_number
    temp['order_date'] = datetime.strftime((transaction.created + timedelta(hours=7)), '%Y-%m-%d')
    temp['buyer'] = buyer
    temp['total_item'] = total_orderlines
    temp['total_quantity'] = total_quantity
    temp['final_price'] = '%.2f' % transaction.final_amount
    return temp, total_orderlines
