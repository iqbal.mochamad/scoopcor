import httplib
from httplib import CREATED

from enum import IntEnum
from flask import Blueprint, jsonify, request, make_response
from flask_appsfoundry.exceptions import Conflict, InternalServerError, NotFound, BadRequest
from slugify import slugify

from app import app, ma, db
from app.eperpus.helpers import assert_user_organization
from app.eperpus.schemas import UserOrganizationSchema
from app.general.models import Address
from app.parental_controls.choices import StudentSaveControlType
from app.users.helpers import upload_logo_to_s3
from app.users.organizations import OrganizationStatus, is_organization_manager, OrganizationUser
from app.users.users import User
from app.users.wallet import Wallet

blueprint = Blueprint('eperpus_clients', __name__, url_prefix='/v1')


class EperpusType(IntEnum):
    general = 1
    smart_library = 2
    premium = 3


class ClientOrganizationSchema(ma.Schema):
    from marshmallow import fields

    name = fields.String(required=True)
    legal_name = fields.String(required=True)
    maximum_user_allowed = fields.Integer(required=True)
    app_name = fields.String(required=False)

    pic_client = fields.String(required=False)
    contact_email = fields.String(required=False)
    phone_primary = fields.String(required=False)
    phone_alternate = fields.String(required=False)

    # mailing
    mailing_address = fields.String(required=False)
    mailing_city = fields.String(required=False)
    mailing_province = fields.String(required=False)
    mailing_postal_code = fields.String(required=False)
    marketing_pic_id = fields.Integer(required=False)

    # clients
    eperpus_type = fields.Integer(required=True)  # general,smart,premium.
    premium_name = fields.String(required=False)
    username_prefix = fields.String(required=False)
    max_device = fields.Integer(required=True)

    # shared library
    user_borrowing_limit = fields.Integer(required=True)
    general_borrowing_time = fields.String(required=False)
    book_borrowing_time = fields.String(required=False)
    magazine_borrowing_time = fields.String(required=False)
    newspaper_borrowing_time = fields.String(required=False)

    # katalog
    catalog_name = fields.String(required=True)

    logo_organization = fields.String(required=False)


@blueprint.route('/new-eperpus', methods=['POST', ])
# @is_organization_manager
def create_new_eperpus():
    from app.utils.marshmallow_helpers import schema_load

    config_schema = ClientOrganizationSchema()
    args = schema_load(schema=config_schema)

    # create organization
    organization = create_organization(args)

    logo_org = args.get('logo_organization', None)

    if logo_org:
        url = upload_logo_to_s3(logo_org, organization.id)
        organization.logo_url = url
        db.session.add(organization)
        db.session.commit()

    # create clients.
    clients = create_client(organization, args)

    # create catalogs
    catalog = create_catalog(organization, args)

    # create shared library
    shared_lib = create_shared_library(organization, args)

    return jsonify(
        {
            'message': 'success create new eperpus!',
            'organization_id': organization.id,
            'href': '',
            'is_manager': None,
            'logo_url': organization.logo_url,
            'maximum_user_allowed': organization.maximum_user_allowed,
            'name': organization.name,
            'type': organization.type,
            'username_prefix': organization.username_prefix
        }), CREATED


def create_organization(args):
    try:
        from app.users.organizations import Organization
        from app.users.base import Party

        # validate organizations name?
        exist_org = Organization.query.filter_by(name=args.get('name')).first()
        if exist_org:
            msg = "Organization already exist"
            raise Conflict(user_message=msg, developer_message=msg)

        # Create mailing address
        mailing_address = Address(
            street1=args.get('mailing_address', None),
            street2=args.get('mailing_address', None),
            city=args.get('mailing_city', None),
            province=args.get('mailing_province', None),
            postal_code=args.get('mailing_postal_code', None)
        )

        db.session.add(mailing_address)
        db.session.commit()

        organization = Organization(
            name=args.get('name', None),
            legal_name=args.get('legal_name', None),
            maximum_user_allowed=args.get('maximum_user_allowed', None),
            pic_client=args.get('pic_client', None),
            contact_email=args.get('contact_email', None),
            phone_primary=args.get('phone_primary', None),
            phone_alternate=args.get('phone_alternate', None),
            mailing_address_id=mailing_address.id,
            username_prefix=args.get('username_prefix', None),
            app_name=args.get('app_name', None),
            slug=slugify(args.get('legal_name', None)),
            status=OrganizationStatus.clear.value,
            type='shared library',
            marketing_pic_id=args.get('marketing_pic_id', None),

        )
        db.session.add(organization)
        db.session.commit()

        # update parties type
        parties = Party.query.filter_by(id=organization.id).first()
        parties.party_type = 'shared library'
        db.session.add(parties)
        db.session.commit()

        # add wallet
        new_wallet = Wallet(party_id=parties.id, balance=0)
        db.session.add(new_wallet)
        db.session.commit()

        # add restricted vendors.
        from app.eperpus.libraries import SharedLibraryRestrictedVendor
        restricted_vendors = [i.strip() for i in app.config['RESTRICTED_VENDORS'].split(',')]
        for restricted in restricted_vendors:
            try:
                restrict = SharedLibraryRestrictedVendor(
                    organization_id=organization.id,
                    vendor_id=int(restricted)
                )
                db.session.add(restrict)
                db.session.commit()
            except:
                continue

        return organization

    except Exception as e:
        msg = 'Error create_organization {}'.format(e)
        raise InternalServerError(user_message=msg, developer_message=msg)


def create_client(organization=None, args=None):
    from app.auth.models import Client, Application

    try:
        if args.get('eperpus_type') == EperpusType.general.value:
            clients = Client.query.filter(Client.id.in_([88, 87])).all()

            organization.app_name = 'ePerpus'
            db.session.add(organization)
            db.session.commit()

        if args.get('eperpus_type') == EperpusType.smart_library.value:
            clients = Client.query.filter(Client.id.in_([104, 105])).all()

            organization.app_name = "Smart Library"
            organization.parental_level = StudentSaveControlType.student_save_level_5.value  # For smart library
            organization.logo_url = 'img/masthead/logo-smart.png'
            db.session.add(organization)
            db.session.commit()

        if args.get('eperpus_type') == EperpusType.premium.value:
            # create application
            application_name = 'ePerpus {}'.format(organization.name)
            application = Application(
                name=application_name,
                max_devices=args.get('max_device', None)
            )
            db.session.add(application)
            db.session.commit()

            # create client android
            client_android = Client(
                client_id=organization.id,
                app_name="{}{}".format(organization.app_name.replace(" ", "_").lower(), "_android"),
                version="1.0.0",
                lowest_supported_version="1.0.0",
                name="{} {}".format(organization.name, "(Android)"),
                slug=slugify("{} {}".format(organization.name, "(Android)")),
                lowest_os_version="4.4.0",
                platform_id=2,
                application_id=application.id
            )

            db.session.add(client_android)
            db.session.commit()

            client_android.client_id = client_android.id
            db.session.add(client_android)
            db.session.commit()

            # create client ios
            client_ios = Client(
                client_id=organization.id,
                app_name="{}{}".format(organization.app_name.replace(" ", "_").lower(), "_ios"),
                version="1.0.0",
                lowest_supported_version="1.0.0",
                name="{} {}".format(organization.name, "(iOS)"),
                slug=slugify("{} {}".format(organization.name, "(iOS)")),
                lowest_os_version='8.0.0',
                platform_id=1,
                application_id=application.id
            )

            db.session.add(client_ios)
            db.session.commit()

            client_ios.client_id = client_ios.id
            db.session.add(client_ios)
            db.session.commit()

            clients = [client_android, client_ios]

        # add client to organizations
        for iclient in clients:
            organization.clients.append(iclient)
            db.session.add(organization)
            db.session.commit()
        return clients

    except Exception as e:
        msg = 'Error create_client {}'.format(e)
        raise InternalServerError(user_message=msg, developer_message=msg)


def create_catalog(organization=None, args=None):
    from app.eperpus.models import Catalog
    from app.users.organizations import OrganizationCatalog

    try:
        catalog = Catalog(
            name=args.get('catalog_name', None),
            organization_id=organization.id)
        db.session.add(catalog)
        db.session.commit()

        # added catalog to new schema of table.
        new_access = OrganizationCatalog(
            organization_id=organization.id,
            catalog_id=catalog.id)
        db.session.add(new_access)
        db.session.commit()

        return catalog

    except Exception as e:
        msg = 'Error create_catalog {}'.format(e)
        raise InternalServerError(user_message=msg, developer_message=msg)


def create_shared_library(organization=None, args=None):
    from sqlalchemy import text
    try:
        SQL = text('''
            INSERT INTO cas_shared_libraries(
                id, user_concurrent_borrowing_limit, reborrowing_cooldown, 
                borrowing_time_limit, locked_message, public_library, book_borrowing_time_limit, 
                magazine_borrowing_time_limit, newspaper_borrowing_time_limit)
            VALUES (
            :id, :user_concurrent_borrowing_limit, '00:00:00', 
            :borrowing_time_limit, NULL, '0', 
            :book_borrowing_time_limit, :magazine_borrowing_time_limit, :newspaper_borrowing_time_limit);
        ''')

        data = {
            'id': organization.id,
            'user_concurrent_borrowing_limit': args.get('user_borrowing_limit', None),
            'borrowing_time_limit': '00:00:00',
            'book_borrowing_time_limit': args.get('book_borrowing_time', None),
            'magazine_borrowing_time_limit': args.get('magazine_borrowing_time', None),
            'newspaper_borrowing_time_limit': args.get('newspaper_borrowing_time', None)
        }

        result = db.engine.execute(SQL, **data)

        return result

    except Exception as e:
        msg = 'Error create_shared_library {}'.format(e)
        raise InternalServerError(user_message=msg, developer_message=msg)


@blueprint.route('/organizations/<organization:entity>/admins', methods=['GET', 'PUT', 'DELETE'])
@is_organization_manager
def eperpus_manager(entity):
    if request.method == 'GET':
        args = request.args.to_dict()
        limit, offset, q = args.get('limit', 20), args.get('offset', 0), args.get('q', None)
        metadata, resultset = {}, {}

        manager = OrganizationUser.query.filter_by(
            organization_id=entity.id,
            is_manager=True
        )

        resultset['count'] = manager.count()

        if request.args.get('email'):
            email = request.args.get('email')
            user_exist = User.query.filter_by(email=email).first()
            if user_exist and assert_user_organization(user_exist, entity.id):
                return make_response(jsonify({'name': ' '.join(
                    [user_exist.first_name if user_exist.first_name else '',
                     user_exist.last_name if user_exist.last_name else '']),
                    'email': user_exist.email}), httplib.OK)
            else:
                return NotFound(user_message='User tidak ditemukan', developer_message='User tidak ditemukan')

        if q:
            manager = manager.filter(User.first_name.ilike('%{}%'.format(q)))
        if limit:
            manager = manager.limit(limit).offset(offset)

        resultset['limit'] = int(limit) if limit else 0
        resultset['offset'] = int(offset) if offset else 0
        metadata['resultset'] = resultset

        data = [{
            'name': ' '.join(
                [i.user.first_name if i.user.first_name else '', i.user.last_name if i.user.last_name else '']),
            'email': i.user.email
        } for i in manager.all()]

        return jsonify({'data': data, 'metadata': metadata})
    if request.method == 'PUT':
        from app.utils.marshmallow_helpers import schema_load

        user_organization = UserOrganizationSchema()
        data = schema_load(schema=user_organization)
        user_exist = User.query.filter_by(email=data.get('email')).first()
        if not user_exist:
            return NotFound(user_message='User tidak ditemukan', developer_message='User tidak ditemukan')

        is_orgs = assert_user_organization(user_exist, entity.id)

        if not is_orgs:
            return BadRequest(developer_message='User tidak ditemukan', user_message='User tidak ditemukan')

        set_manager = OrganizationUser.query.filter_by(
            organization_id=entity.id,
            user_id=user_exist.id
        ).first()

        set_manager.is_manager = True
        db.session.add(set_manager)
        db.session.commit()

        return make_response(jsonify(data), httplib.OK)

    if request.method == 'DELETE':
        from app.utils.marshmallow_helpers import schema_load

        user_organization = UserOrganizationSchema()
        data = schema_load(schema=user_organization)
        user_exist = User.query.filter_by(email=data.get('email')).first()
        if not user_exist:
            return NotFound(user_message='User tidak ditemukan', developer_message='User tidak ditemukan')

        is_orgs = assert_user_organization(user_exist, entity.id)

        if not is_orgs:
            return BadRequest(developer_message='User tidak ditemukan', user_message='User tidak ditemukan')

        set_manager = OrganizationUser.query.filter_by(
            organization_id=entity.id,
            user_id=user_exist.id
        ).first()

        set_manager.is_manager = False
        db.session.add(set_manager)
        db.session.commit()

        return make_response(jsonify({}), httplib.NO_CONTENT)
