import json
import httplib
from smtplib import SMTPException
from flask import Response, request, render_template
from flask_restful import Resource, reqparse

from app.auth.decorators import token_required
from app.helpers import conflict_message, internal_server_message

from app.custom_get import CustomGet
from app.parental_controls.models import ParentalControl
from app.send_mail import sendmail_aws, sendmail_smtp

from .parsers import ParentalControlArgsParser



class ParentalControlListApi(Resource):

    ''' Shows a list of Parental Control and do POST / GET '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('sort_priority', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')
        self.reqparser.add_argument('slug', type=str, required=True, location='json')
        self.reqparser.add_argument('name', type=unicode, required=True, location='json')

        #optional parameters
        self.reqparser.add_argument('description', type=unicode, location='json')
        self.reqparser.add_argument('meta', type=unicode, location='json')
        self.reqparser.add_argument('icon_image_normal', type=str, location='json')
        self.reqparser.add_argument('icon_image_highres', type=str, location='json')
        self.reqparser.add_argument('parent_id', type=int, location='json')

        super(ParentalControlListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_parental_control,'
                    'can_read_write_public, can_read_write_public_ext')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = CustomGet(param, 'parental_controls', 'POST').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="parental_controls", method="POST",
                error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_global_parental_control')
    def post(self):
        args = self.reqparser.parse_args()

        u = ParentalControl.query.filter_by(name=args['name']).first()

        if u:
            return conflict_message(modul="parental control", method="POST",
                conflict=args['name'], req=args)

        #construct in same dict
        kwargs = {}
        for item in args:
            if args[item] == None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            #save author
            m = ParentalControl(**kwargs)
            sv = m.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="parental control", method="POST",
                    error=sv.get('message', ''), req=str(args))

            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="parental control", method="POST",
                error=e, req=str(args))

class ParentalControlApi(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('sort_priority', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')
        self.reqparser.add_argument('slug', type=str, required=True, location='json')
        self.reqparser.add_argument('name', type=unicode, required=True, location='json')

        #optional parameters
        self.reqparser.add_argument('description', type=unicode, location='json')
        self.reqparser.add_argument('meta', type=unicode, location='json')
        self.reqparser.add_argument('icon_image_normal', type=str, location='json')
        self.reqparser.add_argument('icon_image_highres', type=str, location='json')
        self.reqparser.add_argument('parent_id', type=int, location='json')

        super(ParentalControlApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_parental_control,'
                    'can_read_write_public, can_read_write_public_ext')
    def get(self, parent_id=None):
        m = ParentalControl.query.filter_by(id=parent_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_parental_control')
    def put(self, parent_id=None):

        m = ParentalControl.query.filter_by(id=parent_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        args = self.reqparser.parse_args()

        #checking if conflict when edit
        if args['name'] != m.name:
            u = ParentalControl.query.filter_by(name=args['name']).first()
            if u:
                return conflict_message(modul="parental control", method="PUT",
                    conflict=args['name'], req=args)

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            f = ParentalControl.query.get(parent_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="parental control", method="PUT",
                    error=sv.get('message', ''), req=str(args))

            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="parental control", method="PUT",
                error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, parent_id=None):
        f = ParentalControl.query.filter_by(id=parent_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=httplib.NOT_FOUND)

class ParentalControlEmail(Resource):


    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def post(self):

        args = ParentalControlArgsParser().parse_args()

        subject = "Your Parental Control Passcode is Here"

        content = render_template(
            'email/parental_controls/request_passcode.html', **args)

        try:
            sendmail_aws(subject, 'Gramedia Digital Passcode <no-reply@gramedia.com>', [args['email'], ], content)
        except SMTPException:
            sendmail_smtp(subject, 'Gramedia Digital Passcode <no-reply@gramedia.com>', [args['email'],], content)

        return Response(status=httplib.OK)

