from flask_appsfoundry.parsers import (
    ParserBase, InputField, )

class ParentalControlArgsParser(ParserBase):

    email = InputField(required=True)
    device_name = InputField(required=True)
    passcode = InputField(required=True)

