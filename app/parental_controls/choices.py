from __future__ import unicode_literals, absolute_import

from enum import IntEnum


class ParentalControlType(IntEnum):
    parental_level_1 = 1 # 3 - 18 All ages
    parental_level_2 = 2  # +18 age --< Cant changes this value, front end (ANDROID/IOS) Already HARDCODE, by kiratakada
    parental_level_3 = 3 # +3 age
    parental_level_4 = 4 # +7 age
    parental_level_5 = 5 # +12 age
    parental_level_6 = 6 # +16 age


class StudentSaveControlType(IntEnum):
    student_save_level_1 = 1 # All ages
    student_save_level_2 = 2 # +3 - TK
    student_save_level_3 = 3 # +7 age - SD
    student_save_level_4 = 4 # +12 age - SMP
    student_save_level_5 = 5 # +16 age - SMA
    student_save_level_6 = 6 # +17 age - ADULT.

