from app import db

from flask_appsfoundry.models import TimestampMixin


class ParentalControl(db.Model, TimestampMixin):

    __tablename__ = 'core_parentalcontrols'

    name = db.Column(db.String(40))
    description = db.Column(db.Text)
    slug = db.Column(db.String(100))
    meta = db.Column(db.Text)
    icon_image_normal = db.Column(db.String(100))
    icon_image_highres = db.Column(db.String(100))
    sort_priority = db.Column(db.Integer, nullable=False)
    is_active = db.Column(db.Boolean(), default=False)
    parent_id = db.Column(db.Integer)

    def __repr__(self):
        return '<name : %s>' % self.name

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "ParentalControl added"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "ParentalControl deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['name'] = self.name
        rv['description'] = self.description
        rv['slug'] = self.slug
        rv['meta'] = self.meta
        rv['icon_image_normal'] = self.icon_image_normal
        rv['icon_image_highres'] = self.icon_image_highres
        rv['sort_priority'] = self.sort_priority
        rv['is_active'] = self.is_active
        rv['parent_id'] = self.parent_id

        return rv
