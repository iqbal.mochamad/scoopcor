from __future__ import unicode_literals, absolute_import

from app import db
from .models import ParentalControl


def get_parentalcontrol(parentalcontrol_type, session=None):

    if not session:
        session = db.session()

    return session.query(ParentalControl).get(parentalcontrol_type.value)
