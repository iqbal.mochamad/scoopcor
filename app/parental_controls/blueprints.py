from flask import Blueprint
import flask_restful as restful

from .api import ParentalControlApi, ParentalControlListApi, ParentalControlEmail


blueprint = Blueprint('parental_controls', __name__, url_prefix='/v1/parental_controls')

api = restful.Api(blueprint)

api.add_resource(ParentalControlListApi, '')
api.add_resource(ParentalControlApi, '/<int:parent_id>', endpoint='parental_controls')
api.add_resource(ParentalControlEmail, '/email', endpoint='parental_controls_email')


