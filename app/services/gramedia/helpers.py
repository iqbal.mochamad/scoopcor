# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import decimal
import re
from datetime import datetime

import pycountry
from sqlalchemy import and_
from sqlalchemy import or_
from werkzeug.exceptions import InternalServerError

from app import db
from app.discounts.choices.predefined_type import HARPER_COLLINS
from app.discounts.models import Discount
from app.items.choices import STATUS_TYPES, STATUS_READY_FOR_CONSUME, ITEM_TYPES, BOOK, ItemTypes
from app.items.models import Item
from app.offers.choices.offer_type import SINGLE
from app.offers.models import Offer
from app.parental_controls.choices import ParentalControlType
from app.remote_publishing.choices import GRAMEDIA
from app.remote_publishing.models import RemoteOffer, RemoteCategory
from app.services.elevenia.entities.exceptions import ApiOperationFailure
from app.services.elevenia.exceptions import NoMappableCategoriesError
from app.services.elevenia.helpers import OFFER_READY_FOR_SALE
from app.services.gramedia.constants import BOOK_GENERAL_DISCOUNT_ID, HARPER_COLLINS_BOOK_DISCOUNT_ID
from app.services.gramedia.gramedia_export import insert_categories_to_gm_db, build_offer_from_gramedia_digital, \
    insert_offer_to_gm_db
from app.services.gramedia.gramedia_gateway import GramediaProductEntity
from app.services.gramedia.models import get_gramedia_session


# Some function moved to gramedia_export

def init_and_publish_offer_to_gramedia(log=None, max_records=None):
    insert_categories_to_gm_db(log=log)

    if not max_records:
        max_records = 500

    publish_offer(log=log,
                  max_records=max_records)


def publish_offer(max_records, log=None, resend=False):
    # get list of eligible offers
    session = db.session()

    single_offers = get_single_offers(session, max_records, resend)

    total_count = len(single_offers)
    success_count = 0
    failed_count = 0

    failed = []

    if log:
        log.info(u"Publish Scoop Offer to Gramedia.com. Total Offers: {}".format(total_count))

    if single_offers:
        for offer in single_offers:
            gramedia_session = get_gramedia_session()
            gramedia_product = build_offer_from_gramedia_digital(offer, session, gramedia_session)
            insert_offer_to_gm_db(gramedia_product, gramedia_session)

    results = {"total_count": total_count, "success_count": success_count, "failed_count": failed_count,
               "failed_offers": failed}

    session.expunge_all()
    session.close_all()
    return results


def get_single_offers(session, number_of_records, resend=False, item_type=None):
    """ get a list of Offer that match with certain criteria

    Filter Criteria:
    Offer.offer_type_id == 1 ==> Single only
    Offer.offer_status == OFFER_READY_FOR_SALE,
    Offer.is_free == False
    RemoteOffer.offer_id == None ==> Filter only data that not yet exists in RemoteOffer
        not exists means not sended to Gramedia.com
    Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME]
    Item.item_type.in_([u'magazine', u'book']) ==> get book and magazine only
    Item.is_active == True
    Item.content_type == 'pdf' ==> based on discussion per march 2016, only get pdf first,
        still some problem with epub

    :param session: db.session()
    :param number_of_records: number of record to get from database
    :param 'boolean' resend: if its True, process data that already sent to remote service
    :param 'str' item_type: u'magazine' or u'book' or None (to filter only offer with specific item type)
    :return: list of offers
    """
    s = session.query(
        Offer
    ).join(
        Offer.items
    ).outerjoin(
        (RemoteOffer, and_(
            RemoteOffer.offer_id == Offer.id,
            RemoteOffer.remote_service == GRAMEDIA))
    )
    s = s.filter(and_(
        Offer.offer_type_id == 1,
        Offer.offer_status == OFFER_READY_FOR_SALE,
        Offer.is_free == False,
        Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
        Item.is_active == True
    ))

    if item_type:
        s = s.filter(Item.item_type == item_type)
    else:
        s = s.filter(Item.item_type.in_([ItemTypes.magazine.value, ItemTypes.book.value]))

    if resend:
        s = s.filter(RemoteOffer.offer_id != None)
    else:
        s = s.filter(RemoteOffer.offer_id == None)

    # prevent sending adult magazine to gramedia.com
    s = s.filter(or_(Item.item_type == ItemTypes.book.value,
                     Item.parentalcontrol_id == ParentalControlType.parental_level_1.value))

    s = s.order_by(Offer.id.desc()).limit(number_of_records)

    # Offer.id == 66704,
    return s.all()


def get_item(offer):
    item = offer.items.filter(
        (Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME]) &
        (Item.item_type.in_([u'magazine', u'book'])) &
        (Item.is_active == True)).first()

    if item:
        return item
    else:
        raise NoAvailableItemsForOffer(
            "No valid items for offer ({0.id}) -- skipping".format(offer))


def build_gramedia_product_from_scoop_offer(gramedia_gateway, offer, media_base_url, session=None):
    """ convert an offer object to GramediaProductEntity

    :param gramedia_gateway: an instance of scripts.gramedia.gramedia_gateway.GramediaGateway
    :param offer: an Offer
    :param media_base_url: base url for image, real value stored in app.config['MEDIA_BASE_URL']
    :param session: sql alchemy db.session()
    :return: an instance of scripts.gramedia.gramedia_gateway.GramediaProductEntity
    """

    # get the item of the offer, it's only for single offer right now.
    item = get_item(offer)

    remote_category_id = get_remote_category_id(item)

    author_name = item.authors.first().name if item.authors.first() else ""

    publisher = item.brand.vendor.name if item.brand and item.brand.vendor else ""

    description = get_formatted_description(item.description)

    gramedia_product = GramediaProductEntity(gramedia_gateway)

    item_name = re.sub(ur'[–]', '-', item.name, flags=re.MULTILINE | re.UNICODE)

    price_idr = get_discounted_price(offer, base_price=offer.price_idr, session=session)

    if item.is_active and item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME] \
        and offer.offer_status == OFFER_READY_FOR_SALE:
        status = "Enabled"
    else:
        status = "Disabled"

    gramedia_product.new(
        name=item_name,
        sku="SC00P" + str(offer.id),
        isbn=item.gtin14 if item.gtin14 else '-',
        vpn="SC00P" + str(offer.id),
        country_of_manufacture=item.countries[0],
        author=author_name,
        publisher=publisher,
        supplier="SCOOP",
        publish_date=item.release_date.strftime('%Y-%m-%d %H:%M:%S'),
        width=0,
        length=0,
        price=str(price_idr),
        image=u"{base}{img}".format(
            base=media_base_url,
            img=item.image_normal),
        url_path_category=remote_category_id,
        short_description=offer.long_name,
        description=description,
        status=status
    )
    return gramedia_product


def get_formatted_description(description):
    description = description or '-'
    description = description.replace('\r', '<br/>')
    description = description.replace('\n', '<br/>')
    description = re.sub(ur'[–]', '-', description,
                         flags=re.MULTILINE | re.UNICODE)
    return re.sub(ur'[`”“"]', '"',
                  description,
                  flags=re.MULTILINE | re.UNICODE)


def get_remote_category_id(item):
    if item and item.item_type == 'magazine' and item.parentalcontrol_id == 2:
        return 'categories/lifestyle/magazine/adult-magazine.html'
    else:
        remote_category = get_remote_category(item)
        return remote_category.remote_id


def get_remote_category(item):
    """ Iterates a given Item's categories, and returns the first category
    that is found to have a remote mapping.

    :param `app.items.models.Item` item: The item to search for valid
        remote Elevenia category mappings.
    :return: The first valid remote gramedia category.
    :rtype: `app.remote_publishing.models.RemoteCategory`
    :raises `~NoMappableCategoriesError`: If no valid remote categories could
        be found.
    """
    cat_list = [item.brand.primary_category, ] if item.brand and item.brand.primary_category \
        else item.categories.all()

    found_category_id = None
    for category in cat_list:
        found_category_id = category.id
        remote_category = category. \
            remote_categories. \
            filter(RemoteCategory.remote_service == GRAMEDIA) \
            .first()

        if remote_category:
            return remote_category

    raise NoMappableCategoriesError(
        u"""Item ID: {item_id} with Category ID: {category_id}, does not have any known categories that map to
        valid Gramedia categories.  Cannot post to Gramedia""".format(
            item_id=item.id,
            category_id=found_category_id))


def record_offer_published_to_gramedia(session, scoop_offer, remote_id, log=None):
    session.add(RemoteOffer(
        offer_id=scoop_offer.id,
        remote_service=GRAMEDIA,
        remote_id=remote_id,
        last_checked=datetime.utcnow(),
        last_posted=datetime.utcnow()
    ))

    session.commit()

    if log:
        log.info(
            u"Published Scoop Offer: {0.long_name} ({0.id}) -> {remote_id}".format(
                scoop_offer, remote_id=remote_id)
        )


def log_skipped_item(_log, exception, scoop_offer):
    if _log:
        if scoop_offer.offer_type_id == 1:
            if isinstance(exception, NoMappableCategoriesError):
                _log.warn(
                    u"Could not publish Scoop Offer (id:{1.id}) for Item: "
                    u"{0.name} ({0.id}).  No valid category mappings!".format(
                        scoop_offer.items.first(), scoop_offer)
                )
            elif isinstance(exception, NoAvailableItemsForOffer):
                _log.warn(
                    u"Could not publish Scoop Offer {0.name} (id:{0.id}): no valid item found.".format(
                        scoop_offer)
                )
            elif isinstance(exception, ApiOperationFailure) or isinstance(exception, InternalServerError):
                _log.warn(
                    "Could not publish Scoop Offer (id:{1.id}) for Item: "
                    "{0.name} ({0.id}) -- Gramedia Message: {1.message}, {1.code}".format(
                        scoop_offer.items.first(), exception, scoop_offer))
            else:
                _log.exception(
                    "Could not publish Scoop Offer: "
                    "{0.name} ({0.id}) -- Unknown exception type encountered".format(
                        scoop_offer, exception))


def export_offer_to_gramediacom_csv(file_path, max_records=100):
    """
    This is a method to export Scoop Offer to a csv file for gramedia.com initial import catalog data

    :param file_path: csv file name and path
    :param max_records: top n of Single Offers
    :return:
    """

    session = db.session()

    offers = get_single_offers(
        session,
        number_of_records=max_records)

    # filename = '/tmp/samplescoopproduct20160422.csv'

    with open(file_path, 'w') as file:
        file.write('author,country_of_manufacture,cover_type,description,width,isbn,'
                   'kode_tu,length,name,pre_order,price,special_price,product_version,'
                   'publish_date,publisher,sku,scoop_product,status,supplier,tax_class_id,'
                   'vpn,visibility,product.attribute_set,product.type,'
                   'category.path,stock.qty,product.websites,image,url_key,stock.is_in_stock\n')
        if offers:
            for o in offers:
                item = o.items[0]
                author = item.authors.first().name if item.authors.first() else ""

                country_of_manufacture = ''
                if item.countries[0]:
                    country_of_manufacture = pycountry.countries.get(
                        alpha2=item.countries[0].upper()).name
                publisher = item.brand.vendor.name if item.brand and item.brand.vendor else ""
                remote_category_id = get_remote_category_id(item)

                isbn = (item.gtin14 or '')
                description = get_formatted_description(
                    item.description)
                sku = 'SC00P{}'.format(o.id)
                image = 'https://images.apps-foundry.com/magazine_static/{}'.format(
                    item.image_normal)

                item_name = re.sub(ur'[–]', '-', item.name, flags=re.MULTILINE | re.UNICODE)
                url_key = re.sub(ur'[–]', '-', o.long_name, flags=re.MULTILINE | re.UNICODE)

                discounted_price_idr = get_discounted_price(offer=o, base_price=o.price_idr, session=session)

                output = u'{author},\"{country_of_manufacture}\",' \
                         '{cover_type},\"{description}\",{width},' \
                         '"{isbn}",{kode_tu},{length},' \
                         '"{name}",{pre_order},{price},{special_price},' \
                         '{product_version},\"{publish_date}\",\"{publisher}\",{sku},' \
                         '{scoop_product},{status},{supplier},{tax_class_id},' \
                         '{vpn},\"{visibility}\",{product__attribute_set},{product__type},' \
                         '\"{category__path}\",{stock__qty},{product__websites},' \
                         '\"{image}\",\"{url_key}\",' \
                         '{stock__is_in_stock}\n'.format(
                    author=author,
                    country_of_manufacture=country_of_manufacture,
                    cover_type='',
                    description=description,
                    width='0',
                    isbn=isbn,
                    kode_tu='',
                    length='0',
                    name=item_name,
                    pre_order='No',
                    price=discounted_price_idr,
                    special_price='',
                    product_version='eBook',
                    publish_date=item.release_date,
                    publisher=publisher,
                    sku=sku,
                    scoop_product='Yes',
                    status='Enabled',
                    supplier='SCOOP',
                    tax_class_id='None',
                    vpn=sku,
                    visibility='Catalog, Search',
                    product__attribute_set='Books',
                    product__type='virtual',
                    category__path=remote_category_id,
                    stock__qty='1',
                    product__websites='base',
                    image=image,
                    url_key=url_key,
                    stock__is_in_stock='In Stock')

                try:
                    file.write(output)

                    # po.record_offer_published_to_gramedia(
                    #         session=session,
                    #         scoop_offer=o,
                    #         remote_id=sku,
                    #         log=None,
                    #     )

                except Exception as e:
                    print '{}, {}'.format(o.id, e.message)
                    return output


def export_offer_net_price_to_gramedia_com(file_path, max_records=100):
    """
    This is a method to export Scoop Offer to a csv file for gramedia.com initial import catalog data

    :param file_path: csv file name and path
    :param max_records: top n of Single Offers
    :return:
    """

    session = db.session()

    offers = get_single_offers(
        session,
        number_of_records=max_records,
        resend=True)

    # filename = '/tmp/samplescoopproduct20160422.csv'

    with open(file_path, 'w') as file:
        file.write('sku,price,base_price\n')
        if offers:
            for o in offers:
                sku = 'SC00P{}'.format(o.id)
                price_idr = o.price_idr

                discounted_price_idr = get_discounted_price(offer=o, base_price=o.price_idr, session=session)

                if discounted_price_idr > 0 and discounted_price_idr != price_idr:
                    output = '{},{},{}\n'.format(sku, discounted_price_idr, price_idr)
                    file.write(output)


def get_discounted_price(offer, base_price, session):
    discount_price_idr = 0

    discount = get_general_discount_for_gramedia(session, offer)
    if discount:
        discount_price_idr = discount.calculate_offer_discount(decimal.Decimal(base_price), 'IDR')

    if 0 < discount_price_idr < base_price:
        return discount_price_idr
    else:
        return base_price


def get_general_discount_for_gramedia(session, offer):
    """ Get general discount for BOOK and HARPER COLLINS's BOOK
    and SINGLE OFFER ONLY, to publish discounted price to REMOTE Service (Gramedia.com)
        # HARPER COLLINS, TEMPORARY HARDCODED to 741
        Other Books, hardcoded to BOOK_GENERAL_DISCOUNT_ID

    :param session:
    :param offer:
    :return:
    """
    if offer.offer_type_id == SINGLE and offer.items.all() \
        and offer.items[0].item_type == ITEM_TYPES[BOOK]:

        session = session if session else db.session()

        if offer.items[0].brand \
            and offer.items[0].brand.vendor_id == HARPER_COLLINS:
            return session.query(Discount).get(HARPER_COLLINS_BOOK_DISCOUNT_ID)
        else:
            return session.query(Discount).get(BOOK_GENERAL_DISCOUNT_ID)
    else:
        return None


class NoAvailableItemsForOffer(Exception):
    pass
