# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import decimal
import json
import re

import requests
from sqlalchemy import and_
from sqlalchemy import or_
from sqlalchemy.sql.functions import now

from app import app
from app import db
from app.discounts.choices.predefined_type import HARPER_COLLINS
from app.discounts.models import Discount
from app.items.choices import STATUS_TYPES, STATUS_READY_FOR_CONSUME, STATUS_UPLOADED, ITEM_TYPES, BOOK, ItemTypes
from app.items.models import Item
from app.master.models import Category
from app.offers.choices.offer_type import SINGLE
from app.offers.models import Offer
from app.parental_controls.choices import ParentalControlType
from app.remote_publishing.choices import GRAMEDIA
from app.remote_publishing.models import RemoteOffer
from app.services.elevenia.helpers import OFFER_READY_FOR_SALE
from app.services.gramedia.constants import BOOK_GENERAL_DISCOUNT_ID, HARPER_COLLINS_BOOK_DISCOUNT_ID
from app.services.gramedia.models import GramediaVendor, GramediaAuthGroup, get_gramedia_session, GramediaUser, \
    GramediaAuthor, GramediaProductMeta, GramediaProductMetaAuthor, \
    GramediaProductMetaCategory, GramediaProduct, GramediaCategory


def init_and_publish_offer_to_gramedia(log=None, max_records=None, offset=None, item_type=None):
    insert_categories_to_gm_db(log=log)

    if not max_records:
        max_records = 500

    publish_offer(log=log,
                  max_records=max_records,
                  offset=offset, item_type=item_type)


def build_categories_from_gramedia_digital():
    session = db.session()
    categories = session.query(Category).all()
    gramedia_categories = []
    for category in categories:
        gramedia_category = {
            'created': category.created,
            'modified': category.modified,
            'name': {
                'en': category.name,
                'id': category.name
            },
            'slug': ''.join([category.slug, '-ebook']),
            'is_active': category.is_active,
        }
        gramedia_categories.append(gramedia_category)
    return gramedia_categories


def insert_categories_to_gm_db(log=None):
    session = get_gramedia_session()
    categories = build_categories_from_gramedia_digital()
    if log:
        # TODO : Logging
        pass

    ebook = session.query(GramediaCategory).filter(GramediaCategory.slug == 'ebook').first()
    if not ebook:
        ebook = GramediaCategory(name=json.dumps({'en': 'Ebook', 'id': 'Ebook'}), created=now(),
                                 modified=now(),
                                 slug='ebook',
                                 is_active=True,
                                 show_on_header=True,
                                 show_on_footer=False)

        session.add(ebook)
        session.flush()
        session.commit()

    for category in categories:
        slug = category.get('slug').encode('utf-8')
        exists = session.query(GramediaCategory).filter(GramediaCategory.slug == slug).first()
        if not exists:
            session.add(
                GramediaCategory(name=json.dumps(category.get('name')), created=category.get('created'),
                                 modified=category.get('modified'),
                                 slug=slug,
                                 is_active=category.get('is_active'),
                                 show_on_header=False,
                                 show_on_footer=False,
                                 parent_id=ebook.id))
            session.flush()
            session.commit()

    session.expunge_all()
    session.close_all()


def publish_offer(max_records, offset, log=None, resend=False, item_type=None):
    # get list of eligible offers
    session = db.session()
    gramedia_session = get_gramedia_session()

    single_offers = get_single_offers(session, max_records, offset, resend, item_type=item_type)

    total_count = len(single_offers)
    success_count = 0
    failed_count = 0

    failed = []

    if log:
        log.info(u"Publish Scoop Offer to Gramedia.com. Total Offers: {}".format(total_count))

    if single_offers:
        for offer in single_offers:
            gramedia_product = build_offer_from_gramedia_digital(offer, session, gramedia_session, log=log)
            insert_offer_to_gm_db(gramedia_product, gramedia_session, log=log)

    results = {"total_count": total_count, "success_count": success_count, "failed_count": failed_count,
               "failed_offers": failed}

    gramedia_session.expunge_all()
    gramedia_session.close_all()

    session.expunge_all()
    session.close_all()
    return results


def get_single_offers(session, number_of_records, offset, resend=False, item_type=None):
    """ get a list of Offer that match with certain criteria

    Filter Criteria:
    Offer.offer_type_id == 1 ==> Single only
    Offer.offer_status == OFFER_READY_FOR_SALE,
    Offer.is_free == False
    RemoteOffer.offer_id == None ==> Filter only data that not yet exists in RemoteOffer
        not exists means not sended to Gramedia.com
    Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME]
    Item.item_type.in_([u'magazine', u'book']) ==> get book and magazine only
    Item.is_active == True
    Item.content_type == 'pdf' ==> based on discussion per march 2016, only get pdf first,
        still some problem with epub

    :param session: db.session()
    :param number_of_records: number of record to get from database
    :param offset : offset
    :param 'boolean' resend: if its True, process data that already sent to remote service
    :param 'str' item_type: u'magazine' or u'book' or None (to filter only offer with specific item type)
    :return: list of offers
    """
    s = session.query(
        Offer
    ).join(
        Offer.items
    ).outerjoin(
        (RemoteOffer, and_(
            RemoteOffer.offer_id == Offer.id,
            RemoteOffer.remote_service == GRAMEDIA))
    )
    s = s.filter(and_(
        Offer.offer_type_id.in_((1, 2)),
        Offer.offer_status == OFFER_READY_FOR_SALE,
        Offer.is_free == False,
        Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
        Item.is_active == True
    ))

    if item_type:
        s = s.filter(Item.item_type == item_type)
    else:
        # TODO : IF WE WANT TO ADD OTHER TYPE, ADD HERE
        s = s.filter(Item.item_type.in_([ItemTypes.book.value]))

    if resend:
        s = s.filter(RemoteOffer.offer_id != None)
    else:
        s = s.filter(RemoteOffer.offer_id == None)

    # prevent sending adult magazine to gramedia.com
    s = s.filter(or_(Item.item_type == ItemTypes.book.value,
                     Item.parentalcontrol_id == ParentalControlType.parental_level_1.value))

    s = s.order_by(Offer.id.desc()).offset(offset).limit(number_of_records)

    # Offer.id == 66704,
    return s.all()


def get_gramedia_com_category(slug, session):
    category = session.query(GramediaCategory).filter(GramediaCategory.slug == slug).first()
    return category.id


def get_item(offer):
    item = offer.items.filter(
        (Item.item_status.in_([STATUS_TYPES[STATUS_READY_FOR_CONSUME], STATUS_TYPES[STATUS_UPLOADED]])) &
        (Item.item_type.in_([u'magazine', u'book', u'newspaper'])) &
        (Item.is_active == True)).first()

    if item:
        return item
    else:
        raise NoAvailableItemsForOffer(
            "No valid items for offer ({0.id}) -- skipping".format(offer))


class NoAvailableItemsForOffer(Exception):
    pass


def get_discounted_price(offer, base_price, session):
    discount_price_idr = 0

    discount = get_general_discount_for_gramedia(session, offer)
    if discount:
        discount_price_idr = discount.calculate_offer_discount(decimal.Decimal(base_price), 'IDR')

    if 0 < discount_price_idr < base_price:
        return discount_price_idr
    else:
        return base_price


def get_general_discount_for_gramedia(session, offer):
    """ Get general discount for BOOK and HARPER COLLINS's BOOK
    and SINGLE OFFER ONLY, to publish discounted price to REMOTE Service (Gramedia.com)
        # HARPER COLLINS, TEMPORARY HARDCODED to 741
        Other Books, hardcoded to BOOK_GENERAL_DISCOUNT_ID

    :param session:
    :param offer:
    :return:
    """
    if offer.offer_type_id == SINGLE and offer.items.all() \
        and offer.items[0].item_type == ITEM_TYPES[BOOK]:

        session = session if session else db.session()

        if offer.items[0].brand \
            and offer.items[0].brand.vendor_id == HARPER_COLLINS:
            return session.query(Discount).get(HARPER_COLLINS_BOOK_DISCOUNT_ID)
        else:
            return session.query(Discount).get(BOOK_GENERAL_DISCOUNT_ID)
    else:
        return None


def trim_to_value(content, max_length):
    if len(content) > max_length and content:
        content = content[:max_length]
        if content[-1] == '-':
            return content[:-1]
        return content

    return content


GRAMEDIA_MAX_SLUG_LENGTH = 150


def build_offer_from_gramedia_digital(offer, session, gramedia_session, log=None):
    item = get_item(offer)

    category_slug = ''.join([item.categories[0].slug, '-ebook'])
    gramedia_com_category_id = get_gramedia_com_category(category_slug, gramedia_session)

    author_name = item.authors.first().name if item.authors.first() else "Unknown Author"

    author_slug = item.authors.first().slug if item.authors.first() else 'unknown-author'
    publisher_name = item.brand.vendor.name if item.brand and item.brand.vendor else "Unknown Vendor"
    publisher_slug = item.brand.vendor.slug if item.brand and item.brand.vendor else "unknown-vendor"

    description = item.description if item.description else '-'

    author_slug = trim_to_value(author_slug, GRAMEDIA_MAX_SLUG_LENGTH)

    item_name = re.sub(ur'[–]', '-', item.name, flags=re.MULTILINE | re.UNICODE)

    price_idr = offer.price_idr

    if item.is_active and item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME] and \
        offer.offer_status == OFFER_READY_FOR_SALE:
        status = True
    else:
        status = False

    gramedia_digital_offer = {
        'name': item_name,
        'slug': item.slug,
        'sku': "SC00P" + str(offer.id),
        'isbn': item.gtin14 if item.gtin14 else '-',
        'author': {
            'slug': author_slug,
            'name': author_name
        },
        'publisher': {
            'name': publisher_name,
            'slug': publisher_slug
        },
        'publish_date': item.release_date.strftime('%Y-%m-%d %H:%M:%S'),
        'width': 0,
        'length': 0,
        'price': price_idr,
        'image': item.image_highres,
        'description': description,
        'is_active': status,
        'item_type': item.item_type,
        'category_id': gramedia_com_category_id
    }

    log.info("build offer {}".format(gramedia_digital_offer.get('sku')))

    return gramedia_digital_offer


def insert_offer_to_gm_db(gramedia_digital_offer, session, log=None):
    author = build_author(gramedia_digital_offer)
    author_gramedia = insert_author_to_gm_db(author, session)

    vendor = build_vendor(gramedia_digital_offer)
    vendor_gramedia = insert_vendor_to_gm_db(vendor, session)

    product_meta = build_product_meta(gramedia_digital_offer, vendor_gramedia, author_gramedia)
    product_meta_gramedia = insert_product_meta_to_gm_db(product_meta, session)

    product = build_product(gramedia_digital_offer, product_meta_gramedia)
    product_gramedia = insert_product_to_gm_db(product, session, log=log)


def build_author(gramedia_digital_offer):
    name = gramedia_digital_offer.get('author').get('name')
    slug = gramedia_digital_offer.get('author').get('slug')
    author = {
        'name': name,
        'slug': slug,
        'username': slug,
        'first_name': ' '.join(name.split()[:-1]),
        'last_name': name.split()[-1],
        'email': '{slug}@email.com'.format(slug=slug)
    }
    return author


def insert_author_to_gm_db(author_build, session):
    user_gramedia = session.query(GramediaUser).filter(GramediaUser.email == author_build.get('email')).first()

    if not user_gramedia:
        user_gramedia = GramediaUser(username=author_build.get('username'),
                                     email=author_build.get('email'),
                                     first_name=author_build.get('first_name'),
                                     last_name=author_build.get('last_name'),
                                     password='',
                                     is_active=True,
                                     is_superuser=False,
                                     is_staff=False,
                                     is_guest=False,
                                     date_joined=now())

        session.add(user_gramedia)
        session.flush()
        session.commit()

        author_gramedia = GramediaAuthor(gramediauser_ptr_id=user_gramedia.id,
                                         aliases={},
                                         bio={'en': None, 'id': None},
                                         pic='')

        session.add(author_gramedia)
        session.commit()

    else:
        author_gramedia = session.query(GramediaAuthor).filter(
            GramediaAuthor.gramediauser_ptr_id == user_gramedia.id).first()
        if not author_gramedia:
            author_gramedia = GramediaAuthor(gramediauser_ptr_id=user_gramedia.id,
                                             aliases={},
                                             bio={'en': None, 'id': None},
                                             pic='')

            session.add(author_gramedia)
            session.flush()
            session.commit()

    return author_gramedia


def build_vendor(gramedia_digital_offer):
    publisher = gramedia_digital_offer.get('publisher')
    is_active = gramedia_digital_offer.get('is_active')
    vendor = {
        'name': publisher.get('name'),
        'slug': publisher.get('slug'),
        'is_active': is_active,
        'sort_priority': 0
    }
    return vendor


def insert_vendor_to_gm_db(vendor, session):
    vendor_slug = vendor.get('slug')

    vendor_gramedia = session.query(GramediaVendor).filter(GramediaVendor.slug == vendor_slug).first()
    if not vendor_gramedia:
        auth_group = session.query(GramediaAuthGroup).filter(GramediaAuthGroup.name == vendor.get('name')).first()
        if not auth_group:
            auth_group = GramediaAuthGroup(name=vendor.get('name'))
            session.add(auth_group)
            session.flush()
            session.commit()
        vendor_gramedia = GramediaVendor(group_ptr_id=auth_group.id,
                                         created=now(),
                                         modified=now(),
                                         slug=vendor_slug,
                                         is_active=vendor.get('is_active'),
                                         sort_priority=vendor.get('sort_priority'))
        session.add(vendor_gramedia)
        session.flush()
        session.commit()
    return vendor_gramedia


def build_product_meta(gramedia_digital_offer, vendor, author):
    created = now()
    modified = now()
    name = gramedia_digital_offer.get('name')
    isbn = gramedia_digital_offer.get('isbn')
    is_active = gramedia_digital_offer.get('is_active')
    pic = gramedia_digital_offer.get('image')
    description = gramedia_digital_offer.get('description', '')
    slug = gramedia_digital_offer.get('slug')
    category_id = gramedia_digital_offer.get('category_id')
    product_meta = {
        'created': created,
        'modified': modified,
        'name': name,
        'slug': slug,
        'is_active': is_active,
        'pic': pic,
        'description': description,
        'category_id': category_id,
        'vendor_id': vendor.group_ptr_id,
        'author_id': author.gramediauser_ptr_id
    }

    bucket = 'ebook-covers/'
    aws_public = app.config['AWS_PUBLIC'] + bucket

    path = pic.split('/')
    aws_path = '/'.join(path[2:])

    image_url = aws_public + aws_path
    aws_session = get_aws_session()
    key = 'uploads/' + pic
    upload_to_s3(image_url, key, aws_session)

    return product_meta


def get_aws_session():
    import boto3

    aws_access_key_id = app.config['AWS_ACCESS_KEY_ID']
    aws_secret_access_key = app.config['AWS_SECRET_ACCESS_KEY']
    session = boto3.Session(
        aws_access_key_id=aws_access_key_id,
        aws_secret_access_key=aws_secret_access_key,
    )
    return session


def upload_to_s3(url, key, session_aws):
    bucket = app.config['AWS_STORAGE_BUCKET_NAME']

    s3 = session_aws.resource('s3')

    img_data = requests.get(url).content
    bucket = s3.Bucket(bucket)
    if img_data:
        bucket.Object(key).put(Body=img_data)


def insert_product_meta_to_gm_db(product_meta_build, session):
    product_meta = session.query(GramediaProductMeta).filter(
        GramediaProductMeta.slug == product_meta_build.get('slug')).first()
    if not product_meta:
        product_meta = GramediaProductMeta(created=now(),
                                           modified=now(),
                                           description=product_meta_build.get('description'),
                                           name=product_meta_build.get('name'),
                                           is_active=False,
                                           slug=product_meta_build.get('slug'),
                                           pic=product_meta_build.get('pic'),
                                           vendor_id=product_meta_build.get('vendor_id'),
                                           product_type_id=1)

        session.add(product_meta)
        # session.flush()
        session.commit()

        product_meta_author = GramediaProductMetaAuthor(productmeta_id=product_meta.id,
                                                        author_id=product_meta_build.get('author_id'))

        product_meta_category = GramediaProductMetaCategory(productmeta_id=product_meta.id,
                                                            category_id=product_meta_build.get('category_id'))

        session.add(product_meta_author)
        session.add(product_meta_category)
        session.commit()
    else:
        product_meta.modified = now()
        product_meta.pic = product_meta_build.get('pic')

        session.add(product_meta)
        # session.flush()
        session.commit()

    return product_meta


def build_product(gramedia_digital_offer, product_meta):
    is_active = gramedia_digital_offer.get('is_active')
    name = gramedia_digital_offer.get('name')
    base_price = gramedia_digital_offer.get('price')
    slug = gramedia_digital_offer.get('slug')
    sku = gramedia_digital_offer.get('sku')
    description = gramedia_digital_offer.get('desctiption')
    publish_date = gramedia_digital_offer.get('publish_date')
    isbn = gramedia_digital_offer.get('isbn')
    item_type = gramedia_digital_offer.get('item_type', None)
    product = {
        'created': now(),
        'modified': now(),
        'is_active': is_active,
        'name': name,
        'base_price': base_price,
        'slug': slug,
        'sku': sku,
        'desctiption': description,
        'format': 'ebook',
        'type': item_type,
        'meta_id': product_meta.id,
        'publish_date': publish_date,
        'meta_title': product_meta.name,
        'type_data': {
            'isbn': isbn
        }
    }

    return product


def check_slug(slug, session, tries=0):
    candidate_slug = "{slug}-{tries}".format(slug=slug, tries=tries) if tries else slug
    if session.query(GramediaProduct).filter(GramediaProduct.slug == candidate_slug).first():
        return check_slug(slug=slug, session=session, tries=tries + 1)

    return candidate_slug


def insert_product_to_gm_db(product_build, session, log=None):
    product = session.query(GramediaProduct).filter(GramediaProduct.sku == product_build.get("sku")).first()

    if not product:

        product = GramediaProduct(created=product_build.get('created'),
                                  modified=product_build.get('modified'),
                                  name=product_build.get('name'),
                                  is_active=False,
                                  base_price=product_build.get('base_price'),
                                  slug=check_slug(product_build.get('slug'), session),
                                  sku=product_build.get('sku'),
                                  description=product_build.get('description', ''),
                                  publish_date=product_build.get('publish_date'),
                                  type=product_build.get('type'),
                                  format=product_build.get('format'),
                                  meta_id=product_build.get('meta_id'),
                                  meta_title=product_build.get('meta_title'),
                                  meta_keyword='',
                                  meta_description='',
                                  need_review=False,
                                  type_data={'isbn': product_build.get('type_data').get('isbn')})

        session.add(product)
        session.flush()
        session.commit()
        log.info("{} newly published".format(product.sku))
    else:
        if product.base_price != product_build.get('base_price'):
            product.base_price = product_build.get('base_price')
            product.is_active = product_build.get('is_active')
            product.type = product_build.get('type')

            session.add(product)
            session.flush()
            session.commit()
            log.info("{} price updated".format(product.sku))

        elif product.type != product_build.get('type'):
            log.info("change product type {old_type} to {new_type}".format(
                old_type=product.type, new_type=product_build.get('type')))

            product.type = product_build.get('type')
            session.add(product)
            session.flush()
            session.commit()
            log.info("product type changed to {new_type}".format(
                old_type=product.type, new_type=product_build.get('type')))

    log.info("{} published".format(product.sku))
    return product


def update_product_price_to_gm_db(offer, gramedia_session):
    sku = 'SC00P{id}'.format(id=offer.id)
    product = gramedia_session.query(GramediaProduct).filter(GramediaProduct.sku == sku).first()
    if product:
        product.base_price = offer.price_idr

        gramedia_session.add(product)
        gramedia_session.commit()
