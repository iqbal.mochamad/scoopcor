from suds.client import Client
from suds.xsd.doctor import ImportDoctor, Import

import json

from werkzeug.exceptions import InternalServerError


class GramediaGateway(object):

    def __init__(self, url_host, user, password, is_mock_mode=False):
        self.url_host = url_host
        self.user = user
        self.password = password
        self.is_mock_mode = is_mock_mode

    def send_to_gramedia_product_catalog(self, data):

        if self.is_mock_mode:
            return '{"data": [{"status": "success" }]}'

        imp = Import('http://schemas.xmlsoap.org/soap/encoding/')
        imp.filter.add('urn:Magento')
        d = ImportDoctor(imp)

        # ex: url = 'http://dev.gramedia.com/api/?wsdl'
        client = Client(self.url_host, doctor=d, timeout=120)

        remote_session = client.service.login(self.user, self.password)
        # ex: 'tomo', '123456'

        response = client.service.call(remote_session, 'scoop.postdata', data)

        if response:
            return response
        else:
            return None


class GramediaProductEntity(object):

    def __init__(self, gateway, **kwargs):
        self.gateway = gateway
        self.name = None
        self.sku = None
        self.isbn = None
        self.vpn = None
        self.country_of_manufacture = None
        self.author = None
        self.publisher = None
        self.supplier = "SCOOP"
        self.publish_date = None
        self.width = 0
        self.length = 0
        self.price = None
        self.image = None
        self.url_path_category = None
        self.short_description = None
        self.description = None

    def new(self, name, description, short_description, sku, isbn,
            vpn, country_of_manufacture, author, publisher,
            supplier, publish_date,
            width, length,
            price,
            image, url_path_category, status
            ):
        self.name = name
        self.sku = sku
        self.isbn = isbn
        self.vpn = vpn
        self.country_of_manufacture = country_of_manufacture

        self.author = author
        self.publisher = publisher
        self.supplier = supplier
        self.publish_date = publish_date
        self.width = width

        self.length = length
        self.price = price
        self.image = image
        self.url_path_category = url_path_category
        self.short_description = short_description
        self.description = description
        self.status = status

    def get_json_data(self):
        data = self.__dict__.copy()

        # remove gateway from json data
        data.pop('gateway')

        return json.dumps({"items": [data, ]})

    def send_to_gramedia(self):

        data = self.get_json_data()

        response = self.gateway.send_to_gramedia_product_catalog(data)

        self.parse_response_status(response)

    @staticmethod
    def parse_response_status(response):
        if response:
            response = json.loads(response).get('data', None)

        status = response[0].get('status', 'empty response') if response else 'empty response'
        if status != 'success':
            raise InternalServerError('Post to Gramedia API got valid response code '
                                      'but operation FAILED (status = {})'.format(status))

