from sqlalchemy import Column, String, Date, Boolean, Integer, ForeignKey, Float
from sqlalchemy import create_engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy.orm import relationship

from app import app

Base = automap_base()


def get_gramedia_session():
    gm_core_db_uri = app.config['GRAMEDIA_DATABASE_URI']
    engine = create_engine(gm_core_db_uri)
    Base.prepare(engine, reflect=True)
    session = Session(engine)

    return session


class GramediaCategory(Base):
    __tablename__ = 'category'
    created = Column('created', Date)
    modified = Column('modified', Date)
    name = Column('name', String)
    is_active = Column('is_active', Boolean)
    slug = Column('slug', String)


class GramediaAuthGroup(Base):
    __tablename__ = 'auth_group'
    name = Column('name', String)


class GramediaVendor(Base):
    __tablename__ = 'vendor'
    group_ptr_id = Column(Integer, ForeignKey('auth_group.id'), primary_key=True)
    created = Column('created', Date)
    modified = Column('modified', Date)
    is_active = Column('is_active', Boolean)
    slug = Column('slug', String)
    sort_priority = Column('sort_priority', Integer)


class GramediaUser(Base):
    __tablename__ = 'user'
    username = Column('username', String)
    email = Column('email', String)
    first_name = Column('first_name', String)
    last_name = Column('last_name', String)


class GramediaAuthor(Base):
    __tablename__ = 'author'
    gramediauser_ptr_id = Column(Integer, ForeignKey('user.id'), primary_key=True)
    user = relationship(GramediaUser, backref='user', foreign_keys=[gramediauser_ptr_id])


class GramediaProductMeta(Base):
    __tablename__ = 'product_meta'
    created = Column('created', Date)
    modified = Column('modified', Date)
    name = Column('name', String)
    is_active = Column('is_active', Boolean)
    slug = Column('slug', String)
    vendor_id = Column(ForeignKey('auth_group.id'))
    product_type_id = Column(ForeignKey('product_type.id'))


class GramediaProductMetaAuthor(Base):
    __tablename__ = 'product_meta_authors'
    id = Column(Integer, primary_key=True)
    productmeta_id = Column(ForeignKey('product_meta.id'))
    author_id = Column(ForeignKey('author.gramediauser_ptr_id'))


class GramediaProductMetaCategory(Base):
    __tablename__ = 'product_meta_categories'
    id = Column(Integer, primary_key=True)
    productmeta_id = Column(ForeignKey('product_meta.id'))
    category_id = Column(ForeignKey('category.id'))


class GramediaProduct(Base):
    __tablename__ = 'product'
    created = Column('created', Date)
    modified = Column('modified', Date)
    name = Column('name', String)
    is_active = Column('is_active', Boolean)
    base_price = Column('base_price', Float)
    slug = Column('slug', String)
    sku = Column('sku', String)
    description = Column('description', String)
    publish_date = Column('publish_date', Date)
    type = Column('type', String)
    format = Column('format', String)
    meta_id = Column(ForeignKey('product_meta.id'))
