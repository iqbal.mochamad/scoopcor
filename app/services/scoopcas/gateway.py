from __future__ import unicode_literals, absolute_import
import random
import string
from httplib import OK, NOT_FOUND, CREATED, NO_CONTENT
from warnings import warn
from sha import sha

import requests


__all__ = ('ScoopCas', )


class ScoopCas(object):
    """ Gateway service for communicating with the ScoopCas API. """
    def __init__(self, base_url, auth_token, testing=False):
        """
        :param `str` base_url: The base url for HTTP requests to this service.
        :param `str` auth_token: The complete 'Authorize' header value.
        """
        self.base_url = base_url
        self.auth_token = auth_token
        self.testing = testing

    def get_client(self, client_id):
        """
        Requests client data from Scoopcas about a single client.

        :param int client_id: Client ID in scoopcor database.
        :return: A dictionary representation of the client.
        :rtype: dict
        :raises: :py:class:`ValueError` if a non-200 http status code
            is received.
        """

        if self.testing:
            warn("System in test mode -- calls to scoopcas are skipped!")
            return int(client_id)

        resp = requests.get(
            "{base_url}/clients/{id}".format(
                base_url=self.base_url,
                id=client_id),
            headers={
                'Authorization': self.auth_token,
                'Accept': 'application/json',
            })

        if resp.status_code != OK:
            raise ValueError("Invalid response from "
                             "server: {0}".format(resp.status_code))

        return resp.json()

    def get_or_create_user_by_email(self, email, origin_client_id, password=None):
        """ Fetches a user by their e-mail address.  If none found, creates a new user.

        :param string email: The user's e-mail address to perform the search with.
            If the user doesn't exist, the provided e-mail address will be used
            for BOTH their username and e-mail address.
        :param int origin_client_id: The scoopcas ClientID
        :param string password: An initial password to be assigned to the
            user if a new user account is be created.
        :return: A dictionary representing the user's properties returned
            from scoopcas
        :rtype: dict
        """
        user = self.get_user_by_email(email) or \
               self.create_user(username=email,
                                email=email,
                                origin_client_id=origin_client_id,
                                password=password)
        return user

    def get_user(self, user_id):
        """ Fetch a User by their user id.

        :param int user_id: The User.ID of the account to retrieve.
        :return: None if the user could not be found, otherwise returns a
            dictionary representing the User.
        :rtype: dict
        :raises: `ValueError` if the scoopcas server returns any other
            status code than HTTP 200, 204, or 404.
        """
        resp = requests.get(
            "{base_url}/users/{user_id}".format(
                base_url=self.base_url,
                user_id=user_id),
                headers={
                    'Authorization': self.auth_token,
                    'Accept': 'application/json',
                    'Accept-charset': 'utf-8',
                })
        if resp.status_code == OK:
            return resp.json()
        elif resp.status_code in [NO_CONTENT, NOT_FOUND]:
            return None
        else:
            raise ValueError("Invalid response from "
                             "server: {0}".format(resp.status_code))

    def get_user_by_email(self, email):
        """ Fetch a User by their e-mail address.

        :param string email: The e-mail address of the user to fetch.
        :return: None if the user could not be found, otherwise returns a
            dictionary representing the User.
        :rtype: dict
        :raises: `ValueError` if the scoopcas server returns any other
            status code than HTTP 200, 204, or 404.
        """
        resp = requests.get(
            "{base_url}/users?gift_email={email}".format(
                base_url=self.base_url,
                email=email),
             headers={
                 'Accept': 'application/json',
                 'Accept-charset': 'utf-8',
                 'Authorization': self.auth_token})

        if resp.status_code == OK:
            if int(resp.json()['metadata']['resultset']['count']) == 1:
                return resp.json()['users'][0]
            else:
                return None
        elif resp.status_code in [NO_CONTENT, NOT_FOUND]:
            return None
        else:
            raise ValueError("Invalid response from "
                             "server: {0}".format(resp.status_code))

    def create_user(self, username, email, origin_client_id, password=None):
        """ Creates a new user account with the specified parameters.

        :param string username: New account username.
        :param string email:  New account's e-mail address (typically, the same
            as their e-mail address)
        :param int origin_client_id: A valid scoopcas Client.ID.
        :param string password:  An initial password to assign to the user.
            If none is provided, a random alpha-numeric string of 10 digits
            will be created.
        :return: A dictionary representing the new user.
        :rtype: dict
        :raises: `ValueError` if the account cannot be created.
        """
        # prehash the password for sending to SC1
        password = password or self._generate_random_password()
        password = self._prehash_password('5gvi-ojhgf%^&YGBNeds1{', password)

        resp = requests.post(
            "{base_url}/users".format(base_url=self.base_url),
            json={
                'username': username,
                'password': password,
                'email': email,
                'origin_client_id': origin_client_id
            },
            headers={
                'accept': 'application/json',
                'accept-charset': 'utf-8',
                'authorization': self.auth_token,
                'content-type': 'application/json'
            })

        if resp.status_code == CREATED:
            return resp.json()
        else:
            raise ValueError("Invalid response from "
                             "server: {0}".format(resp.status_code))

    def _generate_random_password(self, length=10):
         return "".join(random.choice(string.ascii_uppercase + string.digits)
            for _ in range(length))

    def _prehash_password(self, salt, plaintext):
        return sha(salt + plaintext).hexdigest()

    def resource(self, resource_class):
        return resource_class.get_resource_manager(self)

#
# cas = ScoopCas()
# user = cas.resource(User).get(1234)
#
# >>> for m in f.parse("/v1/users/{id}/password-reset"):
# ...     print m
# ...
# ('/v1/users/', 'id', '', None)
# ('/password-reset', None, None, None)
# >>> for m in f.parse("/v1/users/{id}/password-reset/{bcde}"):
# ...     print m
# ...
# ('/v1/users/', 'id', '', None)
# ('/password-reset/', 'bcde', '', None)
#
