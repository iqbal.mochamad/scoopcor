from __future__ import unicode_literals, absolute_import
from enum import IntEnum


class CasResourceManager(object):
    def __init__(self, gateway, resource_class):
        self.gateway = gateway
        self.resource_class = resource_class

    def list(self, **kwargs):
        raise NotImplementedError

    def get(self, **kwargs):
        raise NotImplementedError

    def create(self, entity):
        raise NotImplementedError

    def update(self, entity):
        raise NotImplementedError

    def delete(self, entity):
        raise NotImplementedError


class CasResourceBase(object):
    pass


class User(CasResourceBase):

    __uri_path__ = '/v1/users/{id}'

    @classmethod
    def get_resource_manager(cls, gateway):
        return CasResourceManager(gateway, cls)

    def __init__(self, **kwargs):
        self.id = kwargs.get('id')
        self.user_id = kwargs.get('user_id')
        self.username = kwargs.get('username')
        self.password = kwargs.get('password')
        self.email = kwargs.get('email')
        self.origin_client_id = kwargs.get('origin_client_id')
        self.first_name = kwargs.get('first_name')
        self.last_name = kwargs.get('last_name')
        self.title = kwargs.get('title')
        self.fb_id = kwargs.get('fb_id')
        self.is_active = kwargs.get('is_active')
        self.status = kwargs.get('status')
        self.note = kwargs.get('note')
        self.banned_from = kwargs.get('banned_from')
        self.banned_to = kwargs.get('banned_to')
        self.is_verified = kwargs.get('is_verified')
        self.referral_code = kwargs.get('referral_code')
        self.referral_id = kwargs.get('referral_id')
        self.profile = kwargs.get('profile')


class UserProfile(CasResourceBase):
    def __init__(self, **kwargs):
        self.gender = kwargs.get('gender')
        self.birthdate = kwargs.get('birthdate')
        self.origin_latitude = kwargs.get('origin_latitude')
        self.origin_longitude = kwargs.get('origin_longitude')
        self.origin_country_code = kwargs.get('origin_country_code')
        self.origin_device_model = kwargs.get('origin_device_model')
        self.origin_partner_id = kwargs.get('origin_partner_id')
        self.current_latitude = kwargs.get('current_latitude')
        self.current_longitude = kwargs.get('current_longitude')


class UserTitle(IntEnum):
    mr = 1
    mrs = 2


class UserStatus(IntEnum):
    clear = 1
    banned = 2
