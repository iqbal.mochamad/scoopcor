from __future__ import absolute_import, unicode_literals
from enum import Enum


SANDBOX_BASE_URI = "http://dev.kredivo.com/kredivo"
PRODUCTION_BASE_URI = "https://api.kredivo.com/kredivo"


class KredivoPaymentType(Enum):
    thirty_days = '30_days'
    three_months = '3_months'
    six_months = '6_months'
    twelve_months = '12_months'
