from __future__ import absolute_import, unicode_literals
import requests
from flask import current_app, g

from app.services.kredivo.kredivo_gateway import Kredivo
from app.services.kredivo.models import KredivoCheckoutEntity, ItemDetail, TransactionDetail, CustomerDetail


def build_kredivo_entity(order_id, order_items, amount, back_to_store_uri):
    items = [ItemDetail(
        id=item['offer_id'],
        name=item['name'],
        price=item['price'],
        type="",
        url="",
        quantity=item['quantity']
    ) for item in order_items]
    transaction_details = TransactionDetail(
        amount=amount,
        order_id=order_id,
        items=items
    )
    customer_details = CustomerDetail(
        first_name=g.current_user.first_name if g.current_user and g.current_user.first_name else "",
        last_name=g.current_user.last_name if g.current_user and g.current_user.last_name else "",
        email=g.current_user.email if g.current_user and g.current_user.email else "",
        phone=""
    )

    return KredivoCheckoutEntity(
            transaction_details=transaction_details,
            customer_details=customer_details,
            push_uri="{}payments/kredivo/capture".format(current_app.config['BASE_URL']),
            back_to_store_uri=back_to_store_uri)
    # order_status_uri = "",


def build_kredivo_gateway():
    """ create an instance of kredivo gateway

    in config.py: SERVER KEY FOR LIVE
    KREDIVO_SERVER_KEY = "WqzHqeCna9zxYTg3CdXRfBNpQ75Qej"

    in local_config.py: SERVER KEY FOR DEV/Testing
    KREDIVO_SERVER_KEY = "8tLHIx8V0N6KtnSpS9Nbd6zROFFJH7"
    """
    return Kredivo(
        server_key=current_app.config['KREDIVO_SERVER_KEY'],
        development_mode=current_app.config['DEBUG'],
        http_client=requests
    )

