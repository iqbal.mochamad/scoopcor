from xml.etree import ElementTree
from xml.etree.ElementTree import ParseError

import requests
from requests.exceptions import RequestException

from .base import EleveniaEntity, XmlProperty, XmlBooleanProperty, XmlFloatProperty, XmlIntegerProperty
from .client_message import ClientMessage
from .choices import SALE_TYPES, ITEM_CONDITIONS, TAX_CHOICES, SALES_PERIOD_CODES
from .choices.result_status import OK
from .exceptions import UnknownApiResponseError, ApiOperationFailure


class Product(EleveniaEntity):
    """ Anything which may be sold on the Elevenia web store.

    id
        A unique id which is automatically assigned BY Elevenia.
    name
        A plaintext string that will be shown to potential buyers.  By request
        of Elevenia, all names should be formatted as
        "[SCOOP Digital] {our-item-name}"
    nickname
        The displayed item name.
    scoop_item_id
        Our internal item id.  By request of elevenia, this must be formatted
        as "SC00P{our-item-id}".
    service_type
        An internal elevenia product type identifier.  By request of Elevenia
        this should always be '25' (represents a digital coupon).
    display_category_id

    delivery_template_id
        Used for determining 'delivery charges' for product.  For our cases,
        our account should be configured with only 1 delivery template.
    sale_type
        'ready sale' (everything we sell), 'second hand', or 'pre-order'
    advertising_statement
        Marketing sales pitch for the product.
    weight_kilograms
        Shipping information -- for our cases, this is irrelevant, and should
        always be set to 1 (elevenia will not accept anything else).
    item_condition
        'new' or 'used'.  For our purposes, always 'new'
    country_of_origin
        ISO3166 country-code to indicate this product's country of origin.
        This is unused.
    regional_code
        An additional regional code to describe where this product originated.
        This is unused.
    vat_tax_code
        Indicates whether this item is taxed or not.  See choices.
    is_sellable_to_minors
        Indicates whether this item is sellable to users under 17 years.
    uses_sales_period
        Used to indicate preorder or items that are only sellable at
        specific/for specific periods of time.  This is unused.
    sales_period_code
        Used in conjunction with uses_sales_period to indicate the the type
        of time period this item is sellable.
    price
        Selling price in IDR.  This must be specified as an integer.
    quantity_in_stock
        Indicates the number of units in stock.  As all our items are digital,
        this should always be set to 99999 (one shy of 100,000).
    point_unit_code
        Not used as we are not supporting Elevenia's point redemption system.
    after_service_details
        Short amount of text to inform the buyer how to communicate with
        our customer support representative.
    return_or_exchange_details
        Short amount of text to inform the buyer how to exchange or return
        their item (for our purposes, this means contacting our customer
        support e-mail address).
    has_delivery_guarantee
        Do we guarentee that the buyer will get their product?  Always True.
    primary_image
        The URL for the product image shown on Elevenia's web portal.
        MUST be accessible by Elevenia's servers at the time of upload or
        an error will be thrown.
    primary_image_bytecode
        Same as primary_image, but instead of a URL, the image is specified
        as a base64 encoded string.  We are not using this option.
    html_detail
        Marketing information specified as HTML.
    """
    __uri_part__ = "/prodservices/product"

    id = XmlIntegerProperty("prdNo")
    name = XmlProperty("prdNm", required=True)
    nickname = XmlProperty("selMnbdNckNm", required=True)
    scoop_item_id = XmlProperty("sellerPrdCd")
    service_type = XmlIntegerProperty("prdTypCd", required=True, default=25)

    display_category_id = XmlIntegerProperty("dispCtgrNo", required=True)
    delivery_template_id = XmlIntegerProperty("tmpltSeq", required=True)

    sale_type = XmlProperty("selMthdCd", choices=SALE_TYPES, required=True)
    advertising_statement = XmlProperty("advrtStmt")
    weight_kilograms = XmlFloatProperty("prdWght", required=True)
    item_condition = XmlProperty("prdStatCd", choices=ITEM_CONDITIONS, required=True)

    country_of_origin = XmlProperty("orgnTypCd")
    regional_code = XmlProperty("orgnNmVal")

    vat_tax_code = XmlProperty("suplDtyfrPrdClfCd", choices=TAX_CHOICES, required=True)

    is_sellable_to_minors = XmlBooleanProperty("minorSelCnYn", required=True)
    uses_sales_period = XmlBooleanProperty("selTermUseYn", required=True)
    sales_period_code = XmlProperty("selPrdClfCd", choices=SALES_PERIOD_CODES)

    price = XmlIntegerProperty("selPrc", required=True)
    quantity_in_stock = XmlIntegerProperty("prdSelQty", required=True)
    user_purchase_limit = XmlIntegerProperty("selLimitQty", required=True, default=1)
    point_unit_code = XmlProperty("spplWyCd")

    after_service_details = XmlProperty("asDetail", required=True)
    return_or_exchange_details = XmlProperty("rtngExchDetail", required=True)
    has_delivery_guarantee = XmlBooleanProperty("dlvGrntYn", required=True)

    primary_image = XmlProperty("prdImage01", required=True)
    primary_image_bytecode = XmlProperty("prdImage01Src")

    html_detail = XmlProperty("htmlDetail", required=True)

    def single(self, prod_id):
        """ Fetches a paginated listing of Products.

        :param `int` page_num: The page number to retrieve (defaults to 1).
        :return: `list` of `~Product` instances.
        """
        resp = requests.get(
            "{base_uri}{uri_part}/{prod_id}".format(
                base_url=self.gateway.base_uri,
                uri_part=self.__uri_part__,
                prod_id=prod_id),
            headers={
                'Accept-Charset': 'utf-8',
                'Accept': 'application/xml',
                'openapikey': self.gateway.api_key})

        try:
            return [Product(self.gateway, doc) for doc
                    in ElementTree.fromstring(resp.content)]

        except RequestException as e:
            raise UnknownApiResponseError(
                message="Couldn't communicate with the Elevenia API.",
                inner_exception=e)

        except ParseError as e:
            raise UnknownApiResponseError(
                message="Couldn't parse the response from the Elevenia API.",
                inner_exception=e)

    def list(self, page_num=1):
        """ Fetches a paginated listing of Products.

        :param `int` page_num: The page number to retrieve (defaults to 1).
        :return: `list` of `~Product` instances.
        """
        resp = requests.get(
            "{base_uri}{uri_part}/list?page={page_num}".format(
                base_url=self.gateway.base_uri,
                uri_part=self.__uri_part__,
                page_num=page_num),
            headers={
                'Accept-Charset': 'utf-8',
                'Accept': 'application/xml',
                'openapikey': self.gateway.api_key})

        try:
            return [Product(self.gateway, doc) for doc
                    in ElementTree.fromstring(resp.content)]

        except RequestException as e:
            raise UnknownApiResponseError(
                message="Couldn't communicate with the Elevenia API.",
                inner_exception=e)

        except ParseError as e:
            raise UnknownApiResponseError(
                message="Couldn't parse the response from the Elevenia API.",
                inner_exception=e)

    def new(self, **kwargs):
        """ Builds a new product which has not yet been posted to Elevenia.

        :return: A product entity that has not been posted to the
        API yet.
        :rtype: `~Product`
        :raises `ValueError`: If an invalid attribute name is passed in kwargs
        """
        product = Product(self.gateway, ElementTree.Element("Product"))

        # Assigns all the properties to new elevenia object.
        # If there is a default value specified, attempts to use that
        # default value if none is provided directly.
        for propname in dir(product.__class__):
            try:
                # get the property and make sure it has a value
                attr = getattr(product.__class__, propname)
                if not isinstance(attr, XmlProperty):
                    continue

                if propname in kwargs:
                    setattr(product, propname, kwargs.pop(propname))
                else:
                    default_value = getattr(attr, 'default') if hasattr(attr, 'default') else None
                    if default_value is not None:
                        setattr(product, propname, default_value)

            except (ValueError, TypeError):
                # silently swallow these.. nbd bro!
                pass

        # raise an exception if there are any kwargs passed that this class
        # does not have attributes for.
        if len(kwargs):
            raise ValueError(
                "Product does not have attribute(s): {}".format(
                    ",".join(kwargs))
            )

        return product

    def save(self):
        """ Validates then attempts to submit this instance to EleveniaI.

        This method has the side-effect of saving the returned message from
        Elevenia to its parent gateway's last_client_message property.
        **See:** `app.services.elevenia.entities.client_message.ClientMessage`

        :return: Nothing
        :rtype: `None`
        :raises: `app.services.elevenia.entities.exceptions.ValidationError` if
            client-side validation fails.
        :raises: :class:`~BaseApiResponseError` if server-side operation fails,
            or there was a network communication problem.
        """
        self.validate_all_properties()

        # Creating a new product or updating an existing one?
        if self.id is None:
            try:
                resp = requests.post(
                    self.gateway.base_uri + self.__uri_part__,
                    data=self.tostring(),
                    headers={
                        'Accept-Charset': 'utf-8',
                        'Accept': 'application/xml',
                        'openapikey': self.gateway.api_key,
                        'Content-Type': 'application/xml'})

                client_message = ClientMessage(
                    self.gateway, ElementTree.fromstring(resp.content)
                )
                self.gateway.last_client_message = client_message

                # test http status code -- if it's bad, raise an exception
                if resp.status_code != OK:
                    raise UnknownApiResponseError(
                        code=resp.status_code,
                        message=resp.text)

                else:
                    if client_message.result_code == OK:
                        self.id = client_message.product_id
                    else:
                        raise ApiOperationFailure(client_message)

            except RequestException as e:
                raise UnknownApiResponseError(
                    message="An HTTP exception occurred",
                    inner_exception=e
                )

        else:
            try:
                resp = requests.post(
                    "{base_uri}{partial}/{id}".format(
                        base_uri=self.gateway.base_uri,
                        partial=self.__uri_part__,
                        id=self.id
                    ),
                    data=self.tostring(),
                    headers={
                        'Accept-Charset': 'utf-8',
                        'Accept': 'application/xml',
                        'openapikey': self.gateway.api_key,
                        'Content-Type': 'application/xml'
                    })

                client_message = ClientMessage(
                    self.gateway, ElementTree.fromstring(resp.content)
                )
                self.gateway.last_client_message = client_message

                if resp.status_code != OK:
                    raise UnknownApiResponseError(
                        code=resp.status_code, message=resp.text)
                else:
                    if client_message.result_code == OK:
                        self.id = client_message.product_id
                    else:
                        raise ApiOperationFailure(client_message)

            except RequestException as e:
                raise UnknownApiResponseError(
                    message="An HTTP exception occurred",
                    inner_exception=e
                )

    def deactivate(self):
        try:
            resp = requests.put(
                '{base_uri}/prodstatservice/stat/stopdisplay/{elevenia_id}'.format(
                    base_uri=self.gateway.base_uri,
                    elevenia_id=self.id
                ),
                headers={
                    'Accept-Charset': 'utf-8',
                    'Accept': 'application/xml',
                    'openapikey': self.gateway.api_key,
                    'Content-Type': 'application/xml'
                })

            if resp.status_code != OK:
                raise UnknownApiResponseError(
                    code=resp.status_code, message=resp.text)
            else:
                client_message = ClientMessage(
                    self.gateway, ElementTree.fromstring(resp.content))

                if client_message.result_code == OK:
                    self.id = client_message.product_id
                else:
                    raise ApiOperationFailure(client_message)

        except RequestException as e:
            raise UnknownApiResponseError(
                message="An HTTP exception occurred",
                inner_exception=e
            )

    def __str__(self):
        return u"<Product(id:{0.id}, name:{0.name}))>".format(self)
