from .base import EleveniaEntity, XmlProperty, XmlIntegerProperty


class ClientMessage(EleveniaEntity):
    """ Response data returned from Elevenia to indicate the success or failure

    message
        A free-form english-language string indicating the status
    product_id
        The Elevenia product id associated with the operation :class:`Product`
        id attribute.
    result_code
        A pseudo HTTP status code that represents whether or not the operation
        succeeded or failed.
    """
    message = XmlProperty("Message")
    product_id = XmlIntegerProperty("productNo")
    result_code = XmlIntegerProperty("resultCode")

    def list(self, *args, **kwargs):
        raise NotImplementedError(
            "This method is not supported on ClientMessage")

    def new(self, **kwargs):
        raise NotImplementedError(
            "This method is not supported on ClientMessage")

    def save(self):
        raise NotImplementedError(
            "This method is not supported on ClientMessage")
