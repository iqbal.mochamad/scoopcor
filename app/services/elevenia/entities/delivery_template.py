from xml.etree import ElementTree

import requests

from .base import EleveniaEntity, XmlIntegerProperty, XmlProperty


class DeliveryTemplate(EleveniaEntity):
    """ Metadata about how this product will be delivered to users.

    .. note::

        For our purposes, our goods are delivered digtally, so this is
        completely irrelevant, however it must still be specified for all
        :class:`Products within in the Elevenia API.`

        A single delivery template should be created in the Elevania webstore
        manually (using the web interface) before we attempt to upload any
        of our products.

    id
        A unique ID assigned by Elevenia.
    name
from requests.exceptions import BaseHTTPError    """
    __uri_part__ = '/delivery/template'

    id = XmlIntegerProperty("dlvTmpltSeq")
    name = XmlProperty("dlvTmpltNm")

    def list(self):
        """ Fetch a list of all entities.

        There should only ever be a single one of these configured in our
        webstore.

        :return: `list` of :class:`DeliveryTemplate`
        """
        resp = requests.get(
            self.gateway.base_uri + self.__uri_part__,
            headers={
                'Accept-Charset': 'utf-8',
                'Accept': 'application/xml',
                'openapikey': self.gateway.api_key})

        return [DeliveryTemplate(self.gateway, doc) for doc
                in ElementTree.fromstring(resp.content).findall('.//template')]

    def new(self, **kwargs):
        raise NotImplementedError("DeliveryTemplates are Read-Only")

    def save(self):
        raise NotImplementedError("DeliveryTemplates are Read-Only")