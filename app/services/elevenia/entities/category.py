from xml.etree import ElementTree

import requests

from .base import (
    EleveniaEntity, XmlProperty, XmlIntegerProperty, XmlBooleanProperty
)


class Category(EleveniaEntity):
    """ A logical grouping for similar :class:`Product` entities.

    .. warning::

        Implementation: This endpoint differs from all other Elevenia
        XML responses in that the returned documents are namespaced.

    id
        Unique identifier of this entity within Elevenia.
    name
        A human-readable name for this entity.
    depth
        Indicates the number of parents this this entity has.  This
        'index' number is 1 (not zero) based.  So, depth=1 represents an
        entity with no parents.
    is_globally_deliverable
        Whether this entity can be delivered anywhere in the world.  For
        scoop items, this should always be True.
    parent_id
        Represents the parent (if any) id of this category.
    """
    __uri_part__ = '/cateservice/category'

    id = XmlIntegerProperty('dispNo')
    name = XmlProperty('dispNm')
    depth = XmlIntegerProperty('depth')
    is_globally_deliverable = XmlBooleanProperty('gblDlvYn')
    parent_id = XmlIntegerProperty('parentDispNo')

    @property
    def attributes(self):
        """ Additional metadata about this entity.

        This is a lazy-evaluated property that requires an additiaonl call
        to the Elevenia API to fetch it's data.

        :return: A list of all attributes for this category.
        :rtype: `list` of :class:`CategoryAttribute` entities.
        """
        if not hasattr(self, '_attributes'):

            resp = requests.get(
                '{}/cateservice/categoryAttributes/{}'.format(
                    self.gateway.base_uri,
                    self.id),
                headers={
                    'Accept-Charset': 'utf-8',
                    'Accept': 'application/xml',
                    'openapikey': self.gateway.api_key})

            attr_elements = ElementTree.fromstring(resp.text).findall(
                ".//ns2:productCtgrAttribute",
                {'ns2': 'http://skt.tmall.business.openapi.'
                        'spring.service.client.domain'})

            self._attributes = [CategoryAttribute(self.gateway, element, self)
                                for element in attr_elements]

        return self._attributes

    @property
    def children(self):
        """ List of all child categories (if present).

        :return: `list` of :class:`Category` objects which are grouped under
            this entity.
        """
        if not hasattr(self, '_children') or self._children is None:
            self._children = []
        return self._children

    @children.setter
    def children(self, value):
        self._children = value

    def list(self):
        """ Returns a list of category entities.

        Each Elevenia :class:`Product` must have EXACTLY 1 tier 3 category
        assigned to it (and no others).

        .. note::

            Implementation: Elevenia returns all categories in a flat structure,
            however they are logically nested into 3 tiers, so from the list
            method we return a recursively-nested structure of Categories.

        .. note::

            Unlike :class:`Product` entities, this endpoint IS NOT paginated,
            so calling this function will return a list of ALL categories.

        :return: List of all Elevenia categories.
        :rtype: `list` of `Category`
        """
        resp = requests.get(
            self.gateway.base_uri + self.__uri_part__,
            headers={
                'Accept-Charset': 'utf-8',
                'Accept': 'application/xml',
                'openapikey': self.gateway.api_key
            })

        all_categories = [Category(self.gateway, doc) for doc
                          in ElementTree.fromstring(resp.content)]

        top_level_categories = [cat for cat in all_categories if cat.depth == 1]

        def build_child_cats(category):
            children = [child_cat for child_cat in all_categories
                        if child_cat.parent_id == category.id]
            for child in children:
                child.children = build_child_cats(child)
            return children

        for tlc in top_level_categories:
            tlc.children = build_child_cats(tlc)

        return top_level_categories

    def new(self, **kwargs):
        raise NotImplementedError("Categories are Read-Only")

    def save(self):
        raise NotImplementedError("Categories are Read-Only")


class CategoryAttribute(EleveniaEntity):
    """ Additional metadata about a given :class:`Category`.

    .. note::

        We're not currently using this for anything at all.  We're only
        providing this additional sub-entity because it's present in the API.
    """

    def __init__(self, gateway, doc, category):
        """
            :param :class:`Elevenia` gateway: Gateway object used for communication
                with the Elevenia API.
            :param :class:`ElementTree` doc: Parsed XML document containing the
                underlying information for this instance.
            :param :class:`Category` category: Category object that this entity
                belongs to.
            """
        super(CategoryAttribute, self).__init__(gateway, doc)
        self.category = category

    id = XmlIntegerProperty("prdAttrNo")
    name = XmlProperty("prdAttrNm")
    code = XmlIntegerProperty("prdAttrCode")

    def new(self, **kwargs):
        raise NotImplementedError(
            "CategoryAttributes cannot be iterated directly. "
            "See the :meth:`Category.attributes` property.")

    def list(self, *args, **kwargs):
        raise NotImplementedError("CategoryAttributes are Read-Only")

    def save(self):
        raise NotImplementedError("CategoryAttributes are Read-Only")
