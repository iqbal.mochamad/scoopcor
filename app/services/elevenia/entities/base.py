"""
Elevenia Entity Base
====================

This module contains all the base entity and property types used to create
logical entities returned from/posted to the Elevenia API.

This constitutes a mini declarative framework that is specialized for use
with the Elevenia XML API.
"""
from six import text_type, string_types
from StringIO import StringIO
from xml.etree import ElementTree

from .exceptions import ValidationError


class EleveniaEntity(object):
    """ Base class for any objects sent/returned from the Elevenia API.

    This class should not be instantiated directly.
    """
    __uri_part__ = '/'

    def __init__(self, gateway, doc=None):
        """
        .. warning::

            Don't instantiate entities directly!  Use the
            :meth:`Elevenia.resource` to create new instances.

        :param :class:`Elevenia` gateway: Gateway dependency that will be used
            for retrieving metadata about the elevenia API.
        :param :class:`ElementTree` doc: The parsed XML document that contains
            the underlying representation of this instance.
        """
        self.gateway = gateway
        self.__xml_doc__ = doc

    def list(self, *args, **kwargs):
        """ Fetch a list of entities that match a provided set of arguments.

        :return: A list of retrieved entities from the Elevenia API.
        :rtype: `List` of `EleveniaEntity`
        """
        raise NotImplementedError("This method must be overridden.")

    def new(self, **kwargs):
        """ Creates a new entity that has not been saved to Elevenia.

        This method is used to construct a new entity that will be posted
        to Elevenia (NOT an entity that is retrived from Elevenia).

        :param kwargs: name-value pairs that are used to set the XmlProperties
            of this instance.  These must be valid property names for the type
            defined.
        :return:
        """
        raise NotImplementedError("This method must be overridden.")

    def save(self):
        """ Commits this instance to the Elevenia API.

        This is used for both creating new objects in the API, and updating
        existing entities in the API.
        """
        raise NotImplementedError("This method must be overridden.")

    def tostring(self):
        """ Returns this entity's underlying XML representation as a string.

        suitable for
        sending to the EleveniaAPI with a create/update operation.

        :return: A string-encoded XML document representation of this instance.
        :rtype: `str`
        """
        f = StringIO()

        etree = ElementTree.ElementTree(element=self.__xml_doc__)
        etree.write(f, xml_declaration=True, encoding='utf-8')
        text = f.getvalue()
        f.close()

        del etree
        del f

        return text

    def validate_all_properties(self):
        """ Runs the 'assert_is_valid` method on all XmlProperty attributes.

        An exception will be raised on the first invalid property discovered.

        :return: None
        :raises: :class:`ValidationError` if validation fails on any property.
        """
        props = \
            [prop for _, prop in vars(self.__class__).items()
                if isinstance(prop, XmlProperty)
                and hasattr(prop, 'assert_is_valid')]

        for p in props:
            p.assert_is_valid(self)


class XmlProperty(property):
    """ Specialized type for retrieving data/storing data in an XML doc.

    This is basically the workings of a mini declarative-framework specifically
    for Elevenia.
    """
    def __init__(self, name, element_type=text_type,
                 choices=None, required=None, default=None):
        """
        :param `str` name: The name of the tag that this property represents
            in the underlying XML document.
        :param `type` element_type: The python type that this element's text()
            data should map to.  Default `unicode`
        :param `list` choices: If provided, restricts the possible set of
            values that this property can be set to.  Default `None`.
        :param `bool` required: If True, this value must be set.  Otherwise,
            this this property is considered optional.  Default `False`.
        :param `object` default: The default value (if none is provided),
            that this value will be set to on initialization if no value is
            provided.
        """
        self.name = name
        self.required = required
        self.choices = choices
        self.element_type = element_type
        self.default = default

        def getter(entity):
            element = entity.__xml_doc__.find(name)
            value = element_type(element.text) if element is not None else self.default
            if isinstance(value, string_types):
                value = value.strip()
            return value

        def setter(entity, value):
            element = entity.__xml_doc__.find(name)
            if element is not None:
                # must convert to a string type for serialization
                element.text = text_type(value)
            else:
                entity.__xml_doc__.append(ElementTree.Element(name))
                setter(entity, value)

        super(XmlProperty, self).__init__(fget=getter, fset=setter)

    def assert_is_valid(self, entity):
        """ Performs local validation on this property's value.

        In practice this checks that Required properties are present, and
        that the current value matches one of the values in choices, if
        provided.

        :param `property` entity: A reference to this actual property.
        :return: None if validation succeeds.
        :raises: :class:`~ValidationError` If the property fails validation.
        """
        value = self.fget(entity)

        if self.required and value is None:
            raise ValidationError("{} is required".format(self.name))

        if self.choices and (value not in self.choices and value is not None):
            raise ValidationError(
                "Bad value ({}) for property: {}.  "
                "Valid choices are: {}".format(
                    value, self.name, ",".join([text_type(c) for c
                                                in self.choices])))


class XmlBooleanProperty(XmlProperty):
    """ Similar to the standard XmlProperty, but represents a boolean value.

    Elevenia's boolean representation is a capital Y for true, and capital N
    for false.

    .. note::

        This property does not support all the attributes of the standard
        XmlProperty (specifically, the choices attribute)
    """
    def __init__(self, name, required=None):
        """
        :param `str` name: The name of the tag that this property represents
            in the underlying XML document.
        :param `bool` required: If True, this value must be set.  Otherwise,
            this this property is considered optional.  Default `False`.
        """
        self.name = name
        self.required = required
        self.choices = []

        def getter(entity):
            element = entity.__xml_doc__.find(name)
            if element is None:
                return None
            else:
                return element.text.upper() == "Y"

        def setter(entity, value):
            element = entity.__xml_doc__.find(name)
            if element is not None:
                if value is None:
                    element.text = None
                else:
                    element.text = "Y" if value else "N"
            else:
                entity.__xml_doc__.append(ElementTree.Element(name))
                setter(entity, value)

        super(XmlProperty, self).__init__(fget=getter, fset=setter)


class XmlIntegerProperty(XmlProperty):
    """ Property that contains integer-type value.
    """
    def __init__(self, name, required=None, default=None):
        super(XmlIntegerProperty, self).__init__(
            name=name, element_type=int, required=required, default=default
        )


class XmlFloatProperty(XmlProperty):
    """ Property that contains a float-type value.
    """
    def __init__(self, name, required=None, default=None):
        super(XmlFloatProperty, self).__init__(
            name=name, element_type=float, required=required, default=default
        )
