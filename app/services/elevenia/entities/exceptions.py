class ValidationError(Exception):
    """ Raised when one of an Entity's properties fails to validate.

    .. note::

        This is client-side validation that happens BEFORE we submit to the
        Elevenia API.
    """
    pass


class BaseApiResponseError(Exception):
    """ Base class for any errors that occur server-side from Elevenia.
    """
    def __init__(self,
                 code=500,
                 message="There was a problem communicating with "
                         "the Elevenia API",
                 inner_exception=None):

        self.code = code
        self.message = message
        self.inner_exception = inner_exception


class ApiOperationFailure(BaseApiResponseError):
    """ Raised when we receive a valid response from the API, however the
    response indicated that the operation we requested failed.
    """
    def __init__(self, client_message_entity):
        super(ApiOperationFailure, self).__init__(
            code=client_message_entity.result_code,
            message=client_message_entity.message)


class UnknownApiResponseError(BaseApiResponseError):
    """ Raise when a non-200 repsonse status is returned from the Elevenia API.
    """
    pass


