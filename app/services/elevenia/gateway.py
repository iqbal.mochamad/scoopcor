from app.services.elevenia.constants import PRODUCTION_BASE_URI, \
    SANDBOX_BASE_URI


class Elevenia(object):

    def __init__(self, api_key, development_mode=False):
        self.development_mode = development_mode
        self.api_key = api_key
        self.__last_client_message = None

    @property
    def base_uri(self):
        return \
            SANDBOX_BASE_URI if self.development_mode else PRODUCTION_BASE_URI

    def resource(self, entity):
        return entity(self)

    @property
    def last_client_message(self):
        return self.__last_client_message

    @last_client_message.setter
    def last_client_message(self, value):
        self.__last_client_message = value
