"""
Elevenia Helpers
================

A set of simple functions used to instantiate Elevenia objects
"""
from app import app
from app.remote_publishing.choices import ELEVENIA
from app.remote_publishing.models import RemoteCategory
from app.services.elevenia.exceptions import NoMappableCategoriesError
from app.services.elevenia import DeliveryTemplate, Product
from app.services.elevenia.entities.choices.item_conditions import NEW
from app.services.elevenia.entities.choices.sale_types import READY_STOCK
from app.services.elevenia.entities.choices.tax_codes import NOT_TAXED
# from scripts.elevenia_items_publish import SINGLE_OFFER, SUBSCRIPTION_OFFER



WEB_PLATFORM = 4
OFFER_READY_FOR_SALE = 7
SINGLE_OFFER = 1
SUBSCRIPTION_OFFER = 2



def get_delivery_template(gateway):
    """ Gets the delivery template that will be used for items.

    Since we aren't actually delivering products, we're just going to grab the
    first delivery template and assume it's the only one.

    :param `app.services.elevenia.gateway.Elevenia` gateway: The gateway object
        that should be used to communicate with the Elevenia API.
    :return: The single delivery template that should be used with all items.
    :rtype: `app.services.elevenia.entities.delivery_template.DeliveryTemplate`
    """
    delivery_templates = gateway.resource(DeliveryTemplate).list()
    if len(delivery_templates):
        return delivery_templates[0]
    else:
        raise ValueError("No delivery templates uploaded yet!")


def get_is_sellable_to_minors(item):
    """ Returns True if this item is avaialble for sale to anyone under 17
    years of age.

    :param `app.items.model.Item` item: The item to check.
    :return: Boolean indication of whether or not this items is sellable
        to anyone under 17 years of age.
    """
    if item.parentalcontrol is None:
        return True
    else:
        # This is VERY hacky.  It would be cool if parental controls grew
        # a minimum age int field, or this should be an enum.
        return item.parentalcontrol.name != "17+"


def get_elevenia_category_id(item):
    """ Iterates a given Item's categories, and returns the first category
    that is found to have a remote mapping.

    :param `app.items.models.Item` item: The item to search for valid
        remote Elevenia category mappings.
    :return: The first valid remote Elevenia category.
    :rtype: `app.remote_publishing.models.RemoteCategory`
    :raises `~NoMappableCategoriesError`: If no valid remote categories could
        be found.
    """
    cat_list = [item.brand.primary_category,] if item.brand.primary_category else item.categories.all()


    for category in cat_list:
        remote_category = category.remote_categories\
            .filter(RemoteCategory.remote_service == ELEVENIA)\
            .first()

        if remote_category:
            # we store these as string values in the database, because we don't
            # care about their actual type in order to be as flexible as
            # possible to future remote stores.
            return int(remote_category.remote_id)

    raise NoMappableCategoriesError(
        u"""Item ID: {} does not have any known categories that map to
        valid Elevenia categories.  Cannot post to Elevenia""".format(item.id))


def build_elevenia_product_from_scoop_offer(offer, delivery_template, gateway):

    if offer.offer_type_id == SINGLE_OFFER:
        author = offer.items.first().authors.first()
        if author:
            author_name = ' by {author_name}'.format(
                author_name=author.name)
        else:
            author_name = ''

        name = u"[SCOOP Digital] {item_name}{author_name}".format(
            item_name=offer.items.first().name,
            author_name=author_name)

        nickname = offer.items.first().name
        scoop_id = u"SC00P{}".format(offer.id)
        sellable_to_minors = get_is_sellable_to_minors(offer.items.first())
        description = offer.items.first().description or " "
        image_highres = offer.items.first().image_highres
        price = int(offer.price_idr)
        category = get_elevenia_category_id(offer.items.first())
        if not all([price, image_highres, category]):
            raise ValueError("Offer {0.id} is missing a required field"
                ":\nprice={0.price_idr}\nimage_highres={1.image_highres}\ncategory={2}".format(offer, offer.items.first(), category))
    elif offer.offer_type_id == SUBSCRIPTION_OFFER:
        name = u"[SCOOP Digital] {}".format(offer.long_name)
        nickname = offer.long_name
        scoop_id = u"SC00P{}".format(offer.id)
        sellable_to_minors = get_is_sellable_to_minors(offer.brands.first().items.first())
        description = offer.subscription.first().description or " "
        image_highres = offer.brands.first().items.first().image_highres
        price = int(offer.price_idr)
        category = 4373 # get_elevenia_category_id(offer.brands.first().items.first())
        if not all([price, image_highres, category]):
            raise ValueError(
                "Offer {0.id} is missing a required field:\n"
                "price={0.price_idr}\n"
                "image_highres={1.image_highres}\n"
                "category={2}".format(offer, offer.items.first(), category))

    else:
        raise ValueError("Only single/subscription/buffet offers are currently supported.")

    # create and publish the new item to elevenia
    p = gateway.resource(Product).new(
        name=name,
        nickname=nickname,
        scoop_item_id=scoop_id,
        sale_type=READY_STOCK,
        weight_kilograms=1,  # BS required field
        quantity_in_stock=99999,  # BS required field..
        item_condition=NEW,
        vat_tax_code=NOT_TAXED,
        is_sellable_to_minors=sellable_to_minors,
        uses_sales_period=False,
        after_service_details="Contact person for scoop: "
                              "customercare@gramedia.com",
        return_or_exchange_details="Refunds/exchanges are not permitted for "
                                   "digital content.",
        has_delivery_guarantee=True,
        primary_image=u"{base}{img}".format(base=app.config['MEDIA_BASE_URL'],
                                           img=image_highres),
        html_detail=u"<img src='http://ebooks.gramedia.com/ext/img/sites/promo/elevenia-purchase-process.jpg' alt='To download your purchase, please sign into the scoop android or iphone app' />",

        delivery_template_id=delivery_template.id,
        display_category_id=category,

        price=price
    )

    return p
