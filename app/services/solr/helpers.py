from __future__ import unicode_literals, absolute_import
from abc import ABCMeta, abstractmethod
from six import with_metaclass

from app.utils.dicts import CorDefaultDict

__all__ = ['SolrQueryBuilder', 'SolrResponseFormatterBase', ]


class SolrQueryBuilder(object):
    """ Helper class to construct a query string for Solr /select endpoints.

    This is intended to be used within the context of a Flask request.
    """

    def __init__(self, flask_req, facet_list=None, default_limit=20, default_offset=0, version='1.0'):
        """
        :param `flask.Request` flask_req: The current Flask request object.
        :param `list` facet_list: A list of facet.field names to apply to the query.
        :param `int` default_limit:
            Number of records to retrieve from Solr if 'limit' not specified in the request.
        :param `int` default_offset:
            Number of records to skip from the beginning of the result set if 'offset' not
            specified in the request.
        """
        self.flask_req = flask_req
        self.facet_list = facet_list or []
        self.default_limit = default_limit
        self.default_offset = default_offset
        self.version = version

    def _build_query(self):
        return {'q': self.flask_req.args.get('q', '*')}

    def _build_filter_queries(self):
        return {'fq': ['{}:"{}"'.format(f.split(":")[0], f.split(":")[1]) for f in self.flask_req.args.getlist('facets')]}

    def _build_facet_fields(self):
        if self.version == '2.0':
            # remove facet whos return 0
            return {'facet.field': self.facet_list, 'facet.mincount': 1}
        else:
            return {'facet.field': self.facet_list}

    def _build_order(self):
        order = self.flask_req.args.get('order')
        if order:
            return {'sort': "{} asc".format(order) if not order.startswith("-") else "{} desc".format(order[1:])}
        else:
            return {}

    def _build_pagination(self):
        """ Returns a dictionary with pagination information for a Solr request

        :param `flask.Request` flask_req:
        :param `int` default_limit:
        :param `int` default_offset:
        :return: A dictionary that can be passed to the 'params' arg of 'requests' lib.
        :rtype: dict
        """
        return {
            'rows': self.flask_req.args.get('limit', self.default_limit, type=int),
            'start': self.flask_req.args.get('offset', self.default_offset, type=int)
        }

    def build(self):
        """ Constructs a dictionary of Query String parameters to be requested from Solr.

        Query parameters that are specified multiple times will be encapsulated in a list.

        :return: A dictionary containing all the non-url-escaped query string parameters.
        :rtype: dict
        """
        query = self._build_facet_fields()
        query.update(self._build_order())
        query.update(self._build_query())
        query.update(self._build_pagination())
        query.update(self._build_filter_queries())

        return query


class SolrResponseFormatterBase(with_metaclass(ABCMeta, object)):
    facet_name_mapper = CorDefaultDict()

    def __init__(self, response_json):
        """
        :param `dict` response_json:
        """
        self._response_json = response_json

    def _build_metadata_resultset(self):
        return {
            "metadata": {
                "resultset": {
                    "count": self._response_json['response']['numFound'],
                    "limit": len(self._response_json['response']['docs']),
                    "offset": self._response_json['response']['start']
                }
            }
        }

    def _build_metadata_facets(self):
        return {
            'metadata': {"facets": [{"field_name": self.facet_name_mapper[key],
                                     "values": [
                                         {"value": t[0], "count": t[1]} for t in zip(facet[0::2], facet[1::2])
                                         ]
                                     } for key, facet in
                                    self._response_json.get('facet_counts', {}).get('facet_fields', {}).items()]
                         }
        }

    def _build_spelling_suggestions(self):
        return {
            'metadata': {
                "spelling_suggestions": [{'value': d['word'],
                                          'count': d['freq']} for d in self._response_json
                                             .get("spellcheck", {})
                                             .get("suggestions", [None, {}])[1]
                                             .get("suggestion", [])
                                         ]

            }
        }

    @abstractmethod
    def _build_entities(self):
        pass

    def build(self):
        response_json = {}
        for builder_method in [m for m in dir(self) if m.startswith('_build_')]:
            method = getattr(self, builder_method)
            response_json.update(method())
        return response_json
