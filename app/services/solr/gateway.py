from __future__ import unicode_literals, absolute_import

import json

import requests

__all__ = ['SolrGateway', ]


class SolrGateway(object):

    def __init__(self, solr_base_url, core_name, http_client=requests):
        """
        :param `six.string_types` solr_base_url:
        :param `six.string_types` core_name:
        :param `requests` http_client:
        """
        self.solr_base_url = solr_base_url
        self.core_name = core_name
        self.http_client = http_client

    def search(self, query_dict):

        resp = self.http_client.get(
            '{solr_base_url}/{core_name}/select'.format(**vars(self)),
            params=query_dict)
        return resp.json()

    def update(self, data):
        params = {"boost": "1.0",
                  "commitWithin": 1000,
                  "overwrite": "true",
                  "wt": "json"
                  }
        resp = self.http_client.post(
            '{solr_base_url}/{core_name}/update'.format(**vars(self)),
            headers={'Content-Type': 'application/json'},
            params=params,
            data=json.dumps(data))

        return resp.json()

    def suggest(self):
        pass

    def delete_all(self):
        pass
