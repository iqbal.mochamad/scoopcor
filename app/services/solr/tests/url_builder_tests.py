from __future__ import absolute_import, unicode_literals
from unittest import TestCase

from flask import Flask
from mock import MagicMock

from app.services.solr.helpers import SolrQueryBuilder

fake_app = Flask(__name__)


class SolrUrlBuilderTests(TestCase):

    def test_solr_pagination_builder(self):

        with fake_app.test_request_context('/?offset=99&limit=4') as ctx:

            self.assertDictEqual(
                SolrQueryBuilder(ctx.request)._build_pagination(),
                {'rows': 4, 'start': 99}
            )

    def test_query_param_with_value(self):

        with fake_app.test_request_context('/?q=Ramdani is Lucu %26 Stuff') as ctx:

            self.assertDictEqual(
                SolrQueryBuilder(ctx.request)._build_query(),
                {'q': 'Ramdani is Lucu & Stuff'}
            )

    def test_query_param_without_value(self):
        with fake_app.test_request_context('/') as ctx:
            self.assertDictEqual(
                SolrQueryBuilder(ctx.request)._build_query(),
                {'q': '*'}
            )

    def test_query_fq_with_value(self):
        with fake_app.test_request_context('/?facets=author_names:test aja&'
                                           'facets=category_names:Science %26 Fiction') as ctx:
            self.assertDictEqual(
                SolrQueryBuilder(ctx.request)._build_filter_queries(),
                {'fq': ['author_names:"test aja"', 'category_names:"Science & Fiction"']}
            )

    def test_query_fq_without_value(self):
        with fake_app.test_request_context('/') as ctx:
            self.assertDictEqual(
                SolrQueryBuilder(ctx.request)._build_filter_queries(),
                {'fq': []}
            )

    def test_query_facet_field(self):
        with fake_app.test_request_context('/') as ctx:
            self.assertDictEqual(
                SolrQueryBuilder(
                    ctx.request,
                      ["author_names",
                       "category_names",
                       "item_languages"])._build_facet_fields(),
                {"facet.field": ["author_names",
                                 "category_names",
                                 "item_languages"]})

    def test_query_order_asc(self):
        with fake_app.test_request_context('/?order=brand_name') as ctx:
            self.assertDictEqual(
                SolrQueryBuilder(ctx.request)._build_order(),
                {"sort": "brand_name asc"}
            )

    def test_query_order_desc(self):
        with fake_app.test_request_context('/?order=-brand_name') as ctx:
            self.assertDictEqual(
                SolrQueryBuilder(ctx.request)._build_order(),
                {"sort": "brand_name desc"}
            )

    def test_query_order_without_value(self):
        with fake_app.test_request_context('/') as ctx:
            self.assertDictEqual(
                SolrQueryBuilder(ctx.request)._build_order(),
                {}
            )

    def test_build_query(self):

        with fake_app.test_request_context('/') as ctx:

            query_builder = SolrQueryBuilder(ctx.request)

            mock_methods = {}

            for build_method_name in ['_build_query', '_build_filter_queries', '_build_facet_fields',
                                      '_build_order', '_build_pagination']:

                mock_method = MagicMock(return_value={build_method_name: 1})

                mock_methods[build_method_name] = mock_method

                setattr(query_builder, build_method_name, mock_method)

            query_params = query_builder.build()

            for name in mock_methods:
                self.assertTrue(mock_methods[name].called, msg="Expected {} to be called".format(name))

            self.assertDictEqual(
                query_params,
                {
                    '_build_query': 1,
                    '_build_filter_queries': 1,
                    '_build_facet_fields': 1,
                    '_build_order': 1,
                    '_build_pagination': 1,
                }
            )

