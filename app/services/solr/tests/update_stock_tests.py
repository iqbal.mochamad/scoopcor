from unittest import TestCase

from mock import MagicMock

from app.eperpus.solr import StockSolr
from app.services.solr import SolrGateway


data = {"facet_counts":
            {"facet_heatmaps": {},
             "facet_ranges": {},
             "facet_fields": {},
             "facet_queries": {},
             "facet_intervals": {}
             }, "spellcheck":
    {"correctlySpelled": False, "suggestions": []},
        "responseHeader":
            {"status": 0, "QTime": 3, "params":
                {"q": "item_id:90494", "fl": "*"}
             }, "response":
            {"start": 0,
             "numFound": 1,
             "docs": [
                 {
                     "stock": 2,
                     "category_names": ["Children\'s", "Religion & Spirituality"],
                     "brand_name_sortable": "Princess Shalehah 1: Kumpulan Kisah Princes Muslimah",
                     "item_thumb_highres": "https://images.apps-foundry.com/magazine_static/images/1/28360/small_covers/ID_BHIP2015MTH11PSKKPM_S_.jpg",
                     "item_name": ["Princess Shalehah 1: Kumpulan Kisah Princes Muslimah"],
                     "item_languages": ["ind"],
                     "org_id": 711, "brand_name": "Princess Shalehah 1: Kumpulan Kisah Princes Muslimah",
                     "catalog_ids_combined": "1", "author_names": ["Dian K."], "item_type": "book",
                     "item_thumb_normal": "https://images.apps-foundry.com/magazine_static/images/1/28360/general_small_covers/ID_BHIP2015MTH11PSKKPM_S_.jpg",
                     "item_release_date": "2015-11-16T00:00:00Z", "item_id": 90494, "_version_": 1540182294681092096,
                     "vendor_name": "Bhuana Ilmu Populer", "catalog_ids": [1]}
             ]}
        }


class RequestSolrTests(TestCase):

    def setUp(self):
        fake_solr = MagicMock(spec=SolrGateway)
        fake_solr.search = lambda *args, **kwargs: data
        fake_solr.update = lambda *args, **kwargs: {'some-key': {'sub-key': 15, 'status': 0}}

        self.stocked_solr = StockSolr(solr=fake_solr)

    def test_select(self):
        actual = self.stocked_solr.select_item(90494, 1, ['item_id', 'stock'])
        expected = {
             "stock": 2,
             "category_names": ["Children\'s", "Religion & Spirituality"],
             "brand_name_sortable": "Princess Shalehah 1: Kumpulan Kisah Princes Muslimah",
             "item_thumb_highres": "https://images.apps-foundry.com/magazine_static/images/1/28360/small_covers/ID_BHIP2015MTH11PSKKPM_S_.jpg",
             "item_name": ["Princess Shalehah 1: Kumpulan Kisah Princes Muslimah"],
             "item_languages": ["ind"],
             "org_id": 711, "brand_name": "Princess Shalehah 1: Kumpulan Kisah Princes Muslimah",
             "catalog_ids_combined": "1", "author_names": ["Dian K."], "item_type": "book",
             "item_thumb_normal": "https://images.apps-foundry.com/magazine_static/images/1/28360/general_small_covers/ID_BHIP2015MTH11PSKKPM_S_.jpg",
             "item_release_date": "2015-11-16T00:00:00Z", "item_id": 90494, "_version_": 1540182294681092096,
             "vendor_name": "Bhuana Ilmu Populer", "catalog_ids": [1]
        }
        self.assertDictEqual(actual, expected)

    def test_parameter_select(self):
        actual = self.stocked_solr._set_params(90494, 1, '*')
        expected = {
            "fq": ["catalog_ids:1"],
            "q": "item_id:90494",
            "fl": "*"
        }
        self.assertDictEqual(actual, expected)

    def test_update_stock_with_value(self):
        item = {'quantity_available': 10}
        actual = self.stocked_solr._update_stock(item, 2)
        expected = {'quantity_available': 12}
        self.assertItemsEqual(actual, expected)

    def test_update_stock_with_parameter(self):
        item = {'quantity_available': 10}
        actual = self.stocked_solr._update_stock(item, 10)
        expected = {'quantity_available': {'inc': 10}}
        self.assertDictEqual(actual, expected)

    def test_increment_stock(self):
        item = {'item_id': 10, 'quantity_available': 1}
        self.stocked_solr._update_stock = MagicMock(return_value={'item_id': 10, 'quantity_available': 2})

        expected = {'some-key': {'sub-key': 15, 'status': 0}}

        actual = self.stocked_solr.increment_stock(item)

        self.stocked_solr._update_stock.assert_called_with(item, 1)
        self.assertDictEqual(actual, expected)

    def test_decrement_stock(self):
        item = {'item_id': 10, 'quantity_available': 1}
        self.stocked_solr._update_stock = MagicMock(return_value={'item_id': 10, 'quantity_available': 0})

        expected = {'some-key': {'sub-key': 15, 'status': 0}}

        actual = self.stocked_solr.decrement_stock(item)

        self.stocked_solr._update_stock.assert_called_with(item, -1)
        self.assertDictEqual(actual, expected)


