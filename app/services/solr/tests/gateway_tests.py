from __future__ import unicode_literals, absolute_import
from unittest import TestCase

import requests
from mock import MagicMock

from app.services.solr.gateway import SolrGateway


class SolrGatewayTests(TestCase):

    def test_search_http_request(self):
        fake_http_client = MagicMock(spec=requests)

        gateway = SolrGateway('http://cor.local', 'some-core', fake_http_client)

        query_args = {'some': 'dict', 'with': 'values'}

        gateway.search(query_args)

        self.assertTrue(fake_http_client.get.called)

        args, kwargs = fake_http_client.get.call_args

        self.assertTupleEqual(args, ('http://cor.local/some-core/select', ))
        self.assertDictEqual(kwargs, {'params': query_args})

    def test_return_json(self):

        fake_http_client = MagicMock(spec=requests)

        fake_http_response = MagicMock(spec=requests.Response)
        fake_http_response.json = lambda *args, **kwargs: {'some': 'json', 'from': 'solr'}

        fake_http_client.get.return_value = fake_http_response

        gateway = SolrGateway('http://cor.local', 'some-core', fake_http_client)

        result_json = gateway.search({'some': 'dict', 'with': 'values'})

        self.assertDictEqual(
            result_json,
            {'some': 'json', 'from': 'solr'}
        )


