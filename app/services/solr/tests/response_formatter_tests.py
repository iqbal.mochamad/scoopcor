from __future__ import absolute_import, unicode_literals
from unittest import TestCase

from app.helpers import SchemaType
from app.services.solr.helpers import SolrResponseFormatterBase
from tests.fixtures.solr.factory import solr_response_fixture


class MetadataFormatter(SolrResponseFormatterBase):

    def _build_entities(self):
        return {}


class SolrResponseFormatterBaseTests(TestCase):

    def setUp(self):
        self.fixture = solr_response_fixture('complete-response', SchemaType.select)
        self.formatter = MetadataFormatter(self.fixture)

    def test_response_format(self):
        pass

    def test_metadata_resultset(self):
        pass

    def test_metadata_facets(self):
        pass

    def test_metadata_spellling_suggestions(self):
        pass


#
# def setUp(self):
#     self.session = db.session()
#
#
# def test_response_format(self):
#     schema = load_json_schema('users', 'catalog-items', SchemaType.select)
#     response_json = format_catalog_items_response(None,
#                                                   solr_response_fixture("scoop-sharedlib.json"))
#     jsonschema.validate(response_json, schema)
#
#
# def test_metadata_response(self):
#     fake_solr_response = solr_response_fixture("scoop-sharedlib.json")
#
#     response_json = format_catalog_items_response(self.session, fake_solr_response)
#
#     self.assertDictEqual(
#         response_json['metadata']['resultset'],
#         {
#             'offset': 0,
#             'limit': 20,
#             'count': 653
#         }
#     )
#
#
# def test_facets_response(self):
#     fake_solr_response = solr_response_fixture("scoop-sharedlib.json")
#     response_json = format_catalog_items_response(self.session, fake_solr_response)
#     self.assertItemsEqual(
#         response_json['metadata']['facets'],
#         [{
#             "field_name": "item_categories",
#             "values": [
#                 {
#                     "value": "School",
#                     "count": 103,
#                 },
#                 {
#                     "value": "Politics & Current Affairs",
#                     "count": 47,
#                 },
#             ]
#         },
#             {
#                 "field_name": "item_authors",
#                 "values": [
#                     {
#                         "value": "Togi",
#                         "count": 9,
#                     },
#                     {
#                         "value": "Ramdani",
#                         "count": 8,
#                     }
#
#                 ]
#             },
#             {
#                 "field_name": "item_languages",
#                 "values": [
#                     {
#                         "value": "ind",
#                         "count": 646,
#                     },
#                     {
#                         "value": "eng",
#                         "count": 7,
#                     }
#                 ]
#             }]
#     )
#
#
# def test_spelling_suggestion_response(self):
#     fake_solr_response = solr_response_fixture("scoop-sharedlib-spellsuggestion.json")
#     response_json = format_catalog_items_response(self.session, fake_solr_response)
#     self.assertItemsEqual(
#         response_json['metadata']['spelling_suggestions'],
#         [{
#             "value": "jakarta",
#             "count": 1660,
#         },
#             {
#                 "value": "jakarta!",
#                 "count": 1,
#             }]
#     )
