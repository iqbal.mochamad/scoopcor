from datetime import datetime
from .models import PointUser

from dateutil.relativedelta import relativedelta

def create_point_data(user_id=None, point=None):

    point_user = PointUser.query.filter_by(user_id=user_id).first()

    if point_user:
        # add points
        point_user.point_amount = point_user.point_amount + point
        point_user.save()
    else:
        new_point_user = PointUser(
            user_id = user_id,
            client_id = 1,
            point_amount = point,
            point_used = 0,
            acquire_datetime = datetime.now(),
            expired_datetime = datetime.now() + relativedelta(months=12),
            is_active = True
        )
        new_point_user.save()
