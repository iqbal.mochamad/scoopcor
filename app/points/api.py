import json, httplib
from sqlalchemy.sql import desc

from datetime import datetime
from flask_restful import Resource
from flask_restful import reqparse
from flask import Response, request

from app.auth.decorators import token_required

from app.helpers import err_response, date_validator
from app.helpers import conflict_message, internal_server_message

from app.points.models import PointUser, PointUse, PointRegister, PointExpired
from app.items.models import ItemRegister
from app.custom_get import CustomGet
from .helpers import create_point_data

from app.messaging.email import HtmlEmail
from app.messaging.email import get_smtp_server

from dateutil.relativedelta import relativedelta

from app import db

from . import _log

class PointUserListApi(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('client_id', type=int, required=True, location='json')
        self.reqparser.add_argument('point_amount', type=int, required=True, location='json')
        self.reqparser.add_argument('point_used', type=int, required=True, location='json')
        self.reqparser.add_argument('acquire_datetime', type=str, required=True, location='json')
        self.reqparser.add_argument('expired_datetime', type=str, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')

        super(PointUserListApi, self).__init__()

    def check_date(self, acquire_datetime=None, expired_datetime=None):
        acquire_datetime = date_validator(acquire_datetime)
        if acquire_datetime.get('invalid', False):
            return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                error_code=httplib.BAD_REQUEST,
                developer_message="Invalid acquire_datetime",
                user_message="acquire_datetime invalid")

        expired_datetime = date_validator(expired_datetime)
        if expired_datetime.get('invalid', False):
            return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                error_code=httplib.BAD_REQUEST,
                developer_message="Invalid expired_datetime",
                user_message="expired_datetime invalid")

        if expired_datetime <= acquire_datetime:
            return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                error_code=httplib.BAD_REQUEST,
                developer_message="Invalid expired_datetime must greater than acquire_datetime",
                user_message="expired_datetime invalid")

        return 'OK', 'OK'

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_global_user_point')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = CustomGet(param, 'points', 'POST').construct()
            return scene

        except Exception, e:
            return internal_server_message(modul="point user", method="GET",
                error=str(e), req='')

    @token_required('can_read_write_global_all, can_read_write_global_user_point')
    def post(self):
        args = self.reqparser.parse_args()

        pts = PointUser.query.filter_by(user_id=args['user_id']).first()
        if pts:
            return conflict_message(modul="user points", method="POST",
                conflict=args['user_id'], req=args)

        chck_date = self.check_date(args['acquire_datetime'], args['expired_datetime'])
        if chck_date[0] != 'OK':
            return chck_date[1]

        #construct in same dict
        kwargs = {}
        for item in args:
            if args[item] == None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            m = PointUser(**kwargs)
            sv = m.save() #if this return invalid True raise exceptions
            if sv.get('invalid', False):
                return internal_server_message(modul="point user", method="POST",
                    error=sv.get('message', ''), req=str(args))
            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="point user", method="POST",
                error=e, req=str(args))

class PointUserApi(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('client_id', type=int, required=True, location='json')
        self.reqparser.add_argument('point_amount', type=int, required=True, location='json')
        self.reqparser.add_argument('point_used', type=int, required=True, location='json')
        self.reqparser.add_argument('acquire_datetime', type=str, required=True, location='json')
        self.reqparser.add_argument('expired_datetime', type=str, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')

        super(PointUserApi, self).__init__()

    def check_date(self, acquire_datetime=None, expired_datetime=None):
        acquire_datetime = date_validator(acquire_datetime)
        if acquire_datetime.get('invalid', False):
            return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                error_code=httplib.BAD_REQUEST,
                developer_message="Invalid acquire_datetime",
                user_message="acquire_datetime invalid")

        expired_datetime = date_validator(expired_datetime)
        if expired_datetime.get('invalid', False):
            return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                error_code=httplib.BAD_REQUEST,
                developer_message="Invalid expired_datetime",
                user_message="expired_datetime invalid")

        if expired_datetime <= acquire_datetime:
            return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                error_code=httplib.BAD_REQUEST,
                developer_message="Invalid expired_datetime must greater than acquire_datetime",
                user_message="expired_datetime invalid")

        return 'OK', 'OK'

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_global_user_point')
    def get(self, point_id=None):
        m = PointUser.query.filter_by(id=point_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        rv = json.dumps(m.values())
        return Response(rv, status=httplib.OK, mimetype='application/json')

    @token_required('can_read_write_global_all, can_read_write_global_user_point')
    def put(self, point_id=None):
        args = self.reqparser.parse_args()

        m = PointUser.query.filter_by(id=point_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        if m.user_id != args['user_id']:
            pts = PointUser.query.filter_by(user_id=args['user_id']).first()
            if pts:
                return conflict_message(modul="user points", method="PUT",
                    conflict=args['user_id'], req=args)

        chck_date = self.check_date(args['acquire_datetime'], args['expired_datetime'])
        if chck_date[0] != 'OK':
            return chck_date[1]

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            f = PointUser.query.get(point_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="point user", method="PUT",
                    error=sv.get('message', ''), req=str(args))

            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="point user", method="PUT",
                error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, point_id=None):
        f = PointUser.query.filter_by(id=point_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=httplib.NOT_FOUND)

class PointSummaryApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def get(self, user_id=None):
        m = PointUser.query.filter_by(user_id=user_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        rv = json.dumps(m.mobile_values())
        return Response(rv, status=httplib.OK, mimetype='application/json')

class MigratePoints(Resource):

    """
        FOR MIGRATE POINTS (TEMPORARY API BY: KIRA)
    """

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        self.reqparser.add_argument('sign', type=str, required=True, location='json')
        self.reqparser.add_argument('point', type=int, required=True, location='json')
        self.reqparser.add_argument('client_id', type=int, location='json')

        super(MigratePoints, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def post(self, user_id=None):
        try:
            args = self.reqparser.parse_args()

            sign = args.get('sign', None)
            user_points = PointUser.query.filter_by(user_id=user_id).first()

            if sign == 'ADD':
                if not user_points:
                    sv = PointUser(
                        user_id = user_id,
                        client_id = args.get('client_id', 1),
                        point_amount = args['point'],
                        point_used = 0,
                        acquire_datetime = datetime.now(),
                        expired_datetime = datetime.now() + relativedelta(months=12),
                        is_active = True
                    )
                    sv.save()
                else:
                    user_points.point_amount = user_points.point_amount + args['point']
                    user_points.save()

            if sign == 'DEC':
                point_used = user_points.point_amount - int(args['point'])
                if point_used <= 0:
                    point_used = 0

                user_points.point_amount = point_used
                user_points.point_used = user_points.point_used + int(args['point'])
                user_points.save()

                sv = PointUse(
                    client_id = args.get('client_id', 1),
                    user_id = user_id,
                    use_datetime = datetime.now(),
                    amount = int(args['point']),
                    is_active = True
                )
                sv.save()

            return Response(json.dumps(args), status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="MigratePoints", method="POST",
                error=e, req='')

class PointProfileUpdate(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')

        self.reqparser.add_argument('user_refered_id', type=int, location='json')
        self.reqparser.add_argument('is_referral', type=bool, location='json')
        self.reqparser.add_argument('is_profile', type=bool, location='json')

        super(PointProfileUpdate, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def post(self):

        args = self.reqparser.parse_args()

        try:
            register_id = args.get('user_id', 0)
            referred_id = args.get('user_refered_id', 0)
            referred_point, profile_point, point = 0, 0, 0

            is_referral = args.get('is_referral', False)
            is_profile = args.get('is_profile', False)

            data_point = ItemRegister.query.filter_by(name='point_reward').first()
            history = PointRegister.query.filter_by(user_id=register_id).first()

            # this is first time register
            if not history:
                new_history = PointRegister(user_id = register_id)

                if is_referral:
                    referred_point = data_point.point_referral
                    point = point + referred_point
                    new_history.refered_user_id = referred_id

                    # add new points user first register
                    create_point_data(register_id, referred_point)

                    # update point for referred id
                    # THIS IS DISABLE, REFERRED USER WILL GET POINT AFTER
                    # NEW USER IS VERIFIED
                    # create_point_data(referred_id, referred_point)

                if is_profile:
                    profile_point = data_point.point
                    point = point + profile_point

                    # add new points first register
                    create_point_data(register_id, profile_point)

                new_history.is_profile = is_profile
                new_history.is_referal = is_referral
                new_history.point = point
                new_history.save()

            else:

                # this is only for update profiles
                if not history.is_profile:
                    profile_point = data_point.point

                    history.point = history.point + profile_point
                    history.is_profile = True
                    history.save()

                    create_point_data(register_id, profile_point)

            return Response(None, status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            internal_server_message(modul="point profile updated",
                method="POST", error=e, req=str(args))
            pass

        return Response(None, status=httplib.OK, mimetype='application/json')

class PointProfileVerified(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('user_referred_id', type=int, required=True, location='json')

        super(PointProfileVerified, self).__init__()

    def post(self):

        args = self.reqparser.parse_args()

        try:
            data_point = ItemRegister.query.filter_by(name='point_reward').first()
            if data_point:
                create_point_data(args['user_referred_id'], data_point.point_referral)
                return Response(None, status=httplib.OK, mimetype='application/json')

        except Exception, e:
            internal_server_message(modul="point profile verified",
                method="POST", error=e, req=str(args))
            pass

class PointEmailViewModel(object):
    def __init__(self, point_data):
        self.point_data = point_data
        self.customer_email = point_data.users.email
        self.point_cut = point_data.point_amount #app.config['POINT_CUT']
        self.current_point = point_data.point_amount


    @property
    def cut_datetime(self):
        return self.point_data.cut_datetime.strftime('%d %b %Y')

    @property
    def remaining_points(self):
        remaining = self.current_point - self.point_cut
        if remaining <= 0:
            return 0
        return remaining

def pointexpirednotification(user_id=None, limit=50):
    try:
        if user_id:
            all_expired_users = PointUser.query.filter(
                PointUser.user_id == user_id,
                datetime.utcnow() >= PointUser.expired_datetime,
                PointUser.notify_expired == False,
                PointUser.point_amount > 0
            ).all()
        else:
            # retrieve all users who has already expired
            all_expired_users = PointUser.query.filter(
                datetime.utcnow() >= PointUser.expired_datetime,
                PointUser.notify_expired == False,
                PointUser.point_amount > 0
            ).order_by(desc(PointUser.id)).limit(limit).all()

        # send email notifications
        for ipoint in all_expired_users:
            try:
                if not ipoint.notify_expired:
                    server = get_smtp_server()
                    msg = HtmlEmail(
                        'Gramedia Digital <no-reply@gramedia.com>',
                        [ipoint.users.email, ],
                        'Your Gramedia Digital Point is expiring!',
                        'email/points/point_expired_notifications.html',
                        'email/points/point_expired_notifications.txt',
                        context=PointEmailViewModel(ipoint))
                    server.send_message(message=msg, log_to_solr=False)

                    #update this user already notified
                    ipoint.notify_expired = True
                    db.session.add(ipoint)
                    db.session.commit()

            except Exception as e:
                _log.exception("Send point notification, settings are misconfigured. Error: {}".format(str(e)))

    except Exception as e:
        _log.exception("Send pointexpirednotification. Error: {}".format(str(e)))


def pointexpiredcut(user_id=None, limit=50):

    try:
        # retrieve all user who has already notify about expired point,
        # and cut datetime overdue.
        if user_id:
            all_expired_users = PointUser.query.filter(
                PointUser.user_id == user_id,
                datetime.utcnow() >= PointUser.cut_datetime,
                PointUser.notify_expired == True,
                PointUser.point_amount > 0
            ).all()
        else:
            all_expired_users = PointUser.query.filter(
                datetime.utcnow() >= PointUser.cut_datetime,
                PointUser.notify_expired == True,
                PointUser.point_amount > 0
            ).order_by(desc(PointUser.id)).limit(limit).all()

        for ipoint in all_expired_users:

            if ipoint.point_amount > 0:
                # send the emails for notification, point has been cutted
                server = get_smtp_server()
                msg = HtmlEmail(
                    'Gramedia Digital <no-reply@gramedia.com>',
                    [ipoint.users.email, ],
                    'Your Gramedia Digital Point is already expired',
                    'email/points/point_cut_notifications.html',
                    'email/points/point_cut_notifications.txt',
                    context=PointEmailViewModel(ipoint))
                server.send_message(message=msg, log_to_solr=False)

                # record soo, we know how much user has ben cut before
                record_cut = PointExpired(
                    user_id=ipoint.user_id,
                    cut_point=ipoint.point_amount,
                    cut_datetime=datetime.utcnow())
                db.session.add(record_cut)

                # reset point user to 0
                ipoint.point_amount = 0
                db.session.add(ipoint)
                db.session.commit()

    except Exception as e:
        print e
        _log.exception("Send pointexpiredcut. Error: {}".format(str(e)))


class QAexpiredNotification(Resource):
    def get(self, user_id=None):
        pointexpirednotification(user_id)
        return "SUCCESS"

class QACutexpiredNotification(Resource):
    def get(self, user_id=None):
        pointexpiredcut(user_id)
        return "SUCCESS"
