from flask_appsfoundry.models import TimestampMixin
from app import db


class PointUser(db.Model, TimestampMixin):

    __tablename__ = 'core_userpoints'

    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'))
    users = db.relationship('User', backref='userpoints')
    client_id = db.Column(db.Integer, nullable=False)
    point_amount = db.Column(db.Integer, nullable=False)
    point_used = db.Column(db.Integer)
    acquire_datetime = db.Column(db.DateTime(), nullable=False)
    expired_datetime = db.Column(db.DateTime(), nullable=False)
    cut_datetime = db.Column(db.DateTime())
    notify_expired = db.Column(db.Boolean())
    is_active = db.Column(db.Boolean(), nullable=False)

    def __repr__(self):
        return '<client_id : %s, user_id : %s>' % (self.client_id, self.user_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Point user added successfully"}
        except Exception, e:
            print "save error??", e
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Point user deleted successfully"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def mobile_values(self):
        rv = {}

        rv['user_id'] = self.user_id
        rv['available_points'] = self.point_amount
        rv['used_points'] = self.point_used

        return rv

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['user_id'] = self.user_id
        rv['client_id'] = self.client_id
        rv['point_amount'] = self.point_amount
        rv['point_used'] = self.point_used
        rv['acquire_datetime'] = self.acquire_datetime.isoformat()
        rv['expired_datetime'] = self.expired_datetime.isoformat()
        rv['is_active'] = self.is_active
        return rv


class PointUse(db.Model, TimestampMixin):

    __tablename__ = 'core_pointuses'

    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.String(80), nullable=False)
    user_id = db.Column(db.Integer)
    use_datetime = db.Column(db.DateTime)
    amount = db.Column(db.Integer)
    is_active = db.Column(db.Boolean(), nullable=False, default=True)
    order_id = db.Column(db.Integer)

    def __repr__(self):
        return '<client_id : %s, user_id : %s>' % (self.client_id, self.user_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Point used added successfully"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    #def get_points_sum(self):
    #    total = 0
    #
    #    # Distinct by user_id TO GET TOTAL
    #    #rv = self.__class__.query.filter(self.__class__.user_id==self.user_id).distinct('user_id').all()
    #    rv = self.__class__.query.filter(self.__class__.user_id==self.user_id,
    #                                     self.__class__.is_active==True).all()
    #    for i in rv:
    #        total += i.amount
    #
    #    return total

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Point used deleted successfully"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['client_id'] = self.client_id
        rv['user_id'] = self.user_id
        rv['use_datetime'] = self.use_datetime.isoformat()
        rv['amount'] = self.amount
        rv['is_active'] = self.is_active
        rv['order_id'] = self.order_id

        return rv


class PointUsedDetail(db.Model, TimestampMixin):

    __tablename__ = 'core_pointusedetails'

    id = db.Column(db.Integer, primary_key=True)
    pointuse_id = db.Column(db.Integer, db.ForeignKey('core_pointuses.id'))
    detail = db.Column(db.Text)

    def __repr__(self):
        return '<point_use : %s>' % (self.pointuse_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Point used detail added successfully"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Point used detail deleted successfully"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['point_use_id'] = self.pointuse_id
        rv['detail'] = self.detail
        return rv


class PointRegister(db.Model, TimestampMixin):

    """
        This model for store records when user edit their profiles, got 50 points
        https://trello.com/c/hRGI0Uj7
    """

    __tablename__ = 'core_pointprofiles'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    refered_user_id = db.Column(db.Integer)
    point = db.Column(db.Integer)
    is_referal = db.Column(db.Boolean())
    is_profile = db.Column(db.Boolean())

    def __repr__(self):
        return '<user_id : %s>' % (self.user_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Point Profiles added successfully"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Point Profiles deleted successfully"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['user_id'] = self.user_id
        rv['point'] = self.point
        rv['is_referal'] = self.is_referal
        rv['is_profile'] = self.is_profile

        return rv


class PointExpired(db.Model):

    __tablename__ = 'core_pointcutexpires'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    cut_point = db.Column(db.Integer)
    cut_datetime = db.Column(db.DateTime(), nullable=False)
