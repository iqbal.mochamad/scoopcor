from flask import Blueprint
from flask.ext import restful

from .api import PointUserListApi, PointUserApi, PointSummaryApi, MigratePoints, \
    PointProfileUpdate, PointProfileVerified, QAexpiredNotification, QACutexpiredNotification

blueprint = Blueprint('points', __name__, url_prefix='/v1')

api = restful.Api(blueprint)

api.add_resource(PointUserListApi, '/points')
api.add_resource(PointUserApi, '/points/<int:point_id>', endpoint='points')
api.add_resource(PointSummaryApi, '/points/summary/<int:user_id>', endpoint='summary')
api.add_resource(MigratePoints, '/migrate_points/<int:user_id>', endpoint='migrate_points')
api.add_resource(PointProfileUpdate, '/points_profiles')
api.add_resource(PointProfileVerified, '/points_verified')

api.add_resource(QAexpiredNotification, '/ex-point/<int:user_id>', endpoint='ex_points')
api.add_resource(QACutexpiredNotification, '/cut-point/<int:user_id>', endpoint='point_points')
