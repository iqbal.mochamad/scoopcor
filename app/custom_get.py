import httplib

from flask import Response, request

from app.items.models import Author
from app.master.models import Brand, Currencies, Category, Vendor, \
    Partner, Platform
from app.campaigns.models import Campaign
from app.offers.models import OfferType
from app.parental_controls.models import ParentalControl
from app.payment_gateways.models import PaymentGateway
from app.points.models import PointUser
from app.tiers.models import TierAndroid, TierIos, TierWp

from app.utils.shims import item_types


class CustomGet(object):

    def __init__(self, param=None, module=None, method=None):

        self.limit = param.get('limit', 20)
        self.offset = param.get('offset', 0)
        self.fields = param.get('fields', None)
        self.q = param.get('q', None)

        self.is_active = param.get('is_active', False)
        # Adding filter order by
        self.order = param.get('order', None)

        self.category_country_id = param.get('country_id', None)
        self.organization_id = param.get('organization_id', None)
        self.vendor_id = param.get('vendor_id', None)
        self.item_type_id = param.get('item_type_id', None)
        self.default_itemtype_id = param.get('default_itemtype_id', None)
        self.parent_category_id = param.get('parent_category_id', None)

        self.modified_lt = param.get('modified__lt', None)
        self.modified_lte = param.get('modified__lte', None)
        self.modified_gt = param.get('modified__gt', None)
        self.modified_gte = param.get('modified__gte', None)

        self.slug = param.get('slug', None)

        self.module = module
        self.method = method
        self.master = ''
        self.q_brands = ''
        self.total = 0

    def json_success(self, data_items=None, offset=None, limit=None):

        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            if self.fields:
                cur_values = [u.custom_values(self.fields) for u in data_items]
            else:
                cur_values = [u.values() for u in data_items]

            temp={}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}
            # rv = json.dumps({self.module: cur_values, "metadata": tempx})
            # return Response(rv, status=httplib.OK, mimetype='application/json')

            rv = {self.module: cur_values, "metadata": tempx}
            return rv, httplib.OK, {'Content-Type': 'application/json'}

    def author_master(self):
        #self.total = Author.query.count()
        return Author.query

    def item_currencies_master(self):
        #self.total = Currencies.query.count()
        return Currencies.query

    def item_campaign_master(self):
        #self.total = Campaign.query.count()
        return Campaign.query

    def item_vendor_master(self):
        #self.total = Vendor.query.count()
        return Vendor.query

    def item_brand_master(self):
        #self.total = Brand.query.count()
        return Brand.query

    def item_partners_master(self):
        #self.total = Partner.query.count()
        return Partner.query

    def item_categories_master(self):
        #self.total = Category.query.count()
        return Category.query

    def item_parental_controls_master(self):
        #self.total = ParentalControl.query.count()
        return ParentalControl.query

    def item_payment_gateway_master(self):
        #self.total = PaymentGateway.query.count()
        return PaymentGateway.query.filter_by(is_active=True)

    def offer_type_master(self):
        #self.total = OfferType.query.count()
        return OfferType.query

    def platform_master(self):
        #self.total = Platform.query.count()
        return Platform.query

    def tierios_master(self):
        #self.total = TierIos.query.count()
        return TierIos.query

    def tierandroid_master(self):
        #self.total = TierAndroid.query.count()
        return TierAndroid.query

    def tierwp_master(self):
        #self.total = TierWp.query.count()
        return TierWp.query

    def points_master(self):
        #self.total = PointUser.query.count()
        return PointUser.query


    def construct(self):

        if self.q:
            data_search = self.q

            self.q = "%s%s%s" % ('%', data_search, '%')
            self.q_brands = "%s%s" % (data_search, '%')

        if self.module == 'authors':
            self.master = self.author_master()

            if self.q:
                self.master = self.master.filter(Author.name.ilike(self.q))

            # SEO SEARCH FOR BRYAN GETSCOOP
            if self.modified_lt:
                self.master = self.master.filter(Author.modified < self.modified_lt)

            if self.modified_lte:
                self.master = self.master.filter(Author.modified <= self.modified_lte)

            if self.modified_gt:
                self.master = self.master.filter(Author.modified > self.modified_gt)

            if self.modified_gte:
                self.master = self.master.filter(Author.modified >= self.modified_gte)

        if self.module == 'campaigns':
            self.master = self.item_campaign_master()

            if self.q:
                self.master = self.master.filter(Campaign.name.ilike(self.q))

        if self.module == 'vendors':
            self.master = self.item_vendor_master()
            if self.q:
                self.master = self.master.filter(Vendor.name.ilike(self.q))

            if self.organization_id:
                self.master = self.master.filter_by(organization_id=self.organization_id)

            # SEO SEARCH FOR BRYAN GETSCOOP
            if self.modified_lt:
                self.master = self.master.filter(Vendor.modified < self.modified_lt)

            if self.modified_lte:
                self.master = self.master.filter(Vendor.modified <= self.modified_lte)

            if self.modified_gt:
                self.master = self.master.filter(Vendor.modified > self.modified_gt)

            if self.modified_gte:
                self.master = self.master.filter(Vendor.modified >= self.modified_gte)

        if self.module == 'brands':
            self.master = self.item_brand_master()

            if self.q:
                self.master = self.master.filter(Brand.name.ilike(self.q_brands))

            if self.vendor_id:
                self.master = self.master.filter_by(vendor_id=self.vendor_id)

            if self.default_itemtype_id:
                self.master = self.master.filter_by(
                    default_item_type=item_types.id_to_enum(self.default_itemtype_id))

            # SEO SEARCH FOR BRYAN GETSCOOP
            if self.modified_lt:
                self.master = self.master.filter(Brand.modified < self.modified_lt)

            if self.modified_lte:
                self.master = self.master.filter(Brand.modified <= self.modified_lte)

            if self.modified_gt:
                self.master = self.master.filter(Brand.modified > self.modified_gt)

            if self.modified_gte:
                self.master = self.master.filter(Brand.modified >= self.modified_gte)

        if self.module == 'partners':
            self.master = self.item_partners_master()

            if self.q:
                self.master = self.master.filter(Partner.name.ilike(self.q))

        if self.module == 'categories':
            self.master = self.item_categories_master()

            if self.q:
                self.master = self.master.filter(Category.name.ilike(self.q))

            if self.item_type_id:
                self.master = self.master.filter_by(item_type=item_types.id_to_enum(int(self.item_type_id)))

            if self.parent_category_id:
                self.master = self.master.filter_by(parent_category_id=self.parent_category_id)

        if self.module == 'parental_controls':
            self.master = self.item_parental_controls_master()

            if self.q:
                self.master = self.master.filter(ParentalControl.name.ilike(self.q))

        if self.module == 'currencies':
            self.master = self.item_currencies_master()

            if self.q:
                self.master = self.master.filter(Currencies.name.ilike(self.q))

        if self.module == 'payment_gateways':
            self.master = self.item_payment_gateway_master()

            # Do Not remove this. Scoop already switch bank account to Gramedia.
            # old version android still used scoop as payment bank inside SDK.
            # hide google inappbilling payments below 5.4.1 version
            if 'android' in request.headers.get('User-Agent').lower():
                old_version = request.headers.get('User-Agent', '').split('/')[1].split(' ')[0]
                self.master = self.master.filter(PaymentGateway.id != PaymentGateway.Type.apple.value,
                                                 PaymentGateway.id != PaymentGateway.Type.free.value,
                                                 PaymentGateway.lowest_supported_version <= old_version)

            if 'ios' in request.headers.get('User-Agent').lower():
                self.master = self.master.filter(PaymentGateway.id == PaymentGateway.Type.apple.value)

            if 'web' in request.headers.get('User-Agent').lower():
                self.master = self.master.filter(PaymentGateway.id != PaymentGateway.Type.apple.value,
                                                 PaymentGateway.id != PaymentGateway.Type.free.value,
                                                 PaymentGateway.id != PaymentGateway.Type.iab.value)

            if self.q:
                self.master = self.master.filter(PaymentGateway.name.ilike(self.q))

            if self.organization_id:
                self.master = self.master.filter_by(organization_id=self.organization_id)

            self.master = self.master.order_by(PaymentGateway.sort_priority)

        if self.module == 'offer_types':
            self.master = self.offer_type_master()

            if self.q:
                self.master = self.master.filter(OfferType.name.ilike(self.q))

        if self.module == 'platforms':
            self.master = self.platform_master()

            if self.q:
                self.master = self.master.filter(Platform.name.ilike(self.q))

        if self.module == 'ios_tiers':
            self.master = self.tierios_master()

            if self.q:
                self.master = self.master.filter(TierIos.tier_code.ilike(self.q))

        if self.module == 'android_tiers':
            self.master = self.tierandroid_master()

            if self.q:
                self.master = self.master.filter(TierAndroid.tier_code.ilike(self.q))

        if self.module == 'wp_tiers':
            self.master = self.tierwp_master()

            if self.q:
                self.master = self.master.filter(TierWp.tier_code.ilike(self.q))

        if self.module == 'currencies':
            self.master = self.item_currencies_master()

            if self.q:
                self.master = self.master.filter(Currencies.iso4217_code.ilike(self.q))

        if self.module == 'points':
            self.master = self.points_master()

        if self.is_active:
            self.master = self.master.filter_by(is_active=self.is_active)

        if self.order:
            order_data = self.order.split(',')
            for orderx in order_data:
                if orderx[0] == '-': #descending
                    order_param = '%s %s' % (orderx[1:], 'desc')
                    self.master = self.master.order_by(order_param)
                else:
                    self.master = self.master.order_by(orderx)

        self.total = self.master.count()

        self.master = self.master.limit(self.limit).offset(self.offset).all()
        return self.json_success(self.master, self.offset, self.limit)
