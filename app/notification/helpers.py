import requests, json
from app import app, db
from app.master.choices import ANDROID, IOS

from app.helpers import log_api

WATCHLIST_TITLE = 'ePerpus'
WATCHLIST_BODY = "Item telah tersedia: %s"
FIREBASE_API = 'https://fcm.googleapis.com/fcm/send'


def remove_notification_devices(user_id=None, application=None):
    client_ids = [data.id for data in application.clients] if application else []

    if client_ids:
        sql = """
            update notifications set user_id=NULL
            WHERE
                user_id = %(user_id)s AND
                client_id = ANY(%(client_ids)s)
        """
        db.engine.execute(sql, {'user_id': user_id, 'client_ids': client_ids})


def watchlist_ios_notification_requests(watchlist_data=None,
                                        notification_data=None, href_detail=None):
    data = {
        "to": notification_data.device_register_key,
        "notification": {
            "body": WATCHLIST_BODY % (watchlist_data.item.name)
        },
        "data": {
            "watchlist_id": watchlist_data.id,
            "item_id": watchlist_data.item.id,
            "type": "Detail",
            "href": href_detail
        }
    }
    return json.dumps(data)


def watchlist_android_notification_request(watchlist_data=None,
                                           notification_data=None, href_detail=None):
    # why request android different from ios?
    # if request for android has "notification", notif cant handle by background android.
    # foreground only, that why remove notification param for android.
    data = {
        "to": notification_data.device_register_key,
        "priority": "high",
        "data": {
            "watchlist_id": watchlist_data.id,
            "body": WATCHLIST_BODY % (watchlist_data.item.name),
            "item_id": watchlist_data.item.id,
            "type": "Detail",
            "href": href_detail
        }
    }
    return json.dumps(data)


def watch_list_push_notif(watchlist_data=None, notification_data=None):
    try:
        # ?? this is temporary, cos shared catalogs used objects -_-"
        href_detail = '%sorganizations/%s/shared-catalogs/%s/%s' % (
            app.config['BASE_URL'], watchlist_data.stock_org_id,
            watchlist_data.catalog_id, watchlist_data.item_id)

        if notification_data.client.platform_id == IOS:
            request_data = watchlist_ios_notification_requests(watchlist_data,
                                                               notification_data, href_detail)

        if notification_data.client.platform_id == ANDROID:
            request_data = watchlist_android_notification_request(watchlist_data,
                                                                  notification_data, href_detail)

        headers = {'Authorization': notification_data.client.firebase_server_key, 'Content-Type': 'application/json'}
        result = requests.post(FIREBASE_API, request_data, headers=headers)

        # deactivate devices invalid, (somehow user already uninstall the apps,
        # when re-install again, device have new firebase token different with the old one.)
        # this is handle for clean junk data on core_notification
        try:
            firebase_response = result.json()
            firebase_notif = firebase_response.get('results', None)[0].get('error', None)

            if firebase_notif in ['InvalidRegistration', 'NotRegistered']:
                notification_data.is_allow_notification = False
                notification_data.error_deactivate = str(firebase_response)
                db.session.add(notification_data)
                db.session.commit()

            return firebase_response

        except Exception as e:
            log_api(name='watch list push notif 2', method='GET', msg=str(e))
    except Exception as e:
        log_api(name='watch list push notif', method='GET', msg=str(e))

    return {}
