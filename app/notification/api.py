from __future__ import unicode_literals
from httplib import CREATED, NO_CONTENT, OK

from marshmallow import fields

from flask import Blueprint, jsonify, g
from flask_appsfoundry.exceptions import NotFound
from sqlalchemy import desc

from app.eperpus.constants import PUBLIC_LIBRARY_CLIENT_ID
from app.eperpus.models import WatchList
from app.eperpus.libraries import SharedLibraryItem
from app.notification.models import Notification
from app.notification.helpers import watch_list_push_notif
from app.auth.decorators import user_is_authenticated
from app.utils.marshmallow_helpers import schema_load

from app.users.organizations import Organization

from app import db, ma, app

blueprint = Blueprint('notification', __name__, url_prefix='/v1')


class AddNotificationSchema(ma.Schema):
    device_register_key = fields.Str(attribute='device_register_key', required=True)
    is_allow_notification = fields.Boolean()


@blueprint.route('/users/current-user/notification')
@user_is_authenticated
def details():
    session = db.session
    notification_data = session.query(Notification).filter(
        Notification.user_id == g.current_user.id,
        Notification.client_id == g.current_client.id
    ).first()
    if not notification_data: raise NotFound()

    resp = jsonify(AddNotificationSchema().dump(notification_data).data)
    return resp


@blueprint.route('/users/current-user/notification', methods=['POST'])
@user_is_authenticated
def add():
    """ add notifications records """

    form = schema_load(AddNotificationSchema())
    device_register_key = form.get('device_register_key', 'Unknown')

    session = db.session()
    current_user_id = g.current_user.id
    current_client_id = g.current_client.id

    # find the old data of users, if already exist pass
    notification_data = session.query(Notification).filter(
        Notification.device_register_key == device_register_key,
        Notification.client_id == current_client_id
    ).first()

    if not notification_data:
        notification_data = Notification(
            user_id = current_user_id,
            device_register_key = device_register_key,
            client_id = current_client_id
        )
    else:
        # update user_id on 1 devices.
        notification_data.user_id = current_user_id
        notification_data.error_deactivate = None,
        notification_data.is_allow_notification = True

    session.add(notification_data)
    session.commit()

    resp = jsonify(AddNotificationSchema().dump(notification_data).data)
    resp.status_code = CREATED
    return resp


@blueprint.route('/users/current-user/notification', methods=['PUT', ])
@user_is_authenticated
def update():
    '''
        someday this used for disable notification
        from product team request notification by users, not devices!!
    '''
    session = db.session()
    form = schema_load(AddNotificationSchema())
    device_register_key = form.get('device_register_key', 'Unknown')
    device_allow = form.get('is_allow_notification', True)

    # cos product request notification disable by user_id,
    notification_data = session.query(Notification).filter(
        Notification.user_id == g.current_user.id,
        Notification.client_id == g.current_client.id
    ).first()

    if not notification_data: raise NotFound()
    else:
        # update all data notification by user_id
        notification_data.is_allow_notification = device_allow
        notification_data.user_id = g.current_user.id
        session.add(notification_data)
        session.commit()

    resp = jsonify(AddNotificationSchema().dump(notification_data).data)
    return resp


@blueprint.route('/users/current-user/notification/<device_register_key>', methods=['DELETE', ])
@user_is_authenticated
def remove(device_register_key):
    session = db.session
    notification_data = session.query(Notification).filter(
        Notification.device_register_key == device_register_key,
        Notification.client_id == g.current_client.id
    ).first()

    if notification_data:
        notification_data.user_id = None
        notification_data.is_allow_notification = False
        notification_data.error_deactivate = 'Logout'
        db.session.add(notification_data)
        db.session.commit()

    resp = jsonify({})
    resp.status_code = NO_CONTENT
    return resp

# temporary API for now, used for QA,
# TO DO : script by crontab.. ^^
@blueprint.route('/send_notification/watch-list')
def frontend_testing():
    return watchlist_notifications()

def watchlist_notifications():
    """
        Retrieve all watch-list, and sending notification to user if book is available stock
    """
    notice = []
    try:
        session = db.session()

        # get watchlist with stock available
        ready_stock_items = session.query(WatchList).filter(WatchList.end_date == None).outerjoin(
            WatchList.shared_library_item).filter(SharedLibraryItem.quantity_available > 0).order_by(
            desc(WatchList.start_date)).all()

        for watch_data in ready_stock_items:
            organization = Organization.query.filter_by(id=watch_data.stock_org_id).first()
            organization_clients = list(set([client.id for client in organization.clients] + PUBLIC_LIBRARY_CLIENT_ID))

            # get all devices by client_id
            data_notification = Notification.query.filter(
                Notification.user_id == watch_data.user_id,
                Notification.is_allow_notification == True,
                Notification.client_id.in_(organization_clients)
            ).all()

            if data_notification:
                for notification_user in data_notification:
                    key_sended = 'NOTIF:{}{}'.format(watch_data.id, notification_user.id)
                    already_sended = app.kvs.get(key_sended)

                    if not already_sended and notification_user.client.id in organization_clients:
                        notice = watch_list_push_notif(watch_data, notification_user)
                        app.kvs.set(key_sended, notice, ex=86400)

    except Exception as e:
        return 'Send Notification Failed %s' % str(e)

    return notice


@blueprint.route('/send_notification/scoop-list')
def scoop_notifications():
    '''
        Do all stuff sending notification for scoop here.
    '''


def send_eperpus_notifications(organization, user_id):
    from app.master.choices import ANDROID

    watchlist = WatchList.query.filter(
        WatchList.stock_org_id==organization.id,
        WatchList.user_id==user_id,
        WatchList.end_date==None
    ).all()

    organization_clients = [client.id for client in organization.clients]

    # get data device notifications.
    data_notifications = Notification.query.filter(
        Notification.user_id == user_id,
        Notification.is_allow_notification == True,
        Notification.client_id.in_(organization_clients)
    ).all()

    data = []
    for watchlist_data in watchlist:
        for notif in data_notifications:
            send_notif = watch_list_push_notif(watchlist_data, notif)
            temp = {}
            temp['notification_id'] = notif.id
            temp['watchlist_id'] = watchlist_data.id
            temp['user'] = watchlist_data.user.email
            temp['platform'] = 'ANDROID' if notif.client.platform_id == ANDROID else 'IOS'
            temp['watchlist_item'] = watchlist_data.item.name
            temp['client_id'] = notif.client_id
            temp['device_register_key'] = notif.device_register_key
            temp['notification_status'] = send_notif
            data.append(temp)

    return jsonify({'send-notification': data}), OK

@blueprint.route('/send_notification/<organization:entity>/<user_id>', methods=['GET'])
def specified_notifications(entity, user_id):
    '''
        Send notification specified users and organizations
    '''
    return send_eperpus_notifications(entity, user_id)

