from app import db
from flask.ext.appsfoundry.models import TimestampMixin

class EbookNotification(TimestampMixin, db.Model):

    __tablename__ = 'notifications_ebooks'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    user = db.relationship('User')
    client_id = db.Column(db.Integer, db.ForeignKey('cas_clients.id'), nullable=False)
    client = db.relationship('Client')
    device_register_key = db.Column(db.Text, nullable=False)
    device_model = db.Column(db.String(250), nullable=True)
    is_active = db.Column(db.Boolean(), default=True)
