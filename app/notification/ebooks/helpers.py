import json, requests
from datetime import datetime, timedelta
from sqlalchemy import extract, cast, Date
from unidecode import unidecode

from app import app, db
from app.auth.clients import Client
from app.campaigns.models import Campaign
from app.offers.choices.quantity_unit import UNIT, DAILY, WEEKLY, MONTHLY
from app.items.models import UserSubscription
from app.layouts.banners import Banner, BannerActions
from app.master.choices import ANDROID, IOS, GETSCOOP

from enum import IntEnum

from app.paymentgateway_connectors.atm_transfer.models import PaymentVirtualAccount
from app.payments.choices.gateways import VA_PERMATA, VA_MANDIRI, VA_BNI
from app.payments.models import Payment
from app.payments.payment_status import PAYMENT_IN_PROCESS, WAITING_FOR_PAYMENT
from app.users.buffets import UserBuffet
from .models import EbookNotification


FIREBASE_API = 'https://fcm.googleapis.com/fcm/send'
PENDING_TIME = 518400 #2 days (in seconds)

class EbookNotificationType(IntEnum):
    premium_expired = 1
    subscription_expired = 2
    pending_transaction = 3
    banner_discount = 4

EBOOK_NOTIFICATION_TRANSLATE = {
    EbookNotificationType.premium_expired.value : 'Premium Expired',
    EbookNotificationType.subscription_expired.value : 'Subscription Expired',
    EbookNotificationType.pending_transaction.value : 'Pending Transaction',
    EbookNotificationType.banner_discount.value : 'Promo'
}

BANNER_TYPE_REVERT = {'landing page': 1, 'open url': 2, 'static': 3, 'brand': 4,
                      'buffet': 5, 'promo': 6, 'banner only': 7}

def deauthorization_remove_notification_devices(user_id):
    exist_data = EbookNotification.query.filter_by(user_id=user_id).all()
    for exist in exist_data:
        db.session.delete(exist)
        db.session.commit()

def failure_notifications(response_firebase, device_key):
    failure = response_firebase.get('failure', None)
    if failure:
        db.session.delete(device_key)
        db.session.commit()

def default_ios_notification_request(device_register_key, messages, data_notification, notification_type):
    data = {
        "to": device_register_key,
        "notification": {"body": messages},
        "type": EBOOK_NOTIFICATION_TRANSLATE[notification_type],
        "data": data_notification}
    return json.dumps(data)

def default_android_notification_request(device_register_key, messages, data_notification, notification_type,
                                         title, offer_id=None, order_id=None):

    notice_resp = {}
    if data_notification: notice_resp['banners'] = data_notification
    if offer_id: notice_resp['offer_id'] = offer_id
    if order_id: notice_resp['order_id'] = order_id

    notice_resp['body'] = messages
    notice_resp['title'] = title
    notice_resp['type'] = EBOOK_NOTIFICATION_TRANSLATE[notification_type]

    data = {"to": device_register_key, "priority": "high", "data": notice_resp}
    return json.dumps(data)

def retrieve_data_key(platform_id, specified_user=None, all_key=False):
    if specified_user:
        notice_records = EbookNotification.query.filter(
            EbookNotification.client_id == platform_id,
            EbookNotification.is_active == True,
            EbookNotification.user_id.in_([specified_user])).all()
    else:
        if all_key:
            notice_records = EbookNotification.query.filter(
                EbookNotification.client_id == platform_id).all()
        else:
            notice_records = EbookNotification.query.filter(
                EbookNotification.client_id == platform_id,
                EbookNotification.is_active == True).all()
    return notice_records


def send_firebase_notification(client_id=None, notification_data={}):
    data_client = Client.query.filter_by(id=client_id).first()
    headers = {'Authorization': data_client.firebase_server_key, 'Content-Type': 'application/json'}
    response = requests.post(FIREBASE_API, notification_data, headers=headers)
    return response.json()


def send_banner_ebook_notification():
    try:
        data_redis = app.kvs.keys('notification-banner-*')
        for notice in data_redis:
            banner_id = app.kvs.get(notice)
            # only notif active banner.
            exist_banner = Banner.query.filter(Banner.id == banner_id, Banner.valid_from <= datetime.now(),
                                               Banner.valid_to >= datetime.now()).first()
            if exist_banner:
                app.kvs.delete(notice)
                notification_banner_promotion(banner_id, None)
    except:
        pass


def notification_pending_transaction():
    '''
        This for sending notification user still has pending transaction and will expired.
        ex : Hi Man! your virtual number will expired in 1 Hours!.
    '''
    time_filter = (datetime.now() - timedelta(hours=2))
    notification_type = EbookNotificationType.pending_transaction
    response_data, notification_title, notification_data = [], "Pending Transaction", None
    notification_content = 'You have pending transactions. Hurry up pay for it to read your selected content.'

    pending_exist_data = Payment.query.filter(
        cast(Payment.modified, Date) == time_filter.date(),
        time_filter.hour == extract('hour', Payment.modified),
        Payment.payment_status == WAITING_FOR_PAYMENT,
        Payment.paymentgateway_id.in_([VA_MANDIRI, VA_BNI, VA_PERMATA])).all()

    for payment_pending in pending_exist_data:
        if payment_pending.order.client_id == ANDROID:
            register_key_data = retrieve_data_key(ANDROID, payment_pending.order.user_id)
            va_data = PaymentVirtualAccount.query.filter_by(order_id=payment_pending.order.id).first()

            if register_key_data and va_data:
                for device_key in register_key_data:
                    redis_key = 'pending-{}-{}'.format(payment_pending.order.id, device_key.id)
                    request_data = default_android_notification_request(
                        device_key.device_register_key, notification_content, notification_data, notification_type,
                        notification_title, None, payment_pending.order.id)

                    cached_response = app.kvs.get(redis_key)
                    if not cached_response:
                        response_firebase = send_firebase_notification(ANDROID, request_data)
                        failure_notifications(response_firebase, device_key)
                        response_data.append(response_firebase)
                        app.kvs.set(redis_key, {'sended': redis_key}, PENDING_TIME)

    return response_data


def notification_banner_promotion(banner_id, specified_user):
    '''
        This function for sending notification with direct to banner landing page apps all platforms.
    '''

    exist_banner = Banner.query.filter_by(id=banner_id).first()

    if exist_banner.banner_type.value == BannerActions.landing_page.value:
        exist_campaign = Campaign.query.filter_by(id=exist_banner.payload).first()
        notification_content = exist_campaign.description
        notification_title = exist_banner.name
    else:
        notification_content = exist_banner.name
        notification_title = exist_banner.name

    notification_type = EbookNotificationType.banner_discount
    response_data = []
    notification_data = {
        "id": exist_banner.id,
        "banner_type": BANNER_TYPE_REVERT[exist_banner.banner_type.value],
        "name": exist_banner.name,
        "slug": exist_banner.slug,
        "media_base_url": app.config['BANNER_BASE_URL'],
        "image_highres": exist_banner.image_highres,
        "image_iphone": exist_banner.image_iphone,
        "term_and_condition": exist_banner.term_and_condition,
        "payload": exist_banner.payload}

    if GETSCOOP in exist_banner.clients:
        pass

    if ANDROID in exist_banner.clients:
        device_register_key = retrieve_data_key(platform_id=ANDROID, specified_user=specified_user, all_key=True)
        for device_key in device_register_key:
            request_data = default_android_notification_request(
                device_key.device_register_key, notification_content, notification_data, notification_type, notification_title)
            response_firebase = send_firebase_notification(ANDROID, request_data)
            failure_notifications(response_firebase, device_key)
            response_data.append(response_firebase)

    if IOS in exist_banner.clients:
        device_register_key = retrieve_data_key(platform_id=IOS, specified_user=specified_user, all_key=True)
        for device_key in device_register_key:
            request_data = default_ios_notification_request(
                device_key.device_register_key, notification_content, notification_data, notification_type)

            response_firebase = send_firebase_notification(IOS, request_data)
            failure_notifications(response_firebase, device_key)
            response_data.append(response_firebase)

    return response_data

def notification_subscription_expired():
    '''
        This function for sending notification premium user already expired,
        ex : Hi Man! your KOMPAS 12 Month already end!. please purchase for more ~
    '''
    expired_date = datetime.now() - timedelta(days=1)
    notification_type = EbookNotificationType.subscription_expired.value
    notification_title = "Subscription Ended"
    response_data, notification_data = [], None

    # get all duration subs expired.
    exist_subs = UserSubscription.query.filter(
        cast(UserSubscription.valid_to, Date) == expired_date.date(),
        UserSubscription.quantity_unit.in_([DAILY, WEEKLY, MONTHLY])).all()

    for data_subs in exist_subs:
        # croscek if data have active subs?
        active_subs = UserSubscription.query.filter(
            UserSubscription.user_id == data_subs.user_id,
            UserSubscription.valid_to >= datetime.now(),
            UserSubscription.brand_id == data_subs.brand_id).first()

        notification_content = 'Your {} already expired. Immediately extend ' \
            'to read your subscription content.'.format(unidecode(unicode(data_subs.orderline.offer.long_name)))

        if not active_subs and data_subs.orderline.order.client_id == ANDROID:
            device_register_key = retrieve_data_key(ANDROID, data_subs.user_id)

            for device_key in device_register_key:
                redis_key = 'subsexpired-{}-{}'.format(data_subs.id, device_key.id)
                cached_response = app.kvs.get(redis_key)

                if not cached_response:
                    request_data = default_android_notification_request(device_key.device_register_key,
                        notification_content, notification_data, notification_type, notification_title,
                        data_subs.orderline.offer_id)

                    response_firebase = send_firebase_notification(ANDROID, request_data)
                    failure_notifications(response_firebase, device_key)
                    response_data.append(response_firebase)
                    app.kvs.set(redis_key, {'sended': redis_key}, PENDING_TIME)

    return response_data

def notification_premiumexpired():
    '''
        This function for sending notification premium user already expired,
        ex : Hi Man! your Full Premium Package already end!. please purchase for more ~
    '''
    expired_date = datetime.now() - timedelta(days=1)
    notification_type = EbookNotificationType.premium_expired.value
    notification_title = "Subscription Ended"
    response_data = []

    # get all data expired, ONLY for ANDROID AND WEB
    exist_buffet = UserBuffet.query.filter(cast(UserBuffet.valid_to, Date) == expired_date.date(),
        UserBuffet.original_transaction_id == None).all()

    for data_buffet in exist_buffet:
        #croscek all data again if has premium active
        active_premium = UserBuffet.query.filter(UserBuffet.user_id == data_buffet.user_id,
            UserBuffet.valid_to >= datetime.now(), UserBuffet.offerbuffet_id == data_buffet.offerbuffet_id).first()

        if not active_premium and data_buffet.orderline.order.client_id == ANDROID:
            device_register_key = retrieve_data_key(ANDROID, data_buffet.user_id)
            notification_content = 'Your {} already expired. Immediately extend ' \
                                   'to read your premium content.'.format(data_buffet.orderline.offer.name)

            for device_key in device_register_key:
                notification_data = None
                redis_key = 'premiumexpired-{}-{}'.format(data_buffet.id, device_key.id)

                cached_response = app.kvs.get(redis_key)
                if not cached_response:
                    request_data = default_android_notification_request(device_key.device_register_key, notification_content,
                        notification_data, notification_type, notification_title, data_buffet.orderline.offer_id)

                    response_firebase = send_firebase_notification(ANDROID, request_data)
                    failure_notifications(response_firebase, device_key)
                    response_data.append(response_firebase)
                    app.kvs.set(redis_key, {'sended': redis_key}, PENDING_TIME)

        if not active_premium and data_buffet.orderline.order.client_id == GETSCOOP:
            pass

    return response_data
