from __future__ import unicode_literals
from httplib import CREATED, NO_CONTENT, OK

from flask import Blueprint, jsonify, g, request
from marshmallow import fields

from app.auth.decorators import user_is_authenticated
from app.utils.marshmallow_helpers import schema_load

from app import db, ma, app
from .helpers import *

blueprint = Blueprint('ebook-notification', __name__, url_prefix='/v1')


class EbookNotificationSchema(ma.Schema):
    device_register_key = fields.Str(attribute='device_register_key', required=True)


class EbookBannerNotificationSchema(ma.Schema):
    banner_id = fields.Int(required=False)
    discount_id = fields.Int(required=False)
    user_id = fields.Str(required=False)


@blueprint.route('/users/current-user/notification/ebook', methods=['POST'])
@user_is_authenticated
def add():
    """ add notifications records """
    try:
        form = schema_load(EbookNotificationSchema())
        device_register_key = form.get('device_register_key', 'Unknown')

        try:
            device_model = request.headers.get('User-Agent').split('(')[1].split(';')[0]
        except:
            device_model = 'Unknown'

        session = db.session()
        current_user_id = g.current_user.id
        current_client_id = g.current_client.id

        # find the old data of users, if already exist pass
        notification_data = session.query(EbookNotification).filter(
            EbookNotification.device_register_key == device_register_key,
            EbookNotification.client_id == current_client_id).first()

        if not notification_data:
            notification_data = EbookNotification(
                user_id = current_user_id, device_register_key = device_register_key,
                client_id = current_client_id, is_active=True, device_model=device_model)
            session.add(notification_data)
            session.commit()
        else:
            notification_data.user_id = current_user_id
            notification_data.is_active = True
            notification_data.device_model = device_model

            session.add(notification_data)
            session.commit()

        resp = jsonify(EbookNotificationSchema().dump(notification_data).data)
    except:
        resp = jsonify({'message': 'register key saved.'})

    # Do Not send status code Error Here, Android and IOS will crash, cos they hit this api from splash screen.!
    resp.status_code = CREATED
    return resp


@blueprint.route('/users/current-user/notification/ebook/<device_register_key>', methods=['DELETE', ])
@user_is_authenticated
def remove(device_register_key):
    session = db.session
    notification_data = session.query(EbookNotification).filter(
        EbookNotification.device_register_key == device_register_key,
        EbookNotification.client_id == g.current_client.id
    ).first()

    if notification_data:
        notification_data.is_active = False
        session.add(notification_data)
        session.commit()

    resp = jsonify({})
    resp.status_code = NO_CONTENT
    return resp


@blueprint.route('/users/current-user/notification/ebook-promotion', methods=['POST', ])
@user_is_authenticated
def send_banner_notification():
    data = schema_load(EbookBannerNotificationSchema())
    banner_id = data.get('banner_id', None)
    discount_id = data.get('discount_id', None)
    user_ids = data.get('user_id', None) # this purpose for sending only specific users.

    if banner_id:
        data = notification_banner_promotion(banner_id, user_ids)

    resp = jsonify({'notifications': data})
    resp.status_code = OK
    return resp


@blueprint.route('/users/current-user/notification/ebook-pending-transaction', methods=['GET', ])
@user_is_authenticated
def send_pending_notification():
    data = notification_pending_transaction()
    resp = jsonify({'notifications': data})
    resp.status_code = OK
    return resp


@blueprint.route('/users/current-user/notification/ebook-expired-premium', methods=['GET', ])
@user_is_authenticated
def send_pending_premium():
    data = notification_premiumexpired()
    resp = jsonify({'notifications': data})
    resp.status_code = OK
    return resp


@blueprint.route('/users/current-user/notification/ebook-expired-subs', methods=['GET', ])
@user_is_authenticated
def send_subscription_expired():
    data = notification_subscription_expired()
    resp = jsonify({'notifications': data})
    resp.status_code = OK
    return resp
