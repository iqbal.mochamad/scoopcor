from app.auth.models import Application
from app.notification.helpers import remove_notification_devices
from app.notification.models import Notification
from app.utils.testcases import TestBase


class HelperTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(HelperTests, cls).setUpClass()

        cls.notif1 = Notification(
            id=1, user_id=cls.super_admin.id, client_id=88, is_allow_notification=True,
            device_register_key=cls.fake.bs())
        cls.notif2 = Notification(
            id=2, user_id=cls.super_admin.id, client_id=87, is_allow_notification=True,
            device_register_key=cls.fake.bs())
        cls.notif3 = Notification(
            id=3, user_id=cls.regular_user.id, client_id=88, is_allow_notification=True,
            device_register_key=cls.fake.bs())

        cls.session.add(cls.notif1)
        cls.session.add(cls.notif2)
        cls.session.add(cls.notif3)
        cls.session.commit()

    def test_remove_notification_devices(self):
        application = self.session.query(Application).get(8)
        remove_notification_devices(self.super_admin.id, application)

        notif1 = self.session.query(Notification).get(1)
        self.assertIsNone(notif1.user_id)

        notif2 = self.session.query(Notification).get(2)
        self.assertIsNone(notif2.user_id)
