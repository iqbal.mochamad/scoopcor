from enum import Enum
from flask.ext.appsfoundry.models import TimestampMixin

from app import db
from app.users.users import User
from app.auth.models import Client

class Notification(TimestampMixin, db.Model):
    """
        For notifications eperpus and scoopcor
    """

    __tablename__ = 'notifications'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("cas_users.id"))
    user = db.relationship(User, backref='notifications')

    client_id = db.Column(db.Integer, db.ForeignKey("cas_clients.id"))
    client = db.relationship(Client, backref='notifications')

    device_register_key = db.Column(db.Text, nullable=False)
    is_allow_notification = db.Column(db.Boolean, default=True)
    error_deactivate = db.Column(db.Text, nullable=True)

