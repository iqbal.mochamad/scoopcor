from flask import Blueprint


class CorBlueprint(Blueprint):

    def add_url_rule(self, rule, endpoint=None, view_func=None, **options):
        if view_func:
            # disable the automatic implementation of the HTTP OPTIONS response.
            # we will implement our own options methods
            view_func.provide_automatic_options = False
            if 'options' not in view_func.methods:
                if type(view_func.methods) is tuple:
                    new_methods = [method for method in view_func.methods]
                    new_methods.append('options')
                    view_func.methods = new_methods
                else:
                    view_func.methods.append('options')
        return super(CorBlueprint, self).add_url_rule(rule, endpoint=endpoint, view_func=view_func, **options)

