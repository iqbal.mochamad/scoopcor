import functools

from flask import request, jsonify
from flask_appsfoundry import viewmodels, parsers, marshal, serializers
from hashlib import md5

from app.utils.http import weak_etag_generator


def get_response(data, key_name, count, limit, offset):
    response = jsonify({
        key_name: data,
        "metadata": {
            "resultset": {
                "count": count,
                "offset": offset,
                "limit": limit
            }
        }
    })

    weak_etag = weak_etag_generator(response.data, key_name)
    if weak_etag is not None:
        response.set_etag(weak_etag, weak=True)
    return response.make_conditional(request)


def append_weak_etag(response):
    """ Appends a weak etag to Scoop List Responses.

    .. WARNING:

        There are many assumptions made about what lists will look like and what makes a list response
        unique.

    :param `flask.Response` response:
    :return:
    """

    if request.method in ["GET", "HEAD"]:
        response.set_etag(etag=md5("1,2,3,4,5").hexdigest(), weak=True)
    return response


def get_list_response(query_set, serializer_name, serializer,
                      element_viewmodel=viewmodels.SaViewmodelBase, filtering=None):
    """ parse query set, apply filtering + ordering, serialized it then return response for a list api

    example usage:
    return get_list_response(
        query_set=session.query(Catalog),
        serializer_name='catalogs',
        serializer=ImplicitRelHref(),
        element_viewmodel=SaViewmodelBase,
        filtering=SharedCatalogArgsParser().parse_args(req=request)['filters']
    )

    :param query_set:
    :param serializer_name:
    :param serializer:
    :param element_viewmodel:
    :param filtering:
    :return:
    """

    vm = standard_list_view_model(
        query_set,
        element_viewmodel=element_viewmodel,
        filtering=filtering or []
    )

    _serializer = serializers.PaginatedSerializer(serializer, serializer_name)

    response = jsonify(marshal(vm, _serializer))
    weak_etag = weak_etag_generator(response.data, serializer_name)
    if weak_etag is not None:
        response.set_etag(weak_etag, weak=True)
    return response.make_conditional(request)


def standard_list_view_model(*args, **kwargs):
    standard_vm = functools.partial(
        viewmodels.ListViewmodel,
        limit=request.args.get('limit', 20),
        offset=request.args.get('offset', 0),
        ordering=parsers.SqlAlchemyOrderingParser().parse_args()['order'] or [])

    return standard_vm(*args, **kwargs)


def generate_weak_etag(data):
    from hashlib import md5
    if len(data):
        if hasattr(data[0], 'modified'):
            weak_etag = "".join([e.modified.isoformat() for e in data])
        elif hasattr(data[0], 'id'):
            weak_etag = "".join([str(e.id) for e in data])
        elif hasattr(data[0], 'created'):
            weak_etag = "".join([e.created.isoformat() for e in data])
        else:
            weak_etag = ""
    else:
        weak_etag = ""
    return md5(weak_etag).hexdigest()
