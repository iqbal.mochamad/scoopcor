"""
Serializer Helpers
==================

Serializer classes and fields that are only used within ScoopCOR APIs.

**TODO:** Everything in this package should be moved into Flask-AppsFoundry as soon as practical.
"""
from __future__ import unicode_literals, absolute_import

from datetime import datetime
from enum import Enum

from flask_appsfoundry.serializers import SerializerBase, fields


class SimpleImplicitHref(SerializerBase):
    title = fields.String
    href = fields.String


class SimpleImplicitHrefWithRel(SimpleImplicitHref):
    rel = fields.String


class SimpleImplicitHrefWithId(SimpleImplicitHref):
    id = fields.Integer


class SimpleImplicitHrefWithRelId(SimpleImplicitHrefWithRel):
    id = fields.Integer


class ImplicitRelHref(SerializerBase):

    def __init__(self, title_attribute=None):
        super(ImplicitRelHref, self).__init__()
        if title_attribute:
            self.__serializer_fields__['title'] = fields.String(attribute=title_attribute)

    title = fields.String(attribute='name')
    id = fields.Integer
    href = fields.String(attribute='api_url')


class ResultsetSerializer(object):
    def __new__(cls, *args, **kwargs):
        return {
            'count': fields.Integer(),
            'offset': fields.Integer(),
            'limit': fields.Integer(),
        }


class MetadataSerializer(object):
    def __new__(cls, *args, **kwargs):
        return {
            'resultset': fields.Nested(ResultsetSerializer()),
        }


class ISO8601Date(fields.Raw):
    """ Formats an ISO8601 encoded date string from either a date or datetime.

    >>> from datetime import date
    >>> ISO8601Date().format(date(2015, 2, 15))
    '2015-02-15'

    >>> ISO8601Date().format(datetime(2015, 2, 15, 0, 0))
    '2015-02-15'

    >>> ISO8601Date().format(datetime(2015, 2, 15, 12, 34))
    '2015-02-15'
    """

    def format(self, date_or_datetime):
        """ Returns a JSON-serializable format for the given value.

        :param `datetime.date` or `datetime.datetime` date_or_datetime:
            Value that will be string encoded.
        :return:
        """
        if isinstance(date_or_datetime, datetime):
            date_or_datetime = date_or_datetime.date()
        return date_or_datetime.isoformat()


class Py3Enum(fields.Raw):
    """ Formats a Python-3 compatible enum class into a string for JSON serialization.

    >>> class Layout(Enum):
    ...    landscape = 'landscape bro!'
    ...    portrait = 'portrait yo!'

    >>> Py3Enum().format(Layout.portrait) == 'portrait yo!'
    True

    >>> Py3Enum().format(None) == None
    True
    """

    def format(self, py3enum):
        """ Returns a JSON-serializable format for the given value.

        :param `enum.Enum` py3enum: A python-3 compatible enum class.
        :return:
        """
        return None if py3enum is None else py3enum.value


class ScoopV2RelatedObject(SerializerBase):
    """ Formats a related object (eg, Item.brand) according to the Api v2 Specification.

    .. warning::

        This is a depreciated format, and new responses should include ImplicitHrefs.

    >>> class MySerializer(SerializerBase):
    ...     brand = fields.Nested(ScoopV2RelatedObject())
    ...
    >>> class AnItem(object):
    ...     def __init__(self, b):
    ...         self.brand = b
    ...
    >>> class SomeBrand(object):
    ...     def __init__(self, id, name):
    ...         self.id = id
    ...         self.name = name
    ...
    >>> from flask_appsfoundry import marshal
    >>> result = marshal(AnItem(SomeBrand(123, 'Book Seller')), MySerializer())
    ...
    >>> True == all([result['brand']['id'] == 123, result['brand']['name'] == 'Book Seller'])
    True
    """
    id = fields.Integer
    name = fields.String


class ScoopV2SluggedRelatedObject(ScoopV2RelatedObject):
    """ Formats a related object (eg, Item.brand) according to the Api v2 Specification.

    .. warning::

        This is a depreciated format, and new responses should include ImplicitHrefs.

    >>> class MySerializer(SerializerBase):
    ...     brand = fields.Nested(ScoopV2SluggedRelatedObject())
    ...
    >>> class AnItem(object):
    ...     def __init__(self, b):
    ...         self.brand = b
    ...
    >>> class SomeBrand(object):
    ...     def __init__(self, id, name, slug):
    ...         self.id = id
    ...         self.name = name
    ...         self.slug = slug
    ...
    >>> from flask_appsfoundry import marshal
    >>> result = marshal(AnItem(SomeBrand(123, 'Book Seller', 'book-seller')), MySerializer())
    ...
    >>> True == all([result['brand']['id'] == 123,
    ...              result['brand']['name'] == 'Book Seller',
    ...              result['brand']['slug'] == 'book-seller', ])
    True
    """
    slug = fields.String


class AlwaysNullField(fields.Raw):
    """ I am a beautiful hack.
    """

    def format(self, value):
        return None


class RatingSerializer(SerializerBase):
    average = fields.Float
    total = fields.Integer
