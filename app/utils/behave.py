"""
Behave Test Helpers
===================

Test helper functions specifically for behave-based tests.
"""
from __future__ import unicode_literals, absolute_import

import difflib
import json
import pprint

from app.users.organizations import OrganizationUser
from tests.fixtures.helpers import create_user_and_token, bearer_string


API_USER_CONTEXT_KEY = 'authorizing_user'
API_TOKENS_CONTEXT_KEY = '_user_tokens'


def build_headers(context, has_body=False):
    """ Returns a headers dictionary for a request.

    :param `behave.runner.Context` context:
    :param `bool` has_body: If true, will set the request Content-Type to application/json.
    :return:
    """
    headers = {
        'Accept': 'application/vnd.scoop.v3+json',
    }
    if has_body:
        headers['Content-Type'] = 'application/json'

    authenticating_user = getattr(context, API_USER_CONTEXT_KEY, None)
    if authenticating_user is not None:
        headers['Authorization'] = 'Bearer {}'.format(
            bearer_string(
                authenticating_user,
                getattr(context, API_TOKENS_CONTEXT_KEY, {})[authenticating_user]
            )
        )

    return headers


def create_user(context, primary_role, *additional_roles):

    roles = [primary_role, ]
    roles.extend(additional_roles)

    user, token = create_user_and_token(context.session, *roles)
    if not hasattr(context, API_TOKENS_CONTEXT_KEY):
        setattr(context, API_TOKENS_CONTEXT_KEY, {})
    getattr(context, API_TOKENS_CONTEXT_KEY)[user] = token
    return user


def set_api_user(context, user):
    setattr(context, API_USER_CONTEXT_KEY, user)


def assert_scoop_error_format(response, **kwargs):
    """ Asserts that the JSON of a response object matches ScoopCOR's common error response format.

    Ex, something like the following:

    .. sourcecode:: json

        {
          "status": 401,
          "developer_message": "You cannot access this resource with the provided credentials.",
          "error_code": 401,
          "user_message": "You cannot access this resource with the provided credentials."
        }

    Additionally, any of the provided parameters will have it's value checked, otherwise just the schema
    of the returned data will be checked.
    """
    expected_keys = ['user_message', 'developer_message', 'status', 'error_code']
    # validate kwargs doesn't contain anything we're not expecting.
    for k in kwargs:
        if k not in expected_keys:
            raise AssertionError(
                '{invalid_key} is not a valid argument for assertScoopErrorFormat.  '
                'Valid arguments are: {valid_keys}'.format(
                    invalid_key=k,
                    valid_keys=', '.join(expected_keys)
                )
            )

        response_json = json.loads(response.data)

        if not sorted(response_json.keys()) == sorted(['user_message', 'developer_message', 'status', 'error_code']):
            raise AssertionError("Bad json format")

        for param_name in kwargs:
            if kwargs[param_name] != response_json[param_name]:
                msg = '{param_name} did not match.  Expected: {expected_value} Actual: {actual_value}'.format(
                    param_name=param_name,
                    expected_value=kwargs[param_name],
                    actual_value=response_json[param_name]
                )
                raise AssertionError(msg)


def assign_user_to_organization(context, user, organization, is_manager=False):

    # if the organization isn't a parent organization, find it's parent and add there as well
    if organization.parent_organization is not None:
        context.session.add(
            OrganizationUser(
                organization_id=organization.parent_organization.id,
                user_id=user.id,
                is_manager=is_manager
            ))

    context.session.add(
        OrganizationUser(
            organization_id=organization.id,
            user_id=user.id,
            is_manager=is_manager
        ))

    context.session.commit()


def assert_unordered_json_equal(response, expected):
    """ Similar to assertDictEqual, but the ordering of list elements will be ignored.

    :param `flask.Response` response:
    :param `dict` expected:
    """
    try:
        actual_json = _sort_dict_lists(json.loads(response.data))
    except AttributeError:
        raise AssertionError("Response data is not JSON.  Actual response data: {}".format(response.data))
    expected = _sort_dict_lists(expected)

    if actual_json != expected:
        # standardMsg = '%s != %s' % (safe_repr(actual_json, True), safe_repr(expected, True))
        diff = ('\n' + '\n'.join(
            difflib.ndiff(pprint.pformat(actual_json).splitlines(), pprint.pformat(expected).splitlines()
        )))
        raise AssertionError(diff)


def _sort_dict_lists(response_json):
    """ Recursively order all lists present within a dictionary object.

    :param `dict` json:
    :return: The original dictionary reference, with all lists ordered.
    :rtype: dict
    """
    for key, value in response_json.items():
        if isinstance(value, (list, tuple)):
            response_json[key] = sorted(response_json[key])
        elif isinstance(value, dict):
            _sort_dict_lists(response_json[key])
        # str, float, None.. do nothing
    return response_json

