from collections import namedtuple

from flask import request, Request

GeoInfo = namedtuple(
    "GeoInfo", ["latitude", "longitude", "city", "country_code", "country_name", "client_ip"]
)


def get_geo_info():
    """
    
    :return `GeoInfo`:
    """

    return GeoInfo(
        latitude=request.headers.get('GEOIP_LATITUDE', request.environ.get('GEOIP_LATITUDE')),
        longitude=request.headers.get('GEOIP_LONGITUDE', request.environ.get('GEOIP_LONGITUDE')),
        city=request.headers.get('GEOIP_CITY', request.environ.get('GEOIP_CITY')),
        country_code=request.headers.get('GEOIP_COUNTRY_CODE', request.environ.get('GEOIP_COUNTRY_CODE')),
        country_name=request.headers.get('GEOIP_COUNTRY_NAME', request.environ.get('GEOIP_COUNTRY_NAME')),
        client_ip=request.headers.get('X-Forwarded-For', request.remote_addr),
    )


class GeoAwareRequest(Request):

    @property
    def geo(self):
        return GeoInfo(
            latitude=self.headers.get('GEOIP_LATITUDE', self.environ.get('GEOIP_LATITUDE')),
            longitude=self.headers.get('GEOIP_LONGITUDE', self.environ.get('GEOIP_LONGITUDE')),
            city=self.headers.get('GEOIP_CITY', self.environ.get('GEOIP_CITY')),
            country_code=self.headers.get('GEOIP_COUNTRY_CODE', self.environ.get('GEOIP_COUNTRY_CODE')),
            country_name=self.headers.get('GEOIP_COUNTRY_NAME', self.environ.get('GEOIP_COUNTRY_NAME')),
            client_ip=self.headers.get('X-Forwarded-For', self.remote_addr),
        )

    @property
    def real_ip(self):
        return self.headers.get('X-Forwarded-For', self.remote_addr)
