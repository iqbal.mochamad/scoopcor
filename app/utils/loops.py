from datetime import datetime, timedelta
from time import sleep


class LoopRetarder(object):
    """ Keeps an infinite loop from iterating too quickly.
    """
    def __init__(self, minimum_iteration_seconds):
        self.start_time = datetime.now()
        self.minimum_iteration = timedelta(seconds=minimum_iteration_seconds)

    @property
    def remaining_delay(self):
        return (datetime.now() - self.start_time) - self.minimum_iteration

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.remaining_delay.total_seconds() > 1:
            sleep(self.remaining_delay.total_seconds())
