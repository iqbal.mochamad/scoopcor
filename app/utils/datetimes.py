from __future__ import unicode_literals, absolute_import

from calendar import timegm
from datetime import datetime, tzinfo, timedelta
from pytz import reference, UTC

get_local_nieve = lambda: datetime.now()

get_local = lambda: get_local_nieve().replace(tzinfo=reference.LocalTimezone())

get_utc_nieve = lambda: datetime.utcnow()

get_utc = lambda: get_utc_nieve().replace(tzinfo=reference.UTC)


class WIB(tzinfo):

    def utcoffset(self, date_time):
        return timedelta(hours=7)

    def tzname(self, date_time):
        return "WIB +7"


def datetime_to_isoformat(dt):
    """ Turn a datetime object into an ISO8601 formatted date.

    We use this method in test code, to get isoformat string from a datetime field.
    to easily compare expected value from actual value that generated from serialized date field

        this method is same like flask_appsfoundry.serializer.field._iso8601 method
        used by serializer: fields.DateTime('iso8601')

    Example:

    >>> datetime_to_isoformat(datetime(2012, 05, 20, 3, 2, 3))
    '2012-05-20T03:02:03+00:00'

    >>> datetime(2012, 05, 20, 10, 2, 3, tzinfo=WIB()).isoformat()
    '2012-05-20T10:02:03+07:00'

    >>> datetime_to_isoformat(datetime(2012, 05, 20, 10, 2, 3, tzinfo=WIB()))
    '2012-05-20T03:02:03+00:00'

    :param `datetime` dt: The datetime to transform
    :type dt: datetime
    :return: A ISO 8601 formatted date string
    """
    return datetime.isoformat(
        datetime.fromtimestamp(timegm(dt.utctimetuple()), tz=UTC)
    )
