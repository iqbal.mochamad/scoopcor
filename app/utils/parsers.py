from datetime import datetime

from flask_appsfoundry.parsers import FilterField, converters
from flask_appsfoundry.parsers.operators import Contains

from app.utils.shims.countries import id_to_iso3166
from app.utils.shims.languages import id_to_iso639_alpha3


class CountryIdFilter(FilterField):
    """ Special-case parser field for searching on country_id on PopularItems.

    In the database, items_popular, has a field named
    """
    operators = (Contains, )

    def __init__(self, **kwargs):
        kwargs.update({
            'default_operator': 'contains',
            'type': id_to_iso3166
        })
        super(CountryIdFilter, self).__init__(**kwargs)


class LanguageIdFilter(FilterField):
    """ Special-case parser field for searching on language_id on PopularItems.

    In the database, items_popular, has a field named
    """
    operators = (Contains, )

    def __init__(self, **kwargs):
        kwargs.update({
            'default_operator': 'contains',
            'type': id_to_iso639_alpha3
        })
        super(LanguageIdFilter, self).__init__(**kwargs)


def scoopcor1_or_iso8601_datetime(input):
    try:
        return datetime.strptime(input, "%Y-%m-%d %H:%M:%S")
    except ValueError:
        return converters.naive_datetime_from_iso8601(input)
