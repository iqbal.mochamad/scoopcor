from functools import wraps

from app import db
from flask_appsfoundry.exceptions import NotFound


def model_from_url(model_class, url_capture_key="db_id", argument_name="entity"):
    """ Retrieves a sqlalchemy model based on a URL capture and injects into
    the view function.

    :param `object` model_class:  The type of model that will be retrieved.
    :param `str|unicode` url_capture_key: Name of the URL capture from the
        flask blueprint.
    :param `str|unicode` argument_name: The keyword argument name that the
        retrieved model instance will be injected into the view function with.
    :return: The wrapped function
    """
    def wrapper(func):

        @wraps(func)
        def inner(*args, **kwargs):
            s = db.session()
            entity_id = kwargs.pop(url_capture_key, None)
            entity = s.query(model_class).get(entity_id)
            if not entity:
                raise NotFound(
                    developer_message="{0} id={1} not found".format(model_class,
                                                                    entity_id))
            kwargs[argument_name] = entity
            return func(*args, **kwargs)

        return inner

    return wrapper
