import json

from datetime import datetime

import requests


class EmailLog(object):

    def __init__(self, BASE_URL, http=requests):
        self.base_url = BASE_URL
        self.http = http

    def add(self, email_log_entity):

        data = {"add": {"doc": vars(email_log_entity),
                        "boost": 1.0, "overwrite": True, "commitWithin": 1000}}

        headers = {'Content-Type': 'application/json'}

        return self.http.post('{}/email-log/update?wt=json'.format(self.base_url), data=json.dumps(data), headers=headers)


class EmailLogEntity(object):

    def __init__(self, sender, target, html_template, is_error, log_info, send_date=None):
        """
            :param `str` sender: String-encoded e-mail address for the 'FROM' field.
            :param `list` target: Iterable of string-encoded e-mail address to send to.
            :param `str` html_template: Name of the JINJA2 template to use for
            :param `boolean` is_error: status of email True of False
            :param `unicode` log_info: error description if any
            :param `str` send_date: ISO format. when email sent
         """

        self.sender = sender
        self.target = target
        self.html_template = html_template
        self.is_error = is_error
        self.log_info = log_info
        self.send_date = datetime.utcnow().isoformat() + 'Z' if send_date is None else send_date
