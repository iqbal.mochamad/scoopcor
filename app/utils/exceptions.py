from httplib import BAD_GATEWAY

from werkzeug import exceptions as wzexc
from flask_appsfoundry.exceptions import ScoopApiException

from flask_babel import gettext as _, ngettext


class BadGateway(ScoopApiException, wzexc.BadGateway):
    def __init__(self, **kwargs):
        super(BadGateway, self).__init__(
            code=BAD_GATEWAY,
            error_code=BAD_GATEWAY,
            user_message=kwargs.get("user_message", _("Error accessing remote server")),
            developer_message=kwargs.get("developer_message", "Error accessing remote server")
        )


class SendMailException(BadGateway):
    def __init__(self, **kwargs):
        super(BadGateway, self).__init__(
            code=BAD_GATEWAY,
            error_code=BAD_GATEWAY,
            user_message=kwargs.get("user_message", _("Error sending email via SMTP server")),
            developer_message=kwargs.get("developer_message", "Error sending email via SMTP server")
        )
