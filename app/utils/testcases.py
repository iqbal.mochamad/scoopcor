from __future__ import absolute_import, unicode_literals

import json
import random
import unittest
from decimal import Decimal

from abc import abstractmethod
from httplib import OK, CREATED, NO_CONTENT
from unittest import SkipTest

from datetime import datetime
from faker import Factory
from flask import url_for
from marshmallow.utils import isoformat
from nose.tools import nottest

from app import app, db
from app.auth.helpers import jwt_encode_from_user
from app.constants import RoleType
from tests.fixtures.helpers import create_user_and_token, generate_static_sql_fixtures, bearer_string


class ScoopAssertionsMixin(object):
    """ Container for any common assertions that are specific to Scoop APIs/Data Structures.
    """
    def assertScoopErrorFormat(self, response, **kwargs):
        """ Asserts that the JSON of a response object matches ScoopCOR's common error response format.

        Ex, something like the following:

        .. sourcecode:: json

            {
              "status": 401,
              "developer_message": "You cannot access this resource with the provided credentials.",
              "error_code": 401,
              "user_message": "You cannot access this resource with the provided credentials."
            }

        Additionally, any of the provided parameters will have it's value checked, otherwise just the schema
        of the returned data will be checked.
        """
        expected_keys = ['user_message', 'developer_message', 'status', 'error_code']
        # validate kwargs doesn't contain anything we're not expecting.
        for k in kwargs:
            if k not in expected_keys:
                self.fail(
                    '{invalid_key} is not a valid argument for assertScoopErrorFormat.  '
                    'Valid arguments are: {valid_keys}'.format(
                        invalid_key=k,
                        valid_keys=', '.join(expected_keys)
                    )
                )

        # todo: replace this with a json schema.
        response_json = json.loads(response.data)
        self.assertEqual(
            sorted(response_json.keys()),
            sorted(['user_message', 'developer_message', 'status', 'error_code'])
        )

        for param_name in kwargs:
            self.assertEqual(
                kwargs[param_name],
                response_json[param_name],
                msg='{param_name} did not match.  Expected: {expected_value} Actual: {actual_value}'.format(
                    param_name=param_name,
                    expected_value=kwargs[param_name],
                    actual_value=response_json[param_name]
                ))

    def assertUnorderedJsonEqual(self, response, expected):
        """ Similar to assertDictEqual, but the ordering of list elements will be ignored.

        :param `flask.Response` response:
        :param `dict` expected:
        """
        try:
            actual_json = self._sort_dict_lists(json.loads(response.data))
        except AttributeError:
            self.fail("Response data is not JSON.  Actual response data: {}".format(response.data))
        expected = self._sort_dict_lists(expected)
        self.assertDictEqual(expected, actual_json)

    def _sort_dict_lists(self, response_json):
        """ Recursively order all lists present within a dictionary object.

        :param `dict` json:
        :return: The original dictionary reference, with all lists ordered.
        :rtype: dict
        """
        for key, value in response_json.items():
            if isinstance(value, (list, tuple)):
                response_json[key] = sorted(response_json[key])
            elif isinstance(value, dict):
                self._sort_dict_lists(response_json[key])
            # str, float, None.. do nothing
        return response_json


class SessionAttacher(object):

    def __init__(self, e2e_test_case, *models):
        self.__test_case = e2e_test_case
        self.models = models

    def __enter__(self):
        for m in self.models:
            self.__test_case.reattach_model(m)

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            self.__test_case.session.commit()
            for m in self.models:
                self.__test_case.detach_model(m)
        else:
            self.__test_case.session.rollback()


class E2EBase(ScoopAssertionsMixin, unittest.TestCase):

    maxDiff = None

    def create_user(self, role_type):
        user, token = create_user_and_token(self.session, role_type)
        self._user_tokens[user] = token
        return user

    def setUp(self):
        # make sure the test mode flag is set, or bail out
        if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
            raise SkipTest("COR is not in testing mode.  Cannot continue.")

        super(E2EBase, self).setUp()
        self._user_tokens = {}

        db.session().close_all()
        db.session().expunge_all()
        db.engine.dispose()
        db.drop_all()
        db.engine.dispose()
        db.create_all()

        self.session = db.session

        generate_static_sql_fixtures(self.session)
        self.api_client = app.test_client(use_cookies=False)

        self.super_admin = self.create_user(RoleType.super_admin)
        self.regular_user = self.create_user(RoleType.verified_user)

        self._authorizing_user = None

    def set_api_authorized_user(self, user):
        self.reattach_model(user)
        self._authorizing_user = user

    def build_headers(self, has_body=False):
        headers = {
            'Accept': 'application/vnd.scoop.v3+json',
        }
        if has_body:
            headers['Content-Type'] = 'application/json'

        if self._authorizing_user is not None:
            with app.test_request_context('/'):
                headers['Authorization'] = 'JWT {}'.format(
                    jwt_encode_from_user(self._authorizing_user)
                )

        return headers

    def tearDown(self):
        super(E2EBase, self).tearDown()
        self.session.expunge_all()
        self.session.close_all()
        db.drop_all()

    def url_for(self, endpoint, **kwargs):
        """ Helper method for constructing URLs.  Works just like flask's vanilla url_for, but all
        urls will automatically be _external=True, and a test request context will be pushed.
        """
        with app.test_request_context():
            return url_for(endpoint, _external=True, **kwargs)

    def reattach_model(self, model):
        try:
            self.session.add(model)
        except Exception as e:
            self.session.merge(model)
        self.session.refresh(model)

    def detach_model(self, model):
        self.session.refresh(model)
        self.session.expunge(model)

    def on_session(self, *models):
        """ Returns a context manager, which when entered will reattach all models to the session.

        When the context manager is left, the session will be committed and all models will be removed.

        :param models:
        :return:
        """
        return SessionAttacher(self, *models)


class TestBase(E2EBase):
    """
    The difference with E2EBase is that database will be created on setUpClass instead of setUp
        (it will create once in the class setup, not multiple times on each setup when test method run)
    """

    _user_tokens = {}
    fake = Factory.create()

    module_based_db = False

    def setUp(self):
        # in case some commit error, add rollback, so next tests didn't get error exception during flush.
        self.session.rollback()
        # no need to recreate database on setUp, db created on setUpClass method

    def tearDown(self):
        # override tearDown method, no need to drop db on tearDown
        self.session.expunge_all()

    @classmethod
    def create_user(cls, role_type, session=None):
        session = session or cls.session
        user, token = create_user_and_token(session, role_type)
        cls._user_tokens[user] = token
        return user

    @classmethod
    def setUpClass(cls):
        # make sure the test mode flag is set, or bail out
        if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
            raise SkipTest("COR is not in testing mode.  Cannot continue.")

        super(TestBase, cls).setUpClass()
        cls._user_tokens = {}
        cls._clean_db()
        if not cls.module_based_db:
            db.create_all()
        cls.session = db.session

        if not cls.module_based_db:
            generate_static_sql_fixtures(cls.session)

        cls.super_admin = cls.create_user(RoleType.super_admin, cls.session)
        cls.regular_user = cls.create_user(RoleType.verified_user, cls.session)

        cls._authorizing_user = None
        cls.session.commit()
        cls.session.expunge_all()

        cls.ctx = app.test_request_context('/')
        cls.ctx.push()
        cls.api_client = app.test_client(use_cookies=False)

    @classmethod
    def tearDownClass(cls):
        cls.ctx.pop()

        cls.session.expunge_all()
        cls.session.close()
        cls.session.close_all()
        cls._clean_db()
        super(TestBase, cls).tearDownClass()

    @classmethod
    def _clean_db(cls):
        db.session.expunge_all()
        db.session.close_all()
        db.engine.dispose()
        if not cls.module_based_db:
            db.drop_all()

    @classmethod
    def set_api_authorized_user(cls, user):
        if user:
            cls.reattach_model(user)
        cls._authorizing_user = user

    @classmethod
    def reattach_model(cls, model):
        try:
            cls.session.add(model)
        except Exception as e:
            cls.session.merge(model)
        cls.session.refresh(model)

    @classmethod
    def build_headers(cls, has_body=False):
        headers = {
            'Accept': 'application/vnd.scoop.v3+json',
        }
        if has_body:
            headers['Content-Type'] = 'application/json'

        if cls._authorizing_user is not None:
            with app.test_request_context('/'):
                headers['Authorization'] = 'JWT {}'.format(
                    jwt_encode_from_user(cls._authorizing_user)
                )

        return headers

    def _assert_dict_contain_subset(self, expected, actual, msg=None):
        expected_copy = expected.copy()
        actual_copy = actual.copy()
        for key, value in expected_copy.items():
            if type(value) in (dict, list):
                # remove to prevent further test
                expected_copy.pop(key)
                actual_value = actual_copy.pop(key, None)
                self.assertIsNotNone(
                    actual_value,
                    msg='\nKey [{key}] not exists in actual.\nExpected -> {key}: {expected},\n'
                        'actual -> {actual}\n{msg}'.format(
                            key=key, actual=actual_value, expected=value, msg=msg,
                        ))
                if type(value) is dict:
                    self._assert_dict_contain_subset(
                        expected=value, actual=actual_value,
                        msg='\nDict with Key [{key}]: expected value not exists in actual.\nExpected {expected},\n'
                            'actual {actual}\n{msg}'.format(
                                key=key, actual=actual_value, expected=value, msg=msg,
                        ))
                elif type(value) is list:
                    self._assert_list_equal(
                        expected=value, actual=actual_value,
                        msg='\nList with Key [{key}]: expected != actual.\nExpected {expected},\n'
                            'actual {actual}\n{msg}'.format(
                                key=key, actual=actual_value, expected=value, msg=msg,
                        ))

        self.assertDictContainsSubset(expected_copy, actual_copy,
                                      msg="\n Expected {expected} \nvs Actual {actual}\n{msg}\n".format(
                                          expected=expected_copy, actual=actual_copy, msg=msg
                                      ))

    def _assert_list_equal(self, expected, actual, msg=None):
        try:
            expected_id = expected[0].get('id', None)
            actual_id = actual[0].get('id', None)
        except Exception:
            # not a list of object with id
            expected_id = None
            actual_id = None

        if actual and expected and len(expected) > 0 and expected_id \
                and len(actual) > 0 and actual_id:
            for entity_json in actual:
                entity_id = entity_json['id']
                # get entity with the same id
                current_expected = filter(lambda e: e.get('id') == entity_id, expected)
                if not current_expected:
                    self.fail(msg='Id {id} exists in actual but cannot find entity with id {id} in Expected Response. '
                                  'Expected response {expected}\n{msg}'.format(
                                    id=entity_id, expected=expected, msg=msg))
                self._assert_dict_contain_subset(current_expected[0], entity_json, msg)
            self.assertEqual(len(expected), len(actual), msg)
        else:
            self.assertItemsEqual(expected, actual, msg)

    def assert_status_code(self, response, expected_status_code):
        self.assertEqual(response.status_code, expected_status_code,
                         '\nResponse status code {0.status_code}!= {1}. '
                         'Response {0.data}'.format(response, expected_status_code))
        self.assertEqual('application/json', response.content_type)

    def assert_list_response_metadata(self, response_json, expected_count):
        metadata = response_json['metadata']['resultset']
        self.assertItemsEqual(
            expected_seq=['offset', 'limit', 'count'],
            actual_seq=metadata.keys()
        )
        actual_count = metadata.get('count')
        if actual_count <= metadata.get('limit'):
            self.assertEqual(
                actual_count, expected_count,
                '\nActual count in list {actual_count} != expected {expected_count}'.format(
                    actual_count=actual_count, expected_count=expected_count))

    def _get_response_json(self, response):
        # load response data
        response_json = json.loads(response.data)
        if 'developer_message' in response_json:
            self.fail(msg='Response status code = {}. Message: {}'.format(
                response.status_code,
                response_json['developer_message']))
        return response_json


@nottest
class CrudBase(TestBase):
    """ Base class for testing CRUD API: Create/POST, Retrieve/GET (single or list), Update/PUT, and Delete/DELETE

    example: see file: app/payment_gateways/tests/e2e/crud_tests.py

    important: make sure to add decorator @istest in the inherited test class
    """

    # set base request url, ex: '/v1/payment_gateways'
    request_url = ""
    # set base model to Test, ex: model = PaymentGateway
    model = None
    # set base Model Factory, ex: fixture_factory = PaymentGatewayFactory
    fixture_factory = None
    # set key name of list response, ex: list_key = 'payment_gateways'
    list_key = ""
    request_body = None

    maxDiff = None
    longMessage = True

    resultset_max_size = 19
    resultset_min_size = 3
    max_response_size = 20

    @classmethod
    def setUpClass(cls):
        super(CrudBase, cls).setUpClass()
        cls.session.commit()

    def test_get_single_api(self):
        entity = self.set_up_fixtures()
        with self.on_session(entity):
            self.set_api_authorized_user(self.regular_user)

            expected = self.get_expected_response(entity)
            entity_id = entity.id

        response = self.api_client.get('{base}/{id}'.format(base=self.request_url, id=entity_id),
                                       headers=self.build_headers())
        self.assert_status_code(response, OK)
        response_json = self._get_response_json(response)
        with self.on_session(entity):
            self.assert_response_body(response_json, expected, entity,
                                      msg="\nFail on Test Create API."
                                          "\nExpected {expected}\nActual {actual}\n".format(
                                            expected=expected, actual=response_json
                                            ))

    def test_create_api(self):
        self.set_api_authorized_user(self.super_admin)

        # execute api and get response
        request_body = self.get_request_body()
        response = self.api_client.post(
            self.request_url,
            data=json.dumps(request_body),
            headers=self.build_headers(has_body=True))

        self.assert_status_code(response, CREATED)

        response_json = json.loads(response.data)
        saved_entity = self.session.query(self.model).get(response_json.get('id'))
        expected = self.get_expected_response(saved_entity)
        # assert saved data vs request body
        self.assert_saved_data(saved_entity, request_body)
        # assert response body vs expected response
        response_json = self._get_response_json(response)
        self.assert_response_body(response_json, expected, saved_entity,
                                  msg="\nFail on Test Create API."
                                      "\nExpected {expected}\nActual {actual}\n".format(
                                        expected=expected, actual=response_json
                                        ))

    def set_up_fixtures(self):
        entity = self.fixture_factory()
        self.session.add(entity)
        self.session.commit()
        self.session.expunge(entity)
        return entity

    def test_update_api(self):
        entity_to_update = self.set_up_fixtures()
        with self.on_session(entity_to_update):
            self.set_api_authorized_user(self.super_admin)

            # execute api and get response
            request_body = self.get_request_body()
            response = self.api_client.put(
                '{base}/{id}'.format(base=self.request_url, id=entity_to_update.id),
                data=json.dumps(request_body),
                headers=self.build_headers(has_body=True))

            self.assert_status_code(response, OK)

            # reload saved/updated data
            self.session.refresh(entity_to_update)
            # assert response body vs expected response
            expected_response = self.get_expected_response(entity_to_update)
            response_json = self._get_response_json(response)
            self.assert_response_body(response_json, expected_response, entity_to_update,
                                      msg="\nFail on Test Update API."
                                          "\nExpected {expected}\nActual {actual}\n".format(
                                            expected=expected_response, actual=response_json
                                            ))
            # assert saved data vs request body
            self.assert_saved_data(entity_to_update, request_body)

    def assert_saved_data(self, saved_entity, request_body):
        # assert saved data vs request body
        # for key with value as dict or list in request body, we don't compare here,
        #   override this method to compare complex field if needed
        for key, expected_value in request_body.items():
            if type(expected_value) not in (dict, list):
                if hasattr(saved_entity, key):
                    actual = getattr(saved_entity, key)
                    if isinstance(actual, datetime):
                        self.assertEqual(
                            expected_value, isoformat(actual),
                            '\nField {key}: {expected} != {actual}'.format(
                                key=key, expected=expected_value, actual=actual))
                    elif isinstance(actual, Decimal):
                        self.assertTrue(
                            float(actual) == float(expected_value),
                            '\nField {key}: {expected} != {actual}'.format(
                                key=key, expected=expected_value, actual=actual))
                    else:
                        self.assertEqual(
                            actual, expected_value,
                            '\nField {key}: {expected} != {actual}'.format(
                                key=key, expected=expected_value, actual=actual))
                elif key == 'title' and hasattr(saved_entity, 'name'):
                    actual = getattr(saved_entity, 'name')
                    self.assertEqual(
                        actual, expected_value,
                        '\nField {key}: {expected} != {actual}'.format(
                            key='title/name', expected=expected_value, actual=actual))

    def assert_response_body(self, response_json, expected, entity=None, msg=None):
        # override this method if needed
        self._assert_dict_contain_subset(expected, actual=response_json, msg=msg)

    def set_up_list_fixtures(self):
        fixtures = [self.fixture_factory() for _
                in range(random.randint(self.resultset_min_size,
                                        self.resultset_max_size))]
        self.session.commit()
        return fixtures

    def test_list_api(self):
        # setup some more data
        self.set_up_list_fixtures()

        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get('{url}?limit=20'.format(url=self.request_url),
                                       headers=self.build_headers())

        self.assert_status_code(response, OK)
        expected_entities = self.session.query(self.model).limit(20).all()
        response_json = self._get_response_json(response)
        self.assert_list_response(response_json, expected_entities)

    def assert_list_response(self, response_json, expected_entities):
        # for each response in list, compare with expected response
        self.assertIsNotNone(response_json.get(self.list_key),
                             '\nData with key [{}] not found in response. Response: {}'.format(
                                 self.list_key, response_json
                             ))
        # test each data in response body
        self.assert_list_main_data(entities=expected_entities, response_json=response_json,
                                   msg="\nFail on Test List API."
                                       "\nActual {actual}\n".format(actual=response_json))
        # test meta data
        self.assert_list_response_metadata(response_json, expected_count=len(expected_entities))

    def assert_list_main_data(self, entities, response_json, msg=None):
        for entity_json in response_json.get(self.list_key, []):
            entity_id = entity_json['id']
            # get entity with the same id
            match_entities = filter(lambda e: e.id == entity_id, entities)
            if not match_entities:
                self.fail(msg='Cannot find entity with id {id} in Expected Data. '
                              'Actual {actual}\n{msg}'.format(
                                id=entity_id, actual=entity_json, msg=msg))

            expected = self.get_expected_list_response(match_entities[0])
            self.assert_response_body(entity_json, expected, match_entities[0], msg=msg)

        actual_count = len(response_json.get(self.list_key, []))
        self.assertEqual(len(entities), actual_count,
                         msg='\nExpected count {expected_count} != actual count {actual_count} '
                             'in list api\n{msg}'.format(
                             expected_count=len(entities), actual_count=actual_count, msg=msg))

    @abstractmethod
    def get_request_body(self):
        # define the request body for create and update tests
        # example:
        # request_body = {
        #     "name": self.fake.name(),
        #     "description": self.fake.bs(),
        #     'start_date': datetime_to_isoformat(get_local() - timedelta(days=1)),
        #     'end_date': datetime_to_isoformat(get_local() + timedelta(days=2)),
        #     'total_cost': self.fake.pyfloat(right_digits=4),
        #     'is_active': True,
        # }
        # return request_body
        return None

    @abstractmethod
    def get_expected_response(self, entity):
        # define expected response body based on a saved data (entity)
        # example:
        #     expected = {
        #         "name": entity.name,
        #         "id": entity.id,
        #         "icon_image_normal": entity.icon_image_normal,
        #         "currency": {
        #             "id": entity.base_currency.id,
        #             "iso4217_code": entity.base_currency.iso4217_code,
        #         },
        #     }
        #     return expected
        return None

    def get_expected_list_response(self, entity):
        # in case list response is different from get single response, override this method
        return self.get_expected_response(entity)

    def test_delete_api(self):
        entity_to_delete = self.set_up_fixtures()
        with self.on_session(entity_to_delete):
            entity_id = entity_to_delete.id
            self.set_api_authorized_user(self.super_admin)

            response = self.api_client.delete(
                '{base}/{id}'.format(base=self.request_url, id=entity_id),
                headers=self.build_headers())
            self.assert_status_code(response, NO_CONTENT)
            self.assert_deleted_data(entity_to_delete)

    def assert_deleted_data(self, entity_to_delete):
        self.session.refresh(entity_to_delete)
        self.assertEqual(entity_to_delete.is_active, False)



# def load_tests(loader, tests, pattern):
#     # don't load any tests from this module, as they're all base tests
#     import unittest
#     suite = unittest.TestSuite()
#     return suite


