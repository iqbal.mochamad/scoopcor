from __future__ import unicode_literals

from dateutil.relativedelta import relativedelta

__all__ = ('camel_to_snake', 'format_iso8601_duration', 'FORMAT_ISO8601_DURATION', )


def camel_to_snake(camel_case_string):
    """ Takes a Camel-Cased string, and returned a Snake-Cased variant.

    >>> camel_to_snake("FooBarBazQ")
    u'foo_bar_baz_q'

    :param `str` camel_case_string: Any string.
    :return: Snake-cased version of the string
    :rtype: str
    """
    for i in range(0, len(camel_case_string)):
        if camel_case_string[i].isupper():
            if i == 0:
                camel_case_string = camel_case_string.replace(
                    camel_case_string[i], camel_case_string[i].lower())
            else:
                camel_case_string = camel_case_string.replace(
                    camel_case_string[i], "_" + camel_case_string[i].lower())
            return camel_to_snake(camel_case_string)

    return camel_case_string


FORMAT_ISO8601_DURATION = 'P{0.years}Y{0.months}M{0.days}DT{0.hours}H{0.minutes}M{0.seconds}S'


def format_iso8601_duration(time):
    """ Formats a relative delta as an ISO8601 duration string.

    >>> format_iso8601_duration(relativedelta(years=1,months=2,days=3,hours=4,minutes=5,seconds=6.789))
    u'P1Y2M3DT4H5M6.789S'

    :param `dateutil.relativedelta.relativedelta` time:
    :return: An ISO8601 formatted duration string.
    :rtype: six.text_type
    """
    return FORMAT_ISO8601_DURATION.format(time)
