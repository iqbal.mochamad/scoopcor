from __future__ import unicode_literals, absolute_import
from httplib import INTERNAL_SERVER_ERROR
from functools import wraps

from flask import current_app, request, jsonify, make_response, g
from flask.views import MethodView

from flask_appsfoundry.helpers import get_or_create_instance
from werkzeug.exceptions import HTTPException

from app.auth.helpers import is_superuser
from flask_appsfoundry.exceptions import BadRequest, Forbidden, Unauthorized
from .auth_handlers import TokenRequired


class CorListControllerMixin(object):
    """ List controller base class.

    .. depreciated::

        Similar functionality available through the base class libraries in Flask-AppsFoundry version 1.x branch.
    """
    def get(self, *args, **kwargs):
        self._assert_limit_param_is_valid()
        return super(CorListControllerMixin, self).get(*args, **kwargs)

    def _assert_limit_param_is_valid(self):
        """ Raises an error if 'limit' query paremeter is larger than
        MAXIMUM_ALLOWED_PAGE_SIZE.
        """
        limit = request.args.get('limit', type=int)
        if limit > current_app.config['MAXIMUM_ALLOWED_PAGE_SIZE']:
            raise BadRequest(
                user_message="Page size was too large.  Please try again.",
                developer_message=
                    "'limit' query parameter invalid.  "
                    "Your requested limit: {req_limit}.  "
                    "Max allowed: {max_limit}".format(
                        req_limit=limit,
                        max_limit=current_app.config['MAXIMUM_ALLOWED_PAGE_SIZE'])
            )


class CorApiErrorHandlingMixin(object):
    """ Use this controller instead of the base ListCreateApiResource when registering the controller with a blueprint.

    If you're still registering your APIs through the Flask-Restful Api class, don't use this!
    """
    def _handle_all(self, exception):
        """ Catch-all handler when another error handler wasn't defined.

        :param `BaseException` exception: The error that was actually raised by this controller.
        """
        if isinstance(exception, HTTPException):
            raise exception

        code = getattr(exception, 'code', INTERNAL_SERVER_ERROR)
        response = jsonify({
            'status': code,
            'error_code': code,
            'user_message': 'A problem occurred, please try again.',
            'developer_message': str(exception)
        })
        response.status_code = code

        return response

    _handle_400 = _handle_all
    _handle_401 = _handle_all
    _handle_404 = _handle_all


class CorAuthMixin(object):

    get_security_tokens = []
    post_security_tokens = []
    put_security_tokens = []
    delete_security_tokens = []

    provide_automatic_options = False

    # define the allowed method for options check, override this value as needed
    allowed_options = ['GET', 'HEAD', 'OPTIONS', 'PUT', 'POST', 'DELETE']

    def __init__(self, *args, **kwargs):

        # noinspection PyArgumentList
        super(CorAuthMixin, self).__init__(*args, **kwargs)

        if 'options' not in [m.lower() for m in self.methods]:
            self.methods.append('options')

        for http_method_name in self.methods:
            http_method_name = http_method_name.lower()

            if http_method_name != 'options':
                if hasattr(self, http_method_name):
                    tokens = getattr(self, http_method_name + '_security_tokens')
                    # don't assign this auth check decorator in here for now,
                    # cause we will declared it directly in each api class manually
                    # wrapped_method = user_has_any_tokens(*tokens)(getattr(self, http_method_name))
                    # setattr(self, http_method_name, wrapped_method)

    def default_options(self, *args, **kwargs):

        current_methods = ['OPTIONS', ]
        for http_method_name in [m.lower() for m in self.methods if m.lower() not in ('options',)]:
            http_method_name = http_method_name.lower()
            if hasattr(self, http_method_name):
                current_methods = self.__update_allowed_methods(http_method_name, current_methods)

        resp = make_response("")
        resp.headers['Allow'] = ",".join(current_methods)
        return resp

    def options(self, *args, **kwargs):
        """  Derived classes can override this method and assert the condition for allowed options
        """
        return self.default_options(*args, **kwargs)

    def __update_allowed_methods(self, candidate_method, current_methods):
        required_permissions = getattr(self, candidate_method+'_security_tokens'.lower(), None)
        candidate_method = candidate_method.upper()

        user_perm = g.user.perm if g and getattr(g, 'user', None) else []
        if not required_permissions:
            # if no permission defined, then it's allowed for everyone
            is_allowed = True
        elif is_superuser(getattr(g, 'current_user', None)):
            is_allowed = True
        elif any([req_perm in user_perm for req_perm in required_permissions]):
            is_allowed = True
        else:
            is_allowed = False

        if is_allowed:
            if (candidate_method not in current_methods
                    and candidate_method in self.allowed_options):
                current_methods.append(candidate_method)
            if candidate_method == 'GET' and 'HEAD' not in current_methods:
                current_methods.append('HEAD')
        else:
            # if not allowed, remove from candidate_method
            if candidate_method in current_methods and candidate_method != 'OPTIONS':
                current_methods.remove(candidate_method)

        return current_methods

    @classmethod
    def as_view(cls, *args, **kwargs):
        av = super(CorAuthMixin, cls).as_view(*args, **kwargs)
        av.provide_automatic_options = False
        if 'options' not in [m.lower() for m in av.methods]:
            av.methods.append('options')
        # TODO: somehow ends up with duplicates in methods.. but w/e
        av.methods = list(set(av.methods))
        return av


class ListAuthMixin(CorAuthMixin):
    allowed_options = ['OPTIONS', 'GET', 'POST', ]


class DetailAuthMixin(CorAuthMixin):
    allowed_options = ['OPTIONS', 'GET', 'PUT', 'DELETE']


def authorization_attributes(*auth_attributes):
    def wrapper(func):
        setattr(func, '_auth_attributes', auth_attributes)
        @wraps(func)
        def inner(*args, **kwargs):
            return func(*args, **kwargs)
        return inner
    return wrapper


class AuthMethodView(MethodView):
    """ Check request permission
    """
    authorization_handler = TokenRequired

    def options(self, *args, **kwargs):

        opts = []

        if not g.user and 'Authorization' in request.headers:
            # if the user has provided auth, but it was invalid
            opts.append('OPTIONS')
        else:
            for method_name in self.methods:

                meth = getattr(self, method_name.lower())

                if not g.user:
                    # if there is no user Authorized on the session, then we
                    # need to assume this is a browser 'preflight check'
                    # and we need to return ALL the possible methods.
                    opts.append(method_name)

                else:
                    # this is a request from one of our apps, so we return
                    # only the things the user has permission to do.
                    auth_attributes = getattr(meth, '_auth_attributes', [])
                    if auth_attributes:
                        handler = get_or_create_instance(self.authorization_handler, *auth_attributes)
                        try:
                            handler.authorize(controller=self, request=request, user=g.user, token=g.token, *args, **kwargs)
                            opts.append(method_name)
                        except (Forbidden, Unauthorized):
                            pass
                    else:
                        opts.append(method_name)

            # always add the options verb to allow
            if 'OPTIONS' not in opts:
                opts.append('OPTIONS')

            # users that can GET, can also HEAD by default
            if 'GET' in opts and 'HEAD' not in opts:
                opts.append('HEAD')

        return make_response("", 200, {'Allow': ",".join(opts)})

    @classmethod
    def as_view(cls, *args, **kwargs):
        controller_as_view = super(AuthMethodView, cls).as_view(*args, **kwargs)
        controller_as_view.provide_automatic_options = False
        return controller_as_view

    def dispatch_request(self, *args, **kwargs):
        """ Overrides the default dispatch_request from MethodView
        :param args:
        :param kwargs:
        :return:
        """
        meth = getattr(self, request.method.lower(), None)
        # if the request method is HEAD and we don't have a handler for it
        # retry with GET
        if meth is None and request.method == 'HEAD':
            meth = getattr(self, 'get', None)

        assert meth is not None, 'Unimplemented method %r' % request.method

        try:
            handler = get_or_create_instance(self.authorization_handler, *getattr(meth, '_auth_attributes', []))
            handler.authorize(controller=self, request=request, user=g.user, token=g.token, *args, **kwargs)
        except Unauthorized:
            # special case -- if options request, and unauthorized... serve anyway
            if request.method != 'OPTIONS':
                raise

        return meth(*args, **kwargs)
