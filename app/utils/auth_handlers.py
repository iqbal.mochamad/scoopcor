from __future__ import unicode_literals, absolute_import

from abc import ABCMeta, abstractmethod

from six import with_metaclass

from flask_appsfoundry.exceptions import Unauthorized, Forbidden

class AuthorizationHandlerBase(with_metaclass(ABCMeta, object)):

    def __init__(self, *args, **kwargs):
        pass

    @abstractmethod
    def authorize(self, controller, request, user, token, *view_args, **view_kwargs):
        pass


class AuthenticationRequired(AuthorizationHandlerBase):

    def authorize(self, controller, request, user, token, *view_args, **view_kwargs):
        if not user:
            raise Unauthorized("This endpoint requires authentication")


class TokenRequired(AuthenticationRequired):

    def __init__(self, *tokens):
        super(TokenRequired, self).__init__()
        self.tokens = tokens

    def authorize(self, controller, request, user, token, *view_args, **view_kwargs):

        super(TokenRequired, self).authorize(controller, request, user, token, *view_args, **view_kwargs)

        if self.tokens and not any([(perm in self.tokens) for perm in token.permissions]):
            raise Forbidden


class RoleRequired(AuthenticationRequired):

    def __init__(self, *roles):
        super(RoleRequired, self).__init__()
        self.roles = roles

    def authorize(self, controller, request, user, token, *view_args, **view_kwargs):

        super(AuthenticationRequired, self).authorize(controller, request, user, token, *view_args, **view_kwargs)

        if self.roles and not any([(role.id in self.roles) for role in user.roles]):
            raise Forbidden
