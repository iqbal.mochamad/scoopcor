"""
HTTP Utilities
==============

Simple utility classes used for various HTTP-related tasks.
"""
from __future__ import unicode_literals, absolute_import
from hashlib import md5
import json

from enum import Enum
import re

from flask import request


def get_offset_limit(current_request=None):
    current_request = current_request or request
    offset = current_request.args.get('offset', type=int, default=0)
    limit = current_request.args.get('limit', type=int, default=20)
    return offset, limit


class LinkHeaderRel(Enum):
    """ Represents the 'rel' value of a link header, with a few of the more
    common choices;  however, you're not restricted to ONLY using these values.
    """
    first = 'first'
    next = 'next'
    previous = 'previous'
    prev = 'prev'
    last = 'last'


class LinkHeaderParser(object):
    """ Parses a complete HTTP link header string.
    """
    def __init__(self, link_header_complete):
        """
        :param `str` link_header_complete:
        """
        self.links = [LinkHeaderField.from_string(link.strip()) for link
                      in link_header_complete.split(',')]

    def get(self, link_rel):
        links = [link for link in self.links if link.rel == link_rel]
        return links[0] if links else None


class LinkHeaderField(object):
    """ A single link value from an HTTP link header string.
    """
    def __init__(self, url, rel, title=None):
        self.url = url
        self.rel = rel
        self.title = title

    @classmethod
    def from_string(cls, link_header_part):
        """ Alternate constructor when only have an unparsed string field.

        :param `str` link_header_part:
            A single field from a link header (ie, a complete section of a CSV
            link header).
        :return: A new instance of a LinkHeaderField
        """
        return LinkHeaderField(
            url=LinkHeaderField._parse_url(link_header_part),
            rel=LinkHeaderField._parse_rel(link_header_part),
            title=LinkHeaderField._parse_title(link_header_part)
        )

    @staticmethod
    def _parse_rel(link_header_part):
        m = re.search(r';\srel="(?P<rel>\w+)"(;|$)',
                      link_header_part, re.IGNORECASE)
        try:
            rel_str = m.groupdict().get('rel')
            return LinkHeaderRel(rel_str)
        except (ValueError, AttributeError):
            raise MalformedLinkHeader("HTTP Link Header missing/malformed "
                                      "'rel': {}".format(link_header_part))

    @staticmethod
    def _parse_url(link_header_part):
        m = re.search(r'^<(?P<url>.+)>', link_header_part, re.IGNORECASE)
        try:
            return m.groupdict().get('url')
        except AttributeError:
            raise MalformedLinkHeader(
                "Invalid format for HTTP "
                "Link Header: {}.".format(link_header_part))

    @staticmethod
    def _parse_title(link_header_part):
        m = re.search(r';\stitle="(?P<title>.+)"(;|$)',
                      link_header_part, re.IGNORECASE)
        return m.groupdict().get('title') if m is not None else None

    def __eq__(self, other):
        # are link, title and url attributes all equivalent?
        props = ['rel', 'url', 'title', ]
        return all([getattr(self, p) == getattr(other, p) for p in props])

    def __str__(self):
        link = '<{url}>; rel="{rel.value}"'.format(**vars(self))
        link += '; title="{title}"'.format(**vars(self)) if self.title else ""
        return link


class MalformedLinkHeader(Exception):
    """ Raised when an `HTTP link header`_ is in an invalid format.

    .. _HTTP link header: https://www.w3.org/wiki/LinkHeader
    """
    pass


def weak_etag_generator(response_body, list_object_key):
    """ Creates a `weak etag`_ based upon our standard list response format.

    >>> fake_response_body = '''
    ... {
    ...     "scoops": [1,2,3,4],
    ...     "metadata": {
    ...         "resultset": {
    ...             "count": 6902774,
    ...             "limit": 20,
    ...             "offset": 0
    ...         }
    ...     }
    ... }'''
    >>> weak_etag_generator(fake_response_body, 'scoops')
    'd5397571a7f9c05bd58bed77f9dbe8f0'

    :param `dict` response_body: The entire response content (including metadata) that will be serialized into
        our (usually) JSON response.
    :param `six.string_types` list_object_key: The key in the dictionary that actually contains our response data.
    :return: A value suitable for assigning an etag.
    :rtype: six.string_types

    .. _weak etag: https://en.wikipedia.org/wiki/HTTP_ETag#Strong_and_weak_validation
    """
    try:
        return md5(json.dumps(json.loads(response_body)[list_object_key])).hexdigest()
    except (KeyError, ValueError):
        return None

