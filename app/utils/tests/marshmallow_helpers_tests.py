from __future__ import unicode_literals, absolute_import

import json
from httplib import NO_CONTENT, OK, CREATED

from faker import Factory
from flask import g
from flask_appsfoundry.models import SoftDeletableMixin
from marshmallow import fields
from marshmallow_sqlalchemy import ModelSchema
from slugify import slugify

from app import db, ma
from app.auth.models import UserInfo
from app.constants import RoleType
from app.utils.marshmallow_helpers import update_entity, schema_load, create_slug, soft_delete_response, create_response, \
    update_response, option_response
from app.utils.testcases import TestBase


class DummyModel(SoftDeletableMixin, db.Model):

    name = db.Column(db.Text)
    description = db.Column(db.Text)
    slug = db.Column(db.Text)


class DummySchema(ModelSchema):
    class Meta:
        model = DummyModel
        sqla_session = db.session


class DummyMaSchema(ma.Schema):
    name = fields.Str()
    description = fields.Str()
    slug = fields.Str()


class HelpersTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(HelpersTests, cls).setUpClass()
        cls.dummy = DummyModel(
            name=cls.fake.name(),
            description=cls.fake.text(),
            slug='a-b-c',
            is_active=True
        )
        cls.session.add(cls.dummy)
        cls.session.commit()

    def setUp(self):
        super(HelpersTests, self).setUp()
        self.json_data = {
            "name": self.fake.name(),
            "description": self.fake.text(),
            "unknown_field": self.fake.bs()
        }

    def test_update_entity(self):
        with self.on_session(self.dummy):
            update_entity(self.dummy, schema=DummyMaSchema(), json_data=self.json_data)
            self.assertEqual(self.dummy.name, self.json_data.get('name'))
            self.assertEqual(self.dummy.description, self.json_data.get('description'))

    def test_schema_load(self):
        with self.on_session(self.dummy):
            # if schema is inherited from ma.Schema
            entity_dict = schema_load(schema=DummyMaSchema(), json_data=self.json_data)
            self.assertEqual(entity_dict.get('name'), self.json_data.get('name'))
            self.assertEqual(entity_dict.get('description'), self.json_data.get('description'))

            # if schema is inherited from ModelSchema
            entity = schema_load(schema=DummySchema(), json_data=self.json_data,
                                 session=self.session)
            self.assertEqual(entity.name, self.json_data.get('name'))
            self.assertEqual(entity.description, self.json_data.get('description'))
            entity = schema_load(schema=DummySchema(), json_data=self.json_data)
            self.assertEqual(entity.name, self.json_data.get('name'))

            # load request body (request.data or optional json_data)
            #   to an instance of model
            dummy_data = schema_load(
                schema=DummySchema(),
                instance=self.dummy,
                session=self.session,
                json_data=self.json_data,
                )
            self.assertEqual(dummy_data, self.dummy)
            self.assertEqual(dummy_data.name, self.json_data.get('name'))
            self.assertEqual(dummy_data.description, self.json_data.get('description'))
            # commit change to database
            self.session.commit()
            # assert saved data
            saved_data = self.session.query(DummyModel).get(self.dummy.id)
            self.assertEqual(saved_data.name, self.json_data.get('name'))
            self.assertEqual(saved_data.description, self.json_data.get('description'))

    def test_create_slug(self):
        with self.on_session(self.dummy):
            name = self.fake.name()
            new_slug = create_slug(name=name, model=DummyModel, session=self.session)
            self.assertEqual(new_slug, slugify(name))
            new_slug_but_exists = create_slug(name=self.dummy.slug, model=DummyModel, session=self.session)
            self.assertEqual(new_slug_but_exists, self.dummy.slug + '-2')

    def test_create_response(self):

        def before_commit_method(entity, session):
            entity.slug = create_slug(entity.name, DummyModel, session)

        request_data = {
            "name": self.fake.name(),
            "description": self.fake.text(),
        }
        response = create_response(schema=DummySchema(),
                                   session=self.session,
                                   json_data=request_data,
                                   before_commit_method=before_commit_method)
        self.assertEqual(response.status_code, CREATED,
                         'Response status code {0.status_code} != 201 (CREATED), '
                         'response: {0.data}'.format(response))
        actual = json.loads(response.data)
        self.assertDictContainsSubset(request_data, actual)
        self.assertIsNotNone(actual.get('slug'))
        # compare saved data vs request body (json_data)
        saved_data = self.session.query(DummyModel).get(actual.get('id'))
        self.assertIsNotNone(saved_data)
        self.assertEqual(saved_data.name, request_data.get('name'))
        self.assertEqual(saved_data.description, request_data.get('description'))

    def test_update_response(self):
        with self.on_session(self.dummy):
            request_data = {
                "name": self.fake.name(),
                "description": self.fake.text(),
                "slug": self.dummy.slug
            }
            response = update_response(schema=DummySchema(),
                                       instance=self.dummy,
                                       session=self.session,
                                       json_data=request_data,
                                       before_commit_method=None)
        self.assertEqual(response.status_code, OK,
                         'Response status code {0.status_code} != 200 (OK), '
                         'response: {0.data}'.format(response))
        actual = json.loads(response.data)
        self.assertDictContainsSubset(request_data, actual)
        self.assertIsNotNone(actual.get('slug'))
        # compare saved data vs request body (json_data)
        saved_data = self.session.query(DummyModel).get(actual.get('id'))
        self.assertIsNotNone(saved_data)
        self.assertEqual(saved_data.name, request_data.get('name'))
        self.assertEqual(saved_data.description, request_data.get('description'))

    def test_soft_delete_response(self):
        with self.on_session(self.dummy):
            response = soft_delete_response(self.dummy, self.session)
            self.assertEqual(response.status_code, NO_CONTENT,
                             'Response status code {0.status_code} != 204 (NO_CONTENT), '
                             'response: {0.data}'.format(response))
            self.session.refresh(self.dummy)
            self.assertFalse(self.dummy.is_active)

    def test_option_response(self):
        list_tokens = ['can_read_write_global_all', 'can_read_global_vendor', 'can_read_write_global_vendor', ]
        modify_tokens = ['can_read_write_global_all', 'can_read_write_global_vendor', ]

        # super user:
        g.current_user_roles = [RoleType.super_admin.value, ]
        expected_allowed_options = ['OPTIONS', 'GET', 'PUT', 'DELETE', 'POST', 'HEAD', ]

        response = option_response(list_get_tokens=list_tokens, modify_tokens=modify_tokens)
        allowed_header = response.headers.get('allow', '').split(',')
        self.assertItemsEqual(allowed_header, expected_allowed_options)

        # test user only allowed to get
        g.current_user_roles = [RoleType.verified_user.value, ]
        g.user = UserInfo(
            'username', "LUARBIASAIMNOTATOKENFORREAL", 'email@user.com',
            perm=['can_read_global_vendor', ])
        expected_allowed_options = ['OPTIONS', 'GET', 'HEAD', ]
        response = option_response(list_get_tokens=list_tokens, modify_tokens=modify_tokens)
        allowed_header = response.headers.get('allow', '').split(',')
        self.assertItemsEqual(allowed_header, expected_allowed_options)

        # test user only allowed to get and modify data
        g.current_user_roles = [RoleType.verified_user.value, ]
        g.user = UserInfo(
            'username', "LUARBIASAIMNOTATOKENFORREAL", 'email@user.com',
            perm=['can_read_write_global_vendor', ])
        expected_allowed_options = ['OPTIONS', 'GET', 'PUT', 'DELETE', 'POST', 'HEAD', ]
        response = option_response(list_get_tokens=list_tokens, modify_tokens=modify_tokens)
        allowed_header = response.headers.get('allow', '').split(',')
        self.assertItemsEqual(allowed_header, expected_allowed_options)

