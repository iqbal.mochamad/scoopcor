from unittest import TestCase

from app import app
from app.utils.geo_info import get_geo_info


class GeoInfoTests(TestCase):

    def test_get_geo_info(self):
        mock_header_from_nginx_geoip = {
            "GEOIP_LATITUDE": 123.00,
            "GEOIP_LONGITUDE": 222.00,
            "GEOIP_CITY": "Jakarta",
            "GEOIP_COUNTRY_CODE": "ID",
            "GEOIP_COUNTRY_NAME": "Indonesia",
            "X-Forwarded-For": "127.0.0.1",
        }
        with app.test_request_context('/', headers=mock_header_from_nginx_geoip) as ctx:
            geo_info = get_geo_info()
            self.assertEqual(geo_info.latitude, str(mock_header_from_nginx_geoip.get('GEOIP_LATITUDE')))
            self.assertEqual(geo_info.longitude, str(mock_header_from_nginx_geoip.get('GEOIP_LONGITUDE')))
            self.assertEqual(geo_info.country_code, mock_header_from_nginx_geoip.get('GEOIP_COUNTRY_CODE'))
            self.assertEqual(geo_info.city, mock_header_from_nginx_geoip.get('GEOIP_CITY'))
            self.assertEqual(geo_info.country_name, mock_header_from_nginx_geoip.get('GEOIP_COUNTRY_NAME'))
            self.assertEqual(geo_info.client_ip, mock_header_from_nginx_geoip.get('X-Forwarded-For'))
