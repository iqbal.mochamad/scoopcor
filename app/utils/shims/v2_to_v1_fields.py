from flask_restful import fields
import json
import os


# TODO: discuss what we need to do if the language isn't/wasn't in the
# original database..  Maybe we should just return a null value
# or a fake languages[ { id: 999, name: 'German' }, ]
# - I actually think that returning a fake language/country is the best
#   way forward, but confirm with the rest of the team before doing so.

class V1LanguageDataFromIso639(fields.Raw):
    """
    In a refactor of the items data, we dropped the core_language table.
    We're no longer storing languages as a separate table, but as ISO codes,
    so this acts as a shim so v1/items/ endpoints can still output the same
    format (which included the database id) of data and maintain compatibility.
    """
    _DATA = json.load(open(os.path.join(os.path.dirname(__file__),
                                        'data',
                                        'core_languages.json')))

    # mapping of iso 639 language codes to their database ids
    _MAPPING = {
        'en': 1, 'eng': 1,
        'id': 2, 'ind': 2,
        'zh': 3, 'chi': 3, 'zho': 3,
        'vi': 4, 'vie': 4,
        'ta': 5, 'tam': 5,
        'te': 6, 'tel': 6,
        'ml': 7, 'mal': 7,
        'hi': 8, 'hin': 8,
        'ms': 9, 'may': 9, 'msa': 9,
        'pt': 10, 'por': 10,
        'tl': 11, 'tgl': 11,  # NOTE: Tagalog is_active=false in original db
        'th': 12, 'tha': 12,
        'fil': 13,  # NOTE: Filipino has no ISO639-1 code.
        'jp': 14, 'jpn': 14,
        'de': 15, 'deu': 15,
        'ko': 16, 'kor': 16,
        'es': 17, 'spa': 17,
    }

    def format(self, value):
        """
        Returns a dictionary for a language matching the
        /v1/items/ documentation.

        :param value: ISO639 alpha 2 or 3 language code.
        :return: The original dictionary representation for v1's documented api.
        :rtype: dict
        :raises: :py:class:`flask_restful.fields.MarshallingException`
        """
        try:
            lang_code = value.lower()
            v1_db_id = self._MAPPING[lang_code]

            v1_data = filter(
                lambda row: True if row['id'] == v1_db_id else False,
                self._DATA)[0]

            return v1_data

        except (KeyError, IndexError) as e:
            raise fields.MarshallingException(underlying_exception=e)


class V1CountryDataFromIso3166(fields.Raw):
    """
    In a refactor of the items data, we dropped the core_countries table.
    We're no longer storing countries as a separate table, but as ISO codes,
    so this acts as a shim so v1/items/ endpoints can still output the same
    format (which included the database id) of data and maintain compatibility.
    """
    _DATA = json.load(open(os.path.join(os.path.dirname(__file__),
                                        'data',
                                        'core_countries.json')))

    # possible iso3166 country codes to their original db ids
    _MAPPING = {
        'id': 1, 'idn': 1,
        'my': 2, 'mys': 2,
        'sg': 3, 'sgp': 3,
        'cn': 4, 'chn': 4,
        'gb': 5, 'gbr': 5,
        'in': 6, 'ind': 6,
        'vn': 7, 'vnm': 7,
        'ph': 8, 'phl': 8,
        'br': 9, 'bra': 9,
        'us': 10, 'usa': 10,
        'hk': 11, 'hkg': 11,
        'th': 12, 'tha': 12,
        'fr': 13, 'fra': 13,
        'bn': 14, 'brn': 14,
        'es': 15, 'esp': 15,
    }

    def format(self, value):
        """
        Returns a dictionary for a country matching the
        /v1/items/ documentation.

        :param value: ISO3166 alpha 2 or 3 country code.
        :return: The original dictionary representation for v1's documented api.
        :rtype: dict
        :raises: :py:class:`flask_restful.fields.MarshallingException`
        """
        try:
            country_code = value.lower()

            v1_db_id = self._MAPPING[country_code]
            v1_data = filter(
                lambda row: True if row['id'] == v1_db_id else False,
                self._DATA)[0]

            return v1_data
        except (KeyError, IndexError) as e:
            raise fields.MarshallingException(underlying_exception=e)
