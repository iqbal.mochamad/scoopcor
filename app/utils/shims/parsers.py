"""
Parsers
=======

Extra parser fields for our Filter/Single parsers.
"""
from __future__ import unicode_literals

from flask_appsfoundry.parsers import StringFilter


def wildcard_sql(input_string):
    """ Add wildcard '%{input_string}%'

    >>> wildcard_sql('anything')
    u'%anything%'

    :param `unicode` input_string:
        Any string that you want to surround with sql 'match-any' wildcards.
    :return: returning string with wildcard
    :rtype: `unicode`
    """
    return "%{}%".format(input_string)


class WildcardFilter(StringFilter):
    """ Parse input and added wildcard for the purpose query

    >>> WildcardFilter().type('anything') == u"%anything%"
    True
    """
    _default_operator = 'ilike'

    def __init__(self, **kwargs):
        kwargs['type'] = wildcard_sql
        super(WildcardFilter, self).__init__(**kwargs)
