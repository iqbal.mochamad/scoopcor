def iso3166_to_id(country_code):
    mapping = {'id': 1, 'my': 2, 'sg': 3, 'cn': 4, 'gb': 5, 'in': 6, 'vn': 7, 'ph': 8, 'br': 9, 'us': 10, 'hk': 11, 'th': 12, 'fr': 13, 'bn': 14, 'es': 15, 'jp': 16, 'au': 17, 'kr': 18}
    return mapping[country_code]


def id_to_iso3166(id):
    mapping = {1: 'id', 2: 'my', 3: 'sg', 4: 'cn', 5:'gb', 6:'in', 7:'vn', 8:'ph', 9:'br', 10:'us', 11:'hk', 12:'th', 13:'fr', 14:'bn', 15: 'es', 16: 'jp', 17: 'au', 18: 'kr'}
    return mapping[int(id)]
