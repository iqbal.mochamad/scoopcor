# mapping of iso 639 language codes to their database ids

def iso639_to_id(code):
    mapping = {
        'en': 1, 'eng': 1,
        'id': 2, 'ind': 2,
        'zh': 3, 'chi': 3, 'zho': 3,
        'vi': 4, 'vie': 4,
        'ta': 5, 'tam': 5,
        'te': 6, 'tel': 6,
        'ml': 7, 'mal': 7,
        'hi': 8, 'hin': 8,
        'ms': 9, 'may': 9, 'msa': 9,
        'pt': 10, 'por': 10,
        'tl': 11, 'tgl': 11,  # NOTE: Tagalog is_active=false in original db
        'th': 12, 'tha': 12,
        'fil': 13,  # NOTE: Filipino has no ISO639-1 code.
        'jp': 14, 'jpn': 14,
        'de': 15, 'deu': 15,
        'ko': 16, 'kor': 16,
        'es': 17, 'spa': 17,
    }
    return mapping[code]

def id_to_iso639_alpha3(id):
    mapping = {
        1: 'eng',
        2: 'ind',
        3: 'zho',
        4: 'vie',
        5: 'tam',
        6: 'tel',
        7: 'mal',
        8: 'hin',
        9: 'msa',
        10: 'por',
        11: 'tgl',
        12: 'tha',
        13: 'fil',
        14: 'jpn',
        15: 'deu',
        16: 'kor',
        17: 'spa',
    }
    return mapping[int(id)]

