from app.items.choices.item_type import ITEM_TYPES

def id_to_enum(id):
    return ITEM_TYPES[id]

def enum_to_id(enum):
    reversed_enum = {v: k for k, v in ITEM_TYPES.items()}
    return reversed_enum[enum]
