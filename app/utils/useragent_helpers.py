"""
User Agent Helpers
==================

Helper classes and functions for parsing out the User-Agent/X-Scoop-Client header to identify the requesting client.

Typically, we would expect front-end apps to send requests identifying themselves in the following format:


.. code-block:: http

    User-Agent: Scoop IOS/1.2.3 (Any Extra Information they wish to send)

Alternatively, for applications that CANNOT override the User-Agent header (e.g., any browser-based applications,
like Scoop Portal), we allow the same information to be provided in a header named **X-Scoop-Client**.

NOTE:  The application name is stored in our database with _, instead of spaces (e.g., 'scoop_ios', not 'scoop ios').
"""
from __future__ import absolute_import, unicode_literals
import re


class UserAgentMeta(object):
    """ DTO containing parsed header information.
    """
    def __init__(self, app_name, version, metadata):
        self.app_name = app_name.lower().replace(" ", "_")
        self.version = version
        self.metadata = metadata


class UnparsableUserAgent(Exception):
    """ Raised when we can't parse the User-Agent header.
    """
    def __init__(self, user_agent_string):
        self.message = "Unable to parser User-Agent: {}".format(user_agent_string)


def construct_useragent_meta(user_agent):
    """ Converts an HTTP User-Agent string into a `Client`.app_name and retrieves it's specified version string.

    If the provided User-Agent can't be parsed, None will be returned.

    :param `six.string_types` user_agent: The HTTP Request User-Agent string.
    :return: Parsed UserAgent information
    :rtype: UserAgentMeta
    """
    regex = re.compile("^(?P<app_name>.+?)/(?P<version>\S+?)(\s\((?P<metadata>.+?)\))?$")

    result = regex.match(user_agent.encode('utf-8'))

    try:
        return UserAgentMeta(**result.groupdict())
    except AttributeError:
        raise UnparsableUserAgent(user_agent)
