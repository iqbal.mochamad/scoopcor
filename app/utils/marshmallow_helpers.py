from __future__ import unicode_literals, absolute_import
import json
from httplib import CREATED, NO_CONTENT

from flask import g
from flask import request, jsonify
from flask.views import MethodView
from flask_appsfoundry.auth.helpers import assert_user_permission
from flask_appsfoundry.parsers import SqlAlchemyOrderingParser
from flask_appsfoundry.exceptions import BadRequest, UnprocessableEntity, Unauthorized, InternalServerError
from marshmallow import fields
from slugify import slugify
from werkzeug import exceptions as werkzeug_execptions

from app import ma, db
from app.constants import RoleType
from app.utils.controllers import CorApiErrorHandlingMixin
from app.utils.responses import generate_weak_etag


class MaApiBase(CorApiErrorHandlingMixin, MethodView):

    session = db.session
    model = None
    schema = None
    get_security_tokens = ('can_read_write_global_all', 'can_read_write_public',)
    edit_security_tokens = ('can_read_write_global_all', )

    def _validate_before_save(self):
        """ Override this method to make further validation on request body data
        (or use Marshmallow validates_schema decorator on the schema class:
            see CampaignSchema in app/campaign/api.py for example)

        :return:
        """
        pass

    def _before_commit(self, data, session):
        """ Method that will be execute before commit.
        Override this method to make further modification on data before commit to database

        :param data: an instance of sql alchemy object model that will be updated
        :param session: (optional) an instance of sql alchemy session
        :return: None
        """
        pass

    def _get_entity(self, entity_id):
        return self.session.query(self.model).get(entity_id)

    @staticmethod
    def _authorize_request(security_tokens):
        user_perms = g.user.perm if getattr(g, 'user', None) and getattr(g.user, 'perm', []) else []
        if not security_tokens:
            # no security token defined, no need to check user permission
            return
        elif not user_perms:
            # security tokens is defined, but user not logged on, raise un authorized
            raise Unauthorized()
        assert_user_permission(required_permissions=security_tokens, user_perm=user_perms)

    def options(self):
        return option_response(self.edit_security_tokens, self.get_security_tokens)


class MaGetEditBase(MaApiBase):
    """ Base class for GET API, PUT API, and DELETE API

    example:
    # for Class based API: see app/campaigns/api.py
    """

    def get(self, entity_id):
        self._authorize_request(self.get_security_tokens)
        entity = self._get_entity(entity_id)
        return jsonify(self.schema.dump(entity).data)

    def put(self, entity_id):
        self._authorize_request(self.edit_security_tokens)
        self._validate_before_save()

        entity = self._get_entity(entity_id)
        # load/update Campaign data with data from request body (request.data) and return jsonify response
        return update_response(schema=self.schema, instance=entity, session=self.session,
                               before_commit_method=self._before_commit)

    def delete(self, entity_id):
        self._authorize_request(self.edit_security_tokens)

        entity = self._get_entity(entity_id)
        return soft_delete_response(instance=entity, session=self.session)

    def options(self, entity_id):
        return option_response(self.edit_security_tokens, self.get_security_tokens)


class MaListResponse(MaApiBase):
    """ Base class for POST API, LIST/Search API, and Options API

    POST: validate json request, create an entity, then return response based on marshmallow schema
    LIST: Create a list response based on a query set and serialize it with marshmallow schema

    example:
    # for Class based API: see app/campaigns/api.py

    # for Method based API:
    @blueprint.route('/<shared_library:entity>/shared-catalogs')
    def list_catalogs(entity):

        return MaListResponse(
            query=entity.catalogs,
            schema=CatalogSchema(),
            key_name="catalogs",
            filter_parsers=SharedCatalogArgsParser()
        ).response()
    """
    key_name = "data"
    query = None
    filter_parsers = []
    default_filters = []
    ordering = []
    default_limit = 20

    def __init__(self, query=None, schema=None, key_name=None,
                 filter_parsers=None, default_filters=[],
                 ordering=[]):
        """

        :param query:
        :param schema: an instance of marshmallow schema or model schema
        :param key_name:
        :param filter_parsers: an instance of SqlAlchemyFilterParser or it's inherited class
        :param default_filters:
        :param ordering: a list of sql alchemy order syntax
        """
        if query:
            self.query = query
        else:
            if not self.model:
                raise InternalServerError(
                    user_message="Sorry, something went wrong!",
                    developer_message="Model is None (model is not defined properly in Class declaration)"
                )
            self.query = self.session.query(self.model)
        self.schema = schema or self.schema
        self.key_name = key_name or self.key_name
        self.filter_parsers = filter_parsers or self.filter_parsers
        self.default_filters = default_filters or self.default_filters
        self.ordering = ordering or self.ordering

    def post(self):
        self._authorize_request(self.edit_security_tokens)
        self._validate_before_save()
        return create_response(self.schema, session=self.session, before_commit_method=self._before_commit)

    def get(self):
        self._authorize_request(self.get_security_tokens)
        return self.response()

    def _get_filters(self):
        filter_exps = self.filter_parsers.parse_args(request)['filters'] if self.filter_parsers else []

        def_filters = [defexp for defexp in self.default_filters
                       if defexp.left.name
                       not in [reqexp.left.name for reqexp in filter_exps]]

        filter_exps.extend(def_filters)
        return filter_exps

    def _get_ordering(self):
        try:
            ordering = self.ordering or SqlAlchemyOrderingParser().parse_args()['order']
        except werkzeug_execptions.BadRequest as ex:
            raise BadRequest(
                developer_message='{}. Make sure [Content-Type] not exists in request headers'.format(ex.description)
            )
        except Exception as ex:
            raise BadRequest(developer_message=ex.message)
        return ordering

    def response(self):
        for f in self._get_filters():
            self.query = self.query.filter(f)

        # total data found
        total_row = self.query.count()

        limit = request.args.get('limit', default=self.default_limit, type=int)
        offset = request.args.get('offset', default=0, type=int)

        for column_ordering in self._get_ordering():
            self.query = self.query.order_by(column_ordering)

        self.query = self.query.offset(offset).limit(limit)

        entities = self.query.all()

        list_result = [self.schema.dump(entity).data for entity in entities]

        response = jsonify({
            # for each data in entities, dump/serialize it with an instance of Marshmallow schema
            self.key_name: list_result,
            'metadata': {
                'resultset': {
                    'limit': limit,
                    'offset': offset,
                    'count': total_row
                }
            }
        })

        weak_etag = self._generate_weak_etag(entities, list_result)
        response.set_etag(weak_etag, weak=True)

        return response.make_conditional(request)

    def _generate_weak_etag(self, entities, list_result):
        # override this method if needed
        return generate_weak_etag(entities)


def update_entity(entity, schema, json_data=None):
    """  validate request body with marshmallow schema then update data

    warning: only for Schema inherited from ma.Schema
        else: data.items() will raise error
        to update using Schema inherited from ModelSchema use schema_load below

    :param entity: an instance of sql alchemy object model that will be updated
    :param schema: an instance of marshmallow schema inherited from ma.Schema
    :param json_data: (optional) dict from request body (request.json)
    :return:
    """
    data = schema_load(schema, json_data=json_data)
    for key, value in data.items():
        if hasattr(entity, key):
            setattr(entity, key, value)


def create_response(schema, session=None, json_data=None, before_commit_method=None):
    """ method for Create API (load request data to a New Flask-Marhsmallow instance and save to db)

    example:

        @blueprint.route('/brands', methods=['POST', ])
        def create_brand():
            return create_response(schema=BrandSchema())

        class BrandSchema(ModelSchema):
            class meta:
                model = Brand
                sqla_session = db.session

    :param schema:
    :param session: (optional) an instance of sql alchemy session
    :param json_data: (optional) dict from request body (request.json)
    :param before_commit_method: method name that will be run before commit
    :return: jsonify response with status code = CREATED
    """
    # create new data based on model in schema
    response = update_response(schema, instance=None, session=session, json_data=json_data,
                               before_commit_method=before_commit_method)
    response.status_code = CREATED
    return response


def update_response(schema, instance=None, session=None, json_data=None, before_commit_method=None):
    """ method for Update API (load request data to a Flask-Marhsmallow instance and save to db)

    example:

        @blueprint.route('/brands/<int:brand_id>', methods=['PUT', ])
        def update_brand(brand_id):
            session = db.session()
            brand = session.query(Brand).get(brand_id)
            return update_response(schema=BrandSchema(), instance=brand, session=session)

        class BrandSchema(ModelSchema):
            class meta:
                model = Brand
                sqla_session = db.session

    :param schema:
    :param instance: (optional) an instance of sql alchemy object model that will be updated
        (if none provided, new one will be created)
    :param session: (optional) an instance of sql alchemy session
    :param json_data: (optional) dict from request body (request.json)
    :param before_commit_method: (optional) method name that will be run before commit
    :return: jsonify response with status code = OK
    """

    session = session or db.session()
    if instance:
        session.add(instance)
    data = schema_load(schema, session, instance, json_data)

    if before_commit_method:
        # run method before commit if provided
        before_commit_method(data, session)

    if not instance:
        # create new record (for create API)
        session.add(data)

    session.commit()
    session.refresh(data)
    resp = jsonify(schema.dump(data).data)
    if hasattr(data, 'api_url'):
        resp.headers['Location'] = data.api_url
    return resp


def schema_load(schema, session=None, instance=None, json_data=None):
    """ Load and validate request data/json with a marshmallow schema

    Load request body (request.data) based on a schema:
    # if schema inherited from ma.Schema, session must be None (else Error)
        data = schema_load(UserSchema())
    # if schema is inherited from ModelSchema, session is optional
        data = schema_load(BannerSchema(), session)

    Load request data an update to an instance model
    ex: update banner:
        # get existing banner
        banner = session.query(Banner).get(id)
        banner_data = schema_load(schema=BannerSchema(), session=session, instance=banner)
        session.commit()
        # return response:
        return jsonify(BannerSchema().dump(banner_data).data)

    :param schema: an instance of marshmallow schema
    :param session: (optional) an instance of sql alchemy session
    :param instance: (optional) an instance of sql alchemy object model that will be updated
    :param json_data: (optional) dict from request body (request.json)
    :return:
    """
    json_data = json_data or json.loads(request.data)
    if not json_data:
        raise BadRequest()

    # Validate and deserialize input
    if session:
        if instance:
            # load request.data to an instance of data model
            # example: load request.data to Banner instance (to update Banner data)
            data, errors = schema.load(json_data, session=session, instance=instance)
        else:
            # create new data
            json_data.pop('id', None)
            if schema.instance:
                schema.instance = None
            data, errors = schema.load(json_data, session=session, instance=None)
    else:
        data, errors = schema.load(json_data)
    if errors:
        raise UnprocessableEntity(developer_message=json.dumps(errors))
    if not data:
        raise UnprocessableEntity(developer_message='Request data is empty. '
                                                    'Header Content Type must be set to Application/json')
    return data


def soft_delete_response(instance, session=None):
    session = session or db.session
    # instance = session.query(model).get(id)
    instance.is_active = False
    session.add(instance)
    session.commit()
    response = jsonify()
    response.status_code = NO_CONTENT
    return response


def option_response(modify_tokens, list_get_tokens=None):
    allowed_methods = ['HEAD', 'OPTIONS']
    modify_methods = ['POST', 'PUT', 'DELETE']

    if getattr(g, 'current_user_roles', None) and RoleType.super_admin in g.current_user_roles:
        allowed_methods.extend(modify_methods)
        allowed_methods.append('GET')
    else:
        user_perm = g.user.perm if g and getattr(g, 'user', None) else []
        if not list_get_tokens or any([req_perm in user_perm for req_perm in list_get_tokens]):
            allowed_methods.append('GET')

        if any([req_perm in user_perm for req_perm in modify_tokens]):
            # user has permission to modify the data
            allowed_methods.extend(modify_methods)

    response = jsonify()
    response.headers['Allow'] = ",".join(allowed_methods)
    return response


def create_slug(name, model, session):
    with session.no_autoflush:
        new_slug = slugify(name)
        existing_slug_count = session.query(model).filter(
            model.slug.like(new_slug + '%')).count()
        if existing_slug_count:
            new_slug = new_slug + "-" + str(existing_slug_count + 1)
        return new_slug


class SimpleHrefMixin(object):

    id = fields.Int()
    title = fields.Str(attribute='name', dump_only=True)
    href = fields.Str(attribute='api_url', dump_only=True)


class SimpleNameHrefMixin(object):

    id = fields.Int()
    name = fields.Str(dump_only=True)
    href = fields.Str(attribute='api_url', dump_only=True)


class SimpleHRefSchema(ma.Schema):
    id = fields.Int()
    title = fields.Str(attribute="name")
    href = fields.Url(attribute="api_url", dump_only=True)


class SimpleSchema(SimpleHRefSchema):
    id = fields.Int()
