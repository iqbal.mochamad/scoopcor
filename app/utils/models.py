"""
Database Model Helpers
======================

Various helper classes and functions to make working with SQLAlchemy model classes easier.
"""
from __future__ import unicode_literals, absolute_import

from enum import Enum
from flask import url_for
from flask_appsfoundry.models import BaseDatamodelMixin
from sqlalchemy import DateTime
from sqlalchemy import orm, create_engine, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Enum as sqlalchemy_enum
from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.types import TypeDecorator, String

from app.utils.datetimes import get_utc
from app.utils.strings import camel_to_snake
from app import app, db


engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'])


session_factory = orm.sessionmaker(bind=engine)
Session = orm.scoped_session(orm.sessionmaker(bind=engine))


meta = MetaData(bind=engine)
Base = declarative_base(metadata=meta)


__all__ = ('pg_enum_column', 'url_mapped', 'get_mapped_attribute', 'get_url_for_model', )


def pg_enum_column(enum_type, **kwargs):
    """ Creates a POSTGRES Enum column, based on a Python3x-style Enum value.

    >>> class SomeEnum(Enum):
    ...     a = 'A'
    ...     b = 'B'
    >>> c = pg_enum_column(SomeEnum, nullable=False)
    >>> c.type
    ENUM(u'A', u'B', name='some_enum')
    >>> c.nullable
    False

    :param `enum.Enum` enum_type: Any enum value.
    :param kwargs: Extra keyword arguments to pass to Column's constructor.
    :return: A new ENUM Column.
    :rtype: `sqlalchemy.Column`
    """
    return Column(ENUM(*[en.value for en in enum_type],
                       name=camel_to_snake(enum_type.__name__)),
                  **kwargs)


_mappings = {}


def url_mapped(endpoint, mapping_args=None):
    """ Data class decorator for registering models that map against an a single URL.

    .. sourcecode:: python

        @app.route('/books/<int:db_id>', endpoint='book_details')
        def book_details(db_id):
            pass

        @url_mapped('book_details', (('db_id', 'id'), ))
        class Book(db.Model):
            id = db.Column(db.Integer, primary_key=True)

    :param `six.string_types` endpoint: The string registered as the endpoint name.
    :param `tuple` mapping_args: A tuple of 2-element tuples (the first element should by the URL capture key,
        followed by the model attribute name)
    """
    def wrapper(cls):
        _mappings[cls] = (endpoint, mapping_args)
        return cls
    return wrapper


def get_mapped_attribute(model_instance, attribute_name):
    """

    :param `object` model_instance:
    :param `six.string_types` attribute_name: ex = catalog.shared_library.id
    :return:
    """
    attributes = attribute_name.split('.')
    if len(attributes) == 1:
        # ex: attribute_name = 'id'
        return getattr(model_instance, attribute_name)
    else:
        current_instance = getattr(model_instance, attributes[0])
        attributes.pop(0)
        attribute_name_left = ".".join(attributes)
        return get_mapped_attribute(current_instance, attribute_name_left)


def get_url_for_model(model_instance, _second_try=False):
    """ Fetches the absolute URL for a mapped model class.

    :param `object` model_instance: Some data model class that was mapped with
    :param `bool` _second_try: Internal use.  Don't set this yourself!
        Indicates if this is the second try at finding the URL.
    :return: An absolute URL for the given model.
    :rtype: six.string_types

    .. seealso: :py:func:`url_mapped`
    """
    try:
        # look for direct class match
        (endpoint, mapping_args) = _mappings[type(model_instance)]

        mapped_args = {map_arg[0]: get_mapped_attribute(model_instance, map_arg[1]) for map_arg in mapping_args}

        return url_for(
            endpoint,
            _external=True,
            **mapped_args
        )
    except RuntimeError:
        # runtime error means that this method was requested outside of an application context.
        # manually generate an
        if _second_try:
            raise

        ctx = app.app_context()
        ctx.push()

        url = get_url_for_model(model_instance, True)

        ctx.pop()

        return url

    except KeyError:
        # look for subclass match
        for cls, (endpoint, mapping_args) in _mappings.items():
            if issubclass(type(model_instance), cls):
                return url_for(
                    endpoint,
                    _external=True,
                    **{map_arg[0]: getattr(model_instance, map_arg[1]) for map_arg in mapping_args}
                )

    return None


class ScoopCommonMixin(object):
    """ Includes common functionality found in all the model classes such as save, delete, and the values() method.

    .. deprecated:: 2.10.0

        Use the base model classes provided by Flask-AppsFoundry, and avoid creating methods on
        data models.  There are better places for this logic to go.
    """
    def save(self):
        """ Attempts to save the object's current state to the session.
        If it fails, the transaction will be rolled back.  Returns a
        dictionary to indicate success status.
        """
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False,
                    'message': "{type_name} added".format(
                        type_name=self.__class__.__name__)}
        except Exception as e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        """ Attempts to remove an object from the database.
        If this fails, the transaction will be caught and rolled back.
        Returns a dictionary to indicate operation success/failure.
        """
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False,
                    'message': "{type_name} deleted".format(
                        type_name=self.__class__.__name__)}
        except Exception as e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        """ Returns a dictionary of all the fields specified in the
        class.__serialization_fields__ dictionary.  If this dictionary
        is not present, raises a TypeError.
        """
        if not hasattr(self, '__serialization_fields__'):
            raise TypeError('class must define __serialization_fields__')

        return {fname: getattr(self, fname)
                for fname in self.__serialization_fields__}


class Py3EnumType(TypeDecorator):

    impl = sqlalchemy_enum

    def __init__(self, py3_enum, name):
        self.py3_enum = py3_enum
        super(Py3EnumType, self).__init__(*[e.value for e in py3_enum], name=name)

    def process_bind_param(self, value, dialect=None):
        return value.value

    def process_result_value(self, value, dialect=None):
        return self.py3_enum(value)


class TimestampMixinWithTimezone(BaseDatamodelMixin):
    """ Model with auto-updating 'created' and 'modified' datetime properties.

    Includes support for automatic updating of these records.  Note: this is done in python, not at the database level.
    """
    created = Column(DateTime(timezone=True),
                     default=get_utc)
    modified = Column(DateTime(timezone=True),
                      default=get_utc,
                      onupdate=get_utc)
