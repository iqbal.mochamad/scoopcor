import json

from app.utils.datetimes import get_local

ONE_DAY = 86400


class RedisCache(object):
    def __init__(self, redis, expiry=ONE_DAY):
        self.redis = redis
        self.expiry = expiry

    def write(self, key, data):
        cache_data = json.dumps(data, sort_keys=True)
        # print("About to write the cache data -> ", cache_data)
        if self.expiry:
            self.redis.setex(key, self.expiry, cache_data)
        else:
            self.redis.set(key, cache_data)

    def write_hash(self, key, data, etag):
        cache_data = json.dumps(data, sort_keys=True)
        # print("About to write the cache data -> ", cache_data)
        if self.expiry:
            self.redis.hmset(key, {"etag": etag, "data": cache_data, "created": get_local()})
            self.redis.expire(key, self.expiry)
        else:
            self.redis.hmset(key, {"etag": etag, "data": cache_data, "created": get_local()})

    def get(self, key):
        return self.redis.get(key)

    def get_hash(self, key):
        return self.redis.hgetall(key)

    def delete_with_key_prefix(self, key):
        [self.redis.delete(cached_key) for cached_key in self.redis.scan_iter('{}*'.format(key))]

    def delete(self, key):
        self.redis.delete(key)
