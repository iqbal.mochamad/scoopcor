"""
Parser Fields
=============

These are little helper functions that assist with parsing incoming data to
the API, into a more proper python representation (usually for processing
further, eg, assigning to our SQLAlchemy model attributes).
"""
import datetime
from dateutil.parser import parse

from flask_appsfoundry.parsers.filterinputs import StringFilter


def split_csv_string(input_str):
    """ Takes any input CSV string and returns a list.

    .. note::

        I don't understand the point of this, because the Scoop-Flask parsers
        do this just fine all by themselves.

    >>> split_csv_string(u"A,B,C")
    [u'A', u'B', u'C']

    :param `unicode` input_str: Any CSV-encoded string.
    :return: A list of strings.
    """
    return input_str.split(',')


def date_or_datetime_iso8601_string_to_date(stringified_datetime):
    """ Converts a string to a datetime without a timezone component.

    >>> date_or_datetime_iso8601_string_to_date("2015-02-15")
    datetime.datetime(2015, 2, 15, 0, 0)

    >>> date_or_datetime_iso8601_string_to_date("2015-02-15T00:00:00.000Z")
    datetime.datetime(2015, 2, 15, 0, 0)

    >>> date_or_datetime_iso8601_string_to_date("ABCD")
    Traceback (most recent call last):
       ...
    TypeError: 'NoneType' object is not iterable

    .. warning::

        This is essentially a hack to allow datetime objects to be posted
        without including a 'time' component.  Far, far better to modify
        the underlying database to accept a 'date' only value, and to modify
        our ISO8601 parser to read 'date' and not just 'datetime' input values.

    :param `basestring` stringified_datetime:
        A string in either ScoopCore V1 format *OR* ISO8601 and converts that
        to a datetime.
    :return: Naive (no timezone information) datetime
    :rtype: datetime.datetime
    """
    dt = parse(stringified_datetime)
    dt = dt.replace(tzinfo=None)
    return dt


def create_sql_wildcard_field(input_string):
    """ Takes an input string and surrounds it with '%' for use in a SQL query.

    >>> create_sql_wildcard_field("anything")
    u'%anything%'

    :param `str` input_string:
        Any string =D
    :return:
    """
    return u"%{}%".format(input_string)


class WildcardFilter(StringFilter):
    """
    >>> WildcardFilter().type('anything')
    u'%anything%'
    """

    def __init__(self, **kwargs):
        kwargs['type'] = create_sql_wildcard_field
        super(WildcardFilter, self).__init__(**kwargs)