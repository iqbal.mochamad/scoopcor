class HrefAttribute(object):
    def __init__(self, title, href):
        self.title = title
        self.href = href


class HrefAttributeWithRel(HrefAttribute):
    def __init__(self, title, href, rel):
        super(HrefAttributeWithRel, self).__init__(title, href)
        self.rel = rel


class HrefAttributeWithId(HrefAttribute):
    def __init__(self, title, href, id):
        super(HrefAttributeWithId, self).__init__(title, href)
        self.id = id


class HrefAttributeWithRelId(HrefAttributeWithRel):
    def __init__(self, title, href, rel, id):
        super(HrefAttributeWithRelId, self).__init__(title, href, rel)
        self.id = id


class RatingAttribute(object):
    def __init__(self, average, total):
        self.average = average
        self.total = total
