from flask import Blueprint
from flask_restful import Api

from .api import CafeLogListApi, IabErrorLogListApi, IabErrorLogApi

logs_blueprint = Blueprint('logs', __name__, url_prefix='/v1/logs')

logs_api = Api(logs_blueprint)
logs_api.add_resource(CafeLogListApi, '/cafes')

logs_api.add_resource(IabErrorLogListApi, '/iab_errors')

logs_api.add_resource(IabErrorLogApi, '/iab_errors/<int:db_id>',
                      endpoint='logs')