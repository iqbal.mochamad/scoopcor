NEW = u'new'
PROCESSING = u'processing'
ERROR = u'error'
COMPLETE = u'complete'


STATUSES = [
    NEW,
    PROCESSING,
    ERROR,
    COMPLETE,
]

