"""
Log Helpers
===========

These are commonly-used logging calls that can be reused in multiple modules.
"""
from __future__ import unicode_literals
from flask import request, g

from flask_restful.fields import DateTime

def record_primary_entity_inserted(mapper, connection, target, logger):
    """ Called when an SQLAlchemy model has been created.

    .. warning::

        This will not be fired if a bulk insert is issued.

    .. info::

        Inserts that occur outside of a request environment will still
        be logged, but the identity will not be recorded.

    :param `logging.Logger` logger: The logging instance to use.
    :param mapper: Not used in the context of this request.
    :param connection: The current SQLAlchemy connection that's being used.
    :param target: The model instance that was created.
    """
    # noinspection PyBroadException
    try:
        log_message = "{0.__class__.__name__} Created(id={0.id})".format(target)
        logger.info(log_message)
    except Exception:
        logger.exception("Could not log insert")


def record_primary_entity_updated(mapper, connection, target, logger):
    """ Called when a SQLAlchemy model has been updated/soft-deleted.

    .. warning::

        This will not be fired if a bulk update is issued.

    .. info::

        Inserts that occur outside of a request environment will still
        be logged, but the identity will not be recorded.

    :param `logging.Logger` logger: The logging instance to use.
    :param mapper:
    :param connection: The current SQLAlchemy connection that's being used.
    :param target: The model instance that was updated.
    """
    log_format = (
        "{action} {target.__class__.__name__}(id={target.id})"
    )
    # noinspection PyBroadException
    try:
        action_message = \
            "Deleted (soft)" if request and request.method == 'DELETE' else "Updated"

        logger.info(
            log_format.format(
                action=action_message,
                target=target))

    except Exception:
        logger.exception("Could not log update/delete")



