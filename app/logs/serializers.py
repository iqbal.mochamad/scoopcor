from flask_appsfoundry.serializers import SerializerBase
from flask_appsfoundry.serializers import fields

from flask_appsfoundry import serializers

from app.logs.choices import STATUSES


class CafeLogSerializer(serializers.SerializerBase):
    id = fields.Integer
    client_id = fields.Integer
    cafe_email = fields.String
    user_email = fields.String


class IabErrorLogSerializer(SerializerBase):
    id = fields.Integer
    user_id = fields.String
    status = fields.String

    iab_order_id = fields.String
    package_name = fields.String

    product_id = fields.String
    purchase_time = fields.String
    purchase_state = fields.Integer
    developer_payload = fields.String
    purchase_token = fields.String
    is_active = fields.Boolean
    created = fields.DateTime('iso8601')
    modified = fields.DateTime('iso8601')
