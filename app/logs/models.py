from sqlalchemy.dialects import postgresql

from app import db
from flask_appsfoundry.models import TimestampMixin, SoftDeletableMixin

from .choices import STATUSES

"""
    LEGEND:
        STATUS PROCESSING
        1. NEW
        2. IN-PROCESS
        3. COMPLETE
        4. ERROR
        5. DELIVER EMAILS
"""


class ApiLog(db.Model, TimestampMixin):

    __tablename__ = 'core_logs'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    http_method = db.Column(db.String(7))
    user_id = db.Column(db.Integer)
    role_id = db.Column(db.Integer)
    request = db.Column(db.Text)
    respons = db.Column(db.Text)
    response_code = db.Column(db.Integer)
    err_message = db.Column(db.Text)
    ip_address = db.Column(db.String(20))
    headers = db.Column(db.Text)

    def __repr__(self):
        return '<name %s>' % self.name

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
        except Exception, e:
            db.session.rollback()
            return True

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
        except Exception, e:
            db.session.rollback()
            return True

    def values(self):
        rv = {}
        rv['id'] = self.id
        rv['name'] = self.name
        rv['http_method'] = self.http_method
        rv['user_id'] = self.user_id
        rv['role_id'] = self.role_id
        rv['request'] = self.request
        rv['respons'] = self.respons
        rv['response_code'] = self.response_code
        rv['err_message'] = self.err_message
        rv['ip_address'] = self.ip_address
        rv['headers'] = self.headers
        return rv


class ItemLog(db.Model, TimestampMixin):
    __tablename__ = 'core_itemlogs'

    id = db.Column(db.Integer, primary_key=True)

    item_id = db.Column(db.Integer)
    brand_id = db.Column(db.Integer)
    subs_id = db.Column(db.Integer)
    useritem_id = db.Column(db.Integer)
    status = db.Column(db.Integer)
    error = db.Column(db.Text)
    user_id = db.Column(db.Integer)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "ItemLog success add"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
        except Exception, e:
            db.session.rollback()
            return True

    def values(self):
        rv = {}
        rv['id'] = self.id
        rv['item_id'] = self.item_id
        rv['brand_id'] = self.brand_id
        rv['subs_id'] = self.subs_id
        rv['useritem_id'] = self.useritem_id
        rv['status'] = self.status
        rv['user_id'] = self.user_id

        return rv


class ItemProcessing(db.Model, TimestampMixin):

    __tablename__ = 'core_itemprocesses'

    # note: I checked this in the production db.  no item_id 0 or nulls exist.
    item_id = db.Column(db.Integer, db.ForeignKey('core_items.id'), nullable=False)
    #item = db.relationship("Item", backref='processing_logs')
    brand_id = db.Column(db.Integer)
    status = db.Column(db.Integer) #READ LEGEND : STATUS PROCESSING
    is_notified = db.Column(db.Boolean(), default=False)
    error = db.Column(db.Text)
    process_starts = db.Column(db.DateTime)
    process_ends = db.Column(db.DateTime)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "ItemProcessing success add"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
        except Exception, e:
            db.session.rollback()
            return True

    def values(self):
        rv = {}
        rv['id'] = self.id
        rv['item_id'] = self.item_id
        rv['brand_id'] = self.brand_id
        rv['status'] = self.status
        rv['is_notified'] = self.is_notified
        rv['error'] = self.error
        return rv


class CafeLog(TimestampMixin, db.Model):

    __tablename__ = 'core_logs_cafes'

    id = db.Column(db.Integer, primary_key=True)
    client_id = db.Column(db.Integer)
    cafe_email = db.Column(db.String(250), nullable=False)
    user_email = db.Column(db.String(250), nullable=False)


class IabErrorLog(TimestampMixin, SoftDeletableMixin, db.Model):
    __tablename__ = 'core_logs_iab'

    user_id = db.Column(db.Integer, nullable=False)
    status = db.Column(postgresql.ENUM(*STATUSES,
                                       name='scoop_iab_status'))
    iab_order_id = db.Column(db.String, doc="Order ID from Google ")
    package_name = db.Column(db.String)

    product_id = db.Column(db.String)
    purchase_time = db.Column(db.String)
    purchase_state = db.Column(db.Integer)
    developer_payload = db.Column(db.String)
    purchase_token = db.Column(db.String)

