from flask_appsfoundry.parsers import (
    ParserBase, InputField, SqlAlchemyFilterParser, IntegerField, BooleanField)

from flask_appsfoundry.parsers.filterinputs import (
    IntegerFilter, StringFilter, DateTimeFilter, BooleanFilter)

from app.logs.choices import STATUSES, NEW
from app.logs.models import CafeLog, IabErrorLog


class CafeLogArgsParser(ParserBase):
    client_id = InputField(required=True)
    cafe_email = InputField(required=True)
    user_email = InputField(required=True)


class CafeLogListArgsParser(SqlAlchemyFilterParser):

    __model__ = CafeLog

    id = IntegerFilter()
    client_id = IntegerFilter()
    cafe_email = StringFilter()
    user_email = StringFilter()


class IabErrorLogArgsParser(ParserBase):
    user_id = IntegerField(required=True)
    status = InputField(choices=STATUSES, default=NEW)
    iab_order_id = InputField()
    package_name = InputField()

    product_id = InputField()
    purchase_time = InputField()
    purchase_state = IntegerField()
    developer_payload = InputField()
    purchase_token = InputField()
    is_active = BooleanField(default=True)


class IabErrorLogListArgsParsers(SqlAlchemyFilterParser):
    __model__ = IabErrorLog

    id = IntegerFilter()
    user_id = IntegerFilter()
    status = StringFilter(choices=STATUSES)
    iab_order_id = StringFilter()
    package_name = StringFilter()

    product_id = StringFilter()
    purchase_time = StringFilter()
    purchase_state = IntegerFilter()
    developer_payload = StringFilter()
    purchase_token = StringFilter()
    is_active = BooleanFilter()
