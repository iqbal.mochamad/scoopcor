from __future__ import unicode_literals
from flask_appsfoundry.controllers import ListCreateApiResource, \
    DetailApiResource
from flask_appsfoundry import controllers
from sqlalchemy import desc

from app.logs.models import CafeLog, IabErrorLog
from app.logs.parser import CafeLogArgsParser, CafeLogListArgsParser, \
    IabErrorLogArgsParser, IabErrorLogListArgsParsers
from app.logs.serializers import CafeLogSerializer, IabErrorLogSerializer


class CafeLogListApi(controllers.ListCreateApiResource):

    query_set = CafeLog.query

    default_ordering = (desc(CafeLog.id),
                        )
    create_request_parser = CafeLogArgsParser

    list_request_parser = CafeLogListArgsParser

    response_serializer = CafeLogSerializer

    serialized_list_name = 'cafe_logs'

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public_ext',
                           )

    post_security_tokens = ('can_read_write_global_all',
                            'can_read_write_public_ext',
                            )


class IabErrorLogListApi(ListCreateApiResource):
    """
    Controller for fetching lists of Iab Error Log, and creating new Iab Error Log.
    """
    query_set = IabErrorLog.query

    list_request_parser = IabErrorLogListArgsParsers

    create_request_parser = IabErrorLogArgsParser

    serialized_list_name = 'iab_error_log'

    response_serializer = IabErrorLogSerializer

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public_ext',
                           )

    post_security_tokens = ('can_read_write_global_all',
                            'can_read_write_public_ext',
                            )


class IabErrorLogApi(DetailApiResource):
    """
    view individual Iab Error Log
    """
    query_set = IabErrorLog.query

    update_request_parser = IabErrorLogArgsParser

    response_serializer = IabErrorLogSerializer

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public_ext',
                           )

    put_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public_ext',
                           )

    delete_security_tokens = ('can_read_write_global_all',
                              )
