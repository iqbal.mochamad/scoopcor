from __future__ import unicode_literals

import os
from flask import current_app
from six import text_type

from flask_appsfoundry.exceptions import NotFound
from app.items.choices import WebReaderTaskStatus
from app.items.exceptions import (
    WebReaderFilesNotReady)

__all__ = ('get_web_reader_content_pages_directory',
           'assert_web_reader_files_ready', 'get_or_create_processing_job',
           'assert_item_type_valid', 'try_set_task_high_priority',
           'add_user_notification')


def get_web_reader_content_pages_directory(item_id, brand_id):
    """ Fetches the local directory to web_reader content pages for an item.

    :param `int` item_id: Primary key for an `app.items.models.Item`
    :param `int` brand_id: brand_id

    :return: Absolute filesystem path.
    :rtype: str
    """
    return os.path.join(
        current_app.static_folder, 'item_content_files',
        'web_reader', text_type(brand_id), text_type(item_id))


def get_thumbnails_directory(item):

    return os.path.join(current_app.static_folder,
                'item_content_files',
                'thumbnails',
                str(item.brand_id),
                str(item.id))


def assert_web_reader_files_ready(item_id, brand_id, redis):
    """ Verifies that a given Item's web-reader content files are available.

    :param `int` item_id:
        The database primary key for an `app.items.models.Item`.
    :param `int` brand_id: brand_id from an `app.items.models.Item`
    :param `redis.StrictRedis` redis:
        An instance of our caching server used to check if there are any
        pending processing jobs for an item.
    :return:
    """
    from .task_queue import ProcessingTaskManager, ProcessingTask
    if not os.path.exists(get_web_reader_content_pages_directory(item_id, brand_id)):
        raise WebReaderFilesNotReady()

    mgr = ProcessingTaskManager(redis)
    task = mgr.get_by_item_id(item_id)
    if task and task.status != WebReaderTaskStatus.complete:
        raise WebReaderFilesNotReady()


def get_or_create_processing_job(item_id, redis_instance):
    """ Helper method that fetches the ProcessingTask for an item.

    If the task doesn't already exist in the queue, it will be created.

    :param `int` item_id: The Item.id to get/create a ProcessingTask for.
    :param `redis.StrictRedis` redis_instance:
        Backend that ultimately stores the tasks.
    :returns: ProcessingTask for the requested item_id.
    :rtype: `ProcessingTask`
    """
    from .task_queue import ProcessingTaskManager, ProcessingTask

    mgr = ProcessingTaskManager(redis_instance)
    task = mgr.get_by_item_id(item_id)

    if task is None:
        task = ProcessingTask(item_id=item_id)
        mgr.enqueue(task)

    return task


def try_set_task_high_priority(task, redis_instance):
    """ Updates the task to be 'is_high_priority=True' if it's status is still
    pending.

    :param `ProcessingTask` task: The task to update.
    :return: This method has no return value.
    :rtype: None
    """
    from .task_queue import ProcessingTaskManager, ProcessingTask

    mgr = ProcessingTaskManager(redis_instance)

    if task.status == WebReaderTaskStatus.pending:
        task.is_high_priority = True
        mgr.update(task)


def add_user_notification(task, user, redis_instance):
    """

    :param task:
    :param user:
    :param redis_instance:
    :return:
    """
    from .task_queue import ProcessingTaskManager, ProcessingTask
    mgr = ProcessingTaskManager(redis_instance)

    task.notify_users.append(user)
    mgr.update(task)


def assert_item_type_valid(item):
    """ Raises an exception if an Item's content_type is not valid for WebReader

    Currently this means that the Item must be a PDF.

    :param `app.items.models.Item` item: The item to check.
    :return: Nothing
    :raises: `NotFound` if the Item cannot be read through WebReader.
    """
    if item.content_type != 'pdf':
        raise NotFound(
            developer_message="{item} content is {item.content_type}.  "
                              "Only PDFs are currently "
                              "supported".format(item=item),
            user_message="{item.name} is not available through "
                         "the Scoop Web Reader".format(item=item))

