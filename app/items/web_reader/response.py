from __future__ import unicode_literals

import httplib
import os

from flask import make_response, url_for
from six import text_type

from app import app
from app.helpers import request_is_expectation_check
from app.items.web_reader import get_web_reader_content_pages_directory
from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files, aws_generate_link
from app.utils.http import LinkHeaderField, LinkHeaderRel

__all__ = ('construct_links', 'construct_nginx_internal_url',
           'construct_page_response', 'construct_preview_links', 'construct_preview_page_response',)


def construct_links(item, page_num):
    """ Builds the link headers for an Item object.

    :param `app.items.models.Item` item:
    :param `int` page_num: The page number that links should be generated from.
    :return: A list of LinkHeaderFields.
    :rtype: list
    """
    links = []

    can_nav_prev = page_num > 1
    if can_nav_prev:
        links.append(
            LinkHeaderField(
                url=url_for('items.web_reader_page', db_id=item.id, page_num=(page_num-1)),
                rel=LinkHeaderRel.prev))
        links.append(
            LinkHeaderField(
                url=url_for('items.web_reader_page', db_id=item.id, page_num=1),
                rel=LinkHeaderRel.first))

    can_nav_next = page_num < item.page_count
    if can_nav_next:
        links.append(
            LinkHeaderField(
                url=url_for('items.web_reader_page', db_id=item.id, page_num=(page_num+1)),
                rel=LinkHeaderRel.next))
        links.append(
            LinkHeaderField(
                url=url_for('items.web_reader_page', db_id=item.id, page_num=item.page_count),
                rel=LinkHeaderRel.last))

    return links


def construct_preview_links(item, page_num, allowed_pages):
    """ Builds the link headers for an Item object.

    :param `app.items.models.Item` item:
    :param `int` page_num: The page number that links should be generated from.
    :return: A list of LinkHeaderFields.
    :rtype: list
    """
    links = []
    if len(allowed_pages) <= 1:
        return links

    current_page_position = allowed_pages.index(page_num)

    can_nav_prev = page_num > 1
    if can_nav_prev:

        prev_page_num = allowed_pages[current_page_position - 1]

        links.append(
            LinkHeaderField(
                url=url_for('items.preview_page', db_id=item.id, page_num=1),
                rel=LinkHeaderRel.first))
        links.append(
            LinkHeaderField(
                url=url_for('items.preview_page', db_id=item.id, page_num=(prev_page_num)),
                rel=LinkHeaderRel.prev))

    can_nav_next = page_num < item.page_count
    if can_nav_next:
        next_page_num = allowed_pages[current_page_position + 1]

        links.append(
            LinkHeaderField(
                url=url_for('items.preview_page', db_id=item.id, page_num=(next_page_num)),
                rel=LinkHeaderRel.next))
        links.append(
            LinkHeaderField(
                url=url_for('items.preview_page', db_id=item.id, page_num=item.page_count),
                rel=LinkHeaderRel.last))

    return links


def construct_preview_page_response(item, page_num, allowed_pages):
    """ Constructs the HttpResponse for a given web-reader page.

    .. warning::

        This method returns an HTTP response that relies on NGINX-specific
        functionality.  The API will still return a value that can be tested,
        but content will not properly be sent to the client.

    :param `app.items.models.Item` item:
        The item that owns the content being displayed.
    :param `int` page_num:
        The page number currently being displayed.
    :return: An HTTP response.
    """
    if request_is_expectation_check():
        response = make_response("", httplib.CONTINUE)
    else:
        response = make_response("", httplib.OK, {
            'Content-Type': "image/jpeg",
            'X-Accel-Redirect': construct_nginx_internal_url(item, page_num),
            # 'X-Accel-Redirect': construct_preview_page_s3(item, page_num),
            'Link': ",".join([text_type(link) for link
                              in construct_preview_links(item, page_num, allowed_pages)])
        })
    return response


def construct_page_response(item, page_num):
    """ Constructs the HttpResponse for a given web-reader page.

    .. warning::

        This method returns an HTTP response that relies on NGINX-specific
        functionality.  The API will still return a value that can be tested,
        but content will not properly be sent to the client.

    :param `app.items.models.Item` item:
        The item that owns the content being displayed.
    :param `int` page_num:
        The page number currently being displayed.
    :return: An HTTP response.
    """
    if request_is_expectation_check():
        response = make_response("", httplib.CONTINUE)
    else:
        response = make_response("", httplib.OK, {
            'Content-Type': "image/jpeg",
            'X-Accel-Redirect': construct_nginx_internal_url(item, page_num),
            #'X-Accel-Redirect': construct_preview_page_s3(item, page_num),
            'Link': ",".join([text_type(link) for link
                              in construct_links(item, page_num)])
        })
    return response


def construct_nginx_internal_url(item, page_num):
    """ Builds an required NGINX internal url for a given web reader page.

    :param `app.items.models.Item` item: The Item that owns the content page.
    :param `int` page_num: The page number being displayed.
    :return: A stringified URL representation
    :rtype: str
    """
    file_extension = get_image_file_extension(item.id, item.brand_id, page_num)

    return ('{secure_media_base}/'
            'item_content_files/'
            'web_reader/'
            '{item.brand_id}/'
            '{item.id}/'
            '{page_num}.{file_extension}').format(
                secure_media_base=app.config['NGINX_SECURE_MEDIA_BASE'],
                item=item,
                page_num=page_num,
                file_extension=file_extension)


def get_image_file_extension(item_id, brand_id, page_num):
    """ Get the extension of image file of the item

    :param `int` item_id: The Item Id that owns the content page.
    :param `int` brand_id: The Item - Brand - Id that owns the content page.
    :param `int` page_num: The page number being displayed.
    :return: a string of image file extension (jpg or png)
    :rtype: str
    """
    file_path = '{web_reader_item_path}/{page_num}.png'.format(
        web_reader_item_path=get_web_reader_content_pages_directory(item_id=item_id, brand_id=brand_id),
        page_num=page_num)

    # check if image file in high res PNG format exists, if yes: set it as image extension, else set to default jpg
    return 'png' if os.path.isfile(file_path) else 'jpg'


def construct_preview_page_s3(item, page_num):
    s3_server = connect_gramedia_s3()

    # try get images jpg or png
    image_ext = ['.jpg', '.png']
    for iext in image_ext:

        bucket = app.config['BOTO3_GRAMEDIA_WEBREADER_BUCKET']
        bucket_path = '{}/{}/{}{}'.format(item.brand_id, item.id, page_num, iext)
        check_files = aws_check_files(s3_server, bucket, bucket_path)

        if check_files:
            url_link = aws_generate_link(s3_server, bucket, bucket_path)
            gen_url_link = url_link.replace('https://ebook-webreaders.s3.amazonaws.com/', '')
            return ('{}/{}'.format(app.config['NGINX_S3_WEB_READER'], gen_url_link))
