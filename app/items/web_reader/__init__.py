"""
Web-Reader Helpers
==================

Helper classes and functions that support the Scoop Web-Reader feature, along
with our poor-man's task queue.

.. note::

    The web-reader feature's functionality relies on an NGINX-specific feature,
    called X-Accel-Redirect.  This is similar to Apache's X-Sendfile.

    https://www.nginx.com/resources/wiki/start/topics/examples/x-accel/

"""

# Just proxying all the modules out through this namespace
from .task_queue import *
from .helpers import *
from .response import *
from .error_reporting import *
from app.items.choices import WebReaderTaskStatus
