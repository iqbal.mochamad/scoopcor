from __future__ import unicode_literals

import json
import os
from datetime import datetime

from dateutil.parser import parse
from six import string_types

from app.auth.models import UserInfo
from app.items.choices import WebReaderTaskStatus

from app.items.exceptions import (
    DuplicateTaskError, NonExistentTaskError)
from .helpers import get_web_reader_content_pages_directory

__all__ = ('WebReaderTaskJsonEncoder', 'WebReaderTaskJsonDecoder',
           'ProcessingTask', 'ProcessingTaskManager', )


class ProcessingTask(object):
    """ A queued job for generating files required by the Web Reader.
    """
    def __init__(self, item_id, **kwargs):
        """
        :param `int` item_id: Primary key for an `app.items.models.Item`
        """
        self.item_id = item_id
        self.processing_start_time = kwargs.pop('processing_start_time', None)
        self.processing_end_time = kwargs.pop('processing_end_time', None)
        self.status = kwargs.pop('status', WebReaderTaskStatus.pending)
        self.sub_task_status = kwargs.pop('sub_task_status', None)
        self.error_message = kwargs.pop('error_message', None)
        self.is_high_priority = kwargs.pop('is_high_priority', False)
        self.notify_users = kwargs.pop('users', [])

        if kwargs:
            raise ValueError('Got invalid kwarg to __init__: {}'.format(
                kwargs.keys()[0]))

    @classmethod
    def from_json(cls, json_object):
        """ Alternate constructor for building a Task from JSON data.

        :param `dict` or `str` json_object:
            A dictionary representing a JSON object.
        :return: New instance of ProcessingTask.
        :rtype: `ProcessingTask`
        """
        if isinstance(json_object, string_types):
            json_object = json.loads(json_object, cls=WebReaderTaskJsonDecoder)

        json_object = json_object.copy()
        inst = cls(item_id=json_object.pop('item_id'))

        for k, v in json_object.items():
            setattr(inst, k, v)

        return inst


class ProcessingTaskManager(object):
    """ Manager object that handles CRUD for ProcessingTasks.
    """
    key = 'WEB_READER_PROCESSING'
    key_for_config = 'WEB_READER_CONFIG'
    key_value_last_min_item = 'last_min_item_id'

    def __init__(self, redis):
        """
        :param `redis.StrictRedis` redis: Backend storage for this class.
        """
        self.redis = redis

    def get_by_item_id(self, item_id):
        """ Gets a single stored ProcessingTask with a matching item_id.

        If no ProcessingTask is found, None will be returned.

        :param `int` item_id: Database primary key for an item.
        :return: A ProcessingTask with a matching item_id
        :rtype: ProcessingTask
        """
        redis_data = self.redis.get("{0.key}:{1}".format(self, item_id))
        return ProcessingTask.from_json(redis_data) \
            if redis_data else None

    def get_or_create_by_item_id(self, item_id, brand_id, ignore_existing=False):

        task = self.get_by_item_id(item_id)

        if task is None:
            if os.path.exists(get_web_reader_content_pages_directory(item_id, brand_id)) and not ignore_existing:
                raise RuntimeError("Cannot create task -> Item files already exist!")
            task = ProcessingTask(item_id=item_id)
            self.enqueue(task)

        return task

    def get_all_tasks(self):
        """ Gets all stored ProcessingTasks, regardless of their status.

        :return: A list of all `ProcessingTasks` currently stored, regardless
            of their status.
        :rtype: list
        """
        tasks = []
        for key in self.redis.scan_iter("{0.key}:*".format(self)):
            redis_data = self.redis.get(key)
            tasks.append(ProcessingTask.from_json(redis_data))
        return tasks

    def get_tasks(self, high_priority_only=None, low_priority_only=None, status=None):

        tasks = self.get_all_tasks()

        if high_priority_only:
            tasks = [t for t in tasks if t.is_high_priority]
        elif low_priority_only:
            tasks = [t for t in tasks if not t.is_high_priority]

        if status:
            tasks = [t for t in tasks if t.status == status]

        return tasks

    def delete(self, task):
        """ Removes a task from the underlying storage engine.

        :param `ProcessingTask` task: The task instance to delete.
        :return: None
        :raises: `NonExistentTaskError` if task does not exist.
        """
        key = "{0.key}:{1}".format(self, task.item_id)
        if not self.redis.delete(key):
            raise NonExistentTaskError

    def update(self, task):
        """ Updates a task in the underlying storage engine.

        :param `ProcessingTask` task: The task instance to update.
        :return: The task that was updated
        :raises: `NonExistentTaskError` if task does not exist. Ala, you need
            to call `enqueue` instead.
        """
        key = "{0.key}:{1.item_id}".format(self, task)
        if not self.redis.get(key):
            raise NonExistentTaskError

        self.redis.set(key, json.dumps(task, cls=WebReaderTaskJsonEncoder ))

        return task

    def enqueue(self, task):
        """ Adds a new task instance to the queue.

        :param `ProcessingTask` task: The task to add to the queue.
        :return: The task instance that was added.
        :raises: `DuplicateTaskError` if the task is already queued.
        """
        key = "{0.key}:{1.item_id}".format(self, task)
        if self.redis.get(key):
            raise DuplicateTaskError

        self.redis.set(key, json.dumps(task, cls=WebReaderTaskJsonEncoder))

        return task

    def set_last_minimum_item_id(self, item_id):
        """

        :param item_id:
        :return:
        """
        key = "{key_for_config}:{key_value_last_min_item}".format(
            key_for_config=self.key_for_config,
            key_value_last_min_item=self.key_value_last_min_item)
        self.redis.set(key, item_id)

        return item_id

    def get_last_minimum_item_id(self):
        key = "{key_for_config}:{key_value_last_min_item}".format(
            key_for_config=self.key_for_config,
            key_value_last_min_item=self.key_value_last_min_item)
        last_processed_item_id = self.redis.get(key)
        return int(last_processed_item_id) if last_processed_item_id not in [None, 'None'] else None


class WebReaderTaskJsonEncoder(json.JSONEncoder):

    def default(self, o):
        """ Turns a task into a format that can be serialized to JSON.

        :param `ProcessingTask` o: The task to serialize.
        :rtype: dict
        :returns: A view of a processing task that can be turned into JSON.
        """

        o = vars(o).copy()
        for k, v in o.items():
            if isinstance(v, datetime):
                o[k] = v.isoformat()
            elif isinstance(v, WebReaderTaskStatus):
                o[k] = v.value
            elif k == 'notify_users':
                o[k] = [
                    {
                        'username': user_info.username,
                        'user_id': user_info.user_id
                    } for user_info in o[k]]
        return o


class WebReaderTaskJsonDecoder(json.JSONDecoder):

    def decode(self, *args, **kwargs):
        decoded = super(WebReaderTaskJsonDecoder, self).decode(*args, **kwargs)
        if decoded.get('processing_start_time'):
            decoded['processing_start_time'] = parse(decoded['processing_start_time'])

        if decoded.get('processing_end_time'):
            decoded['processing_end_time'] = parse(decoded['processing_end_time'])

        if decoded.get('status'):
            decoded['status'] = WebReaderTaskStatus(decoded['status'])

        if decoded.get('notify_users'):
            decoded['notify_users'] = [
                UserInfo(username=user_info['username'],
                         user_id=user_info['user_id'],
                         perm=[],
                         token=None) for user_info in decoded['notify_users']
            ]


        return decoded
