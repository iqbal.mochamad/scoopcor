"""
Error Reporting
===============

CRUD classes to store/retrieve information from REDIS about user-reported
errors in the web-reader files.
"""
from __future__ import unicode_literals

import json
from datetime import datetime

from dateutil.parser import parse
from dateutil.tz import tzutc
from six import string_types

from app.auth.models import UserInfo

__all__ = ('WebReaderErrorReport', 'WebReaderErrorManager', )


class WebReaderErrorReport(object):

    def __init__(self, item, user, message, report_id, pages=None):
        """
        :param `app.items.models.Item` item:
        :param `app.auth.models.UserInfo` user:
        :param `str|unicode` message:
        :param `str|unicode` report_id: Some kind of unique identifier for this instance.
        :param `list` of `int` pages:
        """
        if not pages:
            pages = []
        self.item = item
        self.user = user
        self.message = message
        self.pages = pages
        self.report_id = report_id
        self.report_time = datetime.utcnow().replace(tzinfo=tzutc())

    @classmethod
    def from_json(cls, json_object):

        if isinstance(json_object, string_types):
            json_object = json.loads(json_object, cls=ErrorReportJsonDecoder)

            json_object = json_object.copy()
            report_time = json_object.pop('report_time')

            inst = cls(**json_object)
            inst.report_time = report_time

            return inst


class WebReaderErrorManager(object):

    REDIS_KEY_PREFIX = "WEB_READER_ERROR_REPORT"

    def __init__(self, redis):
        """
        :param `redis.StrictRedis` redis: Underlying data storage instance.
        """
        self.redis = redis

    def insert(self, report):
        """
        :param `WebReaderErrorReport` report:
        """
        key = "{prefix}:{id}".format(prefix=self.REDIS_KEY_PREFIX,
                                     id=report.report_id)

        self.redis.set(key, json.dumps(report, cls=ErrorReportJsonEncoder))

        return key

    def get_all(self):

        error_reports = [WebReaderErrorReport.from_json(self.redis.get(k))
                         for k in self.redis.scan_iter("{prefix}:*".format(prefix=self.REDIS_KEY_PREFIX))]

        return error_reports


class ErrorReportJsonDecoder(json.JSONDecoder):

    def decode(self, *args, **kwargs):
        decoded = super(ErrorReportJsonDecoder, self).decode(*args, **kwargs)

        decoded['report_time'] = parse(decoded['report_time'])
        decoded['user'] = UserInfo(user_id=decoded['user']['user_id'],
                                   username=decoded['user']['username'],
                                   token='',
                                   perm=[])

        return decoded


class ErrorReportJsonEncoder(json.JSONEncoder):

    def default(self, o):
        o = vars(o).copy()
        o['report_time'] = o['report_time'].isoformat()
        o['user'] = {
            'username': o['user'].username,
            'user_id': o['user'].user_id,
        }
        o['item'] = {
            'id': o['item'].id
        }
        return o
