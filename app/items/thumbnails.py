"""
Item Page Thumbnail Helpers
===========================

Helper classes and functions that support returning page thumbnail images.

.. note::

    These **are not** the same as the Item model's thumbnail normal/highres
    properties.  Those are only the cover image thumbnails, whereas this module
    deals with 'page' thumbnails.

.. note::

    We're not going out of our way to generate thumbnail data from this module.
    It is intended that this will only be fetched by the web-reader, meaning
    that it's data should be constructed as part of the web reader anyway.
"""
from __future__ import unicode_literals, absolute_import

import httplib

from flask import url_for, make_response, current_app
from six import text_type

from flask_appsfoundry.exceptions import NotFound
from app.helpers import request_is_expectation_check
from app.utils.http import LinkHeaderField, LinkHeaderRel
from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files, aws_generate_link
from app import app


def assert_item_type_valid(item):
    """ Makes sure that a given item supports page thumbnails (only PDFs).

    If the item does not have a supported content type, an HTTP 404 will be
    raised.

    :param `app.items.models.Item` item:
        The item model which we are validating.
    :return: None if successful.
    :raises: `app.exceptions.NotFound` if the content type is invalid.
    """
    if item.content_type != 'pdf':
        raise NotFound(
            developer_message="{item} thumbnails is {item.content_type}.  "
                              "Only PDFs are currently "
                              "supported".format(item=item),
            user_message="Thumbnail not found")


def construct_thumbnail_response(item, page_num):
    if request_is_expectation_check():
        response = make_response("", httplib.CONTINUE)
    else:
        response = make_response("", httplib.OK, {
            'Content-Type': "image/jpeg",
            #'X-Accel-Redirect': construct_nginx_internal_url(item, page_num),
            'X-Accel-Redirect': construct_thumbnails_page_s3(item, page_num),
            'Link': ",".join([text_type(link) for link
                              in construct_links(item, page_num)])
        })
    return response


def construct_nginx_internal_url(item, page_num):
    """ Builds an required NGINX internal url for a given web reader page.

    :param `app.items.models.Item` item: The Item that owns the content page.
    :param `int` page_num: The page number being displayed.
    :return: A stringified URL representation.
    :rtype: `str`
    """
    return ('{secure_media_base}/'
            'item_content_files/'
            'thumbnails/'
            '{item.brand_id}/'
            '{item.id}/'
            '{page_num}.jpg').format(
                secure_media_base=current_app.config['NGINX_SECURE_MEDIA_BASE'],
                item=item,
                page_num=page_num)


def construct_thumbnails_page_s3(item, page_num):
    s3_server = connect_gramedia_s3()
    image_ext = ['.jpg', '.png']
    for iext in image_ext:

        bucket = app.config['BOTO3_GRAMEDIA_THUMBNAILS_BUCKET']
        bucket_path = '{}/{}/{}{}'.format(item.brand_id, item.id, page_num, iext)
        check_files = aws_check_files(s3_server, bucket, bucket_path)

        if check_files:
            url_link = aws_generate_link(s3_server, bucket, bucket_path)
            gen_url_link = url_link.replace('https://ebook-thumbnails.s3.amazonaws.com/', '')
            return ('{}/{}'.format(app.config['NGINX_S3_THUMBNAILS'], gen_url_link))

def construct_links(item, page_num):

    links = []

    can_nav_prev = page_num > 1
    if can_nav_prev:
        links.append(
            LinkHeaderField(
                url=url_for('items.page_thumbnail', db_id=item.id, page_num=(page_num-1)),
                rel=LinkHeaderRel.prev))
        links.append(
            LinkHeaderField(
                url=url_for('items.page_thumbnail', db_id=item.id, page_num=1),
                rel=LinkHeaderRel.first))

    can_nav_next = page_num < item.page_count
    if can_nav_next:
        links.append(
            LinkHeaderField(
                url=url_for('items.page_thumbnail', db_id=item.id, page_num=(page_num+1)),
                rel=LinkHeaderRel.next))
        links.append(
            LinkHeaderField(
                url=url_for('items.page_thumbnail', db_id=item.id, page_num=item.page_count),
                rel=LinkHeaderRel.last))

    return links
