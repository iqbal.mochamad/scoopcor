import os
from app import app

from flask_appsfoundry.serializers import SerializerBase, fields

from app.utils.serializers import ISO8601Date
from .choices import FILE_TYPES, READING_DIRECTIONS, ITEM_TYPES, STATUS_TYPES


class ItemSerializer(SerializerBase):
    id = fields.Integer
    name = fields.String
    slug = fields.String
    description = fields.String
    edition_code = fields.String
    issue_number = fields.String
    content_type = fields.EnumFieldFormatter(FILE_TYPES)
    item_status = fields.EnumFieldFormatter(STATUS_TYPES)
    item_type_id = fields.EnumFieldFormatter(enum_dict=ITEM_TYPES,
                                             attribute='item_type')

    previews = fields.List(fields.String)
    meta = fields.String
    reading_direction = fields.EnumFieldFormatter(READING_DIRECTIONS)
    release_date = fields.DateTime('iso8601')

    sort_priority = fields.Integer

    is_featured = fields.Boolean
    is_extra = fields.Boolean
    is_active = fields.Boolean
    is_latest = fields.Boolean

    extra_items = fields.List(fields.String)
    subs_weight = fields.Integer
    parental_control_id = fields.Integer(attribute='parentalcontrol_id')
    filenames = fields.List(fields.String)
    brand_id = fields.Integer

    thumb_image_normal = fields.String
    thumb_image_highres = fields.String
    image_normal = fields.String
    image_highres = fields.String

    # Replace this with a custom type if it's more efficient.
    media_base_url = fields.String("https://images.apps-foundry.com/magazine_static/")


class PopularItemSerializer(SerializerBase):
    id = fields.Integer
    vendor = fields.Nested({'id': fields.Integer, 'name': fields.String}, attribute='item.brand.vendor')
    name = fields.String(attribute='item.name')
    brand = fields.Nested({'id': fields.Integer, 'name': fields.String, 'slug': fields.String}, attribute='item.brand')
    item_type_id = fields.EnumFieldFormatter(enum_dict=ITEM_TYPES,
                                             attribute='item.item_type')
    media_base_url = fields.String(default=os.path.join(app.config['AWS_PUBLIC'], app.config['BOTO3_GRAMEDIA_COVERS_BUCKET']))
    categories = fields.List(fields.Nested({'id': fields.Integer, 'name': fields.String}), attribute='item.categories')

    thumb_image_normal = fields.String(attribute='item.ser_thumb_normal')
    thumb_image_highres = fields.String(attribute='item.ser_thumb_highres')
    issue_number = fields.String(attribute='item.issue_number')
    content_type = fields.EnumFieldFormatter(FILE_TYPES, attribute='item.content_type')
    display_offers = fields.Raw(attribute='display_offers')
    is_latest = fields.Boolean()
    item_thumb_highres = fields.String(attribute='item.ser_thumb_highres')
    is_buffet = fields.Boolean()
    parental_control = fields.Nested({'id': fields.Integer, 'name': fields.String}, attribute='item.parentalcontrol')
    slug = fields.String(attribute='item.slug')
    authors = fields.Raw()


class ItemUploadStatusSerializer(SerializerBase):
    file_name = fields.String()
    file_size = fields.Integer()
    pic_id = fields.Integer()
    pic_name = fields.String()
    role_id = fields.Integer()
    processing_start_time = fields.DateTime(
        'iso8601', attribute='start_process_time')
    processing_complete_time = fields.DateTime(
        'iso8601', attribute='end_process_time')
    status = fields.Integer()
    error = fields.String()


class AuthorSerializer(SerializerBase):
    id = fields.Integer()
    name = fields.String()
    sort_priority = fields.Integer()
    is_active = fields.Boolean()
    slug = fields.String()

    first_name = fields.String()
    last_name = fields.String()
    title = fields.String()
    academic_title = fields.String()
    birthdate = ISO8601Date()
    meta = fields.String()
    modified = fields.DateTime('iso8601')
    profile_pic_url = fields.String()
