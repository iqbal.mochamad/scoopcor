from __future__ import absolute_import, unicode_literals

import hashlib
import httplib
import json
import os
import time
from datetime import datetime, timedelta

import paramiko
import requests
from dateutil.relativedelta import relativedelta
from enum import Enum, IntEnum
from flask import Response, current_app, g, request, make_response
from flask_appsfoundry.exceptions import NotFound, Forbidden, Unauthorized
from flask_appsfoundry.parsers import specialized
from six import text_type
from sqlalchemy import or_
from sqlalchemy.sql import desc
from werkzeug.exceptions import Conflict

from app import db, app
from app.analytics.helpers import save_user_search_activity
from app.constants import MIME_TYPE_V3_JSON
from app.eperpus.libraries import SharedLibraryRestrictedVendor
from app.helpers import (err_response, date_validator, general_message, bad_request_message,
                         internal_server_message, regenerate_slug, generate_slug)
from app.items import models
from app.items.choices import READING_DIRECTIONS, FILE_TYPES, STATUS_TYPES, ItemUploadProcessStatus, RESTRICTED, ALLOWED
from app.items.previews import preview_s3_covers
from app.logs.models import ItemProcessing
from app.master.choices import PlatformType
from app.master.models import Category, Brand, Vendor
from app.offers.choices.offer_type import SINGLE
from app.offers.models import Offer
from app.parental_controls.choices import ParentalControlType, StudentSaveControlType
from app.parental_controls.models import ParentalControl
from app.services.solr import SolrQueryBuilder
from app.uploads.aws.helpers import aws_shared_link, connect_gramedia_s3, aws_check_files
from app.users.buffets import UserBuffet
from app.users.organizations import Organization
from app.users.users import User, is_apps_foundry_users
from app.utils.datetimes import get_utc_nieve, datetime_to_isoformat
from app.utils.geo_info import get_geo_info
from app.utils.shims import item_types
from .choices.item_type import BOOK, MAGAZINE, NEWSPAPER, ITEM_TYPES
from .choices.status import STATUS_UPLOADED, STATUS_NEW
from .encrypt import EncryptData
from .models import (Item, ItemFile, UserItem, UserSubscription, UserSubscriptionItem, DistributionCountryGroup, Author)


def get_filter_parental_control(param_parental_control_id=None):
    if param_parental_control_id:
        return param_parental_control_id
    elif getattr(g, 'current_user', None) and \
        MIME_TYPE_V3_JSON in request.headers.get('accept', "") and \
        not g.current_user.allow_age_restricted_content:
        # if it's from the new front end APPS (it didn't send parental_control_id in query param anymore).
        # use User Setting: if current_user.allow_age_restricted_content = False, then return all ages item only
        return ParentalControlType.parental_level_1.value
    else:
        return None


def get_dist_country_filter():
    """

    :return `sqlalchemy.sql.elements.BooleanClauseList`:
    """
    dist_country_filter = None
    user_agent = request.headers.get('user-agent', '').lower()

    # current_client_id = g.current_client.id if getattr(g, 'current_client', None) else None
    # platform_id = g.current_client.platform_id if getattr(g, 'current_client', None) else None

    # Only for IOS/ANDROID/WEB
    if 'web' in user_agent or 'android' in user_agent or 'ios' in user_agent:
        # filter distribution group by country
        geo_info = get_geo_info()
        if geo_info.country_code:
            # user from selected country, crosscheck with distribution country group!
            # like for harper collins, if user in AUSTRALIA, harper collin's items should not selected
            restricted = DistributionCountryGroup.query.filter(
                DistributionCountryGroup.group_type == RESTRICTED,
                DistributionCountryGroup.is_active == True,
                DistributionCountryGroup.countries.any(geo_info.country_code),
            ).all()
            if restricted:
                restricted_dist_country_group_id = [r.id for r in restricted]
                dist_country_filter = or_(
                    Item.item_distribution_country_group_id.notin_(restricted_dist_country_group_id),
                    Item.item_distribution_country_group_id == None,
                    Item.item_distribution_country_group_id == 0,
                )

            allowed = DistributionCountryGroup.query.filter(
                DistributionCountryGroup.group_type == ALLOWED,
                DistributionCountryGroup.is_active == True,
                DistributionCountryGroup.countries.any(geo_info.country_code),
            ).all()
            if allowed:
                allowed_dist_country_group_id = [r.id for r in allowed]
                allowed_dist_country_group_id.append(0)
                dist_country_filter = or_(
                    Item.item_distribution_country_group_id.in_(allowed_dist_country_group_id),
                    Item.item_distribution_country_group_id == None, )
    return dist_country_filter


def file_type_shim(numeric_file_type):
    """ Takes the old-style numeric file types and returns something reasonable.

    >>> file_type_shim(1)
    u'pdf'
    >>> file_type_shim(2)
    u'epub'

    :param `int` numeric_file_type:
    :return: 'pdf' or 'epub' edepending on the input numeric type.
    :rtype: six.string_types
    """
    return {
        1: 'pdf',
        2: 'epub',
    }[numeric_file_type]


# TODO: lookup everything that calls this.  Better to do it on a single
# session object so we don't get partial commits/failures.
# TODO: ask about this in the db.  can an item have multiple item processing
# records?
def record_item_processing(item_id, brand_id):
    """ Records that a newly-created currently has a status of NEW (1).

    :param `int` item_id:  The database ID of the Item.
    :param `int` brand_id: The database ID of the Item's brand.
    :return: `None`
    """
    session = ItemProcessing.query.session

    try:
        # NOTE: this still uses the old-style integer types for it's status
        # noinspection PyArgumentList
        item_processing = ItemProcessing(
            item_id=item_id,
            brand_id=brand_id,
            status=ItemUploadProcessStatus.new.value,
            is_notified=False,
            process_starts=datetime.now(),
            process_ends=datetime.now())

        session.add(item_processing)
        session.commit()

    except Exception:
        session.rollback()
        raise


class ItemConstruct():

    def __init__(self, args=None, method=None, old_data=None):
        self.args = args
        self.method = method

        # for PUT use
        self.old_item = old_data

        # self.old_item used for PUT method ONLY
        self.old_item = old_data
        self.release_date = args.get('release_date', '')
        self.release_schedule = args.get('release_schedule', '')

        self.author = args.get('authors', None)
        self.categories = args.get('categories', None)
        self.language = args.get('language', None)
        self.countries = args.get('countries', None)
        self.brand = args.get('brand_id', None)

        self.item_type = args.get('item_type_id', 0)
        self.parentalcontrol = args.get('parental_control_id', 0)
        self.is_active = args.get('is_active', False)
        self.item_status = args.get('item_status', 0)

        self.distribution_country = args.get('item_distribution_country_group_id', 0)

        self.item_master = ['name', 'edition_code', 'content_type',
                            'sort_priority', 'item_status', 'release_date',
                            'is_featured', 'is_extra', 'is_active',
                            'parental_control_id', 'item_type_id', 'brand_id',
                            'display_until', 'author', 'authors', 'categories',
                            'language', 'countries', 'id', 'item_distribution_country_group_id',
                            'release_schedule']

        self.dict_master = {}
        self.master_item = 0
        self.sub_master_item = 0

        self.item_digital = ['issue_number', 'revision_number', 'extra_items',
                             'slug', 'thumb_image_normal',
                             'thumb_image_highres', 'subs_weight']
        self.dict_item_digital = {}

        self.subs_magazine_book = ['description', 'meta', 'image_normal',
                                   'image_highres', 'gtin13', 'gtin14',
                                   'gtin8', 'filenames', 'previews',
                                   'reading_direction']
        self.dict_subs_magazine_book = {}

        self.subs_newspaper = ['description', 'meta', 'image_normal',
                               'image_highres', 'filenames', 'previews', 'reading_direction']
        self.dict_subs_newspaper = {}

        self.user_subs = []

    def check_brands(self):
        try:
            brand = Brand.query.filter_by(id=self.brand).first()
            if not brand:
                return 'FAILED', bad_request_message(modul="item 0", method=self.method,
                                                     param="brand_id", not_found=self.brand, req=self.args)
            self.brand = brand.id
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="item 0", method=self.method,
                                                     error=e, req=str(self.args))

    def check_parental_control(self):
        try:
            parentalcontrol = ParentalControl.query.filter_by(id=self.parentalcontrol).first()
            if not parentalcontrol:
                return 'FAILED', bad_request_message(modul="item", method=self.method,
                                                     param="itemtype_id", not_found=self.parentalcontrol, req=self.args)
            self.parentalcontrol = parentalcontrol.id
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="item 1", method=self.method,
                                                     error=e, req=str(self.args))

    def check_release_date(self):
        valid_from = date_validator(self.release_date)
        if valid_from.get('invalid', False):
            return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                                          error_code=httplib.BAD_REQUEST,
                                          developer_message="Invalid release date format (2014-01-01T00:00:00)",
                                          user_message="Release date invalid")

        return 'OK', 'OK'

    def check_release_schedule(self):
        valid_from = date_validator(self.release_schedule)
        if valid_from.get('invalid', False):
            return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                                          error_code=httplib.BAD_REQUEST,
                                          developer_message="Invalid release schedule date format (2014-01-01T00:00:00)",
                                          user_message="release schedule date invalid")

        return 'OK', 'OK'

    def check_author(self):
        try:
            author_data = []
            for i in self.author:
                author = Author.query.filter_by(id=i).first()
                if not author:
                    return 'FAILED', bad_request_message(modul="item 3", method=self.method,
                                                         param="author", not_found=i, req=self.args)
                author_data.append(author)
            self.author = author_data
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="item 3", method=self.method,
                                                     error=e, req=str(self.args))

    def check_categories(self):
        try:
            categories_data = []
            for i in self.categories:
                categories = Category.query.filter_by(id=i).first()
                if not categories:
                    return 'FAILED', bad_request_message(modul="item 4", method=self.method,
                                                         param="categories", not_found=i, req=self.args)
                categories_data.append(categories)
            self.categories = categories_data
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="item 4", method=self.method,
                                                     error=e, req=str(self.args))

    def check_restricted_countries(self):
        try:
            distribution = DistributionCountryGroup.query.filter_by(id=self.distribution_country).first()

            if not distribution:
                return 'FAILED', bad_request_message(modul="item 6b", method=self.method,
                                                     param="item_distribution_country_group_id",
                                                     not_found=self.distribution_country, req=self.args)

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="item 6b", method=self.method,
                                                     error=e, req=str(self.args))

    def construct_parameters(self):
        try:
            for item in self.args:
                if self.args[item] == None:
                    pass
                else:
                    if item in self.item_master:
                        self.dict_master[item] = self.args[item]

                    if item in self.item_digital:
                        self.dict_item_digital[item] = self.args[item]

                    if item in self.subs_magazine_book:
                        self.dict_subs_magazine_book[item] = self.args[item]

                    if item in self.subs_newspaper:
                        self.dict_subs_newspaper[item] = self.args[item]
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="item 7", method=self.method,
                                                     error=e, req=str(self.args))

    # ---------------- POST -------------------------
    def post_items(self):
        """
            POST TO ITEM TABLE
        """
        try:
            if self.author:
                del self.dict_master['authors']

            if self.categories:
                del self.dict_master['categories']

            if self.language:
                del self.dict_master['language']

            if self.countries:
                del self.dict_master['countries']

            # Overide status, is_active
            # self.dict_master['is_active'] = False

            # OVERIDE parental_control, item_type_id #JIRA SCOOP-2115
            parental = self.dict_master.get('parental_control_id', None)
            itype = self.dict_master.get('item_type_id', None)

            if parental:
                self.dict_master['parentalcontrol_id'] = parental
                del self.dict_master['parental_control_id']

            if itype:
                self.dict_master['itemtype_id'] = itype
                del self.dict_master['item_type_id']

            # noinspection PyArgumentList
            items = Item(**self.dict_master)

            if self.author:
                for i in self.author:
                    items.authors.append(i)

            if self.categories:
                for x in self.categories:
                    items.categories.append(x)

            if self.language:
                for y in self.language:
                    items.language.append(y)

            if self.countries:
                for z in self.countries:
                    items.countries.append(z)

            sv = items.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="item 8", method=self.method,
                                                         error=sv.get('message', ''), req=str(self.args))
            else:
                self.master_item = items
                return 'OK', Response(json.dumps(items.values()),
                                      status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="item 8", method=self.method,
                                                     error=e, req=str(self.args))

    # ---------------- PUT -------------------------
    def put_items(self):
        try:
            if self.author:
                del self.dict_master['authors']

            if self.categories:
                del self.dict_master['categories']

            if self.language:
                del self.dict_master['language']

            if self.countries:
                del self.dict_master['countries']

            # -============== update all table relations
            if self.author:
                for old_author in self.old_item.authors:
                    self.old_item.authors.remove(old_author)

                for new_author in self.author:
                    self.old_item.authors.append(new_author)

            if self.categories:
                for old_categories in self.old_item.categories:
                    self.old_item.categories.remove(old_categories)

                for new_categories in self.categories:
                    self.old_item.categories.append(new_categories)

            if self.language:
                for old_language in self.old_item.language:
                    self.old_item.language.remove(old_language)

                for new_language in self.language:
                    self.old_item.language.append(new_language)

            if self.countries:
                for old_countries in self.old_item.countries:
                    self.old_item.countries.remove(old_countries)

                for new_countries in self.countries:
                    self.old_item.countries.append(new_countries)

            # OVERIDE parental_control, item_type_id #JIRA SCOOP-2115
            parental = self.dict_master.get('parental_control_id', None)
            itype = self.dict_master.get('item_type_id', None)

            if parental:
                self.dict_master['parentalcontrol_id'] = parental
                del self.dict_master['parental_control_id']

            if itype:
                self.dict_master['itemtype_id'] = itype
                del self.dict_master['item_type_id']

            for k, v in self.dict_master.iteritems():
                setattr(self.old_item, k, v)

            sv = self.old_item.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="item 12", method=self.method,
                                                         error=sv.get('message', ''), req=str(self.args))

            self.master_item = self.old_item
            return "OK", "OK"

        except Exception as e:
            return 'FAILED', internal_server_message(modul="item 12", method=self.method,
                                                     error=e, req=str(self.args))

    def record_item_processing(self):
        try:
            # noinspection PyArgumentList
            item_processing = ItemProcessing(
                item_id=self.master_item.id,
                brand_id=self.brand,
                status=STATUS_NEW,
                is_notified=False,
                process_starts=datetime.now(),
                process_ends=datetime.now()
            )
            item_processing.save()

        except Exception, e:
            internal_server_message(modul="item 18", method=self.method,
                                    error=e, req=str(self.args))
            pass

    def success_response(self):
        items = Item.query.filter_by(id=self.master_item.id).first()
        if items:
            return Response(json.dumps(items.values()),
                            status=httplib.CREATED, mimetype='application/json')
        else:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

    def construct_items(self):
        brand_chck = self.check_brands()
        if brand_chck[0] != 'OK':
            return brand_chck[1]

        item_type = self.check_parental_control()
        if item_type[0] != 'OK':
            return item_type[1]

        parentalcontrol = self.check_item_types()
        if parentalcontrol[0] != 'OK':
            return parentalcontrol[1]

        release_date = self.check_release_date()
        if release_date[0] != 'OK':
            return release_date[1]

        if self.release_schedule:
            scheduled_date = self.check_release_schedule()
            if scheduled_date[0] != 'OK':
                return scheduled_date[1]

        if self.author:
            check_author = self.check_author()
            if check_author[0] != 'OK':
                return check_author[1]

        if self.categories:
            check_categories = self.check_categories()
            if check_categories[0] != 'OK':
                return check_categories[1]

        if self.language:
            check_language = self.check_language()
            if check_language[0] != 'OK':
                return check_language[1]

        if self.countries:
            check_countries = self.check_countries()
            if check_countries[0] != 'OK':
                return check_countries[1]

        if self.distribution_country:
            check_restricted = self.check_restricted_countries()
            if check_restricted[0] != 'OK':
                return check_restricted[1]

        # construct parameter json and separate
        construct_param = self.construct_parameters()
        if construct_param[0] != 'OK':
            return construct_param[1]

        # create ITEM
        if self.method == "POST":
            items = self.post_items()

            # create sub digital (digital book, magazine, newspaper)
            if self.item_type == 1:  # digital magazine
                digital = self.post_create_magazine()

            if self.item_type == 2:  # digital magazine
                digital = self.post_create_book()

            if self.item_type == 3:  # digital magazine
                digital = self.post_create_newspaper()

            if digital[0] != 'OK':
                return digital[1]

        if self.method == 'PUT':

            if item_types.enum_to_id(self.old_item.item_type) != self.item_type:
                return internal_server_message(
                    modul="item",
                    method=self.method,
                    error="ITEM TYPES CHANGES, PLEASE CONSULT WITH YOUR ADMIN",
                    req=str(self.args))
            else:
                items = self.put_items()

                # create sub digital (digital book, magazine, newspaper)
                if self.item_type == MAGAZINE:
                    digital = self.put_create_magazine()

                if self.item_type == BOOK:
                    digital = self.put_create_book()

                if self.item_type == NEWSPAPER:
                    digital = self.put_create_newspaper()

                if digital[0] != 'OK':
                    return digital[1]

        if items[0] != 'OK':
            return items[1]

        # THIS COMMENT OUT HANDLE FROM UPLOADERS
        if self.item_status not in [STATUS_NEW, STATUS_UPLOADED]:
            self.record_item_processing()

        success = self.success_response()
        return success


class ItemFileConstruct():

    def __init__(self, args=None, method=None):
        self.args = args
        self.method = method

        self.item_files = args.get('item_files', None)

    def check_items(self):
        try:
            for xitem in self.item_files:
                item_id = xitem.get('item_id', None)
                if item_id:
                    item = Item.query.filter_by(id=item_id).first()
                    if not item:
                        return 'FAILED', bad_request_message(modul="item file",
                                                             method=self.method, param="item_id",
                                                             not_found=item_id, req=self.args)
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="item file",
                                                     method=self.method, error=str(e), req=str(self.args))

    def post_item_files(self):
        try:
            data_files = []
            for xitem in self.item_files:
                temp = {}

                item_id = xitem.get('item_id', None)
                file_name = xitem.get('file_name', None)

                # check if item oledy exist or not
                item = ItemFile.query.filter_by(item_id=item_id,
                                                file_name=file_name).first()
                if item:
                    temp = item.values()
                    temp['status'] = httplib.OK
                else:
                    kwargs = {}
                    for item in xitem:
                        if xitem[item] == None:
                            pass
                        else:
                            kwargs[item] = xitem[item]

                    data = ItemFile(**kwargs)
                    sv = data.save()
                    if sv.get('invalid', False):
                        temp['status'] = httplib.BAD_REQUEST
                        temp['error_code'] = 400101
                        temp['developer_message'] = sv.get('message', '')
                        temp['user_message'] = "Failed"
                    else:
                        temp = data.values()
                        temp['status'] = httplib.CREATED
                data_files.append(temp)

            return 'OK', Response(json.dumps({'item_files': data_files},
                                             ensure_ascii=False), status=httplib.MULTI_STATUS,
                                  mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="item file",
                                                     method=self.method, error=str(e), req=str(self.args))

    def construct(self):
        item_check = self.check_items()
        if item_check[0] != 'OK':
            return item_check[1]

        if self.method == 'POST':
            itemfiles = self.post_item_files()
            return itemfiles[1]


class OwnedItemConstruct():

    def __init__(self, args=None, method=None):
        self.args = args
        self.method = method
        self.owned_items = args.get('owned_items', None)

    def post_owned_items(self):

        owned_data = []
        for data in self.owned_items:
            response_temp = {}

            item_id = data.get('item_id', None)
            user_id = data.get('user_id', None)
            orderline_id = data.get('orderline_id', 0)
            exist_item = Item.query.filter_by(id=item_id).first()
            if not exist_item:
                return err_response(status=httplib.BAD_REQUEST, error_code=httplib.BAD_REQUEST,
                    developer_message="{} Not Found".format(item_id),
                    user_message="{} Not Found".format(item_id))

            if exist_item:
                owned_item = UserItem.query.filter(UserItem.user_buffet_id == None).filter_by(
                    item_id=item_id, user_id=user_id).first()
                if owned_item:
                    response_temp.update(owned_item.values())
                    response_temp['status'] = httplib.OK
                else:
                    new_useritem = UserItem(
                        user_id = user_id,
                        item_id = exist_item.id,
                        edition_code = exist_item.edition_code,
                        orderline_id = orderline_id,
                        is_active = True,
                        is_restore = False,
                        itemtype_id = item_types.enum_to_id(exist_item.item_type),
                        vendor_id = exist_item.brand.vendor_id,
                        brand_id = exist_item.brand.id)
                    db.session.add(new_useritem)
                    db.session.commit()
                    response_temp.update(new_useritem.values())
                    response_temp['status'] = httplib.CREATED

            owned_data.append(response_temp)
        return Response(json.dumps({'results': owned_data}, ensure_ascii=False), status=httplib.MULTI_STATUS, mimetype='application/json')

class OwnedItemSubsConstruct():

    def __init__(self, args=None, method=None):
        self.args = args
        self.method = method

        self.owned_subs = args.get('owned_subscriptions', None)

        self.user_subs = []

    def item_required(self):
        try:
            for xitem in self.owned_subs:
                user_id = xitem.get('user_id', 0)
                item_id = xitem.get('item_id', 0)
                valid_from = xitem.get('valid_from', 0)
                valid_to = xitem.get('valid_to', 0)
                quantity = xitem.get('quantity', 0)
                quantity_unit = xitem.get('quantity_unit', 0)
                brand_id = xitem.get('brand_id', 0)

                if user_id == 0 or user_id == '':
                    return 'FAILED', bad_request_message(modul="owned subs 1",
                                                         method=self.method, param="user_id",
                                                         not_found="user_id NULL", req=self.args)

                # BENNY REQUEST : ( SUPPORT ITEM RENEWAL FROM OLD SCOOPCORE)
                # if item_id == 0 or item_id == '':
                #    return 'FAILED', bad_request_message(modul="owned subs 1",
                #        method=self.method, param="item_id",
                #        not_found="item_id NULL", req=self.args)

                if valid_from == 0 or valid_from == '':
                    return 'FAILED', bad_request_message(modul="owned subs 1",
                                                         method=self.method, param="valid_from",
                                                         not_found="valid_from NULL", req=self.args)

                if quantity == 0 or quantity == '':
                    return 'FAILED', bad_request_message(modul="owned subs 1",
                                                         method=self.method, param="quantity",
                                                         not_found="quantity NULL", req=self.args)

                if quantity_unit == 0 or quantity_unit == '':
                    return 'FAILED', bad_request_message(modul="owned subs 1",
                                                         method=self.method, param="quantity_unit",
                                                         not_found="quantity_unit NULL", req=self.args)

                if brand_id == 0 or brand_id == '':
                    return 'FAILED', bad_request_message(modul="owned subs 1",
                                                         method=self.method, param="brand_id",
                                                         not_found="brand_id NULL", req=self.args)

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="owned subs 1",
                                                     method=self.method, error=str(e), req=str(self.args))

    def check_date(self):
        """
            CHECKING DATE FORMAT YYYY-MM-DD
            INVALID IF VALID_TO < VALID_FROM
        """
        try:
            for idata in self.owned_subs:
                valid_from = idata.get('valid_from', None)
                valid_to = idata.get('valid_to', None)

                valid_from = date_validator(valid_from, 'custom')
                if valid_from.get('invalid', False):
                    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                                                  error_code=httplib.BAD_REQUEST,
                                                  developer_message="Invalid valid from, format YYYY-MM-DD HH:MM:SS",
                                                  user_message="valid from invalid")

                valid_to = date_validator(valid_to, 'custom')
                if valid_to.get('invalid', False):
                    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                                                  error_code=httplib.BAD_REQUEST,
                                                  developer_message="Invalid valid from, format YYYY-MM-DD HH:MM:SS",
                                                  user_message="valid to invalid")

                if valid_to <= valid_from:
                    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                                                  error_code=httplib.BAD_REQUEST,
                                                  developer_message="Invalid valid to must greater than valid from",
                                                  user_message="valid to invalid")
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="owned subs 2",
                                                     method=self.method, error=e, req=str(self.args))

    def check_brands(self):
        try:
            for idata in self.owned_subs:
                brand = idata.get('brand_id', None)
                brand_data = Brand.query.filter_by(id=brand).first()
                if not brand_data:
                    return 'FAILED', bad_request_message(modul="owned subs 3",
                                                         method=self.method, param="brand_id",
                                                         not_found=brand, req=self.args)
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="owned subs 3",
                                                     method=self.method, error=e, req=str(self.args))

    def item_brands(self):
        try:
            for idata in self.owned_subs:
                brand_id = idata.get('brand_id', None)
                item_id = idata.get('item_id', None)

                if item_id:
                    item = Item.query.filter_by(id=item_id).first()
                    if not item:
                        return 'FAILED', bad_request_message(modul="owned subs 4",
                                                             method=self.method, param="item_id",
                                                             not_found=item_id, req=self.args)
                    else:
                        if item.brand_id != brand_id:
                            return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                                                          error_code=httplib.BAD_REQUEST,
                                                          developer_message="Item : %s brand invalid: %s" % (
                                                              item_id, brand_id),
                                                          user_message="Item brand invalid")

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="owned subs 4",
                                                     method=self.method, error=e, req=str(self.args))

    def valid_to_calc(self, valid_from=None, quantity_unit=None, quantity=None):
        form_valid = datetime.strptime(valid_from, '%Y-%m-%d %H:%M:%S')
        valid_to = form_valid

        if quantity_unit == 2:
            valid_to = form_valid + timedelta(days=quantity)

        # weeks
        if quantity_unit == 3:
            valid_to = form_valid + timedelta(weeks=quantity)

        # month
        if quantity_unit == 4:
            valid_to = form_valid + relativedelta(months=quantity)

        return valid_to

    def get_items(self, item_id=None):
        item_data = None
        item = Item.query.filter_by(id=item_id).first()
        if item:
            item_data = item
        return item

    def save_user_items(self, subs=None, user_id=None, item_id=None):

        # TO DO : deliver items
        item_user = UserItem.query.filter_by(
            user_id=user_id, item_id=item_id).first()

        if item_user:
            pass
        else:
            item = self.get_items(item_id)
            if item:

                try:
                    vendors = item.get_vendor()
                    vendor_id = vendors.get('id', 0)
                except:
                    vendor_id = 0

                # noinspection PyArgumentList
                userit = UserItem(
                    user_id=user_id,
                    item_id=item_id,
                    edition_code=item.edition_code,
                    is_active=True,
                    brand_id=item.brand_id,
                    itemtype_id=item_types.enum_to_id(item.item_type),
                    vendor_id=vendor_id
                )
                sv = userit.save()

                if sv.get('invalid', False):
                    pass
                else:
                    pass
                    # noinspection PyArgumentList
                    # mo = UserSubscriptionItem(
                    #     usersubscription_id=subs.id,
                    #     useritem_id=userit.id,
                    #     item_id=item_id,
                    #     user_id=user_id,
                    #     note='ADD FROM /v1/owned_subscriptions'
                    # )
                    # mo.save()

    def post_owned_subs(self):
        try:
            data_files = []
            for xitem in self.owned_subs:
                temp = {}

                item_id = xitem.get('item_id', None)
                user_id = xitem.get('user_id', None)
                brand_id = xitem.get('brand_id', None)
                quantity = xitem.get('quantity', 0)
                quantity_unit = xitem.get('quantity_unit', 0)
                subs_code = xitem.get('subscription_code', '')
                valid_from = xitem.get('valid_from', None)

                kwargs = {}
                for item in xitem:
                    if xitem[item] == None:
                        pass
                    else:
                        kwargs[item] = xitem[item]

                kwargs['current_quantity'] = quantity

                if item_id:
                    del kwargs['item_id']
                    if quantity_unit == 1:
                        kwargs['current_quantity'] = quantity - 1

                kwargs['valid_to'] = self.valid_to_calc(valid_from, quantity_unit, quantity)

                subs = UserSubscription.query.filter_by(brand_id=brand_id,
                                                        user_id=user_id).order_by(UserSubscription.id).all()

                if subs:
                    for isubs in subs:
                        if user_id in self.user_subs:
                            pass
                        else:
                            # new date
                            new_date = datetime.strptime(valid_from, '%Y-%m-%d %H:%M:%S')

                            # get old records
                            old_rec = UserSubscription.query.filter_by(user_id=user_id,
                                                                       brand_id=brand_id).order_by(
                                desc(UserSubscription.id)).all()

                            list_data_old_date = [i.valid_from.date() for i in old_rec]

                            # BENNY REQUEST IF DATE SAME, THIS IS RESTORE TRANSLATIONS
                            if new_date.date() in list_data_old_date:
                                exist_rec = UserSubscription.query.filter_by(user_id=user_id,
                                                                             brand_id=brand_id,
                                                                             valid_from=new_date).order_by(
                                    desc(UserSubscription.id)).first()

                                if exist_rec:
                                    temp = exist_rec.values()
                                else:
                                    temp = {}
                                temp['status'] = httplib.OK

                            else:
                                # noinspection PyArgumentList
                                subs = UserSubscription(**kwargs)
                                sv = subs.save()

                                if sv.get('invalid', False):
                                    temp['status'] = httplib.BAD_REQUEST
                                    temp['error_code'] = 400771
                                    temp['developer_message'] = sv.get('message', '')
                                    temp['user_message'] = "Failed"
                                else:
                                    if item_id:
                                        self.save_user_items(isubs, user_id, item_id)

                                    temp = subs.values()
                                    self.user_subs.append(isubs.user_id)
                                    temp['status'] = httplib.CREATED

                        data_files.append(temp)
                        break

                else:
                    # Create new records...
                    # noinspection PyArgumentList
                    subs = UserSubscription(**kwargs)
                    sv = subs.save()

                    if sv.get('invalid', False):
                        temp['status'] = httplib.BAD_REQUEST
                        temp['error_code'] = 400771
                        temp['developer_message'] = sv.get('message', '')
                        temp['user_message'] = "Failed"
                    else:
                        temp = subs.values()
                        temp['status'] = httplib.CREATED

                        if item_id:
                            # save_user_items..
                            self.save_user_items(subs, user_id, item_id)
                    data_files.append(temp)

            return 'OK', Response(json.dumps({'results': data_files},
                                             ensure_ascii=False), status=httplib.MULTI_STATUS,
                                  mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="owned subs 5",
                                                     method=self.method, error=str(e), req=str(self.args))

    def construct(self):
        item_required = self.item_required()
        if item_required[0] != 'OK':
            return item_required[1]

        item_date = self.check_date()
        if item_date[0] != 'OK':
            return item_date[1]

        item_brand = self.check_brands()
        if item_brand[0] != 'OK':
            return item_brand[1]

        item_check = self.item_brands()
        if item_check[0] != 'OK':
            return item_check[1]

        if self.method == 'POST':
            owned_subs_post = self.post_owned_subs()
            return owned_subs_post[1]


class DownloadItems():

    def __init__(self, args=None, method=None):
        self.args = args
        self.method = method

        self.user_id = args.get('user_id', 0)
        self.item_id = args.get('item_id', 0)
        self.signature = args.get('signature', '')
        self.timestamp = args.get('timestamp_unix', '')

        self.file_orders_lte = args.get('file_order__lte', None)
        self.file_orders_gte = args.get('file_order__gte', None)
        self.file_orders_in = args.get('file_order__in', None)

        self.country_code = args.get('country_code', 'ID')
        self.ip_address = args.get('ip_address', None)

        self.limit = args.get('limit', 20)
        self.offset = args.get('offset', 0)

        self.download_salt = current_app.config['DOWNLOAD_SALT']
        self.scoopgs_server = current_app.config['SCOOPGS_SERVER']

        self.master_server = ''
        self.master = ItemFile.query
        self.item_buffet = []

        self.signature_encrypt = ''

    def check_user_items(self):

        if not g or not g.user:
            raise Unauthorized

        session = db.session()

        item = session.query(Item).get(self.item_id)

        assert_user_owned_item(item, user_info=g.user, session=session)

        return 'OK', 'OK'

    def check_signature(self):
        try:
            # BASE64(str(HTTP_TIMESTAMP generated by client + (item_id (x2)) + (int(user_id) + int(1947194879)) + DOWNLOAD_SALT))
            sign_form = str('%s%s%s%s' % (
                self.timestamp, int(self.item_id) * 2, int(self.user_id) + int(self.timestamp), self.download_salt))

            max_loop = int(str(self.timestamp)[-2:]) + 1
            loop_range = range(1, max_loop)

            # first key
            self.signature_encrypt = hashlib.sha256(self.download_salt + sign_form).hexdigest()

            for ixo in loop_range:
                self.signature_encrypt = hashlib.sha256(self.download_salt + self.signature_encrypt).hexdigest()

            if self.signature_encrypt != self.signature:
                return 'FAILED', general_message(modul="downloads",
                                                 method=self.method, req=self.args,
                                                 mssge="Invalid Signature %s" % self.signature,
                                                 usermsg="Download Failed", code=httplib.BAD_REQUEST,
                                                 error_code=400601)
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="download 3",
                                                     method=self.method, error=e, req=str(self.args))

    def check_server(self):
        try:
            if not self.country_code and not self.ip_address:
                return 'FAILED', general_message(modul="downloads",
                                                 method=self.method, req=self.args,
                                                 mssge="Invalid request country code or ip address must fill",
                                                 usermsg="Download Failed", code=httplib.BAD_REQUEST,
                                                 error_code=400604)

            if self.country_code:
                if self.country_code == 'ID':
                    self.master_server = current_app.config['SERVER_62001']
                else:
                    self.master_server = current_app.config['SERVER_65001']
            else:
                # retrieve code from scoopgs
                link_url = '%s%s' % (self.scoopgs_server, self.ip_address)
                scoopgs = requests.get(link_url)

                if scoopgs.status_code == 200:
                    gs_data = scoopgs.json()
                    country_code = gs_data.get('country', {}).get('code_alpha2')

                    if country_code == 'ID':
                        self.master_server = current_app.config['SERVER_62001']
                    else:
                        self.master_server = current_app.config['SERVER_65001']

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="download 4",
                                                     method=self.method, error=e, req=str(self.args))

    def retrieve_data_users(self):
        try:
            self.master = self.master.filter_by(item_id=self.item_id)

            if self.limit:
                self.master = self.master.limit(self.limit)

            if self.offset:
                self.master = self.master.offset(self.offset)

            if self.file_orders_in:
                self.master = self.master.filter(ItemFile.file_order.in_(self.file_orders_in))

            if self.file_orders_lte:
                self.master = self.master.filter(ItemFile.file_order <= self.file_orders_lte)

            if self.file_orders_gte:
                self.master = self.master.filter(ItemFile.file_order >= self.file_orders_gte)

            self.master = self.master.order_by(ItemFile.file_order)

            data_downloads = []
            for item in self.master.all():
                tmp = {}

                data_url = EncryptData(url_base=self.master_server, url_file=item.file_name,
                                       item_id=self.item_id, user_id=self.user_id, itemfile_id=item.id,
                                       country_code=self.country_code, real_item=item.item)

                encrypt_url = data_url.generate_link()

                if encrypt_url == '':
                    return 'FAILED', general_message(modul="downloads",
                                                     method=self.method, req=self.args,
                                                     mssge="Raw file path invalid in 62001/65001",
                                                     usermsg="Download Failed", code=httplib.BAD_REQUEST,
                                                     error_code=400605)

                tmp['url'] = '%s' % encrypt_url

                encrypt_key = EncryptData(key=item.key, item_id=self.item_id).construct()
                tmp['key'] = encrypt_key

                tmp['item_id'] = item.item_id
                tmp['file_type'] = item.file_type
                tmp['file_order'] = item.file_order
                tmp['is_active'] = item.is_active
                tmp['file_status'] = item.file_status
                tmp['file_version'] = item.file_version
                tmp['md5_checksum'] = item.md5_checksum
                data_downloads.append(tmp)

            temp = {}
            temp['count'] = self.master.count()
            if self.offset:
                temp['offset'] = int(self.offset)
            else:
                temp['offset'] = 0

            if self.limit:
                temp['limit'] = int(self.limit)
            else:
                temp['limit'] = 0
            tempx = {'resultset': temp}

            response = {'downloads': data_downloads, 'metadata': tempx}
            return 'OK', Response(json.dumps(response), status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="download 5",
                                                     method=self.method, error=e, req=str(self.args))

    def construct(self):
        chk = self.check_user_items()
        if chk[0] != 'OK':
            return chk[1]

        chk_sign = self.check_signature()
        if chk_sign[0] != 'OK':
            return chk_sign[1]

        chk_server = self.check_server()
        if chk_server[0] != 'OK':
            return chk_server[1]

        retrieve_data = self.retrieve_data_users()
        if retrieve_data[0] != 'OK':
            return retrieve_data[1]

        return retrieve_data[1]


class BuffetPage():

    def __init__(self, item=None, data_page=None):
        self.item = item
        self.data_page = data_page
        self.itemfiles = ''

        self.private_key = current_app.config['PRIVATE_KEY']
        self.ssh_username = current_app.config['SSH_USER_NAME']
        self.ssh_password = current_app.config['SSH_PASSWORD']
        self.temp_files = ''

        self.path62001 = current_app.config['SERVER_62001_PATH']
        self.path62001_tmp = current_app.config['SERVER_62001_TEMP']

        self.server62001 = current_app.config['SERVER_62001']
        self.port62001 = current_app.config['SERVER_62001_PORT']

        self.decrypt_zip_hash = current_app.config['GARAMZIP']
        self.decrypt_pdf_hash = current_app.config['GARAMPDF']

        self.zip_password = ""
        self.pdf_password = ""

    def get_itemfiles(self):
        itemfiles = ItemFile.query.filter_by(item_id=self.item.id).all()
        if itemfiles:
            self.itemfiles = itemfiles
        else:
            # log this item no itemfiles
            self.data_page.status = 4
            self.data_page.error = "ITEM FILES NOT FOUND"
            self.data_page.save()

        return 'OK'

    def setup_ssh(self, server_name=None, port=None):
        server, port, username, password, rsa_private_key = (server_name, port,
                                                             self.ssh_username, self.ssh_password,
                                                             r'%s' % (self.private_key))
        priv_key = paramiko.RSAKey.from_private_key_file(rsa_private_key, password)

        ssh = paramiko.SSHClient()
        # paramiko.util.log_to_file(settings.SSH_LOG_FILE)
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server, port, username=username, password=password, pkey=priv_key)
        return ssh

    def check_data_62001(self):
        try:
            ssh = self.setup_ssh(self.server62001, self.port62001)

            for itemfile in self.itemfiles:
                original = os.path.join(self.path62001,
                                        str(itemfile.file_name).replace('images/1/', ''))

                temp_files = os.path.join(self.path62001_tmp,
                                          os.path.basename(itemfile.file_name))
                self.temp_files = temp_files

                command = 'ls %s' % (original)
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)

                if ssh_stderr.read():
                    file_notexist = ssh_stderr.read().splitlines()[0].split(' ')[-1]
                    if file_notexist == 'directory':
                        return False, "%s not exist" % original

                # if exists copy to temporary files
                command2 = 'cp %s %s' % (original, temp_files)
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command2)

                self.unzip_files(itemfile, ssh)

            ssh.close()
            return True, 'OK'

        except Exception, e:
            return False, "CHECK DATA 62001 ERROR %s" % e

    def decrypt_password(self, itemfiles=None, ssh=None):
        try:
            # DECRYPT ZIP PASSWORD
            # item_id + decrypt_zip_hash + brand_id + hash_2 + content_version_info
            dec_zip = "%s%s%s%s%s%s%s" % (self.item.id, "~#%&", self.decrypt_zip_hash,
                                          "*&#@!", self.item.brand_id, "&%^)", itemfiles.key)
            zip_password = hashlib.sha256(dec_zip).hexdigest()

            # DECRYPT PDF PASSWORD
            # item_id + decrypt_pdf_hash + brand_id + hash_2 + content_version_info
            dec_pdf = "%s%s%s%s%s%s%s" % (self.item.id, "~#%&", self.decrypt_pdf_hash,
                                          "*&#@!", self.item.brand_id, "&%^)", itemfiles.key)
            pdf_password = hashlib.sha256(dec_pdf).hexdigest()

            self.zip_password = zip_password
            self.pdf_password = pdf_password

            return True, 'OK'

        except Exception, e:
            return False, "DECRYPT PASSWORD ERROR %s" % e

    def unzip_files(self, itemfile=None, ssh=None):
        try:
            page_count = 0

            self.decrypt_password(itemfile)
            destination_folder = os.path.join(self.path62001_tmp, str(itemfile.item_id))

            ### extract zip
            command = "unzip -P %s %s -d %s" % (self.zip_password,
                                                self.temp_files, destination_folder)
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
            time.sleep(2)

            ## get extract files page
            thumbs_folder = os.path.join(destination_folder, 'thumbs')
            command2 = "ls -1 %s | wc -l" % (thumbs_folder)
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command2)

            if ssh_stderr.read():
                ssh.close()
                return False, "Folder thumbs can't read"

            page_count = ssh_stdout.read()
            itemfile.page_view = page_count
            itemfile.save()

            self.data_page.total_page = page_count
            self.data_page.save()

            # remove files
            remove_tmp_folder = 'rm -rf %s' % (destination_folder)
            remove_tmp_zip = 'rm -rf %s' % (self.temp_files)

            ssh.exec_command(remove_tmp_folder)
            ssh.exec_command(remove_tmp_zip)

            return True, 'OK'

        except Exception, e:
            return False, "UNZIP FILES ERROR : %s" % e

    def construct(self):
        self.get_itemfiles()

        if self.itemfiles:
            # process check data exist or not
            status, check_data = self.check_data_62001()
            return status, check_data


def build_item_from_request(args, session, existing_item=None):
    """ Constructs an Item with properties set from the request data.

    If existing_item is passed, it will have it's properties updated and the
    same object reference will be returned, otherwise a new instance will
    be created that must later be assigned to the SQLAlchemy session.

    :param `dict` args: Parsed request body arguments.
    :param `sqlalchemy.session` session: A single session to be used for all
        database operations.
    :param `~Item` existing_item: An existing Item object that will have it's
        properties updated based on the passed request data.
    :return: An Item instance, built and configured from the request data,
        along with all it's related data
    """
    skip_args = [
        'authors',
        'categories',
    ]
    # noinspection PyArgumentList
    item = existing_item or Item()

    slug = args.get('slug', None)

    if slug is None:
        slug = generate_slug(args.get('name', None), None)

    # regenerate new slug if slug is exists
    args['slug'] = regenerate_slug(Item, slug, existing_item)

    # updates our standard (non-relation properties)
    for k in args:
        if k in skip_args:
            continue
        setattr(item, k, args[k])

    # updates our relationship properties
    authors = [session.query(Author).get(db_id) for db_id in args['authors']]
    if not all(authors):
        raise Conflict(
            "Requested authors were invalid"
            ": {}.  Please check the database".format(
                ",".join([str(s) for s in args['authors']])))
    else:
        item.authors = authors

    categories = [session.query(Category).get(db_id)
                  for db_id in args['categories']]
    if not all(categories):
        raise Conflict(
            "Requested categories were invalid: "
            "{}.  Please check the database".format(",".join(
                [str(s) for s in args['categories']])))
    else:
        item.categories = categories

    return item


def get_latest_items_for_brand(session, brand_id, max_age=None, max_results=None):
    """ Fetches the latest items for a brand that can be displayed in the store.

    :param `sqlalchemy.orm.Session` session:
        Session object to be used for querying.
    :param `int` brand_id:
        Only includes Items that have a brand_id matching this param.
    :param `datetime.timedelta` max_age:
        Items included in the results will not have a release date older than
        max age from the current time.
    :param `int` max_results:
        Maximum size of returned list.
    :return: A list of Items that meet the search critera.
    :rtype: list
    """
    query = session.query(Item).filter(
        (Item.brand_id == brand_id) &
        (Item.is_active == True) &
        (Item.item_status == 'ready for consume')
    ).order_by(desc(Item.release_date))

    if max_age:
        query = query.filter(Item.release_date >= (datetime.now() - max_age))
    if max_results:
        query = query.limit(max_results)

    return query.all()


def get_all_latest_editions(session, max_results, last_min_item_id=None):
    """ Returns all the latest magazines/newspapers for web reader processor.

    Ignores books.

    :param session: database session
    :param 'int' max_results: limit/maximum top n record for the latest edition
        that get selected for current loop
    :param 'int' last_min_item_id:
    :return: List of all Items.
    """
    # to make sure uploader already finished uploading latest editions, we set limit to 30 minutes ago
    time_limit = get_utc_nieve() - timedelta(minutes=30)

    latest_editions = session.query(Item.id, Item.brand_id)

    if last_min_item_id:
        latest_editions = latest_editions.filter(
            Item.id < last_min_item_id,
            Item.item_type.in_(['book', 'magazine']),
        )

    latest_editions = latest_editions.filter(
        Item.item_status == 'ready for consume',
        Item.is_active == True,
        Item.content_type == 'pdf',
        Item.page_count == None,
        Item.modified < time_limit
    ).order_by(Item.id.desc()).limit(max_results).all()
    session.close_all()

    return latest_editions


def record_user_analytics_data(scene):
    # noinspection PyArgumentList
    try:
        activity = save_user_search_activity(
            user_id=g.user.user_id,
            query_text=request.args.get('q'),
            query_results=json.loads(scene.data)
                .get('metadata', {})
                .get('resultset', {})
                .get('count') if scene.status_code == httplib.OK else 0)
    except Exception as e:
        # purpously swallow all exceptions here, because we don't want
        # a logging failure to crash the app.
        app.logger.exception("Error recording analytics data for user search on record_user_analytics_data")


def assert_user_owns_item(item_id, user_id):
    s = db.session()
    if s.query(UserItem).filter_by(item_id=item_id, user_id=user_id).first():
        return
    else:
        raise Forbidden


def get_item_or_404(db_id):
    s = db.session()
    item = s.query(models.Item).get(db_id)
    if not item:
        raise NotFound(
            developer_message='<Item(id: {})> does not exist'.format(db_id),
            user_message="Sorry, we can't find that Item!")
    return item


def get_thumbnail_url_for_item(item_id, page_num):
    return os.path.join(
        current_app.static_folder, 'item_content_files', 'thumbnails',
        text_type(item_id), '{}.jpg'.format(page_num)
    )


class ItemOwnershipStatus(Enum):
    does_not_own = 'does not own'
    permanent_ownership = 'permanent ownership'
    buffet_ownership = 'buffet ownership'
    publisher_ownership = 'publisher ownership'
    borrowed_item = 'borrow ownership'


class OfferTypeValue(IntEnum):
    single = 1
    subscription = 2
    bundle = 3
    buffet = 4


class UserItemRepository(object):

    def __init__(self, user, session=None):
        self.user = user
        self.session = session or db.session()

    def create(self, item, orderline=None):

        if not orderline:
            # get a standard order
            if not orderline:
                raise RuntimeError("Couldn't determine the order line to use.")

        user_item = UserItem(
            item=item,
            orderline=orderline,
            user_id=self.user.user_id,
            is_active=True
        )

        if orderline.offer.offer_type_id == OfferTypeValue.buffet:
            user_buffet = self.session.query(UserBuffet).filter(UserBuffet.orderline_id == orderline.id).first()
            user_item.user_buffet = user_buffet

        db.session.add(user_item)
        db.session.commit()

        return user_item

    def get(self, item):
        return (self.session.query(UserItem)
                .filter(UserItem.item_id == item.id,
                        UserItem.user_id == self.user.user_id)
                .order_by(desc(UserItem.created))
                .first())

    def get_or_new_user_item(self, item, orderline=None):

        user_item = self.get(item)

        if user_item is None:
            user_item = UserItem(
                item=item,
                orderline=orderline,
                user_id=self.user.user_id,
                is_active=True
            )

        return user_item


def assert_user_owned_item_for_web_reader(item, session=None, organization_id=None):
    """

    :param item:
    :param user_info:
    :param session:
    :return:
    """
    try:
        session = session or db.session()

        # for user in apps-foundry organizations, can freely download the item
        user = getattr(g, 'current_user', None)

        if not is_apps_foundry_users(user=user):
            ownership_status = get_current_ownership_status(user_id=user.id, item=item, organization_id=organization_id)

            if ownership_status == ItemOwnershipStatus.does_not_own:
                # check if user is from publisher
                ownership_status = assert_item_vs_user_publisher(
                    item=item, user_id=user.id, session=session)

                if ownership_status == ItemOwnershipStatus.does_not_own:
                    check_and_create_buffet_user_item(user.id, item, session)

    except Forbidden:
        if not 'can_read_write_global_all' in g.user.perm:
            raise


def assert_user_owned_item(item, user_info, session=None):
    """ assert user ownership status of an item.

    if user owned a buffet, check item based on buffet brands or buffet vendors too
        if valid, add the item to user item data

    :param `app.items.models.Item` item:
    :param `app.auth.models.UserInfo` user_info:
    :param `sqlalchemy.orm.session.Session` session:
    :return:
    """
    try:
        session = session if session else db.session()

        # for user in apps-foundry organizations, can freely download the item
        if not is_apps_foundry_users(user=getattr(g, 'current_user', None)):

            ownership_status = get_current_ownership_status(user_info.user_id, item, session)

            if ownership_status == ItemOwnershipStatus.does_not_own:

                # check from user borrowed item (from Shared Library)
                ownership_status = get_current_borrow_status(user_info.user_id, item.id, session)

                if ownership_status == ItemOwnershipStatus.does_not_own:
                    # check if user is from publisher
                    ownership_status = assert_item_vs_user_publisher(
                        item=item, user_id=user_info.user_id, session=session)

                if ownership_status == ItemOwnershipStatus.does_not_own:
                    # final check, from user buffet, if not owned, raise forbidden
                    check_and_create_buffet_user_item(user_info.user_id, item, session)

    except Forbidden:
        if 'can_read_write_global_all' not in user_info.perm:
            raise


def assert_item_vs_user_publisher(item, user_id=None, session=None):
    """ assert item vendor id vs current user organization id's
    and assert g.current user roles too (must be in allowed user-publisher roles)

    :param `app.items.models.Item`item:
    :param `integer` user_id: user identifier of app.auth.models.User instance,
        if none provided, it will get from g.current_user object
    :param `sqlalchemy.orm.session.Session` session:
    :return:
    :rtype: ItemOwnershipStatus
    """
    valid_item_vs_user_publisher = False
    if user_id and g.current_user.id != user_id:
        session = session or db.session()
        user = session.query(User).get(user_id)
    else:
        user = g.current_user
    from app.users.items import is_user_role_publisher
    if user and is_user_role_publisher(user):
        user_org_ids = [org.id for org in user.organizations.all()]
        if user_org_ids and item and item.brand and item.brand.vendor and \
            item.brand.vendor.organization_id in user_org_ids:
            valid_item_vs_user_publisher = True

    if valid_item_vs_user_publisher:
        return ItemOwnershipStatus.publisher_ownership
    else:
        return ItemOwnershipStatus.does_not_own


def get_current_borrow_status(user_id, item_id, session=None):
    """

    :param `integer` user_id: user identifier of app.auth.models.User instance
    :param `integer` item_id: item identifier of app.items.models.Item instance
    :param `sqlalchemy.orm.session.Session` session:
    :return:
    :rtype: ItemOwnershipStatus
    """
    from app.eperpus.members import UserBorrowedItem

    session = session or db.session()

    user_borrowed_item = (
        session.query(UserBorrowedItem)
            .filter(
            UserBorrowedItem.user_id == user_id,
            UserBorrowedItem.catalog_item_item_id == item_id,
            UserBorrowedItem.returned_time == None
        )
            .first())

    if user_borrowed_item:
        return ItemOwnershipStatus.borrowed_item
    else:
        return ItemOwnershipStatus.does_not_own


def get_current_ownership_status(user_id, item, session=None, organization_id=None):
    """

    :param `integer` user_id:
    :param `app.items.models.Item` item:
    :param `sqlalchemy.orm.session.Session` session:
    :return:
    :rtype: ItemOwnershipStatus
    """
    from app.eperpus.members import UserBorrowedItem

    session = session or db.session()

    user_item = (session.query(UserItem)
                 .filter(UserItem.item == item, UserItem.user_id == user_id)
                 .order_by(desc(UserItem.created))
                 .first())

    if user_item is None:
        # check if this item has borrowed for eperpus.
        if organization_id:
            eperpus_borrowed = UserBorrowedItem.query.filter(
                UserBorrowedItem.user_id == user_id,
                UserBorrowedItem.catalog_item_item_id == item.id,
                UserBorrowedItem.catalog_item_org_id == organization_id,
                UserBorrowedItem.returned_time is not None).first()
            if eperpus_borrowed:
                return ItemOwnershipStatus.permanent_ownership
        return ItemOwnershipStatus.does_not_own

    elif user_item.user_buffet is not None:
        if user_item.user_buffet.valid_to < datetime.utcnow():
            return ItemOwnershipStatus.does_not_own
        else:
            return ItemOwnershipStatus.buffet_ownership
    else:
        return ItemOwnershipStatus.permanent_ownership


def get_active_user_buffet_for_item(user_id, item, session):
    """

    :param `integer` user_id:
    :param `app.items.models.Item` item:
    :param session:
    :return:
    """
    session = session or db.session()

    active_buffet = session.query(UserBuffet).filter(
        UserBuffet.user_id == user_id,
        UserBuffet.valid_to >= datetime.utcnow(),
    ).join(UserBuffet.offerbuffet).join(
        Offer).join(Offer.brands).filter(Brand.id == item.brand_id).first()

    if not active_buffet:
        # if failed to check user buffet by offer-brands
        #  try to check by vendor registered in premium offers
        active_buffet = session.query(UserBuffet).filter(
            UserBuffet.user_id == user_id,
            UserBuffet.valid_to >= datetime.utcnow(),
        ).join(UserBuffet.offerbuffet).join(
            Offer).join(Offer.vendors).filter(Vendor.id == item.brand.vendor_id).first()
    return active_buffet


def check_and_create_buffet_user_item(user_id, item, session=None):
    session = session or db.session()

    if get_current_ownership_status(user_id, item, session) == ItemOwnershipStatus.does_not_own:

        active_buffet = get_active_user_buffet_for_item(user_id, item, session)

        if active_buffet:
            # add to user owned item based on user buffet
            create_buffet_user_item(user_id, item, active_buffet, session)
        else:
            raise Forbidden


def create_buffet_user_item(user_id, item, active_buffet, session=None):
    session = session or db.session()

    buffet_user_item = UserItem(
        user_id=user_id,
        user_buffet=active_buffet,
        item=item,
        orderline=active_buffet.orderline,
        is_active=True,
        edition_code=item.edition_code,
        vendor_id=item.brand.vendor_id,
        brand_id=item.brand_id,
        itemtype_id=item_types.enum_to_id(item.item_type)
    )

    session.add(buffet_user_item)
    session.commit()


def get_item_cache_key(item_id, platform_id):
    return 'item_latest_response:{}:platform_id:{}'.format(item_id, platform_id)


def get_item_request_body(item, platform_id):
    restricted, allowed = item.get_restriction_country()
    display_offers, display_price = item.get_offers(platform_id=platform_id)
    return {
        'id': item.id,
        'name': item.name,
        'item_type_id': {ITEM_TYPES[k]: k for k in ITEM_TYPES}[item.item_type],
        'item_type': item.get_item_type(),
        'brand_id': item.brand_id,
        'vendor': {
            'slug': item.brand.vendor.slug,
            'name': item.brand.vendor.name,
            'id': item.brand.vendor.id,
        },
        "item_status": {STATUS_TYPES[k]: k for k in STATUS_TYPES}[item.item_status],
        "restricted_countries": restricted,
        "allowed_countries": allowed,
        'media_base_url': app.config['MEDIA_BASE_URL'],
        "thumb_image_highres": preview_s3_covers(item, 'thumb_image_highres'),
        "thumb_image_normal": preview_s3_covers(item, 'thumb_image_normal'),
        "issue_number": item.issue_number,
        "authors": item.get_authors(),
        "display_offers": display_offers,
        "display_price": display_price,
        "is_latest": item.get_is_latest(),  # it should be optimized, not needed.. just read release data!
        'release_date': datetime_to_isoformat(item.release_date),
        "parental_control_id": item.parentalcontrol_id,
        "edition_code": item.edition_code,
        "is_featured": item.is_featured,
        "is_extra": item.is_extra,
        "printed_currency_code": item.printed_currency_code,
        "page_count": item.page_count,
        "languages": item.get_languages(),
        "reading_direction": {READING_DIRECTIONS[k]: k for k in READING_DIRECTIONS}[item.reading_direction],
        "filenames": item.filenames,
        "release_schedule": datetime_to_isoformat(item.release_schedule) if item.release_schedule else None,
        "brand": {
            "slug": item.brand.slug,
            "name": item.brand.name,
            "id": item.brand_id,
        },
        "is_active": item.is_active,
        "slug": item.slug,
        "image_highres": preview_s3_covers(item, 'image_highres'),
        "printed_price": '%.2f' % item.printed_price if item.printed_price else None,
        "content_type": {FILE_TYPES[k]: k for k in FILE_TYPES}[item.content_type],
        # "web_reader_files_ready": True if item.page_count > 0 else False,
        "web_reader_files_ready": False  # Temporary disable for web readers
    }


def construct_item_download_response(item):
    """ Constructs the HttpResponse for an item.

    :param `app.items.models.Item` item:
        The item that owns the content being displayed.
    :return: An HTTP response.
    """
    bucket = app.config['BOTO3_GRAMEDIA_MOBILE_BUCKET']
    file_bucket_path = '{}/{}.zip'.format(item.brand_id, item.edition_code)
    shared_link = aws_shared_link(bucket, file_bucket_path)

    if shared_link:
        link_url = shared_link.replace('https://ebook-mobilebundles.s3.amazonaws.com/', '')
        url_link = '{}/{}'.format(app.config['NGINX_S3_MOBILE_BUNDLES'], link_url)
    else:
        url_link = construct_nginx_internal_download_url(item)

    response = make_response("", httplib.OK, {
        'Content-Type': "application/zip",
        'X-Accel-Redirect': url_link
    })
    return response


def construct_nginx_internal_download_url(item):
    """ Builds an required NGINX internal url for an item

    :param `app.items.models.Item` item: The Item that owns the content page.
    :return: A stringified URL representation
    :rtype: str
    """

    if item.files and item.files[0]:
        zip_file_path = item.files[0].file_name.replace('images/1/', '')
    else:
        zip_file_path = '{0.brand_id}/{0.edition_code}.zip'.format(item)

    return ('{secure_media_base}/'
            'item_content_files/'
            'processed/{zip_file_path}').format(
        secure_media_base=app.config['NGINX_SECURE_MEDIA_BASE'],
        zip_file_path=zip_file_path)


class ItemQueryBuilder(SolrQueryBuilder):
    """ Build parameter query for getting items from solr
    """

    def _build_query(self):
        params = super(ItemQueryBuilder, self)._build_query()
        if self.flask_req.args.get('with_content', False):
            params.update({
                'q': 'content:{}'.format(params.get('q'))
            })

        return params

    def _build_highligting(self):
        return {
            'hl': 'on',
            'hl.fl': 'content'
        }

    def _build_filter_queries(self):
        params = super(ItemQueryBuilder, self)._build_filter_queries()
        is_restricted = self.flask_req.args.get('is_age_restricted', False)
        if is_restricted:
            params['fq'].append('is_age_restricted:{}'.format(is_restricted))

        exclude_free_item = self.flask_req.args.get('exclude-free-item', None)
        if exclude_free_item and exclude_free_item.lower() in ['true', '1']:
            # hide item with price 0
            params['fq'].append('price_web_idr:[0.1 TO *]')

        include_inactive = self.flask_req.args.get('include-inactive', None)
        if not include_inactive or include_inactive.lower() in ['false', '0']:
            # default, show only active item
            params['fq'].append('is_active:true')
            # for portal that need to maintain ALL item, set: include-inactive=true (show all item)

        # for certain ePerpus (ex KG Smart), they only allowed to search item from specific vendors only
        eperpus_org_id = self.flask_req.args.get('eperpus-id', None)
        if eperpus_org_id:
            student_level = "({} OR {})".format(StudentSaveControlType.student_save_level_1.value,
                                                StudentSaveControlType.student_save_level_6.value)

            organization_data = Organization.query.filter_by(id=eperpus_org_id).first()
            if organization_data and organization_data.parental_level:
                if organization_data.parental_level in [StudentSaveControlType.student_save_level_1.value,
                                                        StudentSaveControlType.student_save_level_6.value]:
                    student_level = "({} OR {})".format(StudentSaveControlType.student_save_level_1.value,
                                                        StudentSaveControlType.student_save_level_6.value)

                if organization_data.parental_level in [StudentSaveControlType.student_save_level_2.value,
                                                        StudentSaveControlType.student_save_level_3.value]:
                    student_level = "({} OR {})".format(StudentSaveControlType.student_save_level_2.value,
                                                        StudentSaveControlType.student_save_level_3.value)

                if organization_data.parental_level in [StudentSaveControlType.student_save_level_4.value]:
                    student_level = "({} OR {} OR {})".format(StudentSaveControlType.student_save_level_2.value,
                                                              StudentSaveControlType.student_save_level_3.value,
                                                              StudentSaveControlType.student_save_level_4.value)

                if organization_data.parental_level in [StudentSaveControlType.student_save_level_5.value]:
                    student_level = "({} OR {} OR {} OR {})".format(StudentSaveControlType.student_save_level_2.value,
                                                                    StudentSaveControlType.student_save_level_3.value,
                                                                    StudentSaveControlType.student_save_level_4.value,
                                                                    StudentSaveControlType.student_save_level_5.value)

            params['fq'].append('studentsave_id:{}'.format(student_level))

            vendors = Vendor.query.join(
                Vendor.restricted_shared_libraries).filter(
                SharedLibraryRestrictedVendor.organization_id == eperpus_org_id).all()

            if vendors:
                restricted_vendors = [str(i.id) for i in vendors]
                query_restricted_vendors = ' OR '.join(restricted_vendors)
                params['fq'].append('-vendor_id:({})'.format(query_restricted_vendors))

        return params

    def build(self):
        query = super(ItemQueryBuilder, self).build()
        if self.flask_req.args.get('with_content', False):
            query.update(self._build_highligting())

        return query


def _handle_409(exception):
    return {
               "status": httplib.CONFLICT,
               "developer_message": "author : {} already exists".format(exception.field),
               "error_code": 409101,
               "user_message": "An author with that {} already exist".format(exception.field)
           }, httplib.CONFLICT


class AuthorValidationException(Exception):
    def __init__(self, field_name, value):
        self.value = value
        self.field = field_name
        self.code = httplib.CONFLICT


def assert_author_slug_unique(query, slug, author_id):
    conflicting_count = query.filter((Author.slug == slug) & (Author.id != author_id)).count()
    if conflicting_count:
        raise AuthorValidationException("slug", slug)


def assert_author_name_unique(query, name, author_id):
    print query
    conflicting_count = query.filter((Author.name == name) & (Author.id != author_id)).count()
    if conflicting_count:
        raise AuthorValidationException("name", name)


def build_query(base_query, input_args):
    """
        :param base_query: base models
        :param input_args: args from parsers
        :return: result query and count query
    """
    query = base_query
    query = query.filter_by(is_active=input_args['is_active'])

    if input_args['q']:
        query = query.filter(or_(Author.name.ilike('%' + input_args['q'] + '%'),
                                 Author.first_name.ilike('%' + input_args['q'] + '%'),
                                 Author.last_name.ilike('%' + input_args['q'] + '%')))

    ordering = specialized.SqlAlchemyOrderingParser().parse_args().get('order')
    for order_opt in ordering:
        query = query.order_by(order_opt)

    # pagination
    query_count = query.count()

    query = query.limit(input_args['limit']).offset(input_args['offset']).all()
    query = {'query': query, 'query_count': query_count}
    return query


def reformat_response_json(response):
    """ reformat response from solr response and make it suitable for Item API.
    Maybe need to be refactored.

    :param `dict` response: from solr
    :return: returning response for Item API
    :rtype: `dict`
    """
    facet_items = response['facet_counts']['facet_fields'].items() if 'facet_counts' in response \
        else []
    facet_values = []
    if facet_items:
        facet_values = [
            {"field_name": key, "values": [
                {"value": t[0], "count": t[1]} for t in zip(facet[0::2], facet[1::2])
            ]
             } for key, facet in facet_items]

    spell_check = []
    if not response.get('spellcheck', {'correctlySpelled': True})['correctlySpelled']:
        suggestions = response['spellcheck']['suggestions'][1]['suggestion'] \
            if response['spellcheck'].get('suggestions') else []
        spell_check = [{"value": data['word'], "count": data['freq']} for data in suggestions]

    items = []
    session = db.session()
    for doc in response['response']['docs']:
        item_id = doc.get('id')
        offer_single = session.query(Offer).join(Item.core_offers).filter(
            Item.id == item_id).filter_by(offer_status=7,
                                          is_active=True, offer_type_id=SINGLE).order_by(desc(Offer.id)).first()
        if offer_single:
            offer_single_discounted_price = offer_single.display_discounts(
                idiscounts=offer_single.discount_id,
                values={},
                platform_id=PlatformType.web,
                offer=offer_single
            )
            discount_price_usd = float(offer_single_discounted_price.get('discount_price_usd', '0'))
            discount_price_idr = float(offer_single_discounted_price.get('discount_price_idr', '0'))
            offer_single_response = {
                'id': offer_single.id,
                'title': offer_single.long_name,
                'href': offer_single.api_url,
                'price_usd': offer_single.price_usd,
                'price_idr': offer_single.price_idr,
                'discount_price_usd': discount_price_usd if discount_price_usd else offer_single.price_usd,
                'discount_price_idr': discount_price_idr if discount_price_idr else offer_single.price_idr,
            }
        else:
            offer_single_response = None

        item_response = {
            'id': item_id,
            'item_name': doc.get('item_name'),
            'brand_name': doc.get('brand_name'),
            'offer_single': offer_single_response,
            'category_names': doc.get('category_names'),
            'vendor_name': doc.get('vendor_name'),

            'price_android_usd': doc.get('price_android_usd'),
            'price_android_idr': doc.get('price_android_idr'),

            'price_ios_idr': doc.get('price_ios_idr'),
            'price_ios_usd': doc.get('price_ios_usd'),

            'price_web_usd': doc.get('price_web_usd'),
            'price_web_idr': doc.get('price_web_idr'),

            'countries': doc.get('countries'),
            'languages': doc.get('languages'),

            'item_thumb_highres': doc.get('item_thumb_highres'),
            'is_age_restricted': doc.get('is_age_restricted'),
            'highlighting': {
                'content': response.get('highlighting', {}).get(doc.get('id')).get('content')[0] if
                response.get('highlighting', {}).get(doc.get('id')).get('content') else ''
            } if response.get('highlighting') else {}
        }
        items.append(item_response)

    return {
        'items': items,
        'metadata': {
            'resultset': {
                'offset': request.args.get('offset', type=int, default=0),
                'limit': request.args.get('limit', type=int, default=20),
                'count': response['response']['numFound']
            },
            'facets': facet_values,
            'spelling_suggestions': spell_check,
        }
    }


def check_web_reader_file_exist(item, page_num):
    s3_server = connect_gramedia_s3()
    bucket = app.config['BOTO3_GRAMEDIA_WEBREADER_BUCKET']
    image_ext = ['.jpg', '.png']
    file_object = None
    for iext in image_ext:
        bucket_path = '{}/{}/{}{}'.format(item.brand_id, item.id, page_num, iext)
        check_files = aws_check_files(s3_server, bucket, bucket_path)
        if check_files is not None:
            file_object = s3_server.get_object(Bucket=bucket, Key=bucket_path)

    return file_object


def get_file_object(item, page_num):
    s3_server = connect_gramedia_s3()
    bucket = app.config['BOTO3_GRAMEDIA_WEBREADER_BUCKET']
    image_ext = ['.jpg', '.png']
    file_object = None
    for iext in image_ext:
        bucket_path = '{}/{}/{}{}'.format(item.brand_id, item.id, page_num, iext)
        check_files = aws_check_files(s3_server, bucket, bucket_path)
        if check_files is not None:
            file_object = s3_server.get_object(Bucket=bucket, Key=bucket_path)

    return file_object


def watermark_image(file_object, email, organization_id=None):
    from PIL import Image, ImageDraw, ImageFont

    photo = Image.open(file_object)
    font_dir = os.path.join(app.config['FONTS_BASE_DIR'], 'RobotoSlab-Bold.ttf')

    image_width = photo.width
    image_height = photo.height

    font_size = 60
    if image_width in range(0, 700): font_size = 35
    if image_width in range(701, 1500): font_size = 60
    if image_width > image_height: font_size = 35
    if image_width == image_height: font_size = 35

    # create new layer for watermark
    font = ImageFont.truetype(font=font_dir, size=font_size)
    txt = Image.new('RGBA', (image_width, image_height), (255, 255, 255, 0))
    drawing = ImageDraw.Draw(txt)

    # color
    fill_color = (0, 102, 204, 70)
    text_draw = '{}:{}'.format(organization_id, email) if organization_id else email

    # specific locations.
    if image_width > image_height or image_width == image_height:
        drawing.text((int(image_width * 0.4), int(image_height * 0.3)), text_draw, font=font, fill=fill_color)
        drawing.text((int(image_width * 0.4), int(image_height * 0.0)), text_draw, font=font, fill=fill_color)
    else:
        drawing.text((10, int(image_height * 0.25)), text_draw, font=font, fill=fill_color)
        drawing.text((40, int(image_height * 0.05)), text_draw, font=font, fill=fill_color)

    rotate = txt.rotate(50, expand=1)
    photo.paste(rotate, (0, 100), rotate)
    return photo


def get_avg_and_total_rating(item_id):
    sql = """select avg(rating), count(*) from core_reviews where item_id = {item_id} and is_active;""".format(
        item_id=item_id)
    resultset = db.engine.execute(sql)
    result = resultset.fetchone()
    average = result[0] if result[0] is not None else 0
    total = result[1] if result[1] is not None else 0
    return float("{0:.1f}".format(average)), total
