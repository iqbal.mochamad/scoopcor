import httplib
import json
from copy import deepcopy
from datetime import datetime, timedelta

import re
import solr
from flask import Response
from sqlalchemy import func
from sqlalchemy.sql import desc

from app import app, db
from app.caching import generate_redis_key
from app.items.helpers import get_filter_parental_control, get_dist_country_filter
from app.items.models import DistributionCountryGroup, Author
from app.items.models import Item, ItemFile, UserItem, UserSubscription
from app.master.models import Brand, Vendor
from app.offers.choices.offer_type import SINGLE
from app.offers.models import Offer
from app.parental_controls.choices import ParentalControlType
from app.utils.shims import languages, countries
from .choices import STATUS_TYPES, ITEM_TYPES, MAGAZINE, NEWSPAPER, BOOK, AUDIOBOOK


class CustomGet(object):

    def __init__(self, param=None, module=None):
        self.args = param
        self.module = module

        self.limit = int(param.get('limit', 20))
        self.offset = int(param.get('offset', 0))

        self.item_id = param.get('item_id', None)
        self.fields = param.get('fields', None)
        self.category_id = param.get('category_id', None)

        self.item_type_id = int(param.get('item_type_id', 0))
        self.parental_control_id = int(param.get('parental_control_id', 0))

        self.brand_id = param.get('brand_id', None)
        self.language_id = param.get('language_id', None)
        self.country_id = param.get('country_id', None)
        self.author_id = param.get('author_id', None)
        self.q = param.get('q', None)
        self.q2 = param.get('q2', None)

        if param.get('is_active', None):
            self.is_active = False \
                if param.get('is_active', 'True').lower() == ['false', '0'] \
                else True
        else:
            self.is_active = None

        # convert to enum
        self.item_status = param.get('item_status', None)

        self.extend = param.get('expand', None)
        self.display_offer_plid = param.get('display_offer_plid', None)
        self.vendor_id = param.get('vendor_id', None)

        self.created_lt = param.get('created__lt', None)
        self.created_lte = param.get('created__lte', None)
        self.created_gt = param.get('created__gt', None)
        self.created_gte = param.get('created__gte', None)

        self.modified_lt = param.get('modified__lt', None)
        self.modified_lte = param.get('modified__lte', None)
        self.modified_gt = param.get('modified__gt', None)
        self.modified_gte = param.get('modified__gte', None)

        self.release_date_lt = param.get('release_date__lt', None)
        self.release_date_lte = param.get('release_date__lte', None)
        self.release_date_gt = param.get('release_date__gt', None)
        self.release_date_gte = param.get('release_date__gte', None)

        self.payment_gateway = param.get('payment_gateway', None)
        self.order = param.get('order', None)

        self.is_free = param.get('is_free', False)
        self.is_bundle = param.get('is_bundle', False)
        self.is_buffet = param.get('is_buffet', False)
        self.item_id__notin = param.get('item_id__notin', None)
        self.slug = param.get('slug', None)
        self.redis_cache = param.get('redis_cache', True)
        self.api_version = param.get('api_version', '1.0')

        self.master = ''
        self.total = 0
        self.redis_key = 'unknown'

    def json_success(self, data_items=None, offset=None, limit=None):

        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:

            if self.display_offer_plid: platform_id = self.display_offer_plid
            else: platform_id = 4 # DEFAULT OFFER PLATFORMS

            if self.fields and self.extend:
                extends = self.extend.split(',')
                cur_values = [u.extension_values(extension=extends,
                    custom=self.fields, platform_id=platform_id) for u in data_items]

            elif self.extend:
                extends = self.extend.split(',')
                cur_values = [u.extension_values(extension=extends,
                    platform_id=platform_id) for u in data_items]

            elif self.fields:
                cur_values = [u.custom_values(self.fields) for u in data_items]
            else:
                cur_values = [u.values() for u in data_items]

            temp = {}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}

            # Additional validation for free newspaper
            if self.is_free and self.item_type_id == 3:
                for cur_value in cur_values:
                    cur_value['is_latest'] = True

            if self.module == 'ITEMS_DETAILS':
                data = data_items[0]
                values = cur_values[0]
                if not self.fields:
                    values['description'] = data.get_descriptions()
                values['parental_control'] = data.get_parental_control()
                rv = json.dumps(values)

                if not app.kvs.get(self.redis_key):
                    app.kvs.set(self.redis_key, rv, app.config['ITEM_DETAILS_REDIS_TIME'])

            else:
                # only Item Latest API
                if self.module == 'LATEST':
                    if self.offset / self.limit < app.config['MAXIMUM_PAGE'] and self.offset % self.limit == 0:
                        start = self.offset
                        end = self.limit
                        for i in range(app.config['MAXIMUM_PAGE']):
                            args = self.args
                            args.update({'offset': str(start)})

                            tempx['resultset']['limit'] = self.limit
                            tempx['resultset']['offset'] = start

                            result = cur_values[start:end]
                            start = end
                            end = start + limit

                            cached_rv = {'items': result, "metadata": tempx}

                            app.kvs.set(generate_redis_key('LATEST', args=args),
                                        json.dumps(cached_rv), app.config['LATEST_ITEM_REDIS_TIME'])

                            if i == self.offset / self.limit < app.config['MAXIMUM_PAGE']:
                                rv = deepcopy(cached_rv)
                    else:
                        rv = {'items': cur_values, "metadata": tempx}
                else:
                    rv = {'items': cur_values, "metadata": tempx}
                rv = json.dumps(rv)

            return Response(rv, status=httplib.OK, mimetype='application/json')

    def items_master(self):
        ''' Returns a Query object for app.items.models.Item.query '''
        if self.api_version == '3.0':
            # this is for Audiobook only.
            return Item.query.filter(
                Item.item_type.in_([ITEM_TYPES[BOOK], ITEM_TYPES[MAGAZINE], ITEM_TYPES[NEWSPAPER], ITEM_TYPES[AUDIOBOOK]]))
        else:
            return Item.query.filter(Item.item_type.in_([ITEM_TYPES[BOOK], ITEM_TYPES[MAGAZINE], ITEM_TYPES[NEWSPAPER]]))

    def compressor(self):
        if self.is_free in ['True', 'true', '1']: self.is_free = True
        else: self.is_free = False

    def filter_custom(self):
        self.compressor()
        self.master = self.items_master()

        key_dict = str(hash(json.dumps(self.args))).replace('-', '')
        if self.module == 'ITEMS_DETAILS' and self.redis_cache is True:
            key_name = 'items-details-{}'.format(self.item_id)
            self.redis_key = '{}:{}'.format(key_name, key_dict)
            data_redis = app.kvs.get(self.redis_key)
            if data_redis:
                return Response(data_redis, status=httplib.OK, mimetype='application/json')

        # if filter by slug
        if self.slug:
            self.master = self.master.filter_by(slug = self.slug)

        # this is for iap ios filtering is_free offer only
        if self.is_free:
            item_ids = []

            # if filter specific item_id v1/items?item_id=1,2,3,4&is_free=True.
            if self.item_id:
                idx = self.item_id.split(',')
                offer_free = Offer.query.join(Item.core_offers).filter(
                    Item.id.in_(idx)).filter_by(is_free=True, is_active=True).limit(100)

                if offer_free:
                    for i in offer_free:
                        if i.get_items():
                            item_ids.append(str(i.get_items()[0]))
                    self.item_id = ','.join(item_ids)
                else:
                    self.item_id = None

            else:
                # if filter only v1/items?is_free=True, display items with latest offer free
                if self.item_type_id == 2:
                    offer_free = Offer.query.filter_by(is_free=True,
                        is_active=True).order_by(desc(Offer.id)).limit(500)
                else:
                    offer_free = Offer.query.filter_by(is_free=True,
                        is_active=True).order_by(desc(Offer.id)).limit(200)

                for i in offer_free:
                    if i.get_items():
                        item_ids.append(str(i.get_items()[0]))
                self.item_id = ','.join(item_ids)

        # this is for iap ios bundle page
        if self.is_bundle:
            item_ids = []
            offer_bundle = Offer.query.filter_by(offer_type_id=3).order_by(desc(Offer.id)).all()

            if offer_bundle:
                for i in offer_bundle:
                    for d in i.get_items():
                        item_ids.append(str(d))
                self.item_id = ','.join(item_ids)

        # this is for buffet
        if self.is_buffet:
            item_ids = []

            offer_buffet = Offer.query.filter_by(offer_type_id=4,
                is_active=True, offer_status=7).order_by(desc(Offer.id)).all()

            if offer_buffet:
                for i in offer_buffet:

                    # item by brands
                    if i.get_brands_id():
                        item_by_brands = Item.query.filter(Item.brand_id.in_(
                            i.get_brands_id())).order_by(desc(Item.release_date)).all()

                        item_id_brand = [j.id for j in item_by_brands]
                        item_ids = item_ids + item_id_brand

                    # is on list items, add to temporary
                    if i.get_items():
                        item_ids = item_ids + i.get_items()

                    # group by item id
                    item_ids = list(set(item_ids))

                self.item_id = ",".join(str(i) for i in item_ids)

        if self.order:
            #    v1/ITEMS ONLY
            if self.module in ['ITEMS', 'SEARCH', 'RECOMMENDATION']:
                order_data = self.order.split(',')
                for orderx in order_data:
                    if orderx[0] == '-': # descending
                        order_param = '%s %s' % (orderx[1:], 'desc')
                        self.master = self.master.order_by(order_param)
                    else:
                        self.master = self.master.order_by(orderx)

        if self.module == 'LATEST':
            #    automatic order by date created
            if int(self.item_type_id) == 3: #    Newspapers LATEST##
                if self.vendor_id is None and self.is_free is False:

                    # Additional default filter, to show the latest date newspapers only
                    # Start
                    one_week_duration = datetime.utcnow() - timedelta(days=7)
                    latest_ids = db.session.query(func.max(Item.id))\
                        .filter(Item.release_date > one_week_duration, Item.item_type == ITEM_TYPES[self.item_type_id])\
                        .group_by(Item.brand_id).all()

                    # latest_ids = db.session.query(func.max(Item.id))\
                    #     .filter(Item.item_type == ITEM_TYPES[self.item_type_id])\
                    #     .group_by(Item.brand_id).all()

                    latest_ids = [id[0] for id in latest_ids]
                    self.master = self.master.filter(Item.id.in_(latest_ids))

                self.master = self.master.order_by(desc(Item.release_date))\
                    .order_by(desc(Item.sort_priority))\
                    .order_by(desc(Item.created))

            elif int(self.item_type_id) == 2: # BOOK LATEST ISSUE : SORT PRIORITY
                self.master = self.master.order_by(desc(
                    Item.sort_priority)).order_by(desc(Item.created))

            else:
                self.master = self.master.order_by(desc(Item.created))

        if self.item_type_id:
            self.master = self.master.filter_by(item_type=ITEM_TYPES[self.item_type_id])

        filter_parental_control_id = get_filter_parental_control(self.parental_control_id)
        if filter_parental_control_id:

            # All Ages
            if filter_parental_control_id == ParentalControlType.parental_level_1.value:
                self.master = self.master.filter(Item.parentalcontrol_id.in_([
                    ParentalControlType.parental_level_1.value,
                    ParentalControlType.parental_level_3.value,
                    ParentalControlType.parental_level_4.value,
                    ParentalControlType.parental_level_5.value,
                    ParentalControlType.parental_level_6.value]
                ))

            # +3 old
            if filter_parental_control_id == ParentalControlType.parental_level_3.value:
                self.master = self.master.filter(Item.parentalcontrol_id.in_([
                    #ParentalControlType.parental_level_1.value,
                    ParentalControlType.parental_level_3.value]
                ))

            # +7 old
            if filter_parental_control_id == ParentalControlType.parental_level_4.value:
                self.master = self.master.filter(Item.parentalcontrol_id.in_([
                    #ParentalControlType.parental_level_1.value,
                    ParentalControlType.parental_level_3.value,
                    ParentalControlType.parental_level_4.value]
                ))

            # +12 old
            if filter_parental_control_id == ParentalControlType.parental_level_5.value:
                self.master = self.master.filter(Item.parentalcontrol_id.in_(
                    #ParentalControlType.parental_level_1.value,
                    ParentalControlType.parental_level_3.value,
                    ParentalControlType.parental_level_4.value,
                    ParentalControlType.parental_level_5.value
                ))

            # +16 old
            if filter_parental_control_id == ParentalControlType.parental_level_6.value:
                self.master = self.master.filter(Item.parentalcontrol_id.in_(
                    #ParentalControlType.parental_level_1.value,
                    ParentalControlType.parental_level_3.value,
                    ParentalControlType.parental_level_4.value,
                    ParentalControlType.parental_level_5.value,
                    ParentalControlType.parental_level_6.value
                ))

            # +18 old
            if filter_parental_control_id == ParentalControlType.parental_level_2.value:
                self.master = self.master.filter_by(parentalcontrol_id=filter_parental_control_id)

            # self.master = self.master.filter_by(parentalcontrol_id=filter_parental_control_id)

        if self.category_id:
            category_id = self.category_id.split(',')
            for idx in category_id:
                self.master = self.master.filter(Item.categories.any(id=int(idx)))

        if self.brand_id:

            if self.module == 'RECOMMENDATION':
                xo = []
                for brands in self.brand_id:
                    item = Item.query.filter_by(brand_id=brands).order_by(desc(Item.release_date)).first()
                    xo.append(str(item.id))
                self.item_id = ','.join(xo)

            elif self.module in ['LATEST', 'SEARCH']:
                brand_id = self.brand_id.split(',')
                self.master = self.master.filter(Item.brand_id.in_(brand_id))

            else:
                self.master = self.master.filter_by(brand_id=self.brand_id)

        if self.language_id:
            # post version: TODO: note version number
            # countries with integer ids have been denormalized into
            # array fields in our database.
            language_codes = [languages.id_to_iso639_alpha3(int(lang_id))
                              for lang_id
                              in self.language_id.split(',')]

            for lang_code in language_codes:
                self.master = \
                    self.master.filter(Item.languages.any(lang_code))

            # next to be delivered feature by @kiratakada
            # self.master = self.master.join(Language.core_items).filter(Language.id.in_(language_id))

        if self.country_id:
            country_codes = [countries.id_to_iso3166(int(country_id)) for country_id in self.country_id.split(',')]
            for country_code in country_codes:
                self.master = self.master.filter(Item.countries.any(country_code))
            # next to be delivered feature by @kiratakada
            # self.master = self.master.join(Country.core_items).filter(Country.id.in_(country_id))

        dist_country_filter = get_dist_country_filter()
        if dist_country_filter is not None:
            # cannot use: "if dist_country_filter:" error: Boolean value of this clause is not defined
            self.master = self.master.filter(dist_country_filter)

        if self.author_id:
            author_id = self.author_id.split(',')
            for idx in author_id:
                self.master = self.master.filter(Item.authors.any(id=int(idx)))
            # next to be delivered feature by @kiratakada
            # self.master = self.master.join(Author.core_items).filter(Author.id.in_(author_id))

        if self.item_id:
            if self.module == 'EDITIONS':
                self.master = self.master.filter_by(id=self.item_id)
            else:
                item_id = self.item_id.split(',')
                self.master = self.master.filter(Item.id.in_(item_id))

        if self.q2:
            self.q2 = "%s%s%s" % ('%', self.q2, '%')
            self.master = self.master.filter(Item.name.ilike(self.q2))

        if self.q:
            # check if the user has provided their query request in a
            # special format that indicates they want to query directly
            # against a field
            allowed_fields = {'title': 'item_name',
                              'author': 'author_name',
                              'publisher': 'vendor_name',
                              # 'isbn', -- maybe later.  massive number
                              # of joins required to make this work proper
                              'text': 'text',
                              }
            m = re.search("^(?P<search_field>\w+):\s?(?P<query_term>.+)$",
                          self.q,
                          re.IGNORECASE)

            # determine what field our search is going to run against,
            # and what the query text is going to be.
            if m is None:
                search_field = 'text'
                query_term = self.q
            else:
                search_field = m.groupdict().get('search_field', None).lower()
                if search_field in allowed_fields:
                    search_field = allowed_fields[search_field]
                    query_term = m.groupdict().get('query_term', '')
                else:
                    search_field = 'text'
                    query_term = self.q

            # for the time being, because the ordering coming from
            # solr is being ignored by our later logic, we're going
            # to need to default to an 'ALL' query
            # apparently, the mm and default operator are no longer
            # respected in 4.10.x of solr, so we'll do this manually.
            # NOTE: 'AND' (all caps) has special meaning within solr.
            query_term = " AND ".join(query_term.split())

            # lastly, we need to escape *, ?, and some other stuff
            # this is more complicated because some lucene special
            # characters are regex special chars.
            lucene_special_chars = [
                '+', '-', '!', '(', ')', '{',
                '}', '[', ']', '^', '"', '~', '*', '?',
                ':', "'",
            ]
            re_special_chars = ['.', '^', '$', '*', '+', '?', '(', ')', '{',
                                '}', '[', ']']
            for lcp in lucene_special_chars:
                lcp = lcp if lcp not in re_special_chars \
                    else r"\{0}".format(lcp)
                query_term = re.sub(lcp, '\\' + lcp, query_term)

            filter_active = " AND item_is_active:{}".format(self.is_active) if self.is_active is not None else ""

            # rows will default to 10.
            # since we're still using this custom_get logic, we're going
            # to place 'rows' at an arbitrarily large number
            s = solr.Solr(app.config['SOLR_URL'])
            response = s.select(
                u'{search_field}:{query}{filter_active}'.format(
                    search_field=search_field,
                    query=query_term,
                    filter_active=filter_active),
                start=0,
                rows=100000,
                fields=['id'])
            ids = [d['id'] for d in response]
            if not ids:
                response = s.select(
                    u'{search_field}:{query}{filter_active}'.format(
                        search_field=search_field,
                        query='{}*'.format(query_term),
                        filter_active=filter_active),
                    start=0,
                    rows=100000,
                    fields=['id'])
                ids = [d['id'] for d in response]
            if ids:
                self.master = self.master.filter(Item.id.in_(ids))
            else:
                # no data found in solr for the given keyword, return None
                self.master = self.master.filter(Item.id == 0)
        else:
            if self.is_active is not None:
                self.master = self.master.filter_by(is_active=self.is_active)

        if self.item_status:

            # DONT REMOVE, HOT FIX FOR STEVE JOBS IOS ISSUE, v1/<id>?items_status=9
            if self.module == 'ITEMS_DETAILS':
                self.master = self.master
            else:
                item_status = self.item_status.split(',')
                related_item_status = [STATUS_TYPES[int(i)] for i in item_status]
                self.master = self.master.filter(Item.item_status.in_(related_item_status))

        if self.release_date_lt:
            self.master = self.master.filter(Item.release_date < self.release_date_lt)

        if self.release_date_lte:
            self.master = self.master.filter(Item.release_date <= self.release_date_lte)

        if self.release_date_gt:
            self.master = self.master.filter(Item.release_date > self.release_date_gt)

        if self.release_date_gte:
            self.master = self.master.filter(Item.release_date >= self.release_date_gte)

        if self.created_lt:
            self.master = self.master.filter(Item.created < self.created_lt)

        if self.created_lte:
            self.master = self.master.filter(Item.created <= self.created_lte)

        if self.created_gt:
            self.master = self.master.filter(Item.created > self.created_gt)

        if self.created_gte:
            self.master = self.master.filter(Item.created >= self.created_gte)

        #    SEO SEARCH FOR BRYAN GETSCOOP
        if self.modified_lt:
            self.master = self.master.filter(Item.modified < self.modified_lt)

        if self.modified_lte:
            self.master = self.master.filter(Item.modified <= self.modified_lte)

        if self.modified_gt:
            self.master = self.master.filter(Item.modified > self.modified_gt)

        if self.modified_gte:
            self.master = self.master.filter(Item.modified >= self.modified_gte)

        if self.vendor_id:
            data = self.master.join(Brand)
            #self.master = data.filter_by(vendor_id=self.vendor_id)

            vendor_ids = self.vendor_id.split(',')
            self.master = data.filter(Brand.vendor_id.in_(vendor_ids))

        if self.item_id__notin:
            self.master = self.master.filter(~Item.id.in_(self.item_id__notin))

        #if item_type id are magazine or newspaper
        if int(self.item_type_id) in [1, 3] and self.module == 'POPULAR':
            self.master = self.master.filter(Item.later_item_count == 0)

            if int(self.item_type_id) == 3:
                one_week_duration = datetime.utcnow() - timedelta(days=7)
                latest_ids = db.session.query(func.max(Item.id))\
                    .filter(Item.release_date > one_week_duration, Item.item_type == ITEM_TYPES[self.item_type_id])\
                    .group_by(Item.brand_id).all()

                latest_ids = [id[0] for id in latest_ids]
                self.master = self.master.filter(Item.id.in_(latest_ids))

        self.total = self.master.count()

        if self.module == 'LATEST':

            #if request has done 7 times do normal request
            if self.offset/self.limit >= app.config['MAXIMUM_PAGE'] or self.offset % self.limit != 0:
                offset = self.offset
                limit = self.limit
            else:
                offset = 0
                limit = app.config['MAXIMUM_PAGE'] * self.limit

        else:
            offset = self.offset
            limit = self.limit

        self.master = self.master.limit(limit).offset(offset).all()

        if self.module == 'RECOMMENDATION':
            self.total = len(self.master)

        return self.json_success(self.master, self.offset, self.limit)


class Suggestions(object):

    def __init__(self, param=None, module=None):
        self.module = module

        self.q = param.get('q', None)
        self.brand_id = param.get('brand_id', None)
        self.vendor_id = param.get('vendor_id', None)
        self.limit = param.get('limit', 20)
        self.offset = param.get('offset', 0)
        self.fields = param.get('fields', None)

        self.is_active = param.get('is_active', None)

        self.total = 0

    def construct(self):
        """ Suggestion search flow
        brands startwith (huj*)
        if result less then limit data: search again: authors startwith (huj*)
        if result less then limit data: search again: vendors startwith (huj*)
        if result less then limit data: search again: brands contain (*huj*)
        if result less then limit data: search again: authors contain (*huj*)
        if result less then limit data: search again: vendors contain (*huj*)
        :return:
        """
        data = self.search_data(data=[], q_param=self.q, model=Brand)
        if len(data) < self.limit and not self.vendor_id and not self.brand_id:
            # no data found, or less then limit
            #    exclude white label search (brand_id , vendor_id)
            #  next, search author for suggestion result
            data = self.search_data(data, q_param=self.q, model=Author, limit=self.limit-len(data))
            if len(data) < self.limit:
                # not found or less then limit, next, search by vendor
                data = self.search_data(data, q_param=self.q, model=Vendor, limit=self.limit-len(data))
            if len(data) < self.limit:
                # next, search by brand with name contains text search
                data = self.search_data(data, q_param='%{}'.format(self.q), model=Brand, limit=self.limit-len(data))
            if len(data) < self.limit:
                # next, search by author with name contains text search
                data = self.search_data(data, q_param='%{}'.format(self.q), model=Author, limit=self.limit-len(data))
            if len(data) < self.limit:
                # next, search by vendor
                data = self.search_data(data, q_param='%{}'.format(self.q), model=Vendor, limit=self.limit-len(data))

        self.total = len(data)
        return self.json_success(data, self.offset, self.limit)

    def search_data(self, data, q_param, model, limit=None):
        if not isinstance(data, list):
            data = []
        master = db.session.query(model)
        search_limit = limit or self.limit
        if q_param:
            q_param = "%s%s" % (q_param, '%')
            master = master.filter(model.name.ilike(q_param))

        # For Whitelabel
        if self.vendor_id:
            master = master.filter_by(vendor_id=int(self.vendor_id))
        if self.brand_id:
            brand_id = self.brand_id.split(',')
            master = master.filter(model.id.in_(brand_id))
        if self.is_active:
            master = master.filter_by(is_active=self.is_active)

        # DEFAULT ORDER BY NAME #
        master = master.order_by(model.name)
        entities = master.limit(search_limit).offset(self.offset).all()
        for e in entities:
            # add new search result to existing data, remove duplicate value
            #  do not use list(set(data)), it will messed up the previous ordering
            if e.name not in data:
                data.append(e.name)
        return data

    def json_success(self, data_items=None, offset=None, limit=None):

        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            resultset = {
                "resultset": {
                    "count": self.total,
                    "offset": int(offset) if offset else 0,
                    "limit": int(limit) if limit else 0
                }
            }

            rv = json.dumps({self.module: data_items, "metadata": resultset})
            return Response(rv, status=httplib.OK, mimetype='application/json')


class GetItemFiles(object):

    def __init__(self, param=None, module=None):
        self.module = module

        self.q = param.get('q', None)
        self.limit = param.get('limit', 20)
        self.offset = param.get('offset', 0)
        self.fields = param.get('fields', None)

        self.item_id = param.get('item_id', None)
        self.order = param.get('order', None)

        self.is_active = param.get('is_active', None)

        self.master = ''
        self.total = 0

    def json_success(self, data_items=None, offset=None, limit=None):

        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            if self.fields:
                cur_values = [u.custom_values(self.fields) for u in data_items]
            else:
                cur_values = [u.values() for u in data_items]

            temp = {}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}

            rv = json.dumps({self.module: cur_values, "metadata": tempx})
            return Response(rv, status=httplib.OK, mimetype='application/json')

    def item_files_master(self):
        return ItemFile.query

    def construct(self):
        self.master = self.item_files_master()

        if self.q:
            self.q = "%s%s%s" % ('%', self.q, '%')
            self.master = self.master.filter(ItemFile.file_name.ilike(self.q))

        if self.item_id:
            item_id = self.item_id.split(',')
            self.master = self.master.filter(ItemFile.item_id.in_(item_id))

        if self.order:
            order_data = self.order.split(',')
            for orderx in order_data:
                if orderx[0] == '-': #    descending
                    order_param = '%s %s' % (orderx[1:], 'desc')
                    self.master = self.master.order_by(order_param)
                else:
                    self.master = self.master.order_by(orderx)

        if self.is_active:
            self.master = self.master.filter_by(is_active=self.is_active)

        self.master = self.master.limit(self.limit).offset(self.offset).all()
        self.total = len(self.master)

        return self.json_success(self.master, self.offset, self.limit)


class GetItemOwned(object):

    def __init__(self, param=None, module=None):
        self.module = module

        self.q = param.get('q', None)
        self.limit = param.get('limit', 20)
        self.offset = param.get('offset', 0)
        self.fields = param.get('fields', None)

        self.item_id = param.get('item_id', None)
        self.remote_item_id = param.get('remote_item_id', None)
        self.user_id = param.get('user_id', None)
        self.order = param.get('order', None)
        self.extend = param.get('expand', None)
        self.subs_id = param.get('subs_id', None)
        self.brand_id = param.get('brand_id', None)
        self.vendor_id = param.get('vendor_id', None)
        self.itemtype_id = param.get('itemtype_id', None)
        self.include_buffet_id = param.get('include_buffet_id', None)

        self.is_active = param.get('is_active', False)

        self.created_lt = param.get('created__lt', None)
        self.created_lte = param.get('created__lte', None)
        self.created_gt = param.get('created__gt', None)
        self.created_gte = param.get('created__gte', None)

        self.modified_lt = param.get('modified__lt', None)
        self.modified_lte = param.get('modified__lte', None)
        self.modified_gt = param.get('modified__gt', None)
        self.modified_gte = param.get('modified__gte', None)

        self.master = ''
        self.total = 0

    def json_success(self, data_items=None, offset=None, limit=None):

        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            if self.fields:
                cur_values = [u.custom_values(self.fields) for u in data_items]
            elif self.extend:
                extends = self.extend.split(',')
                cur_values = [u.extension_values(extends) for u in data_items]
            else:
                cur_values = [u.values() for u in data_items]

            temp = {}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}

            if self.module == 'owned_subscriptions_details':
                rv = json.dumps(cur_values[0])
            else:
                rv = json.dumps({self.module: cur_values, "metadata": tempx})

            return Response(rv, status=httplib.OK, mimetype='application/json')

    def item_files_master(self):
        return UserItem.query.filter(UserItem.user_buffet_id == None)

    def item_subs_master(self):
        return UserSubscription.query

    def construct(self):
        if self.module == 'owned_items':
            self.master = self.item_files_master()

            if self.q:
                self.q = "%s%s%s" % ('%', self.q, '%')
                self.master = self.master.filter(
                    UserItem.edition_code.ilike(self.q))

            if self.brand_id:
                brands = self.brand_id.split(',')
                self.master = self.master.filter(UserItem.brand_id.in_(brands))

            if self.vendor_id:
                vendors = self.vendor_id.split(',')
                self.master = self.master.filter(UserItem.vendor_id.in_(vendors))

            # NOTE: this stays as it is.  We did not update UserItems
            if self.itemtype_id:
                self.master = self.master.filter_by(itemtype_id=self.itemtype_id)

            if self.created_lt:
                self.master = self.master.filter(UserItem.created < self.created_lt)

            if self.created_lte:
                self.master = self.master.filter(UserItem.created <= self.created_lte)

            if self.created_gt:
                self.master = self.master.filter(UserItem.created > self.created_gt)

            if self.created_gte:
                self.master = self.master.filter(UserItem.created >= self.created_gte)

            if self.modified_lt:
                self.master = self.master.filter(UserItem.modified < self.modified_lt)

            if self.modified_lte:
                self.master = self.master.filter(UserItem.modified <= self.modified_lte)

            if self.modified_gt:
                self.master = self.master.filter(UserItem.modified > self.modified_gt)

            if self.modified_gte:
                self.master = self.master.filter(UserItem.modified >= self.modified_gte)

        if self.module in ['owned_subscriptions', 'owned_subscriptions_details']:
            self.master = self.item_subs_master()

            if self.subs_id:
                self.master = self.master.filter_by(id=self.subs_id)

            if self.q:
                self.q = "%s%s%s" % ('%', self.q, '%')
                self.master = self.master.filter(
                    UserSubscription.subscription_code.ilike(self.q))

            if self.brand_id:
                brands = self.brand_id.split(',')
                self.master = self.master.filter(UserSubscription.brand_id.in_(brands))

            if self.created_lt:
                self.master = self.master.filter(UserSubscription.created < self.created_lt)

            if self.created_lte:
                self.master = self.master.filter(UserSubscription.created <= self.created_lte)

            if self.created_gt:
                self.master = self.master.filter(UserSubscription.created > self.created_gt)

            if self.created_gte:
                self.master = self.master.filter(UserSubscription.created >= self.created_gte)

            if self.modified_lt:
                self.master = self.master.filter(UserSubscription.modified < self.modified_lt)

            if self.modified_lte:
                self.master = self.master.filter(UserSubscription.modified <= self.modified_lte)

            if self.modified_gt:
                self.master = self.master.filter(UserSubscription.modified > self.modified_gt)

            if self.modified_gte:
                self.master = self.master.filter(UserSubscription.modified >= self.modified_gte)

        if self.remote_item_id:
            # used by remote service to check their remote checkout process
            # search by remote item id, exampled: SC00P123 (123 is offer id)
            list_of_offers = self.remote_item_id.split(',')
            item_ids = []
            for o in list_of_offers:
                offer_id = int(o.replace('SC00P', ''))
                offer = Offer.query.get(offer_id)
                if offer and offer.offer_type.id == SINGLE:
                    for item in offer.items:
                        item_ids.append(item.id)

            if len(item_ids) > 0:
                self.master = self.master.filter(UserItem.item_id.in_(item_ids))
            else:
                # list of item not found when searched by offer id, show no user items (return 204)
                self.master = self.master.filter(UserItem.item_id == -1)

        if self.item_id:
            item_id = self.item_id.split(',')
            self.master = self.master.filter(UserItem.item_id.in_(item_id))

        if self.user_id:
            user_id = self.user_id.split(',')
            self.master = self.master.filter(UserItem.user_id.in_(user_id))

        if self.order:
            order_data = self.order.split(',')
            for orderx in order_data:
                if orderx[0] == '-': #    descending
                    order_param = '%s %s' % (orderx[1:], 'desc')
                    self.master = self.master.order_by(order_param)
                else:
                    self.master = self.master.order_by(orderx)

        if self.is_active:
            self.master = self.master.filter_by(is_active=self.is_active)

        self.total = self.master.count()
        self.master = self.master.limit(self.limit).offset(self.offset).all()

        return self.json_success(self.master, self.offset, self.limit)


class DistributionCountryGroupGet():

    def __init__(self, param=None, module=None, method=None):

        self.limit = param.get('limit', 20)
        self.offset = param.get('offset', 0)
        self.fields = param.get('fields', None)

        self.q = param.get('q', None)
        self.is_active = param.get('is_active', False)
        self.vendor_id = param.get('vendor_id', None)
        self.group_type = param.get('group_type', None)

        self.module = module
        self.method = method
        self.master = ''
        self.total = 0

    def json_success(self, data_items=None, offset=None, limit=None):

        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            if self.fields:
                cur_values = [u.custom_values(self.fields) for u in data_items]
            else:
                cur_values = [u.values() for u in data_items]

            temp={}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}
            rv = json.dumps({self.module: cur_values, "metadata": tempx})
            return Response(rv, status=httplib.OK, mimetype='application/json')

    def distribution_group(self):
        self.total = DistributionCountryGroup.query.count()
        return DistributionCountryGroup.query

    def construct(self):
        self.master = self.distribution_group()

        if self.q:
            self.q = "%s%s%s" % ('%', self.q, '%')
            self.master = self.master.filter(DistributionCountryGroup.name.ilike(self.q))

        if self.is_active:
            self.master = self.master.filter_by(is_active=self.is_active)

        if self.vendor_id:
            self.master = self.master.filter_by(vendor_id=self.vendor_id)

        if self.group_type:
            self.master = self.master.filter_by(group_type=self.group_type)

        self.total = self.master.count()
        self.master = self.master.limit(self.limit).offset(self.offset).all()
        return self.json_success(self.master, self.offset, self.limit)
