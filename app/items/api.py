import os, base64, httplib, io, json, requests, boto3, iso3166, iso639

from abc import abstractmethod
from datetime import timedelta
from hashlib import md5

from botocore.exceptions import ClientError
from flask import Response, request, jsonify, g, make_response, current_app, render_template, url_for
from flask.views import MethodView
from flask_appsfoundry import marshal, controllers
from flask_appsfoundry.exceptions import NotFound, Conflict, UnprocessableEntity, Forbidden, NotAcceptable, Unauthorized
from flask_appsfoundry.parsers import reqparse, SqlAlchemyFilterParser, filterinputs as fi
from humanize import naturalsize
from marshmallow.utils import isoformat
from six import text_type
from sqlalchemy import desc, func, or_
from werkzeug.routing import BaseConverter

from app import app, db
# from app.analytics.helpers import insert_user_item_view_data
from app.auth.decorators import token_required, user_has_any_tokens, user_is_authenticated, user_is_superuser
from app.caching import generate_redis_key
from app.constants import MIME_TYPE_V3_JSON
from app.helpers import (
    conflict_message, bad_request_message, internal_server_message, err_response, date_validator,
    request_is_expectation_check, slugify,
    purge_redis_cache, portal_metadata_changes)
from app.items.choices import STATUS_READY_FOR_CONSUME, ITEM_TYPES, NEWSPAPER, FILE_TYPES, AUDIOBOOK, FILE_AUDIO
from app.items.choices import STATUS_TYPES
from app.items.helpers import construct_item_download_response, \
    file_type_shim, ItemQueryBuilder, get_dist_country_filter
from app.items.models import Item, Review
from app.items.parsers import FileItemUploadToServerParser
from app.items.previews import preview_rule_factory
from app.items.previews import preview_s3_covers, preview_s3_urls
from app.items.viewmodels import ItemV3ViewModels
from app.master.models import Brand, Category, Vendor, StudentSave
from app.offers.models import OfferPlatform
from app.parental_controls.choices import ParentalControlType, StudentSaveControlType
from app.services.solr import SolrGateway
from app.uploads.helpers import contruct_sc1_response
from app.users.users import User
from app.utils.controllers import CorListControllerMixin
from app.utils.datetimes import get_local
from app.utils.decorators import model_from_url
from app.utils.dicts import CorDefaultDict
from app.utils.http import get_offset_limit
from app.utils.redis_cache import RedisCache
from app.utils.responses import get_response
from app.utils.shims import countries
from app.utils.shims import item_types
from app.utils.shims import languages
from . import (
    _log, helpers, models, parsers, serializers, choices, custom_get, viewmodels, web_reader, thumbnails, exceptions
)
from .exceptions import BadItemTypeChangeException
from .file_server_gateway import AmazonS3ServerGateway, InternalFileServerGateway
from .migration_subs import MigrateSubs, LinkSubs


class ItemConverter(BaseConverter):

    def to_python(self, value):
        session = db.session()
        entity = session.query(models.Item).get(value)
        if not entity:
            raise NotFound
        return entity

    def to_url(self, value):
        return text_type(value.id)


app.url_map.converters['item'] = ItemConverter


class AuthorListApi(controllers.ListCreateApiResource):
    """
    Controller for fetching lists of Authors, and creating new Author.
    """
    query_set = models.Author.query
    list_request_parser = parsers.AuthorsListArgsParser
    create_request_parser = parsers.AuthorsArgsParser
    serialized_list_name = 'authors'
    response_serializer = serializers.AuthorSerializer
    get_security_tokens = ('can_read_write_global_all', 'can_read_write_public', 'can_read_global_author',)
    post_security_tokens = ('can_read_write_global_all', 'can_read_write_global_author',)

    def _before_insert(self, model, req_data):
        if not req_data['slug']:
            model.slug = slugify(req_data['name'])


class AuthorApi(controllers.DetailApiResource):
    """
    perform actions on an individual author instance
    """
    query_set = models.Author.query
    update_request_parser = parsers.AuthorsArgsParser
    response_serializer = serializers.AuthorSerializer
    get_security_tokens = ('can_read_write_global_all', 'can_read_write_public', 'can_read_global_author',)
    put_security_tokens = ('can_read_write_global_all', 'can_read_write_global_author',)
    delete_security_tokens = ('can_read_write_global_all',)


class ItemListApi(controllers.ErrorHandlingApiMixin, controllers.Resource):

    @token_required(
        'can_read_write_global_all, '
        'can_read_write_public, '
        'can_read_write_public_ext,'
        'can_read_global_item, '
        'can_read_associated_item, '
        'can_read_associated_magazine,'
        'can_read_associated_book, '
        'can_read_associated_newspaper')
    def get(self):
        if MIME_TYPE_V3_JSON in request.headers.get('accept', ""):
            return self._get_version_3()
        elif request.path == '/v2/items/searches':
            return self._get_version_2()
        else:
            return self._get_version_1()

    def _get_version_1(self):
        scene = custom_get.CustomGet(request.args.to_dict(), 'ITEMS').filter_custom()
        # THIS CODE MAKE ANALITYC SLOW & FUCKUP THE ANALITYC SERVER LINES, DO NOT TRY OPENED THIS CODE - KIRA
        # items = json.loads(scene.data) if scene.data else None
        # if items and request.args.get('item_id') and len(items.get('items')) == 1:
        #     insert_user_item_view_data(
        #         user_id=g.current_user.id if g.current_user else None,
        #         item_id=items.get('items')[0].get('id'),
        #         client_id=g.current_client.id if g.current_client else None,
        #         log=_log,
        #     )

        return scene

    def _get_version_2(self):
        try:
            resp = requests.get(u"{solr_url}/select?{query}&wt=json".format(
                solr_url=current_app.config['SOLR_URL'],
                query=request.query_string))

            solr_data = resp.json()
            # THIS CODE MAKE ANALITYC SLOW & FUCKUP THE ANALITYC SERVER LINES, DO NOT TRY OPENED THIS CODE - KIRA
            # session = db.session()
            # try:
            #     activity = helpers.save_user_search_activity(
            #         user_id=g.user.user_id,
            #         query_text=request.args.get('q'),
            #         query_results=solr_data.get('response', {}).get('numFound')
            #                       or solr_data.get('grouped', [{}])
            #                           .get(request.args.get('group.field'))
            #                           .get('matches'),
            #         query_facets=request.args.getlist('facet.field'))
            # except Exception as e:
            #     # purpously swallow all exceptions here, because we don't want
            #     # a logging failure to crash the app.
            #     app.logger.exception("Error recording analytics data for user search on /v2/items/searches")

            return solr_data

        except requests.exceptions.RequestException as e:
            raise exceptions.SolrError(inner_exception=e)

    def _get_version_3(self):
        solr_gateway = SolrGateway(current_app.config['SOLR_BASE_URL'], 'scoop-content', requests)
        item_query = ItemQueryBuilder(
            flask_req=request,
            facet_list=["author_names", "category_names", "languages", ]
        )
        response = solr_gateway.search(item_query.build())

        facet_name_mapper = CorDefaultDict(category_names='categories', author_names='authors')

        facet_items = response['facet_counts']['facet_fields'].items() if 'facet_counts' in response \
            else []
        facet_values = []
        if facet_items:
            facet_values = [{"field_name": facet_name_mapper[key], "values": [
                {"value": t[0], "count": t[1]} for t in zip(facet[0::2], facet[1::2])
            ]
                             } for key, facet in facet_items
                            ]

        spell_check = []
        if response.get('spellcheck', {'correctlySpelled': True})['correctlySpelled'] == 'false':
            suggestions = response['spellcheck']['suggestions'][1]['suggestion']
            spell_check = [{"value": data['word'], "count": data['freq']} for data in suggestions]

        pre_processing = ItemV3ViewModels.item_brand_response_builder(response)

        rebuild = {
            "items": pre_processing,
            'metadata': {
                'resultset': {
                    'offset': request.args.get('offset', type=int, default=0),
                    'limit': request.args.get('limit', type=int, default=20),
                    'count': response.get('response', {}).get('numFound', {})
                },
                "facets": facet_values,
                "spelling_suggestions": spell_check
            }
        }

        return jsonify(rebuild)

    @token_required(
        'can_read_write_global_all,'
        'can_read_write_associated_item,'
        'can_read_write_associated_magazine,'
        'can_read_write_associated_book, '
        'can_read_write_global_item,'
        'can_read_write_associated_newspaper,'
        'can_view_global_external_admin_panel')
    def post(self):
        """ Creates a new Item and returns it's dictionary representation.

        :returns: A tuple containing the response data, and the response http
            status code.
        :rtype: `tuple` (`dict`, `int`)
        """
        session = db.session()

        parser = parsers.ItemArgsParser()
        args = parser.parse_args()

        try:
            post_name = args.get('name', 'kiratakada')
            item_type = args.get('item_type', 'kiratakada')

            # validate newpaper only 1 item per 1 day.
            if item_type == 'newspaper':
                exist_item = Item.query.filter_by(name=post_name).first()
                if exist_item:
                    return make_response(json.dumps(
                        {
                            "status": 409,
                            "developer_message": "{} already exist".format(post_name),
                            "error_code": 409101,
                            "user_message": "{} already exist!".format(post_name)}
                    ), httplib.CONFLICT, {'Content-type': 'application/json'})

            item = helpers.build_item_from_request(args, session)
            if item.item_distribution_country_group_id == 0:
                item.item_distribution_country_group_id = None

            # ADD Student save..
            parental_level = args.get('parentalcontrol_id', ParentalControlType.parental_level_1.value)
            new_st_lv = StudentSave.query
            if parental_level == ParentalControlType.parental_level_2.value:
                student_level = new_st_lv.filter_by(id=StudentSaveControlType.student_save_level_6.value).first()
            else:
                student_level = new_st_lv.filter_by(id=StudentSaveControlType.student_save_level_1.value).first()

            item.student_save.append(student_level)
            session.add(item)
            session.commit()

            # THIS COMMENT OUT HANDLE FROM UPLOADERS
            # can this go yet or not?
            if args.get('item_status') not in [
                choices.STATUS_TYPES[choices.STATUS_NEW],
                choices.STATUS_TYPES[choices.STATUS_READY_FOR_UPLOAD],
                choices.STATUS_TYPES[choices.STATUS_UPLOADED]]:
                helpers.record_item_processing(item.id, item.brand_id)

            args['release_schedule'] = args['release_schedule'].isoformat() if args['release_schedule'] else None
            args['release_date'] = args['release_date'].isoformat() if args['release_schedule'] else None
            portal_metadata_changes(user_id=g.current_user.id, data_id=item.id, data_url=request.base_url,
                                    data_method=request.method, data_request=args)

            return item.values(), httplib.CREATED

        except Exception:
            _log.exception("Failed to create Item")
            session.rollback()
            raise

    def _handle_SolrError(self, exception):
        return {
                   "status": httplib.BAD_REQUEST,
                   "error_code": httplib.BAD_REQUEST,
                   "user_message": exception.get('description', str(exception)),
                   "developer_message": exception.get('developer_message', str(exception))
               }, httplib.INTERNAL_SERVER_ERROR


class ItemApi(controllers.Resource, controllers.ErrorHandlingApiMixin):

    @token_required(
        'can_read_write_global_all, '
        'can_read_write_public, '
        'can_read_write_public_ext,'
        'can_read_global_item, '
        'can_read_associated_item, '
        'can_read_associated_magazine,'
        'can_read_associated_book, '
        'can_read_associated_newspaper')
    def get(self, item_id):
        m = models.Item.query.get_or_404(item_id)
        session = db.session()

        if MIME_TYPE_V3_JSON in request.headers.get('accept', ""):

            languages, countries = None, None
            if m.languages:
                languages = ", ".join([str(iso639.find(i)['name']) for i in m.languages])

            if m.countries:
                countries = ", ".join([str(iso3166.countries.get(i)[0]) for i in m.countries])

            if m.item_type == 'book':
                sub_title = ", ".join([author.name for author in m.authors])
            else:
                sub_title = text_type(m.issue_number)

            eperpus = request.args.get('eperpus')
            if eperpus and eperpus.lower() in ('1', 'true'):
                # for eperpus apps, don't need to send offers info --> faster response for this API
                offers_resp = []
            else:
                all_offers = []
                all_offers.extend(m.core_offers.all())
                all_offers.extend(m.brand.core_offers.all())
                # all_offers.extend(m.brand.vendor.offers.all())

                offers_resp = []
                g_platform_id = g.current_client.platform_id if getattr(g, 'current_client', None) else None

                for offer in all_offers:
                    if offer.id not in [o.get('id') for o in offers_resp]:
                        displayed_discount = offer.display_discounts(idiscounts=offer.discount_id,
                                                                     values={},
                                                                     platform_id=g_platform_id,
                                                                     offer=offer) if g_platform_id else None
                        implicit_href_discount = None if not displayed_discount else {
                            'title': displayed_discount.get('discount_name', None),
                            'id': displayed_discount.get('discount_id', None)[0],
                            'href': url_for('discounts.discounts',
                                            discount_id=displayed_discount.get('discount_id', None)[0],
                                            _external=True)
                        }
                        offer_platform = session.query(OfferPlatform).filter_by(
                            offer_id=offer.id,
                            platform_id=g_platform_id).first()
                        offers_resp.append({
                            "id": offer.id,
                            "href": url_for('offers.offers', offer_id=offer.id, _external=True),
                            "title": offer.name,
                            "sub_title": offer.long_name,
                            'offer_type_id': offer.offer_type_id,
                            'is_free': offer.is_free,
                            'tier_id': offer_platform.tier_id if offer_platform else None,
                            'tier_code': offer_platform.tier_code if offer_platform else None,
                            'price': {
                                'idr': {'base': float(offer.price_idr),
                                        'net': float(offer.price_idr) if not displayed_discount else
                                        float(displayed_discount.get('discount_price_idr', offer.price_idr))},
                                'point': {'base': offer.price_point,
                                          'net': offer.price_point if not displayed_discount else displayed_discount.get(
                                              'discount_price_point', offer.price_point)},
                                'usd': {'base': float(offer.price_usd),
                                        'net': float(offer.price_usd) if not displayed_discount else
                                        float(displayed_discount.get('discount_price_usd', offer.price_usd))},
                            },
                            'discount': implicit_href_discount
                        })

            gtin14 = m.gtin14 if m.item_type == Item.Types.book.value else ''

            return jsonify({
                "id": m.id,
                # "previews": PreviewUrlBuilder(app.config['BASE_URL']).make_urls(m, True),
                "previews": preview_s3_urls(m),
                "title": m.name,
                "sub_title": sub_title,
                "edition": m.edition_code,
                "description": m.description,
                "offers": offers_resp,
                "content_type": m.content_type,
                "is_age_restricted": m.parentalcontrol_id == 2,
                "item_type": m.item_type,
                "edition_code": m.edition_code,
                "issue_number": m.issue_number,
                "release_date": isoformat(m.release_date),
                "slug": m.slug,
                # authors = fields.List(fields.Nested(SimpleHRefSchema))
                "category": [
                    {
                        'id': i.id,
                        'title': i.name,
                    } for i in m.categories
                ] if m.categories else None,
                "brand": {
                    "id": m.brand.id,
                    "title": m.brand.name,
                    "href": m.brand.api_url,
                    "slug": m.brand.slug,
                } if m.brand else None,
                "vendor": {
                    "id": m.brand.vendor.id,
                    "title": m.brand.vendor.name,
                    "href": m.brand.vendor.api_url,
                    "slug": m.brand.vendor.slug,
                } if m.brand and m.brand.vendor else None,
                "thumb_image_highres": preview_s3_covers(m, 'thumb_image_highres', True),
                "thumb_image_normal": preview_s3_covers(m, 'thumb_image_normal', True),
                "image_highres": preview_s3_covers(m, 'image_highres', True),
                "gtin14": gtin14,
                # "thumb_image_highres": '{}{}'.format(app.config['MEDIA_BASE_URL'], m.thumb_image_highres),
                # "thumb_image_normal": '{}{}'.format(app.config['MEDIA_BASE_URL'], m.thumb_image_normal),
                # "image_highres": '{}{}'.format(app.config['MEDIA_BASE_URL'], m.image_highres),
                "languages": languages,
                "countries": countries,
                "page_count": m.page_count,
                "file_size": naturalsize(m.file_size) if m.file_size and m.file_size > 0 else '- MB',
            })
        else:
            api_version = request.headers.get('version', '1.0')
            param = request.args.to_dict()
            param['item_id'] = str(item_id)
            param['api_version'] = api_version
            scene = custom_get.CustomGet(param, 'ITEMS_DETAILS').filter_custom()
            return scene

    @token_required(
        'can_read_write_global_all, '
        'can_read_write_associated_magazine,'
        'can_read_write_associated_book, '
        'can_read_write_global_item,'
        'can_read_write_associated_newspaper,'
        'can_view_global_external_admin_panel')
    def put(self, item_id):

        session = models.Item.query.session

        item = models.Item.query.get_or_404(item_id)

        parser = parsers.ItemArgsParser()
        args = parser.parse_args()

        try:
            if not args.get('vendor_product_id_print', None):
                args['vendor_product_id_print'] = item.vendor_product_id_print

            if item.item_type != args['item_type']:
                raise BadItemTypeChangeException("You cannot change Item Type.")

            item = helpers.build_item_from_request(args, session, item)
            if item.item_distribution_country_group_id == 0:
                item.item_distribution_country_group_id = None

            parental_level = args.get('parentalcontrol_id', ParentalControlType.parental_level_1.value)
            up_level = StudentSave.query
            if parental_level == ParentalControlType.parental_level_2.value:
                student_level = up_level.filter_by(id=StudentSaveControlType.student_save_level_6.value).first()
            else:
                student_level = up_level.filter_by(id=StudentSaveControlType.student_save_level_1.value).first()

            st_level = StudentSave.query.filter(StudentSave.id.in_([StudentSaveControlType.student_save_level_1.value,
                                                                    StudentSaveControlType.student_save_level_6.value])).all()
            for i in st_level:
                if i.id in [k.id for k in item.student_save]:
                    item.student_save.remove(i)

            item.student_save.append(student_level)
            session.add(item)

            # THIS COMMENT OUT HANDLE FROM UPLOADERS
            if args.get('item_status') not in [
                choices.STATUS_TYPES[choices.STATUS_NEW],
                choices.STATUS_TYPES[choices.STATUS_READY_FOR_UPLOAD],
                choices.STATUS_TYPES[choices.STATUS_UPLOADED]]:
                helpers.record_item_processing(item.id, item.brand_id)

            session.commit()

            _log.info("Updated Item (id={0.id})".format(item))
            args['release_schedule'] = args['release_schedule'].isoformat() if args['release_schedule'] else None
            args['release_date'] = args['release_date'].isoformat() if args['release_schedule'] else None
            portal_metadata_changes(user_id=g.current_user.id, data_id=item.id, data_url=request.base_url,
                                    data_method=request.method, data_request=args)

            return item.values(), httplib.OK

        except Exception:
            _log.exception("Failed to update "
                           "{0.__class__.__name__}: (id={0.id})".format(item))
            session.rollback()
            raise

    @token_required('can_read_write_global_all')
    def delete(self, item_id):

        item = models.Item.query.get_or_404(item_id)

        try:
            item.is_active = False
            item.query.session.commit()

            _log.info("Deleted Item (id={0.id}) !SOFT DELETE".format(item))

            return None, httplib.NO_CONTENT
        except Exception:
            _log.info("Failed to delete "
                      "{0.__class__.__name__}: (id={0.id})")

    def _handle_BadItemTypeChangeException(self, exception):
        return internal_server_message(
            modul="item",
            method=self.method,
            error="ITEM TYPES CHANGES, PLEASE CONSULT WITH YOUR ADMIN",
            req=str(self.args))


class ItemContentFileUploadNotificationApi(MethodView):
    """ Notifies COR that an Item has a ePUB/PDF content file needs processing.

    This API doesn't do the processing, it simply adds it to the
    ItemUploadProcess table (which serves as a sort of task-queue).

    This API is intended to be called by Portal only.
    """

    @user_has_any_tokens('can_read_write_global_all')
    def post(self, db_id):

        session = db.session()
        item = session.query(models.Item).get(db_id)
        if item is None:
            raise NotFound

        self.assert_new_upload_allowed(session, item)
        req_args = self.parser_factory(item).parse_args()

        # noinspection PyArgumentList
        upload_process = models.ItemUploadProcess(
            item_id=item.id,
            status=choices.ItemUploadProcessStatus.new.value,
            **req_args)
        session.add(upload_process)
        session.commit()

        purge_redis_cache('items-details-', item.id)

        response = jsonify(
            marshal(upload_process, serializers.ItemUploadStatusSerializer()))
        response.status_code = httplib.ACCEPTED

        return response

    def assert_new_upload_allowed(self, session, item):
        if session.query(models.ItemUploadProcess).filter(
            (models.ItemUploadProcess.item_id == item.id) &
            (models.ItemUploadProcess.status.notin_([
                choices.ItemUploadProcessStatus.complete.value,
                choices.ItemUploadProcessStatus.failed.value]))
        ).count():
            raise Conflict(
                user_message="PDF already uploaded.  Please wait for "
                             "processing to complete and try again")

    def parser_factory(self, item):
        """
        >>> item = models.Item(content_type='pdf')
        >>> parser = ItemContentFileUploadNotificationApi().parser_factory(item)
        >>> isinstance(parser, parsers.ItemPdfContentUploadParser)
        True

        :param `app.items.models.Item` item:
        :return:
        """
        try:
            parser_class = {
                'pdf': parsers.ItemPdfContentUploadParser,
                'epub': parsers.ItemEpubContentUploadParser
            }[item.content_type]
            return parser_class()
        except KeyError:
            raise UnprocessableEntity


class ItemContentFileDownloadApi(MethodView):
    """ Fetches item zip file via nginx masked url, for mobile devices.
    """

    @model_from_url(models.Item, argument_name="item")
    def get(self, item):
        """ Returns item zip file for an item.

        :param `models.Item` item: The item model
        :return: A Flask HttpResponse.
        """
        if not g or not getattr(g, 'user') or not getattr(g, 'current_user'):
            raise Unauthorized

        helpers.assert_user_owned_item(item, user_info=g.user)

        return construct_item_download_response(item) if \
            not request_is_expectation_check() else make_response('', 100, {})


class ItemContentFileMetaApi(MethodView):
    """ Returns metadata about the downloadable files for a given `Item`, such as it's decryption key.
    """

    @model_from_url(models.Item, argument_name="item")
    def get(self, item):
        try:
            if not g or not getattr(g, 'user') or not getattr(g, 'current_user'):
                raise Unauthorized

            helpers.assert_user_owned_item(item, user_info=g.user)

            from app.items.helpers import EncryptData
            old_style_key = EncryptData(key=item.files[0].key, item_id=item.id).construct()
            return jsonify({
                'key': item.files[0].key,
                'file_type': file_type_shim(item.files[0].file_type),
                'old_style_key': old_style_key,
            })
        except IndexError:
            raise NotFound


class PreviewPageApi(MethodView):
    """ Fetches a JPEG image of a single page at 150 DPI resolution for page preview

    """

    @model_from_url(models.Item, argument_name="item")
    def get(self, item, page_num):
        """ Returns an image data for a single page.

        :param `models.Item` item: The item model that we're showing a
            web-reader page for.
        :param `int` page_num: Page number to fetch.  This is 1-indexed.
        :return: A Flask HttpResponse.
        """
        web_reader.assert_item_type_valid(item)

        rules = preview_rule_factory(item)

        allowed_pages = rules.get_page_numbers()

        if page_num not in allowed_pages:
            raise Forbidden

        return web_reader.construct_preview_page_response(item, page_num, allowed_pages)


class WebReaderQueueStatus(MethodView):

    @user_is_superuser
    def get(self):
        from .web_reader.task_queue import WebReaderTaskJsonEncoder, ProcessingTaskManager
        from .choices import WebReaderTaskStatus
        mgr = ProcessingTaskManager(current_app.kvs)
        in_progress = [t for t in mgr.get_all_tasks() if t.status not in [
            WebReaderTaskStatus.failed,
            WebReaderTaskStatus.complete,
            WebReaderTaskStatus.pending]]
        return make_response(
            json.dumps(in_progress, cls=WebReaderTaskJsonEncoder),
            200,
            {'Content-type': 'application/json'}
        )


class WebReaderQueueExactStatus(MethodView):

    @user_is_superuser
    def get(self, status):
        from .web_reader.task_queue import WebReaderTaskJsonEncoder, ProcessingTaskManager
        from .choices import WebReaderTaskStatus
        mgr = ProcessingTaskManager(current_app.kvs)
        return make_response(
            json.dumps(mgr.get_tasks(status=WebReaderTaskStatus[status]), cls=WebReaderTaskJsonEncoder),
            200,
            {'Content-type': 'application/json'}
        )


class WebReaderPageApi(MethodView):
    """ Fetches a JPEG image of a single page at 150 DPI resolution.

    Supports a web-reading feature on getscoop.com.

    .. warning::

        **Calling this method has side-effects on the server!**

        Because of bandwidth/disk limitations on our production servers
        we cannot preprocess all of our content into web-reader format.

        When called, this API will queue a job to generate the web-reader
        files, if they do not already exist on our production servers.
    """

    @model_from_url(models.Item, argument_name="item")
    def get(self, item, page_num):
        """ Returns an image data for a single page.

        :param `models.Item` item: The item model that we're showing a
            web-reader page for.
        :param `int` page_num: Page number to fetch.  This is 1-indexed.
        :return: A Flask HttpResponse.
        """
        if not g or not getattr(g, 'user', None) or not getattr(g, 'current_user', None):
            raise Unauthorized

        param = request.args.to_dict()
        organization_id = param.get('organization_id', None)

        web_reader.assert_item_type_valid(item)
        helpers.assert_user_owned_item_for_web_reader(item=item, organization_id=organization_id)
        file_object = helpers.get_file_object(item, page_num)
        # try:
        #     web_reader.assert_web_reader_files_ready(item.id, item.brand_id, app.kvs)
        # except WebReaderFilesNotReady:
        #     task = web_reader.get_or_create_processing_job(item.id, app.kvs)
        #     web_reader.try_set_task_high_priority(task, app.kvs)
        #
        #     if g.user.username not in [u.username for u in task.notify_users]:
        #         web_reader.add_user_notification(task, g.user, app.kvs)
        #
        #     return make_response(
        #         WebReaderFileProcessing(task, item).get_body(),
        #         418,
        #         {'Content-Type': 'application/json'})
        #
        # return web_reader.construct_page_response(item, page_num) if \
        #     not request_is_expectation_check() else make_response('', 100, {})
        if file_object is not None:
            image_result = file_object['Body'].read()
            file_obj = io.BytesIO(image_result)
            file_image = helpers.watermark_image(file_obj, g.current_user.email, organization_id)
            with io.BytesIO() as output:
                file_image.save(output, format='JPEG')
                contents = output.getvalue()
                response = make_response(contents, httplib.OK)
                response.headers['Content-Type'] = 'image/jpeg'
                return response
        return make_response(
            "",
            httplib.NOT_FOUND
        )


class PageThumbnailApi(MethodView):
    """ Serves a single thumbnail image for a given Item.

    .. note::

        This API was determined not to require any authentication.
    """

    def get(self, db_id, page_num):
        """ Returns an image data for a single thumbnail of a page.

        :param `int` db_id:
            Database Primary Key for an `app.items.models.Item`
        :param `int` page_num:
            Page number to fetch.  This is 1-indexed.
        :return: A Flask HttpResponse.
        """
        if not g or not getattr(g, 'user', None) or not getattr(g, 'current_user', None):
            raise Unauthorized

        item = helpers.get_item_or_404(db_id)

        thumbnails.assert_item_type_valid(item)

        return thumbnails.construct_thumbnail_response(item, page_num)


class WebReaderErrorReportApi(MethodView):

    @user_is_superuser
    def get(self):
        if 'text/html' not in request.accept_mimetypes:
            raise NotAcceptable
        mgr = web_reader.WebReaderErrorManager(current_app.kvs)

        return render_template("reports/web-reader-error-reports.html",
                               reports=mgr.get_all())

    @user_is_authenticated
    @model_from_url(models.Item, argument_name="item")
    def post(self, item):
        mgr = web_reader.WebReaderErrorManager(current_app.kvs)

        post_args = parsers.WebReaderErrorReportParser().parse_args()

        report = web_reader.WebReaderErrorReport(
            user=g.user,
            item=item,
            report_id=g.request_id,
            **post_args)

        mgr.insert(report)

        return make_response("", httplib.CREATED)


class ItemFileListApi(controllers.Resource):
    ''' Shows a list of ItemFile and do POST / GET '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #    required for database which can't null
        self.reqparser.add_argument('item_files', type=list, required=True,
                                    location='json')

        super(ItemFileListApi, self).__init__()

    @token_required(
        'can_read_write_global_all, '
        'can_read_global_item_file, '
        'can_read_associated_item_file')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = custom_get.GetItemFiles(param, 'item_files').construct()
            return scene
        except Exception as e:
            return internal_server_message(modul="item_files", method="POST",
                                           error=str(e), req=str(param))

    @token_required('can_read_write_global_all,can_read_write_global_item_file,'
                    'can_read_write_associated_item_file')
    def post(self):
        args = self.reqparser.parse_args()

        try:
            scene = helpers.ItemFileConstruct(args, 'POST').construct()
            return scene
        except Exception as e:
            return internal_server_message(modul="item_files", method="POST",
                                           error=e, req=str(args))


class ItemFileApi(controllers.Resource):
    ''' GET/PUT/DELETE ItemFile '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #    required for database whos can't null
        self.reqparser.add_argument('item_id', type=int, required=True, location='json')
        self.reqparser.add_argument('file_type', type=int, required=True, location='json')
        self.reqparser.add_argument('file_name', type=str, required=True, location='json')
        self.reqparser.add_argument('file_order', type=int, required=True, location='json')
        self.reqparser.add_argument('file_status', type=int, required=True, location='json')

        #    optional value
        self.reqparser.add_argument('file_version', type=str, location='json')
        self.reqparser.add_argument('is_active', type=bool, location='json')
        self.reqparser.add_argument('key', type=str, location='json')
        self.reqparser.add_argument('md5_checksum', type=str, location='json')

        super(ItemFileApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_item_file,'
                    'can_read_associated_item_file')
    def get(self, item_id=None):
        m = models.ItemFile.query.filter_by(id=item_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all,can_read_write_global_item_file,'
                    'can_read_write_associated_item_file')
    def put(self, item_id=None):

        m = models.ItemFile.query.filter_by(id=item_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        args = self.reqparser.parse_args()

        #    checking if conflict when edit
        if args['item_id'] != m.item_id:
            u = models.ItemFile.query.filter_by(item_id=args['item_id']).first()
            if u:
                return conflict_message(modul="item file", method="PUT",
                                        conflict=args['item_id'], req=args)

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        if args['item_id']:
            items = models.Item.query.filter_by(id=args['item_id']).first()
            if not items:
                return bad_request_message(modul="item file", method="PUT",
                                           param="item_id", not_found=args['item_id'], req=args)
            kwargs['item_id'] = items.id

        try:
            for k, v in kwargs.iteritems():
                setattr(m, k, v)

            sv = m.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="item file", method="PUT",
                                               error=sv.get('message', ''), req=str(args))
            else:
                rv = m.values()
                purge_redis_cache('items-details-', item_id)
                return Response(json.dumps(rv), status=httplib.OK, mimetype='application/json')

        except Exception as e:
            return internal_server_message(modul="item file", method="PUT",
                                           error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, item_id=None):
        f = models.ItemFile.query.filter_by(id=item_id).first()
        if f:
            #    set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)


class ItemFilexi(controllers.Resource):

    @token_required('can_read_write_global_all, can_read_write_public, can_read_write_public_ext')
    def get(self, item_id=None, order_id=None):
        m = models.ItemFile.query.filter_by(id=item_id, file_order=order_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)


class ItemApiWithCacheBase(CorListControllerMixin, controllers.Resource):
    """ Base class for item api with cache data
    used in item latest and item recommendation API
    """

    # set redis cache expiry time to 3 hour
    CACHE_EXPIRY_TIME = 10800
    # define redis key name for cache per response in this variable later
    REQUEST_CACHE_KEY = 'LATEST'
    # define redis key name for cache per item in this variable
    PER_ITEM_CACHE_KEY = 'item_latest_response'
    methods = ['GET', 'PURGE']

    def __init__(self):
        super(ItemApiWithCacheBase, self).__init__()
        self.request_cache_key_prefix_item_id = ""

    @user_has_any_tokens(
        'can_read_write_global_all',
        'can_read_write_public',
        'can_read_write_public_ext',
        'can_read_global_item')
    def purge(self):
        """ purge data from cache """
        redis_cache = RedisCache(app.kvs)
        redis_cache.delete_with_key_prefix(self.REQUEST_CACHE_KEY)
        redis_cache.delete_with_key_prefix(self.PER_ITEM_CACHE_KEY)
        return jsonify({'success_message': 'Cached data for item latest api successfully purged'})

    def _bypass_cache(self):
        return request.headers.get('Cache-Control', '').lower() == 'no-cache'

    def _get_response_cache_key_prefix(self):
        if self.request_cache_key_prefix_item_id:
            return '{}:{}'.format(self.REQUEST_CACHE_KEY, self.request_cache_key_prefix_item_id)
        else:
            return self.REQUEST_CACHE_KEY

    def _get_cached_response(self, param, redis_cache):
        if self._bypass_cache():
            return None, None
        response_cache_key = self._get_response_cache_key_prefix()
        cached_data = redis_cache.get_hash(generate_redis_key(
            response_cache_key, args=param))
        if cached_data and cached_data.get('data', None):
            cached_response = jsonify(json.loads(cached_data.get('data')))
            weak_etag = cached_data.get('etag', None)
            return cached_response, weak_etag
        else:
            return None, None

    @user_has_any_tokens(
        'can_read_write_global_all',
        'can_read_write_public',
        'can_read_write_public_ext',
        'can_read_global_item')
    def get(self):
        param = request.args.to_dict()

        return self._get_response(param)

    def _get_response(self, param):
        # get whole response body from cache if exists, based on query param
        redis_cache = RedisCache(redis=current_app.kvs, expiry=self.CACHE_EXPIRY_TIME)
        response, weak_etag = self._get_cached_response(param, redis_cache)

        if not response:
            # create new response body

            query_set, total_count = self._get_query_set(param)

            offset = request.args.get('offset', type=int, default=0)
            limit = request.args.get('limit', type=int, default=20)

            items = query_set.offset(offset).limit(limit).all()

            # platform for display offer (must be integer, or string comparison to enum value will failed
            platform_id = request.args.get('display_offer_plid', type=int, default=4)

            # get response data per item (from cache or new from db)
            items_data = self._get_response_data_per_item(items,
                                                          platform_id=platform_id,
                                                          redis_cache=redis_cache)
            response_body = {
                "items": items_data,
                'metadata': {
                    'resultset': {
                        'offset': offset,
                        'limit': limit,
                        'count': total_count
                    }
                }
            }
            weak_etag = self._generate_weak_etag(response_body)
            response = jsonify(response_body)
            response_cache_key = self._get_response_cache_key_prefix()
            redis_cache.write_hash(
                key=generate_redis_key(response_cache_key, args=param),
                data=response_body,
                etag=weak_etag)
        response.set_etag(weak_etag, weak=True)
        response = response.make_conditional(request)
        return response

    @staticmethod
    def _generate_weak_etag(response_body):
        weak_etag = "".join(str(e.get('id', None)) for e in response_body.get('items', None))
        return md5(weak_etag).hexdigest()

    def _get_response_data_per_item(self, items, platform_id, redis_cache):
        data = []
        for item in items:
            key = helpers.get_item_cache_key(item_id=item.id, platform_id=platform_id)
            # get or create now cached data per item
            cached_item_data = redis_cache.get(key)
            if not cached_item_data or self._bypass_cache():
                item_data = helpers.get_item_request_body(item, platform_id)
                redis_cache.write(key=key, data=item_data)
            else:
                item_data = json.loads(cached_item_data)
            data.append(item_data)
        return data

    @abstractmethod
    def _get_query_set(self, param):
        pass


class ItemLatest(ItemApiWithCacheBase):
    """ This api for retrieving latest items.
    """
    # set redis key name for cache per response
    REQUEST_CACHE_KEY = 'LATEST'
    # set redis key name for cache per item
    PER_ITEM_CACHE_KEY = 'item_latest_response'

    def _get_query_set(self, param):

        newspaper_order_by = None
        session = db.session()
        query_set = session.query(models.Item)

        filters = []
        item_type_id = int(param.get('item_type_id', 0))
        vendor_id = int(param.get('vendor_id', 0))
        brand_id = param.get('brand_id', None)

        if item_type_id == NEWSPAPER and not brand_id and not vendor_id:
            # Additional default filter, to show the latest date newspapers only per brand
            # Start
            one_week_duration = get_local() - timedelta(days=7)
            latest_ids = session.query(func.max(Item.id)).filter(
                Item.release_date > one_week_duration,
                Item.item_type == ITEM_TYPES[item_type_id],
                Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                Item.is_active == True
            ).group_by(Item.brand_id).all()

            latest_ids = [id[0] for id in latest_ids]
            query_set = query_set.filter(Item.id.in_(latest_ids))
            newspaper_order_by = True

        if item_type_id:
            filters.append(models.Item.item_type == ITEM_TYPES[item_type_id])

        category_ids = str(param.get('category_id', ''))
        if category_ids:
            category_ids = category_ids.split(',')

        query_set = apply_additional_item_filters(
            query_set, param, filters, brand_id, vendor_id, category_ids)

        total_count = query_set.count()

        if newspaper_order_by:
            # additional filter by news paper, sort by this (default is by Item.id desc)
            query_set = query_set.order_by(
                desc(models.Item.release_date), desc(models.Item.sort_priority), desc(models.Item.id))
        else:
            query_set = query_set.order_by(desc(models.Item.release_date), desc(models.Item.id))

        return query_set, total_count


class ItemListArgsParser(SqlAlchemyFilterParser):
    release_date = fi.DateTimeFilter()
    __model__ = Item


def apply_additional_item_filters(query_set, param, filters, brand_id, vendor_id, category_ids):
    if brand_id:
        if ',' in brand_id:
            filters.append(models.Item.brand_id.in_(brand_id.split(',')))
        else:
            filters.append(models.Item.brand_id == brand_id)

    filters.append(models.Item.is_active == True)
    filters.append(models.Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME])

    is_free = True if param.get('is_free', False) in ['True', 'true', '1', True] else False
    is_bundle = True if param.get('is_bundle', False) in ['True', 'true', '1', True] else False
    is_buffet = True if param.get('is_buffet', False) in ['True', 'true', '1', True] else False
    from app.offers.models import Offer
    from app.offers.choices.status_type import READY_FOR_SALE
    from app.offers.choices.offer_type import BUNDLE, BUFFET
    if is_free:
        query_set = query_set.join(Item.core_offers).filter(
            Offer.is_free == True,
            Offer.offer_status == READY_FOR_SALE,
            Offer.is_active == True
        )
    if is_bundle:
        query_set = query_set.join(Item.core_offers).filter(
            Offer.offer_type_id == BUNDLE,
            Offer.offer_status == READY_FOR_SALE,
            Offer.is_active == True
        )

    parental_control_id = helpers.get_filter_parental_control(int(param.get('parental_control_id', 0)))
    if parental_control_id:
        filters.append(models.Item.parentalcontrol_id == parental_control_id)

    # get more filter defined in ItemListArgsParser
    filter_from_arg_class = ItemListArgsParser().parse_args(request)['filters']
    # removed filter in filter_from_arg_class that already exists in filters above
    new_filter_from_args = [f for f in filter_from_arg_class
                            if f.left.name
                            not in [reqexp.left.name for reqexp in filters]]
    for f in new_filter_from_args:
        filters.append(f)

    # don't put these filters (by ANY) before reqexp.left.name above (error)
    country_id = str(param.get('country_id', ''))
    if country_id:
        country_codes = [countries.id_to_iso3166(int(c_id)) for c_id in country_id.split(',')]
        for code in country_codes:
            filters.append(models.Item.countries.any(code))
    language_id = str(param.get('language_id', ''))
    if language_id:
        language_codes = [languages.id_to_iso639_alpha3(int(lang_id))
                          for lang_id
                          in language_id.split(',')]
        for code in language_codes:
            filters.append(models.Item.languages.any(code))

    dist_country_filter = get_dist_country_filter()
    if dist_country_filter is not None:
        # cannot use: "if dist_country_filter:" error: Boolean value of this clause is not defined
        filters.append(dist_country_filter)

    # apply above filter to query set
    query_set = query_set.filter(*filters)

    already_joined_with_brand = False
    if vendor_id:
        already_joined_with_brand = True
        query_set = query_set.join(models.Item.brand).filter(
            Brand.vendor_id == vendor_id)
    if is_buffet:
        if not already_joined_with_brand:
            query_set = query_set.join(models.Item.brand)
        query_set = query_set.join(Brand.core_offers).filter(
            Offer.offer_type_id == BUFFET,
            Offer.offer_status == READY_FOR_SALE,
            Offer.is_active == True
        )
    author_id = str(param.get('author_id', ''))
    if author_id:
        author_id = author_id.split(',')
        query_set = query_set.join(models.Item.authors).filter(
            models.Author.id.in_(author_id))

    if category_ids:
        query_set = query_set.join(models.Item.categories).filter(
            Category.id.in_(category_ids))
    return query_set


class ItemRecommendation(ItemApiWithCacheBase):
    # set redis key name for cache per response
    REQUEST_CACHE_KEY = 'RECOMMENDATION'
    # set redis key name for cache per item
    PER_ITEM_CACHE_KEY = 'item_latest_response'

    @user_has_any_tokens(
        'can_read_write_global_all',
        'can_read_write_public',
        'can_read_write_public_ext',
        'can_read_global_item')
    def get(self, item_id=None):
        self.item = models.Item.query.get(item_id)
        if not self.item:
            raise NotFound()
        self.request_cache_key_prefix_item_id = item_id
        return super(ItemRecommendation, self).get()

    def _get_query_set(self, param):
        from app.parental_controls.choices import ParentalControlType

        session = db.session()
        query_set = session.query(models.Item).distinct(
            models.Item.id,
            models.Item.release_date, models.Item.sort_priority)

        newspaper_mag_order_by = False
        filters = []

        # ambil category id dari current item
        category_ids = [c.id for c in self.item.categories]
        # jika majalah/newspaper: ambil list brand id utk category yg sama
        #   dan ambil item id terakhir utk masing2 brand order by release date
        if self.item.item_type in [Item.Types.magazine.value, Item.Types.newspaper.value]:
            newspaper_mag_order_by = True
            query_item_ids = session.query(func.max(Item.id)).join(Item.categories).filter(
                Item.item_type == self.item.item_type,
                Item.brand_id != self.item.brand_id,
                Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                Item.is_active == True,
                Category.id.in_(category_ids)
            )
            if self.item.item_type == Item.Types.newspaper.value:
                # for newspaper, get only item that released in the last 7 days
                one_week_duration = get_local() - timedelta(days=7)
                query_item_ids = query_item_ids.filter(Item.release_date > one_week_duration)

            related_brand_item_ids = query_item_ids.group_by(Item.brand_id).all()

            item_ids = [id[0] for id in related_brand_item_ids]
            if item_ids:
                query_set = query_set.filter(Item.id.in_(item_ids))
            # clear filter by category id, and replace it to filter by item ids
            category_ids = []
        else:
            filters.append(models.Item.id != self.item.id)

        vendor_id = int(param.get('vendor_id', 0))
        brand_id = param.get('brand_id', None)
        parentalcontrol_id = param.get('parental_control_id', 0)

        if parentalcontrol_id:
            filters.append(models.Item.parentalcontrol_id == ParentalControlType.parental_level_1.value)

        filters.append(models.Item.item_type == self.item.item_type)

        query_set = apply_additional_item_filters(
            query_set, param, filters, brand_id, vendor_id, category_ids)

        total_count = query_set.count()

        if newspaper_mag_order_by:
            # additional filter by news paper, sort by this (default is by Item.id desc)
            query_set = query_set.order_by(desc(models.Item.release_date), desc(models.Item.sort_priority))
        else:
            query_set = query_set.order_by(desc(models.Item.id))

        return query_set, total_count

    @user_has_any_tokens(
        'can_read_write_global_all',
        'can_read_write_public',
        'can_read_write_public_ext',
        'can_read_global_item')
    def purge(self, item_id=None):
        return super(ItemRecommendation, self).purge()

    # @token_required('can_read_write_global_all, can_read_write_public, can_read_write_public_ext')
    def get_old_method_api(self, item_id=None):
        m = models.Item.query.filter_by(id=item_id).first()
        if m:
            item_category = [str(i.id) for i in m.categories.all()]

            dict_data = {}
            param = request.args.to_dict()

            #    DEFAULT value - RECOMMENDED BY CATEGORY:
            #    dict_data['category_id'] = ",".join(item_category)

            # filter by category
            if m.item_type in [choices.ITEM_TYPES[choices.MAGAZINE], choices.ITEM_TYPES[choices.NEWSPAPER]]:
                #
                # SELECT * FROM core_items AS item
                # JOIN core_brands AS b ON b.id = item.brand_id
                # WHERE b.id != my_item_brand_id
                #
                #

                item = models.Item.query.join(Category.core_items).filter(Category.id.in_(item_category)).filter(
                    models.Item.brand_id != m.brand_id).all()
                brand_id = [i.brand_id for i in item]
                brand_ids = list(set(brand_id))
                dict_data['brand_id'] = brand_ids

            if m.item_type in [choices.ITEM_TYPES[choices.BOOK]]:
                dict_data['category_id'] = ",".join(item_category)

            #    allow only:
            fields = param.get('fields', None)
            offset = param.get('offset', None)
            limit = param.get('limit', None)
            order = param.get('order', None)
            extend = param.get('expand', None)
            is_active = param.get('is_active', False)
            item_status = param.get('item_status', None)
            display_offer_plid = param.get('display_offer_plid', None)
            vendor_id = param.get('vendor_id', None)
            brand_id = param.get('brand_id', None)

            if fields: dict_data['fields'] = fields
            if offset: dict_data['offset'] = offset
            if limit: dict_data['limit'] = limit
            if extend: dict_data['expand'] = extend
            if is_active: dict_data['is_active'] = is_active
            if item_status: dict_data['item_status'] = item_status
            if display_offer_plid: dict_data['display_offer_plid'] = display_offer_plid
            if vendor_id: dict_data['vendor_id'] = vendor_id
            if brand_id: dict_data['brand_id'] = brand_id

            dict_data['order'] = '-release_date'

            scene = custom_get.CustomGet(dict_data, 'RECOMMENDATION').filter_custom()
            return scene
        else:
            return Response(status=httplib.NOT_FOUND)


class ItemSearch(controllers.Resource):
    """ Version 1 of the Scoop Items Search API.

    Subsequent versions of 'search' are run through the ItemListAPI.
    """

    @user_has_any_tokens(
        'can_read_write_global_all',
        'can_read_write_public',
        'can_read_write_public_ext')
    def get(self):
        param = request.args.to_dict()

        # if they're not providing an order, we default to release date desc
        order_by = param.get('order', None)
        if not order_by:
            param['order'] = '-release_date'

        scene = custom_get.CustomGet(param, 'SEARCH').filter_custom()

        # helpers.record_user_analytics_data(scene)

        return scene


class ItemTypeAheadSuggestionApi(controllers.ErrorHandlingApiMixin, controllers.Resource):
    """ Type-ahead suggestions for users searching the Items API.

    This is used on frontend for auto-complete on item search textboxes.
    """

    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext')
    def get(self, suggest=None):
        if MIME_TYPE_V3_JSON in request.headers.get('accept', ""):
            return self._get_version_3(request, search_term=(suggest or request.args.get('q')))
        elif request.path == '/v2/items/searches/suggestions':
            # per 14 juli 2017, use the same algorithm for suggestion search with version 1
            return self._get_version_1(request, search_term=(suggest or request.args.get('q')))
            # return self._get_version_2(request, search_term=(suggest or request.args.get('q')))
        else:
            return self._get_version_1(request, search_term=(suggest or request.args.get('q')))

    def _get_version_1(self, flask_request, search_term):
        """ Suggestion search flow
        brands startwith (huj*)
        if result less then limit data: search again: authors startwith (huj*)
        if result less then limit data: search again: vendors startwith (huj*)
        if result less then limit data: search again: brands contain (*huj*)
        if result less then limit data: search again: authors contain (*huj*)
        if result less then limit data: search again: vendors contain (*huj*)
        :return:
        """
        request_params = flask_request.args.to_dict()
        request_params['q'] = search_term
        scene = custom_get.Suggestions(request_params, 'suggestions').construct()
        return scene

    def _get_version_2(self, flask_request, search_term):
        try:
            query_field = flask_request.args.get("df", "item_name_suggest")

            response = {'counts': {}, 'suggestions': []}

            all_suggestions = []

            for facet_item_type in ['book', 'magazine', 'newspaper']:
                req_url = (
                    u"{solr_url}/select?"
                    u"fl=item_name,score,brand_name&"
                    u"fq=item_is_active:true&"
                    u"q={query_field}:{query}&"
                    u"fq=item_type:{item_type}&"
                    u"wt=json&"
                    u"rows={rows}").format(
                    solr_url=current_app.config['SOLR_URL'],
                    query_field=query_field,
                    query=search_term,
                    item_type=facet_item_type,
                    rows=5 if facet_item_type == 'book' else 30)

                resp = requests.get(req_url)
                solr_data = resp.json()

                if facet_item_type == 'book':
                    all_suggestions.extend(
                        [{'name': solr_item["item_name"], 'score': solr_item['score']} for solr_item in
                         solr_data['response']['docs']])
                    response['counts'][facet_item_type] = solr_data['response']['numFound']
                elif facet_item_type == 'magazine':

                    for solr_item in solr_data['response']['docs']:
                        if solr_item['brand_name'] in [si['name'] for si in all_suggestions]:
                            continue
                        all_suggestions.append({'name': solr_item["brand_name"], 'score': solr_item['score']})

                    response['counts'][facet_item_type] = solr_data['response']['numFound']
                elif facet_item_type == 'newspaper':

                    for solr_item in solr_data['response']['docs']:

                        if solr_item['brand_name'] in [si['name'] for si in all_suggestions]:
                            continue
                        all_suggestions.append({'name': solr_item["brand_name"], 'score': solr_item['score']})

                    response['counts'][facet_item_type] = solr_data['response']['numFound']

            all_suggestions = sorted(all_suggestions, key=lambda e: e['score'], reverse=True)

            response['suggestions'] = [si['name'] for si in all_suggestions]

            return response

        except requests.exceptions.RequestException as e:
            raise exceptions.SolrError(inner_exception=e)

    def get_brand_count(self, brand_name):
        url = u"{solr_url}/select?q=brand_name:\"{brand_name}\"&rows=0&wt=json".format(
            solr_url=current_app.config["SOLR_URL"],
            brand_name=brand_name)
        resp = requests.get(url)
        return resp.json()['response']['numFound']

    def _handle_SolrError(self, exception):
        return {
                   "status": httplib.BAD_REQUEST,
                   "error_code": httplib.BAD_REQUEST,
                   "user_message": exception.get('description', str(exception)),
                   "developer_message": exception.get('developer_message',
                                                      str(exception))
               }, httplib.INTERNAL_SERVER_ERROR

    def _get_version_3(self, flask_request, search_term):
        raise NotImplemented


class ItemEdition(controllers.Resource):

    @token_required('can_read_write_global_all, can_read_write_public, can_read_write_public_ext')
    def get(self, edition_code=None):
        m = models.Item.query.filter_by(edition_code=edition_code).first()
        if m:
            dict_data = {}
            param = request.args.to_dict()

            #    DEFAULT value:
            dict_data['item_id'] = m.id
            dict_data['expand'] = 'languages,authors,categories,countries,item_type,brand,vendor,parental_control'

            #    Optional filter limited only:
            extend_par = param.get('expand', None)
            fields_par = param.get('fields', None)
            if extend_par:
                dict_data['expand'] = '%s,%s' % (dict_data['expand'], extend_par)

            if fields_par:
                dict_data['fields'] = fields_par

            scene = custom_get.CustomGet(dict_data, 'EDITIONS').filter_custom()
            return scene
        else:
            return Response(status=httplib.NOT_FOUND)


class PopularItemListApi(controllers.ListApiResource):
    query_set = models.PopularItem.query.join(models.PopularItem.item)

    default_ordering = (desc(models.PopularItem.download_count),)

    viewmodel = viewmodels.PopularViewmodel

    list_request_parser = parsers.PopularItemArgsParser

    response_serializer = serializers.PopularItemSerializer

    serialized_list_name = u'items'

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext')

    def _append_serializer_fields(self, serializer):
        # hack: ignore "expand" query string parameter
        return serializer

    def _get_filters(self):
        filters = super(PopularItemListApi, self)._get_filters()
        if 'parental_control_id' in request.args:
            filters.append(
                models.PopularItem.item.has(
                    parentalcontrol_id=request.args.get('parental_control_id', type=int)
                )
            )
        display_offer_plid = request.args.get('display_offer_plid', type=int)
        is_free = request.args.get('is_free', None)
        if is_free and is_free.lower() in ['true', '1', 1]:
            # free popular item only (fields non_free_platforms is null)
            filters.append(models.PopularItem.non_free_platforms == None)
        else:
            # for non free item, field non_free_platforms (array) contains platform id (display_offer_plid)
            display_offer_plid = display_offer_plid or (
                g.current_client.platform_id if getattr(g, 'current_client') else None)
            if display_offer_plid:
                filters.append(models.PopularItem.non_free_platforms.any(display_offer_plid))

        return filters


class ItemOwnedListApi(controllers.Resource):

    def get(self):
        """ This is old api: v1/owned_items?user_id={user_id}&item_id={item_id}, {item_id2}

        for the new api: v1/users/current-user/owned-items or v1/users/{user_id}/owned-items
            check method: app.users.items.get_user_owned_items

        :return:
        """
        api_version = request.headers.get('version', '1.0')
        user_id = request.args.get('user_id', None, type=int)
        if user_id is None or getattr(g, 'current_user', None) is None:
            raise Unauthorized

        session = db.session()
        if user_id and g.current_user.id != user_id:
            from app.users.users import User
            user = session.query(User).get(user_id)
        else:
            user = g.current_user

        offset, limit = get_offset_limit(request)
        item_ids = request.args.get('item_id', type=str, default=None)
        item_ids = item_ids.split(',') if item_ids else []

        # get data from the new api
        from app.users.items import get_user_items_data, get_other_eligible_items
        owned_items, count = get_user_items_data(user=user, item_ids=item_ids, limit=limit, offset=offset,
                                                 session=session, api_version=api_version)

        items_data = []
        for owned_item in owned_items:
            items_data.append(self._get_item_response(owned_item, owned_item.item, user_id))

        eligible_items = []
        if item_ids:
            # if front end send list of item id ( filter by item id's ):
            #   if user is a publisher or apps foundry user or some other criteria
            #       get other eligible items (item that fit the criteria but not yet exists in owned items)
            eligible_items = get_other_eligible_items(user, session, item_ids, owned_items)
            if eligible_items:
                count += len(eligible_items)
                for item in eligible_items:
                    items_data.append(self._get_item_response(owned_item=None, item=item, user_id=user_id))

        if items_data:
            return get_response(data=items_data, key_name="owned_items", count=count, limit=limit, offset=offset)
        else:
            response = jsonify('')
            response.status_code = httplib.NO_CONTENT
            return response

    def _get_item_response(self, owned_item, item, user_id):
        dummy_date = item.created.isoformat()
        return {
            'user_id': user_id,
            'created': owned_item.created.isoformat() if owned_item else dummy_date,
            'modified': owned_item.modified.isoformat() if owned_item else dummy_date,
            'is_active': owned_item.is_active if owned_item else True,
            'id': owned_item.id if owned_item else -1 * item.id,
            'edition_code': item.edition_code,
            'brand': {'id': item.brand.id, 'name': item.brand.name},
            'item_id': item.id,
            'item': {
                'item_type': {'id': {ITEM_TYPES[k]: k for k in ITEM_TYPES}[item.item_type], 'name': item.item_type},
                'audiobook': item.get_audiobooks(),
                'id': item.id,
                'name': item.name,
                'image_highres': preview_s3_covers(item, 'image_highres'),
                'thumb_image_highres': preview_s3_covers(item, 'image_highres'),
                'media_base_url': current_app.config['MEDIA_BASE_URL'],
                'brand_id': item.brand.id,
                'parental_control_id': item.parentalcontrol_id,
                'content_type': {FILE_TYPES[k]: k for k in FILE_TYPES}[item.content_type],
                'item_type_id': {ITEM_TYPES[k]: k for k in ITEM_TYPES}[item.item_type],
                'issue_number': item.issue_number,
                'release_date': isoformat(item.release_date),
                'edition_code': item.edition_code,
                'brand': {'id': item.brand.id, 'name': item.brand.name},
                'vendor': {'id': item.brand.vendor.id, 'name': item.brand.vendor.name},
            },
        }

    @token_required('can_read_write_global_all, can_read_write_global_payment_user_item')
    def post(self):
        self.reqparser = reqparse.RequestParser()
        self.reqparser.add_argument('owned_items', type=list, required=True, location='json')
        args = self.reqparser.parse_args()
        return helpers.OwnedItemConstruct(args, 'POST').post_owned_items()


class ItemOwnedApi(controllers.Resource):
    ''' GET/PUT/DELETE ItemOwned '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #    required for database whos can't null
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('item_id', type=int, required=True, location='json')

        #    optional value
        self.reqparser.add_argument('edition_code', type=str, location='json')
        self.reqparser.add_argument('order_line_id', type=int, location='json')
        self.reqparser.add_argument('is_active', type=bool, location='json')
        self.reqparser.add_argument('md5_checksum', type=str, location='json')

        super(ItemOwnedApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public, '
                    'can_read_write_public_ext, can_read_global_payment_user_item')
    def get(self, owned_id=None):

        m = models.UserItem.query.filter_by(id=owned_id).first()

        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_payment_user_item')
    def put(self, owned_id=None):
        args = self.reqparser.parse_args()

        m = models.UserItem.query.filter_by(id=owned_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        #    check conflict or not if edit
        if args['item_id'] != m.item_id:
            u = models.UserItem.query.filter_by(user_id=args['user_id'],
                                                item_id=args['item_id']).first()
            if u:
                return conflict_message(modul="item owned", method="PUT",
                                        conflict=args['item_id'], req=args)

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        kwargs['orderline_id'] = kwargs['order_line_id']
        del kwargs['order_line_id']

        if args['item_id']:
            items = models.Item.query.filter_by(id=args['item_id']).first()
            if not items:
                return bad_request_message(modul="owned subs", method="PUT",
                                           param="item_id", not_found=args['item_id'], req=args)
            kwargs['item_id'] = items.id

        try:
            f = models.UserItem.query.get(owned_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="item owned", method="PUT",
                                               error=sv.get('message', ''), req=str(args))

            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.OK, mimetype='application/json')

        except Exception as e:
            return internal_server_message(modul="item owned", method="PUT",
                                           error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, owned_id=None):
        f = models.UserItem.query.filter_by(id=owned_id).first()
        if f:
            #    set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=httplib.NOT_FOUND)


class ItemOwnedSubsListApi(controllers.Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()
        self.reqparser.add_argument('owned_subscriptions', type=list, required=True, location='json')
        super(ItemOwnedSubsListApi, self).__init__()

    @token_required(
        'can_read_write_global_all, can_read_write_public, can_read_write_public_ext, can_read_global_user_subscription')
    def get(self):
        param = request.args.to_dict()
        try:
            scene = custom_get.GetItemOwned(param, 'owned_subscriptions').construct()
            return scene
        except Exception as e:
            return internal_server_message(modul="owned subs", method="POST",
                                           error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_global_user_subscription')
    def post(self):
        args = self.reqparser.parse_args()
        try:
            scene = helpers.OwnedItemSubsConstruct(args, 'POST').construct()
            return scene

        except Exception as e:
            return internal_server_message(modul="owned subs",
                                           method="POST", error=e, req=str(args))


class ItemOwnedSubsApi(controllers.Resource):
    ''' GET/PUT/DELETE ItemOwned '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #    required for database whos can't null
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('quantity', type=int, required=True, location='json')
        self.reqparser.add_argument('quantity_unit', type=int, required=True, location='json')
        self.reqparser.add_argument('valid_from', type=str, required=True, location='json')
        self.reqparser.add_argument('valid_to', type=str, required=True, location='json')
        self.reqparser.add_argument('brand_id', type=int, required=True, location='json')

        #    optional value
        self.reqparser.add_argument('subscription_code', type=str, location='json')
        self.reqparser.add_argument('orderline_id', type=int, location='json')
        self.reqparser.add_argument('allow_backward', type=bool, location='json')
        self.reqparser.add_argument('is_active', type=bool, location='json')

        super(ItemOwnedSubsApi, self).__init__()

    @token_required(
        'can_read_write_global_all, can_read_write_public, can_read_write_public_ext, can_read_global_user_subscription')
    def get(self, owned_id=None):
        param = request.args.to_dict()
        try:
            param['subs_id'] = owned_id
            scene = custom_get.GetItemOwned(param, 'owned_subscriptions_details').construct()
            return scene
        except Exception as e:
            return internal_server_message(modul="owned subs", method="POST",
                                           error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_global_user_subscription')
    def put(self, owned_id=None):
        args = self.reqparser.parse_args()

        m = models.UserSubscription.query.filter_by(id=owned_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        #    check conflict or not if edit
        if args['brand_id'] != m.brand_id:
            return err_response(status=httplib.BAD_REQUEST,
                                error_code=httplib.BAD_REQUEST,
                                developer_message="Brand id can't changes, please contact your admin",
                                user_message="brand id invalid")

        if args['user_id'] != m.user_id:
            return err_response(status=httplib.BAD_REQUEST,
                                error_code=httplib.BAD_REQUEST,
                                developer_message="user id can't changes, please contact your admin",
                                user_message="user id invalid")

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        if args['brand_id']:
            brand = Brand.query.filter_by(id=args['brand_id']).first()
            if not brand:
                return bad_request_message(modul="owned subs", method="PUT",
                                           param="brand_id", not_found=args['brand_id'], req=args)
            kwargs['brand_id'] = brand.id

        if args['valid_from']:
            valid_from = date_validator(args['valid_from'], 'custom')
            if valid_from.get('invalid', False):
                return err_response(status=httplib.BAD_REQUEST,
                                    error_code=httplib.BAD_REQUEST,
                                    developer_message="Invalid valid from, format YYYY-MM-DD HH:MM:SS",
                                    user_message="valid from invalid")
            kwargs['valid_from'] = args['valid_from']

        if args['valid_to']:
            valid_to = date_validator(args['valid_to'], 'custom')
            if valid_to.get('invalid', False):
                return err_response(status=httplib.BAD_REQUEST,
                                    error_code=httplib.BAD_REQUEST,
                                    developer_message="Invalid valid to, format YYYY-MM-DD HH:MM:SS",
                                    user_message="valid to invalid")
            kwargs['valid_to'] = args['valid_to']

        if valid_to <= valid_from:
            return err_response(status=httplib.BAD_REQUEST,
                                error_code=httplib.BAD_REQUEST,
                                developer_message="Invalid valid to must greater than valid from",
                                user_message="valid to invalid")

        try:
            f = models.UserSubscription.query.get(owned_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="owned subs", method="PUT",
                                               error=sv.get('message', ''), req=str(args))

            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.OK, mimetype='application/json')

        except Exception as e:
            return internal_server_message(modul="owned subs", method="PUT",
                                           error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, owned_id=None):
        f = models.UserSubscription.query.filter_by(id=owned_id).first()
        if f:
            #    set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=httplib.NOT_FOUND)


class ItemSubsApi(controllers.Resource):
    """
        This api for migrate subscription data scoopcore --> scoopcor
    """

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #    required for database whos can't null
        self.reqparser.add_argument('id', type=int, required=False, location='json')

        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('quantity', type=int, required=True, location='json')
        self.reqparser.add_argument('quantity_unit', type=int, required=True, location='json')
        self.reqparser.add_argument('current_quantity', type=int, required=True, location='json')
        self.reqparser.add_argument('valid_from', type=str, required=True, location='json')
        self.reqparser.add_argument('valid_to', type=str, required=True, location='json')
        self.reqparser.add_argument('brand_id', type=int, required=True, location='json')
        self.reqparser.add_argument('owned_items', type=list, required=True, location='json')

        #    optional value
        self.reqparser.add_argument('subscription_code', type=str, location='json')
        self.reqparser.add_argument('orderline_id', type=int, location='json')
        self.reqparser.add_argument('allow_backward', type=bool, location='json')
        self.reqparser.add_argument('is_active', type=bool, location='json')

        super(ItemSubsApi, self).__init__()

    @token_required('can_read_write_global_all')
    def post(self):
        try:
            args = self.reqparser.parse_args()
            scene = MigrateSubs(args, 'POST').construct()
            return scene
        except Exception as e:
            return internal_server_message(modul="ItemSubsApi", method="POST",
                                           error=e, req='')


class ItemSubsLinkApi(controllers.Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('item_id', type=int, required=True, location='json')
        self.reqparser.add_argument('brand_id', type=int, required=True, location='json')

        self.reqparser.add_argument('edition_code', type=str, location='json')
        self.reqparser.add_argument('orderline_id', type=int, location='json')
        self.reqparser.add_argument('is_active', type=bool, location='json')

        super(ItemSubsLinkApi, self).__init__()

    @token_required('can_read_write_global_all')
    def post(self):
        try:
            args = self.reqparser.parse_args()
            scene = LinkSubs(args, 'POST').construct()
            return scene
        except Exception as e:
            return internal_server_message(modul="ItemSubsLinkApi", method="POST",
                                           error=e, req='')


class ItemDownloadApi(controllers.Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #    required for database whos can't null
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('item_id', type=int, required=True, location='json')
        self.reqparser.add_argument('timestamp_unix', type=int, required=True, location='json')
        self.reqparser.add_argument('signature', type=str, required=True, location='json')

        self.reqparser.add_argument('file_order__gte', type=int, location='json')
        self.reqparser.add_argument('file_order__lte', type=int, location='json')
        self.reqparser.add_argument('file_order__in', type=list, location='json')

        self.reqparser.add_argument('limit', type=int, location='json')
        self.reqparser.add_argument('offset', type=int, location='json')

        self.reqparser.add_argument('country_code', type=str, location='json')
        self.reqparser.add_argument('ip_address', type=str, location='json')

        super(ItemDownloadApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def post(self):
        try:
            args = self.reqparser.parse_args()
            scene = helpers.DownloadItems(args, 'POST').construct()
            return scene

        except Exception as e:
            return internal_server_message(modul="ItemDownloadApi", method="POST",
                                           error=e, req='')


class ItemDisctrictListApi(controllers.Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()
        self.reqparser.add_argument('name', type=str, required=True, location='json')
        self.reqparser.add_argument('group_type', type=int, required=True, location='json')
        self.reqparser.add_argument('countries', type=list, required=True, location='json')
        self.reqparser.add_argument('vendor_id', type=int, required=True, location='json')

        # optional parameters
        self.reqparser.add_argument('is_active', type=bool, location='json')
        super(ItemDisctrictListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public,'
                    'can_read_write_public_ext, can_read_global_district')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = custom_get.DistributionCountryGroupGet(param,
                                                           'distribution_country_groups', 'POST').construct()
            return scene

        except Exception, e:
            return internal_server_message(modul="item district", method="POST",
                                           error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_global_district')
    def post(self):
        args = self.reqparser.parse_args()

        # check if currencies already exist with same right_symbol
        po = models.DistributionCountryGroup.query.filter_by(name=args['name']).first()
        if po:
            return conflict_message(modul="item district", method="POST",
                                    conflict=args['name'], req=args)

        # cek vendors
        vendor_data = Vendor.query.filter_by(id=args['vendor_id']).first()
        if not vendor_data:
            return bad_request_message(modul="item district", method="POST",
                                       param="vendor_id", not_found=args['vendor_id'], req=args)

        # construct in same dict
        kwargs = {}
        for item in args:
            if args[item] == None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            m = models.DistributionCountryGroup(**kwargs)
            sv = m.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="item district", method="POST",
                                               error=sv.get('message', ''), req=str(args))
            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="item district", method="POST",
                                           error=str(e), req=str(args))


class ItemDisctrictApi(controllers.Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('name', type=str, required=True, location='json')
        self.reqparser.add_argument('group_type', type=int, required=True, location='json')
        self.reqparser.add_argument('countries', type=list, required=True, location='json')
        self.reqparser.add_argument('vendor_id', type=int, required=True, location='json')

        # optional parameters
        self.reqparser.add_argument('is_active', type=bool, location='json')
        super(ItemDisctrictApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public, can_read_global_district')
    def get(self, district_id=None):
        m = models.DistributionCountryGroup.query.filter_by(id=district_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_district')
    def put(self, district_id=None):

        m = models.DistributionCountryGroup.query.filter_by(id=district_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        args = self.reqparser.parse_args()

        # checking if conflict when edit
        if args['name'] != m.name:
            u = models.DistributionCountryGroup.query.filter_by(name=args['name']).first()
            if u:
                return conflict_message(modul="item district", method="PUT",
                                        conflict='%s group' % args['name'], req=args)

        # cek vendors
        vendor_data = Vendor.query.filter_by(id=args['vendor_id']).first()
        if not vendor_data:
            return bad_request_message(modul="item district", method="POST",
                                       param="vendor_id", not_found=args['vendor_id'], req=args)

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            f = models.DistributionCountryGroup.query.get(district_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="item district", method="PUT",
                                               error=sv.get('message', ''), req=str(args))
            else:

                rv = m.values()
                return Response(json.dumps(rv), status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="item district", method="PUT",
                                           error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, district_id=None):
        f = models.DistributionCountryGroup.query.filter_by(id=district_id).first()
        if f:
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)


class ItemBonusesListApi(controllers.Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        self.reqparser.add_argument('name', type=unicode, required=True, location='json')
        self.reqparser.add_argument('valid_to', type=str, required=True, location='json')
        self.reqparser.add_argument('valid_from', type=str, required=True, location='json')

        self.reqparser.add_argument('description', type=str, location='json')
        self.reqparser.add_argument('is_active', type=bool, location='json')
        self.reqparser.add_argument('items', type=list, location='json')
        self.reqparser.add_argument('brands', type=list, location='json')

        self.reqparser.add_argument('point', type=int, location='json')
        self.reqparser.add_argument('point_referral', type=int, location='json')

        super(ItemBonusesListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public,'
                    'can_read_write_public_ext, can_read_global_gift')
    def get(self):
        m = models.ItemRegister.query.all()
        if m:
            rv = json.dumps({"register": [i.values() for i in m]})
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_gift')
    def post(self):

        args = self.reqparser.parse_args()

        # cek items
        if args['items']:
            for items in args['items']:
                item_data = models.Item.query.filter_by(id=items).first()
                if not item_data:
                    return bad_request_message(modul="item register", method="POST",
                                               param="items", not_found=items, req=args)

        # cek brands
        if args['brands']:
            for brand_id in args['brands']:
                brand_data = Brand.query.filter_by(id=brand_id).first()
                if not brand_data:
                    return bad_request_message(modul="item register", method="POST",
                                               param="brands", not_found=brand_id, req=args)

        valid_from = date_validator(args['valid_from'], 'custom')
        if valid_from.get('invalid', False):
            return err_response(status=httplib.BAD_REQUEST,
                                error_code=httplib.BAD_REQUEST,
                                developer_message="Invalid valid from",
                                user_message="valid from invalid")

        valid_to = date_validator(args['valid_to'], 'custom')
        if valid_to.get('invalid', False):
            return err_response(status=httplib.BAD_REQUEST,
                                error_code=httplib.BAD_REQUEST,
                                developer_message="Invalid valid to",
                                user_message="valid to invalid")

        if valid_to <= valid_from:
            return err_response(status=httplib.BAD_REQUEST,
                                error_code=httplib.BAD_REQUEST,
                                developer_message="Invalid valid to must greater than valid from",
                                user_message="valid to invalid")

        # construct in same dict
        kwargs = {}
        for item in args:
            if args[item] == None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            m = models.ItemRegister(**kwargs)
            sv = m.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="item register", method="POST",
                                               error=sv.get('message', ''), req=str(args))
            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="item register", method="POST",
                                           error=str(e), req=str(args))


class ItemBonusesApi(controllers.Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        self.reqparser.add_argument('name', type=unicode, required=True, location='json')
        self.reqparser.add_argument('valid_to', type=str, required=True, location='json')
        self.reqparser.add_argument('valid_from', type=str, required=True, location='json')

        self.reqparser.add_argument('description', type=str, location='json')
        self.reqparser.add_argument('is_active', type=bool, location='json')
        self.reqparser.add_argument('items', type=list, location='json')
        self.reqparser.add_argument('brands', type=list, location='json')

        self.reqparser.add_argument('point', type=int, location='json')
        self.reqparser.add_argument('point_referral', type=int, location='json')

        super(ItemBonusesApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public,'
                    'can_read_write_public_ext, can_read_global_gift')
    def get(self, gift_name=None):
        m = models.ItemRegister.query.filter_by(name=gift_name).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_gift')
    def put(self, gift_name=None):

        m = models.ItemRegister.query.filter_by(name=gift_name).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        args = self.reqparser.parse_args()

        # cek items
        if args['items']:
            for items in args['items']:
                item_data = models.Item.query.filter_by(id=items).first()
                if not item_data:
                    return bad_request_message(modul="item register", method="POST",
                                               param="items", not_found=items, req=args)

        # cek brands
        if args['brands']:
            for brand_id in args['brands']:
                brand_data = Brand.query.filter_by(id=brand_id).first()
                if not brand_data:
                    return bad_request_message(modul="item register", method="POST",
                                               param="items", not_found=brand_id, req=args)

        valid_from = date_validator(args['valid_from'], 'custom')
        if valid_from.get('invalid', False):
            return err_response(status=httplib.BAD_REQUEST,
                                error_code=httplib.BAD_REQUEST,
                                developer_message="Invalid valid from",
                                user_message="valid from invalid")

        valid_to = date_validator(args['valid_to'], 'custom')
        if valid_to.get('invalid', False):
            return err_response(status=httplib.BAD_REQUEST,
                                error_code=httplib.BAD_REQUEST,
                                developer_message="Invalid valid to",
                                user_message="valid to invalid")

        if valid_to <= valid_from:
            return err_response(status=httplib.BAD_REQUEST,
                                error_code=httplib.BAD_REQUEST,
                                developer_message="Invalid valid to must greater than valid from",
                                user_message="valid to invalid")

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            f = models.ItemRegister.query.filter_by(name=gift_name).first()
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="item register", method="PUT",
                                               error=sv.get('message', ''), req=str(args))
            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="item register", method="PUT",
                                           error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, gift_name=None):
        f = models.ItemRegister.query.filter_by(name=gift_name).first()
        if f:
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)


class ItemBonusesDelivery(controllers.Resource):
    """
        This api deliver free item when user registers
    """

    def __init__(self):
        self.reqparser = reqparse.RequestParser()
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')

        super(ItemBonusesDelivery, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public,'
                    'can_read_write_public_ext, can_read_global_gift')
    def put(self, gift_name=None):

        try:
            args = self.reqparser.parse_args()

            item_data, rv = [], {}
            item_register = models.ItemRegister.query.filter_by(name=gift_name).all()

            for claim in item_register:
                if claim.is_valid():
                    # recheck if item already inside UserItem

                    for item in claim.get_items():
                        item_data.append(item)

                        user_item = models.UserItem.query.filter_by(user_id=args['user_id'],
                                                                    item_id=item.id).first()

                        try:
                            vendors = item.get_vendor()
                            vendor_id = vendors.get('id', 0)
                        except:
                            vendor_id = 0

                        if not user_item:
                            user_put = models.UserItem(
                                user_id=args['user_id'],
                                item_id=item.id,
                                edition_code=item.edition_code,
                                orderline_id=0,
                                is_active=True,
                                brand_id=item.brand_id,
                                itemtype_id=item_types.enum_to_id(item.item_type),
                                vendor_id=vendor_id
                            )
                            user_put.save()
                        else:
                            pass

            rv['name'] = gift_name

            if item_data:
                rv['items'] = [i.values() for i in item_data]
                return Response(json.dumps(rv), status=httplib.CREATED, mimetype='application/json')
            else:
                return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="item register claim", method="PUT",
                                           error=e, req=str(args))


class ItemMigrationSC1(controllers.Resource):
    """ This for patch SC2 item to SC1 data """

    @token_required('can_read_write_global_all')
    def get(self, item_id=None):

        try:
            item = models.Item.query.filter_by(id=item_id).first()

            if not item:
                return Response(status=httplib.NOT_FOUND)

            status, data_json = contruct_sc1_response(item)
            if not status:
                return internal_server_message(
                    modul="Item Migration SC1", method="GET",
                    error=data_json, req=data_json)

            data = json.dumps(data_json)

            # authorization signature
            data_signature = '%s%s%s' % (item.id, item.brand_id, app.config['SC1_SALT'])
            headers = {'Authorization': base64.b64encode(data_signature)}
            send_files = requests.post(app.config['SC1_METADATA'], data=data, headers=headers)

            if send_files.status_code not in [201]:
                return internal_server_message(
                    modul="Item Migration SC1", method="POST",
                    error=send_files.text, req=data)

            return Response(data, status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return internal_server_message(
                modul="Item Migration SC1", method="POST",
                error=e, req="item_id : %s" % item_id)


class ItemMigrationPortal(controllers.Resource):
    """ This for patching data from scoopportal, directly """

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('content_type', type=str, required=True, location='json')

        super(ItemMigrationPortal, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def put(self, item_id=None):

        try:
            args = self.reqparser.parse_args()

            item = models.Item.query.filter_by(id=item_id).first()

            if not item:
                return Response(status=httplib.NOT_FOUND)

            item.content_type = args['content_type']
            item.save()

            return Response(None, status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(
                modul="Item Migration Portal", method="PUT",
                error=e, req="item_id : %s" % item_id)


class ItemCheckRelatedFilesApi(MethodView):

    def get(self, item_id):
        item_file = models.ItemFile.query.filter_by(item_id=item_id).first()

        if not item_file:
            return Response('{}', status=httplib.NOT_FOUND, mimetype='application/json')

        file_name = item_file.file_name

        # check in server 62/in NAS, already attached to static folder in COR
        # ex: scoopcor/www/app/static/item-file-archives/processed
        internal_server_path = os.path.join(
            app.static_folder,
            'item_content_files',
            'processed')

        response_data = self.get_response_data(
            file_name=file_name,
            internal_server_path=internal_server_path,
            aws_server=app.config['BOTO3_SERVER'],
            aws_region=app.config['BOTO3_AWS_REGION'],
            aws_app_id=app.config['BOTO3_AWS_APP_ID'],
            aws_secret=app.config['BOTO3_AWS_APP_SECRET'],
            aws_bucket_name=app.config['BOTO3_BUCKET_NAME']
        )

        return Response(json.dumps(response_data), status=httplib.OK, mimetype='application/json')

    @staticmethod
    def get_response_data(file_name, internal_server_path,
                          aws_server, aws_region, aws_app_id, aws_secret, aws_bucket_name):
        response_data = {}

        server_62_gateway = InternalFileServerGateway(
            path=internal_server_path
        )
        response_data['NAS'] = ItemCheckRelatedFilesApi.get_file_info(
            file_name=file_name,
            file_server_gateway=server_62_gateway)

        # check in server s3
        s3 = boto3.client(
            's3',
            app.config['BOTO3_AWS_REGION'],
            aws_access_key_id=app.config['BOTO3_AWS_APP_ID'],
            aws_secret_access_key=app.config['BOTO3_AWS_APP_SECRET'],
        )

        amazon_s3_server_gateway = AmazonS3ServerGateway(
            s3,
            aws_server=app.config['BOTO3_SERVER'],
            aws_bucket_name=app.config['BOTO3_BUCKET_NAME']
        )

        response_data['S3'] = ItemCheckRelatedFilesApi.get_file_info(
            file_name=file_name,
            file_server_gateway=amazon_s3_server_gateway)

        return response_data

    @staticmethod
    def get_file_info(file_name, file_server_gateway):
        return file_server_gateway.get_file_info(file_name)


class ItemFileUploadToServerApi(MethodView):

    def post(self):

        parser = FileItemUploadToServerParser()
        args = parser.parse_args(req=request)

        items_to_upload = args['items']

        response_data = self.post_init_s3_then_upload_items(items_to_upload)

        return Response(json.dumps(response_data), status=httplib.OK, mimetype='application/json')

    def post_item_to_s3_from_script(self, item_id):

        # this method will be run from script: item_files_upload_to_s3.py
        #  with 1 parameter item_id

        items_to_upload = [{'id': item_id}]

        return self.post_init_s3_then_upload_items(items_to_upload=items_to_upload)

    def post_init_s3_then_upload_items(self, items_to_upload):

        s3 = boto3.client(
            's3',
            app.config['BOTO3_AWS_REGION'],
            aws_access_key_id=app.config['BOTO3_AWS_APP_ID'],
            aws_secret_access_key=app.config['BOTO3_AWS_APP_SECRET'],
        )

        amazon_s3_server_gateway = AmazonS3ServerGateway(
            s3,
            aws_server=app.config['BOTO3_SERVER'],
            aws_bucket_name=app.config['BOTO3_BUCKET_NAME']
        )

        # source file is in server 62/in NAS, already attached to static folder in COR
        # ex: /srv/scoopcor/www/app/static/item-file-archives/processed
        internal_server_path = os.path.join(
            app.static_folder,
            'item_content_files',
            'processed')

        response_data = self.upload_item(
            items_to_upload=items_to_upload,
            amazon_s3_server_gateway=amazon_s3_server_gateway,
            internal_server_path=internal_server_path
        )

        return response_data

    def upload_item(self, items_to_upload, amazon_s3_server_gateway, internal_server_path):
        result = []

        for item in items_to_upload:

            response = {}
            item_id = item.get('id')
            if not item_id:
                response['item_id'] = 'Item Id is None'

            else:
                response['item_id'] = item_id

                item_file = models.ItemFile.query.filter_by(item_id=item_id).first()

                if not item_file:
                    response['item_file'] = 'Not Found'
                else:
                    file_name = item_file.file_name
                    response['item_file'] = file_name

                    item_file_full_path = os.path.join(
                        internal_server_path,
                        InternalFileServerGateway.get_file_name(file_name))

                    response['upload_status'] = self.upload_item_to_s3(
                        item_file_full_path=item_file_full_path,
                        amazon_s3_server_gateway=amazon_s3_server_gateway,
                        file_name=file_name,
                    )

            result.append(response)

        return {'results': result}

    @staticmethod
    def upload_item_to_s3(item_file_full_path, amazon_s3_server_gateway, file_name):

        if os.path.isfile(item_file_full_path):
            try:
                s3_file_size = amazon_s3_server_gateway.get_file_size(file_name)
            except ClientError:
                s3_file_size = -1

            local_file_size = os.path.getsize(item_file_full_path)

            if s3_file_size != local_file_size:
                return amazon_s3_server_gateway.file_upload(
                    item_file_full_path=item_file_full_path,
                    key=file_name,
                )
            else:
                return 'File already exists in S3'
        else:
            return 'File not found in local server'


class ItemReviewApiList(controllers.Resource):

    @token_required('can_read_write_global_all')
    def get(self):
        args = request.args.to_dict()
        limit, offset, q = args.get('limit', 20), args.get('offset', 0), args.get('q', None)
        data_review = Review.query

        if q:
            q = '{}{}{}'.format('%', q, '%')
            data_review = data_review.join(User).join(Item).filter(
                or_(Review.review.ilike(q),
                    User.email.ilike(q),
                    Item.name.ilike(q)))

        total_count = data_review.count()
        data_review = data_review.order_by(desc(Review.created)).limit(limit).offset(offset).all()

        rebuild = {
            "reviews": [data.response_data() for data in data_review],
            'metadata': {
                'resultset': {
                    'offset': offset,
                    'limit': limit,
                    'count': total_count
                },
            }
        }
        data = jsonify(rebuild)
        data.status_code = httplib.OK
        return data


class ItemReviewApi(controllers.Resource):

    def get(self, review_id):
        detail_review = Review.query.get(review_id)
        if not detail_review: return httplib.NOT_FOUND
        data = jsonify(detail_review.response_data())
        data.status_code = httplib.OK
        return data
