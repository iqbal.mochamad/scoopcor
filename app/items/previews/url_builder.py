from __future__ import absolute_import, unicode_literals

import os
from six import text_type

from . import preview_rule_factory

from app import app
from app.uploads.aws.helpers import aws_shared_link

__all__ = ('PreviewUrlBuilder', 'preview_s3_urls', 'preview_s3_covers', 'preview_s3_statics')


class PreviewUrlBuilder(object):

    def __init__(self, base_url):
        self.base_url = base_url

    def make_urls(self, item, as_implicit_href=False):
        """ Returns the URLs for 'pages' that users can access, even if they don't own an Item.

        If the Item cannot be previewed, an empty list will be returned.

        :param `app.items.models.Item` item:
        :param `bool` as_implicit_href: If true, will use the standard href format, otherwise response will be a list.
        :return: A list of stringified URLs.
        :rtype: list
        :raises: `TypeError` if the Item is not a book/magazine/newspaper.
        """

        if not item.page_count:
            return []
        else:
            rules = preview_rule_factory(item)

            if as_implicit_href:
                return [
                    {
                        'href': self.__build_single_url(item, page_num),
                        'title': text_type(page_num)
                    }
                    for page_num in rules.get_page_numbers()
                ]
            else:
                return [self.__build_single_url(item,page_num) for page_num in rules.get_page_numbers()]

    def __build_single_url(self, item, page_number):
        """ Construct a single web-reader page URL for an Item.

        :param `app.items.models.Item` item:
        :param `int` page_number:
        :return: A stringified URL
        :rtype: unicode
        """
        return "{base_url}/items/{item_id}/preview/{page_number}.jpg".format(
                base_url=self.base_url.replace('v1/', 'v1'),
                item_id=item.id,
                page_number=page_number)


def preview_s3_urls(item):

    previews, preview_format = [], []
    if not item.page_count:
        pass
    else:
        rules = preview_rule_factory(item)

        # https://s3-ap-southeast-1.amazonaws.com/ebook-previews/17/131806/2.jpg
        for i in rules.get_page_numbers():
            link_url = os.path.join(app.config['AWS_PUBLIC'], app.config['BOTO3_GRAMEDIA_PREVIEW_BUCKET'],
                str(item.brand_id), str(item.id), '{}.jpg'.format(i))
            previews.append(link_url)

    # if preview_format:
    #     for bucket_path in preview_format:
    #         preview_link = aws_shared_link(app.config['BOTO3_GRAMEDIA_WEBREADER_BUCKET'], bucket_path)
    #         if preview_link:
    #             link_url = preview_link.replace(app.config['BOTO3_NGINX_WEB_READER_BUCKET'], '')
    #             url = '{}{}/{}'.format('https://scoopadm.apps-foundry.com', app.config['NGINX_S3_WEB_READER'], link_url)
    #             previews.append(url)

    return previews

def preview_s3_covers(item=None, mode=None, is_full_path=False):

    OLD_PATH, bucket_path = 'images/1/', None

    if mode == 'image_normal' and item.image_normal:
        bucket_path = item.image_normal.replace(OLD_PATH, '')

    if mode == 'image_highres' and item.image_highres:
        bucket_path = item.image_highres.replace(OLD_PATH, '')

    if mode == 'thumb_image_normal' and item.thumb_image_normal:
        bucket_path = item.thumb_image_normal.replace(OLD_PATH, '')

    if mode == 'thumb_image_highres' and item.thumb_image_highres:
        bucket_path = item.thumb_image_highres.replace(OLD_PATH, '')

    # THIS IS FOR PUBLIC BUCKET SETTINGS..
    # https://s3-ap-southeast-1.amazonaws.com/ebook-covers/10/big_covers/ID_SME2012MTH04_B.jpg
    if bucket_path:
        if is_full_path:
            link_url = os.path.join(app.config['AWS_PUBLIC'], app.config['BOTO3_GRAMEDIA_COVERS_BUCKET'], bucket_path)
        else:
            link_url = bucket_path

        return link_url

    # THIS IS FOR PRIVATE BUCKET SETTINGS..
    # if bucket_path:
    #     cover_link = aws_shared_link(app.config['BOTO3_GRAMEDIA_COVERS_BUCKET'], bucket_path)
    #     if cover_link:
    #         link_url = cover_link.replace(app.config['BOTO3_NGINX_COVERS'], '')
	#
    #         if is_full_path:
    #             link_url = '{}{}/{}'.format('https://scoopadm.apps-foundry.com', app.config['NGINX_S3_COVERS'], link_url)
	#
    #         return link_url
	#
    # return ''


def preview_s3_statics(item=None, mode=None, is_full_path=True):

    bucket_path = None

    if mode == 'image_offers':
        bucket_path = '{}/{}'.format('offers', item.image_highres)

    if bucket_path:
        cover_link = aws_shared_link(app.config['BOTO3_GRAMEDIA_STATIC_BUCKET'], bucket_path)

        if cover_link:
            link_url = cover_link.replace(app.config['BOTO3_NGINX_STATICS'], '')

            if is_full_path:
                link_url = '{}{}/{}'.format('https://scoopadm.apps-foundry.com', app.config['NGINX_S3_STATICS'], link_url)
            return link_url
    return ''
