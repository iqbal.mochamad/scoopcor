from __future__ import unicode_literals
from abc import ABCMeta
from math import ceil

__all__ = ('preview_rule_factory',)


class PreviewUrlPageRules(object):

    __metaclass__ = ABCMeta

    def __init__(self, item):
        self.item = item

    def get_page_numbers(self):
        return range(1, self.MAXIMUM_PREVIEW_PAGE_NUMBER + 1)


class BookRules(PreviewUrlPageRules):
    """
    for Book:
        max allowed page = 10
        allowed page to preview: First Page + Middle Pages + Last Page
        Middle Pages = (page 10 to 10+n)
            where n = (5% of Total page) - 2, if n > 8 then n = 8

    Example :

    1. buku dgn total 100 halaman.
    5% x 100 = 5 halaman
    PREVIEW : 1 halaman pertama + 3 halaman mulai dari hal.10 + 1 halaman terakhir

    2. buku dgn total 500 halaman.
    5% x 500 = 25 halaman
    (if total > 10, then preview = 10 pages)
    PREVIEW : 1 halaman pertama + 8 halaman mulai dari hal.10 + 1 halaman terakhir

    3. buku dgn total 205 halaman
    5% x 205 = 10.25 halaman
    (if total > 10, then preview = 10 pages)
    PREVIEW : 1 halaman pertama + 8 halaman mulai dari hal.10 + 1 halaman terakhir

    4. buku dgn total 195 halaman
    5% x 195 = 9.75 halaman ~~ round up 10 halaman
    (if total > 10, then preview = 10 pages)
    PREVIEW : 1 halaman pertama + 8 halaman mulai dari hal.10 + 1 halaman terakhir

    5. buku dgn total 35 halaman.
    5% x 35 = 1.75 halaman ~~ round up 2 halaman
    PREVIEW : 1 halaman pertama + 1 halaman terakhir

    6. buku dgn total 50 halaman.
    5% x 35 = 2.5 halaman ~~ round up 3 halaman
    PREVIEW : 1 halaman pertama + hal. 10 + 1 halaman terakhir
    """
    BOOK_PREVIEW_LIMIT = 0.05
    MAXIMUM_BOOK_PREVIEW_LIMIT = 10

    def get_page_numbers(self):
        preview_urls = []

        first_page = [1]
        last_page = [self.item.page_count]

        # max_limit - 2 , cause 2 pages already taken by first page and last page
        max_middle_pages = self.MAXIMUM_BOOK_PREVIEW_LIMIT - 2

        total_middle_page_preview = int(ceil(self.item.page_count * self.BOOK_PREVIEW_LIMIT)) - 2

        if total_middle_page_preview >= max_middle_pages:
            total_middle_page_preview = max_middle_pages

        # get page 10 to 10+n
        middle_pages = range(self.MAXIMUM_BOOK_PREVIEW_LIMIT,
                             self.MAXIMUM_BOOK_PREVIEW_LIMIT + total_middle_page_preview)

        preview_urls = first_page + middle_pages + last_page

        return preview_urls


class MagazineRules(PreviewUrlPageRules):
    """
    for magazine: allowed page to preview: 5 first page + 1 last page
    """
    MAXIMUM_PREVIEW_PAGE_NUMBER = 5

    def get_page_numbers(self):
        page_numbers = super(MagazineRules, self).get_page_numbers()
        page_numbers.append(self.item.page_count)
        return page_numbers


class NewspaperRules(PreviewUrlPageRules):
    """
    for newspaper: allowed page to preview: First page only
    """
    MAXIMUM_PREVIEW_PAGE_NUMBER = 1

class AudioRules(PreviewUrlPageRules):
    MAXIMUM_PREVIEW_PAGE_NUMBER = 1

def preview_rule_factory(item):
    rules = {
        'magazine': MagazineRules,
        'newspaper': NewspaperRules,
        'book': BookRules,
        'audio book': AudioRules,
    }[item.item_type]

    return rules(item)
