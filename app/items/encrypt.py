import base64, os, paramiko

from datetime import datetime, timedelta
from random import randrange
from sqlalchemy.sql import desc

from Crypto.Cipher import AES

from app.items.models import DownloadItem, Item, ItemFile
from app.send_mail import sendmail_smtp, sendmail_aws

from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME
from app.utils.shims import item_types

from app import app

from app.uploads.aws.helpers import aws_shared_link


"""
    HOW TO TEST:

    from app.items.encrypt import EncryptData
    encrypt = EncryptData(link='http://kira-keren-bingits/cryptografi', item_id=1).construct()
    print encrypt

    decript = EncryptData(dec_key=encrypt).decrypt()
    print decript
"""

class EncryptData():

    def __init__(self, key=None, dec_key=None, item_id=None, url_base=None,
            url_file=None, user_id=None, itemfile_id=None, country_code=None, real_item=None):

        self.key = key
        self.salt = app.config['SALT_LINK']
        self.item_id = item_id
        self.master_item = real_item
        self.user_id = user_id
        self.itemfile_id = itemfile_id

        self.block_size = 16
        self.dec_key = dec_key
        self.url_base = url_base
        self.url_file = url_file
        self.country_code = country_code

        self.admin = app.config['OPERATIONAL_EMAILS']
        self.operator = app.config['OPERATOR_EMAILS']
        self.server = app.config['ADMIN_SERVER']

        self.data_iv = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
                    'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
                    'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E',
                    'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'O', 'P', 'Q',
                    'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']

        self.private_key = app.config['PRIVATE_KEY']
        self.ssh_username = app.config['SSH_USER_NAME']
        self.ssh_password = app.config['SSH_PASSWORD']

    def setup_ssh(self, server_name=None, port=None):
        server, port, username, password, rsa_private_key = (server_name, port, self.ssh_username, self.ssh_password, r'%s' % (self.private_key))
        priv_key = paramiko.RSAKey.from_private_key_file(rsa_private_key, password)

        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server, port, username = username,password = password, pkey = priv_key)
        return ssh

    def link_file_ssh(self, dir_name, file_name, new_file_name, path, download_path, server_name, ssh):
        try:
            # Create a new directory with write permission
            mkdir_command = 'mkdir %s%s' % (download_path, dir_name)
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(mkdir_command)

            # We have error
            if len(ssh_stderr.read().splitlines()) > 0:
                return False

            # remove 'images/1 from file_name'
            my_file = os.path.join(path, str(file_name).replace('images/1/', ''))

            #recheck data original exist or not
            ori_check = 'ln %s' % (my_file)
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(ori_check)

            file_exist = ssh_stderr.read().splitlines()[0].split(' ')[-1]
            if file_exist == 'directory':
                return False
            else:
                cp_command = 'ln -s %s %s%s/%s' % (my_file, download_path, dir_name, new_file_name)
                ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cp_command)

                if len(ssh_stderr.read().splitlines()) > 0 or file_name == '':
                    return False
                return True

        except Exception, e:
            return False

    def remove_file_ssh(ssh_62001=None, ssh_65001=None, additionals_link=None):

        remove_command_62001 = 'rm -rf %s%s' % (app.config['SERVER_62001_DOWNLOAD_PATH'], additionals_link)
        remove_command_65001 = 'rm -rf %s%s' % (app.config['SERVER_65001_DOWNLOAD_PATH'], additionals_link)

        # Delete directory
        try:
            ssh_stdin, ssh_stdout, ssh_stderr = ssh_62001.exec_command(remove_command_62001)
            ssh_stdin, ssh_stdout, ssh_stderr = ssh_65001.exec_command(remove_command_65001)
        except:
            pass

    def iv_generator(self):
        data_iv = []

        for iv in range(0, 16):
            random_index = randrange(0, len(self.data_iv))
            iv_char = self.data_iv[random_index]
            data_iv.append(iv_char)
        return ''.join(data_iv)

    def random_generator(self):
        data_iv = []

        for iv in range(0, 60):
            random_index = randrange(0, len(self.data_iv))
            iv_char = self.data_iv[random_index]
            data_iv.append(iv_char)
        return ''.join(data_iv)

    def connect_62(self, link_hash=None, file_hash=None):

        ssh_62001 = self.setup_ssh(app.config['SERVER_62001'], app.config['SERVER_62001_PORT'])
        is_62001_success = self.link_file_ssh(
            link_hash, self.url_file,
            file_hash,
            app.config['SERVER_62001_PATH'],
            app.config['SERVER_62001_DOWNLOAD_PATH'],
            app.config['SERVER_62001'],
            ssh_62001)
        ssh_62001.close()
        return is_62001_success

    def send_mail_server_invalid(self, stat=None):

        try:
            if stat == 1:
                subject = """
                    ERROR : (%s) Item ID %s can't be downloaded,
                    File Not founds in 62 or S3 Gramedia server""" % (
                    self.server, self.item_id)

            if stat == 2:
                subject = "WARNING : (%s) Item ID %s S3 server not public access" % (
                    self.server, self.item_id)

            content = """
                Hi Admin, <br><br>

                SCOOP_ID: %s <br><br>

                Item id: %s <br>
                Item path : %s <br>
                Time downloads : %s <br><br>

                Backend team - SALAM KOMPAK!! <br>
                MAY FORCE BE WITH YOU <br>
            """ % (
                self.user_id,
                self.item_id,
                self.url_file,
                datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
            )
            email_data = self.admin + self.operator

            try:
                #print 'send by aws.'
                sendmail_aws(subject, 'Gramedia Digital Download <no-reply@gramedia.com>', email_data, content)
            except:
                #print 'send by smtp'
                sendmail_smtp(subject, 'Gramedia Digital Download <no-reply@gramedia.com>', email_data, content)

        except Exception, e:
            pass

    def generate_link(self):
        ''' Rules: try for S3, if S3 not found, go to 62001 '''

        try:
            history_download = DownloadItem.query.filter_by(user_id=self.user_id,
                item_id = self.item_id, itemfile_id = self.itemfile_id).first()

            if history_download and history_download.valid_status():
                url_encryptbase = history_download.download_uri
                return url_encryptbase
            else:
                bucket = app.config['BOTO3_GRAMEDIA_MOBILE_BUCKET']
                file_bucket_path = '{}/{}.zip'.format(self.master_item.brand_id, self.master_item.edition_code)
                s3_encrypt_url = aws_shared_link(bucket, file_bucket_path)

                if s3_encrypt_url:
                    link_url = s3_encrypt_url.replace('https://ebook-mobilebundles.s3.amazonaws.com/', '')
                    url_link = '{}{}/{}'.format('https://scoopadm.apps-foundry.com',
                        app.config['NGINX_S3_MOBILE_BUNDLES'], link_url)
                    return url_link
                else:
                    link_hash, file_hash = self.random_generator(), '{}.zip'.format(self.random_generator())

                    # try for 62001 generator link
                    ssh_62001 = self.connect_62(link_hash, file_hash)
                    if ssh_62001:
                        encrypt_url = 'https://{}/download/{}/{}'.format(app.config['SERVER_62001'], link_hash, file_hash)

                        # save link 62001
                        history_download = DownloadItem(
                            user_id = self.user_id,
                            item_id = self.item_id,
                            itemfile_id = self.itemfile_id,
                            file_path = self.url_file,
                            download_uri = encrypt_url,
                            valid_from = datetime.now(),
                            valid_to = datetime.now() + timedelta(minutes=15)
                        )
                        history_download.save()
                        return encrypt_url
                    else:
                        pass
                        # send an email..
                        # self.send_mail_server_invalid(1)
            return None

        except Exception as e:
            return None

    def encrypt(self):
        # passphrase MUST be 16, 24 or 32 bytes long
        #IV = Random.new().read(self.block_size)
        IV = self.iv_generator()
        aes = AES.new(self.salt, AES.MODE_CBC, IV)
        link = '%s%s' % (IV, base64.b64encode(aes.encrypt(self.key)))
        return link

    def decrypt(self):
        #IV = Random.new().read(self.block_size)
        IV = self.dec_key[:16]
        aes = AES.new(self.salt, AES.MODE_CBC, IV)
        return aes.decrypt(base64.b64decode(self.dec_key[16:]))

    def construct(self):
        if self.dec_key:
            return self.decrypt()

        if self.key:
            return self.encrypt()

class RecheckServer():

    def __init__(self, limit=None, offset=None, itemtype_id=None, data_id=None):
        self.limit = limit
        self.offset = offset
        self.itemtype_id = itemtype_id
        self.data_id = data_id

        self.private_key = app.config['PRIVATE_KEY']
        self.ssh_username = app.config['SSH_USER_NAME']
        self.ssh_password = app.config['SSH_PASSWORD']

    def setup_ssh(self, server_name=None, port=None):
        server, port, username, password, rsa_private_key = (server_name, port, self.ssh_username, self.ssh_password, r'%s' % (self.private_key))
        priv_key = paramiko.RSAKey.from_private_key_file(rsa_private_key, password)

        ssh = paramiko.SSHClient()
        #paramiko.util.log_to_file(settings.SSH_LOG_FILE)
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(server, port, username = username,password = password, pkey = priv_key)
        return ssh

    def link_file_ssh(self, file_name=None, path=None, ssh=None):
        try:
            # remove 'images/1 from file_name'
            my_file = os.path.join(path, str(file_name).replace('images/1/', ''))

            #recheck data original exist or not
            ori_check = 'ln %s' % (my_file)
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(ori_check)

            file_exist = ssh_stderr.read().splitlines()[0].split(' ')[-1]
            if file_exist == 'directory':
                return 'FAILED', my_file
            return 'OK', my_file

        except Exception, e:
            return 'FAILED', str(e)

    def recheck_connect_62(self):
        ssh_62001 = self.setup_ssh(app.config['SERVER_62001'], app.config['SERVER_62001_PORT'])

        if self.data_id:
            data_item = Item.query\
                .filter(Item.id.in_(self.data_id))\
                .filter_by(
                    item_type=item_types.id_to_enum(self.itemtype_id),
                    item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                    is_active=True)\
                .order_by(desc(Item.release_date))\
                .all()
        else:
            data_item = Item.query\
                .filter_by(
                    item_type=item_types.id_to_enum(self.itemtype_id),
                    item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                    is_active=True)\
                .order_by(desc(Item.release_date))\
                .limit(self.limit)\
                .offset(self.offset)\
                .all()

        item_name = '62_lost_%s_%s.txt' % (self.limit, self.offset)
        file_rename = os.path.join('/tmp/', item_name)
        file_data = open(file_rename, 'wb')

        for item in data_item:
            itemfile = ItemFile.query.filter_by(item_id=item.id).first()
            if not itemfile:
                wrap_text = 'ITEM FILE NOT EXIST, item_id : %s' % item.id
                file_data.write('%s \n' % wrap_text)

            else:

                is_62001_success = self.link_file_ssh(itemfile.file_name,
                    app.config['SERVER_62001_PATH'], ssh_62001)

                if is_62001_success[0] == 'OK':
                    #wrap_text = 'OK [item_id : %s, edition_code : %s, path : %s ]' % (
                    #    item.id, item.edition_code, is_62001_success[1]
                    #)
                    print 'OK', item.id
                    #file_data.write('%s \n' % is_62001_success[1])
                else:
                    wrap_text = 'item_id : %s, edition_code : %s, path : %s' % (
                        item.id, item.edition_code, is_62001_success[1]
                    )

                    file_data.write('%s \n' % wrap_text)
                    print 'item_id : %s, name : %s' % (item.id, item.name)

        ssh_62001.close()
        file_data.close()

        #return is_62001_success
