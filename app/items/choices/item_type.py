from enum import Enum

MAGAZINE = 1
BOOK = 2
NEWSPAPER = 3
BONUS_ITEM = 4
AUDIOBOOK = 5

ITEM_TYPES = {
    MAGAZINE: 'magazine',
    BOOK: 'book',
    NEWSPAPER: 'newspaper',
    BONUS_ITEM: 'bonus item',
    AUDIOBOOK: 'audio book'
}

ITEM_TYPES_SHIMS = {
    'magazine': MAGAZINE,
    'book': BOOK,
    'newspaper': NEWSPAPER,
    'bonus item': BONUS_ITEM,
    'audio book': AUDIOBOOK
}

EDITION, MONTHLY = 1, 2
SCHEDULE = {
    EDITION : 'edition',
    MONTHLY : 'monthly'
}

class ItemTypes(Enum):
    magazine = 'magazine'
    book = 'book'
    newspaper = 'newspaper'
    bonus_item = 'bonus item'
    audio_book = 'audio book'
