"""
STATUS LEGEND :
    1: NEW - New item in preparation, this is the default status set to when item created if no other status specified
    2: READY FOR UPLOAD - Item required metadata completed, waiting for item's files (for digital type item) to upload
    3: UPLOADED - Item's files uploaded, ready for next action
    4: WAITING FOR REVIEW - Item metadata and files completed, waiting for reviewer action. This status only used if later vendor upload the items by their self and our internal reviewer will review it. This status will shows to vendor side so they know the item status
    5: IN REVIEW - Item in review by reviewer. This status will shows to vendor side so they know the item status
    6: REJECTED - Item rejected by reviewer. This status will shows to vendor side so they know the item status
    7: APPROVED - Item approved by reviewer. This status will shows to vendor side so they know the item status
    8: PREPARE FOR CONSUME - Item is in preparation to store. This status will shows to vendor side so they know the item status
    9: READY FOR CONSUME - Item is on store and ready for consumption
    10: NOT FOR CONSUME - Item is taken down from store for reasons. Users who owned the item still can see and download the item files. To disable the item completely, refer to is_active property
"""
STATUS_NEW = 1
STATUS_READY_FOR_UPLOAD = 2
STATUS_UPLOADED = 3
STATUS_WAITING_FOR_REVIEW = 4
STATUS_IN_REVIEW = 5
STATUS_REJECTED = 6
STATUS_APPROVED = 7
STATUS_PREPARE_FOR_CONSUME = 8
STATUS_READY_FOR_CONSUME = 9
STATUS_NOT_FOR_CONSUME = 10
STATUS_MCGRAWHILL_CONTENT = 11


STATUS_TYPES = {
    STATUS_NEW: 'new',
    STATUS_READY_FOR_UPLOAD: 'ready for upload',
    STATUS_UPLOADED: 'uploaded',
    STATUS_WAITING_FOR_REVIEW: 'waiting for review',
    STATUS_REJECTED: 'rejected',
    STATUS_IN_REVIEW: 'in review',
    STATUS_APPROVED: 'approved',
    STATUS_PREPARE_FOR_CONSUME: 'prepare for consume',
    STATUS_READY_FOR_CONSUME: 'ready for consume',
    STATUS_NOT_FOR_CONSUME: 'not for consume',
    STATUS_MCGRAWHILL_CONTENT: 'mcgrawhill content'
}

# allow lookups on either keys or values
#STATUS_TYPES.update({v:k for k,v in STATUS_TYPES.items()})
