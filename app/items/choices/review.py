from collections import OrderedDict

NEW = 'new'
ON_PROCESS = 'on_process'
PROCESSED = 'processed'
REVOKED = 'revoked'

SARA = 'sara'
HARSH_WORD = 'harsh_word'
REVIEW_UNRELATED = 'review_unrelated'
OTHER = 'other'

STATUS_REPORT = {
    NEW: 'new',
    ON_PROCESS: 'on_process',
    PROCESSED: 'processed',
    REVOKED: 'revoked'
}

REPORT_REASON = {
    SARA: 'sara',
    HARSH_WORD: 'harsh_word',
    REVIEW_UNRELATED: 'review_unrelated',
    OTHER: 'other'
}

REPORT_REASON_OPTION = OrderedDict()
REPORT_REASON_OPTION[SARA] = 'SARA'
REPORT_REASON_OPTION[HARSH_WORD] = 'Kata kasar / tidak pantas'
REPORT_REASON_OPTION[REVIEW_UNRELATED] = 'Tidak berhubungan dengan konten'
REPORT_REASON_OPTION[OTHER] = 'Lainnya'
