from enum import Enum


__all__ = ('ProcessingMode', )

class ProcessingMode(Enum):
    """ Processing mode for our web-reader-task-queue daemon.
    """
    normal = 'normal'
    high = 'high'
