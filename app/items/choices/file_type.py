"""
FILE TYPE LEGEND :
    1. PDF
    2. EPUB
    3. IMAGE
"""

FILE_PDF = 1
FILE_EPUB = 2
FILE_IMAGE = 3
FILE_AUDIO = 4

FILE_TYPES = {FILE_PDF: 'pdf',
              FILE_EPUB: 'epub',
              FILE_IMAGE: 'image',
              FILE_AUDIO: 'mp3'
}
