# This is just done to make this accessible from a more sane location
from __future__ import unicode_literals
from enum import IntEnum, Enum

from .file_type import *
from .status import *
from .text_direction import *
from .item_type import *
from .distribution_type import *
from .web_reader import *


class SubscriptionType(IntEnum):
    per_edition = 1
    daily = 2  # unused
    weekly = 3  # unused
    monthly = 4


class ItemUploadProcessStatus(IntEnum):
    new = 1
    processing = 2
    complete = 10
    failed = 11
    preview_web = 12

    # create_temp_path = 3
    # generate_password = 4
    # generate_covers = 5
    # send_cover_to_scoopadm = 6
    # generate_thumbs = 7
    # generate_zip_files = 8
    # update_item_files = 9
    # send_zip_to_62 = 10
    # complete = 11
    # failed = 12


class WebReaderTaskStatus(Enum):
    pending = 'pending'
    downloading = 'downloading'
    extracting_zip = 'extracting zip'
    creating_web_reader_images = 'creating web reader images'
    creating_thumbnail_images = 'creating thumbnail images'
    complete = 'complete'
    failed = 'failed'
