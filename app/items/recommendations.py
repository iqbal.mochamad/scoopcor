from __future__ import absolute_import, unicode_literals
import json

from sqlalchemy import desc

from app.items.models import Item
from app.master.models import Category


class RecommendationService(object):

    def __init__(self, redis, session):
        self.redis = redis
        self.session = session

    def save_item_document(self, item):
        """

        :param `app.items.models.Item` item:
        :return:
        """
        self.redis.set(
            "ITEM:{}:RECOMMENDATION_SUMMARY".format(item.id),
            json.dumps(item.extension_values(
                extension=['authors', 'brand', 'display_offers', ],
                custom='id,slug,item_type_id,name,issue_number,edition_code,'
                       'media_base_url,thumb_image_normal,thumb_image_highres,'
                       'display_offers,authors,brand,item_status,content_type,'
                       'web_reader_files_ready,restricted_countries,allowed_countries,is_active')
            )
        )

    def get_item_document(self, item_id):
        """

        :param `int` item_id:
        :return:
        """
        pass

    def generate_recommendations(self, item):
        if item.item_type == 'book':
            query = self.session.query(Item).filter(
                Item.id != item.id,
                Item.is_active == True,
                Item.item_type == 'book',
                Item.item_status == 'ready for consume',
                Category.id.in_([c.id for c in item.categories])
            )
        else:

            query = self.session.query(Item).filter(
                Item.brand != item.brand,
                Item.is_active == True,
                Item.item_type == item.item_type,
                Item.item_status == 'ready for consume',
                Category.id.in_([c.id for c in item.categories])
            ).order_by(desc(Item.release_date))


