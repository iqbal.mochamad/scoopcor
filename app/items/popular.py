from app import db
from datetime import datetime, timedelta
from sqlalchemy import select
from sqlalchemy.sql import func, desc

from app.items.models import item_category, Item, UserItem
from app.orders.models import OrderLine
from app.offers.models import offer_item, offer_brand, Offer


# get the SQLAlchemy table objects from our current metadata
core_orderlines = OrderLine.__table__
core_offers_items = offer_item
core_items_categories = item_category
core_items = Item.__table__
core_offers_brands = offer_brand
core_offers = Offer.__table__
core_useritems = UserItem.__table__

# set these to whatever you want
desired_category = 11
desired_itemtype = 'magazine'

conn = db.session.bind


def query_popular_items(desired_itemtype,desired_category=None, timespan=None, request_args=None):
    '''

    :param str desired_itemtype: Limits result set to only include Item IDs
        specified by this parameter ('book', 'magazine', or 'newspaper')
    :param int desired_category: If specified, limits result set to only include
        Items with the specified category id
        (see :py:class:`app.items.models.Item`)
    :param :py:class`datetime.timedelta` timespan:
    :return:
    '''

    timespan = timespan or timedelta(days=30)

    # For popular buffet ( orderline vs core_items)
    if request_args['is_buffet']:

        # get offer id buffets
        offer_buffets = Offer.query.filter_by(offer_type_id=4, offer_status=7, is_active=True).all()
        offer_ids = [i.id for i in offer_buffets]

        query = select([core_useritems.c.item_id, func.count()]).select_from(
            core_orderlines
                .join(core_useritems, core_orderlines.c.id == core_useritems.c.orderline_id)
                .join(core_items, core_useritems.c.item_id == core_items.c.id)
                .join(core_items_categories, core_items_categories.c.item_id == core_items.c.id)
        ).where(
            (core_orderlines.c.created > (datetime.now() - timespan)) &
            (core_items.c.item_type == desired_itemtype) &
            (core_items.c.item_status.in_(request_args['item_status'])) &
            (core_items.c.is_active == request_args['is_active']) &
            (core_orderlines.c.orderline_status == 90000) &
            (core_orderlines.c.offer_id.in_(offer_ids)) &
            (core_useritems.c.is_active == True)
        ).group_by(core_useritems.c.item_id).order_by(desc(func.count()))

    else:
        if desired_itemtype in ['magazine','book']:
            query = select([core_offers_items.c.item_id.label('item_id'), func.count()]).select_from(
                core_orderlines
                    .join(core_offers_items, core_offers_items.c.offer_id == core_orderlines.c.offer_id)
                    .join(core_items_categories, core_offers_items.c.item_id == core_items_categories.c.item_id)
                    .join(core_items, core_items.c.id == core_items_categories.c.item_id)
                    .join(core_offers, core_offers.c.id == core_offers_items.c.offer_id)
            ).where(
                (core_orderlines.c.created > (datetime.now() - timespan)) &
                (core_items.c.item_type == desired_itemtype) &
                (core_offers.c.is_active == request_args['is_active']) &
                (core_offers.c.is_free == False) &
                (core_items.c.item_status.in_(request_args['item_status'])) &
                (core_items.c.is_active == request_args['is_active']) &
                (core_orderlines.c.orderline_status == 90000)
            ).group_by(core_offers_items.c.item_id).order_by(desc(func.count()))

        elif desired_itemtype == 'newspaper':
            query = select([func.max(core_items.c.id).label('item_id'), func.count()]).select_from(
                core_orderlines
                    .join(core_offers_brands, core_offers_brands.c.offer_id == core_orderlines.c.offer_id)
                    .join(core_items, core_items.c.brand_id == core_offers_brands.c.brand_id)
                    .join(core_offers, core_offers.c.id == core_offers_brands.c.offer_id)
                    .join(core_items_categories, core_items.c.id == core_items_categories.c.item_id)
            ).where(
                (core_orderlines.c.created > (datetime.now() - timespan)) &
                (core_items.c.item_type == desired_itemtype) &
                (core_offers.c.is_active == request_args['is_active']) &
                (core_offers.c.is_free == False) &
                (core_items.c.item_status.in_(request_args['item_status'])) &
                (core_items.c.is_active == request_args['is_active']) &
                (core_orderlines.c.orderline_status == 90000)
            ).group_by(core_items.c.brand_id).order_by(desc(func.count()))

    if desired_category:
        query = query.where(core_items_categories.c.category_id.in_(desired_category))

    total_row = count_items(query)
    result_proxy = conn.execute(query)
    rows = [row['item_id'] for row in result_proxy]
    return rows, total_row


def count_items(query):
    result_proxy = conn.execute(query)
    return result_proxy.rowcount


