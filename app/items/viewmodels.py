import os

from flask import current_app
from flask import request
from flask_appsfoundry.viewmodels import SaViewmodelBase, ListViewmodel
from six import text_type

from app import db
from app.items.models import Item
from app.offers.helpers import get_discounted_price
from app.parental_controls.choices import ParentalControlType
from . import _log
from helpers import build_query

from app.items.previews import preview_s3_covers

class PopularViewmodel(SaViewmodelBase):
    @property
    def display_offers(self):
        platform_id = 4
        try:
            platform_id = request.args.get('display_offer_plid', type=int, default=4)
        except RuntimeError:
            _log.debug("RuntimeError fetching request arg platform_id")

        display_offers, display_price = self.item.get_offers(platform_id)
        return display_offers

    @property
    def authors(self):
        return self.item.get_authors()


class AuthorListViewModel(object):
    def __init__(self, query, args):
        query = build_query(query, args)
        self.authors = query['query']

        self.metadata = {'resultset':
                             {'count': query['query_count'],
                              'offset': args['offset'],
                              'limit': args['limit']}}


class ItemV3ViewModels(object):
    @staticmethod
    def item_brand_response_builder(response_from_solr):
        item_list = []
        session = db.session()

        for item in response_from_solr.get('response', {}).get('docs', {}):
            item_obj = session.query(Item).filter_by(id=item["id"]).first()

            item = item_obj
            if item and item.item_type == Item.Types.book.value:
                subtitle = ', '.join([author.name for author in item.authors])
                gtin14 = item.gtin14
            else:
                subtitle = item.issue_number if item else None
                gtin14 = ''

            if item:
                rv = {
                    "brand": {
                        "title": item.brand.name,
                        "href": item.brand.api_url,
                        "slug": item.brand.slug
                    },
                    "categories": [{
                        "title": category.name,
                        "href": category.api_url
                    } for category in item.categories],
                    "countries": item.countries,
                    "highlighting": {},
                    "id": item.id,
                    "title": item.name,
                    "subtitle": subtitle,
                    "href": item.api_url,
                    "type": item.item_type,
                    "slug": item.slug,
                    "is_age_restricted": True if item.parentalcontrol.id == ParentalControlType.parental_level_2.value else False,
                    "is_active": item.is_active,
                    "status": item.item_status,
                    "images": get_image_from_item(item),
                    "languages": item.languages,
                    "offers": get_offers_from_item(item),
                    "gtin14": gtin14,
                    "release_date": item.release_date,
                    "vendor": {
                        "title": item.brand.vendor.name,
                        "href": item.brand.vendor.api_url
                    }
                }
                item_list.append(rv)
        return item_list


def get_offers_from_item(item):
    offer_object = item.core_offers.all()
    try:
        return [dict(href=offer.api_url, id=offer.id, title=offer.long_name, type=offer.offer_type.name.lower(),
                     price=price_offer_build_response(offer))
                for offer in offer_object]
    except:
        raise


def price_offer_build_response(offer):
    # net_idr, net_usd, net_point, platform_type = get_discounted_price(offer=offer)
    return dict(
        idr=dict(base=round(float(offer.price_idr), 0), net=round(offer.price_idr, 0)),
        usd=dict(base=round(float(offer.price_usd), 2), net=round(offer.price_usd, 2)),
        point=dict(base=round(float(offer.price_point), 0), net=round(offer.price_point, 0))
    )


def get_image_from_item(item):
    try:
        # return [dict(rel="highres",
        #              href=current_app.config["MEDIA_BASE_URL"] + item.image_highres,
        #              tittle="Gambar Highres"),
        #         dict(rel="normal",
        #              href=current_app.config["MEDIA_BASE_URL"] + item.image_normal,
        #              tittle="Gambar Normal"),
        #         dict(rel="thumb_highres",
        #              href=current_app.config["MEDIA_BASE_URL"] + item.thumb_image_highres,
        #              tittle="Thumbnail Highres"),
        #         dict(rel="thumb_normal",
        #              href=current_app.config["MEDIA_BASE_URL"] + item.thumb_image_normal,
        #              tittle="Thumbnail Normal")]

        return [dict(rel="highres",
                     href=preview_s3_covers(item, 'image_highres', True),
                     tittle="Gambar Highres"),
                dict(rel="normal",
                     href=preview_s3_covers(item, 'image_normal', True),
                     tittle="Gambar Normal"),
                dict(rel="thumb_highres",
                     href=preview_s3_covers(item, 'thumb_image_highres', True),
                     tittle="Thumbnail Highres"),
                dict(rel="thumb_normal",
                     href=preview_s3_covers(item, 'thumb_image_normal', True),
                     tittle="Thumbnail Normal")]
    except:
        raise
