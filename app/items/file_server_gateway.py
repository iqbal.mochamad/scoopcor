import datetime
import os
import httplib
import boto3
from urlparse import urlparse

from botocore.exceptions import ClientError
from dateutil.tz import tzutc


class FileServerGateway(object):

    host = None

    def get_file_info(self, file_name):
        raise NotImplementedError("Please Implement this method")

    def is_file_exists(self, file_name):
        raise NotImplementedError("Please Implement this method")

    def file_access_status(self, file_name):
        raise NotImplementedError("Please Implement this method")

    def get_file_size(self, file_name):
        raise NotImplementedError("Please Implement this method")

    @staticmethod
    def date_to_str(dt):
        return dt.replace(tzinfo=tzutc()).strftime('%Y-%m-%d %H:%M:%S%z') if dt else ''


class AmazonS3ServerGateway(FileServerGateway):

    def __init__(self, s3, aws_server, aws_bucket_name):
        self.s3 = s3
        self.aws_server = aws_server
        self.aws_bucket_name = aws_bucket_name

    def get_file_info(self, file_name):
        is_exists = False
        read_access = 'private'
        error_message = ''
        last_modified_date = ''
        file_size = 0
        try:
            # for faster access, we only need to get the object from S3 once, then read the property
            s3_file = self.get_head_object(file_name)
            is_exists = True
            read_access = self.file_access_status(file_name=file_name)
            file_size = s3_file.get('ContentLength', 0) if s3_file else 0
            last_modified_date = self.__get_last_modified_date(s3_file)

        except Exception as e:
            error_message = 'Failed to read file in {host}, error: {error_message}'.format(
                host='s3',
                error_message=str(e)
            )
        finally:
            result = {
                'is_exists': is_exists,
                'read_access': read_access,
                'error_message': error_message,
                'file_size': file_size,
                'last_modified_date': last_modified_date,
            }

        return result

    def is_file_exists(self, file_name):
        try:
            # check file exists or not, if not exists, it will raise an error
            file_exist = self.get_head_object(file_name)
            return True
        except ClientError as ce:
            if ce.response['ResponseMetadata'].get('HTTPStatusCode') == 404:
                return False
            else:
                raise

    @staticmethod
    def __get_last_modified_date(s3_file):
        last_modified_date = s3_file.get('LastModified', '') if s3_file else ''
        return FileServerGateway.date_to_str(last_modified_date)

    def get_last_modified_date(self, file_name):
        try:
            # check file exists or not, if not exists, it will raise an error
            s3_file = self.get_head_object(file_name)
            return self.__get_last_modified_date(s3_file)
        except ClientError as ce:
            if ce.response['ResponseMetadata'].get('HTTPStatusCode') == 404:
                return ''
            else:
                raise

    def get_head_object(self, file_name):
        return self.s3.head_object(Bucket=self.aws_bucket_name, Key=file_name)

    def get_file_size(self, file_name):
        s3_file = self.get_head_object(file_name)
        return s3_file.get('ContentLength', 0) if s3_file else 0

    def file_access_status(self, file_name):
        # Check file exist access permision, public access or private access?
        # public access return 200, private access return 401 and 301

        buckets_file = '/{bucket_name}/{file_name}'.format(
            bucket_name=self.aws_bucket_name,
            file_name=file_name
        )

        parse_link = urlparse(self.aws_server)
        conn = httplib.HTTPConnection(parse_link.netloc)
        conn.request("HEAD", buckets_file)
        access_status = conn.getresponse().status

        if access_status == httplib.OK:
            return 'public'
        else:
            return 'private'

    def file_upload(self, item_file_full_path, key):
        if os.path.exists(item_file_full_path):
            try:
                self.s3.upload_file(
                    item_file_full_path,
                    self.aws_bucket_name,
                    key)
                # key value example = item_file.file_name
                return 'File uploaded to s3 with key={}'.format(key)
            except Exception as e:
                return 'Failed to upload to s3, error={}'.format(str(e))


class InternalFileServerGateway(FileServerGateway):

    def __init__(self, path):
        self.path = path

    def get_file_info(self, file_name):
        is_exists = False
        read_access = 'private'
        error_message = ''
        last_modified_date = ''
        file_size = 0
        try:
            is_exists = self.is_file_exists(file_name)
            read_access = self.file_access_status(file_name=file_name)
            file_size = self.get_file_size(file_name)
            last_modified_date = self.get_last_modified_date(file_name)
        except Exception as e:
            error_message = 'Failed to read file in {host}, error: {error_message}'.format(
                host='s3',
                error_message=str(e)
            )
        finally:
            result = {
                'is_exists': is_exists,
                'read_access': read_access,
                'error_message': error_message,
                'file_size': file_size,
                'last_modified_date': last_modified_date,
            }

        return result

    def is_file_exists(self, file_name):

        # remove 'images/1 from file_name'
        item_file_full_path = os.path.join(self.path, self.get_file_name(file_name))

        if os.path.isfile(item_file_full_path):
            return True
        else:
            return False

    @staticmethod
    def get_file_name(file_name):
        return str(file_name).replace('images/1/', '')

    def file_access_status(self, file_name):
        return 'public'

    def get_file_size(self, file_name):
        try:
            item_file_full_path = os.path.join(self.path, self.get_file_name(file_name))
            return os.path.getsize(item_file_full_path)
        except Exception as e:
            return 0

    def get_last_modified_date(self, file_name):
        try:
            item_file_full_path = os.path.join(self.path, self.get_file_name(file_name))
            dt = datetime.datetime.utcfromtimestamp(os.path.getmtime(item_file_full_path))
            return FileServerGateway.date_to_str(dt)
        except Exception as e:
            return ''
