from __future__ import unicode_literals

import json
from unittest import TestCase

from datetime import datetime, timedelta
from mockredis import mock_strict_redis_client

from app.auth.models import UserInfo
from app.items.web_reader import WebReaderErrorManager, WebReaderErrorReport
from dateutil import parser, tz

class ErrorReportTaskManagerTests(TestCase):

    maxDiff = None

    def test_insert_report(self):

        mock_redis = mock_strict_redis_client()

        mgr = WebReaderErrorManager(mock_redis)

        class FakeItem(object):
            def __init__(self, id):
                self.id = id

        report = WebReaderErrorReport(
            item=FakeItem(id=1234),
            user=UserInfo(username="foo", token="bar", user_id=9999, perm=["abc"]),
            message="Semua JELEK",
            pages=[1,2,3,4,5],
            report_id="ABCDEFG"
        )

        mgr.insert(report)

        redis_data = json.loads(mock_redis.get("WEB_READER_ERROR_REPORT:ABCDEFG"))

        report_time_iso8601 = parser.parse(redis_data.pop("report_time"))

        self.assertAlmostEqual(datetime.utcnow().replace(tzinfo=tz.tzutc()), report_time_iso8601, delta=timedelta(seconds=1))

        self.assertDictEqual(
            redis_data,
            {
                "item": {"id": 1234},
                "user": {"username": "foo", "user_id": 9999},
                "pages": [1, 2, 3, 4, 5, ],
                "report_id": "ABCDEFG",
                "message": "Semua JELEK",
            }
        )

