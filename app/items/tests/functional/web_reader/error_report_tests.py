from __future__ import unicode_literals

import json
from unittest import TestCase

from app.auth.models import UserInfo
from app import app, db, set_request_id
from flask import g
from mockredis import mock_strict_redis_client
from mock import patch
from dateutil import tz, parser
from datetime import datetime, timedelta

from tests.fixtures.sqlalchemy import ItemFactory


class CreateErrorReportTests(TestCase):

    def setUp(self):
        self.session = db.session()
        self.session.expire_on_commit = False
        self.test_item = ItemFactory()
        self.session.commit()

    @patch('app.app.kvs', mock_strict_redis_client())
    def test_create_error_report(self):

        mock_redis = app.kvs

        mock_redis.flushdb()

        self.assertEqual(len(mock_redis.keys("*")), 0, msg="Sanity test")

        client = app.test_client(use_cookies=False)

        resp = client.post(
            'v1/items/{0.id}/web-reader/error-report'.format(self.test_item),
            data=json.dumps({
                'message': 'OMG INI JELEK',
                'pages': [1, 2, 3, ],
            }),
            headers={
                'Accept': 'application/json',
                'Content-type': 'application/json',
                'Authorization': 'Bearer some-fake-bearer'
            })

        self.assertEqual(resp.status_code, 201, msg="Response is HTTP 201")
        self.assertEqual(resp.data, "", msg="Response body is empty")

        self.assertEqual(len(mock_redis.keys("*")), 1, msg="Something was persisted to redis")

        error_report = json.loads(mock_redis.get("WEB_READER_ERROR_REPORT:{}".format(FAKE_REQUEST_ID)))

        report_time = error_report.pop('report_time', None)
        self.assertAlmostEqual(
            parser.parse(report_time),
            datetime.utcnow().replace(tzinfo=tz.tzutc()),
            delta=timedelta(seconds=1)
        )

        self.assertDictEqual(error_report, {
            'message': 'OMG INI JELEK',
            'pages': [1, 2, 3, ],
            'user': {
                'user_id': 1234,
                'username': 'foo',
            },
            'item': {
                'id': self.test_item.id
            },
            'report_id': FAKE_REQUEST_ID
        }, msg='Error report persisted to Redis properly')


FAKE_REQUEST_ID = None
original_before_request_func = None


def setup_module():

    db.create_all()
    global original_before_request_func
    original_before_request_func = app.before_request_funcs

    app.before_request_funcs = {None: [
        fake_request_id,
        fake_authenticate_user_on_session,
    ]}


def teardown_module():
    db.session().close_all()
    db.drop_all()
    app.before_request_funcs = original_before_request_func


def fake_authenticate_user_on_session(*args, **kwargs):
    # noinspection PyBroadException
    g.user = UserInfo(username="foo", token="bar", user_id=1234,
                      perm="a b")


def fake_request_id(*args, **kwargs):
    set_request_id()
    global FAKE_REQUEST_ID
    FAKE_REQUEST_ID = g.request_id
