from __future__ import unicode_literals
from datetime import datetime
import json
from unittest import TestCase

from faker import Factory
from nose.tools import istest
from mockredis import mock_strict_redis_client
from six import text_type

from app import app
from app.items.web_reader import (
    ProcessingTaskManager, ProcessingTask, WebReaderTaskStatus)
from tests.testcases.redis import RedisUnitTestCase


class WebReaderProcessingTaskTests(TestCase):

    def test_from_json(self):
        """ ProcessingTask is deserialized from a JSON-object properly.
        """
        start_date = _fake.date_time()
        end_date = _fake.date_time()
        json_object = {
            'item_id': _fake.pyint(),
            'processing_start_time': start_date.isoformat(),
            'processing_end_time': end_date.isoformat()
        }
        task = ProcessingTask.from_json(json.dumps(json_object))

        self.assertEqual(task.item_id, json_object['item_id'])
        self.assertEqual(task.processing_start_time, start_date)
        self.assertEqual(task.processing_end_time, end_date)


@istest
class WebReaderProcessingTaskManagerTests(RedisUnitTestCase):

    redis_key_prefix = "WEB_READER_PROCESSING:"
    redis_instance = mock_strict_redis_client()

    def test_get_by_id(self):
        """ ProcessingTasks can be retrieved one at a time by their item_id
        """
        item_id = _fake.pyint()
        self.mgr.enqueue(ProcessingTask(item_id))

        task = self.mgr.get_by_item_id(item_id)

        self.assertEqual(task.item_id, item_id)
        self.assertIsNone(task.processing_start_time)
        self.assertIsNone(task.processing_end_time)

    def test_create_task(self):
        """ New ProcessingTasks can be inserted into redis by passing it to .enqueue()
        """
        item_id = _fake.pyint()
        self.mgr.enqueue(ProcessingTask(item_id))

        self.assertDictEqual(
            json.loads(self.redis_instance.get(self.redis_key_prefix + text_type(item_id))),
            {
                'item_id': item_id,
                'processing_start_time': None,
                'processing_end_time': None,
                'status': WebReaderTaskStatus.pending.name,
                'sub_task_status': None,
                'error_message': None,
                'is_high_priority': False,
                'notify_users': [],
            }
        )

    def test_update_task(self):
        """ Existing ProcessingTasks can be updated by passing it to .update()
        """
        item_id = _fake.pyint()
        task = self.mgr.enqueue(ProcessingTask(item_id))

        task.processing_start_time = datetime.now()
        task.processing_end_time = datetime.now()
        task.status = WebReaderTaskStatus.complete
        task.is_high_priority = True
        self.mgr.update(task)

        self.assertDictEqual(
            json.loads(self.redis_instance.get(self.redis_key_prefix + text_type(item_id))),
            {
                'item_id': item_id,
                'processing_start_time': task.processing_start_time.isoformat(),
                'processing_end_time': task.processing_end_time.isoformat(),
                'status': WebReaderTaskStatus.complete.name,
                'sub_task_status': None,
                'error_message': None,
                'is_high_priority': True,
                'notify_users': [],
            }
        )

    def test_delete_task(self):
        """ Existing ProcessingTasks can be deleted by passing it to .delete()
        """
        item_id = _fake.pyint()
        task = self.mgr.enqueue(ProcessingTask(item_id))
        self.mgr.delete(task)

        self.assertIsNone(self.mgr.get_by_item_id(item_id))

    def test_get_all_tasks(self):
        """ All tasks can be retrieved with .get_all_tasks()
        """
        # if exists somehow, just remove first, to prevent error DuplicateTaskError
        for key in self.mgr.redis.keys("{}:*".format(self.mgr.key)):
            self.mgr.redis.delete(key)

        # create some dummy task
        for task in [ProcessingTask(_fake.pyint()) for _ in range(20)]:
            self.mgr.enqueue(task)

        # test get_all_task method
        results = self.mgr.get_all_tasks()
        self.assertEqual(len(results), 20)

    def setUp(self):
        super(WebReaderProcessingTaskManagerTests, self).setUp()
        self.mgr = ProcessingTaskManager(self.redis_instance)


_fake = Factory.create()
client = app.test_client(use_cookies=False)

