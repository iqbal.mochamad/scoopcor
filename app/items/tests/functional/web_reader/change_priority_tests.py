import random
from unittest import SkipTest
from unittest import TestCase

from flask import g
from mock import MagicMock
from mockredis import mock_strict_redis_client
from redis import StrictRedis

from app import app, db, set_request_id
from app.auth.models import UserInfo
from app.items.choices import WebReaderTaskStatus
from app.items.web_reader import ProcessingTaskManager, ProcessingTask
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from app.utils.testcases import E2EBase

from tests.fixtures.sqlalchemy import ItemFactory


class ChangeProcessingTaskPriorityTests(E2EBase):
    """ If a task is queued, and still pending, when a web request is received,
    it should be marked as "is_high_priority=True"
    """
    def setUp(self):
        super(ChangeProcessingTaskPriorityTests, self).setUp()

        # create the fake item
        s = db.session()
        self.item = ItemFactory(content_type='pdf')
        s.commit()
        app.kvs.flushdb()
        self.mgr = ProcessingTaskManager(app.kvs)
        task = ProcessingTask(self.item.id)
        self.mgr.enqueue(task)

    def test_change_priority(self):
        # test original priority
        task = self.mgr.get_by_item_id(self.item.id)
        self.assertFalse(task.is_high_priority,
                         msg="task starts normal priority")

        self._authorizing_user = self.super_admin
        c = app.test_client(use_cookies=False)
        c.get('/v1/items/{}/web-reader/1.jpg'.format(self.item.id),
              headers=self.build_headers())

        db.session().add(self.item)
        task = self.mgr.get_by_item_id(self.item.id)
        self.assertTrue(task.is_high_priority,
                        msg="task was changed to high_priority")


class SkipHighPriorityTests(E2EBase):
    """ If a task is queued, and is ANY status other than pending, then
    it should NOT be marked as "is_high_priority".

    This is because another task queue has already begun to run this job!
    """

    def setUp(self):
        super(SkipHighPriorityTests, self).setUp()

        # create the fake item
        s = db.session()
        self.item = ItemFactory(content_type='pdf')
        s.commit()
        app.kvs.flushdb()
        self.mgr = ProcessingTaskManager(app.kvs)
        task = ProcessingTask(self.item.id)
        task.status = random.choice([
            st for st in WebReaderTaskStatus
            if st != WebReaderTaskStatus.pending])

        self.mgr.enqueue(task)

    def test_priority_unchanged_after_web_request(self):
        task = self.mgr.get_by_item_id(self.item.id)
        self.assertFalse(task.is_high_priority,
                         msg="task starts normal priority")

        c = app.test_client(use_cookies=False)
        c.get('/v1/items/{}/web-reader/1.jpg'.format(self.item.id))
        db.session().add(self.item)
        task = self.mgr.get_by_item_id(self.item.id)
        self.assertFalse(task.is_high_priority,
                         msg="task is_high_priority cannot be changed for task "
                             "that is not 'pending'")


class NoPreviousTaskTests(E2EBase):
    """ If a task is queued, and is ANY status other than pending, then
    it should NOT be marked as "is_high_priority".

    This is because another task queue has already begun to run this job!
    """

    def setUp(self):
        super(NoPreviousTaskTests, self).setUp()
        # create the fake item
        s = db.session()
        s.expire_on_commit = False
        self.item = ItemFactory(content_type='pdf')
        s.commit()
        app.kvs.flushdb()
        self.mgr = ProcessingTaskManager(app.kvs)

    def test_item_immediately_is_high_priority(self):
        self.assertIsNone(self.mgr.get_by_item_id(self.item.id),
                          msg="Verify there is no task queued for this item")

        self._authorizing_user = self.super_admin
        c = app.test_client(use_cookies=False)
        c.get('/v1/items/{}/web-reader/1.jpg'.format(self.item.id),
              headers=self.build_headers())

        db.session().add(self.item)
        task = self.mgr.get_by_item_id(self.item.id)
        self.assertTrue(task.is_high_priority,
                        msg="Task is immediately created with "
                            "'is_high_priority=True'")

