from __future__ import unicode_literals, absolute_import

from unittest import SkipTest

from PIL import Image
from faker import Factory
from httplib import OK, FORBIDDEN
from shutil import rmtree
import os
from nose.tools import istest, nottest
from os import makedirs, unlink, path
from flask import g

from app import app, db, set_request_id
from app.auth.models import UserInfo
from app.items.choices import WebReaderTaskStatus
from app.items.web_reader import ProcessingTaskManager, \
    get_web_reader_content_pages_directory
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from tests.fixtures.sqlalchemy.items import ItemFactory
from tests import testcases


@istest
class SecurityTests(testcases.DetailTestCase):
    """
    Test unknown user to access an item that he didn't owned
    """

    fixture_factory = ItemFactory
    request_url = '/v1/items/{0.id}/web-reader/1.jpg'
    request_headers = {'Accept': 'image/jpeg'}

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        item = ItemFactory(content_type='pdf')
        s.commit()
        app.before_request_funcs = {
            None: [
                set_request_id,
                fake_un_authorized_user,
            ]}
        return item

    @classmethod
    def remove_fixtures_from_session(cls):
        pass

    def test_response_body(self):
        return

    def test_response_code(self):
        # Forbidden, not allowed to access cause item not owned
        self.assertEqual(FORBIDDEN, self.response.status_code)


@istest
class ApiTests(testcases.DetailTestCase):
    """
    Test web reader API - file exists in web_reader folders and redis task queue is completed
    """

    fixture_factory = ItemFactory
    request_url = '/v1/items/{0.id}/web-reader/1.jpg'
    request_headers = {'Accept': 'image/jpeg'}

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        item = ItemFactory(content_type='pdf')
        s.commit()
        cls.create_fake_webreader_image(item)
        app.before_request_funcs = {
            None: [
                set_request_id,
                fake_authorized_user,
            ]}
        return item

    @classmethod
    def remove_fixtures_from_session(cls):
        pass

    def test_response_body(self):
        return

    def test_response_status_code(self):
        self.assertEqual(OK, self.response.status_code)

    def test_link_headers(self):
        self.assertIn('link', self.response.headers)

    def test_response_content_type(self):
        self.assertEqual(self.response.content_type, 'image/jpeg')

    def test_x_accel_redirect(self):
        self.assertIn('x-accel-redirect', self.response.headers)

    def test_content_length(self):
        # self.assertEqual(self.response.content_length, '')
        pass

    @classmethod
    def create_fake_webreader_image(cls, item):
        with app.app_context():
            web_reader_fixtures_dir = os.path.join(os.path.dirname(__file__).replace('web_reader', ''),
                                                   'fixtures', 'web_reader')
            expected_directory = get_web_reader_content_pages_directory(
                item.id, item.brand_id)
            try:
                makedirs(expected_directory)
            except:
                pass
            for i in range(1, 6):
                image = Image.new('RGB', (i, 10))
                image.save("{}/{}.jpg".format(expected_directory, i))

            # flag web reader processing task in redis to completed
            mgr = ProcessingTaskManager(app.kvs)
            task = mgr.get_or_create_by_item_id(item.id, item.brand_id, ignore_existing=True)
            task.status = WebReaderTaskStatus.complete
            mgr.update(task)

    @classmethod
    def tearDownClass(cls):
        cls.remove_fake_item_pages_and_directory()
        super(ApiTests, cls).tearDownClass()

    @classmethod
    def remove_fake_item_pages_and_directory(cls):
        with app.app_context():
            for i in range(1, 6):
                unlink(path.join(
                    get_web_reader_content_pages_directory(cls.fixture.id, cls.fixture.brand_id),
                    "{}.jpg".format(i)))
            rmtree(os.path.abspath(
                os.path.join(get_web_reader_content_pages_directory(cls.fixture.id, cls.fixture.brand_id),
                             os.pardir)))


_fake = Factory.create()
client = app.test_client(use_cookies=False)
original_before_request_func = None


def clear_redis():
    pass


def setup_module():
    # make sure the test mode flag is set, or bail out
    if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
        raise SkipTest("COR is not in testing mode.  Cannot continue.")

    db.session().close_all()
    db.drop_all()
    db.create_all()
    global original_before_request_func
    original_before_request_func = app.before_request_funcs
    user = UserFactory(id=1234)
    db.session.add(user)
    db.session.commit()
    # g.current_user = db.session.query(User).get(1234)


def teardown_module():
    app.before_request_funcs = original_before_request_func
    db.session().close_all()
    db.drop_all()


@nottest
def fake_authorized_user(*args, **kwargs):
    g.user = UserInfo(username="ramdani@apps-foundry.com",
                      user_id=1234,
                      token='abcd',
                      perm=['can_read_write_global_all', 'b', 'c', ])
    g.current_user = db.session.query(User).get(1234)


@nottest
def fake_un_authorized_user(*args, **kwargs):
    g.user = UserInfo(username="randomusertest@apps-foundry.com",
                      user_id=1234,
                      token='abcd',
                      perm=['a', 'b', 'c', ])
    g.current_user = db.session.query(User).get(1234)
