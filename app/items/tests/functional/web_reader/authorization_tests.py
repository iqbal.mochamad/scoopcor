from datetime import datetime, timedelta
from unittest import TestCase

from app import db
from app.auth.models import UserInfo
from app.items.helpers import ItemOwnershipStatus, \
    UserItemRepository, get_current_ownership_status
from app.offers.choices.status_type import READY_FOR_SALE
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy.items import ItemFactory
from tests.fixtures.sqlalchemy.offers import OfferBuffetFactory, OfferFactory, \
    standard_offer_types, OfferSingleFactory
from tests.fixtures.sqlalchemy.orders import OrderLineFactory
from app.users.tests.fixtures import UserFactory
from app.users.tests.fixtures_user_buffet import UserBuffetFactory


class UserOwnsItemTests(TestCase):
    """ BUGFIX: https://github.com/appsfoundry/scoopcor/issues/128

    Users with a buffet, but haven't yet downloaded an item on Mobile,
    should have that item accessible from their 'User Items'.
    """
    def test_permanent_ownership(self):

        user = UserInfo(username="dj@scoop.com",
                        user_id=9999,
                        token="abcd",
                        perm=[])

        item = ItemFactory()

        single_offer = OfferSingleFactory(
            offer=OfferFactory(
                offer_status=READY_FOR_SALE,
                offer_type=standard_offer_types()['single'],
                items=[item, ]
            )
        )

        order_line_item = OrderLineFactory(user_id=user.user_id, quantity=1, offer=single_offer.offer)

        UserItemRepository(user, self.session).create(item, order_line_item)

        self.assertEqual(
            get_current_ownership_status(user.user_id, item, self.session),
            ItemOwnershipStatus.permanent_ownership)

    def test_active_buffet(self):
        user = UserFactory(id=9999, username="dj@scoop.com", email="dj@scoop.com",)
        user_info = UserInfo(username="dj@scoop.com",
                        user_id=9999,
                        token="abcd",
                        perm=[])

        item = ItemFactory()

        buffet_offer = OfferBuffetFactory(
            available_from=datetime.utcnow()-timedelta(days=31),
            available_until=datetime.utcnow()-timedelta(days=1),
            offer=OfferFactory(
                offer_status=READY_FOR_SALE,
                offer_type=standard_offer_types()['buffet'],
                items=[item, ]
            )
        )

        order_line_item = OrderLineFactory(user_id=user.id, quantity=1, offer=buffet_offer.offer)

        user_buffet = UserBuffetFactory(
            user_id=user.id,
            valid_to=datetime.utcnow() + timedelta(days=30),
            orderline=order_line_item,
            offerbuffet=buffet_offer
        )

        self.session.commit()

        UserItemRepository(user_info, self.session).create(item, order_line_item)

        self.assertEqual(
            get_current_ownership_status(user_info.user_id, item, self.session),
            ItemOwnershipStatus.buffet_ownership)

    def test_expired_buffet(self):
        user = UserFactory(id=9999, username="dj@scoop.com", email="dj@scoop.com", )
        user_info = UserInfo(username="dj@scoop.com",
                        user_id=9999,
                        token="abcd",
                        perm=[])

        item = ItemFactory()

        buffet_offer = OfferBuffetFactory(
            available_from=datetime.utcnow()-timedelta(days=31),
            available_until=datetime.utcnow()-timedelta(days=1),
            offer=OfferFactory(
                offer_status=READY_FOR_SALE,
                offer_type=standard_offer_types()['buffet'],
                items=[item, ]
            )
        )

        order_line_item = OrderLineFactory(user_id=user.id, quantity=1, offer=buffet_offer.offer)

        user_buffet = UserBuffetFactory(
            user_id=user.id,
            valid_to=datetime.utcnow() - timedelta(days=2),
            orderline=order_line_item,
            offerbuffet=buffet_offer
        )

        self.session.commit()

        UserItemRepository(user_info, self.session).create(item, order_line_item)

        self.assertEqual(
            get_current_ownership_status(user_info.user_id, item, self.session),
            ItemOwnershipStatus.does_not_own)

    def test_unowned_item(self):

        user = UserInfo(username="dj@scoop.com",
                        user_id=9999,
                        token="abcd",
                        perm=[])

        item = ItemFactory()

        OfferSingleFactory(
            offer=OfferFactory(
                offer_status=READY_FOR_SALE,
                offer_type=standard_offer_types()['single'],
                items=[item, ]
            )
        )

        self.assertEqual(
            get_current_ownership_status(user.user_id, item, self.session),
            ItemOwnershipStatus.does_not_own)

    def setUp(self):
        db.create_all()
        self.session = db.session()
        generate_static_sql_fixtures(self.session)

    def tearDown(self):
        self.session.close_all()
        db.drop_all()
