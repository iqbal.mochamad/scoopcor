from unittest import SkipTest
from unittest import TestCase

from flask import g
from mockredis import mock_strict_redis_client

from app import app, db, set_request_id
from app.auth.models import UserInfo
from app.items.web_reader import ProcessingTaskManager, ProcessingTask
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from tests.fixtures.sqlalchemy import ItemFactory


class UserAddedToNotifyUsers(TestCase):
    """ If a task is queued, and still pending, when a web request is received,
    it should be marked as "is_high_priority=True"
    """

    def setUp(self):
        app.kvs = mock_strict_redis_client()

        # create the fake item
        s = db.session()
        s.expire_on_commit = False
        self.item = ItemFactory(content_type='pdf')
        s.commit()

        self.mgr = ProcessingTaskManager(app.kvs)
        task = ProcessingTask(self.item.id)
        self.mgr.enqueue(task)

    def test_user_included_in_notify_users(self):
        c = app.test_client(use_cookies=False)
        c.get('/v1/items/{}/web-reader/1.jpg'.format(self.item.id))

        task = self.mgr.get_by_item_id(self.item.id)

        self.assertEqual(len([user_info for user_info in task.notify_users if
                              user_info.user_id == 1234 and
                              user_info.username == 'ramdani@apps-foundry.com']),
                         1)

    def test_user_not_duplicated_in_notify_users(self):
        """

        :return:
        """

        c = app.test_client(use_cookies=False)
        for _ in range(3):
            c.get('/v1/items/{}/web-reader/1.jpg'.format(self.item.id))

        task = self.mgr.get_by_item_id(self.item.id)

        self.assertEqual(len([user_info for user_info in task.notify_users if
                              user_info.user_id == 1234 and
                              user_info.username == 'ramdani@apps-foundry.com']),
                         1)


original_before_request_func = None


def setup_module():
    # make sure the test mode flag is set, or bail out
    if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
        raise SkipTest("COR is not in testing mode.  Cannot continue.")

    db.session().close_all()
    db.drop_all()
    db.create_all()
    global original_before_request_func
    original_before_request_func = app.before_request_funcs
    app.before_request_funcs = {
        None: [
            set_request_id,
            fake_authorized_user,
        ]}
    user = UserFactory(id=1234)
    db.session.add(user)
    db.session.commit()


def teardown_module():
    db.session.expunge_all()
    db.session().close_all()
    db.drop_all()
    app.before_request_funcs = original_before_request_func


def fake_authorized_user(*args, **kwargs):
    g.user = UserInfo(username="ramdani@apps-foundry.com",
                      user_id=1234,
                      token='abcd',
                      perm=['can_read_write_global_all', 'b', 'c', ])
    g.current_user = db.session.query(User).get(1234)

