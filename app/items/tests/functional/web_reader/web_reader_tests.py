from __future__ import unicode_literals, absolute_import

from unittest import SkipTest

from PIL import Image
from faker import Factory
from httplib import EXPECTATION_FAILED, PROCESSING, OK
from shutil import rmtree, copyfile
import os
from flask import g
from mock import MagicMock
from mockredis import mock_strict_redis_client
from nose.tools import istest, nottest
from os import makedirs, unlink, path

from redis import StrictRedis

from app import app, db, set_request_id
from app.auth.models import UserInfo
from app.items.models import Item
from app.items.web_reader import ProcessingTaskManager, \
    get_web_reader_content_pages_directory
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from tests.fixtures.sqlalchemy.items import ItemFactory
from tests import testcases


@istest
class WebReaderJobEnqueueingTests(testcases.RedisTestCase):
    """ Requests for a valid item, that doesn't yet have web-reader files,
    should automatically enqueue a rendering job.

    - Requests for jobs that are already enqueued SHOULD not enqueue a new job.
    - GET requests for queued jobs should return
        - HTTP 418
    """
    def clear_all_items(self):
        s = db.session()
        for item in s.query(Item):
            s.delete(item)
        s.commit()

    def create_item_fixture(self):
        s = db.session()
        s.expire_on_commit = False
        self.item = ItemFactory(id=101, content_type='pdf')
        s.commit()
        return self.item

    def get_api_response(self, headers=None):
        headers = headers or {
                'Accept': 'image/jpeg',
                'Expect': '100-continue',
            }
        r = client.get(
            '/v1/items/{0.id}/web-reader/1.jpg'.format(self.item),
            headers=headers)
        return r

    def setUp(self):
        # app.kvs already mocked in setup_module
        self.redis_instance = app.kvs
        super(WebReaderJobEnqueueingTests, self).setUp()
        self.item = self.create_item_fixture()

    def tearDown(self):
        super(WebReaderJobEnqueueingTests, self).tearDown()
        self.clear_all_items()

    def test_job_queued(self):
        self.get_api_response()
        db.session().add(self.item)
        mgr = ProcessingTaskManager(self.redis_instance)
        task = mgr.get_by_item_id(self.item.id)

        self.assertIsNotNone(task)
        self.assertEqual(task.item_id, self.item.id)
        self.assertIsNone(task.processing_start_time)
        self.assertIsNone(task.processing_end_time)

    def test_response(self):
        response = self.get_api_response({'Accept': 'image/jpeg'})
        self.assertEqual(response.status_code, 418)

    #
    # @patch('app.items.api_v1.web_reader.assert_web_reader_files_ready')
    # def test_files_available(self, mock):
    #     response = client.get(
    #         '/v1/items/{0.id}/web-reader/1'.format(self.item),
    #         headers={
    #             'Accept': 'image/jpeg',
    #             'Expect': '100-continue',
    #         })
    #     self.assertEqual(response.status_code, CONTINUE)


@istest
class WebReaderTests(testcases.DetailTestCase):

    fixture_factory = ItemFactory
    request_url = '/v1/items/{0.id}/web-reader/1.jpg'
    request_headers = {'Accept': 'image/jpeg'}

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        s.expire_on_commit = False
        item = ItemFactory(content_type='pdf')
        s.commit()
        cls.create_fake_webreader_image(item)
        return item

    @classmethod
    def remove_fixtures_from_session(cls):
        pass

    def test_response_body(self):
        return

    def test_response_status_code(self):
        self.assertEqual(OK, self.response.status_code)

    def test_link_headers(self):
        self.assertIn('link', self.response.headers)

    def test_response_content_type(self):
        self.assertEqual(self.response.content_type, 'image/jpeg')

    def test_x_accel_redirect(self):
        self.assertIn('x-accel-redirect', self.response.headers)

    def test_content_length(self):
        self.assertEqual(self.response.content_length, 0)

    # @classmethod
    # def setUpClass(cls):
    #     super(WebReaderTests, cls).setUpClass()

    @classmethod
    def create_fake_webreader_image(cls, item):
        with app.app_context():
            web_reader_fixtures_dir = os.path.join(os.path.dirname(__file__).replace('web_reader', ''),
                                                   'fixtures', 'web_reader')
            expected_directory = get_web_reader_content_pages_directory(
                item.id, item.brand_id)
            try:
                rmtree(os.path.abspath(
                    os.path.join(get_web_reader_content_pages_directory(item.id, item.brand_id),
                                 os.pardir)))
            except:
                pass
            makedirs(expected_directory)
            for i in range(1, 6):
                image = Image.new('RGB', (i, 3))
                image.save("{}/{}.jpg".format(expected_directory, i))

    @classmethod
    def tearDownClass(cls):
        cls.remove_fake_item_pages_and_directory()
        super(WebReaderTests, cls).tearDownClass()

    @classmethod
    def remove_fake_item_pages_and_directory(cls):
        with app.app_context():
            for i in range(1, 6):
                unlink(path.join(
                    get_web_reader_content_pages_directory(cls.fixture.id, cls.fixture.brand_id),
                    "{}.jpg".format(i)))
            rmtree(os.path.abspath(
                os.path.join(get_web_reader_content_pages_directory(cls.fixture.id, cls.fixture.brand_id),
                             os.pardir)))

_fake = Factory.create()
client = app.test_client(use_cookies=False)
original_before_request_func = None
original_app_kvs = app.kvs


def clear_redis():
    pass


def setup_module():
    # make sure the test mode flag is set, or bail out
    if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
        raise SkipTest("COR is not in testing mode.  Cannot continue.")

    db.session().close_all()
    db.drop_all()
    db.create_all()
    app.kvs = mock_strict_redis_client()
    global original_before_request_func
    original_before_request_func = app.before_request_funcs
    app.before_request_funcs = {
        None: [
            set_request_id,
            fake_authorized_user,
        ]}
    user = UserFactory(id=1234)
    db.session.add(user)
    db.session.commit()


def teardown_module():
    app.before_request_funcs = original_before_request_func
    app.kvs = original_app_kvs

    db.session.expunge_all()
    db.session().close_all()
    db.drop_all()


def fake_authorized_user(*args, **kwargs):
    g.user = UserInfo(username="ramdani@apps-foundry.com",
                      user_id=1234,
                      token='abcd',
                      perm=['can_read_write_global_all', 'b', 'c', ])
    g.current_user = db.session.query(User).get(1234)
