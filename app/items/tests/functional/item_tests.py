"""
These tests exist to validate the functionality of the Items API.
It should be noted that these tests are EXTREMELY fragile and need
to be refactored so that they are always run against a known copy
of the database.

It would also likely be quite a good idea to mock out the token_required
call as well, or to create a mock service to run it against.. whichever
is better.
"""
import unittest
import json
import os

from app import app, db
from app.constants import RoleType
from app.items.models import Item
from app.offers.choices.offer_type import SINGLE
from app.offers.models import OfferType
from tests.fixtures.helpers import create_user_and_token, generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.offers import OfferTypeFactory


class ItemApiV1_HTTP_GET_Tests(unittest.TestCase):

    maxDiff = None
    STANDARD_RESPONSE = json.load(open(os.path.join(os.path.dirname(__file__),
                                               "fixtures",
                                               "item_details",
                                               "full_standard.json")))

    @classmethod
    def setUpClass(cls):
        session = db.session()
        generate_static_sql_fixtures(session)
        user, token = create_user_and_token(session, RoleType.super_admin)
        cls.request_headers = {'Authorization': 'Bearer {}'.format(token.token),
                               'Accept': 'application/json'}
        brand = BrandFactory(id=212)
        offer_type = session.query(OfferType).get(SINGLE)
        offer = OfferFactory(offer_type=offer_type, is_free=True, price_idr=0, price_usd=10, price_point=10)
        offer.brands.append(brand)
        item = ItemFactory(id=59500, item_type='book', parentalcontrol_id=1, revision_number='123', brand=brand)
        session.commit()
        # create some authors
        # create some categories

    def assert_dict_keys(self, resp_json, expected):
        for key in expected:
            self.assertIn(key, resp_json)

    def test_http_get_returns_expected(self):

        expected = self.STANDARD_RESPONSE

        with app.test_client() as c:
            resp = c.get('/v1/items/59500',
                         headers=self.request_headers)

            self.assertEqual(resp.status_code, 200)

            resp_json = json.loads(resp.data)
            self.assert_dict_keys(resp_json, expected)

    def test_expand_languages_returns_expected(self):

        expected = json.load(open(os.path.join(os.path.dirname(__file__),
                                               "fixtures",
                                               "item_details",
                                               "subset_expand_languages.json")))
        with app.test_client() as c:
            resp = c.get('/v1/items/59500?expand=languages',
                         headers=self.request_headers)

            self.assertEqual(resp.status_code, 200)

            resp_json = json.loads(resp.data)
            # self.assertDictContainsSubset(expected, resp_json)

            expected.update(self.STANDARD_RESPONSE)
            self.assert_dict_keys(resp_json, expected)

    def test_expand_categories_returns_expected(self):

        expected = json.load(open(os.path.join(os.path.dirname(__file__),
                                               "fixtures",
                                               "item_details",
                                               "subset_expand_categories.json")))
        with app.test_client() as c:
            resp = c.get('/v1/items/59500?expand=categories',
                         headers=self.request_headers)

            self.assertEqual(resp.status_code, 200)

            resp_json = json.loads(resp.data)
            self.assert_dict_keys(resp_json, expected)
            # self.assertDictContainsSubset(expected, resp_json)

            expected.update(self.STANDARD_RESPONSE)
            self.assert_dict_keys(resp_json, expected)

    def test_expand_brand_returns_expected(self):

        expected = json.load(open(os.path.join(os.path.dirname(__file__),
                                               "fixtures",
                                               "item_details",
                                               "subset_expand_brand.json")))
        with app.test_client() as c:
            resp = c.get('/v1/items/59500?expand=brand',
                         headers=self.request_headers)

            self.assertEqual(resp.status_code, 200)

            resp_json = json.loads(resp.data)
            self.assert_dict_keys(resp_json, expected)
            # self.assertDictContainsSubset(expected, resp_json)

            expected.update(self.STANDARD_RESPONSE)
            self.assert_dict_keys(resp_json, expected)

    def test_expand_countries_returns_expected(self):

        expected = json.load(open(os.path.join(os.path.dirname(__file__),
                                               "fixtures",
                                               "item_details",
                                               "subset_expand_countries.json")))
        with app.test_client() as c:
            resp = c.get('/v1/items/59500?expand=countries',
                         headers=self.request_headers)

            self.assertEqual(resp.status_code, 200)

            resp_json = json.loads(resp.data)
            self.assert_dict_keys(resp_json, expected)
            # self.assertDictContainsSubset(expected, resp_json)

            expected.update(self.STANDARD_RESPONSE)
            self.assert_dict_keys(resp_json, expected)

    def test_expand_item_type_returns_expected(self):

        expected = json.load(open(os.path.join(os.path.dirname(__file__),
                                               "fixtures",
                                               "item_details",
                                               "subset_expand_item_type.json")))
        with app.test_client() as c:
            resp = c.get('/v1/items/59500?expand=item_type',
                         headers=self.request_headers)

            self.assertEqual(resp.status_code, 200)

            resp_json = json.loads(resp.data)
            # self.assertDictContainsSubset(expected, resp_json)

            expected.update(self.STANDARD_RESPONSE)
            self.assert_dict_keys(resp_json, expected)

    def test_expand_vendor_returns_expected(self):

        expected = json.load(open(os.path.join(os.path.dirname(__file__),
                                               "fixtures",
                                               "item_details",
                                               "subset_expand_vendor.json")))
        with app.test_client() as c:
            resp = c.get('/v1/items/59500?expand=vendor',
                         headers=self.request_headers)

            self.assertEqual(resp.status_code, 200)

            resp_json = json.loads(resp.data)
            self.assert_dict_keys(resp_json, expected)
            # self.assertDictContainsSubset(expected, resp_json)

            expected.update(self.STANDARD_RESPONSE)
            self.assert_dict_keys(resp_json, expected)

    def test_expand_parental_control_returns_expected(self):
        expected = json.load(open(os.path.join(os.path.dirname(__file__),
                                               "fixtures",
                                               "item_details",
                                               "subset_expand_parental_control.json")))
        with app.test_client() as c:
            resp = c.get('/v1/items/59500?expand=parental_control',
                         headers=self.request_headers)

            self.assertEqual(resp.status_code, 200)

            resp_json = json.loads(resp.data)
            self.assert_dict_keys(resp_json, expected)
            # self.assertDictContainsSubset(expected, resp_json)

            expected.update(self.STANDARD_RESPONSE)
            self.assert_dict_keys(resp_json, expected)

    def test_expand_display_offers_returns_expected(self):

        expected = json.load(open(os.path.join(os.path.dirname(__file__),
                                               "fixtures",
                                               "item_details",
                                               "subset_expand_display_offers.json")))
        with app.test_client() as c:
            resp = c.get('/v1/items/59500?expand=display_offers',
                         headers=self.request_headers)

            self.assertEqual(resp.status_code, 200)

            resp_json = json.loads(resp.data)
            self.assert_dict_keys(resp_json, expected)
            # self.assertDictContainsSubset(expected, resp_json)

    def test_expand_ext_returns_expected(self):

        expected = json.load(open(os.path.join(os.path.dirname(__file__),
                                               "fixtures",
                                               "item_details",
                                               "full_expand_ext.json")))
        with app.test_client() as c:
            resp = c.get('/v1/items/59500?expand=ext',
                         headers=self.request_headers)

            self.assertEqual(resp.status_code, 200)

            resp_json = json.loads(resp.data)
            self.assert_dict_keys(resp_json, expected)


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
