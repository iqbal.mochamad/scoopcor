import unittest
import json
from datetime import datetime, timedelta
from urllib import urlencode

from mockredis import mock_strict_redis_client

from app import app, db
from app.items.api import ItemLatest
from app.items.choices import STATUS_READY_FOR_CONSUME
from app.items.choices import STATUS_TYPES
from tests.fixtures.sqlalchemy.items import ItemFactory


class ItemLatestRedisTests(unittest.TestCase):

    maxDiff = None
    client = app.test_client(use_cookies=False)
    headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
    }

    original_app_kvs = app.kvs

    @classmethod
    def setUpClass(cls):
        # mock the redis
        cls.original_app_kvs = app.kvs
        app.kvs = mock_strict_redis_client()
        db.create_all()
        s = db.session()

        cls.items = []
        today = datetime.today().replace(day=21)
        for i in range(20):
            cls.items.append(ItemFactory(
                release_date=today,
                is_active=True,
                item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]
            ))
            today = today - timedelta(days=1)

        s.commit()

        cls.args = {'offset': 0, 'limit': 10}

        cls.resp = cls.client.get('/v1/items/latest?{params}'.format(params=urlencode(cls.args)), headers=cls.headers)
        cls.result = json.loads(cls.resp.data)

    @classmethod
    def tearDownClass(cls):
        db.session.close_all()
        db.drop_all()
        # restore original redis
        app.kvs = cls.original_app_kvs

    def test_response_status_code(self):
        self.assertEqual(200, self.resp.status_code)

    def test_response_content_type(self):
        self.assertEqual('application/json', self.resp.content_type)

    def test_parameter(self):
        self.assertEqual(self.args['limit'], self.result['metadata']['resultset']['limit'])
        self.assertEqual(self.args['offset'], self.result['metadata']['resultset']['offset'])

    def test_different_offset1(self):
        self.args = {'offset': 10, 'limit': 10}

        self.resp = self.client.get('/v1/items/latest?{params}'.format(params=urlencode(self.args)), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(self.args['limit'], self.result['metadata']['resultset']['limit'])
        self.assertEqual(self.args['offset'], self.result['metadata']['resultset']['offset'])
        self.assertEqual(self.args['limit'], len(self.result['items']))

    def test_different_offset2(self):
        self.args = {'offset': 0, 'limit': 10}

        self.resp = self.client.get('/v1/items/latest?{params}'.format(params=urlencode(self.args)), headers=self.headers)
        self.result = json.loads(self.resp.data)

        # expected = [item.release_date.isoformat() for item in self.items[0:10]]

        self.assertEqual(self.args['limit'], self.result['metadata']['resultset']['limit'])
        self.assertEqual(self.args['offset'], self.result['metadata']['resultset']['offset'])
        self.assertEqual(self.args['limit'], len(self.result['items']))
        # self.assertListEqual(expected, [item['release_date'] for item in self.result['items']])

    def test_check_item_latest_in_redis(self):
        expected = 10
        total_redis_item_latest = app.kvs.keys('{}*'.format(ItemLatest().PER_ITEM_CACHE_KEY))
        self.assertEqual(len(total_redis_item_latest), expected)
