from datetime import datetime, timedelta
from unittest import TestCase

from nose.tools import istest

from app import db
from app.items.choices import SubscriptionType
from app.helpers import DateRange
from app.master.models import Currencies
from app.offers.choices.offer_type import SINGLE, SUBSCRIPTIONS
from app.offers.models import OfferType
from app.orders.choices import COMPLETE
from app.payment_gateways.models import PaymentGateway
from app.payments.choices import CHARGED
from app.reports.user_subscriptions import get_latest_user_subscription, get_user_ids_with_subscriptions, \
    get_user_subscribed_brands, SingleUserSubscriptionsReport
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.offers import OfferTypeFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.orders import PaymentGatewayFactory
from tests.testcases.database import SATestCaseMixin
from tests.fixtures.sqlalchemy.items import UserSubscriptionFactory
from tests.fixtures.sqlalchemy.master import BrandFactory


user_benny = 1
user_aditya = 2

fhm_brand_id = None
popular_brand_id = None


@istest
class HelperFunctionTests(SATestCaseMixin, TestCase):

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        generate_static_sql_fixtures(s)

        global fhm_brand_id, popular_brand_id

        popular_brand = BrandFactory()
        fhm_brand = BrandFactory()

        s.commit()
        fhm_brand_id = fhm_brand.id
        popular_brand_id = popular_brand.id

        offer_type = s.query(OfferType).get(SUBSCRIPTIONS)
        offer = OfferFactory(offer_type=offer_type, is_free=True, price_idr=0, price_usd=10, price_point=10)

        order_id = 123455
        cur_idr = s.query(Currencies).get(1)
        pg_kredivo = s.query(PaymentGateway).get(1)
        order = OrderFactory(id=order_id, paymentgateway=pg_kredivo,
                             total_amount=offer.price_idr, final_amount=offer.price_idr,
                             user_id=1122,
                             order_status=COMPLETE)
        order_line = OrderLineFactory(order=order, offer=offer, user_id=1122, currency_code='IDR',
                                      price=offer.price_idr, final_price=offer.price_idr,
                                      orderline_status=COMPLETE)


        return [
            # Create Benny's Subscriptions
            UserSubscriptionFactory(
                user_id=user_benny,
                quantity_unit=SubscriptionType.monthly.value,
                brand=popular_brand,
                valid_to=datetime.now() + timedelta(days=15),
                modified=datetime.now() + timedelta(days=15),
                orderline_id=order_line.id,
                is_active=True),
            UserSubscriptionFactory(
                user_id=user_benny,
                quantity_unit=SubscriptionType.monthly.value,
                brand=popular_brand,
                valid_to=datetime.now() - timedelta(days=15),
                modified=datetime.now() - timedelta(days=15),
                orderline_id=order_line.id,
                is_active=True),
            UserSubscriptionFactory(
                user_id=user_benny,
                quantity_unit=SubscriptionType.monthly.value,
                brand=popular_brand,
                valid_to=datetime.now() - timedelta(days=45),
                modified=datetime.now() - timedelta(days=45),
                orderline_id=order_line.id,
                is_active=True),

            UserSubscriptionFactory(
                user_id=user_benny,
                quantity_unit=SubscriptionType.per_edition.value,
                current_quantity=0,
                brand=fhm_brand,
                modified=datetime.now()-timedelta(days=15),
                orderline_id=order_line.id,
                is_active=True
            ),
            UserSubscriptionFactory(
                user_id=user_benny,
                quantity_unit=SubscriptionType.per_edition.value,
                current_quantity=0,
                brand=fhm_brand,
                modified=datetime.now()-timedelta(days=45),
                orderline_id=order_line.id,
                is_active=True
            ),

            UserSubscriptionFactory(
                user_id=user_benny,
                quantity_unit=SubscriptionType.per_edition.value,
                current_quantity=0,
                brand=popular_brand,
                modified=datetime.now()-timedelta(days=75),
                orderline_id=order_line.id,
                is_active=True
            )

        ]

    def test_get_user_ids_with_subscriptions(self):
        users_with_subs = get_user_ids_with_subscriptions(db.session())
        self.assertEqual([r[0] for r in users_with_subs], [1, ])

    def test_get_user_subscribed_brands(self):
        subs_and_types = get_user_subscribed_brands(user_benny, db.session())
        self.assertItemsEqual(
            [
                (popular_brand_id, SubscriptionType.monthly.value),
                (fhm_brand_id, SubscriptionType.per_edition),
                (popular_brand_id, SubscriptionType.per_edition),
            ],
            [(r.brand_id, r.quantity_unit) for r in subs_and_types]
        )

    def test_get_latest_user_subscriptions(self):

        sub = get_latest_user_subscription(
            user_benny,
            popular_brand_id,
            SubscriptionType.monthly.value,
            db.session())

        self.assertEqual(
            sub.user_id,
            user_benny)
        self.assertEqual(
            sub.brand_id,
            popular_brand_id)
        self.assertEqual(
            sub.quantity_unit,
            SubscriptionType.monthly)
        # allow for some fuzziness
        self.assertAlmostEqual(
            sub.valid_to,
            datetime.now()+timedelta(days=15),
            delta=timedelta(minutes=10))

    def test_report_model(self):
        report = SingleUserSubscriptionsReport(user_benny, db.session())
        expired_subs = report.get_expired_subscriptions(
            expired_between=DateRange(
                datetime.now()-timedelta(days=16), datetime.now())
        )

        self.assertEqual(len([_ for _ in expired_subs]), 1)


def setup_module():
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
