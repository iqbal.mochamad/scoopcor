from __future__ import unicode_literals

from httplib import OK
from unittest import TestCase
import json

import mock
from flask import current_app


from app import db, app
from app.items.models import Item
from app.items.viewmodels import get_offers_from_item, get_image_from_item
from app.parental_controls.choices import ParentalControlType
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import AuthorFactory
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferTypeFactory


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    return MockResponse(mock_solr_response(), 200)


def mock_solr_response():
    return {
        "response": {
            "docs": [{
                "id":"1634",
                "languages":["ind"],
                "price_ios_idr":59000.0,
                "price_web_usd":3.99,
                "vendor_name":"Elex Media Komputindo",
                "item_name":["1000+ Kejayaan Sains Muslim"],
                "brand_name":"1000+ Kejayaan Sains Muslim",
                "countries":["id"],
                "category_names":["Children's"],
                "price_android_usd":3.99,
                "price_web_idr":59000.0,
                "item_thumb_highres":"https://images.apps-foundry.com/magazine_static/images/1/32862/small_covers/ID_EMK2016MTH05KSM_S.jpg",
                "price_ios_usd":3.99,
                "is_age_restricted": "true",
                "price_android_idr":59000.0
            }],
            "numFound": 1,
        },
        "facet_counts": {
            "facet_queries": {},
            ""
            "facet_fields": {
                "item_categories":[
                    "Fiction & Literature", 2,
                    "Reference", 1
                ],
                "author_names": [
                    "buddy", 2
                ],
                "item_languages":[
                    "id", 2,
                    "en", 0
                ]
            }
        },
        "spellcheck": {
            "suggestions": [
                "temop", {
                    "numFound": 2,
                    "startOffset": 0,
                    "endOffset": 7,
                    "origFreq": 0,
                    "suggestion": [
                        {
                            "word": "tempo",
                            "freq": 8
                        },
                        {
                            "word": "temon",
                            "freq": 1
                        }
                    ]
                }
            ],
            "correctlySpelled": False
        },
        "highlighting": {
            "1634": {
                "content": [
                    "COLD WAR SHADOW United States Policy Toward <em>Indonesia</em>  1953-1963 Baskara T. Wardaya COLD"]
            },
        }
    }


class ItemV3GetTest(TestCase):
    maxDiff = None

    @classmethod
    @mock.patch('app.items.api.requests.get', mock.Mock(side_effect=mocked_requests_get))
    def setUpClass(cls):
        cls.app_client = app.test_client(use_cookies=False)

        cls.response = cls.app_client.get('v1/items?q=indonesia', headers={'accept': "application/vnd.scoop.v3+json"})

    def test_status_code(self):
        self.assertEqual(self.response.status_code, OK)

    def test_response_body_items(self):
        response = self.response
        item = db.session.query(Item).filter_by(id=1634).first()
        with app.test_request_context('/'):
            if item and item.item_type == Item.Types.book.value:
                subtitle = ', '.join([author.name for author in item.authors])
            else:
                subtitle = item.issue_number if item else None
            expected = {
                "brand": {
                    "title": item.brand.name,
                    "href": item.brand.api_url,
                    "slug": item.brand.slug
                },

                "categories": [{
                    "title": category.name,
                    "href": category.api_url
                } for category in item.categories],
                "countries": item.countries,
                "highlighting": {},
                "id": item.id,
                "title": item.name,
                "subtitle": subtitle,
                "href": item.api_url,
                "type": item.item_type,
                "slug": item.slug,
                "is_age_restricted": True if item.parentalcontrol.id == ParentalControlType.parental_level_2.value else False,
                "is_active": item.is_active,
                "status": item.item_status,
                "images": get_image_from_item(item),
                "languages": item.languages,
                "offers": get_offers_from_item(item),
                "vendor": {
                    "title": item.brand.vendor.name,
                    "href": item.brand.vendor.api_url
                }
            }
        self.assertDictEqual(expected, json.loads(response.data).get("items")[0])
        self.assertTrue(expected.get('offers')[0].get('type').islower())


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()
    init_data()


def teardown_module():
    db.session().close_all()
    db.drop_all()


def init_data():
    s = db.session()
    s.expire_on_commit = False
    generate_static_sql_fixtures(s)
    author = AuthorFactory()
    item1 = ItemFactory(id=1634)
    item1.parentalcontrol_id = 1
    item1.authors.append(author)
    offer = OfferFactory(offer_type_id=1)
    offer.items.append(item1)

    s.commit()
