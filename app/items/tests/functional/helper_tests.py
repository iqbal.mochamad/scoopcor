from __future__ import unicode_literals, print_function, absolute_import

from datetime import timedelta, datetime

from nose.tools import istest

from app import db, app
from app.items.helpers import get_latest_items_for_brand, get_all_latest_editions, get_dist_country_filter
from app.utils.datetimes import get_utc_nieve
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy.items import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory


@istest
class LatestItemsForBrandTests(TestBase):

    def test_default_results(self):
        items = get_latest_items_for_brand(db.session(), self.fixtures[0].id)
        self.assertEqual(len(items), 4)

    def test_limiting(self):
        items = get_latest_items_for_brand(db.session(), self.fixtures[0].id, max_results=1)
        self.assertEqual(len(items), 1)

    def test_max_age(self):
        items = get_latest_items_for_brand(db.session(), self.fixtures[0].id, max_age=timedelta(days=3))
        # would expect 3, but 2 because small amount of time variance between
        # fixtures created, and now.
        self.assertEqual(len(items), 2)

    def test_result_ordering(self):
        # list should be sorted in descending order by release_date
        last_age = None
        for item in get_latest_items_for_brand(db.session(), self.fixtures[0].id):
            if last_age is not None:
                self.assertLess(item.release_date, last_age)
            last_age = item.release_date

    @classmethod
    def setUpClass(cls):
        super(LatestItemsForBrandTests, cls).setUpClass()
        with cls.session.no_autoflush:
            cls.fixtures = [BrandFactory(), BrandFactory()]
            item_id = 100
            for brand in cls.fixtures:
                for i in range(1, 5):
                    item_id += 1
                    ItemFactory(id=item_id,
                                brand=brand,
                                is_active=True,
                                release_date=(datetime.now()-timedelta(days=i)),
                                item_status='ready for consume')
            cls.session.commit()


@istest
class GetLatestEditionForWebReaderProcessingTests(TestBase):

    def test_default_results(self):
        items = get_all_latest_editions(
            session=db.session(),
            max_results=100,
            last_min_item_id=None
        )
        self.assertEqual(len(items), 6)

    def test_limiting(self):
        items = get_all_latest_editions(
            session=db.session(),
            max_results=3,
            last_min_item_id=None
        )
        self.assertEqual(len(items), 3)

    def test_limiting_by_item_id(self):
        items = get_all_latest_editions(
            session=db.session(),
            max_results=100,
            last_min_item_id=3
        )
        self.assertEqual(len(items), 2)

    @classmethod
    def setUpClass(cls):
        super(GetLatestEditionForWebReaderProcessingTests, cls).setUpClass()
        cls.fixtures = cls.set_up_fixtures()

    @classmethod
    def set_up_fixtures(cls):
        fixtures = []
        item_id = 0
        modified = get_utc_nieve() - timedelta(minutes=100)
        with cls.session.no_autoflush:
            for i in range(1, 3):
                item_id += 1
                item = ItemFactory(
                    id=item_id,
                    is_active=True,
                    release_date=(datetime.now()-timedelta(days=i)),
                    item_status='ready for consume',
                    item_type='magazine',
                    content_type='pdf',
                    modified=modified,
                    page_count=None)
                fixtures.append(item)
            for i in range(1, 2):
                item_id += 1
                item = ItemFactory(
                    id=item_id,
                    is_active=True,
                    release_date=(datetime.now()-timedelta(days=i)),
                    item_status='ready for consume',
                    item_type='book',
                    content_type='pdf',
                    modified=modified,
                    page_count=None)
                fixtures.append(item)
            for i in range(1, 4):
                item_id += 1
                item = ItemFactory(
                    id=item_id,
                    is_active=True,
                    release_date=(datetime.now()-timedelta(days=i)),
                    item_status='ready for consume',
                    item_type='newspaper',
                    content_type='pdf',
                    modified=modified,
                    page_count=None)
                fixtures.append(item)
            cls.session.commit()
        return fixtures


class CustomGetTests(TestBase):

    def test_get_dist_country_filter(self):
        with app.test_request_context('/v1/items', headers={
            'Accept': 'application/json',
            'User-Agent': 'scoop_ios/v1.1.0',
            # simulate header by GeoIP extension in Nginx
            'GEOIP_COUNTRY_CODE': 'AU',
        }):
            dist_country_filter = get_dist_country_filter()
            self.assertIsNotNone(dist_country_filter)

        with app.test_request_context('/v1/items', headers={
            'Accept': 'application/json',
            'User-Agent': 'scoop_ios/v1.1.0',
            # simulate header by GeoIP extension in Nginx
            'GEOIP_COUNTRY_CODE': 'ID',
        }):
            dist_country_filter = get_dist_country_filter()
            self.assertIsNone(dist_country_filter)
