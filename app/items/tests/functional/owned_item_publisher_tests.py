from __future__ import unicode_literals, absolute_import

from unittest import TestCase

from flask import g

from app import db, app
from app.auth.helpers import jwt_encode_from_user
from app.auth.models import UserInfo
from app.constants import RoleType
from app.items import helpers
from app.items.choices import STATUS_READY_FOR_CONSUME
from app.items.choices import STATUS_TYPES
from app.items.helpers import ItemOwnershipStatus
from app.items.models import UserItem
from app.users.tests.fixtures import RoleFactory, UserFactory, OrganizationFactory
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import VendorFactory, BrandFactory


class UserPublisherOwnershipFuncTests(TestCase):

    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.session().close_all()
        db.drop_all()
        db.create_all()
        cls.session = db.session()
        cls.session.expire_on_commit = False
        role_publisher = RoleFactory(id=RoleType.organization_specific_admin.value)
        role_verified = RoleFactory(id=RoleType.verified_user.value)
        cls.org_publisher = OrganizationFactory(id=3311)
        cls.user_publisher = UserFactory(id=4411)
        cls.user_publisher.organizations.append(cls.org_publisher)
        cls.user_publisher.roles.append(role_publisher)
        org_other = OrganizationFactory(id=3312)
        cls.user_others = UserFactory(id=4412)
        cls.user_others.organizations.append(org_other)
        cls.user_verified = UserFactory(id=4413)
        cls.user_verified.roles.append(role_verified)
        cls.user_verified.organizations.append(cls.org_publisher)

        vendor_publisher = VendorFactory(organization_id=cls.org_publisher.id)
        brand_publisher = BrandFactory(vendor=vendor_publisher)
        cls.item_publisher = ItemFactory(id=9911, brand=brand_publisher, is_active=True,
                                         item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME])
        brand_publisher2 = BrandFactory()
        cls.item_other = ItemFactory(id=9912, brand=brand_publisher2, is_active=True,
                                     item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME])
        cls.session.commit()

        ctx = app.test_request_context('/')
        ctx.push()

    @staticmethod
    def set_current_user_roles_and_orgs(user):
        g.current_user = user
        g.current_user_roles = [role.id for role in user.roles.all()]

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.drop_all()

    def test_assert_user_publisher_valid(self):
        g.current_user = self.user_publisher
        self.set_current_user_roles_and_orgs(self.user_publisher)
        actual = helpers.assert_item_vs_user_publisher(
            item=self.item_publisher,
        )
        self.assertEqual(actual, ItemOwnershipStatus.publisher_ownership)

    def test_assert_not_same_organization(self):
        self.set_current_user_roles_and_orgs(self.user_others)
        actual = helpers.assert_item_vs_user_publisher(
            item=self.item_publisher,
        )
        self.assertEqual(actual, ItemOwnershipStatus.does_not_own)

    def test_assert_not_user_publisher_role(self):
        self.set_current_user_roles_and_orgs(self.user_verified)
        actual = helpers.assert_item_vs_user_publisher(
            item=self.item_publisher,
        )
        self.assertEqual(actual, ItemOwnershipStatus.does_not_own)

    def test_assert_user_not_authorized(self):
        # user not authorized..current user empty
        g.current_user_roles = None
        actual = helpers.assert_item_vs_user_publisher(
            item=self.item_publisher,
        )
        self.assertEqual(actual, ItemOwnershipStatus.does_not_own)


def get_jwt_valid_token(user):
    with app.test_request_context('/'):
        return jwt_encode_from_user(user)
