from __future__ import unicode_literals, absolute_import

from faker import Factory
from nose.tools import istest
from httplib import FORBIDDEN

from app import app, db
from tests.fixtures.sqlalchemy.items import ItemFactory
from tests import testcases


class BasePreviewApiTests(testcases.DetailTestCase):

    fixture_factory = ItemFactory
    request_url = '/v1/items/{0.id}/preview/1'
    request_headers = {'Accept': 'image/jpeg'}

    @classmethod
    def set_up_fixtures(cls):
        return ItemFactory(id=500, page_count=500, item_type="book", content_type='pdf')

    def test_response_content_type(self):
        self.assertEqual(self.response.content_type, "image/jpeg")

    def test_response_body(self):
        self.assertEqual(self.response.data, "")

    def test_link_headers(self):
        self.assertIn('link', self.response.headers)

    def test_x_accel_redirect(self):
        self.assertIn('x-accel-redirect', self.response.headers)

    def test_content_length(self):
        self.assertEqual(self.response.content_length, 0)


class BasePreviewForbidenApiTests(testcases.DetailTestCase):
    """ For preview web reader, only certain page allowed (based on certain formulas, here we tests forbidden pages

    """
    fixture_factory = ItemFactory
    request_url = '/v1/items/{0.id}/preview/30'
    request_headers = {'Accept': 'image/jpeg'}

    @classmethod
    def set_up_fixtures(cls):
        return ItemFactory(id=500, page_count=500, item_type="book", content_type='pdf')

    def test_response_content_type(self):
        self.assertEqual(self.response.content_type, "application/json")

    def test_response_body(self):
        return

    def test_response_code(self):
        self.assertEqual(FORBIDDEN, self.response.status_code)


@istest
class BookPreviewApiTests(BasePreviewApiTests):

    request_url = '/v1/items/{0.id}/preview/1'

    @classmethod
    def set_up_fixtures(cls):
        return ItemFactory(id=500, page_count=500, item_type="book", content_type='pdf')


@istest
class BookPreviewForbidenPageApiTests(BasePreviewForbidenApiTests):

    request_url = '/v1/items/{0.id}/preview/20'

    @classmethod
    def set_up_fixtures(cls):
        return ItemFactory(id=500, page_count=500, item_type="book", content_type='pdf')


@istest
class MagazinePreviewApiTests(BasePreviewApiTests):

    request_url = '/v1/items/{0.id}/preview/1'

    @classmethod
    def set_up_fixtures(cls):
        return ItemFactory(id=30, page_count=30, item_type="magazine", content_type='pdf')


@istest
class MagazinePreviewSecondPageApiTests(BasePreviewApiTests):

    request_url = '/v1/items/{0.id}/preview/2'

    @classmethod
    def set_up_fixtures(cls):
        return ItemFactory(id=30, page_count=30, item_type="magazine", content_type='pdf')


@istest
class MagazinePreviewForbidenApiTests(BasePreviewForbidenApiTests):

    request_url = '/v1/items/{0.id}/preview/6'

    @classmethod
    def set_up_fixtures(cls):
        return ItemFactory(id=30, page_count=30, item_type="magazine", content_type='pdf')


@istest
class NewspaperPreviewApiTests(BasePreviewApiTests):

    @classmethod
    def set_up_fixtures(cls):
        return ItemFactory(id=30, page_count=30, item_type="newspaper", content_type='pdf')


@istest
class NewspaperPreviewForbidenApiTests(BasePreviewForbidenApiTests):

    request_url = '/v1/items/{0.id}/preview/20'

    @classmethod
    def set_up_fixtures(cls):
        return ItemFactory(id=30, page_count=30, item_type="newspaper", content_type='pdf')


_fake = Factory.create()
client = app.test_client(use_cookies=False)


def setup_module():
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
