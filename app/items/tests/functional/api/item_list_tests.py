from __future__ import unicode_literals, absolute_import
from unittest import TestCase

from app import app

from mock import patch

from app.items.api import ItemListApi


@patch('app.items.api.ItemListApi._get_version_3')
@patch('app.items.api.ItemListApi._get_version_2')
@patch('app.items.api.ItemListApi._get_version_1')
class VersionDispatchTests(TestCase):
    """ Item List API supports a variable dispatch mechanism, based on the 'accept type'.
    """
    def test_v1_dispatch(self, v1_get, v2_get, v3_get):

        with app.test_request_context('/v1/items', headers={'Accept': 'application/json'}):
            ItemListApi().get()

        v1_get.assert_called_once_with()
        self.assertFalse(v2_get.called)
        self.assertFalse(v3_get.called)

    def test_v2_dispatch(self, v1_get, v2_get, v3_get):

        with app.test_request_context('/v2/items/searches', headers={'Accept': 'application/json'}):
            ItemListApi().get()

        self.assertFalse(v1_get.called)
        v2_get.assert_called_once_with()
        self.assertFalse(v3_get.called)

    def test_v3_dispatch(self, v1_get, v2_get, v3_get):

        with app.test_request_context('/v1/items', headers={'Accept': 'application/vnd.scoop.v3+json'}):
            ItemListApi().get()

        self.assertFalse(v1_get.called)
        self.assertFalse(v2_get.called)
        v3_get.assert_called_once_with()

    def test_default_dispatch_to_v1(self, v1_get, v2_get, v3_get):

        with app.test_request_context('/v1/items'):
            ItemListApi().get()

        v1_get.assert_called_once_with()
        self.assertFalse(v2_get.called)
        self.assertFalse(v3_get.called)
