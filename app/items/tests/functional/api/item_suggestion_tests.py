from __future__ import unicode_literals, absolute_import
from unittest import TestCase

from app import app

from mock import patch

from app.items.api import ItemTypeAheadSuggestionApi


@patch('app.items.api.ItemTypeAheadSuggestionApi._get_version_3')
@patch('app.items.api.ItemTypeAheadSuggestionApi._get_version_2')
@patch('app.items.api.ItemTypeAheadSuggestionApi._get_version_1')
class VersionDispatchTests(TestCase):
    """ Item List API supports a variable dispatch mechanism, based on the 'accept type'.
    """
    def test_v1_dispatch(self, v1_get, v2_get, v3_get):

        with app.test_request_context('/v1/items/searches/suggestions/abc',
                                      headers={'Accept': 'application/json'}) as ctx:

            ItemTypeAheadSuggestionApi().get(**(ctx.request.view_args or {}))

            v1_get.assert_called_once_with(ctx.request, search_term='abc')
            self.assertFalse(v2_get.called)
            self.assertFalse(v3_get.called)

    def test_v2_dispatch(self, v1_get, v2_get, v3_get):

        with app.test_request_context('/v2/items/searches/suggestions?q=abc', headers={'Accept': 'application/json'}) as ctx:
            ItemTypeAheadSuggestionApi().get(**(ctx.request.view_args or {}))

            self.assertFalse(v2_get.called)
            # per 14 juli 2017, use the same algorithm for suggestion search with version 1
            v1_get.assert_called_once_with(ctx.request, search_term='abc')
            self.assertFalse(v3_get.called)

    def test_v3_dispatch(self, v1_get, v2_get, v3_get):

        with app.test_request_context('/v1/items/suggest?q=abc', headers={'Accept': 'application/vnd.scoop.v3+json'}) as ctx:
            ItemTypeAheadSuggestionApi().get(**(ctx.request.view_args or {}))

            self.assertFalse(v1_get.called)
            self.assertFalse(v2_get.called)
            v3_get.assert_called_once_with(ctx.request, search_term='abc')

    def test_default_dispatch_to_v1(self, v1_get, v2_get, v3_get):

        with app.test_request_context('/v1/items/searches/suggestions?q=abc') as ctx:
            ItemTypeAheadSuggestionApi().get(**(ctx.request.view_args or {}))

            v1_get.assert_called_once_with(ctx.request, search_term='abc')
            self.assertFalse(v2_get.called)
            self.assertFalse(v3_get.called)
