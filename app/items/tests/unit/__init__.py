from inspect import ArgSpec

PARAMETERLESS_METHOD_SIG = ArgSpec(args=['self'],
                                   varargs=None,
                                   keywords=None,
                                   defaults=None)
