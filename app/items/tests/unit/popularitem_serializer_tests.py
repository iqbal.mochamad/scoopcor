import random
# import unittest
#
# from faker import Factory
#
# from flask.ext.appsfoundry import marshal
#
# from app.items.choices import ITEM_TYPES
# from app.items.choices.item_type import BOOK, MAGAZINE, NEWSPAPER
# from app.items.serializers import PopularItemSerializer
#
#
# fake = Factory.create()
#
#
# class PopularItemSerializerTests(unittest.TestCase):
#
#     def test_serialization(self):
#
#         item_type = random.choice([BOOK, MAGAZINE, NEWSPAPER])
#
#         popular_item = PopularItem(
#             id=fake.pyint(),
#             item_id=fake.pyint(),
#             sort_priority=fake.pyint(),
#             item_type=ITEM_TYPES[item_type]
#         )
#
#         expected = {
#             'id': popular_item.id,
#             'item_id': popular_item.item_id,
#             'sort_priority': popular_item.sort_priority,
#             'itemtype_id': item_type
#         }
#
#
#         actual = marshal(popular_item, PopularItemSerializer())
#
#         self.assertDictEqual(expected, actual)
