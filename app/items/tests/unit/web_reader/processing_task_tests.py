from __future__ import unicode_literals
from datetime import datetime
import json
from unittest import TestCase

from app.items.choices import WebReaderTaskStatus
from app.items.web_reader import (
    ProcessingTask, WebReaderTaskJsonEncoder, WebReaderTaskJsonDecoder)
from app.auth.models import UserInfo


class WebReaderProcessingTest(TestCase):

    maxDiff = None

    def test_json_serialization_deserialization(self):
        task = ProcessingTask(item_id=1234)
        task.processing_start_time = datetime.now()

        notify_users = [
            UserInfo(username='ramdani@apps-foundry.com',
                     user_id=321, perm=[], token='abc'),
            UserInfo(username='derek.curtis@apps-foundry.com',
                     user_id=123, perm=[], token='def'),
        ]

        task.notify_users = notify_users

        actual_json = json.dumps(task, cls=WebReaderTaskJsonEncoder)

        self.assertDictEqual(
            json.loads(actual_json, cls=WebReaderTaskJsonDecoder),
            {
                'item_id': 1234,
                'processing_start_time': task.processing_start_time,
                'processing_end_time': None,
                'status': WebReaderTaskStatus.pending,
                'sub_task_status': None,
                'error_message': None,
                'is_high_priority': False,
                'notify_users': [
                    UserInfo(username='ramdani@apps-foundry.com',
                             user_id=321,
                             perm=[],
                             token=None),
                    UserInfo(username='derek.curtis@apps-foundry.com',
                             user_id=123,
                             perm=[],
                             token=None),
                ]
            }
        )
