from __future__ import unicode_literals
import random
from collections import namedtuple
from functools import wraps

from faker import Factory

from app.items.choices.item_type import ITEM_TYPES, ItemTypes

_fake = Factory.create()


def inject_mock_item(id=None, item_type=None, page_count=None, content_type='pdf'):
    def wrapper(func):
        @wraps(func)
        def inner(*args, **kwargs):
            ItemMock = namedtuple('Item', ['id', 'item_type', 'page_count', 'content_type'])
            kwargs['item'] = ItemMock(id=id or _fake.pyint(),
                                      item_type=item_type or
                                                random.choice([ItemTypes.book, ItemTypes.magazine, ItemTypes.newspaper]),
                                      page_count=page_count,
                                      content_type=content_type)
            func(*args, **kwargs)
        return inner
    return wrapper
