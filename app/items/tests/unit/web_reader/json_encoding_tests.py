from datetime import datetime
import json
from unittest import TestCase

from app.items.web_reader import WebReaderTaskJsonEncoder
from app.items.choices import WebReaderTaskStatus


class JsonSerializerTest(TestCase):
    """ Our JSON serializer has 2 special cases to deal with:

    - DateTimes
    - Enums

    All other data types are natively JSON serializable (and we just allow
    python's json library to do it's thing there.).
    """

    def test_datetime(self):
        class ObjectWithDate(object):
            def __init__(self):
                self.current_date = datetime.now()

        obj = ObjectWithDate()

        expected = json.dumps({'current_date': obj.current_date.isoformat()})

        actual = json.dumps(obj, cls=WebReaderTaskJsonEncoder)

        self.assertEqual(actual, expected)

    def test_enum(self):
        class ObjectWithEnum(object):
            def __init__(self):
                self.status = WebReaderTaskStatus.complete

        obj = ObjectWithEnum()

        expected = json.dumps({'status': 'complete'})

        actual = json.dumps(obj, cls=WebReaderTaskJsonEncoder)

        self.assertEqual(actual, expected)

