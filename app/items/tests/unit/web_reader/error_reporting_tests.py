from __future__ import unicode_literals, absolute_import
import json
from datetime import datetime, timedelta
from unittest import TestCase
import random

from dateutil.parser import parse
from dateutil.tz import tzutc
from mockredis import mock_strict_redis_client
from faker import Factory
from faker.providers import BaseProvider

from app.items.web_reader import WebReaderErrorReport, WebReaderErrorManager


class ErrorReportManagerTests(TestCase):
    def setUp(self):
        self.redis_instance = mock_strict_redis_client()
        self.gateway = WebReaderErrorManager(self.redis_instance)

    def test_insert_with_page_number(self):
        # sanity test
        self.assertEqual(len(self.redis_instance.keys("*")), 0)

        # noinspection PyTypeChecker
        report = WebReaderErrorReport(
            item=FakeItem(id=9999),
            user=FakeUser(user_id=1234,
                          username="derek.curtis@apps-foundry.com"),
            message="Semua jelek!! Nga bisa baca!",
            pages=[1, 2, 3, 4, 5, 6, ],
            report_id="abcdefgh"
        )
        self.gateway.insert(report)

        expected = {
            'item': {
                'id': 9999
            },
            'user': {
                'username': 'derek.curtis@apps-foundry.com',
                'user_id': 1234
            },
            'pages': [1, 2, 3, 4, 5, 6, ],
            'message': 'Semua jelek!! Nga bisa baca!',
            'report_id': 'abcdefgh'
        }

        actual_json = json.loads(
            self.redis_instance.get(self.redis_instance.keys("*")[0]))

        report_time = actual_json.pop('report_time')
        self.assertAlmostEqual(
            parse(report_time),
            datetime.utcnow().replace(tzinfo=tzutc()),
            delta=timedelta(seconds=1))

        self.assertDictEqual(expected, actual_json)


class GetAllTests(TestCase):
    def setUp(self):
        self.redis_instance = mock_strict_redis_client()
        self.gateway = WebReaderErrorManager(self.redis_instance)

        self.test_fixtures = []

        for _ in range(10):
            error_report = _fake.error_report()
            self.test_fixtures.append(error_report)
            self.gateway.insert(error_report)

    def test_returned_count(self):
        self.assertEqual(len(self.gateway.get_all()), 10)

    def test_returned_object_type(self):
        for report in self.gateway.get_all():
            self.assertIsInstance(report, WebReaderErrorReport)

    def test_returned_object_report_time(self):
        test_report = random.choice(self.gateway.get_all())

        original_report = [rpt for rpt in self.test_fixtures
                           if rpt.report_id == test_report.report_id][0]

        self.assertEqual(test_report.report_time,
                         original_report.report_time)

    def test_returned_object_user_info(self):
        test_report = random.choice(self.gateway.get_all())

        original_report = [rpt for rpt in self.test_fixtures
                           if rpt.report_id == test_report.report_id][0]

        self.assertEqual(test_report.user.user_id,
                         original_report.user.user_id)

        self.assertEqual(test_report.user.username,
                         original_report.user.username)

    def test_returned_object_item(self):
        test_report = random.choice(self.gateway.get_all())

        original_report = [rpt for rpt in self.test_fixtures
                           if rpt.report_id == test_report.report_id][0]

        self.assertEqual(test_report.item['id'], original_report.item.id)


class FakeUser(object):
    def __init__(self, user_id, username):
        self.user_id = user_id
        self.username = username


class FakeItem(object):
    def __init__(self, id):
        self.id = id


class ErrorReportProvider(BaseProvider):

    def error_report(self):

        # noinspection PyTypeChecker
        return WebReaderErrorReport(
            item=self.item(),
            user=self.user(),
            message=_fake.text(),
            pages=[i for i in range(0, random.randint(1,5))],
            report_id=_fake.md5()
        )

    def user(self, user_id=None, username=None):
        return FakeUser(user_id=user_id or _fake.pyint(),
                        username=username or _fake.email())

    def item(self, id=None):
        return FakeItem(id=id or _fake.pyint())


_fake = Factory.create()
_fake.add_provider(ErrorReportProvider)
