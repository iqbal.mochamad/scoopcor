"""
Our error messages here matter because they inform front-end about our progress
in making the WebReader files ready for viewing.
"""
from __future__ import unicode_literals

import re
import json
from unittest import TestCase

from nose.tools import nottest

from app.items.exceptions import WebReaderFileProcessing
from app.items import web_reader
from app.items.choices import WebReaderTaskStatus


class WebReaderProcessingExceptionTests(TestCase):

    def test_pending(self):

        task = web_reader.ProcessingTask(item_id=123)
        task.status = WebReaderTaskStatus.pending

        item = ItemMock()
        ex = WebReaderFileProcessing(task, item)

        response_json = json.loads(ex.get_body())

        self.assertItemsEqual(
            response_json.keys(),
            ['status', 'error_code', 'user_message', 'developer_message'],
            msg="Response contains standard error json keys")

        self.assertRegexpMatches(
            response_json['user_message'], re.compile(r'1/5'), msg="User message should indicate step 1 of 5")
        self.assertRegexpMatches(
            response_json['developer_message'], re.compile(r'1/5'), msg="User message should indicate step 1 of 5")


@nottest
class ItemMock(object):

    def __init__(self):
        self.id = 123
        self.brand_id = 123
        self.name = 'abc'
        self.page_count = 100
