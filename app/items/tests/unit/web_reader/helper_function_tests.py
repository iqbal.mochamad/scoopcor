#
# These tests pass, but require a database be avaialble...
# This sucks! We probably need to directly pass in the Item object
# itself so we can mock it.
#
# from __future__ import unicode_literals
# import os
# from shutil import rmtree
# from unittest import TestCase
#
# from faker import Factory
# from nose.tools import raises
# from six import text_type
#
# from app import app
# from app.items.exceptions import WebReaderFilesNotReady
# from app.items.web_reader import assert_web_reader_files_ready, \
#     get_web_reader_content_pages_directory
# from tests.helpers import get_app_base_dir
#
#
# class WebReaderFilesReadyTests(TestCase):
#
#     @raises(WebReaderFilesNotReady)
#     def test_files_not_available(self):
#         with app.test_request_context('/'):
#             #  slightly fragile -- picking an ITEM ID larger than any we
#             #  should ever have in our system.
#             fake_item_id = 1000000000  # one billion
#             fake_brand_id = 1000000011  # one billion
#             assert_web_reader_files_ready(fake_item_id, fake_brand_id, app.kvs)
#
#     def test_files_avaialble(self):
#         with app.test_request_context('/'):
#             self.assertIsNone(assert_web_reader_files_ready(self.valid_item_id, self.valid_brand_id, app.kvs))
#
#     def setUp(self):
#         with app.test_request_context('/'):
#             valid_item_id = _fake.pyint()
#             valid_brand_id = _fake.pyint()
#             first_page_path = os.path.join(
#                 get_web_reader_content_pages_directory(valid_item_id),
#                 '1.jpg')
#
#             os.makedirs(get_web_reader_content_pages_directory(valid_item_id))
#
#             with open(first_page_path, 'wb') as f:
#                 f.write('somefakeimagedata'.encode('utf-8'))
#
#             self.first_page_path = first_page_path
#             self.valid_item_id = valid_item_id
#             self.valid_brand_id = valid_brand_id
#
#     def tearDown(self):
#         rmtree(os.path.dirname(self.first_page_path))
#
#
# _fake = Factory.create()
