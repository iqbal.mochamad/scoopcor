from __future__ import unicode_literals

import json
from datetime import datetime
from unittest import TestCase

from mockredis import mock_strict_redis_client
from nose.tools import raises

from app.items.exceptions import NonExistentTaskError, DuplicateTaskError
from app.items.web_reader import ProcessingTask, ProcessingTaskManager
from app.items.choices import WebReaderTaskStatus


class ProcessingTaskManagerTests(TestCase):
    """ The ProcessingTaskManager can perform it's CRUD operations properly.

    Also checks internally inside of Redis to make sure that the task data
    is represented properly when stored to Redis and that it is decoded
    into it's proper data types when retrieved.
    """
    def test_enqueue(self):

        redis = mock_strict_redis_client()

        mgr = ProcessingTaskManager(redis=redis)
        task = ProcessingTask(item_id=123)

        mgr.enqueue(task)

        self.assertEqual(len(redis.keys("{key}:*".format(key=mgr.key))), 1)

        expected = {
            'item_id': 123,
            'processing_start_time': None,
            'processing_end_time': None,
            'status': 'pending',
            'sub_task_status': None,
            'error_message': None,
            'is_high_priority': False,
            'notify_users': [],
        }

        self.assertDictEqual(
            expected,
            json.loads(redis.get('WEB_READER_PROCESSING:123'))
        )

    def test_get_by_id(self):

        redis = mock_strict_redis_client()
        mgr= ProcessingTaskManager(redis=redis)
        mgr.enqueue(task=ProcessingTask(item_id=123))

        task = mgr.get_by_item_id(123)

        self.assertIsNotNone(task)
        self.assertEqual(123, task.item_id)
        self.assertIsNone(task.processing_start_time)
        self.assertIsNone(task.processing_end_time)
        self.assertIsNone(task.sub_task_status)
        self.assertEqual(WebReaderTaskStatus.pending, task.status)
        self.assertIsNone(task.error_message)

    def test_get_all_tasks(self):

        redis = mock_strict_redis_client()
        mgr = ProcessingTaskManager(redis=redis)

        task1 = ProcessingTask(item_id=1)

        task2 = ProcessingTask(item_id=2)
        task2.processing_start_time = datetime.now()
        task2.processing_end_time = datetime.now()
        task2.status = WebReaderTaskStatus.complete

        mgr.enqueue(task1)
        mgr.enqueue(task2)

        all_tasks = mgr.get_all_tasks()

        self.assertEqual(len(all_tasks), 2)

        # make sure our special parsing values are correct
        self.assertEqual(
            filter(lambda t: t.item_id == 2, all_tasks)[0].processing_start_time,
            task2.processing_start_time
        )

        self.assertEqual(
            filter(lambda t: t.item_id == 2, all_tasks)[0].status,
            WebReaderTaskStatus.complete,
            msg="Incorrect status.  Expected WebReaderTaskStatus.complete, but "
                "got {}".format(
                filter(lambda t: t.item_id == 2, all_tasks)[0].status)
        )

    def test_update_task(self):

        redis = mock_strict_redis_client()
        mgr = ProcessingTaskManager(redis=redis)
        task = ProcessingTask(item_id=456)
        mgr.enqueue(task)

        expected_start_time = datetime.now()
        task.processing_start_time = expected_start_time
        task.status = WebReaderTaskStatus.creating_web_reader_images
        task.sub_task_status = '14/32'

        mgr.update(task)

        expected = {
            'item_id': 456,
            'processing_start_time': unicode(expected_start_time.isoformat()),
            'processing_end_time': None,
            'status': 'creating web reader images',
            'sub_task_status': '14/32',
            'error_message': None,
            'is_high_priority': False,
            'notify_users': [],
        }

        self.assertDictEqual(
            expected,
            json.loads(redis.get('WEB_READER_PROCESSING:456'))
        )

    def test_delete_task(self):

        redis = mock_strict_redis_client()

        mgr = ProcessingTaskManager(redis=redis)
        task = ProcessingTask(item_id=123)

        mgr.enqueue(task)

        self.assertEqual(len(redis.keys("WEB_READER_PROCESSING:*")), 1)

        mgr.delete(task)

        self.assertEqual(len(redis.keys("WEB_READER_PROCESSING:*")), 0)

    @raises(NonExistentTaskError)
    def test_delete_non_existant_task(self):
        redis = mock_strict_redis_client()

        mgr = ProcessingTaskManager(redis=redis)
        task = ProcessingTask(item_id=123)

        mgr.delete(task)

    @raises(NonExistentTaskError)
    def test_update_non_existant_task(self):
        redis = mock_strict_redis_client()

        mgr = ProcessingTaskManager(redis=redis)

        task = ProcessingTask(item_id=123)

        mgr.update(task)

    @raises(DuplicateTaskError)
    def test_enqueue_duplicate_item_id(self):
        redis = mock_strict_redis_client()
        mgr = ProcessingTaskManager(redis=redis)

        task = ProcessingTask(item_id=123)

        # doing this just to make sure that the second 'enqueue' call is
        # the one that fails.
        self.assertIsInstance(mgr.enqueue(task), ProcessingTask)

        mgr.enqueue(task)

    def test_set_and_get_last_minimum_item_id(self):
        redis = mock_strict_redis_client()
        mgr = ProcessingTaskManager(redis=redis)
        last_min_item_id = 10
        mgr.set_last_minimum_item_id(item_id=10)

        actual = mgr.get_last_minimum_item_id()
        self.assertEqual(last_min_item_id, actual)


