from __future__ import unicode_literals
from unittest import TestCase

from flask import Flask
from mock import patch

from app.items.helpers import ItemQueryBuilder
from app.master.models import Vendor

fake_app = Flask(__name__)


class ItemQueryBuilderTests(TestCase):

    maxDiff = None

    def test_with_content(self):
        with fake_app.test_request_context('/?q=jakarta&with_content=true') as ctx:

            self.assertDictEqual(
                ItemQueryBuilder(ctx.request)._build_query(),
                {
                    'q': 'content:jakarta'
                }
            )

    def test_without_content(self):
        with fake_app.test_request_context('/?q=jakarta') as ctx:
            self.assertDictEqual(
                ItemQueryBuilder(ctx.request)._build_query(),
                {
                    'q': 'jakarta'
                }
            )

    def test_highlighting(self):
        with fake_app.test_request_context('/') as ctx:
            self.assertDictEqual(
                ItemQueryBuilder(ctx.request)._build_highligting(),
                {
                    'hl': 'on',
                    'hl.fl': 'content'
                }
            )

    def test_is_age_restricted(self):
        with fake_app.test_request_context('/?is_age_restricted=true') as ctx:
            self.assertDictEqual(
                ItemQueryBuilder(ctx.request)._build_filter_queries(),
                {
                    'fq': ['is_age_restricted:true']
                }
            )

    def test_without_is_age_restricted(self):
        with fake_app.test_request_context('/') as ctx:
            self.assertDictEqual(
                ItemQueryBuilder(ctx.request)._build_filter_queries(),
                {
                    'fq': []
                }
            )

    def test_build_with_content_highliting(self):
        with fake_app.test_request_context('/?q=jakarta&with_content=true') as ctx:
            self.assertDictEqual(
                ItemQueryBuilder(ctx.request).build(),
                {
                    'q': 'content:jakarta',
                    'hl': 'on',
                    'hl.fl': 'content',
                    'facet.field': [],
                    'rows': 20,
                    'start': 0,
                    'fq': []
                }
            )

    def test_build_without_content_highliting(self):
        with fake_app.test_request_context('/?q=jakarta') as ctx:
            self.assertDictEqual(
                ItemQueryBuilder(ctx.request).build(),
                {
                    'q': 'jakarta',
                    'facet.field': [],
                    'rows': 20,
                    'start': 0,
                    'fq': []
                }
            )

    @patch("flask_sqlalchemy.orm.query.Query.all", autospec=True)
    def test_build_filter_queries(self, mocked_get_vendors):
        # &include-inactive=false
        with fake_app.test_request_context(
                '/?is_age_restricted=true&exclude-free-item=true&eperpus-id=723') as ctx:
            mocked_get_vendors.return_value = [Vendor(id=1, name='vendor_abc'),
                                               Vendor(id=1, name='vendor_cde'), ]
            actual_params = ItemQueryBuilder(ctx.request)._build_filter_queries()
            self.assertDictEqual(
                actual_params,
                {
                    'fq': ['is_age_restricted:true',
                           'price_web_idr:[0.1 TO *]',
                           # "is_active:true",
                           'vendor_name:("vendor_abc" "vendor_cde")'],
                })
                # is_active:true
