import os

import boto
import boto3

from unittest import TestCase

from boto.s3.key import Key
from botocore.exceptions import ClientError
from faker import Factory
from moto import mock_s3
from nose.tools import nottest

from app import app
from app.items.file_server_gateway import AmazonS3ServerGateway, InternalFileServerGateway


class InternalFileServerGatewayTests(TestCase):

    def setUp(self):
        self.server = InternalFileServerGateway(
            path='/tmp'
        )
        # path=app.config['BOTO3_DIR_DOWNLOAD']

    def test_file_exists(self):

        file_name = 'testfile.txt'

        # create dummy file for test
        item_file_full_path = os.path.join(self.server.path, file_name)
        if not os.path.exists(item_file_full_path):
            open(item_file_full_path, 'w').close()

        actual = self.server.is_file_exists(file_name=file_name)

        expected = True

        self.assertEqual(actual, expected)

    def test_file_not_exists(self):

        file_name = 'dummyfile.abcd'

        actual = self.server.is_file_exists(file_name=file_name)

        expected = False

        self.assertEqual(actual, expected)


@nottest
class AmazonS3FileServerGatewayTests(TestCase):
    # this test class is mark as nottest cause somehow mock moto is not working.. e2e test is available

    def setUp(self):

        self.moto = mock_s3()
        # We enter "moto" mode using this
        self.moto.start()
        self.region_name = "us-east-1"
        self.bucket_name = 'abc'
        self.file_name = 'testfile.txt'
        faker = Factory.create()

        # conn = boto.connect_s3()
        # bucket = conn.create_bucket(self.bucket_name, location=self.region_name)
        # k = Key(bucket)
        # k.key = self.file_name
        # k.set_contents_from_string(faker.text())

        s3_resource = boto3.resource(
            's3',
            region_name=self.region_name,
        )

        s3_resource.create_bucket(Bucket=self.bucket_name)
        # # s3_resource = boto3.session.Session().resource('s3')
        # # s3_resource.create_bucket(Bucket=self.bucket_name)

        s3_resource.Object(self.bucket_name, self.file_name).put(Body=faker.text())

        s3 = boto3.client(
            's3',
            self.region_name,
            aws_access_key_id='12',
            aws_secret_access_key='a123',
        )

        # AWS_DEFAULT_REGION = "ap-south-1"

        self.server = AmazonS3ServerGateway(
            s3,
            aws_server=s3._endpoint.host,
            aws_bucket_name=self.bucket_name,
        )
        # aws_server=s3._endpoint.host,

    def tearDown(self):
        self.moto.stop()

    def test_upload_file(self):

        file_name = 'test_boto_upload.txt'
        # create dummy file for test
        item_file_full_path = os.path.join('/tmp', file_name)
        if not os.path.exists(item_file_full_path):
            open(item_file_full_path, 'w').close()

        # upload to mock boto:
        actual = self.server.file_upload(
            item_file_full_path=item_file_full_path,
            key=file_name)

        expected = 'File uploaded to s3 with key={}'.format(file_name)

        self.assertEqual(actual, expected)

    def test_file_exists(self):

        # self.file_name = 'images/1/8/ID_TEC2010ED22.zip'

        actual = self.server.is_file_exists(file_name=self.file_name)

        expected = True

        self.assertEqual(actual, expected)

    def test_file_not_exists(self):

        file_name = 'dummyfile.abcd'

        actual = self.server.is_file_exists(file_name=file_name)

        self.assertEqual(actual, False)

    # def test_file_access_status_public(self):
    #     pass
    #     # # self.file_name = 'images/1/8/ID_TEC2010ED22.zip'
    #     #
    #     # actual = self.server.file_access_status(file_name=self.file_name)
    #     #
    #     # expected = 'public'
    #     #
    #     # self.assertEqual(actual, expected)


