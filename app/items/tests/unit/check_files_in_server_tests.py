from datetime import datetime
import json
from unittest import TestCase

from app.items.api import ItemCheckRelatedFilesApi
from app.items.file_server_gateway import FileServerGateway


class MockFileServer(FileServerGateway):

    def __init__(self):
        self.file_public = 'file1.txt'
        self.file_private = 'fileprivate1.txt'

    def is_file_exists(self, file_name):
        if file_name in (self.file_public, self.file_private):
            return True
        else:
            return False

    def file_access_status(self, file_name):
        if file_name == self.file_private:
            return 'private'
        elif file_name == self.file_public:
            return 'public'
        else:
            return 'private'

    def get_file_size(self, file_name):
        if file_name in (self.file_public, self.file_private):
            return 123
        else:
            return 0

    def get_last_modified_date(self, file_name):
        if file_name in (self.file_public, self.file_private):
            return '2015-16-10 01:01:02'
        else:
            return ''

    def get_file_info(self, file_name):
        is_exists = False
        read_access = 'private'
        error_message = ''
        last_modified_date = ''
        file_size = 0
        try:
            is_exists = self.is_file_exists(file_name)
            read_access = self.file_access_status(file_name=file_name)
            file_size = self.get_file_size(file_name)
            last_modified_date = self.get_last_modified_date(file_name)
        except Exception as e:
            error_message = 'Failed to read file in {host}, error: {error_message}'.format(
                host='s3',
                error_message=str(e)
            )
        finally:
            result = {
                'is_exists': is_exists,
                'read_access': read_access,
                'error_message': error_message,
                'file_size': file_size,
                'last_modified_date': last_modified_date,
            }

        return result


class ItemCheckRelatedFilesApiTests(TestCase):

    def setUp(self):
        self.server = MockFileServer()
        self.internal_server = MockFileServer()
        self.amazon_s3_server = MockFileServer()

    def test_exists_in_amazon_s3(self):

        expected = {
            'is_exists': True,
            'read_access': 'public',
            'error_message': '',
            'file_size': 123,
            'last_modified_date': '2015-16-10 01:01:02',
        }

        file_name = self.server.file_public

        actual = ItemCheckRelatedFilesApi.get_file_info(
            file_name=file_name,
            file_server_gateway=self.amazon_s3_server
        )

        self.assertDictEqual(actual, expected)

    def test_exists_in_amazon_s3_but_private(self):

        expected = {
            'is_exists': True,
            'read_access': 'private',
            'error_message': '',
            'file_size': 123,
            'last_modified_date': '2015-16-10 01:01:02',
        }

        file_name = self.server.file_private

        actual = ItemCheckRelatedFilesApi.get_file_info(
            file_name=file_name,
            file_server_gateway=self.amazon_s3_server
        )

        self.assertDictEqual(actual, expected)

    def test_not_exists_in_amazon_s3(self):

        expected = {
            'is_exists': False,
            'read_access': 'private',
            'error_message': '',
            'file_size': 0,
            'last_modified_date': '',
        }

        file_name = 'filenotexists.txt'

        actual = ItemCheckRelatedFilesApi.get_file_info(
            file_name=file_name,
            file_server_gateway=self.amazon_s3_server
        )

        self.assertDictEqual(actual, expected)

    def test_exists_in_internal_server(self):
        expected = {
            'is_exists': True,
            'read_access': 'public',
            'error_message': '',
            'file_size': 123,
            'last_modified_date': '2015-16-10 01:01:02',
        }

        file_name = self.server.file_public

        actual = ItemCheckRelatedFilesApi.get_file_info(
            file_name=file_name,
            file_server_gateway=self.internal_server
        )

        self.assertDictEqual(actual, expected)

    def test_not_exists_in_internal_server(self):
        expected = {
            'is_exists': False,
            'read_access': 'private',
            'error_message': '',
            'file_size': 0,
            'last_modified_date': '',
        }

        file_name = 'filenotexists.txt'

        actual = ItemCheckRelatedFilesApi.get_file_info(
            file_name=file_name,
            file_server_gateway=self.internal_server
        )

        self.assertDictEqual(actual, expected)
