# -* coding: utf-8 *-
import unittest

from faker import Factory

from app.helpers import generate_slug


class SlugifyTests(unittest.TestCase):
    """ Tests our slug generation logic.
    """

    def test_ascii_title(self):
        """ Must be all lower-case, spaces replaced with hyphens.
        """
        entity = Entity(name='The Story of Scoop', slug=None)
        result = generate_slug(entity.name)
        self.assertEqual(result, 'the-story-of-scoop')

    def test_ascii_title_with_conflict(self):
        """  When given a numeric 'suffix' parameter, slugs should have
        a suffix appended with a roman numeral.
        """
        entity = Entity(name='The Story of Scoop', slug=None)
        result = generate_slug(entity.name, 6)
        self.assertEqual(result, 'the-story-of-scoop-vi')

    def test_latin1(self):
        """ Unicode LATIN1 should be converted to it's ASCII representation.
        """
        entity = Entity(name=u'Scheiße', slug=None)
        result = generate_slug(entity.name, 2)
        self.assertEqual(result, 'scheisse-ii')

    def test_chinesee(self):
        """ UTF-8 characters should be converted to ASCII
        """
        entity = Entity(name=u'汉语 / 漢語', slug=None)
        result = generate_slug(entity.name, 2)
        self.assertEqual(result, 'yi-yu-han-yu-ii')

    def test_unusual_slug(self):
        entity = Entity(name=u'汉语 / 漢語', slug='ii-')
        result = generate_slug(entity.name, None)
        self.assertEqual(result, 'yi-yu-han-yu')

    def test_unusual_slug_with_value(self):
        entity = Entity(name=u'汉语 / 漢語', slug='ii-')
        result = generate_slug(entity.name, 2)
        self.assertEqual(result, 'yi-yu-han-yu-ii')

    def test_return_unicode(self):
        """ Slugs should be unicode strings, even though they contain only ASCII.
        """
        entity = Entity(name=u'汉语 / 漢語', slug='ii-')
        result = generate_slug(entity.name, None)
        self.assertIsInstance(result, unicode)



class Entity(object):
    def __init__(self, name, slug):
        self.name = name
        self.slug = slug

_fake = Factory.create()
