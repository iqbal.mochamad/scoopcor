from __future__ import unicode_literals

from unittest import TestCase

from app.items.previews import PreviewUrlBuilder
from app.items.tests.unit.web_reader.fixtures import inject_mock_item

MAGAZINE_PREVIEW_LIMIT = 5


class ContentPreviewTests(TestCase):

    maxDiff = None

    def setUp(self):
        self.builder = PreviewUrlBuilder(base_url='http://scoopadm.apps-foundry.com/api/v1')

    @inject_mock_item(item_type='magazine', page_count=199)
    def test_preview_magazine(self, item):

        expected = []

        # 5 page
        for page_number in range(1, MAGAZINE_PREVIEW_LIMIT + 1):
            expected.append(
                'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'.format(item.id, page_number),
            )

        # 1 page in last
        expected.append('http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'
                        .format(item.id, item.page_count))

        self.assertItemsEqual(self.builder.make_urls(item), expected)

    @inject_mock_item(page_count=None)
    def test_no_page_count(self, item):
        """ If item.page_count=None, then we can't display previews

        .. note::

            An empty page_count means that we have NOT rendered web-reader files for this
            item yet.
        """
        self.assertItemsEqual(self.builder.make_urls(item), [])

    @inject_mock_item(page_count=0)
    def test_zero_page_count(self, item):
        """ If item.page_count=0-Zero, then we can't display previews

        .. note::

            A zero page_count means that we have NOT rendered web-reader files for this
            item yet.
                item was processed in web reader processor (flag as zero) but failed/error
        """
        self.assertItemsEqual(self.builder.make_urls(item), [])

    @inject_mock_item(item_type='newspaper', page_count=199)
    def test_preview_newspaper(self, item):

        expected = [
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/1.jpg'.format(item.id),
        ]

        self.assertItemsEqual(self.builder.make_urls(item), expected)

    @inject_mock_item(item_type='book', page_count=100)
    def test_preview_book_with_100_pages(self, item):

        expected = [
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/1.jpg'.format(item.id)
        ]

        for page_number in range(10, 13):
            expected.append(
                'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'
                    .format(item.id, page_number)
            )

        expected.append('http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'
                        .format(item.id, item.page_count))

        self.assertItemsEqual(self.builder.make_urls(item), expected)

    @inject_mock_item(item_type='book', page_count=500)
    def test_preview_book_with_500_pages(self, item):

        expected = [
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/1.jpg'.format(item.id)
        ]

        for page_number in range(10, 18):
            expected.append(
                'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'
                    .format(item.id, page_number)
            )

        expected.append('http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'
                        .format(item.id, item.page_count))

        self.assertItemsEqual(self.builder.make_urls(item), expected)

    @inject_mock_item(item_type='book', page_count=205)
    def test_preview_book_with_205_pages(self, item):

        expected = [
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/1.jpg'.format(item.id)
        ]

        for page_number in range(10, 18):
            expected.append(
                'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'
                    .format(item.id, page_number)
            )

        expected.append('http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'
                        .format(item.id, item.page_count))

        self.assertItemsEqual(self.builder.make_urls(item), expected)

    @inject_mock_item(item_type='book', page_count=195)
    def test_preview_book_with_195_pages(self, item):

        expected = [
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/1.jpg'.format(item.id)
        ]

        for page_number in range(10, 18):
            expected.append(
                'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'
                    .format(item.id, page_number)
            )

        expected.append('http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'
                        .format(item.id, item.page_count))

        self.assertItemsEqual(self.builder.make_urls(item), expected)

    @inject_mock_item(item_type='book', page_count=35)
    def test_preview_book_with_35_pages(self, item):

        expected = [
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/1.jpg'.format(item.id),
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'.format(item.id, item.page_count)
        ]

        self.assertItemsEqual(self.builder.make_urls(item), expected)

    @inject_mock_item(item_type='book', page_count=50)
    def test_preview_book_with_50_pages(self, item):

        expected = [
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/1.jpg'.format(item.id),
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/10.jpg'.format(item.id),
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'.format(item.id, item.page_count)
        ]

        self.assertItemsEqual(self.builder.make_urls(item), expected)

    @inject_mock_item(item_type='book', page_count=10)
    def test_preview_book_with_10_pages(self, item):

        expected = [
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/1.jpg'.format(item.id),
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'.format(item.id, item.page_count)
        ]

        self.assertItemsEqual(self.builder.make_urls(item), expected)

    @inject_mock_item(item_type='book', page_count=600)
    def test_preview_book_with_600_pages(self, item):

        expected = [
            'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/1.jpg'.format(item.id)
        ]

        for page_number in range(10, 18):
            expected.append(
                'http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'
                    .format(item.id, page_number)
            )

        expected.append('http://scoopadm.apps-foundry.com/api/v1/items/{0}/preview/{1}.jpg'
                        .format(item.id, item.page_count))

        self.assertItemsEqual(self.builder.make_urls(item), expected)
