from unittest import TestCase

from app import app
from app.items import web_reader
from app.items.previews import preview_rule_factory
from app.items.tests.unit.web_reader.fixtures import inject_mock_item


class BookPreviewResponseLinkTest(TestCase):

    def setUp(self):
        pass

    @inject_mock_item(id=1, item_type='book', page_count=500)
    def test_url_link_for_book_page_1(self, item):

        expected = [
            '/v1/items/1/preview/10',
            '/v1/items/1/preview/500'
        ]

        allowed_pages = preview_rule_factory(item).get_page_numbers()

        with app.test_request_context():
            actual = web_reader.construct_preview_links(item, 1, allowed_pages)

        self.assertEqual([link.url for link in actual], expected)
        self.assertEqual([link.rel.name for link in actual], ['next', 'last'])

    @inject_mock_item(id=1, item_type='book', page_count=500)
    def test_url_link_for_book_page_10(self, item):

        expected = [
            '/v1/items/1/preview/1',
            '/v1/items/1/preview/1',
            '/v1/items/1/preview/11',
            '/v1/items/1/preview/500'
        ]

        rules = preview_rule_factory(item)

        allowed_pages = rules.get_page_numbers()

        with app.test_request_context():
            actual = web_reader.construct_preview_links(item, 10, allowed_pages)

        self.assertEqual([link.url for link in actual], expected)
        self.assertEqual([link.rel.name for link in actual], ['first', 'prev', 'next', 'last'])

    @inject_mock_item(id=1, item_type='book', page_count=500)
    def test_url_link_for_book_page_11(self, item):

        expected = [
            '/v1/items/1/preview/1',
            '/v1/items/1/preview/10',
            '/v1/items/1/preview/12',
            '/v1/items/1/preview/500'
        ]

        rules = preview_rule_factory(item)

        allowed_pages = rules.get_page_numbers()

        with app.test_request_context():
            actual = web_reader.construct_preview_links(item, 11, allowed_pages)

        self.assertEqual([link.url for link in actual], expected)
        self.assertEqual([link.rel.name for link in actual], ['first', 'prev', 'next', 'last'])

    @inject_mock_item(id=1, item_type='book', page_count=500)
    def test_url_link_for_book_page_500(self, item):

        expected = [
            '/v1/items/1/preview/1',
            '/v1/items/1/preview/17',
        ]

        rules = preview_rule_factory(item)

        allowed_pages = rules.get_page_numbers()

        with app.test_request_context():
            actual = web_reader.construct_preview_links(item, 500, allowed_pages)

        self.assertEqual([link.url for link in actual], expected)
        self.assertEqual([link.rel.name for link in actual], ['first', 'prev'])


class MagazinePreviewResponseLinkTest(TestCase):

    @inject_mock_item(id=1, item_type='magazine', page_count=30)
    def test_url_link_for_magazine_page_1(self, item):

        expected = [
            '/v1/items/1/preview/2',
            '/v1/items/1/preview/30'
        ]

        allowed_pages = preview_rule_factory(item).get_page_numbers()

        with app.test_request_context():
            actual = web_reader.construct_preview_links(item, 1, allowed_pages)

        self.assertEqual([link.url for link in actual], expected)
        self.assertEqual([link.rel.name for link in actual], ['next', 'last'])

    @inject_mock_item(id=1, item_type='magazine', page_count=30)
    def test_url_link_for_magazine_page_2(self, item):

        expected = [
            '/v1/items/1/preview/1',
            '/v1/items/1/preview/1',
            '/v1/items/1/preview/3',
            '/v1/items/1/preview/30'
        ]

        allowed_pages = preview_rule_factory(item).get_page_numbers()

        with app.test_request_context():
            actual = web_reader.construct_preview_links(item, 2, allowed_pages)

        self.assertEqual([link.url for link in actual], expected)
        self.assertEqual([link.rel.name for link in actual], ['first', 'prev', 'next', 'last'])

    @inject_mock_item(id=1, item_type='magazine', page_count=30)
    def test_url_link_for_magazine_page_4(self, item):

        expected = [
            '/v1/items/1/preview/1',
            '/v1/items/1/preview/3',
            '/v1/items/1/preview/5',
            '/v1/items/1/preview/30'
        ]

        allowed_pages = preview_rule_factory(item).get_page_numbers()

        with app.test_request_context():
            actual = web_reader.construct_preview_links(item, 4, allowed_pages)

        self.assertEqual([link.url for link in actual], expected)
        self.assertEqual([link.rel.name for link in actual], ['first', 'prev', 'next', 'last'])

    @inject_mock_item(id=1, item_type='magazine', page_count=30)
    def test_url_link_for_magazine_page_30(self, item):

        expected = [
            '/v1/items/1/preview/1',
            '/v1/items/1/preview/5',
        ]

        allowed_pages = preview_rule_factory(item).get_page_numbers()

        with app.test_request_context():
            actual = web_reader.construct_preview_links(item, 30, allowed_pages)

        self.assertEqual([link.url for link in actual], expected)
        self.assertEqual([link.rel.name for link in actual], ['first', 'prev'])


class NewspaperPreviewResponseLinkTest(TestCase):

    @inject_mock_item(id=1, item_type='newspaper', page_count=30)
    def test_url_link_for_newspaper_page_1(self, item):

        expected = [
        ]

        allowed_pages = preview_rule_factory(item).get_page_numbers()

        with app.test_request_context():
            actual = web_reader.construct_preview_links(item, 1, allowed_pages)

        self.assertEqual([link.url for link in actual], expected)
        self.assertEqual([link.rel.name for link in actual], [])
