import unittest

from faker import Factory

from app.caching import generate_redis_key, generate_cache_key

fake = Factory.create()


class GenerateRedisKeyTests(unittest.TestCase):

    def setUp(self):
        self.args = {
            'limit': 0,
            'offset': fake.pyint()
        }

    def test_generate_redis_key(self):
        key_prefix = 'LATEST'
        key = generate_redis_key(key_prefix=key_prefix, args=self.args)
        self.assertIsNotNone(key)
        self.assertIn(key_prefix, key)

    def test_generate_cache_key(self):
        key = generate_cache_key(self.args)
        self.assertIsNotNone(key)
