import unittest

from flask.ext.restful import fields
from app.depreciated import fields as shims


class V1LanguageDataFromIso639_Tests(unittest.TestCase):

    def test_language_codes_return_expected_dict(self):
        """
        Any of the language codes that were present in the core_languages
        table in the database should return a dictionary that complies with
        our previous documentation.
        """
        # taken from data in v1 database -- if you want a dump of it in JSON
        # look at the json file loaded by V1LanguageDataFromIso639
        expected = {"id": 1,
                    "name": "English"}

        # All different valid variations of 'english'
        for lang_code in ["EN", 'en', 'ENG', 'eng']:
            shim = shims.V1LanguageDataFromIso639()
            v1_data = shim.format(lang_code)
            self.assertEqual(v1_data['id'], expected['id'])
            self.assertEqual(v1_data['name'], expected['name'])


    def test_invalid_language_raises_marshalling_error(self):
        """
        If a language code is requested that we're not aware of, we should
        raise a marshalling exception.
        """
        # we know that de (german) isn't in the original database
        # make sure that raises a validation exception.
        shim = shims.V1LanguageDataFromIso639()
        self.assertRaises(fields.MarshallingException,
                          lambda: shim.format('de'))


class V1CountryDataFromIso3166_Tests(unittest.TestCase):

    def test_format_returnes_expected_dict(self):
        """
        Any countries that were present in our original dataset should be
        returned by our API.
        """
        expected = {"id": 1, "name": "Indonesia"}

        for country_code in ["ID", "id", "IDN", "idn"]:
            shim = shims.V1CountryDataFromIso3166()
            v1_data = shim.format(country_code)
            self.assertEqual(v1_data['id'], expected['id'])
            self.assertEqual(v1_data['name'], expected['name'])

    def test_invalid_country_raises_marshalling_error(self):
        """
        Country codes that are not part of the original api should raise
        a marshalling exception.
        """
        shim = shims.V1CountryDataFromIso3166()
        self.assertRaises(fields.MarshallingException,
                          lambda: shim.format('de'))
