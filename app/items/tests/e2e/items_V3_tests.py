import json
from httplib import OK
from app.discounts.choices.discount_type import DISCOUNT_OFFER
from app.items.choices import STATUS_READY_FOR_CONSUME
from app.items.choices import STATUS_TYPES
from app.master.choices import ANDROID, IOS, GETSCOOP
from app.master.models import Platform
from app.parental_controls.models import ParentalControl
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.discounts import DiscountFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferPlatformFactory


class ItemsV3E2eTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(ItemsV3E2eTests, cls).setUpClass()
        cls.parental_control = cls.session.query(ParentalControl).first()
        cls.platform_android = cls.session.query(Platform).get(ANDROID)
        cls.platform_ios = cls.session.query(Platform).get(IOS)
        cls.platform_getscoop = cls.session.query(Platform).get(GETSCOOP)
        cls.discount = DiscountFactory(id=1, is_active=True, discount_type=DISCOUNT_OFFER)
        cls.discount.platforms.append(cls.platform_android)
        cls.session.commit()
        cls.offer = OfferFactory(discount_id=[cls.discount.id])
        cls.offer_platform_android = OfferPlatformFactory(offer=cls.offer,
                                                          platform=cls.platform_android)
        cls.offer.core_discounts.append(cls.discount)
        cls.brand = BrandFactory()
        cls.item_1 = ItemFactory(id=1, parentalcontrol=cls.parental_control, item_type='magazine',
                                 item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                 brand=cls.brand)
        cls.offer.items.append(cls.item_1)
        cls.item_2 = ItemFactory(id=2, parentalcontrol=cls.parental_control, item_type='book',
                                 item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                 brand=cls.brand)
        cls.offer.items.append(cls.item_2)
        cls.item_3 = ItemFactory(id=3, parentalcontrol=cls.parental_control, item_type='magazine',
                                 item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                 brand=cls.brand)
        cls.offer.items.append(cls.item_3)
        cls.session.commit()

    def test_response_body(self):
        self.set_api_authorized_user(self.super_admin)
        self.reattach_model(self.offer)
        response = self.api_client.get("/v1/items/1", headers=self.build_headers())
        self.assertEquals(response.status_code, OK, response.data)
        actual = json.loads(response.data)
        self.assertIsNotNone(actual.get('offers'))

    def test_request_from_eperpus(self):
        self.set_api_authorized_user(self.super_admin)
        self.reattach_model(self.offer)
        response = self.api_client.get("/v1/items/1?eperpus=1", headers=self.build_headers())
        self.assertEquals(response.status_code, OK, response.data)
        actual = json.loads(response.data)
        self.assertEqual(actual.get('offers'), [])

    def test_using_client_header(self):
        self.set_api_authorized_user(self.super_admin)
        headers = self.build_headers()
        headers["x-scoop-client"] = "scoop android/4.6.2"
        response = self.api_client.get("/v1/items/2", headers=headers)
        self.assertEquals(response.status_code, OK, response.data)
