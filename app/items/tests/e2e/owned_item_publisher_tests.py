from __future__ import unicode_literals, absolute_import

import json
from httplib import OK, NO_CONTENT, NOT_MODIFIED
from unittest import TestCase

from marshmallow.utils import isoformat

from app import db, app
from app.auth.helpers import jwt_encode_from_user
from app.constants import RoleType
from app.items.choices import STATUS_READY_FOR_CONSUME, FILE_TYPES, ITEM_TYPES
from app.items.choices import STATUS_TYPES
from app.items.models import Item
from app.users.tests.fixtures import UserFactory, OrganizationFactory
from app.users.users import Role
from app.utils.shims import item_types
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import ItemFactory, AuthorFactory, UserItemFactory
from tests.fixtures.sqlalchemy.master import VendorFactory, BrandFactory


class UserPublisherOwnershipTests(TestCase):

    maxDiff = None

    @classmethod
    def setUpClass(cls):
        cls.init_data()

        cls.request_headers = {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'Cache-Control': 'no-cache',
            'Authorization': 'JWT {}'.format(get_jwt_valid_token(cls.user_publisher))
        }

        cls.client = app.test_client(use_cookies=False)
        # request_url = 'v1/owned_items?user_id={user_id}&item_id={item_id}&item_id={item_id2}'.format(
        #     user_id=cls.user_publisher.id,
        #     item_id=cls.item_publisher.id,
        #     item_id2=cls.item_publisher2.id
        # )
        cls.request_url = 'v1/owned_items?user_id={user_id}&item_id={item_ids}&order=-id'.format(
            user_id=cls.user_publisher.id,
            item_ids='{},{}'.format(cls.item_publisher.id, cls.item_publisher2.id)
        )
        cls.response = cls.client.get(cls.request_url, headers=cls.request_headers)

    @classmethod
    def init_data(cls):
        db.session().close_all()
        db.drop_all()
        db.create_all()
        cls.session = db.session()
        cls.session.expire_on_commit = False
        generate_static_sql_fixtures(cls.session)
        role_publisher = Role.query.get(RoleType.organization_specific_admin.value)
        role_verified = Role.query.get(RoleType.verified_user.value)
        cls.org_publisher = OrganizationFactory(id=3311)
        cls.user_publisher = UserFactory(id=4411)
        cls.user_publisher.organizations.append(cls.org_publisher)
        cls.user_publisher.roles.append(role_publisher)
        org_other = OrganizationFactory(id=3312)
        cls.user_others = UserFactory(id=4412)
        cls.user_others.organizations.append(org_other)
        cls.user_verified = UserFactory(id=4413)
        cls.user_verified.roles.append(role_verified)
        cls.user_verified.organizations.append(cls.org_publisher)
        vendor_publisher = VendorFactory(organization_id=cls.org_publisher.id)
        brand_publisher = BrandFactory(vendor=vendor_publisher)
        author = AuthorFactory()
        cls.item_publisher = ItemFactory(id=9911, brand=brand_publisher, is_active=True,
                                         item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME])
        cls.item_publisher.authors.append(author)
        cls.item_publisher2 = ItemFactory(id=9912, brand=brand_publisher, is_active=True,
                                          item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME])
        cls.item_publisher2.authors.append(author)

        cls.owned_other = UserItemFactory(user_id=cls.user_others.id)
        cls.session.commit()

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.drop_all()

    def test_status_code(self):
        self.assertEqual(self.response.status_code, OK, self.response.data)

    def test_response_body(self):
        with app.test_request_context('/'):
            actuals = json.loads(self.response.data)
            self.assertIsNotNone(actuals.get('owned_items', None))
            owned_items = actuals.get('owned_items', None)
            self.assertEqual(2, len(owned_items), 'Owned item count not 2, {}'.format(actuals))
            for actual in owned_items:
                item = Item.query.get(actual.get('item_id', 0))
                self.assertDictContainsSubset(
                    expected=get_expected_owned_item_response(self.user_publisher, item),
                    actual=actual
                )

    def test_user_owned_items(self):
        with app.test_request_context('/'):
            self.session.add(self.user_others)
            self.session.add(self.owned_other)
            header = self.request_headers.copy()
            header['Authorization'] = 'JWT {}'.format(
                get_jwt_valid_token(self.user_others))
            request_url = 'v1/owned_items?user_id={user_id}'.format(
                user_id=self.user_others.id
            )
            response = self.client.get(request_url, headers=header)
            self.assertEqual(response.status_code, OK)
            actuals = json.loads(response.data)
            actual_owned_items = actuals.get('owned_items', None)
            self.assertEqual(1, len(actual_owned_items), 'Owned item count not 1, {}'.format(actuals))
            self.assertDictContainsSubset(
                expected=get_expected_owned_item_response(
                    self.user_others, self.owned_other.item, owned_item=self.owned_other),
                actual=actual_owned_items[0]
            )

    def test_user_not_owned(self):
        with app.test_request_context('/'):
            self.session.add(self.user_verified)
            header = self.request_headers.copy()
            header['Authorization'] = 'JWT {}'.format(
                get_jwt_valid_token(self.user_verified))
            request_url = 'v1/owned_items?user_id={user_id}'.format(
                user_id=self.user_verified.id
            )
            response = self.client.get(request_url, headers=header)
            self.assertEqual(
                response.status_code, NO_CONTENT,
                'response status code != 204-NO_CONTENT. '
                'Response: {} {}'.format(
                    response.status_code, response.data))

    def test_etag_support(self):

        response = self.client.get(self.request_url, headers=self.request_headers)

        self.assertIn('ETag', response.headers)

        etag = response.headers['ETag']
        not_modified_headers = self.request_headers.copy()
        not_modified_headers['If-None-Match'] = etag

        response_not_modified = self.client.get(
            self.request_url,
            headers=not_modified_headers
        )

        self.assertEqual(response_not_modified.status_code, NOT_MODIFIED)


def get_jwt_valid_token(user):
    with app.test_request_context('/'):
        return jwt_encode_from_user(user)


def get_expected_owned_item_response(user, item, owned_item=None):
    return {
        "user_id": user.id,
        "id": owned_item.id if owned_item else -1 * item.id,
        # "created": "2016-11-08T03:27:48.594212",
        "created": owned_item.created.isoformat() if owned_item else item.created.isoformat(),
        "modified": owned_item.modified.isoformat() if owned_item else item.created.isoformat(),
        "is_active": True,
        # "authors": [{'name': a.name, 'id': a.id} for a in item.authors],
        # "itemtype_id": item_types.enum_to_id(item.item_type),
        # "order_line_id": owned_item.orderline_id if owned_item else None,
        # "user_buffet_id": None,
        # "vendor_id": item.brand.vendor_id,
        # "brand_id": item.brand_id,
        'brand': {
            'id': item.brand.id,
            'name': item.brand.name
        },
        "edition_code": item.edition_code,
        "item_id": item.id,
        "item": {
            "id": item.id,
            "name": item.name,
            'image_highres': item.image_highres,
            'thumb_image_highres': item.thumb_image_highres,
            'media_base_url': app.config['MEDIA_BASE_URL'],
            'brand_id': item.brand.id,
            'parental_control_id': item.parentalcontrol_id,
            'content_type': {FILE_TYPES[k]: k for k in FILE_TYPES}[item.content_type],
            'item_type_id': {ITEM_TYPES[k]: k for k in ITEM_TYPES}[item.item_type],
            'issue_number': item.issue_number,
            'release_date': isoformat(item.release_date),
            "edition_code": item.edition_code,
            "brand": {
                "id": item.brand_id,
                "name": item.brand.name
            },
            "vendor": {
                "id": item.brand.vendor_id,
                "name": item.brand.vendor.name
            },
        },
    }
