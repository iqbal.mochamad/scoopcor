from __future__ import unicode_literals
import os
import json

from httplib import OK, NOT_FOUND
from unittest import TestCase, SkipTest
from faker import Factory
from nose.tools import nottest

from app import app, db
from app.items.api import ItemCheckRelatedFilesApi
from app.items.file_server_gateway import InternalFileServerGateway
from tests.fixtures.sqlalchemy.items import ItemFactory, ItemFileFactory
from tests.fixtures.sqlalchemy.master import BrandFactory


class ItemCheckRelatedFilesApiTests(TestCase):

    db = db
    maxDiff = None

    def setUp(self):

        # make sure the test mode flag is set, or bail out
        if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
            raise SkipTest("COR is not in testing mode.  Cannot continue.")

        # 8 = Brand ID
        # ID_TEC2010ED22 = edition code
        self.file_name = 'images/1/8/ID_TEC2010ED22.zip'
        self.internal_path_for_testing = '/tmp'

        self.db.drop_all()
        self.db.create_all()

    def tearDown(self):
        self.db.session.rollback()
        self.db.drop_all()

    @nottest
    def create_folder_and_file_for_testing(self, path, brand_id, file_name):
        try:
            test_directory = os.path.join(path, brand_id)
            if not os.path.exists(test_directory):
                os.makedirs(test_directory)

            # item full path for testing: '/tmp/8/ID_TEC2010ED22.zip'
            item_file_full_path = os.path.join(self.internal_path_for_testing,
                                               InternalFileServerGateway.get_file_name(file_name))
            if not os.path.exists(item_file_full_path):
                with open(item_file_full_path, 'w') as f:
                    faker = Factory.create()
                    f.write(faker.text())
                    f.close()
            return item_file_full_path
        except Exception as e:
            pass

    def test_method_get_response_data_all_is_well(self):

        item_file_full_path = self.create_folder_and_file_for_testing(
            path=self.internal_path_for_testing,
            brand_id='8',
            file_name=self.file_name)

        file_size = os.path.getsize(item_file_full_path)

        expected = {
            'NAS': {
                'is_exists': True,
                'read_access': 'public',
                'error_message': '',
                'file_size': file_size,
                },
            'S3': {
                'is_exists': True,
                'read_access': 'public',
                'error_message': '',
                'file_size': 45872746,
                }
        }

        actual = ItemCheckRelatedFilesApi.get_response_data(
            file_name=self.file_name,
            internal_server_path=self.internal_path_for_testing,
            aws_server=app.config['BOTO3_SERVER'],
            aws_region=app.config['BOTO3_AWS_REGION'],
            aws_app_id=app.config['BOTO3_AWS_APP_ID'],
            aws_secret=app.config['BOTO3_AWS_APP_SECRET'],
            aws_bucket_name=app.config['BOTO3_BUCKET_NAME']
        )

        actual_nas = actual.get('NAS')
        actual_nas_last_modified_date = actual_nas.pop('last_modified_date')
        actual_s3 = actual.get('S3')
        actual_s3_last_modified_date = actual_s3.pop('last_modified_date')
        actual['NAS'] = actual_nas
        actual['S3'] = actual_s3
        self.assertIsNotNone(actual_nas_last_modified_date)
        self.assertIsNotNone(actual_s3_last_modified_date)
        self.assertDictEqual(actual, expected)

    def test_get_all_is_well(self):

        s = db.session()
        brand = BrandFactory(id=8, name='Sample1')
        item = ItemFactory(
            id=123,
            name='test item check files',
            brand=brand
        )
        itemfile = ItemFileFactory(
            item=item,
            file_name=self.file_name,
        )
        s.commit()

        expected = {
            'NAS': {
                # because we don't have static linked folder to NAS, it will return False!
                'is_exists': False,
                'read_access': 'public',
                'error_message': '',
                'file_size': 0,
                },
            'S3': {
                'is_exists': True,
                'read_access': 'public',
                'error_message': '',
                'file_size': 45872746,
                }
        }

        c = app.test_client(use_cookies=False)

        get_url = '/v1/items/{}/check-files'.format(item.id)
        response = c.get(get_url)

        if response.status_code == OK:
            actual = json.loads(response.data)

            actual_nas = actual.get('NAS')
            actual_nas_last_modified_date = actual_nas.pop('last_modified_date')
            actual_s3 = actual.get('S3')
            actual_s3_last_modified_date = actual_s3.pop('last_modified_date')
            actual['NAS'] = actual_nas
            actual['S3'] = actual_s3

            self.assertDictEqual(actual, expected)

        self.assertEqual(response.status_code, OK)
        self.assertEqual(response.content_type, 'application/json')

    def test_get_item_file_not_found(self):
        c = app.test_client(use_cookies=False)

        get_url = '/v1/items/{}/check-files'.format(100)
        response = c.get(get_url)

        self.assertEqual(response.status_code, NOT_FOUND)
        self.assertEqual(response.content_type, 'application/json')

