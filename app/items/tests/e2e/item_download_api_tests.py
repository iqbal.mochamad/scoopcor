from __future__ import unicode_literals, absolute_import

from datetime import timedelta
from faker import Factory
from httplib import OK, FORBIDDEN, UNAUTHORIZED
from flask import g
from nose.tools import istest, nottest

from app import app, db, set_request_id
from app.auth.models import UserInfo, ANONYMOUS_USER
from app.items.models import Item
from app.master.models import Currencies
from app.offers.choices.offer_type import BUFFET
from app.orders.choices import COMPLETE
from app.payments.choices import CHARGED
from app.users.users import User, Role
from app.utils.datetimes import get_utc_nieve
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import UserItemFactory
from app.users.tests.fixtures import UserFactory
from app.users.tests.fixtures_user_buffet import UserBuffetFactory
from tests.fixtures.sqlalchemy.items import ItemFactory, ItemFileFactory
from tests.fixtures.sqlalchemy.master import BrandFactory, VendorFactory
from tests.fixtures.sqlalchemy.offers import OfferBuffetFactory, OfferTypeFactory, OfferFactory, standard_offer_types
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.payments.gateways import PaymentGatewayFactory
from tests import testcases


""" These are a set of test to test new Item Download API with url: '/v1/items/{0.id}/download'
the code is in: ItemContentFileDownloadApi

for old item download api.. see '/v1/downloads' class ItemDownloadApi
    see test file: item_downloads_tests.py
"""


@nottest
class ItemDownloadApiTestsBase(testcases.DetailTestCase):
    """ Base class to test user download api -- for valid response """
    fixture_factory = ItemFactory
    request_url = '/v1/items/{0.id}/download'
    request_headers = {'Accept': 'application/zip'}

    @classmethod
    def mocked_g_user(cls):
        pass

    @classmethod
    def setUpClass(cls):
        ctx = app.test_request_context('/')
        ctx.push()
        cls.mocked_g_user()
        super(ItemDownloadApiTestsBase, cls).setUpClass()

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        s.expire_on_commit = False
        cls.item = s.query(Item).first()
        return cls.item

    @classmethod
    def remove_fixtures_from_session(cls):
        pass

    def test_response_body(self):
        pass

    def test_response_status_code(self):
        self.assertEqual(OK, self.response.status_code)

    def test_response_content_type(self):
        self.assertEqual(self.response.content_type, 'application/zip')

    def test_x_accel_redirect(self):
        self.assertIn('x-accel-redirect', self.response.headers)
        actual_download_url = self.response.headers.get('x-accel-redirect')

        zip_file_path = '{0.brand_id}/{0.edition_code}.zip'.format(self.item)

        expected_url = ('{secure_media_base}/'
                        'item_content_files/'
                        'processed/{zip_file_path}').format(
                            secure_media_base=app.config['NGINX_SECURE_MEDIA_BASE'],
                            zip_file_path=zip_file_path)
        self.assertEqual(actual_download_url, expected_url)

    def test_content_length(self):
        self.assertEqual(self.response.content_length, 0)

    @classmethod
    def tearDownClass(cls):
        if getattr(g, 'current_user', None):
            db.session.expunge(g.current_user)
        db.session.expunge(cls.item)
        db.session.expunge_all()


@istest
class UserSuperAdminTests(ItemDownloadApiTestsBase):
    """ Test for user with super admin roles, should be able to download anything,
    even not exists in user owned item
    """

    @classmethod
    def mocked_g_user(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                fake_authorized_user,
            ]
        }


@istest
class UserThatOwnedItemTestsTests(ItemDownloadApiTestsBase):
    """ Test for user that owned the item (data exists in user-item tables) """

    @classmethod
    def mocked_g_user(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_user_with_item,
            ]
        }


@istest
class UserWithValidBuffetTests(ItemDownloadApiTestsBase):
    """ Test for user that didn't have the item directly but have valid Buffet for that item
     User Buffet Exists and Valid
     and user owned item also already exists for that user buffet
    """

    @classmethod
    def mocked_g_user(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_user_with_valid_buffet_already_registered_in_user_item,
            ]
        }


@istest
class UserWithValidBuffetCreateUserItemTests(ItemDownloadApiTestsBase):
    """ Test for user that didn't have the item directly but have valid Buffet for that item
     User Buffet Exists and Valid
     BUT user owned item not yet exists for that user buffet
    """

    @classmethod
    def mocked_g_user(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_user_with_valid_buffet_not_yet_registered_in_user_item,
            ]
        }


@istest
class DownloadByBuffetVendorsTests(ItemDownloadApiTestsBase):
    """ Test for user that didn't have the item directly but have valid Buffet for that item via OFFER-VENDORS list
    """

    @classmethod
    def mocked_g_user(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_user_for_buffet_vendors,
            ]
        }


@nottest
class InvalidUserDownloadTestsBase(testcases.DetailTestCase):
    """ Base class to tests forbidden/invalid user item download """

    fixture_factory = ItemFactory
    request_url = '/v1/items/{0.id}/download'
    request_headers = {'Accept': 'application/zip'}

    @classmethod
    def mocked_g_user(cls):
        pass

    @classmethod
    def setUpClass(cls):
        ctx = app.test_request_context('/')
        ctx.push()
        cls.mocked_g_user()
        super(InvalidUserDownloadTestsBase, cls).setUpClass()

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        s.expire_on_commit = False
        cls.item = s.query(Item).first()
        return cls.item

    @classmethod
    def remove_fixtures_from_session(cls):
        pass

    def test_response_body(self):
        return

    def test_response_code(self):
        self.assertEqual(FORBIDDEN, self.response.status_code)

    @classmethod
    def tearDownClass(cls):
        if getattr(g, 'current_user', None):
            db.session.expunge(g.current_user)
        db.session.expunge(cls.item)
        db.session.expunge_all()


@istest
class UnAuthorizedUserDownloadTests(InvalidUserDownloadTestsBase):

    def test_response_code(self):
        self.assertEqual(UNAUTHORIZED, self.response.status_code)

    @classmethod
    def mocked_g_user(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                not_authorized_user,
            ]
        }


@istest
class UserWithExpiredBuffetTests(InvalidUserDownloadTestsBase):
    """ user buffet already expired, forbidden """

    @classmethod
    def mocked_g_user(cls):
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_user_with_expired_user_buffet,
            ]
        }

_fake = Factory.create()
client = app.test_client(use_cookies=False)
original_before_request_funcs = None


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()
    global original_before_request_funcs
    original_before_request_funcs = app.before_request_funcs
    init_data()


def init_data():
    s = db.session()
    s.expire_on_commit = False
    generate_static_sql_fixtures(s)
    vendor = VendorFactory()
    brand = BrandFactory(id=212)
    brand2 = BrandFactory(id=2123, vendor=vendor)
    offer_type = standard_offer_types()['buffet']
    offer = OfferFactory(offer_type=offer_type, is_free=True, price_idr=0, price_usd=10, price_point=10)
    offer.brands.append(brand)
    offer.vendors.append(vendor)
    item = ItemFactory(id=10, content_type='pdf', brand=brand, edition_code='test_item_2')
    item2 = ItemFactory(id=11, content_type='pdf', brand=brand2, edition_code='test_item_11')
    item_file = ItemFileFactory(
        item=item,
        file_name='images/1/{0.brand.id}/{0.edition_code}.zip'.format(item))
    item_file = ItemFileFactory(
        item=item2,
        file_name='images/1/{0.brand.id}/{0.edition_code}.zip'.format(item2))
    offer_buffet = OfferBuffetFactory(
        available_from=get_utc_nieve() - timedelta(days=7),
        available_until=get_utc_nieve() + timedelta(days=7),
        offer=offer
    )

    order_id = 123455
    cur_idr = s.query(Currencies).get(1)
    order = OrderFactory(id=order_id,
                         total_amount=offer.price_idr, final_amount=offer.price_idr,
                         user_id=1122,
                         order_status=COMPLETE)
    order_line = OrderLineFactory(order=order, offer=offer, user_id=1122, currency_code='IDR',
                                  price=offer.price_idr, final_price=offer.price_idr,
                                  orderline_status=COMPLETE)

    # generate data for testing user that owned the item

    # user super admin
    user1234 = UserFactory(id=1234)
    role_super_admin = s.query(Role).get(1)
    user1234.roles.append(role_super_admin)

    user_reg = UserFactory(id=12345)

    user = UserFactory(id=1122)
    user_item = UserItemFactory(user_id=user.id, item=item, orderline=order_line)

    # for: test user buffet valid - already registered in user owned item
    # generate data for testing user that owned the item via buffet/premum purchase
    user33 = UserFactory(id=1133)
    user_buffet = UserBuffetFactory(
        user_id=user33.id, valid_to=get_utc_nieve()+timedelta(days=7),
        offerbuffet=offer_buffet, orderline=order_line)
    user_item = UserItemFactory(user_id=user33.id, item=item, orderline=order_line,
                                user_buffet=user_buffet)

    # for: test user buffet valid - not yet registered in user owned item
    user44 = UserFactory(id=1144)
    user_buffet = UserBuffetFactory(
        user_id=user44.id, valid_to=get_utc_nieve()+timedelta(days=7),
        offerbuffet=offer_buffet, orderline=order_line)

    # for: test user buffet invalid (already expired)
    user55 = UserFactory(id=1155)
    user_buffet = UserBuffetFactory(
        user_id=user55.id, valid_to=get_utc_nieve()-timedelta(days=7),
        offerbuffet=offer_buffet, orderline=order_line)
    user_item = UserItemFactory(user_id=user55.id, item=item, orderline=order_line,
                                user_buffet=user_buffet)

    # for: test user buffet - with item not exists in user-item but vendor exists in premium/buffet offers
    user66 = UserFactory(id=116611)
    user_buffet = UserBuffetFactory(
        user_id=user66.id, valid_to=get_utc_nieve() + timedelta(days=7),
        offerbuffet=offer_buffet, orderline=order_line)
    s.commit()


def teardown_module():
    db.session().close_all()
    db.drop_all()
    app.before_request_funcs = original_before_request_funcs


def get_user(session, user_id):
    return session.query(User).get(user_id)


def not_authorized_user(*args, **kwargs):
    g.user = ANONYMOUS_USER
    g.current_user = None


def fake_authorized_user(*args, **kwargs):
    g.user = UserInfo(username="ramdani@apps-foundry.com",
                      user_id=1234,
                      token='abcd',
                      perm=['can_read_write_global_all', 'b', 'c', ])
    g.current_user = get_user(db.session(), 1234)


def init_g_regular_user():
    g.user = UserInfo(username='dfdsafsdf', user_id=12345, token='abcde', perm=['abc',])
    g.current_user = get_user(db.session(), 12345)


def init_user_with_item(*args, **kwargs):
    g.user = UserInfo(username="ramdani123@apps-foundry.com",
                      user_id=1122,
                      token='abcd',
                      perm=['bc', 'b', 'c', ])
    g.current_user = get_user(db.session(), 1122)


def init_user_with_valid_buffet_already_registered_in_user_item(*args, **kwargs):
    g.user = UserInfo(username="ramdani33@apps-foundry.com",
                      user_id=1133,
                      token='abcd',
                      perm=['bc', 'b', 'c', ])
    g.current_user = get_user(db.session(), 1133)


def init_user_with_valid_buffet_not_yet_registered_in_user_item(*args, **kwargs):
    g.user = UserInfo(username="ramdani44@apps-foundry.com",
                      user_id=1144,
                      token='abcd',
                      perm=['bc', 'b', 'c', ])
    g.current_user = get_user(db.session(), 1144)


def init_user_with_expired_user_buffet(*args, **kwargs):
    g.user = UserInfo(username="ramdani55@apps-foundry.com",
                      user_id=1155,
                      token='abcd',
                      perm=['bc', 'b', 'c', ])
    g.current_user = get_user(db.session(), 1155)


def init_user_for_buffet_vendors(*args, **kwargs):
    g.user = UserInfo(username="ramdani66@apps-foundry.com",
                      user_id=116611,
                      token='abcd',
                      perm=['bc', 'b', 'c', ])
    g.current_user = get_user(db.session(), 116611)
