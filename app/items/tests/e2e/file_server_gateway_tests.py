import os
import boto3

from unittest import TestCase
from datetime import datetime

from botocore.exceptions import ClientError
from faker import Factory

from app import app
from app.items.file_server_gateway import AmazonS3ServerGateway, InternalFileServerGateway


class InternalFileServerGatewayTests(TestCase):

    def setUp(self):
        self.server = InternalFileServerGateway(
            path='/tmp'
        )
        # path=app.config['BOTO3_DIR_DOWNLOAD']

    def test_file_exists(self):

        file_name = 'testfile.txt'

        # create dummy file for test
        item_file_full_path = os.path.join(self.server.path, file_name)
        if not os.path.exists(item_file_full_path):
            open(item_file_full_path, 'w').close()

        expected = {
                'is_exists': True,
                'read_access': 'public',
                'error_message': '',
                'file_size': self.server.get_file_size(file_name),
                'last_modified_date': self.server.get_last_modified_date(file_name=file_name),
            }

        actual = self.server.get_file_info(file_name)

        actual_exists = self.server.is_file_exists(file_name=file_name)

        self.assertEqual(actual_exists, True)
        self.assertEqual(actual, expected)

    def test_file_not_exists(self):

        file_name = 'dummyfile.abcd'

        actual = self.server.is_file_exists(file_name=file_name)

        expected = False

        self.assertEqual(actual, expected)


class AmazonS3FileServerGatewayTests(TestCase):

    def setUp(self):

        s3 = boto3.client(
            's3',
            app.config['BOTO3_AWS_REGION'],
            aws_access_key_id=app.config['BOTO3_AWS_APP_ID'],
            aws_secret_access_key=app.config['BOTO3_AWS_APP_SECRET'],
        )

        self.server = AmazonS3ServerGateway(
            s3,
            aws_server=app.config['BOTO3_SERVER'],
            aws_bucket_name=app.config['BOTO3_BUCKET_NAME']
        )

        # self.file_name = 'images/1/21896/ID_HCO2015MTH03ABAR.zip'
        # 'images/1/8/ID_TEC2010ED22.zip'
        # create file first
        self.file_name = 'cor_test_s3_upload_test_exists.zip'

    def create_dummy_file_then_upload_to_s3(self, file_name):
        item_file_full_path = os.path.join('/tmp/', file_name)
        # if not os.path.exists(item_file_full_path):
        with open(item_file_full_path, 'w') as f:
            faker = Factory.create()
            f.write(faker.text())
            f.close()

        # upload to s3
        self.server.file_upload(item_file_full_path=item_file_full_path, key=file_name)

    def tearDown(self):
        pass

    def delete_dummy_file_from_s3(self, file_name):
        # delete test file from s3
        s3 = boto3.resource(
            's3',
            app.config['BOTO3_AWS_REGION'],
            aws_access_key_id=app.config['BOTO3_AWS_APP_ID'],
            aws_secret_access_key=app.config['BOTO3_AWS_APP_SECRET'],
        )
        obj = s3.Object(app.config['BOTO3_BUCKET_NAME'], file_name)
        if obj:
            obj.delete()

    def test_upload_file(self):

        # create file first
        file_name1 = 'cor_test_s3_upload.zip'
        self.create_dummy_file_then_upload_to_s3(file_name1)

        # get expected file size from local storage
        nas = InternalFileServerGateway(
            path='/tmp'
        )
        expected = nas.get_file_size(file_name1)

        actual = self.server.get_file_size(file_name1)

        self.assertEqual(actual, expected)

        # delete test file from s3
        self.delete_dummy_file_from_s3(file_name1)

        with self.assertRaises(ClientError):
            self.server.get_file_size(file_name1)

    def test_file_exists(self):

        self.create_dummy_file_then_upload_to_s3(self.file_name)

        actual_exists = self.server.is_file_exists(file_name=self.file_name)

        expected = {
                'is_exists': True,
                'read_access': 'public',
                'error_message': '',
                'file_size': self.server.get_file_size(self.file_name),
                'last_modified_date': self.server.get_last_modified_date(file_name=self.file_name),
            }

        actual = self.server.get_file_info(self.file_name)

        self.assertEqual(actual_exists, True)
        self.assertEqual(actual, expected)

        self.delete_dummy_file_from_s3(self.file_name)

    def test_file_not_exists(self):

        file_name = 'dummyfile.abcd'

        actual = self.server.is_file_exists(file_name=file_name)

        self.assertEqual(actual, False)

    def test_file_access_status_public(self):
        # self.file_name = 'images/1/8/ID_TEC2010ED22.zip'

        self.create_dummy_file_then_upload_to_s3(self.file_name)

        actual = self.server.file_access_status(file_name=self.file_name)

        expected = 'public'

        self.assertEqual(actual, expected)

        self.delete_dummy_file_from_s3(self.file_name)


