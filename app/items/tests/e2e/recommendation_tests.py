from __future__ import unicode_literals, absolute_import

import unittest
import json
from datetime import timedelta
from httplib import OK

from nose.tools import nottest
from sqlalchemy import desc

from app import db, app
from app.auth.helpers import jwt_encode_from_user
from app.utils.testcases import E2EBase
from app.items.choices import ITEM_TYPES
from app.items.models import Item
from app.master.models import Brand
from app.offers.choices.offer_type import BUNDLE, BUFFET
from app.offers.models import OfferType
from app.utils.datetimes import datetime_to_isoformat, get_utc_nieve
from tests.fixtures.helpers import generate_static_sql_fixtures
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.items import ItemFactory, AuthorFactory
from tests.fixtures.sqlalchemy.master import BrandFactory, VendorFactory, CategoryFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from faker import Factory

fake = Factory.create()


class ApiTest(unittest.TestCase):

    maxDiff = None

    client = app.test_client(use_cookies=False)
    headers = {
        'Accept': 'application/vnd.scoop.v3+json',
        # 'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
    }
    platform_id = 4

    @classmethod
    def setUpClass(cls):
        db.session().close_all()
        db.drop_all()
        db.create_all()
        # clean up previous cached data
        ctx = app.test_request_context('/')
        ctx.push()
        app.kvs.flushdb()

        session = db.session()
        session.expire_on_commit = False
        generate_static_sql_fixtures(session)
        cls.user = UserFactory(id=12345, allow_age_restricted_content=True)
        cls.user_kid = UserFactory(id=12346, allow_age_restricted_content=False)

        cls.build_data(session)

    @classmethod
    def build_data(cls, session):

        # create valid book
        # self.items index no: 0, 1, 2 is valid book, all related by the same categories
        # we will test to search item recommendation based on self.item[2], self.item[0] and 1 should be selected
        # see app.utils.shim.languages
        cls.language_id = 2
        language_code = 'ind'
        cls.country_id = 1
        country_code = 'id'
        cls.category_id = 3312
        category = CategoryFactory(id=cls.category_id)
        cls.author_id_book_first = 112
        cls.items = []
        for _ in range(3):
            author = AuthorFactory(id=cls.author_id_book_first+_)
            brand = BrandFactory(id=200+_)
            item = ItemFactory(
                id=1000-_, item_status='ready for consume',
                item_type=Item.Types.book.value,
                brand=brand,
                parentalcontrol_id=1 if _ == 0 else 2,
                languages=[language_code, ] if _ == 0 else None,
                countries=[country_code, ] if _ == 0 else None,
                edition_code='{}'.format(1000+_+1))
            item.slug = item.id
            item.authors.append(author)
            item.categories.append(category)
            cls.items.append(item)

        # create valid magazine
        # self.items index no: 3, 4, 5 is valid magazine, all related by the same categories
        # we will test to search item recommendation based on self.item[5],
        # self.item[3] and 4 should be selected
        language_code_eng = 'eng'
        country_code_eng = 'us'
        release_date = get_utc_nieve()
        for _ in range(3):
            vendor_mag1 = VendorFactory(id=300+_)
            brand_mag = BrandFactory(id=300+_, vendor=vendor_mag1)
            item = ItemFactory(id=600-_, item_status='ready for consume',
                               item_type=Item.Types.magazine.value,
                               brand=brand_mag,
                               parentalcontrol_id=2, languages=[language_code_eng, ],
                               countries=[country_code_eng, ],
                               sort_priority=10-_,
                               release_date=release_date,
                               edition_code='{}'.format(600+_+1))
            item.slug = item.id
            item.categories.append(category)
            cls.items.append(item)

        # create primary latest edition of newspaper that should get selected
        # self.items 6, 7, 8 is valid newspaper, all related by the same categories
        # we will test to search item recommendation based on self.item[8],
        # newspaper recommendation only select item with release date up to 7 days before.
        # so the result will valid for self.item with id = 400 only (self.item[6] only)
        release_date = get_utc_nieve()
        for _ in range(3):
            brand_news = BrandFactory(id=400+_)
            item = ItemFactory(id=400-_, item_status='ready for consume',
                               item_type=Item.Types.newspaper.value,
                               brand=brand_news,
                               parentalcontrol_id=2, languages=[language_code_eng, ],
                               countries=[country_code_eng, ],
                               release_date=release_date if _ == 0 else release_date - timedelta(days=20),
                               sort_priority=10-_,
                               edition_code='{}'.format(400+_+1))
            item.slug = item.id
            item.categories.append(category)
            cls.items.append(item)

        session.commit()

        # create other old edition of newspaper that should not get selected
        for _ in range(3):
            brand_news = session.query(Brand).get(400+_)
            item = ItemFactory(id=300-_, item_status='ready for consume',
                               item_type=Item.Types.newspaper.value,
                               brand=brand_news,
                               parentalcontrol_id=2, languages=[language_code_eng, ],
                               countries=[country_code_eng, ],
                               release_date=get_utc_nieve() - timedelta(days=30),
                               sort_priority=10 - _,
                               edition_code='{}'.format(300+_+1))
            item.slug = item.id
            item.categories.append(category)
            cls.items.append(item)

        # generate non active items or different category, should not get selected in api
        category_others = CategoryFactory(id=3311)
        for _ in range(3):
            is_active = True if _ < 2 else False
            item = ItemFactory(id=_ + 1, item_status='ready for consume',
                               item_type=Item.Types.book.value, is_active=is_active,
                               edition_code='{}'.format(_ + 1))
            item.slug = item.id
            item.categories.append(category_others)

        for _ in range(3):
            is_active = True if _ < 2 else False
            item = ItemFactory(id=_ + 4, item_status='ready for consume',
                               item_type=Item.Types.magazine.value, is_active=is_active,
                               edition_code='{}'.format(3 + _ + 1))
            item.slug = item.id
            item.categories.append(category_others)

        for _ in range(3):
            is_active = True if _ < 2 else False
            item = ItemFactory(id=_ + 10, item_status='ready for consume',
                               item_type=Item.Types.newspaper.value, is_active=is_active,
                               edition_code='{}'.format(6 + _ + 1))
            item.slug = item.id
            item.categories.append(category_others)

        from app.offers.choices.offer_type import SINGLE
        offer_type = session.query(OfferType).get(SINGLE)

        cls.FREE_ITEM_INDEX_LIST = [3, 4]
        # build offer item with single offer
        for index in range(12):
            if index in cls.FREE_ITEM_INDEX_LIST:
                # create some data with free item
                offer = OfferFactory(id=index+1, offer_type=offer_type, is_free=True, price_idr=0, price_usd=0,
                                     price_point=0)
            else:
                offer = OfferFactory(id=index+1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                     price_point=10)
            offer.items.append(cls.items[index])
            # oi = offer_item.insert()
            # oi.offer_id = offer.id
            # oi.item_id = cls.items[index].id
            # db.engine.execute(offer_item.insert(), offer_id=offer.id, item_id=cls.items[index].id)

        # create bundle offer
        cls.BUNDLE_ITEM_INDEX_LIST = [1, 4]
        offer_type_bundle = session.query(OfferType).get(BUNDLE)
        for index in cls.BUNDLE_ITEM_INDEX_LIST:
            offer = OfferFactory(id=50000 + index, offer_type=offer_type_bundle, is_free=False, price_idr=10000, price_usd=10,
                                 price_point=10)
            offer.items.append(cls.items[index])

        # create BUFFET offer
        cls.BUFFET_ITEM_INDEX_LIST = [1, 2]
        offer_type_buffet = session.query(OfferType).get(BUFFET)
        for index in cls.BUFFET_ITEM_INDEX_LIST:
            offer = OfferFactory(id=60000 + index, offer_type=offer_type_buffet, is_free=False, price_idr=10000, price_usd=10,
                                 price_point=10)
            offer.brands.append(cls.items[index].brand)

        session.commit()

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.session.remove()
        db.drop_all()

    def get_dict_item(self, item):
        restricted, allowed = item.get_restriction_country()
        display_offer, display_price = item.get_offers(platform_id=self.platform_id)
        return {
            'id': item.id,
            'name': item.name,
            'item_type_id': {ITEM_TYPES[k]: k for k in ITEM_TYPES}[item.item_type],
            'brand_id': item.brand_id,
            'vendor': {
                'slug': item.brand.vendor.slug,
                'name': item.brand.vendor.name,
                'id': item.brand.vendor.id,
            },
            "restricted_countries": restricted,
            "allowed_countries": allowed,
            'media_base_url': app.config['MEDIA_BASE_URL'],
            "thumb_image_highres": item.thumb_image_highres,
            "thumb_image_normal": item.thumb_image_normal,
            "issue_number": item.issue_number,
            "authors": item.get_authors(),
            "display_offers": display_offer,
            "display_price": display_price,
            "is_latest": item.get_is_latest(), # it should be optimized, not needed.. just read release data!
            'release_date': datetime_to_isoformat(item.release_date),
            "parental_control_id": item.parentalcontrol_id,
            "edition_code": item.edition_code,
        }

    def test_purge_cache(self):
        response = self.client.open(
            '/v1/items/1122/recommendations',
            method='PURGE',
            headers=self.headers)
        self.assertEqual(response.status_code, OK, response.data)

    def test_book(self):
        session = db.session()
        # self.items 0, 1, 2 is book
        session.add(self.items[2])
        resp = self.client.get('/v1/items/{}/recommendations'.format(
            self.items[2].id), headers=self.headers)
        self.assertEqual(resp.status_code, OK, resp)
        resp_data = json.loads(resp.data)

        # filter by self.item[2].id related categories, self.item[0 & 1] should be selected
        self.assert_response_body(resp_data, [self.items[0], self.items[1], ])

    def assert_response_body(self, resp_data, expected_items):
        # with app.test_request_context('/'):
        self.assertEqual(
            len(resp_data.get('items')), len(expected_items),
            'Actual count {} != expected {}. Response data {} '.format(
                len(resp_data.get('items')), len(expected_items), resp_data
            ))
        session = db.session()
        expected_resp_items = []
        index = 0
        for item in expected_items:
            session.add(item)
            expected = self.get_dict_item(item)
            self.assertEqual(
                item.id, resp_data.get('items')[index].get('id', None),
                'At item pos: {}, expected {} != actual {}. response = {}'.format(
                    index+1,
                    item.id,
                    resp_data.get('items')[index].get('id', None),
                    resp_data.get('items')[index])
            )
            self.assertDictContainsSubset(expected, resp_data.get('items')[index])
            index += 1

    def test_by_author(self):
        resp = self.client.get('/v1/items/{}/recommendations?author_id={}'.format(
            self.items[2].id, self.author_id_book_first), headers=self.headers)
        self.assertEqual(resp.status_code, OK, resp)
        resp_data = json.loads(resp.data)

        # filter by self.item[2].id related categories and author_id item 0, self.item[0] should be selected
        self.assert_response_body(resp_data, [self.items[0], ])

    def test_by_parental_control(self):
        resp = self.client.get('/v1/items/{}/recommendations?parental_control_id=1'.format(
            self.items[2].id), headers=self.headers)
        self.assertEqual(resp.status_code, OK, resp)
        resp_data = json.loads(resp.data)

        # filter by self.item[2].id related categories and parental_control_id=1,
        # self.item[0] should be selected (see data creation, the only book with parental_control_id=1)
        self.assert_response_body(resp_data, [self.items[0], ])

        resp = self.client.get('/v1/items/{}/recommendations?parental_control_id=0'.format(
            self.items[2].id), headers=self.headers)
        self.assertEqual(resp.status_code, OK, resp)
        resp_data = json.loads(resp.data)
        # filter by self.item[2].id related categories and parental_control_id=0 (==view all item/no age restriction),
        # self.item[0 & 1] should be selected,
        self.assert_response_body(resp_data, [self.items[0], self.items[1], ])

        # filter by user setting "allow_age_restricted_content"
        session = db.session()
        header_kids = self.headers.copy()
        header_kids['Authorization'] = 'JWT {}'.format(jwt_encode_from_user(self.user_kid))
        resp = self.client.get('/v1/items/{}/recommendations'.format(
            self.items[2].id), headers=header_kids)
        self.assertEqual(resp.status_code, OK, resp)
        resp_data = json.loads(resp.data)
        # filter by self.item[2].id related categories and user_kid.allow_age_restricted_content=False,
        # self.item[0] should be selected (see data creation, the only book with parental_control_id=1=all ages)
        self.assert_response_body(resp_data, [self.items[0], ])

        # test adult magazine only
        resp = self.client.get('/v1/items/{}/recommendations?parental_control_id=2'.format(
            self.items[2].id), headers=self.headers)
        self.assertEqual(resp.status_code, OK, resp)
        resp_data = json.loads(resp.data)
        # filter by self.item[2].id related categories and parental_control_id=2 (==view adult magazine only),
        # self.item[1] should be selected,
        self.assert_response_body(resp_data, [self.items[1], ])

    def test_by_country(self):
        resp = self.client.get('/v1/items/{}/recommendations?country_id={}'.format(
            self.items[2].id, self.country_id), headers=self.headers)
        self.assertEqual(resp.status_code, OK, resp)
        resp_data = json.loads(resp.data)

        # filter by self.item[2].id related categories and country, self.item[0] should be selected
        self.assert_response_body(resp_data, [self.items[0], ])

    def test_by_language(self):
        resp = self.client.get('/v1/items/{}/recommendations?language_id={}'.format(
            self.items[2].id, self.language_id), headers=self.headers)
        self.assertEqual(resp.status_code, OK, resp)
        resp_data = json.loads(resp.data)

        # filter by self.item[2].id related categories and self.language_id,
        # self.item[0] should be selected
        self.assert_response_body(resp_data, [self.items[0], ])

    def test_magazine(self):
        session = db.session()
        # self.items 3, 4, 5 is magazine
        session.add(self.items[5])
        resp = self.client.get('/v1/items/{}/recommendations'.format(self.items[5].id), headers=self.headers)
        resp_data = json.loads(resp.data)
        self.assertEqual(resp.status_code, OK, resp)

        # filter by self.item[5].id related categories, self.item[3 & 4] should be selected
        self.assert_response_body(resp_data, [self.items[3], self.items[4], ])

    def test_by_brand_magazine(self):
        session = db.session()
        # self.items 3, 4, 5 is magazine
        session.add(self.items[5])
        resp = self.client.get('/v1/items/{}/recommendations?brand_id=300'.format(self.items[5].id), headers=self.headers)
        resp_data = json.loads(resp.data)
        self.assertEqual(resp.status_code, OK, resp)
        # filter by self.item[5].id related categories and by brand id 300, self.item[3] should be selected
        self.assert_response_body(resp_data, [self.items[3], ])

    def test_by_brand_multiple_magazine(self):
        session = db.session()
        # self.items 3, 4, 5 is magazine
        session.add(self.items[5])
        resp = self.client.get('/v1/items/{}/recommendations?brand_id=300,301'.format(self.items[5].id), headers=self.headers)
        resp_data = json.loads(resp.data)
        self.assertEqual(resp.status_code, OK, resp)
        # filter by self.item[5].id related categories and by brand id 300 & 301, self.item[3 & 4] should be selected
        self.assert_response_body(resp_data, [self.items[3], self.items[4], ])

    def test_by_vendor_magazine(self):
        session = db.session()
        # self.items 3, 4, 5 is magazine
        session.add(self.items[5])
        resp = self.client.get('/v1/items/{}/recommendations?vendor_id=300'.format(self.items[5].id),
                               headers=self.headers)
        resp_data = json.loads(resp.data)
        self.assertEqual(resp.status_code, OK, resp)
        # filter by self.item[5].id related categories and by vendor id 300, self.item[3] should be selected
        self.assert_response_body(resp_data, [self.items[3], ])

    def test_newspaper(self):
        session = db.session()
        # self.items 6, 7, 8 is valid news paper
        session.add(self.items[8])
        resp = self.client.get('/v1/items/{}/recommendations'.format(self.items[8].id), headers=self.headers)
        resp_data = json.loads(resp.data)
        self.assertEqual(resp.status_code, OK, resp)
        # item selected should by in self.items 6 only
        # (latest release with same category with release date >- now-7days)
        self.assert_response_body(resp_data, [self.items[6], ])

    # def test_free(self):
    #     resp = self.client.get('/v1/items/1122/recommendations?is_free=true', headers=self.headers)
    #     resp_data = json.loads(resp.data)
    #
    #     self.assertEqual(len(resp_data.get('items')), len(self.FREE_ITEM_INDEX_LIST),
    #                      'Free item total data not {}, response {}'.format(len(self.FREE_ITEM_INDEX_LIST), resp.data))
    #     session = db.session()
    #     with app.test_request_context('/'):
    #         index = 0
    #         for item_pos in self.FREE_ITEM_INDEX_LIST:
    #             session.add(self.items[item_pos])
    #             actual = resp_data.get('items')[index]
    #             item_id = actual.get('id')
    #             self.assertEqual(self.items[item_pos].id, item_id)
    #             expected = self.get_dict_item(self.items[item_pos])
    #             self.assertDictContainsSubset(expected, actual)
    #             index += 1
    #
    # def test_bundle(self):
    #     resp = self.client.get('/v1/items/1122/recommendations?is_bundle=true', headers=self.headers)
    #     resp_data = json.loads(resp.data)
    #
    #     self.assertEqual(len(resp_data.get('items')), len(self.BUNDLE_ITEM_INDEX_LIST),
    #                      'Bundle item total data not {}, response {}'.format(len(self.BUNDLE_ITEM_INDEX_LIST), resp.data))
    #     session = db.session()
    #     with app.test_request_context('/'):
    #         index = 0
    #         for item_pos in self.BUNDLE_ITEM_INDEX_LIST:
    #             session.add(self.items[item_pos])
    #             actual = resp_data.get('items')[index]
    #             item_id = actual.get('id')
    #             self.assertEqual(self.items[item_pos].id, item_id)
    #             expected = self.get_dict_item(self.items[item_pos])
    #             self.assertDictContainsSubset(expected, actual)
    #             index += 1
    #
    # def test_buffet(self):
    #     resp = self.client.get('/v1/items/1122/recommendations?is_buffet=true', headers=self.headers)
    #     resp_data = json.loads(resp.data)
    #
    #     self.assertEqual(len(resp_data.get('items')), len(self.BUFFET_ITEM_INDEX_LIST),
    #                      'Buffet item total data not {}, response {}'.format(len(self.BUFFET_ITEM_INDEX_LIST), resp.data))
    #     session = db.session()
    #     with app.test_request_context('/'):
    #         index = 0
    #         for item_pos in self.BUFFET_ITEM_INDEX_LIST:
    #             session.add(self.items[item_pos])
    #             actual = resp_data.get('items')[index]
    #             item_id = actual.get('id')
    #             self.assertEqual(self.items[item_pos].id, item_id)
    #             expected = self.get_dict_item(self.items[item_pos])
    #             self.assertDictContainsSubset(expected, actual)
    #             index += 1


@nottest
class FullCacheRebuildTests(E2EBase):

    def setUp(self):
        super(FullCacheRebuildTests, self).setUp()
        self.item = ItemFactory()

    def test_cache_hit(self):
        """ Should not call 'customget' and should match the expected response format.
        """
        pass
        # self.fail("You don't write code, you dolt.")

    def test_cache_miss(self):
        """ Should call through to 'customget' and should match the expected response format.
        """
        pass
        # self.fail("You don't write code, you dolt.")





