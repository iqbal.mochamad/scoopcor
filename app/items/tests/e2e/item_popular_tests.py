from __future__ import unicode_literals
import unittest
import json

from datetime import datetime
from httplib import OK

from flask import g
from six import text_type

from app import db, app
from app.analytics.models import UserDownload
from app.auth.models import UserInfo, Client
from app.items.choices import ITEM_TYPES, FILE_TYPES
from app.items.models import PopularItem
from app.layouts.layout_type import LayoutTypes
from app.master.choices import IOS
from app.master.models import Platform
from app.offers.models import offer_item, OfferType
from app.users.users import User
from app.utils.datetimes import datetime_to_isoformat
from app.utils.shims.item_types import enum_to_id
from tests.fixtures.helpers import generate_static_sql_fixtures
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.items import ItemFactory
from scripts.popular_generator import main
from tests.fixtures.sqlalchemy.layouts import LayoutFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferPlatformFactory
from faker import Factory

fake = Factory.create()


class GetListItemPopularTest(unittest.TestCase):
    maxDiff = None
    client = app.test_client(use_cookies=False)
    headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8'
    }

    @classmethod
    def setUpClass(cls):
        cls.drop_popular_view()
        db.drop_all()
        db.create_all()

        ctx = app.test_request_context('/')
        ctx.push()

        session = db.session()
        generate_static_sql_fixtures(session)
        session.commit()
        user = UserFactory(id=12345)
        layout = LayoutFactory(id=110, layout_type=LayoutTypes.popular_all)
        client = session.query(Client).get(1)
        client.allow_age_restricted_content = True
        session.add(client)
        # table item popular automatically created when db.create_all
        PopularItem.__table__.drop(db.engine, checkfirst=True)
        # db.engine.execute('DROP TABLE utility.item_popular;')

        cls.build_popular_data(session)

        main()
        session.commit()

    @classmethod
    def drop_popular_view(cls):
        try:
            db.engine.execute('DROP MATERIALIZED VIEW utility.item_popular;')
        except:
            pass
        try:
            db.engine.execute('DROP MATERIALIZED VIEW utility.item_later_editions;')
        except:
            pass
        try:
            db.engine.execute('DROP MATERIALIZED VIEW utility.item_downloads;')
        except:
            pass
        try:
            db.engine.execute('DROP VIEW utility.non_free_item RESTRICT;')
        except:
            pass

    @classmethod
    def build_popular_data(cls, session):
        # build items
        items = []
        for _ in range(3):
            item = ItemFactory(item_status='ready for consume')
            items.append(item)

        offer_type = session.query(OfferType).get(1)
        offer = OfferFactory(is_free=False, offer_type=offer_type)
        session.commit()
        platform_ios = session.query(Platform).get(IOS)
        offer_platform = OfferPlatformFactory(
            offer=offer,
            platform=platform_ios,
            price_usd=offer.price_usd,
            price_idr=offer.price_idr,
            price_point=offer.price_point,
        )
        session.commit()

        # build offer item
        for index in range(3):
            oi = offer_item.insert()
            oi.offer_id = offer.id
            oi.item_id = items[index].id
            db.engine.execute(offer_item.insert(), offer_id=offer.id, item_id=items[index].id)

        # build user activities
        cls.most_popular_item = items[0]
        cls.more_popular_item = items[1]
        cls.least_popular_item = items[2]

        cls.generate_user_activities(session, cls.most_popular_item, 50)

        cls.generate_user_activities(session, cls.more_popular_item, 20)

        cls.generate_user_activities(session, cls.least_popular_item, 5)

        session.commit()

    @classmethod
    def generate_user_activities(cls, session, item, total_record):
        session.add(item)
        for _ in range(total_record):
            # UserActivityFactory(user_id=1, item=item, download_status=SUCCESS,
            #                     activity_type=DOWNLOAD, datetime=datetime.now())
            data = {
                "device_id": "123",
                "user_id": 1,
                "item_id": item.id,
                "session_name": "some session",
                "ip_address": "127.0.0.1",
                "download_status": "success",
                "activity_type": "download",
                "location": "64.54 32.64",
                "client_version": "1.2.3",
                "os_version": "abc",
                "device_model": "abc",
                "datetime": datetime_to_isoformat(datetime.now())
            }
            user_download = UserDownload(user_activity=data)
            session.add(user_download)

    @classmethod
    def tearDownClass(cls):

        # for testing purpose we delete view non_free_item

        db.session().close_all()
        db.session().commit()
        # remove the current SESSION from the SESSIONFACTORY
        db.session.remove()
        cls.drop_popular_view()
        db.drop_all()

    def get_dict_item(self, item):
        reversed_enum_file_type = {v: k for k, v in FILE_TYPES.items()}
        display_offer, display_price = item.get_offers(4)
        return {
            'id': item.id,
            'brand': {
                'id': item.brand.id,
                'name': item.brand.name,
                'slug': item.brand.slug,
            },
            'categories': [],
            'content_type': reversed_enum_file_type[item.content_type],
            'display_offers': display_offer,
            'display_price': display_price,
            'is_buffet': False,
            u'parental_control': {u'id': item.parentalcontrol.id, u'name': item.parentalcontrol.name},
            # 'is_latest': item.get_is_latest(),
            'issue_number': item.issue_number,
            'name': item.name,
            'authors': item.get_authors(),
            'item_thumb_highres': item.thumb_image_highres,
            'item_type_id': enum_to_id(item.item_type),
            'media_base_url': 'https://images.apps-foundry.com/magazine_static/',
            'thumb_image_highres': item.thumb_image_highres,
            'thumb_image_normal': item.thumb_image_normal,
            'vendor': {
                'id': item.brand.vendor.id,
                'name': item.brand.vendor.name
            },
            "slug": item.slug,
        }

    def test_most_popular(self):
        self.resp = self.client.get('/v1/items/popular?display_offer_plid=4', headers=self.headers)
        self.assertEqual(self.resp.status_code, OK,
                         'Status Code != 200 (OK). Response: {}'.format(self.resp.data))
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)

        del self.result['items'][0]['is_latest']
        del self.result['items'][1]['is_latest']
        del self.result['items'][2]['is_latest']
        session = db.session()
        session.add(self.most_popular_item)
        session.add(self.more_popular_item)
        session.add(self.least_popular_item)

        self.assertDictEqual(
            self.result,
            {
                'items': [
                    self.get_dict_item(self.most_popular_item),
                    self.get_dict_item(self.more_popular_item),
                    self.get_dict_item(self.least_popular_item),
                ],
                'metadata': {
                    'resultset': {
                        'count': 3,
                        'offset': 0,
                        'limit': 20
                    }
                }
            })

    def test_is_free(self):
        self.resp = self.client.get('/v1/items/popular?is_free=true&display_offer_plid=1', headers=self.headers)
        self.assertEqual(self.resp.status_code, OK,
                         'Status Code != 200 (OK). Response: {}'.format(self.resp.data))

        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 0, self.resp.data)


def init_g_current_user():
    g.user = UserInfo(username='dfdsafsdf', user_id=12345, token='abcd', perm=['can_read_write_global_all',])
    session = db.session()
    g.current_user = session.query(User).get(12345)
    g.current_client = session.query(Client).get(1)
    session.close()
