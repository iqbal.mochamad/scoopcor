import json
from httplib import OK, NO_CONTENT

from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import AuthorFactory
from tests.fixtures.sqlalchemy.master import BrandFactory, VendorFactory


class ItemSearchSuggestionTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(ItemSearchSuggestionTests, cls).setUpClass()
        vendor = VendorFactory(name='Gramedia')
        vendor2 = VendorFactory(name='Gajah Tunggal')
        VendorFactory(name='Gajah Mabuk')
        for i in (1, 2, 3):
            BrandFactory(id=i, name='Hujan k{}'.format(i), vendor=vendor)
        for i in (1, 2, 3, 4, 5):
            BrandFactory(id=i+10, name='Internet k{}'.format(i+10), vendor=vendor2)

        for i in range(1, 3):
            AuthorFactory(id=i, name='John k{}'.format(i))
        for i in range(3, 7):
            AuthorFactory(id=i+10, name='Agatha k{}'.format(i+10))
        cls.session.commit()

        cls.headers = cls.build_headers().copy()
        cls.headers.pop('Accept', None)

    def test_item_search_suggestion_api(self):
        resp = self.api_client.get('/v1/items/searches/suggestions/huj', headers=self.headers)
        self.assert_status_code(resp, OK)
        result = self._get_response_json(resp)
        actual = result.get('suggestions', [])
        self.assertIsNotNone(actual)
        self.assertEqual(3, len(actual))

        resp = self.api_client.get('/v1/items/searches/suggestions/wrong', headers=self.headers)
        self.assert_status_code(resp, NO_CONTENT)

    def test_search_by_author(self):
        # search in brand not found! should return Author data
        resp = self.api_client.get('/v1/items/searches/suggestions/john', headers=self.headers)
        self.assert_status_code(resp, OK)
        result = json.loads(resp.data)
        actual = result.get('suggestions', [])
        self.assertIsNotNone(actual)
        self.assertEqual(2, len(actual))

    def test_search_by_vendor(self):
        # search in brand and author not found! should return Vendor data
        resp = self.api_client.get('/v1/items/searches/suggestions/gramedia', headers=self.headers)
        self.assert_status_code(resp, OK)
        result = json.loads(resp.data)
        actual = result.get('suggestions', [])
        self.assertIsNotNone(actual)
        self.assertEqual(1, len(actual))

    def test_search_by_brand_contains(self):
        # search in brand, author, vendor not found! should return brand data that contains keyword
        resp = self.api_client.get('/v1/items/searches/suggestions/ter', headers=self.headers)
        self.assert_status_code(resp, OK)
        result = json.loads(resp.data)
        actual = result.get('suggestions', [])
        self.assertIsNotNone(actual)
        self.assertEqual(5, len(actual))

    def test_search_by_author_contains(self):
        # search in brand, author, vendor not found! should return brand data that contains keyword
        resp = self.api_client.get('/v1/items/searches/suggestions/atha', headers=self.headers)
        self.assert_status_code(resp, OK)
        result = json.loads(resp.data)
        actual = result.get('suggestions', [])
        self.assertIsNotNone(actual)
        self.assertEqual(4, len(actual))

    def test_search_by_vendor_contains(self):
        # search in brand, author, vendor not found! should return brand data that contains keyword
        resp = self.api_client.get('/v1/items/searches/suggestions/jah', headers=self.headers)
        self.assert_status_code(resp, OK)
        result = json.loads(resp.data)
        actual = result.get('suggestions', [])
        self.assertIsNotNone(actual)
        self.assertEqual(2, len(actual))
