from __future__ import unicode_literals
import json
import random

from faker import Factory
from nose.tools import istest

from tests.fixtures.sqlalchemy import AuthorFactory
from tests.helpers import gen_api_safe_iso8601_date
from tests import testcases

from app import db

from app.utils.datetimes import datetime_to_isoformat


@istest
class CreateAuthorTest(testcases.CreateTestCase):

    request_url = '/v1/authors'
    fixture_factory = AuthorFactory

    @classmethod
    def get_api_request_body(cls):
        return {
            'name': _fake.name(),
            'sort_priority': _fake.pyint(),
            'is_active': _fake.pybool(),
            'slug': _fake.slug(),
            'first_name': _fake.first_name(),
            'last_name': _fake.last_name(),
            'title': random.choice(["MR", "MRS", "MISS", None]),
            'academic_title': random.choice(["DR", "PHD", None]),
            'birthdate': gen_api_safe_iso8601_date(_fake.date_time()),
            'meta': _fake.text(),
            'profile_pic_url': _fake.url()
        }

    def test_response_body(self):
        self.assertDictContainsSubset(
            expected=self.request_body,
            actual=json.loads(self.response.data))


@istest
class UpdateAuthorTest(testcases.UpdateTestCase):

    request_url = '/v1/authors/{0.id}'
    fixture_factory = AuthorFactory

    @classmethod
    def get_api_request_body(cls):
        return {
            'name': _fake.name(),
            'sort_priority': _fake.pyint(),
            'is_active': _fake.pybool(),
            'slug': _fake.slug(),
            'first_name': _fake.first_name(),
            'last_name': _fake.last_name(),
            'title': random.choice(["MR", "MRS", "MISS", None]),
            'academic_title': random.choice(["DR", "PHD", None]),
            'birthdate': gen_api_safe_iso8601_date(_fake.date_time()),
            'meta': _fake.text(),
            'profile_pic_url': _fake.url()
        }

    def test_response_body(self):
        self.assertDictContainsSubset(
            expected=self.request_body,
            actual=json.loads(self.response.data))


@istest
class DeleteAuthorTest(testcases.SoftDeleteTestCase):

    request_url = '/v1/authors/{0.id}'
    fixture_factory = AuthorFactory


@istest
class GetAuthorTest(testcases.DetailTestCase):

    maxDiff = None

    request_url = '/v1/authors/{0.id}'
    fixture_factory = AuthorFactory


    def test_response_body(self):
        self.assertDictEqual(
            json.loads(self.response.data),
            {
                'id': self.fixture.id,
                'name': self.fixture.name,
                'sort_priority': self.fixture.sort_priority,
                'is_active': self.fixture.is_active,
                'slug': self.fixture.slug,
                'first_name': self.fixture.first_name,
                'last_name': self.fixture.last_name,
                'title': self.fixture.title,
                'academic_title': self.fixture.academic_title,
                'birthdate': self.fixture.birthdate.date().isoformat(),
                'meta': self.fixture.meta,
                'modified': datetime_to_isoformat(self.fixture.modified),
                'profile_pic_url': self.fixture.profile_pic_url
            }
        )


@istest
class ListAuthorsTest(testcases.ListTestCase):

    request_url = '/v1/authors'
    list_key = 'authors'
    fixture_factory = AuthorFactory

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        self.assertDictEqual(
            fixture_json,
            {
                'id': fixture.id,
                'name': fixture.name,
                'sort_priority': fixture.sort_priority,
                'is_active': fixture.is_active,
                'slug': fixture.slug,
                'first_name': fixture.first_name,
                'last_name': fixture.last_name,
                'title': fixture.title,
                'academic_title': fixture.academic_title,
                'birthdate': fixture.birthdate.date().isoformat(),
                'meta': fixture.meta,
                'modified': datetime_to_isoformat(fixture.modified),
                'profile_pic_url': fixture.profile_pic_url
            }
        )


_fake = Factory.create()


def setup_module():
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
