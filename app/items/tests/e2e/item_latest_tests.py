from __future__ import unicode_literals
import unittest
import json
from datetime import timedelta
from httplib import OK

from marshmallow.utils import isoformat
from sqlalchemy import desc

from app import db, app
from app.auth.helpers import jwt_encode_from_user
from app.items.choices import BOOK, MAGAZINE, NEWSPAPER, ITEM_TYPES, STATUS_TYPES, STATUS_READY_FOR_CONSUME
from app.items.models import Item
from app.master.models import Brand
from app.offers.choices.offer_type import BUNDLE, BUFFET
from app.offers.models import OfferType
from app.parental_controls.choices import ParentalControlType
from app.users.users import User
from app.utils.datetimes import datetime_to_isoformat, get_utc_nieve
from app.utils.testcases import TestBase
from tests.fixtures.helpers import generate_static_sql_fixtures
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.items import ItemFactory, AuthorFactory
from tests.fixtures.sqlalchemy.master import BrandFactory, VendorFactory, CategoryFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from faker import Factory

fake = Factory.create()
USER_ID = 12345


class GetListItemLatestTest(TestBase):

    maxDiff = None

    client = app.test_client(use_cookies=False)
    headers = {
        'Accept': 'application/vnd.scoop.v3+json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'X-Scoop-Client': 'scoop web/v1.0.0'
    }
    platform_id = 4

    @classmethod
    def setUpClass(cls):
        super(GetListItemLatestTest, cls).setUpClass()
        # clean up previous cached data
        app.kvs.flushdb()
        user = UserFactory(id=USER_ID, allow_age_restricted_content=True)
        cls.headers['Authorization'] = 'JWT {}'.format(jwt_encode_from_user(user))

        cls.build_data(cls.session)

    def tearDown(self):
        super(GetListItemLatestTest, self).tearDown()
        self.session.expunge_all()

    @classmethod
    def build_data(cls, session):
        # generate non active items, should not get selected in api
        for _ in range(3):
            item = ItemFactory(id=_+1, item_status='ready for consume',
                               item_type=ITEM_TYPES[BOOK], is_active=False,
                               edition_code='{}'.format(_+1))
            item.slug = item.id

        for _ in range(3):
            item = ItemFactory(id=_+4, item_status='ready for consume',
                               item_type=ITEM_TYPES[MAGAZINE], is_active=False,
                               edition_code='{}'.format(3+_+1))
            item.slug = item.id

        for _ in range(3):
            item = ItemFactory(id=_+10, item_status='ready for consume',
                               item_type=ITEM_TYPES[NEWSPAPER], is_active=False,
                               edition_code='{}'.format(6+_+1))
            item.slug = item.id

        # build latest items

        release_date = get_utc_nieve()
        # create BOOK that should get selected
        # see app.utils.shim.languages
        cls.language_id = 2
        language_code = 'ind'
        cls.country_id = 1
        country_code = 'id'
        cls.category_id = 3312
        category = CategoryFactory(id=cls.category_id)
        cls.author_id = 112
        author = AuthorFactory(id=cls.author_id)
        cls.items = []
        for _ in range(3):
            brand = BrandFactory(id=200+_)
            item = ItemFactory(id=1000-_, item_status='ready for consume',
                               item_type=ITEM_TYPES[BOOK],
                               brand=brand,
                               parentalcontrol_id=1, languages=[language_code, ],
                               release_date=release_date,
                               countries=[country_code, ],
                               edition_code='{}'.format(1000+_+1))
            item.slug = item.id
            item.authors.append(author)
            item.categories.append(category)
            cls.items.append(item)

        # create magazine that should get selected, and adult (for parental control test later
        language_code_eng = 'eng'
        country_code_eng = 'us'
        vendor_mag1 = VendorFactory(id=11223)
        brand_mag1 = BrandFactory(id=300, vendor=vendor_mag1)
        for _ in range(3):
            item = ItemFactory(id=600-_, item_status='ready for consume',
                               item_type=ITEM_TYPES[MAGAZINE],
                               brand=brand_mag1,
                               parentalcontrol_id=ParentalControlType.parental_level_2.value,
                               release_date=release_date,
                               languages=[language_code_eng, ],
                               countries=[country_code_eng, ],
                               edition_code='{}'.format(600+_+1))
            item.slug = item.id
            cls.items.append(item)

        # create primary latest edition of newspaper that should get selected
        for _ in range(3):
            brand_news = BrandFactory(id=400+_)
            item = ItemFactory(id=400-_, item_status='ready for consume',
                               item_type=ITEM_TYPES[NEWSPAPER],
                               brand=brand_news,
                               parentalcontrol_id=ParentalControlType.parental_level_1.value,
                               languages=[language_code_eng, ],
                               countries=[country_code_eng, ],
                               release_date=release_date,
                               sort_priority=10-_,
                               edition_code='{}'.format(400+_+1))
            item.slug = item.id
            cls.items.append(item)

        session.commit()

        # create other old edition of newspaper that should not get selected
        for _ in range(3):
            brand_news = session.query(Brand).get(400+_)
            item = ItemFactory(id=300-_, item_status='ready for consume',
                               item_type=ITEM_TYPES[NEWSPAPER],
                               brand=brand_news,
                               parentalcontrol_id=2, languages=[language_code_eng, ],
                               countries=[country_code_eng, ],
                               release_date=get_utc_nieve() - timedelta(days=1),
                               sort_priority=10 - _,
                               edition_code='{}'.format(300+_+1))
            item.slug = item.id
            cls.items.append(item)

        from app.offers.choices.offer_type import SINGLE
        offer_type = session.query(OfferType).get(SINGLE)

        cls.FREE_ITEM_INDEX_LIST = [3, 4]
        # build offer item with single offer
        for index in range(12):
            if index in cls.FREE_ITEM_INDEX_LIST:
                # create some data with free item
                offer = OfferFactory(id=index+1, offer_type=offer_type, is_free=True, price_idr=0, price_usd=0,
                                     price_point=0)
            else:
                offer = OfferFactory(id=index+1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                     price_point=10)
            offer.items.append(cls.items[index])
            # oi = offer_item.insert()
            # oi.offer_id = offer.id
            # oi.item_id = cls.items[index].id
            # db.engine.execute(offer_item.insert(), offer_id=offer.id, item_id=cls.items[index].id)

        # create bundle offer
        cls.BUNDLE_ITEM_INDEX_LIST = [1, 4]
        offer_type_bundle = session.query(OfferType).get(BUNDLE)
        for index in cls.BUNDLE_ITEM_INDEX_LIST:
            offer = OfferFactory(id=50000 + index, offer_type=offer_type_bundle, is_free=False, price_idr=10000, price_usd=10,
                                 price_point=10)
            offer.items.append(cls.items[index])

        # create BUFFET offer
        cls.BUFFET_ITEM_INDEX_LIST = [1, 2]
        offer_type_buffet = session.query(OfferType).get(BUFFET)
        for index in cls.BUFFET_ITEM_INDEX_LIST:
            offer = OfferFactory(id=60000 + index, offer_type=offer_type_buffet, is_free=False, price_idr=10000, price_usd=10,
                                 price_point=10)
            offer.brands.append(cls.items[index].brand)

        session.commit()
        session.expunge_all()

    def get_dict_item(self, item):
        restricted, allowed = item.get_restriction_country()
        display_offer, display_price = item.get_offers(platform_id=self.platform_id)
        return {
            'id': item.id,
            'name': item.name,
            'item_type_id': {ITEM_TYPES[k]: k for k in ITEM_TYPES}[item.item_type],
            'brand_id': item.brand_id,
            'vendor': {
                'slug': item.brand.vendor.slug,
                'name': item.brand.vendor.name,
                'id': item.brand.vendor.id,
            },
            "restricted_countries": restricted,
            "allowed_countries": allowed,
            'media_base_url': app.config['MEDIA_BASE_URL'],
            "thumb_image_highres": item.thumb_image_highres,
            "thumb_image_normal": item.thumb_image_normal,
            "issue_number": item.issue_number,
            "authors": item.get_authors(),
            "display_offers": display_offer,
            "display_price": display_price,
            "is_latest": item.get_is_latest(), # it should be optimized, not needed.. just read release data!
            'release_date': datetime_to_isoformat(item.release_date),
            "parental_control_id": item.parentalcontrol_id,
            "edition_code": item.edition_code,
        }

    def test_purge_cache(self):
        response = self.client.open(
            '/v1/items/latest',
            method='PURGE',
            headers=self.headers)
        self.assertEqual(response.status_code, OK, response.data)

    def test_latest_all(self):
        self.resp = self.client.get('/v1/items/latest', headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), len(self.items),
                         'Count results is not {}, response data: {}'.format(len(self.items), self.resp.data))
        for _ in range(9):
            item_id = self.result.get('items')[_].get('id')
            # to check whether data in the correct order
            self.reattach_model(self.items[_])
            self.assertEqual(self.items[_].id, item_id)
            # check response body
            expected = self.get_dict_item(self.items[_])
            self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_book(self):
        # self.resp = self.client.get('/v1/items/popular', headers=self.headers)
        self.resp = self.client.get('/v1/items/latest?item_type_id={}'.format(BOOK), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        session = db.session
        items = []
        with app.test_request_context('/'):
            for _ in range(3):
                self.reattach_model(self.items[_])
                expected = self.get_dict_item(self.items[_])
                self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_by_author(self):
        self.resp = self.client.get('/v1/items/latest?author_id={}'.format(self.author_id), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        session = db.session
        items = []

        for _ in range(3):
            self.reattach_model(self.items[_])
            expected = self.get_dict_item(self.items[_])
            self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_by_category(self):
        self.resp = self.client.get('/v1/items/latest?category_id={}'.format(self.category_id), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        session = db.session
        items = []

        for _ in range(3):
            self.reattach_model(self.items[_])
            expected = self.get_dict_item(self.items[_])
            self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_by_parental_control_in_query_param(self):
        self.resp = self.client.get('/v1/items/latest?parental_control_id={}'.format(1), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 6,
                         'items count {} != 6. data: {}'.format(len(self.result.get('items')), self.resp.data))
        session = db.session
        for _ in range(3):
            self.reattach_model(self.items[_])
            expected = self.get_dict_item(self.items[_])
            self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_by_user_parental_control_setting(self):
        session = db.session
        # set user setting to allow_age_restricted_content to False (don't show adult items)
        user = session.query(User).get(USER_ID)
        user.allow_age_restricted_content = False
        self.session.add(user)
        session.commit()
        try:
            self.resp = self.client.get('/v1/items/latest', headers=self.headers)
        finally:
            # reset user setting
            user.allow_age_restricted_content = True
            self.session.add(user)
            session.commit()

        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 6,
                         'items count {} != 6. data: {}'.format(len(self.result.get('items')), self.resp.data))
        items = []

        for _ in range(3):
            self.reattach_model(self.items[_])
            expected = self.get_dict_item(self.items[_])
            self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_by_country(self):
        self.resp = self.client.get('/v1/items/latest?country_id={}'.format(self.country_id), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        session = db.session
        items = []
        for _ in range(3):
            self.reattach_model(self.items[_])
            expected = self.get_dict_item(self.items[_])
            self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_by_language(self):
        self.resp = self.client.get('/v1/items/latest?language_id={}'.format(self.language_id), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        session = db.session
        items = []

        for _ in range(3):
            self.reattach_model(self.items[_])
            expected = self.get_dict_item(self.items[_])
            self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_magazine(self):
        self.resp = self.client.get('/v1/items/latest?item_type_id={}'.format(MAGAZINE), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        session = db.session

        for _ in range(3):
            item_pos = _+3
            self.reattach_model(self.items[item_pos])
            expected = self.get_dict_item(self.items[item_pos])
            self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_by_brand_magazine(self):
        self.resp = self.client.get('/v1/items/latest?brand_id={}'.format(300), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        session = db.session
        for _ in range(3):
            item_pos = _+3
            self.reattach_model(self.items[item_pos])
            expected = self.get_dict_item(self.items[item_pos])
            self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_by_vendor_magazine(self):
        self.resp = self.client.get('/v1/items/latest?vendor_id={}'.format(11223), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        session = db.session
        for _ in range(3):
            item_pos = _+3
            self.reattach_model(self.items[item_pos])
            expected = self.get_dict_item(self.items[item_pos])
            self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_newspaper(self):
        self.resp = self.client.get('/v1/items/latest?item_type_id={}'.format(NEWSPAPER), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), 3, self.resp.data)
        session = db.session
        for _ in range(3):
            item_pos = _ + 6
            self.reattach_model(self.items[item_pos])
            item_id = self.result.get('items')[_].get('id')
            self.assertEqual(self.items[item_pos].id, item_id)
            expected = self.get_dict_item(self.items[item_pos])
            self.assertDictContainsSubset(expected, self.result.get('items')[_])

    def test_latest_newspaper_by_brand(self):
        brand_id = 401
        self.resp = self.client.get('/v1/items/latest?item_type_id={}&brand_id={}'.format(
            NEWSPAPER, brand_id), headers=self.headers)
        self.result = json.loads(self.resp.data)

        session = db.session
        items = session.query(Item).filter_by(brand_id=brand_id).order_by(desc(Item.release_date)).all()
        expected_items = []
        for item in items:
            expected_items.append(self.get_dict_item(item))

        self.assertEqual(len(self.result.get('items')), len(expected_items),
                         'Data Count is not {}. Response data {}'.format(len(expected_items), self.resp.data))

        for _ in range(len(expected_items)):
            self.assertDictContainsSubset(expected_items[_], self.result.get('items')[_])

    def test_free(self):
        self.resp = self.client.get('/v1/items/latest?is_free=true', headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), len(self.FREE_ITEM_INDEX_LIST),
                         'Free item total data not {}, response {}'.format(len(self.FREE_ITEM_INDEX_LIST), self.resp.data))

        index = 0
        for item_pos in self.FREE_ITEM_INDEX_LIST:
            self.reattach_model(self.items[item_pos])
            actual = self.result.get('items')[index]
            item_id = actual.get('id')
            self.assertEqual(self.items[item_pos].id, item_id)
            expected = self.get_dict_item(self.items[item_pos])
            self.assertDictContainsSubset(expected, actual)
            index += 1

    def test_bundle(self):
        self.resp = self.client.get('/v1/items/latest?is_bundle=true', headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), len(self.BUNDLE_ITEM_INDEX_LIST),
                         'Bundle item total data not {}, response {}'.format(len(self.BUNDLE_ITEM_INDEX_LIST), self.resp.data))
        session = db.session

        index = 0
        for item_pos in self.BUNDLE_ITEM_INDEX_LIST:
            self.reattach_model(self.items[item_pos])
            actual = self.result.get('items')[index]
            item_id = actual.get('id')
            self.assertEqual(self.items[item_pos].id, item_id)
            expected = self.get_dict_item(self.items[item_pos])
            self.assertDictContainsSubset(expected, actual)
            index += 1

    def test_buffet(self):
        self.resp = self.client.get('/v1/items/latest?is_buffet=true', headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEqual(len(self.result.get('items')), len(self.BUFFET_ITEM_INDEX_LIST),
                         'Buffet item total data not {}, response {}'.format(len(self.BUFFET_ITEM_INDEX_LIST), self.resp.data))
        session = db.session

        index = 0
        for item_pos in self.BUFFET_ITEM_INDEX_LIST:
            self.reattach_model(self.items[item_pos])
            actual = self.result.get('items')[index]
            item_id = actual.get('id')
            self.assertEqual(self.items[item_pos].id, item_id)
            expected = self.get_dict_item(self.items[item_pos])
            self.assertDictContainsSubset(expected, actual)
            index += 1

    def test_white_labels_filters(self):
        # this is query from white lables
        self.resp = self.client.get('/v1/items/latest?country_id=1&display_offer_plid=1&expand=display_offers'
                                    '%2Cauthors%2Cbrand%2Cvendor%2Cparental_control&fields=media_base_url%2Cid%2C'
                                    'authors%2Cissue_number%2Cdisplay_offers%2Cthumb_image_normal'
                                    '%2Cthumb_image_highres%2Citem_type_id%2Cname%2Cis_latest%2Cvendor%2C'
                                    'brand%2Cedition_code%2Crelease_date%2Ccontent_type%2Crestricted_countries'
                                    '%2Callowed_countries%2Cparental_control'
                                    '&is_active=1&item_status=9&item_type_id=1&language_id=2'
                                    '&limit=20&offset=0&vendor_id=65',
                                    headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEquals(self.resp.status_code, OK)

    def test_multiple_brand_id(self):
        brands = '300,201'
        self.resp = self.client.get('/v1/items/latest?is_active=1&item_status=9&brand_id={}'.format(brands),
                                    headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEquals(self.resp.status_code, OK)
        total_latest_item_current_brand = Item.query.filter(Item.brand_id.in_(brands.split(','))).count()
        self.assertEquals(len(json.loads(self.resp.data).get('items')), total_latest_item_current_brand)

    def test_latest_by_release_date(self):
        self.resp = self.client.get('/v1/items/latest?release_date={}'.format(
            isoformat(get_utc_nieve())), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEquals(self.resp.status_code, OK)
        expected_items = Item.query.filter(Item.release_date == get_utc_nieve()).count()
        self.assertEquals(len(json.loads(self.resp.data).get('items')), expected_items)
        # "release_date__lt=2017-03-01T00%3A00%3A00%2B00%3A00&vendor_id=65"
        self.resp = self.client.get('/v1/items/latest?release_date__lt={}&limit=200'.format(
            isoformat(get_utc_nieve())[:19]), headers=self.headers)
        self.result = json.loads(self.resp.data)

        self.assertEquals(self.resp.status_code, OK)
        expected_items = Item.query.filter(
            Item.release_date < isoformat(get_utc_nieve())[:19],
            Item.is_active == True,
            Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
        ).count()
        self.assertEquals(len(json.loads(self.resp.data).get('items')), expected_items)
