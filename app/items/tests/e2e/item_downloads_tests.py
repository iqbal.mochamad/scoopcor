from __future__ import unicode_literals, absolute_import

import hashlib
import json
from datetime import timedelta
from faker import Factory
from httplib import OK, FORBIDDEN
from flask import g
from mock import patch
from nose.tools import istest, nottest

from app import app, db, set_request_id
from app.auth.models import UserInfo
from app.items.models import Item
from app.master.models import Currencies
from app.offers.choices.offer_type import BUFFET
from app.orders.choices import COMPLETE
from app.payments.choices import CHARGED
from app.utils.datetimes import get_utc_nieve
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import UserItemFactory
from app.users.tests.fixtures import UserFactory
from app.users.tests.fixtures_user_buffet import UserBuffetFactory
from tests.fixtures.sqlalchemy.items import ItemFactory, ItemFileFactory
from tests.fixtures.sqlalchemy.master import BrandFactory, VendorFactory
from tests.fixtures.sqlalchemy.offers import OfferBuffetFactory, OfferTypeFactory, OfferFactory, standard_offer_types
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.payments.gateways import PaymentGatewayFactory
from tests import testcases


""" These are a set of test to test old item download api.. see '/v1/downloads' class ItemDownloadApi

for new Item Download API with url: '/v1/items/{0.id}/download' class ItemContentFileDownloadApi
    see test file: item_download_tests.py (download without s)
"""


# todo: mock some method in helpers.DownloadItems(args, 'POST').construct()
def generate_download_signature(user_id, item_id, timestamp):
    download_salt = app.config['DOWNLOAD_SALT']
    sign_form = str('%s%s%s%s' % (timestamp, int(item_id) * 2, int(user_id) +
                                  int(timestamp), download_salt))

    max_loop = int(str(timestamp)[-2:]) + 1
    loop_range = range(1, max_loop)

    # first key
    signature_encrypt = hashlib.sha256(download_salt + sign_form).hexdigest()

    for ixo in loop_range:
        signature_encrypt = hashlib.sha256(download_salt + signature_encrypt).hexdigest()
    return signature_encrypt


@nottest
class ItemDownloadApiTestsBase(testcases.DetailTestCase):
    """ Base class to test user download api -- for valid response """

    maxDiff = None
    fixture_factory = ItemFactory
    request_url = '/v1/downloads'
    # request_url = '/v1/items/{0.id}/download'
    request_headers = {'Accept': 'application/zip',
                       "Content-Type": "application/json"}
    mocked_download_url = 'http://dummy-download-link'

    @classmethod
    def mocked_g_user(cls):
        # override this method and define the user_id + item_id + before request funcs (g.user)
        pass

    @classmethod
    def setUpClass(cls):
        cls.mocked_g_user()
        super(ItemDownloadApiTestsBase, cls).setUpClass()

    @classmethod
    @patch('app.items.encrypt.EncryptData.generate_link')
    def get_api_response(cls, mock_generate_link):
        with app.test_request_context('/'):
            mock_generate_link.return_value = cls.mocked_download_url
            return cls.client.post(cls.request_url.format(cls.fixture),
                                   data=json.dumps(cls.get_request_body()),
                                   headers=cls.request_headers)
            # return cls.client.get(cls.request_url.format(cls.fixture),
            #                       headers=cls.request_headers)

    @classmethod
    def get_request_body(cls):
        timestamp = 192461648711
        signature = generate_download_signature(cls.user_id, cls.item_id, timestamp)
        # ex signature: "e30838dc8400660940d2418e3c1abf55ec97876e69e9d9f73bdd0974dccdc2f6"
        return {
            "user_id": cls.user_id,
            "item_id": cls.item_id,
            "timestamp_unix": timestamp,
            "signature": signature,
            "file_order__in": [1, 2, 3, 4, 5],
            "file_order__gte": 1,
            "file_order__lte": 20,
            "country_code": "ID",
            "ip_address": "139.0.18.154"
        }

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        s.expire_on_commit = False
        cls.item = s.query(Item).first()
        return cls.item

    @classmethod
    def remove_fixtures_from_session(cls):
        pass

    def test_response_body(self):
        expected = {
            "url": self.mocked_download_url,
            "file_version": None,
            "is_active": True,
            "md5_checksum": None,
            "file_type": 1,
            "file_status": 1,
            "file_order": 1,
            "key": None,
            "item_id": self.item_id
        }
        self.assertIsNotNone(self.response.data)
        actual = json.loads(self.response.data).get('downloads', None)
        actual = actual[0] if actual else None
        self.assertDictEqual(expected, actual)

    def test_response_code(self):
        self.assertEqual(OK, self.response.status_code)

    def test_response_content_type(self):
        self.assertEqual(self.response.content_type, 'application/json')

    @classmethod
    def tearDownClass(cls):
        pass


@istest
class UserSuperAdminTests(ItemDownloadApiTestsBase):
    """ Test for user with super admin roles, should be able to download anything,
    even not exists in user owned item """

    @classmethod
    def mocked_g_user(cls):
        cls.user_id = 1234
        cls.item_id = 10
        app.before_request_funcs = {
            None: [
                set_request_id,
                fake_authorized_user,
            ]
        }


@istest
class UserThatOwnedItemTestsTests(ItemDownloadApiTestsBase):
    """ Test for user that owned the item (data exists in user-item tables) """

    @classmethod
    def mocked_g_user(cls):
        cls.user_id = 1122
        cls.item_id = 10
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_user_with_item,
            ]
        }


@istest
class UserWithValidBuffetTests(ItemDownloadApiTestsBase):
    """ Test for user that didn't have the item directly but have valid Buffet for that item
     User Buffet Exists and Valid
     and user owned item also already exists for that user buffet
    """

    @classmethod
    def mocked_g_user(cls):
        cls.user_id = 1133
        cls.item_id = 10
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_user_with_valid_buffet_already_registered_in_user_item,
            ]
        }


@istest
class UserWithValidBuffetCreateUserItemTests(ItemDownloadApiTestsBase):
    """ Test for user that didn't have the item directly but have valid Buffet for that item
     User Buffet Exists and Valid
     BUT user owned item not yet exists for that user buffet
    """

    @classmethod
    def mocked_g_user(cls):
        cls.user_id = 1144
        cls.item_id = 10
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_user_with_valid_buffet_not_yet_registered_in_user_item,
            ]
        }


@istest
class DownloadByBuffetVendorsTests(ItemDownloadApiTestsBase):
    """ Test for user that didn't have the item directly but have valid Buffet for that item via OFFER-VENDORS list
    """

    @classmethod
    def mocked_g_user(cls):
        cls.user_id = 1155
        cls.item_id = 11
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_user_for_buffet_vendors,
            ]
        }


@nottest
class InvalidUserDownloadTestsBase(ItemDownloadApiTestsBase):
    """ Base class to tests forbidden/invalid user item download """

    @classmethod
    def remove_fixtures_from_session(cls):
        pass

    def test_response_body(self):
        return

    def test_response_code(self):
        # we only need to test status code
        self.assertEqual(FORBIDDEN, self.response.status_code)

    def test_response_content_type(self):
        pass

    @classmethod
    def tearDownClass(cls):
        pass


@istest
class UnauthorizedUserItemDownloadTests(InvalidUserDownloadTestsBase):
    """ user doesn't owned the item, forbidden """

    @classmethod
    def mocked_g_user(cls):
        cls.user_id = 1100
        cls.item_id = 10
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_regular_user,
            ]
        }


@istest
class UserWithExpiredBuffetTests(InvalidUserDownloadTestsBase):
    """ user buffet already expired, forbidden """

    @classmethod
    def mocked_g_user(cls):
        cls.user_id = 1166
        cls.item_id = 10
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_user_with_expired_user_buffet,
            ]
        }

_fake = Factory.create()
client = app.test_client(use_cookies=False)
original_before_request_funcs = None


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()
    global original_before_request_funcs
    original_before_request_funcs = app.before_request_funcs
    init_data()


def init_data():
    s = db.session()
    s.expire_on_commit = False
    generate_static_sql_fixtures(s)
    vendor = VendorFactory()
    brand = BrandFactory(id=212)
    brand2 = BrandFactory(id=2123, vendor=vendor)
    offer_type = standard_offer_types()['buffet']
    offer = OfferFactory(offer_type=offer_type, is_free=True, price_idr=0, price_usd=10, price_point=10)
    offer.brands.append(brand)
    offer.vendors.append(vendor)
    item = ItemFactory(id=10, content_type='pdf', brand=brand, edition_code='test_item_2')
    item2 = ItemFactory(id=11, content_type='pdf', brand=brand2, edition_code='test_item_11')
    item_file = ItemFileFactory(
        item=item, file_order=1, file_status=1, file_type=1,
        file_name='images/1/{0.brand.id}/{0.edition_code}.zip'.format(item))
    item_file = ItemFileFactory(
        item=item2, file_order=1, file_status=1, file_type=1,
        file_name='images/1/{0.brand.id}/{0.edition_code}.zip'.format(item2))
    offer_buffet = OfferBuffetFactory(
        available_from=get_utc_nieve() - timedelta(days=7),
        available_until=get_utc_nieve() + timedelta(days=7),
        offer=offer
    )

    order_id = 123455
    cur_idr = s.query(Currencies).get(1)
    order = OrderFactory(id=order_id,
                         total_amount=offer.price_idr, final_amount=offer.price_idr,
                         user_id=1122,
                         order_status=COMPLETE)
    order_line = OrderLineFactory(order=order, offer=offer, user_id=1122, currency_code='IDR',
                                  price=offer.price_idr, final_price=offer.price_idr,
                                  orderline_status=COMPLETE)

    # generate data for testing user that owned the item
    user = UserFactory(id=1122)
    user_item = UserItemFactory(user_id=user.id, item=item, orderline=order_line)

    # for: test user buffet valid - already registered in user owned item
    # generate data for testing user that owned the item via buffet/premum purchase
    user33 = UserFactory(id=1133)
    user_buffet = UserBuffetFactory(
        user_id=user33.id, valid_to=get_utc_nieve()+timedelta(days=7),
        offerbuffet=offer_buffet, orderline=order_line)
    user_item = UserItemFactory(user_id=user33.id, item=item, orderline=order_line,
                                user_buffet=user_buffet)

    # for: test user buffet valid - not yet registered in user owned item
    user44 = UserFactory(id=1144)
    user_buffet = UserBuffetFactory(
        user_id=user44.id, valid_to=get_utc_nieve()+timedelta(days=7),
        offerbuffet=offer_buffet, orderline=order_line)

    # for: test user buffet invalid (already expired)
    user55 = UserFactory(id=1155)
    user_buffet = UserBuffetFactory(
        user_id=user55.id, valid_to=get_utc_nieve()-timedelta(days=7),
        offerbuffet=offer_buffet, orderline=order_line)
    user_item = UserItemFactory(user_id=user55.id, item=item, orderline=order_line,
                                user_buffet=user_buffet)

    # for: test user buffet - with item not exists in user-item but vendor exists in premium/buffet offers
    user66 = UserFactory(id=116611)
    user_buffet = UserBuffetFactory(
        user_id=user66.id, valid_to=get_utc_nieve() + timedelta(days=7),
        offerbuffet=offer_buffet, orderline=order_line)
    s.commit()


def teardown_module():
    db.session().close_all()
    db.drop_all()
    app.before_request_funcs = original_before_request_funcs


def fake_authorized_user(*args, **kwargs):
    g.user = UserInfo(username="ramdani@apps-foundry.com",
                      user_id=1234,
                      token='abcd',
                      perm=['can_read_write_global_all', 'b', 'c', ])


def init_g_regular_user():
    g.user = UserInfo(username='dfdsafsdf', user_id=1100, token='abcde', perm=['abc',])


def init_user_with_item(*args, **kwargs):
    g.user = UserInfo(username="ramdani123@apps-foundry.com",
                      user_id=1122,
                      token='abcd',
                      perm=['bc', 'b', 'c', ])


def init_user_with_valid_buffet_already_registered_in_user_item(*args, **kwargs):
    g.user = UserInfo(username="ramdani33@apps-foundry.com",
                      user_id=1133,
                      token='abcd',
                      perm=['bc', 'b', 'c', ])


def init_user_with_valid_buffet_not_yet_registered_in_user_item(*args, **kwargs):
    g.user = UserInfo(username="ramdani44@apps-foundry.com",
                      user_id=1144,
                      token='abcd',
                      perm=['bc', 'b', 'c', ])


def init_user_with_expired_user_buffet(*args, **kwargs):
    g.user = UserInfo(username="ramdani55@apps-foundry.com",
                      user_id=1155,
                      token='abcd',
                      perm=['bc', 'b', 'c', ])


def init_user_for_buffet_vendors(*args, **kwargs):
    g.user = UserInfo(username="ramdani66@apps-foundry.com",
                      user_id=116611,
                      token='abcd',
                      perm=['bc', 'b', 'c', ])

