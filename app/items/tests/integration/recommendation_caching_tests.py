from __future__ import unicode_literals, absolute_import
import json
import os

from mockredis import mock_strict_redis_client

from app import app
from app.items.recommendations import RecommendationService
# from app.utils.json_schema import validate_scoop_jsonschema
import jsonschema
from app.utils.testcases import E2EBase
from tests.fixtures.sqlalchemy import ItemFactory


class RecommendationCachingServiceTests(E2EBase):

    def setUp(self):
        super(RecommendationCachingServiceTests, self).setUp()
        self.item = ItemFactory(
            issue_number='some author name??',
            thumb_image_highres='high.jpg',
            thumb_image_normal='norm.jpg'
        )
        self.session.commit()

    def test_cache_format(self):
        with app.test_request_context("/meh"):
            r = mock_strict_redis_client()

            svc = RecommendationService(redis=r, session=self.session)

            svc.save_item_document(self.item)

            cached_json = json.loads(r.get("ITEM:{}:RECOMMENDATION_SUMMARY".format(self.item.id)))

            with open(os.path.abspath(os.path.join(
                        os.path.dirname(__file__), os.pardir, os.pardir,
                        'schemas',  'v2', 'item.recommendation.json')
                    )) as fh:
                schema = json.load(fh)
            jsonschema.validate(cached_json, schema)

    def test_get_item_document(self):
        pass



