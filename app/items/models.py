from datetime import datetime

import iso3166, iso639
from enum import Enum
from flask import url_for, g
from flask_appsfoundry.models import TimestampMixin, SoftDeletableMixin
from sqlalchemy import UniqueConstraint, func
from sqlalchemy.dialects.postgresql import ARRAY, ENUM, JSONB
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.sql import desc, select

from app import app, db
from app.items.choices.review import STATUS_REPORT, REPORT_REASON, NEW
from humanize import naturalsize
from app.items.previews import preview_s3_urls, preview_s3_covers
from app.master.choices import GETSCOOP
from app.master.models import Brand
from app.offers.choices.offer_type import BUFFET, SINGLE, BUNDLE, SUBSCRIPTIONS
from app.offers.choices.status_type import READY_FOR_SALE
from app.offers.models import Offer, offer_item
from app.users.users import User
from app.utils.models import ScoopCommonMixin, url_mapped
from app.utils.shims import item_types, languages, countries
from .choices import STATUS_TYPES, FILE_TYPES, ITEM_TYPES, READING_DIRECTIONS
from .choices.item_type import BOOK, MAGAZINE, NEWSPAPER, AUDIOBOOK
from app.audiobooks.models import AudioBook


@url_mapped('authors.authors', (('db_id', 'id'),))
class Author(db.Model, TimestampMixin):
    __tablename__ = 'core_authors'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False, unique=True)
    sort_priority = db.Column(db.SmallInteger)
    is_active = db.Column(db.Boolean(), default=False)
    slug = db.Column(db.Text, nullable=False, unique=True)

    first_name = db.Column(db.Text)
    last_name = db.Column(db.Text)
    title = db.Column(db.String(5))
    academic_title = db.Column(db.String(20))
    birthdate = db.Column(db.DateTime)
    meta = db.Column(db.Text)
    profile_pic_url = db.Column(db.String(255), doc='author profile picture')

    @property
    def api_url(self):
        try:
            return url_for('authors.authors', db_id=self.id, _external=True)
        except RuntimeError:
            return None


item_author = db.Table(
    'core_items_authors',
    db.Column('item_id', db.Integer, db.ForeignKey('core_items.id'), nullable=False),
    db.Column('author_id', db.Integer, db.ForeignKey('core_authors.id'), nullable=False),
    db.PrimaryKeyConstraint('item_id', 'author_id', name='core_items_authors_pk'))

item_category = db.Table(
    'core_items_categories',
    db.Column('item_id', db.Integer, db.ForeignKey('core_items.id'), nullable=False),
    db.Column('category_id', db.Integer, db.ForeignKey('core_categories.id'), nullable=False),
    db.PrimaryKeyConstraint('item_id', 'category_id', name='core_items_categories_pk'))

student_save_items = db.Table(
    'core_items_studentsaves',
    db.Column('item_id', db.Integer, db.ForeignKey('core_items.id'), nullable=False),
    db.Column('studentsave_id', db.Integer, db.ForeignKey('core_eperpus_studentsaves.id'), nullable=False),
    db.PrimaryKeyConstraint('item_id', 'studentsave_id', name='core_items_studentsaves_pk'))


item_audiobooks = db.Table(
    'core_items_audiobooks',
    db.Column('item_id', db.Integer, db.ForeignKey('core_items.id'), nullable=False),
    db.Column('audiobook_id', db.Integer, db.ForeignKey('core_audiobooks.id'), nullable=False),
    db.PrimaryKeyConstraint('item_id', 'audiobook_id', name='core_items_audiobooks_pk'))


class Item(TimestampMixin, SoftDeletableMixin, ScoopCommonMixin, db.Model):
    """ A digital product that can be purchased by individual users.

    Prices are not directly associated with an Item, instead they are handled
    by Offer objects to represent the currently 4 different ways in which
    Items may be sold.
    """
    __tablename__ = 'core_items'

    __serialization_fields__ = ['id', 'name', 'sort_priority',
                                'edition_code', 'brand_id', 'is_featured',
                                'is_extra', 'is_active', 'page_count', ]

    # 'web_reader_files_ready',]

    class Types(Enum):
        magazine = 'magazine'
        book = 'book'
        newspaper = 'newspaper'
        bonus_item = 'bonus item'
        audio_book = 'audio book'

    class Statuses(Enum):
        new = 'new'
        ready_for_upload = 'ready for upload'
        uploaded = 'uploaded'
        waiting_for_review = 'waiting for review'
        rejected = 'rejected'
        in_review = 'in review'
        approved = 'approved'
        prepare_for_consume = 'prepare for consume'
        ready_for_consume = 'ready for consume'
        not_for_consume = 'not for consume'
        mcgrawhill_content = 'mcgrawhill content'

    name = db.Column(db.String(250), nullable=False)
    slug = db.Column(db.Text, nullable=False, unique=True)
    description = db.Column(db.Text)
    meta = db.Column(db.Text)

    display_until = db.Column(db.DateTime)
    release_date = db.Column(db.DateTime,
                             nullable=False,
                             index=True)
    release_schedule = db.Column(db.DateTime)

    is_featured = db.Column(db.Boolean(), default=False)
    is_extra = db.Column(db.Boolean(), default=False)

    edition_code = db.Column(
        db.String(250), unique=True, nullable=False, index=True)
    extra_items = db.Column(ARRAY(db.String))

    subs_weight = db.Column(db.Integer)
    sort_priority = db.Column(db.SmallInteger, nullable=False)

    thumb_image_normal = db.Column(db.String(100), nullable=False)
    thumb_image_highres = db.Column(db.String(100), nullable=False)
    image_normal = db.Column(db.String(150), nullable=False)
    image_highres = db.Column(db.String(150), nullable=False)

    page_count = db.Column(db.Integer)

    # Newspaper/magazine only
    issue_number = db.Column(db.Text)

    # Book/magazine
    gtin13 = db.Column(db.String(25))
    gtin14 = db.Column(db.String(27))
    gtin8 = db.Column(db.String(15))

    # to store vendor's ProductID of pyhsical product/print, for bundling between ebook and print
    vendor_product_id_print = db.Column(db.String(15))

    # Book-only
    revision_number = db.Column(db.Text)

    filenames = db.Column(ARRAY(db.String))
    previews = db.Column(ARRAY(db.String))

    countries = db.Column(ARRAY(db.String(3)),
                          server_default='{}',
                          nullable=False)

    languages = db.Column(ARRAY(db.String(3)),
                          server_default='{}',
                          nullable=False)

    item_status = db.Column(ENUM(*STATUS_TYPES.values(), name='item_status'),
                            nullable=False)
    content_type = db.Column(ENUM(*FILE_TYPES.values(), name='item_file_type'),
                             nullable=False)
    item_type = db.Column(ENUM(*ITEM_TYPES.values(), name='item_type'),
                          nullable=False)
    reading_direction = db.Column(
        ENUM(*READING_DIRECTIONS.values(),
             name='reading_direction'),
        nullable=False)

    parentalcontrol_id = db.Column(db.Integer, db.ForeignKey('core_parentalcontrols.id'))
    parentalcontrol = db.relationship("ParentalControl")

    brand_id = db.Column(db.Integer, db.ForeignKey('core_brands.id'), nullable=False, index=True)
    brand = db.relationship('Brand', backref=db.backref('items', lazy='dynamic'))

    item_distribution_country_group_id = db.Column(db.SmallInteger, db.ForeignKey('core_distributioncountries.id'))
    item_distribution_country_group = db.relationship("DistributionCountryGroup")

    parent_item_id = db.Column(db.Integer, db.ForeignKey('core_items.id'), nullable=True)
    parent_item = db.relationship('Item')

    printed_price = db.Column(db.Numeric(10, 2))
    printed_currency_code = db.Column(db.String(3))
    file_size = db.Column(db.Integer, nullable=True)
    is_internal_content = db.Column(db.Boolean(), default=False)

    authors = db.relationship(
        'Author',
        secondary=item_author,
        backref=db.backref('core_items', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    categories = db.relationship(
        'Category',
        secondary=item_category,
        backref=db.backref('core_items', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    student_save = db.relationship(
        'StudentSave',
        secondary=student_save_items,
        backref=db.backref('core_items', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    audiobooks = db.relationship(
        'AudioBook',
        secondary=item_audiobooks,
        backref=db.backref('core_items', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    @property
    def ser_thumb_normal(self):
        return self.thumb_image_normal.replace('images/1', '')

    @property
    def ser_thumb_highres(self):
        return self.thumb_image_highres.replace('images/1', '')

    @property
    def api_url(self):
        try:
            return url_for('items.details', item_id=self.id, _external=True)
        except RuntimeError:
            return None

    @property
    def download_url(self):
        try:
            return url_for('items.download_content_file', db_id=self.id, _external=True)
        except RuntimeError:
            return None

    @property
    def web_reader_url(self):
        try:
            # TODO: it should get from blueprints and have it's own API
            url = url_for('items.details', item_id=self.id, _external=True)
            return '{}/web-reader'.format(url)
        except RuntimeError:
            return None

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def get_is_latest(self):
        if self.item_type in ['magazine', 'newspaper', 'book']:
            item_by_brands = Item.query.filter_by(
                brand_id=self.brand_id,
                is_active=True,
                item_status='ready for consume'
            ).order_by(
                desc(Item.release_date)
            ).order_by(
                desc(Item.id)
            ).first()

            if item_by_brands:
                if int(self.id) == int(item_by_brands.id):
                    return True

        return False

    def get_latest(self):
        if self.item_type in ['magazine', 'newspaper', 'book']:
            item_by_brands = Item.query.join(offer_item).join(Offer).filter(
                Item.brand_id == self.brand_id,
                Item.is_active == True,
                Item.item_status == 'ready for consume'
            ).filter(Offer.is_free == False).order_by(
                desc(Item.release_date)
            ).order_by(
                desc(Item.id)
            ).first()

            if item_by_brands:
                return item_by_brands

        return self

    @hybrid_property
    def later_item_count(self):
        count = 0
        if self.item_type in ['magazine', 'newspaper', 'book']:
            count = Item.query\
                .filter(
                    (Item.brand_id==self.brand_id) &
                    (Item.created > self.created)
                ).count()
        return count

    @later_item_count.expression
    def later_items_count(cls):
        tbl = Item.__table__.alias('a')
        offer = Offer.__table__.alias('b')
        oi = offer_item

        return select([func.count()]) \
            .select_from(tbl) \
            .where((tbl.c.brand_id == cls.brand_id) & (tbl.c.created > cls.created)) \
            .label("later_items_count")

    @property
    def web_reader_files_ready(self):
        return False
        # try:
        #     assert_web_reader_files_ready(self.id, self.brand_id, app.kvs)
        #     return True
        # except WebReaderFilesNotReady:
        #     return False

    def eperpus_web_reader_files_ready(self):
        from app.eperpus.models import WebReaderLog
        return bool(WebReaderLog.query.filter_by(item_id=self.id, is_web_reader=True).count())

    def extension_values(self, extension=None, custom=None, platform_id=None, is_active=None):
        data_values = self.values()

        for param in extension:
            if param == 'languages':
                data_values['languages'] = self.get_languages()

            if param == 'authors':
                data_values['authors'] = self.get_authors()

            if param == 'categories':
                data_values['categories'] = self.get_categories()

            if param == 'brand':
                data_values['brand'] = self.get_brand()

            if param == 'countries':
                data_values['countries'] = self.get_country()

            if param == 'item_type':
                data_values['item_type'] = self.get_item_type()

            if param == 'vendor':
                data_values['vendor'] = self.get_vendor()

            if param == 'parental_control':
                data_values['parental_control'] = self.get_parental_control()

            if param == 'audiobook':
                data_values['audiobook'] = self.get_audiobooks()
                data_values['audiobook_offers'] = []

            if param in ['display_offers', 'premium_ext']:
                if param == 'premium_ext':
                    display_other_premium = True
                else:
                    display_other_premium = False

                if platform_id == GETSCOOP:
                    display_offer, display_price = self.get_offers(platform_id=GETSCOOP, display_other_premium=display_other_premium)
                    data_values['display_offers'] = display_offer
                    data_values['display_price'] = display_price
                else:
                    display_offer, display_price = self.get_offers(platform_id=platform_id, display_other_premium=display_other_premium)
                    data_values['display_offers'] = display_offer
                    data_values['display_price'] = display_price

                #    show restricted and allowed country.
                restricted, allowed = self.get_restriction_country()
                data_values['restricted_countries'] = restricted
                data_values['allowed_countries'] = allowed

            if param in ['ext', 'premium_ext']:
                if param == 'premium_ext':
                    display_other_premium = True
                else:
                    display_other_premium = False

                data_values['languages'] = self.get_languages()
                data_values['authors'] = self.get_authors()
                data_values['categories'] = self.get_categories()
                data_values['countries'] = self.get_country()
                data_values['item_type'] = self.get_item_type()
                data_values['brand'] = self.get_brand()
                data_values['vendor'] = self.get_vendor()
                data_values['parental_control'] = self.get_parental_control()

                if platform_id == GETSCOOP:
                    display_offer, display_price = self.get_offers(platform_id=GETSCOOP, display_other_premium=display_other_premium)
                    data_values['display_offers'] = display_offer
                    data_values['display_price'] = display_price
                else:
                    display_offer, display_price = self.get_offers(platform_id=platform_id, display_other_premium=display_other_premium)
                    data_values['display_offers'] = display_offer
                    data_values['display_price'] = display_price

                #    show restricted and allowed country.
                restricted, allowed = self.get_restriction_country()
                data_values['restricted_countries'] = restricted
                data_values['allowed_countries'] = allowed

        if custom:
            original_values = data_values

            kwargs = {}
            fields_required = custom.split(',')
            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
            return kwargs

        return data_values

    def get_offers(self, platform_id=None, display_other_premium=False):
        data_offers, display_offers, offer_single, offer_subs, offer_buffet, offer_bundle = [], [], [], [], [], []

        if item_types.enum_to_id(self.item_type) in [MAGAZINE, BOOK, NEWSPAPER]:

            # if newspapers, only show latest
            if self.item_type == ITEM_TYPES[NEWSPAPER]:
                offer_single = Offer.query.join(Item.core_offers).filter(
                    Item.id.in_([self.id])).filter_by(offer_status=READY_FOR_SALE,
                        is_active=True, offer_type_id=SINGLE).order_by(Offer.price_usd).all()
            else:
                offer_single = Offer.query.join(Item.core_offers).filter(
                    Item.id.in_([self.id])).filter_by(offer_status=READY_FOR_SALE,
                        is_active=True, offer_type_id=SINGLE).order_by(Offer.price_usd).all()

                if self.item_type == ITEM_TYPES[BOOK]:
                    if display_other_premium:
                        offer_buffet = Offer.query.join(Brand.core_offers).filter(
                            Brand.id.in_([self.brand_id])).filter_by(offer_status=READY_FOR_SALE,
                                is_active=True, offer_type_id=BUFFET).order_by(Offer.price_usd).all()
                    else:
                        offer_buffet = Offer.query.join(Brand.core_offers).filter(
                            Brand.id.in_([self.brand_id])).filter_by(
                            offer_status=READY_FOR_SALE, is_active=True, offer_type_id=BUFFET, id=43103).order_by(
                            Offer.price_usd).all()

            # find offer bundle, all item type can have bundle..!
            offer_bundle = Offer.query.join(Item.core_offers).filter(
                Item.id.in_([self.id])).filter_by(offer_status=READY_FOR_SALE,
                is_active=True, offer_type_id=BUNDLE).order_by(Offer.price_usd).all()

            # Product Request, displayed premium --> single --> subs --> bundle.
            if offer_buffet:
                for buffet in offer_buffet:
                    if buffet not in data_offers:
                        data_offers.append(buffet)
            data_offers = data_offers + offer_single + offer_bundle

        if item_types.enum_to_id(self.item_type) in [MAGAZINE, NEWSPAPER, BOOK]:
            # only latest show subs for magazine
            if self.item_type in (ITEM_TYPES[MAGAZINE], ITEM_TYPES[BOOK]):
                latest_stat = self.get_is_latest()

                if display_other_premium:
                    offer_buffet = Offer.query.join(Brand.core_offers).filter(
                        Brand.id.in_([self.brand_id])).filter_by(offer_status=READY_FOR_SALE,
                            is_active=True, offer_type_id=BUFFET).order_by(Offer.price_usd).all()
                else:
                    offer_buffet = Offer.query.join(Brand.core_offers).filter(Brand.id.in_([self.brand_id])).filter_by(
                        offer_status=READY_FOR_SALE, is_active=True, offer_type_id=BUFFET, id=43103).order_by(
                        Offer.price_usd).all()

                if latest_stat:
                    offer_subs = Offer.query.join(Brand.core_offers).filter(
                        Brand.id.in_([self.brand_id])).filter_by(offer_status=READY_FOR_SALE,
                        is_active=True, offer_type_id=SUBSCRIPTIONS).order_by(Offer.price_usd).all()

                # Product Request, displayed premium --> single --> subs --> bundle.
                if offer_buffet:
                    for buffet in offer_buffet:
                        if buffet not in data_offers:
                            data_offers.append(buffet)
                data_offers = data_offers + offer_subs

            # all edition show subs.
            if self.item_type in (ITEM_TYPES[NEWSPAPER]):
                offer_subs = Offer.query.join(Brand.core_offers).filter(
                    Brand.id.in_([self.brand_id])).filter_by(offer_status=READY_FOR_SALE,
                    is_active=True, offer_type_id=SUBSCRIPTIONS).order_by(Offer.price_usd).all()

                if display_other_premium:
                    offer_buffet = Offer.query.join(Brand.core_offers).filter(
                        Brand.id.in_([self.brand_id])).filter_by(offer_status=READY_FOR_SALE,
                        is_active=True, offer_type_id=BUFFET).order_by(Offer.price_usd).all()
                else:
                    offer_buffet = Offer.query.join(Brand.core_offers).filter(Brand.id.in_([self.brand_id])).filter_by(
                        offer_status=READY_FOR_SALE, is_active=True, offer_type_id=BUFFET, id=43103).order_by(
                        Offer.price_usd).all()

                # Product Request, displayed premium --> single --> subs --> bundle.
                if offer_buffet:
                    for buffet in offer_buffet:
                        if buffet not in data_offers:
                            data_offers.append(buffet)
                data_offers = data_offers + offer_subs

        # data_offers = list(set(data_offers))
        current_client_id = g.current_client.id if getattr(g, 'current_client', None) and g.current_client else None

        for ioffer in data_offers:
            # exclude premium offers for whitelabel clients
            if current_client_id and current_client_id in app.config['WHITELABEL_CLIENT_ID'] and ioffer.offer_type_id == BUFFET:
                offers = []
            else:
                offers = ioffer.display_offers([platform_id])
            display_offers = display_offers + offers

        display_price = {}
        for idisplay in display_offers:
            offer_type = idisplay.get('offer_type_id', None)

            # always display free if one of offer already set as free purchase.
            is_free = idisplay.get('is_free', False)
            if is_free:
                display_price = idisplay
                break

            if offer_type == SINGLE:
                display_price = idisplay
                break
            if offer_type == SUBSCRIPTIONS:
                display_price = idisplay
                break

        return display_offers, display_price

    def get_parental_control(self):
        return {
            'id': self.parentalcontrol.id,
            'name': self.parentalcontrol.name,
        }

    def get_item_type(self):
        return {
            # reverse the enums to find the key by the name
            'id': {ITEM_TYPES[k]: k for k in ITEM_TYPES}[self.item_type],
            # make the first letter caps
            'name': self.item_type[:1].upper() + self.item_type[1:]
        }

    def get_vendor(self):
        return {
            'id': self.brand.vendor.id,
            'name': self.brand.vendor.name,
            'slug': self.brand.vendor.slug
        }

    def get_languages(self):
        return [
            {
                'id': languages.iso639_to_id(lang_code),
                'name': iso639.find(lang_code)['name']
            }
            for lang_code in self.languages]

    def get_brand(self):
        return {
            'id': self.brand.id,
            'name': self.brand.name,
            'slug': self.brand.slug
        }

    def get_country(self):
        return [
            {
                'name': iso3166.countries.get(country_code).name,
                'id': countries.iso3166_to_id(country_code),
            }
            for country_code in self.countries]

    def get_authors(self):
        data = []
        if self.authors:
            data = [{'id': i.id, 'name': i.name, 'slug': i.slug} for i in self.authors]
        return data

    def get_categories(self):
        data = []
        if self.categories:
            data = [{'id': i.id, 'name': i.name, 'slug': i.slug} for i in self.categories]
        return data

    def get_extra_items(self):
        return self.extra_items

    def get_restriction_country(self):

        restricted_countries, allowed_countries = [], []
        _restricted, _allowed = 1, 2

        if self.item_distribution_country_group:

            if self.item_distribution_country_group.group_type == _restricted:
                restricted_countries = self.item_distribution_country_group.countries

            if self.item_distribution_country_group.group_type == _allowed:
                allowed_countries = self.item_distribution_country_group.countries

        return restricted_countries, allowed_countries

    def get_descriptions(self):
        return self.description

    def get_magazine(self):
        magazine = {
            'issue_number': self.issue_number,
            'slug': self.slug,
            'thumb_image_normal': preview_s3_covers(self, 'thumb_image_normal'),
            'thumb_image_highres': preview_s3_covers(self, 'thumb_image_highres'),
            'subs_weight': self.subs_weight,
            'extra_items': self.extra_items or [],
            'meta': self.meta,
            'image_normal': preview_s3_covers(self, 'image_normal'),
            'image_highres': preview_s3_covers(self, 'image_highres'),
            'filenames': self.filenames,
            'gtin13': self.gtin13,
            'gtin14': self.gtin14,
            'gtin8': self.gtin8,
            'reading_direction': {READING_DIRECTIONS[k]: k
                                  for k in READING_DIRECTIONS}[self.reading_direction],
        }

        return magazine

    def get_books(self):
        book = {
            'revision_number': self.revision_number,
            'issue_number': self.issue_number,
            'slug': self.slug,
            'thumb_image_normal': preview_s3_covers(self, 'thumb_image_normal'),
            'thumb_image_highres': preview_s3_covers(self, 'thumb_image_highres'),
            'subs_weight': self.subs_weight,
            'extra_items': self.extra_items or [],
            'meta': self.meta,
            'image_normal': preview_s3_covers(self, 'image_normal'),
            'image_highres': preview_s3_covers(self, 'image_highres'),
            'filenames': self.filenames,
            'gtin13': self.gtin13,
            'gtin14': self.gtin14,
            'gtin8': self.gtin8,
            'reading_direction':
                {READING_DIRECTIONS[k]: k
                 for k in READING_DIRECTIONS}[self.reading_direction],
        }

        return book

    def get_news(self):
        news = {
            'issue_number': self.issue_number,
            'slug': self.slug,
            'thumb_image_normal': preview_s3_covers(self, 'thumb_image_normal'),
            'thumb_image_highres': preview_s3_covers(self, 'thumb_image_highres'),
            'subs_weight': self.subs_weight,
            'extra_items': self.extra_items or [],
            'meta': self.meta,
            'image_normal': preview_s3_covers(self, 'image_normal'),
            'image_highres': preview_s3_covers(self, 'image_highres'),
            'filenames': self.filenames,
            'reading_direction': {READING_DIRECTIONS[k]: k
                                  for k in READING_DIRECTIONS}[self.reading_direction],
        }

        return news

    def get_audiobooks(self):
        data_audio = None
        if not self.parent_item_id:
            child_audio = Item.query.filter_by(parent_item_id=self.id).first()
            if child_audio:
                data_audio = child_audio.audiobooks.filter_by(is_active=True).order_by(AudioBook.chapter).all()
        else:
            if self.item_type == ITEM_TYPES[AUDIOBOOK]:
                data_audio = self.audiobooks.filter_by(is_active=True).order_by(AudioBook.chapter).all()

        return [
            {'subtitle': '{} {}'.format(audio.subtitle, audio.chapter),
             'title': audio.title,
             'chapter': audio.chapter,
             'file_size': naturalsize(audio.file_size) if audio.file_size > 0 else '- MB',
             'file_time': audio.file_time
             } for audio in data_audio] if data_audio else []

    def values(self):
        rv = super(Item, self).values()

        rv['brand'] = {'id': self.brand.id, 'name': self.brand.name}
        rv['parental_control_id'] = self.parentalcontrol_id
        rv['media_base_url'] = app.config['MEDIA_BASE_URL']
        rv['is_latest'] = self.get_is_latest()
        rv['release_date'] = self.release_date.isoformat()
        rv['created'] = self.created.isoformat()
        rv['modified'] = self.modified.isoformat()
        rv['printed_currency_code'] = self.printed_currency_code
        rv['is_webreader'] = self.eperpus_web_reader_files_ready()

        if self.printed_price:
            rv['printed_price'] = '%.2f' % self.printed_price
        else:
            rv['printed_price'] = None

        # all of these are enum fields, but for API compatibility, they need
        # converted back to integer format
        rv['item_type_id'] = \
            {ITEM_TYPES[k]: k for k in ITEM_TYPES}[self.item_type]
        rv['item_status'] = \
            {STATUS_TYPES[k]: k for k in STATUS_TYPES}[self.item_status]
        rv['content_type'] = \
            {FILE_TYPES[k]: k for k in FILE_TYPES}[self.content_type]

        if self.release_schedule:
            rv['release_schedule'] = self.release_schedule.isoformat()
        else:
            rv['release_schedule'] = ''

        rv['item_distribution_country_group_id'] = self.item_distribution_country_group_id

        if self.display_until:
            #    need cron job for set is_active if
            #    display_until > datetime.now()
            rv['display_until'] = self.display_until.isoformat()

        if self.item_type == ITEM_TYPES[MAGAZINE]:
            rv.update(self.get_magazine())

        if self.item_type == ITEM_TYPES[BOOK]:
            rv.update(self.get_books())

        if self.item_type == ITEM_TYPES[NEWSPAPER]:
            rv.update(self.get_news())

        # preview_url_builder = PreviewUrlBuilder(app.config['BASE_URL'])
        # rv['previews'] = preview_url_builder.make_urls(self)
        rv['previews'] = preview_s3_urls(self)
        rv['vendor_product_id_print'] = self.vendor_product_id_print
        rv['is_internal_content'] = self.is_internal_content
        rv['file_size'] = naturalsize(self.file_size) if self.file_size > 0 else '- MB'

        for key in rv:
            # hack for bad input parsing?  Flask-Restful has a bug with
            # null string values in input parsing (resolved in scoopflask)
            if rv[key] in ['None', '']:
                rv[key] = None

        return rv


class ItemFile(TimestampMixin, ScoopCommonMixin, db.Model):
    __tablename__ = 'core_itemfiles'

    __serialization_fields__ = ['id', 'item_id', 'file_type',
                                'file_name', 'file_order', 'file_status',
                                'file_version', 'is_active', 'key',
                                'md5_checksum', 'page_view']

    id = db.Column(db.Integer, primary_key=True)
    item_id = db.Column(db.Integer, db.ForeignKey('core_items.id'), nullable=False)

    # TODO: do items have multiple files?
    item = db.relationship("Item", backref=db.backref("files"))

    file_type = db.Column(db.SmallInteger, nullable=False)  # READ : FILE TYPE LEGEND
    file_name = db.Column(db.Text, nullable=False)
    file_order = db.Column(db.SmallInteger, nullable=False)
    file_status = db.Column(db.SmallInteger, nullable=False)  # READ : FILE STATUS LEGEND
    file_version = db.Column(db.String(250))

    is_active = db.Column(db.Boolean())
    key = db.Column(db.String(250))
    md5_checksum = db.Column(db.String(250))
    page_view = db.Column(db.Integer)

    def __repr__(self):
        return '<File name : %s>' % self.file_name

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs


class UserItem(TimestampMixin, ScoopCommonMixin, db.Model):
    __tablename__ = 'core_useritems'

    __table_args__ = (
        UniqueConstraint(
            'user_id',
            'user_buffet_id',
            'item_id',
            name='unique_user_item_buffet'),
    )

    __serialization_fields__ = ['id', 'user_id', 'item_id', 'edition_code',
                                'is_active', 'itemtype_id', 'vendor_id', 'brand_id', 'user_buffet_id', ]

    def values(self):
        rv = ScoopCommonMixin.values(self)
        rv['created'] = self.created.isoformat()
        rv['modified'] = self.modified.isoformat()
        rv['order_line_id'] = self.orderline_id
        rv['brand'] = self.get_brand()
        rv['vendor'] = self.get_vendor()
        rv['item'] = self.get_item()
        rv['authors'] = self.get_authors()

        return rv

    user_id = db.Column(db.Integer, nullable=False)

    user_buffet_id = db.Column(db.Integer, db.ForeignKey('core_userbuffets.id'), nullable=True)
    user_buffet = db.relationship("UserBuffet")

    item_id = db.Column(db.Integer, db.ForeignKey('core_items.id'), nullable=False)
    item = db.relationship("Item", backref="user_items")
    edition_code = db.Column(db.String(250))

    orderline_id = db.Column(
        db.Integer,
        db.ForeignKey('core_orderlines.id'),
        nullable=True,
        doc="References the order line item that this UserItem was acquired "
            "from. This is purpously left NULLABLE, because UserItems migrated "
            "from SCOOPCORE 1 do not have an order line item.")
    orderline = db.relationship("OrderLine")

    is_active = db.Column(db.Boolean(), default=False)
    is_restore = db.Column(db.Boolean(), default=False)

    # all 3 of these can come from item
    itemtype_id = db.Column(db.Integer)
    vendor_id = db.Column(db.Integer, db.ForeignKey('core_vendors.id'))
    vendor = db.relationship("Vendor")
    brand_id = db.Column(db.Integer, db.ForeignKey('core_brands.id'))
    brand = db.relationship("Brand")

    def __repr__(self):
        return '<user : %s item: %s>' % (self.user_id, self.item_id)

    def extension_values(self, extension=None):
        data_values = self.values()

        for param in extension:
            if param == 'item':
                data_values['item'] = self.get_items()
        return data_values

    def get_items(self):
        xo = {}
        if self.item_id:
            item = Item.query.filter_by(id=self.item_id).first()
            if item:
                xo = item.values()
        return xo

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def get_brand(self):
        temp = {}
        if self.brand:
            temp = {'id': self.brand.id, 'name': self.brand.name}
        else:
            temp = {
                'id': self.item.brand.id,
                'name': self.item.brand.name
            }

        return temp

    def get_item(self):
        temp = {}
        if self.item:
            temp = {'id': self.item.id, 'name': self.item.name}

        return temp

    def get_vendor(self):
        temp = {}
        if self.vendor:
            temp = {'id': self.vendor.id, 'name': self.vendor.name}

        return temp

    def get_authors(self):
        temp = []
        authors = self.item.authors.all()
        if authors:
            for author in authors:
                temp.append({'id': author.id, 'name': author.name})

        return temp


class UserSubscription(TimestampMixin, ScoopCommonMixin, db.Model):
    """
        *** This models record subscriptions data purchase
    """
    __tablename__ = 'core_usersubscriptions'

    __serialization_fields__ = ['id', 'user_id', 'quantity',
                                'quantity_unit', 'current_quantity',
                                'brand_id', 'subscription_code',
                                'allow_backward', 'backward_quantity',
                                'backward_quantity_unit', 'is_active', ]

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    quantity = db.Column(db.Integer, nullable=False)
    quantity_unit = db.Column(db.Integer, nullable=False)
    current_quantity = db.Column(db.Integer, nullable=False)
    valid_from = db.Column(db.DateTime, nullable=False)
    valid_to = db.Column(db.DateTime, nullable=False)

    brand_id = db.Column(db.Integer, db.ForeignKey('core_brands.id'), nullable=False)
    brand = db.relationship('Brand')

    subscription_code = db.Column(db.String(250))
    orderline_id = db.Column(db.Integer, db.ForeignKey('core_orderlines.id'))
    orderline = db.relationship('OrderLine')

    allow_backward = db.Column(db.Boolean(), default=False)
    backward_quantity = db.Column(db.Integer)
    backward_quantity_unit = db.Column(db.Integer)
    is_active = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return '<user_id : %s quantity: %s>' % (self.user_id, self.quantity)

    def subs_valid(self):
        valid = False

        if self.quantity_unit == 1:  # by unit
            if int(self.current_quantity) > 0:
                return True

        if self.quantity_unit in [2, 3, 4]:  # by days/week/month
            if datetime.now() >= self.valid_from and datetime.now() <= self.valid_to:
                return True

        return valid

    def extension_values(self, extension=None):
        data_values = self.values()

        for param in extension:
            if param == 'brand':
                data_values['brand'] = self.get_brand()

            if param == 'items':
                data_values['items'] = self.get_items()

            if param == 'ext':
                data_values['brand'] = self.get_brand()
                data_values['items'] = self.get_items()

        return data_values

    def get_items(self):
        subs = UserSubscriptionItem.query.filter_by(usersubscription_id=self.id).all()
        data = []
        for item in subs:
            sub_item = UserItem.query.filter_by(id=item.item_id).first()
            if sub_item:
                data.append(sub_item.values())

        return data

    def get_brand(self):
        brand = Brand.query.filter_by(id=self.brand_id).first()
        if brand:
            return brand.values()
        else:
            return {}

    def values(self):
        rv = ScoopCommonMixin.values(self)
        if self.valid_from:
            rv['valid_from'] = self.valid_from.isoformat()
        if self.valid_to:
            rv['valid_to'] = self.valid_to.isoformat()

        rv['order_line_id'] = self.orderline_id
        return rv

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs


class UserSubscriptionItem(TimestampMixin, ScoopCommonMixin, db.Model):
    """
        *** This models store relations data from core_usersubscriptions --> core_useritems
    """

    __tablename__ = 'core_usersubscriptions_items'

    __serialization_fields__ = ['id', 'usersubscription_id', 'useritem_id',
                                'item_id', 'user_id', 'note', ]

    id = db.Column(db.Integer, primary_key=True)
    usersubscription_id = db.Column(db.Integer, nullable=False)  # UserSubscription id
    useritem_id = db.Column(db.Integer, nullable=False)  # UserItem id
    item_id = db.Column(db.Integer, nullable=False)
    user_id = db.Column(db.Integer, nullable=False)
    note = db.Column(db.String(250))

    def __repr__(self):
        return '<user_subscription : %s user_item: %s>' % (
            self.usersubscription_id, self.useritem_id)


class PopularItem(db.Model):
    __table__ = db.Table(
        'item_popular',
        db.metadata,
        db.Column('id', db.Integer, db.ForeignKey('core_items.id'), primary_key=True),
        db.Column('item_status', db.Text()),
        db.Column('download_count', db.BigInteger),
        db.Column('item_type', db.String()),
        db.Column('is_active', db.Boolean),
        db.Column('vendor_id', db.Integer),
        db.Column('is_latest', db.Boolean),
        db.Column('category_id', ARRAY(db.Integer)),
        db.Column('non_free_platforms', ARRAY(db.Integer)),
        db.Column('countries', ARRAY(db.String(3))),
        db.Column('languages', ARRAY(db.String(3))),
        db.Column('is_buffet', db.Boolean),
        schema='utility'
    )
    item = db.relationship('Item')


class DownloadItem(TimestampMixin, ScoopCommonMixin, db.Model):
    """
        *** This model only store link download for 15 minutes
        TO DO :
            - clean up all data > 1 hours
    """

    __tablename__ = 'core_itemdownloads'

    __serialization_fields__ = ['id', 'user_id', 'item_id',
                                'itemfile_id', 'dynamic_path',
                                'file_path', ]

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    item_id = db.Column(db.Integer)
    itemfile_id = db.Column(db.Integer)
    download_uri = db.Column(db.String(250))
    file_path = db.Column(db.String(250))
    valid_from = db.Column(db.DateTime)
    valid_to = db.Column(db.DateTime)

    def __repr__(self):
        return '<user_id : %s item_id: %s>' % (self.user_id, self.item_id)

    def valid_status(self):
        return datetime.now() <= self.valid_to

    def values(self):
        rv = ScoopCommonMixin.values(self)
        rv['valid_from'] = self.valid_from.isoformat()
        rv['valid_to'] = self.valid_to.isoformat()
        return rv


class DistributionCountryGroup(TimestampMixin, ScoopCommonMixin, db.Model):
    """
        *** This model for data master restricted/allowed countries group

        GROUP TYPE:
            1. RESTRICTS
            2. ALLOWS
    """

    __tablename__ = 'core_distributioncountries'

    __serialization_fields__ = ['id', 'name', 'group_type', 'countries',
                                'is_active', 'vendor_id']

    name = db.Column(db.String(250))
    group_type = db.Column(db.Integer)  # READ LEGEND : GROUP TYPE
    countries = db.Column(ARRAY(db.String))
    is_active = db.Column(db.Boolean(), default=False)
    vendor_id = db.Column(db.Integer)

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs


class ItemUploadProcess(TimestampMixin, ScoopCommonMixin, db.Model):
    """ Processing status information for an Item content file that has been
    uploaded.

        STATUS
            1. New
            2. Processing
            3. Create temp path
            4. Generate password
            5. Generate covers
            6. Send cover to scoopadm
            7. Generate thumbs
            8. Generate zip files
            9. Update item files
            10. Send zip file to 62
            11. Complete
    """
    __tablename__ = 'core_itemuploads'

    __serialization_fields__ = ['pic_id', 'pic_name', 'role_id', 'item_id',
                                'release_schedule', 'start_process_time', 'end_process_time',
                                'file_name', 'file_dir', 'file_size', 'status', 'error', 'file_check']

    pic_id = db.Column(db.Integer)
    pic_name = db.Column(db.String(250))
    role_id = db.Column(db.Integer)
    item_id = db.Column(
        db.Integer, db.ForeignKey('core_items.id'), nullable=False)
    item = db.relationship(
        "Item", backref=db.backref("upload_process", uselist=False))

    # TO DO : REMOVE RELEASE SCHEDULE --<
    release_schedule = db.Column(db.DateTime)  # TODO: is this used?

    start_process_time = db.Column(
        db.DateTime,
        doc="Date/Time when our offline processing script begins trying to"
            "convert this file into our storage format.")

    end_process_time = db.Column(
        db.DateTime,
        doc='Date/Time when our processing script completes (or fails).')

    file_name = db.Column(
        db.String(250), nullable=False,
        doc="Primary EPUB/PDF that was uploaded.")

    file_dir = db.Column(
        db.String(250), nullable=False)

    # cover_image_file_name = db.Column(
    #    db.String(250), doc="JPG/PNG cover image file (only used for EPUBs)")

    file_size = db.Column(db.Integer, doc='The size of the uploaded file, '
                                          'in bytes.')  # TODO: consider removal
    status = db.Column(db.Integer, nullable=False, default=1)
    file_check = db.Column(db.Integer, default=0)
    error = db.Column(db.Text, doc="Error message if processing has failed.")


class ItemRegister(TimestampMixin, ScoopCommonMixin, db.Model):
    """ This model for store all data free items when register
    """

    __tablename__ = 'core_gifts'

    __serialization_fields__ = ['id', 'name', 'description',
                                'items', 'brands', 'is_active', 'point', 'point_referral']

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250))
    description = db.Column(db.Text)
    items = db.Column(ARRAY(db.String))
    brands = db.Column(ARRAY(db.String))
    valid_from = db.Column(db.DateTime)
    valid_to = db.Column(db.DateTime)
    is_active = db.Column(db.Boolean(), default=False)
    point = db.Column(db.Integer)
    point_referral = db.Column(db.Integer)

    def __repr__(self):
        return '<name : %s>' % (self.name)

    def is_valid(self):
        valid = False

        if datetime.now() >= self.valid_from and datetime.now() <= self.valid_to:
            valid = True
        return valid

    def get_items(self):
        items_data = []

        if self.brands:
            # get latest items
            for brand_id in self.brands:
                item_by_brand = Item.query.filter_by(brand_id=brand_id, is_active=True).order_by(
                    desc(Item.release_date)).order_by(desc(Item.id)).first()

                if item_by_brand:
                    items_data.append(item_by_brand)

        if self.items:
            items_specific = Item.query.filter(Item.id.in_(self.items)).all()
            items_data = items_data + items_specific

        return items_data

    def values(self):
        rv = ScoopCommonMixin.values(self)
        rv['valid_from'] = self.valid_from.isoformat()
        rv['valid_to'] = self.valid_to.isoformat()
        return rv


class Review(db.Model, TimestampMixin):
    __tablename__ = 'core_reviews'

    id = db.Column(db.Integer, primary_key=True)
    is_active = db.Column(db.Boolean(), default=True)
    review = db.Column(db.Text(), nullable=True)
    rating = db.Column(db.SmallInteger(), nullable=False)

    item_id = db.Column(db.Integer, db.ForeignKey('core_items.id'), nullable=False)
    item = db.relationship(Item, backref=db.backref("item_review"))

    user_id = db.Column(db.Integer, db.ForeignKey("cas_users.id"), nullable=False)
    user = db.relationship(User, backref='user_review')

    @property
    def report_review_url(self):
        return url_for('eperpus_review.report_review', review=self, _external=True)

    @property
    def reported_detail_url(self):
        return url_for('vendor.reported-review-detail', review_id=self.id, _external=True)

    @property
    def reported_count(self):
        return len(self.reports)

    @property
    def item_name(self):
        return self.item.name

    def response_data(self):
        return {
            "id": self.id,
            "review": self.review,
            "rating": self.rating,
            "email": self.user.email if self.user else None,
            "item": self.item.name if self.item else None
        }


class ReportReview(db.Model, TimestampMixin):
    __tablename__ = 'core_report_review'

    id = db.Column(db.Integer, primary_key=True)
    is_active = db.Column(db.Boolean(), default=False)
    status = db.Column(ENUM(*STATUS_REPORT.values(), name='status'), nullable=False, default=NEW)
    reason = db.Column(ENUM(*REPORT_REASON.values(), name='reason'), nullable=False)
    notes = db.Column(db.Text(), nullable=True)

    user_id = db.Column(db.Integer, db.ForeignKey("cas_users.id"), nullable=False)
    user = db.relationship(User, backref='user_reports')

    review_id = db.Column(db.Integer, db.ForeignKey('core_reviews.id'), nullable=False)
    review = db.relationship(Review, backref=db.backref("reports"))


class ReportDataChangeModified(db.Model, TimestampMixin):

    __tablename__ = 'core_report_data_modified'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey("cas_users.id"), nullable=False)
    user = db.relationship(User, backref='core_report_data_modified')
    data_id = db.Column(db.Integer)
    data_url = db.Column(db.Text(), nullable=True)
    data_method = db.Column(db.String(50))
    data_request = db.Column(JSONB)
