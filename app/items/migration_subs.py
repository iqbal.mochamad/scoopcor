import json
import httplib

from flask_restful import Resource, Api, reqparse
from flask_restful import Api, reqparse
from flask import abort, Response, jsonify, request

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta

from app import app, db
from app.constants import *
from app.auth.decorators import token_required

from app.helpers import log_api, assertion_error, message_json
from app.helpers import dtserializer, err_response, date_validator
from app.helpers import conflict_message, bad_request_message, internal_server_message

from sqlalchemy.sql import and_, or_, desc

from app.items.models import Item, UserItem, UserSubscription, UserSubscriptionItem
from app.master.models import Brand

from app.utils.shims import item_types


class LinkSubs():

    def __init__(self, args=None, module=None):
        self.args = args
        self.method = module

        self.user_id = args.get('user_id', None)
        self.item_id = args.get('item_id', None)
        self.brand_id = args.get('brand_id', None)

        self.edition_code = args.get('edition_code', None)
        self.orderline_id = args.get('orderline_id', None)
        self.is_active = args.get('is_active', None)

        self.item_id_purchased = []

        self.subs_data = ''
        self.item_data = ''
        self.valid_data = False

    def check_brands(self):
        try:
            brands = Brand.query.filter_by(id=self.brand_id).first()
            if not brands:
                return 'FAILED', bad_request_message(modul="link subs1", method="POST",
                    param="brand_id invalid:", not_found=self.brand_id, req=self.args)
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="link subs 1",
                method=self.method, error=e, req=str(self.args))

    def check_items(self):
        try:

            #check items exist or not:
            item = Item.query.filter_by(id=self.item_id).first()
            if not item:
                return 'FAILED', bad_request_message(modul="link subs2", method="POST",
                    param="item_id invalid:", not_found=self.item_id, req=self.args)
            else:
                if int(item.brand_id) != int(self.brand_id):
                    return 'FAILED', bad_request_message(modul="link subs2", method="POST",
                        param="brand_id not match with items data:", not_found=self.brand_id, req=self.args)

            self.item_data = item
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="link subs 2",
                method=self.method, error=e, req=str(self.args))

    def get_user_subscriptions(self):
        try:
            user_subs = UserSubscription.query.filter_by(user_id=self.user_id, brand_id=self.brand_id).order_by(desc(UserSubscription.valid_to)).first()
            if not user_subs:
                return 'FAILED', bad_request_message(modul="link subs3", method="POST",
                    param="Data Subscription not found", not_found="brand_id %s user_id : %s" % (
                        self.brand_id, self.user_id), req=self.args)
            else:
                if user_subs.quantity_unit == 1:

                    if int(user_subs.current_quantity) > 0:
                        self.valid_data = True
                    else:
                        return 'FAILED', bad_request_message(modul="link subs3", method="POST",
                            param= "Invalid current quantity, exceed quantity",
                            not_found="subs_id %s item_id : %s" % (user_subs.id, self.item_id),
                            req=self.args)

                if user_subs.quantity_unit in [2,3,4]:
                    if (self.item_data.release_date >= user_subs.valid_from and
                        self.item_data.release_date <= user_subs.valid_to):
                        self.valid_data = True
                    elif (user_subs.allow_backward is True and
                          self.item_data.release_date <= user_subs.valid_to):
                        self.valid_data = True
                    else:
                        return 'FAILED', bad_request_message(modul="link subs3", method="POST",
                            param= "Invalid Duration, Release date invalid for Subscription",
                            not_found="subs_id %s item_id : %s" % (user_subs.id, self.item_id),
                            req=self.args)

                if self.valid_data:

                    user_own = UserItem.query.filter(
                        UserItem.user_buffet_id == None).filter_by(
                        item_id=self.item_id, user_id=self.user_id).first()

                    if user_own:
                        pass
                    else:

                        try:
                            vendors = self.item_data.get_vendor()
                            vendor_id = vendors.get('id', 0)
                        except:
                            vendor_id = 0

                        user_item = UserItem(
                            user_id = self.user_id,
                            item_id = self.item_id,
                            edition_code = self.edition_code,
                            orderline_id = self.orderline_id,
                            is_active = self.is_active,
                            brand_id = self.item_data.brand_id,
                            itemtype_id = item_types.enum_to_id(self.item_data.item_type),
                            vendor_id = vendor_id
                        )
                        sv = user_item.save()

                        if sv.get('invalid', False):
                            return 'FAILED', internal_server_message(
                                modul="link subs 3", method="POST",
                                error=sv.get('message', ''), req=str(self.args))
                        else:
                            pass
                            # go = UserSubscriptionItem(
                            #     usersubscription_id = user_subs.id,
                            #     useritem_id = user_item.id,
                            #     item_id = self.item_id,
                            #     user_id = self.user_id,
                            #     note = 'LINK SUBS'
                            # )
                            # lo = go.save()
                            # if lo.get('invalid', False):
                            #     pass

                        if user_subs.quantity_unit == 1:
                            user_subs.current_quantity = user_subs.current_quantity - 1
                            user_subs.save()

            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="link subs 3",
                method=self.method, error=e, req=str(self.args))

    def json_success(self):
        return Response(json.dumps(self.args), status=httplib.CREATED, mimetype='application/json')

    def construct(self):

        chk_brand = self.check_brands()
        if chk_brand[0] != 'OK':
            return chk_brand[1]

        chk_items = self.check_items()
        if chk_items[0] != 'OK':
            return chk_items[1]

        chk_subs = self.get_user_subscriptions()
        if chk_subs[0] != 'OK':
            return chk_subs[1]

        return self.json_success()


class MigrateSubs():

    def __init__(self, args=None, module=None):
        self.args = args
        self.method = module

        self.user_id = args.get('user_id', None)
        self.brand = args.get('brand_id', None)
        self.old_id = args.get('old_id_library', None)
        self.subs_download = args.get('total_subs_downloaded', None)

        self.valid_from = args.get('valid_from', None)
        self.valid_to = args.get('valid_to', None)

        self.quantity = args.get('quantity', 0)
        self.quantity_unit = args.get('quantity_unit', 0)
        self.subs_id = args.get('id', 0)

        self.owned_items = args.get('owned_items', None)
        self.owned_ids = []

        self.success_subs = {}
        self.success_owned = {}
        self.data_owned_suc = []

    def check_brand(self):
        try:
            brand = Brand.query.filter_by(id=self.brand).first()
            if not brand:
                return 'FAILED', bad_request_message(modul="migrate subs",
                    method="POST", param="brand_id", not_found=self.brand, req=self.args)

            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="check brand FK",
                method=self.method, error=e, req=str(self.args))

    def check_date(self):
        try:
            valid_from = date_validator(self.valid_from, "custom")
            if valid_from.get('invalid', False):
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    error_code=httplib.BAD_REQUEST,
                    developer_message="Invalid valid from, format YYYY-MM-DD",
                    user_message="valid from invalid")
            self.valid_from = valid_from.get('message', '')

            valid_to = date_validator(self.valid_to, "custom")
            if valid_to.get('invalid', False):
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    error_code=httplib.BAD_REQUEST,
                    developer_message="Invalid valid to, format YYYY-MM-DD",
                    user_message="valid to invalid")
            self.valid_to = valid_to.get('message', '')

            if self.valid_to <= self.valid_from:
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    error_code=httplib.BAD_REQUEST,
                    developer_message="Invalid valid to must greater than valid from",
                    user_message="valid to invalid")

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="check date FK",
                method=self.method, error=e, req=str(self.args))

    def check_owned_items(self):
        try:
            for data in self.owned_items:
                data_id = data.get('id', None)
                item_id = data.get('item_id', None)

                #check if owned ids double
                if data_id in self.owned_ids:
                    return 'FAILED', err_response(status=httplib.CONFLICT,
                        error_code=httplib.CONFLICT,
                        developer_message="Conflict owned_ids :%s" % data_id,
                        user_message="Conflict owned id")
                else:
                    self.owned_ids.append(data_id)

                #check if owned ids already exist inside DB
                us_it = UserItem.query.filter_by(id=data_id).first()
                if us_it:
                    return 'FAILED', err_response(status=httplib.CONFLICT,
                        error_code=httplib.CONFLICT,
                        developer_message="owned id : %s already register in DB" % data_id,
                        user_message="Conflict owned id")

                #check item id
                items = Item.query.filter_by(id=item_id).first()
                if not items:
                    return 'FAILED', err_response(status=httplib.NOT_FOUND,
                        error_code=httplib.NOT_FOUND,
                        developer_message="item id : %s Not founds" % item_id,
                        user_message="Item id invalid")

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="check owned items FK",
                method=self.method, error=e, req=str(self.args))

    def check_subs_items(self):
        try:
            subs_items = UserSubscription.query.filter_by(id=self.subs_id).first()
            if subs_items:
                return 'FAILED', err_response(status=httplib.CONFLICT,
                    error_code=httplib.CONFLICT,
                    developer_message="Subs id : %s already register in DB" % self.subs_id,
                    user_message="Conflict subs id")
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="check_subs_items FK",
                method=self.method, error=e, req=str(self.args))

    def post_user_subscriptions(self):
        try:
            kwargs = {}
            for item in self.args:
                if self.args[item] is None:
                    pass
                else:
                    kwargs[item] = self.args[item]

            del kwargs['owned_items']

            m = UserSubscription(**kwargs)
            sv = m.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(
                    modul="post_user_subscriptions", method="POST",
                    error=sv.get('message', ''), req=str(self.args))

            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="post_user_subscriptions FK",
                method=self.method, error=e, req=str(self.args))

    def post_owned_items(self):
        try:
            moi = []
            for data in self.owned_items:
                temp = {}
                item_id = data.get('item_id', None)
                data['user_id'] = self.user_id

                #check if in owned items already exist
                own = UserItem.query.filter_by(item_id=item_id, user_id=self.user_id).first()
                if not own:
                    mo = UserItem(**data)
                    sv = mo.save()
                    if sv.get('invalid', False):
                        temp['status'] = 300
                        temp['message'] = 'FAILEDDD =================='
                        temp['item_id'] = item_id
                    else:
                        temp['status'] = httplib.CREATED
                        temp['message'] = 'user library: %s OK' % data.get('id', None)
                        temp['item_id'] = item_id

                        #create junk table owned --> subscriptions
                        # go = UserSubscriptionItem(
                        #     usersubscription_id = self.subs_id,
                        #     useritem_id = data.get('id', None),
                        #     item_id = item_id,
                        #     user_id = self.user_id,
                        #     note = 'MIGRATION DATA'
                        # )
                        # lo = go.save()
                        # if lo.get('invalid', False):
                        #     temp['status junk table'] = 'JUNK TABLE FAILEDDD =================='
                        # else:
                        #     temp['status junk table'] = 'OK'
                else:
                    temp['status'] = httplib.CONFLICT
                    temp['message'] = 'FAILEDDD : owned items already exist for user %s item_id %s' % (self.user_id, item_id)
                    temp['item_id'] = item_id
                    temp['status junk table'] = 'conflict owned items=================='

                moi.append(temp)

            self.data_owned_suc = moi
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="post_owned_items FK",
                method=self.method, error=e, req=str(self.args))

    def json_success(self):
        rv = {}
        rv.update(self.args)
        rv['owned_items'] = self.data_owned_suc

        return Response(json.dumps(rv), status=httplib.CREATED, mimetype='application/json')

    def construct(self):
        chk_date = self.check_date()
        if chk_date[0] != 'OK':
            return chk_date[1]

        chk_brand = self.check_brand()
        if chk_brand[0] != 'OK':
            return chk_brand[1]

        chk_owned_items = self.check_owned_items()
        if chk_owned_items[0] != 'OK':
            return chk_owned_items[1]

        chk_subs_items = self.check_subs_items()
        if chk_subs_items[0] != 'OK':
            return chk_subs_items[1]

        post_subs = self.post_user_subscriptions()
        if post_subs[0] != 'OK':
            return post_subs[1]

        post_owned = self.post_owned_items()
        if post_owned[0] != 'OK':
            return post_owned[1]

        return self.json_success()
