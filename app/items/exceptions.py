from __future__ import unicode_literals
import os

from flask import current_app, request
from app import app
from flask_appsfoundry.exceptions import Processing, ScoopApiException
from .choices import WebReaderTaskStatus

class BadItemTypeChangeException(Exception):
    pass


class SolrError(Exception):
    pass


class WebReaderFilesNotReady(Exception):
    pass


class DuplicateTaskError(Exception):
    """ Attempting to register a task that has already been registered.
    """
    pass


class NonExistentTaskError(KeyError):
    """ Attempting to perform an operation on task that doesn't exist.
    """
    pass


def get_completed_page_count(item):
    DIR = os.path.join(app.static_folder, 'item_content_files', 'web_reader',
                       str(item.brand_id), str(item.id))

    try:
        return len([name for name in os.listdir(DIR) if
               os.path.isfile(os.path.join(DIR, name))])
    except OSError:
        return "?"


def get_completed_thumbnail_count(item):
    DIR = os.path.join(app.static_folder, 'item_content_files', 'thumbnails',
                       str(item.brand_id), str(item.id))
    try:
        return len([name for name in os.listdir(DIR) if
               os.path.isfile(os.path.join(DIR, name))])
    except OSError:
        return "?"


class WebReaderFileProcessing(ScoopApiException):
    """ Specialized exception;  Raise when a page is requested from WebReader,
    but the WebReaderProcessingDaemon is still converting the PDF to JPG images.
    """
    @property
    def _lang_en(self):
        try:
            return 'en' in request.accept_languages
        except RuntimeError:
            return False

    @property
    def __message(self):
        return {
            WebReaderTaskStatus.pending:
                ('Queuing' if self._lang_en else
                 'Proses dalam antrian') + '; 1/5',
            WebReaderTaskStatus.downloading:
                ('Downloading' if self._lang_en else
                 'Mengunduh berkas konten') + '; 2/5',
            WebReaderTaskStatus.extracting_zip:
                ('Extracting/decrypting ZIP/PDF' if self._lang_en else
                 'Extracting/decrypting ZIP/PDF') + '; 3/5',
            WebReaderTaskStatus.creating_web_reader_images:
                ('Generating pages' if self._lang_en else
                 'Membuat halaman konten') + '; 4/5; ({complete}/{total})'.format(
                    complete=get_completed_page_count(self.item),
                    total=self.item.page_count
                ),
            WebReaderTaskStatus.creating_thumbnail_images:
                ('Generating thumbnails' if self._lang_en else
                 'Membuat thumbnails') + '; 5/5; ({complete}/{total})'.format(
                    complete=get_completed_thumbnail_count(self.item),
                    total=self.item.page_count
                ),
            WebReaderTaskStatus.complete:
                'Processing completed successfully!',  # this is never displayed

            WebReaderTaskStatus.failed:
                "ERROR: {message}".format(message=self.task.error_message)

        }[self.task.status]

    def __init__(self, task, item):
        """
        :param `app.items.web_reader.ProcessingTask` task:
            The task that raised the Error message.
        :return:
        """
        self.task = task
        self.item = item

        super(WebReaderFileProcessing, self).__init__(
            code=418,
            error_code=418,
            user_message=self.__message,
            developer_message=self.__message
        )
