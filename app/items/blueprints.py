from flask import Blueprint
import flask_restful as restful

from .api import ItemListApi, ItemApi, ItemLatest, ItemSearch, \
    ItemTypeAheadSuggestionApi, ItemEdition, ItemRecommendation, \
    ItemFilexi, PopularItemListApi, ItemFileListApi, ItemFileApi, ItemOwnedListApi, \
    ItemOwnedApi, ItemOwnedSubsApi, ItemOwnedSubsListApi, ItemSubsApi, \
    ItemSubsLinkApi, ItemDownloadApi, ItemContentFileDownloadApi, ItemDisctrictApi, ItemDisctrictListApi, \
    ItemBonusesApi, ItemBonusesDelivery, ItemContentFileUploadNotificationApi, \
    ItemBonusesListApi, ItemMigrationSC1, ItemMigrationPortal, \
    WebReaderPageApi, PageThumbnailApi, ItemCheckRelatedFilesApi, \
    WebReaderErrorReportApi, PreviewPageApi, WebReaderQueueStatus, WebReaderQueueExactStatus, ItemContentFileMetaApi, \
    AuthorListApi, AuthorApi, ItemReviewApiList, ItemReviewApi

items_blueprint = Blueprint('items', __name__, url_prefix='/v1/items')
items_api = restful.Api(items_blueprint)
items_api.add_resource(ItemListApi, '', endpoint='list')

items_api.add_resource(ItemApi, '/<int:item_id>', endpoint='details')

items_blueprint.add_url_rule(
    '/<int:db_id>/upload',
    view_func=ItemContentFileUploadNotificationApi.as_view('upload_content_file'))

items_blueprint.add_url_rule(
    '/<int:db_id>/download',
    view_func=ItemContentFileDownloadApi.as_view('download_content_file'))

items_blueprint.add_url_rule(
    '/<int:db_id>/download/meta',
    view_func=ItemContentFileMetaApi.as_view('content_file_meta'))

items_blueprint.add_url_rule(
    '/<int:db_id>/web-reader/<int:page_num>',
    view_func=WebReaderPageApi.as_view('web_reader_page'))

# old url for web reader - for backward compatibility with front-end
items_blueprint.add_url_rule(
    '/<int:db_id>/web-reader/<int:page_num>.jpg',
    view_func=WebReaderPageApi.as_view('web_reader_page_old'))

items_blueprint.add_url_rule(
    '/<int:db_id>/preview/<int:page_num>',
    view_func=PreviewPageApi.as_view('preview_page'))

# old url for preview - for backward compatibility with front-end
items_blueprint.add_url_rule(
    '/<int:db_id>/preview/<int:page_num>.jpg',
    view_func=PreviewPageApi.as_view('preview_page_old'))

items_blueprint.add_url_rule(
    '/<int:db_id>/web-reader/error-report',
    view_func=WebReaderErrorReportApi.as_view('web_reader_error_report')
)
items_blueprint.add_url_rule(
    '/web-reader/error-report',
    view_func=WebReaderErrorReportApi.as_view('web_reader_error_report_viewer')
)
items_blueprint.add_url_rule(
    '/web-reader/queue',
    view_func=WebReaderQueueStatus.as_view('web_reader_queue_status')
)
items_blueprint.add_url_rule(
    '/web-reader/queue/<status>',
    view_func=WebReaderQueueExactStatus.as_view('web_reader_queue_exact_status')
)


items_blueprint.add_url_rule(
    '/<int:db_id>/thumbnails/<int:page_num>.jpg',
    view_func=PageThumbnailApi.as_view('page_thumbnail'))

items_blueprint.add_url_rule(
    '/<int:item_id>/check-files',
    view_func=ItemCheckRelatedFilesApi.as_view('check_item_file_in_server'))


items_api.add_resource(ItemLatest, '/latest', endpoint='latest')
items_api.add_resource(ItemSearch, '/searches')

items_api.add_resource(ItemTypeAheadSuggestionApi, '/searches/suggestions/<suggest>', endpoint='suggestions')



items_api.add_resource(ItemEdition, '/edition_code/<edition_code>', endpoint='edition_code')
items_api.add_resource(ItemRecommendation, '/<int:item_id>/recommendations', endpoint='recommendations')
items_api.add_resource(ItemFilexi, '/<int:item_id>/files/file_order/<int:order_id>', endpoint='itemfile')
items_api.add_resource(PopularItemListApi, '/popular', endpoint='popular')
items_api.add_resource(ItemMigrationSC1, '/sc1-patch/<item_id>', endpoint='sc1_patch')
items_api.add_resource(ItemMigrationPortal, '/patch/<item_id>', endpoint='portal_patch')

items_api.add_resource(ItemReviewApiList, '/reviews', endpoint='review-list')
items_api.add_resource(ItemReviewApi, '/reviews/<int:review_id>', endpoint='review-details')


itemfiles_blueprint = Blueprint('itemfiles', __name__, url_prefix='/v1/itemfiles')
itemfiles_api = restful.Api(itemfiles_blueprint)
itemfiles_api.add_resource(ItemFileListApi, '')
itemfiles_api.add_resource(ItemFileApi, '/<int:item_id>', endpoint='itemfiles')

owneditems_blueprint = Blueprint('owned_items', __name__, url_prefix='/v1/owned_items')
owneditems_api = restful.Api(owneditems_blueprint)
owneditems_api.add_resource(ItemOwnedListApi, '')
owneditems_api.add_resource(ItemOwnedApi, '/<int:owned_id>', endpoint='owned_items')

ownedsubs_blueprint = Blueprint('owned_subscriptions', __name__, url_prefix='/v1/owned_subscriptions')
ownedsubs_api = restful.Api(ownedsubs_blueprint)
ownedsubs_api.add_resource(ItemOwnedSubsListApi, '')
ownedsubs_api.add_resource(ItemOwnedSubsApi, '/<int:owned_id>', endpoint='owned_subs')

itemsmisc_blueprint = Blueprint('items_misc', __name__, url_prefix='/v1')
itemsmisc_api = restful.Api(itemsmisc_blueprint)
itemsmisc_api.add_resource(ItemSubsApi, '/migrate_subs', endpoint='migrate_subs')
itemsmisc_api.add_resource(ItemSubsLinkApi, '/link_subs', endpoint='link_subs')
itemsmisc_api.add_resource(ItemDownloadApi, '/downloads', endpoint='downloads')

itemdisctrict_blueprint = Blueprint('items_district', __name__, url_prefix='/v1/item_distribution_country_groups')
itemdisctrict_api = restful.Api(itemdisctrict_blueprint)
itemdisctrict_api.add_resource(ItemDisctrictListApi, '')
itemdisctrict_api.add_resource(ItemDisctrictApi, '/<district_id>', endpoint='distribution_country')


item_bonuses_blueprint = Blueprint('item_bonuses', __name__, url_prefix='/v1/bonuses')
item_bonuses_api = restful.Api(item_bonuses_blueprint)
item_bonuses_api.add_resource(ItemBonusesListApi, '')
item_bonuses_api.add_resource(ItemBonusesApi, '/<gift_name>', endpoint='bonuses')
item_bonuses_api.add_resource(ItemBonusesDelivery, '/claim/<gift_name>', endpoint='claim_bonuses')

# NOTE:  These endpoints are depreciated!
items_v2_blueprint = Blueprint('items_v2', __name__, url_prefix='/v2/items')
items_v2_api = restful.Api(items_v2_blueprint)
items_v2_api.add_resource(ItemTypeAheadSuggestionApi, '/searches/suggestions')
items_v2_api.add_resource(ItemListApi, '/searches')


authors_blueprint = Blueprint('authors', __name__, url_prefix='/v1/authors')
authors_api = restful.Api(authors_blueprint)
authors_api.add_resource(AuthorListApi, '')
authors_api.add_resource(AuthorApi, '/<int:db_id>', endpoint='authors')
