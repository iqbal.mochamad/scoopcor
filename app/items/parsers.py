from flask_appsfoundry.parsers import (
    SqlAlchemyFilterParser, ParserBase, IntegerField, BooleanField,
    IntegerFilter, BooleanFilter, EnumFilter, StringField, EnumField,
    InputField, StringFilter, DateTimeFilter)

from app.utils.parser_fields import WildcardFilter, date_or_datetime_iso8601_string_to_date
from app.utils.parsers import (
    CountryIdFilter, LanguageIdFilter, scoopcor1_or_iso8601_datetime)
from app.utils.shims import countries, languages

from .choices import ITEM_TYPES, FILE_TYPES, STATUS_TYPES, READING_DIRECTIONS
from .choices.text_direction import LTR
from .models import PopularItem, Item


from .models import Author


class AuthorsListArgsParser(SqlAlchemyFilterParser):

    __model__ = Author

    name = StringFilter()
    is_active = BooleanFilter()
    sort_priority = IntegerFilter()
    slug = StringFilter()
    first_name = StringFilter()
    last_name = StringFilter()
    title = StringFilter()
    academic_title = StringFilter()
    birthdate = DateTimeFilter()
    meta = StringFilter()
    id = IntegerFilter()
    q = WildcardFilter(dest='name')
    modified = DateTimeFilter()


class AuthorsArgsParser(ParserBase):
    name = StringField(required=True)
    is_active = BooleanField(required=True)
    sort_priority = IntegerField(required=True)
    slug = StringField()
    first_name = StringField()
    last_name = StringField()
    title = StringField()
    academic_title = StringField()
    birthdate = InputField(type=date_or_datetime_iso8601_string_to_date)
    meta = StringField()
    profile_pic_url = StringField()


class ItemArgsParser(ParserBase):
    id = IntegerField(store_missing=False)
    name = StringField(required=True)
    slug = StringField(required=True)
    edition_code = StringField(required=True)
    content_type = EnumField(FILE_TYPES, required=True)
    sort_priority = IntegerField(required=True)
    item_status = EnumField(STATUS_TYPES, required=True)
    release_date = InputField(type=scoopcor1_or_iso8601_datetime, required=True)
    parental_control_id = IntegerField(dest='parentalcontrol_id', required=True)
    item_type_id = EnumField(ITEM_TYPES, dest='item_type', required=True)
    brand_id = IntegerField(required=True)

    is_featured = BooleanField()
    is_extra = BooleanField()
    is_active = BooleanField()
    is_internal_content = BooleanField()

    authors = IntegerField(action='append', default=[])
    categories = IntegerField(action='append', default=[])

    languages = InputField(
        type=lambda e: languages.id_to_iso639_alpha3(int(e)),
        action='append',
        default=[])
    countries = InputField(
        type=lambda e: countries.id_to_iso3166(int(e)),
        action='append',
        default=[])

    thumb_image_normal = StringField()
    thumb_image_highres = StringField()
    issue_number = StringField()
    revision_number = StringField()

    extra_items = StringField(action='append')
    subs_weight = IntegerField()

    image_normal = StringField()
    image_highres = StringField()

    description = StringField()
    meta = StringField()

    # TODO: should be validating these or at least stripping invalid chars.
    # these should also be unique, as well..
    gtin13 = StringField()
    gtin14 = StringField()
    gtin8 = StringField()

    filenames = StringField(action='append')
    # previews = StringField(action='append')

    reading_direction = EnumField(READING_DIRECTIONS,
                                  default=READING_DIRECTIONS[LTR])

    item_distribution_country_group_id = IntegerField()

    release_schedule = InputField(type=scoopcor1_or_iso8601_datetime)

    printed_price = StringField()
    printed_currency_code = StringField()
    vendor_product_id_print = StringField()


class ItemDistributionArgsParser(ParserBase):
    name = StringField(required=True)
    group_type = IntegerField(required=True)
    #  NOTE: these are being passed in as ISO codes.
    # Create a speciality parser for these to validate.
    countries = StringField(action='append', required=True)
    vendor_id = IntegerField(required=True)
    is_active = BooleanField(default=True)


class PopularItemArgsParser(SqlAlchemyFilterParser):

    __model__ = PopularItem

    item_type_id = EnumFilter(enum=ITEM_TYPES, dest='item_type')
    item_status = EnumFilter(enum=STATUS_TYPES)
    is_active = BooleanFilter()
    is_latest = BooleanFilter()
    vendor_id = IntegerFilter()
    category_id = IntegerFilter(default_operator='contains')
    # display_offer_plid = IntegerFilter(
    #     dest='non_free_platforms', default_operator='contains')
    country_id = CountryIdFilter(dest='countries')
    language_id = LanguageIdFilter(dest='languages')
    is_buffet = BooleanFilter()


class ItemPdfContentUploadParser(ParserBase):
    file_name = StringField(required=True)
    file_dir = StringField(required=True)
    file_size = IntegerField(required=True)
    pic_id = IntegerField(required=True)
    pic_name = StringField(required=True)
    role_id = IntegerField(required=True)


class ItemEpubContentUploadParser(ParserBase):
    file_name = StringField(required=True)
    file_dir = StringField(required=True)
    file_size = IntegerField(required=True)
    pic_id = IntegerField(required=True)
    pic_name = StringField(required=True)
    role_id = IntegerField(required=True)


class WebReaderErrorReportParser(ParserBase):
    message = StringField()
    pages = IntegerField(action='append', default=[])


def list_of_item_to_upload(input):
    return {
        'id': input['id'],
    }


class FileItemUploadToServerParser(ParserBase):
    items = InputField(type=list_of_item_to_upload, required=True, action='append')
