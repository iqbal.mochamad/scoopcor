"""
Mobile Bundle Classes
=====================

Handles building or extracting the mobile bundle .zip files.
"""
from __future__ import unicode_literals

import hashlib
import shutil
from tempfile import mkdtemp

import os
from PyPDF2 import PdfFileReader, PdfFileWriter
from flask import current_app
from subprocess32 import check_call

from app.helpers import create_dir_if_not_exists
from app.items import _log
from app.items.files.locations import mobile_bundle_path


class PdfDecryptionError(Exception):
    def __init__(self, message=None, inner_exception=None):
        self.inner_exception = inner_exception
        self.message = message or inner_exception.message or "Failed to decrypt"


class ExtractionTempFileHandler(object):
    """ Context manager for working with Mobile Bundles.
    """

    TEMP_DIR_PREFIX = "SC_WEBREADER_EXTRACT_"

    def __init__(self, item, static_folder):
        """
        :param `app.items.models.Item` item:
        :param `six.string_types` static_folder:
        """
        self.item = item
        self.static_folder = static_folder
        self.temp_dir = None

    @property
    def mobile_bundle_path(self):
        """ full path to the item's encrypted ZIP that's served to clients.
        """
        return mobile_bundle_path(item=self.item, local_static_folder=self.static_folder)

    @property
    def temporary_mobile_bundle_path(self):
        return os.path.join(self.temp_dir, "{}.zip".format(self.item.edition_code))

    def copy_mobile_bundle_to_temp(self):
        shutil.copyfile(
            self.mobile_bundle_path, self.temporary_mobile_bundle_path)

    def __enter__(self):
        self.temp_dir = mkdtemp(prefix="SC_WEB_READER_EXTRACT")
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        try:
            shutil.rmtree(os.path.dirname(self.temporary_mobile_bundle_path))
        except Exception:
            _log.exception("Failed cleaning up temp dir")


class MobileBundleExtractor(object):
    """ Extracts the original, unencrypted PDF from a zipped original.
    """
    ZIP_SALT = "!@#$Gnjiolkuy44567890-yHUIkjhtrfgHY&0pl/09876`wdfBNJK"
    PDF_SALT = "pou56^YHNyfvbn@#$%^YJM./-[)12efvb%tgHJkL@#RghjI()P:?!][98yh"

    def __init__(self, item, static_folder, temp_handler=None):
        """

        :param `app.items.models.Item` item:
        :param `ExtractionTempFileHandler` temp_handler:
        :return:
        """
        self.item = item
        self.brand_id_for_password = item.brand_id
        self.static_folder = static_folder
        self.temp_handler = (
            temp_handler or ExtractionTempFileHandler(item, static_folder))

    def zip_password(self, brand_id):
        """ SC1 legacy code.. literally, I have no idea.
        """
        pass_zip = "%s%s%s%s%s%s%s" % (
            self.item.id,
            '~#%&',
            self.ZIP_SALT,
            '*&#@!',
            brand_id,
            '&%^)',
            self.item.files[0].key)
        return hashlib.sha256(pass_zip).hexdigest()

    def extract_zip(self, output_dir, input_file):
        if not self.item.files:
            raise MobileBundleItemFilesNotDefined(
                'Item files data is empty for item {}. Check core_itemfiles'.format(
                    self.item.id)
            )
        try:
            check_call([
                '7za',
                'e',
                '-p{}'.format(self.zip_password(self.item.brand_id)),
                '-o{}'.format(output_dir),
                input_file, '-aoa'])
        except Exception as e:
            # try again with alternative password from brand_id in file path
            file_name = self.item.files[0].file_name if self.item.files else None
            path_brand_id = file_name.split('/')[2] if file_name and len(file_name.split('/')) > 1 else ''
            if file_name and path_brand_id and path_brand_id != self.brand_id_for_password:
                # try again with new brand id as password
                try:
                    check_call([
                        '7za',
                        'e',
                        '-p{}'.format(self.zip_password(path_brand_id)),
                        '-o{}'.format(output_dir),
                        input_file, '-aoa'])
                    # if it's valid, set current brand_id from path as brand id for password (later used to extract pdf)
                    self.brand_id_for_password = path_brand_id
                except Exception as e:
                    raise MobileBundleZipExtractError('Failed to extract zip (possibly password not match). '
                                                      'Error: {}'.format(e.message))
            else:
                raise MobileBundleZipExtractError('Failed to extract zip (possibly password not match). '
                                                  'Error: {}'.format(e.message))

    @property
    def pdf_password(self):
        pass_pdf = "%s%s%s%s%s%s%s" % (
            self.item.id,
            '~#%&',
            self.PDF_SALT,
            '*&#@!',
            self.brand_id_for_password,
            '&%^)',
            self.item.files[0].key)
        return hashlib.sha256(pass_pdf).hexdigest()

    def decrypt_pdf(self, pdf_base_dir):
        encrypted_pdf_path = os.path.join(pdf_base_dir, 'magazine.pdf')
        pdf_reader = PdfFileReader(file(encrypted_pdf_path, "rb"))

        # https://pythonhosted.org/PyPDF2/PdfFileReader.html#PyPDF2.PdfFileReader.decrypt
        # has bizarre exit statuses..
        exit_status = pdf_reader.decrypt(self.pdf_password)
        if exit_status not in [1, 2]:
            raise PdfDecryptionError(message="exit status: {}".format(exit_status))

        return pdf_reader

    def write_decrypted_pdf_to_disk(self, pdf_reader):
        """

        :param `PyPDF2.PdfFileReader` pdf_reader:
        :return:
        """
        pdf_writer = PdfFileWriter()
        page_data = range(0, int(pdf_reader.numPages))
        for page in page_data:
            pdf_writer.addPage(pdf_reader.getPage(page))

        output_path = os.path.join(self.static_folder,
                                   #'item_content_files',
                                   #'original',
                                   'original_local',
                                   str(self.item.brand_id),
                                   '{}.pdf'.format(self.item.edition_code))
        create_dir_if_not_exists(output_path)

        outputStream = file(output_path, "wb")
        pdf_writer.write(outputStream)
        outputStream.close()
        return output_path

    def extract_original(self):

        with self.temp_handler as tmp:

            tmp.copy_mobile_bundle_to_temp()

            self.extract_zip(
                output_dir=tmp.temp_dir,
                input_file=tmp.temporary_mobile_bundle_path
            )

            try:
                pdf = self.decrypt_pdf(tmp.temp_dir)
                output_path = self.write_decrypted_pdf_to_disk(pdf)
            except Exception as e:
                output_path = self.decrypt_with_qpdf(
                    prev_err_message=e.message,
                    pdf_base_dir=tmp.temp_dir,
                )

        return output_path

    def decrypt_with_qpdf(self, prev_err_message, pdf_base_dir):
        output_path = os.path.join(
            self.static_folder,
            #'item_content_files',
            #'original',
            'original_local',
            str(self.item.brand_id),
            '{}.pdf'.format(self.item.edition_code))
        create_dir_if_not_exists(output_path)

        try:
            check_call([
                'qpdf',
                '--password={password}'.format(password=self.pdf_password),
                '--decrypt',
                '{temp_dir}'.format(temp_dir=os.path.join(pdf_base_dir, 'magazine.pdf')),
                '{output_path}'.format(output_path=output_path)
            ])
        except Exception as e:
            raise PdfDecryptionError(
                message="Failed to decrypt pdf for item {item_id}. "
                        "QPDF failed and pypdf2 error: {error_message}".format(
                            item_id=self.item.id,
                            error_message=e.message))

        if not os.path.isfile(output_path):
            raise PdfDecryptionError(
                message="Failed to decrypt pdf for item {item_id}. "
                        "QPDF failed and pypdf2 error: {error_message}".format(
                            item_id=self.item.id,
                            error_message=prev_err_message))

        return output_path


def mobile_bundle_factory(bundle_class, item, flask_app=None):

    flask_app = flask_app or current_app

    extractor = bundle_class(
        item=item,
        static_folder=flask_app.static_folder)

    return extractor


class MobileBundleBuilder(object):
    pass


class MobileBundleZipExtractError(Exception):
    pass


class MobileBundleItemFilesNotDefined(Exception):
    pass

