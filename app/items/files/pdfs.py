"""
PDF Utilities
=============

For each of our PDFs, we extract a small thumbnail for each page,
which is displayed to the user within the frontend applications.
"""
from __future__ import unicode_literals

import os

from flask import current_app
from subprocess32 import check_call

from app.items.files.locations import pdf_original_path
from app.items.web_reader import get_web_reader_content_pages_directory
from app.items.web_reader.helpers import get_thumbnails_directory
from app.uploads.pdf.utils import PdfMetadata


class PdfThumbnailExtractor(object):
    """ Generates a single PNG thumbnail for a single page within a PDF.
    """

    def __init__(self, input_pdf_path, output_base_dir, page_num):
        """
        :param `str` input_pdf_path:
            The PDF to use when generating the thumbnails.
        :param `str` output_base_dir:
            The full filesystem directory path where the thumbnail will be
            stored. This folder will be created, if it doesn't exist.
        """
        self.input_pdf_path = input_pdf_path
        self.output_base_dir = output_base_dir
        self.page_num = page_num

    pixel_density = 24

    @property
    def output_file_full_path(self):
        """ Gets the location where the new thumbnail will be saved.

        :return: The full filesystem path where the thumbnail will be saved.
        :rtype: str
        """
        return os.path.join(
            self.output_base_dir,
            "{}.jpg".format(self.page_num + 1))  # HACK -> input is a page 'index'

    @property
    def processing_command(self):
        """ The command that should be executed when *process* is called.

        :return: A single command that may be passed as an argument to
            :func:`subprocess.check_call`
        """
        return ('convert -define pdf:use-cropbox=true '
                '-density {self.pixel_density} '
                '\"{self.input_pdf_path}\"[{self.page_num}] '
                '-background white -flatten '
                '{self.output_file_full_path}').format(self=self)

    def process(self):
        """ Executes the shell job.

        :return: The full path for the extracted thumbnail.
        :rtype: `str`
        """
        if not os.path.exists(os.path.dirname(self.output_file_full_path)):
            os.makedirs(os.path.dirname(self.output_file_full_path))

        # remove previous image files if exist (in case of reprocessing)
        remove_file(self.output_file_full_path)

        check_call(self.processing_command, shell=True)

        return self.output_file_full_path


class PdfBatchThumbnailExtractor(object):
    """ Generates a thumbnail for every single page of a given PDF.
    """

    def __init__(self, input_pdf_path, output_base_dir):
        """
        :param `str` input_pdf_path:
            The PDF to use when generating the thumbnails.
        :param `str` output_base_dir:
            The full filesystem path where the thumbnails will be stored.
            This folder will be created, if it doesn't exist.
        """
        self.input_pdf_path = input_pdf_path
        self.output_base_dir = output_base_dir
        self.pdf_meta = PdfMetadata(self.input_pdf_path)

    def process(self):
        """ Executes shell job for every page in the input_pdf.

        :return: A list of the full path for all extracted thumbnails.
        :rtype: list
        """
        all_thumbnail_paths = []
        for page_num in range(0, self.pdf_meta.page_count):
            extractor = PdfThumbnailExtractor(self.input_pdf_path,
                                              self.output_base_dir,
                                              page_num)
            thumbnail_file_path = extractor.process()
            all_thumbnail_paths.append(thumbnail_file_path)
        return all_thumbnail_paths


class WebReaderPageExtractor(PdfThumbnailExtractor):
    pixel_density = 112
    filter = 'Cubic'

    @property
    def output_file_full_path(self):
        """ Gets the location where the new thumbnail will be saved.

        :return: The full filesystem path where the thumbnail will be saved.
        :rtype: str
        """
        return os.path.join(
            self.output_base_dir,
            "{}.jpg".format(self.page_num + 1))

    @property
    def processing_command(self):
        """ The command that should be executed when *process* is called.

        :return: A single command that may be passed as an argument to
            :func:`subprocess.check_call`
        """
        return ('convert -define pdf:use-cropbox=true '
                '-density {self.pixel_density} '
                '-filter {self.filter} '
                '-quality 60 '
                '-background white -flatten '
                '\"{self.input_pdf_path}\"[{self.page_num}] '
                '{self.output_file_full_path}').format(self=self)


class WebReaderPageExtractorHighRes(PdfThumbnailExtractor):
    pixel_density = 150

    @property
    def output_file_full_path_before_pngquant(self):
        """ Gets the location where the new PNG file will be saved
        (this is before further compress with PngQuant).

        :return: The full filesystem path where the thumbnail will be saved.
        :rtype: str
        """
        return os.path.join(
            self.output_base_dir,
            "{}_beforepngquant.png".format(self.page_num + 1))

    @property
    def output_file_full_path(self):
        """ Gets the location where the new thumbnail will be saved.

        :return: The full filesystem path where the thumbnail will be saved.
        :rtype: str
        """
        return os.path.join(
            self.output_base_dir,
            "{file_name}.png".format(
                file_name=self.page_num + 1))

    @property
    def output_file_full_path_jpg(self):
        """ Gets the location where the new thumbnail will be saved.

        :return: The full filesystem path where the thumbnail will be saved.
        :rtype: str
        """
        return os.path.join(
            self.output_base_dir,
            "{file_name}.jpg".format(
                file_name=self.page_num + 1))

    @property
    def processing_command(self):
        """ The command that should be executed when *process* is called.
        Convert single page of a pdf to a JPEG

        :return: A single command that may be passed as an argument to
            :func:`subprocess.check_call`
        """
        return ('convert -define pdf:use-cropbox=true '
                '-density 140 -filter Cubic -quality 75 '
                '-background white -flatten '
                '\"{self.input_pdf_path}\"[{self.page_num}] '
                '{self.output_file_full_path_jpg}').format(self=self)

    @property
    def processing_command_to_png(self):
        """ The command that should be executed when *process* is called.
        Convert single page of a pdf to a PNG

        :return: A single command that may be passed as an argument to
            :func:`subprocess.check_call`
        """
        return ('convert -define pdf:use-cropbox=true '
                '-density {self.pixel_density} '
                '-depth 8 -alpha on '
                '-background white -flatten '
                '\"{self.input_pdf_path}\"[{self.page_num}] '
                '{self.output_file_full_path_before_pngquant}').format(self=self)

    @property
    def processing_command_compress_with_pngquant(self):
        """ The command that should be executed when *process* is called.
        Compress PNG file with pngquant

        :return: A single command that may be passed as an argument to
            :func:`subprocess.check_call`
        """
        return ('pngquant '
                '{self.output_file_full_path_before_pngquant} '
                '--output {self.output_file_full_path}').format(self=self)

    def process(self):
        """ Executes the shell job.

        :return: The full path for the extracted thumbnail.
        :rtype: `str`
        """
        if not os.path.exists(os.path.dirname(self.output_file_full_path)):
            os.makedirs(os.path.dirname(self.output_file_full_path))

        # remove previous jpg files if exist (in case of reprocessing)
        remove_file(self.output_file_full_path_jpg)

        # Process pdf to high res JPEG
        check_call(self.processing_command, shell=True)

        return self.output_file_full_path_jpg

    def process_pdf_to_png(self):
        # remove previous png files if exist (in case of reprocessing)
        remove_file(self.output_file_full_path_before_pngquant)
        remove_file(self.output_file_full_path)

        check_call(self.processing_command_to_png, shell=True)
        check_call(self.processing_command_compress_with_pngquant, shell=True)

        # remove temp png file
        remove_file(self.output_file_full_path_before_pngquant)


class WebReaderBatchExtractor(object):
    """ Generates a thumbnail for every single page of a given PDF.
    """

    def __init__(self, input_pdf_path, output_base_dir, high_res=False):
        """
        :param `str` input_pdf_path:
            The PDF to use when generating the thumbnails.
        :param `str` output_base_dir:
            The full filesystem path where the thumbnails will be stored.
            This folder will be created, if it doesn't exist.
        """
        self.input_pdf_path = input_pdf_path
        self.output_base_dir = output_base_dir
        self.pdf_meta = PdfMetadata(self.input_pdf_path)
        self.high_res = high_res

    def process(self):
        """ Executes shell job for every page in the input_pdf.

        :return: A list of the full path for all extracted thumbnails.
        :rtype: list
        """
        all_thumbnail_paths = []
        for page_num in range(0, self.pdf_meta.page_count):
            if self.high_res:
                extractor = WebReaderPageExtractorHighRes(
                    self.input_pdf_path,
                    self.output_base_dir,
                    page_num)
            else:
                extractor = WebReaderPageExtractor(
                    self.input_pdf_path,
                    self.output_base_dir,
                    page_num)
            thumbnail_file_path = extractor.process()
            all_thumbnail_paths.append(thumbnail_file_path)
        return all_thumbnail_paths


def thumbnail_batch_extractor_factory(item, local_static_folder=None):
    if local_static_folder is None:
        local_static_folder = current_app.static_folder

    extractor = PdfBatchThumbnailExtractor(
        pdf_original_path(item, local_static_folder),
        get_thumbnails_directory(item)
    )
    return extractor


def web_reader_batch_extractor_factory(item, local_static_folder=None, high_res=False):
    if local_static_folder is None:
        local_static_folder = current_app.static_folder

    extractor = WebReaderBatchExtractor(
        pdf_original_path(item, local_static_folder),
        get_web_reader_content_pages_directory(item_id=item.id, brand_id=item.brand_id),
        high_res=high_res)
    return extractor


def remove_file(file_path):
    # remove file and ignore if it not exists
    try:
        os.remove(file_path)
    except Exception:
        pass
