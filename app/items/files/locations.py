from __future__ import unicode_literals
import os

from six import text_type


def pdf_original_path(item, local_static_folder):
    return os.path.join(
        local_static_folder,
        # 'item_content_files',
        # 'original',
        # 'original_local',
        text_type(item.brand_id),
        '{}.pdf'.format(item.edition_code)
    )


def has_original_locally(item, local_static_folder):
    """ Do we have the original, unencrypted PDF locally?

    :param `app.items.models.Item` item:
    :param `six.string_types` local_static_folder:
    :return:
    """
    try:
        file_path = pdf_original_path(item, local_static_folder)
        file_exists = os.path.exists(file_path)
        if file_exists:
            # validate file size and permission
            file_size = os.path.getsize(file_path)
            if file_size == 0:
                file_exists = False
                # delete file with zero size (invalid)
                os.remove(file_path)

        if file_exists:
            return True

    except OSError as e:
        # file inaccessible, or other OSError:
        raise OSErrorFailedToReadFiles(
            message='{error}. File: {file_path}'.format(
                error=e.message,
                file_path=file_path,
            ))
    except:
        raise


class OSErrorFailedToReadFiles(OSError):
    def __init__(self, message):
        self.message = message


class OSErrorFileNotFound(OSError):
    def __init__(self, message):
        self.message = message


def mobile_bundle_path(item, local_static_folder):
    """full path to the item's encrypted ZIP that's served to clients.
    """
    path = os.path.join(
        local_static_folder,
        'item_content_files',
        'processed',
        str(item.brand_id),
        '{edition_code}.zip'.format(**vars(item))
    )
    if os.path.exists(path):
        return path
    else:
        # if not exists in path with default brand + edition code, search in item files
        if item.files:
            for f in item.files:
                path = os.path.join(
                    local_static_folder,
                    'item_content_files',
                    'processed',
                    f.file_name.replace('images/1/', '')
                )
                if os.path.exists(path):
                    return path

        # if still not exists, last, check by item.filenames
        for filename in item.filenames:
            path = os.path.join(
                local_static_folder,
                'item_content_files',
                'processed',
                filename.replace('images/1/', '')
            )
            if os.path.exists(path):
                return path

    # if all still not exists
    raise OSErrorFileNotFound('File zip for item {item.id} not found, last path checked: {path}'.format(
        item=item, path=path))


def has_mobile_bundle_locally(item, local_static_folder):
    """ Do we have the zip file locally?

    :param `app.items.models.Item` item:
    :param `six.string_types` local_static_folder:
    :return:
    """
    try:
        path = mobile_bundle_path(item, local_static_folder)
        if path:
            return True
    except OSErrorFileNotFound as e:
        return False
