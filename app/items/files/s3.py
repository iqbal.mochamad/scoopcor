from __future__ import unicode_literals
import os
from boto3.s3.transfer import S3Transfer, boto3

from flask import current_app
from singledispatch import singledispatch
from six import text_type

from app.helpers import create_dir_if_not_exists

__all__ = ('S3Uploader', 'S3Downloader', 's3_transfer_factory', 's3_downloader_factory')


class S3Base(object):

    BUCKET_NAME = 'item-file-archives'

    @property
    def local_bundle_path(self):
        return os.path.join(
            self.local_static_folder,
            'item_content_files',
            'processed',
            text_type(self.item.brand_id),
            '{0}.zip'.format(self.item.edition_code))

    @property
    def remote_bundle_path(self):
        return 'images/1/{item.brand_id}/{item.edition_code}.zip'.format(
            item=self.item)

    def __init__(self, item, s3, local_static_folder):
        """
        :param `app.items.models.Item` item:
        :param `boto3.s3.transfer.S3Transfer` s3:
        :param `six.string_types` local_static_folder:
        """
        self.item = item
        self.s3 = s3
        self.local_static_folder = local_static_folder


class S3Uploader(S3Base):

    def upload_mobile_bundle(self):
        pass


class S3Downloader(S3Base):

    def download_mobile_bundle(self):

        create_dir_if_not_exists(self.local_bundle_path)

        self.s3.download_file(
            self.BUCKET_NAME,
            self.remote_bundle_path,
            self.local_bundle_path
        )

        return self.local_bundle_path


@singledispatch
def s3_transfer_factory(s3_class, item, flask_app=None):
    pass


@s3_transfer_factory.register(S3Uploader)
def s3_uploader_factory(s3_class, item, flask_app=None):
    raise NotImplementedError


@s3_transfer_factory.register(S3Downloader)
def s3_downloader_factory(s3_class, item, flask_app=None):

    flask_app = flask_app or current_app

    transfer = S3Transfer(
        client=boto3.client(
            's3',
            flask_app.config['BOTO3_AWS_REGION'],
            aws_access_key_id=flask_app.config['BOTO3_AWS_APP_ID'],
            aws_secret_access_key=flask_app.config['BOTO3_AWS_APP_SECRET']
        )
    )

    return s3_class(
        item=item,
        s3=transfer,
        local_static_folder=flask_app.static_folder
    )
