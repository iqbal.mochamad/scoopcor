from app.items.models import Item, DistributionCountryGroup
from app.offers.models import Offer, OfferPlatform
from app.master.models import Brand, Vendor

from app.utils.shims import item_types

from sqlalchemy.sql import and_, or_, desc

def harper_collin_patch():

    """
        For patching data harper collin, set restricted country.
    """

    brands = Brand.query.filter_by(vendor_id=467).all()

    for br in brands:
        items = Item.query.filter_by(brand_id=br.id).order_by(desc(Item.release_date)).all()

        for data in items:
            sv = ItemDistributionCountry.query.filter_by(item_id=data.id).first()

            if sv:
                pass
            else:
                #['AU','CA','GB','US','NZ','ZA','IN']
                #australia, canada, united kingdom, united stated, new zealand, south afrika, india

                mo = ItemDistributionCountry(
                    item_id = data.id,
                    restricted_countries = ['AU','CA','GB','US','NZ','ZA','IN'],
                    is_active = True
                )
                mo.save()
                print 'Patch complete -- item_id : %s' % (data.id)

def reset_offers(offers_datas=None):
    try:

        if offers_datas:
            offer_data = Offer.query.filter(Offer.id.in_(offers_datas))
        else:
            offer_data = Offer.query.filter_by(is_discount=True).order_by(desc(Offer.id)).all()

        for i in offer_data:
            if i.discount_id:
                discounts = list(set(i.discount_id))
                i.discount_id = discounts
                i.save()
                print "clear done offer_id : %s discount_id : %s" % (i.id, discounts)

            offer_platform = OfferPlatform.query.filter_by(offer_id=i.id).all()
            for j in offer_platform:
                if j.discount_id:
                    discount_pl = list(set(j.discount_id))
                    j.discount_id = discount_pl
                    j.save()
                    print "clear OFFER PLATFORM done offers : %s discount_id : %s" % (i.id, discount_pl)

    except Exception, e:
        print "ERORR", e


class PatchDefaultBrand():

    """
        This is for patch data brands all defaults brands

        from app.items.patch import PatchDefaultBrand
        PatchDefaultBrand(limit=100, offset=0).construct()

    """

    def __init__(self, brand_id=None, limit=None, offset=None):
        self.brand_id = brand_id
        self.limit = limit
        self.offset = offset
        self.master_brands = None

        self.list_countries = []
        self.list_languages = []
        self.list_categories = []
        self.list_authors = []
        self.list_distributrions = []
        self.list_parentalcontrol = []
        self.list_itemtype = []

        self.records = 0

    def master(self):
        if self.brand_id:
            brands = Brand.query.filter_by(id=self.brand_id).all()
        else:
            brands = Brand.query.order_by(desc(Brand.id)).limit(self.limit).offset(self.offset).all()

        return brands

    def update_countries(self, brands=None, item_data=None):
        try:
            self.list_countries = []

            for item in item_data:
                # gather all languages from items
                self.list_countries = self.list_countries + item.countries

            # exclude multiple languages list(set())
            print "Update brand default countries", list(set(
                brands.default_countries + self.list_countries))

            brands.default_countries = list(set(brands.default_countries + self.list_countries))
            brands.save()
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', e

    def update_languages(self, brands=None, item_data=None):
        try:
            self.list_languages = []

            for item in item_data:
                # gather all languages from items
                self.list_languages = self.list_languages + item.languages

            # exclude multiple languages list(set())
            print "Update brand default languages", list(set(
                brands.default_languages + self.list_languages))

            brands.default_languages = list(set(brands.default_languages + self.list_languages))
            brands.save()
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', e

    def update_categories(self, brands=None, item_data=None):
        try:
            self.list_categories = []

            #validate not double
            brand_categories_def = [ i.id for i in brands.default_categories]

            for item in item_data:
                item_categories = [i for i in item.categories]
                self.list_categories = self.list_categories + item_categories

            next_categories = list(set(self.list_categories))
            for br_categories in next_categories:

                if br_categories.id not in brand_categories_def:
                    print "Update brand : %s with categories %s" % (brands.id, br_categories.name)

                    brands.default_categories.append(br_categories)
                    brands.save()

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', e

    def update_authors(self, brands=None, item_data=None):
        try:
            self.list_authors = []

            #validate not double
            brand_authors_def = [ i.id for i in brands.default_author]

            for item in item_data:
                item_authors = [i for i in item.authors]
                self.list_authors = self.list_authors + item_authors

            next_authors = list(set(self.list_authors))
            for br_authors in next_authors:

                if br_authors.id not in brand_authors_def:
                    print "Update brand : %s with authors %s" % (brands.id, br_authors.name)

                    brands.default_author.append(br_authors)
                    brands.save()

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', e

    def update_distributions(self, brands=None, item_data=None):
        try:
            self.list_distributrions = []

            #validate not double
            brand_distributions_def = [ i.id for i in brands.default_distributions]

            for item in item_data:
                self.list_distributrions.append(item.item_distribution_country_group)

            next_distributions = list(set(self.list_distributrions))
            for br_distributions in next_distributions:
                if br_distributions:
                    print "Update brand : %s with distributions %s" % (brands.id, br_distributions.name)
                    brands.default_distributions.append(br_distributions)
                    brands.save()

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', e

    def update_parentalcontrol(self, brands=None, item_data=None):
        try:
            if item_data:
                #for parental control, get 1 sample data item
                item = item_data[0]

                print "Update brand : %s with parentalcontrol %s" % (brands.id, item.parentalcontrol.id)
                brands.default_parentalcontrol_id = item.parentalcontrol_id
                brands.default_parentalcontrol = item.parentalcontrol.id
                brands.save()

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', e

    def update_itemtype(self, brands=None, item_data=None):
        try:
            if item_data:
                #for item type, get 1 item data sample
                item = item_data[0]

                brands.default_item_type = item.item_type
                brands.save()

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', e


    def construct(self):

        self.master_brands = self.master()
        data_all = len(self.master_brands)

        for brands in self.master_brands:
            print " %s " % brands.name.upper()
            print "============================================================="
            print "===== start update brands records %s of %s" % (self.records, data_all)
            self.records = self.records + 1
            items = Item.query.filter_by(brand_id=brands.id).order_by(desc(Item.id)).limit(100).all()

            ## update_countries
            countries = self.update_countries(brands, items)
            if countries[0] != 'OK':
                return countries[1]

            # update_languages
            languages = self.update_languages(brands, items)
            if languages[0] != 'OK':
                return languages[1]

            # update_categories
            categories = self.update_categories(brands, items)
            if categories[0] != 'OK':
                return categories[1]

            # update_authors
            authors = self.update_authors(brands, items)
            if authors[0] != 'OK':
                return authors[1]

            # update_distributions
            distributions = self.update_distributions(brands, items)
            if distributions[0] != 'OK':
                return distributions[1]

            ## update_parental
            #parental = self.update_parentalcontrol(brands, items)
            #if parental[0] != 'OK':
            #    return parental[1]

            # update item type
            itemtype = self.update_itemtype(brands, items)
            if itemtype[0] != 'OK':
                return itemtype[1]

            print "============================================================="

        print 'Update FIN...'
