from __future__ import absolute_import, unicode_literals
from enum import Enum

from app import db
from app.utils.models import pg_enum_column


class PartyType(Enum):
    organization = 'organization'
    user = 'user'
    vendor = 'vendor'
    partner = 'partner'
    payment_gateway = 'payment gateway'
    shared_library = 'shared library'


class Party(db.Model):
    """ A main class for Organization and User (and other related entity)
    """
    __tablename__ = 'parties'
    id = db.Column(db.Integer, primary_key=True)
    party_type = pg_enum_column(PartyType, nullable=False)

    __mapper_args__ = {
        'polymorphic_on': party_type,
        'polymorphic_identity': None,
        'with_polymorphic': '*'
    }
