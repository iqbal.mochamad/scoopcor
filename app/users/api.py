from httplib import CREATED, NO_CONTENT, OK, CONFLICT

from flask import jsonify, make_response, request
from flask_appsfoundry.exceptions import NotFound
from six import text_type
from werkzeug.routing import BaseConverter

from app import app, db
from app.offers.models import Offer
from app.users.helpers import get_organization_cart, get_or_create
from app.users.organizations import is_organization_manager, blueprint, OrganizationCartOffer
from app.users.schemas import OrganizationOfferSchema
from app.utils.marshmallow_helpers import schema_load


class CartOfferConverter(BaseConverter):
    def to_python(self, value):
        session = db.session()
        cart_offer = session.query(OrganizationCartOffer).get(value)
        if not cart_offer:
            raise NotFound(user_message='Item not found')
        return cart_offer

    def to_url(self, value):
        return text_type(value.id)


app.url_map.converters['cart_offer'] = CartOfferConverter


@blueprint.route('/<organization:entity>/cart', methods=['GET', 'DELETE', ])
@is_organization_manager
def organization_cart(entity):
    session = db.session
    cart = get_organization_cart(entity.id, session)
    if request.method == 'DELETE':
        cart.is_active = False
        session.add(cart)
        session.commit()
        return make_response(jsonify({'message': "Cart deleted"}), NO_CONTENT)
    return make_response(jsonify(cart.values()), OK)


@blueprint.route('/<organization:entity>/cart/offer', methods=['POST'])
@is_organization_manager
def organization_cart_offer(entity):
    cart_offer_schema = OrganizationOfferSchema()
    request_data = schema_load(cart_offer_schema)
    session = db.session
    org_cart = get_organization_cart(entity.id, session)
    offer = Offer.query.get(request_data.get('offer_id'))
    if not offer:
        raise NotFound(user_message='Offer not found.')
    quantity = request_data.pop('quantity')
    cart_offer, created = get_or_create(session, OrganizationCartOffer, cart_id=org_cart.id, **request_data)
    if not created:
        return make_response(jsonify({'message': 'Offer already exists'}), CONFLICT)
    cart_offer.quantity = quantity
    session.add(cart_offer)
    session.commit()
    return make_response(jsonify(cart_offer.values()), CREATED)


@blueprint.route('/<organization:entity>/cart/offer/<cart_offer:cart_offer>', methods=['PUT', 'DELETE'])
@is_organization_manager
def update_organization_cart(entity, cart_offer):
    cart_offer_schema = OrganizationOfferSchema()
    request_data = schema_load(cart_offer_schema)
    session = db.session
    org_cart = get_organization_cart(entity.id, session)
    quantity = request_data.pop('quantity')
    cart_offer_exist = OrganizationCartOffer.query.filter_by(id=cart_offer.id, cart_id=org_cart.id,
                                                             **request_data).first()
    if not cart_offer_exist:
        raise NotFound(user_message='Item not found.')

    if request.method == 'DELETE':
        session.delete(cart_offer_exist)
        session.commit()
        return make_response(jsonify({'message': "Cart offer deleted"}), NO_CONTENT)

    cart_offer_exist.quantity = quantity
    session.add(cart_offer_exist)
    session.commit()
    return make_response(jsonify(cart_offer_exist.values()), OK)
