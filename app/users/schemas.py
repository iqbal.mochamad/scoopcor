from marshmallow import fields
from marshmallow.validate import Range

from app import ma


class OrganizationOfferSchema(ma.Schema):
    offer_id = fields.Integer(required=True)
    quantity = fields.Integer(required=True, validate=[Range(min=1)])
