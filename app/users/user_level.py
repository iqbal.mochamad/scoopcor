"""
    This api temporary comment out,
    uncomment this if ePerpus CMS portal used master for level/Tingkatan
"""



# from flask import Blueprint, jsonify
# from flask_appsfoundry import parsers
# from flask_appsfoundry.exceptions import NotFound, Forbidden
#
# from app import db
# from app.users.organizations import Organization, is_organization_manager_by_id
# from app.auth.decorators import user_is_authenticated
#
# from marshmallow_sqlalchemy import ModelSchema
# from app.utils.marshmallow_helpers import update_response, create_response, MaListResponse
# from app.utils.shims.parsers import WildcardFilter
#
# blueprint = Blueprint('user_level', __name__, url_prefix='/v1')
#
#
# class UserLevel(db.Model):
#
#     __tablename__ = 'user_levels'
#
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(100), nullable=False)
#     organization_id = db.Column(db.Integer, db.ForeignKey("cas_organizations.id"), nullable=False)
#     organization = db.relationship(Organization, backref='user_levels')
#     is_active = db.Column(db.Boolean, default=True)
#
#
# class UserLevelSchema(ModelSchema):
#     class Meta:
#         model = UserLevel
#         sqla_session = db.session
#
#
# def get_organization(organization_id, session):
#     return session.query(Organization).get(organization_id)
#
#
# def assert_organization(organization_id, session):
#     org = get_organization(organization_id, session)
#     if not org: raise NotFound
#     if org.parent_organization: raise Forbidden
#
#
# def get_userlevel(level_id, session=None):
#     return session.query(UserLevel).get(level_id)
#
#
# @blueprint.route('/organizations/<int:organization_id>/user_level/<int:level_id>')
# @user_is_authenticated
# @is_organization_manager_by_id
# def details(organization_id=None, level_id=None):
#     session = db.session
#     assert_organization(organization_id, session)
#     user_level = get_userlevel(level_id, session)
#     return jsonify(UserLevelSchema().dump(user_level).data)
#
#
# @blueprint.route('/organizations/<int:organization_id>/user_level', methods=['POST'])
# @user_is_authenticated
# @is_organization_manager_by_id
# def create(organization_id=None):
#     session = db.session
#     assert_organization(organization_id, session)
#     return create_response(UserLevelSchema())
#
#
# @blueprint.route('/organizations/<int:organization_id>/user_level/<int:level_id>', methods=['PUT'])
# @user_is_authenticated
# @is_organization_manager_by_id
# def update(organization_id=None, level_id=None):
#     session = db.session
#     assert_organization(organization_id, session)
#     user_level = get_userlevel(level_id, session)
#     return update_response(schema=UserLevelSchema(), instance=user_level, session=session)
#
#
# class UserLevelArgsParser(parsers.SqlAlchemyFilterParser):
#     __model__ = UserLevel
#     q = WildcardFilter(dest='name')
#     name = parsers.StringFilter()
#     organization_id = parsers.IntegerFilter()
#
#
# @blueprint.route('/organizations/<int:organization_id>/user_level')
# @user_is_authenticated
# @is_organization_manager_by_id
# def list(organization_id=None):
#     return MaListResponse(
#         query=db.session.query(UserLevel).filter_by(organization_id=organization_id),
#         schema=UserLevelSchema(),
#         key_name="data",
#         filter_parsers=UserLevelArgsParser()
#     ).response()
