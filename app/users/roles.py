import flask_restful as restful
from app.users.users import Role

from flask_appsfoundry import controllers
from flask_appsfoundry import parsers
from flask_appsfoundry.parsers import (ParserBase, StringField, IntegerField, InputField, EnumField, BooleanField)
from flask_appsfoundry.parsers.filterinputs import (IntegerFilter, StringFilter, BooleanFilter)
from flask_appsfoundry.serializers import fields, SerializerBase

from flask import Blueprint


class RolesListArgsParser(parsers.SqlAlchemyFilterParser):
    __model__ = Role
    id = IntegerFilter()
    name = StringFilter()
    is_active = BooleanFilter()

class RoleArgsParser(ParserBase):
    name = StringField(required=True)
    perms = IntegerField(action='append', default=[])
    is_active = BooleanField(default=True)

class RolesSerializer(SerializerBase):
    id = fields.Integer
    name = fields.String
    perms = fields.List(fields.Nested({'id': fields.Integer, 'name': fields.String}))
    is_active = fields.Boolean


class RolesListApi(controllers.ListCreateApiResource):
    """ Shows a list of Role and do POST / GET """
    query_set = Role.query
    list_request_parser = RolesListArgsParser
    serialized_list_name = 'roles'
    response_serializer = RolesSerializer
    create_request_parser = RoleArgsParser
    get_security_tokens = ('can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext',
                           'can_read_global_role',)
    post_security_tokens = ('can_read_write_global_all', 'can_read_write_global_role',)

class RolesApi(controllers.DetailApiResource):
    """ GET/PUT/DELETE Role """
    query_set = Role.query
    update_request_parser = RoleArgsParser
    response_serializer = RolesSerializer
    get_security_tokens = ('can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext',
                           'can_read_global_role',)
    put_security_tokens = ('can_read_write_global_all', 'can_read_write_global_role',)
    delete_security_tokens = ('can_read_write_global_all',)


roles_blueprint = Blueprint('roles', __name__, url_prefix='/v1/roles')
roles_api = restful.Api(roles_blueprint)
roles_api.add_resource(RolesListApi, '', endpoint='role-list')
roles_api.add_resource(RolesApi, '/<int:db_id>', endpoint='role-details')
