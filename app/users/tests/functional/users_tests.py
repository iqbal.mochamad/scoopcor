from unittest import TestCase

from datetime import timedelta
from flask_appsfoundry.exceptions import Conflict
from mock import patch

from app import db, app
from app.items.models import ItemRegister, UserItem
from app.points.models import PointUser, PointRegister
from app.users import users
from app.users.tests.fixtures import UserFactory, OrganizationFactory
from app.users.users import Profile, APPS_FOUNDRY_ORG, is_apps_foundry_users
from app.utils.datetimes import get_utc
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory


class UsersTests(TestCase):

    @classmethod
    def setUpClass(cls):
        db.session().close_all()
        db.drop_all()
        db.create_all()

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.drop_all()

    def test_assert_username_and_email(self):
        s = db.session()
        UserFactory(username='abc', email='abc@testmail.com')
        s.commit()

        self.assertRaises(Conflict, users.assert_username_and_email,
                          s, 'abc', 'kkk@testmail.com')

        self.assertRaises(Conflict, users.assert_username_and_email,
                          s, 'kkk', 'abc@testmail.com')

        actual = users.assert_username_and_email(session=s, username='kkk', email='kkk@testmail.com')
        self.assertIsNone(actual)

    @patch('app.utils.solr_gateway.EmailLog.add')
    def test_send_verification_email(self, mock_send_email_log):
        # comment this mock_stmplib if u want to test sending email to mailtrap.io
        # or other smtp configured in config.py/local_config.py
        mock_smtplib()

        s = db.session()
        user = UserFactory(username='testmail', email='testmail@testmail.com')
        user.key_verify = users.get_verify_token()
        s.add(user)
        s.commit()
        with app.test_request_context('/'):
            mock_send_email_log.return_value = None
            actual = users.send_verification_email(user)
            self.assertIsNone(actual)

    def test_deliver_point_on_user_registration_or_update_profile(self):
        s = db.session()
        user = UserFactory(username='testmail1', email='testmail1@testmail.com')
        user2 = UserFactory(username='testmail2', email='testmail2@testmail.com')
        user3 = UserFactory(username='testmail3', email='testmail3@testmail.com')
        user4 = UserFactory(username='testmail4', email='testmail4@testmail.com')
        user_referrer = UserFactory(username='referrer1', email='referrer1@testmail.com')
        s.add(user)
        s.add(user2)
        s.add(user_referrer)

        # add Point setting
        point_refferal = 0
        point_valid_profile = 0
        item_register = ItemRegister(name='point_reward', point=point_valid_profile, point_referral=point_refferal)
        s.add(item_register)
        s.commit()

        # user register with no referrer and empty profile, no point rewarded
        users.deliver_point_on_user_registration(user, user_referrer=None, session=s)
        user_point = s.query(PointUser).filter_by(user_id=user.id).first()
        user_point_total = user_point.point_amount if user_point else 0
        self.assertEqual(user_point_total, 0,
                         'User register with no referrer and invalid profile should get 0 point, actual {}'.format(
                            user_point_total))

        # user register with a referrer and empty profile, got a point refferal
        user.referral_id = user_referrer.id
        users.deliver_point_on_user_registration(user, user_referrer=user_referrer, session=s)
        user_point = s.query(PointUser).filter_by(user_id=user.id).first()
        user_point_total = user_point.point_amount if user_point else 0
        self.assertEqual(user_point_total, point_refferal,
                         'User register with valid referrer but invalid profile should get {} point, actual {}'.format(
                            point_refferal, user_point_total))

        # user register with a referrer and valid profile, got a point refferal + point
        user2.referral_id = user_referrer.id
        user2.first_name = 'valid_first_name'
        user_profile2 = Profile(user_id=user2.id, birthdate='2008-01-01', gender='1')
        s.add(user_profile2)
        s.commit()
        users.deliver_point_on_user_registration(user2, user_referrer=user_referrer, session=s)
        user_point = s.query(PointUser).filter_by(user_id=user2.id).first()
        user_point_total = user_point.point_amount if user_point else 0
        self.assertEqual(user_point_total, point_refferal + point_valid_profile,
                         'User register with valid referrer and valid profile should get {} point, actual {}'.format(
                          point_refferal + point_valid_profile, user_point_total))

        self.assertEqual(users.is_user_profile_valid(user2), True)
        self.assertEqual(users.is_user_profile_valid(user), False)

        # test user update profile - profile is incomplete -> no point delivered
        users.deliver_point_on_valid_profile(user3, session=s)
        user_point = s.query(PointUser).filter_by(user_id=user3.id).first()
        user_point_total = user_point.point_amount if user_point else 0
        self.assertEqual(user_point_total, 0,
                         'User update data but profile still not completed should get 0 point, actual {}'.format(
                          user_point_total))

        # test user update profile - profile is complete/valid -> point delivered
        user4.first_name = 'valid_first_name_too'
        user_profile4 = Profile(user_id=user4.id, birthdate='2008-01-01', gender='1')
        s.add(user_profile4)
        s.commit()
        users.deliver_point_on_valid_profile(user4, session=s)
        user_point = s.query(PointUser).filter_by(user_id=user4.id).first()
        user_point_total = user_point.point_amount if user_point else 0
        self.assertEqual(user_point_total, point_valid_profile,
                         'User update data and profile valid, should get {} point, actual {}'.format(
                          point_valid_profile, user_point_total))

    def test_deliver_gifts_on_user_registration(self):
        s = db.session()
        user = UserFactory(username='usergift1', email='usergift1@testmail.com')
        s.add(user)

        from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
        brand1 = BrandFactory(id=11)
        brand2 = BrandFactory(id=12)
        item1 = ItemFactory(id=221, brand=brand1, is_active=True, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME])
        item2 = ItemFactory(id=222, brand=brand2, is_active=True, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME])
        item3 = ItemFactory(id=223, is_active=True, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME])
        item4 = ItemFactory(id=224, is_active=True, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME])
        item_register = ItemRegister(
            name='register',
            valid_from=get_utc() - timedelta(days=10),
            valid_to=get_utc() + timedelta(days=10),
            items=[item3.id, item4.id, ],
            brands=[brand1.id, brand2.id, ],
            is_active=True
        )
        s.add(item_register)
        s.commit()

        users.deliver_items_on_user_registration(user, s)
        user_items = s.query(UserItem).filter_by(user_id=user.id).all()
        self.assertEqual(len(user_items), 4)
        actual_items = [ui.item_id for ui in user_items]
        self.assertIn(item1.id, actual_items)
        self.assertIn(item2.id, actual_items)
        self.assertIn(item3.id, actual_items)
        self.assertIn(item4.id, actual_items)

    def test_referral_code_generator(self):
        first_name = 'abc'
        referral_code = users.referral_code_generator(first_name)
        self.assertEqual(len(referral_code), 10)

        first_name = 'abcdefghijklmnopq'
        referral_code = users.referral_code_generator(first_name)
        self.assertEqual(len(referral_code), 10)

    def test_generate_referral_code(self):
        s = db.session()
        user = UserFactory(username='user_ref1', email='user_ref1@testmail.com', is_active=True, is_verified=True,
                           first_name='valid', referral_code=None)
        user2 = UserFactory(username='user_ref2', email='user_ref2@testmail.com', is_active=True,
                           is_verified=False,
                           first_name=None, referral_code=None)
        s.add(user)
        s.commit()

        users.generate_referral_code(user, session=s)
        self.assertIsNotNone(user.referral_code)
        # user data still not completed, no referral code generated
        users.generate_referral_code(user2, session=s)
        self.assertIsNone(user2.referral_code)

    def test_is_apps_foundry_users(self):
        with app.test_request_context('/'):
            org_apps_foundry = OrganizationFactory(id=APPS_FOUNDRY_ORG, name='apps foundry')
            org_other1 = OrganizationFactory(id=9911, name='apa aja bro')
            org_other2 = OrganizationFactory(id=9912, name='apa aja bro 2')
            user_apps_foundry = UserFactory(id=56)
            user_apps_foundry.organizations.append(org_apps_foundry)
            user_apps_foundry.organizations.append(org_other1)
            db.session().commit()
            actual = is_apps_foundry_users(user_apps_foundry)
            self.assertTrue(actual)

            user_other = UserFactory(id=66)
            user_other.organizations.append(org_other2)
            db.session().commit()
            actual = is_apps_foundry_users(user_other)
            self.assertFalse(actual)
