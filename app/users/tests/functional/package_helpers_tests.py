# from __future__ import unicode_literals
#
# from unittest import TestCase
#
# from datetime import datetime, timedelta
#
# from dateutil.relativedelta import relativedelta
# from flask import g
# from mock import patch
#
# from nose.tools import istest, nottest
# from flask_appsfoundry.exceptions import Conflict, BadRequest, Forbidden, NotFound
#
# from app import app, set_request_id, db
# from app.auth.models import UserInfo
# from app.items.choices import STATUS_READY_FOR_CONSUME
# from app.items.choices import STATUS_TYPES
# from app.offers.choices.offer_type import SINGLE
# from app.offers.choices.offer_type import SUBSCRIPTIONS
# from app.users.helpers import get_stock_holding_org, assert_user_has_not_reached_maximum_allowed, update_qty_available, \
#     assert_user_already_borrowed_the_item, assert_catalog_item, assert_qty_available, assert_borrowed_cooldown, \
#     assert_cooldown_for_reborrowing_item, assert_user_vs_organization, returned_user_borrowed_item, facet_field_mapper, \
#     assert_organization_valid_manager, assert_valid_wallet_user, get_manageable_organizations, \
#     assert_user_already_exists, update_shared_library_item_from_order
# from app.users.models import User, UserBorrowedItem, SharedLibraryBrand
# from app.users.shared_library import SharedLibraryItem, SharedLibraryBrand
# from app.wallet.models import Wallet
# from tests.fixtures.sqlalchemy import ItemFactory, OrganizationFactory
# from tests.fixtures.sqlalchemy.master import BrandFactory, VendorFactory
# from tests.fixtures.sqlalchemy.offers import OfferTypeFactory, OfferFactory
# from tests.fixtures.sqlalchemy.users import UserFactory, CatalogFactory, UserBorrowedItemFactory, \
#     CatalogItemFactory, SharedLibraryItemFactory, SharedLibraryFactory, OrganizationUserFactory, \
#     SharedLibraryBrandFactory
#
#
# @istest
# class HelpersTests(TestCase):
#
#     maxDiff = None
#
#     max_borrowed_time = relativedelta(days=7)
#
#     @classmethod
#     def setUpClass(cls):
#         db.drop_all()
#         db.create_all()
#
#         cls.init_data()
#
#         app.before_request_funcs = {
#             None: [
#                 set_request_id,
#                 init_g_current_user,
#             ]
#         }
#
#     @classmethod
#     def tearDownClass(cls):
#         db.session().close_all()
#         db.drop_all()
#
#     @classmethod
#     def init_data(cls):
#         s = db.session()
#         s.expire_on_commit = False
#         cls.library_org = SharedLibraryFactory(
#             id=11,
#             user_concurrent_borrowing_limit=10,
#             reborrowing_cooldown=relativedelta(days=3),
#             borrowing_time_limit=cls.max_borrowed_time)
#         cls.child_org = SharedLibraryFactory(
#             id=12,
#             parent_organization=cls.library_org,
#             reborrowing_cooldown=relativedelta(days=0),)
#         cls.user = UserFactory(id=12345, username='test12345', email='test@email.com')
#         cls.user_not_authorized = UserFactory(id=12350)
#
#         cls.child_org.users.append(cls.user)
#
#         offer_type = OfferTypeFactory(id=SINGLE)
#         offer_type_sub = OfferTypeFactory(id=SUBSCRIPTIONS)
#
#         cls.item1 = ItemFactory(id=1)
#         cls.item2 = ItemFactory(id=2)
#         # item 3, item not borrowed yet
#         cls.item3 = ItemFactory(id=3)
#         # item 4, item is borrowed and already returned, should not shown in list of borrowed items
#         cls.item4 = ItemFactory(id=4)
#         cls.item_new = ItemFactory(id=15)
#         # to test item not exist in catalog
#         cls.itemNonInCatalog = ItemFactory(id=5)
#
#         cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
#                                   price_point=10)
#         cls.offer1.items.append(cls.item1)
#         cls.offer_new = OfferFactory(id=111, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
#                                      price_point=10)
#         cls.offer_new.items.append(cls.item_new)
#
#         vendor = VendorFactory(id=2001)
#         cls.brand_news = BrandFactory(id=12, vendor=vendor)
#         item_news = ItemFactory(id=12, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=cls.brand_news)
#         cls.offer_sub = OfferFactory(id=12, offer_type=offer_type_sub, is_free=False, price_idr=20000, price_usd=20,
#                                      price_point=20)
#         cls.offer_sub.brands.append(cls.brand_news)
#         cls.brand_news_new = BrandFactory(id=123, vendor=vendor)
#         cls.offer_sub_new = OfferFactory(id=123, offer_type=offer_type_sub, is_free=False, price_idr=20000,
#                                          price_usd=20,
#                                          price_point=20)
#         cls.offer_sub_new.brands.append(cls.brand_news_new)
#
#         shared_lib_item1 = SharedLibraryItemFactory(
#             shared_library=cls.library_org,
#             item=cls.item1,
#             quantity=10,
#             quantity_available=10,
#             item_name_override='{} additional title'.format(cls.item1.name)
#         )
#         shared_lib_item2 = SharedLibraryItemFactory(
#             shared_library=cls.library_org,
#             item=cls.item2,
#             quantity=10,
#             quantity_available=7,
#             item_name_override='{} additional title'.format(cls.item2.name)
#         )
#         shared_lib_item3 = SharedLibraryItemFactory(
#             shared_library=cls.library_org,
#             item=cls.item3,
#             quantity=10,
#             quantity_available=8,
#             item_name_override='{} additional title'.format(cls.item3.name)
#         )
#         shared_lib_item4 = SharedLibraryItemFactory(
#             shared_library=cls.library_org,
#             item=cls.item4,
#             quantity=10,
#             quantity_available=0,
#             item_name_override='{} additional title'.format(cls.item4.name)
#         )
#         org_brand = SharedLibraryBrandFactory(
#             shared_library=cls.library_org,
#             brand=cls.brand_news,
#             quantity=10
#         )
#         cls.catalog = CatalogFactory(id=1,shared_library=cls.library_org,)
#         catalog_item1 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item1, )
#         catalog_item2 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item2, )
#         catalog_item3 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item3, )
#         catalog_item4 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item4, )
#         cls.user_borrowed_item1 = UserBorrowedItemFactory(
#             id=1000,
#             user=cls.user,
#             catalog_item=catalog_item1,
#             borrowing_start_time=datetime.now(),
#             returned_time=None,
#         )
#         cls.user_borrowed_item2 = UserBorrowedItemFactory(
#             id=1001,
#             user=cls.user,
#             catalog_item=catalog_item2,
#             borrowing_start_time=datetime.now(),
#             returned_time=None,
#         )
#         cls.user_borrowed_item4 = UserBorrowedItemFactory(
#             id=1004,
#             user=cls.user,
#             catalog_item=catalog_item4,
#             borrowing_start_time=datetime.now() - timedelta(days=7),
#             returned_time=datetime.now() - timedelta(days=1),
#         )
#
#         wallet = Wallet(party=cls.user, balance=1000)
#
#         s.add(wallet)
#
#         cls.user_manager = UserFactory(id=67890)
#
#         cls.organization = OrganizationFactory(id=711)
#
#         s.commit()
#
#         org_user_non_manager = OrganizationUserFactory(user_id=cls.user.id,
#                                                        organization_id=cls.organization.id,
#                                                        is_manager=False)
#
#         org_user_manager = OrganizationUserFactory(user_id=cls.user_manager.id,
#                                                    organization_id=cls.organization.id,
#                                                    is_manager=True)
#
#         s.commit()
#
#     def test_get_stock_holding_org(self):
#         actual_holding_org = get_stock_holding_org(self.child_org)
#         self.assertEqual(self.library_org.id, actual_holding_org.id)
#
#     def test_update_qty_available(self):
#         session = db.session()
#         org_item = session.query(SharedLibraryItem).filter(
#             SharedLibraryItem.organization_id == self.catalog.organization_id,
#             SharedLibraryItem.item_id == self.item3.id
#         ).first()
#         available_qty_before_borrowed = org_item.quantity_available
#
#         update_qty_available(session,
#                              current_org=self.library_org,
#                              item_id=self.item3.id,
#                              qty=-1)
#
#         # get qty available now after borrowed:
#         org_item = session.query(SharedLibraryItem).filter(
#             SharedLibraryItem.organization_id == self.catalog.organization_id,
#             SharedLibraryItem.item_id == self.item3.id
#         ).first()
#         actual_qty = org_item.quantity_available if org_item else -1
#
#         self.assertEqual(actual_qty, available_qty_before_borrowed - 1)
#
#     def test_returned_user_borrowed_item(self):
#         session = db.session()
#         trx_user_borrowed_item = session.query(UserBorrowedItem).get(self.user_borrowed_item1.id)
#         trx_user_borrowed_item_concurrent = session.query(UserBorrowedItem).get(self.user_borrowed_item2.id)
#
#         # returned the item
#         self.assertIsNotNone(returned_user_borrowed_item(session, trx_user_borrowed_item))
#
#         # when concurrent update happen, Conflict should raised
#         # simulate concurrent update (other user update the same row), via updating the version_id
#         db.engine.execute("update core_user_borrowed_items set modified = now() "
#                           "where id = {}".format(trx_user_borrowed_item_concurrent.id))
#         self.assertRaises(Conflict, returned_user_borrowed_item,
#                           session, trx_user_borrowed_item_concurrent)
#
#     def test_assert_user_has_not_reached_maximum_allowed(self):
#         session = db.session()
#         session.expire_on_commit = False
#
#         # set borrowing limit to 2, to test limit and expect error
#         self.library_org.user_concurrent_borrowing_limit = 2
#         self.assertRaises(Conflict, assert_user_has_not_reached_maximum_allowed,
#                           session, self.library_org, 12345)
#
#         # limit not reached, no error raised
#         self.library_org.user_concurrent_borrowing_limit = 10
#         actual_allowed = assert_user_has_not_reached_maximum_allowed(
#             session=session,
#             stock_org=self.library_org,
#             user_id=12345,
#         )
#         self.assertIsNone(actual_allowed)
#
#     def test_assert_user_already_borrowed_the_item(self):
#         session = db.session()
#         session.expire_on_commit = False
#
#         # user already borrowed the item --> raise Conflict
#         self.assertRaises(Conflict, assert_user_already_borrowed_the_item,
#                           session, self.library_org, 12345, self.item1.id, self.catalog.id)
#
#         # user not yet borrowed the item --> return none
#         actual = assert_user_already_borrowed_the_item(
#             session=session,
#             stock_org=self.library_org,
#             current_user_id=12345,
#             item_id=self.item3.id,
#             catalog_id=self.catalog.id
#         )
#         self.assertIsNone(actual)
#
#     def test_assert_cooldown_for_reborrowing_item(self):
#
#         actual = assert_cooldown_for_reborrowing_item(self.child_org)
#         self.assertIsNone(actual)
#
#         self.assertRaises(Conflict, assert_cooldown_for_reborrowing_item,
#                           self.library_org)
#
#     def test_assert_borrowed_cooldown(self):
#         session = db.session()
#         session.expire_on_commit = False
#
#         self.assertRaises(Conflict, assert_borrowed_cooldown,
#                           session, self.library_org, 12345, self.item4.id, self.catalog.id)
#
#         actual = assert_borrowed_cooldown(
#             session=session,
#             stock_org=self.library_org,
#             current_user_id=12345,
#             item_id=self.item3.id,
#             catalog_id=self.catalog.id
#         )
#         self.assertIsNone(actual)
#
#     def test_assert_catalog_item(self):
#         session = db.session()
#         session.expire_on_commit = False
#
#         # item not found in catalog --> raise BadRequest
#         self.assertRaises(BadRequest, assert_catalog_item,
#                           session, self.catalog, self.itemNonInCatalog.id, self.library_org)
#
#         # item found in catalog
#         actual = assert_catalog_item(
#             session=session,
#             catalog=self.catalog,
#             item_id=self.item1.id,
#             stock_org=self.library_org
#         )
#         self.assertIsNotNone(actual)
#
#     def test_assert_qty_available(self):
#         session = db.session()
#         session.expire_on_commit = False
#
#         # item qty not available --> raise BadRequest
#         self.assertRaises(BadRequest, assert_qty_available,
#                           session, self.library_org.id, self.item4.id)
#
#         # item found in catalog --> return none
#         actual = assert_qty_available(
#             session=session,
#             stock_org_id=self.library_org.id,
#             item_id=self.item1.id,
#         )
#         self.assertIsNone(actual)
#
#     def test_assert_user_vs_organization(self):
#         session = db.session()
#         session.expire_on_commit = False
#
#         # user not belong to the org --> raise error
#         self.assertRaises(Forbidden, assert_user_vs_organization,
#                           session, self.library_org.id, self.user_not_authorized.id)
#
#         # user belong to the org
#         actual = assert_user_vs_organization(
#             session=session,
#             org_id=self.library_org.id,
#             user_id=self.user.id,
#         )
#         self.assertIsNone(actual)
#
#     def test_assert_organization_not_manager(self):
#         session = db.session()
#         self.assertRaises(Forbidden, assert_organization_valid_manager,
#                           session, org_id=self.organization.id,
#                           user=self.user)
#
#     def test_assert_organization_valid_manager(self):
#         session = db.session()
#         self.assertIsNone(assert_organization_valid_manager(session, org_id=self.organization.id,
#                                                             user=self.user_manager))
#
#     def test_assert_valid_wallet_user(self):
#         session = db.session()
#         self.assertRaises(NotFound, assert_valid_wallet_user, session, self.user_not_authorized)
#
#     def test_get_manageable_organizations(self):
#         session = db.session()
#         self.assertNotEqual(len(get_manageable_organizations(session, self.user_manager)), 0)
#
#     def test_get_manageable_organizations_failed(self):
#         session = db.session()
#         self.assertEqual(len(get_manageable_organizations(session, self.user_not_authorized)), 0)
#
#     def test_facet_field_mapper(self):
#         field_list = ["item_categories", "item_authors",
#                       "item_languages", "category_names",
#                       "author_names"]
#         self.assertEqual(facet_field_mapper(field_list[0]), field_list[3])
#         self.assertEqual(facet_field_mapper(field_list[1]), field_list[4])
#         self.assertEqual(facet_field_mapper(field_list[2]), field_list[2])
#         self.assertEqual(facet_field_mapper(field_list[3]), field_list[3])
#         self.assertEqual(facet_field_mapper(field_list[4]), field_list[4])
#
#         self.assertNotEqual(facet_field_mapper(field_list[3]), field_list[0])
#         self.assertNotEqual(facet_field_mapper(field_list[4]), field_list[1])
#
#     def test_assert_user_already_exists(self):
#         session = db.session()
#         session.expire_on_commit = False
#         username_exists = 'test12345'
#         email_exists = 'test@email.com'
#
#         # assert username already exists
#         self.assertRaises(Conflict, assert_user_already_exists,
#                           session, username_exists, 'fakemail@email.com',  self.library_org)
#
#         # assert email already exists
#         self.assertRaises(Conflict, assert_user_already_exists,
#                           session, 'fakeusername', email_exists,  self.library_org)
#
#         # assert user already exists in an organization
#         self.assertRaises(Conflict, assert_user_already_exists,
#                           session, username_exists, email_exists,  self.child_org)
#
#         actual = assert_user_already_exists(
#             session=session,
#             username='newusername',
#             email='newemail@test.com',
#             organization_to_add=self.library_org
#         )
#         self.assertIsNone(actual)
#
#     def test_update_shared_library_item(self):
#         with app.test_request_context('/'):
#             session = db.session()
#             session.add(self.item1)
#             session.add(self.offer1)
#             org_item = session.query(SharedLibraryItem).filter(
#                 SharedLibraryItem.organization_id == self.library_org.id,
#                 SharedLibraryItem.item_id == self.item1.id
#             ).first()
#             quantity_buy = 10
#             quantity_expected = org_item.quantity + quantity_buy
#             quantity_available_expected = org_item.quantity_available + quantity_buy
#
#             update_shared_library_item_from_order(session, self.library_org, self.offer1, quantity_buy=quantity_buy)
#             session.commit()
#             session.add(self.item1)
#             org_item = session.query(SharedLibraryItem).filter(
#                 SharedLibraryItem.organization_id == self.library_org.id,
#                 SharedLibraryItem.item_id == self.item1.id
#             ).first()
#             self.assertEqual(quantity_expected, org_item.quantity,
#                              'Shared Library Item Quantity not match, '
#                              'expect {expect} vs actual {actual}'.format(
#                                  expect=quantity_expected,
#                                  actual=org_item.quantity))
#             self.assertEqual(quantity_available_expected, org_item.quantity_available,
#                              'Shared Library Item Quantity Available not match, '
#                              'expect {expect} vs actual {actual}'.format(
#                                  expect=quantity_available_expected,
#                                  actual=org_item.quantity_available))
#
#     def test_update_shared_library_item_new(self):
#         with app.test_request_context('/'):
#             session = db.session()
#             session.add(self.item_new)
#             session.add(self.offer_new)
#             org_item = session.query(SharedLibraryItem).filter(
#                 SharedLibraryItem.organization_id == self.library_org.id,
#                 SharedLibraryItem.item_id == self.item_new.id
#             ).first()
#             quantity_buy = 30
#
#             update_shared_library_item_from_order(session, self.library_org, self.offer_new, quantity_buy=quantity_buy)
#             session.commit()
#
#             org_item = session.query(SharedLibraryItem).filter(
#                 SharedLibraryItem.organization_id == self.library_org.id,
#                 SharedLibraryItem.item_id == self.item_new.id
#             ).first()
#             self.assertEqual(quantity_buy, org_item.quantity,
#                              'Shared Library New Item Quantity not match, '
#                              'expect {expect} vs actual {actual}'.format(
#                                  expect=quantity_buy,
#                                  actual=org_item.quantity))
#             self.assertEqual(quantity_buy, org_item.quantity_available,
#                              'Shared Library New Item Quantity Available not match, '
#                              'expect {expect} vs actual {actual}'.format(
#                                  expect=quantity_buy,
#                                  actual=org_item.quantity_available))
#
#     def test_update_shared_library_brand(self):
#         with app.test_request_context('/'):
#             session = db.session()
#             org_item = session.query(SharedLibraryBrand).filter(
#                 SharedLibraryBrand.organization_id == self.library_org.id,
#                 SharedLibraryBrand.brand_id == self.brand_news.id
#             ).first()
#             quantity_buy = 12
#             quantity_expected = org_item.quantity + quantity_buy
#
#             update_shared_library_item_from_order(session, self.library_org, self.offer_sub, quantity_buy=quantity_buy)
#             session.commit()
#
#             org_item = session.query(SharedLibraryBrand).filter(
#                 SharedLibraryBrand.organization_id == self.library_org.id,
#                 SharedLibraryBrand.brand_id == self.brand_news.id
#             ).first()
#             self.assertEqual(quantity_expected, org_item.quantity,
#                              'Shared Library Brand Quantity not match, '
#                              'expect {expect} vs actual {actual}'.format(
#                                  expect=quantity_expected,
#                                  actual=org_item.quantity))
#
#     def test_update_shared_library_brand_new(self):
#         with app.test_request_context('/'):
#             session = db.session()
#             session.add(self.brand_news_new)
#             session.add(self.offer_sub_new)
#             org_item = session.query(SharedLibraryBrand).filter(
#                 SharedLibraryBrand.organization_id == self.library_org.id,
#                 SharedLibraryBrand.brand_id == self.brand_news_new.id
#             ).first()
#             quantity_buy = 30
#
#             update_shared_library_item_from_order(session, self.library_org, self.offer_sub_new, quantity_buy=quantity_buy)
#             session.commit()
#             session.add(self.brand_news_new)
#             org_item = session.query(SharedLibraryBrand).filter(
#                 SharedLibraryBrand.organization_id == self.library_org.id,
#                 SharedLibraryBrand.brand_id == self.brand_news_new.id
#             ).first()
#             self.assertEqual(quantity_buy, org_item.quantity,
#                              'Shared Library New Brand Quantity not match, '
#                              'expect {expect} vs actual {actual}'.format(
#                                  expect=quantity_buy,
#                                  actual=org_item.quantity))
#
#
# def init_g_current_user():
#     g.user = UserInfo(username='dfdsafsdf', user_id=1234, token='abcd', perm=['can_read_write_global_all',])
#     session = db.session()
#     g.current_user = session.query(User).get(12345)
#     session.close()
#
