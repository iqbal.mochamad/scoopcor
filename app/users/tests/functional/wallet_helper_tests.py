from unittest import TestCase

from datetime import timedelta
from flask import g
from nose.tools import istest

from app import db, app, set_request_id
from app.auth.models import UserInfo
from flask_appsfoundry.exceptions import BadRequest, Conflict, NotFound

from app.eperpus.tests.fixtures import SharedLibraryFactory
from app.orders.choices import COMPLETE
from app.payments.choices import CHARGED
from app.payments.choices.gateways import WALLET as WALLET_PAYMENT_GATEWAY_ID
from app.users.tests.fixtures import OrganizationFactory
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from app.users.wallet import (
    PAYMENT, ADD_BALANCE, WALLET_OFFER_TYPE_ID,
    UserOrOrganizationNotFoundException,
    wallet_top_up_transaction, get_party, assert_wallet_balance,
    save_wallet_transaction, assert_order_and_wallet_payment_gateway, assert_wallet_balance_by_party_id,
    assert_offer_for_wallet, get_wallet_for_organization, get_wallet,
    Wallet, WalletTransaction)
from tests.fixtures.sqlalchemy.master import CurrencyFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferTypeFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.payments.gateways import PaymentGatewayFactory


class HelpersTests(TestCase):

    maxDiff = None
    original_before_request_funcs = app.before_request_funcs

    @classmethod
    def setUpClass(cls):
        db.drop_all()
        db.create_all()

        cls.init_data()

        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }

    @classmethod
    def init_data(cls):
        s = db.session()
        s.expire_on_commit = False
        cur_idr = CurrencyFactory(id=2, iso4217_code='IDR')
        cls.pg_wallet = PaymentGatewayFactory(id=WALLET_PAYMENT_GATEWAY_ID, base_currency=cur_idr,
                                              payment_flow_type=CHARGED, minimal_amount=1)
        cls.pg_others = PaymentGatewayFactory(id=11, base_currency=cur_idr,
                                              payment_flow_type=CHARGED, minimal_amount=1)

        offer_type = OfferTypeFactory(id=1)
        offer_type_wallet = OfferTypeFactory(id=WALLET_OFFER_TYPE_ID)
        cls.offer_wallet = OfferFactory(id=100, offer_type=offer_type_wallet)
        cls.offer_single = OfferFactory(id=121, offer_type=offer_type)
        cls.library_org = SharedLibraryFactory(
            id=11,
            user_concurrent_borrowing_limit=10,
            reborrowing_cooldown=timedelta(days=0),
            borrowing_time_limit=timedelta(days=7))
        cls.child_org = SharedLibraryFactory(
            id=12,
            parent_organization=cls.library_org)
        cls.user = UserFactory(id=12345)
        wallet = Wallet(party=cls.user, balance=1000)
        cls.organization = OrganizationFactory(id=711)
        org_wallet = Wallet(party=cls.organization, balance=1000000)

        s.add(wallet)
        s.add(org_wallet)
        s.commit()

    @classmethod
    def tearDownClass(cls):
        app.before_request_funcs = cls.original_before_request_funcs
        db.session().close_all()
        db.drop_all()

    def test_wallet_payment_transaction(self):
        # checkout - payment from wallet - reduce wallet balance
        pass

    def test_assert_order_and_wallet_payment_gateway(self):
        session = db.session()
        session.add(self.user)
        order = OrderFactory(user_id=self.user.id, party=self.user, order_status=COMPLETE,
                             paymentgateway=self.pg_wallet)
        session.commit()
        self.assertIsNone(assert_order_and_wallet_payment_gateway(session, order_id=order.id, party_id=self.user.id))

        # change order to other payment gateway and assertion should raise error
        order.paymentgateway = self.pg_others
        session.add(order)
        session.commit()
        self.assertRaises(BadRequest, assert_order_and_wallet_payment_gateway,
                          session, order.id, self.user.id)

        # change user id and assertion should raise error
        order.user_id = 11
        session.add(order)
        session.commit()
        self.assertRaises(BadRequest, assert_order_and_wallet_payment_gateway,
                          session, order.id, self.user.id)

        # clean up data
        session.delete(order)
        session.commit()

    def test_assert_wallet_balance_by_party_id(self):
        session = db.session()
        session.add(self.user)
        wallet1 = session.query(Wallet).filter(Wallet.party_id == self.user.id).first()
        # assert wallet balance is more then order amount, if it's ok then return None
        order_amount = wallet1.balance - 1
        self.assertIsNone(assert_wallet_balance_by_party_id(session,
                                                            party_id=self.user.id,
                                                            order_amount=order_amount))

        # assert wallet balance is less then order amount, must raise BadRequest
        self.assertRaises(BadRequest, assert_wallet_balance_by_party_id,
                          session, self.user.id, order_amount + 1000)

    def test_assert_wallet_balance(self):
        session = db.session()
        session.add(self.user)
        wallet1 = session.query(Wallet).filter(Wallet.party_id == self.user.id).first()
        # assert wallet balance is more then order amount, if it's ok then return None
        order_amount = wallet1.balance - 1
        self.assertIsNone(assert_wallet_balance(wallet=wallet1, order_amount=order_amount))

        # assert wallet balance is less then order amount, must raise BadRequest
        self.assertRaises(BadRequest, assert_wallet_balance,
                          wallet1, order_amount+1000)

    def test_assert_offer_for_wallet(self):
        # ssert offer is valid for wallet top up trx, if it's ok then return None
        self.assertIsNone(assert_offer_for_wallet(self.offer_wallet))

        # assert offer is valid for wallet top up trx, if not must raise Conflict
        self.assertRaises(Conflict, assert_offer_for_wallet,
                          self.offer_single)

    def test_save_wallet_transaction_for_payment(self):
        session = db.session()
        session.add(self.user)
        session.add(self.pg_wallet)
        order = OrderFactory(user_id=self.user.id, order_status=COMPLETE,
                             paymentgateway=self.pg_wallet)
        order_line = OrderLineFactory(offer=self.offer_single, order=order)
        wallet1 = session.query(Wallet).filter(Wallet.party_id == self.user.id).first()

        # test for payment
        expected_balance = wallet1.balance - order_line.final_price
        actual = save_wallet_transaction(session, wallet1,
                                         amount=order_line.final_price,
                                         transaction_type=PAYMENT,
                                         created_by=self.user.id,
                                         order_id=order.id)
        self.assertIsNone(actual)
        self.assertEqual(wallet1.balance, expected_balance)

        # assert error raised (optimistic concurrency on wallet)
        db.engine.execute('update wallets set modified = now() '
                          'where id = {}'.format(wallet1.id))
        self.assertRaises(Conflict, save_wallet_transaction,
                          session, wallet1,
                          order_line.final_price, PAYMENT, self.user.id, order.id)

    def test_save_wallet_transaction_for_add_balance(self):
        session = db.session()
        session.add(self.user)
        session.add(self.pg_wallet)
        order = OrderFactory(user_id=self.user.id, order_status=COMPLETE,
                             paymentgateway=self.pg_others)
        order_line = OrderLineFactory(offer=self.offer_wallet, order=order)
        wallet1 = session.query(Wallet).filter(Wallet.party_id == self.user.id).first()

        # test  save wallet transcation for adding wallet balance
        expected_balance = wallet1.balance + order_line.final_price
        actual = save_wallet_transaction(session, wallet1,
                                         amount=order_line.final_price,
                                         transaction_type=ADD_BALANCE,
                                         created_by=self.user.id,
                                         order_id=order.id)
        self.assertIsNone(actual)
        self.assertEqual(wallet1.balance, expected_balance)

        # assert error raised (optimistic concurrency on wallet)
        db.engine.execute('update wallets set modified = now() '
                          'where id = {}'.format(wallet1.id))
        self.assertRaises(Conflict, save_wallet_transaction,
                          session, wallet1,
                          order_line.final_price, ADD_BALANCE, self.user.id, order.id)

    def test_wallet_add_balance_transaction(self):
        session = db.session()
        session.add(self.user)
        session.add(self.offer_wallet)
        order = OrderFactory(party=self.library_org, order_status=COMPLETE,
                             paymentgateway=self.pg_others)
        order_line = OrderLineFactory(offer=self.offer_wallet, order=order)
        session.commit()
        with app.app_context():
            g.current_user = session.query(User).get(self.user.id)
            wallet_top_up_transaction(order=order, session=db.session)
            db.session.commit()
            wallet = session.query(Wallet).filter(Wallet.party_id == 11).first()
            self.assertIsNotNone(wallet)
            self.assertEqual(wallet.balance, order_line.final_price)
            self.assertEqual(wallet.party_id, order.user_id)

            # assert wallet transaction that created
            wallet_trx = session.query(WalletTransaction).filter(WalletTransaction.order_id == order.id).first()
            self.assertIsNotNone(wallet_trx)
            self.assertEqual(wallet_trx.amount, order_line.final_price)

    def test_get_party(self):
        session = db.session()
        self.assertRaises(UserOrOrganizationNotFoundException,
                          get_party, session, 12399)
        session.add(self.user)
        wallet_user = get_party(session, self.user.id)
        self.assertIsNotNone(wallet_user)
        self.assertEqual(wallet_user.id, self.user.id)

    def test_get_wallet(self):
        session = db.session()
        self.assertIsNotNone(get_wallet(session, self.user.id))
        self.assertIsNone(get_wallet(session, 1234))

    def test_get_wallet_for_organization(self):
        session = db.session()
        self.assertIsNotNone(get_wallet_for_organization(session, self.organization.id))

    def test_get_wallet_for_organization_failed(self):
        session = db.session()
        self.assertRaises(NotFound, get_wallet_for_organization, session, 111)


def init_g_current_user():
    g.user = UserInfo(username='dfdsafsdf', user_id=12345, token='abcd', perm=['can_read_write_global_all',])
    session = db.session()
    g.current_user = session.query(User).get(12345)
    session.close()
