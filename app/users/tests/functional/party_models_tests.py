from __future__ import absolute_import, unicode_literals

from dateutil.relativedelta import relativedelta

from app.users.organizations import Organization
from app.users.base import Party
from app.users.tests.e2e.base import E2EBase
from app.users.tests.fixtures import OrganizationFactory, UserFactory
from app.eperpus.tests.fixtures import SharedLibraryFactory


class PartyModelsTests(E2EBase):
    """ Learning Test:  Verifies our application defines it's user/organization model
    structure in a way SQLAlchemy likes.
    """
    def test_create_organization(self):
        self.session.expire_on_commit = False
        org_id = 111
        organization = OrganizationFactory(id=org_id)
        self.session.commit()

        saved_organization = self.session.query(Organization).get(org_id)
        self.assertIsNotNone(saved_organization)
        self.assertEquals(organization.name, saved_organization.name)
        party = self.session.query(Party).get(org_id)
        self.assertIsNotNone(party)
        self.assertEquals(party.party_type, 'organization')

        library_org = SharedLibraryFactory(
            id=11,
            user_concurrent_borrowing_limit=10,
            reborrowing_cooldown=relativedelta(days=3),
            borrowing_time_limit=relativedelta(days=7))
        self.session.add(library_org)
        child_org = SharedLibraryFactory(
            id=12,
            parent_organization=library_org,
            reborrowing_cooldown=relativedelta(days=0), )

        user = UserFactory(id=12345)

        self.session.commit()
        saved_child_org = self.session.query(Party).get(child_org.id)
        saved_user = self.session.query(Party).get(user.id)
        self.assertIsNotNone(saved_child_org)
        self.assertEquals(child_org.name, saved_child_org.name)
        self.assertIsNotNone(saved_user)
        self.assertEquals(user.username, saved_user.username)
