from __future__ import absolute_import, unicode_literals

from behave import given, when, then
import httplib

from app import app
from app.utils.behave import (
    build_headers, set_api_user, assert_scoop_error_format, assign_user_to_organization, assert_unordered_json_equal
)
from app.utils.datetimes import datetime_to_isoformat
from app.eperpus.tests.fixtures import SharedLibraryFactory


@given('an authenticated user')
def _(context):
    set_api_user(context, context.regular_user)


@given('that user belongs to some organizations')
def _(context):
    context.parent_organization = SharedLibraryFactory()
    context.session.commit()
    context.child_organization = SharedLibraryFactory(
        parent_organization_id=context.parent_organization.id
    )
    context.session.commit()

    assign_user_to_organization(context, context.regular_user, context.child_organization)


@given('an anonymous user')
def _(context):
    pass


@when('requesting /users/current-user')
def _(context):
    context.response = context.api_client.get(
        '/v1/users/current-user',
        headers=build_headers(context)
    )


@then('HTTP response status is "{status}"')
def _(context, status):
    assert context.response.status_code == getattr(httplib, status)


@then('the authenticated user\'s data is returned')
def _(context):
    with app.test_request_context('/'):
        # need a test context here to generate the URLs
        context.session.add(context.parent_organization)
        parent_url = context.parent_organization.api_url
        context.session.add(context.child_organization)
        child_url = context.child_organization.api_url

    assert_unordered_json_equal(
        context.response,
        {
            'id': context.regular_user.id,
            'first_name': context.regular_user.first_name,
            'last_name': context.regular_user.last_name,
            'roles': [{'href': None, 'id': 10, 'title': 'verified user'}],
            'organizations': [
                {
                    'id': context.parent_organization.id,
                    'title': context.parent_organization.name,
                    'href': parent_url,
                    'is_manager': False,
                    'type': 'shared library',
                },
                {
                    'id': context.child_organization.id,
                    'title': context.child_organization.name,
                    'href': child_url,
                    'is_manager': False,
                    'type': 'shared library',
                }],
            'is_active': context.regular_user.is_active,
            'email': context.regular_user.email,
            'username': context.regular_user.username,
            'is_verified': context.regular_user.is_verified,
            'note': context.regular_user.note,
            'profile_pic': {'href': None, 'id': 0, 'title': None},
            'signup_date': datetime_to_isoformat(context.regular_user.created)
        }
    )


@then('scoop standard error format is returned')
def _(context):
    assert_scoop_error_format(context.response)
