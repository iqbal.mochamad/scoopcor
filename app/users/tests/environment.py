from app import db, app
from app.constants import RoleType
from app.utils.behave import create_user

from tests.fixtures.helpers import generate_static_sql_fixtures


def before_scenario(context, scenario):
    db.drop_all()
    db.create_all()

    context.session = db.session()

    generate_static_sql_fixtures(db.session())

    context.api_client = app.test_client(use_cookies=False)

    context.regular_user = create_user(context, RoleType.verified_user)
    context.super_user = create_user(context, RoleType.super_admin)


def after_scenario(context, scenario):
    context.session.close_all()
    db.drop_all()
