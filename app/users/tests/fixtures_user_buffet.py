from factory import alchemy, Faker, SubFactory

from app import db
from app.users.buffets import UserBuffet
from tests.fixtures.sqlalchemy.offers import OfferBuffetFactory
from tests.fixtures.sqlalchemy.orders import OrderLineFactory


class UserBuffetFactory(alchemy.SQLAlchemyModelFactory):
    user_id = Faker('pyint')
    valid_to = Faker('date_time_this_month')
    is_restore = Faker('pybool')
    is_trial = Faker('pybool')
    orderline = SubFactory(OrderLineFactory)
    offerbuffet = SubFactory(OfferBuffetFactory)

    class Meta:
        sqlalchemy_session = db.session
        model = UserBuffet
