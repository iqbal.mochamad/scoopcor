# from __future__ import absolute_import, unicode_literals
# from unittest import TestCase
#
# from flask import Flask
# import jsonschema
#
# from app import db
# from flask_appsfoundry.exceptions import BadRequest
# from app.helpers import load_json_schema, SchemaType
# from app.users.helpers import CatalogItemQueryBuilder, format_catalog_items_response
# from app.utils.json_schema import ScoopDraft4Validator, validate_scoop_jsonschema
# from tests.fixtures.solr.factory import solr_response_fixture
#
# fake_app = Flask(__name__)
#
#
# class CatalogItemQueryBuilderTest(TestCase):
#
#     def test_build_query_with_default_sort(self):
#         with fake_app.test_request_context('/') as ctx:
#             query_builder = CatalogItemQueryBuilder(catalog_id=1, flask_req=ctx.request)
#
#             self.assertDictEqual({
#                 'q': '*',
#                 'sort': "brand_name_sortable asc"
#             },
#                 query_builder._build_query()
#             )
#
#     def test_build_query_without_default_sort(self):
#
#         with fake_app.test_request_context('/?q=abc') as ctx:
#             query_builder = CatalogItemQueryBuilder(catalog_id=1, flask_req=ctx.request)
#
#             self.assertDictEqual({
#                 'q': 'abc'
#                  },
#                 query_builder._build_query()
#             )
#
#     def test_build_filter_query(self):
#
#         with fake_app.test_request_context('/') as ctx:
#
#             query_builder = CatalogItemQueryBuilder(catalog_id=1, flask_req=ctx.request)
#
#             self.assertDictEqual(
#                 {'fq': ['catalog_ids:{}'.format(1)]},
#                 query_builder._build_filter_queries()
#             )
#
#     def test_build_sort_by_brand_name(self):
#         with fake_app.test_request_context('/?order=brand_name') as ctx:
#             query_builder = CatalogItemQueryBuilder(catalog_id=1, flask_req=ctx.request)
#
#             self.assertDictEqual(
#                 {'sort': 'brand_name_sortable asc'},
#                 query_builder._build_order()
#             )
#
#     def test_build_sort_by_not_brand_name(self):
#         with fake_app.test_request_context('/?order=item_name') as ctx:
#             query_builder = CatalogItemQueryBuilder(catalog_id=1, flask_req=ctx.request)
#
#             self.assertRaises(BadRequest, query_builder._build_order)
#
#     def test_build_with_filter_by_available_stock(self):
#         with fake_app.test_request_context('/?available=true') as ctx:
#             query_builder = CatalogItemQueryBuilder(catalog_id=1, flask_req=ctx.request)
#
#             self.assertDictEqual(
#                 {'fq': ['catalog_ids:1', 'quantity_available:[1 TO *]'],
#                  'facet.field': [],
#                  'q': '*',
#                  'rows': 20,
#                  'sort': 'brand_name_sortable asc',
#                  'start': 0
#                  },
#                 query_builder.build()
#             )
#
#     def test_build_advanced_filter_query(self):
#         with fake_app.test_request_context('/?facets=category_names:Arts %26 Entertainment') as ctx:
#             query_builder = CatalogItemQueryBuilder(catalog_id=1, flask_req=ctx.request)
#
#             self.assertDictEqual(
#                 {'fq': ['category_names:"Arts & Entertainment"', 'catalog_ids:{}'.format(1)]},
#                 query_builder._build_filter_queries()
#             )
#
#
# class CatalogItemResponseTests(TestCase):
#
#     def setUp(self):
#         self.session = db.session()
#
#     def test_response_format(self):
#
#         schema = load_json_schema('users', 'catalog-items.list')
#
#         response_json = format_catalog_items_response(
#             self.session,
#             solr_response_fixture('scoop-sharedlib', SchemaType.select)
#         )
#
#         validate_scoop_jsonschema(response_json, schema)
#
#
#     def test_metadata_response(self):
#         fake_solr_response = solr_response_fixture('scoop-sharedlib', SchemaType.select)
#
#         response_json = format_catalog_items_response(self.session, fake_solr_response)
#
#         self.assertDictEqual(
#             response_json['metadata']['resultset'],
#             {
#                 'offset': 0,
#                 'limit': 20,
#                 'count': 653
#             }
#         )
#
#     def test_facets_response(self):
#         fake_solr_response = solr_response_fixture('scoop-sharedlib', SchemaType.select)
#         response_json = format_catalog_items_response(self.session, fake_solr_response)
#         self.assertItemsEqual(
#             response_json['metadata']['facets'],
#             [{
#                 'field_name': 'item_categories',
#                 'values':[
#                     {'value': 'School', 'count': 103, },
#                     {'value': 'Politics & Current Affairs', 'count': 47, },
#                 ]
#             },
#             {
#                 'field_name': 'item_authors',
#                 'values': [
#                     {'value': 'Togi', 'count': 9, },
#                     {'value': 'Ramdani', 'count': 8, },
#                 ]
#             },
#             {
#                 'field_name': 'item_languages',
#                 'values': [
#                     {'value': 'ind', 'count': 646, },
#                     {'value': 'eng', 'count': 7, }
#                 ]
#             }]
#         )
#
#     def test_spelling_suggestion_response(self):
#         fake_solr_response = solr_response_fixture('scoop-sharedlib-spellsuggestion', SchemaType.select)
#         response_json = format_catalog_items_response(self.session, fake_solr_response)
#         self.assertItemsEqual(
#             response_json['metadata']['spelling_suggestions'],
#             [
#                 {'value': 'jakarta', 'count': 1660, },
#                 {'value': 'jakarta!', 'count': 1, }
#             ]
#         )
#
#
