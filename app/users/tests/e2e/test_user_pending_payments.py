from __future__ import unicode_literals, absolute_import
from datetime import datetime

from marshmallow.utils import isoformat

from app.offers.models import Offer
from app.orders.models import Order, OrderLine
from app.payment_gateways.models import PaymentGateway
from app.paymentgateway_connectors.creditcard.models import PaymentVeritrans
from app.payments.models import Payment
from app.services.elevenia.entities.choices.result_status import OK
from app.utils.testcases import E2EBase


ONE_MILLION = 1000000
ONE_HUNDRED = 100


class UserPendingPaymentTests(E2EBase):

    def setUp(self):
        super(UserPendingPaymentTests, self).setUp()

        VERITRANS = self.session.query(PaymentGateway).get(PaymentGateway.Type.veritrans.value)

        self.session.add(self.regular_user)

        self.regular_user.orders.append(
            Order(
                order_number=123456789,
                total_amount=ONE_MILLION,
                final_amount=ONE_MILLION,
                order_status=Order.Status.waiting_for_payment.value,
                currency_code='IDR',
                is_active=True,
                paymentgateway=VERITRANS,
                payments=[
                    Payment(
                        paymentgateway=VERITRANS,
                        currency_code='USD',
                        amount=ONE_HUNDRED,
                        payment_status=Payment.Status.waiting.value,
                        payment_datetime=datetime.now(),
                        payment_veritrans=[
                            PaymentVeritrans(
                                transaction_id=1234,
                                payment_code='ABCD'
                            )
                        ]
                    )
                ],
                order_lines=[
                    OrderLine(
                        name='Some Offer',
                        offer=Offer(
                            name='Some Offer',
                            long_name='Some Offer',
                            offer_status=Offer.Status.ready_for_sale.value,
                            sort_priority=1,
                            is_active=True,
                            offer_type_id=Offer.Type.single.value,
                            price_usd=ONE_HUNDRED,
                            price_idr=ONE_MILLION,
                            price_point=ONE_HUNDRED,
                            is_discount=False,
                            items=[],
                            brands=[],
                            vendors=[]
                        ),
                        is_active=True,
                        is_free=False,
                        is_discount=False,
                        user_id=self.regular_user.id,
                        orderline_status=OrderLine.Status.new.value,
                        currency_code='IDR',
                        price=ONE_MILLION,
                        final_price=ONE_MILLION,
                        quantity=1,
                        localized_final_price=ONE_HUNDRED,
                        localized_currency_code='USD'
                    )
                ]
            )
        )
        self.session.commit()

    def test_pending_with_payment_id(self):
        """ BASIC: payment gateways (like veritrans) include a payment_code field.
        """
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/users/current-user/pending-transactions',
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, OK)
        self.session.add(self.regular_user)

        pending = []
        for order in self.regular_user.orders:
            self.session.add(order)
            self.session.add(order.paymentgateway)
            line_items = []
            for line_item in order.order_lines:
                line_items.append({
                    'title': line_item.name,
                    'href': self.url_for('offers.offers', offer_id=line_item.offer_id),
                    'id': line_item.id,
                    'offer_id': line_item.offer_id,
                    'price': {
                        'final': ONE_HUNDRED,
                        'currency': 'USD'
                    }
                })
            pending.append({
                'title': 'Pending Order',
                'href': self.url_for('orders.orders', order_id=order.id),
                'order_id': order.id,
                'date': isoformat(order.created),
                'order_number': order.order_number,
                'payment_gateway': {
                    'id': order.paymentgateway.id,
                    'title': order.paymentgateway.name,
                    'href': self.url_for(
                        'payment_gateways.payment_gateway',
                        gateway_id=order.paymentgateway.id)
                },
                'payment_code': 'ABCD',
                'price': {
                    'final': ONE_HUNDRED,
                    'currency': 'USD',
                },
                'line_items': line_items
            })

        self.assertUnorderedJsonEqual(
            response,
            {
                'pending': pending,
                'metadata': {
                    'count': 1,
                    'offset': 0,
                    'limit': 20
                }

            }
        )
