from __future__ import absolute_import, unicode_literals

from httplib import NOT_FOUND, OK, CONFLICT, BAD_REQUEST

from app.utils.testcases import E2EBase


class SignupApiTest(E2EBase):

    def test_root(self):
        """ BASIC: signup-check root returns a list of possible attribute check endpoints.
        """
        response = self.api_client.get('/v1/users/signup-check', headers=self.build_headers())

        self.assertEqual(response.status_code, OK)

        self.assertUnorderedJsonEqual(
            response,
            {
                "urls": [
                    self.url_for('signup_checks.validate_attribute', attribute_name='email'),
                ]
            }
        )

    def test_invalid_field_name(self):
        response = self.api_client.get(
            '/v1/users/signup-check/some_invalid_field?value=abc',
            self.build_headers()
        )
        self.assertEqual(response.status_code, NOT_FOUND)
        self.assertScoopErrorFormat(response)

    def test_unused_email_address(self):
        response = self.api_client.get(
            '/v1/users/signup-check/email?value=random-email@apps-foundry.com',
            self.build_headers()
        )
        self.assertEqual(response.status_code, OK)
        self.assertEqual(response.data, "")

    def test_existing_email_address(self):
        response = self.api_client.get(
            '/v1/users/signup-check/email?value={}'.format(self.super_admin.email),
            self.build_headers()
        )
        self.assertEqual(response.status_code, CONFLICT)
        self.assertScoopErrorFormat(response)

    def test_missing_value_query_param(self):
        response = self.api_client.get(
            '/v1/users/signup-check/email',
            self.build_headers()
        )
        self.assertEqual(response.status_code, BAD_REQUEST)
        self.assertScoopErrorFormat(response)

