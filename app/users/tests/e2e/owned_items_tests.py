from __future__ import unicode_literals, absolute_import

from httplib import OK, NOT_MODIFIED

from marshmallow.utils import isoformat

from app import app, db
from app.items.models import Item
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import UserItemFactory, ItemFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, standard_offer_types


def get_owned_item_format(item):
    return {
        "id": item.id,
        "href": item.api_url,
        "title": item.name,
        "image": {
            "title": "Cover Image",
            "href": app.config['MEDIA_BASE_URL'] + item.image_highres
        },
        "thumbnail": {
            "title": "Thumbnail Image",
            "href": app.config['MEDIA_BASE_URL'] + item.thumb_image_highres
        },
        "is_age_restricted": item.parentalcontrol_id == 2,
        "content_type": item.content_type,
        "item_type": item.item_type,
        "edition_code": item.edition_code,
        "issue_number": item.issue_number,
        "release_date": isoformat(item.release_date),
        # "authors": [
        #     {
        #         "id": a.id,
        #         "title": a.name,
        #         "href": a.api_url
        #     } for a in item.authors
        # ],
        "brand": {
            "id": item.brand.id,
            "title": item.brand.name,
            "href": item.brand.api_url,
            "vendor": {
                "id": item.brand.vendor.id,
                "title": item.brand.vendor.name,
                "href": item.brand.vendor.api_url
            }
        }
    }


class UserOwnedItemsTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(UserOwnedItemsTests, cls).setUpClass()
        s = db.session()
        item_a = ItemFactory(
            name='Kompas / 24 JAN 2017',
            item_type=Item.Types.book.value,)
        cls.owned_a = UserItemFactory(
            user_id=cls.regular_user.id,
            item=item_a
        )
        item_b = ItemFactory(
            name='Bola / 25 JAN 2017',
            item_type=Item.Types.magazine.value, )
        cls.owned_b = UserItemFactory(
            user_id=cls.regular_user.id,
            item=item_b
        )
        cls.not_owned_a = UserItemFactory(user_id=cls.super_admin.id)
        cls.offer_a = OfferFactory(offer_type=standard_offer_types()['single'])
        cls.offer_a.items.append(cls.owned_a.item)
        s.commit()

    def test_by_user_id(self):

        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/users/{0.id}/owned-items'.format(self.regular_user),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, OK)
        self.session.add(self.owned_a)
        self.session.add(self.owned_b)
        self.assert_response_body(response, expected_items=[self.owned_a.item, self.owned_b.item, ])

    def test_current_user(self):

        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/users/current-user/owned-items',
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, OK)
        self.session.add(self.owned_a)
        self.session.add(self.owned_b)
        self.assert_response_body(response, expected_items=[self.owned_a.item, self.owned_b.item, ])

    def test_by_brand_id(self):
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/users/{0.id}/owned-items?brand_id={1}'.format(self.regular_user, self.owned_a.item.brand_id),
            headers=self.build_headers()
        )
        self.assertEqual(response.status_code, OK)
        self.session.add(self.owned_a)
        self.assert_response_body(response, expected_items=[self.owned_a.item, ])

    def test_by_vendor_id(self):
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/users/{0.id}/owned-items?vendor_id={1}'.format(self.regular_user, self.owned_a.item.brand.vendor_id),
            headers=self.build_headers()
        )
        self.assertEqual(response.status_code, OK)
        self.session.add(self.owned_a)
        self.assert_response_body(response, expected_items=[self.owned_a.item, ])

    def test_by_item_type(self):
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/users/{user.id}/owned-items?item_type={item_type}'.format(
                user=self.regular_user,
                item_type=Item.Types.book.value),
            headers=self.build_headers()
        )
        self.assertEqual(response.status_code, OK)
        self.session.add(self.owned_a)
        self.assert_response_body(response, expected_items=[self.owned_a.item, ])

    def test_by_item_name(self):
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/users/{user.id}/owned-items?q={filter_by_name}'.format(
                user=self.regular_user,
                filter_by_name='kompas'),
            headers=self.build_headers()
        )
        self.assertEqual(response.status_code, OK)
        self.session.add(self.owned_a)
        self.assert_response_body(response, expected_items=[self.owned_a.item, ])

    def test_by_remote_offer_id(self):
        self.set_api_authorized_user(self.regular_user)
        self.session.add(self.offer_a)
        response = self.api_client.get(
            '/v1/users/{0.id}/owned-items?remote_item_id=SC00P{1}'.format(
                self.regular_user, self.offer_a.id),
            headers=self.build_headers()
        )
        self.assertEqual(response.status_code, OK)
        self.session.add(self.offer_a)
        self.assert_response_body(response, expected_items=self.offer_a.items.all())

    def assert_response_body(self, response, expected_items):
        items = []
        with app.test_request_context():
            for item in expected_items:
                items.append(get_owned_item_format(item))
            self.assertUnorderedJsonEqual(
                response,
                {
                    "items": items,
                    "metadata": {
                        "resultset": {
                            "limit": 20,
                            "offset": 0,
                            "count": len(expected_items)
                        }
                    }
                }
            )

    def test_etag_support(self):

        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/users/current-user/owned-items',
            headers=self.build_headers()
        )

        self.assertIn('ETag', response.headers)

        etag = response.headers['ETag']
        not_modified_headers = self.build_headers()
        not_modified_headers['If-None-Match'] = etag

        response_not_modified = self.api_client.get(
            '/v1/users/current-user/owned-items',
            headers=not_modified_headers
        )

        self.assertEqual(response_not_modified.status_code, NOT_MODIFIED)
