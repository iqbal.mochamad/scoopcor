from __future__ import absolute_import, unicode_literals
from collections import defaultdict

from app.constants import RoleType
from app.users.organizations import OrganizationUser, OrganizationType
from app.utils.testcases import E2EBase
from app.users.tests.fixtures import OrganizationFactory
from app.eperpus.tests.fixtures import SharedLibraryFactory


class OrganizationE2EBase(E2EBase):

    # set this to use a specific organization type for all tests
    force_organization_fixture_type = None

    __organization_factory = defaultdict(
        lambda: OrganizationFactory,
    )
    __organization_factory.update({
        OrganizationType.shared_library: SharedLibraryFactory,
    })

    @property
    def organization_factory(self):
        return self.__organization_factory[self.force_organization_fixture_type]

    def setUp(self):
        super(OrganizationE2EBase, self).setUp()

        organization = self.organization_factory()
        self.organization_abc = self.organization_factory()

        self.organization_manager = self.create_user(RoleType.verified_user)
        self.session.add(
            OrganizationUser(
                organization_id=organization.id,
                user_id=self.organization_manager.id,
                is_manager=True
            ))
        self.session.add(
            OrganizationUser(
                organization_id=self.organization_abc.id,
                user_id=self.organization_manager.id,
                is_manager=True
            ))

        self.organization_member = self.create_user(RoleType.verified_user)
        self.session.add(
            OrganizationUser(
                organization_id=organization.id,
                user_id=self.organization_member.id,
                is_manager=False
            )
        )
        self.organization_member_unverified = self.create_user(RoleType.unverified_user)
        self.organization_member_unverified.username = self.organization_member_unverified.email
        self.organization_member_unverified.first_name = ""
        self.organization_member_unverified.last_name = ""
        self.organization_member_unverified.is_verified = False
        self.session.add(self.organization_member_unverified)
        self.session.add(
            OrganizationUser(
                organization_id=organization.id,
                user_id=self.organization_member_unverified.id,
                is_manager=False
            )
        )

        self.session.commit()
        self.session.refresh(organization)
        self.session.expunge(organization)
        self.organization = organization

        self.child_organization = self.organization_factory(parent_organization_id=self.organization.id)
        self.session.commit()
        self.session.refresh(self.child_organization)
        self.session.expunge(self.child_organization)

    def assign_user_to_organization(self, user, organization, is_manager=False):
        self.session.add(
            OrganizationUser(
                organization_id=organization.id,
                user_id=user.id,
                is_manager=is_manager
            ))
        self.session.commit()

    def assertUserIsOrganizationMember(self, user, organization):
        self.failUnless(bool(self.session.query(OrganizationUser).filter_by(
            organization_id=organization.id,
            user_id=user.id
        ).count()))

    def assertUserIsNotOrganizationMember(self, user, organization):
        self.failIf(bool(self.session.query(OrganizationUser).filter_by(
            organization_id=organization.id,
            user_id=user.id
        ).count()))


def load_tests(loader, tests, pattern):
    # don't load any tests from this module, as they're all base tests
    import unittest
    suite = unittest.TestSuite()
    return suite
