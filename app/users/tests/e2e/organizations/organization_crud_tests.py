from __future__ import unicode_literals

import json
from httplib import OK
from unittest import SkipTest

from faker import Factory
from nose.tools import istest

from app import app, db
from app.auth.helpers import jwt_encode_from_user
from app.users.tests.fixtures import OrganizationFactory, UserFactory
from app.users.users import User
from app.utils.testcases import TestBase
from tests import testcases


@istest
class ListTest(testcases.ListTestCase):

    maxDiff = None
    request_url = '/v1/organizations'
    list_key = 'organizations'
    fixture_factory = OrganizationFactory

    @classmethod
    def setUpClass(cls):
        ctx = app.test_request_context('/')
        ctx.push()
        s = db.session()
        user = s.query(User).get(USER_ID)
        cls.request_headers['Authorization'] = 'JWT {}'.format(
            jwt_encode_from_user(user)
        )
        super(ListTest, cls).setUpClass()

    def test_response_code(self):
        self.assertEqual(OK, self.response.status_code,
                         'Response status code is not OK, response: {}'.format(self.response.data))

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        self.assertDictContainsSubset(
            get_expected_response(fixture),
            fixture_json,
        )


def get_expected_response(fixture):
    return {
        'id': fixture.id,
        'name': fixture.name,
        'type': fixture.type,
        'href': fixture.api_url,
        'is_active': fixture.is_active
    }


@istest
class ListEperpusTest(testcases.ListTestCase):

    maxDiff = None
    request_url = '/v1/organizations?eperpus-id=711'
    list_key = 'organizations'
    fixture_factory = None

    @classmethod
    def setUpClass(cls):
        ctx = app.test_request_context('/')
        ctx.push()
        s = db.session()
        user = s.query(User).get(USER_ID)
        cls.request_headers['Authorization'] = 'JWT {}'.format(
            jwt_encode_from_user(user)
        )
        cls.session = db.session()
        super(ListEperpusTest, cls).setUpClass()

    @classmethod
    def remove_fixtures_from_session(cls):
        pass

    @classmethod
    def set_up_fixtures(cls):
        cls.eperpus = OrganizationFactory(id=711)
        cls.eperpus_child1 = OrganizationFactory(id=712, parent_organization_id=711)
        cls.eperpus_child2 = OrganizationFactory(id=713, parent_organization_id=711)
        cls.eperpus_grand_child1 = OrganizationFactory(id=7121, parent_organization_id=712)
        expected_eperpus = [cls.eperpus, cls.eperpus_child1, cls.eperpus_child2,
                            cls.eperpus_grand_child1]
        cls.session.commit()
        return expected_eperpus

    @classmethod
    def clean_database(cls):
        cls.session.delete(cls.eperpus_grand_child1)
        cls.session.delete(cls.eperpus_child1)
        cls.session.delete(cls.eperpus_child2)
        cls.session.delete(cls.eperpus)
        cls.session.commit()
        cls.session.expunge_all()

    def test_response_code(self):
        self.assertEqual(OK, self.response.status_code,
                         'Response status code is not OK, response: {}'.format(self.response.data))

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        self.assertDictContainsSubset(
            get_expected_response(fixture),
            fixture_json,
        )


@istest
class GetDetailsTest(testcases.DetailTestCase):

    maxDiff = None

    request_url = '/v1/organizations/{0.id}'
    fixture_factory = OrganizationFactory

    @classmethod
    def setUpClass(cls):
        ctx = app.test_request_context('/')
        ctx.push()
        s = db.session()
        user = s.query(User).get(USER_ID)
        cls.request_headers['Authorization'] = 'JWT {}'.format(
            jwt_encode_from_user(user)
        )
        super(GetDetailsTest, cls).setUpClass()

    def test_response_body(self):

        self.assertDictContainsSubset(
            get_expected_response(self.fixture),
            json.loads(self.response.data),
        )


_fake = Factory.create()
USER_ID = 12345


def setup_module():
    # make sure the test mode flag is set, or bail out
    if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
        raise SkipTest("COR is not in testing mode.  Cannot continue.")

    db.session().close_all()
    db.drop_all()
    db.create_all()
    s = db.session()
    user = UserFactory(id=USER_ID)
    s.add(user)
    s.commit()


def teardown_module():
    db.session().close_all()
    db.drop_all()
