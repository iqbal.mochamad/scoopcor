from __future__ import absolute_import, unicode_literals

import json

import httpretty
from faker import Factory
from httplib import CREATED, NO_CONTENT, FORBIDDEN, CONFLICT, OK

from app import db
from app.users.organizations import post_user_organization_data_to_solr, OrganizationUser
from app.users.users import User
from app.users.tests.e2e.base import OrganizationE2EBase


class OrganizationUserTests(OrganizationE2EBase):

    def setUp(self):
        db.engine.execute("drop view if exists utility.users_solr_dih;")
        super(OrganizationUserTests, self).setUp()
        db.engine.execute("""CREATE OR REPLACE VIEW utility.users_solr_dih AS
        SELECT cas_users.id,
            cas_users.username,
            cas_users.first_name,
            cas_users.last_name,
            cas_users.email,
            cas_users.note,
            ARRAY( SELECT cas_users_organizations.organization_id
                   FROM cas_users_organizations
                  WHERE cas_users_organizations.user_id = cas_users.id) AS organization_ids,
            cas_users.is_active,
            cas_users.created AS signup_date,
            cas_users.modified
           FROM cas_users;
        """)

    def tearDown(self):
        db.engine.execute("drop view if exists utility.users_solr_dih;")
        super(OrganizationUserTests, self).tearDown()

    def api_add_organization_user(self, organization, user):
        return self.api_client.open(
            '/v1/organizations/{}/users'.format(organization.id),
            method='LINK',
            data=json.dumps({'id': user.id}),
            headers=self.build_headers(has_body=True))

    def api_remove_organization_user(self, organization, user):
        return self.api_client.open(
            '/v1/organizations/{}/users'.format(organization.id),
            method='UNLINK',
            data=json.dumps({'id': user.id}),
            headers=self.build_headers(has_body=True))

    @httpretty.activate
    def test_get_users(self):
        self.assign_user_to_organization(self.organization_member, self.child_organization)

        self.set_api_authorized_user(self.organization_manager)
        # test query direct from database
        response = self.api_client.get(
            '/v1/organizations/{}/users?direct-db=1'.format(self.organization.id),
            headers=self.build_headers())
        self.assertEqual(response.status_code, OK,
                         "response status code != 200(OK). Response {}".format(response.data))

        # test query from solr
        url = "http://localhost:8983/solr/scoop-users/select?" \
              "fq=organization_ids:{}&start=0&rows=20&facet=on&facet.field=is_active&q=**&wt=json".format(
                self.organization.id)
        httpretty.register_uri(
            httpretty.GET,
            url,
            body=json.dumps(self._mock_solr_response(self.organization)))

        self.set_api_authorized_user(self.organization_manager)
        response = self.api_client.get(
            '/v1/organizations/{}/users'.format(self.organization.id),
            headers=self.build_headers())
        self.assertEqual(response.status_code, OK,
                         "response status code != 200(OK). Response {}".format(response.data))

    def _mock_solr_response(self, org):
        users = []
        user_in_org_data = self.session.query(User).filter(User.is_active == True).join(OrganizationUser).filter_by(
            organization=org,
        )
        for user in user_in_org_data:
            users.append({'id': user.id})

        response = {
            "response": {
                "docs": users,
                "numFound": len(users),
            },
            "facet_counts": {
                "facet_queries": {},
                ""
                "facet_fields": {
                    "item_categories": [
                        "Automotive", 2,
                        "Crime & Thrillers", 1
                    ],
                    "author_names": [
                        "ramdani", 2
                    ],
                    "item_languages": [
                        "id", 2,
                        "en", 0
                    ]
                }
            },
            "spellcheck": {
                "suggestions": [
                    "sedekha", {
                        "numFound": 2,
                        "startOffset": 0,
                        "endOffset": 7,
                        "origFreq": 0,
                        "suggestion": [
                            {
                                "word": "sedekah",
                                "freq": 8
                            },
                            {
                                "word": "selekta",
                                "freq": 1
                            }
                        ]
                    }
                ],
                "correctlySpelled": 'false'
            }
        }

        return response

    def test_get_users_details(self):
        self.assign_user_to_organization(self.organization_member, self.child_organization)

        self.set_api_authorized_user(self.organization_manager)
        # test query direct from database
        response = self.api_client.get(
            '/v1/organizations/{}/users/{}'.format(self.organization.id, self.organization_member.id),
            headers=self.build_headers())
        self.assertEqual(response.status_code, OK,
                         "response status code != 200(OK). Response {}".format(response.data))

    def test_create_user(self):
        # Test for POST: /v1/organizations/{org_id}/users
        # test create new user for an organizations
        # given
        new_user_data = {
            'first_name': _fake.name(),
            'last_name': _fake.name(),
            # test input with upper, later should be converted to lower on user registration
            "username": _fake.email().upper(),
            "email": _fake.email().upper(),
            "password": "5d981e9beea184962ab8399724375b4ea4e79265",
        }

        self.set_api_authorized_user(self.organization_manager)
        headers = self.build_headers(has_body=True)
        headers['X-Scoop-Client'] = 'eperpus ios/1.0.0'
        headers['User-Agent'] = 'eperpus ios/1.0.0'

        # when
        response = self.api_client.post(
            '/v1/organizations/{}/users'.format(self.organization.id),
            data=json.dumps(new_user_data), headers=headers)

        # then
        self.assertEqual(response.status_code, CREATED,
                         'response status code is not CREATED. response {}'.format(response.data))
        actual = json.loads(response.data)
        self.assertIsNotNone(actual)
        actual_user = db.session.query(User).get(actual.get('id'))
        self.assertIsNotNone(actual_user)
        self.assertEqual(actual_user.username, new_user_data.get('username').lower())
        self.assertEqual(actual_user.email, new_user_data.get('email').lower())
        self.assertEqual(actual_user.first_name, new_user_data.get('first_name'))
        self.assertEqual(actual_user.last_name, new_user_data.get('last_name'))
        self.assertIn(self.organization.id, [org.id for org in actual_user.organizations])
        # db.session.expunge(actual_user)

        # TEST: user already exists, but not yet registered to this organization, POST method should allowed!
        #  and user should added to this new organization
        # given
        with self.on_session(self.organization_abc, self.organization_member_unverified):
            new_org_id = self.organization_abc.id
            new_user_data = {
                'first_name': _fake.name(),
                'last_name': _fake.name(),
                "username": "BCA123456",
                "email": self.organization_member_unverified.email.upper(),
                "password": "5d981e9beea184962ab8399724375b4ea4e79265",
            }

        self.set_api_authorized_user(self.organization_manager)
        headers = self.build_headers(has_body=True)
        headers['X-Scoop-Client'] = 'eperpus ios/1.0.0'
        headers['User-Agent'] = 'eperpus ios/1.0.0'

        # when
        response = self.api_client.post(
            '/v1/organizations/{}/users'.format(new_org_id),
            data=json.dumps(new_user_data), headers=headers)

        # then
        self.assertEqual(response.status_code, CREATED,
                         'response status code is not CREATED. response {}'.format(response.data))
        actual = json.loads(response.data)
        self.assertIsNotNone(actual)
        actual_user = db.session.query(User).get(actual.get('id'))
        self.assertEqual(actual_user.username, new_user_data.get('username').lower())
        self.assertEqual(actual_user.email, new_user_data.get('email').lower())
        self.assertEqual(actual_user.first_name, new_user_data.get('first_name'))
        self.assertEqual(actual_user.last_name, new_user_data.get('last_name'))
        self.assertIn(new_org_id, [org.id for org in actual_user.organizations])

    def test_update_organization_user(self):
        # PUT: /v1/organizations/{org_id}/users/{user_id}
        # Test Update a member/user of an organization
        # given:
        update_data = {
            'first_name': _fake.name(),
            'last_name': _fake.name(),
            'username': self.organization_member.username,
            'email': self.organization_member.email,
            'profile': {
                'birthdate': None,
                'gender': "1",
                'origin_country_code': None,
                'origin_device_model': None,
                'origin_latitude': None,
                'origin_longitude': None,
                'origin_partner_id': None,
                'source_type': None
            },
            # "profile": {"birthdate": "1970-01-01T07:00:00", "gender": "1", "origin_country_code": None,
            #             "origin_device_model": "Firefox 45.0", "origin_latitude": None, "origin_longitude": None,
            #             "origin_partner_id": None, "source_type": 1}
        }

        self.set_api_authorized_user(self.organization_manager)
        # when
        response = self.api_client.put(
            '/v1/organizations/{}/users/{}'.format(self.organization.id, self.organization_member.id),
            data=json.dumps(update_data),
            headers=self.build_headers(has_body=True))

        # then
        self.assertEqual(response.status_code, OK, response.data)
        actual = json.loads(response.data)
        profile = update_data.pop("profile", {})
        self.assertDictContainsSubset(update_data, actual)
        # self.assertDictContainsSubset(profile, actual.get('profile', {}))
        user_data = User.query.get(self.organization_member.id)
        self.assertEqual(user_data.first_name, update_data.get('first_name'))
        self.assertEqual(user_data.last_name, update_data.get('last_name'))
        self.assertEqual(user_data.allow_age_restricted_content, self.organization_member.allow_age_restricted_content)

        # update/change email but user already verified, not allowed
        # given
        update_data = {
            'first_name': _fake.name(),
            'last_name': _fake.name(),
            'username': self.organization_member.username,
            'email': self.organization_manager.email,
        }

        self.set_api_authorized_user(self.organization_manager)
        # when
        response = self.api_client.put(
            '/v1/organizations/{}/users/{}'.format(self.organization.id, self.organization_member.id),
            data=json.dumps(update_data),
            headers=self.build_headers(has_body=True))
        # then
        self.assertEqual(response.status_code, CONFLICT, response.data)

        # update/change email but new email already exists, not allowed
        # given
        with self.on_session(self.organization_member_unverified):
            user_id = self.organization_member_unverified.id
            email = self.organization_member_unverified.email
            update_data = {
                'first_name': _fake.name(),
                'last_name': _fake.name(),
                'username': self.organization_member_unverified.username,
                'email': self.organization_manager.email,
            }

        self.set_api_authorized_user(self.organization_manager)
        # when
        response = self.api_client.put(
            '/v1/organizations/{}/users/{}'.format(self.organization.id, user_id),
            data=json.dumps(update_data),
            headers=self.build_headers(has_body=True))
        # then
        self.assertEqual(response.status_code, CONFLICT, response.data)

        # update username but already exists, not allowed
        # given
        update_data = {
            'first_name': _fake.name(),
            'last_name': _fake.name(),
            'username': self.organization_manager.username,
            'email': email,
        }

        self.set_api_authorized_user(self.organization_manager)
        # when
        response = self.api_client.put(
            '/v1/organizations/{}/users/{}'.format(self.organization.id, user_id),
            data=json.dumps(update_data),
            headers=self.build_headers(has_body=True))
        # then
        self.assertEqual(response.status_code, CONFLICT, response.data)

    def test_can_add_user(self):
        self.set_api_authorized_user(self.organization_manager)

        response = self.api_add_organization_user(self.child_organization, self.organization_member)

        self.assertEqual(response.status_code, OK)
        self.assertUserIsOrganizationMember(self.organization_member, self.child_organization)

    def test_can_remove_user(self):
        """ Basic functionality.  A `User` can be 'removed' from an `Organization`, without removing them from SCOOP.
        """
        self.assign_user_to_organization(self.organization_member, self.child_organization)

        self.set_api_authorized_user(self.organization_manager)

        response = self.api_remove_organization_user(self.child_organization, self.organization_member)

        self.assertEqual(response.status_code, OK, msg='Successful request should be 200 OK')
        self.assertUserIsNotOrganizationMember(self.organization_member, self.child_organization)

        self.assertEqual(
            self.session.query(User).filter_by(id=self.organization_member.id).count(),
            1,
            msg='User should not be removed from SCOOP')

    def test_non_managers_cannot_add_users(self):
        """ SECURITY TEST:  Users must be a manager of an organization to add children.
        """
        self.set_api_authorized_user(self.organization_member)

        response = self.api_add_organization_user(self.child_organization, self.organization_member)

        self.assertEqual(response.status_code, FORBIDDEN)
        self.assertUserIsNotOrganizationMember(self.organization_member, self.child_organization)

    def test_removing_users_from_parent_removes_from_children(self):
        """ When a `User` is removed from a parent `Organization`, they should automatically
         be removed from all children of that `Organization`s.
        """
        self.assign_user_to_organization(self.organization_member, self.child_organization)

        self.set_api_authorized_user(self.organization_manager)

        response = self.api_remove_organization_user(self.organization, self.organization_member)

        self.assertEqual(response.status_code, OK)
        self.assertUserIsNotOrganizationMember(self.organization_member, self.organization)
        self.assertUserIsNotOrganizationMember(self.organization_member, self.child_organization)

    def test_cannot_add_non_parent_members_to_child(self):
        """ If a `User` isn't a member of a parent `Organization`, they cannot be added to a child.
        """
        self.set_api_authorized_user(self.organization_manager)

        response = self.api_add_organization_user(self.child_organization, self.regular_user)

        self.assertEqual(response.status_code, CONFLICT)
        self.assertUserIsNotOrganizationMember(self.regular_user, self.child_organization)

    @httpretty.activate
    def test_post_user_organization_data_to_solr(self):
        url = "http://localhost:8983/solr/scoop-users/update/json/docs?commit=true"
        httpretty.register_uri(
            httpretty.POST,
            url,
            body=json.dumps({"ok": "ok"}))

        response = post_user_organization_data_to_solr(self.regular_user.id)
        db.engine.execute("drop view utility.users_solr_dih;")
        self.assertEqual(response.get('status_code', None), OK)


_fake = Factory.create()
