from __future__ import absolute_import, unicode_literals
from httplib import CREATED, FORBIDDEN, NO_CONTENT, METHOD_NOT_ALLOWED, OK
import json

from app.eperpus.tests.fixtures import SharedLibraryFactory
from app.users.organizations import Organization
from app.users.tests.e2e.base import OrganizationE2EBase
from app.users.tests.fixtures import OrganizationFactory


class OrganizationChildrenTests(OrganizationE2EBase):

    def api_create_child(self, parent_organization, data):
        return self.api_client.post(
            '/v1/organizations/{org.id}/child-organizations'.format(org=parent_organization),
            data=json.dumps(data),
            headers=self.build_headers(has_body=True)
        )

    def api_delete_organization(self, organization):
        return self.api_client.delete(
            '/v1/organizations/{org.id}'.format(org=organization),
            headers=self.build_headers()
        )

    def test_create_child(self):
        """ BASIC: `Organizations` can add children.
        """
        self.set_api_authorized_user(self.organization_manager)

        response = self.api_create_child(self.organization, {'name': 'Some Child Organization'})

        self.assertEqual(response.status_code, CREATED,
                         "response status code != 201 (CREATED). resp: {}".format(response.data))

        created_child = (
            self.session
                .query(Organization)
                .filter_by(parent_organization_id=self.organization.id,
                           name='Some Child Organization')
                .first())
        self.assertIsNotNone(created_child)
        self.assertEqual(created_child.name, 'Some Child Organization')
        self.assertEqual(created_child.slug, 'some-child-organization')

    def test_update_child(self):
        """ BASIC: `Organizations` can update children.
        """
        shared_lib = SharedLibraryFactory()
        self.session.commit()
        self.session.refresh(shared_lib)
        id = shared_lib.id

        self.set_api_authorized_user(self.organization_manager)

        response = self.api_client.put(
            '/v1/organizations/{parent_id}/child-organizations/{id}'.format(
                parent_id=self.organization.id, id=id),
            data=json.dumps({'name': 'Some Child Organization 2'}),
            headers=self.build_headers(has_body=True)
        )
        self.assertEqual(response.status_code, OK,
                         "response status code != 200 (OK). resp: {}".format(response.data))
        updated_child = (
            self.session
                .query(Organization)
                .get(id))
        self.assertIsNotNone(updated_child)
        actual_name = updated_child.name
        self.session.expunge(updated_child)
        self.assertEqual(actual_name, 'Some Child Organization 2')

    def test_children_cannot_have_children(self):
        """ BUSINESS RULE:  Only an `Organization` with no parent can have children.
        """
        child = OrganizationFactory(parent_organization_id=self.organization.id)
        self.session.commit()

        self.set_api_authorized_user(self.organization_manager)

        response = self.api_create_child(child, {'name': 'Some other child organization'})

        self.assertEqual(response.status_code, METHOD_NOT_ALLOWED)

    def test_only_managers_can_create_children(self):
        """ SECURITY:  `User`s that are not `Organization` managers, cannot add children.
        """
        self.set_api_authorized_user(self.organization_member)

        response = self.api_create_child(self.organization, {'name': 'A child org that should fail.'})

        self.assertEqual(response.status_code, FORBIDDEN)

    def test_child_delete(self):
        """ BASIC:  HTTP delete on a child organization should perform hard delete.
        """
        self.set_api_authorized_user(self.organization_manager)

        response = self.api_delete_organization(self.child_organization)
        self.assertEqual(response.status_code, NO_CONTENT)

        child_exists = bool(self.session.query(Organization).get(self.child_organization.id))
        self.assertFalse(child_exists)

    def test_only_managers_can_delete_children(self):
        """ SECURITY: `User`s that are not `Organization` managers, cannot delete children.
        """
        self.set_api_authorized_user(self.organization_member)

        response = self.api_delete_organization(self.child_organization)

        self.assertEqual(response.status_code, FORBIDDEN)

    def test_managers_cannot_delete_parent_organizations(self):
        """ SECURITY: `Organization` managers **cannot** delete their own parent organization.
        """
        self.set_api_authorized_user(self.organization_manager)

        response = self.api_delete_organization(self.organization)

        self.assertEqual(response.status_code, FORBIDDEN)
