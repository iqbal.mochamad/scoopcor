from __future__ import unicode_literals, absolute_import

import json
from httplib import OK, UNAUTHORIZED, CREATED, NO_CONTENT

import datetime

from faker import Factory
from marshmallow.utils import isoformat

from app import app, db
from app.constants import RoleType
from app.users.tests.e2e.base import E2EBase
from app.users.users import User
from tests.fixtures.sqlalchemy import ItemFactory


class CurrentUserWishListTests(E2EBase):

    url = 'v1/users/current-user/wish-list'

    def setUp(self):
        super(CurrentUserWishListTests, self).setUp()

        for k in range(2):
            item = ItemFactory(id=k+1)
            self.regular_user.wish_list_items.append(item)

        self.session.add(self.regular_user)

        self.new_item1 = ItemFactory(id=100)

        self.session.commit()

        self.regular_user2 = self.create_user(RoleType.verified_user)
        self.session.add(self.regular_user2)
        self.session.commit()

    def test_get(self):
        """ BASIC: Users can fetch their own wish list details.
        """
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(self.url, headers=self.build_headers())

        user = self.session.query(User).get(self.regular_user.id)

        self.assertEqual(response.status_code, OK)

        self.assertListEqual(
            json.loads(response.data).get('items'),
            self.get_expected_response(user)
        )

        response = self.api_client.get('{}?item_id=1'.format(self.url), headers=self.build_headers())
        self.assertEqual(response.status_code, OK)
        actual = json.loads(response.data).get('items')
        self.assertEqual(actual[0].get('id', None), 1)

    def get_expected_response(self, user):
        with app.test_request_context('/'):
            items = []
            for item in user.wish_list_items.all():
                items.append({
                    'id': item.id,
                    'title': item.name,
                    'href': item.api_url,
                })
            return items

    def test_unauthenticated_request(self):
        """ BASIC: Unauthenticated requests return the expected error message.
        """
        response = self.api_client.get(self.url, headers=self.build_headers())

        self.assertEqual(response.status_code, UNAUTHORIZED)

        self.assertScoopErrorFormat(
            response,
            user_message='Unauthorized, You cannot access this resource with the provided credentials.',
            developer_message='You cannot access this resource with the provided credentials.',
            status=401,
            error_code=401
        )

    def test_add_item_to_wish_list(self):
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.open(
            self.url,
            method='LINK',
            data=json.dumps({'id': self.new_item1.id}),
            headers=self.build_headers(has_body=True))

        self.assertEqual(response.status_code, CREATED, response.data)

        self.assertDictEqual(
            json.loads(response.data),
            {"success_message": "Item added"}
        )

        user = self.session.query(User).get(self.regular_user.id)
        self.assertIn(self.new_item1.id, [item.id for item in user.wish_list_items.all()],
                      'Item {} not exists in user wish list'.format(self.new_item1.id))

    def test_add_item_unauthorized(self):
        response = self.api_client.open(
            self.url,
            method='LINK',
            data=json.dumps({'id': self.new_item1.id}),
            headers=self.build_headers(has_body=True))

        self.assertEqual(response.status_code, UNAUTHORIZED, response.data)

    def test_remove_item_to_wish_list(self):
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.open(
            self.url,
            method='UNLINK',
            data=json.dumps({'id': 1}),
            headers=self.build_headers(has_body=True))

        self.assertEqual(response.status_code, NO_CONTENT, response.data)

        user = self.session.query(User).get(self.regular_user.id)
        self.assertNotIn(1, [item.id for item in user.wish_list_items.all()])

    def test_remove_item_unauthorized(self):
        response = self.api_client.open(
            self.url,
            method='UNLINK',
            data=json.dumps({'id': 1}),
            headers=self.build_headers(has_body=True))

        self.assertEqual(response.status_code, UNAUTHORIZED, response.data)


_fake = Factory.create()
