from __future__ import unicode_literals, absolute_import

import json
from httplib import OK, UNAUTHORIZED, CREATED, UNPROCESSABLE_ENTITY

import datetime

from faker import Factory
from marshmallow.utils import isoformat

from app import app
from app.auth.models import Client
from app.constants import RoleType
from app.eperpus.constants import PUBLIC_LIBRARY_CLIENT_ID
from app.users.tests.fixtures_user_buffet import UserBuffetFactory
from app.users.users import User
from app.eperpus.tests.fixtures import SharedLibraryFactory
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy.offers import OfferBuffetFactory
from tests.fixtures.sqlalchemy.orders import OrderLineFactory, OrderFactory


class CurrentUserTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(CurrentUserTests, cls).setUpClass()

        cls.library_org = SharedLibraryFactory()
        cls.child_org = SharedLibraryFactory(parent_organization=cls.library_org)

        cls.session.add(cls.regular_user)
        cls.session.commit()
        cls.offerbuffet = OfferBuffetFactory()
        order = OrderFactory(user_id=cls.regular_user.id)
        cls.session.commit()
        cls.orderline = OrderLineFactory(order=order, offer=cls.offerbuffet.offer,
                                         user_id=cls.regular_user.id)
        cls.session.commit()
        cls.user_buffet = UserBuffetFactory(
            offerbuffet=cls.offerbuffet,
            orderline=cls.orderline,
            user_id=cls.regular_user.id,
            valid_to=datetime.datetime.now() + datetime.timedelta(days=1, hours=3))

        cls.session.commit()
        cls.library_org.users.append(cls.regular_user)
        cls.child_org.users.append(cls.regular_user)

        cls.public_library1 = SharedLibraryFactory(id=1000001, public_library=True)
        cls.public_library2 = SharedLibraryFactory(id=1000002, public_library=True)

        cls.regular_user2 = cls.create_user(RoleType.verified_user)
        cls.session.add(cls.regular_user2)
        cls.session.commit()

        cls.session.refresh(cls.regular_user)
        cls.session.refresh(cls.regular_user2)
        cls.session.refresh(cls.library_org)
        cls.session.refresh(cls.child_org)
        cls.session.refresh(cls.user_buffet)
        cls.session.expunge_all()

    def test_current_user(self):
        """ BASIC: Users can fetch their own account details.
        """
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get('/v1/users/current-user', headers=self.build_headers())

        self.assertEqual(response.status_code, OK)

        actual = self._sort_dict_lists(json.loads(response.data))
        expected = self._sort_dict_lists(self.get_expected_response(self.regular_user))
        self.assertDictContainsSubset(expected, actual)

    def get_expected_response(self, user):
        with app.test_request_context():
            # need a test context here to generate the URLs
            parent_url = self.library_org.api_url
            child_url = self.child_org.api_url
            buffet_url = self.user_buffet.api_url
            self.session.add(self.user_buffet)
            return {
                    'id': user.id,
                    'first_name': user.first_name,
                    'last_name': user.last_name,
                    'roles': [{'href': None, 'id': 10, 'title': 'verified user'}],
                    'organizations': [
                        {
                            'id': self.library_org.id,
                            'name': self.library_org.name,
                            'href': parent_url,
                            'is_manager': False,
                            'type': self.library_org.type,
                            'logo_url': self.library_org.logo_url,
                            'username_prefix': self.library_org.username_prefix,
                        },
                        {
                            'id': self.child_org.id,
                            'name': self.child_org.name,
                            'href': child_url,
                            'is_manager': False,
                            'type': self.child_org.type,
                            'logo_url': self.child_org.logo_url,
                            'username_prefix': self.child_org.username_prefix,
                        }],
                    'is_active': user.is_active,
                    'email': user.email,
                    'username': user.username,
                    'is_verified': user.is_verified,
                    'note': user.note,
                    'profile_pic': {
                        'href': user.profile_pic_url or
                            "https://images.apps-foundry.com/scoop/defaults/user-profile-pic.png",
                        'title': 'Gambar Profil'},
                    'signup_date': isoformat(user.created),
                    'active_buffet': {
                        "href": buffet_url,
                        "title": "Active Buffet Status",
                        "offers": [
                            {
                                "id": user_buffet.offerbuffet.offer_id,
                                "title": user_buffet.offerbuffet.offer.name,
                                "valid_until": isoformat(user_buffet.valid_to),
                            } for user_buffet in [self.user_buffet, ]
                        ],
                        # for compatibility with getscoop, can be removed after getscoop updated
                        "valid_until": isoformat(self.user_buffet.valid_to)
                    },
                    'allow_age_restricted_content': user.allow_age_restricted_content,
                    'level': user.level
                }

    def test_unauthenticated_request(self):
        """ BASIC: Unauthenticated requests return the expected error message.
        """
        self.set_api_authorized_user(None)
        response = self.api_client.get('/v1/users/current-user', headers=self.build_headers())

        self.assertEqual(response.status_code, UNAUTHORIZED)

        self.assertScoopErrorFormat(
            response,
            # user_message='You cannot access this resource with the provided credentials.',
            # developer_message='You cannot access this resource with the provided credentials.',
            status=401,
            error_code=401
        )

    def test_update_user(self):
        self.set_api_authorized_user(self.regular_user2)

        update_data = {
            'first_name': _fake.name(),
            'last_name': _fake.name(),
            'allow_age_restricted_content': not self.regular_user2.allow_age_restricted_content,
        }
        response = self.api_client.put(
            '/v1/users/current-user',
            data=json.dumps(update_data),
            headers=self.build_headers(has_body=True))

        self.assertEqual(response.status_code, OK, response.data)

        user_data = User.query.get(self.regular_user2.id)
        self.assertEqual(user_data.first_name, update_data.get('first_name'))
        self.assertEqual(user_data.allow_age_restricted_content, update_data.get('allow_age_restricted_content'))

        update_data.pop("profile", None)
        self.assertDictContainsSubset(
            update_data, json.loads(response.data),
        )

    def test_register_create_user(self):
        # test user register/create user
        headers = self.build_headers(has_body=True)
        headers['X-Scoop-Client'] = 'scoop web/1.0.0'
        headers['User-Agent'] = 'scoop web/1.0.0'
        self._test_register_create_user(headers)

    def test_register_user_with_invalid_email(self):
        # test user register/create user
        headers = self.build_headers(has_body=True)
        headers['X-Scoop-Client'] = 'scoop web/1.0.0'
        headers['User-Agent'] = 'scoop web/1.0.0'
        new_user_data = self._get_request_body(email="emailinvalid")
        response = self.api_client.post(
            '/v1/users',
            data=json.dumps(new_user_data),
            headers=headers)

        self.assertEqual(response.status_code, UNPROCESSABLE_ENTITY,
                         'response status code is not UNPROCESSABLE_ENTITY. response {}'.format(response.data))

    def test_register_create_user_eperpus(self):
        # test user register/create user from eperpus general & public

        # setup data eperpus public:
        self.session.add(self.public_library1)
        self.session.add(self.public_library2)
        client_eperpus = self.session.query(Client).get(PUBLIC_LIBRARY_CLIENT_ID[0])
        client_eperpus.organizations.append(self.public_library1)
        client_eperpus.organizations.append(self.public_library2)
        self.session.add(client_eperpus)
        self.session.commit()
        self.session.expunge(client_eperpus)
        self.session.expunge(self.public_library1)
        self.session.expunge(self.public_library2)

        headers = self.build_headers(has_body=True)
        headers['X-Scoop-Client'] = 'eperpus ios/1.0.0'
        headers['User-Agent'] = 'eperpus ios/1.0.0'
        saved_user = self._test_register_create_user(headers)

        # validate user - registered to eperpus library 1 and 2
        actual_org_id = [org.id for org in saved_user.organizations]
        self.session.expunge(saved_user)
        self.assertIn(1000001, actual_org_id)
        self.assertIn(1000002, actual_org_id)

    def _test_register_create_user(self, headers, email=None, session=None):
        session = session or self.session
        # comment this mock_stmplib if u want to test sending email to mailtrap.io
        # or other smtp configured in config.py/local_config.py
        mock_smtplib()

        self.set_api_authorized_user(self.regular_user2)

        new_user_data = self._get_request_body(email)
        response = self.api_client.post(
            '/v1/users',
            data=json.dumps(new_user_data),
            headers=headers)

        self.assertEqual(response.status_code, CREATED,
                         'response status code is not CREATED. response {}'.format(response.data))

        actual = json.loads(response.data)
        user_data = session.query(User).get(actual.get('id'))
        self.assertEqual(user_data.first_name, new_user_data.get('first_name'))
        # make sure user name and email become lower case in saved data
        self.assertEqual(user_data.username, new_user_data.get('username').lower())
        self.assertEqual(user_data.email, new_user_data.get('email').lower())
        self.assertEqual(user_data.allow_age_restricted_content, new_user_data.get('allow_age_restricted_content'))

        new_user_data.pop('password')
        # username and email converted to lower in user registration process
        new_user_data['username'] = new_user_data['username'].lower()
        new_user_data['email'] = new_user_data['email'].lower()
        expected_profile = new_user_data.pop('profile')
        actual_profile = actual.pop('profile')

        self.assertDictContainsSubset(new_user_data, actual)

        expected_birthdate = expected_profile.pop('birthdate')
        actual_birthdate = actual_profile.pop('birthdate')
        self.assertDictContainsSubset(expected_profile, actual_profile)
        self.assertEqual(actual_birthdate[:10], expected_birthdate[:10])
        return user_data

    def _get_request_body(self, email):
        with self.on_session(self.regular_user2):
            new_user_data = {
                'first_name': _fake.name(),
                'last_name': _fake.name(),
                # test input with upper, later should be converted to lower on user registration
                "username": email or _fake.email().upper(),
                "email": email or _fake.email().upper(),
                "password": "5d981e9beea184962ab8399724375b4ea4e79265",
                'allow_age_restricted_content': not self.regular_user2.allow_age_restricted_content,
                "origin_client_id": 2,
                "profile": {
                    "birthdate": '1990-12-01',
                    "gender": "1",
                    "origin_device_model": "ASUS_T00J"
                }
            }
        return new_user_data


_fake = Factory.create()
