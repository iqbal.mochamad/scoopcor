from __future__ import unicode_literals, absolute_import
import json

from app.helpers import load_json_schema
from app.users.tests.e2e.base import OrganizationE2EBase
from app.users.wallet import WalletTransaction
from app.utils.datetimes import datetime_to_isoformat
from app.utils.json_schema import validate_scoop_jsonschema

from tests.fixtures.sqlalchemy.wallets import UserWalletFactory, UserWalletTransactionFactory


class UserWalletApiTests(OrganizationE2EBase):

    def setUp(self):
        super(UserWalletApiTests, self).setUp()
        self.session.expire_on_commit = False
        self.session.add(self.regular_user)
        self.wallet = UserWalletFactory(party=self.regular_user)
        self.session.commit()
        self.balance = self.wallet.balance

    def test_response_body(self):
        self.set_api_authorized_user(self.regular_user)
        self.response = self.api_client.get(
            '/v1/users/current-user/wallet',
            headers=self.build_headers()
        )
        schema = load_json_schema("users", "user-wallet.balance")
        validate_scoop_jsonschema(json.loads(self.response.data), schema)
        self.assertEqual("application/json", self.response.content_type)

        expected = {
            "balance": self.balance,
            "transactions": {
                    "href": "http://localhost/v1/users/current-user/wallet/transactions",
                    "title": "Transaction History"
                }
        }
        self.assertDictEqual(json.loads(self.response.data), expected)

    def test_transactions(self):
        self.wallet_transaction = UserWalletTransactionFactory(wallet=self.wallet)
        self.session.add(self.wallet_transaction)
        self.session.commit()

        self.set_api_authorized_user(self.regular_user)
        response = self.api_client.get(
            '/v1/users/current-user/wallet/transactions',
            headers=self.build_headers()
        )

        # self.session.add(self.wallet_transaction)
        expected_wt = WalletTransaction.query.get(self.wallet_transaction.id)

        schema = load_json_schema("users", "user-wallet.transactions")
        validate_scoop_jsonschema(json.loads(response.data), schema)

        self.assertEqual("application/json", response.content_type)

        self.assertUnorderedJsonEqual(response, {
            "metadata": {
                "resultset": {
                    "count": 1,
                    "limit": 20,
                    "offset": 0
                }
            },
            "wallet_transactions": [
                {
                    "amount": expected_wt.amount,
                    "date": datetime_to_isoformat(expected_wt.created),
                    "href": "http://localhost/v1/orders/1",
                    "title": "Summary Transaction"
                }
            ]
        })
