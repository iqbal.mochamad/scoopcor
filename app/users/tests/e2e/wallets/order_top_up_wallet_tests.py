from __future__ import absolute_import, unicode_literals
from datetime import timedelta
from decimal import Decimal
from httplib import OK, CREATED
import json

from app import db, app
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.master.models import Currencies
from app.offers.models import OfferType
from app.orders.choices import COMPLETE
from app.parental_controls.models import ParentalControl
from app.payment_gateways.models import PaymentGateway
from app.payments.choices.gateways import DIRECT_PAYMENT
from app.payments.signature_generator import GenerateSignature
from app.payments.choices import CHARGED
from app.users.wallet import WALLET_OFFER_TYPE_ID, Wallet
from app.utils.testcases import E2EBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferTypeFactory
from tests.fixtures.sqlalchemy.payments.gateways import PaymentGatewayFactory


class OrderTopUpWalletTests(E2EBase):

    def setUp(self):
        super(OrderTopUpWalletTests, self).setUp()
        self.session.expire_on_commit = False
        cur_idr = Currencies.query.get(2)
        pg_direct = self.session.query(PaymentGateway).get(DIRECT_PAYMENT)

        offer_type_wallet = self.session.query(OfferType).get(WALLET_OFFER_TYPE_ID)
        self.offer1 = OfferFactory(id=909, offer_type=offer_type_wallet, is_free=False, price_idr=10000, price_usd=0,
                                   price_point=0)
        parental_control1 = self.session.query(ParentalControl).get(1)
        brand = BrandFactory()
        item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand,
                            parentalcontrol=parental_control1)
        self.offer1.items.append(item1)
        self.session.add(self.regular_user)
        self.user = self.regular_user
        self.wallet_init_amount = 5000000
        self.wallet = Wallet(party=self.user, balance=self.wallet_init_amount)

        self.top_up_amount = 500000.0
        self.session.add(self.wallet)
        self.session.commit()

    def test_order_top_up_wallet(self):
        mock_smtplib()

        self.set_api_authorized_user(self.user)
        header_data = self.build_headers()
        header_data['Content-Type'] = 'application/json'
        # get request body for order
        request_body = self.get_request_body_checkout()

        client = app.test_client(use_cookies=False)

        # first, do order checkout
        response = client.post('/v1/orders/checkout',
                               data=json.dumps(request_body),
                               headers=header_data)
        self.assertEqual(response.status_code, CREATED, response)
        if response.status_code == CREATED:
            response_checkout = json.loads(response.data)
            self.assertIsNotNone(response_checkout.get('order_number', None))
            self.assertIsNotNone(response_checkout.get('temp_order_id', None))

            # next, confirm order
            request_body = self.get_request_body_confirm(response_checkout)
            response = client.post('/v1/orders/confirm',
                                   data=json.dumps(request_body),
                                   headers=header_data)
            self.assertEqual(response.status_code, CREATED)
            if response.status_code == CREATED:
                response_confirm = json.loads(response.data)
                order_id = response_confirm.get('order_id', None)
                self.assertIsNotNone(order_id)

                # finally, process payment (charge payment)
                request_body = self.get_request_body_payment(response_confirm)
                response = client.post('/v1/payments/charge',
                                       data=json.dumps(request_body),
                                       headers=header_data)
                self.assertEqual(response.status_code, OK)
                if response.status_code == OK:
                    response_charge = json.loads(response.data)
                    self.assertIsNotNone(response_charge)
                    self.assertEqual(order_id, response_charge.get('order_id', None))
                    self.assertEqual(COMPLETE, response_charge.get('order_status', None))

                    # assert wallet balance after transaction
                    order_amount = Decimal(response_checkout.get('final_amount', None))
                    self.assertEqual(order_amount, self.top_up_amount,
                                     'Order Total Amount {order_amount} not equal with top up amount {top_up}'.format(
                                         order_amount=order_amount, top_up=self.top_up_amount
                                     ))
                    with self.on_session(self.wallet):
                        wallet = self.session.query(Wallet).get(self.wallet.id)
                        self.assertEqual(wallet.balance, self.wallet_init_amount + self.top_up_amount)

    @staticmethod
    def get_request_body_payment(response_confirm):
        return {
            "order_id": response_confirm.get('order_id', None),
            "signature": GenerateSignature(order_id=response_confirm.get('order_id', None)).construct(),
            "user_id": response_confirm.get('user_id', None),
            "payment_gateway_id": response_confirm.get('payment_gateway_id', None)
        }

    @staticmethod
    def get_request_body_confirm(response_checkout):
        return {
            "user_id": response_checkout.get('user_id', None),
            "payment_gateway_id": response_checkout.get('payment_gateway_id', None),
            "client_id": response_checkout.get('client_id', None),
            "currency_code": response_checkout.get('currency_code', None),
            "final_amount": response_checkout.get('final_amount', None),
            "order_number": response_checkout.get('order_number', None),
            "temp_order_id": response_checkout.get('temp_order_id', None),
        }

    def get_request_body_checkout(self):
        with self.on_session(self.user, self.offer1):
            return {
                "user_id": self.user.id,
                "payment_gateway_id": DIRECT_PAYMENT,
                "client_id": 7,
                "platform_id": 4,
                "order_lines": [
                    {
                        "offer_id": self.offer1.id,
                        "quantity": 1,
                        "name": self.offer1.name,
                        "top_up_amount": self.top_up_amount,
                    },
                ],
                "user_email": "ahmad.syafrudin@apps-foundry.com",
                "user_name": "",
                "user_street_address": "",
                "user_city": "Jakarta",
                "user_zipcode": "",
                "user_state": "",
                "user_country": "Indonesia",
                "latitude": -6.1744,
                "longitude": 106.8294,
                "ip_address": "202.179.188.106",
                "os_version": "Mac OS X",
                "device_model": "Chrome 51.0.2704.84",
                "client_version": "3.10.1"
            }

