from __future__ import unicode_literals, absolute_import
import json
from httplib import OK

from app.helpers import load_json_schema
from app.utils.datetimes import datetime_to_isoformat
from app.utils.json_schema import validate_scoop_jsonschema
from app.users.wallet import WalletTransaction
from app.users.tests.e2e.base import OrganizationE2EBase
from tests.fixtures.sqlalchemy.wallets import OrganizationWalletFactory, OrganizationWalletTransactionFactory


class OrganizationWalletApiTests(OrganizationE2EBase):

    def setUp(self):
        super(OrganizationWalletApiTests, self).setUp()
        self.session.add(self.organization)
        self.wallet = OrganizationWalletFactory(party=self.organization, balance=100)
        self.session.add(self.wallet)
        self.session.commit()

    def test_update_wallet(self):
        self.session.add(self.organization_manager)
        self.session.add(self.organization)
        self.set_api_authorized_user(self.organization_manager)

        wallet_update_data = {
            "balance": 1233456,
        }
        headers = self.build_headers()
        headers['Content-Type'] = 'application/json'

        response = self.api_client.put(
            '/v1/organizations/{org.id}/wallet'.format(org=self.organization),
            data=json.dumps(wallet_update_data),
            headers=headers
        )
        self.assertEqual(response.status_code, OK, response.data)

    def test_summary(self):
        """ BASIC: Can fetch organization wallet summary.
        """
        self.set_api_authorized_user(self.organization_manager)

        response = self.api_client.get(
            '/v1/organizations/{org.id}/wallet'.format(org=self.organization),
            headers=self.build_headers()
        )

        schema = load_json_schema("users", "organization-wallet.balance")

        validate_scoop_jsonschema(json.loads(response.data), schema)
        self.assertEqual(response.status_code, OK)
        self.assertEqual("application/json", response.content_type)
        self.session.add(self.organization)

        self.assertUnorderedJsonEqual(response, {
             "balance": float(self.organization.wallet.balance),
             "transactions": {
                 "href": "http://localhost/v1/organizations/{org.id}/wallet/transactions".format(org=self.organization),
                 "title": "Transaction History"
             }
        })

    def test_transactions(self):
        self.wallet_transaction = OrganizationWalletTransactionFactory(wallet=self.wallet)
        self.session.expire_on_commit = False
        self.session.add(self.wallet_transaction)
        self.session.commit()

        self.set_api_authorized_user(self.organization_manager)
        response = self.api_client.get(
            '/v1/organizations/{org.id}/wallet/transactions'.format(org=self.organization),
            headers=self.build_headers()
        )

        # self.session.add(self.wallet_transaction)
        expected_wt = WalletTransaction.query.get(self.wallet_transaction.id)

        schema = load_json_schema("users", "organization-wallet.transactions")
        validate_scoop_jsonschema(json.loads(response.data), schema)

        self.assertEqual("application/json", response.content_type)

        self.assertUnorderedJsonEqual(response, {
            "metadata": {
                "resultset": {
                    "count": 1,
                    "limit": 20,
                    "offset": 0
                }
            },
            "wallet_transactions": [
                {
                    "amount": expected_wt.amount,
                    "date": datetime_to_isoformat(expected_wt.created),
                    "href": "http://localhost/v1/orders/1",
                    "title": "Summary Transaction"
                }
            ]
        })



