from __future__ import unicode_literals, absolute_import

from httplib import OK, NOT_MODIFIED

from datetime import timedelta
from marshmallow.utils import isoformat

from app import app
from app.users.tests.fixtures_user_buffet import UserBuffetFactory
from app.utils.datetimes import get_local, datetime_to_isoformat
from app.utils.testcases import TestBase


def get_owned_buffet_format(owned_buffet):
    offer = owned_buffet.offerbuffet.offer
    return {
        "id": owned_buffet.id,
        "offerbuffet_id": owned_buffet.offerbuffet_id,
        "orderline_id": owned_buffet.orderline_id,
        "user_id": owned_buffet.user_id,
        "valid_to": datetime_to_isoformat(owned_buffet.valid_to),
        "offerbuffet": {
            "id": owned_buffet.offerbuffet.id,
            "available_from": isoformat(owned_buffet.offerbuffet.available_from),
            "available_until": isoformat(owned_buffet.offerbuffet.available_until),
            "buffet_duration": "P{0.years}Y{0.months}M{0.days}DT{0.hours}H{0.minutes}M{0.seconds}S".format(
                owned_buffet.offerbuffet.buffet_duration),
            "offer": {
                "id": offer.id,
                "name": offer.name,
                "offer_status": offer.offer_status,
                "sort_priority": offer.sort_priority,
                "is_active": offer.is_active,
                "offer_type_id": offer.offer_type_id,
            }
        }
    }


class UserOwnedBuffetsTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(UserOwnedBuffetsTests, cls).setUpClass()
        cls.owned_a = UserBuffetFactory(
            user_id=cls.regular_user.id,
            valid_to=get_local() + timedelta(days=10)
        )
        cls.owned_not_valid = UserBuffetFactory(
            user_id=cls.regular_user.id,
            valid_to=get_local() - timedelta(days=10)
        )
        cls.not_owned_a = UserBuffetFactory(user_id=cls.super_admin.id)
        cls.session.commit()
        cls.session.add(cls.owned_a)
        cls.session.refresh(cls.owned_a)

    def test_by_user_id(self):

        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.get(
            '/v1/owned_buffets?user_id={0.id}'.format(self.regular_user),
            headers=self.build_headers()
        )

        self.assertEqual(response.status_code, OK)
        self.session.add(self.owned_a)
        self.assert_response_body(response, expected_owned_buffets=[self.owned_a, ])

    def assert_response_body(self, response, expected_owned_buffets):
        owned_buffets = []
        with app.test_request_context():
            for owned_buffet in expected_owned_buffets:
                owned_buffets.append(get_owned_buffet_format(owned_buffet))
            self.assertUnorderedJsonEqual(
                response,
                {
                    "owned_buffets": owned_buffets,
                    "metadata": {
                        "resultset": {
                            "limit": 20,
                            "offset": 0,
                            "count": len(expected_owned_buffets)
                        }
                    }
                }
            )

    # def test_etag_support(self):
    #
    #     self.set_api_authorized_user(self.regular_user)
    #
    #     response = self.api_client.get(
    #         '/v1/owned_buffets?user_id={0.id}'.format(self.regular_user),
    #         headers=self.build_headers()
    #     )
    #
    #     self.assertIn('ETag', response.headers)
    #
    #     etag = response.headers['ETag']
    #     not_modified_headers = self.build_headers()
    #     not_modified_headers['If-None-Match'] = etag
    #
    #     response_not_modified = self.api_client.get(
    #         '/v1/owned_buffets?user_id={0.id}'.format(self.regular_user),
    #         headers=not_modified_headers
    #     )
    #
    #     self.assertEqual(response_not_modified.status_code, NOT_MODIFIED)
