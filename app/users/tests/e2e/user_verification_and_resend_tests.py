import json
from httplib import NO_CONTENT

from app.constants import RoleType
from app.users.users import get_verify_token
from app.utils.testcases import TestBase


class UserVerifyTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(UserVerifyTests, cls).setUpClass()
        cls.regular_user.is_verified = False
        cls.regular_user.key_verify = None
        cls.verify_token = get_verify_token()
        cls.super_admin.is_verified = False
        cls.super_admin.key_verify = cls.verify_token
        cls.session.add(cls.regular_user)
        cls.session.add(cls.super_admin)
        cls.session.commit()
        cls.session.expunge_all()

    def test_resend_email_verification(self):
        with self.on_session(self.regular_user):
            response = self.api_client.post(
                '/v1/users/verify-email'.format(self.regular_user.id),
                data=json.dumps({
                    "email": self.regular_user.email
                }), headers=self.build_headers(has_body=True))

            self.assertEqual(response.status_code, NO_CONTENT)
            self.reattach_model(self.regular_user)
            self.assertIsNotNone(self.regular_user.key_verify)

    def test_update_user_verified_status(self):
        with self.on_session(self.super_admin):
            response = self.api_client.put(
                '/v1/users/{}/verify-email'.format(self.super_admin.id),
                data=json.dumps({
                    "verification_token": self.verify_token
                }), headers=self.build_headers(has_body=True))

            self.assertEqual(response.status_code, NO_CONTENT)

            self.reattach_model(self.super_admin)
            self.assertIsNone(self.super_admin.key_verify)
            self.assertTrue(self.super_admin.is_verified)
            self.assertIn(RoleType.verified_user.value, [role.id for role in self.super_admin.roles])
