Feature: Current User API

    @e2e
    Scenario: Request by Authenticated User
        Given an authenticated user
            And that user belongs to some organizations
        When requesting /users/current-user
        Then HTTP response status is "OK"
            And the authenticated user's data is returned

    @e2e
    Scenario: Request by Anonymous User
        Given an anonymous user
        When requesting /users/current-user
        Then HTTP response status is "UNAUTHORIZED"
            And scoop standard error format is returned
