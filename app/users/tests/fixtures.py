import os
import random
from datetime import datetime

from factory import alchemy, Faker, LazyAttribute

from app import db
from app.users.organizations import OrganizationStatus, OrganizationType, Organization, OrganizationUser
from app.users.users import Role, User


class RoleFactory(alchemy.SQLAlchemyModelFactory):
    name = Faker('bs')
    is_active = Faker('boolean')

    class Meta:
        sqlalchemy_session = db.session
        model = Role


class OrganizationFactory(alchemy.SQLAlchemyModelFactory):
    name = Faker('bs')
    status = LazyAttribute(lambda _: random.choice([v.value for v in OrganizationStatus]))
    slug = Faker('slug')
    type = LazyAttribute(lambda _: random.choice([v.value for v in OrganizationType]))
    is_active = True

    class Meta:
        sqlalchemy_session = db.session
        model = Organization


class UserFactory(alchemy.SQLAlchemyModelFactory):

    username = Faker('email')
    email = Faker('email')
    first_name = Faker('first_name')
    last_name = Faker('last_name')
    last_login = LazyAttribute(lambda _: datetime.now())
    password = Faker('sha1')
    salt = LazyAttribute(lambda _: os.urandom(32).encode('hex'))
    is_active = True
    is_verified = True
    allow_age_restricted_content = True

    class Meta:
        sqlalchemy_session = db.session
        model = User


class OrganizationUserFactory(alchemy.SQLAlchemyModelFactory):
    user_id = Faker('pyint')
    organization_id = Faker('pyint')
    is_manager = Faker('pybool')

    class Meta:
        sqlalchemy_session = db.session
        model = OrganizationUser
