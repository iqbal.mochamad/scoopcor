"""
Users
=====

Users represent **anyone** and **everything** that accesses SCOOP APIs.

This includes regular customers, internal users, developers, and programmatic services that authorize against our APIs.
"""
from __future__ import unicode_literals, absolute_import

import hashlib, json, os, random, smtplib, string, scrypt
from collections import defaultdict
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from httplib import CREATED, NO_CONTENT, OK

from flask import url_for, g, jsonify, Blueprint, request
from flask_appsfoundry import models
from flask_appsfoundry.exceptions import NotFound, Forbidden, BadRequest, Unauthorized, Conflict
from flask_appsfoundry.serializers import fields
from flask_babel import gettext
from marshmallow import fields as ma_fields, post_load
from marshmallow.utils import isoformat
from sqlalchemy import desc, or_, extract, func,and_
from werkzeug.routing import BaseConverter

from app import app, db, ma
from app.auth.constants import PW_HASH_BYTES, PW_SALT_BYTES
from app.auth.decorators import user_is_authenticated
from app.auth.eperpus import assign_user_to_eperpus_public, assign_user_to_telkomsel
from app.auth.helpers import is_superuser, get_user
from app.auth.models import Perm, Client
from app.constants import RoleType
from app.messaging.email import HtmlEmail, get_smtp_server
from app.offers.choices.offer_type import SUBSCRIPTIONS, BUFFET
from app.offers.choices.quantity_unit import UNIT, DAILY, WEEKLY, MONTHLY
from app.paymentgateway_connectors.choices.renewal_status import NEW, RENEWAL_STATUS
from app.payments.choices.gateways import CREDITCARD
from app.points.helpers import create_point_data
from app.points.models import PointRegister
from app.services.gramedia.models import get_gramedia_session, GramediaUser
from app.utils.datetimes import get_utc
from app.utils.exceptions import SendMailException
from app.utils.marshmallow_helpers import update_entity, schema_load
from app.utils.responses import get_list_response
from app.utils.serializers import ImplicitRelHref
from .base import Party

from app.eperpus.constants import PUBLIC_LIBRARY_CLIENT_ID

blueprint = Blueprint('users', __name__, url_prefix='/v1')


class UserConverter(BaseConverter):

    def to_python(self, value):
        session = db.session()
        if value == 'current-user':
            # HOW THE FUCK IS THIS NOT EXECUTED WITHIN AN APPLICATION CONTEXT????
            # session.add(g.current_user)
            # entity = g.current_user
            entity = 'current-user'
        else:
            entity = session.query(User).get(value)
        if not entity:
            raise NotFound
        return entity

    def to_url(self, value):
        return unicode(value.id)


app.url_map.converters['user'] = UserConverter


@blueprint.route('/users/<user:user>')
@user_is_authenticated
def details(user):
    # managers, organization managers
    # url: v1/users/current-user or v1/users/<int:user_id>
    EBOOKS_USER_AGENT = ['scoop_android', 'scoop_ios']

    if user == 'current-user':
        user = g.current_user
        db.session().add(user)

    data_borrowed = {'total_title': 0, 'latest_title': 0}
    user_agents = request.headers.get('User-Agent').lower().replace(' ', '_').split('/')[0]

    if user_agents not in EBOOKS_USER_AGENT:
        data_borrowed = retrieve_borrowed_total_item(user_agents, g.current_user.id)

    response = UserSchema().dump(user).data
    response.update(data_borrowed)
    return jsonify(response)


def retrieve_borrowed_total_item(user_agents, user_id):
    from app.users.organizations import retrieve_orgs_by_client
    from app.eperpus.members import UserBorrowedItem

    duration_filter = datetime.today() - timedelta(days=30)
    key = str(hash("{}{}{}".format(user_id, user_agents, duration_filter.strftime("%m-%d-%Y"))))
    redis_key = 'profile-borrowed:{}'.format(key)
    total_title, latest_title = 0, 0

    # get data redis to minimize load database
    data_redis = app.kvs.get(redis_key)
    if data_redis:
        return json.loads(data_redis)

    # get all organization by user agent
    client = Client.query.filter_by(app_name=user_agents).first()
    if client:
        all_organization = retrieve_orgs_by_client(client.id)

        # get total borrowed item and total borrowed last 30 days
        borrowed = UserBorrowedItem.query.distinct(UserBorrowedItem.catalog_item_item_id).filter(
            (UserBorrowedItem.catalog_item_org_id.in_(all_organization)) & (UserBorrowedItem.user_id == user_id))

        last_borrowed = borrowed.filter(and_(func.date(UserBorrowedItem.borrowing_start_time) <= datetime.now().date()),
                                        func.date(UserBorrowedItem.borrowing_start_time) >= duration_filter.date())

        total_title, latest_title = borrowed.count(), last_borrowed.count()

    data_borrowed = {'total_title': int(total_title), 'latest_title': int(latest_title)}
    app.kvs.set(redis_key, json.dumps(data_borrowed), ex=app.config['ITEM_DETAILS_REDIS_TIME'])
    return data_borrowed


@blueprint.route('/users')
@user_is_authenticated
def list_of_users():
    args = request.args.to_dict()
    q = args.get('q', None)
    limit, offset = args.get('limit', 25), args.get('offset', 0)
    role_id = args.get('role_id', None)
    data_user = User.query

    if q:
        search = '{}{}{}'.format('%', q, '%')
        data_user = data_user.filter(User.username.ilike(search))

    if role_id:
        # data_user = data_user.filter(User.roles.any(id=role_id))
        # if used method above roles.any, somehow timeout.!
        raw_sql = """
            select user_id from cas_users_roles where role_id = {}
        """.format(role_id)
        data = db.engine.execute(raw_sql)
        user_ids = [i[0] for i in data.fetchall()]
        data_user = data_user.filter(User.id.in_(user_ids))

    data_user = data_user.order_by(desc(User.id))
    total_count = data_user.count()
    if limit:
        data_user = data_user.limit(limit).offset(offset).all()

    response_data = []
    for idata in data_user:
        response_data.append(
            {
                'id': idata.id,
                'fullname': '{} {}'.format(
                    idata.first_name if idata.first_name else '',
                    idata.last_name if idata.last_name else ''
                ),
                'first_name': idata.first_name if idata.first_name else '',
                'last_name': idata.last_name if idata.last_name else '',
                'username': idata.username,
                'is_verified': idata.is_verified,
                'email': idata.email,
                'phone_number': idata.phone_number,
                'roles': [{'id': jdata.id, 'name': jdata.name} for jdata in idata.roles]
            }
        )

    resultset = {}
    resultset['count'] = total_count
    resultset['limit'] = limit
    resultset['offset'] = offset

    metadata = {}
    metadata['resultset'] = resultset

    return jsonify({'users': response_data, 'metadata': metadata}), OK


@blueprint.route('/users/verify-email', methods=['POST', ])
def resend_email_verification():
    json_data = json.loads(request.data)

    email = json_data.get('email')
    user_agent = request.headers.get('User-Agent', 'ebooks / 1.0').split('/')[0].lower().strip().replace(" ", "_")

    if not email:
        raise BadRequest(user_message=gettext("email is required"), developer_message="email is required")

    user = get_user(email, db.session())
    if user:
        if not user.key_verify:
            user.key_verify = get_verify_token()
            db.session.add(user)
            db.session.commit()

        if not user.is_verified:
            send_verification_email(user, user_agent=user_agent)

    # Always return successfully response.
    return jsonify({}), NO_CONTENT


@blueprint.route('/users/<user:user>/verify-email', methods=['PUT', ])
def update_user_verified_status(user):

    session = db.session()
    if user == 'current-user':
        user = getattr(g, 'current_user', None)
        if not user:
            # do not return 401 here, IOS and Android will automaticly hit refresh-token api!
            return jsonify({}), NO_CONTENT
        session.add(user)

    if user:
        json_data = json.loads(request.data)
        verification_token = json_data.get('verification_token', 'unknown')

        if user.key_verify == verification_token:
            # set status to verified
            user.is_verified = True
            user.key_verify = None

            # change user role from unverified to verified
            verified_role = None
            unverified_role = None
            banned_role = None

            for role in user.roles:
                if role.id == RoleType.unverified_user.value:
                    unverified_role = role
                elif role.id == RoleType.verified_user.value:
                    verified_role = role
                elif role.id == RoleType.banned_user.value:
                    banned_role = role

            if unverified_role:
                user.roles.remove(unverified_role)

            if not verified_role:
                verified_role = session.query(Role).get(RoleType.verified_user.value)
                user.roles.append(verified_role)

            if banned_role:
                # delete all roles active and set banned permanent!
                for role in user.roles:
                    user.roles.remove(role)

                banned = session.query(Role).get(RoleType.banned_user.value)
                user.roles.append(banned)

            session.add(user)
            session.commit()

    # Always return this api with 204
    return jsonify({}), NO_CONTENT


@blueprint.route('/users/<user:user>', methods=['PUT', ])
@user_is_authenticated
def update_current_user(user):
    session = db.session
    if user == 'current-user':
        user = g.current_user
        session.add(user)
    else:
        if not user.id == g.current_user.id and not 'webportal' in request.headers.get('User-Agent').lower():
            raise Unauthorized()

    user_schema = UserRegisterSchema()
    update_user(user, user_schema, session)

    # generate_referral_code(user=user, session=session)
    # deliver_point_on_valid_profile(user, session)
    return jsonify(user_schema.dump(user).data)


def update_user(user, user_schema, session, json_data=None):
    from app.users.organizations import Organization, OrganizationUser

    json_data = json_data or json.loads(request.data)

    if not json_data.get('change_password'):
        json_data.pop('password', None)

    profile_entity = json_data.pop('profile', None)
    data_role = json_data.pop('roles', [RoleType.verified_user.value])
    data_organization = json_data.pop('organizations', [])
    change_organization = json_data.get('change_organization', False)
    change_roles = json_data.get('change_roles', False)
    allow_age_restricted_content = json_data.get('allow_age_restricted_content', False)

    update_entity(entity=user, schema=user_schema, json_data=json_data)

    if change_roles:
        for idata in user.roles: user.roles.remove(idata)

        for new_role in data_role:
            role_ = session.query(Role).get(new_role)
            user.roles.append(role_)

    # this were used by portal ebooks.!
    if change_organization:
        for iorg in user.organizations: user.organizations.remove(iorg)
        for new_org in data_organization:
            org_ = session.query(Organization).get(new_org)
            user.organizations.append(org_)

    # this were used by portal eperpus!
    if data_organization:
        old_data_organization = [old.id for old in user.organizations]

        # Exclude parent organization, User must always inside parent organization (FE always put parent when PUT)
        latest_update_organization = []
        parent_organization_id = None

        for new_org in data_organization:
            is_parent = new_org.get('is_parent_organization', False)
            if is_parent:
                parent_organization_id = new_org.get('id')
            else:
                latest_update_organization.append(new_org.get('id'))

        # Find child old organization who related with parent organization_id.
        old_organization_by_parent = Organization.query.filter(
            Organization.id.in_(old_data_organization),
            Organization.parent_organization_id == parent_organization_id).all()

        # This mean only child we remove, parent already excluded, on top!
        for old_data in old_organization_by_parent:
            user.organizations.remove(old_data)

        # insert new child organizations
        for new_data in latest_update_organization:
            org_ = session.query(Organization).get(new_data)
            user.organizations.append(org_)

    if json_data.get('change_password') and json_data.get('password'):
        user.set_password(json_data.get('password'))

    if profile_entity:
        profile = user.profile if user.profile else Profile(user_id=user.id)
        update_entity(entity=profile, schema=ProfileSchema(), json_data=profile_entity)
        session.add(profile)

    # user.allow_age_restricted_content = allow_age_restricted_content

    session.add(user)
    session.commit()


@blueprint.route('/users', methods=['POST', ])
def create_user():
    session = db.session()
    user_schema = UserRegisterSchema()
    user_agent = request.headers.get('User-Agent', 'ebooks').lower().replace(' ', '-')

    if 't-perpus' in user_agent:
        error_message, new_user = assign_user_to_telkomsel(user_schema, session)
        if error_message:
            raise BadRequest(user_message=gettext(error_message), developer_message=error_message)
    else:
        user_agent = request.headers.get('User-Agent', 'ebooks / 1.0').split('/')[0].lower().strip().replace(" ", "_")

        if user_agent in ['scoop_ios', 'scoop_android', 'scoop_web', 'ebooks']:
            new_user, user_referrer = save_new_user(user_schema, session)
            assign_user_to_eperpus_public(new_user, client=getattr(g, 'current_client', None), session=session)
            # deliver_point_on_user_registration(user=new_user, user_referrer=user_referrer, session=session)
            deliver_items_on_user_registration(user=new_user, session=session)
        else:
            # else here come from ePerpus!
            new_user = save_new_user_eperpus(user_schema=user_schema, session=session, user_agent=user_agent)

        if new_user:
            try:
                send_verification_email(new_user, user_agent=user_agent)
            except:
                pass

    return jsonify(user_schema.dump(new_user).data), CREATED


def save_new_user_eperpus(user_schema, session, user_agent):

    # cannot import this on top of files, this user.py is imported to in organization.py (looping) T_T
    from app.users.organizations import Organization, OrganizationUser
    from app.eperpus.libraries import SharedLibrary

    new_user = None
    entity = schema_load(user_schema)
    exist_client = Client.query.filter_by(app_name=user_agent).first()

    if exist_client:
        if exist_client.id in PUBLIC_LIBRARY_CLIENT_ID:
            parent_organization = SharedLibrary.query.filter_by(public_library=True).order_by(SharedLibrary.id).first()
        else:
            parent_organization = Organization.query.filter(Organization.clients.any(id=exist_client.id),
                                                            Organization.parent_organization_id == None).first()

        username = entity["username"].strip()
        email = entity["email"].strip()
        user_exists = session.query(User).filter(or_(User.username == username, User.email == email)).first()

        if user_exists:
            # if user already registered in ebook but still not include inside organization.
            exist_user_organization = OrganizationUser.query.filter(OrganizationUser.organization==parent_organization,
                                                                    OrganizationUser.user==user_exists).first()
            if not exist_user_organization:
                # insert automaticaly
                new_user_organization = OrganizationUser(organization=parent_organization, user=user_exists)
                db.session.add(new_user_organization)
                db.session.commit()
                new_user = user_exists
            else:
                msg = 'Already register with email {}'.format(email)
                raise Conflict(user_message=gettext(msg), developer_message=msg)

        else:
            # create new user and automatically insert into organization
            new_user, user_referrer = save_new_user(user_schema, session)

            new_user_organization = OrganizationUser(organization=parent_organization, user=new_user)
            db.session.add(new_user_organization)
            db.session.commit()

    return new_user


def save_new_user(user_schema, session):
    from app.users.organizations import Organization
    # username and email will be lowered case in UserRegisterSchema PostLoad
    entity = schema_load(user_schema)

    if not entity.get('origin_client_id', None):
        entity['origin_client_id'] = g.current_client.id

    assert_username_and_email(session, entity["username"], entity["email"])
    referral_code = entity.pop('referral_code', None)
    profile_entity = entity.pop('profile', None)
    data_role = entity.pop('roles', [RoleType.verified_user.value])
    data_organization = entity.pop('organizations', [])

    new_user = User()
    for key, value in entity.items():
        if hasattr(new_user, key):
            setattr(new_user, key, value)
    new_user.set_password(new_user.password)

    for new_role in data_role:
        role_ = session.query(Role).get(new_role)
        new_user.roles.append(role_)

    for new_org in data_organization:
        org_ = session.query(Organization).get(new_org)
        new_user.organizations.append(org_)

    if profile_entity:
        profile = Profile()
        for key, value in profile_entity.items():
            if hasattr(profile, key):
                setattr(profile, key, value)
        profile.user = new_user
        session.add(profile)

    new_user.is_verified = False
    new_user.is_active = True
    new_user.key_verify = get_verify_token()

    # UNCOMMENT THIS IF REFFERAL ACTIVATE AGAIN!!
    user_referrer = None
    # if referral_code:
    #     user_referrer = session.query(User).filter_by(referral_code=referral_code).first()
    #     if user_referrer:
    #         new_user.referral_id = user_referrer.id
    #
    # generate_referral_code(user=new_user, session=session)

    session.add(new_user)
    session.commit()
    return new_user, user_referrer


def assert_username_and_email(session, username, email, user_id=None):
    user_exists = session.query(User).filter(or_(
        User.username == username,
        User.email == email))
    if user_id:
        user_exists = user_exists.filter(User.id != user_id)
    user_exists = user_exists.first()
    if user_exists:
        if user_exists.username == username:
            msg = 'Username {} had been registered. Please use different username.'.format(username)
        else:
            msg = 'Please use different email, {} had been registered with Username : {}.'.format(
                email, user_exists.username)
        raise Conflict(user_message=gettext(msg), developer_message=msg)


def generate_referral_code(user, session):
    # generate referral code if first name is valid and referral code still empty
    if user.is_verified and user.is_active and user.first_name and not user.referral_code:
        referral_code_empty = True
        while referral_code_empty:
            referral_code = referral_code_generator(user.first_name.strip())
            user_existing = session.query(User).filter_by(referral_code=referral_code).first()
            if not user_existing:
                referral_code_empty = False
                user.referral_code = referral_code
                session.add(user)
                session.commit()


def referral_code_generator(first_name):
    digits_random = ''.join(random.choice(string.digits) for _ in range(3))

    # <7 digits name><3 digit random integer>
    if len(first_name) >= 7:
        ref_data = '%s%s' % (first_name[:7], digits_random)
    else:
        need_length = int(7 - len(first_name))
        part_name = first_name[:need_length]
        moci_name = '%s%s' % (first_name, part_name)

        if len(moci_name) < 7:
            need_length = int(7 - len(moci_name))
            part_namex = ''.join(random.choice(string.ascii_uppercase) for _ in range(need_length))
            ref_data = "%s%s%s" % (moci_name, part_namex, digits_random)
        else:
            ref_data = "%s%s%s" % (first_name, part_name, digits_random)

    return ref_data.upper()


def deliver_items_on_user_registration(user, session):
    # previous api/url callback from cas: http:\/\/dev6.apps-foundry.com\/scoopcor\/api\/v1\/bonuses\/claim\/register
    items_gift = []
    from app.items.models import ItemRegister, Item, UserItem
    gifts = session.query(ItemRegister).filter(
        ItemRegister.name == 'register',
        ItemRegister.valid_from <= get_utc(),
        ItemRegister.valid_to >= get_utc(),
        ItemRegister.is_active == True).all()
    for gift in gifts:
        if gift.brands:
            for brand_id in gift.brands:
                from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
                item = session.query(Item).filter_by(
                    brand_id=brand_id, is_active=True,
                    item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]
                ).order_by(
                    desc(Item.release_date)).first()
                if item:
                    items_gift.append(item)
        if gift.items:
            items = session.query(Item).filter(Item.id.in_(gift.items)).all()
            if items:
                items_gift.extend(items)
    if items_gift:
        from app.utils.shims import item_types
        for item in items_gift:
            user_item = session.query(UserItem).filter_by(user_id=user.id,
                                                          item_id=item.id).first()
            if not user_item:
                user_item = UserItem(
                    user_id=user.id,
                    item_id=item.id,
                    edition_code=item.edition_code,
                    orderline_id=None,
                    is_active=True,
                    brand_id=item.brand_id,
                    itemtype_id=item_types.enum_to_id(item.item_type),
                    vendor_id=item.brand.vendor_id
                )
                session.add(user_item)
                session.commit()


def deliver_point_on_user_registration(user, user_referrer, session):
    # /v1/points_profile
    referred_point, profile_point, point = 0, 0, 0

    from app.items.models import ItemRegister
    data_point = session.query(ItemRegister).filter_by(name='point_reward').first()
    if not data_point:
        return None

    history = session.query(PointRegister).filter_by(user_id=user.id).first()
    if not history:
        history = PointRegister(user_id=user.id, is_referal=False, is_profile=False)

    if user_referrer:
        point = point + data_point.point_referral
        history.is_referal = True
        # deliver point for referrer. Note from previously api:
        # update point for referred id
        # THIS IS DISABLE, REFERRED USER WILL GET POINT AFTER
        # NEW USER IS VERIFIED
        # create_point_data(referred_id, referred_point)

    if is_user_profile_valid(user):
        point = point + data_point.point
        history.is_profile = True

    if point:
        create_point_data(user.id, point)

    history.point = point
    session.add(history)
    session.commit()


def is_user_profile_valid(user):
    return True if user.first_name and user.profile and user.profile.birthdate and user.profile.gender else False


def deliver_point_on_valid_profile(user, session):
    from app.items.models import ItemRegister
    data_point = session.query(ItemRegister).filter_by(name='point_reward').first()
    history = session.query(PointRegister).filter_by(user_id=user.id).first()
    if not history:
        history = PointRegister(user_id=user.id, is_referal=False, is_profile=False)
        session.add(history)
        session.commit()

    if data_point and history and not history.is_profile and is_user_profile_valid(user):
        # only deliver point if user update profile and previously the value is not yet filled correctly
        history.point = data_point.point
        history.is_profile = True
        session.add(history)
        session.commit()
        create_point_data(user.id, data_point.point)


def assign_user_organizations(user, client, session):
    """ Auto-assign User's signed up with a certain `Client` to predefined `Organization`s.

    :param `app.users.models.User` user:
    :param `app.clients.models.Client` client:
    :param `sqlalchemy.orm.sessions.Session` session:
    """
    PERPUSTAKAAN_NASIONAL_ORG_ID = 711

    PERPUSNAS_IOS_CLIENT_ID = 83
    PERPUSNAS_ANDROID_CLIENT_ID = 84

    try:
        if client:
            orgs_to_join = {
                PERPUSNAS_IOS_CLIENT_ID: [PERPUSTAKAAN_NASIONAL_ORG_ID, ],
                PERPUSNAS_ANDROID_CLIENT_ID: [PERPUSTAKAAN_NASIONAL_ORG_ID, ],
            }[client.id]

            for org_id in orgs_to_join:
                from app.users.organizations import Organization
                org = session.query(Organization).get(org_id)
                user.organizations.append(org)
                user.roles = [session.query(Role).get(RoleType.verified_user.value), ]
                user.is_verified = True

    except KeyError:
        pass


def get_verify_token():
    token = "%s%s" % (app.config['SECRET_GENERATE_TOKEN_KEY'], str(datetime.now()))
    verify_token = hashlib.md5(token).hexdigest()
    return verify_token


def send_verification_email(user, user_agent=None):

    server = get_smtp_server()

    # split user agent ebooks and eperpus?
    if user_agent in ['scoop_ios', 'scoop_android', 'scoop_web', 'ebooks']:
        message = HtmlEmail(
            sender='no-reply@gramedia.com',
            to=[user.email, ],
            subject='Gramedia Digital - Konfirmasi E-Mail',
            html_template='email/users/email_verification.html',
            plaintext_template='email/users/email_verification.txt',
            confirmation_url='{int_base}/account-activation/{verify_token}/{user_id}'.format(
                int_base=app.config['INT_BASE_URL'],
                user_id=user.id,
                verify_token=user.key_verify),
            user_email=user.email
        )
    else:
        from app.eperpus.constants import PUBLIC_LIBRARY_CLIENT_ID
        from app.users.organizations import Organization

        # Find organization parent by user agent.
        exist_client = Client.query.filter_by(app_name=user_agent).first()
        exist_organization_parent = Organization.query.filter(Organization.clients.any(id=exist_client.id),
            Organization.parent_organization_id == None).first()

        if exist_organization_parent:
            organization_name = exist_organization_parent.app_name

            # for handling eperpus public who has more than 1 clients!
            if exist_client.id in PUBLIC_LIBRARY_CLIENT_ID:
                organization_name = 'Eperpus'

            if exist_organization_parent.logo_url:
                image_logo = exist_organization_parent.logo_url.split('/')[-1:][0]
                logo_client = os.path.join(app.config['OFFERS_BASE_URL'], 'eperpus-logo', str(exist_organization_parent.id), image_logo)
            else:
                # Default logo.. if None in database.
                logo_client = os.path.join(app.config['OFFERS_BASE_URL'], 'eperpus-logo', 'eperpus.png')

            message = HtmlEmail(
                sender='no-reply@gramedia.com',
                to=[user.email, ],
                subject='{} - Konfirmasi E-Mail'.format(organization_name),
                html_template='email/users/eperpus_verification.html',
                plaintext_template='email/users/eperpus_verification.txt',
                confirmation_url='{eperpus_base}/account-activation/{hashed}/{user_id}'.format(
                    eperpus_base=app.config['EPERPUS_URL_BASE'], hashed=user.key_verify, user_id=user.id),
                logo_client=logo_client,
                organization_name=organization_name,
                user_email=user.email,
                pic_client_email=exist_organization_parent.contact_email)

    try:
        server.send_message(message)
    except Exception as e:
        pass


@blueprint.route('/users/<user:user>/pending-transactions')
@user_is_authenticated
def pending_transactions(user):
    """ Returns all the pending transactions for a given `User`

    :param `User|str` user: requesting entity, unless requested with 'current-user',
    """
    # todo: this needs to be better..
    session = db.session()
    if user == 'current-user':
        user = g.current_user
        session.add(user)
    else:
        # otherwise, make sure that request user is superadmin
        # and then ignore the 'user' attribute.. it's already been set to the user they want.
        session.add(g.current_user)
        if not is_superuser(g.current_user):
            raise Forbidden

    from app.orders.models import Order
    from app.payments.models import Payment

    pending_orders = user.orders.filter(
        Order.payments.any(Payment.payment_status == Payment.Status.waiting.value)
    )

    offset = request.args.get('offset', 0, type=int)
    pending_orders = pending_orders.offset(offset)
    limit = request.args.get('limit', 20, type=int)
    pending_orders = pending_orders.limit(limit)

    def get_payment_code(order):
        # welcome to our absolutely unmaintainable data modelling.
        # there's no good way to do this because of the **crap** way details are modelled.
        from app.paymentgateway_connectors.finnet195.models import PaymentFinnet195
        from app.paymentgateway_connectors.mandiri_ecash.models import MandiriEcash
        from app.paymentgateway_connectors.paypal.models import PaymentPaypal
        from app.paymentgateway_connectors.creditcard.models import PaymentVeritrans

        for payment in order.payments:
            for detail_model in [PaymentVeritrans, PaymentPaypal, MandiriEcash, PaymentFinnet195]:
                detail = session.query(detail_model).filter_by(payment_id=payment.id).first()
                if detail:
                    return detail.payment_code

    return jsonify({
        'pending': [
            {
                'title': 'Pending Order',
                'href': url_for('orders.orders', order_id=order.id, _external=True),
                'order_id': order.id,
                'date': isoformat(order.created),
                'order_number': order.order_number,
                'payment_gateway': {
                    'id': order.paymentgateway.id,
                    'title': order.paymentgateway.name,
                    'href': url_for('payment_gateways.payment_gateway',
                                    gateway_id=order.paymentgateway.id, _external=True)
                },
                'payment_code': get_payment_code(order),
                'price': {
                    'final': sum(li.localized_final_price for li in order.order_lines),
                    'currency': order.order_lines.first().localized_currency_code,
                },
                'line_items': [
                    {
                        'title': line_item.name,
                        'href': url_for('offers.offers', offer_id=line_item.offer_id, _external=True),
                        'id': line_item.id,
                        'offer_id': line_item.offer_id,
                        'price': {
                            'final': line_item.localized_final_price,
                            'currency': line_item.localized_currency_code
                        }
                    } for line_item in order.order_lines
                ]
            } for order in pending_orders
        ],
        'metadata': {
            'count': pending_orders.count(),
            'offset': offset,
            'limit': limit
        }
    })


@blueprint.route('/users/current-user/wish-list')
@user_is_authenticated
def get_current_user_wish_list():
    session = db.session()
    user = g.current_user
    session.add(user)

    query_set = user.wish_list_items
    item_id = request.args.get('item_id', None)
    if item_id:
        from app.items.models import Item
        query_set = query_set.filter(Item.id == item_id)
    return get_list_response(
        query_set=query_set,
        serializer_name='items',
        serializer=ImplicitRelHref()
    )


@blueprint.route('/users/current-user/wish-list', methods=['LINK', ])
@user_is_authenticated
def add_item_to_wish_list():
    session = db.session()
    user = g.current_user
    session.add(user)

    s = db.session()
    from app.items.models import Item
    item_to_add = s.query(Item).get(request.json['id'])
    if item_to_add is None:
        raise NotFound(
            user_message=gettext('Item does not exist'),
            developer_message='Item {} does not exist'.format(request.json['id'])
        )
    if item_to_add not in user.wish_list_items.all():
        user.wish_list_items.append(item_to_add)

    return jsonify({"success_message": "Item added"}), CREATED


@blueprint.route('/users/current-user/wish-list', methods=['UNLINK', ])
@user_is_authenticated
def remove_item_from_wish_list():
    session = db.session()
    user = g.current_user
    session.add(user)

    s = db.session()
    from app.items.models import Item
    item = s.query(Item).get(request.json['id'])
    user.wish_list_items.remove(item)
    return jsonify(''), NO_CONTENT


APPS_FOUNDRY_ORG = 1100276


def is_apps_foundry_users(user):
    """

    :param `app.users.users.User' user:
    :return:
    """
    if user and APPS_FOUNDRY_ORG in [org.id for org in user.organizations.all()]:
        return True
    else:
        return False


users_roles = db.Table(
    'cas_users_roles',
    db.Column('user_id', db.Integer, db.ForeignKey('cas_users.id'), primary_key=True),
    db.Column('role_id', db.Integer, db.ForeignKey('cas_roles.id'), primary_key=True),
)

perm_roles = db.Table(
    'cas_perms_roles',
    db.Column('perm_id', db.Integer, db.ForeignKey('cas_perms.id'), primary_key=True),
    db.Column('role_id', db.Integer, db.ForeignKey('cas_roles.id'), primary_key=True)
)

users_wish_list_items = db.Table(
    'wish_list',
    db.Column('user_id', db.Integer, db.ForeignKey('cas_users.id'), primary_key=True),
    db.Column('item_id', db.Integer, db.ForeignKey('core_items.id'), primary_key=True)
)


class Role(models.SoftDeletableMixin, models.TimestampMixin, db.Model):
    """ A group that is used for authenticating `Users`.
    """
    __tablename__ = 'cas_roles'
    name = db.Column(db.String(80), unique=True, nullable=False)
    perms = db.relationship(
        Perm,
        secondary=perm_roles,
        backref=db.backref('cas_roles', lazy='dynamic'),
        lazy='dynamic',
        uselist=True)

    @property
    def api_url(self):
        # api role not yet ready
        return None


class User(models.SoftDeletableMixin, models.TimestampMixin, Party):
    """ A single account that can access the Scoop APIs.

    These generally represent humans that have a registered account, but they can also represent a
    3rd-party service that needs to authenticate to perform actions against the Scoop API.
    """
    __tablename__ = 'cas_users'

    id = db.Column(db.Integer, db.ForeignKey('parties.id'), primary_key=True)
    username = db.Column(db.String(100), unique=True, nullable=False)
    email = db.Column(db.String(150), unique=True, nullable=False)
    phone_number = db.Column(db.String(100), nullable=True)

    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    last_login = db.Column(db.DateTime)
    password = db.Column(db.String(PW_HASH_BYTES * 2))
    salt = db.Column(db.String(PW_SALT_BYTES * 2))

    is_verified = db.Column(db.Boolean(), default=False, nullable=False)
    excluded_from_reporting = db.Column(db.Boolean(), default=False, nullable=False)

    origin_client_id = db.Column(db.Integer, db.ForeignKey('cas_clients.id'))
    origin_client = db.relationship(Client)

    reset_password_token = db.Column(db.String(200), unique=True)
    key_verify = db.Column(db.String(200), unique=True)
    fb_id = db.Column(db.String(50), unique=True)

    note = db.Column(db.Text)

    referral_code = db.Column(db.String(50), unique=True)

    referral_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'))
    referrer = db.relationship(
        'User',
        backref=db.backref('referred_users'),
        remote_side=id,
        foreign_keys=[referral_id, ]
    )

    profile_pic_url = db.Column(db.String(255), doc='user-uploaded profile picture')
    allow_age_restricted_content = db.Column(db.Boolean(), default=True, nullable=False)

    # this is used for ePerpus
    level = db.Column(db.String(50))

    roles = db.relationship(
        Role,
        secondary=users_roles,
        backref=db.backref('users', lazy='dynamic'),
        lazy='dynamic',
        uselist=True,
    )

    wish_list_items = db.relationship(
        'Item',
        secondary=users_wish_list_items,
        backref=db.backref('wish_list_users', lazy='dynamic'),
        lazy='dynamic',
        uselist=True,
    )

    def set_password(self, password):
        self.salt = os.urandom(PW_SALT_BYTES).encode('hex')
        self.password = self.hash_password(password)

    def hash_password(self, plaintext_password):
        return (
            scrypt
                .hash(
                str(plaintext_password),
                self.salt.decode('hex'),
                buflen=PW_HASH_BYTES)
                .encode('hex'))

    def verify_password(self, plaintext_password):
        hashed = self.hash_password(plaintext_password)
        return self.password == hashed

    __mapper_args__ = {
        'polymorphic_identity': 'user'
    }

    def __repr__(self):
        return '<username: {}>'.format(self.username)


class Profile(models.TimestampMixin, db.Model):
    __tablename__ = 'cas_profiles'

    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'))
    user = db.relationship(User, backref=db.backref('profile', uselist=False))

    gender = db.Column(db.String(1))
    birthdate = db.Column(db.DateTime)

    origin_latitude = db.Column(db.String(20))
    origin_longitude = db.Column(db.String(20))
    origin_country_code = db.Column(db.String(20))
    current_latitude = db.Column(db.String(20))
    current_longitude = db.Column(db.String(20))

    origin_device_model = db.Column(db.String(250))
    origin_partner_id = db.Column(db.Integer)
    source_type = db.Column(db.Integer)


class RoleNestedSchema(ma.Schema):
    id = ma_fields.Int()
    title = ma_fields.Str(attribute='name')
    href = ma_fields.Url(attribute='api_url')


class ProfileSchema(ma.Schema):
    class Meta:
        model = Profile

    birthdate = ma_fields.DateTime(allow_none=True)
    origin_device_model = ma_fields.Str(allow_none=True)
    gender = ma_fields.Str(allow_none=True)

    origin_latitude = ma_fields.Str(allow_none=True)
    origin_longitude = ma_fields.Str(allow_none=True)
    origin_country_code = ma_fields.Str(allow_none=True)

    origin_device_model = ma_fields.Str(allow_none=True)
    origin_partner_id = ma_fields.Str(allow_none=True)
    source_type = ma_fields.Int(allow_none=True)

    # gender = ma_fields.Method('dump_gender', deserialize='load_gender')

    # def dump_gender(self, obj):
    #     # generate response
    #     genders = defaultdict(lambda: None)
    #     genders.update({
    #         'male': '1',
    #         'female': '2'
    #     })
    #     return genders[obj.gender]
    #
    # def load_gender(self, value):
    #     # to save/update data
    #     genders = defaultdict(lambda: 'not specified')
    #     genders.update({
    #         '1': 'male',
    #         '2': 'female',
    #     })
    #     return genders[str(value)]


class UserBaseSchema(ma.Schema):
    class Meta:
        model = User

    id = ma_fields.Int(dump_only=True)
    username = ma_fields.Str(dump_only=True, )
    email = ma_fields.Str(dump_only=True)
    password = ma_fields.Str(load_only=True)
    first_name = ma_fields.Str(allow_none=True)
    last_name = ma_fields.Str(allow_none=True)
    is_verified = ma_fields.Bool(dump_only=True)
    is_active = ma_fields.Bool(dump_only=True)
    origin_client_id = ma_fields.Integer(dump_only=True)
    level = ma_fields.Str(allow_none=True)
    phone_number = ma_fields.Str(allow_none=True, required=False)

    profile_pic = ma_fields.Method('get_profile_pic', dump_only=True)

    def get_profile_pic(self, obj):
        return {
            'href': (obj.profile_pic_url or
                     "https://images.apps-foundry.com/scoop/defaults/user-profile-pic.png"),
            'title': "Gambar Profil"
        }

    organizations = ma_fields.Method('get_user_orgs', dump_only=True)

    def get_user_orgs(self, obj):

        sess = db.session()
        orgs = []
        current_role = [role.id for role in obj.roles]

        # this case for internal admin.
        if RoleType.internal_admin_eperpus in current_role:
            from app.users.organizations import Organization, OrganizationType
            org_obj = Organization.query.filter(
                Organization.type == OrganizationType.shared_library.value,
                Organization.parent_organization_id == None,
                Organization.is_active == True).order_by(desc(Organization.created)).all()

            for current_obj in org_obj:
                orgs.append({
                    'id': current_obj.id,
                    'name': current_obj.name,
                    'href': current_obj.api_url,
                    'is_manager': True,
                    'is_parent_organization': False if current_obj.parent_organization_id else True,
                    'type': current_obj.type,
                    'logo_url': current_obj.logo_url,
                    'username_prefix': current_obj.username_prefix,
                    'maximum_user_allowed': current_obj.maximum_user_allowed,
                    'app_name': current_obj.app_name
                })
        else:
            # I don't like this at all.  It would be best if this could be queried through 'users'
            from app.users.organizations import OrganizationUser
            import operator

            user_organizations = sess.query(OrganizationUser).filter_by(user=obj)
            for org_user in user_organizations:
                allowed = True

                if getattr(g, 'allowed_org_ids', None):
                    # for cms portal, only show user groups/organizations belong to the same eperpus
                    if org_user.organization.id not in g.allowed_org_ids:
                        allowed = False

                if allowed:
                    orgs.append({
                        'id': org_user.organization.id,
                        'name': org_user.organization.name,
                        'href': org_user.organization.api_url,
                        'is_manager': org_user.is_manager,
                        'type': org_user.organization.type,
                        'is_parent_organization': False if org_user.organization.parent_organization_id else True,
                        'logo_url': org_user.organization.logo_url,
                        'username_prefix': org_user.organization.username_prefix,
                        'maximum_user_allowed': org_user.organization.maximum_user_allowed,
                        'app_name': org_user.organization.app_name
                    })

            # sorted data by id
            if orgs: orgs.sort(key=operator.itemgetter('id'), reverse=True)
        return orgs

    roles = ma_fields.List(ma_fields.Nested(RoleNestedSchema), dump_only=True)
    note = ma_fields.Str(dump_only=True)
    signup_date = ma_fields.DateTime(attribute='created', dump_only=True)
    profile = ma_fields.Nested(ProfileSchema, required=False)
    total_point = ma_fields.Method('get_profile_point', dump_only=True)
    is_display_point = ma_fields.Method('get_point_status_payment', dump_only=True)

    @post_load
    def process_input(self, data):
        if data.get('username', None):
            # only for registration, some update doesn't allow to change username/email
            data['username'] = data.get('username', None).lower().strip()
        if data.get('email', None):
            # only for registration, some update doesn't allow to change username/email
            data['email'] = data.get('email', None).lower().strip()
        return data

    def get_profile_point(self, obj):
        ''' For retrieve GD point of user for profile page at Frontend Side'''
        from app.points.models import PointUser

        exist_point = PointUser.query.filter_by(user_id=obj.id).first()
        return exist_point.point_amount if exist_point else 0

    def get_point_status_payment(self, obj):
        ''' For status displaying point of user at profile page,
            for purpose if someday point is takedown again, BE can controlled from portal! '''
        from app.payment_gateways.models import PaymentGateway

        active_payment = PaymentGateway.query.filter_by(id=PaymentGateway.Type.point.value, is_active=True).first()
        return True if active_payment else False


class UserSchema(UserBaseSchema):
    # schema for update user (from scoop/web)

    # some existing data didn't have valid email -- ex: eperpus users, publisher users
    #  change email attribute Str so it didn't return null value in current user response
    #     if email null , mobile apps will sign out/not logged in
    email = ma_fields.Str(dump_only=True)
    active_buffet = ma_fields.Method('get_active_buffet', dump_only=True)

    def get_active_buffet(self, obj):
        from .buffets import UserBuffet
        from datetime import timedelta

        user_buffet_data = obj.buffets.filter(UserBuffet.valid_to > datetime.now()).all()
        return {
            "href": user_buffet_data[0].api_url,
            "title": "Active Buffet Status",
            "offers": [
                {
                    "id": user_buffet.offerbuffet.offer_id,
                    "title": user_buffet.offerbuffet.offer.name,
                    "valid_until": isoformat(user_buffet.valid_to),
                    "valid_from": isoformat(user_buffet.valid_to - timedelta(days=30)),
                } for user_buffet in user_buffet_data
            ],
            # for compatibility with getscoop, can be removed after getscoop updated
            "valid_until": isoformat(user_buffet_data[0].valid_to)
        } if user_buffet_data else None

    allow_age_restricted_content = ma_fields.Bool()


class UserRegisterSchema(UserSchema):
    # schema for create new user from scoop/web scoop
    username = ma_fields.Str()
    email = ma_fields.Email()
    password = ma_fields.Str(load_only=True)
    fb_id = ma_fields.Str(allow_none=True)
    referral_code = ma_fields.Str(load_only=True)
    referral_id = ma_fields.Str(dump_only=True)
    origin_client_id = ma_fields.Integer()

    allow_age_restricted_content = ma_fields.Bool(required=False, default=False)
    roles = ma_fields.List(ma_fields.Integer, attribute='roles', required=False)
    organizations = ma_fields.List(ma_fields.Integer, attribute='organizations', required=False)
    phone_number = ma_fields.Str(required=False, allow_none=True)
    # is_verified = ma_fields.Bool(load_only=True)


class UserOrganizationUpdateSchema(UserBaseSchema):
    # schema to update user from eperpus cms (update a member of an organization)
    username = ma_fields.Str(required=True)
    email = ma_fields.Email(required=True)
    change_password = ma_fields.Bool(load_only=True, missing=False)
    password = ma_fields.Str(load_only=True, allow_none=True)


class UserOrganizationCreateSchema(UserOrganizationUpdateSchema):
    # schema to create user from eperpus cms (create new member of an organization)
    username = ma_fields.Str(required=True)
    email = ma_fields.Email(required=True)
    password = ma_fields.Str(load_only=True, required=True)


class ProfileField(fields.Raw):

    def format(self, value):

        if not value:
            return None

        genders = defaultdict(lambda key: 'not specified')
        genders.update({
            '1': 'male',
            '2': 'female',
        })

        sources = defaultdict(lambda key: 'scoop')
        sources.update({
            1: 'scoop',
            2: 'facebook'
        })

        origin_client_json = None
        if value.user.origin_client:
            origin_client_json = {
                'id': value.user.origin_client.id,
                'title': value.user.origin_client.name,
                'href': value.user.origin_client.api_url,
            }

        return {
            'gender': genders[value.gender],
            'birthdate': value.birthdate.date.isoformat() if value.birthdate else None,
            'account_meta': {
                'origin': {
                    'latitude': value.origin_latitude,
                    'longitude': value.origin_longitude,
                    'country_code': value.origin_country_code,
                    'source': sources[value.source_type],
                    'device_model': value.origin_device_model,
                    'origin_client': origin_client_json,
                },
                'current': {
                    'latitude': value.current_latitude,
                    'longitude': value.current_longitude,
                }
            }
        }


def create_user_from_gramedia(json_data):
    session = db.session()
    user_schema = UserRegisterSchema()

    new_user = save_user_from_gramedia(user_schema, session, json_data=json_data)
    return new_user


def save_user_from_gramedia(user_schema, session, json_data):
    # username and email will be lowered case in UserRegisterSchema PostLoad
    entity = schema_load(user_schema, json_data=json_data)
    if not entity.get('origin_client_id', None):
        entity['origin_client_id'] = g.current_client.id

    assert_username_and_email(session, entity["username"], entity["email"])

    profile_entity = entity.pop('profile', None)

    new_user = User()
    for key, value in entity.items():
        if hasattr(new_user, key):
            setattr(new_user, key, value)

    new_user.roles = [session.query(Role).get(RoleType.verified_user.value), ]

    if profile_entity:
        profile = Profile()
        for key, value in profile_entity.items():
            if hasattr(profile, key):
                setattr(profile, key, value)
        profile.user = new_user
        session.add(profile)
    if not new_user.is_verified:
        new_user.key_verify = get_verify_token()
        new_user.is_verified = True

    session.add(new_user)
    session.commit()
    return new_user


def get_user_from_gramedia_db(email):
    session = get_gramedia_session()
    user = session.query(GramediaUser).filter(GramediaUser.email == email).first()

    json_data = {
        'username': user.username,
        'email': user.email,
        'password': user.password,
        'origin_client_id': 7
    }
    user = create_user_from_gramedia(json_data)
    return user


def custom_list_limit(selection, offset=0, limit=None):
    return selection[offset:(limit + offset if limit is not None else None)]


def reformat_api_url(orderline_id, offer_type_id):
    try:
        return url_for('users.subs-renewal-detail', orderline_id=orderline_id, offer_type_id=offer_type_id, _external=True)
    except RuntimeError:
        return None


def reformat_renewal(all_subs, exist_user, user_id):
    from app.paymentgateway_connectors.creditcard.models import PaymentCreditCardRenewal
    from app.payment_gateways.models import PaymentGateway

    response_data = []

    payment_credit_card = PaymentGateway.query.filter_by(id=CREDITCARD).first()
    for data_subs in all_subs:

        if payment_credit_card.is_renewal:
            renewal_record = PaymentCreditCardRenewal.query.filter(
                PaymentCreditCardRenewal.user_id == user_id,
                PaymentCreditCardRenewal.orderline_id == data_subs.orderline.id,
                PaymentCreditCardRenewal.offer_id == data_subs.orderline.offer_id,
                PaymentCreditCardRenewal.renewal_status == RENEWAL_STATUS[NEW]
            ).first()
        else:
            renewal_record = None

        valid_to = data_subs.valid_to.isoformat()
        if data_subs.orderline.offer.offer_type_id == SUBSCRIPTIONS:
            if data_subs.quantity_unit == UNIT and data_subs.current_quantity >= 4:
                valid_to = (datetime.now() + relativedelta(months=1)).isoformat()  # asume 1 week can release 1 edition

        href_details = reformat_api_url(data_subs.orderline.id, data_subs.orderline.offer.offer_type_id)
        response_data.append(
            {
                'href': href_details,
                'valid_to': valid_to,
                'final_price': '%.2f' % data_subs.orderline.final_price,
                'currency_code': data_subs.orderline.currency_code,
                'username': exist_user.email,
                'subscription_name': data_subs.orderline.offer.long_name,
                'payment_id': data_subs.orderline.order.paymentgateway.id,
                'payment_name': data_subs.orderline.order.paymentgateway.name,
                'renewal': {
                    'id': renewal_record.id,
                    'status': renewal_record.renewal_status,
                    'note': renewal_record.note,
                    'masked_credit_card': renewal_record.masked_credit_card
                } if renewal_record else None
            })

    return response_data


@blueprint.route('/users/current-user/subscription-renewals', methods=['GET', ])
@user_is_authenticated
def get_subscription_renewals():
    from app.items.models import UserSubscription
    from app.users.buffets import UserBuffet

    args = request.args.to_dict()
    limit, offset, total_count = int(args.get('limit', 20)), int(args.get('offset', 0)), 0
    response_data = []

    try:
        exist_user = g.current_user
        user_id = g.current_user.id

        exist_buffet = UserBuffet.query.filter(
            UserBuffet.user_id == user_id, UserBuffet.orderline_id != (None or 0),
            UserBuffet.is_restore == False, UserBuffet.valid_to >= datetime.now()).order_by(desc(UserBuffet.valid_to)).all()

        exist_duration = UserSubscription.query.filter(
            UserSubscription.user_id == user_id, UserSubscription.valid_to >= datetime.now(),
            UserSubscription.orderline_id != (None or 0), UserSubscription.quantity_unit.in_([DAILY, WEEKLY, MONTHLY])
        ).order_by(desc(UserSubscription.valid_to)).all()

        exist_edition = UserSubscription.query.filter(UserSubscription.user_id == user_id,
            UserSubscription.quantity_unit == UNIT, UserSubscription.orderline_id != (None or 0),
            UserSubscription.current_quantity > 0).order_by(desc(UserSubscription.current_quantity)).all()

        all_subs = exist_buffet + exist_duration + exist_edition
        total_count = len(all_subs)
        if limit:
            all_subs = custom_list_limit(all_subs, offset, limit)

        # reformat responses.
        response_data = reformat_renewal(all_subs, exist_user, user_id)

    except Exception as e:
        print e
        pass

    response_json = jsonify({
        'subscriptions': response_data,
        'metadata': {'resultset': {'offset': offset, 'limit': limit, 'count': total_count}}})
    response_json.status_code = OK
    return response_json


@blueprint.route('/users/current-user/subscription-renewals/<int:orderline_id>/<int:offer_type_id>',
                 methods=['GET', ], endpoint='subs-renewal-detail')
@user_is_authenticated
def get_subscription_renewals_details(orderline_id, offer_type_id):
    from app.items.models import UserSubscription
    from app.users.buffets import UserBuffet

    try:
        exist_user = g.current_user
        user_id = g.current_user.id
        all_subs = []

        if offer_type_id == BUFFET:
            all_subs = UserBuffet.query.filter(
                UserBuffet.user_id == user_id, UserBuffet.orderline_id == orderline_id,
                UserBuffet.is_restore == False, UserBuffet.valid_to >= datetime.now()).order_by(
                desc(UserBuffet.valid_to)).all()

        if offer_type_id == SUBSCRIPTIONS:
            exist_duration = UserSubscription.query.filter(
                UserSubscription.user_id == user_id, UserSubscription.valid_to >= datetime.now(),
                UserSubscription.orderline_id == orderline_id, UserSubscription.quantity_unit.in_([DAILY, WEEKLY, MONTHLY])
            ).order_by(desc(UserSubscription.valid_to)).all()

            exist_edition = UserSubscription.query.filter(UserSubscription.user_id == user_id,
                UserSubscription.quantity_unit == UNIT, UserSubscription.orderline_id == orderline_id,
                UserSubscription.current_quantity > 0).order_by(desc(UserSubscription.current_quantity)).all()

            all_subs = exist_duration + exist_edition

        response_data = reformat_renewal(all_subs, exist_user, user_id)
        data = response_data[0] if response_data else {}

    except Exception as e:
        data = {}

    response_json = jsonify(data)
    response_json.status_code = OK
    return response_json

@blueprint.route('/users/portal-info/<int:user_id>',methods=['GET', ], endpoint='user-info-detail')
@user_is_authenticated
def get_users_portal_info(user_id):
    """
        This api for simplify portal when get user info,
        This api will access only from new portal 3.0 with python 3 and django 2.2

        Old portal ebooks method is complicate when retrieve data of users for access.
        Old portal flow api --> Get users, Get Vendor, Get Organization, Get perms, Get Brands API, what the..??!
    """

    from app.users.organizations import OrganizationUser
    from app.master.models import Vendor

    exist_users = User.query.filter_by(id=user_id).first()
    if not exist_users: return NotFound

    # retrieve access!
    permission = []
    for access in exist_users.roles.all(): permission = permission + access.perms.all()
    permission = list(set(permission))

    # retrieve organizations!
    user_organizations = OrganizationUser.query.filter_by(user=exist_users).all()
    data_organization = [org_user for org_user in user_organizations]

    # retrieve vendor access!
    vendor = []
    for data_org in user_organizations:
        vendor = vendor + Vendor.query.filter_by(organization_id=data_org.organization.id).all()
    vendors = list(set(vendor))

    data = {
        "id": exist_users.id,
        "email": exist_users.email,
        "full_name": "{} {}".format(exist_users.first_name if exist_users.first_name else '',
                                    exist_users.last_name if exist_users.last_name else '',).strip().title(),
        "role": [role_.id for role_ in exist_users.roles],
        "roles": [{'id': role_.id, 'name': role_.name} for role_ in exist_users.roles],
        "access": [perm.name for perm in permission],
        "organizations": [{'id': org_.organization.id, 'name': org_.organization.name} for org_ in data_organization],
        "organization": [org_.organization.id for org_ in data_organization],
        "vendor": [vnd.id for vnd in vendors],
        "vendors": [{'id': vnd.id, 'name': vnd.name} for vnd in vendors]
    }
    response_json = jsonify(data)
    response_json.status_code = OK
    return response_json
