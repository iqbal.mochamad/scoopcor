"""
User Device Auth/De-Authorization
=================================

Users can only access scoopcor on a limited number of devices at any given point in time.  Currently we allow 5
devices.

Device authorization/de-authorization is enforced from frontend and is checked when a user attempts to log into
the application.
"""
from __future__ import absolute_import, unicode_literals

from datetime import datetime

from flask_appsfoundry.models import TimestampMixin
from sqlalchemy.dialects.postgresql import ARRAY

from app import db


class Device(db.Model, TimestampMixin):
    """
    unique_id -> an unique id generated by our system
    device_id -> id provided by device
    device_model ->
    device_imei -> imei of a device, not all devices have imei number
    """
    __tablename__ = 'cas_devices'

    unique_id = db.Column(db.String(80), unique=True, nullable=False)
    device_mid = db.Column(db.String(80))
    device_model = db.Column(db.String(80))
    device_imei = db.Column(db.String(20), nullable=True)


class DeviceHistory(db.Model, TimestampMixin):

    __tablename__ = 'cas_log_devicehistory'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    client_id = db.Column(db.String(80))
    device_id = db.Column(db.String(250))


class UserDevice(db.Model, TimestampMixin):
    """
    unique_id -> an unique id generated by our system
    device_id -> id provided by device
    device_model ->
    device_imei -> imei of a device, not all devices have imei number
    """
    __tablename__ = 'cas_userdevices'

    user_id = db.Column(db.Integer, nullable=False)
    client_id = db.Column(db.String(80))
    device_id = db.Column(ARRAY(db.String))


class LogDeauthorized(db.Model):
    """ Historical information that records when a user has deauthorized a device.

    """
    __tablename__ = 'cas_log_deauthorized'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    created = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
