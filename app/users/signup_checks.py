"""
User Signup Checks
==================

A convenience API for frontend to validate a provided attribute for a user is valid before submitting
their full user data (when creating a new account), ex -> Does the provided e-mail address already exist in
SCOOP?

**NOTE: ** This API is also used by some 3rd party services, like Elevenia, when signing up new users in SCOOP.
"""
from __future__ import absolute_import, unicode_literals

from flask import Blueprint, url_for, request, jsonify, make_response

from app import db
from flask_appsfoundry.exceptions import BadRequest, Conflict, NotFound


blueprint = Blueprint('signup_checks', __name__, url_prefix='/v1/users/signup-check')


available_validators = {}


def validate_by_email(email_address):
    from app.users.users import User
    s = db.session()
    return s.query(User).filter(User.email.ilike(email_address)).first()

available_validators['email'] = validate_by_email


@blueprint.route('')
def root():
    return jsonify({
        "urls": [
            url_for('signup_checks.validate_attribute', attribute_name=attr_name, _external=True)
            for attr_name in available_validators
        ]
    })


@blueprint.route('/<attribute_name>')
def validate_attribute(attribute_name):
    try:
        if 'value' not in request.args:
            raise BadRequest
        existing_user = available_validators[attribute_name](request.args.get('value'))
    except KeyError:
        raise NotFound(
            developer_message="{} is invalid.".format(attribute_name),
            user_message="{} is invalid.".format(attribute_name)
        )

    if existing_user:
        raise Conflict

    return make_response("", 200)
