"""
Mothballed
==========

Purgatory for code.

Anything in here is related to features that we no longer support, but we
don't want to yet remove from the application.
"""
from flask_appsfoundry.models import SoftDeletableMixin, TimestampMixin

from app import db


class UserCafes(db.Model, SoftDeletableMixin, TimestampMixin):
    """ User Cafes was used with a feature called SCOOP CAFE.

    This feature has been abandoned.  API endpoints to support this feature can
    primarily be found within SCOOPCAS.
    """
    __tablename__ = 'cas_cafes_passwords'

    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    password = db.Column(db.String(100))
    valid_from = db.Column(db.DateTime)
    valid_to = db.Column(db.DateTime)
