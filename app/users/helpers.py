from __future__ import unicode_literals, absolute_import

import base64

import requests
from flask import current_app, request
from flask_appsfoundry.exceptions import NotFound
from sqlalchemy.sql import ClauseElement

from app import app
from app.helpers import generate_invoice
from app.uploads.aws.helpers import connect_gramedia_s3


def get_search_results(db_id, log):
    solr_url = current_app.config['SOLR_BASE_URL'] + '/scoop-users/select'
    solr_url += '?fq=organization_ids:{}'.format(db_id)
    solr_url += '&start={}'.format(request.args.get('offset', type=int, default=0))
    solr_url += '&rows={}'.format(request.args.get('limit', type=int, default=20))
    solr_url += '&facet=on&facet.field=is_active'
    q = request.args.get('q', '')
    # solr_url += '&q=*{}*'.format(q)
    solr_url += '&q=_text_:*{}*'.format(q)
    order = request.args.get('order')
    if order:
        is_desc = order.startswith('-')
        if is_desc:
            order = order[1:]
        solr_url += "&sort={} {}".format(order, "desc" if is_desc else "asc")
    solr_url += "&wt=json"
    log.info("query was -> {}".format(solr_url))
    return requests.get(solr_url).json()


def assert_valid_wallet_user(user):
    """
    :param user:
    :raises: :class:`NotFound` if user don't have wallet yet.
    :returns: None if the authorization checks succeed.
    """
    if not user or not user.wallet:
        raise NotFound(developer_message='Wallet resource not found for user_id:{}'.format(user.id if user else None),
                       user_message='User Wallet Not Found')


def upload_logo_to_s3(data, orgs_id):
    dec = base64.b64decode(data)
    s3_server = connect_gramedia_s3()
    bucket_name = app.config['BOTO3_GRAMEDIA_STATIC_BUCKET']
    location = '{folder}/{orgs_id}/{name}.png'.format(folder='eperpus-logo', orgs_id=orgs_id, name=generate_invoice())
    s3_server.put_object(Bucket=bucket_name, Key=location, Body=dec, ContentType='image/png', ACL='public-read',
                         ContentEncoding='base64')

    url = "{base}{bucket}/{location}".format(base=app.config['AWS_PUBLIC'], bucket=bucket_name, location=location)

    return url


def get_organization_cart(org_id, session):
    from app.users.organizations import OrganizationCart
    cart, created = get_or_create(session, OrganizationCart, organization_id=org_id, is_active=True)
    return cart


def get_or_create(session, model, defaults=None, **kwargs):
    """Taken from here
    https://stackoverflow.com/questions/2546207/does-sqlalchemy-have-an-equivalent-of-djangos-get-or-create"""
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance, False
    else:
        params = dict((k, v) for k, v in kwargs.iteritems() if not isinstance(v, ClauseElement))
        params.update(defaults or {})
        instance = model(**kwargs)
        session.add(instance)
        session.commit()
        return instance, True
