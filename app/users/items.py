from __future__ import absolute_import, unicode_literals

from flask import Blueprint, current_app, g, request
from marshmallow import fields
from sqlalchemy import desc

from app import ma, db
from app.auth.decorators import user_is_authenticated
from app.auth.helpers import get_user_role_ids
from app.constants import RoleType
from app.items.choices import STATUS_READY_FOR_CONSUME, BOOK, MAGAZINE, NEWSPAPER, AUDIOBOOK
from app.items.choices import STATUS_TYPES
from app.items.models import UserItem, Item
from app.master.models import Brand, Vendor
from app.users.users import is_apps_foundry_users
from app.utils.http import get_offset_limit
from app.utils.marshmallow_helpers import SimpleHRefSchema
from app.utils.responses import get_response

blueprint = Blueprint('user_items', __name__, url_prefix='/v1/users')


@blueprint.route('/<user:user>/owned-items')
@user_is_authenticated
def get_user_owned_items(user):
    if user == 'current-user': user = g.current_user

    offset, limit = get_offset_limit(request)
    api_version = request.headers.get('version', '1.0')

    item_ids = request.args.get('item_id', type=str, default=None)
    item_ids = item_ids.split(',') if item_ids else []

    session = db.session()
    owned_items, count = get_user_items_data(user=user, item_ids=item_ids, limit=limit, offset=offset,
                                             session=session, api_version=api_version)

    items = []
    if owned_items:
        items.extend([OwnedItemSchema().dump(owned_item.item).data for owned_item in owned_items])

    if item_ids:
        # if front end send list of item id ( filter by item id's ):
        #   check if user is a publisher or apps foundry user or some other criteria
        #       and get other eligible items (item that fit the criteria but not yet exists in owned items)
        eligible_items = get_other_eligible_items(user, session, item_ids, owned_items)
        if eligible_items:
            items.extend([OwnedItemSchema().dump(item).data for item in eligible_items])
            count += len(eligible_items)

    return get_response(data=items, key_name="items", count=count, limit=limit, offset=offset)


def get_user_items_data(user, item_ids, limit, offset, session=None, api_version='1.0'):
    session = session or db.session()

    if api_version == '3.0':
        # api version 3.0 only for SUPPORT Audiobooks.
        query_set = session.query(UserItem).filter(
            UserItem.user_id == user.id,
            UserItem.user_buffet_id == None,
            UserItem.itemtype_id.in_([BOOK, MAGAZINE, NEWSPAPER, AUDIOBOOK]))
    else:
        query_set = session.query(UserItem).filter(
            UserItem.user_id == user.id,
            UserItem.user_buffet_id == None,
            UserItem.itemtype_id.in_([BOOK, MAGAZINE, NEWSPAPER]))


    if item_ids: query_set = query_set.filter(UserItem.item_id.in_(item_ids))

    query_set = query_set.filter_by(is_active=True)
    remote_item_id = request.args.get('remote_item_id', type=str, default=None)
    brand_id = request.args.get('brand_id', type=str, default=None)
    vendor_id = request.args.get('vendor_id', type=str, default=None)
    item_type = request.args.get('item_type', type=str, default="")
    q = request.args.get('q', type=str, default="")
    if remote_item_id or brand_id or vendor_id or item_type or q:
        query_set = query_set.join(UserItem.item)

    if item_type:
        query_set = query_set.filter(Item.item_type == item_type)

    if q:
        query_set = query_set.filter(Item.name.ilike('%{}%'.format(q)))

    if remote_item_id:
        # used by remote service to check their remote checkout process
        # for Gramedia.com, to check whether a user already owned an item or not
        # search by remote item id, exampled: SC00P123 (123 is offer id)
        from app.offers.models import Offer
        offer_ids = [int(o.replace('SC00P', '')) for o in remote_item_id.split(',')]
        query_set = query_set.join(Item.core_offers).filter(Offer.id.in_(offer_ids))

    # brand id and vendor used by White Label
    if brand_id:
        query_set = query_set.filter(Item.brand_id.in_(brand_id.split(',')))
    if vendor_id:
        from app.master.models import Brand
        query_set = query_set.join(Item.brand).filter(Brand.vendor_id.in_(vendor_id.split(',')))

    # get total rows for metadata resultset
    count = query_set.count()

    # create order by clause
    order_by = request.args.get('order', type=str, default=None)
    if order_by:
        for order in order_by.split(','):
            if order[0] == '-':  # descending
                order_param = '%s %s' % (order[1:], 'desc')
                query_set = query_set.order_by(desc(order[1:]))
            else:
                query_set = query_set.order_by(order)
    else:
        # order by latest purchased
        query_set = query_set.order_by(desc(UserItem.id))

    owned_items = query_set.offset(offset).limit(limit).all()

    # ideally, we only return list of Item..
    #   but cause old api (for old android/ios) still need UserItem.created, so we forced to return the value here
    # owned_items : list of UserItem
    # eligible_items : list of Item
    return owned_items, count


class BrandSchema(SimpleHRefSchema):
    vendor = fields.Nested(SimpleHRefSchema)


class OwnedItemSchema(SimpleHRefSchema):
    image = fields.Function(lambda x: {
        "title": "Cover Image",
        "href": current_app.config['MEDIA_BASE_URL'] + x.image_highres
    })
    thumbnail = fields.Function(lambda  x: {
        "title": "Thumbnail Image",
        "href": current_app.config['MEDIA_BASE_URL'] + x.thumb_image_highres
    })
    content_type = fields.Str()
    is_age_restricted = fields.Function(lambda x: x.parentalcontrol_id == 2)
    item_type = fields.Str()
    edition_code = fields.Str()
    issue_number = fields.Str()
    release_date = fields.DateTime()
    # authors = fields.List(fields.Nested(SimpleHRefSchema))
    brand = fields.Nested(BrandSchema)


def is_user_role_publisher(user=None):
    allowed_user_role = [RoleType.organization_specific_admin.value, ]
    # RoleType.organization_specific_marketing.value,
    # RoleType.organization_specific_finance.value]
    user_role_ids = get_user_role_ids(user)
    if user_role_ids and any([user_role for user_role in user_role_ids if user_role in allowed_user_role]):
        return True
    else:
        return False


def get_other_eligible_items(user, session, item_ids, owned_items):
    """ get a list of item (that belong to the publisher or an organizations)
    that query by item_ids and not yet exists in owned_items list

    :param user:
    :param session:
    :param item_ids: list of item id
    :param owned_items: list of items that already owned
    :return:
    :rtype: list of items
    """
    is_user_publisher = is_user_role_publisher(user)
    if not is_user_publisher and not is_apps_foundry_users(user):
        # this function only if a user is from a publisher
        return None

    # remove item_id that already exists in owned items list
    if owned_items:
        owned_item_ids = [item.id for item in owned_items]
        for item_id in item_ids:
            if item_id in owned_item_ids:
                item_ids.remove(item_id)

    if item_ids:
        user_org_ids = [org.id for org in user.organizations.all()]
        item_query = session.query(Item)
        if is_user_publisher:
            item_query = session.query(Item).join(Item.brand).join(Brand.vendor)

        item_query = item_query.filter(
            Item.id.in_(item_ids),
            Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            Item.is_active == True,
        )
        if is_user_publisher:
            item_query = item_query.filter(
                Vendor.organization_id.in_(user_org_ids)
            )
        return item_query.all()
    else:
        return []
