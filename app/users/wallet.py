"""
Scoop Wallet
============

Allows a `User` or an `Organization` to hold a balance (in IDR) that can be used for purchasing content.
"""
from __future__ import absolute_import, unicode_literals
from datetime import datetime
from decimal import Decimal

from flask import current_app
from flask import g, jsonify, request, Blueprint
from flask import url_for
from flask_appsfoundry import viewmodels, parsers, marshal
from flask_appsfoundry.serializers import SerializerBase, fields, PaginatedSerializer
from flask_appsfoundry.models import TimestampMixin, SoftDeletableMixin

from app import db, app, ma
from app.auth.decorators import user_is_authenticated
from flask_appsfoundry.exceptions import BadRequest, Conflict, NotFound, UnprocessableEntity, Forbidden
from app.orders.choices import COMPLETE
from app.orders.models import Order
from app.payments.choices.gateways import WALLET as WALLET_PAYMENT_GATEWAY_ID
from app.users.helpers import assert_valid_wallet_user
from app.users.organizations import is_organization_manager, OrganizationConverter
from app.utils.datetimes import get_local_nieve
from app.utils.models import get_url_for_model
from app.utils.serializers import SimpleImplicitHref
from app.utils.viewmodels import HrefAttribute
from .base import Party
from .users import User


WALLET_OFFER_TYPE_ID = 5
PAYMENT = -1
ADD_BALANCE = 1


blueprint = Blueprint('wallets', __name__, url_prefix='/v1')
app.url_map.converters['organization'] = OrganizationConverter


class Wallet(db.Model, SoftDeletableMixin):
    __tablename__ = 'wallets'
    party_id = db.Column(db.Integer, db.ForeignKey('parties.id'))
    party = db.relationship(Party, backref=db.backref('wallet', uselist=False))
    balance = db.Column(db.Numeric(12, 2), default=0)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    modified = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)
    __mapper_args__ = {
        "version_id_col": modified,
        "version_id_generator": lambda version: get_local_nieve(),
    }


class WalletTransaction(db.Model, TimestampMixin, SoftDeletableMixin):
    __tablename__ = 'wallet_transactions'
    wallet_id = db.Column(db.Integer, db.ForeignKey('wallets.id'))
    wallet = db.relationship(Wallet, backref=db.backref('transactions', lazy='dynamic'))
    amount = db.Column(db.Numeric(12, 2))
    created_by = db.Column(db.Integer, db.ForeignKey('cas_users.id'))
    created_by_user = db.relationship(User)
    order_id = db.Column(db.Integer, db.ForeignKey('core_orders.id'))
    order = db.relationship(Order, backref=db.backref('wallet_transactions'))


class UserOrOrganizationNotFoundException(Exception):
    """ Special purpose exception for the Wallet API.

    Raised whenever the user or organization specified in the order request body does not exist in database.
    """


def wallet_payment_transaction(session, order_id, order_amount, party_id):
    """ use wallet as payment method in an order

    used in: app.payments.helpers.charge_transaction

    :param session: db.session
    :param `int` order_id:
    :param `decimal` order_amount:
    :param `int` party_id: party id (owner of the wallet, can be a user or an organization)
    :return:
    """
    assert_order_and_wallet_payment_gateway(session, order_id, party_id)

    wallet = session.query(Wallet).filter(Wallet.party_id == party_id).first()

    assert_wallet_balance(wallet, order_amount)

    # save wallet transactino and reduce wallet balance
    save_wallet_transaction(session, wallet,
                            amount=order_amount,
                            transaction_type=PAYMENT,
                            created_by=g.current_user.id if g.current_user else None,
                            order_id=order_id)


def assert_order_and_wallet_payment_gateway(session, order_id, party_id):
    """ assert order vs party id vs Wallet as Payment gateway

    :param session:
    :param order_id:
    :param party_id:
    :return:
    """
    order = session.query(Order).filter_by(
        id=order_id,
        user_id=party_id,
        paymentgateway_id=WALLET_PAYMENT_GATEWAY_ID).first()
    if not order:
        raise BadRequest(
            developer_message='Order with id {order_id}, user id {user_id} and '
                              'payment gateway id {payment_gateway_id} Not found!'.format(
                                    order_id=order_id, user_id=party_id, payment_gateway_id=WALLET_PAYMENT_GATEWAY_ID
                                ),
            user_message='Order Not found')


def assert_wallet_balance_by_party_id(session, party_id, order_amount):
    wallet = session.query(Wallet).filter(Wallet.party_id == party_id).first()
    assert_wallet_balance(wallet, order_amount)


def assert_wallet_balance(wallet, order_amount):
    """ Assert wallet balance, is it more than order amount, if not, raise BadRequest

    :param wallet: app.wallet.models.wallet
    :param order_amount: order total amount in IDR only as of now
    :return:
    """
    if not wallet or wallet.balance < order_amount:
        raise BadRequest(developer_message='Wallet Payment Invalid, insufficient balance',
                         user_message='Payment Invalid')


def save_wallet_transaction(session, wallet, transaction_type, amount, created_by, order_id):
    try:
        amount_update = -1 * amount if transaction_type == PAYMENT else amount

        wallet_trx = WalletTransaction(wallet=wallet, amount=amount_update, created_by=created_by, order_id=order_id)
        # Update wallet balance
        wallet.balance = wallet.balance + Decimal(amount_update)

        # instead of this, adding to the wallet transactions collection preferred
        session.add(wallet_trx)
        session.add(wallet)
        session.commit()
    except Exception as e:
        session.rollback()
        raise Conflict(developer_message='Failed when trying to save wallet transaction. '
                                         'Error {}'.format(e.message),
                       user_message='Wallet payment failed')


def assert_offer_for_wallet(offer):
    if offer.offer_type_id != WALLET_OFFER_TYPE_ID:
        raise Conflict(developer_message='Wallet Top Up failed. Offer (id {}) type is not a Wallet.'.format(offer.id),
                       user_message='Wallet Top Up failed')


def assert_top_up_wallet(session, order_temp, order_lines_temp, top_up_amount):
    # for top up wallet, 1 order can only have 1 lines, what if more???
    order_line_wallet = get_order_line_for_wallet(order_lines=order_lines_temp)
    if order_line_wallet:
        # price diubah di detail, dan di header
        order_line_wallet.price_idr = top_up_amount
        order_line_wallet.final_price_idr = top_up_amount
        order_temp.price_idr = sum([line.price_idr for line in order_lines_temp])
        order_temp.final_price_idr = sum([line.price_idr for line in order_lines_temp])


def wallet_top_up_transaction(order, session):
    """ Add balance of a wallet via an Order with wallet as an Offer

    used in app.orders.order_complete.order_change_status

    :param order: app.orders.model.order
    :param session: db.session
    :return:
    """
    order_line = get_order_line_for_wallet(order_lines=order.order_lines)
    if order_line and order.order_status == COMPLETE:
        # wallet owner = order.user_id
        wallet = session.query(Wallet).filter(Wallet.party_id == order.user_id).first()
        if not wallet:
            party = get_party(session, party_id=order.user_id)
            wallet = Wallet(party=party, balance=0)

        save_wallet_transaction(session, wallet,
                                amount=order_line.final_price,
                                transaction_type=ADD_BALANCE,
                                created_by=g.current_user.id if g.current_user else None,
                                order_id=order.id)


def get_party(session, party_id):
    party = session.query(Party).get(party_id)
    if party:
        return party
    else:
        raise UserOrOrganizationNotFoundException(
            "User or Organization with ID {} not found!.".format(party_id))


def get_order_line_for_wallet(order_lines):
    order_line = None
    if order_lines:
        for line in order_lines:
            if line.offer.offer_type_id == WALLET_OFFER_TYPE_ID:
                order_line = line

    return order_line


def get_wallet(session, party_id):
    return session.query(Wallet).filter(Wallet.party_id == party_id).first()


def get_wallet_for_organization(session, org_id):
    wallet = get_wallet(session, org_id)
    if wallet:
        return wallet
    else:
        raise NotFound(developer_message='Wallet resource not found for org_id:{}'.format(org_id),
                       user_message='Organization Wallet failed')


class WalletApiSerializer(SerializerBase):
    balance = fields.Float
    transactions = fields.Nested(SimpleImplicitHref())


class WalletTransactionApiSerializer(SerializerBase):
    href = fields.String
    title = fields.String
    date = fields.DateTime('iso8601')
    amount = fields.Float


@blueprint.route('/users/current-user/wallet')
@user_is_authenticated
def user_wallet_details():
    assert_valid_wallet_user(user=g.current_user)
    return jsonify(
        marshal(UserWalletViewModel(g.current_user.wallet), WalletApiSerializer())
    )


@blueprint.route('/users/current-user/wallet/transactions')
@user_is_authenticated
def user_wallet_transaction_list():
    assert_valid_wallet_user(user=g.current_user)
    vm = viewmodels.ListViewmodel(
        g.current_user.wallet.transactions,
        element_viewmodel=UserWalletTransactionViewModel,
        limit=request.args.get('limit', 20),
        offset=request.args.get('offset', 0),
        ordering=parsers.SqlAlchemyOrderingParser().parse_args()['order'] or [],
        filtering=[]
    )
    serializer = PaginatedSerializer(WalletTransactionApiSerializer(), 'wallet_transactions')
    return jsonify(marshal(vm, serializer))


class UserWalletViewModel(viewmodels.SaViewmodelBase):
    @property
    def transactions(self):
        return HrefAttribute(
            title="Transaction History",
            href=url_for('wallets.user_wallet_transaction_list', _external=True)
        )


class UserWalletTransactionViewModel(viewmodels.SaViewmodelBase):
    @property
    def title(self):
        return "Summary Transaction"

    @property
    def href(self):
        return get_url_for_model(self._model_instance.order)


    @property
    def date(self):
        return self._model_instance.created


@blueprint.route('/organizations/<organization:entity>/wallet')
@is_organization_manager
def organization_wallet_details(entity):

    # maybe we handle this by just automatically creating a wallet??
    if not entity.wallet:
        raise NotFound(developer_message='This account does not have a Wallet')

    # org_wallet_schema = WalletDetailsSchema()
    # return org_wallet_schema.jsonify(entity.wallet)

    return jsonify(
        marshal(OrganizationWalletViewModel(entity.wallet), WalletApiSerializer())
    )


@blueprint.route('/organizations/<organization:entity>/wallet', methods=['PUT', ])
@is_organization_manager
def organization_wallet_update(entity):
    """ testing marshmallow with PUT method.. not meant for public consumption

    :param entity:
    :return:
    """
    if not current_app.testing:
        # for testing only.. not for production
        raise Forbidden()

    json_data = request.get_json()
    if not json_data:
        raise BadRequest

    org_wallet_schema = WalletDetailsSchema()
    data, errors = org_wallet_schema.load(json_data, instance=entity.wallet)
    if errors:
        raise UnprocessableEntity(developer_message='Error updating wallet, error: {}'.format(errors))

    db.session.commit()
    return org_wallet_schema.jsonify(data)


class WalletDetailsSchema(ma.ModelSchema):
    class Meta:
        model = Wallet
        # field to expose
        fields = ('balance', 'transactions')

    transactions = ma.Hyperlinks({
        'href': ma.AbsoluteURLFor('wallets.organization_wallet_transaction_list', entity='<party>'),
        'title': "Transaction History"
    })


class OrganizationWalletViewModel(viewmodels.SaViewmodelBase):
    @property
    def transactions(self):
        return HrefAttribute(
            title="Transaction History",
            href=url_for("wallets.organization_wallet_transaction_list",
                         entity=self._model_instance.party, _external=True)
        )


@blueprint.route('/organizations/<organization:entity>/wallet/transactions')
@is_organization_manager
def organization_wallet_transaction_list(entity):
    vm = viewmodels.ListViewmodel(
        entity.wallet.transactions,
        element_viewmodel=OrganizationWalletTransactionViewModel,
        limit=request.args.get('limit', 20),
        offset=request.args.get('offset', 0),
        ordering=parsers.SqlAlchemyOrderingParser().parse_args()['order'] or [],
        filtering=[]
    )
    serializer = PaginatedSerializer(WalletTransactionApiSerializer(), 'wallet_transactions')
    return jsonify(marshal(vm, serializer))


class OrganizationWalletTransactionViewModel(viewmodels.SaViewmodelBase):
    title = property(lambda self: "Summary Transaction")
    href = property(lambda self: get_url_for_model(self._model_instance.order))
    date = property(lambda self: self._model_instance.created)
