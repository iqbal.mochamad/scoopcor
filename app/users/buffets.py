from __future__ import absolute_import, unicode_literals

from httplib import NO_CONTENT

from datetime import datetime

from flask import g, request, jsonify, Blueprint
from flask import url_for
from flask_appsfoundry import viewmodels, parsers, models, marshal
from flask_appsfoundry.serializers import PaginatedSerializer, SerializerBase, fields
from marshmallow.utils import isoformat

from app import db
from app.auth.decorators import user_has_any_tokens
from .users import User


blueprint = Blueprint('buffets', __name__, url_prefix='/v1')


@blueprint.route('/owned_buffets')  # terrible choice of URL.
@user_has_any_tokens('can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext')
def search_user_buffets():
    """ Lists all the user buffets.

    This is an insecure api and should be depreciated as fast as humanly possible.
    It relies on front-end not lying to us about their user id, instead of verifying the
    user's token against the data they're requesting.
    """
    default_filters = (UserBuffet.valid_to >= datetime.utcnow(), )
    filter_exps = UserBuffetsListArgsParser().parse_args()['filters']
    def_filters = [defexp for defexp in default_filters
                   if defexp.left.name
                   not in [reqexp.left.name for reqexp in filter_exps]]
    filter_exps.extend(def_filters)

    if g.current_user:
        vm = viewmodels.ListViewmodel(
            g.current_user.buffets,
            element_viewmodel=UserBuffetViewmodel,
            limit=request.args.get('limit', 20),
            offset=request.args.get('offset', 0),
            ordering=parsers.SqlAlchemyOrderingParser().parse_args()['order'] or [],
            filtering=filter_exps
        )
        serializer = PaginatedSerializer(UserBuffetSerializer(), 'owned_buffets')
        return jsonify(marshal(vm, serializer))
    else:
        response = jsonify({})
        response.status_code = NO_CONTENT
        return response

class UserBuffet(models.TimestampMixin, db.Model):
    """ A scoop user's buffet subscriptions.

    .. note::

        an explicit design consideration is the that user IS permitted to
        purchase the same buffet multiple times, thus may acquire the same
        items multiple times as part of different buffets
    """
    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    user = db.relationship(User, backref=db.backref('buffets', lazy='dynamic'))

    orderline_id = db.Column(db.Integer, db.ForeignKey('core_orderlines.id'), nullable=False)
    orderline = db.relationship("OrderLine")

    offerbuffet_id = db.Column(db.Integer, db.ForeignKey('core_offerbuffets.id'), nullable=False)
    offerbuffet = db.relationship("OfferBuffet")

    valid_to = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    is_restore = db.Column(db.Boolean(), nullable=False)
    is_trial = db.Column(db.Boolean(), nullable=False)

    original_transaction_id = db.Column(db.Text)

    @property
    def api_url(self):
        return url_for('buffets.search_user_buffets', _external=True)

    def save(self):
        # todo: please find where this is used and kill it.
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "user buffet success add"}
        except Exception as e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}


class UserBuffetSerializer(SerializerBase):
    id = fields.Integer
    user_id = fields.Integer
    orderline_id = fields.Integer
    offerbuffet_id = fields.Integer()
    valid_to = fields.DateTime('iso8601')
    valid_from = fields.DateTime('iso8601')
    offerbuffet = fields.Raw
    valid_from = fields.DateTime('iso8601')


class UserBuffetsListArgsParser(parsers.SqlAlchemyFilterParser):
    __model__ = UserBuffet
    id = parsers.IntegerFilter()
    user_id = parsers.IntegerFilter()
    offerbuffet_id = parsers.IntegerFilter()
    orderline_id = parsers.IntegerFilter()
    valid_to = parsers.DateTimeFilter()


class UserBuffetViewmodel(viewmodels.SaViewmodelBase):

    @property
    def offerbuffet(self):
        offer = self._model_instance.offerbuffet.offer
        return {
            "id": self._model_instance.offerbuffet.id,
            "available_from": isoformat(self._model_instance.offerbuffet.available_from),
            "available_until": isoformat(self._model_instance.offerbuffet.available_until),
            "buffet_duration": "P{0.years}Y{0.months}M{0.days}DT{0.hours}H{0.minutes}M{0.seconds}S".format(
                self._model_instance.offerbuffet.buffet_duration),
            "offer": {
                "id": offer.id,
                "name": offer.name,
                "offer_status": offer.offer_status,
                "sort_priority": offer.sort_priority,
                "is_active": offer.is_active,
                "offer_type_id": offer.offer_type_id,
            }
        }
        # return OfferBuffetViewmodel(self._model_instance.offerbuffet)

    @property
    def valid_from(self):
        from datetime import timedelta
        return self._model_instance.valid_to - timedelta(days=30)
