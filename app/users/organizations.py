"""
Organizations
=============

Organizations represent partner companies.

Users can belong to organizations.  Additionally, organizations can have sub-organizations.  Some authorization checks
utilize a User's organization, but group based permissions are primarily based upon Role (which is another kind of
user group).
"""
from __future__ import unicode_literals, absolute_import

import httplib
import json
import logging
from datetime import datetime
from functools import wraps
from httplib import NO_CONTENT, CREATED

import requests
from enum import Enum
from flask import url_for, jsonify, request, make_response, Blueprint, g
from flask_appsfoundry import models, parsers
from flask_appsfoundry.exceptions import NotFound, Forbidden, Conflict, MethodNotAllowed, Unauthorized, \
    UnprocessableEntity, BadRequest
from flask_appsfoundry.models import TimestampMixin
from flask_babel import gettext
from marshmallow import fields
from marshmallow.utils import isoformat
from sqlalchemy import desc
from validate_email import validate_email
from werkzeug.routing import BaseConverter

from app import app, db, ma
from app.auth.decorators import user_is_authenticated, user_has_any_tokens
from app.auth.helpers import is_superuser
from app.auth.models import Client
from app.constants import RoleType
from app.general.models import Address
from app.helpers import slugify, generate_slug, regenerate_slug
from app.offers.models import Offer
from app.users.helpers import upload_logo_to_s3
from app.utils.datetimes import get_utc_nieve
from app.utils.http import get_offset_limit
from app.utils.marshmallow_helpers import create_slug, schema_load
from app.utils.models import pg_enum_column
from app.utils.responses import generate_weak_etag
from app.utils.serializers import ImplicitRelHref
from app.utils.shims.parsers import WildcardFilter
from .base import Party
from .helpers import get_search_results
from .users import User, update_user, assert_username_and_email, \
    UserOrganizationUpdateSchema, UserOrganizationCreateSchema, save_new_user

blueprint = Blueprint('organization', __name__, url_prefix='/v1/organizations')


def get_organization(org_id, session=None):
    session = session or db.session()
    organization = session.query(Organization).get(org_id)
    if not organization:
        raise NotFound
    return organization


class OrganizationConverter(BaseConverter):
    def to_python(self, value):
        organization = get_organization(value)
        return organization

    def to_url(self, value):
        return unicode(value.id)


app.url_map.converters['organization'] = OrganizationConverter


def check_is_manager(organization_id, user_id):
    s = db.session
    allowed = False

    current_user = s.query(User).filter_by(id=user_id).first()
    if not current_user: return allowed

    current_role = [role.id for role in current_user.roles]
    if RoleType.internal_admin_eperpus.value in current_role or RoleType.super_admin.value in current_role:
        allowed = True
    else:
        allowed = bool(
            s.query(OrganizationUser).filter_by(
                organization_id=organization_id,
                user_id=user_id,
                is_manager=True
            ).count()
        )
    return allowed


def is_organization_manager(f):
    """ Authorization Requirement: Current `User` must be a **manager** of the `Organization` this endpoint represents.

    If the `User` is a manager of a parent organization, they are considered to be a manager all it's child
    organizations as well.
    """

    @wraps(f)
    def inner(entity, *args, **kwargs):
        s = db.session()
        s.add(entity)

        allowed = False
        current_user = s.query(User).filter_by(id=g.current_user.id).first() if g.current_user else None
        if not current_user: raise Forbidden

        current_role = [role.id for role in current_user.roles]
        if RoleType.internal_admin_eperpus.value in current_role or RoleType.super_admin.value in current_role:
            allowed = True
        else:
            if entity.parent_organization:
                allowed = bool(
                    s.query(OrganizationUser).filter_by(
                        organization_id=entity.parent_organization_id,
                        user_id=g.current_user.id,
                        is_manager=True
                    ).count()
                )
            else:
                allowed = bool(
                    s.query(OrganizationUser).filter_by(
                        organization_id=entity.id,
                        user_id=g.current_user.id,
                        is_manager=True
                    ).count()
                )

        if not allowed:
            raise Forbidden

        return f(entity, *args, **kwargs)

    return inner


def is_organization_manager_by_id(f):
    @wraps(f)
    def inner(organization_id, *args, **kwargs):
        # check directly for this organization
        allowed = check_is_manager(organization_id, user_id=g.current_user.id)

        if not allowed:
            raise Forbidden

        return f(organization_id, *args, **kwargs)

    return inner


def is_organization_member(f):
    """ Authorization Requirement: Current `User` must be a member of the `Organization` this endpoint represents.
    """

    @wraps(f)
    def inner(entity, *args, **kwargs):

        user = getattr(g, 'current_user')
        if not user:
            raise Unauthorized

        s = db.session()
        s.add(user)

        allowed = False
        current_role = [role.id for role in user.roles]

        if RoleType.internal_admin_eperpus.value in current_role or RoleType.super_admin.value in current_role:
            allowed = True
        else:
            # different than is_manager check.. this only directly checks a single organization.
            allowed = bool(
                s.query(OrganizationUser).filter_by(
                    organization=entity,
                    user=user
                ).count()
            )

        if not allowed:
            raise Forbidden(
                developer_message='User {user.username} is not a member of {org.name} ({org.id})'.format(
                    user=user,
                    org=entity
                )
            )

        return f(entity, *args, **kwargs)

    return inner


def only_child_organizations(f):
    """ Business rule:  An endpoint that is only valid for a 'child' `Organization`

    eg, an Organization with a non-null parent_organization attribute.
    """

    @wraps(f)
    def inner(entity, *args, **kwargs):
        is_child = entity.parent_organization is not None
        if not is_child:
            raise MethodNotAllowed(developer_message='This endpoint is only available for child organizations.')
        return f(entity, *args, **kwargs)

    return inner


def not_child_organization(f):
    """ Business rule:  An endpoint that is only valid for a 'parent' `Organization`

    eg, an Organization with a **NULL** parent_organization attribute.
    """

    @wraps(f)
    def inner(entity, *args, **kwargs):
        is_child = entity.parent_organization is not None
        if is_child:
            raise MethodNotAllowed(developer_message='This endpoint is only available for child organizations.')
        return f(entity, *args, **kwargs)

    return inner


class OrganizationType(Enum):
    vendor = 'vendor'
    partner = 'partner'
    payment_gateway = 'payment gateway'
    shared_library = 'shared library'


class OrganizationStatus(Enum):
    new = 'new'
    waiting_for_review = 'waiting for review'
    in_review = 'in review'
    rejected = 'rejected'
    approved = 'approved'
    clear = 'clear'
    banned = 'banned'


@blueprint.route('')
@user_is_authenticated
def search():
    """ get List of organizations
        v1/organizations
    :return:
    """
    session = db.session()
    query = session.query(Organization)
    eperpus_id = request.args.get('eperpus-id', None)
    if eperpus_id:
        # show all organizations bellow this eperpus id (including this eperpus id)
        elibrary = session.query(Organization).get(eperpus_id)
        elibrary_ids = get_org_and_child_org_ids(elibrary) if elibrary else []
        if elibrary_ids:
            query = query.filter(Organization.id.in_(elibrary_ids))

    include_inactive = request.args.get('include-inactive', None)
    default_filters = []
    if not include_inactive or include_inactive.lower() in ['false', '0']:
        # default, show only active item
        # for cms portal - group/organization maintenance: show all item (include inactive)
        default_filters = [Organization.is_active == True, ]

    filter_exps = OrganizationListArgsParser().parse_args(request)['filters']

    def_filters = [defexp for defexp in default_filters
                   if defexp.left.name
                   not in [reqexp.left.name for reqexp in filter_exps]]

    filter_exps.extend(def_filters)

    for f in filter_exps:
        query = query.filter(f)

    query = query.order_by(desc(Organization.sort_priority), desc(Organization.id))

    limit = request.args.get('limit', default=20, type=int)
    offset = request.args.get('offset', default=0, type=int)

    total_row = query.count()
    query = query.offset(offset).limit(limit)

    organizations = query.all()
    response = jsonify({
        'organizations': [
            __serialize_org(org) for org in organizations
        ],
        'metadata': {
            'resultset': {
                'limit': request.args.get('limit', default=20, type=int),
                'offset': request.args.get('offset', default=0, type=int),
                'count': total_row
            }
        }
    })

    weak_etag = generate_weak_etag(organizations)
    response.set_etag(weak_etag, weak=True)

    return response.make_conditional(request)


def organization_mailing(data_request):
    mailing_address = data_request.get("mailing_address", None)
    del data_request['mailing_address']

    mailing_city = data_request.get("mailing_city", None)
    del data_request['mailing_city']

    mailing_province = data_request.get("mailing_province", None)
    del data_request['mailing_province']

    mailing_postal_code = data_request.get("mailing_postal_code", None)
    del data_request['mailing_postal_code']

    return data_request, mailing_address, mailing_city, mailing_province, mailing_postal_code


@blueprint.route('', methods=['POST', ])
def create():
    session = db.session()
    request_data = schema_load(schema=OrganizationDefaultSchema())
    new_org_name = request_data.get('name')
    new_org_slug = create_slug(new_org_name, Organization, session)

    exist_organization = Organization.query.filter_by(name=new_org_name).first()
    if exist_organization:
        return make_response(json.dumps(
            {
                "status": 409,
                "developer_message": "{} already exist".format(new_org_name),
                "error_code": 409101,
                "user_message": "{} already exist".format(new_org_name)}
        ), httplib.CONFLICT, {'Content-type': 'application/json'})
    else:
        request_data['slug'] = new_org_slug

        exist_organization = Organization(**request_data)
        db.session.add(exist_organization)
        db.session.commit()

    return make_response(jsonify(__serialize_org(exist_organization)), httplib.CREATED)


@blueprint.route('/<int:org_id>', methods=['PUT', ])
def update(org_id):
    session = db.session()

    exist_organization = Organization.query.filter_by(id=org_id).first()
    if not exist_organization:
        return make_response("", httplib.NOT_FOUND)

    request_data = schema_load(schema=OrganizationDefaultSchema())
    new_org_name = request_data.get('name', None)

    contact_email = request_data.get("contact_email", None)
    wallet = request_data.pop("wallet", None)

    logo_org = request_data.pop("logo_organization", None)

    if logo_org:
        url = upload_logo_to_s3(logo_org, exist_organization.id)
        exist_organization.logo_url = url
        request_data.update({'logo_url': url})

    # Save wallet data
    if wallet is not None:
        orgs_wallet = exist_organization.wallet
        orgs_wallet.balance = wallet
        db.session.add(orgs_wallet)
        db.session.commit()

    if contact_email:
        if not validate_email(contact_email):
            msg = "Contact email, Invalid email format."
            raise BadRequest(user_message=msg, developer_message=msg)

    request_data, mailing_address, mailing_city, mailing_province, mailing_postal_code = organization_mailing(
        request_data)

    if exist_organization.mailing_address_id:
        mailing = exist_organization.mailing_address
        mailing.street1 = mailing_address
        mailing.city = mailing_city
        mailing.province = mailing_province
        mailing.postal_code = mailing_postal_code
        db.session.add(mailing)
        db.session.commit()
    else:
        new_address = Address(
            street1=mailing_address,
            city=mailing_city,
            province=mailing_province,
            postal_code=mailing_postal_code)
        db.session.add(new_address)
        db.session.commit()
        request_data['mailing_address_id'] = new_address.id

    if new_org_name:
        new_org_slug = create_slug(new_org_name, Organization, session)
        request_data['slug'] = new_org_slug

        if exist_organization.name != new_org_name:
            another_org = Organization.query.filter_by(name=new_org_name).first()
            if another_org:
                msg = "{} organization already exist!".format(new_org_name)
                raise Conflict(user_message=msg, developer_message=msg)

    kwargs = {}
    for item in request_data:
        if request_data[item] == None:
            pass
        else:
            kwargs[item] = request_data[item]

    for k, v in kwargs.iteritems():
        setattr(exist_organization, k, v)

    db.session.add(exist_organization)
    db.session.commit()

    return make_response(jsonify(__serialize_org(exist_organization)), httplib.OK)


@blueprint.route("/<organization:entity>")
@user_is_authenticated
def details(entity):
    return jsonify(__serialize_org(entity))


@blueprint.route('/<organization:entity>', methods=['DELETE', ])
@is_organization_manager
def delete(entity):
    """ Soft-deletes parent organizations, otherwise hard deletes child organizations.

    :param `Organization` entity:
    """
    s = db.session()
    if entity.parent_organization:
        s.delete(entity)
    else:
        if not is_superuser(g.current_user):
            raise Forbidden

        entity.is_active = False

    return make_response("", NO_CONTENT)


@blueprint.route('/<organization:entity>/child-organizations')
@not_child_organization
def search_children(entity):
    """ Searches organization children and returns a paginated list of child organizations.

    :param `Organization` entity:
    """

    def custom_list_limit(selection, offset=0, limit=None):
        return selection[offset:(limit + offset if limit is not None else None)]

    def format_summary(org):
        json_summary = {
            'id': org.id,
            'name': org.name,
            'href': org.api_url,
            'users': {
                'href': url_for('organization.search_users', entity=org, _external=True),
                'title': 'Organization Members',
                'count': org.users.count()
            },
        }

        if org.party_type == 'shared library':
            from app.eperpus.models import Catalog
            data_catalogs = OrganizationCatalog.query.filter(OrganizationCatalog.organization_id == org.id).all()
            cat_id = [i.catalog_id for i in data_catalogs]
            catalogs = Catalog.query.filter(Catalog.id.in_(cat_id)).all()

            json_summary.update({
                'is_parent_organization': True if org.parent_organization_id in (None, 0) else False,
                'is_locked': getattr(org, 'locked_message', None),
                'catalogs': {
                    'href': url_for('eperpus_catalogs.search_catalogs', entity=org, _external=True),
                    'title': 'EPerpus Catalogs',
                    'count': org.catalogs.count()
                },
                'catalog': [{'id': data.id, 'title': data.name} for data in catalogs]
            })
        return json_summary

    offset, limit = get_offset_limit()
    qword = request.args.get('q', None)

    parent_data = entity
    child_data = entity.child_organizations.filter_by(is_active=True)

    if qword:
        parent_name = parent_data.name.lower()
        if parent_name.find(qword.lower()) >= 0:
            parent_data = [parent_data]
        else:
            parent_data = []
        qword = "{}{}{}".format('%', qword, '%')
        child_data = child_data.filter(Organization.name.ilike(qword))
        data_organizations = parent_data + child_data.all()
    else:
        data_organizations = [parent_data] + child_data.all()

    total_count = len(data_organizations)
    data_organizations = custom_list_limit(data_organizations, offset, limit)

    org_data = []
    for output in data_organizations:
        org_data.append(format_summary(output))

    data_response = jsonify({
        'entities': org_data,
        'metadata': {'resultset': {'offset': offset, 'limit': limit, 'count': total_count}}})
    data_response.status_code = httplib.OK
    return data_response


@blueprint.route('/<organization:entity>/child-organizations', methods=['POST', ])
@is_organization_manager
@not_child_organization
def create_child(entity):
    """ Creates a new `Organization`, that is a child (and of the same type) as the parent.

    :param `Organization` entity:
    """
    session = db.session()

    request_data = schema_load(schema=OrganizationSchema())
    new_org_name = request_data.get('name')

    default_catalog = OrganizationCatalog.query.filter_by(organization_id=entity.id).all()
    new_catalog = request_data.get('catalog', [data.catalog_id for data in default_catalog])

    # this is way too simplistic to be practical.  also, there's no handling for duplicate slugs.
    new_org_slug = create_slug(new_org_name, Organization, session)

    exist_child = Organization.query.filter(Organization.parent_organization_id == entity.id,
                                            Organization.name == new_org_name,
                                            Organization.type == entity.type).first()

    if exist_child and exist_child.is_active is True:
        raise Conflict(user_message=gettext("Group name already exist!"), developer_message="Group name already exist!")

    if exist_child and exist_child.is_active is False:
        exist_child.is_active = True
        child = exist_child
        db.session.add(exist_child)
        db.session.commit()
    else:
        child = entity.__class__(name=new_org_name, status=entity.status, type=entity.type, slug=new_org_slug,
                                 parent_organization=entity)

    from app.eperpus.libraries import SharedLibrary
    if isinstance(child, SharedLibrary):
        child.user_concurrent_borrowing_limit = request_data.get('user_concurrent_borrowing_limit', 5)
        child.reborrowing_cooldown = request_data.get('reborrowing_cooldown', 'P7D'),
        child.borrowing_time_limit = request_data.get('borrowing_time_limit', 'P7D')

    session.add(child)
    session.commit()

    from app.eperpus.models import Catalog
    for new in new_catalog:
        new_catalog = Catalog.query.filter_by(id=new).first()
        if new_catalog:
            catalog_info = OrganizationCatalog(catalog_id=new_catalog.id, organization_id=child.id)
            db.session.add(catalog_info)
            db.session.commit()

    resp = jsonify(__serialize_org(child))
    resp.status_code = CREATED
    resp.headers['Location'] = child.api_url
    return resp


@blueprint.route('/<organization:entity>/child-organizations/<int:child_id>', methods=['PUT', ])
@is_organization_manager
@not_child_organization
def update_child(entity, child_id):
    from app.eperpus.libraries import SharedLibrary
    from app.eperpus.models import Catalog

    session = db.session()
    try:
        child = session.query(Organization).get(child_id)
        if not child:
            raise NotFound()

        request_data = schema_load(schema=OrganizationSchema())

        default_catalog = OrganizationCatalog.query.filter_by(organization_id=entity.id).all()
        new_catalog = request_data.get('catalog', [data.catalog_id for data in default_catalog])
        new_org_name = request_data.get('name')

        if not new_catalog:
            new_catalog = [data.catalog_id for data in default_catalog]
        if child.name != new_org_name:
            exist_child = Organization.query.filter(Organization.parent_organization_id == entity.id,
                                                    Organization.name == new_org_name, Organization.type == entity.type,
                                                    Organization.is_active == True).first()
            if exist_child:
                return Conflict(user_message=gettext("Group name already exist!"),
                                developer_message="Group name already exist!")
            new_slug = create_slug(new_org_name, Organization, session)
            child.slug = new_slug

        if isinstance(child, SharedLibrary):
            child.user_concurrent_borrowing_limit = request_data.get('user_concurrent_borrowing_limit', 5)
            child.reborrowing_cooldown = request_data.get('reborrowing_cooldown', 'P7D'),
            child.borrowing_time_limit = request_data.get('borrowing_time_limit', 'P7D')

        # update child name..
        child.name = new_org_name
        session.add(child)
        session.commit()

        old_catalog = OrganizationCatalog.query.filter(OrganizationCatalog.organization_id == child.id).all()
        for old in old_catalog:
            db.session.delete(old)
            db.session.commit()

        for new in new_catalog:
            new_catalog = Catalog.query.filter_by(id=new).first()
            if new_catalog:
                catalog_info = OrganizationCatalog(
                    catalog_id=new_catalog.id,
                    organization_id=child.id)
                db.session.add(catalog_info)
                db.session.commit()

        resp = jsonify(__serialize_org(child))
        resp.headers['Location'] = child.api_url
        return resp

    except Exception as e:
        return UnprocessableEntity(
            user_message='Update organizations Failed Error : {}'.format(e),
            developer_message='Update organizations Failed Error : {}'.format(e)
        )


@blueprint.route('/<organization:entity>/users')
@is_organization_manager
def search_users(entity):
    """ Returns a paginated list of organization members.

    :param `Organization` entity:
    """
    from app.eperpus.members import get_all_child_orgs

    facet_values = None
    spell_check = None
    session = db.session()

    offset = request.args.get('offset', type=int, default=0)
    limit = request.args.get('limit', type=int, default=20)
    direct_db = request.args.get('direct-db', type=int, default=0)
    if direct_db in [None, 1]:
        # get direct from db
        query_set = session.query(User).filter(User.is_active == True).join(
            OrganizationUser).filter_by(organization=entity,)
        total = query_set.count()
        users = query_set.offset(offset).limit(limit).all()
    else:
        # update this query set for retrieve all user include inside childs.
        users = []
        if entity.parent_organization_id:
            organization_query = entity.id
        else:
            organization_query = '({})'.format(
                ' OR '.join(['{}'.format(child) for child in get_all_child_orgs([entity])]))

        response_json = get_search_results(organization_query, logging.getLogger('scoopcor.users'))
        facet_items = response_json['facet_counts']['facet_fields'].items() if 'facet_counts' in response_json else []

        facet_values = []
        if facet_items:
            facet_values = [
                {"field_name": key,
                 "values": [{"value": t[0], "count": t[1]} for t in zip(facet[0::2],
                                                                        facet[1::2])]} for key, facet in facet_items]

        spell_check = []
        if response_json.get('spellcheck', {'correctlySpelled': True})['correctlySpelled'] == 'false':
            suggestions = response_json['spellcheck']['suggestions'][1]['suggestion']
            spell_check = [{"value": data['word'], "count": data['freq']} for data in suggestions]

        total = response_json['response']['numFound']
        for doc in response_json['response']['docs']:
            user = session.query(User).get(doc.get('id'))
            if user:
                users.append(user)

    return jsonify({
        'users': [{
            'id': u.id,
            'username': u.username,
            'email': u.email,
            'first_name': u.first_name,
            'last_name': u.last_name,
            'is_active': u.is_active,
            'signup_date': isoformat(u.created),
            'organizations': retrieve_org_user(entity,user_id=u.id)[0],
            'organizations_name': '; '.join(retrieve_org_user(entity,user_id=u.id)[1]),
            'note': u.note,
            'level': u.level} for u in users],
        'metadata': {
            'resultset': {
                'offset': offset,
                'limit': limit,
                'count': total
            },
            'facets': facet_values,
            'spelling_suggestions': spell_check,
        }
    })


def retrieve_org_user(entity, user_id):
    """
    this function is to find all organization that is related to user
    :param entity:
    :param user_id:
    :return:
    """
    from app.eperpus.helpers import organization_list_parent
    child_org = organization_list_parent(entity)

    exist_user_org = OrganizationUser.query.filter(
        OrganizationUser.organization_id.in_([org.id for org in child_org]),
        OrganizationUser.user_id==user_id).all()

    obj_org_name, obj_org = [], []
    for org_ in exist_user_org:
        obj_org.append({"id":org_.organization.id, "name": org_.organization.name})
        obj_org_name.append(str(org_.organization.name))
    return obj_org, obj_org_name


@blueprint.route('/<organization:entity>/users', methods=['POST', ])
@is_organization_manager
@not_child_organization
def create_user(entity):
    """ Creates a completely new user within SCOOP and automatically sets them as a member of an `Organization`

    :param `Organization` entity:
    """

    session = db.session
    user_schema = UserOrganizationCreateSchema()
    new_user_data = schema_load(user_schema)
    email = new_user_data.get('email', None)
    username = new_user_data.get('username', None)

    # check if this new user add to total current user, exceed maximum user allowed
    total_users = entity.users.count() + 1
    if entity.parent_organization_id:
        allowed_user = entity.parent_organization.maximum_user_allowed
    else:
        allowed_user = entity.maximum_user_allowed

    if allowed_user and total_users > allowed_user:
        raise Conflict(
            user_message=gettext(
                "You have exceeded the max number of users allowed. Please contact your sales for further information."),
            developer_message="You have exceeded the max number of users allowed. Please contact your sales for further information.")

    # check by username first
    user = None
    user_by_username = session.query(User).filter(User.username == username).first()
    if user_by_username:
        if entity.id in [org.id for org in user_by_username.organizations]:
            raise Conflict(
                user_message=gettext("User with this username/[no induk] already registered in this organization"),
                developer_message="User with this username/[no induk] already registered in this organization")
        elif user_by_username.email != email:
            msg = gettext("User with this username/[no induk] already exists with different email")
            raise Conflict(
                user_message="{msg} {prev_email}".format(msg=msg, prev_email=user_by_username.email),
                developer_message="User with this username/[no induk] already exists with different email")
        elif user_by_username.email == email:
            user = user_by_username

    if not user:
        # get/check by email
        user = session.query(User).filter(User.email == email).first()
        # if user.username != new_username -->> ignore new_username, don't update username

    if user:
        if entity.id in [org.id for org in user.organizations]:
            # user already exists in this organizations
            raise Conflict(
                user_message=gettext("User with this email already registered"),
                developer_message="User with this email already registered")

        # update existing user if applicable (later on, add this user to the organization (via link API endpoint)
        if user.email == user.username:
            # only update username if username == email

            user.username = username

        if not user.first_name:
            user.first_name = new_user_data.get('first_name')
        if not user.last_name:
            user.last_name = new_user_data.get('last_name')
    else:
        user, user_referrer = save_new_user(user_schema, session)

    # add this entity/organization to this user-organizations
    user.organizations.append(entity)
    user_id = user.id
    user.is_verified = True
    session.add(user)
    session.commit()

    try:
        post_user_organization_data_to_solr(user_id)
    except Exception as ex:
        pass

    return jsonify(user_schema.dump(user).data), CREATED


@blueprint.route('/<organization:entity>/users/<int:user_id>', methods=['PUT', ])
@is_organization_manager
@not_child_organization
def update_organization_user(entity, user_id):
    """ Update an existing member/user of an organization

    :param `Organization` entity:
    :param `int` user_id:
    :return:
    """
    session = db.session
    user = session.query(User).get(user_id)
    if not user:
        raise NotFound

    user_schema = UserOrganizationUpdateSchema()
    # load request body and validate base on user schema
    json_data = json.loads(request.data)
    if not json_data.get('profile', 'empty'):
        json_data.pop('profile', None)

    new_user_data = schema_load(user_schema, json_data=json_data)

    # validate new user data
    if user.email != new_user_data.get('email') and user.is_verified:
        raise Conflict(
            user_message=gettext("User's email already verified, email changes not allowed"),
            developer_message="User's email already verified, email changes not allowed")
    if user.username != new_user_data.get('username') or user.email != new_user_data.get('email'):
        # if user change username or email, make sure new email/username not yet exists
        assert_username_and_email(
            session, username=new_user_data.get('username'),
            email=new_user_data.get('email'), user_id=user.id)

    # TODO: user become inactive/remove from a shared library? how to add back?
    # if not new_user_data.is_active and user.is_active:

    # temporary fix for bug "can't compare offset-naive and offset-aware datetimes"
    #  root cause: cas_profile.birthdate is not datetime with timezone,
    #   when update with "1970-01-01T00:00:00+00:00" --> error raised in live server
    # since CMS portal didn't have Profile update anyway, remove profile update for now until we found better solution.
    json_data.pop("profile", None)

    update_user(user, user_schema, session, json_data)

    return jsonify(user_schema.dump(user).data)


@blueprint.route('/<organization:entity>/users/<user:user>')
@is_organization_manager
def user_details(entity, user):
    from .users import details
    from app.eperpus.members import get_all_child_orgs

    # show only organization+child orgs belong to this eperpus (in user.organizations)
    all_child_orgs = get_all_child_orgs([entity, ])

    if user:
        # this case only happen for BTPN, assign only to child but parent are not assign.
        user_org = []
        for org in user.organizations:
            user_org.append(org.id)
            user_org.append(org.parent_organization_id)

        if entity.id not in list(set(user_org)):
            raise NotFound()

    from app.eperpus.members import get_all_child_orgs
    # show only organization+child orgs belong to this eperpus (in user.organizations)
    g.allowed_org_ids = all_child_orgs
    try:
        response = details(user)
        g.allowed_org_ids = None
        return response
    except Exception as e:
        g.allowed_org_ids = None
        raise e


@blueprint.route('/<organization:entity>/users', methods=['LINK', ])
@is_organization_manager
def add_user_to_child(entity):
    """ Adds an existing SCOOP user to an organization.

    :param `Organization` entity:
    """
    s = db.session()
    user_id = request.json.get('id', None)
    user_to_add = s.query(User).get(user_id)
    if user_to_add is None:
        raise NotFound(
            user_message=gettext('User does not exist'),
            developer_message='User {} does not exist'.format(user_id)
        )

    if entity.parent_organization:
        if not s.query(OrganizationUser).filter_by(organization=entity.parent_organization,
                                                   user=user_to_add).count():
            raise Conflict(
                user_message=gettext('Cannot add User'),
                developer_message='Cannot add user to child org;  they are not a member of the parent org.'
            )

    if s.query(OrganizationUser).filter_by(organization=entity, user=user_to_add).count():
        resp = {"db_status": "User already exists"}
        return jsonify(resp)

    s.add(
        OrganizationUser(
            organization=entity,
            user=user_to_add,
            is_manager=request.json.get('is_manager', False)
        )
    )
    # change modified for solr update
    user_to_add.modified = get_utc_nieve()
    s.add(user_to_add)
    s.commit()

    resp = {"db_status": "User added"}
    try:
        resp["solr_response"] = post_user_organization_data_to_solr(user_id)
    except Exception as ex:
        resp["solr_response"] = {
            "status": "error",
            "error_message": "{} - {}".format(str(ex), ex.message)
        }
    return jsonify(resp)


class SolrSchema(ma.Schema):
    user_id = fields.Int()


@blueprint.route('/users/update-solr', methods=['POST', ])
def update_user_organization_solr():
    """ Delta import organization users data to SOLR

    /v1/organizations/users/update-solr

    :return:
    """
    data, errors = SolrSchema().load(request.json)
    # user_id = data.get('user_id', None) if data else None
    response = post_user_organization_data_to_solr(user_id=None)
    return jsonify(response)


def post_user_organization_data_to_solr(user_id=None):
    solr_url = '{}/scoop-users'.format(app.config['SOLR_BASE_URL'])
    if user_id:
        sql = "SELECT row_to_json(users_solr_dih) FROM utility.users_solr_dih where id = {}".format(user_id)
        user_data = db.engine.execute(sql).scalar()
        if user_data:
            user_data.pop('modified', None)
            signup_date = user_data.get('signup_date', None)
            if signup_date and 'Z' not in signup_date and '+' not in signup_date:
                user_data['signup_date'] = signup_date + 'Z'
            headers = {
                'Content-Type': 'application/json'
            }
            url = '{}/update/json/docs?commit=true'.format(solr_url)
            # update user data to solr
            response = requests.post(url, data=json.dumps(user_data), headers=headers)
    else:
        # do delta import
        url = '{}/dataimport'.format(solr_url)
        response = requests.post(url, data={'command': 'delta-import', 'wt': 'json'})

    return {
        "status_code": response.status_code,
        "text": response.json()
    }


@blueprint.route('/<organization:entity>/users', methods=['UNLINK', ])
@is_organization_manager
def remove_user_from_child(entity):
    """ Removes a user from an organization's membership, without removing their account from SCOOP.

    :param `Organization` entity:
    """
    s = db.session()
    user_id = request.json.get('id', None)
    if not user_id:
        raise UnprocessableEntity(
            user_message=gettext('Please define user id!'),
            developer_message='Id is empty'
        )

    org_user = s.query(OrganizationUser).filter_by(organization=entity, user_id=user_id).first()
    if not org_user:
        raise NotFound
    s.delete(org_user)

    for child_org in entity.child_organizations:
        child_org_user = s.query(OrganizationUser).filter_by(organization=child_org, user_id=user_id).first()
        if child_org_user:
            s.delete(child_org_user)

    # change modified for solr update
    user = org_user.user
    user.modified = get_utc_nieve()
    s.add(user)
    s.commit()

    # remove from watch list if user is remove from main/parent eperpus
    if entity.parent_organization_id is None:
        db.engine.execute(
            'UPDATE watch_list set end_date = now() '
            'where user_id = {user_id} and stock_org_id = {stock_org_id} and end_date is null'.format(
                user_id=user_id,
                stock_org_id=entity.id,
            ))

    resp = {"db_status": "User removed"}
    try:
        resp["solr_response"] = post_user_organization_data_to_solr(user_id)
    except Exception as ex:
        resp["solr_response"] = {
            "status": "error",
            "error_message": "{} - {}".format(str(ex), ex.message)
        }

    return jsonify(resp)


def __serialize_org(org):
    org_json = {
        'id': org.id,
        'name': org.name,
        'type': org.type,
        'is_active': org.is_active,
        'href': org.api_url,
        'accounting_id': org.accounting_id,
        'legal_name': org.legal_name,
        'status': org.status,
        'logo_url': org.logo_url,
        'username_prefix': org.username_prefix,
        'maximum_user_allowed': org.maximum_user_allowed,
        'total_user': org.users.count(),
        'parental_level': org.parental_level,
        'contact_email': org.contact_email,
        'phone_primary': org.phone_primary,
        'phone_alternate': org.phone_alternate,
        'mailing_address': org.mailing_address.street1 if org.mailing_address else None,
        'mailing_city': org.mailing_address.city if org.mailing_address else None,
        'mailing_province': org.mailing_address.province if org.mailing_address else None,
        'mailing_postal_code': org.mailing_address.postal_code if org.mailing_address else None,
        'pic_client': org.pic_client,
        'app_name': org.app_name,
        'pic_marketing': {
            'id': org.marketing_pic.id,
            'email': org.marketing_pic.email,
            'phone_number': org.marketing_pic.phone_number
        } if org.marketing_pic else None,
        'is_parent_organization': False if org.parent_organization_id else True
    }

    if org.parent_organization is None:
        org_json.update({
            'organizations': {
                'href': url_for('organization.search_children', entity=org, _external=True),
                'title': 'Child Organizations',
                'count': org.child_organizations.count()
            }
        })

    from app.eperpus.libraries import SharedLibrary
    if isinstance(org, SharedLibrary):
        org_json['catalogs'] = {
            'href': url_for('eperpus_catalogs.search_catalogs', entity=org, _external=True),
            'title': 'EPerpus Catalogs',
            'count': org.catalogs.count()
        }

    from app.eperpus.models import Catalog
    data_catalogs = OrganizationCatalog.query.filter(OrganizationCatalog.organization_id == org.id).all()
    cat_id = [i.catalog_id for i in data_catalogs]
    catalogs = Catalog.query.filter(Catalog.id.in_(cat_id)).all()
    org_json.update({"catalog": [{'id': data.id, 'title': data.name} for data in catalogs]})

    return org_json


users_organizations = db.Table(
    'cas_users_organizations',
    db.Column('user_id', db.Integer, db.ForeignKey('cas_users.id'), primary_key=True),
    db.Column('organization_id', db.Integer, db.ForeignKey('cas_organizations.id'), primary_key=True),
    db.Column('is_manager', db.Boolean, nullable=False, default=False)
)

organizations_clients = db.Table(
    'cas_organizations_clients',
    db.Column('organization_id', db.Integer, db.ForeignKey('cas_organizations.id'), primary_key=True),
    db.Column('client_id', db.Integer, db.ForeignKey('cas_clients.id'), primary_key=True)
)


class OrganizationCatalog(db.Model, TimestampMixin):
    __tablename__ = 'cas_organizations_catalogs'

    id = db.Column(db.Integer, primary_key=True)
    catalog_id = db.Column(db.Integer, db.ForeignKey('core_catalogs.id'))
    organization_id = db.Column(db.Integer, db.ForeignKey('cas_organizations.id'))


class Organization(Party):
    """ A type of partner company.
    """
    __tablename__ = 'cas_organizations'
    id = db.Column(db.Integer, db.ForeignKey('parties.id'), primary_key=True)
    name = db.Column(db.Text, nullable=True)
    app_name = db.Column(db.String(250))
    legal_name = db.Column(db.String(100))
    slug = db.Column(db.Text, nullable=False, unique=True)

    status = pg_enum_column(OrganizationStatus, nullable=False)
    type = pg_enum_column(OrganizationType, nullable=True)

    phone_primary = db.Column(db.String(40))
    phone_alternate = db.Column(db.String(40))
    contact_email = db.Column(db.String(254))
    accounting_id = db.Column(db.String(40), unique=True)

    # This used for ePerpus, username BPK123, BPK124, BPK125
    username_prefix = db.Column(db.String(10))
    parental_level = db.Column(db.Integer)

    marketing_pic_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'))
    marketing_pic = db.relationship(User, foreign_keys=marketing_pic_id)

    pic_client = db.Column(db.String(100))

    users = db.relationship(
        User,
        secondary=users_organizations,
        backref=db.backref('organizations', lazy='dynamic'),
        uselist=True,
        lazy='dynamic'
    )

    clients = db.relationship(
        Client,
        secondary=organizations_clients,
        backref=db.backref('organizations', lazy='dynamic'),
        uselist=True,
        lazy='dynamic'
    )

    parent_organization_id = db.Column(db.Integer, db.ForeignKey('cas_organizations.id'))
    parent_organization = db.relationship(
        'Organization',
        backref=db.backref('child_organizations', lazy='dynamic'),
        remote_side=id,
        foreign_keys=[parent_organization_id]
    )

    mailing_address_id = db.Column(db.Integer, db.ForeignKey('cas_addresses.id'))
    mailing_address = db.relationship(Address, foreign_keys=mailing_address_id)

    billing_address_id = db.Column(db.Integer, db.ForeignKey('cas_addresses.id'))
    billing_address = db.relationship(Address, foreign_keys=billing_address_id)

    logo_url = db.Column(db.String(255))

    sort_priority = db.Column(db.SmallInteger, nullable=False, default=1)
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    modified = db.Column(db.DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    maximum_user_allowed = db.Column(db.Integer, default=0, nullable=True)

    __mapper_args__ = {
        'polymorphic_identity': 'organization'
    }

    @property
    def api_url(self):
        return url_for('organization.details', entity=self, _external=True)


class BankAccount(models.TimestampMixin, db.Model):
    """ A single bank account owned by an Organization.
    """
    __tablename__ = 'cas_bankaccounts'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150))

    swift_code = db.Column(db.String(150))
    branch = db.Column(db.String(250))
    account_number = db.Column(db.String(100))
    account_name = db.Column(db.String(250))
    notes = db.Column(db.Text, nullable=False)  # nullable = false??  seems weird.

    organization_id = db.Column(db.Integer, db.ForeignKey('cas_organizations.id'), nullable=False)
    organization = db.relationship(Organization, backref=db.backref('bank_accounts', lazy='dynamic'))


class OrganizationUser(db.Model):
    __table__ = users_organizations
    organization = db.relationship(Organization)
    user = db.relationship(User)


class OrganizationSchema(ma.Schema):
    id = fields.Integer(dump_only=True)
    name = fields.String(required=True, allow_none=False)
    href = fields.String(attribute='api_url', dump_only=True)
    parent_organization = fields.Nested(ImplicitRelHref(title_attribute='name'), attribute='parent_organization')
    user_concurrent_borrowing_limit = fields.Int()
    reborrowing_cooldown = fields.Str()
    borrowing_time_limit = fields.Str()
    catalog = fields.List(fields.Integer, required=False)


class OrganizationDefaultSchema(ma.Schema):
    id = fields.Integer(dump_only=True, required=False)
    name = fields.String(allow_none=False, required=False)
    type = fields.String(required=False)
    is_active = fields.Boolean(default=True)
    accounting_id = fields.String(required=False)
    legal_name = fields.String(required=False)
    status = fields.String(required=False)
    logo_url = fields.String(required=False)
    username_prefix = fields.String(required=False)
    maximum_user_allowed = fields.Integer(required=False)
    parental_level = fields.Integer(required=False)

    pic_client = fields.String(required=False)
    contact_email = fields.String(required=False)
    phone_primary = fields.String(required=False)
    phone_alternate = fields.String(required=False)
    mailing_address = fields.String(required=False)
    mailing_city = fields.String(required=False)
    mailing_province = fields.String(required=False)
    mailing_postal_code = fields.String(required=False)
    marketing_pic_id = fields.Integer(required=False)
    app_name = fields.String(required=False)
    wallet = fields.Integer(required=False)
    logo_organization = fields.String(required=False)


class OrganizationUsersArgsParser(parsers.SqlAlchemyFilterParser):
    __model__ = OrganizationUser
    user_id = parsers.IntegerFilter()
    email = parsers.StringFilter()
    note = parsers.StringFilter()
    first_name = parsers.StringFilter()
    last_name = parsers.StringFilter()


def get_org_and_child_org_ids(current_org):
    """ Given an organization, gets all of it's child organizations.

    :param current_org:
    :return:
    """
    list_of_org_ids = [current_org.id, ]
    for child_org in current_org.child_organizations:
        list_of_org_ids.append(child_org.id)
        for child_org_level_2 in child_org.child_organizations:
            list_of_org_ids.append(child_org_level_2.id)
    return list_of_org_ids


class OrganizationListArgsParser(parsers.SqlAlchemyFilterParser):
    q = WildcardFilter(dest='name')
    __model__ = Organization


class OrganizationCart(db.Model, TimestampMixin):
    __tablename__ = 'core_organization_cart'

    id = db.Column(db.Integer, primary_key=True)
    is_active = db.Column(db.Boolean(), default=True)

    organization_id = db.Column(db.Integer, db.ForeignKey("cas_organizations.id"), nullable=False)
    organization = db.relationship(Organization, backref='organization_cart')

    def values(self):
        return {
            'id': self.id,
            'modified': self.modified.isoformat(),
            'cart_offers': [
                cart_offer.values() for cart_offer in self.organization_cart_offer
            ]
        }


class OrganizationCartOffer(db.Model, TimestampMixin):
    __tablename__ = 'core_organization_cart_offer'

    id = db.Column(db.Integer, primary_key=True)
    quantity = db.Column(db.SmallInteger(), nullable=False)

    cart_id = db.Column(db.Integer, db.ForeignKey("core_organization_cart.id"), nullable=False)
    cart = db.relationship(OrganizationCart, backref='organization_cart_offer')

    offer_id = db.Column(db.Integer, db.ForeignKey("core_offers.id"), nullable=False)
    offer = db.relationship(Offer, backref='cart_offer')

    @property
    def api_url(self):
        try:
            return url_for('organization.update_organization_cart', entity=self.cart.organization, cart_offer=self,
                           _external=True)
        except RuntimeError:
            return None

    def values(self):
        from app.items.viewmodels import price_offer_build_response
        from app.items.previews import preview_s3_covers
        item = self.offer.items.first()
        return {
            'id': self.id,
            'quantity': self.quantity,
            'offer': {
                'id': self.offer.id,
                'href': self.offer.api_url,
                'title': self.offer.name,
                'type': self.offer.offer_type.slug,
                'price': price_offer_build_response(self.offer)
            },
            'title': item.name if item else '-',
            'vendor': item.brand.vendor.name if item else '-',
            'author': item.brand.name if item else '-',
            'cover_image': preview_s3_covers(item, 'image_highres', True) if item else '-',
            'price': round(float(self.offer.price_idr)),
            'href': self.api_url
        }

def get_org_ids_by_user_agent(user_agent):
    exist_organization = None
    exist_client = Client.query.filter_by(app_name=user_agent).first()
    if exist_client:
        exist_organization = Organization.query.filter(
            Organization.clients.any(id=exist_client.id), Organization.parent_organization_id == None).first()

    return exist_organization


@blueprint.route('/check-registration-info', methods=['GET', ])
@user_has_any_tokens('can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext')
def get_organization_registration_info():

    user_agent = request.headers.get('User-Agent').split('/')[0].lower().strip().replace(" ", "_")
    language = request.headers.get('Accept-Language', 'en').lower()

    if language != ("id" or "en"): language = "en"

    exist_organization = get_org_ids_by_user_agent(user_agent)
    if not exist_organization:
        error_message = "Organization has not found!, invalid User Agent {} not exist".format(user_agent)
        return BadRequest(user_message=error_message, developer_message=error_message)

    if exist_organization.users.count() > exist_organization.maximum_user_allowed:
        if language == 'en':
            error_message = "The registration is closed, please contact {}".format(exist_organization.contact_email)
        else:
            error_message = "Pendaftaran sedang tutup, silakan hubungi {}.".format(exist_organization.contact_email)

        return BadRequest(user_message=error_message, developer_message=error_message)
    else:
        response = jsonify()
        response.status_code = httplib.OK
        return response


def retrieve_orgs_by_client(client_id):
    from app.eperpus.members import get_all_child_orgs

    exist_organization = Organization.query.filter(Organization.clients.any(id=client_id)).all()
    organization_ids = []
    for data_org in exist_organization:
        all_child = get_all_child_orgs([data_org])
        organization_ids = organization_ids + all_child
    return list(set(organization_ids))
