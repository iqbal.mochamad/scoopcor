from __future__ import unicode_literals, absolute_import
import os
import json

import iso3166
from faker import Factory
import jsonschema
from nose.tools import istest

from app import db, app
from app.utils.shims import countries, item_types
from tests.fixtures.sqlalchemy.master import CategoryFactory
from tests import testcases


_this_dir = os.path.abspath(os.path.dirname(__file__))
_base_dir = os.path.join(_this_dir, os.pardir, os.pardir, os.pardir, os.pardir)


@istest
class CreateTest(testcases.CreateTestCase):

    request_url = '/v1/categories'
    fixture_factory = CategoryFactory
    maxDiff = None

    @classmethod
    def get_api_request_body(cls):
        return {
            'name': _fake.name(),
            'description': _fake.bs(),
            'slug': _fake.slug(),
            'meta': _fake.bs(),
            'parent_category_id': None,
            'icon_image_normal': _fake.url(),
            'icon_image_highres': _fake.url(),
            'sort_priority': _fake.pyint(),
            'item_type_id': 1,
            'countries': [1, ],
            'is_active': _fake.pybool(),
        }

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = self.request_body
        expected['countries'] = [
                {
                    'name': iso3166.countries.get(country_code).name,
                    'id': countries.iso3166_to_id(country_code),
                }
                for country_code in ['id', ]]

        self.assertDictContainsSubset(
            expected=expected,
            actual=actual)


@istest
class UpdateTest(testcases.UpdateTestCase):

    request_url = '/v1/categories/{0.id}'
    fixture_factory = CategoryFactory

    @classmethod
    def get_api_request_body(cls):
        return {
            'name': _fake.name(),
            'description': _fake.bs(),
            'slug': _fake.slug(),
            'meta': _fake.bs(),
            'parent_category_id': None,
            'icon_image_normal': _fake.url(),
            'icon_image_highres': _fake.url(),
            'sort_priority': _fake.pyint(),
            'item_type_id': 1,
            'countries': [1, ],
            'is_active': _fake.pybool(),
        }

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = self.request_body
        expected['countries'] = [
                {
                    'name': iso3166.countries.get(country_code).name,
                    'id': countries.iso3166_to_id(country_code),
                }
                for country_code in ['id', ]]

        self.assertDictContainsSubset(
            expected=expected,
            actual=actual)


@istest
class DeleteTest(testcases.SoftDeleteTestCase):

    request_url = '/v1/categories/{0.id}'
    fixture_factory = CategoryFactory


@istest
class GetDetailTest(testcases.DetailTestCase):

    request_url = '/v1/categories/{0.id}'
    fixture_factory = CategoryFactory
    maxDiff = None

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = {
            'id': self.fixture.id,
            'name': self.fixture.name,
            'description': self.fixture.description,
            'slug': self.fixture.slug,
            'meta': self.fixture.meta,
            'parent_category_id': None,
            'icon_image_normal': self.fixture.icon_image_normal,
            'icon_image_highres': self.fixture.icon_image_highres,
            'sort_priority': self.fixture.sort_priority,
            'item_type_id': item_types.enum_to_id(self.fixture.item_type),
            'countries': self.fixture.countries,
            'is_active': self.fixture.is_active,
            'media_base_url': 'https://images.apps-foundry.com/magazine_static/'
            # unicode(app.config['MEDIA_BASE_URL']),
        }
        expected['countries'] = [
                {
                    'name': iso3166.countries.get(country_code).name,
                    'id': countries.iso3166_to_id(country_code),
                 }
                for country_code in expected['countries']]

        self.assertDictEqual(
            actual,
            expected
        )

    def test_response_schema(self):

        with open(os.path.join(_base_dir, 'app', 'items', 'schemas', 'v2', 'category.details.json')) as fh:
            schema = json.load(fh)

        response_json = json.loads(self.response.data)
        jsonschema.validate(response_json, schema)


@istest
class ListTest(testcases.ListTestCase):

    request_url = '/v1/categories'
    list_key = 'categories'
    fixture_factory = CategoryFactory

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        expected = {
            'id': fixture.id,
            'name': fixture.name,
            'description': fixture.description,
            'slug': fixture.slug,
            'meta': fixture.meta,
            'parent_category_id': None,
            'icon_image_normal': fixture.icon_image_normal,
            'icon_image_highres': fixture.icon_image_highres,
            'sort_priority': fixture.sort_priority,
            'item_type_id': item_types.enum_to_id(fixture.item_type),
            'countries': fixture.countries,
            'is_active': fixture.is_active,
            'media_base_url': 'https://images.apps-foundry.com/magazine_static/'
            # app.config['MEDIA_BASE_URL'],
        }
        expected['countries'] = [
                {
                    'name': iso3166.countries.get(country_code).name,
                    'id': countries.iso3166_to_id(country_code),
                 }
                for country_code in expected['countries']]

        self.assertDictEqual(fixture_json, expected)

    def test_response_schema(self):
        with open(os.path.join(_base_dir, 'app', 'items', 'schemas', 'v2', 'category.list.json')) as fh:
            schema = json.load(fh)

        response_json = json.loads(self.response.data)
        jsonschema.validate(response_json, schema)


_fake = Factory.create()


def setup_module():
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
