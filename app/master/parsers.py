from flask_appsfoundry import parsers
from flask_appsfoundry.parsers import (
    ParserBase, StringField, DateTimeField, IntegerField, InputField, EnumField,
    BooleanField, SqlAlchemyFilterParser)
from flask_appsfoundry.parsers.converters import naive_datetime_from_iso8601
from flask_appsfoundry.parsers.filterinputs import (
    IntegerFilter, StringFilter, DateTimeFilter, BooleanFilter, EnumFilter)

from app.items.choices import ITEM_TYPES
from app.master import choices
from app.master.models import VendorRevenueShare
from app.utils.shims import countries, languages
from app.utils.shims.parsers import WildcardFilter
from . import models


def fields_column(input):
    return input.split(',')


class BrandListArgsParser(parsers.SqlAlchemyFilterParser):
    __model__ = models.Brand
    id = IntegerFilter()
    name = StringFilter()
    slug = StringFilter()
    sort_priority = IntegerFilter()
    is_active = BooleanFilter()
    q = WildcardFilter(dest='name')
    description = StringFilter()
    meta = StringFilter()
    icon_image_normal = StringFilter()
    icon_image_highres = StringFilter()
    vendor_id = IntegerFilter()
    daily_release_quota = DateTimeFilter()
    modified = DateTimeFilter()
    default_item_type = EnumFilter(enum=ITEM_TYPES)
    default_parentalcontrol_id = IntegerFilter()
    primary_category_id = IntegerFilter()
    brand_code = StringFilter()
    default_item_schedule = StringFilter()


class CategoryListArgsParser(parsers.SqlAlchemyFilterParser):
    __model__ = models.Category
    id = IntegerFilter()
    name = StringFilter()
    slug = StringFilter()
    sort_priority = IntegerFilter()
    item_type_id = EnumFilter(dest='item_type', enum=ITEM_TYPES)
    is_active = BooleanFilter()


class CategoryArgsParser(ParserBase):
    name = StringField(required=True)
    slug = StringField(required=True)
    sort_priority = IntegerField(default=0)
    is_active = BooleanField(default=True)
    item_type_id = EnumField(ITEM_TYPES, dest='item_type', required=True)

    description = StringField()
    meta = StringField()
    icon_image_normal = StringField()
    icon_image_highres = StringField()
    parent_category_id = IntegerField(default=None)

    countries = InputField(
        type=lambda e: countries.id_to_iso3166(int(e)),
        action='append',
        default=[])


class VendorListArgsParser(parsers.SqlAlchemyFilterParser):
    __model__ = models.Vendor
    id = IntegerFilter()
    name = StringFilter()
    description = StringFilter()
    slug = StringFilter()
    sort_priority = IntegerFilter()
    is_active = BooleanFilter()
    organization_id = IntegerFilter()
    vendor_status = IntegerFilter()
    meta = StringFilter()
    icon_image_normal = StringFilter()
    icon_image_highres = StringFilter()
    accounting_identifier = StringFilter()
    iso4217_code = StringFilter()
    q = WildcardFilter(dest='name')
    modified = DateTimeFilter()


class BrandArgsParser(ParserBase):
    id = IntegerField()
    name = StringField(required=True)
    slug = StringField(required=True)
    sort_priority = IntegerField(required=True)
    is_active = BooleanField(required=True)

    description = StringField()
    meta = StringField()
    icon_image_normal = StringField()
    icon_image_highres = StringField()
    vendor_id = IntegerField(required=True)
    daily_release_quota = IntegerField()
    brand_code = StringField()

    links = InputField(type=lambda e: e, action='append', default=[])
    default_categories = IntegerField(action='append', default=[])
    default_languages = InputField(
        type=lambda e: languages.id_to_iso639_alpha3(int(e)),
        action='append',
        default=[])
    default_countries = InputField(
        type=lambda e: countries.id_to_iso3166(int(e)),
        action='append',
        default=[])
    default_authors = IntegerField(action='append', default=[])
    default_distribution_groups = IntegerField(action='append', default=[])

    default_item_type_id = EnumField(ITEM_TYPES, dest="default_item_type")
    release_period = EnumField(choices.RELEASE_PERIOD)
    default_parental_control_id = IntegerField(
        dest="default_parentalcontrol_id")
    default_item_schedule = StringField()
    buffet_offers = IntegerField(action='append', default=[])


class VendorRevenueArgsParser(ParserBase):
    vendor_id = IntegerField(required=True)
    revenue_id = IntegerField(required=True)
    valid_to = DateTimeField(required=True, type=naive_datetime_from_iso8601)
    valid_from = DateTimeField(required=True, type=naive_datetime_from_iso8601)
    is_active = BooleanField()


class VendorRevenueListArgsParser(SqlAlchemyFilterParser):
    __model__ = VendorRevenueShare
    id = IntegerFilter()
    vendor_id = IntegerFilter()
    revenue_id = IntegerFilter()
    valid_to = DateTimeFilter()
    valid_from = DateTimeFilter()
    is_active = BooleanFilter()


class ReportReviewArgsParser(ParserBase):
    is_active = BooleanField(required=True)
