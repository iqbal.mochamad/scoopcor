from enum import IntEnum

__all__ = ('PlatformType',)


class PlatformType(IntEnum):
    ios = 1
    android = 2
    windows_phone = 3
    web = 4


IOS = 1
ANDROID = 2
WINDOWS_PHONE = 3
GETSCOOP = 4

PLATFORM_TYPES = {
    IOS: 'ios',
    ANDROID: 'android',
    WINDOWS_PHONE: 'windows phone',
    GETSCOOP: 'getscoop'
}

DAILY = 'daily'
WEEKLY = 'weekly'
BIWEEKLY = 'biweekly'
MONTHLY = 'monthly'
BIMONTHLY = 'bimonthly'
QUARTERLY = 'quarterly'
SPECIAL = 'special'

RELEASE_PERIOD = {
    DAILY: 'daily',
    WEEKLY: 'weekly',
    BIWEEKLY: 'biweekly',
    MONTHLY: 'monthly',
    BIMONTHLY: 'bimonthly',
    QUARTERLY: 'quarterly',
    SPECIAL: 'special',
}

TNC = 'tnc'
PRIVACY = 'privacy'
INFORMATION_TYPE = {
    TNC: 'tnc',
    PRIVACY: 'privacy'
}
