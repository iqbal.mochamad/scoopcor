import iso3166
import iso639
import os
from flask import url_for
from flask_appsfoundry.models import TimestampMixin, SoftDeletableMixin
from sqlalchemy import PrimaryKeyConstraint
from sqlalchemy.dialects.postgresql import ARRAY, ENUM, JSON, JSONB
from sqlalchemy.orm import deferred

from app import db, app
from app.items.choices import ITEM_TYPES, SCHEDULE
from app.master import choices
from app.master.choices import INFORMATION_TYPE
from app.offers.choices.offer_type import BUFFET
from app.revenue_shares.models import RevenueShares
from app.utils.models import url_mapped
from app.utils.shims import item_types, languages, countries

"""
    LEGEND:

        VENDOR STATUS:
            1. NEW
            2. WAITING FOR REVIEW
            3. IN REVIEW
            4. REJECTED
            5. APPROVED
            6. CLEAR
            7. BANNED

"""

# Junct table for default
db_default_categories = db.Table(
    'core_brands_defaultcategories',
    db.Column('brand_id', db.Integer, db.ForeignKey('core_brands.id')),
    db.Column('category_id', db.Integer, db.ForeignKey('core_categories.id')),
    PrimaryKeyConstraint('brand_id', 'category_id'),
)

# Junct table for default
db_default_parental = db.Table(
    'core_brands_defaultparentalcontrols',
    db.Column('brand_id', db.Integer, db.ForeignKey('core_brands.id')),
    db.Column('parental_id', db.Integer, db.ForeignKey('core_parentalcontrols.id')),
    PrimaryKeyConstraint('brand_id', 'parental_id'),
)

# Junct table for default
db_default_author = db.Table(
    'core_brands_defaultauthors',
    db.Column('brand_id', db.Integer, db.ForeignKey('core_brands.id')),
    db.Column('author_id', db.Integer, db.ForeignKey('core_authors.id')),
    PrimaryKeyConstraint('brand_id', 'author_id'),
)

# Junct table for default
db_default_distribution = db.Table(
    'core_brands_defaultditributioncountries',
    db.Column('brand_id', db.Integer, db.ForeignKey('core_brands.id')),
    db.Column('distribution_id', db.Integer, db.ForeignKey('core_distributioncountries.id')),
    PrimaryKeyConstraint('brand_id', 'distribution_id'),
)


class Currencies(db.Model, TimestampMixin):
    __tablename__ = 'core_currencies'

    id = db.Column(db.Integer, primary_key=True)
    iso4217_code = db.Column(db.String(3), nullable=False)
    left_symbol = db.Column(db.String(5))
    right_symbol = db.Column(db.String(5), nullable=False)
    group_separator = db.Column(db.String(1))
    decimal_separator = db.Column(db.String(1))
    one_usd_equals = db.Column(db.Numeric(10, 2), nullable=False)
    is_active = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return '<iso4217_code : %s>' % self.iso4217_code

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "currencies success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "currencies success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['iso4217_code'] = self.iso4217_code
        rv['left_symbol'] = self.left_symbol
        rv['right_symbol'] = self.right_symbol
        rv['group_separator'] = self.group_separator
        rv['decimal_separator'] = self.decimal_separator
        rv['one_usd_equals'] = str(self.one_usd_equals)
        rv['is_active'] = self.is_active

        return rv


@url_mapped('vendor.details', (('vendor_id', 'id'),))
class Vendor(db.Model, TimestampMixin):
    __tablename__ = 'core_vendors'

    name = db.Column(db.Text)

    description = deferred(db.Column(db.Text), group='details')
    slug = deferred(db.Column(db.Text, unique=True), group='details')
    sort_priority = deferred(db.Column(db.SmallInteger), group='details')
    is_active = deferred(db.Column(db.Boolean(), default=False), group='details')
    organization_id = deferred(db.Column(db.Integer, nullable=False), group='details')
    vendor_status = deferred(db.Column(db.SmallInteger, nullable=False), group='details')  # READ LEGEND VENDOR STATUS
    meta = deferred(db.Column(db.Text), group='details')
    icon_image_normal = deferred(db.Column(db.String(100)), group='details')
    icon_image_highres = deferred(db.Column(db.String(100)), group='details')
    accounting_identifier = deferred(db.Column(db.String(100)), group='details')
    iso4217_code = deferred(db.Column(db.String(3)), group='details')

    revenue_shares = db.relationship("VendorRevenueShare")
    report = db.relationship("VendorReport", uselist=False, backref="vendor")

    def __repr__(self):
        return '<name : %s>' % self.name

    @property
    def api_url(self):
        try:
            return url_for('vendor.details', vendor_id=self.id, _external=True)
        except RuntimeError:
            return None

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Vendor success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Vendor success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def get_links(self):
        links = []

        brlink = VendorLink.query.filter_by(vendor_id=self.id).all()
        for data in brlink:
            temp = {}
            temp['link_type'] = data.link_type
            temp['url'] = data.url
            links.append(temp)
        return links

    def get_reports(self):
        reports = VendorReport.query.filter_by(vendor_id=self.id).first()
        return reports

    def get_objects_reports(self):
        report = self.get_reports()
        if report:
            return report.values()
        else:
            return None

    def get_revenueshares(self):
        data_revenue = []
        revenue = VendorRevenueShare.query.filter_by(vendor_id=self.id, is_active=True).all()
        for irev in revenue:
            data_revenue.append(irev.values())
        return data_revenue

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['name'] = self.name
        rv['description'] = self.description
        rv['slug'] = self.slug
        rv['meta'] = self.meta
        rv['icon_image_normal'] = self.icon_image_normal
        rv['icon_image_highres'] = self.icon_image_highres
        rv['sort_priority'] = self.sort_priority
        rv['vendor_status'] = self.vendor_status
        rv['is_active'] = self.is_active
        rv['organization_id'] = self.organization_id
        rv['links'] = self.get_links()

        rv['accounting_identifier'] = self.accounting_identifier
        rv['iso4217_code'] = self.iso4217_code

        rv['report'] = self.get_objects_reports()
        rv['revenue_shares'] = self.get_revenueshares()
        return rv


class Brand(db.Model, TimestampMixin):
    __tablename__ = 'core_brands'

    name = db.Column(db.Text, nullable=False, unique=True)
    description = deferred(db.Column(db.Text), group='details')
    slug = deferred(db.Column(db.Text, nullable=False), group='details')
    meta = deferred(db.Column(db.Text), group='details')
    icon_image_normal = deferred(db.Column(db.String(100)), group='details')
    icon_image_highres = deferred(db.Column(db.String(100)), group='details')
    sort_priority = deferred(db.Column(db.SmallInteger), group='details')
    is_active = deferred(db.Column(db.Boolean(), default=False), group='details')
    daily_release_quota = deferred(db.Column(db.Integer), group='details')
    brand_code = deferred(db.Column(db.Text), group='details')

    vendor_id = deferred(db.Column(db.Integer, db.ForeignKey('core_vendors.id'), nullable=False), group='details')
    vendor = db.relationship("Vendor")

    default_item_type = deferred(db.Column(ENUM(*ITEM_TYPES.values(), name='item_type')), group='details')
    default_item_schedule = deferred(db.Column(ENUM(*SCHEDULE.values(), name='item_schedule')), group='details')

    default_parentalcontrol_id = deferred(db.Column(db.Integer, db.ForeignKey('core_parentalcontrols.id')),
                                          group='details')
    default_parentalcontrol = db.relationship("ParentalControl")

    default_countries = deferred(db.Column(ARRAY(db.String(50)), server_default="{}", nullable=False), group='details')
    default_languages = deferred(db.Column(ARRAY(db.String(50)), server_default="{}", nullable=False), group='details')

    primary_category_id = deferred(db.Column(db.Integer, db.ForeignKey('core_categories.id')), group='details')
    primary_category = db.relationship('Category')

    release_period = deferred(db.Column(ENUM(*choices.RELEASE_PERIOD.values(), name='upload_period_types')),
                              group='details')

    default_categories = db.relationship(
        'Category',
        secondary=db_default_categories,
        backref=db.backref('core_brands', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    default_author = db.relationship(
        'Author',
        secondary=db_default_author,
        backref=db.backref('core_brands', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    default_distributions = db.relationship(
        'DistributionCountryGroup',
        secondary=db_default_distribution,
        backref=db.backref('core_brands', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    @property
    def api_url(self):
        try:
            return url_for('brands.detail', brand_id=self.id, _external=True)
        except RuntimeError:
            return None

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Brand success add"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Brand success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def get_links(self):
        return [{'link_type': link.link_type,
                 'url': link.url}
                for link in self.links]

    def values(self):
        rv = {}
        rv['id'] = self.id
        rv['name'] = self.name
        rv['description'] = self.description
        rv['slug'] = self.slug
        rv['meta'] = self.meta
        rv['icon_image_normal'] = self.icon_image_normal
        rv['icon_image_highres'] = self.icon_image_highres
        rv['sort_priority'] = self.sort_priority
        rv['is_active'] = self.is_active
        rv['vendor_id'] = self.vendor_id
        rv['links'] = self.get_links()
        rv['daily_release_quota'] = self.daily_release_quota
        rv['default_item_schedule'] = self.default_item_schedule

        rv['default_parental_control_id'] = self.default_parentalcontrol_id

        rv['default_authors'] = [{'name': i.name, 'id': i.id}
                                 for i in self.default_author]

        rv['default_distribution_groups'] = [{'name': i.name, 'id': i.id}
                                             for i in self.default_distributions]

        rv['default_categories'] = [{'name': i.name, 'id': i.id}
                                    for i in self.default_categories]

        # the following require conversions to be consistent
        # with our documented API format:

        # turn the string item_type into it's previous database primary key
        if self.default_item_type:
            rv['default_item_type_id'] = \
                item_types.enum_to_id(self.default_item_type)
        else:
            rv['default_item_type_id'] = None

        # the following two were dropped as database tables and denormalized
        # into arrays of ISO codes.
        rv['default_languages'] = [
            {
                'id': languages.iso639_to_id(lang_code),
                'name': iso639.find(lang_code)['name']
            }
            for lang_code in self.default_languages]

        rv['default_countries'] = [
            {
                'name': iso3166.countries.get(country_code).name,
                'id': countries.iso3166_to_id(country_code),
            }
            for country_code in self.default_countries]

        rv['buffet_offers'] = self.get_buffet_offers()
        rv['brand_code'] = self.brand_code
        rv['default_item_schedule'] = self.default_item_schedule
        rv['release_period'] = self.release_period
        return rv

    def get_buffet_offers(self):
        brand_offers = self.core_offers.filter_by(offer_type_id=BUFFET).all()
        return [{'id': offer.id, 'name': offer.name} for offer in brand_offers]


class Category(db.Model, SoftDeletableMixin, TimestampMixin):
    __tablename__ = 'core_categories'

    name = db.Column(db.String(40), unique=True, nullable=False)
    description = db.Column(db.Text)
    slug = db.Column(db.String(100), unique=True, nullable=False)
    meta = db.Column(db.Text)
    icon_image_normal = db.Column(db.String(100))
    icon_image_highres = db.Column(db.String(100))
    sort_priority = db.Column(db.SmallInteger, default=0, nullable=False)

    parent_category_id = db.Column(db.Integer, db.ForeignKey('core_categories.id'))
    parent_category = db.relationship('Category')

    item_type = db.Column(ENUM(*ITEM_TYPES.values(), name='item_type'), nullable=False)
    countries = db.Column(ARRAY(db.String(3)), server_default="{}", nullable=False)

    @property
    def api_url(self):
        try:
            return url_for('category.details', db_id=self.id, _external=True)
        except RuntimeError:
            return None

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Category success add"}
        except Exception as e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Category success deleted"}
        except Exception as e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['name'] = self.name
        rv['description'] = self.description
        rv['slug'] = self.slug
        rv['meta'] = self.meta
        rv['icon_image_normal'] = self.icon_image_normal or ''
        rv['icon_image_highres'] = self.icon_image_highres or ''
        rv['media_base_url'] = app.config['MEDIA_BASE_URL']
        rv['sort_priority'] = self.sort_priority
        rv['is_active'] = self.is_active
        rv['parent_category_id'] = self.parent_category_id
        rv['item_type_id'] = item_types.enum_to_id(self.item_type)

        if self.countries:
            rv['countries'] = [
                {
                    'name': iso3166.countries.get(country_code).name,
                    'id': countries.iso3166_to_id(country_code),
                }
                for country_code in self.countries]
        else:
            rv['countries'] = None

        return rv


partner_client = db.Table(
    'core_partners_clients',
    db.Column('partner_id', db.Integer, db.ForeignKey('core_partners.id'), nullable=False),
    db.Column('client_id', db.Integer, db.ForeignKey('cas_clients.id'), nullable=False),
    db.PrimaryKeyConstraint('partner_id', 'client_id', name='core_partners_clients_pk'))


class Partner(db.Model, TimestampMixin):

    __tablename__ = 'core_partners'

    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(40))
    sort_priority = db.Column(db.SmallInteger, default=1)
    partner_status = db.Column(db.Integer)  # READ SAME AS VENDOR STATUS LEGEND
    slug = db.Column(db.String(100))
    meta = db.Column(db.Text)
    description = db.Column(db.Text)
    icon_image_normal = db.Column(db.String(100))
    icon_image_highres = db.Column(db.String(100))
    organization_id = db.Column(db.Integer, nullable=True)

    # store api of oauth partner & key
    auth_url = db.Column(db.String(250))
    auth_key = db.Column(db.String(250))

    # store webview oauth of partners
    web_oauth_page = db.Column(db.String(250), nullable=True)
    web_success_page = db.Column(db.String(250), nullable=True)

    reward_offers = db.Column(JSONB)
    display_offers = db.Column(ARRAY(db.String))

    is_active = db.Column(db.Boolean(), nullable=False)
    is_displayed = db.Column(db.Boolean(), nullable=False)

    clients = db.relationship(
        'Client',
        secondary=partner_client,
        backref=db.backref('core_partners', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    def __repr__(self):
        return '<name : %s>' % self.name

    def image_url(self):
        return os.path.join(app.config['BOTO3_NGINX_STATICS'], 'partner', self.icon_image_highres) if self.icon_image_highres else None

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Partner success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Partner success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['name'] = self.name
        rv['description'] = self.description
        rv['partner_status'] = self.partner_status
        rv['slug'] = self.slug
        rv['meta'] = self.meta
        rv['icon_image_normal'] = self.icon_image_normal
        rv['icon_image_highres'] = self.icon_image_highres
        rv['sort_priority'] = self.sort_priority
        rv['is_active'] = self.is_active
        rv['organization_id'] = self.organization_id

        return rv


class Platform(db.Model, TimestampMixin):
    __tablename__ = 'core_platforms'

    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(40))
    description = db.Column(db.Text)
    is_active = db.Column(db.Boolean(), nullable=False)

    @property
    def api_url(self):
        return url_for('platforms.details', platform_id=self.id, _external=True)

    def __repr__(self):
        return '<name : %s>' % self.name

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Platform success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Platform success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['name'] = self.name
        rv['description'] = self.description
        rv['is_active'] = self.is_active

        return rv


class BrandLink(db.Model, TimestampMixin):
    __tablename__ = 'core_brandlinks'

    id = db.Column(db.Integer, primary_key=True)
    brand_id = db.Column(db.Integer, db.ForeignKey("core_brands.id"), nullable=False)
    brand = db.relationship("Brand", backref="links")

    link_type = db.Column(db.Integer)
    url = db.Column(db.String(250))
    is_active = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return '<brand_id : %s url: %s>' % (self.brand_id, self.url)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "BrandLink success add"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "BrandLink success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}
        rv['id'] = self.id
        rv['brand_id'] = self.brand_id
        rv['link_type'] = self.link_type
        rv['url'] = self.url
        rv['is_active'] = self.is_active
        return rv


class VendorLink(db.Model, TimestampMixin):
    __tablename__ = 'core_vendorlinks'

    id = db.Column(db.Integer, primary_key=True)
    vendor_id = db.Column(db.Integer, db.ForeignKey('core_vendors.id'), nullable=False)
    vendor = db.relationship('Vendor', backref='links')
    link_type = db.Column(db.Integer)
    url = db.Column(db.String(250))
    is_active = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return '<vendor_id : %s url: %s>' % (self.vendor_id, self.url)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "VendorLink success add"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "VendorLink success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}
        rv['id'] = self.id
        rv['vendor_id'] = self.vendor_id
        rv['link_type'] = self.link_type
        rv['url'] = self.url
        rv['is_active'] = self.is_active
        return rv


class VendorReport(db.Model, TimestampMixin):
    __tablename__ = 'core_vendorreports'

    id = db.Column(db.Integer, primary_key=True)
    vendor_id = db.Column(db.Integer, db.ForeignKey('core_vendors.id'))
    show_preferred_currency_rates = db.Column(db.Boolean(), default=False)
    report_header_logo = db.Column(db.Integer, default=1)
    report_summary_format = db.Column(db.Integer, default=1)
    report_summary_as_invoice = db.Column(db.Boolean(), default=False)
    report_detail_columns = db.Column(JSON(db.Text))

    def __repr__(self):
        return '<vendor_id : %s>' % (self.vendor_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "VendorReport success add"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "VendorReport success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}
        rv['id'] = self.id
        rv['vendor_id'] = self.vendor_id
        rv['show_preferred_currency_rates'] = self.show_preferred_currency_rates
        rv['report_header_logo'] = self.report_header_logo
        rv['report_summary_format'] = self.report_summary_format
        rv['report_summary_as_invoice'] = self.report_summary_as_invoice
        rv['report_detail_columns'] = self.report_detail_columns
        return rv


class VendorRevenueShare(db.Model, TimestampMixin):
    __tablename__ = 'core_vendorrevenueshares'

    id = db.Column(db.Integer, primary_key=True)
    vendor_id = db.Column(db.Integer, db.ForeignKey('core_vendors.id'),
                          nullable=False)
    vendor = db.relationship('Vendor', uselist=False)
    revenue_id = db.Column(db.Integer, db.ForeignKey('core_revenueshares.id'),
                           nullable=False)
    revenue_share = db.relationship('RevenueShares', uselist=False)
    valid_to = db.Column(db.DateTime, nullable=False)
    valid_from = db.Column(db.DateTime, nullable=False)
    is_active = db.Column(db.Boolean(), default=True)

    def __repr__(self):
        return '<vendor_id : %s>' % (self.vendor_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "VendorRevenueShare success add"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "VendorRevenueShare success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def get_revenue_share(self):
        rev = RevenueShares.query.filter_by(id=self.revenue_id).first()
        if rev:
            return rev.name
        return None

    def values(self):
        rv = {}
        rv['id'] = self.id
        rv['vendor_id'] = self.vendor_id
        rv['revenue_id'] = self.revenue_id
        rv['valid_to'] = self.valid_to.isoformat()
        rv['valid_from'] = self.valid_from.isoformat()
        return rv


class StudentSave(db.Model):
    __tablename__ = 'core_eperpus_studentsaves'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), nullable=False)
    description = db.Column(db.Text)
    is_active = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return '<student_save : {}>'.format(self.name)


class GeneralInformation(db.Model, TimestampMixin):
    __tablename__ = 'core_informations'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(250), nullable=False)
    content = db.Column(db.Text, nullable=False)
    information_type = db.Column(ENUM(*INFORMATION_TYPE.values(), name='information_type'), nullable=False)
    language = db.Column(db.String(2))

    def __repr__(self):
        return '<general_info : {}>'.format(self.title)
