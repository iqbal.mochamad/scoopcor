from flask import request
from pycountry import countries, languages

from flask_appsfoundry import viewmodels

from .helpers import build_query
from app.utils.shims.countries import iso3166_to_id
from app.utils.shims.languages import iso639_to_id


class BrandViewmodel(viewmodels.SaViewmodelBase):

    @property
    def default_countries(self):
        return [{"id": iso3166_to_id(country_code), "name": countries.get(alpha2=country_code.upper()).name}
                for country_code in self._model_instance.default_countries]

    @property
    def default_languages(self):
        return [{"id": iso639_to_id(language_code), "name": languages.get(iso639_2T_code=language_code).name}
                for language_code in self._model_instance.default_languages]

    @property
    def default_distribution_groups(self):
        return [{'id': default_distribution.id, 'name': default_distribution.name}
                for default_distribution in self._model_instance.default_distributions]

    @property
    def buffet_offers(self):
        return [buffet for buffet in self._model_instance.get_buffet_offers()]


class VendorRevenueShareListViewModel(object):
    def __init__(self, query, args):
        query = build_query(query, args)
        self.authors = query['query']

        self.metadata = {'resultset':
                             {'count': query['query_count'],
                              'offset': request.args.get('offset', 0),
                              'limit': request.args.get('limit', 20)}}
