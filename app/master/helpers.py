from smtplib import SMTPException

from flask import request, render_template
from flask_appsfoundry.parsers import specialized

from app.send_mail import sendmail_aws, sendmail_smtp


def build_query(base_query, input_args):
    """
        :param base_query: base models
        :param input_args: args from parsers
        :return: result query and count query
    """
    query = base_query

    ordering = specialized.SqlAlchemyOrderingParser().parse_args().get('order')
    for order_opt in ordering:
        query = query.order_by(order_opt)

    # pagination
    query_count = query.count()

    query = query.limit(request.args.get('limit', 20)).offset(
        request.args.get('offset', 0)).all()
    query = {'query': query, 'query_count': query_count}
    return query


def start_date_overlap(model_class, args):
    """
    checking date in some existing table has column valid_from and valid_to,
    and the new data not allowed to insert if date is between existing
    valid_from and valid_to

    :param model_class: type SQLAlchemy models
    :param args: type request body from current endpoint
    :return: False if overlap, True if not overlap
    """
    existing_data = model_class.query.filter_by(
        vendor_id=args['vendor_id']).all()

    for row in existing_data:
        if row.valid_from < args['valid_from'] < row.valid_to:
            return False
        else:
            return True


def send_email_notify_publiser(email, brand_name):
    context = {'email': email, 'brand_name': brand_name}
    subject = "Reminder for submitting file content"

    content_html = render_template(
        'email/publisher/notify_late_content.html', **context)
    content_text = render_template(
        'email/publisher/notify_late_content.txt', **context)

    try:
        sendmail_aws(subject, 'Gramedia Digital <ebooks@gramedia.digital>', [email, ], content_html, content_text)
    except SMTPException:
        sendmail_smtp(subject, 'Gramedia Digital <ebooks@gramedia.digital>', [email, ], content_html)
