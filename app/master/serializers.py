from flask_appsfoundry.serializers import fields, SerializerBase

from app.depreciated.fields import V1CountryDataFromIso3166, V1CountrySummaryFromIso3166
from app.items.choices import ITEM_TYPES


class Link(fields.Raw):

    def format(self, value):
        return {
            'link_type': value.link_type,
            'url': value.url
        }


class VendorRevenueShareSerializer(SerializerBase):
    id = fields.Integer
    vendor_id = fields.Integer
    revenue_id = fields.Integer
    valid_to = fields.DateTime
    valid_from = fields.DateTime
    is_active = fields.Boolean


class VendorReportSerializer(SerializerBase):
    id = fields.Integer
    vendor_id = fields.Integer
    show_preferred_currency_rates = fields.Boolean
    report_header_logo = fields.Integer
    report_summary_format = fields.Integer
    report_summary_as_invoice = fields.Integer
    report_detail_columns = fields.String


class VendorSerializer(SerializerBase):
    id = fields.Integer
    name = fields.String
    description = fields.String
    slug = fields.String
    sort_priority = fields.Integer
    is_active = fields.Boolean
    organization_id = fields.Integer
    vendor_status = fields.Integer
    meta = fields.String
    icon_image_normal = fields.String
    icon_image_highres = fields.String
    accounting_identifier = fields.String
    iso4217_code = fields.String
    links = fields.List(Link)
    revenue_shares = fields.List(fields.Nested(VendorRevenueShareSerializer()))
    report = fields.Nested(VendorReportSerializer(), allow_null=True)
    modified = fields.DateTime('iso8601')


class V1CountrySummary(V1CountryDataFromIso3166):

    def format(self, value):
        all_data = super(V1CountrySummary, self).format(value)
        return {'id': all_data['id'], 'name': all_data['name']}


class CategorySerializer(SerializerBase):
    id = fields.Integer
    name = fields.String
    description = fields.String
    slug = fields.String
    meta = fields.String
    icon_image_normal = fields.String(default="")
    icon_image_highres = fields.String(default="")
    sort_priority = fields.Integer
    parent_category_id = fields.Integer(default=None)
    item_type_id = fields.EnumFieldFormatter(ITEM_TYPES, attribute='item_type')
    countries = fields.List(V1CountrySummaryFromIso3166)
    is_active = fields.Boolean
    media_base_url = fields.String(default="https://images.apps-foundry.com/magazine_static/")


class BrandSerializer(SerializerBase):
    id = fields.Integer
    name = fields.String
    description = fields.String
    slug = fields.String
    meta = fields.String
    icon_image_normal = fields.String
    icon_image_highres = fields.String
    sort_priority = fields.Integer
    is_active = fields.Boolean
    daily_release_quota = fields.Integer(default=None)
    vendor_id = fields.Integer
    modified = fields.DateTime
    links = fields.List(Link)
    brand_code = fields.String

    default_author = fields.List(fields.Nested({'id': fields.Integer, 'name': fields.String}))
    default_categories = fields.List(fields.Nested({'id': fields.Integer, 'name': fields.String}))
    default_countries = fields.List(fields.Nested({'id': fields.Integer, 'name': fields.String}))
    default_distribution_groups = fields.List(fields.Nested({'id': fields.Integer, 'name': fields.String}))
    default_item_type_id = fields.EnumFieldFormatter(enum_dict=ITEM_TYPES, attribute='default_item_type')
    default_languages = fields.List(fields.Nested({'id': fields.Integer, 'name': fields.String}))
    default_parental_control_id = fields.String
    default_item_schedule = fields.String
    buffet_offers = fields.List(fields.Nested({'id': fields.Integer, 'name': fields.String}))


class VendorRevenueSerializer(SerializerBase):
    id = fields.Integer
    vendor_id = fields.Integer
    revenue_id = fields.Integer
    valid_to = fields.DateTime('iso8601')
    valid_from = fields.DateTime('iso8601')
    is_active = fields.Boolean


class ReportedReviewSerializer(SerializerBase):
    id = fields.Integer
    reported_count = fields.Integer
    review = fields.String
    item_name = fields.String
    is_active = fields.Boolean
    href = fields.String(attribute='reported_detail_url')


class ReportedReviewDetailSerializer(ReportedReviewSerializer):
    reports = fields.List(
        fields.Nested({'id': fields.Integer,
                       'reason': fields.String,
                       'email': fields.String(attribute='user.email')}))
