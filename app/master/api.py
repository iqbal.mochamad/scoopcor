from __future__ import absolute_import

import json
from httplib import OK, CREATED, NO_CONTENT, NOT_FOUND, BAD_REQUEST

from flask import Response, request, jsonify, g
from flask_appsfoundry import Resource, marshal, controllers
from flask_appsfoundry.exceptions import NotFound
from flask_restful import reqparse
from sqlalchemy import func, desc

from app import db
from app.auth.decorators import token_required, user_has_any_tokens
from app.custom_get import CustomGet
from app.helpers import conflict_message, internal_server_message, err_response, regenerate_slug, \
    portal_metadata_changes
from app.items.choices import STATUS_TYPES
from app.items.choices.status import STATUS_NOT_FOR_CONSUME, STATUS_READY_FOR_CONSUME
from app.items.models import Item, DistributionCountryGroup, Author, Review, ReportReview
from app.master.custom_get import OrganizationTypeVendor
from app.master.helpers import send_email_notify_publiser
from app.users.organizations import Organization
from . import _log, parsers, serializers, models, helpers, viewmodels


class CurrenciesListApi(Resource):
    """ Shows a list of Currencies and do POST / GET """

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        self.reqparser.add_argument('iso4217_code', type=str, required=True, location='json')
        self.reqparser.add_argument('right_symbol', type=str, required=True, location='json')
        self.reqparser.add_argument('one_usd_equals', type=str, required=True, location='json')
        self.reqparser.add_argument('left_symbol', type=str, required=True, location='json')

        self.reqparser.add_argument('group_separator', type=str, location='json')
        self.reqparser.add_argument('decimal_separator', type=str, location='json')
        self.reqparser.add_argument('is_active', type=bool, location='json')

        super(CurrenciesListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_currency')
    def get(self):
        param = request.args.to_dict()
        scene = CustomGet(param, 'currencies', 'POST').construct()
        return scene

    @token_required('can_read_write_global_all, can_read_write_global_currency')
    def post(self):
        args = self.reqparser.parse_args()

        # check if currencies already exist with same right_symbol
        po = models.Currencies.query.filter_by(iso4217_code=args['iso4217_code']).first()
        if po:
            return conflict_message(
                modul="currencies",
                method="POST",
                conflict=args['iso4217_code'],
                req=args)

        # construct in same dict
        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            m = models.Currencies(**kwargs)
            sv = m.save()  # if this return invalid True raise exceptions
            if sv.get('invalid', False):
                return internal_server_message(
                    modul="currencies",
                    method="POST",
                    error=sv.get('message', ''),
                    req=str(args))
            else:
                rv = m.values()
                return Response(json.dumps(rv), status=CREATED, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="currencies", method="POST",
                                           error=str(e), req=str(args))


class CurrenciesApi(Resource):
    """ GET/PUT/DELETE Currencies """

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        self.reqparser.add_argument('iso4217_code', type=str, required=True, location='json')
        self.reqparser.add_argument('right_symbol', type=str, required=True, location='json')
        self.reqparser.add_argument('one_usd_equals', type=str, required=True, location='json')
        self.reqparser.add_argument('left_symbol', type=str, required=True, location='json')

        self.reqparser.add_argument('group_separator', type=str, location='json')
        self.reqparser.add_argument('decimal_separator', type=str, location='json')
        self.reqparser.add_argument('is_active', type=bool, location='json')

        super(CurrenciesApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_currency')
    def get(self, cur_id=None):
        m = models.Currencies.query.filter_by(id=cur_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=OK, mimetype='application/json')
        else:
            return Response(status=NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_currency')
    def put(self, cur_id=None):

        m = models.Currencies.query.filter_by(id=cur_id).first()
        if not m:
            return Response(status=NOT_FOUND)

        args = self.reqparser.parse_args()

        # check conflict or not if edit
        if args['iso4217_code'] != m.iso4217_code:
            u = models.Currencies.query.filter_by(iso4217_code=args['iso4217_code']).first()
            if u:
                return conflict_message(modul="Currencies", method="PUT",
                                        conflict=args['iso4217_code'], req=args)

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            f = models.Currencies.query.get(cur_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="currencies", method="PUT",
                                               error=sv.get('message', ''), req=str(args))

            else:
                rv = m.values()
                return Response(json.dumps(rv), status=OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="currencies", method="PUT",
                                           error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, cur_id=None):
        f = models.Currencies.query.filter_by(id=cur_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=NOT_FOUND)


class VendorListApi(controllers.ListApiResource):
    """ Shows a list Vendor and do GET """

    query_set = models.Vendor.query

    list_request_parser = parsers.VendorListArgsParser

    serialized_list_name = u'vendors'

    response_serializer = serializers.VendorSerializer

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_global_vendor',
                           )

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('slug', type=str, required=True, location='json')
        self.reqparser.add_argument('name', type=unicode, required=True, location='json')
        self.reqparser.add_argument('sort_priority', type=int, required=True, location='json')
        self.reqparser.add_argument('vendor_status', type=int, required=True, location='json')
        self.reqparser.add_argument('organization_id', type=int, required=True, location='json')

        # optional parameters
        self.reqparser.add_argument('is_active', type=bool, required=False, location='json')
        self.reqparser.add_argument('description', type=unicode, location='json')
        self.reqparser.add_argument('meta', type=unicode, location='json')
        self.reqparser.add_argument('icon_image_normal', type=str, location='json')
        self.reqparser.add_argument('icon_image_highres', type=str, location='json')
        self.reqparser.add_argument('links', type=list, location='json')

        self.reqparser.add_argument('accounting_identifier', type=str, location='json')
        self.reqparser.add_argument('iso4217_code', type=str, location='json')

        self.reqparser.add_argument('show_preferred_currency_rates', type=bool, location='json')
        self.reqparser.add_argument('report_header_logo', type=int, location='json')
        self.reqparser.add_argument('report_summary_format', type=int, location='json')
        self.reqparser.add_argument('report_summary_as_invoice', type=bool, location='json')
        self.reqparser.add_argument('report_detail_columns', type=dict, location='json')

        self.reqparser.add_argument('id', type=int, location='json')

        super(VendorListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_global_vendor')
    def post(self):
        args = self.reqparser.parse_args()

        # regenerate new slug if slug is exists
        args['slug'] = regenerate_slug(models.Vendor, args.get('slug', None))

        # check Vendor already exist with same name
        po = models.Vendor.query.filter_by(name=args['name']).first()
        if po:
            return conflict_message(modul="vendor", method="POST",
                                    conflict=args['name'], req=args)

        # construct in same dict
        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        if args['links']:
            del kwargs['links']
        if args['show_preferred_currency_rates'] is not None:
            del kwargs['show_preferred_currency_rates']
        if args['report_summary_as_invoice'] is not None:
            del kwargs['report_summary_as_invoice']

        if args['report_header_logo']:
            del kwargs['report_header_logo']
        if args['report_summary_format']:
            del kwargs['report_summary_format']
        if args['report_detail_columns']:
            del kwargs['report_detail_columns']

        try:
            m = models.Vendor(**kwargs)
            sv = m.save()  # if this return invalid True raise exceptions
            if sv.get('invalid', False):
                return internal_server_message(modul="vendor", method="POST",
                                               error=sv.get('message', ''), req=str(args))
            else:

                # Adding brand link urls
                if args['links']:
                    for link in args['links']:
                        url = link.get('url', '')
                        link_type = link.get('link_type', 0)

                        br = models.VendorLink(
                            vendor_id=m.id,
                            link_type=link_type,
                            url=url,
                            is_active=True
                        )
                        mv = br.save()
                        if mv.get('invalid', False):
                            pass

                # create reports vendors
                reports = models.VendorReport(
                    show_preferred_currency_rates=args.get('show_preferred_currency_rates', False),
                    report_header_logo=args.get('report_header_logo', 1),
                    report_summary_format=args.get('report_summary_format', 1),
                    report_summary_as_invoice=args.get('report_summary_as_invoice', False),
                    report_detail_columns=args.get('report_detail_columns', None),
                    vendor_id=m.id
                )
                reports.save()

                rv = m.values()
                return Response(json.dumps(rv), status=CREATED, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="vendor", method="POST",
                                           error=e, req=str(args))


class VendorApi(Resource):
    """ GET/PUT/DELETEmodels.Vendor """

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('slug', type=str, required=True, location='json')
        self.reqparser.add_argument('name', type=unicode, required=True, location='json')
        self.reqparser.add_argument('sort_priority', type=int, required=True, location='json')
        self.reqparser.add_argument('vendor_status', type=int, required=True, location='json')
        self.reqparser.add_argument('organization_id', type=int, required=True, location='json')

        # optional parameters
        self.reqparser.add_argument('is_active', type=bool, required=False, location='json')
        self.reqparser.add_argument('description', type=unicode, location='json')
        self.reqparser.add_argument('meta', type=unicode, location='json')
        self.reqparser.add_argument('icon_image_normal', type=str, location='json')
        self.reqparser.add_argument('icon_image_highres', type=str, location='json')
        self.reqparser.add_argument('links', type=list, location='json')

        self.reqparser.add_argument('accounting_identifier', type=str, location='json')
        self.reqparser.add_argument('iso4217_code', type=str, location='json')

        self.reqparser.add_argument('show_preferred_currency_rates', type=bool, location='json')
        self.reqparser.add_argument('report_header_logo', type=int, location='json')
        self.reqparser.add_argument('report_summary_format', type=int, location='json')
        self.reqparser.add_argument('report_summary_as_invoice', type=bool, location='json')
        self.reqparser.add_argument('report_detail_columns', type=dict, location='json')

        self.reqparser.add_argument('id', type=int, location='json')

        super(VendorApi, self).__init__()

    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_public', 'can_read_global_vendor')
    def get(self, vendor_id=None):
        m = models.Vendor.query.filter_by(id=vendor_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=OK, mimetype='application/json')
        else:
            return Response(status=NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_vendor')
    def put(self, vendor_id=None):

        m = models.Vendor.query.filter_by(id=vendor_id).first()
        if not m:
            return Response(status=NOT_FOUND)

        args = self.reqparser.parse_args()

        # checking if conflict when edit
        if args['name'] != m.name:
            u = models.Vendor.query.filter_by(name=args['name']).first()
            if u:
                return conflict_message(modul="vendor", method="PUT",
                                        conflict=args['name'], req=args)

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        if args['links']:
            del kwargs['links']
        if args['show_preferred_currency_rates'] is not None:
            del kwargs['show_preferred_currency_rates']
        if args['report_summary_as_invoice'] is not None:
            del kwargs['report_summary_as_invoice']

        if args['report_header_logo']:
            del kwargs['report_header_logo']
        if args['report_summary_format']:
            del kwargs['report_summary_format']
        if args['report_detail_columns']:
            del kwargs['report_detail_columns']

        try:
            f = models.Vendor.query.get(vendor_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="vendor", method="PUT",
                                               error=sv.get('message', ''), req=str(args))
            else:

                # Update brand link urls
                if args['links']:

                    # clear all
                    mk = models.VendorLink.query.filter_by(vendor_id=f.id).all()
                    for i in mk:
                        i.delete()

                    for link in args['links']:
                        url = link.get('url', '')
                        link_type = link.get('link_type', 0)

                        br = models.VendorLink(
                            vendor_id=f.id,
                            link_type=link_type,
                            url=url,
                            is_active=True
                        )
                        mv = br.save()
                        if mv.get('invalid', False):
                            pass

                # delete old reports
                old_reports = f.get_reports()
                if old_reports:
                    old_reports.delete()

                reports = models.VendorReport(
                    show_preferred_currency_rates=args.get('show_preferred_currency_rates', False),
                    report_header_logo=args.get('report_header_logo', 1),
                    report_summary_format=args.get('report_summary_format', 1),
                    report_summary_as_invoice=args.get('report_summary_as_invoice', False),
                    report_detail_columns=args.get('report_detail_columns', None),
                    vendor_id=m.id
                )
                reports.save()

                rv = m.values()
                return Response(json.dumps(rv), status=OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="vendor", method="PUT",
                                           error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, vendor_id=None):
        f = models.Vendor.query.filter_by(id=vendor_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=NOT_FOUND)


class BrandListApi(controllers.ListApiResource):
    """ Shows a list of Brands and do POST / GET """

    viewmodel = viewmodels.BrandViewmodel

    query_set = models.Brand.query

    list_request_parser = parsers.BrandListArgsParser

    serialized_list_name = u'brands'

    response_serializer = serializers.BrandSerializer

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext',
                           'can_read_global_brand',
                           )

    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_global_brand')
    def post(self):

        session = models.Brand.query.session

        parser = parsers.BrandArgsParser()
        args = parser.parse_args()

        try:
            skip_args = [
                'default_categories',
                'default_distribution_groups',
                'default_authors',
                'links',
                'buffet_offers'
            ]

            # generate new slug if slug is exists
            args['slug'] = regenerate_slug(models.Brand, args.get('slug', None))

            brand = models.Brand(**{k: args[k] for k in args if k not in skip_args})

            session.add(brand)

            brand.default_categories.extend(
                session.query(models.Category).get(db_id) for db_id
                in args['default_categories'])

            brand.default_distributions.extend(
                session.query(DistributionCountryGroup).get(db_id) for db_id
                in args['default_distribution_groups']
            )

            brand.default_author.extend(
                session.query(Author).get(db_id) for db_id
                in args['default_authors']
            )

            brand.links.extend(
                models.BrandLink(is_active=True,
                                 url=brand_args.get('url'),
                                 link_type=brand_args.get('link_type'))
                for brand_args in args['links']
            )
            session.commit()

            # auto insert new brands to premium.
            buffet_ids = args.get('buffet_offers', [])
            for offer_ in buffet_ids:
                # cos Brand already import in offers.models, offer cant import at here.. looping import issue
                # dont try local import, flask still bug on this.!
                # cant use sql alchemy insert.!!
                sql = """
                    insert into core_offers_brands (offer_id, brand_id)
                    select {}, {}
                """.format(offer_, brand.id)
                db.engine.execute(sql)

            portal_metadata_changes(user_id=g.current_user.id, data_id=brand.id, data_url=request.base_url,
                                    data_method=request.method, data_request=args)

            return brand.values(), CREATED

        except Exception:
            session.rollback()
            raise


class BrandApi(controllers.ErrorHandlingApiMixin, Resource):
    """ GET/PUT/DELETE Brands """

    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_public',
                         'can_read_write_public_ext', 'can_read_global_brand')
    def get(self, brand_id):
        brand = models.Brand.query.get_or_404(brand_id)
        return brand.values(), OK

    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_global_brand')
    def put(self, brand_id):

        session = models.Brand.query.session

        brand = models.Brand.query.get_or_404(brand_id)

        parser = parsers.BrandArgsParser()
        args = parser.parse_args()

        try:
            skip_args = [
                'default_categories',
                'default_distribution_groups',
                'default_authors',
                'links',
                'id',
                'buffet_offers'
            ]

            for k in args:
                if k in skip_args:
                    continue
                setattr(brand, k, args[k])

            brand.default_categories = [
                session.query(models.Category).get(db_id) for db_id
                in args['default_categories']
            ]

            brand.default_distributions = [
                session.query(DistributionCountryGroup).get(db_id) for db_id
                in args['default_distribution_groups']
            ]

            brand.default_author = [
                session.query(Author).get(db_id) for db_id
                in args['default_authors']
            ]

            # this isn't so good.. because brand links doesn't have a naturl
            # key, and they're not posting the ids, we have eseentially the side-effect
            # of dropping all the old relations and creating them again on each post.
            brand.links = [
                models.BrandLink(is_active=True,
                                 url=brand_args.get('url'),
                                 link_type=brand_args.get('link_type'))
                for brand_args in args['links']
            ]
            session.commit()

            old_buffet = [old.get('id', 0) for old in brand.get_buffet_offers()]
            for old_offer_ in old_buffet:
                remove_sql = """
                    delete from core_offers_brands where brand_id={} and offer_id={}
                """.format(brand.id, old_offer_)
                db.engine.execute(remove_sql)

            new_buffet = args.get('buffet_offers', [])
            for offer_ in new_buffet:
                sql = """
                    insert into core_offers_brands (offer_id, brand_id)
                    select {}, {}
                """.format(offer_, brand.id)
                db.engine.execute(sql)

            portal_metadata_changes(user_id=g.current_user.id, data_id=brand.id, data_url=request.base_url,
                                    data_method=request.method, data_request=args)

            return brand.values(), OK

        except Exception:
            session.rollback()
            raise

    @token_required('can_read_write_global_all')
    def delete(self, brand_id):
        session = models.Brand.query.session

        brand = models.Brand.query.get_or_404(brand_id)
        brand.is_active = False
        session.commit()

        return None, NO_CONTENT


class CategoryListApi(controllers.ListCreateApiResource):
    """ Shows a list of Category and do POST / GET """
    query_set = models.Category.query
    list_request_parser = parsers.CategoryListArgsParser
    serialized_list_name = 'categories'
    response_serializer = serializers.CategorySerializer
    create_request_parser = parsers.CategoryArgsParser
    get_security_tokens = ('can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext',
                           'can_read_global_category',)
    post_security_tokens = ('can_read_write_global_all', 'can_read_write_global_category',)


class CategoryApi(controllers.DetailApiResource):
    """ GET/PUT/DELETE Category """
    query_set = models.Category.query
    update_request_parser = parsers.CategoryArgsParser
    response_serializer = serializers.CategorySerializer
    get_security_tokens = ('can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext',
                           'can_read_global_category',)
    put_security_tokens = ('can_read_write_global_all', 'can_read_write_global_category',)
    delete_security_tokens = ('can_read_write_global_all',)


class PartnerListApi(Resource):
    """ Shows a list of Partner and do POST / GET """

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('slug', type=str, required=True, location='json')
        self.reqparser.add_argument('name', type=unicode, required=True, location='json')
        self.reqparser.add_argument('sort_priority', type=int, required=True, location='json')
        self.reqparser.add_argument('partner_status', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')
        self.reqparser.add_argument('organization_id', type=int, required=True, location='json')

        # optional parameters
        self.reqparser.add_argument('description', type=unicode, location='json')
        self.reqparser.add_argument('meta', type=unicode, location='json')
        self.reqparser.add_argument('icon_image_normal', type=str, location='json')
        self.reqparser.add_argument('icon_image_highres', type=str, location='json')

        super(PartnerListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_partner')
    def get(self):
        param = request.args.to_dict()
        scene = CustomGet(param, 'partners', 'POST').construct()
        return scene

    @token_required('can_read_write_global_all, can_read_write_global_partner')
    def post(self):
        args = self.reqparser.parse_args()

        # check if Partner already exist with same name
        po = models.Partner.query.filter_by(name=args['name']).first()
        if po:
            return conflict_message(modul="patner", method="POST",
                                    conflict=args['name'], req=args)

        # construct in same dict
        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        m = models.Partner(**kwargs)
        sv = m.save()  # if this return invalid True raise exceptions
        if sv.get('invalid', False):
            return internal_server_message(modul="patner", method="POST",
                                           error=sv.get('message', ''), req=str(args))

        else:
            rv = m.values()
            return Response(json.dumps(rv), status=CREATED, mimetype='application/json')


class PartnerApi(Resource):
    """ GET/PUT/DELETE Partner """

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whose can't null
        self.reqparser.add_argument('slug', type=str, required=True, location='json')
        self.reqparser.add_argument('name', type=unicode, required=True, location='json')
        self.reqparser.add_argument('partner_status', type=int, required=True, location='json')
        self.reqparser.add_argument('sort_priority', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')
        self.reqparser.add_argument('organization_id', type=int, required=True, location='json')

        # optional parameters
        self.reqparser.add_argument('description', type=unicode, location='json')
        self.reqparser.add_argument('meta', type=unicode, location='json')
        self.reqparser.add_argument('icon_image_normal', type=str, location='json')
        self.reqparser.add_argument('icon_image_highres', type=str, location='json')

        super(PartnerApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_partner')
    def get(self, partner_id=None):
        m = models.Partner.query.filter_by(id=partner_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=OK, mimetype='application/json')
        else:
            return Response(status=NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_partner')
    def put(self, partner_id=None):

        m = models.Partner.query.filter_by(id=partner_id).first()
        if not m:
            return Response(status=NOT_FOUND)

        args = self.reqparser.parse_args()

        # checking if conflict when edit
        if args['name'] != m.name:
            u = models.Partner.query.filter_by(name=args['name']).first()
            if u:
                return conflict_message(modul="patner", method="PUT",
                                        conflict=args['name'], req=args)

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            f = models.Partner.query.get(partner_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="patner", method="PUT",
                                               error=sv.get('message', ''), req=str(args))

            else:
                rv = m.values()
                return Response(json.dumps(rv), status=OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="patner", method="PUT",
                                           error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, partner_id=None):
        f = models.Partner.query.filter_by(id=partner_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=NOT_FOUND)


class PlatformsListApi(Resource):
    """ Shows a list of Platforms and do POST / GET """

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('name', type=unicode, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')

        # optional parameters
        self.reqparser.add_argument('description', type=unicode, location='json')

        super(PlatformsListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_platform')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = CustomGet(param, 'platforms', 'POST').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="platforms", method="POST",
                                           error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_global_platform')
    def post(self):
        args = self.reqparser.parse_args()

        # check if Partner already exist with same name
        po = models.Platform.query.filter_by(name=args['name']).first()
        if po:
            return conflict_message(modul="platforms", method="POST",
                                    conflict=args['name'], req=args)

        # construct in same dict
        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            m = models.Platform(**kwargs)
            sv = m.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="platforms", method="POST",
                                               error=sv.get('message', ''), req=str(args))

            else:
                rv = m.values()
                return Response(json.dumps(rv), status=CREATED,
                                mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="platforms", method="POST",
                                           error=e, req=str(args))


class PlatformsApi(Resource):
    """ GET/PUT/DELETE Platforms """

    def __init__(self):
        self.req_parser = reqparse.RequestParser()

        self.req_parser.add_argument('name', type=unicode, required=True,
                                     location='json')
        self.req_parser.add_argument('is_active', type=bool, required=True,
                                     location='json')
        self.req_parser.add_argument('description', type=unicode,
                                     location='json')
        super(PlatformsApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_platform')
    def get(self, platform_id=None):
        m = models.Platform.query.filter_by(id=platform_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=OK, mimetype='application/json')
        else:
            return Response(status=NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_platform')
    def put(self, platform_id=None):

        m = models.Platform.query.filter_by(id=platform_id).first()
        if not m:
            return Response(status=NOT_FOUND)

        args = self.req_parser.parse_args()

        if args['name'] != m.name:
            u = models.Platform.query.filter_by(name=args['name']).first()
            if u:
                return conflict_message(modul="platforms", method="PUT",
                                        conflict=args['name'], req=args)

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        try:
            f = models.Platform.query.get(platform_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="platforms", method="PUT",
                                               error=sv.get('message', ''),
                                               req=str(args))
            else:
                rv = m.values()
                return Response(json.dumps(rv), status=OK,
                                mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="platforms", method="PUT",
                                           error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, platform_id=None):
        f = models.Platform.query.filter_by(id=platform_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=NO_CONTENT,
                            mimetype='application/json')

        else:
            return Response(status=NOT_FOUND)


class OrganizationVendor(Resource):
    """
        This api for scoopportal external.
        GET v1/vendors/<organization_id>/organizations
        RESPONSE : vendor_list, brand_id list, item_type
    """

    def get(self, organization_id):

        m = models.Vendor.query.filter_by(organization_id=organization_id).first()

        if m:
            rv = {'vendor_id': m.id, 'name': m.name}

            data_vendor = OrganizationTypeVendor(m.id).filter_brandtype()
            rv['item_types'] = data_vendor

            rv = json.dumps(rv)
            return Response(rv, status=OK, mimetype='application/json')
        else:
            return Response(status=NOT_FOUND)


class DeactivateVendor(Resource):
    """Deactivate all items + brands related with vendors"""

    @token_required('can_read_write_global_all')
    def put(self, vendor_id):
        try:
            data = []
            m = models.Vendor.query.filter_by(id=vendor_id).first()
            if not m:
                return Response(status=NOT_FOUND)

            # grep all brands with vendor id
            brands = models.Brand.query.filter_by(vendor_id=vendor_id).all()

            for ibrand in brands:
                tmp = {}

                items = Item.query.filter_by(brand_id=ibrand.id).all()

                # deactivate item status
                for item in items:
                    # deactivate items
                    item.item_status = STATUS_TYPES[STATUS_NOT_FOR_CONSUME]
                    item.save()

                tmp['brand'] = ibrand.name
                tmp['items'] = len(items)
                data.append(tmp)

            m.is_active = False
            m.vendor_status = 7
            m.save()

            rv = {'name': m.name, 'deactivate': data}
            return Response(json.dumps(rv), status=CREATED,
                            mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="vendor deactivate",
                                           method="PUT", error=e,
                                           req="vendor id : %s" % vendor_id)


class ActivateVendor(Resource):
    """Activate all items + brands related with vendors"""

    @token_required('can_read_write_global_all')
    def put(self, vendor_id=None):
        try:
            data = []
            m = models.Vendor.query.filter_by(id=vendor_id).first()
            if not m:
                return Response(status=NOT_FOUND)

            # grep all brands with vendor id
            brands = models.Brand.query.filter_by(vendor_id=vendor_id).all()

            for ibrand in brands:
                tmp = {}

                items = Item.query.filter_by(brand_id=ibrand.id).all()

                # deactivate item status
                for item in items:
                    # deactivate items
                    item.item_status = STATUS_TYPES[STATUS_READY_FOR_CONSUME]
                    item.save()

                tmp['brand'] = ibrand.name
                tmp['items'] = len(items)
                data.append(tmp)

            m.is_active = False
            m.vendor_status = 5
            m.save()

            rv = {'name': m.name, 'activate': data}
            return Response(json.dumps(rv), status=CREATED,
                            mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="vendor deactivate",
                                           method="PUT", error=e,
                                           req="vendor id : %s" % vendor_id)


class VendorRevenueListApi(Resource):
    @user_has_any_tokens('can_read_write_global_all',
                         'can_read_write_public', 'can_read_global_vendor')
    def get(self, vendor_id):

        session = db.session()

        if not session.query(models.Vendor).filter_by(id=vendor_id).count():
            raise NotFound

        query = session.query(models.VendorRevenueShare).filter_by(vendor_id=vendor_id)

        for f in parsers.VendorRevenueListArgsParser().parse_args()['filters']:
            query = query.filter(f)

        view_model = viewmodels.VendorRevenueShareListViewModel(query,
                                                                parsers.VendorRevenueListArgsParser().parse_args())

        serializer = serializers.VendorRevenueSerializer()

        view_model = {'vendor_revenue_shares': marshal(view_model.authors, serializer),
                      'metadata': view_model.metadata}

        return view_model, OK

    @user_has_any_tokens('can_read_write_global_all',
                         'can_read_write_public', 'can_read_global_vendor')
    def post(self, vendor_id):

        session = db.session()

        vendor = session.query(models.Vendor).get(vendor_id)
        if vendor is None:
            raise NotFound

        args = parsers.VendorRevenueArgsParser().parse_args()

        try:
            # recheck valid date
            if args['valid_to'] <= args['valid_from']:
                return err_response(status=BAD_REQUEST,
                                    error_code=BAD_REQUEST,
                                    developer_message="Invalid valid to must greater than "
                                                      "valid from",
                                    user_message="valid to invalid")

            if helpers.start_date_overlap(models.VendorRevenueShare, args):
                return err_response(status=BAD_REQUEST,
                                    error_code=BAD_REQUEST,
                                    developer_message="invalid valid from, "
                                                      "still have active revenue",
                                    user_message="This publisher revenue share validity "
                                                 "dates collided with other record validity"
                                                 " dates. Please resolve the collided "
                                                 "dates invalid valid from")

            kwargs = {}
            for item in args:
                if args[item] is None:
                    pass
                else:
                    kwargs[item] = args[item]

            m = models.VendorRevenueShare(**kwargs)
            sv = m.save()  # if this return invalid True raise exceptions
            if sv.get('invalid', False):
                return internal_server_message(modul="patner", method="POST",
                                               error=sv.get('message', ''), req=str(args))
            else:
                rv = m.values()
                return Response(json.dumps(rv), status=CREATED,
                                mimetype='application/json')

        except Exception as e:
            return internal_server_message(modul="vendor revenue share",
                                           method="POST", error=e, req=str(args))


class VendorRevenueApi(controllers.ErrorHandlingApiMixin, Resource):
    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_public',
                         'can_read_global_vendor')
    def get(self, vendor_id, vendor_revenue_id):
        session = db.session()

        vendor = session.query(models.Vendor).get(vendor_id)
        if vendor is None:
            raise NotFound

        try:
            rev_share = filter(lambda entity: entity.id == vendor_revenue_id,
                               vendor.revenue_shares)[0]
        except IndexError:
            raise NotFound

        return marshal(rev_share, serializers.VendorRevenueSerializer()), OK

    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_public',
                         'can_read_global_vendor')
    def put(self, vendor_id, vendor_revenue_id):
        session = db.session()

        vendor = session.query(models.Vendor).get(vendor_id)
        if vendor is None:
            raise NotFound

        args = parsers.VendorRevenueArgsParser().parse_args()

        try:
            # recheck valid date
            valid_from = args['valid_from']
            valid_to = args['valid_to']

            if valid_to <= valid_from:
                return err_response(
                    status=BAD_REQUEST,
                    error_code=BAD_REQUEST,
                    developer_message="Invalid,  "
                                      "valid to must greater than valid from",
                    user_message="Invalid,  "
                                 "valid to must greater than valid from")

            m = filter(lambda entity: entity.id == vendor_revenue_id, vendor.revenue_shares)[0]

            m.valid_to = args['valid_to']
            m.valid_from = args['valid_from']
            m.revenue_id = args['revenue_id']

            sv = m.save()
            if sv.get('invalid', False):
                return internal_server_message(
                    modul="patner", method="PUT",
                    error=sv.get('message', ''), req=str(args))
            else:
                rv = filter(lambda entity: entity.id == vendor_revenue_id,
                            vendor.revenue_shares)[0]
                return marshal(rv, serializers.VendorRevenueSerializer()), OK

        except Exception as e:
            return internal_server_message(
                modul="vendor revenue share", method="PUT",
                error=e, req=str(args))

    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_public',
                         'can_read_global_vendor')
    def delete(self, vendor_id, vendor_revenue_id):
        session = db.session()

        vendor = session.query(models.Vendor).get(vendor_id)
        if vendor is None:
            raise NotFound

        vendor_rev_share = session.query(
            models.VendorRevenueShare).get(vendor_revenue_id)
        if vendor_rev_share is None:
            raise NotFound
        try:
            session.delete(vendor_rev_share)
            session.commit()
        except:
            session.rollback()
            _log.exception("Unable to Delete  Vendor Revenue Share")
            raise

        return "", NO_CONTENT


class BrandMissingEditionApi(Resource):
    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_public',
                         'can_read_global_vendor')
    def get(self):
        from app import db
        args = request.args.to_dict()
        limit, offset = args.get('limit', 10), args.get('offset', 0)
        sql = 'select * from notify_brand limit {limit} offset {offset}'.format(limit=limit, offset=offset)
        sql_count = 'select count(*) from notify_brand'
        brand_data = db.engine.execute(sql)
        data_count = db.engine.execute(sql_count)
        data_count = data_count.fetchall()
        count = data_count[0][0]
        data = brand_data.fetchall()
        result = []
        for brand in data:
            resp = {'brandId': brand[0], 'itemId': brand[1], 'brandName': brand[2], 'organizationId': brand[3],
                    'status': brand[4], 'itemName': brand[5], 'releasePeriod': brand[7]}
            org = Organization.query.get(resp.get('organizationId'))
            resp.update({'organizationName': org.name if org else ''})
            resp.update({'contactEmail': org.contact_email if org else ''})
            result.append(resp)
        missing_edition = {
            'missing_edition': result,
            "metadata": {
                "resultset": {
                    "count": count,
                    "limit": int(limit),
                    "offset": int(offset)
                }
            }
        }
        rv = json.dumps(missing_edition)
        return Response(rv, status=OK, mimetype='application/json')

    def post(self):
        data = json.loads(request.data)
        for brand in data:
            send_email_notify_publiser(brand.get('contactEmail'), brand.get('brandName'))
        return Response(json.dumps(data), status=OK, mimetype='application/json')


class ReportedReviewApi(Resource):
    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_public')
    def get(self):
        session = db.session()
        args = request.args.to_dict()
        limit, offset = args.get('limit', 10), args.get('offset', 0)

        reviews = session.query(Review).join(ReportReview).group_by(Review.id).order_by(
            desc(func.count(ReportReview.id)))

        response = {'reported_reviews': marshal(reviews.limit(limit).offset(offset).all(),
                                                serializers.ReportedReviewSerializer()),
                    'metadata': {
                        "resultset": {
                            "count": reviews.count(),
                            "limit": int(limit),
                            "offset": int(offset)
                        }
                    }}
        return response, OK


class ReportedReviewDetailApi(Resource):
    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_public')
    def get(self, review_id):
        session = db.session()
        review = session.query(Review).get(review_id)
        if review is None:
            raise NotFound
        return marshal(review, serializers.ReportedReviewDetailSerializer()), OK

    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_public')
    def put(self, review_id):
        session = db.session()
        review = session.query(Review).get(review_id)
        if review is None:
            raise NotFound
        args = parsers.ReportReviewArgsParser().parse_args()
        review.is_active = args['is_active']
        session.add(review)
        session.commit()
        return marshal(review, serializers.ReportedReviewDetailSerializer()), OK

class MobilePrefixApi(Resource):

    def get(self, mobile_provider):
        mobile_prefix = []
        if mobile_provider == 'telkomsel':
            mobile_prefix = ['0811', '0812', '0813', '0822', '0821', '0852', '0853']

        response = {'mobile_prefix': mobile_prefix}
        return response, OK
