from app.items.models import Item
from app.master.models import Brand
from app import db

from app.items.choices.item_type import ITEM_TYPES, BOOK, MAGAZINE, NEWSPAPER


class CampaignItems(object):

    def __init__(self, campaign, limit, offset, platform_id, fields='', extends=''):
        self.campaign = campaign
        self.limit = limit
        self.offset = offset
        self.platform_id = platform_id
        self.items = []
        self.fields = fields
        self.extends = extends

    def get_campaign_items(self):
        data_count = 0

        # Hate this 1 campaign can have more than 5 discount with each discount can have 300+ offer inside.
        # SQL ALCHEMY CAN'T HANDLE THIS.. ITS GOING TAKE > 10 SECONDS COS LOT FOR LOOP DATA.. MORE THAN 5 TIMES!!!!
        # TODO: LOOKING FOR ANOTHER METHOD LATER!! WITHOUT USING RAW SQL (DISCOUNTS TO OFFER ITEMS)
        self.sql_set = """
            select id from core_items where id in (
                select
                    DISTINCT core_offers_items.item_id
                from
                    core_discounts
                    join core_discounts_offers on core_discounts.id = core_discounts_offers.discount_id
                    join core_offers_items on core_discounts_offers.offer_id = core_offers_items.offer_id
                where
                    campaign_id = {campaign_id}
            ) order by release_date desc
        """.format(campaign_id=self.campaign.id)
        data_count = db.engine.execute(self.sql_set).rowcount

        # IF DATA COUNT = 0, THIS MEAN CAMPAIGN ONLY HAVE DISCOUNT (SUBS OR BUFFET) NO SINGLE OFFER
        # QUERY MUST BE CHANGE FROM (DISCOUNT --> OFFER ITEMS) TO (DISCOUNTS --> OFFER BRANDS)
        # AND GET THE LATEST ITEMS BY BRANDS.
        if not data_count:
            self.sql_set = """
                select max(id) from core_items where brand_id in (
                    select
                        core_offers_brands.brand_id
                    from
                        core_discounts
                        join core_discounts_offers on core_discounts.id = core_discounts_offers.discount_id
                        join core_offers_brands on core_offers_brands.offer_id = core_discounts_offers.offer_id 
                    where
                        campaign_id = {campaign_id}
                ) group by brand_id
                order by max(id) DESC
            """.format(campaign_id=self.campaign.id)
            data_count = db.engine.execute(self.sql_set).rowcount

        if data_count:
            self.sql_set = self.sql_set + " limit {limit} offset {offset}".format(limit=self.limit, offset=self.offset)
            sql_ids = [id[0] for id in db.engine.execute(self.sql_set).fetchall()]
            item_object = Item.query.filter(Item.id.in_(sql_ids)).all()

            # NEED REFACTOR THIS EXTENDS AND FIELDS!! IS TO BIG FOR NOW TO REFACTOR!,
            # TODO: LOOKING FOR NEW METHOD FOR PYTHON 3.0 MICROSERVICE LATER TO HANDLE THIS EXTENDS AND FIELDS FROM FRONTEND
            if self.fields or self.extends:
                for item_extends in item_object:
                    self.items.append(item_extends.extension_values(extension=self.extends, custom=self.fields, platform_id=self.platform_id))
            else:
                for item_extends in item_object: self.items.append(item_extends.values())

        return data_count, self.items


class OrganizationTypeVendor():

    def __init__(self, vendor_id=None):
        self.vendor_id = vendor_id
        self.type_brand = []

    def filter_brandtype(self):

        brands = Brand.query.filter_by(vendor_id=self.vendor_id).limit(500).all()

        for ibrand in brands:

            if MAGAZINE not in self.type_brand:
                #check magazine
                magazine = Item.query\
                    .filter_by(
                        brand_id=ibrand.id,
                        item_type=ITEM_TYPES[MAGAZINE])\
                    .first()

                if magazine:
                    self.type_brand.append(1)

            if BOOK not in self.type_brand:
                #check book
                book = Item.query\
                    .filter_by(
                        brand_id=ibrand.id,
                        item_type=ITEM_TYPES[BOOK])\
                    .first()

                if book:
                    self.type_brand.append(2)

            if NEWSPAPER not in self.type_brand:
                #check newspapers
                newspaper = Item.query\
                    .filter_by(
                        brand_id=ibrand.id,
                        item_type=ITEM_TYPES[NEWSPAPER])\
                    .first()

                if newspaper:
                    self.type_brand.append(3)
            #break

        return self.type_brand
