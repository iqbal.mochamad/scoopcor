import flask_restful as restful
from flask import Blueprint

from app.master.api import BrandMissingEditionApi, ReportedReviewApi, ReportedReviewDetailApi
from .api import (
    CurrenciesListApi, CurrenciesApi,
    VendorListApi, VendorApi,
    OrganizationVendor, BrandListApi, BrandApi, CategoryListApi, CategoryApi,
    PartnerListApi, PartnerApi, PlatformsListApi, PlatformsApi,
    DeactivateVendor, ActivateVendor, VendorRevenueListApi, VendorRevenueApi, MobilePrefixApi
)

currencies_blueprint = Blueprint('currencies', __name__, url_prefix='/v1/currencies')
currencies_api = restful.Api(currencies_blueprint)
currencies_api.add_resource(CurrenciesListApi, '')
currencies_api.add_resource(CurrenciesApi, '/<int:cur_id>', endpoint='currencies')

vendors_blueprint = Blueprint('vendor', __name__, url_prefix='/v1')
vendors_api = restful.Api(vendors_blueprint)
vendors_api.add_resource(VendorListApi, '/vendors', endpoint='list')
vendors_api.add_resource(VendorApi, '/vendors/<int:vendor_id>', endpoint='details')
vendors_api.add_resource(VendorRevenueApi, '/vendors/<int:vendor_id>/revenue_shares/<int:vendor_revenue_id>')
vendors_api.add_resource(ReportedReviewApi, '/reported-reviews', endpoint='reported-review')
vendors_api.add_resource(ReportedReviewDetailApi, '/reported-reviews/<int:review_id>', endpoint='reported-review-detail')
vendors_api.add_resource(VendorRevenueListApi, '/vendors/<int:vendor_id>/revenue_shares', endpoint='vendor_revenue')
vendors_api.add_resource(DeactivateVendor, '/vendors/<int:vendor_id>/deactivate')
vendors_api.add_resource(ActivateVendor, '/vendors/<int:vendor_id>/activate')
vendors_api.add_resource(OrganizationVendor, '/organizations/<organization_id>/vendor', endpoint='vendor_org')

organizations_blueprint = Blueprint('organizations', __name__, url_prefix='/v1/organizations')
organizations_api = restful.Api(organizations_blueprint)
organizations_api.add_resource(OrganizationVendor, '/<organization_id>/vendor', endpoint='vendor_org')

brands_blueprint = Blueprint('brands', __name__, url_prefix='/v1/brands')
brands_api = restful.Api(brands_blueprint)
brands_api.add_resource(BrandListApi, '', endpoint='list')
brands_api.add_resource(BrandApi, '/<int:brand_id>', endpoint='detail')
brands_api.add_resource(BrandMissingEditionApi, '/missing-edition', endpoint='missing-edition')

categories_blueprint = Blueprint('category', __name__, url_prefix='/v1/categories')
categories_api = restful.Api(categories_blueprint)
categories_api.add_resource(CategoryListApi, '', endpoint='list')
categories_api.add_resource(CategoryApi, '/<int:db_id>', endpoint='details')

partners_blueprint = Blueprint('partners', __name__, url_prefix='/v1/partners')
partners_api = restful.Api(partners_blueprint)
partners_api.add_resource(PartnerListApi, '')
partners_api.add_resource(PartnerApi, '/<int:partner_id>', endpoint='partners')

platforms_blueprint = Blueprint('platforms', __name__, url_prefix='/v1/platforms')
platforms_api = restful.Api(platforms_blueprint)
platforms_api.add_resource(PlatformsListApi, '')
platforms_api.add_resource(PlatformsApi, '/<int:platform_id>', endpoint='details')


mobile_blueprint = Blueprint('mobile-prefix', __name__, url_prefix='/v1/prefix')
mobile_api = restful.Api(mobile_blueprint)
mobile_api.add_resource(MobilePrefixApi, '/<mobile_provider>', endpoint='mobile_prefix')

