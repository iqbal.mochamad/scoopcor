"""
Faspay Configuration
====================

Static configuration data for FasPay.
"""
from app.paymentgateway_connectors.models import (
    PaymentBCAKlikpay, PaymentBRIEpay, PaymentBRIMocash,
    PaymentXLTunai, PaymentCIMBClick
)


def faspay_config():
    pg_config = list()

    pg = dict()
    pg['pg_name'] = "klikpaybca"
    pg['channel'] = 405
    pg['db'] = PaymentBCAKlikpay
    pg['paymentgateway_id'] = 17
    pg_config.append(pg)

    pg = dict()
    pg['pg_name'] = "briepay"
    pg['channel'] = 401
    pg['db'] = PaymentBRIEpay
    pg['paymentgateway_id'] = 18
    pg_config.append(pg)

    pg = dict()
    pg['pg_name'] = "brimocash"
    pg['channel'] = 400
    pg['db'] = PaymentBRIMocash
    pg['paymentgateway_id'] = 19
    pg_config.append(pg)

    pg = dict()
    pg['pg_name'] = "xltunai"
    pg['channel'] = 303
    pg['db'] = PaymentXLTunai
    pg['paymentgateway_id'] = 20
    pg_config.append(pg)

    pg = dict()
    pg['pg_name'] = "cimbclick"
    pg['channel'] = 700
    pg['db'] = PaymentCIMBClick
    pg['paymentgateway_id'] = 21
    pg_config.append(pg)

    return pg_config


FASPAY_CONFIG = faspay_config()

FASPAY_PAYMENT_CH = [400, 401, 405, 303, 700]
ONESTEPTRANSACTION_PAYMENT_CH = [401, 405, 700]
TWOSTEPTRANSACTION_PAYMENT_CH = [400, 303]
