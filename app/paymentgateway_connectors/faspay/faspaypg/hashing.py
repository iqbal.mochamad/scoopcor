"""
Hashing Utilities
=================

Many of the callback APIs and request APIs require that we generate hash
data for a transaction.

This module contains classes that are used to generate this hash data.

"""
from abc import ABCMeta, abstractmethod
import binascii
import hashlib
import math

from Crypto.Cipher import DES3
import six

from app import app

from . import _log


class HashGeneratorBase(object):
    """ Abstract base class for all hash generator classes.
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def generate(self):
        """ Calculates the hash value from the data passed to the constructor.

        :return: The generated hash value.  This is generally a string, however
            there are exceptions, so you'll need to check the return type.
        :rtype: `str`
        """
        pass


class BCAAuthkeyGen(HashGeneratorBase):


    def __init__(self, klikpaycode, trx_no, currency, trx_date, key_id,
                 clear_key):
        """

        :param `int` klikpaycode:
        :param `int` trx_no:
        :param `str` currency: The transaction currency code.  This value
            must always be IDR.
        :param `str` trx_date:
        :param `str` key_id: A unique key for our Faspay merchant account.
        :param `str` clear_key: (Also) A unique key for our Faspay merchant
            account.
        :return:
        """
        self.klikpaycode = klikpaycode
        self.trx_no = trx_no
        self.currency = currency
        self.trx_date = trx_date
        self.key_id = key_id
        self.clear_key = clear_key

        self.klikpay_padder = '0'
        self.trx_no_padder = 'A'
        self.currency_padder = '1'
        self.trx_date_padder = 'C'
        self.key_id_padder = 'E'

    def string_padder(self, var, padder, length):
        while len(var) < length:
            var += padder
        return var

    def first_step(self):
        self.klikpaycode = self.string_padder(
            self.klikpaycode, self.klikpay_padder, 10
        )

        self.trx_no = self.string_padder(
            self.trx_no, self.trx_no_padder, 18
        )

        self.currency = self.string_padder(
            self.currency, self.currency_padder, 5
        )

        self.trx_date = self.string_padder(
            self.trx_date, self.trx_date_padder, 19
        )

        self.key_id = self.string_padder(
            self.key_id, self.key_id_padder, 32
        )

    def second_step(self):
        second_value = self.klikpaycode + self.trx_no + self.currency \
            + self.trx_date + self.key_id

        return second_value

    def third_step(self, second_value):
        hashed_value = hashlib.md5(second_value).hexdigest().upper()
        return hashed_value

    def fourth_step(self, hash_value_1, hash_keyid):
        hex_keyid = binascii.unhexlify(hash_keyid)
        hex_hash_value_1 = binascii.unhexlify(hash_value_1)

        des3 = DES3.new(hex_keyid, DES3.MODE_ECB)
        result_bin = des3.encrypt(hex_hash_value_1)

        result = binascii.hexlify(result_bin).upper()

        return result

    def generate(self):
        """

        :return:
        :rtype: `str`
        """
        self.first_step()
        second_value = self.second_step()
        hash_value_1 = self.third_step(second_value)
        hash_keyid = BCAKeyIdGen(clearkey=self.clear_key).generate()

        result = self.fourth_step(hash_value_1, hash_keyid)

        _log.debug("Generated BCA Authkey: {}".format(result))

        return result


class BCAKeyIdGen(HashGeneratorBase):
    def __init__(self, clearkey):
        self.clearkey = clearkey

    def generate(self):
        keyid = binascii.hexlify(self.clearkey).upper()

        _log.debug("Generated BCA ID: {}".format(keyid))

        return keyid


class BCASignatureGen(HashGeneratorBase):
    """ Generator that creates a unique signature for a BCA transaction.

    See page 5 of the Klikpay BCA documentation  (Section II, Part 2, Subpart b)
    """
    def __init__(self, klikpaycode, keyid, transaction_no=None, currency=None,
                 transaction_date=None, total_amount=None):
        """
        :param `int` klikpaycode: A unique code for the transaction returned
            by klikpay.  ?? make sure this documentation is correct.
        :param `str` keyid: A unique string key for our account. ?? yes?
            double-check that this is correct.
        :param `int` transaction_no: The klikpay transaction number.
        :param `str` currency: The purchase currency code.  This should
            always be IDR.
        :param `str` transaction_date: A date-formatted string as
            dd/mm/yyyy HH:MM:SS.
        :param `float` total_amount: The total purchase amount for the
            transaction.
        :return:
        """
        self.transaction_no = transaction_no
        self.currency = currency.upper()
        self.keyid = keyid
        self.transaction_date = transaction_date
        self.total_amount = total_amount
        self.klikpaycode = klikpaycode

    def _first_step(self):
        """ Performs the first hash generation step using the values provided
        to this class's constructor.

        :return: A two element tuple, containing strings.
        :rtype: `tuple`
        """
        firstvalue = "{klikpaycode}{transactionno}{currency}{keyid}".format(
            klikpaycode=self.klikpaycode,
            transactionno=self.transaction_no,
            currency=self.currency,
            keyid=self.keyid)

        trx_date = self.transaction_date.split(" ")

        if trx_date[0].find("-") != -1:
            splitted_date = trx_date[0].split("-")
            transaction_date = "{2}{1}{0}".format(*splitted_date)
        else:
            splitted_date = trx_date[0].split("/")
            transaction_date = "{0}{1}{2}".format(*splitted_date)

        total_amount = self._int_val_32_bits(int(float(self.total_amount)))
        secondvalue = "%s" % (self._int_val_32_bits(int(transaction_date)) + total_amount)

        return firstvalue, secondvalue

    def _get_hash(self, text):
        """
        Generates an integer hash value based on a variable length of input
        text.  The text must all be within the ASCII range, or ASCII encoded
        or this method will fail.

        :param `six.binary_type` text: Any ASCII encoded string.
        :return: The computed numeric hash.
        :rtype: `int`
        :raises: :py:class:`TypeError`
        """
        result = 0
        for ascii_char in text:
            result = self._int_val_32_bits(self._add_31_times(result) + ord(ascii_char))
        return result

    @staticmethod
    def _int_val_32_bits(value):
        if value > 2147483647:
            value -= 4294967296
        elif value < -2147483648:
            value += 4294967296
        return value

    def _add_31_times(self, value):
        result = 0
        i = 1
        while i <= 31:
            result = self._int_val_32_bits(result + value)
            i += 1
        return result

    def _third_step(self, firstvalue_hash, secondvalue_hash):

        signature = firstvalue_hash + secondvalue_hash
        if signature < 0:
            signature *= -1

        return int(signature)

    def generate(self):
        """ Generates a hash signature for a BCA transaction based on the
        values provided to the class consturctor.

        :return: The final hashed value that has been generated from all 3
            of our hashing steps
        :rtype: `int`
        """
        first_value, second_value = self._first_step()
        # firstvalue_hash = self._second_step(first_value)
        # secondvalue_hash = self._second_step(second_value)
        first_value_hash = self._get_hash(first_value)
        second_value_hash = self._get_hash(second_value)
        signature = self._third_step(first_value_hash, second_value_hash)

        _log.debug("Generated BCA Signature: {}".format(signature))

        return signature


class MISignature(HashGeneratorBase):
    def __init__(self, bill_no):
        self.bill_no = bill_no

    def generate(self):
        faspay_user_id = app.config['FASPAY_USER_ID']
        faspay_password = app.config['FASPAY_PASSWORD']
        raw_signature = "{faspay_user_id}{faspay_password}{bill_no}".format(
            faspay_user_id=faspay_user_id, faspay_password=faspay_password,
            bill_no=self.bill_no)

        signature = hashlib.sha1(
            hashlib.md5(raw_signature).hexdigest()).hexdigest()

        _log.debug("Generated MI Signature: {}".format(signature))

        return signature

