"""
Faspay Payment Gateway Connectors
=================================

This module contains 'gateway' objects, by which our applciation is able
to communicate with the Faspay payment gateway.

**See Also**

- `Gateway Pattern: <http://martinfowler.com/eaaCatalog/gateway.html>`_
"""
from __future__ import unicode_literals

import json

import requests

from . import _log
from .choices import (
    FASPAY_ONESTEP_CHANNELS, FASPAY_TWOSTEP_CHANNELS, PAYMENTCH_KLIKPAY_BCA)
from .hashing import BCAAuthkeyGen, BCASignatureGen


class BaseTransaction(object):
    """ Base class for faspay transactions.
    """
    def __init__(self, sandbox_mode, payment_channel):
        self.sandbox_mode = sandbox_mode
        self.payment_channel = int(payment_channel)

    @property
    def base_url_mi(self):
        if not self.sandbox_mode:
            return "https://faspay.mediaindonusa.com/pws/300002/383xx00010100000"
        else:
            return "http://faspaydev.mediaindonusa.com/pws/100001/382xx00010100000"

    def post_data(self, postdata_req):
        headers = {'Content-Type': 'application/xml'}
        payload = postdata_req.serialize_to_xml()

        _log.debug(u"Faspay Posting Data: {}".format(payload))

        resp = requests.post(
            '{base_url}'.format(base_url=self.base_url_mi),
            headers=headers,
            data=payload)

        _log.debug(u"Faspay Response Data: {}".format(resp.text))

        return resp


class FaspayTransaction(object):
    def __init__(self, payment_channel, sandbox_mode):
        self.payment_channel = payment_channel
        self.sandbox_mode = sandbox_mode

    def factory(self):
        if self.payment_channel in FASPAY_ONESTEP_CHANNELS:
            return OneStepTransactionBase(
                payment_channel=self.payment_channel,
                sandbox_mode=self.sandbox_mode).factory()
        elif self.payment_channel in FASPAY_TWOSTEP_CHANNELS:
            return TwoStepTransaction(
                payment_channel=self.payment_channel,
                sandbox_mode=self.sandbox_mode)
        else:
            raise Exception("Invalid payment channel: {}".format(self.payment_channel))


class OneStepTransactionBase(BaseTransaction):
    """
    This is an object factory, it will return needed OneStepTransaction class
    according to the payment_channel.
    """
    def factory(self):
        if self.payment_channel == PAYMENTCH_KLIKPAY_BCA:
            return OneStepTransactionBCA(self.sandbox_mode, self.payment_channel)
        else:
            return OneStepTransaction(self.sandbox_mode, self.payment_channel)


class OneStepTransaction(OneStepTransactionBase):
    """
    OneStepTransaction class for handling Bank Payment Channel.
    """
    @property
    def base_url_mi_redirect(self):
        if self.sandbox_mode:
            return "http://faspaydev.mediaindonusa.com/pws/100003/0830000010100000"
        else:
            return "https://faspay.mediaindonusa.com/pws/100003/2830000010100000"

    def redirect_post(self, redirect_req):
        redirect_params = redirect_req.serialize()
        redirect_params = "{}{}".format(str(self.base_url_mi_redirect), redirect_params)

        _log.debug("Generated one step  "
                   "redirect params: {}".format(redirect_params))

        return redirect_params


class OneStepTransactionBCA(OneStepTransactionBase):
    """
    OneStepTransaction special class for handling BCA Klikpay payment channel.
    """

    @property
    def base_url_bca(self):
        if self.sandbox_mode:
            return "https://202.6.215.230:8081/purchasing/purchase.do?action=loginRequest"
        else:
            return "https://klikpay.klikbca.com/purchasing/purchase.do?action=loginRequest"

    @staticmethod
    def check_authkeysignature(klikpaycode, trx_id, currency, signature, authkey,
                               trx_date, key_id, amount=None, clear_key=None):
        """
        This function used to check the BCA Authkey and Signature passed from
        Faspay. This function will get data from DB and generate the Authkey /
        Signature. Later the result will be matched with the value we got from
        Faspay.

        :param `str` klikpaycode:
        :param `str` trx_id:
        :param `str` currency:
        :param `str` signature:
        :param `str` auth_key:
        :param `str` trx_date:
        :param `str` key_id:
        :param `int` amount:
        :param `str` clear_key: ???
        """


        if signature:
            generated_signature = BCASignatureGen(
                klikpaycode=klikpaycode,
                transaction_no=trx_id,
                currency=currency,
                transaction_date=trx_date,
                keyid=key_id,
                total_amount=amount).generate()

            return '1' if signature == str(generated_signature) else '0'

        elif authkey:
            generated_authkey = BCAAuthkeyGen(
                klikpaycode=klikpaycode,
                trx_no=trx_id,
                currency=currency,
                trx_date=trx_date,
                key_id=key_id,
                clear_key=clear_key
            ).generate()
            return '1' if authkey == generated_authkey else '0'

        else:
            raise Exception("Neither signature nor authkey provided.  "
                            "Don't know what to check")


    def redirect_post(self, redirect_req):

        json_response = json.dumps({
            'data': redirect_req.serialize(),
            'url': str(self.base_url_bca)
        })

        _log.debug("Generated one step BCA "
                   "redirect params: {}".format(json_response))

        return json_response


class TwoStepTransaction(BaseTransaction):
    """
    TwoStepTransaction for handling mobile payment.
    """
    @property
    def base_url_mi_redirect(self):
        if self.sandbox_mode:
            return "http://faspaydev.mediaindonusa.com/pws/100003/0830000010100000"
        else:
            return "https://faspay.mediaindonusa.com/pws/100003/2830000010100000"

    def redirect_post(self, redirect_req):
        redirect_params = redirect_req.serialize()
        redirect_params = "{}{}".format(str(self.base_url_mi_redirect), redirect_params)

        _log.debug("Generated two step redirect params: {}".format(redirect_params))

        return redirect_params


class StatusInquiry(object):
    def __init__(self, inquiry_req, sandbox_mode):
        self.inquiry_req = inquiry_req
        self.sandbox_mode = sandbox_mode

    @property
    def base_url_mi_inquiry(self):
        if self.sandbox_mode:
            return "http://faspaydev.mediaindonusa.com/pws/100001/183xx00010100000"
        else:
            return "https://faspay.mediaindonusa.com/pws/100004/383xx00010100000"

    def inquire(self):
        headers = {'Content-Type': 'application/xml'}
        payload = self.inquiry_req

        resp = requests.post(
            '{base_url}'.format(base_url=self.base_url_mi_inquiry),
            headers=headers,
            data=payload)

        xml_response = str(resp.text)

        _log.debug("Fetched status inquiry response: {}".format(xml_response))

        return xml_response
