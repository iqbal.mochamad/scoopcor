"""
Helpers
=======

Generic helper utilities that are used within this package.
"""
from __future__ import unicode_literals

from collections import defaultdict


def xml_to_dict(data):
    """ Takes an ElementTree XML document, and returns it's representation
    as a dictionary.

    .. warning:

        This function has no testing and I'm not sure how it works with
        xml documents that represent their data in the form of attributes.

        Do not reuse this method anywhere outside of this module.

    :param `ElementTree` data: An elementtree document.
    :return: A dictionary representing the XML data as a dictionary.
    :rtype: `dict`
    """
    d = {data.tag: {} if data.attrib else None}
    children = list(data)
    if children:
        dd = defaultdict(list)
        for dc in map(xml_to_dict, children):
            for k, v in dc.iteritems():
                dd[k].append(v)
        d = {data.tag: {k:v[0] if len(v) == 1 else v for k, v in dd.iteritems()}}
    if data.attrib:
        d[data.tag].update(('@' + k, v) for k, v in data.attrib.iteritems())
    if data.text:
        text = data.text.strip()
        if children or data.attrib:
            if text:
                d[data.tag]['#text'] = text
        else:
            d[data.tag] = text
    return d


def klikpay_amount_format(amount):
    """ Appends '00' to the end of a string and returns the result.

    :param `str` amount:
    :return: Returns the input string with '00' appended.
    :rtype: str
    """
    formatted_amount = amount + '00'
    return formatted_amount


def klikpay_format_datetime_as_string(dt):
    """ Helper function to convert a klikpay date/time encoded string into a
    python datetime object.

    :param `datetime.datetime` dt: Reference datetime to use when formatting
        a string.
    :return: A string representing a datetime formatted as YYYY/MM/DD HH:MM:SS.
        Note that formatting using 24 hour time for the hours format.
    :rtype: `str`
    """
    return dt.strftime('%d/%m/%Y %H:%M:%S')
