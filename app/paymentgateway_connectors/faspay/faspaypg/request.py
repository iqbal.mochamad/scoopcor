"""
Request Entities
================

Objects that are used to generate the request data that is posted to the
Faspay APIs.
"""
from __future__ import absolute_import, unicode_literals

from xml.etree import ElementTree as ET

from unidecode import unidecode

from . import mixins, validators, _log
from .helpers import xml_to_dict


class Bill(mixins.RequestEntity):
    """
    Information about the purchase Bill.
    """
    _validators = {'bill_no': validators.StringValidator(max_length=32,
                                                         is_required=True),
                   'bill_reff': validators.StringValidator(max_length=32),
                   'bill_date': validators.DummyValidator(),
                   'bill_expired': validators.DummyValidator(),
                   'bill_desc': validators.StringValidator(max_length=128),
                   'bill_currency': validators.StringValidator(max_length=3),
                   'bill_gross': validators.NumericValidator(),
                   'bill_tax': validators.NumericValidator(),
                   'bill_miscfee': validators.NumericValidator(),
                   'bill_total': validators.NumericValidator(),
                   }

    def __init__(self, bill_no, bill_reff, bill_date, bill_expired, bill_desc,
                 bill_currency, bill_gross, bill_tax, bill_miscfee, bill_total):
        """
        Creates a new Bill instance.

        :param first_name: REQUIRED - Maximum 20 characters.
        :param last_name: Maximum 20 characters.
        :param email: REQUIRED - Maximum 45 characters.
        :param phone: REQUIRED - 5-19 characters.
        :param billing_address: Address object instance.
        :param shipping_address: Address object instance.
        """
        self.bill_no = bill_no
        self.bill_reff = bill_reff
        self.bill_date = bill_date
        self.bill_expired = bill_expired
        self.bill_desc = bill_desc
        self.bill_currency = bill_currency
        self.bill_gross = bill_gross
        self.bill_tax = bill_tax
        self.bill_miscfee = bill_miscfee
        self.bill_total = bill_total


class BillAddress(mixins.RequestEntity):
    """
    Information about the purchase Bill.
    """
    _validators = {'billing_address': validators.StringValidator(max_length=32,
                                                         is_required=True),
                   'billing_address_city': validators.StringValidator(max_length=32),
                   'billing_address_region': validators.DummyValidator(),
                   'billing_address_state': validators.DummyValidator(),
                   'billing_address_poscode': validators.StringValidator(max_length=128),
                   'billing_address_country_code': validators.StringValidator(max_length=3)
                   }

    def __init__(self, billing_address, billing_address_city, billing_address_region,
                 billing_address_state, billing_address_poscode, billing_address_country_code):
        """
        Creates a new Bill instance.

        :param `str` first_name: REQUIRED - Maximum 20 characters.
        :param `str` last_name: Maximum 20 characters.
        :param `str` email: REQUIRED - Maximum 45 characters.
        :param `str` phone: REQUIRED - 5-19 characters.
        :param billing_address: Address object instance.
        :param shipping_address: Address object instance.
        """
        self.billing_address = billing_address
        self.billing_address_city = billing_address_city
        self.billing_address_region = billing_address_region
        self.billing_address_state = billing_address_state
        self.billing_address_poscode = billing_address_poscode
        self.billing_address_country_code = billing_address_country_code


class ShippingAddress(mixins.RequestEntity):
    """
    Information about the purchase Bill.
    """
    _validators = {'shipping_address': validators.StringValidator(max_length=32,
                                                         is_required=True),
                   'shipping_address_city': validators.StringValidator(max_length=32),
                   'shipping_address_region': validators.DummyValidator(),
                   'shipping_address_state': validators.DummyValidator(),
                   'shipping_address_poscode': validators.StringValidator(max_length=128)
                   }

    def __init__(self, shipping_address, shipping_address_city, shipping_address_region,
                 shipping_address_state, shipping_address_poscode):
        """
        Creates a new Bill instance.

        :param first_name: REQUIRED - Maximum 20 characters.
        :param last_name: Maximum 20 characters.
        :param email: REQUIRED - Maximum 45 characters.
        :param phone: REQUIRED - 5-19 characters.
        :param billing_address: Address object instance.
        :param shipping_address: Address object instance.
        """
        self.shipping_address = shipping_address
        self.shipping_address_city = shipping_address_city
        self.shipping_address_region = shipping_address_region
        self.shipping_address_state = shipping_address_state
        self.shipping_address_poscode = shipping_address_poscode


class Customer(mixins.RequestEntity):
    """
    Information about the purchase Bill.
    """
    _validators = {
                   # 'cust_no': validators.StringValidator(max_length=32),
                   # 'cust_name': validators.StringValidator(max_length=32),
                   # 'msisdn': validators.DummyValidator(),
                   'email': validators.StringValidator(max_length=128)
                   }

    def __init__(self, email=None):
        """
        Creates a new Bill instance.

        :param first_name: REQUIRED - Maximum 20 characters.
        :param last_name: Maximum 20 characters.
        :param email: REQUIRED - Maximum 45 characters.
        :param phone: REQUIRED - 5-19 characters.
        :param billing_address: Address object instance.
        :param shipping_address: Address object instance.
        """
        # self.cust_no = cust_no
        # self.cust_name = cust_name
        # self.msisdn = msisdn
        self.email = email


class TrxDetails(mixins.RequestEntity):
    """
    Information about the purchase Bill.
    """
    _validators = {'payment_channel': validators.StringValidator(max_length=32,
                                                         is_required=True),
                   'pay_type': validators.StringValidator(max_length=32),
                   # 'bank_userid': validators.StringValidator(max_length=16),
                   'terminal': validators.StringValidator(max_length=2),
                   # 'receiver_name_for_shipping': validators.DummyValidator()
                   }

    def __init__(self, payment_channel, pay_type, terminal):
        """
        Creates a new Bill instance.

        :param first_name: REQUIRED - Maximum 20 characters.
        :param last_name: Maximum 20 characters.
        :param email: REQUIRED - Maximum 45 characters.
        :param phone: REQUIRED - 5-19 characters.
        :param billing_address: Address object instance.
        :param shipping_address: Address object instance.
        """
        self.payment_channel = payment_channel
        self.pay_type = pay_type
        # self.bank_userid = bank_userid
        self.terminal = terminal
        # self.receiver_name_for_shipping = receiver_name_for_shipping


class Items(mixins.RequestEntity):
    """
    Information about the purchase Bill.
    """
    _validators = {'product': validators.StringValidator(max_length=50),
                   'qty': validators.StringValidator(max_length=50),
                   'amount': validators.DummyValidator(),
                   'payment_plan': validators.StringValidator(max_length=1),
                   'merchant_id': validators.DummyValidator(),
                   'tenor': validators.DummyValidator()
                   }

    def __init__(self, product, qty, amount, payment_plan, merchant_id, tenor):
        """
        Creates a new Bill instance.

        :param first_name: REQUIRED - Maximum 20 characters.
        :param last_name: Maximum 20 characters.
        :param email: REQUIRED - Maximum 45 characters.
        :param phone: REQUIRED - 5-19 characters.
        :param billing_address: Address object instance.
        :param shipping_address: Address object instance.
        """
        self.product = product
        self.qty = qty
        self.amount = amount
        self.payment_plan = payment_plan
        self.merchant_id = merchant_id
        self.tenor = tenor

    def serialize(self):
        rv = {}
        for key in self.__dict__:
            if key == 'product':
                disallowed_char = ['&', '<', '>']
                # product = self.product.decode("utf8")
                product = unidecode(self.product)
                self.product = product.translate(
                    None, ''.join(disallowed_char))

            val = getattr(self, key)

            if hasattr(val, 'serialize'):
                rv[key] = val.serialize()
            else:
                rv[key] = val

        return rv


class PostDataRequest(mixins.RequestEntity):
    """ This is specifically used for a 'check transaction status' request,
    but I'm not sure if it's used anywhere else.

    /payments/faspay/check_trx_status
    """
    _validators = {'bill': validators.PassthroughValidator(),
                   'customer': validators.PassthroughValidator(),
                   'trx_details': validators.PassthroughValidator(),
                   'items': validators.PassthroughValidator(),
                   }

    def __init__(self, bill, trx_details, items, merchant, signature, customer=None):
        """ Initializes a new instance of PostDataRequest.

        :param `Bill` bill:
        :param `TrxDetails` trx_details:
        :param `list` items: A list of :class:`Items`
        :param merchant: ??? No idea.
        :param signature: ??? A signature generator classes.  Maybe from hashing?
        :param `Customer` customer:
        """
        self.bill = bill
        self.customer = customer
        self.trx_details = trx_details
        self.items = items
        self.merchant = merchant
        self.signature = signature

    def validate_attr(self, name, value, validator):
        """

        :param name:
        :param value:
        :param validator:
        :return:
        """
        if 'name' == 'items':
            for item in self.item_details:
                item.validate_all()
        else:
            super(PostDataRequest, self).validate_attr(name, value, validator)

    def serialize(self):
        """
        Manually override the standard logic for serialize().  `charge_type`
        needs to add two keys to the resulting dictionary, and all other types
        need to be placed under specific dictionary keys.

        :rtype: dict
        """
        rv = {}
        rv.update(merchantdetail for merchantdetail in self.merchant.iteritems())
        rv.update({'signature': self.signature.generate()})
        rv.update(self.bill.serialize())
        rv.update(self.customer.serialize())
        rv.update(self.trx_details.serialize())
        rv.update({'item': self.items})

        return rv


    def serialize_to_xml(self):
        """ Returns a string-encoded XML document representing this object.

        :return: A string-encoded XML document to send to FASPAY.
        :rtype: basestring
        """
        data_dict = self.serialize()
        xml_data = self._dicttoXML_faspay(data_dict)
        return xml_data


    def _dicttoXML_faspay(self, data):

        root_el = ET.Element("faspay")

        for (tag, child) in data.items():

            if tag == "item":
                for item in child:
                    request_el = ET.SubElement(root_el, tag)
                    # item_element = ET.SubElement(request_el, 'item')
                    prod_ele = ET.SubElement(request_el, 'product')
                    prod_ele.text = item.product

                    payplan_ele = ET.SubElement(request_el, 'payment_plan')
                    payplan_ele.text = item.payment_plan

                    qty_ele = ET.SubElement(request_el, 'qty')
                    qty_ele.text = str(item.qty)

                    amt_ele = ET.SubElement(request_el, 'amount')
                    amt_ele.text = str(item.amount)

                    tenor_ele = ET.SubElement(request_el, 'tenor')
                    tenor_ele.text = item.tenor

                    merch_ele = ET.SubElement(request_el, 'merchant_id')
                    merch_ele.text = str(self.merchant['merchant_id'])

            else:
                request_el = ET.SubElement(root_el, tag)
                request_el.text = child

        resp = ET.tostring(root_el)

        _log.debug("Built XML document Faspay request: \n{}".format(resp))

        return resp



class PostDataResponse(object):
    def __init__(self, postdataxml):
        """

        :param `str` postdataxml: String-encoded XML document received from faspay.
        :return:
        """
        self.postdataxml = postdataxml

        # _log.debug("Initialized PostDataResponse with: \n{}".format(postdataxml))

    def serialize(self):
        """ Turns this object into a dictionary.

        :return: A dictionary that's supposed to represent an xml document.
        :rtype: `dict`
        """
        parsed_data = ET.fromstring(self.postdataxml)
        serialized_data = xml_to_dict(parsed_data)

        return serialized_data


class RedirectPostParams(mixins.RequestEntity):
    _validators = {'transactionNo': validators.PassthroughValidator(),
                   'totalAmount': validators.PassthroughValidator(),
                   'currency': validators.PassthroughValidator(),
                   'payType': validators.PassthroughValidator(),
                   'callback': validators.PassthroughValidator(),
                   'transactionDate': validators.PassthroughValidator(),
                   'descp': validators.PassthroughValidator(),
                   'miscfee': validators.PassthroughValidator()
                   }

    def __init__(self, trx_no, total_amount, currency,  pay_type,
                 callback, trx_date, descp, miscfee):
        self.transactionNo = trx_no
        self.totalAmount = total_amount
        self.currency = currency
        self.payType = pay_type
        self.callback = callback
        self.transactionDate = trx_date
        self.descp = descp
        self.miscFee = miscfee


class RedirectPostRequest(mixins.RequestEntity):
    def __init__(self, trx_id, merchant_id, bill_no, signature):
        self.trx_id = trx_id
        self.merchant_id = merchant_id
        self.bill_no = bill_no
        self.signature = signature

    def serialize(self):
        query_string = "/{}?trx_id={}&merchant_id={}&bill_no={}".\
            format(self.signature, self.trx_id, self.merchant_id, self.bill_no)

        return query_string


class RedirectPostRequestBCA(mixins.RequestEntity):
    def __init__(self, redirectpost_param, klikpaycode, signature):
        self.redirectpost_param = redirectpost_param
        self.klikPayCode = klikpaycode
        self.signature = signature

    def serialize(self):
        """
        Manually override the standard logic for serialize().  `charge_type`
        needs to add two keys to the resulting dictionary, and all other types
        need to be placed under specific dictionary keys.
        """
        rv = {}
        rv.update({'klikPayCode': self.klikPayCode})
        rv.update(self.redirectpost_param.serialize())
        rv.update({'signature': self.signature})

        # param = ""
        # for (tag, child) in rv.iteritems():
        #     param += "&{0}={1}".format(tag, child)

        # xml_data = self._dicttoxml_faspay(rv)
        return rv

    def _dicttoxml_faspay(self, data):
        root_el = ET.Element("InputPaymentIPAY")

        for (tag, child) in data.iteritems():
            # if tag == "approvalCode":
            #     request_el = ET.SubElement(root_el, tag)
            #     for (subtag, subchild) in child.iteritems():
            #         request_subel = ET.SubElement(request_el, subtag)
            #         request_subel.text = subchild
            request_el = ET.SubElement(root_el, tag)
            request_el.text = child

        # request_el = ET.SubElement(root_el, 'additionalData')
        # request_el.set("xsi:nil", "true")

        resp = ET.tostring(root_el)

        return resp


