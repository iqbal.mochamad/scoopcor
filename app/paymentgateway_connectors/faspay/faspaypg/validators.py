from __future__ import unicode_literals


class ValidationError(Exception):
    '''
    Raised whenever a validator in this module determines the value passed
    to .validate() fails validation.
    '''
    pass


class ValidatorBase(object):
    '''
    This should be the absolute base class for all validators.
    It doesn't nothing special, but allows derrived classes to call .validate()
    on super() with reckless abandon!
    '''
    def __init__(self, *args, **kwargs):
        pass

    def validate(self, value):
        '''
        Given a value, raises a ValueError if validation fails, otherwise
        returns None.
        :param value: The object to test for Validation.
        '''


class RequiredValidator(ValidatorBase):
    '''
    Asserts that a value is not null when it's is_required attribute
    is set to 'True'
    '''
    def __init__(self, is_required=True, *args, **kwargs):
        '''
        Creates a new instance of RequiredValidator.
        :param is_required: When True (or not provided), validate()
            will fail on None values.
        '''
        self.is_required = is_required
        super(RequiredValidator, self).__init__(**kwargs)

    def validate(self, value):
        if self.is_required and value is None:
            raise ValidationError("Required value was None")
        super(RequiredValidator, self).validate(value)


class LengthValidator(ValidatorBase):
    '''
    Asserts that a string's length is between a given
    minimum and maximum length.  If no minimum length is given,
    the string may be None.
    '''
    def __init__(self, min_length=None, max_length=None, *args, **kwargs):
        '''
        Creates a new instance of LengthValidator.

        :param min_length: Minimum required length for a string.
        :param max_length: Maximum allowed length for a string.
        '''

        if None not in [min_length, max_length]:
            if max_length < min_length:
                raise ValueError()

        self.min_length = min_length
        self.max_length = max_length

        super(LengthValidator, self).__init__(**kwargs)

    def validate(self, value):

        if self.max_length:
            if value and len(value) > self.max_length:
                raise ValidationError(
                    "{value} longer than max_length"
                    "{max_length}".format(value=value,
                                          max_length=self.max_length))

        if self.min_length:
            if value is None:
                raise ValidationError(
                    "{value} shorter than min_length"
                    "{min_length}".format(value=value,
                                          min_length=self.min_length))

            if len(value) < self.min_length:
                raise ValidationError(
                    "{value} shorter than min_length"
                    "{min_length}".format(value=value,
                                          min_length=self.min_length))

        super(LengthValidator, self).validate(value)


class StringValidator(RequiredValidator, LengthValidator):
    '''
    Takes a string value.  Can optionally required the string to not be
    null by setting is_required to True (it's default),
    greater-than-or-equal to a min_length or
    less-than-or-equal-to a max_length.
    '''
    pass


class NumericValidator(RequiredValidator, ValidatorBase):
    '''
    Tests that the provided value is a python numeric type.
    '''
    def validate(self, value):
        if value is not None:
            if type(value) not in [int, float, long]:
                raise ValidationError("{value} ({type}) is not numeric".format(
                    value=value,
                    type=type(value)))
        super(NumericValidator, self).validate(value)


class DummyValidator(ValidatorBase):
    '''
    This is a special case validator that never fails validation and accepts
    and parameters passed to it's constructor.  Useful for attributes that
    don't contain basic types (), but instead provide their own validation.
    '''
    pass


class PassthroughValidator(RequiredValidator):
    '''
    Allows validation of a subentity type that implements validators
    on it's own properties.  See request.ChargeRequest() for more
    information
    '''
    def validate(self, value):
        if value is not None:
            value.validate_all()
        super(PassthroughValidator, self).validate(value)
