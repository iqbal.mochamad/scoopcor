"""
Exceptions
==========

Specialized exception types that are raised by the Faspay API.
"""
class SignatureKeyMismatch(Exception):
    pass


class AuthKeyMismatch(Exception):
    pass
