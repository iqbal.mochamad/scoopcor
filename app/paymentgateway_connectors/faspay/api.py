"""
Faspay API Callbacks
====================

Controllers that are used for callback endpoints that FasPay uses to send
us notifications (or to verify that purchases on our system are valid).
"""
from __future__ import unicode_literals, absolute_import

import httplib
from httplib import OK
import json

from flask import Response, request, current_app
from flask.views import MethodView
from flask_appsfoundry import Resource
from werkzeug.exceptions import NotFound, UnprocessableEntity

from app.helpers import payment_logs

from . import parsers, models, helpers, _log
from .exceptions import SignatureKeyMismatch, AuthKeyMismatch


# sentinels
_SIG_CHECK_SUCCESS = '1'
_SIG_CHECK_FAILURE = '0'


class FaspayCheckAuthkeySignatureAPI(MethodView):
    """ Validates the payment signature for a given transaction.

    .. info::

        This API is not called directly from our client apps.  This is a
        callback from the FasPay/KlikPay BCA servers.

    This endpoint should always return a HTTP 200 response with a plaintext
    body with a '0' if the checks fail, and a '1' if the checks fail.
    """
    def get(self):
        """ Live validates a given signature of a FasPay transaction vs req args

        This endpoint is a callback for the FasPAY API.  It can do 2
        different jobs -- either validate the FasPay auth key, or it can
        validate a transaction signature.

        :rtype: `tuple` with 3 elements (body, status, headers)
        :returns: Indication on whether an API check is successful.
            If there is an error checking the signature, a plaintext '0'
            must be returned in the http response body.  This method must
            always return an HTTP 200.
        """
        args = parsers.FaspayCheckAuthSignatureArgsParser().parse_args()

        try:
            faspay_transaction = models.PaymentBCAKlikpay.query.filter_by(
                mi_trx_id=args.get('trx_id')
            ).first()

            if not faspay_transaction:
                raise NotFound

            # this method can do either signature verification, or authkey
            # verification.
            validator = helpers.FaspayBCASignatureValidator(
                klikpay_code=current_app.config['KLIKPAYCODE'],
                clear_key=current_app.config['CLEAR_KEY'])

            if 'signature' in args:
                validator.validate_bca_signature(faspay_transaction,
                                                 args['signature'])
            elif 'authkey' in args:
                validator.validate_bca_auth_key(faspay_transaction,
                                                args['authkey'])
            else:
                raise UnprocessableEntity("Request did not contain either "
                                          "'signature' or 'authkey")

            _log.debug("Signature check succeeded")
            return _SIG_CHECK_SUCCESS, OK, {'Content-Type': 'text/plain'}

        except (NotFound, AuthKeyMismatch, SignatureKeyMismatch, UnprocessableEntity):
            # these are normally expected failures.
            _log.exception("Faspay Auth/Signature Failed")
            return _SIG_CHECK_FAILURE, OK, {'Content-Type': 'text/plain'}

        except Exception:
            # something wen't really wrong and we have to raise an error.
            _log.exception("An unexpected problem occurred")
            raise


class FaspayPaymentNotificationAPI(Resource):
    """ Callback API when the user has completed their Payment through FasPay.
    """
    def post(self):
        """ Called by FasPay after the user has completed their payment.

        This lets us know that the user can have their items delivered.
        """
        try:

            _log.debug("Received Faspay Payment Notification")

            payment_rslt = helpers.PaymentNotification(request.data).complete_payment_trigger()

            _log.debug("Processed Faspay Payment Notification")

            return Response(
                payment_rslt,
                mimetype='application/xml'
            )

        except Exception as e:
            _log.exception("Failed Processing Faspay Payment Notification")
            return payment_logs(
                user_message=str(e),
                error_message=str(e),
                args={
                    'api': '/v1/payments/faspay/payment_notification',
                    'method': 'POST'
                })


class FaspayTransactionStatusAPI(Resource):
    """ Callback for FasPay to inquire about the status of an order in Scoop.
    """
    def post(self):
        """ Called by FasPay to notify about the status of an order.
        """
        args = parsers.FaspayStatusArgsParser().parse_args()

        result = helpers.CheckTransactionStatus(
            transaction_id=args.get('trx_id'),
            transaction_no=args.get('bill_no')
        ).check()

        return Response(json.dumps(result),
                        httplib.OK, mimetype='application/json')

