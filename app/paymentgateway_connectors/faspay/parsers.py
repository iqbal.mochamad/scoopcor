from flask_appsfoundry.parsers import ParserBase, StringField, InputField


class FaspayStatusArgsParser(ParserBase):
    """ Args parsers for status inquiries that we're proxying to faspay.
    """
    trx_id = StringField(required=True)
    bill_no = StringField(required=True)


class FaspayCheckAuthSignatureArgsParser(ParserBase):
    """ Args parser for checking the payment signature.
    """
    trx_id = StringField(required=True)
    authkey = StringField(store_missing=False)
    signature = InputField(type=int, store_missing=False)
