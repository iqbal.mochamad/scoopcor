from __future__ import unicode_literals

from datetime import datetime, timedelta
import json
from xml.etree import ElementTree as ET
from six import text_type

from sqlalchemy import desc, or_, exc
from werkzeug.exceptions import NotFound

from app import app
from app.orders.models import Order
from app.payments import payment_status
from app.payments.models import Payment

from . import _log
from .dbmanager.db_manager import DBManager
from .exceptions import SignatureKeyMismatch, AuthKeyMismatch
from .faspay_config import FASPAY_CONFIG as fc
from .faspaypg import faspay, request, response_status
from .faspaypg.choices import PAYMENTCH_KLIKPAY_BCA, DEBIT_CARD, CREDIT_CARD
from .faspaypg.hashing import BCASignatureGen, BCAAuthkeyGen, BCAKeyIdGen, \
    MISignature
from .faspaypg.helpers import klikpay_amount_format, xml_to_dict


class Faspay(object):

    def __init__(
            self, order_id=None, customer=None, payment_channel=None,
            pay_type=None, bank_userid=None, msisdn=None, email=None,
            terminal=None, items=None, payment_id=None, callback=None,
            trx_id=None, bill=None, pg_record_id=None, mi_signature=None,
            db_class=None, order_number=None, paymentgateway_id=None, **kwargs):

        self.bill = bill
        self.customer = customer
        self.payment_channel = payment_channel
        self.pay_type = pay_type
        self.bank_userid = bank_userid
        self.msisdn = msisdn
        self.email = email
        self.terminal = terminal
        self.items = items
        self.callback = callback
        self.payment_id = payment_id
        self.order_id = order_id

        self.trx_id = trx_id
        self.pg_record_id = pg_record_id
        self.mi_signature = mi_signature
        self.sandbox_mode = app.config['FASPAY_SANDBOX_MODE']
        self.db_class = db_class
        self.order_number = order_number
        self.paymentgateway_id = paymentgateway_id


class FaspayCheckout(Faspay):
    def authorize(self):
        """
        Authorizing payment to Faspay.
        """

        try:
            self.db_class = get_db_class(self.payment_channel)


            if self.payment_channel:
                gateway = faspay.FaspayTransaction(
                    sandbox_mode=self.sandbox_mode,
                    payment_channel=int(self.payment_channel)).factory()
            else:
                raise Exception("Payment channel was not set: {}".format(self.payment_channel))

            postdata = PostData(**vars(self))
            postdata.post_data(gateway)

            self.trx_id = postdata.trx_id
            self.pg_record_id = postdata.pg_record_id
            self.mi_signature = postdata.mi_signature
            self.bill = postdata.bill

            redirectpost = RedirectPost(**vars(self))
            resp_redirectpost_data = redirectpost.redirect_post(gateway)

            return resp_redirectpost_data

        except Exception:
            _log.exception("Could not authorize Faspay checkout")
            raise

    def validate(self):
        """

        :return: A tuple of values about the payment status.
        """
        order_number = self.order_number
        order = Order.query.filter_by(id=self.order_id).first()

        for payment_config in fc:
            if payment_config.get('paymentgateway_id') == self.paymentgateway_id:
                self.db_class = payment_config.get('db')
                self.payment_channel = payment_config.get('channel')
                continue

        faspay_trx = self.db_class.query.filter_by(
            payment_id=self.payment_id).first()

        if not order or not faspay_trx:
            raise Exception("Invalid order number: {}".format(order_number))

        if order.order_number is order_number:
            raise Exception("Order number not matched")

        inquiry_status = StatusInquiry(
            trx_id=faspay_trx.mi_trx_id,
            bill_no=faspay_trx.transaction_no,
            signature=faspay_trx.mi_signature
        ).inquire()

        response_code = inquiry_status['faspay'].get('response_code')
        payment_status = inquiry_status['faspay'].get('payment_status_code')
        payment_date = inquiry_status['faspay'].get('payment_date')

        return response_code, payment_status, payment_date


class PostData(Faspay):
    def _build_request_postdata(self):
        order_details = Order.query.filter_by(id=self.order_id).first()
        bill_date = datetime.utcnow() + timedelta(hours=7)
        bill_expired = datetime.utcnow() + timedelta(days=1) + timedelta(hours=7)

        self.bill = dict()
        self.bill['bill_no'] = str(order_details.order_number)
        self.bill['bill_reff'] = str(order_details.order_number)
        self.bill['bill_date'] = str(bill_date)
        self.bill['bill_expired'] = str(bill_expired)
        self.bill['bill_desc'] = "Pembelian di SCOOP"
        self.bill['bill_currency'] = "IDR"
        self.bill['bill_gross'] = klikpay_amount_format(str(int(order_details.final_amount)))
        self.bill['bill_tax'] = "0"
        self.bill['bill_miscfee'] = "0"
        self.bill['bill_total'] = klikpay_amount_format(str(int(order_details.final_amount)))

        bill = request.Bill(**self.bill)

        customer = request.Customer(
            # msisdn=self.msisdn,
            email=self.email)

        trx_details = request.TrxDetails(
            payment_channel=self.payment_channel,
            pay_type=self.pay_type,
            terminal=self.terminal)

        items = [request.Items(
            product=item['name'],
            qty=str(item['quantity']),
            amount=klikpay_amount_format(str(int(item['price']))),
            payment_plan="01",
            merchant_id=str(app.config['FASPAY_MERCHANT_ID']),
            tenor="00") for item in self.items]

        self.mi_signature = MISignature(self.bill['bill_no'])

        merchant_param = {
            "merchant_id": str(app.config['FASPAY_MERCHANT_ID']),
            "merchant": str(app.config['FASPAY_MERCHANT'])}

        req = request.PostDataRequest(
            bill=bill,
            customer=customer,
            trx_details=trx_details,
            items=items,
            merchant=merchant_param,
            signature=self.mi_signature)

        return req

    def save_pre_post_data(self, bill_no, mi_signature, pay_type, req_postdata):
        trx_data = dict()
        trx_data['payment_id'] = self.payment_id
        trx_data['transaction_no'] = str(bill_no)
        trx_data['mi_signature'] = str(mi_signature)
        trx_data['pay_type'] = str(pay_type)
        trx_data['req_postdata'] = str(req_postdata)

        resp = DBManager(db_class=self.db_class, data=trx_data).insert()
        self.pg_record_id = resp.get('id')

    @staticmethod
    def _build_response_mi(resp_param):
        resp = request.PostDataResponse(resp_param.encode('utf-8'))
        return resp

    def save_post_post_data(
            self, response_code, resp_postdata, trx_id, bca_authkey=None):
        trx_data = dict()
        trx_data['mi_trx_id'] = trx_id
        trx_data['response_code'] = response_code
        trx_data['resp_postdata'] = resp_postdata
        trx_data['bca_authkey'] = bca_authkey

        DBManager(
            db_class=self.db_class,
            data=trx_data,
            record_id=self.pg_record_id).update()

    def post_data(self, gateway):
        req_postdata_data = self._build_request_postdata()
        req_postdata_data_dict = req_postdata_data.serialize()

        # TODO: this is one place we grab XML
        self.save_pre_post_data(
            bill_no=req_postdata_data_dict['bill_no'],
            mi_signature=req_postdata_data_dict['signature'],
            pay_type=req_postdata_data_dict['pay_type'],
            req_postdata=req_postdata_data.serialize_to_xml()
        )

        # NOTE: resp_postdata_param is a `requests.Response` object
        resp_postdata_param = gateway.post_data(req_postdata_data)
        resp_postdata_data = self._build_response_mi(resp_postdata_param.text)

        resp_postdata_data = resp_postdata_data.serialize()
        self.trx_id = resp_postdata_data['faspay']['trx_id']
        self.mi_signature = req_postdata_data_dict['signature']

        post_postdata_params = dict()
        post_postdata_params['response_code'] = \
            resp_postdata_data['faspay']['response_code']
        post_postdata_params['resp_postdata'] = \
            str(resp_postdata_data['faspay'])
        post_postdata_params['trx_id'] = \
            self.trx_id

        if int(self.payment_channel) == PAYMENTCH_KLIKPAY_BCA:
            date_conversion = datetime.strptime(
                self.bill['bill_date'], '%Y-%m-%d %H:%M:%S.%f')

            bca_authkey = BCAAuthkeyGen(
                klikpaycode=app.config['KLIKPAYCODE'],
                trx_no=resp_postdata_data['faspay']['trx_id'],
                currency=req_postdata_data_dict['bill_currency'],
                trx_date=str(date_conversion.strftime('%d/%m/%Y %H:%M:%S')),
                # trx_date=req_postdata_data_dict['bill_date'],
                key_id=BCAKeyIdGen(
                    app.config['CLEAR_KEY']).generate(),
                clear_key=app.config['CLEAR_KEY']
            ).generate()
            post_postdata_params['bca_authkey'] = bca_authkey

        self.save_post_post_data(**post_postdata_params)


class RedirectPost(Faspay):
    def _build_request_redirect(self):
        if int(self.payment_channel) == PAYMENTCH_KLIKPAY_BCA:
            date_conversion = datetime.strptime(
                self.bill['bill_date'], '%Y-%m-%d %H:%M:%S.%f')

            self.bill['bill_total'] = "%.2f" % float(
                self.bill['bill_total'].replace(' ', '')[:-2])

            redirectpost_param = request.RedirectPostParams(
                trx_no=self.trx_id,
                total_amount=self.bill['bill_total'],
                currency=self.bill['bill_currency'],
                pay_type=self.pay_type,
                callback="{}?order_number={}&trx_id={}".format(
                    self.callback,
                    self.bill['bill_no'],
                    self.trx_id),
                trx_date=date_conversion.strftime('%d/%m/%Y %H:%M:%S'),
                descp=self.bill['bill_desc'],
                miscfee=self.bill['bill_miscfee']
            )

            redirect_req = request.RedirectPostRequestBCA(
                klikpaycode=app.config['KLIKPAYCODE'],
                redirectpost_param=redirectpost_param,
                signature=BCASignatureGen(
                    transaction_no=self.trx_id,
                    currency=self.bill['bill_currency'],
                    transaction_date=date_conversion.strftime('%d/%m/%Y %H:%M:%S'),
                    total_amount=self.bill['bill_total'],
                    keyid=BCAKeyIdGen(app.config['CLEAR_KEY']).generate(),
                    klikpaycode=app.config['KLIKPAYCODE']
                ).generate()
            )
        else:
            redirect_req = request.RedirectPostRequest(
                trx_id=self.trx_id,
                merchant_id=app.config['FASPAY_MERCHANT_ID'],
                bill_no=self.bill.get('bill_no'),
                signature=self.mi_signature
            )

        return redirect_req

    def redirect_post(self, gateway):
        # Redirect Post to BCA
        req_redirect_post = self._build_request_redirect()
        serialized_req_redirect_post = req_redirect_post.serialize()

        # Save pre-redirect post
        trx_data = dict()
        trx_data['req_redirectdata'] = str(serialized_req_redirect_post)
        trx_data['purchase_date'] = self.bill['bill_date']

        if int(self.payment_channel) == PAYMENTCH_KLIKPAY_BCA:
            trx_data['bca_signature'] = \
                serialized_req_redirect_post['signature']

        DBManager(
            db_class=self.db_class,
            data=trx_data,
            record_id=self.pg_record_id).update()

        resp_redirectpost_data = gateway.redirect_post(req_redirect_post)

        if self.payment_channel == '405':
            resp_redirectpost_data = json.loads(resp_redirectpost_data)

        return resp_redirectpost_data


class PaymentNotification(object):
    def __init__(self, data):
        self.data = data
        self.payment_channel = None
        self.db_class = None

    def complete_payment_trigger(self):
        try:
            _log.debug("Parsing the XML request")
            parsed_data = ET.fromstring(self.data)
            dict_data = xml_to_dict(parsed_data)
            dict_data = dict_data['faspay']
            mi_trx_id = dict_data['trx_id']
            order_number = int(dict_data['bill_no'])

            _log.debug("Looking up order")
            order = Order.query.filter_by(order_number=order_number).first()
            if not order:
                raise Exception("Invalid order number: {}".format(order_number))

            paymentgateway_id = order.paymentgateway_id
            order_id = order.id
            user_id = order.user_id

            _log.debug("Getting FASPAY configuration for "
                       "paymentgateway_id: {}".format(paymentgateway_id))
            for payment in fc:
                if payment.get('paymentgateway_id') == paymentgateway_id:
                    self.db_class = payment.get('db')
                    self.payment_channel = payment.get('channel')

            _log.debug("Looking up faspay transaction id: {}".format(mi_trx_id))
            faspay_trx = self.db_class.query.filter_by(
                mi_trx_id=mi_trx_id).order_by(desc(self.db_class.id)).first()

            if not faspay_trx:
                raise Exception("Invalid trx_id")

            _log.debug("Constructing payment in database")
            from app.payments.helpers import PaymentConstruct
            trx_init = PaymentConstruct({
                'order_number': order_number,
                'order_id': order_id,
                'user_id': user_id,
                'payment_gateway_id': paymentgateway_id,
                'api': 'v1/payments/validate',
                'method': 'POST',
                'payment_id': faspay_trx.payment_id,
            })
            trx_init.initialize_transaction()


            _log.debug("Building XML response")
            xml_response = self.dict_to_xml({
                'response': "Payment Notification",
                'trx_id': str(mi_trx_id),
                'merchant_id': str(app.config['FASPAY_MERCHANT_ID']),
                'bill_no': str(faspay_trx.transaction_no),
                'response_code': response_status.PAYMENT_SUCCESS,
                'response_desc': response_status.RESPONSE_DESC_SUCCESS,
                'response_date': dict_data['payment_date']
            })

            # HORRIBLE HACK -- this is used to determine if a BCA transaction
            # is a credit or a debit card transaction.
            payment_record_data = {
                'req_paymentnotif': str(dict_data),
                'payment_status_code': dict_data['payment_status_code'],
                'resp_paymentnotif': str(xml_response),
            }

            payment_reff = dict_data.get('payment_reff', None)
            if payment_reff is not None:
                payment_record_data.update({
                    'card_type':
                        CREDIT_CARD if payment_reff != 'null' else DEBIT_CARD
                })

            _log.debug("Updating Payment In Database")
            db_manager = DBManager(
                db_class=self.db_class,
                data=payment_record_data,
                record_id=faspay_trx.id
            )
            db_manager.update()

            self.validate_payment(dict_data)

            return xml_response

        except Exception as e:
            _log.exception("An unexpected error occurred while processing the "
                           "Faspay Payment Notification Trigger: {}".format(e))
            raise

    @staticmethod
    def dict_to_xml(data):
        root_el = ET.Element("faspay")

        for (tag, child) in data.iteritems():
            request_el = ET.SubElement(root_el, tag)
            request_el.text = child

        resp = ET.tostring(root_el)

        return resp

    @staticmethod
    def validate_payment(notification_data):
        if int(notification_data['merchant_id']) != \
                app.config['FASPAY_MERCHANT_ID']:
            raise Exception('Invalid merchant_id {} expected {}'.format(notification_data['merchant_id'], app.config['FASPAY_MERCHANT_ID']))

        if notification_data['merchant'] != \
                app.config['FASPAY_MERCHANT']:
            raise Exception('Invalid merchant {} expected {}'.format(notification_data['merchant'], app.config['FASPAY_MERCHANT']))

        return True


class StatusInquiry(object):
    def __init__(self, trx_id, bill_no, signature):
        self.trx_id = trx_id
        self.bill_no = bill_no
        self.signature = signature

    def inquire(self):
        inquire_params = dict()
        inquire_params['request'] = "Inquiry Status Payment"
        inquire_params['trx_id'] = str(self.trx_id)
        inquire_params['merchant_id'] = str(app.config['FASPAY_MERCHANT_ID'])
        inquire_params['bill_no'] = str(self.bill_no)
        inquire_params['signature'] = str(self.signature)

        inquiry_req = self.dict_to_xml(inquire_params)
        inquiry_rslt = faspay.StatusInquiry(
            inquiry_req, app.config['FASPAY_SANDBOX_MODE']).inquire()

        parsed_data = ET.fromstring(inquiry_rslt)
        serialized_data = xml_to_dict(parsed_data)

        return serialized_data

    @staticmethod
    def dict_to_xml(data):
        root_el = ET.Element("faspay")

        for (tag, child) in data.iteritems():
            request_el = ET.SubElement(root_el, tag)
            request_el.text = child

        resp = ET.tostring(root_el)

        return resp


class CheckTransactionStatus(object):

    def __init__(self, transaction_id, transaction_no):
        self.transaction_id = transaction_id
        self.transaction_no = transaction_no

        self.mi_signature = None
        self.db_class = None
        self.payment_channel = None

    def check(self):
        order = Order.query.filter_by(order_number=self.transaction_no).first()

        if not order:
            raise NotFound()

        paymentgateway_id = order.paymentgateway_id
        order_id = order.id
        order_number = order.order_number

        payment = Payment.query.filter_by(order_id=order.id)
        payment = payment.order_by(desc(Payment.id)).first()

        if not payment:
            raise NotFound()

        payment_id = payment.id

        for payment_config in fc:
            if payment_config.get('paymentgateway_id') == paymentgateway_id:
                self.db_class = payment_config.get('db')
                self.payment_channel = payment_config.get('channel')
                continue

        faspay_trx = self.db_class.query.filter_by(
            payment_id=payment_id).first()

        if not faspay_trx:
            raise NotFound()

        faspay_trx_id = faspay_trx.mi_trx_id

        inquiry_status = StatusInquiry(
            trx_id=faspay_trx_id,
            bill_no=faspay_trx.transaction_no,
            signature=faspay_trx.mi_signature
        ).inquire()

        payment_stts = inquiry_status['faspay'].get('payment_status_code')
        payment_date = inquiry_status['faspay'].get('payment_date')

        result = dict()
        result['transaction_status'] = payment_stts
        result['payment_date'] = payment_date
        result['order_number'] = order_number
        result['order_id'] = order_id
        result['payment_gateway_id'] = paymentgateway_id

        return result


def flag_to_expired():
    """ Flags pending payments as cancelled, if older than 48 hours.

    1. Check today's 2 days age faspay transaction with 20001 payment
        status. we need today's date and last 2 days date.
    2. Change the payment status to Payment Cancelled (50000).

    .. warning::

        This method has side-effects in the database.

    """
    cron_result = dict()
    cron_result['success_trx_id'] = list()
    cron_result['failed_trx_id'] = list()

    date_today = datetime.utcnow()
    limit_date = datetime.utcnow() + timedelta(days=2)

    payment = Payment.query
    pending_transaction = payment.filter(
        or_(Payment.payment_status == payment_status.WAITING_FOR_PAYMENT,
            Payment.created >= limit_date,
            Payment.created <= date_today)).all()

    for trx in pending_transaction:
        trx.payment_status = payment_status.CANCELLED
        try:
            trx.save()
            cron_result['success_trx_id'].appends(trx.id)
        except (Exception, exc.SQLAlchemyError):
            cron_result['failed_trx_id'].appends(trx.id)

    return cron_result


class FaspayBCASignatureValidator(object):

    def __init__(self, klikpay_code, clear_key):
        """
        :param `str` klikpay_code:
        :param `str` clear_key:
        """
        self.klikpay_code = klikpay_code
        self.clear_key = clear_key
        self.key_id = BCAKeyIdGen(self.clear_key).generate()

    def validate_bca_signature(self, faspay_transaction, expected_signature):

        _log.debug("Checking BCA signature.")

        generated_signature = BCASignatureGen(
            klikpaycode=self.klikpay_code,
            transaction_no=faspay_transaction.mi_trx_id,
            currency='IDR',
            transaction_date=faspay_transaction.purchase_date.strftime('%d/%m/%Y %H:%M:%S'),
            keyid=self.key_id,
            total_amount=faspay_transaction.payment.amount
        ).generate()

        if str(expected_signature) != str(generated_signature):
            raise SignatureKeyMismatch(
                "Generated Signature ({0}) != expected {1}".format(
                    generated_signature,
                    expected_signature))

    def validate_bca_auth_key(self, faspay_transaction, expected_auth_key):

            generated_authkey = BCAAuthkeyGen(
                klikpaycode=self.klikpay_code,
                trx_no=faspay_transaction.mi_trx_id,
                currency='IDR',
                trx_date=faspay_transaction.purchase_date.strftime('%d/%m/%Y %H:%M:%S'),
                key_id=self.key_id,
                clear_key=self.clear_key
            ).generate()

            if str(expected_auth_key) != str(generated_authkey):
                _log.debug("Generated AuthKey {0} != expected {1}".format(
                        generated_authkey,
                        expected_auth_key))
                raise AuthKeyMismatch(
                    "Generated AuthKey {0} != expected {1}".format(
                        generated_authkey,
                        expected_auth_key))


def save_to_db(db_class, params, action, record_id=None):
    if action == "INSERT":
        rv = db_class(**params)
    elif action == "UPDATE" and record_id is not None:
        rv = db_class.query.filter_by(
            id=record_id).first()

        for (key, value) in params.iteritems():
            # if key in rv.__dict__:
            setattr(rv, key, params.get(key))
    else:
        raise Exception("Bad Paramater")

    result = rv.save()

    if result.get('invalid', None):
        raise Exception(result.get('message', None))

    return result['id']


def get_db_class(payment_channel):
    for payment in fc:
        if payment.get('channel') == int(payment_channel):
            return payment.get('db')
    return None


