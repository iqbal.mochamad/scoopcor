from sqlalchemy.dialects.postgresql import ENUM
from sqlalchemy.orm import deferred

from flask_appsfoundry.models import TimestampMixin
from app import db
from app.paymentgateway_connectors.faspay.faspaypg import choices


class PaymentBCAKlikpay(db.Model, TimestampMixin):
    """ Payment detail information for Faspay BCA Klikpay transactions.
    """

    __tablename__ = 'core_paymentbcaklikpays'

    payment_id = db.Column(db.Integer, db.ForeignKey('core_payments.id'), nullable=False)
    payment = db.relationship("Payment", backref=db.backref('faspay_bca_details', uselist=False))

    transaction_no = db.Column(db.String(50))
    mi_trx_id = db.Column(db.String(16))
    mi_signature = db.Column(db.String(100))
    pay_type = db.Column(db.String(2)) # what the hell are the options here?
    bca_signature = db.Column(db.String(11))
    bca_authkey = db.Column(db.String(32))
    response_code = db.Column(db.String(2))
    payment_status_code = db.Column(db.String(1))
    purchase_date = db.Column(db.DateTime)
    card_type = db.Column(
        ENUM(*choices.PAYMENT_CARD_TYPES, name='payment_card_types'))

    req_postdata = deferred(db.Column(db.Text), group='bcaklikpaydetails')
    resp_postdata = deferred(db.Column(db.Text), group='bcaklikpaydetails')
    req_redirectdata = deferred(db.Column(db.Text), group='bcaklikpaydetails')
    req_paymentnotif = deferred(db.Column(db.Text), group='bcaklikpaydetails')
    resp_paymentnotif = deferred(db.Column(db.Text), group='bcaklikpaydetails')




class PaymentBRIEpay(db.Model, TimestampMixin):

    __tablename__ = 'core_paymentbriepays'

    payment_id = db.Column(db.Integer, db.ForeignKey('core_payments.id'), nullable=False)

    # Main fields
    transaction_no = db.Column(db.String(50))
    mi_trx_id = db.Column(db.String(16))
    mi_signature = db.Column(db.String(100))
    pay_type = db.Column(db.String(2))
    response_code = db.Column(db.String(2))
    payment_status_code = db.Column(db.String(1))

    # Detail fields
    req_postdata = deferred(db.Column(db.Text), group='briepaydetails')
    resp_postdata = deferred(db.Column(db.Text), group='briepaydetails')
    req_redirectdata = deferred(db.Column(db.Text), group='briepaydetails')
    req_paymentnotif = deferred(db.Column(db.Text), group='briepaydetails')
    resp_paymentnotif = deferred(db.Column(db.Text), group='briepaydetails')




class PaymentBRIMocash(db.Model, TimestampMixin):

    __tablename__ = 'core_paymentbrimocashs'

    payment_id = db.Column(db.Integer, db.ForeignKey('core_payments.id'))

    # Main fields
    transaction_no = db.Column(db.String(50))
    mi_trx_id = db.Column(db.String(16))
    mi_signature = db.Column(db.String(100))
    pay_type = db.Column(db.String(2))
    response_code = db.Column(db.String(2))
    payment_status_code = db.Column(db.String(1))

    # Detail fields
    req_postdata = deferred(db.Column(db.Text), group='brimocashdetails')
    resp_postdata = deferred(db.Column(db.Text), group='brimocashdetails')
    req_redirectdata = deferred(db.Column(db.Text), group='brimocashdetails')
    req_paymentnotif = deferred(db.Column(db.Text), group='brimocashdetails')
    resp_paymentnotif = deferred(db.Column(db.Text), group='brimocashdetails')



class PaymentXLTunai(db.Model, TimestampMixin):

    __tablename__ = 'core_paymentxltunais'

    payment_id = db.Column(db.Integer, db.ForeignKey('core_payments.id'))

    # Main fields
    transaction_no = db.Column(db.String(50))
    mi_trx_id = db.Column(db.String(16))
    mi_signature = db.Column(db.String(100))
    pay_type = db.Column(db.String(2))
    response_code = db.Column(db.String(2))
    payment_status_code = db.Column(db.String(1))

    # Detail fields
    req_postdata = deferred(db.Column(db.Text), group='xltunaidetails')
    resp_postdata = deferred(db.Column(db.Text), group='xltunaidetails')
    req_redirectdata = deferred(db.Column(db.Text), group='xltunaidetails')
    req_paymentnotif = deferred(db.Column(db.Text), group='xltunaidetails')
    resp_paymentnotif = deferred(db.Column(db.Text), group='xltunaidetails')


class PaymentCIMBClick(db.Model, TimestampMixin):

    __tablename__ = 'core_paymentcimbclicks'

    payment_id = db.Column(db.Integer, db.ForeignKey('core_payments.id'))

    # Main fields
    transaction_no = db.Column(db.String(50))
    mi_trx_id = db.Column(db.String(16))
    mi_signature = db.Column(db.String(100))
    pay_type = db.Column(db.String(2))
    response_code = db.Column(db.String(2))
    payment_status_code = db.Column(db.String(1))

    # Detail fields
    req_postdata = deferred(db.Column(db.Text), group='cimbclicksdetails')
    resp_postdata = deferred(db.Column(db.Text), group='cimbclicksdetails')
    req_redirectdata = deferred(db.Column(db.Text), group='cimbclicksdetails')
    req_paymentnotif = deferred(db.Column(db.Text), group='cimbclicksdetails')
    resp_paymentnotif = deferred(db.Column(db.Text), group='cimbclicksdetails')
