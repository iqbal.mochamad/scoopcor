from app import db
from sqlalchemy.exc import SQLAlchemyError


class DBManager(object):
    """ Lightweight database abstraction layer used only with FasPay records.
    """
    def __init__(self, db_class, data, record_id=None):
        """
        :param `db.Model` db_class: A sqlalchemy orm model instance.
        :param `dict` data:
        :param `int` record_id: (OPTIONAL) Primary key of model in the database.
            This will be used by non-insert methods to find the database object
            before using.
        """
        self.db_class = db_class
        self.data = data
        self.record_id = record_id

    def insert(self):
        """ Performs a SQL insert with the data provided to the constructor.

        :return: Returns a dictionary with the id of the new database record,
            and a plain-text success message.
        :rtype: `dict` with keys 'id' and 'message'
        :raises: `Exception`
        """
        try:
            data_obj = self.db_class(**self.data)
            db.session.add(data_obj)
            db.session.commit()
            response = {
                'id': data_obj.id,
                'message': "Successfully inserted"
            }
            return response

        except SQLAlchemyError:
            db.session.rollback()
            raise

        finally:
            db.session.close()

    def update(self):
        """ Performs a SQL update with the data provided to the constructor.

        :return: Nothing
        :rtype: `None`
        :raises: `Exception` - all exceptions are raised to the caller.
        """
        try:
            if self.record_id is None:
                raise Exception("Invalid record_id to update")

            data_obj = self.db_class.query.filter_by(id=self.record_id).first()

            if data_obj is None:
                raise Exception("Record id is not found")

            for (key, value) in self.data.iteritems():
                # if key in rv.__dict__:
                setattr(data_obj, key, self.data.get(key))

            db.session.add(data_obj)
            db.session.commit()

        except SQLAlchemyError:
            db.session.rollback()
            raise

        finally:
            db.session.close()

    def delete(self):
        """ Performs a SQL delete with the data provided to the constructor.

        :return: Nothing
        :rtype: `None`
        :raises: `Exception` - all exceptions are raised to the caller.
        """
        try:
            if self.record_id is None:
                raise Exception("Invalid record_id to update")

            data_obj = self.db_class.query.filter_by(id=self.record_id).first()

            if data_obj is None:
                raise Exception("Record id is not found")

            db.session.delete(self)
            db.session.commit()

        except SQLAlchemyError:
            db.session.rollback()
            raise

        finally:
            db.session.close()
