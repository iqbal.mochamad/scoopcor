import hashlib
import requests
import httplib
import json

from sqlalchemy import desc
from app import app, db
from app.paymentgateway_connectors.finnet195.models import PaymentFinnet195, PaymentFinnet195Detail
from app.payments.models import Payment, PaymentDetail, PaymentLog

from flask import abort, Response, jsonify, request, g

from app.helpers import conflict_message, bad_request_message, internal_server_message
from app.helpers import internal_server_message, payment_logs
from app.orders.helpers import order_status


class Finnet195Checkout():
    def __init__(self, merchant_id=None, mer_signature=None, invoice=None,
                 amount=None, email=None, content_id=None, code=None,
                 timeout=None, return_url=None, payment_code=None, request=None,
                 trax_type=None, payer_id=None, result_code=None, result_desc=None,
                 payment_id=None, payment_source=None, log_no=None):

        self.merchant_id = merchant_id
        self.mer_signature = mer_signature
        self.invoice = invoice
        self.amount = amount
        self.email = email
        self.content_id = content_id
        self.code = code
        self.timeout = timeout
        self.return_url = return_url
        self.payment_code = payment_code
        self.trax_type = trax_type
        self.payer_id = payer_id or g.user.username
        self.result_code = result_code
        self.result_desc = result_desc
        self.payment_id = payment_id
        self.payment_source = payment_source
        self.log_no = log_no
        self.merchant_id = app.config['FINNET195_MERCHANT_ID']
        self.return_url = app.config['FINNET195_RETURN_URL']
        self.mer_signature = mer_signature

    def create_new_transaction(self):
        try:
            self.mer_signature = self.create_signature("create")

            initial_result_code = "04"

            transaction_data = {}
            transaction_data['mer_signature'] = self.mer_signature
            transaction_data['user_id'] = str(self.payer_id)
            transaction_data['invoice'] = self.invoice
            transaction_data['amount'] = self.amount
            transaction_data['add_info1'] = str(self.payer_id)
            transaction_data['add_info2'] = self.content_id
            transaction_data['add_info3'] = self.code
            transaction_data['timeout'] = app.config['FINNET195_TIMEOUT']
            transaction_data['return_url'] = self.return_url
            transaction_data['result_code'] = initial_result_code  # default new transaction status (not paid)
            transaction_data['merchant_id'] = self.merchant_id

            trx_finnet = {
                "result_code": initial_result_code,
                "invoice_id": self.invoice,
                "merchant_signature": self.mer_signature,
                "payment_id": self.payment_id
            }
            trx_finnet = PaymentFinnet195(**trx_finnet).save()

            if trx_finnet.get('invalid', False):
                return False, trx_finnet.get('message', '')

            finnet195_url = "%s%s" % (app.config['FINNET195_HOST_URL'], app.config['FINNET195_REQUEST_URL'])
            headers = {
                "Content-type": "application/x-www-form-urlencoded"
            }
            responsefinnet = requests.post(finnet195_url, transaction_data, headers=headers)

            trx_finnet_detail = {
                "paymentfinnet_id": trx_finnet['id'],
                "transaction_request_details": str(transaction_data),
                "transaction_response_details": str(responsefinnet.text),
                "payment_id": self.payment_id
            }

            trx_finnet_detail = PaymentFinnet195Detail(**trx_finnet_detail).save()

            if trx_finnet_detail.get('invalid', False):
                return False, trx_finnet_detail.get('message', '')

            if responsefinnet.status_code == 200:

                payment_code = PaymentFinnet195.query.filter_by(invoice_id=self.invoice).order_by(desc(PaymentFinnet195.id)).first()

                if payment_code.payment_code:
                    return True, payment_code.payment_code

                else:
                    return False, "Invalid payment code"

            else:
                return False, "Payment finnet failed: %s" % responsefinnet.status_code

        except Exception, e:
            return False, str(e)

    def finalize_transaction(self):
        try:
            #check_existing_trx = PaymentFinnet195.query.filter_by(
            #    invoice_id=self.invoice).order_by(desc(PaymentFinnet195.id)).first()
            #
            #if len(check_existing_trx) == 0:
            transaction = PaymentFinnet195.query.filter_by(invoice_id=self.invoice, payment_code=None).first()

            if transaction is not None:
                transaction.payment_code = self.payment_code
                trx = transaction.save()

                if trx.get('invalid', False):
                    return False, trx.get('message', '')
                else:
                    return True, "00"
            else:
                return False, "Payment finnet not found"
            #else:  # duplicate found
            #    check_existing_trx.delete()
            #return True, "00"

        except Exception, e:
            return False, str(e)

    def transaction_notification(self):
        mer_signature = self.mer_signature
        trax_type = self.trax_type
        merchant_id = self.merchant_id
        payment_source = self.payment_source
        log_no = self.log_no
        self.return_url = app.config['FINNET195_RETURN_URL']

        # result_code 00 -- has been paid by customer
        # result_code 05 -- transaction expired
        try:
            transaction = PaymentFinnet195.query.filter_by(
                invoice_id=self.invoice,
                payment_code=self.payment_code,
                result_code="04"
            ).first()

            if transaction:
                #if mer_signature == transaction.merchant_signature and \
                #        self.payment_code == transaction.payment_code:
                if self.payment_code == transaction.payment_code:

                    if self.result_code == "00":  # and check_transaction_code == "00":
                        transaction.result_code = self.result_code
                        transaction.result_desc = self.result_desc
                        trx = transaction.save()

                        if trx.get('invalid', False):
                            return False, trx.get('message', '')

                        check_transaction_status, check_transaction_code = self.check_transaction()

                        if not check_transaction_status:
                            return False, "Cannot validate transaction"

                        return True, "00"

                    elif self.result_code == "05":  # and check_transaction_code == "05":
                        CANCELLED = 50000

                        payment = Payment.query.filter_by(
                            order_id=self.invoice,
                        ).order_by(desc(Payment.id)).first()

                        if payment:
                            payment.payment_status = CANCELLED
                            payment.save()
                        else:
                            return False, payment_logs(args=self.args, error_message="payment_id not found")

                        os = order_status(order_id=self.invoice, order_status=CANCELLED)

                        if os != 'OK':
                            return payment_logs(args=self.args, error_message="Order id is not found")

                        transaction = PaymentFinnet195.query.filter_by(
                            invoice_id=self.invoice,
                            payment_code=self.payment_code,
                            result_code="04"
                        ).first()

                        transaction.result_code = self.result_code
                        transaction.result_desc = self.result_desc
                        trx = transaction.save()

                        if trx.get('invalid', False):
                            return False, trx.get('message', '')

                        #expired transaction
                        return True, "00"

                    else:
                        return False, "Transaction failed (fraud warning)"

                else:
                    return False, "Invalid transaction, payment code not matched"
            else:
                False, "Transaction not found"

        except Exception, e:
            return False, str(e)

    def create_signature(self, trx_type=None):
        if trx_type == "create":
            signature = "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s" % (
                self.merchant_id, "%",
                self.invoice, "%",
                self.amount, "%",
                self.payer_id, "%",
                self.content_id, "%",
                self.code, "%",
                app.config['FINNET195_TIMEOUT'], "%",
                self.return_url
            )
        elif trx_type == "check":
            signature = "%s%s%s%s%s" % (
                self.merchant_id, "%",
                self.payment_code, "%",
                self.return_url
            )

        signature = signature.upper()
        signature = "%s%s%s" % (signature, "%", app.config['FINNET195_PASSWORD'])
        signature = hashlib.sha256(signature)
        signature = signature.hexdigest()

        return signature

    def check_transaction(self):
        mer_signature = self.create_signature("check")
        transaction_data = {
            'mer_signature': mer_signature,
            'merchant_id': app.config['FINNET195_MERCHANT_ID'],
            'payment_code': self.payment_code,
            'return_url': self.return_url
        }

        finnet195_check_payment_url = "%s%s" % (app.config['FINNET195_HOST_URL'],
                                                app.config['FINNET195_CHECK_STATUS_URL'])

        response = requests.post(finnet195_check_payment_url, transaction_data)

        if response.status_code == 200:
            return True, str(response.text)
        else:
            return False, response.status_code

    def validation_notification(self):
        from app.payments.helpers import PaymentConstruct

        api = 'v1/payments/validate'
        payment_gateway_id = 5

        args_finnet = {
            "api": api,
            "payment_gateway_id": payment_gateway_id,
            "finnet_result_code": self.result_code,
        }

        try:
            order = PaymentFinnet195.query.filter_by(
                payment_code=self.payment_code
            ).order_by(desc(PaymentFinnet195.id)).first()

            order_id = order.values().get('invoice_id', None)
            payment_id = order.values().get('payment_id', None)

            args_finnet['order_id'] = order_id
            args_finnet['payment_id'] = payment_id

            finnet_trx = PaymentConstruct(args=args_finnet).initialize_transaction()

            return '00'
        except Exception, e:
            payment_logs(args=args_finnet, error_message=str(e))
            return '99'
