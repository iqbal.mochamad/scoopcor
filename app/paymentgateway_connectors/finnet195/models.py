from flask_appsfoundry.models import TimestampMixin

from app import db


class PaymentFinnet195(db.Model, TimestampMixin):
    __tablename__ = 'core_paymentfinnets'

    id = db.Column(db.Integer, primary_key=True)
    payment_id = (db.Column(db.Integer, db.ForeignKey('core_payments.id')))
    invoice_id = db.Column(db.BigInteger)

    payment_code = db.Column(db.String(20))
    result_code = db.Column(db.String(5))
    merchant_signature = db.Column(db.String(64))

    def __repr__(self):
        return '<id : %s >' % self.payment_code

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully added", 'id': self.id}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['payment_id'] = self.payment_id
        rv['payment_code'] = self.payment_code
        rv['result_code'] = self.result_code
        rv['invoice_id'] = self.invoice_id
        rv['merchant_signature'] = self.merchant_signature

        return rv


class PaymentFinnet195Detail(db.Model, TimestampMixin):
    __tablename__ = 'core_paymentfinnetdetails'

    id = db.Column(db.Integer, primary_key=True)
    paymentfinnet_id = db.Column(db.Integer, db.ForeignKey('core_paymentfinnets.id'), nullable=False)
    payment_id = db.Column(db.Integer)

    transaction_request_details = db.Column(db.Text)
    transaction_response_details = db.Column(db.Text)

    def __repr__(self):
        return '<id : %s >' % self.id

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully added"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['paymentfinnet_id'] = self.paymentfinnet_id
        rv['payment_id'] = payment_id = self.payment_id
        rv['transaction_request_details'] = self.transaction_request_details
        rv['transaction_response_details'] = self.transaction_response_details

        return rv
