import ast
import json
import httplib

from flask_restful import Resource, Api, reqparse
from flask_restful import Api, reqparse
from flask import abort, Response, jsonify, request

from app import app, db
from app.constants import *
from app.auth.decorators import token_required

from app.helpers import log_api, assertion_error, message_json
from app.helpers import dtserializer, err_response, date_validator
from app.helpers import conflict_message, bad_request_message, internal_server_message


from app.paymentgateway_connectors.finnet195.finnet195 import Finnet195Checkout
from app.helpers import internal_server_message, payment_logs


class FinnetPaymentGatewayAPI(Resource):
    def __init__(self):
        self.data = request

    def post(self):
        try:
            post_data = self.data.form.to_dict()
            trax_type = post_data.get("trax_type", 'unknown').lower()

            if trax_type == "195code":
                mer_signature = post_data.get("mer_signature", None)
                merchant_id = post_data.get("merchant_id", None)
                invoice = post_data.get("invoice", None)
                payment_code = post_data.get("payment_code", None)

                finnettrx_status, finnettrx_rslt = Finnet195Checkout(
                    mer_signature=mer_signature,
                    trax_type=trax_type,
                    merchant_id=merchant_id,
                    invoice=invoice,
                    payment_code=payment_code
                ).finalize_transaction()

                if finnettrx_status:
                    return Response(
                        finnettrx_rslt,
                        status=httplib.OK,
                        mimetype='application/json'
                    )
                else:
                    return payment_logs(
                        args=post_data,
                        error_message=finnettrx_rslt
                    )

            elif trax_type == "payment":
                mer_signature = post_data.get("mer_signature", None)
                merchant_id = post_data.get("merchant_id", None)
                invoice = post_data.get("invoice", None)
                payment_code = post_data.get("payment_code", None)
                result_code = post_data.get("result_code", None)
                result_desc = post_data.get("result_desc", None)
                log_no = post_data.get("log_no", None)
                payment_source = post_data.get("payment_source", None)

                finnettrx_status, finnettrx_rslt = Finnet195Checkout(
                    mer_signature=mer_signature,
                    result_code=result_code,
                    merchant_id=merchant_id,
                    invoice=invoice,
                    payment_code=payment_code,
                    result_desc=result_desc,
                    log_no=log_no,
                    payment_source=payment_source,
                    trax_type=trax_type
                ).transaction_notification()

                if finnettrx_status:
                    return Response(
                        finnettrx_rslt,
                        status=httplib.OK,
                        mimetype='application/json'
                    )
                else:
                    return payment_logs(
                        args=post_data,
                        error_message=finnettrx_rslt
                    )

            elif trax_type == "195status":
                mer_signature = post_data.get("mer_signature", None)
                merchant_id = post_data.get("merchant_id", None)
                payment_code = post_data.get("payment_code", None)
                result_code = post_data.get("result_code", None)
                result_desc = post_data.get("result_desc", None)

                finnettrx = Finnet195Checkout(
                    mer_signature=mer_signature,
                    result_code=result_code,
                    merchant_id=merchant_id,
                    payment_code=payment_code,
                    result_desc=result_desc,
                    trax_type=trax_type
                ).validation_notification()

                return finnettrx

            else:
                return Response(
                    "99",
                    status=httplib.OK,
                    mimetype='application/json'
                )

        except Exception, e:
            return payment_logs(
                args=post_data,
                error_message=str(e)
            )



