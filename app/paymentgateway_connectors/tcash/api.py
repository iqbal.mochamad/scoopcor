import ast
import json
import httplib

from flask_restful import Resource, Api, reqparse
from flask_restful import Api, reqparse
from flask import abort, Response, jsonify, request

from app import app, db
from app.constants import *
from app.auth.decorators import token_required

from app.helpers import log_api, assertion_error, message_json
from app.helpers import dtserializer, err_response, date_validator
from app.helpers import conflict_message, bad_request_message, internal_server_message, payment_logs

from app.paymentgateway_connectors.tcash.tcash import TcashCheckout, TcashPayments


class TcashPaymentGatewayAPI(Resource):
    def __init__(self):
        self.data = request

        super(TcashPaymentGatewayAPI, self).__init__()

    def post(self):
        try:
            post_data = self.data.form.to_dict()

            post_data['api'] = "Tcash"
            post_data['method'] = "POST"
            print "========================== TcashPaymentGatewayAPI", post_data

            tcash_trx_status, tcash_status_code, trx_id, msg = TcashCheckout(post_data=post_data).validate()

            if tcash_trx_status:
                #tcash_msg = post_data.get('msg', None)
                #tcash_msg = tcash_msg.split("#")
                #order_number = tcash_msg[1]

                #msg = "Nomor pesanan %s. Silahkan download pesanan Anda di aplikasi SCOOP." % (str(order_number))
                json_form = "%s:%s:%s" % (tcash_status_code, trx_id, msg)
            else:
                json_form = "%s:%s" % (tcash_status_code, msg)

            return Response(
                json_form,
                status=httplib.OK,
                mimetype='application/html'
            )

        except Exception, e:
            print "==================== ERRORRR TcashPaymentGatewayAPI", e

            msg = "Transaksi gagal karena kesalahan teknis. Silahkan hubungi customer suppport Scoop."
            payment_logs(
                args=post_data,
                error_message=str(e)
            )
            json_form = "99:%s" % (trx_id, msg)

            return Response(
                json.dumps(json_form),
                status=httplib.OK,
                mimetype='application/json'
            )


class PaymentTcashApi(Resource):
    ''' Shows a list of Pending Trx and do GET '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        super(PaymentTcashApi, self).__init__()

    @token_required('can_read_write_global_all')
    def get(self, tcash_column=None, tcash_value=None):
        try:
            param = request.args.to_dict()
            param['api'] = '/v1/payments/tcash'
            param['method'] = 'GET'
            param[tcash_column] = tcash_value

            scene = TcashPayments(param, 'tcash').construct()
            return scene
        except Exception, e:
            return payment_logs(args=param, error_message=str(e))
