from app import db
from flask_appsfoundry.models import TimestampMixin


class PaymentTcash(db.Model, TimestampMixin):
    __tablename__ = 'core_paymenttcashs'

    id = db.Column(db.Integer, primary_key=True)
    payment_id = (db.Column(db.Integer, db.ForeignKey('core_payments.id')))
    transaction_id = db.Column(db.Integer, nullable=False)

    tcash_trx_id = db.Column(db.String(256))
    msisdn = db.Column(db.String(16))
    trx_type = db.Column(db.String(3))
    tcash_status = db.Column(db.String(2))

    def __repr__(self):
        return '<id : %s>' % (self.id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Payment successfully added", 'id': self.id}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Payment successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['payment_id'] = self.payment_id
        rv['transaction_id'] = self.payment_id
        rv['tcash_trx_id'] = self.tcash_trx_id
        rv['trx_type'] = self.trx_type
        rv['msisdn'] = self.msisdn
        rv['tcash_status'] = self.tcash_status

        return rv


class PaymentTcashDetail(db.Model, TimestampMixin):
    __tablename__ = 'core_paymenttcashdetails'

    id = db.Column(db.Integer, primary_key=True)
    paymenttcash_id = (db.Column(db.Integer, db.ForeignKey('core_paymenttcashs.id'), nullable=False))
    payment_id = db.Column(db.Integer, nullable=False)

    #tcash_sms = db.Column(db.Text())
    validate_request = db.Column(db.Text)
    validate_response = db.Column(db.Text)

    def __repr__(self):
        return '<id : %s>' % (self.id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Payment successfully added", 'id': self.id}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Payment successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['paymenttcash_id'] = self.paymenttcash_id
        rv['payment_id'] = self.payment_id
        #rv['tcash_sms'] = self.tcash_sms
        rv['validate_request'] = self.validate_request
        rv['validate_response'] = self.validate_response

        return rv

