import requests
import base64
import json
import httplib

from flask import abort, Response, jsonify, request

from sqlalchemy import func, distinct
from sqlalchemy.sql import and_, or_, desc

from app import app, db
from app.paymentgateway_connectors.tcash.models import PaymentTcash, PaymentTcashDetail
from app.payments.models import Payment, PaymentDetail, PaymentLog

from app.orders.models import Order, OrderLine, OrderLineDiscount, OrderDetail
from app.offers.models import Offer, OfferPlatform
from app.items.models import Item, UserItem, UserSubscription

from app.helpers import internal_server_message, payment_logs

from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME

DIRECT_PAYMENT_REQUEST = '026'
REVERSAL_PAYMENT_REQUEST = '027'


TCASH_SUCCESS = "00"
TCASH_CREDIT_NOT_ENOUGH = "11"
TCASH_RECORD_NOT_FOUND = "22"
TCASH_NEW_TRANSACTION = "05"
TCASH_ERROR = "99"


class TcashCheckout():
    def __init__(self, order_number=None, amount=None, payment_id=None, post_data=None, transaction_id=None,
                 tcash_status_code=None):

        self.order_number = order_number
        self.amount = amount
        self.payment_id = payment_id
        self.post_data = post_data
        self.transaction_id = transaction_id
        self.tcash_status_code = tcash_status_code
        self.trx_date = ''

    def create_new_transaction(self):
        tcash_trx = {
            "payment_id": self.payment_id,
            "transaction_id": self.transaction_id,
            "tcash_status": TCASH_NEW_TRANSACTION
        }
        pt = PaymentTcash(**tcash_trx).save()

        if pt.get('invalid', False):
            return False, pt.get('message', '')

        tcash_trx_detail = {
            "paymenttcash_id": pt.get('id', None),
            "payment_id": self.payment_id
        }

        pvd = PaymentTcashDetail(**tcash_trx_detail).save()
        if pvd.get('invalid', False):
            return False, pvd.get('message', '')

        return True, 'OK'

    def validate(self):
        try:
            merchant = self.post_data.get('merchant', None)
            terminal = self.post_data.get('terminalname', None)
            pwd = self.post_data.get('pwd', None)
            self.trx_date = self.post_data.get('trx_date', None)
            trx_type = self.post_data.get('trx_type', DIRECT_PAYMENT_REQUEST)
            msisdn = self.post_data.get('msisdn', None)
            msg = self.post_data.get('msg', None)
            amount = int(self.post_data.get('amount', None))
            trx_id = self.post_data.get('trx_id', None)

            message = msg.split("#")
            self.order_number = message[1]
            self.payment_id = message[0]

            if trx_type == DIRECT_PAYMENT_REQUEST:
                tcash_trx = PaymentTcash.query.filter_by(
                    payment_id=int(self.payment_id)
                ).first()

                tcash_trx_det = PaymentTcashDetail.query.filter_by(
                    payment_id=int(self.payment_id)
                ).first()
                print "================== tcash trx", tcash_trx, tcash_trx_det

                if tcash_trx and tcash_trx_det:
                    if not self.check_payment_amount(tcash_amount=amount):
                        msg = "Transaksi gagal karena kesalahan teknis. Silahkan hubungi customer suppport Scoop."
                        self.update_tcash_status(TCASH_CREDIT_NOT_ENOUGH)
                        return False, TCASH_CREDIT_NOT_ENOUGH, trx_id, msg

                    tcash_trx.trx_type = trx_type
                    tcash_trx.tcash_trx_id = trx_id
                    tcash_trx.msisdn = msisdn
                    tcash_trx.tcash_status = TCASH_SUCCESS
                    tc = tcash_trx.save()
                    if tc.get('invalid', False):
                        msg = "Transaksi gagal karena kesalahan teknis. Silahkan hubungi customer suppport Scoop."
                        payment_logs(
                            args=self.post_data,
                            error_message=str(tc.get('message', None))
                        )
                        self.update_tcash_status(TCASH_ERROR)
                        return False, TCASH_ERROR, trx_id, msg

                    json_form = "%s:%s:%s" % (TCASH_SUCCESS, trx_id, trx_type)

                    tcash_trx_det.validate_request = str(self.post_data)
                    tcash_trx_det.validate_response = json_form
                    tcd = tcash_trx_det.save()
                    if tcd.get('invalid', False):
                        msg = "Transaksi gagal karena kesalahan teknis. Silahkan hubungi customer suppport Scoop."
                        payment_logs(
                            args=self.post_data,
                            error_message=str(tc.get('message', None))
                        )
                        self.update_tcash_status(TCASH_ERROR)
                        return False, TCASH_ERROR, trx_id, msg

                    self.tcash_status_code = TCASH_SUCCESS

                    order_id = Order.query.filter_by(
                        order_number=int(self.order_number)
                    ).first()

                    if not order_id:
                        return False, TCASH_RECORD_NOT_FOUND, trx_id, trx_type
                    else:
                        self.order_id = order_id.values().get('order_id')

                    self.payment_tcash_validate()

                    msg = "Nomor pesanan %s. Silahkan download pesanan Anda di aplikasi SCOOP." % (str(self.order_number))
                    return True, TCASH_SUCCESS, trx_id, msg

                else:
                    msg = "Transaksi gagal karena kesalahan teknis. Silahkan hubungi customer suppport Scoop."
                    payment_logs(
                        args=self.post_data,
                        error_message="Tcash record not found, invalid message"
                    )
                    #self.update_tcash_status(TCASH_RECORD_NOT_FOUND)
                    return False, TCASH_RECORD_NOT_FOUND, trx_id, msg

            else:
                msg = "Transaksi gagal karena kesalahan teknis. Silahkan hubungi customer suppport Scoop."
                payment_logs(
                    args=self.post_data,
                    error_message=str(e)
                )
                self.update_tcash_status(TCASH_ERROR)
                return False, TCASH_ERROR, trx_id, msg

        except Exception, e:
            print "======================== ERROR tcash validate", e
            payment_logs(
                args=self.post_data,
                error_message=str(e)
            )
            msg = "Transaksi gagal karena kesalahan teknis. Silahkan hubungi customer suppport Scoop."
            return False, TCASH_ERROR, trx_id, msg

    def check_payment_amount(self, tcash_amount):
        payment = Payment.query.filter_by(
            id=int(self.payment_id)
        ).first()

        if not payment:
            return False

        amount = payment.values().get('amount', 0)

        if int(amount) == int(tcash_amount):
            return True
        else:
            return False

    def update_tcash_status(self, status):
        tcash_trx = PaymentTcash.query.filter_by(
            payment_id=int(self.payment_id)
        ).first()

        if tcash_trx:
            tcash_trx.tcash_status = status
            tc = tcash_trx.save()

            if tc.get('invalid', False):
                payment_logs(
                    args=self.post_data,
                    error_message=str(tc.get('message', None))
                )
                return False
            else:
                return True
        else:
            return False

    def payment_tcash_validate(self):
        from app.payments.helpers import PaymentConstruct

        api = 'v1/payments/validate'
        payment_gateway_id = 6

        args_tcash = {
            "api": api,
            "payment_gateway_id": payment_gateway_id,
            "order_id": self.order_id,
            "payment_id": self.payment_id,
            "tcash_status_code": self.tcash_status_code,
            "payment_datetime": self.trx_date
        }

        tcash_trx = PaymentConstruct(args=args_tcash).initialize_transaction()


class TcashPayments():
    def __init__(self, param=None, module=None):
        self.module = module
        self.param = param
        self.tcash_trx_id = param.get("tcash_trx_id", None)
        self.limit = param.get("limit", None)
        self.offset = param.get("offset", None)
        self.tcash_transactions = []
        self.order_id = None
        self.master = ''

    def json_success(self, data_tcash=None, offset=None, limit=None):

        if self.total <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            for transaction in data_tcash:
                try:
                    tcash_data = {}
                    tcash_data['tcash_trx_id'] = transaction.values().get('tcash_trx_id', None)
                    tcash_data['msisdn'] = transaction.values().get('msisdn', None)
                    tcash_data['transaction_id'] = transaction.values().get('transaction_id', None)
                    tcash_data['trx_type'] = transaction.values().get('trx_type', None)
                    tcash_data['tcash_status'] = transaction.values().get('tcash_status', None)

                    payment_status, order_id, user_id = self.get_tcash_payment_status(transaction.values().get('payment_id', None))
                    tcash_data['user_id'] = user_id
                    tcash_data['payment_id'] = transaction.values().get('payment_id', None)
                    tcash_data['payment_status'] = payment_status
                    self.order_id = order_id
                    tcash_data['order_id'] = order_id
                    #if self.expand == "items":
                    tcash_data['orderlines'] = self.get_items()

                    self.tcash_transactions.append(tcash_data)
                except Exception, e:
                    payment_logs(args=self.param, error_message=str(e))

            temp = {}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}

            rv = json.dumps({self.module: self.tcash_transactions, "metadata": tempx})
            return Response(rv, status=httplib.OK, mimetype='application/json')

    def get_tcash_payment_status(self, payment_id):
        payment_status = None
        order_id = None

        payment = Payment.query.filter_by(
            id=payment_id
        ).first()

        if payment.values().get('payment_status', None) is not None:
            payment_status = payment.values().get('payment_status', None)

        if payment.values().get('order_id', None) is not None:
            order_id = payment.values().get('order_id', None)

        if payment.values().get('user_id', None) is not None:
            user_id = payment.values().get('user_id', None)

        return payment_status, order_id, user_id

    def get_items(self):
        orderlines = self.order_master().get_order_line()

        items = []
        for dataline in orderlines:
            temp = {}
            temp['items'] = self.json_items(dataline)
            items.append(temp)

        return items

    def json_items(self, orderline=None):
        data_item = []

        offer = orderline.get_offer()
        if offer:
            if offer.offer_type_id == 2:
                brands = offer.get_brands_id()

                for ibrands in brands:
                    item = {}

                    item_brands = Item.query.filter_by(brand_id=ibrands, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).order_by(
                        desc(Item.release_date)).limit(1).all()

                    if item_brands:
                        ms_it = item_brands[0]
                        item['id'] = ms_it.id
                        item['name'] = ms_it.name
                        item['edition_code'] = ms_it.edition_code
                        item['item_status'] = {v:k for k,v in STATUS_TYPES.items()}[ms_it.item_status]
                        item['release_date'] = ms_it.release_date.isoformat()
                        item['is_featured'] = ms_it.is_featured
                        item['vendor'] = ms_it.get_vendor()
                        item['languages'] = ms_it.get_languages()
                        item['countries'] = ms_it.get_country()
                        item['authors'] = ms_it.get_authors()
                        data_item.append(item)

            else:
                for xo in offer.get_items():
                    item = {}
                    ms_it = self.get_master_item(xo)
                    item['name'] = ms_it.name
                    item['id'] = ms_it.id
                    item['edition_code'] = ms_it.edition_code
                    item['item_status'] = {v:k for k,v in STATUS_TYPES.items()}[ms_it.item_status]
                    item['release_date'] = ms_it.release_date.isoformat()
                    item['is_featured'] = ms_it.is_featured
                    item['vendor'] = ms_it.get_vendor()
                    item['languages'] = ms_it.get_languages()
                    item['countries'] = ms_it.get_country()
                    item['authors'] = ms_it.get_authors()
                    data_item.append(item)

        return data_item

    def get_master_item(self, item_id=None):
        """
            Get ITEM MASTER DATA
        """
        data_item = None

        #ITEM READY FOR CONSUME
        item = Item.query.filter_by(id=item_id, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).first()
        if item:
            data_item = item
        return data_item

    def order_master(self):
        order = Order.query.filter_by(id=self.order_id).first()
        if not order:
            payment_logs(args=self.param, error_message="invalid order id")
        return order

    def tcash_master(self):
        return PaymentTcash.query

    def construct(self):
        self.master = self.tcash_master()

        if self.tcash_trx_id:
            self.master = self.master.filter_by(tcash_trx_id=self.tcash_trx_id)

        if self.limit:
            self.master.limit(self.limit)

        if self.offset:
            self.master.offset(self.offset)

        self.master = self.master.all()
        self.total = len(self.master)

        return self.json_success(self.master, self.offset, self.limit)
