NEW = 1
PROCESSING = 2
ERROR = 3
COMPLETE = 4
EXPIRED = 5
CANCEL = 6

RENEWAL_STATUS = {
    NEW : 'new',
    PROCESSING : 'processing',
    ERROR : 'error',
    COMPLETE : 'complete',
    EXPIRED : 'expired',
    CANCEL : 'cancel'
}
