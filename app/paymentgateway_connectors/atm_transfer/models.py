from sqlalchemy.dialects.postgresql import ENUM, JSONB
from flask_appsfoundry.models import TimestampMixin

from app import db
from app.paymentgateway_connectors.atm_transfer.midtrans_status import BANK_ACCOUNTS, MIDTRANS_STATUS


class PaymentVirtualAccount(db.Model, TimestampMixin):
    __tablename__ = 'core_paymentvirtualaccounts'

    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('core_orders.id'), nullable=False)
    order = db.relationship('Order')
    virtual_number = db.Column(db.String(150), nullable=False)
    bank_account = db.Column(ENUM(*BANK_ACCOUNTS.values(), name='bank_account'), nullable=False)
    status = db.Column(ENUM(*MIDTRANS_STATUS.values(), name='status'), nullable=False)
    data_request = db.Column(JSONB)
    data_response = db.Column(JSONB)
