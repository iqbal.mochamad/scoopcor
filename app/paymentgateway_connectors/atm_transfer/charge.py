import requests, json, base64, httplib

from flask import Response
from sqlalchemy import desc
from datetime import datetime
from unidecode import unidecode

from app.helpers import err_response
from app.orders.models import Order, OrderLine
from app.paymentgateway_connectors.atm_transfer.midtrans_status import *
from app.paymentgateway_connectors.atm_transfer.models import PaymentVirtualAccount
from app.paymentgateway_connectors.helpers import authorize_formatting
from app.payments.choices import WAITING_FOR_PAYMENT
from app.payments.choices.gateways import VA_PERMATA, VA_BCA, VA_MANDIRI, VA_BNI, PAYMENT_GATEWAYS
from app.payments.models import Payment
from app.users.users import User
from app import app, db


MIDTRANS_HEADER = {
    'Content-Type':'application/json',
    'Accept':'application/json',
    'Authorization':base64.b64encode(app.config['MIDTRANS_SERVER_KEY'])
}

class MidtransVirtualAccount():

    def __init__(self, order_id):
        self.order_id = order_id
        self.data_va = None

    def va_permata_request(self):
        return {
                "payment_type": "bank_transfer",
                "bank_transfer":
                    {
                        "bank": "permata",
                        "permata": {
                            "recipient_name": "{}".format(self.user.email)
                        }
                    },
                "transaction_details": {
                    "order_id": self.order.id,
                    "gross_amount": int(self.order.final_amount)
                }
            }

    def va_bca_request(self):
        return {}

    def va_mandiri_request(self):
        orderline = OrderLine.query.filter_by(order_id=self.order.id).order_by(desc(OrderLine.id)).all()

        return {
            "payment_type": "echannel",
            "transaction_details": {
                "order_id": str(self.order.id),
                "gross_amount": int(self.order.final_amount)
            },
            "item_details": [{
                "id": str(iorder.id),
                "price": int(iorder.final_price),
                "quantity": iorder.quantity,
                "name": "{}".format(unidecode(unicode(iorder.name))[:30])
            } for iorder in orderline],
            "echannel": {
                "bill_info1": "Pembelian Ebook Gramedia Digital",
                "bill_info2": str(iorder.order.order_number)
                }
            }

    def va_bni_request(self):
        orderline = OrderLine.query.filter_by(order_id=self.order.id).order_by(desc(OrderLine.id)).all()

        return {
            "payment_type": "bank_transfer",
            "transaction_details": {
                "gross_amount": int(self.order.final_amount),
                "order_id": str(self.order.id)
            },
            "customer_details": {
                "email": "{}".format(self.user.email),
                "first_name": "{}".format(self.user.first_name if self.user.first_name else ''),
                "last_name": "{}".format(self.user.last_name if self.user.last_name else '')
            },
            "item_details": [{
                "id": str(iorder.id),
                "price": int(iorder.final_price),
                "quantity": iorder.quantity,
                "name": "{}".format(unidecode(unicode(iorder.name))[:30])
            } for iorder in orderline],
           "bank_transfer":{
             "bank": "bni"
          }
        }

    def create_payment(self):
        exist_payment = Payment.query.filter_by(order_id=self.order.id).first()
        if not exist_payment:
            new_payment = Payment(
                order_id=self.order.id,
                user_id=self.order.user_id,
                paymentgateway_id=self.order.paymentgateway_id,
                currency_code=self.order.currency_code,
                amount=self.order.final_amount,
                payment_status=WAITING_FOR_PAYMENT,
                is_active=True,
                is_test_payment=True if app.config['DEBUG'] else False,
                is_trial=False,
                payment_datetime=datetime.now(),
                financial_archive_date=None,
                merchant_params=None
            )
            db.session.add(new_payment)
            db.session.commit()

    def create_virtual_data(self, data_request, data_response):
        exist_virtual_acc = PaymentVirtualAccount.query.filter_by(order_id=self.order.id).first()

        if not exist_virtual_acc:
            data_virtual_acc = PaymentVirtualAccount(
                order_id=self.order.id,
                bank_account=PAYMENT_GATEWAYS[self.order.paymentgateway_id],
                status=STATUS_PENDING,
                data_request=data_request,
                data_response=data_response
            )
            db.session.add(data_virtual_acc)
            db.session.commit()
            self.data_va = data_virtual_acc

    def update_virtualnumber(self, virtual_number):
        self.data_va.virtual_number = virtual_number
        db.session.add(self.data_va)
        db.session.commit()

    def charge_midtrans(self):
        data = {}
        if self.order.paymentgateway_id == VA_PERMATA: data = self.va_permata_request()
        if self.order.paymentgateway_id == VA_BCA: data = self.va_bca_request()
        if self.order.paymentgateway_id == VA_MANDIRI: data = self.va_mandiri_request()
        if self.order.paymentgateway_id == VA_BNI: data = self.va_bni_request()

        try:
            # charge midtrans api charge
            charge = requests.post(app.config['MIDTRANS_API_CHARGE'], json.dumps(data), headers=MIDTRANS_HEADER)
            midtrans_response = charge.json()

            # create log
            self.create_payment()
            self.create_virtual_data(data, midtrans_response)

            # if midtrans_response:
            if charge.status_code in [httplib.OK, httplib.CREATED]:
                fraud_status = midtrans_response.get('fraud_status', '')
                transaction_status = midtrans_response.get('transaction_status', '')

                if fraud_status == 'accept' and transaction_status == STATUS_PENDING:
                    data_response = authorize_formatting()

                    if self.order.paymentgateway_id == VA_PERMATA:
                        data_response['virtual_account_number'] = midtrans_response.get('permata_va_number', None)
                        data_response['bank'] = PERMATA
                        self.update_virtualnumber(virtual_number=midtrans_response.get('permata_va_number', None))

                    if self.order.paymentgateway_id == VA_BCA:
                        data_response = {}

                    if self.order.paymentgateway_id == VA_MANDIRI:
                        virtual_account_number = "{}{}".format(midtrans_response.get('biller_code', ''),
                            midtrans_response.get('bill_key', ''))
                        data_response['virtual_account_number'] = virtual_account_number
                        data_response['bank'] = MANDIRI
                        self.update_virtualnumber(virtual_number=virtual_account_number)

                    if self.order.paymentgateway_id == VA_BNI:
                        virtual_account_data = midtrans_response.get('va_numbers', [])[0]
                        if virtual_account_data:
                            data_response['virtual_account_number'] = virtual_account_data.get('va_number', None)
                            data_response['bank'] = BNI
                            self.update_virtualnumber(virtual_number=virtual_account_data.get('va_number', None))
                        else:
                            data_response['virtual_account_number'] = 0
                            data_response['bank'] = BNI
                    return True, data_response
                else:
                    return False, midtrans_response
            else:
                return False, 'Cant reach midtrans API {}'.format(midtrans_response)

        except Exception as e:
            return False, 'Internal Error {}'.format(e)

    def authorize(self):
        self.order = Order.query.filter_by(id=self.order_id).first()
        self.user = User.query.filter_by(id=self.order.user_id).first()

        # charge midtrans
        status, response = self.charge_midtrans()

        if status:
            return Response(json.dumps(response), status=httplib.OK, mimetype='application/json')
        else:
            return err_response(status=httplib.BAD_REQUEST, error_code=httplib.BAD_REQUEST,
                developer_message=response, user_message="Payment Invalid.")
