import requests, json, base64, httplib
from flask import Response

from app.orders.choices import COMPLETE
from app.orders.order_complete import OrderCompleteConstruct
from app.paymentgateway_connectors.atm_transfer.midtrans_status import *
from app import app, db
from app.paymentgateway_connectors.atm_transfer.models import PaymentVirtualAccount
from app.payments.choices import PAYMENT_CANCELLED, PAYMENT_BILLED, PAYMENT_IN_PROCESS
from app.payments.models import Payment

MIDTRANS_HEADER = {
    'Content-Type':'application/json',
    'Accept':'application/json',
    'Authorization':base64.b64encode(app.config['MIDTRANS_SERVER_KEY'])
}

class VirtualAccountProcess():

    def __init__(self, request_data, data_order):
        self.request_data = request_data
        self.data_order = data_order

    def re_validate_order(self):
        api = "{}/v2/{}/status".format(app.config['MIDTRANS_API'], self.data_order.id)
        request_data = requests.get(api, headers=MIDTRANS_HEADER)

        if request_data.status_code in [httplib.OK, httplib.CREATED]:
            midtrans_json = request_data.json()
            transaction_status = midtrans_json.get('transaction_status', '')
            fraud_status = self.request_data.get('fraud_status', None)

            if str(transaction_status) == str(STATUS_SETTLE): #and fraud_status == FRAUD_ACCEPT:
                return True, STATUS_SETTLE
            return False, transaction_status
        else:
            raise Response(json.dumps({'status': 'Midtrans API unreachable!'}),
                status=httplib.BAD_REQUEST, mimetype='application/json')

    def update_status(self, status, payment_status):
        data_virtual = PaymentVirtualAccount.query.filter_by(order_id=self.data_order.id).first()
        if data_virtual:
            data_virtual.status = status
            data_virtual.order.order_status = COMPLETE if payment_status == PAYMENT_BILLED else payment_status
            db.session.add(data_virtual)
            db.session.commit()

        payment = Payment.query.filter_by(order_id=self.data_order.id).first()
        if payment:
            payment.payment_status = payment_status
            db.session.add(payment)
            db.session.commit()

    def validate(self):
        fraud_status = self.request_data.get('fraud_status', None)
        transaction_status = self.request_data.get('transaction_status', None)

        if transaction_status == STATUS_SETTLE:
            status, order_status = self.re_validate_order()

            if status:
                OrderCompleteConstruct(
                    user_id=self.data_order.user_id,
                    payment_gateway_id=self.data_order.paymentgateway_id,
                    order_id=self.data_order.id,
                    order_number=self.data_order.order_number).construct()
                self.update_status(STATUS_SETTLE, PAYMENT_BILLED)
                return True, 'Transaction Success.'
            else:
                if order_status == STATUS_EXPIRED:
                    self.update_status(STATUS_EXPIRED, PAYMENT_CANCELLED)
                if order_status == STATUS_PENDING:
                    self.update_status(STATUS_PENDING, PAYMENT_IN_PROCESS)
                return False, 'Record are {}'.format(order_status)

        else:
            return False, 'Record not settle.!!'

    def complete(self):
        return self.validate()
