SUCCESS = '200'
PENDING = '201'

ERROR_STATUS = {
    400 : "Validation Error, merchant sent bad request data example; validation error, invalid transaction type, invalid credit card format, etc.",
    401 : "Access denied due to unauthorized transaction, please check client key or server key",
    402 : "Merchant doesn't have access for this payment type",
    406 : "Duplicate order ID. Order ID has already been utilized previously",
    410 : "Merchant account is deactivated. Please contact Midtrans support"
}


STATUS_PENDING = 'pending'
STATUS_EXPIRED = 'expire'
STATUS_DENIED = 'deny'
STATUS_CANCEL = 'cancel'
STATUS_REFUND = 'refund'
STATUS_CAPTURE = 'capture'
STATUS_SETTLE = 'settlement'

MIDTRANS_STATUS = {
    STATUS_PENDING:'pending',
    STATUS_EXPIRED:'expire',
    STATUS_DENIED:'deny',
    STATUS_CANCEL:'cancel',
    STATUS_REFUND:'refund',
    STATUS_CAPTURE:'capture',
    STATUS_SETTLE:'settlement'
}


PERMATA='permata'
BCA='bca'
MANDIRI='mandiri'
BNI='bni'

BANK_ACCOUNTS = {
    PERMATA: 'permata',
    BCA: 'bca',
    MANDIRI: 'mandiri',
    BNI: 'bni',
}


FRAUD_ACCEPT = 'accept'
FRAUD_DENY = 'deny'
