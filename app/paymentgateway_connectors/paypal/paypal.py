import paypalrestsdk
import json

from app import app
from app import constants as HTTPCode
from app.helpers import conflict_message, bad_request_message, internal_server_message
from app.paymentgateway_connectors.paypal.models import PaymentPaypal, PaymentPaypalDetail
from app.helpers import internal_server_message, payment_logs


class PaypalCheckout():

    def __init__(self, payment_code=None, payer_id=None, amount=None,
                 currency=None, return_url=None, cancel_url=None, items=None,
                 user_id=None, order_payment_id=None, transaction_id=None,
                 payment_id=None, args=None):

        self.payment_code = payment_code
        self.payer_id = payer_id
        self.amount = amount
        self.currency = currency
        self.return_url = return_url
        self.cancel_url = cancel_url
        self.items = items
        self.user_id = user_id
        self.order_payment_id = order_payment_id
        self.transaction_id = transaction_id
        self.paypal_configuration = None
        self.payment_id = payment_id
        self.args = args
        self.payment = None

    def authorize(self):
        try:
            if self.items is None:
                return False, "Item is empty"

            item_counter = 0
            while len(self.items) != item_counter:
                if self.items[item_counter]['is_free'] is True:
                    self.items.remove(self.items[item_counter])
                    item_counter = item_counter
                else:
                    del self.items[item_counter]['is_free']
                    item_counter = item_counter + 1

            if self.paypal_configuration is None:
                self.paypal_configuration = paypalrestsdk.configure({
                    'mode': app.config['PAYPAL_MODE'],
                    'client_id': app.config['PAYPAL_CLIENT_ID'],
                    'client_secret': app.config['PAYPAL_SECRET']
                })

            payment_request = self.create_payment_request()
            payment = paypalrestsdk.Payment(payment_request)

            payment_result = payment.create()

            payment_logs(
                args=str(payment_request),
                error_message=str(payment.error)
            )

            if not payment_result:
                return False, payment_logs(
                    args=self.args,
                    error_message=str(payment.error)
                )

            approval_url = payment['links'][1]['href']
            self.payment_code = str(payment['id'])
            payment_status = str(payment['state'])

            paypal_trx = {
                "payment_code": self.payment_code,
                "status": payment_status,
                "transaction_id": int(self.transaction_id),
                "payment_id": self.payment_id
                #transaction id currently not supported by paypal REST
            }

            paypal_transaction = PaymentPaypal(**paypal_trx)
            transaction = paypal_transaction.save()
            paypal_id = transaction['id']

            if transaction.get('invalid', False):
                return False, payment_logs(
                    args=self.args,
                    error_message=str(transaction.get('message', ''))
                )

            paypal_trx_detail = {
                "paymentpaypal_id": paypal_id,
                "authorize_response": str(payment),
                "payment_id": self.payment_id
            }

            paypal_transaction_detail = PaymentPaypalDetail(**paypal_trx_detail)
            transaction_detail = paypal_transaction_detail.save()
            paypal_detail_id = transaction_detail['id']

            if transaction_detail.get('invalid', False):
                return False, payment_logs(
                    args=self.args,
                    error_message=str(transaction_detail.get('message', ''))
                )

            authorize_response = {
                "status": payment_status,
                "approval_url": approval_url,
                "payment_code": self.payment_code,
                "paypal_id": paypal_id,
                "paypal_detail_id": paypal_detail_id
            }

            return True, authorize_response

        except Exception, e:
            return False, payment_logs(
                args=self.args,
                error_message=str(e)
            )

    def capture(self):
        try:
            if self.paypal_configuration is None:
                self.paypal_configuration = paypalrestsdk.configure({
                    'mode': app.config['PAYPAL_MODE'],
                    'client_id': app.config['PAYPAL_CLIENT_ID'],
                    'client_secret': app.config['PAYPAL_SECRET']
                })

            self.payment = paypalrestsdk.Payment.find(self.payment_code)

            if not self.payment.execute({"payer_id": self.payer_id}):
                return False, payment_logs(
                    args=self.args,
                    error_message=str(self.payment.error)
                )

            transaction = PaymentPaypal.query.filter_by(payment_code=self.payment_code).first()

            if transaction is None:
                return False, payment_logs(
                    args=self.args,
                    error_message=str("Payment code not found")
                )

            transaction.status = self.payment['state']
            sv = transaction.save()

            if sv.get('invalid', False):
                return False, sv.get('message', '')

            paypal_detail_transaction = PaymentPaypalDetail.query.filter_by(paymentpaypal_id=sv.get('id', None)).first()
            paypal_detail_transaction.capture_response = str(self.payment)
            pdt = paypal_detail_transaction.save()
            if pdt.get('invalid', False):
                return False, pdt.get('message', '')

            #response = payment['state']
            response = self.payment
            return True, response

        except Exception, e:
            return False, payment_logs(
                args=self.args,
                error_message=str(e)
            )

    def create_payment_request(self):
        payment_request = {
                "intent": "sale",
                "payer": {
                  "payment_method": "paypal" },
                "redirect_urls": {
                  "return_url": self.return_url,
                  "cancel_url": self.cancel_url },
                "transactions": [ {
                  "amount": {
                    "total": "%.2f" % self.amount,
                    "currency": self.currency },
                  "description": "creating a payment",
                  "item_list":{
                    "items": self.items
                    }
                }]}

        return payment_request
