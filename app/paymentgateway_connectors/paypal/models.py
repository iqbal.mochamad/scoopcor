from app import db
from flask_appsfoundry.models import TimestampMixin


class PaymentPaypal(db.Model, TimestampMixin):
    __tablename__ = 'core_paymentpaypals'

    id = db.Column(db.Integer, primary_key=True)
    payment_id = db.Column(db.Integer, db.ForeignKey('core_payments.id'))

    payment_code = db.Column(db.String(250), nullable=False)
    transaction_id = db.Column(db.Integer, nullable=False)
    status = db.Column(db.String(50), nullable=False)

    def __repr__(self):
        return '<id : %s payment_id: %d>' % (self.id, self.payment_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Payment successfully added", 'id': self.id}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Payment successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['payment_id'] = self.payment_id
        rv['payment_code'] = self.payment_code
        rv['transaction_id'] = self.transaction_id
        rv['status'] = self.status

        return rv


class PaymentPaypalDetail(db.Model, TimestampMixin):
    __tablename__ = 'core_paymentpaypaldetails'

    id = db.Column(db.Integer, primary_key=True)
    paymentpaypal_id = (db.Column(db.Integer, db.ForeignKey('core_paymentpaypals.id')))
    payment_id = db.Column(db.Integer)

    authorize_response = db.Column(db.Text)
    capture_response = db.Column(db.Text)

    def __repr__(self):
        return '<id : %s paymentpaypal_id: %s>' % (self.id, self.paymentpaypal_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Payment successfully added", 'id': self.id}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Payment successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['paymentpaypal_id'] = self.paymentpaypal_id
        rv['payment_id'] = self.payment_id
        rv['authorize_response'] = self.authorize_response
        rv['capture_response'] = self.capture_response

        return rv
