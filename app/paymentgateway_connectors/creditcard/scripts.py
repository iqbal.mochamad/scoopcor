import base64, requests, httplib

from sqlalchemy import desc
from app.payments.models import PaymentToken
from app.paymentgateway_connectors.creditcard.models import PaymentCreditCardToken
from datetime import datetime
from app import app, db


MIDTRANS_HEADER = {
    'Content-Type':'application/json',
    'Accept':'application/json',
    'Authorization':base64.b64encode(app.config['MIDTRANS_SERVER_KEY'])
}

def extract_bin_cc(limit=20, offset=0):

    valid_token = PaymentToken.query.filter(PaymentToken.expiration >= datetime.now()).order_by(
        desc(PaymentToken.id)).limit(limit).offset(offset).all()

    try:
        for data_cc in valid_token:
            exists_cc_token = PaymentCreditCardToken.query.filter_by(
                user_id = data_cc.user_id, masked_credit_card = data_cc.masked_credit_card).first()

            if not exists_cc_token:
                exists_cc_token = PaymentCreditCardToken(
                    user_id=data_cc.user_id,
                    masked_credit_card=data_cc.masked_credit_card,
                    billing_address=data_cc.billing_address,
                    expiration=data_cc.expiration,
                    first_name=data_cc.first_name,
                    last_name=data_cc.last_name,
                    is_active=True
                )
                db.session.add(exists_cc_token)
                db.session.commit()
                print 'PaymentCreditCardToken created.'

                data_masked_card = data_cc.masked_credit_card.split('-')[0]
                bin_url = '{}/{}'.format(app.config['MIDTRANS_API_BIN'], data_masked_card)
                bin_data = requests.get(bin_url, headers=MIDTRANS_HEADER)

                if bin_data.status_code in [httplib.OK, httplib.CREATED]:
                    bin_response = bin_data.json().get('data', None)

                    if bin_response:
                        exists_cc_token.bin_bank = bin_response.get('bank', None)
                        exists_cc_token.bin_bank_code = bin_response.get('bank_code', None)
                        exists_cc_token.bin_class = bin_response.get('bin_class', None)
                        exists_cc_token.bin_types = bin_response.get('bin_type', None)
                        exists_cc_token.bin_brand = bin_response.get('brand', None)
                        exists_cc_token.bin_country_code = bin_response.get('country_code', None)
                        db.session.add(exists_cc_token)
                        db.session.commit()
                        print 'CC Bin Updated.'
            else:
                pass
                print 'Cc Token already exists user_id: {} masked {}'.format(data_cc.user_id, data_cc.masked_credit_card)

    except Exception as e:
        print 'Extract cc token Failed {}'.format(e)

