import requests, json, base64, httplib

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from sqlalchemy import desc
from unidecode import unidecode

from app.offers.choices.quantity_unit import DAILY, WEEKLY, MONTHLY, UNIT
from app.orders.choices import COMPLETE
from app.orders.models import OrderLine
from app.orders.order_complete import OrderCompleteConstruct
from app.paymentgateway_connectors.atm_transfer.midtrans_status import *
from app.paymentgateway_connectors.creditcard.models import PaymentCreditCard, PaymentCreditCardToken, PaymentCreditCardRenewal
from app.payments.models import Payment
from app.payments.choices import PAYMENT_IN_PROCESS, PAYMENT_BILLED
from app.paymentgateway_connectors.choices.renewal_status import NEW, RENEWAL_STATUS
from app.offers.choices.offer_type import SUBSCRIPTIONS, BUFFET

from app.helpers import err_response
from app.users.users import User

from app import app, db

MIDTRANS_HEADER = {
    'Content-Type':'application/json',
    'Accept':'application/json',
    'Authorization':base64.b64encode(app.config['MIDTRANS_SERVER_KEY'])
}

VERITRANS_DATE_TIME_FORMAT = '%Y-%m-%d %H:%M:%S'

class CreditCardCapture():

    def __init__(self, master_order=None, billing_address=None, save_token=None, args=None):
        self.order = master_order
        self.billing_address = billing_address
        self.save_token = save_token
        self.payment = None
        self.args = args

    def create_payment(self):
        exist_payment = Payment.query.filter_by(order_id=self.order.id).first()
        if not exist_payment:
            exist_payment = Payment(
                order_id=self.order.id,
                user_id=self.order.user_id,
                paymentgateway_id=self.order.paymentgateway_id,
                currency_code=self.order.currency_code,
                amount=self.order.final_amount,
                payment_status=PAYMENT_IN_PROCESS,
                is_active=True,
                is_test_payment=True if app.config['DEBUG'] else False,
                is_trial=False,
                payment_datetime=datetime.now(),
                financial_archive_date=None,
                merchant_params=None
            )
            db.session.add(exist_payment)
            db.session.commit()

        self.payment = exist_payment

    def create_cc_data(self):
        cc_data = PaymentCreditCard(
            order_id=self.order.id,
            status = STATUS_PENDING,
            save_token = self.save_token)
        db.session.add(cc_data)
        db.session.commit()
        self.cc_data = cc_data

    def create_bin_token(self, midtrans_response):
        '''
            1. update/retrieve data bin/bank etc for CC
            2. always update latest token.
        '''

        masked_card = midtrans_response.get('masked_card', None)
        saved_token_id_expired_at = midtrans_response.get('saved_token_id_expired_at', None)
        saved_token_id = midtrans_response.get('saved_token_id', None)
        expired_date = datetime.strptime(saved_token_id_expired_at, VERITRANS_DATE_TIME_FORMAT) if saved_token_id_expired_at else None

        if masked_card:
            exist_bin_token = PaymentCreditCardToken.query.filter(
                PaymentCreditCardToken.user_id==self.order.user_id,
                PaymentCreditCardToken.masked_credit_card==masked_card
                #PaymentCreditCardToken.expiration >= datetime.now()
            ).first()

            self.cc_data.save_token = saved_token_id
            db.session.add(self.cc_data)
            db.session.commit()

            if not exist_bin_token:
                exist_bin_token = PaymentCreditCardToken(
                    user_id = self.order.user_id,
                    masked_credit_card = masked_card,
                    billing_address=self.billing_address,
                    expiration=expired_date,
                    first_name=self.billing_address.get('first_name', self.user.first_name if self.user.first_name else '')[:20],
                    last_name=self.billing_address.get('last_name', self.user.last_name if self.user.last_name else '')[:20],
                    is_active=True,
                    token=saved_token_id
                )
                db.session.add(exist_bin_token)
                db.session.commit()
            else:
                # CC can deactivate, soo always update with latest data
                exist_bin_token.token = saved_token_id
                exist_bin_token.billing_address = self.billing_address
                exist_bin_token.expiration = expired_date
                exist_bin_token.first_name = self.billing_address.get('first_name', self.user.first_name if self.user.first_name else '')[:20]
                exist_bin_token.last_name = self.billing_address.get('last_name', self.user.last_name if self.user.last_name else '')[:20]
                exist_bin_token.is_active = True
                db.session.add(exist_bin_token)
                db.session.commit()

            data_masked_card = masked_card.split('-')[0]
            bin_url = '{}/{}'.format(app.config['MIDTRANS_API_BIN'], data_masked_card)
            bin_data = requests.get(bin_url, headers=MIDTRANS_HEADER)

            if bin_data.status_code in [httplib.OK, httplib.CREATED]:
                bin_response = bin_data.json().get('data', None)

                if bin_response:
                    exist_bin_token.bin_bank = bin_response.get('bank', None)
                    exist_bin_token.bin_bank_code = bin_response.get('bank_code', None)
                    exist_bin_token.bin_class = bin_response.get('bin_class', None)
                    exist_bin_token.bin_types = bin_response.get('bin_type', None)
                    exist_bin_token.bin_brand = bin_response.get('brand', None)
                    exist_bin_token.bin_country_code = bin_response.get('country_code', None)
                    db.session.add(exist_bin_token)
                    db.session.commit()

    def add_renewal_transactions(self, midtrans_response):
        orderline = OrderLine.query.filter_by(order_id=self.order.id).order_by(desc(OrderLine.id)).all()
        masked_card = midtrans_response.get('masked_card', None)

        for data_orderline in orderline:
            expired_date = datetime.now()

            if data_orderline.offer.offer_type_id in [SUBSCRIPTIONS]:
                quantity, quantity_unit, allow_backward, backward_quantity, backward_quantity_unit = data_orderline.offer.get_subscrip_quantity()

                if quantity_unit == UNIT:
                    # this is always set to 1 month for 4 or 5 or 6 or 12 editions/1 month case.
                    expired_date = datetime.now() + relativedelta(months=1)
                if quantity_unit == DAILY:
                    expired_date = datetime.now() + timedelta(days=quantity)
                if quantity_unit == WEEKLY:
                    expired_date = datetime.now() + timedelta(weeks=quantity)
                if quantity_unit == MONTHLY:
                    expired_date = datetime.now() + relativedelta(months=quantity)

            if data_orderline.offer.offer_type_id in [BUFFET]:
                offer_buffet = data_orderline.offer.get_buffet_objects()
                expired_date = datetime.now() + offer_buffet.buffet_duration if offer_buffet else datetime.now() + relativedelta(months=1)

            # renewal only for subscription or buffet
            if data_orderline.offer.offer_type_id in [SUBSCRIPTIONS, BUFFET]:
                #
                exist_renewal = PaymentCreditCardRenewal.query.filter(
                    PaymentCreditCardRenewal.user_id == self.order.user_id,
                    PaymentCreditCardRenewal.offer_id == data_orderline.offer.id,
                    PaymentCreditCardRenewal.expired_date >= datetime.now(),
                    PaymentCreditCardRenewal.renewal_status == RENEWAL_STATUS[NEW]
                ).first()

                if not exist_renewal:
                    new_renewal = PaymentCreditCardRenewal(
                        masked_credit_card=masked_card,
                        renewal_status=RENEWAL_STATUS[NEW],
                        user_id=self.order.user_id,
                        offer_id=data_orderline.offer.id,
                        start_date=datetime.now().replace(microsecond=0), # remove microseconds
                        expired_date=expired_date.replace(microsecond=0), # remove microseconds
                        order_id=self.order.id,
                        orderline_id=data_orderline.id
                    )
                    db.session.add(new_renewal)
                    db.session.commit()


    def request_charge(self):
        orderline = OrderLine.query.filter_by(order_id=self.order.id).order_by(desc(OrderLine.id)).all()

        return {
            "payment_type": "credit_card",
            "transaction_details": {
                "order_id": self.order.id,
                "gross_amount": int(self.order.final_amount)
            },
            "credit_card": {
                "token_id": self.save_token,
                "save_token_id": True
            },
            "item_details": [{
                "id": str(iorder.id),
                "price": int(iorder.final_price),
                "quantity": iorder.quantity,
                "name": "{}".format(unidecode(unicode(iorder.name))[:30])
            } for iorder in orderline],
            "customer_details": {
                "first_name": self.billing_address.get('first_name', self.user.first_name if self.user.first_name else '')[:20],
                "last_name": self.billing_address.get('last_name', self.user.last_name if self.user.last_name else '')[:20],
                "email": self.user.email,
                "phone": self.billing_address.get('phone', self.user.phone_number if self.user.phone_number else '123456')
            }
        }

    def failed_response(self):
        self.cc_data.status = STATUS_DENIED
        db.session.add(self.cc_data)
        db.session.commit()

        return err_response(
            status=httplib.BAD_REQUEST,
            error_code=httplib.BAD_REQUEST,
            developer_message='Payment invalid, Fraud Order - please check all credit card info.',
            user_message='Payment invalid, Fraud Order - please check all credit card info.')

    def charge(self):
        data = self.request_charge()

        # charge midtrans api charge
        charge = requests.post(app.config['MIDTRANS_API_CHARGE'], json.dumps(data), headers=MIDTRANS_HEADER)
        midtrans_response = charge.json()

        self.cc_data.data_request = data
        self.cc_data.data_response = midtrans_response
        db.session.add(self.cc_data)
        db.session.commit()

        # if midtrans_response:
        if charge.status_code in [httplib.OK, httplib.CREATED]:
            fraud_status = midtrans_response.get('fraud_status', '')
            transaction_status = midtrans_response.get('transaction_status', '')

            if fraud_status == FRAUD_ACCEPT and transaction_status == STATUS_CAPTURE:
                self.cc_data.status = STATUS_CAPTURE
                db.session.add(self.cc_data)

                self.payment.payment_status = PAYMENT_BILLED
                db.session.add(self.payment)

                self.order.order_status = COMPLETE
                db.session.add(self.order)
                db.session.commit()

                try:
                    self.create_bin_token(midtrans_response)

                    # # create new renewal record, FREE price not acceptable for renewal.
                    if self.order.final_amount > 0 and self.order.paymentgateway.is_renewal:
                        self.add_renewal_transactions(midtrans_response)

                except Exception as e:
                    pass

                return OrderCompleteConstruct(user_id=self.order.user_id,
                                              payment_gateway_id=self.order.paymentgateway_id,
                                              order_id=self.order.id,
                                              order_number=self.order.order_number).construct()
            else:
                return self.failed_response()
        else:
            self.cc_data.data_response = charge.text
            db.session.add(self.cc_data)
            db.session.commit()
            return self.failed_response()


    def capture(self):
        self.user = User.query.filter_by(id=self.order.user_id).first()
        self.create_payment()
        self.create_cc_data()

        # update order status to payment in process.
        self.order.order_status = PAYMENT_IN_PROCESS
        db.session.add(self.order)
        db.session.commit()

        return self.charge()

