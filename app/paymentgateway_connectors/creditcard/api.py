import httplib

from flask import g, request, make_response, jsonify, Response
from flask_appsfoundry.exceptions import Unauthorized
from flask_restful import Resource

from sqlalchemy import desc
from datetime import datetime
from app.auth.decorators import token_required
from app.constants import CLIENT_400101
from app.helpers import err_response

from app.paymentgateway_connectors.choices.renewal_status import RENEWAL_STATUS, NEW, CANCEL
from app.paymentgateway_connectors.creditcard.models import PaymentCreditCardRenewal, PaymentCreditCardToken
from app.utils.http import get_offset_limit
from app.utils.marshmallow_helpers import schema_load
from app.utils.responses import get_response

from marshmallow import fields
from app import ma, db

class OrganizationOfferSchema(ma.Schema):
    renewal_id = fields.Integer(required=True)
    password = fields.String(required=True)

renewal_schema = OrganizationOfferSchema()


class PaymentCreditCardRenewalListApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public, can_read_write_public_ext')
    def get(self):
        if not getattr(g, 'current_user', None):
            raise Unauthorized()

        offset, limit = get_offset_limit(request)
        renewal_data = PaymentCreditCardRenewal.query.filter(
            PaymentCreditCardRenewal.user_id == g.current_user.id,
            PaymentCreditCardRenewal.expired_date >= datetime.now(),
            PaymentCreditCardRenewal.renewal_status == RENEWAL_STATUS[NEW]).order_by(
            desc(PaymentCreditCardRenewal.expired_date))

        data_count = renewal_data.count()
        if limit: renewal_data = renewal_data.limit(limit).offset(offset).all()

        data_response = [{'id': irenew.id, 'name': irenew.orderline.name.title(),
                          'final_price': '%.2f' % irenew.orderline.final_price,
                          'expired_date': irenew.expired_date.isoformat(),
                          'user_id': irenew.user_id,
                          'cc_number': irenew.masked_credit_card,
                          'status': irenew.renewal_status} for irenew in renewal_data]
        return get_response(data=data_response, key_name="data", count=data_count, limit=limit, offset=offset)


class PaymentCreditCardRenewalCancelApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public, can_read_write_public_ext')
    def post(self):
        if not getattr(g, 'current_user', None): raise Unauthorized()

        request_data = schema_load(renewal_schema)
        renewal_id = request_data.get('renewal_id', 1)
        password = request_data.get('password', '')

        exist_data = PaymentCreditCardRenewal.query.filter(
            PaymentCreditCardRenewal.id == renewal_id,
            PaymentCreditCardRenewal.user_id == g.current_user.id).first()

        if not exist_data:
            return Response(status=httplib.NOT_FOUND)

        valid_password = exist_data.user.verify_password(password)
        if not valid_password:
            return err_response(status=httplib.BAD_REQUEST, error_code=CLIENT_400101,
                                developer_message="Invalid Password, can't processed!",
                                user_message="Invalid Password")

        exist_data.renewal_status = RENEWAL_STATUS[CANCEL]
        exist_data.note = "Cancelled by user from apps."
        db.session.add(exist_data)
        db.session.commit()
        return make_response(jsonify({'message': 'Renewal successfully canceled.'}), httplib.OK)


class PaymentCreditCardRenewalDeleteApi(Resource):

    def exist_data(self, masked_credit_card):
        return PaymentCreditCardRenewal.query.filter(
            PaymentCreditCardRenewal.masked_credit_card == masked_credit_card,
            PaymentCreditCardRenewal.user_id == g.current_user.id,
            PaymentCreditCardRenewal.renewal_status == RENEWAL_STATUS[NEW]).all()

    def get(self, masked_credit_card):
        all_renewal_data = self.exist_data(masked_credit_card)
        message = None
        if all_renewal_data:
            message = "This card is still used for your auto renewal. If deleted, auto renewal will stop. " \
                      "Are you sure you want to delete the card?"
        resp = jsonify({'message': message})
        resp.status_code = httplib.OK
        return resp

    @token_required('can_read_write_global_all, can_read_write_public, can_read_write_public_ext')
    def delete(self, masked_credit_card=None):
        if not masked_credit_card:
            return err_response(status=httplib.BAD_REQUEST, error_code=CLIENT_400101,
                developer_message="Masked credit card can't empty", user_message="Invalid Masked credit card")

        exist_cc = PaymentCreditCardToken.query.filter(
            PaymentCreditCardToken.user_id == g.current_user.id,
            PaymentCreditCardToken.masked_credit_card == masked_credit_card
        ).all()

        if not exist_cc:
            return err_response(status=httplib.BAD_REQUEST, error_code=CLIENT_400101,
                developer_message="You not have access for delete this data.", user_message="Invalid access!")

        all_renewal_data = self.exist_data(masked_credit_card)
        for data in all_renewal_data:
            data.renewal_status = RENEWAL_STATUS[CANCEL]
            data.note = 'Credit card deleted by user'
            db.session.add(data)
            db.session.commit()

        # inactive CC token related to users.
        for data_cc in exist_cc:
            data_cc.is_active = False
            db.session.add(data_cc)
            db.session.commit()

        resp = jsonify()
        resp.status_code = httplib.NO_CONTENT
        return resp
