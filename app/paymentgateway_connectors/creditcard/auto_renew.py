import base64, json, requests, httplib

from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from sqlalchemy import desc, cast, Date
from unidecode import unidecode

from app import db, app
from app.master.choices import ANDROID, GETSCOOP
from app.messaging.email import HtmlEmail
from app.offers.choices.offer_type import SUBSCRIPTIONS, BUFFET
from app.offers.choices.quantity_unit import UNIT, DAILY, WEEKLY, MONTHLY
from app.offers.choices.status_type import READY_FOR_SALE
from app.orders.choices import PAYMENT_IN_PROCESS, COMPLETE, PAYMENT_BILLED
from app.orders.models import OrderLine, Order
from app.offers.models import OfferPlatform
from app.orders.order_complete import OrderCompleteConstruct
from app.paymentgateway_connectors.atm_transfer.midtrans_status import FRAUD_ACCEPT, STATUS_CAPTURE, STATUS_PENDING, \
    STATUS_DENIED
from app.paymentgateway_connectors.choices.renewal_status import RENEWAL_STATUS, NEW, PROCESSING, EXPIRED, CANCEL
from app.paymentgateway_connectors.creditcard.models import PaymentCreditCard, PaymentCreditCardToken, PaymentCreditCardRenewal
from app.payments.models import Payment

MIDTRANS_HEADER = {
    'Content-Type':'application/json',
    'Accept':'application/json',
    'Authorization':base64.b64encode(app.config['MIDTRANS_SERVER_KEY'])
}

class CreditCardRenew():

    def __init__(self):
        pass

    def request_charge(self, new_order, new_orderlines, exist_token, exist_renewal):
        billing_address = exist_token.billing_address

        return {
            "payment_type": "credit_card",
            "transaction_details": {
                "order_id": new_order.id,
                "gross_amount": int(new_order.final_amount)
            },
            "credit_card": {
                "token_id": exist_token.token,
                "save_token_id": True
            },
            "item_details": [{
                "id": str(new_orderlines.id),
                "price": int(new_orderlines.final_price),
                "quantity": new_orderlines.quantity,
                "name": "{}".format(unidecode(unicode(new_orderlines.name))[:30])
            }],
            "customer_details": {
                "first_name": billing_address.get('first_name', '')[:20],
                "last_name": billing_address.get('last_name', '')[:20],
                "email": exist_renewal.user.email,
                "phone": billing_address.get('phone', '123456')
            }
        }

    def create_data_renewal(self, exist_renew):

        if exist_renew.orderline.is_discount:
            # always charge with original price.
            if exist_renew.order.platform_id in [GETSCOOP]:
                charge_final_price = exist_renew.offer.price_idr
            else:
                offer_platform = OfferPlatform.query.filter_by(offer_id=exist_renew.orderline.offer_id,
                    platform_id=exist_renew.order.platform_id).order_by(desc(OfferPlatform.id)).first()
                charge_final_price = offer_platform.price_idr if offer_platform else exist_renew.order.final_amount
        else:
            # else always charge with first buy pricing
            charge_final_price = exist_renew.order.final_amount


        new_order = Order(
            order_number = exist_renew.order.order_number,
            total_amount = charge_final_price,
            final_amount = charge_final_price,
            user_id = exist_renew.user.id,
            client_id = exist_renew.order.client_id,
            partner_id = exist_renew.order.partner_id,
            order_status = PAYMENT_IN_PROCESS, #always make this in proses, change to complete after validate midtrans pass.
            is_active = True,
            point_reward = 0,
            currency_code = exist_renew.order.currency_code,
            paymentgateway_id = exist_renew.order.paymentgateway_id,
            tier_code = exist_renew.order.tier_code,
            platform_id = exist_renew.order.platform_id,
            is_renewal = True
        )
        db.session.add(new_order)
        db.session.commit()

        new_orderlines = OrderLine(
            name = exist_renew.orderline.name,
            offer_id = exist_renew.offer.id,
            is_active = True,
            is_free = False,
            is_discount = False,
            user_id = new_order.user_id,
            order_id = new_order.id,
            quantity = exist_renew.orderline.quantity,
            orderline_status = PAYMENT_IN_PROCESS,
            currency_code = new_order.currency_code,
            price = new_order.final_amount,
            final_price = new_order.final_amount,
            localized_currency_code = 'IDR',
            localized_final_price = new_order.final_amount
        )
        db.session.add(new_orderlines)
        db.session.commit()

        new_payment = Payment(
            order_id = new_order.id,
            user_id=new_order.user_id,
            paymentgateway_id=new_order.paymentgateway_id,
            currency_code=new_order.currency_code,
            amount=new_order.final_amount,
            payment_status=PAYMENT_IN_PROCESS,
            is_active=True,
            is_test_payment=False,
            is_trial=False,
            payment_datetime=datetime.now(),
            financial_archive_date=None,
            merchant_params=None
        )
        db.session.add(new_payment)
        db.session.commit()

        return new_order, new_orderlines, new_payment

    def create_cc_data(self, new_order, request_data, exist_token):
        cc_data = PaymentCreditCard(
            order_id=new_order.id,
            status=STATUS_PENDING,
            save_token=exist_token.token,
            data_request=request_data)
        db.session.add(cc_data)
        db.session.commit()
        return cc_data

    def create_new_renewal(self, exist_renew, new_order, new_orderlines):

        expired_date = datetime.now()

        if exist_renew.offer.offer_type_id in [SUBSCRIPTIONS]:
            quantity, quantity_unit, allow_backward, backward_quantity, backward_quantity_unit = exist_renew.offer.get_subscrip_quantity()

            if quantity_unit == UNIT:
                # this is always set to 1 month for 4 or 5 or 6 or 12 editions/1 month case.
                expired_date = datetime.now() + relativedelta(months=1)
            if quantity_unit == DAILY:
                expired_date = datetime.now() + timedelta(days=quantity)
            if quantity_unit == WEEKLY:
                expired_date = datetime.now() + timedelta(weeks=quantity)
            if quantity_unit == MONTHLY:
                expired_date = datetime.now() + relativedelta(months=quantity)

        if exist_renew.offer.offer_type_id in [BUFFET]:
            offer_buffet = exist_renew.offer.get_buffet_objects()
            expired_date = datetime.now() + offer_buffet.buffet_duration if offer_buffet else datetime.now() + relativedelta(
                months=1)

        # # reset clock
        # expired_date = expired_date.replace(hour=0, minute=0, second=0, microsecond=0)

        new_renewal_data = PaymentCreditCardRenewal(
            masked_credit_card = exist_renew.masked_credit_card,
            renewal_status = RENEWAL_STATUS[NEW],
            user_id = exist_renew.user_id,
            offer_id = exist_renew.offer_id,
            start_date = exist_renew.expired_date,
            expired_date = expired_date.replace(microsecond=0),
            order_id = new_order.id,
            orderline_id = new_orderlines.id,
            note = None)
        db.session.add(new_renewal_data)
        db.session.commit()

    def send_email_failed_renew(self, customer_email, orderline):
        try:
            from app.messaging.email import get_smtp_server

            server = get_smtp_server()
            msg = HtmlEmail(
                sender='Gramedia Digital <no-reply@gramedia.com>',
                to=[customer_email, ],
                subject='Your renewal has been cancelled.',
                html_template='email/cc_renewal/failed.html',
                plaintext_template='email/cc_renewal/failed.txt',
                offer_name=unidecode(unicode(orderline.offer.long_name)),
                order_number=orderline.order.order_number,
                order_date=(orderline.order.created + timedelta(hours=7)).strftime('%d %b %Y | %H:%M WIB')
            )
            server.send_message(msg)

        except Exception as e:
            pass

    def build_data(self):
        # get data from 2 days before..
        start_renew = (datetime.now() - timedelta(days=2)).replace(hour=0, minute=0)
        end_renew = datetime.now().replace(hour=0, minute=0)

        renew_data = PaymentCreditCardRenewal.query.filter(
            PaymentCreditCardRenewal.renewal_status == RENEWAL_STATUS[NEW],
            cast(PaymentCreditCardRenewal.expired_date, Date) >= start_renew.date(),
            cast(PaymentCreditCardRenewal.expired_date, Date) <= end_renew.date()
        ).order_by(PaymentCreditCardRenewal.expired_date).limit(10).all()

        for exist_renew in renew_data:
            exist_renew.renewal_status = RENEWAL_STATUS[PROCESSING]
            db.session.add(exist_renew)
            db.session.commit()

            # get saved token by user.
            exist_token = PaymentCreditCardToken.query.filter(
                PaymentCreditCardToken.user_id == exist_renew.user_id,
                PaymentCreditCardToken.masked_credit_card == exist_renew.masked_credit_card,
                PaymentCreditCardToken.expiration > datetime.now(),
                PaymentCreditCardToken.is_active == True
            ).order_by(desc(PaymentCreditCardToken.expiration)).first()

            # break if offer status already not sale.
            if exist_renew.offer.offer_status != READY_FOR_SALE:
                exist_renew.renewal_status = RENEWAL_STATUS[CANCEL]
                exist_renew.note = 'Offer Not For Sale.'
                db.session.add(exist_renew)
                db.session.commit()
                break

            if exist_token:
                # create order.
                new_order, new_orderlines, new_payment = self.create_data_renewal(exist_renew)

                # create request data.
                request_data = self.request_charge(new_order, new_orderlines, exist_token, exist_renew)

                # create history cc request & response
                new_cc_data = self.create_cc_data(new_order, request_data, exist_token)

                # renew user.
                charge = requests.post(app.config['MIDTRANS_API_CHARGE'], json.dumps(request_data), headers=MIDTRANS_HEADER)
                midtrans_response = charge.json()

                if charge.status_code in [httplib.OK, httplib.CREATED]:
                    fraud_status = midtrans_response.get('fraud_status', '')
                    transaction_status = midtrans_response.get('transaction_status', '')

                    if fraud_status == FRAUD_ACCEPT and transaction_status == STATUS_CAPTURE:

                        # Update all status Related to Finance.
                        exist_renew.renewal_status = RENEWAL_STATUS[EXPIRED]
                        db.session.add(exist_renew)

                        new_order.order_status = COMPLETE
                        db.session.add(new_order)

                        new_orderlines.orderline_status = COMPLETE
                        db.session.add(new_orderlines)

                        new_payment.payment_status = PAYMENT_BILLED
                        db.session.add(new_payment)

                        new_cc_data.status = STATUS_CAPTURE
                        new_cc_data.data_response = midtrans_response
                        db.session.add(new_cc_data)
                        db.session.commit()

                        self.create_new_renewal(exist_renew, new_order, new_orderlines)

                        # Deliver items.
                        OrderCompleteConstruct(
                            user_id=new_order.user_id,
                            payment_gateway_id=new_order.paymentgateway_id,
                            order_id=new_order.id,
                            order_number=new_order.order_number
                        ).construct()

                    else:
                        exist_renew.renewal_status = RENEWAL_STATUS[CANCEL]
                        db.session.add(exist_renew)

                        new_cc_data.status = STATUS_DENIED
                        new_cc_data.data_response = midtrans_response
                        db.session.add(new_cc_data)
                        db.session.commit()

                        self.send_email_failed_renew(customer_email=exist_renew.user.email, orderline=exist_renew.orderline)

                else:
                    exist_renew.renewal_status = RENEWAL_STATUS[CANCEL]
                    db.session.add(exist_renew)

                    new_cc_data.status = STATUS_DENIED
                    new_cc_data.data_response = charge.text or midtrans_response
                    db.session.add(new_cc_data)
                    db.session.commit()

                    self.send_email_failed_renew(customer_email=exist_renew.user.email, orderline=exist_renew.orderline)


        return 'CC recurring Complete.'

    def renewal(self):
        try:
            self.build_data()
        except Exception as e:
            pass
