from app import db
from datetime import datetime

from flask_appsfoundry.models import TimestampMixin
from sqlalchemy.dialects.postgresql import ENUM, JSONB
from app.paymentgateway_connectors.atm_transfer.midtrans_status import MIDTRANS_STATUS
from app.paymentgateway_connectors.choices.renewal_status import RENEWAL_STATUS


class PaymentCreditCard(db.Model, TimestampMixin):

    __tablename__ = 'core_paymentcreditcards'

    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('core_orders.id'), nullable=False)
    order = db.relationship('Order')
    status = db.Column(ENUM(*MIDTRANS_STATUS.values(), name='status'), nullable=False)
    save_token = db.Column(db.String(250))
    data_request = db.Column(JSONB)
    data_response = db.Column(JSONB)


# this table for store history token users who purchased with CC.
# user can select when purchased again.
class PaymentCreditCardToken(db.Model, TimestampMixin):

    __tablename__ = 'core_paymentcreditcardtokens'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    user = db.relationship('User')
    masked_credit_card = db.Column(db.String(250))
    token = db.Column(db.String(250))
    expiration = db.Column(db.DateTime)
    billing_address = db.Column(JSONB)
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    is_active = db.Column(db.Boolean, default=True)
    bin_bank = db.Column(db.String(50))
    bin_bank_code = db.Column(db.String(50))
    bin_class = db.Column(db.String(50))
    bin_types = db.Column(db.String(50))
    bin_brand = db.Column(db.String(50))
    bin_country_code = db.Column(db.String(10))

    def values(self):
        return {
            "bin_bank": self.bin_bank,
            "bin_bank_code": self.bin_bank_code,
            "bin_brand": self.bin_brand,
            "bin_class": self.bin_class,
            "bin_country_code": self.bin_country_code,
            "bin_types": self.bin_types,
            "expiration": self.expiration.isoformat(),
            "first_name": self.first_name,
            "id": self.id,
            "is_active": self.is_active,
            "last_name": self.last_name,
            "masked_credit_card": self.masked_credit_card,
            "token": self.token,
            "user_id": self.user_id}

# This table for store renewal data credit cards.
class PaymentCreditCardRenewal(db.Model, TimestampMixin):

    __tablename__ = 'core_paymentcreditcardrenewals'

    id = db.Column(db.Integer, primary_key=True)
    masked_credit_card = db.Column(db.String(250), nullable=False)
    renewal_status = db.Column(ENUM(*RENEWAL_STATUS.values(), name='renewal-status'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    user = db.relationship('User')
    offer_id = db.Column(db.Integer, db.ForeignKey('core_offers.id'), nullable=False)
    offer = db.relationship('Offer')
    start_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    expired_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    order_id = db.Column(db.Integer, db.ForeignKey('core_orders.id'), nullable=False)
    order = db.relationship('Order')
    orderline_id = db.Column(db.Integer, db.ForeignKey('core_orderlines.id'), nullable=False)
    orderline = db.relationship('OrderLine')
    note = db.Column(db.String(250))



# THIS IS OLD DATABASE.. DO NOT REMOVE.
# ALL RECORD ALREADY STORE TO NEW TABEL
class PaymentVeritrans(db.Model, TimestampMixin):

    __tablename__ = 'core_paymentveritrans'

    id = db.Column(db.Integer, primary_key=True)
    payment_id = (db.Column(db.Integer, db.ForeignKey('core_payments.id')))
    payment = db.relationship('Payment')
    transaction_id = db.Column(db.Integer, nullable=False)
    payment_code = db.Column(db.String(256))

    def __repr__(self):
        return 'id : {}'.format(self.id)

    def values(self):
        return {
            'id': self.id,
            'payment_id': self.payment_id,
            'transaction_id': self.transaction_id,
            'payment_code': self.payment_code
        }


class PaymentVeritransDetail(db.Model, TimestampMixin):
    __tablename__ = 'core_paymentveritransdetails'

    id = db.Column(db.Integer, primary_key=True)
    paymentveritrans_id = (db.Column(db.Integer, db.ForeignKey('core_paymentveritrans.id'), nullable=False))
    payment_id = db.Column(db.Integer, nullable=False)
    capture_request = db.Column(db.Text)
    capture_response = db.Column(db.Text)

    def __repr__(self):
        return 'id :{}'.format(self.id)

    def values(self):
        return {
            'id': self.id,
            'paymentveritrans_id': self.paymentveritrans_id,
            'payment_id': self.payment_id,
            'capture_request': self.capture_request,
            'capture_response': self.capture_response
        }
