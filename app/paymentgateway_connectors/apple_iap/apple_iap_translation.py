import json
import ast
import requests
import httplib

from flask import Response
from datetime import datetime
from werkzeug.exceptions import abort

from app import constants, app
from random import randint

from app.paymentgateway_connectors.apple_iap import itunes_product_type \
    as iproduct_type
from app.paymentgateway_connectors.apple_iap.helpers import AppleIAPHelpers
from app.orders.models import Order, OrderLineDiscount

TRANSLATION_TO_SC2 = 1
TRANSLATION_TO_SC1 = 2
TRANSACTION_SUCCESS = "1"


class AppleIAPTranslation():
    def __init__(self, user_id=None, translation_type=None, order_id=None,
                 receipt=None, client_id=None, api=None, method=None, is_sandbox=False):

        self.user_id = user_id
        self.translation_type = translation_type
        self.order_id = order_id
        self.receipt = receipt
        self.client_id = client_id
        self.is_sandbox = is_sandbox

        self.web_order_line_item_id = None
        self.itunes_product_type = None
        self.purchase_date = ''
        self.original_purchase_date = ''
        self.product_sku = None
        self.json_form = dict()
        self.json_response = dict()
        self.is_promo = 2
        self.transaction_code = ''
        self.original_transaction_code = ''

        self.ol_data = None
        self.offer_data = []
        self.ol_disc_data = None
        self.save_in_db_rslt = None

    def translate_to_sc2(self):
        json_response = dict()
        json_response['new_receipt'] = True
        json_response['applereceiptidentifier_rslt'] = None

        self.web_order_line_item_id = \
            self.receipt.get('receipt').get('web_order_line_item_id', None)

        self.product_sku = \
            self.receipt.get('receipt').get('product_id', None)

        # Define whether the item is subscription or single
        if self.web_order_line_item_id is not None:
            self.itunes_product_type = iproduct_type.SUBSCRIPTION
        else:
            if self.product_sku.find(app.config['CONSUMABLE_MARK']) == -1:
                self.itunes_product_type = iproduct_type.SINGLE_CONSUMABLE
            else:
                self.itunes_product_type = iproduct_type.SINGLE_NON_CONSUMABLE

        new_receipt = AppleIAPHelpers().check_new_receipt(
            client_id=self.client_id,
            web_order_line_id=self.web_order_line_item_id,
            itunes_product_type_id=self.itunes_product_type,
            original_transaction_id=self.receipt.get('receipt').get('original_transaction_id', None))

        if not new_receipt:
            json_response['new_receipt'] = False
            return json_response

        arc_params = {
            "bundle_id": self.receipt.get('receipt').get('bid', None),
            "original_transaction_id": self.receipt.get('receipt').get('original_transaction_id', None),
            "web_order_line_id": self.web_order_line_item_id,
            "itunes_product_type_id": self.itunes_product_type,
            "client_id": self.client_id,
            "order_id": None,
            "purchase_date": self.receipt.get('receipt').get('purchase_date', None),
            "expired_date": self.receipt.get('receipt').get('expires_date_formatted', None),
            "trx_status": self.receipt.get('status'),
            "user_id": self.user_id,
            "is_sandbox": self.is_sandbox}

        ari_status, ari_rslt = AppleIAPHelpers(**arc_params).save_to_applereceiptidentifiers()

        if not ari_status:
            raise Exception("save_to_db AppleIAPTranslation ERROR: code: %d Error occurs when calling SC1 API save to db" % (ari_rslt))

        json_response['applereceiptidentifier_rslt'] = ari_rslt
        return json_response

    def translate_to_sc1(self):
        try:
            self.product_sku = self.receipt.get('receipt').get('product_id', None)

            self.web_order_line_item_id = \
                self.receipt.get('receipt').get('web_order_line_item_id', None)

            # Define whether the item is subscription or single
            if self.web_order_line_item_id is not None:
                self.itunes_product_type = iproduct_type.SUBSCRIPTION
            else:
                if self.product_sku.find(app.config['CONSUMABLE_MARK']) == -1:
                    self.itunes_product_type = iproduct_type.SINGLE_CONSUMABLE
                else:
                    self.itunes_product_type = iproduct_type.SINGLE_NON_CONSUMABLE

            purchase_date_raw = str(self.receipt.get('receipt').get('purchase_date')).split(' ')
            self.purchase_date = "%s %s" % (purchase_date_raw[0], purchase_date_raw[1])
            original_purchase_date_raw = str(self.receipt.get('receipt').get('original_purchase_date')).split(' ')
            self.original_purchase_date = "%s %s" % (original_purchase_date_raw[0], original_purchase_date_raw[1])

            self.transaction_code = self.receipt.get('receipt').get('transaction_id')
            self.original_transaction_code = self.receipt.get('receipt').get('original_transaction_id', None)

            # GET orderline data
            self.ol_data = self.get_orderline()[0]

            # GET orderline discount data
            self.ol_disc_data = self.get_orderlinedisc(self.ol_data.id)

            # GET Offer data
            self.offer_data = self.ol_data.get_offer()
            if not self.offer_data:
                raise Exception("Cannot found any offer")

            if self.itunes_product_type == iproduct_type.SUBSCRIPTION:  # subs
                saved = self.save_to_db_sc1_subs()
            elif self.itunes_product_type in [0, 1]:  # single
                saved = self.save_to_db_sc1_single()
            else:
                raise Exception("Invalid itunes product type")

            return True

        except Exception, e:
            raise Exception(e)

    def save_to_db_sc1_single(self):
        try:
            url = "%s%s" % (
                app.config['SERVER_SCOOPCOR1'],
                app.config['REPORT_SERVICE_SC1_SINGLE'])

            data_constructor = self.json_construct()
            data = data_constructor

            save_to_db_sc1 = requests.post(url, data)

            if save_to_db_sc1.status_code == 200:
                return True

            raise Exception("save_to_db AppleIAPTranslation ERROR: code: %d Error occurs when calling SC1 API save to db" % (save_to_db_sc1.status_code))
        except Exception, e:
            raise Exception("save_to_db AppleIAPTranslation ERROR:", e)

    def save_to_db_sc1_subs(self):
        url = "%s%s" % (
            app.config['SERVER_SCOOPCOR1'],
            app.config['REPORT_SERVICE_SC1_SUBS'])

        data_constructor = self.json_construct()

        save_to_db_sc1 = requests.post(url=url, data=data_constructor)

        if save_to_db_sc1:
            return True
        raise Exception("save_to_db AppleIAPTranslation ERROR: code: %d Error occurs when calling SC1 API save to db" % (save_to_db_sc1.status_code))

    def get_orderline(self):
        order = Order.query.filter_by(id=self.order_id).first()
        orderline = None

        if order:
            orderline = order.get_order_line()

        if orderline:
            return orderline
        raise Exception("get_orderline ERROR: Invalid order_id")

    def get_orderlinedisc(self, orderline_id):
        orderlinedisc = OrderLineDiscount.query.filter_by(
            orderline_id=orderline_id).first()

        return orderlinedisc

    def json_format_info(self):
        info_form = dict()

        info_form['latitude'] = "0.000000"
        info_form['device_language'] = ""
        info_form['app_name'] = "scoop_ios"
        info_form['os_version'] = "7.1.2"
        info_form['device_model'] = ""
        info_form['device_imei'] = ""
        info_form['app_version'] = "4.1.3"
        info_form['longitude'] = "0.000000"
        info_form['device_id'] = ""

        return info_form

    def json_format_auth(self):
        auth_form = dict()
        auth_form['user_id'] = self.user_id

        return auth_form

    def json_format_subscription_item(self):
        discount_price = 0
        data_form = dict()
        data_form['bundle_subscription_code'] = self.offer_data.item_code
        data_form['payment_gateway'] = "%s" % self.ol_data.get_paymentgatewayid()
        data_form['status_message'] = "Translation from SC2"
        if self.ol_disc_data:
            data_form['coupon_code'] = self.ol_disc_data.get('discount_code')
        data_form['status'] = TRANSACTION_SUCCESS
        data_form['transaction_detail'] = ""
        data_form['transaction_currency'] = self.ol_data.currency_code
        data_form['transaction_price'] = str(self.ol_data.final_price)
        data_form['original_datetime'] = self.original_purchase_date
        data_form['transaction_datetime'] = self.purchase_date

        if self.ol_data.currency_code == "USD":
            discount_price = self.offer_data.discount_price_usd
        elif self.ol_data.currency_code == "IDR":
            discount_price = self.offer_data.discount_price_idr
        else:
            discount_price = self.offer_data.discount_price_point

        if self.ol_data.is_discount:
            is_promo = "1"
            data_form['transaction_price_after_discount'] = \
                float(discount_price)
        else:
            is_promo = "2"
        data_form['is_promo'] = is_promo

        data_form['transaction_code'] = self.transaction_code
        data_form['original_transaction_code'] = self.original_transaction_code

        return data_form

    def json_format_single_item(self):

        data_form = dict()
        data_form['transaction_currency_code'] = self.ol_data.currency_code
        data_form['transaction_price'] = str(self.ol_data.final_price)
        data_form['transaction_token'] = "1"
        data_form['transaction_type'] = "%s" % self.ol_data.get_paymentgatewayid()
        data_form['status'] = TRANSACTION_SUCCESS

        if self.ol_data.currency_code == "USD":
            discount_price = self.offer_data.discount_price_usd
        elif self.ol_data.currency_code == "IDR":
            discount_price = self.offer_data.discount_price_idr
        else:
            discount_price = self.offer_data.discount_price_point

        if self.ol_data.is_discount:
            is_promo = "1"
            data_form['transaction_price_after_discount'] = \
                float(discount_price)

        data_form['original_transaction_code'] = self.original_transaction_code
        data_form['transaction_code'] = self.transaction_code
        data_form['payment_gateway'] = "%d" % self.ol_data.get_paymentgatewayid()
        data_form['transaction_detail'] = ""
        data_form['original_datetime'] = self.original_purchase_date
        data_form['datetime'] = self.purchase_date
        data_form['status_message'] = "Translation from SC2"
        data_form['edition_code'] = self.offer_data.item_code
        if self.ol_disc_data:
            data_form['coupon_code'] = self.ol_disc_data.discount_code

        return data_form

    def json_construct(self):
        try:
            self.json_form['info'] = self.json_format_info()

            if self.itunes_product_type in [0, 1]:
                self.json_form['data'] = self.json_format_single_item()
            else:
                self.json_form['data'] = self.json_format_subscription_item()

            self.json_form['auth'] = self.json_format_auth()
            self.json_response['request'] = self.json_form

            return json.dumps(self.json_response)

        except Exception, e:
            raise Exception("%s" % str(e))

    def translate(self):
        try:

            if self.translation_type == TRANSLATION_TO_SC1:
                translate_response = self.translate_to_sc1()
                translate_response['translated'] = True
            elif self.translation_type == TRANSLATION_TO_SC2:
                translate_response = self.translate_to_sc2()
                translate_response['translated'] = True
            else:
                raise Exception("Invalid translation type")

            return translate_response

        except Exception, e:
            raise Exception("AppleIAPTranslation ERROR: %s" % str(e))