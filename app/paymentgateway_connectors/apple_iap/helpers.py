import requests
import json

from app import app
from app.paymentgateway_connectors.apple_iap.models import AppleReceiptIdentifiers
from app.paymentgateway_connectors.apple_iap import itunes_product_type
from . import iap_status


class AppleIAPHelpers():
    def __init__(
            self, receipt_data=None, bundle_id=None, original_transaction_id=None,
            web_order_line_id=None, itunes_product_type_id=None, client_id=None,
            order_id=None, purchase_date=None, expired_date=None,
            trx_status=False, is_sandbox=False, user_id=None, is_trial=False):

        self.receipt_data = receipt_data
        self.bundle_id = bundle_id
        self.original_transaction_id = original_transaction_id
        self.web_order_line_id = web_order_line_id
        self.itunes_product_type_id = itunes_product_type_id
        self.client_id = client_id
        self.order_id = order_id
        self.purchase_date = purchase_date
        self.expired_date = expired_date
        self.trx_status = trx_status
        self.arc = None
        self.is_sandbox = is_sandbox
        self.is_trial = is_trial
        self.user_id = user_id

    def verify_to_apple(self):
        """
        This function is used to construct the request and contact Itunes
        receipt server to do validation. The default is contacting the
        Live itunes server and if it gives 21007 it will try to contact sandbox.
        """

        verification_request = {
            'receipt-data': self.receipt_data,
            'password': app.config['APPLE_IAP_SHARED_SECRET']
        }

        verification_url = "%s%s" % (app.config['APPLE_IAP_HOST_LIVE'],
                                     app.config['URL_VERIFY_RECEIPT'])

        verification_rslt = requests.post(
            verification_url, data=json.dumps(verification_request))

        verification_status = int(verification_rslt.json()['status'])

        if verification_status == iap_status.INVALID_RECEIPT_PRODUCTION:
            verification_url = "%s%s" % (app.config['APPLE_IAP_HOST_SANDBOX'],
                                         app.config['URL_VERIFY_RECEIPT'])

            verification_rslt = requests.post(
                verification_url, data=json.dumps(verification_request))

            self.is_sandbox = True

        if verification_rslt.status_code != 200:
            status_code = verification_rslt.status_code
            message = "Cannot contact apple server"
            verification_rslt = "%s, %s" % (status_code, message)
            return False, verification_rslt

        if verification_rslt.json() == '' or verification_rslt.json() is None:
            status_code = verification_rslt.status_code
            message = "Apple server response nothing"
            verification_rslt = "%s, %s" % (status_code, message)
            return False, verification_rslt

        # Get trial status,
        # Do not move this code try trial on top.
        # cos verification_rslt.json() on top, only response {'status': 207},
        # do not know why..? ~_~"
        try:
            is_trial = verification_rslt.json()['receipt']['is_trial_period']
            if is_trial in ['true', 'True', True]: self.is_trial = True
        except Exception, e:
            self.is_trial = False

        json_response = {
            "is_sandbox": self.is_sandbox,
            "verification_rslt": verification_rslt.json(),
            "is_trial": self.is_trial
        }

        return True, json_response

    def save_to_applereceiptidentifiers(self):
        arc_request = {
            "bundle_id": self.bundle_id,
            "original_transaction_id": self.original_transaction_id,
            "web_order_line_id": self.web_order_line_id,
            "itunes_product_type": self.itunes_product_type_id,
            "client_id": self.client_id,
            "order_id": self.order_id,
            "purchase_date": self.purchase_date,
            "expired_date": self.expired_date,
            "status": self.trx_status,
            "is_test_payment": self.is_sandbox,
            "user_id": self.user_id,
            "is_trial": self.is_trial
        }

        ar = AppleReceiptIdentifiers(**arc_request).save()

        if ar.get('invalid', False):
            return False, ar.get('message', None)

        return True, ar.get('message', None)

    def check_new_receipt(self, client_id=None, web_order_line_id=None,
            itunes_product_type_id=None, original_transaction_id=None):
        """
        This function used to check whether the receipt passed to this function is new
        or not. It has 2 different ways to check the receipt, depend on product types.
        """
        new_receipt = True
        # For Subscriptions, it checks for web order line id and client id
        if client_id is not None and web_order_line_id is not None and \
                itunes_product_type_id == itunes_product_type.SUBSCRIPTION:

            self.arc = AppleReceiptIdentifiers.query.filter_by(
                client_id=client_id,
                web_order_line_id=web_order_line_id).all()
            if len(self.arc) > 0:
                new_receipt = False

        # For Singles, it checks for original transaction id and client id
        if original_transaction_id is not None and client_id is not None and \
                itunes_product_type_id != itunes_product_type.SUBSCRIPTION:

            self.arc = AppleReceiptIdentifiers.query.filter_by(
                original_transaction_id=original_transaction_id,
                client_id=client_id).all()
            if len(self.arc) > 0:
                new_receipt = False

        return new_receipt
