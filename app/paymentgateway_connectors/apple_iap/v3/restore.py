from __future__ import unicode_literals

import multiprocessing

from flask import g, request, jsonify
from flask import json
from flask import url_for
from flask_appsfoundry.exceptions import Unauthorized, UnprocessableEntity, NotFound
from marshmallow import fields
from psycopg2 import OperationalError, DatabaseError
from sqlalchemy import desc
from sqlalchemy.orm import sessionmaker, scoped_session

from app import db, ma
from app.auth.decorators import user_is_authenticated
from app.master.choices import IOS
from app.offers.models import OfferPlatform, Offer
from app.orders.deliver_items import deliver_owned_items
from app.paymentgateway_connectors.blueprints import blueprint_apple
from app.users.buffets import UserBuffet
from . import _log, helpers, models


class RestoreSchema(ma.Schema):
    receipt_data = fields.Str(required=True)
    device_registration_id = fields.Str()


@blueprint_apple.route('/restore', methods=['POST', ])
@user_is_authenticated
def apple_restore():
    """ Restore user owned items data based on Apple Receipt Data (grand unified receipt format).
    for IOS 7+.
    end point: /v1/payments/apple-iap/restore
    """

    if not getattr(g, 'current_user', None):
        raise Unauthorized()

    user_id = g.current_user.id

    _log.info('Apple IAP Restore for user: {user_id} -- START'.format(user_id=user_id))

    request_body, errors = RestoreSchema().load(request.json)
    if errors:
        raise UnprocessableEntity(
            user_message='Failed to restore',
            developer_message=json.dumps(errors),
        )

    receipt_data = request_body.get('receipt_data')
    device_registration_id = request_body.get('device_registration_id', None)

    # force a new session and connection
    db.engine.dispose()
    session_factory = sessionmaker(bind=db.engine)
    main_scoped_session = scoped_session(session_factory)
    session = main_scoped_session()

    log_data = session.query(models.LogReceipt).filter_by(
        user_id=user_id,
        activity_type=models.LogReceipt.ActivityTypes.restore,
        status=models.LogReceipt.StatusProcess.in_progress
    ).order_by(desc(models.LogReceipt.id)).first()

    # create new log data and save encrypted receipt-data
    if not log_data:
        log_data = helpers.create_new_log_receipt_data(
            receipt_data,
            activity_type=models.LogReceipt.ActivityTypes.restore,
            user_id=user_id,
            device_registration_id=device_registration_id,
            session=session)

        task_id = log_data.id
        session.expunge(log_data)

        # process restore in separate thread (async)
        p = create_thread_for_task(receipt_data, task_id,  user_id,
                                   user_email=g.current_user.email,
                                   device_registration_id=device_registration_id)

    else:
        task_id = log_data.id
        # restore is still in progress, return process info. stop here
        _log.info('Apple IAP Restore for user: {user_id} -- still in progress'.format(user_id=user_id))

    return jsonify({
        "id": task_id,
        "title": models.LogReceipt.ActivityTypes.restore.value,
        "status": models.LogReceipt.StatusProcess.in_progress.value,
        "href": get_href_restore(task_id)
    })


def get_href_restore(restore_id):
    return url_for('payments_apple_iap.get_restore_details', restore_id=restore_id, _external=True)


@blueprint_apple.route('/restore/<int:restore_id>')
@user_is_authenticated
def get_restore_details(restore_id):
    session = db.session()
    log_data = session.query(models.LogReceipt).get(restore_id)
    if not log_data:
        raise NotFound()

    response = {
        "id": restore_id,
        "title": log_data.activity_type.value,
        "status": log_data.status.value,
        "href": get_href_restore(restore_id)
    }
    if log_data.status == models.LogReceipt.StatusProcess.error:
        response["error"] = json.loads(log_data.error)

    return jsonify(response)


def create_thread_for_task(receipt_data, task_id, user_id, user_email, device_registration_id):
    _log.info('Apple IAP Restore for user: {user_id} -- Creating thread for Apple RESTORE'.format(
        user_id=user_id))
    p = multiprocessing.Process(
        target=async_restore_from_apple_receipt,
        args=(receipt_data, task_id, user_id, user_email, device_registration_id))
    p.start()
    return p


def async_restore_from_apple_receipt(receipt_data, task_id, user_id, user_email, device_registration_id):
    _log.info('Apple IAP Restore for user: {user_id} -- Async Start - get receipt from Apple'.format(
        user_id=user_id))

    db.engine.dispose()
    session_factory = sessionmaker(bind=db.engine)
    thread_scoped_session = scoped_session(session_factory)
    session = thread_scoped_session()

    # verify to itunes and get receipt data in json format
    try:
        log_data = session.query(models.LogReceipt).get(task_id)

        receipt_json, is_sandbox = helpers.get_receipt_from_itunes(receipt_data)

        # save json receipt data
        if receipt_json:
            log_data.receipt_json = receipt_json
            session.add(log_data)
            session.commit()

        # validate receipt data from apple response
        in_app_data = helpers.validate_receipt_json(receipt_json)

        _log.info('Apple IAP Restore for user: {user_id} -- deliver user items'.format(user_id=user_id))
        list_items = process_receipt_to_user_items(in_app_data, session, user_id=user_id)

        # update log receipt status to finished
        log_data.status = models.LogReceipt.StatusProcess.completed
        session.add(log_data)
        session.commit()

        # create response
        # data = [helpers.generate_response_per_item(item) for item in list_items]
        # count = len(list_items)

        helpers.send_restore_notification(user_email, device_registration_id,
                                          status=log_data.status, restore_id=task_id)
        # return data, count

    except (UnprocessableEntity, helpers.AppleRequestError, helpers.AppleConnectionError) as e:
        e.user_message = 'Failed to restore'
        _log.exception('Apple IAP Restore for user: {} - Error, {}'.format(user_id, e.developer_message))
        helpers.log_error(e.developer_message, log_data=log_data, session=session,
                          user_email=user_email, device_registration_id=device_registration_id)
        raise e
    except (DatabaseError, OperationalError) as e:
        _log.exception('Apple IAP Restore for user: {} - Error, {}'.format(user_id, str(e)))
        helpers.log_error(str(e), log_data=log_data, session=session,
                          user_email=user_email, device_registration_id=device_registration_id)
        raise e
    except Exception as e:
        _log.exception('Apple IAP Restore for user: {} - Error, {}'.format(user_id, str(e)))
        helpers.log_error(str(e), log_data=log_data, session=session,
                          user_email=user_email, device_registration_id=device_registration_id)
        raise e


def process_receipt_to_user_items(in_app_data, session, user_id):
    items = []

    processed_products = []
    for trx in in_app_data:

        product_id = trx.get('product_id', None)
        # _log.debug('Product : {}'.format(product_id))

        if product_id and helpers.SCOOP_PRODUCT_SPLITTER in product_id and product_id not in processed_products:

            tier_code = product_id.split(helpers.SCOOP_PRODUCT_SPLITTER)[1]
            # _log.debug('Tier Code: {}'.format(tier_code))

            if tier_code and '.c.' not in tier_code:
                # only for non consumable
                offer_platform = session.query(OfferPlatform).filter(
                    OfferPlatform.tier_code == tier_code,
                    OfferPlatform.platform_id == IOS
                ).join(Offer).filter(
                    Offer.is_active == True,
                    Offer.offer_status == Offer.Status.ready_for_sale.value
                ).first()

                if offer_platform:
                    # _log.debug('Offer Platform {0}, '
                    #            'offer: {1.id} type: {1.offer_type_id}'.format(
                    #                 offer_platform, offer_platform.offer))

                    if offer_platform.offer.offer_type_id == Offer.Type.single:
                        items = items + offer_platform.offer.items.all()

                    elif offer_platform.offer.offer_type_id == Offer.Type.subscription:
                        purchase_date = helpers.to_date(trx.get('purchase_date'))
                        items_sub = helpers.get_item_subscriptions(
                            offer_platform.offer,
                            session,
                            purchase_date=purchase_date
                        )
                        items.extend(items_sub)

                    elif offer_platform.offer.offer_type_id == Offer.Type.buffet:
                        deliver_item_buffet(
                            session,
                            original_transaction_id=trx.get('original_transaction_id', None),
                            user_id=user_id
                        )

        processed_products.append(product_id)

    if items:
        deliver_owned_items(items, user_id, session, order_line=None, is_restore=True)

    return items


def deliver_item_buffet(session, original_transaction_id, user_id):
    buffet_latest = session.query(UserBuffet).filter_by(
        original_transaction_id=original_transaction_id,
        is_restore=False).order_by(desc(UserBuffet.valid_to)).first()

    user_buffet = session.query(UserBuffet).filter_by(
        original_transaction_id=original_transaction_id,
        user_id=user_id).order_by(desc(UserBuffet.valid_to)).first()

    if buffet_latest and (not user_buffet or user_buffet.orderline_id != buffet_latest.orderline_id):
        new_data = UserBuffet(
            user_id=user_id,
            orderline_id=buffet_latest.orderline_id,
            offerbuffet_id=buffet_latest.offerbuffet_id,
            offerbuffet=buffet_latest.offerbuffet,
            valid_to=buffet_latest.valid_to,
            is_restore=True,
            is_trial=buffet_latest.is_trial,
            original_transaction_id=original_transaction_id
        )
        session.add(new_data)
        session.commit()


