from enum import Enum
from flask_appsfoundry.models import TimestampMixin
from sqlalchemy.dialects.postgresql import JSONB

from app import db
from app.utils.models import Py3EnumType


class LogReceipt(TimestampMixin, db.Model):
    """ Store Receipt data for IOS 7 new receipt format (Grand Unified Receipts :))

    """
    __tablename__ = 'log_receipts'

    class ActivityTypes(Enum):
        purchase = 'purchase'
        renewal = 'renewal'
        restore = 'restore'

    class StatusProcess(Enum):
        in_progress = 'in progress'
        completed = 'completed'
        error = 'error'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    user = db.relationship('User', backref=db.backref('log_receipts', lazy='dynamic'))
    status = db.Column(Py3EnumType(StatusProcess, name='status_process'), nullable=False)
    activity_type = db.Column(Py3EnumType(ActivityTypes, name='activity_type'), nullable=False)
    device_registration_id = db.Column(db.String)
    receipt_data = db.Column(db.TEXT)
    receipt_json = db.Column(JSONB)
    error = db.Column(db.TEXT)
