import json
from datetime import timedelta

from flask import current_app
from flask_appsfoundry.exceptions import UnprocessableEntity, InternalServerError

from app import db
from app.auth.models import Client
from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP, ReceiptVersion
from app.payments.models import Payment
from app.payments.payment_status import PAYMENT_BILLED
from app.utils.datetimes import get_utc_nieve
from . import _log, helpers


def apple_payment_receipt_validation(receipt_data, user_id, order, payment):
    """ Used in Purchase Process
    called by: v1/payments/validate with payment_gateway_id = APPLE

    :param receipt_data: unified receipt data from IOS7+
    :param order_id:
    :param user_id:
    :param order:
    :param payment:
    :return:
    """
    _log.debug('Apple IAP Purchase for user: {user_id} -- validate receipt vs order'.format(user_id=user_id))

    # - create log
    session = db.session()

    # verify to itunes and get receipt data in json format
    try:

        product_id = get_product_id(order, session)
        apple_iap_data = session.query(PaymentAppleIAP).filter_by(payment_id=payment.id).first()
        if not apple_iap_data:
            apple_iap_data = PaymentAppleIAP(
                payment_id=payment.id,
                product_id=product_id,
                receipt_data=receipt_data,
            )

        _log.debug('validate receipt data to apple api and get decrypted receipt json')
        # this will return helpers.AppleRequestError if failed
        receipt_json, is_sandbox = helpers.get_receipt_from_itunes(receipt_data)

        # since unencrypted data from receipt ios7 is too big, we don't store in db anymore, query to apple if needed
        # # save json receipt data
        # if receipt_json:
        #     apple_iap_data.receipt = json.dumps(receipt_json)
        #     session.add(apple_iap_data)
        #     session.commit()

        _log.debug('validate apple response')
        # this will return UnprocessableEntity if failed
        in_app_data = helpers.validate_receipt_json(receipt_json)

        _log.debug('validate order vs apple response (receipt in json format)')
        # this will return UnprocessableEntity if failed
        validation_results = validate_order_vs_apple_receipt(
            product_id, in_app_data, order, apple_iap_data, session,
            environment=receipt_json.get('environment', None))

        return validation_results

    except (UnprocessableEntity, helpers.AppleRequestError, helpers.AppleConnectionError) as e:
        _log.exception(e.developer_message)
        e.user_message = 'Failed on validate apple receipt in payment process'
        raise e
    except Exception as e:
        _log.exception(str(e))
        raise InternalServerError(
            user_message='Failed on validate apple receipt in payment process',
            developer_message=str(e)
        )


def get_product_id(order, session):
    client = session.query(Client).get(order.client_id)
    if not client:
        raise UnprocessableEntity(
            developer_message='Invalid client in order, order client id: {}'.format(order.client_id)
        )
    product_id = '{}{}'.format(client.product_id, order.tier_code)
    return product_id


def validate_order_vs_apple_receipt(product_id, in_app_data, order, apple_iap_data, session, environment=None):

    # white label purchase (ex: client 45 scoop_popular) will have different product id vs Scoop purchase (client 1)
    #   client 45: product id: com.appsfoundry.popular
    #   client 1: product id: com.appsfoundry.scoop
    #   the purchase receipt transaction will be different for different product

    order_lines = order.order_lines.all()
    # in apple purchase, 1 order = 1 lines, as of now
    offer_type = order_lines[0].offer.offer_type_id

    trx_data = get_valid_transaction(in_app_data, product_id)

    if trx_data:
        # valid transaction found. log it and return valid transaction

        # convert apple response date "2015-08-06 07:14:31 Etc/GMT" to valid python datetime
        web_order_line_item_id = trx_data.get('web_order_line_item_id')

        # get purchase date from original purchase date or purchase date
        purchase_date = get_purchase_date(product_id, trx_data, web_order_line_item_id)

        expires_date = helpers.to_date(trx_data.get('expires_date', None))
        transaction_id = trx_data.get('transaction_id')
        original_transaction_id = trx_data.get('original_transaction_id')

        # check if it's a repurchase or not
        is_repurchase = assert_repurchase(purchase_date, transaction_id, web_order_line_item_id, session, product_id)

        # save original trx id, trx id, web order line item id for repurchase checking in the future
        apple_iap_data.status = 0
        apple_iap_data.transaction_id = transaction_id
        apple_iap_data.original_transaction_id = original_transaction_id
        apple_iap_data.web_order_line_item_id = web_order_line_item_id
        apple_iap_data.purchase_date = purchase_date
        apple_iap_data.original_purchase_date = helpers.to_date(trx_data.get('original_purchase_date', None))
        apple_iap_data.expires_date = expires_date
        apple_iap_data.is_processed = False
        apple_iap_data.renewal_status = 1 if not is_repurchase else 0
        # since unencrypted data from receipt ios7 is too big, we don't store in db anymore, query to apple if needed
        # apple_iap_data.receipt = None
        apple_iap_data.receipt_version = ReceiptVersion.ios7_unified_receipt.value

        session.add(apple_iap_data)
        session.commit()

        # check if it's receipt from sandbox data (is_sandbox)
        # used specifically by Apple Reviewer,
        # they will used sandbox account with apps that already used COR Production API
        # so we need to retry again to apple sandbox to get valid receipt
        is_sandbox = "sandbox" in environment.lower()

        validation_results = {
            "purchase_date": purchase_date,
            "original_transaction_id": trx_data.get('original_transaction_id'),
            "expires_date": expires_date,
            "is_trial": trx_data.get('is_trial_period', False),
            "is_sandbox": is_sandbox,
            "is_restore": is_repurchase,
        }

        return validation_results
    else:
        # if failed:
        raise UnprocessableEntity(
            user_message="Failed on validate apple receipt in payment process",
            developer_message='Cannot find valid receipt for product id: {}'.format(product_id))


def get_purchase_date(product_id, trx_data, web_order_line_item_id):
    if not web_order_line_item_id and current_app.config['CONSUMABLE_MARK'] not in product_id:
        # not subscription and non consumable
        #   only for single non consumable get from original purchase date
        #   case from previous receipt (bug in apple?):
        #     if single and non consumable, user clicked purchase at 1 jan 2016, purchase date = 1 jan 2016
        #       then at 3 feb 2016, user purchase again for the same item, there will be no new purchase in itunes
        #           user will not billed (pay nothing)
        #       it will marked as repurchase, same trx id, but purchase date will be updated to 3 feb 2016
        #        here, we need to capture the original purchase date
        purchase_date = helpers.to_date(trx_data.get('original_purchase_date', trx_data.get('purchase_date')))
    else:
        purchase_date = helpers.to_date(trx_data.get('purchase_date'))
    return purchase_date


def assert_repurchase(purchase_date, transaction_id, web_order_line_item_id, session, product_id):
    # check if it's a repurchase or not
    #   if it's a repurchase, flag it, no income from user/no payment from apple, only revalidation
    # web_order_line_item_id --> help to make sure for the latest subscriptions
    previous_purchase = session.query(PaymentAppleIAP).filter_by(
        transaction_id=transaction_id,
        web_order_line_item_id=web_order_line_item_id,
        product_id=product_id
    ).join(PaymentAppleIAP.payment).filter(Payment.payment_status == PAYMENT_BILLED).first()
    if previous_purchase or (get_utc_nieve() - purchase_date).days > 30:
        # if previous_purchase found --> mark as repurchase
        # if purchase date is old (less then 30 days ago) --> and even if history payment apple iap not found
        #     >>> mark as repurchase
        is_repurchase = True
    else:
        is_repurchase = False

    return is_repurchase


def get_valid_transaction(in_app_data, product_id,
                          original_transaction_id=None, previous_expires_date=None):
    trx_data = None
    reversed_index = range(len(in_app_data), 0, -1)
    dummy_date_for_single = get_utc_nieve() + timedelta(days=1)
    # validate product id to each transaction in in app apple receipts
    #   we try to search from the end of the list (latest transaction)
    for index in reversed_index:
        trx = in_app_data[index - 1]

        # get the valid transaction. for subscription, get the latest purchase based on expires_date
        trx_data = validate_product_id_in_transaction(
            product_id, trx, dummy_date_for_single, original_transaction_id, previous_expires_date)
        if trx_data:
            break
    return trx_data


def validate_product_id_in_transaction(product_id, trx, dummy_date_for_single,
                                       original_transaction_id=None, previous_expires_date=None):
    """ Validate a transaction in apple receipt-in_app vs product id

    :param `str` product_id:
    :param trx:
    :param dummy_date_for_single:
    :param original_transaction_id:
    :param previous_expires_date: for renewal process: previous expires date data, for new purchase: utc now
    :return:
    """
    # if it's single, expires_date is None, so we will set it to tomorrow (to make it valid) --> dummy_date_for_single
    # if it's subscriptions, the valid purchase is the one with expires_date is sometime in the future.

    # expires_date is in UTC (+00:00), expires_date_ms is epoch time in milliseconds
    # trx_expires_date = datetime.utcfromtimestamp(trx.get("expires_date_ms", None) / 1000) \
    #     if trx.get("expires_date_ms", None) \
    #     else dummy_date_for_single
    previous_expires_date = previous_expires_date or (get_utc_nieve() - timedelta(minutes=4))

    _log.debug(trx.get('original_transaction_id') + ',' + trx.get('transaction_id') + ','
               + trx.get('expires_date', '') + ',' + trx.get('web_order_line_item_id', ''))
    _log.debug(previous_expires_date)

    expires_date = trx.get('expires_date_formatted', trx.get('expires_date', None))

    trx_expires_date = helpers.to_date(expires_date) if expires_date \
        else dummy_date_for_single
    if trx and trx.get('product_id') == product_id \
            and trx_expires_date > previous_expires_date \
            and trx.get('cancellation_date') is None \
            and (not original_transaction_id
                 or str(original_transaction_id) == str(trx.get('original_transaction_id'))):
        # trx valid only for:
        #   product id is same
        #   expires date is in the future
        #   cancellation date is None
        #       (original_transaction_id not provided [for purchase validation process] OR
        #         original_transaction_id = original_transaction_id in current trx [for renewal process])
        return trx
    else:
        return None
