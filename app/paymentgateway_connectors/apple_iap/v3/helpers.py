import json
from datetime import datetime
from httplib import OK, GATEWAY_TIMEOUT

import requests
from flask import current_app
from flask_appsfoundry.exceptions import ScoopApiException, UnprocessableEntity
from marshmallow.utils import isoformat
from requests import ConnectionError
from werkzeug import exceptions as wzexc
from flask_babel import gettext as _, ngettext

from app import db
from app.messaging.email import HtmlEmail, get_smtp_server
from app.orders.deliver_items import get_items_from_subscription
from . import _log, models

from app.items.previews import preview_s3_covers

SCOOP_PRODUCT_SPLITTER = 'appsfoundry.scoop'


def filter_false(predicate, iterable):
    # filterfalse(lambda x: x%2, range(10))
    if predicate is None:
        predicate = bool
    for x in iterable:
        if not predicate(x):
            yield x


def generate_response_per_item(item):
    return {
        "id": item.id,
        "href": item.api_url,
        "title": item.name,
        "image": {
            "title": "Cover Image",
            "href": preview_s3_covers(item, 'image_highres', True)
            # "href": current_app.config['MEDIA_BASE_URL'] + item.image_highres
        },
        "thumbnail": {
            "title": "Thumbnail Image",
            "href": preview_s3_covers(item, 'thumb_image_highres', True)
            #"href": current_app.config['MEDIA_BASE_URL'] + item.thumb_image_highres
        },
        "is_age_restricted": item.parentalcontrol_id == 2,
        "content_type": item.content_type,
        "item_type": item.item_type,
        "edition_code": item.edition_code,
        "issue_number": item.issue_number,
        "release_date": isoformat(item.release_date),
        "brand": {
            "id": item.brand.id,
            "title": item.brand.name,
            "href": item.brand.api_url,
            # "vendor": {
            #     "id": item.brand.vendor.id,
            #     "title": item.brand.vendor.name,
            #     "href": item.brand.vendor.api_url
            # }
        }
    }


def get_apple_url():
    return current_app.config['APPLE_IAP_HOST_LIVE'] if current_app.config.get('LIVE_SERVER', True) \
        else current_app.config['APPLE_IAP_HOST_SANDBOX']


def get_receipt_from_itunes(receipt_data=None, http_client=requests):
    base_uri = get_apple_url()
    try:

        request_body = json.dumps({
            'receipt-data': receipt_data,
            'password': current_app.config['APPLE_IAP_SHARED_SECRET']
        })
        headers = {'Cache-Control': 'no-cache',
                   'Content-Type': 'application/json',
                   'Accept': 'application/json'}
        response = http_client.post(
            url="{}{}".format(base_uri, current_app.config['URL_VERIFY_RECEIPT']),
            data=request_body,
            headers=headers
        )

        is_sandbox = False
        if response.status_code == OK and response.json().get('status', None) == INVALID_RECEIPT_PRODUCTION:
            # used specifically by Apple Reviewer,
            # they will used sandbox account with apps that already used COR Production API
            # so we need to retry again to apple sandbox to get valid receipt
            base_uri = "{}{}".format(current_app.config['APPLE_IAP_HOST_SANDBOX'],
                                        current_app.config['URL_VERIFY_RECEIPT'])
            response = http_client.post(
                url=base_uri,
                data=request_body,
                headers=headers
            )
            is_sandbox = True

        if response.status_code == OK:
            return response.json(), is_sandbox
        else:
            msg = 'Failed to verify receipt to apple api. Url: {url}, Response: {resp}'.format(
                url=base_uri, resp=response)
            raise AppleRequestError(
                user_message=msg,
                developer_message=msg
            )
    except ConnectionError as e:
        msg = 'Failed to verify receipt to apple api. Url: {url}, Error: {err_message}'.format(
            url=base_uri, err_message=str(e.message))
        raise AppleConnectionError(
            user_message=msg,
            developer_message=msg
        )
    except AppleRequestError as e:
        raise e
    except Exception as e:
        msg = 'Failed to verify receipt to apple api. Url: {url}, Error: {err_message}'.format(
            url=base_uri, err_message=str(e))
        raise AppleConnectionError(
            user_message=msg,
            developer_message=msg
        )


class AppleConnectionError(ScoopApiException):
    def __init__(self, **kwargs):
        super(AppleConnectionError, self).__init__(
            code=GATEWAY_TIMEOUT,
            error_code=GATEWAY_TIMEOUT,
            user_message=kwargs.get("user_message",
                                    _("This server did not receive a timely response from an upstream server")),
            developer_message=kwargs.get("developer_message",
                                         "This server did not receive a timely response from an upstream server")
        )


def validate_receipt_json(receipt_json):

    validate_receipt_json_status(receipt_json)

    latest_receipt_info = receipt_json.get('latest_receipt_info', None)

    if receipt_json.get('receipt', None) and receipt_json.get('receipt').get('in_app', []):
        in_app_data = receipt_json.get('receipt').get('in_app', [])
    else:
        in_app_data = None

    receipt_data = latest_receipt_info if latest_receipt_info else in_app_data

    if not receipt_data:
        error_msg = 'In-App data and Latest Receipt Info is missing in json response from Apple itunes. '
        raise UnprocessableEntity(
            user_message=error_msg,
            developer_message=error_msg
        )
    else:
        return receipt_data


def validate_receipt_json_status(receipt_json):
    receipt_status = receipt_json.get('status', None)
    if receipt_status not in ["0", 0]:
        error_msg = 'Invalid Status, Receipt status = {status} - {status_desc} '.format(
            status=receipt_status,
            status_desc=RECEIPT_STATUS.get(receipt_status, None))
        raise UnprocessableEntity(
            user_message=error_msg,
            developer_message=error_msg
        )


INVALID_RECEIPT_PRODUCTION = 21007


RECEIPT_STATUS = {
    21000: "The App Store could not read the JSON object you provided.",
    21002: "The data in the receipt-data property was malformed or missing.",
    21003: "The receipt could not be authenticated.",
    21004: "The shared secret you provided does not match the shared secret on file for your account. "
           "Only returned for iOS 6 style transaction receipts for auto-renewable subscriptions.",
    21005: "The receipt server is not currently available.",
    21006: "This receipt is valid but the subscription has expired. When "
           "this status code is returned to your server, the receipt data is also decoded "
           "and returned as part of the response. "
           "Only returned for iOS 6 style transaction receipts for auto-renewable subscriptions.",
    INVALID_RECEIPT_PRODUCTION: "This receipt is from the test environment, but it was sent to the "
           "production environment for verification. Send it to the test environment instead.",
    21008: "This receipt is from the production environment, but it was sent to "
           "the test environment for verification. Send it to the production environment instead.",
}


def create_new_log_receipt_data(receipt_data, activity_type, user_id,
                                device_registration_id=None, session=None):
    session = session or db.session()
    log_receipt = models.LogReceipt(
        user_id=user_id,
        receipt_data=receipt_data,
        status=models.LogReceipt.StatusProcess.in_progress,
        activity_type=activity_type,
        device_registration_id=device_registration_id,
    )
    session.add(log_receipt)
    session.commit()
    return log_receipt


def log_error(e, log_data, session, user_email=None, device_registration_id=None):
    log_data.status = models.LogReceipt.StatusProcess.error
    log_data.error = json.dumps({
        'user_message': 'Failed in {} process'.format(log_data.activity_type.value),
        'developer_message': e,
    })
    session.add(log_data)
    session.commit()

    send_restore_notification(user_email, device_registration_id, log_data.status, restore_id=log_data.id)


class AppleRequestError(ScoopApiException):
    def __init__(self, **kwargs):
        super(AppleRequestError, self).__init__(
            code=GATEWAY_TIMEOUT,
            error_code=GATEWAY_TIMEOUT,
            user_message=kwargs.get("user_message",
                                    _("This server did not receive a timely response from an upstream server")),
            developer_message=kwargs.get("developer_message",
                                         "This server did not receive a timely response from an upstream server")
        )


class GatewayTimeout(ScoopApiException):
    def __init__(self, **kwargs):
        super(GatewayTimeout, self).__init__(
            code=GATEWAY_TIMEOUT,
            error_code=GATEWAY_TIMEOUT,
            user_message=kwargs.get("user_message",
                                    _("This server did not receive a timely response from an upstream server")),
            developer_message=kwargs.get("developer_message",
                                         "This server did not receive a timely response from an upstream server")
        )


def send_restore_notification(user_email, device_registration_id, status, restore_id):
    if user_email:
        send_email_restore(user_email, status, restore_id=restore_id)
    if device_registration_id:
        send_push_notification_restore(device_registration_id, status, restore_id=restore_id)


def get_notification_messages(status):
    if status == models.LogReceipt.StatusProcess.error:
        return EmailMessages(
            status_message='gagal',
            status_message_english='failed',
            additional_message='',
            additional_message_english='',
        )
    elif status == models.LogReceipt.StatusProcess.completed:
        return EmailMessages(
            status_message='berhasil',
            status_message_english='success',
            additional_message='',
            additional_message_english='', )


class EmailMessages(object):

    def __init__(self, status_message, additional_message, status_message_english, additional_message_english):
        self.status_message = status_message
        self.status_message_english = status_message_english
        self.additional_message = additional_message
        self.additional_message_english = additional_message_english


def send_email_restore(user_email, status, restore_id):

    email_messages = get_notification_messages(status)
    try:
        server = get_smtp_server()
        msg = HtmlEmail(
            sender='no-reply@gramedia.com',
            to=[user_email, ],
            subject='Gramedia Digital - Restore Purchase Data (Apple IOS)',
            html_template='email/apple/restore.html',
            plaintext_template='email/apple/restore.txt',
            user_email=user_email,
            messages=email_messages
        )

        server.send_message(msg)

    except Exception as e:
        _log.exception(
            "Apple IAP Restore: Failed to send Apple Restore notification to email {user_email}. "
            "Log/restore id: {restore_id}."
            "Error: {error_message}".format(
                user_email=user_email,
                restore_id=restore_id,
                error_message=str(e)))
        return False


def get_push_service():
    from pyfcm import FCMNotification
    api_key = current_app.config.get('FIREBASE_KEY_API', None)
    return FCMNotification(api_key=api_key) if api_key else None


def send_push_notification_restore(device_registration_id, status, restore_id, push_service=None):
    """ send push notification via Firebase Cloud Messaging (FCM)
    :param `str` device_registration_id:
    :param `models.LogReceipt.StatusProcess` status:
    :param `int` restore_id: an identifier from task/LogReceipt data
    :param push_service: an instance of pyfcm.FCMNotification
    :return:
    """
    # https://github.com/olucurious/PyFCM
    if device_registration_id:
        messages = get_notification_messages(status)
        try:
            push_service = push_service or get_push_service()

            if push_service:
                message_title = "Scoop restore update"
                message_body = "Proses scoop-restore telah selesai dengan status: {}".format(messages.status_message)
                result = push_service.notify_single_device(
                    registration_id=device_registration_id,
                    message_title=message_title,
                    message_body=message_body)
                _log.debug('Apple IAP Restore: Push notification result: {}'.format(result))

                return result

        except Exception as e:
            _log.exception(
                "Apple IAP Restore: Failed to send Apple Restore notification to email {device_registration_id}. "
                "Log/restore id: {restore_id}."
                "Error: {error_message}".format(
                    device_registration_id=device_registration_id,
                    restore_id=restore_id,
                    error_message=str(e))
            )
            return False


def to_date(apple_date):
    date = apple_date.split() if apple_date else None
    return datetime.strptime('{} {}'.format(date[0], date[1]), '%Y-%m-%d %H:%M:%S') if date else None


def get_item_subscriptions(offer, session, purchase_date):
    """

    :param offer:
    :param session:
    :param purchase_date:
    :return:
    """
    # get offer subscription data
    offer_subscription = offer.subscription.first()
    offer_brands = offer.brands.all()
    items = []
    for brand in offer_brands:
        brand_id = brand.id
        items = get_items_from_subscription(session, offer_subscription, purchase_date, brand_id, items)

    return items


