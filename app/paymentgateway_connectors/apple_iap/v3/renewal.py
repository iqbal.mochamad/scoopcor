from datetime import timedelta

from app import db
from app.helpers import generate_invoice
from app.orders.models import Order, OrderDetail, OrderLine
from app.orders.deliver_items import deliver_user_buffet, send_receipt_mail, deliver_user_subscription
from app.paymentgateway_connectors.apple_iap.choices.renewal_status import COMPLETE, NEW, ERROR
from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP, ReceiptVersion, RenewalLogProcessings
from app.payments.models import Payment
from app.users.users import User
from app.utils.datetimes import get_local_nieve, get_utc_nieve
from . import helpers, _log, payment_validation


def process_renewal(payment_iap, session=None):
    session = session or db.session

    start_time = get_local_nieve()
    validation_result = validate_renewal(payment_iap=payment_iap, session=session)

    if validation_result.is_valid:
        with session.no_autoflush:
            result, error_msg = create_order_payment(payment_iap, validation_result, session, start_time)
            if result:
                payment_iap.is_processed = True
                payment_iap.renewal_status = COMPLETE
                payment_iap.save(session)
                return "renewal for original trx id {} COMPLETED.".format(payment_iap.original_transaction_id)
            else:
                payment_iap.renewal_status = ERROR
                payment_iap.save(session)
                return "renewal for original trx id {} ERROR {}".format(payment_iap.original_transaction_id, ERROR)
    else:
        # reset status to NEW again, to recheck later
        payment_iap.renewal_status = NEW
        payment_iap.save(session)
        if validation_result.error_msg:
            log_renewal_error(session, payment_iap, start_time, error_msg=validation_result.error_msg)
            return "renewal for original trx id {} NOT VALID {}".format(
                payment_iap.original_transaction_id, validation_result.error_msg)
        else:
            return "renewal for original trx id {} NOT VALID not Error".format(payment_iap.original_transaction_id)


def reset_renewal_status(payment_iap, session):

    payment_iap.renewal_status = NEW
    payment_iap.save(session)


def log_renewal_error(session, payment_iap, start_time, error_msg):
    log = RenewalLogProcessings(
        original_transaction_id=payment_iap.original_transaction_id,
        web_order_line_id=payment_iap.web_order_line_item_id,
        start_process=start_time,
        end_process=get_local_nieve(),
        error=error_msg,
        apple_receipt=None,
        apple_response=None,
        is_patch=False,
        is_notice=False)
    session.add(log)
    session.commit()


class RenewalValidationResult(object):

    def __init__(self, is_valid, is_sandbox=False, is_trial=False,
                 original_transaction_id=None, transaction_id=None,
                 web_order_line_item_id=None, purchase_date=None, expires_date=None,
                 latest_receipt=None, receipt_json=None):
        self.is_valid = is_valid
        self.is_sandbox = is_sandbox
        self.is_trial = is_trial
        self.original_transaction_id = original_transaction_id
        self.transaction_id = transaction_id
        self.web_order_line_item_id = web_order_line_item_id
        self.purchase_date = purchase_date
        self.expires_date = expires_date
        self.latest_receipt = latest_receipt
        self.receipt_json = receipt_json
        self.error_msg = None


def validate_renewal(payment_iap, session=None):

    step = "A. Validate"
    validation_result = RenewalValidationResult(is_valid=False)
    try:
        session = session or db.session

        receipt_data = payment_iap.receipt_data
        product_id = payment_iap.product_id
        original_transaction_id = payment_iap.original_transaction_id
        previous_expires_date = payment_iap.expires_date or get_utc_nieve()

        step = 'A. Validate - 1. Get Receipt from itunes'
        # get un encrypted receipt data from apple
        receipt_json, is_sandbox = helpers.get_receipt_from_itunes(receipt_data)

        if payment_iap.receipt_version >= ReceiptVersion.ios7_unified_receipt.value:
            # validate receipt data from apple response
            step = 'A. Validate - 2. Validate receipt status and get valid in_app_data from apple receipts'
            in_app_data = helpers.validate_receipt_json(receipt_json)

            # get valid transaction for this product id and original_transaction id
            step = 'A. Validate - 3. Get valid transaction from apple receipts in in_app_data'
            trx_data = payment_validation.get_valid_transaction(
                in_app_data, product_id, original_transaction_id, previous_expires_date=previous_expires_date)
        else:
            # receipt IOS6 or previous version
            trx_data = validate_ios6_transaction(receipt_json, product_id,
                                                 original_transaction_id, previous_expires_date)

        if trx_data:
            # valid transaction found

            step = 'A. Validate - 4. All valid, prepare return data'
            # convert apple response date "2015-08-06 07:14:31 Etc/GMT" to valid python datetime
            purchase_date = helpers.to_date(trx_data.get('purchase_date'))

            if payment_iap.receipt_version >= ReceiptVersion.ios7_unified_receipt.value:
                expires_date = helpers.to_date(trx_data.get('expires_date'))
            else:
                # receipt IOS6 or previous version
                expires_date = helpers.to_date(trx_data.get('expires_date_formatted'))

            transaction_id = trx_data.get('transaction_id')

            web_order_line_item_id = trx_data.get('web_order_line_item_id')
            # check if this latest transaction if a repurchase or not / already exists or not
            is_repurchase = payment_validation.assert_repurchase(
                purchase_date, transaction_id, web_order_line_item_id, session, product_id)
            if is_repurchase:
                # this latest transaction id already exists and billed in our payment
                #   not a new renewal transaction
                return validation_result

            # return valid data
            validation_result.is_valid = True
            validation_result.is_trial = trx_data.get('is_trial_period', False)
            validation_result.original_transaction_id = original_transaction_id
            validation_result.transaction_id = trx_data.get('transaction_id')
            validation_result.web_order_line_item_id = trx_data.get('web_order_line_item_id')
            validation_result.purchase_date = purchase_date
            validation_result.expires_date = expires_date
            validation_result.is_sandbox = "sandbox" in receipt_json.get('environment', "").lower() or is_sandbox
            validation_result.latest_receipt = receipt_json.get('latest_receipt', receipt_data)
            validation_result.receipt_json = receipt_json
            return validation_result
        else:
            return validation_result

    except Exception as e:
        msg = """Renewal failed for previous payment IAP id: {payment_iap_id}.
        step: {step}'
        Error {msg}""".format(
            payment_iap_id=payment_iap.id,
            step=step,
            msg=e.developer_message if hasattr(e, 'developer_message') else str(e))
        validation_result.error_msg = msg
        _log.exception(msg)
        return validation_result


def validate_ios6_transaction(receipt_json, product_id, original_transaction_id, previous_expires_date):

    helpers.validate_receipt_json_status(receipt_json)

    trx_data = payment_validation.validate_product_id_in_transaction(
        product_id=product_id,
        trx=receipt_json.get('latest_receipt_info'),
        dummy_date_for_single=get_utc_nieve() + timedelta(days=1),
        original_transaction_id=original_transaction_id,
        previous_expires_date=previous_expires_date
    )
    return trx_data


def create_order_payment(payment_iap, validation_result, session, start_time):
    step = "B. Save Order"
    try:
        previous_order = payment_iap.payment.order

        user = session.query(User).get(previous_order.user_id)
        user_email = user.email if user and user.email else []

        # create new order, copy from previous order
        order = Order(
            order_number=generate_invoice(),
            total_amount=previous_order.total_amount,
            final_amount=previous_order.final_amount,
            user_id=previous_order.user_id,
            client_id=previous_order.client_id,
            partner_id=previous_order.partner_id,
            order_status=previous_order.order_status,
            currency_code=previous_order.currency_code,
            paymentgateway_id=previous_order.paymentgateway_id,
            tier_code=previous_order.tier_code,
            platform_id=previous_order.platform_id,
            temporder_id=None,
            order_detail=[OrderDetail(
                temporder_id=None,
                user_email=user_email,
                user_name=user.username if user else None,
            )],
            is_active=True,
            remote_order_number=None,
            point_reward=0,
            is_renewal=True,
        )
        session.add(order)

        # create new order lines, copy from previous order lines
        for previous_line in previous_order.order_lines:
            line = OrderLine(
                name=previous_line.name,
                offer=previous_line.offer,
                is_active=True,
                is_free=previous_line.is_free,
                is_discount=False,
                campaign_id=None,
                user_id=previous_line.user_id,
                quantity=previous_line.quantity,
                orderline_status=OrderLine.Status.complete.value,
                currency_code=previous_line.currency_code,
                price=previous_line.price,
                final_price=previous_line.final_price,
                localized_currency_code=previous_line.localized_currency_code,
                localized_final_price=previous_line.localized_final_price,
            )
            order.order_lines.append(line)
            session.add(line)

        # create new payment, copy from previous payment
        payment = Payment(
            order=order,
            user_id=payment_iap.payment.user_id,
            paymentgateway_id=payment_iap.payment.paymentgateway_id,
            currency_code=payment_iap.payment.currency_code,
            amount=payment_iap.payment.amount,
            payment_status=Payment.Status.billed.value,
            payment_datetime=validation_result.purchase_date,
            is_active=True,
            is_test_payment=validation_result.is_sandbox,
            is_trial=validation_result.is_trial,
            merchant_params=None,
        )
        session.add(payment)

        # create new payment apple iap data, copy from previous data
        apple_iap_data = PaymentAppleIAP(
            payment=payment,
            product_id=payment_iap.product_id,
            receipt_data=validation_result.latest_receipt,
        )
        apple_iap_data.status = 0
        apple_iap_data.transaction_id = validation_result.transaction_id
        apple_iap_data.original_transaction_id = payment_iap.original_transaction_id
        apple_iap_data.web_order_line_item_id = validation_result.web_order_line_item_id
        apple_iap_data.purchase_date = validation_result.purchase_date
        apple_iap_data.original_purchase_date = payment_iap.original_purchase_date
        apple_iap_data.expires_date = validation_result.expires_date
        apple_iap_data.is_processed = False
        apple_iap_data.renewal_status = 1
        apple_iap_data.receipt_version = payment_iap.receipt_version
        # since unencrypted data from receipt ios7 is too big, we don't store in db anymore, query to apple if needed
        apple_iap_data.receipt = None
        session.add(apple_iap_data)

        # update previous payment iap to Completed
        payment_iap.renewal_status = 4
        payment_iap.is_processed = True
        session.add(payment_iap)

        # commit order-payment first
        step = "B. Save Order - 1. Save Order, Order Lines, and Payment"
        session.commit()

        step = "B. Save Order - 2. Deliver user subscription/buffet data"
        # deliver user subscription data/user buffet
        deliver_user_subscription_buffet(order, validation_result, session)
        session.commit()

        # send receipt email
        step = "B. Save Order - 3. Send receipt email"
        if order:
            send_receipt_mail(order, user_email)

        return True, None
    except Exception as e:
        msg = "Renewal failed for previous payment IAP id: {payment_iap_id}.\n" \
              "step: {step}\n" \
              "Error {msg}".format(
                payment_iap_id=payment_iap.id,
                step=step,
                msg=e.developer_message if hasattr(e, 'developer_message') else str(e))
        _log.exception(msg)
        session.rollback()
        log_renewal_error(session, payment_iap, start_time, error_msg=msg)
        return False, msg


def deliver_user_subscription_buffet(order, validation_result, session):
    for line in order.order_lines:
        offer = line.offer
        if offer.offer_type_id == offer.Type.buffet.value:
            deliver_user_buffet(order.user_id, line.id, offer,
                                validation_result.expires_date,
                                validation_result.is_trial,
                                validation_result.original_transaction_id,
                                session)
        elif offer.offer_type_id == offer.Type.subscription.value:
            deliver_user_subscription(order, line, offer, validation_result.purchase_date, session)


