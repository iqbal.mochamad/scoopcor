import httplib, requests, json, ast
from datetime import datetime, timedelta

from app.helpers import conflict_message, payment_logs

from app.items.models import Item
from app.orders.api import OrderCheckout, OrderComplete
from app.orders.models import Order, OrderDetail, OrderLine, OrderTemp, OrderLineTemp
from app.offers.models import OfferPlatform, Offer
from app.master.choices import IOS, ANDROID, GETSCOOP
from app.paymentgateway_connectors.apple_iap.v3.renewal import process_renewal

from app.payments.api import PaymentValidateApi
from app.payments.choices.gateways import APPLE
from app.payments.models import Payment
from app.payments.signature_generator import GenerateSignature
from app.payments.helpers import PaymentConstruct

from app.paymentgateway_connectors.apple_iap import itunes_product_type
from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP, AppleReceiptIdentifiers, RenewalLogProcessings, \
    ReceiptVersion
from app.paymentgateway_connectors.apple_iap.apple_iap import AppleIAPCheckout
from app.paymentgateway_connectors.apple_iap.helpers import AppleIAPHelpers
from app.paymentgateway_connectors.choices.renewal_status import *

from app.orders.order_checkout_platform import OrderCheckoutConstructPlatform
from app.orders.order_checkout_base import OrderCheckoutConstructBase
from app.orders.order_confirm import OrderConfirmConstruct

from flask_restful import Resource, Api, reqparse, inputs
from flask import Response, request

from sqlalchemy import desc
from app import app, db


class AppleIAPRenewal(object):

    def __init__(self, divider=None, original_transaction_id=None, backdays=None):
        self.divider = divider
        self.original_transaction_id = original_transaction_id
        # how many back days we recheck transaction for renewal,
        #   default is 14 days, we check trx that expired from 14 days back
        self.backdays = backdays or 14
        self.data_renewal = []
        self.orderlines_data = []
        self.master_log = None
        self.new_receipt_data = None
        self.tier_code = None
        #self.allow_auto_purchase = True

        self.active_payments = None

    def json_success(self):
        return Response(json.dumps({'renewal': self.data_renewal}),
            status=httplib.MULTI_STATUS, mimetype='application/json')

    def create_logs(self, renewal=None, active_renewal=None, apple_response=None):
        try:
            is_patch = False
            if self.original_transaction_id:
                is_patch = True

            # find log todays
            log = RenewalLogProcessings.query.filter_by(
                original_transaction_id = renewal.original_transaction_id,
                web_order_line_id = renewal.web_order_line_item_id
            ).order_by(RenewalLogProcessings.id).first()

            if not log:
                log = RenewalLogProcessings(
                    original_transaction_id = renewal.original_transaction_id,
                    web_order_line_id = renewal.web_order_line_item_id,
                    start_process = datetime.now(),
                    apple_receipt = renewal.receipt_data.decode('unicode_escape'),
                    is_patch = is_patch
                )
                log.save()

            if apple_response:
                log.apple_response = apple_response
                log.save()

            self.master_log = log

        except Exception, e:
            pass

    def error_logs(self, error=None, code=None, renewal=None, verify_result=None):
        # count as error 1. this will notice by emails if error count > 3
        self.master_log.error = error
        self.master_log.end_process = datetime.utcnow()
        self.master_log.save()

        tmp = {}
        tmp['status'] = code
        tmp['error'] = error
        tmp['original_transaction_id'] = renewal.original_transaction_id

        renewal.renewal_status = 3
        renewal.is_processed = True
        renewal.save()

        return tmp

    def construct_order_line(self):
        try:
            # reset orderlines
            self.orderlines_data = []

            offer_platform = OfferPlatform.query.filter_by(
                tier_code=self.tier_code, platform_id=IOS).first()

            if not offer_platform:
                return False, 'No Offer platform founds, tier code : %s' % self.tier_code

            offer = Offer.query.filter_by(id=offer_platform.offer_id).first()
            if not offer:
                return False, 'No offer active founds, offer_id : %s' % offer_platform.offer_id

            ord_lines = {}
            ord_lines['offer_id'] = offer.id
            ord_lines['quantity'] = 1  # hardcode
            # ord_lines['name'] = offer.name
            self.orderlines_data.append(ord_lines)

            return True, 'OK'

        except Exception, e:
            return False, e

    def auto_purchase(self, renewal=None, active_renewal=None):
        '''
            There's 3 steps in auto_purchase
            1. order checkout
            2. order confirm
            3. payment validate
        '''
        try:
            # checkout construct
            check_args = {}
            check_args['user_id'] = active_renewal.user_id
            check_args['payment_gateway_id'] = APPLE
            check_args['client_id'] = active_renewal.client_id
            check_args['platform_id'] = IOS
            check_args['order_lines'] = self.orderlines_data

            # avoid all renewal subs cut by active promos
            check_args['is_renewal'] = True

            checkout = OrderCheckoutConstructPlatform(check_args, 'POST').construct()

            if checkout.status_code not in [httplib.CREATED, httplib.OK]:
                return False, "Order checkout Failed : %s" % checkout.data

            # loads in json files checkout response
            data_checkout = json.loads(checkout.data)

            session = db.session()
            patch_renewal = False
            if renewal and renewal.original_purchase_date < datetime(2016, 11, 1):
                order_temp = session.query(OrderTemp).get(data_checkout.get('temp_order_id', None))
                order_line_temp = session.query(OrderLineTemp).filter_by(order_id=order_temp.id).all()
                list_offer_id_in_detail = [line.offer_id for line in order_line_temp]
                if order_line_temp and 43103 in list_offer_id_in_detail:
                    # patch for renewal ios premium, use old price
                    patch_renewal = True

            if patch_renewal and order_temp and order_line_temp:
                price_usd = 3.99
                price_idr = 59000
                order_temp.price_usd = price_usd
                order_temp.final_price_usd = price_usd
                order_temp.price_idr = price_idr
                order_temp.final_price_idr = price_idr
                order_temp.price_point = 0
                order_temp.final_price_point = 0
                session.add(order_temp)
                for line in order_line_temp:
                    line.price_usd = price_usd
                    line.final_price_usd = price_usd
                    line.price_idr = price_idr
                    line.final_price_idr = price_idr
                    line.price_point = 0
                    line.final_price_point = 0
                    session.add(line)
                session.commit()
                if data_checkout.get('currency_code', 'USD') == 'IDR':
                    final_price = price_idr
                else:
                    final_price = '{}'.format(price_usd)
            else:
                final_price = data_checkout.get('final_amount', None)

            # confirm construct
            confirm_args = {}
            confirm_args['user_id'] = active_renewal.user_id
            confirm_args['payment_gateway_id'] = APPLE
            confirm_args['client_id'] = active_renewal.client_id
            confirm_args['currency_code'] = data_checkout.get('currency_code', 'USD')
            confirm_args['final_amount'] = final_price
            confirm_args['order_number'] = data_checkout.get('order_number', None)
            confirm_args['temp_order_id'] = data_checkout.get('temp_order_id', None)

            confirm = OrderConfirmConstruct(confirm_args, 'POST').construct()
            if checkout.status_code not in [httplib.CREATED, httplib.OK]:
                return False, "Order confirm Failed : %s" % confirm.data

            # loads in json files confirm response
            data_confirm = json.loads(confirm.data)

            order_id = data_confirm.get('order_id', None)
            signature = GenerateSignature(order_id=order_id).construct()

            # validate payments
            validate_args = {}
            validate_args['order_id'] = order_id
            validate_args['signature'] = signature
            validate_args['user_id'] = active_renewal.user_id
            validate_args['payment_gateway_id'] = APPLE
            validate_args['receipt_data'] = self.new_receipt_data
            validate_args['api'] = 'v1/payments/validate'
            validate_args['method'] = 'POST'

            validate = PaymentConstruct(args=validate_args).initialize_transaction()
            if validate.status_code not in [httplib.CREATED, httplib.OK]:
                return False, "Validate confirm Failed : %s" % validate.data

            # flag order detail as renewal
            order_detail = session.query(OrderDetail).filter_by(order_id=order_id).first()
            order_detail.note = '{}:{}'.format(
                'renewal', order_detail.note
            )
            session.add(order_detail)
            session.commit()
            return True, 'OK'

        except Exception as e:
            return False, e

    def reset_renewal(self, renewal=None, active_payments=None, session=None):

        date_now = datetime.utcnow().date() + timedelta(days=1)

        # avoid receipt H+1 errors, H+1 if error always reset to NEW
        if active_payments.expired_date.date() == date_now:
            renewal.renewal_status = NEW
            renewal.is_processed = False
            renewal.save(session)

            if active_payments:
                active_payments.is_processed = False
                active_payments.save(session)

    def validate_renewal_data(self):
        try:
            start_date = datetime.utcnow().date() - timedelta(days=self.backdays)
            end_date = datetime.utcnow().date() + timedelta(days=1)

            session = db.session

            if self.original_transaction_id:

                # if patch, by operator specific original_transaction_id,
                # get the latest payments
                payments = session.query(PaymentAppleIAP).filter_by(
                    original_transaction_id=self.original_transaction_id,
                    is_processed=False
                ).order_by(desc(PaymentAppleIAP.expires_date)).first()
                payments = [payments]

            else:
                # retrieve data payment iap between H-14 -> H+1
                payment_iap_query = session.query(PaymentAppleIAP).filter(
                    PaymentAppleIAP.expires_date >= start_date,
                    PaymentAppleIAP.expires_date <= end_date,
                    PaymentAppleIAP.product_id != None,
                    PaymentAppleIAP.payment_id != None).filter_by(
                    is_processed=False, renewal_status=NEW)
                # get data from the oldest
                payments = payment_iap_query.order_by(
                    PaymentAppleIAP.expires_date).limit(self.divider).all()
                # get data from the newest, that not yet exists in the first query
                payments2 = payment_iap_query.filter(PaymentAppleIAP.id.notin_(
                    [p.id for p in payments]
                )).order_by(
                    desc(PaymentAppleIAP.expires_date)).limit(self.divider).all()
                # combine
                payments.extend(payments2)

            if payments:
                for renewal in payments:
                    tmp = {}
                    self.allow_auto_purchase = True

                    renewal.renewal_status = PROCESSING
                    renewal.save(session)

                    data = process_renewal(payment_iap=renewal, session=session)
                    self.data_renewal.append(data)

                    #     # get active receipt
                    #     active_renewal = renewal.get_receipt_data()
                    #     self.active_payments = active_renewal
                    #
                    #     # create logs renewal
                    #     self.create_logs(renewal, active_renewal)
                    #
                    #     # log if not subs types
                    #     if not active_renewal:
                    #         error = 'original_transaction_id %s not renewal receipt' % renewal.original_transaction_id
                    #         corse = self.error_logs(error, httplib.NOT_FOUND, renewal)
                    #         self.data_renewal.append(corse)
                    #         self.reset_renewal(renewal, active_renewal, session)
                    #         self.allow_auto_purchase = False
                    #         break
                    #
                    #     ## verify receipt data to apple servers
                    #     receipt_data = renewal.receipt_data.decode('unicode_escape')
                    #     verify_status, verify_result = AppleIAPHelpers(receipt_data=receipt_data).verify_to_apple()
                    #
                    #     if not verify_status:
                    #         error = 'verify apple failed : %s' % verify_result
                    #         corse = self.error_logs(error, httplib.NOT_FOUND, renewal, verify_result)
                    #         self.data_renewal.append(corse)
                    #         self.reset_renewal(renewal, active_renewal, session)
                    #         self.allow_auto_purchase = False
                    #         break
                    #
                    #     # get apple verifications data
                    #     apple_verification_result= verify_result.get('verification_rslt', None)
                    #     apple_receipt = apple_verification_result.get('receipt', None)
                    #     apple_latest_receipt_info = apple_verification_result.get('latest_receipt_info', None)
                    #     apple_latest_receipt_data = apple_verification_result.get('latest_receipt', None)
                    #
                    #     if not apple_latest_receipt_data:
                    #         # No latest receipt data this mean users already inactivated auto renewal
                    #         renewal.is_processed = True
                    #         renewal.save(session)
                    #
                    #         active_renewal.is_processed = True
                    #         active_renewal.save(session)
                    #
                    #         error = "Latest receipt data not found %s, Users not activate auto renewal" % renewal.original_transaction_id
                    #         corse = self.error_logs(error, httplib.NOT_FOUND, renewal, apple_verification_result)
                    #         self.data_renewal.append(corse)
                    #         self.allow_auto_purchase = False
                    #         self.reset_renewal(renewal, active_renewal, session)
                    #         break
                    #
                    #     self.new_receipt_data = apple_latest_receipt_data
                    #     apple_latest_web_orderline_id = apple_latest_receipt_info.get('web_order_line_item_id', None)
                    #     apple_web_orderline_id = apple_receipt.get('web_order_line_item_id', None)
                    #
                    #     # this mean orderline first buy same as orderline renewal, this re-purchase
                    #     if apple_web_orderline_id == apple_latest_web_orderline_id:
                    #         error = "Invalid web apple orderline id, re-purchased"
                    #         corse = self.error_logs(error, httplib.INTERNAL_SERVER_ERROR, renewal, apple_verification_result)
                    #         self.data_renewal.append(corse)
                    #         self.allow_auto_purchase = False
                    #         #break
                    #
                    #     apple_new_original_transaction_id = apple_latest_receipt_info.get('original_transaction_id', None)
                    #
                    #     # get tier codes
                    #     if apple_web_orderline_id is None:
                    #         origin_tier_code = apple_latest_receipt_info.get('product_id', None)
                    #         splitted_tier_code = origin_tier_code.split(".")
                    #         self.tier_code = ".%s" % splitted_tier_code[3]
                    #
                    #     else:
                    #         origin_tier_code = apple_latest_receipt_info.get('product_id', None)
                    #         splitted_tier_code = origin_tier_code.split(".")
                    #         self.tier_code = ".%s.%s" % (splitted_tier_code[3], splitted_tier_code[4])
                    #
                    #     # construct orderlines, for checkout
                    #     status, data = self.construct_order_line()
                    #     if not status:
                    #         error = data
                    #         corse = self.error_logs(error, httplib.INTERNAL_SERVER_ERROR, renewal, apple_verification_result)
                    #         self.data_renewal.append(corse)
                    #         self.allow_auto_purchase = False
                    #
                    #     # recheck new receipts
                    #     new_receipt_check = AppleIAPHelpers().check_new_receipt(
                    #         client_id=active_renewal.client_id,
                    #         web_order_line_id=apple_latest_web_orderline_id,
                    #         itunes_product_type_id=itunes_product_type.SUBSCRIPTION,
                    #         original_transaction_id=apple_new_original_transaction_id)
                    #
                    #     if not new_receipt_check:
                    #         error = "Invalid receipt, subs still active"
                    #         # corse = self.error_logs(error, httplib.INTERNAL_SERVER_ERROR, renewal, apple_verification_result)
                    #         # self.data_renewal.append(corse)
                    #         self.allow_auto_purchase = False
                    #
                    #         renewal.renewal_status = NEW
                    #         renewal.is_processed = False
                    #         renewal.save(session)
                    #
                    #         if self.active_payments:
                    #             self.active_payments.is_processed = False
                    #             self.active_payments.save(session)
                    #
                    #     # do auto renewal.
                    #     if self.allow_auto_purchase:
                    #         status_auto, data_auto = self.auto_purchase(renewal, active_renewal)
                    #
                    #         if status_auto:
                    #             renewal.is_processed = True
                    #             renewal.renewal_status = COMPLETE
                    #             renewal.save(session)
                    #
                    #             if active_renewal:
                    #                 active_renewal.is_processed = True
                    #                 active_renewal.save(session)
                    #
                    #             tmp['status'] = httplib.OK
                    #             tmp['original_transaction_id'] = renewal.original_transaction_id
                    #             self.data_renewal.append(tmp)
                    #
                    #             self.master_log.end_process = datetime.now()
                    #             self.master_log.save()
                    #
                    #         else:
                    #             error = "Auto Purchase Failed : %s" % data_auto
                    #             corse = self.error_logs(error, httplib.INTERNAL_SERVER_ERROR, renewal)
                    #             self.data_renewal.append(corse)
                    #     else:
                    #         self.reset_renewal(renewal, active_renewal, session)
                    #
                    #     self.master_log.save()

            return True, None

        except Exception as e:
            return False, e

    def construct(self):
        #get all receipts, renewal last day
        self.validate_renewal_data()
        return self.json_success()
