import httplib, json

from app import app, db
from datetime import datetime, timedelta

from flask_appsfoundry.exceptions import UnprocessableEntity
from flask_restful import Resource, Api, reqparse, inputs
from flask import Response, request

from app.helpers import payment_logs, MIME_TYPE_V3_JSON
from app.constants import CLIENT_500243
from .apple_iap_renewal import AppleIAPRenewal
from .apple_iap_renewal_temp import AppleIAPRenewalTemp # this is only temporary for backup old renewal
from .apple_iap_restore import AppleIAPRestore
from .apple_iap_translation import AppleIAPTranslation

from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP
from app.paymentgateway_connectors.apple_iap.renewal_monitor import reset_patch, reset_receipt

api = Api(app)


class AppleIAPRenewalAPI(Resource):
    def __init__(self):
        super(AppleIAPRenewalAPI, self).__init__()

    def get(self, divider=None, original_transaction_id=None):
        args = {}
        args['api'] = '/v1/appleiaps/renewal'
        args['method'] = 'GET'
        renewal = AppleIAPRenewal(divider, original_transaction_id).construct()
        return renewal


class AppleIAPRenewalTempAPI(Resource):
    """
        this is old code rassel made. temporary for monitoring after
        new code is stable

        TO DO : Remove this api (kiratakada Note)
    """
    def __init__(self):
        super(AppleIAPRenewalTempAPI, self).__init__()

    def get(self, divider=None, original_transaction_id=None):
        args = {}
        args['api'] = '/v1/appleiaps/renewal'
        args['method'] = 'GET'
        apple_renewal_status, apple_renewal_rslt = AppleIAPRenewalTemp(
            divider, original_transaction_id).subscription_renewal()

        if not apple_renewal_status:
            return payment_logs(args=args, error_message=str(apple_renewal_rslt))

        return Response(
            apple_renewal_rslt,
            status=httplib.OK,
            mimetype='application/json'
        )


class AppleIAPRestoreAPI(Resource):
    """
    This is version 1. (IOS <7)
    for latest apple iap Restore, see:
    app/paymentgateway_connectors/apple_iap/v3/restore.py
        url: /v1/payments/apple-iaps/restore

    Apple restore class, this class use post method to restore
    the items to the user.
    """
    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        self.reqparser.add_argument('user_id', type=int, required=True)
        self.reqparser.add_argument('receipt_data', required=True)
        self.reqparser.add_argument('platform_id', type=int)

    def post(self):
        """
        Restore IAP post method.

        :param int user_id: REQUIRED
        :param string receipt_data: REQUIRED, data acquired from front end
        :param int platform_id: REQUIRED
        :param bool is_sandbox: REQUIRED
        :return: apple_restore_rslt
        :rtype: :py:class: 'app.paymentgateway_connectors.apple_iap.apple_iap_restore.AppleIAPRestore'
        """

        args = self.reqparser.parse_args()
        args['api'] = '/v1/appleiaps/restore'
        args['method'] = 'POST'

        apple_restore_status, apple_restore_rslt = AppleIAPRestore(args).restore()

        if not apple_restore_status:
            return payment_logs(
                args=args,
                error_message=apple_restore_rslt['error_message'],
                error_code=apple_restore_rslt['error_code'],
                user_message=apple_restore_rslt['user_message']
            )

        return apple_restore_rslt


class AppleIAPTranslationAPI(Resource):
    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        self.reqparser.add_argument('receipt',
                                    type=dict,
                                    required=True)

        self.reqparser.add_argument('client_id',
                                    type=int,
                                    required=True)

        self.reqparser.add_argument('translation_type',
                                    type=int,
                                    required=True)

        self.reqparser.add_argument('user_id',
                                    type=int,
                                    required=True)

    def post(self):
        try:
            args = self.reqparser.parse_args()
            args['api'] = '/v1/appleiaps/translation'
            args['method'] = 'POST'

            apple_translation = AppleIAPTranslation(**args).translate()

            return Response(json.dumps(apple_translation), status=httplib.OK)

        except Exception, e:
            response_form = {
                'error_message': str(e),
                'error_code': CLIENT_500243,
                'user_message': "Translation to SC2 failed"}
            payment_logs(**response_form)
            return Response(json.dumps(response_form), status=httplib.INTERNAL_SERVER_ERROR)


class MonitoringRenewalAPI(Resource):

    def get(self):

        try:
            time_monitor = datetime.now() - timedelta(hours=12)

            payments = PaymentAppleIAP.query.filter(
                PaymentAppleIAP.expires_date >= time_monitor,
                PaymentAppleIAP.product_id != None,
                PaymentAppleIAP.payment_id != None,
                PaymentAppleIAP.renewal_status.in_([2])).order_by(
                PaymentAppleIAP.expires_date).all()

            for idata in payments:
                receipt_status, receipt_data = reset_receipt([idata.original_transaction_id])

                if receipt_status:
                    reset_status, reset_data = reset_patch([idata.original_transaction_id])
                    print reset_data

            return httplib.OK

        except Exception as e:
            pass

api.add_resource(AppleIAPRenewalAPI,
    '/v1/payments/appleiaps/renewal/patch/<string:original_transaction_id>')
