from __future__ import unicode_literals, absolute_import

import json
from httplib import OK, UNPROCESSABLE_ENTITY

from datetime import timedelta
from time import sleep

from marshmallow.utils import isoformat
from mock import patch
from sqlalchemy import desc
from sqlalchemy.orm import scoped_session, sessionmaker

from app import app, db
from app.items.models import Item, UserItem
from app.master.choices import IOS
from app.master.models import Platform
from app.offers.models import OfferType, Offer
from app.paymentgateway_connectors.apple_iap.v3.helpers import create_new_log_receipt_data
from app.paymentgateway_connectors.apple_iap.v3.models import LogReceipt
from app.paymentgateway_connectors.apple_iap.v3.restore import async_restore_from_apple_receipt
from app.utils.datetimes import get_local
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferPlatformFactory, OfferSubscriptionFactory


def get_expected_item_format(item):
    return {
        "id": item.id,
        "href": item.api_url,
        "title": item.name,
        "image": {
            "title": "Cover Image",
            "href": app.config['MEDIA_BASE_URL'] + item.image_highres
        },
        "thumbnail": {
            "title": "Thumbnail Image",
            "href": app.config['MEDIA_BASE_URL'] + item.thumb_image_highres
        },
        "is_age_restricted": item.parentalcontrol_id == 2,
        "content_type": item.content_type,
        "item_type": item.item_type,
        "edition_code": item.edition_code,
        "issue_number": item.issue_number,
        "release_date": isoformat(item.release_date),
        "brand": {
            "id": item.brand.id,
            "title": item.brand.name,
            "href": item.brand.api_url,
            # "vendor": {
            #     "id": item.brand.vendor.id,
            #     "title": item.brand.vendor.name,
            #     "href": item.brand.vendor.api_url
            # }
        }
    }


class RestoreTests(TestBase):
    """ E2E tests for Apple Restore API

    """

    url = '/v1/payments/apple-iap/restore'

    @classmethod
    def setUpClass(cls):
        super(RestoreTests, cls).setUpClass()
        cls.init_data(cls.session)

    @classmethod
    def init_data(cls, session):
        session = db.session()
        cls.item_a = ItemFactory(item_status=Item.Statuses.ready_for_consume.value)
        cls.item_b = ItemFactory(item_status=Item.Statuses.ready_for_consume.value)
        offer_type_single = session.query(OfferType).get(Offer.Type.single.value)
        offer_a = OfferFactory(offer_type=offer_type_single)
        # offer_a.brands.append(cls.item_a.brand)
        offer_a.items.append(cls.item_a)
        offer_b = OfferFactory(offer_type=offer_type_single)
        # offer_b.brands.append(cls.item_a.brand)
        offer_b.items.append(cls.item_b)

        offer_c = OfferFactory(offer_type=offer_type_single)
        cls.item_c = ItemFactory(item_status=Item.Statuses.ready_for_consume.value)
        offer_c.items.append(cls.item_c)

        offer_d = OfferFactory(offer_type=offer_type_single)
        cls.item_d = ItemFactory(item_status=Item.Statuses.ready_for_consume.value)
        offer_d.items.append(cls.item_d)

        platform = session.query(Platform).get(IOS)
        cls.offer_platform_a = OfferPlatformFactory(
            offer=offer_a,
            platform=platform,
            tier_code='.h.ID_AADFSDF'
        )
        cls.offer_platform_b = OfferPlatformFactory(
            offer=offer_b,
            platform=platform,
            tier_code='.h.ID_BBEDFFF'
        )
        # Consumable offer platform, should not processed!
        cls.offer_platform_c = OfferPlatformFactory(
            offer=offer_c,
            platform=platform,
            tier_code='.c.ID_CCDFDF'
        )
        offer_platform_d = OfferPlatformFactory(
            offer=offer_d,
            platform=platform,
            tier_code='.d.ID_DDAADFDFDF'
        )

        # Offer Subscriptions data
        brand_sub_1 = BrandFactory()
        item_sub_0 = ItemFactory(
            item_status=Item.Statuses.ready_for_consume.value,
            brand=brand_sub_1,
            release_date=get_local() - timedelta(days=10)
        )
        cls.item_sub_1 = ItemFactory(
            item_status=Item.Statuses.ready_for_consume.value,
            brand=brand_sub_1,
            release_date=get_local()
        )
        cls.item_sub_2 = ItemFactory(
            item_status=Item.Statuses.ready_for_consume.value,
            brand=brand_sub_1,
            release_date=get_local() + timedelta(days=1)
        )
        item_sub_3 = ItemFactory(
            item_status=Item.Statuses.ready_for_consume.value,
            brand=brand_sub_1,
            release_date=get_local() + timedelta(days=2)
        )
        offer_type_sub = session.query(OfferType).get(Offer.Type.subscription.value)
        offer_sub_1 = OfferFactory(offer_type=offer_type_sub)
        offer_sub_1.brands.append(cls.item_sub_1.brand)
        offer_subscription_1 = OfferSubscriptionFactory(
            offer=offer_sub_1,
            quantity_unit=1,
            quantity=2,

        )
        cls.offer_platform_sub_1 = OfferPlatformFactory(
            offer=offer_sub_1,
            platform=platform,
            tier_code='.ARS.ID_11A22CCDFDF'
        )
        session.commit()

        # mock send email (mock smtp object) if needed
        mock_smtplib()

    def setUp(self):
        self.session.add(self.super_admin)
        self.session.add(self.regular_user)

    def tearDown(self):
        self.detach_model(self.super_admin)
        self.detach_model(self.regular_user)

    @patch('app.paymentgateway_connectors.apple_iap.v3.helpers.get_receipt_from_itunes')
    def test_all_is_ok(self, mocked_iap_response):
        session = db.session()

        mocked_iap_response.return_value = (self._mock_apple_response(), True)
        self.set_api_authorized_user(self.super_admin)

        response = self.api_client.post(
            self.url,
            headers=self.build_headers(has_body=True),
            data=json.dumps({
                "receipt_data": "dfkslafjlsdajfdlsjfsdlafsdajklfsjalkfsda",
            })
        )

        self.assertEqual(response.status_code, OK)
        expected = {
            "title": LogReceipt.ActivityTypes.restore.value,
            "status": LogReceipt.StatusProcess.in_progress.value
        }
        self.assertDictContainsSubset(
            expected,
            json.loads(response.data),
        )

        sleep(2)
        # assert log data - async process is completed
        log_data = self.session.query(LogReceipt).filter_by(user_id=self.super_admin.id).first()
        self.session.refresh(log_data)
        self.assertEqual(log_data.status, LogReceipt.StatusProcess.completed)

        expected_items = [self.item_a, self.item_b, self.item_sub_1, self.item_sub_2, ]
        # self._assert_response_body(
        #     response,
        #     expected_items=expected_items,
        #     session=self.session)

        # validate user items
        user_items = self.session.query(UserItem).filter_by(user_id=self.super_admin.id).all()
        actual_user_items = [ui.item_id for ui in user_items]
        expected = [item.id for item in expected_items]
        self.assertItemsEqual(
            actual_user_items,
            expected
        )

    @patch('app.paymentgateway_connectors.apple_iap.v3.helpers.get_receipt_from_itunes')
    def test_async_restore_from_apple_receipt(self, mocked_iap_response):
        """ test method async_restore_from_apple_receipt
        since it need exact data like e2e test for Apple-Restore, we put functionality test for this method here

        :param mocked_iap_response:
        :return:
        """
        # prepare additional data - start

        mocked_iap_response.return_value = (self._mock_apple_response(), True)

        user_id = self.regular_user.id
        receipt_data = 'dfasfaslfkjdsalkfjsaldkjff'
        device_registration_id = 'abc123'
        log_data = create_new_log_receipt_data(
            receipt_data,
            activity_type=LogReceipt.ActivityTypes.restore,
            user_id=user_id,
            device_registration_id=device_registration_id,
            session=self.session)
        # prepare additional data - end
        task_id = log_data.id

        # execute method to test it
        actual_result = async_restore_from_apple_receipt(
            receipt_data='salkfdlfjsf',
            task_id=task_id ,
            user_id=user_id,
            user_email=self.regular_user.email,
            device_registration_id=device_registration_id,
        )
        self.assertIsNone(actual_result)

        # assert log data - async process is completed

        db.engine.dispose()
        session_factory = sessionmaker(bind=db.engine)
        thread_scoped_session = scoped_session(session_factory)
        session = thread_scoped_session()

        log_data_actual = session.query(LogReceipt).get(task_id )
        self.assertEqual(log_data_actual.status, LogReceipt.StatusProcess.completed)

        with self.on_session(self.item_a, self.item_b, self.item_sub_1, self.item_sub_2):
            expected_items = [self.item_a, self.item_b, self.item_sub_1, self.item_sub_2, ]

            # validate user items
            user_items = self.session.query(UserItem).filter_by(user_id=self.regular_user.id).all()
            actual_user_items = [ui.item_id for ui in user_items]
            expected = [item.id for item in expected_items]
            self.assertItemsEqual(
                actual_user_items,
                expected
            )

    def _mock_apple_response(self):
        with self.on_session(self.offer_platform_a, self.offer_platform_b, self.offer_platform_c,
                             self.offer_platform_sub_1):
            return {
                "status": 0,
                "receipt": {
                    "in_app": [
                        {
                            "product_id": "com.appsfoundry.scoop{}".format(self.offer_platform_a.tier_code),
                        },
                        {
                            "product_id": "com.appsfoundry.scoop{}".format(self.offer_platform_b.tier_code),
                            "original_transaction_id": "1231313123123",
                            "transaction_id": "1231313123123",
                        },
                        {
                            # consumable, should not processed
                            "product_id": "com.appsfoundry.scoop{}".format(self.offer_platform_c.tier_code),
                        },
                        {
                            # double tier code, should not processed
                            "product_id": "com.appsfoundry.scoop{}".format(self.offer_platform_b.tier_code),
                            "original_transaction_id": "1231313123123d4444",
                            "transaction_id": "1231313123123",
                        },
                        {
                            # Offers Subscriptions
                            "product_id": "com.appsfoundry.scoop{}".format(self.offer_platform_sub_1.tier_code),
                            "original_transaction_id": "1231313123123d555",
                            "original_purchase_date": get_local().strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
                            "purchase_date": get_local().strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
                            "expires_date": (get_local() + timedelta(days=60)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
                        },
                    ]
                }
            }

    def test_get_restore_details(self):
        log_data = LogReceipt(
            user_id=self.regular_user.id,
            receipt_data="sfkasfdsajlfasdf",
            status=LogReceipt.StatusProcess.error,
            activity_type=LogReceipt.ActivityTypes.restore,
            error=json.dumps({
                'user_message': 'Failed in {} process'.format(LogReceipt.ActivityTypes.restore.value),
                'developer_message': 'Error bla bla bla',
            })
        )
        self.session.add(log_data)
        self.session.commit()
        self.set_api_authorized_user(self.regular_user)
        response = self.api_client.get(
            '{}/{}'.format(self.url, log_data.id),
            headers=self.build_headers()
        )
        self.assertEqual(response.status_code, OK)
        expected = {
            "id": log_data.id,
            "title": log_data.activity_type.value,
            "status": log_data.status.value,
            "href": "http://localhost/v1/payments/apple-iap/restore/{}".format(log_data.id)
                # get_href_restore(log_data.id)
        }
        self.assertDictContainsSubset(
            expected, json.loads(response.data)
        )

    @patch('app.paymentgateway_connectors.apple_iap.v3.helpers.get_receipt_from_itunes')
    def test_invalid_apple_response(self, mocked_iap_response):

        # mock invalid response from apple itunes
        mocked_iap_response.return_value = ({
            "status": 0,
            "receipt": {
                "a": "b"
            }
        }, True)
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.post(
            self.url,
            headers=self.build_headers(has_body=True),
            data=json.dumps({
                "receipt_data": "dfkslafjlsdajfdlsjfsdlafsdajklfsjalkfsda",
            })
        )

        # because it's async, response status code will be: OK, but log will show it's error
        self.assertEqual(response.status_code, OK)

        sleep(3)
        # check log data
        log_data = self.session.query(LogReceipt).filter_by(
            user_id=self.regular_user.id
        ).order_by(desc(LogReceipt.id)).first()
        self.session.refresh(log_data)
        self.assertEqual(log_data.status, LogReceipt.StatusProcess.error)

    def test_invalid_request_body(self):
        self.set_api_authorized_user(self.regular_user)

        response = self.api_client.post(
            self.url,
            headers=self.build_headers(has_body=True),
            data=json.dumps({
                "receipt_data_wrong_body": "dfkslafjlsdajfdlsjfsdlafsdajklfsjalkfsda",
            })
        )

        self.assertEqual(response.status_code, UNPROCESSABLE_ENTITY)

    # @patch('app.paymentgateway_connectors.apple_iap.v3.helpers.get_receipt_from_itunes')
    # def test_failed_to_get_iap_response(self, mocked_iap_response):
    #     mocked_iap_response. = AppleRequestError(
    #         "Failed", "fddsfa"
    #     )
    #
    #     self.set_api_authorized_user(self.regular_user)
    #
    #     response = self.api_client.post(
    #         self.url,
    #         headers=self.build_headers(has_body=True),
    #         data=json.dumps({
    #             "receipt_data": "dfkslafjlsdajfdlsjfsdlafsdajklfsjalkfsda",
    #         })
    #     )
    #
    #     self.assertEqual(response.status_code, GATEWAY_TIMEOUT)

    def _assert_response_body(self, response, expected_items, session):
        items_data = []
        for item in expected_items:
            session.add(item)
            items_data.append(get_expected_item_format(item))
        self.assertUnorderedJsonEqual(
            response,
            {
                "items": items_data,
                "metadata": {
                    "resultset": {
                        "limit": len(expected_items),
                        "offset": 0,
                        "count": len(expected_items)
                    }
                }
            }
        )
