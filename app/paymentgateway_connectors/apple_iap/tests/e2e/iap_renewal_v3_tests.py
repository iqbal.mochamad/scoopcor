import json
from datetime import datetime

from datetime import timedelta
from unittest import SkipTest

import httpretty
from copy import deepcopy
from nose.tools import istest, nottest

from app import app, db
from app.auth.models import Client
from app.items.models import Item
from app.master.models import Platform
from app.offers.models import OfferType, Offer
from app.orders.models import Order
from app.payment_gateways.models import PaymentGateway
from app.paymentgateway_connectors.apple_iap.choices.renewal_status import NEW, COMPLETE
from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP, ReceiptVersion
from app.paymentgateway_connectors.apple_iap.v3.helpers import get_apple_url
from app.paymentgateway_connectors.apple_iap.v3.renewal import process_renewal
from app.payments.models import Payment
from app.users.buffets import UserBuffet
from app.utils.datetimes import get_local_nieve
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib, generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferBuffetFactory, OfferPlatformFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory
from tests.fixtures.sqlalchemy.orders import OrderLineFactory
from tests.fixtures.sqlalchemy.payments import PaymentFactory


class RenewalV3Base(TestBase):
    """ Base class for renewal

    """

    maxDiff = None
    longMessage = False

    @classmethod
    def get_product_id(cls, client, order):
        return "{}{}".format(
            client.product_id,
            order.tier_code
        )

    @classmethod
    def create_order(cls, pg, order_id, user):
        order = OrderFactory(id=order_id, paymentgateway=pg,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             user_id=user.id, party=user,
                             tier_code='.c.usd.1.99',
                             order_status=Order.Status.complete.value,
                             currency_code=pg.base_currency.iso4217_code,
                             client_id=cls.client.id)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=user.id,
                                      currency_code=pg.base_currency.iso4217_code,
                                      price=cls.offer1.price_usd, final_price=cls.offer1.price_usd,
                                      orderline_status=Order.Status.complete.value)
        return order

    def _assert_new_data(self, previous_payment_iap, order, original_trx_id, new_trx_id):
        # previous payment iap should be COMPLETE
        self.assertEqual(previous_payment_iap.renewal_status, COMPLETE)
        self.assertTrue(previous_payment_iap.is_processed)

        # check new payment iap data
        new_payment_iap = self.session.query(PaymentAppleIAP).filter(
            PaymentAppleIAP.original_transaction_id == original_trx_id,
            PaymentAppleIAP.expires_date > get_local_nieve()
        ).first()
        self.assertIsNotNone(new_payment_iap)
        self.assertEqual(new_payment_iap.transaction_id, new_trx_id)

        # check new user buffet data
        new_user_buffet = self.session.query(UserBuffet).filter(
            UserBuffet.user_id == order.user_id,
            UserBuffet.valid_to > get_local_nieve(),
            UserBuffet.original_transaction_id == "{}".format(original_trx_id)
        ).first()
        self.assertIsNotNone(new_user_buffet)

        # new order vs previous order, should be same
        previous_order = previous_payment_iap.payment.order
        new_order = new_payment_iap.payment.order
        self.assertEqual(new_order.order_status, new_order.Status.complete.value)
        self.assertEqual(new_order.final_amount, previous_order.final_amount)
        self.assertEqual(new_order.user_id, previous_order.user_id)
        self.assertEqual(new_order.paymentgateway_id, previous_order.paymentgateway_id)

        # new payment vs previous payment, should be same
        self.assertEqual(new_payment_iap.payment.amount, previous_payment_iap.payment.amount)
        self.assertEqual(new_payment_iap.payment.paymentgateway_id, previous_payment_iap.payment.paymentgateway_id)

        # new order line vs previous order line
        previous_line = previous_payment_iap.payment.order.order_lines[0]
        new_line = new_payment_iap.payment.order.order_lines[0]
        self.assertEqual(new_line.final_price, previous_line.final_price)
        self.assertEqual(new_line.offer_id, previous_line.offer_id)


@nottest
class RenewalBuffetBase(RenewalV3Base):
    """ Base class to test Renewal for BUFFET transaction

    """
    # order id
    order_id_buffet = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
    new_trx_id = 21224
    original_trx_id_buffet = 21223
    receipt_version = ReceiptVersion.ios7_unified_receipt.value
    module_based_db = True

    @classmethod
    def setUpClass(cls):
        super(RenewalBuffetBase, cls).setUpClass()
        cls.init_data(cls.session)

    @classmethod
    def init_data(cls, s):
        pg = s.query(PaymentGateway).get(PaymentGateway.Type.apple.value)
        cls.offer1 = s.query(Offer).get(43103)
        cls.client = s.query(Client).get(1)

        cls.order_buffet = cls.create_order(pg, cls.order_id_buffet, cls.super_admin)
        payment = PaymentFactory(id=cls.order_id_buffet, order=cls.order_buffet,
                                 user_id=cls.order_buffet.user_id,
                                 payment_status=Payment.Status.billed.value)
        s.commit()
        cls.payment_iap_buffet = PaymentAppleIAP(
            payment_id=cls.order_id_buffet,
            product_id=cls.get_product_id(cls.client, cls.order_buffet),
            original_transaction_id=cls.original_trx_id_buffet,
            expires_date=datetime.now() - timedelta(days=1),
            web_order_line_item_id=1,
            transaction_id=cls.original_trx_id_buffet,
            purchase_date=datetime.now() - timedelta(days=20),
            original_purchase_date=datetime.now() - timedelta(days=20),
            status='new',
            receipt_data='dlfkasjdlkfsajf',
            receipt='{"receipt": {}',
            is_processed=False,
            renewal_status=NEW,
            receipt_version=cls.receipt_version
        )
        s.add(cls.payment_iap_buffet)
        s.commit()

    @httpretty.activate
    def test_process_renewal(self):
        with self.on_session(self.payment_iap_buffet, self.client, self.order_buffet):
            mock_ios_receipt_response = self.get_mock_valid_ios_response(
                self.client,
                self.order_buffet,
                original_trx_id=self.original_trx_id_buffet,
                new_trx_id=self.new_trx_id
            )

            # here we mock the response from apple ios when we hit their API (GET payment status api)
            matched_url_to_mock = "{}{}".format(get_apple_url(), app.config['URL_VERIFY_RECEIPT'])
            httpretty.register_uri(
                httpretty.POST,
                matched_url_to_mock,
                content_type='application/json',
                body=json.dumps(mock_ios_receipt_response))

            mock_smtplib()

            # executed method to test
            process_renewal(self.payment_iap_buffet, self.session)

            self._assert_new_data(
                previous_payment_iap=self.payment_iap_buffet,
                order=self.order_buffet,
                original_trx_id=self.original_trx_id_buffet,
                new_trx_id=self.new_trx_id
            )

    def get_mock_valid_ios_response(self, client, order, original_trx_id=1199, new_trx_id=1200):
        # override this method, return mock response from apple ios
        pass


@istest
class RenewalIos7BuffetTests(RenewalBuffetBase):
    """ Test renewal process for ios7 unified receipt for Buffet transaction

    """
    order_id_buffet = int((datetime.now() - datetime(1970, 1, 1)).total_seconds()) + 1
    new_trx_id = 21224
    original_trx_id_buffet = 21223
    receipt_version = ReceiptVersion.ios7_unified_receipt.value

    def get_mock_valid_ios_response(self, client, order, original_trx_id=1199, new_trx_id=1200):
        # return mock ios 7 unified receipt response json
        in_app_data = [
                    {
                        # first purchase trx
                        # "product_id": "com.appsfoundry.com.c.0.99.usd",
                        "product_id": self.get_product_id(client, order),
                        "transaction_id": str(original_trx_id),
                        "original_transaction_id": str(original_trx_id),
                        "expires_date": (get_local_nieve() - timedelta(1)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
                        "web_order_line_item_id": "{}123".format(original_trx_id),
                        "purchase_date": (get_local_nieve() - timedelta(20)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
                        "original_purchase_date": (get_local_nieve() - timedelta(20)).strftime(
                            "%Y-%m-%d %H:%M:%S Etc/GMT"),
                        "is_trial_period": False,
                    },
                    # some dummy data
                    {
                        "quantity": "1",
                        "product_id": "com.appsfoundry.scoop.ars.SUB_1003MTH",
                        "transaction_id": "1000000166563547",
                        "original_transaction_id": "1000000166563403",
                        "purchase_date": "2015-08-06 07:14:31 Etc/GMT",
                        "purchase_date_ms": "1438845271000",
                        "purchase_date_pst": "2015-08-06 00:14:31 America/Los_Angeles",
                        "original_purchase_date": "2015-08-06 07:10:52 Etc/GMT",
                        "original_purchase_date_ms": "1438845052000",
                        "original_purchase_date_pst": "2015-08-06 00:10:52 America/Los_Angeles",
                        "expires_date": "2015-08-06 07:29:31 Etc/GMT",
                        "expires_date_ms": "1438846171000",
                        "expires_date_pst": "2015-08-06 00:29:31 America/Los_Angeles",
                        "web_order_line_item_id": "1000000030259814",
                        "is_trial_period": "false"
                    },
                ]
        # per 20 jun 2017, new renewal data exists in latest receipt info, but not in in-app data
        latest_receipt_data = deepcopy(in_app_data)
        latest_receipt_data.append({
            # renewal data from apple
            # "product_id": "com.appsfoundry.com.c.0.99.usd",
            "product_id": self.get_product_id(client, order),
            "transaction_id": str(new_trx_id),
            "original_transaction_id": str(original_trx_id),
            "web_order_line_item_id": "{}124".format(original_trx_id),
            "purchase_date": get_local_nieve().strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
            "original_purchase_date": get_local_nieve().strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
            "expires_date": (get_local_nieve() + timedelta(1)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
        })
        return {
            "status": 0,
            "environment": "Sandbox",
            "latest_receipt": "fdsafds123kldjfsklafjlsdaf.dflsafjsaldjkf",
            "receipt": {
                "in_app": in_app_data
            },
            "latest_receipt_info": latest_receipt_data
        }


@istest
class RenewalIos6BuffetTests(RenewalBuffetBase):
    """ Test renewal process for ios6 Old receipt format for Buffet transaction

    """
    order_id_buffet = int((datetime.now() - datetime(1970, 1, 1)).total_seconds()) + 2
    new_trx_id = 2122334
    original_trx_id_buffet = 2122333
    receipt_version = ReceiptVersion.ios6_receipt.value

    def get_mock_valid_ios_response(self, client, order, original_trx_id=1199, new_trx_id=1200):
        # return mock ios 6 old receipt response json
        return {
            "status": 0,
            "latest_receipt": "fdsafds123kldjfsklafjlsdaf.dflsafjsaldjkf",
            "receipt": {
                # first purchase trx
                # "product_id": "com.appsfoundry.com.c.0.99.usd",
                "product_id": self.get_product_id(client, order),
                "transaction_id": str(original_trx_id),
                "original_transaction_id": str(original_trx_id),
                "expires_date_formatted": (get_local_nieve() - timedelta(1)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
                "web_order_line_item_id": "{}123".format(original_trx_id),
                "purchase_date": (get_local_nieve() - timedelta(20)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
                "original_purchase_date": (get_local_nieve() - timedelta(20)).strftime(
                    "%Y-%m-%d %H:%M:%S Etc/GMT"),
                "is_trial_period": False,
            },
            "latest_receipt_info": {
                # renewal data from apple
                # "product_id": "com.appsfoundry.com.c.0.99.usd",
                "product_id": self.get_product_id(client, order),
                "transaction_id": str(new_trx_id),
                "original_transaction_id": str(original_trx_id),
                "web_order_line_item_id": "{}124".format(original_trx_id),
                "purchase_date": get_local_nieve().strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
                "original_purchase_date": get_local_nieve().strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
                "expires_date_formatted": (get_local_nieve() + timedelta(1)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),

                # sample response from apple
                # "original_purchase_date_pst": "2016-10-31 06:20:10 America/Los_Angeles",
                "unique_identifier": "d35373078d8b798521cdec2f0bb75e1376b118e4",
                # "original_transaction_id": "90000244953168",
                # "expires_date": "1493558407000",
                "app_item_id": "402166944",
                # "transaction_id": "90000286393024",
                "quantity": "1",
                # "product_id": "com.appsfoundry.scoop.ars.SUB_TEM04ED01MTH",
                "bvrs": "4.8.1.0",
                "bid": "com.apps-foundry.SCOOP",
                "unique_vendor_identifier": "DF8DA496-D557-44CD-AFC5-69FAA33C840E",
                # "web_order_line_item_id": "90000050034283",
                "original_purchase_date_ms": "1477920010000",
                # "expires_date_formatted": "2017-04-30 13:20:07 Etc/GMT",
                # "purchase_date": "2017-03-31 13:20:07 Etc/GMT",
                "purchase_date_ms": "1490966407000",
                "expires_date_formatted_pst": "2017-04-30 06:20:07 America/Los_Angeles",
                "purchase_date_pst": "2017-03-31 06:20:07 America/Los_Angeles",
                # "original_purchase_date": "2016-10-31 13:20:10 Etc/GMT",
                "item_id": "778392999"
            },
        }


original_apple_iap = app.config['APPLE_IAP_HOST_LIVE']
original_apple_iap_sandbox = app.config['APPLE_IAP_HOST_LIVE']


def setup_module():
    # make sure the test mode flag is set, or bail out
    if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
        raise SkipTest("COR is not in testing mode.  Cannot continue.")
    db.session.expunge_all()
    db.session.close_all()
    db.engine.dispose()
    db.drop_all()
    db.create_all()
    session = db.session
    generate_static_sql_fixtures(session)
    session.commit()
    init_data(session)

    app.config['APPLE_IAP_HOST_LIVE'] = 'http://buy.itunes.apple.com'
    app.config['APPLE_IAP_HOST_SANDBOX'] = original_apple_iap_sandbox.replace('https', 'http')


def teardown_module():
    app.config['APPLE_IAP_HOST_LIVE'] = original_apple_iap
    app.config['APPLE_IAP_HOST_SANDBOX'] = original_apple_iap_sandbox

    db.session.expunge_all()
    db.session.close_all()
    db.engine.dispose()
    db.drop_all()


def init_data(s):
    # create offer buffet
    offer_type = s.query(OfferType).get(Offer.Type.buffet.value)
    offer1 = OfferFactory(id=43103, name='Scoop Premium',
                              offer_type=offer_type, is_free=False, price_idr=89000, price_usd=6.99,
                              price_point=1)
    brand = BrandFactory()
    item_id = 1
    item1 = ItemFactory(id=item_id, item_status=Item.Statuses.ready_for_consume.value, brand=brand)
    offer1.items.append(item1)
    offer1.brands.append(brand)
    offer_buffet = OfferBuffetFactory(
        available_from=datetime(2016, 10, 1),
        available_until=datetime.now() + timedelta(days=330),
        buffet_duration=timedelta(days=30),
        offer=offer1
    )
    offer_platform = OfferPlatformFactory(
        offer=offer1,
        platform=s.query(Platform).get(1),
        tier_id=1,
        tier_code='c.fake.code',
        currency='USD',
        price_usd=6.99,
        price_idr=89000,
        price_point=1,
        discount_price_usd=6.99,
        discount_price_idr=89000,
        discount_price_point=1
    )
    s.commit()
