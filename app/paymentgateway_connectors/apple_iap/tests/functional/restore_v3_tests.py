from __future__ import unicode_literals, absolute_import

from faker import Factory
from sqlalchemy import desc

from app import db
from app.paymentgateway_connectors.apple_iap.v3.restore import deliver_item_buffet
from app.users.buffets import UserBuffet
from app.users.tests.fixtures_user_buffet import UserBuffetFactory
from app.utils.testcases import TestBase


class RestoreTests(TestBase):

    def test_deliver_item_buffet(self):
        session = db.session()
        with self.on_session(self.super_admin, self.regular_user):
            self.session.add(self.super_admin)
            original_transaction_id = str(_fake.unix_time())
            last_user_buffet = UserBuffetFactory(
                original_transaction_id=original_transaction_id,
                is_restore=False,
                user_id=self.super_admin.id,
            )
            session.commit()
            self.session.add(self.regular_user)

            result = deliver_item_buffet(session, original_transaction_id, user_id=self.regular_user.id)
            self.assertIsNone(result)
            saved_user_buffet = session.query(UserBuffet).filter_by(
                original_transaction_id=original_transaction_id,
                user_id=self.regular_user.id).order_by(desc(UserBuffet.valid_to)).first()
            self.assertIsNotNone(saved_user_buffet)
            self.assertEqual(saved_user_buffet.orderline_id, last_user_buffet.orderline_id)
            self.assertEqual(saved_user_buffet.offerbuffet_id, last_user_buffet.offerbuffet_id)
            self.assertEqual(saved_user_buffet.is_restore, True)

            # if previous transaction id not found
            invalid_transaction_id = str(_fake.unix_time() + 123)
            result = deliver_item_buffet(session, invalid_transaction_id, user_id=self.regular_user.id)
            saved_user_buffet2 = session.query(UserBuffet).filter_by(
                original_transaction_id=invalid_transaction_id,
                user_id=self.regular_user.id).order_by(desc(UserBuffet.valid_to)).first()
            self.assertIsNone(saved_user_buffet2)


_fake = Factory.create()
