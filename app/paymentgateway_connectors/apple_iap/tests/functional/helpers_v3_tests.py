from __future__ import unicode_literals, absolute_import

import httplib
import smtplib
from datetime import datetime, timedelta

from flask import current_app
from flask import g
from flask import json
from flask_appsfoundry.exceptions import UnprocessableEntity
from marshmallow.utils import isoformat

from app import app, db
from app.items.models import Item
from app.master.choices import IOS
from app.master.models import Platform
from app.offers.models import OfferType, Offer
from app.paymentgateway_connectors.apple_iap.v3.helpers import AppleRequestError, \
    get_receipt_from_itunes, get_apple_url, \
    validate_receipt_json, create_new_log_receipt_data, log_error, generate_response_per_item, send_email_restore, \
    get_notification_messages, send_push_notification_restore, to_date, get_item_subscriptions
from app.paymentgateway_connectors.apple_iap.v3.models import LogReceipt
from app.utils.datetimes import get_local
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferPlatformFactory
from tests.fixtures.sqlalchemy.offers import OfferSubscriptionFactory


class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data


class HelpersTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(HelpersTests, cls).setUpClass()
        cls.session1 = db.session()
        cls.original_config = current_app.config['LIVE_SERVER']
        cls.session1.commit()

    def tearDown(self):
        current_app.config['LIVE_SERVER'] = self.original_config

    def test_to_date(self):
        actual = to_date("2015-08-06 07:14:31 Etc/GMT")
        self.assertEqual(actual, datetime(2015, 8, 6, 7, 14, 31))

    def test_get_receipt_from_itunes(self):

        expected = {
            "dummy": "dummy"
        }

        class mock_http_client(object):

            def post(self, *args, **kwargs):
                mock_resp = MockResponse(expected, httplib.OK)
                return mock_resp

        actual, is_sandbox = get_receipt_from_itunes(
            "fasldfjsalkfjsadklf",
            mock_http_client())
        self.assertEqual(actual, expected)

    def test_get_receipt_from_itunes_failed(self):

        class mock_http_client(object):

            def post(self, *args, **kwargs):
                mock_resp = MockResponse('', httplib.REQUEST_TIMEOUT)
                return mock_resp

        self.assertRaises(
            AppleRequestError,
            get_receipt_from_itunes,
            "fasldfjsalkfjsadklf",
            mock_http_client()
        )

    def test_get_apple_url(self):
        current_app.config['LIVE_SERVER'] = False
        self.assertEqual(get_apple_url(), current_app.config['APPLE_IAP_HOST_SANDBOX'])
        current_app.config['LIVE_SERVER'] = True
        self.assertEqual(get_apple_url(), current_app.config['APPLE_IAP_HOST_LIVE'])

    def test_validate_receipt_json(self):
        expected_in_app = [
            {
                "product_id": "com.appsfoundry.com.c.0.99.usd",
                "original_transaction_id": "1231231321",
            }
        ]
        valid_receipt = {
            "status": 0,
            "receipt": {
                "in_app": expected_in_app
            },
            "latest_receipt_info": expected_in_app
        }
        invalid_receipt1 = {
            "status": 21002
        }
        invalid_receipt2 = {
            "status": 0,
            "receipt": {
                "a": "a123123"
            }
        }

        log_data = LogReceipt(
            user_id=self.regular_user.id,
            receipt_data="sfkasfdsajlfasdf",
            status=LogReceipt.StatusProcess.in_progress,
            activity_type=LogReceipt.ActivityTypes.restore,
        )
        actual_in_app = validate_receipt_json(
            receipt_json=valid_receipt
        )
        self.assertItemsEqual(actual_in_app, expected_in_app)

        # receipt without latest receipt info
        valid_receipt2 = {
            "status": 0,
            "receipt": {
                "in_app": expected_in_app
            }
        }
        actual_in_app = validate_receipt_json(
            receipt_json=valid_receipt2
        )
        self.assertItemsEqual(actual_in_app, expected_in_app)

        self.assertRaises(
            UnprocessableEntity,
            validate_receipt_json, invalid_receipt1
        )
        self.assertRaises(
            UnprocessableEntity,
            validate_receipt_json, invalid_receipt2
        )

    def test_create_new_log_receipt_data(self):
        actual_data = create_new_log_receipt_data(
            receipt_data="dfalskdfsalkfjdsaf",
            activity_type=LogReceipt.ActivityTypes.restore,
            user_id=self.regular_user.id,
            device_registration_id='abc123',
            session=self.session1
        )
        self.assertEqual(actual_data.receipt_data, "dfalskdfsalkfjdsaf")
        self.assertEqual(actual_data.user_id, self.regular_user.id)
        self.assertEqual(actual_data.device_registration_id, 'abc123')
        self.assertEqual(actual_data.status, LogReceipt.StatusProcess.in_progress)
        self.assertEqual(actual_data.activity_type, LogReceipt.ActivityTypes.restore)

    def test_log_error(self):
        log_data = LogReceipt(
            user_id=self.regular_user.id,
            receipt_data="sfkasfdsajlfasdf123",
            status=LogReceipt.StatusProcess.in_progress,
            activity_type=LogReceipt.ActivityTypes.restore,
        )
        error_msg = "error message 123"
        self.session1.add(log_data)
        self.session1.commit()

        log_error(e=error_msg, log_data=log_data, session=self.session1)

        self.assertEqual(log_data.status, LogReceipt.StatusProcess.error)
        self.assertDictEqual(json.loads(log_data.error), {
            'user_message': 'Failed in {} process'.format(log_data.activity_type.value),
            'developer_message': error_msg,
        })

    def test_generate_response_per_item(self):
        item = ItemFactory()
        self.session1.commit()
        expected_response = generate_response_per_item(item)
        self.assertEqual(
            expected_response,
            {
                "id": item.id,
                "href": item.api_url,
                "title": item.name,
                "image": {
                    "title": "Cover Image",
                    "href": current_app.config['MEDIA_BASE_URL'] + item.image_highres
                },
                "thumbnail": {
                    "title": "Thumbnail Image",
                    "href": current_app.config['MEDIA_BASE_URL'] + item.thumb_image_highres
                },
                "is_age_restricted": item.parentalcontrol_id == 2,
                "content_type": item.content_type,
                "item_type": item.item_type,
                "edition_code": item.edition_code,
                "issue_number": item.issue_number,
                "release_date": isoformat(item.release_date),
                "brand": {
                    "id": item.brand.id,
                    "title": item.brand.name,
                    "href": item.brand.api_url,
                    # "vendor": {
                    #     "id": item.brand.vendor.id,
                    #     "title": item.brand.vendor.name,
                    #     "href": item.brand.vendor.api_url
                    # }
                }
            }
        )

    def test_get_notification_messages(self):
        messages = get_notification_messages(status=LogReceipt.StatusProcess.error)
        self.assertEqual(messages.status_message, 'gagal')
        self.assertEqual(messages.status_message_english, 'failed')
        messages = get_notification_messages(status=LogReceipt.StatusProcess.completed)
        self.assertEqual(messages.status_message, 'berhasil')
        self.assertEqual(messages.status_message_english, 'success')

    def test_send_email_restore(self):
        # smtplib.SMTP = smtplib.SMTP
        mock_smtplib()
        email_result = send_email_restore(user_email='teste.gmail.com',
                                          status=LogReceipt.StatusProcess.completed,
                                          restore_id=1)
        self.assertIsNone(email_result)

    def test_send_push_notification_restore(self):

        expected_return = 'berhasil bos'

        class MockPushService(object):

            def notify_single_device(self, registration_id, message_title, message_body):
                return expected_return

        actual = send_push_notification_restore(
            device_registration_id='abc123',
            status=LogReceipt.StatusProcess.completed,
            restore_id=1,
            push_service=MockPushService()
        )
        self.assertEqual(actual, expected_return)


class SubscriptionTests(TestBase):
    """ Test get items from subscription
    method: get_item_subscriptions

    """
    @classmethod
    def setUpClass(cls):
        super(SubscriptionTests, cls).setUpClass()
        session = db.session()

        # Offer Subscriptions data
        brand_sub_1 = BrandFactory()

        # create item backward
        item_sub_01 = ItemFactory(
            id=1,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=brand_sub_1,
            release_date=get_local() - timedelta(days=100)
        )
        cls.item_sub_02 = ItemFactory(
            id=2,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=brand_sub_1,
            release_date=get_local() - timedelta(days=15)
        )
        cls.item_sub_03 = ItemFactory(
            id=3,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=brand_sub_1,
            release_date=get_local() - timedelta(days=10)
        )

        # create normal item
        cls.item_sub_1 = ItemFactory(
            id=100,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=brand_sub_1,
            release_date=get_local()
        )
        cls.item_sub_2 = ItemFactory(
            id=101,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=brand_sub_1,
            release_date=get_local() + timedelta(days=10)
        )
        item_sub_3 = ItemFactory(
            id=102,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=brand_sub_1,
            release_date=get_local() + timedelta(days=100)
        )
        offer_type_sub = session.query(OfferType).get(Offer.Type.subscription.value)
        cls.offer_sub_1 = OfferFactory(offer_type=offer_type_sub)
        session.commit()
        cls.offer_sub_1.brands.append(brand_sub_1)
        cls.offer_subscription_1 = OfferSubscriptionFactory(
            offer=cls.offer_sub_1,
            quantity_unit=1,
            quantity=2,

        )
        platform = session.query(Platform).get(IOS)
        cls.offer_platform_sub_1 = OfferPlatformFactory(
            offer=cls.offer_sub_1,
            platform=platform,
            tier_code='.ARS.ID_11A22CCDFDF'
        )
        session.commit()

        cls.purchase_date = get_local()

    def test_sub_unit_per_edition(self):
        session = db.session()
        self.offer_subscription_1.quantity_unit = 1
        self.offer_subscription_1.allow_backward = False
        session.add(self.offer_subscription_1)
        session.commit()
        with self.on_session(self.offer_sub_1, self.item_sub_1, self.item_sub_2):
            actual = get_item_subscriptions(
                offer=self.offer_sub_1,
                session=session,
                purchase_date=self.purchase_date
            )
            expected = [self.item_sub_1, self.item_sub_2, ]
            self.assertEqual(len(actual), len(expected))
            self.assertItemsEqual(actual, expected)

    def test_sub_unit_per_edition_with_backward(self):
        session = db.session()
        self.offer_subscription_1.quantity_unit = 1
        self.offer_subscription_1.allow_backward = True
        self.offer_subscription_1.backward_quantity = 1
        session.add(self.offer_subscription_1)
        session.commit()
        with self.on_session(
            self.offer_sub_1, self.item_sub_1, self.item_sub_2,
            self.item_sub_02, self.item_sub_03
        ):
            actual = get_item_subscriptions(
                offer=self.offer_sub_1,
                session=session,
                purchase_date=self.purchase_date
            )
            expected = [self.item_sub_1, self.item_sub_2, self.item_sub_03, self.item_sub_02, ]
            self.assertEqual(len(actual), len(expected))
            self.assertItemsEqual(actual, expected)

    def test_sub_unit_per_month(self):
        session = db.session()
        self.offer_subscription_1.quantity_unit = 4
        self.offer_subscription_1.quantity = 2
        self.offer_subscription_1.allow_backward = False
        session.add(self.offer_subscription_1)
        session.commit()
        with self.on_session(
            self.offer_sub_1, self.item_sub_1, self.item_sub_2
        ):
            actual = get_item_subscriptions(
                offer=self.offer_sub_1,
                session=session,
                purchase_date=self.purchase_date
            )
            expected = [self.item_sub_1, self.item_sub_2, ]
            self.assertEqual(len(actual), len(expected))
            self.assertItemsEqual(actual, expected)

    def test_sub_unit_per_month_with_backward(self):
        session = db.session()
        self.offer_subscription_1.quantity_unit = 4
        self.offer_subscription_1.quantity = 2
        self.offer_subscription_1.allow_backward = True
        self.offer_subscription_1.backward_quantity = 1
        session.add(self.offer_subscription_1)
        session.commit()
        with self.on_session(
            self.offer_sub_1, self.item_sub_1, self.item_sub_2,
            self.item_sub_02, self.item_sub_03
        ):
            actual = get_item_subscriptions(
                offer=self.offer_sub_1,
                session=session,
                purchase_date=self.purchase_date
            )
            expected = [self.item_sub_1, self.item_sub_2, self.item_sub_03, self.item_sub_02, ]
            self.assertEqual(len(actual), len(expected))
            self.assertItemsEqual(actual, expected)
