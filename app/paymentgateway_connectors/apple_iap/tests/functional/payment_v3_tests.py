from __future__ import absolute_import, unicode_literals

import json
from datetime import datetime, timedelta
from httplib import OK

import httpretty
from flask_appsfoundry.exceptions import UnprocessableEntity
from mock import patch

from app import db, app
from app.auth.models import Client
from app.items.models import Item
from app.master.models import Currencies
from app.offers.choices.offer_type import SINGLE
from app.offers.models import OfferType
from app.orders.choices import NEW, PAYMENT_BILLED
from app.payment_gateways.models import PaymentGateway
from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP
from app.paymentgateway_connectors.apple_iap.v3 import helpers
from app.paymentgateway_connectors.apple_iap.v3.payment_validation import apple_payment_receipt_validation, \
    validate_order_vs_apple_receipt, get_product_id, validate_product_id_in_transaction, assert_repurchase, \
    get_valid_transaction, get_purchase_date
from app.payments.choices import WAITING_FOR_PAYMENT
from app.payments.choices.gateways import APPLE
from app.utils.datetimes import get_local, get_local_nieve, get_utc_nieve
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.payments import PaymentFactory


class ApplePaymentReceiptValidation(TestBase):

    @classmethod
    def setUpClass(cls):
        super(ApplePaymentReceiptValidation, cls).setUpClass()
        cls.init_data(cls.session)

    @classmethod
    def init_data(cls, s):

        cur_usd = s.query(Currencies).get(1)
        pg = s.query(PaymentGateway).get(APPLE)
        offer_type = s.query(OfferType).get(SINGLE)
        cls.client = s.query(Client).get(1)
        cls.offer1 = OfferFactory(id=43103, name='Scoop Premium',
                                  offer_type=offer_type, is_free=False, price_idr=89000, price_usd=6.99,
                                  price_point=1)
        brand = BrandFactory()
        cls.item_id = 1
        item1 = ItemFactory(id=cls.item_id, item_status=Item.Statuses.ready_for_consume.value, brand=brand)
        cls.offer1.items.append(item1)
        cls.offer1.brands.append(brand)

        cls.order_id_unified_receipt = int((datetime.now() - datetime(1970, 1, 1)).total_seconds()) + 1
        cls.order_id_unified_receipt_subs = int((datetime.now() - datetime(1970, 1, 1)).total_seconds()) + 2
        # create order for testing new receipt (IOS7+, unified receipt) - validation process
        cls.order_unified = OrderFactory(
            id=cls.order_id_unified_receipt, paymentgateway=pg,
            total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
            user_id=cls.super_admin.id, party=cls.super_admin,
            tier_code='.c.usd.1.99',
            order_status=WAITING_FOR_PAYMENT,
            currency_code=pg.base_currency.iso4217_code,
            client_id=cls.client.id)
        order_line2 = OrderLineFactory(
            order=cls.order_unified, offer=cls.offer1, user_id=cls.super_admin.id,
            currency_code=pg.base_currency.iso4217_code,
            price=cls.offer1.price_usd, final_price=cls.offer1.price_usd,
            orderline_status=NEW)
        cls.payment = PaymentFactory(
            order=cls.order_unified, user_id=cls.super_admin.id,
            paymentgateway_id=pg.id,
            currency_code=pg.base_currency.iso4217_code,
            amount=cls.order_unified.final_amount,
            is_active=True
            )
        # create for subscription purchase
        cls.order_unified_subscription = OrderFactory(
            id=cls.order_id_unified_receipt_subs, paymentgateway=pg,
            total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
            user_id=cls.super_admin.id, party=cls.super_admin,
            tier_code='.c.usd.1.99',
            order_status=WAITING_FOR_PAYMENT,
            currency_code=pg.base_currency.iso4217_code,
            client_id=cls.client.id)
        order_line_subs = OrderLineFactory(
            order=cls.order_unified_subscription, offer=cls.offer1, user_id=cls.super_admin.id,
            currency_code=pg.base_currency.iso4217_code,
            price=cls.offer1.price_usd, final_price=cls.offer1.price_usd,
            orderline_status=NEW)
        cls.payment_subs = PaymentFactory(
            order=cls.order_unified_subscription, user_id=cls.super_admin.id,
            paymentgateway_id=pg.id,
            currency_code=pg.base_currency.iso4217_code,
            amount=cls.order_unified_subscription.final_amount,
            is_active=True
        )
        s.commit()

    def setUp(self):
        self.session.add(self.super_admin)
        self.session.add(self.order_unified)
        self.session.add(self.payment)
        self.session.add(self.client)

    @httpretty.activate
    def test_apple_payment_receipt_validation(self):

        valid_receipt = self.get_mock_valid_unified_receipt_json(self.client, self.order_unified)

        self._mock_apple_response(
            status_code=OK,
            body=json.dumps(valid_receipt)
        )
        actual = apple_payment_receipt_validation(
            'dfasdfsafsadfsafsadfdsa',
            self.super_admin.id,
            self.order_unified,
            self.payment,
        )
        self.assertIsNotNone(actual)

    @staticmethod
    def _mock_apple_response(status_code=OK, body=None):
        mock_url_apple = "{}{}".format(helpers.get_apple_url(), app.config['URL_VERIFY_RECEIPT'])
        httpretty.register_uri(
            httpretty.POST,
            mock_url_apple,
            status=status_code,
            content_type='application/json',
            body=body
        )

    @httpretty.activate
    def test_failed_from_apple_api(self):
        # test apple_payment_receipt_validation but failed to get response from apple
        self._mock_apple_response(status_code=504)

        self.assertRaises(
            helpers.AppleRequestError,
            apple_payment_receipt_validation,
            'dfasdfsafsadfsafsadfdsa',
            self.super_admin.id,
            self.order_unified,
            self.payment,
        )

    @httpretty.activate
    def test_failed_receipt_json(self):
        # test apple_payment_receipt_validation but response apple is with status != 0 (not valid)
        self._mock_apple_response(
            status_code=OK,
            body=json.dumps({
                "status": 21002
            })
        )
        self.assertRaises(
            UnprocessableEntity,
            apple_payment_receipt_validation,
            'dfasdfsafsadfsafsadfdsa',
            self.super_admin.id,
            self.order_unified,
            self.payment,
        )

    def test_get_valid_transaction(self):
        s = db.session
        product_id = get_product_id(self.order_unified, s)

        dummy_trx_id = "442323232323"
        receipt_json = self.get_mock_valid_unified_receipt_json(self.client, self.order_unified, dummy_trx_id)
        actual = get_valid_transaction(
            in_app_data=receipt_json.get('receipt').get('in_app'),
            product_id=product_id
        )
        self.assertIsNotNone(actual)
        self.assertEqual(actual.get("original_transaction_id"), dummy_trx_id)
        self.assertEqual(actual.get("product_id"), product_id)

    def test_validate_order_vs_apple_receipt(self):
        s = db.session
        product_id = get_product_id(self.order_unified, s)

        dummy_trx_id = "442323232323"
        receipt_json = self.get_mock_valid_unified_receipt_json(self.client, self.order_unified, dummy_trx_id)

        apple_iap_data = PaymentAppleIAP(
            payment_id=self.payment.id,
            product_id=product_id,
            receipt_data="dfslakfjsalkfjsalfadfsdfasdfsd",
        )

        actual = validate_order_vs_apple_receipt(
            product_id,
            in_app_data=receipt_json.get('receipt').get('in_app'),
            order=self.order_unified,
            apple_iap_data=apple_iap_data,
            session=s,
            environment="SandBox",
        )
        self.assertIsNotNone(actual)
        self.assertEqual(actual.get("original_transaction_id"), dummy_trx_id)
        self.assertEqual(actual.get("is_sandbox"), True)
        self.assertEqual(actual.get("is_restore"), False)

        # test execute again for is_restore/is_repurchase flag (should returned True now)
        self.payment.payment_status = PAYMENT_BILLED
        self.session.add(self.payment)
        actual = validate_order_vs_apple_receipt(
            product_id,
            in_app_data=receipt_json.get('receipt').get('in_app'),
            order=self.order_unified,
            apple_iap_data=apple_iap_data,
            session=s,
            environment="SandBox",
        )
        self.assertEqual(actual.get("is_restore"), True)

    def test_validate_product_id_in_transaction(self):
        s = db.session
        dummy_date_for_single = get_utc_nieve() + timedelta(days=1)
        product_id = get_product_id(self.order_unified, s)
        dummy_trx_id = "12321321999111"

        trx = self._get_valid_trx_json(self.client, self.order_unified, dummy_trx_id)
        # test single:
        valid_trx = validate_product_id_in_transaction(product_id, trx, dummy_date_for_single)
        self.assertIsNotNone(valid_trx)

        # test single with valid original trx id
        valid_trx = validate_product_id_in_transaction(
            product_id, trx, dummy_date_for_single, original_transaction_id=dummy_trx_id)
        self.assertIsNotNone(valid_trx)

        # test single with invalid original trx id
        invalid_trx = validate_product_id_in_transaction(
            product_id, trx, dummy_date_for_single, original_transaction_id="123")
        self.assertIsNone(invalid_trx)

        # test valid subscription:
        trx['expires_date'] = (get_utc_nieve() + timedelta(days=30)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT")
        valid_trx = validate_product_id_in_transaction(product_id, trx, dummy_date_for_single,
                                                       previous_expires_date=None)
        self.assertIsNotNone(valid_trx)

        # test valid subscription and renewal:
        previous_expires_date = get_utc_nieve() - timedelta(days=10)
        trx['expires_date'] = (get_utc_nieve() + timedelta(days=30)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT")
        valid_trx = validate_product_id_in_transaction(
            product_id, trx, dummy_date_for_single,
            original_transaction_id=dummy_trx_id, previous_expires_date=previous_expires_date)
        self.assertIsNotNone(valid_trx)

        # test not valid subscription, already expires
        trx['expires_date'] = (get_utc_nieve() - timedelta(days=15)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT")
        valid_trx = validate_product_id_in_transaction(product_id, trx, dummy_date_for_single)
        self.assertIsNone(valid_trx)

        # test not valid subscription, trx with original trx id not found
        previous_expires_date = get_utc_nieve() - timedelta(days=10)
        trx['expires_date'] = (get_utc_nieve() - timedelta(days=15)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT")
        valid_trx = validate_product_id_in_transaction(
            product_id, trx, dummy_date_for_single,
            original_transaction_id="12345", previous_expires_date=previous_expires_date)
        self.assertIsNone(valid_trx)

        # test not valid subscription, already expires compare to previous_expires_date
        previous_expires_date = get_utc_nieve() - timedelta(days=10)
        trx['expires_date'] = (get_utc_nieve() - timedelta(days=15)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT")
        valid_trx = validate_product_id_in_transaction(
            product_id, trx, dummy_date_for_single,
            original_transaction_id=dummy_trx_id, previous_expires_date=previous_expires_date)
        self.assertIsNone(valid_trx)

        # test no valid subscription, trx was cancelled
        trx['expires_date'] = (get_utc_nieve() + timedelta(days=15)).strftime("%Y-%m-%d %H:%M:%S Etc/GMT")
        trx['cancellation_date'] = get_utc_nieve().strftime("%Y-%m-%d %H:%M:%S Etc/GMT")
        valid_trx = validate_product_id_in_transaction(product_id, trx, dummy_date_for_single)
        self.assertIsNone(valid_trx)

    def test_get_product_id(self):
        s = db.session
        product_id = get_product_id(self.order_unified, s)
        self.assertEqual(
            product_id,
            'com.appsfoundry.scoop{}'.format(self.order_unified.tier_code)
        )

    def test_assert_repurchase(self):
        payment = PaymentFactory(
            payment_status=PAYMENT_BILLED
        )
        transaction_id = 7789112345678
        web_order_line_item_id = 7789112345678123
        product_id = 'abc.pprod.c.kldjfalksjdfs'
        apple_iap_data = PaymentAppleIAP(
            payment=payment,
            product_id=product_id,
            receipt_data='fsklfaslkjdfksal',
            transaction_id=transaction_id,
            web_order_line_item_id=web_order_line_item_id
        )
        self.session.add(apple_iap_data)
        self.session.commit()

        # test not repurchase
        actual_is_purchase = assert_repurchase(
            purchase_date=get_utc_nieve(),
            transaction_id=77881232312321,
            web_order_line_item_id=77881232312321123,
            session=self.session,
            product_id="com.abc.com.123123"
        )
        self.assertEqual(actual_is_purchase, False)

        # test repurchase because transaction id found!
        actual_is_purchase = assert_repurchase(
            purchase_date=get_utc_nieve(),
            transaction_id=transaction_id,
            web_order_line_item_id=web_order_line_item_id,
            session=self.session,
            product_id=product_id
        )
        self.assertEqual(actual_is_purchase, True)

        # test repurchase because purchase date is old date!
        actual_is_purchase = assert_repurchase(
            purchase_date=get_utc_nieve() - timedelta(days=60),
            transaction_id="12321312321312",
            web_order_line_item_id=web_order_line_item_id,
            session=self.session,
            product_id=product_id
        )
        self.assertEqual(actual_is_purchase, True)

    def test_get_purchase_date(self):
        original_purchase_date = (get_local_nieve() - timedelta(days=10)).replace(microsecond=0)
        purchase_date = get_local_nieve().replace(microsecond=0)
        trx_id = {
            "original_purchase_date": original_purchase_date.strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
            "purchase_date": purchase_date.strftime("%Y-%m-%d %H:%M:%S Etc/GMT")
        }
        # test single non consumable
        web_order_line_item_id = None
        product_id = 'com.appsfoundry.scoop.ars.SUB_1003MTH'
        actual_date = get_purchase_date(product_id, trx_id, web_order_line_item_id)
        self.assertEqual(actual_date, original_purchase_date)

        # test single consumable
        web_order_line_item_id = None
        product_id = 'com.appsfoundry.scoop.c.usd.6.99'
        actual_date = get_purchase_date(product_id, trx_id, web_order_line_item_id)
        self.assertEqual(actual_date, purchase_date)

        # test subscription (not single)
        web_order_line_item_id = "12312321312"
        product_id = 'com.appsfoundry.scoop.ars.SUB_1003MTH'
        actual_date = get_purchase_date(product_id, trx_id, web_order_line_item_id)
        self.assertEqual(actual_date, purchase_date)

    @staticmethod
    def _get_valid_trx_json(client, order, dummy_trx_id="1231231321"):
        # valid receipt transaction
        return {
            # "product_id": "com.appsfoundry.com.c.0.99.usd",
            "product_id": "{}{}".format(
                client.product_id,
                order.tier_code
            ),
            "transaction_id": str(dummy_trx_id),
            "original_transaction_id": str(dummy_trx_id),
            "web_order_line_item_id": "{}123".format(dummy_trx_id),
            "purchase_date": get_local().strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
            "original_purchase_date": get_local().strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
        }

    def get_mock_valid_unified_receipt_json(self, client, order, dummy_trx_id="1231231321"):
        return {
            "status": 0,
            "environment": "Sandbox",
            "receipt": {
                "in_app": [
                    self._get_valid_trx_json(client, order, dummy_trx_id),
                    # some dummy data
                    {
                        "quantity": "1",
                        "product_id": "com.appsfoundry.scoop.ars.SUB_1003MTH",
                        "transaction_id": "1000000166563547",
                        "original_transaction_id": "1000000166563403",
                        "purchase_date": "2015-08-06 07:14:31 Etc/GMT",
                        "purchase_date_ms": "1438845271000",
                        "purchase_date_pst": "2015-08-06 00:14:31 America/Los_Angeles",
                        "original_purchase_date": "2015-08-06 07:10:52 Etc/GMT",
                        "original_purchase_date_ms": "1438845052000",
                        "original_purchase_date_pst": "2015-08-06 00:10:52 America/Los_Angeles",
                        "expires_date": "2015-08-06 07:29:31 Etc/GMT",
                        "expires_date_ms": "1438846171000",
                        "expires_date_pst": "2015-08-06 00:29:31 America/Los_Angeles",
                        "web_order_line_item_id": "1000000030259814",
                        "is_trial_period": "false"
                    },
                    {
                        "quantity": "1",
                        "product_id": "com.appsfoundry.scoop.ID_MIIN2015MTH07DT0612",
                        "transaction_id": "1000000166162028",
                        "original_transaction_id": "1000000166162028",
                        "purchase_date": "2015-08-04 04:29:24 Etc/GMT",
                        "purchase_date_ms": "1438662564000",
                        "purchase_date_pst": "2015-08-03 21:29:24 America/Los_Angeles",
                        "original_purchase_date": "2015-08-04 04:29:24 Etc/GMT",
                        "original_purchase_date_ms": "1438662564000",
                        "original_purchase_date_pst": "2015-08-03 21:29:24 America/Los_Angeles",
                        "is_trial_period": "false"
                    },
                    {
                        "quantity": "1",
                        "product_id": "com.appsfoundry.scoop.c.usd.6.99",
                        "transaction_id": "1000000185071679",
                        "original_transaction_id": "1000000185071679",
                        "purchase_date": "2015-12-15 07:02:09 Etc/GMT",
                        "purchase_date_ms": "1450162929000",
                        "purchase_date_pst": "2015-12-14 23:02:09 America/Los_Angeles",
                        "original_purchase_date": "2015-12-15 07:02:09 Etc/GMT",
                        "original_purchase_date_ms": "1450162929000",
                        "original_purchase_date_pst": "2015-12-14 23:02:09 America/Los_Angeles",
                        "is_trial_period": "false"
                    },
                ],
            }
        }
