import requests
import json
import ast
from datetime import datetime, timedelta
from sqlalchemy import desc

from app import app
from app.helpers import conflict_message, payment_logs

from app.master.choices import PlatformType
from app.orders.api import OrderCheckout, OrderComplete
from app.payments.api import PaymentValidateApi

from app.orders.models import Order, OrderDetail, OrderLine
from app.offers.models import Offer
from app.items.models import Item
from app.payments.choices.gateways import APPLE
from app.payments.models import Payment
from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP, AppleReceiptIdentifiers
from app.paymentgateway_connectors.apple_iap.apple_iap import AppleIAPCheckout
from app.payments.signature_generator import GenerateSignature
from app.paymentgateway_connectors.apple_iap.helpers import AppleIAPHelpers
from app.orders.order_checkout_platform import OrderCheckoutConstructPlatform
from app.orders.order_checkout_base import OrderCheckoutConstructBase
from app.orders.order_confirm import OrderConfirmConstruct
from app.payments.helpers import PaymentConstruct
from app.paymentgateway_connectors.apple_iap import itunes_product_type
from app.offers.models import OfferPlatform, Offer


class AppleIAPRenewalTemp(object):
    def __init__(self, divider=None, original_transaction_id=None):
        self.renewal_succeed = 0
        self.renewal_failed = 0
        self.results = None
        self.order_id = None
        self.payment_id = None
        self.receipt_data = None
        self.tier_code = None

        self.divider = divider
        self.original_transaction_id = original_transaction_id

        self.args = None
        self.user_id = None
        self.payment_gateway_id = None
        self.client_id = None
        self.platform_id = None
        self.email = None
        self.order_lines = []
        self.user_city = None
        self.user_country = None
        self.user_name = None
        self.user_street_address = None
        self.user_zipcode = None
        self.user_state = None
        self.latitude = None
        self.longitude = None
        self.ip_address = None
        self.os_version = None
        self.device_model = None
        self.offer_id = None
        self.quantity = None
        self.name = None
        self.signature = None
        self.new_receipt_data = None
        self.order_number = None
        self.temp_order_id = None
        self.currency_code = None
        self.final_amount = None
        self.is_sandbox = False
        self.result_json = dict()
        self.count = 0

    def get_user_order_line(self):

        op = OfferPlatform.query.filter_by(tier_code=self.tier_code).first()
        o = Offer.query.filter_by(id=op.offer_id).first()

        self.order_lines = list()

        self.order_lines.append({
            'offer_id': o.id,
            'quantity': 1,
            'name': o.name,
        })

        return True

    def do_order_checkout(self):
        oc_args = {
            'user_id': self.user_id,
            'payment_gateway_id': self.payment_gateway_id,
            'client_id': self.client_id,
            'platform_id': self.platform_id,
            'order_lines': self.order_lines,  # [{"offer_id":35086,"quantity":1,"name":"Asian Diver \/ ED 135 2014"}],
            'user_email': self.email,
            'user_name': self.user_name,
            'user_street_address': self.user_street_address,
            'user_city': self.user_city,
            'user_zipcode': self.user_zipcode,
            'user_state': self.user_state,
            'user_country': self.user_country,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'ip_address': self.ip_address,
            'os_version': self.os_version,
            'device_model': self.device_model
        }

        if self.platform_id in [PlatformType.ios, PlatformType.android, PlatformType.windows_phone]:
            oc = OrderCheckoutConstructPlatform(oc_args, 'POST').construct()
        else:
            oc = OrderCheckoutConstructBase(oc_args, 'POST').construct()

        if oc.status_code not in [201, 200]:
            return Exception("Order Checkout Failed: %s" % oc.data)

        return json.loads(oc.data)

    def do_order_confirm(self):
        oconfirm_args = {
            'user_id': self.user_id,
            'payment_gateway_id': self.payment_gateway_id,
            'client_id': self.client_id,
            'currency_code': self.currency_code,
            'final_amount': self.final_amount,
            'order_number': self.order_number,
            'temp_order_id': self.temp_order_id
        }

        oconfirm = OrderConfirmConstruct(oconfirm_args, 'POST').construct()

        return json.loads(oconfirm.data)

    def do_payment_validate(self):
        pv_args = {
            "order_id": self.order_id,
            "signature": self.signature,
            "user_id": self.user_id,
            "payment_gateway_id": self.payment_gateway_id,
            "receipt_data": self.new_receipt_data,
            "api": "v1/payments/validate",
            "method": "POST"
        }

        pv = PaymentConstruct(args=pv_args).initialize_transaction()

        return pv

    def auto_purchase(self):
        '''
        There's 3 steps in auto_purchase
        1. order checkout
        2. order confirm
        3. payment validate
        '''
        try:
            oc = self.do_order_checkout()
            self.order_number = oc.get('order_number', None)
            self.temp_order_id = oc.get('temp_order_id', None)
            self.currency_code = oc.get('currency_code', None)
            self.final_amount = oc.get('final_amount', None)

            oconfirm = self.do_order_confirm()
            self.order_id = oconfirm.get('order_id', None)

            self.signature = GenerateSignature(order_id=self.order_id).construct()
            pv = self.do_payment_validate()

            return True

        except Exception:
            return False

    def get_payment(self):
        payment = Payment.query.filter_by(order_id=self.order_id).first()
        return payment

    def json_response(self):
        json_form = {
            'results': self.results
        }

        return json.dumps(json_form)

    def subscription_renewal(self):
        self.result_json['trx_result'] = list()
        try:
            if self.original_transaction_id:
                expired_receipt_data = list()
                expired_data = AppleReceiptIdentifiers.query.filter_by(
                    original_transaction_id=self.original_transaction_id
                ).order_by(desc(AppleReceiptIdentifiers.expired_date)).all()[0]
                expired_receipt_data.append(expired_data)
            else:
                expired_receipt_data = PaymentAppleIAP().get_expired_receipt_data().all()

            if not len(expired_receipt_data) > 0:
                self.results = "No expired receipt detected."
                return True, self.json_response()

            total_expired_receipt = len(expired_receipt_data)

            if not self.original_transaction_id:
                expired_receipt_limit = total_expired_receipt / self.divider

                if (total_expired_receipt % self.divider) != 0:
                    expired_receipt_limit += 1

                expired_receipt_data = \
                    PaymentAppleIAP().get_expired_receipt_data().limit(
                        expired_receipt_limit).all()

            for receipt_data in expired_receipt_data:
                self.is_sandbox = receipt_data.is_test_payment
                self.user_id = receipt_data.user_id

                if receipt_data.itunes_product_type == itunes_product_type.SUBSCRIPTION:
                    pa = PaymentAppleIAP.query.filter_by(
                        original_transaction_id=receipt_data.original_transaction_id,
                        web_order_line_item_id=receipt_data.web_order_line_id).first()
                else:
                    pa = PaymentAppleIAP.query.filter_by(
                        original_transaction_id=receipt_data.original_transaction_id).first()

                self.receipt_data = pa.receipt_data.decode('unicode_escape')
                self.client_id = receipt_data.client_id
                self.platform_id = PlatformType.ios.value
                self.payment_gateway_id = APPLE

                apple_verification_status, av_rslt = AppleIAPHelpers(
                    receipt_data=self.receipt_data).verify_to_apple()

                if not apple_verification_status:
                    payment_logs(args=self.args, error_message="Verification Failed")

                    self.result_json['trx_result'].append(
                        "Renewal no. %s Verification Failed" % self.count)
                    self.renewal_failed += 1
                    continue

                apple_verification_rslt = av_rslt['verification_rslt']
                receipt = apple_verification_rslt.get('receipt')

                latest_receipt_info = apple_verification_rslt.get('latest_receipt_info', None)
                latest_receipt_data = apple_verification_rslt.get('latest_receipt')

                if not latest_receipt_data:
                    payment_logs(
                        args=self.args,
                        error_message="Latest receipt data not found, not renewed. verification_rslt: {}".format(
                            apple_verification_rslt))

                    receipt_data.is_processed = True
                    receipt_data.save()
                    self.result_json['trx_result'].append(
                        "Renewal no. %s Latest receipt data not found %s" % (self.count, self.receipt_data))
                    self.renewal_failed += 1
                    continue

                latest_web_orderline_id = latest_receipt_info.get('web_order_line_item_id', None)
                web_orderline_id = receipt.get('web_order_line_item_id', None)

                if web_orderline_id == latest_web_orderline_id:
                    self.result_json['trx_result'].append(
                        "Renewal no. %s Invalid web_orderline_id" % self.count)
                    self.renewal_failed += 1
                    continue

                self.new_receipt_data = latest_receipt_data
                new_original_transaction_id = latest_receipt_info.get('original_transaction_id')

                if web_orderline_id is None:
                    origin_tier_code = latest_receipt_info.get('product_id', None)
                    splitted_tier_code = origin_tier_code.split(".")
                    self.tier_code = ".%s" % splitted_tier_code[3]
                else:
                    origin_tier_code = latest_receipt_info.get('product_id', None)
                    splitted_tier_code = origin_tier_code.split(".")
                    self.tier_code = ".%s.%s" % (splitted_tier_code[3], splitted_tier_code[4])

                self.get_user_order_line()

                new_receipt_check = AppleIAPHelpers().check_new_receipt(
                    client_id=self.client_id,
                    web_order_line_id=latest_web_orderline_id,
                    itunes_product_type_id=itunes_product_type.SUBSCRIPTION,
                    original_transaction_id=new_original_transaction_id)

                if not new_receipt_check:
                    self.result_json['trx_result'].append(
                        "Renewal no. %s Invalid receipt" % self.count)
                    self.renewal_failed += 1
                    continue

                if self.auto_purchase():
                    receipt_data.is_processed = True
                    receipt_data.save()
                    self.renewal_succeed += 1
                else:
                    self.result_json['trx_result'].append(
                        "Renewal no. %s auto purchase failed" % self.count)
                    self.renewal_failed += 1
                self.count += 1

            self.result_json['renewal_succeed'] = self.renewal_succeed
            self.result_json['renewal_failed'] = self.renewal_failed
            self.results = self.result_json
            return True, self.json_response()

        except Exception as e:
            return False, e
