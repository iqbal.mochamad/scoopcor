from datetime import datetime, timedelta, date

from enum import IntEnum
from flask_appsfoundry.models import TimestampMixin

from sqlalchemy.orm import deferred
from sqlalchemy.sql.expression import false
from sqlalchemy import desc

from app import db


class ReceiptVersion(IntEnum):
    ios6_receipt = 6
    ios7_unified_receipt = 7


class PaymentAppleIAP(db.Model, TimestampMixin):
    __tablename__ = 'core_paymentappleiaps'

    id = db.Column(db.Integer, primary_key=True)
    payment_id = db.Column(db.Integer, db.ForeignKey('core_payments.id'))
    payment = db.relationship("Payment", backref=db.backref('payment_apple_iap', lazy='dynamic'))
    product_id = db.Column(db.String(100))
    original_transaction_id = db.Column(db.BigInteger)
    expires_date = db.Column(db.DateTime())
    web_order_line_item_id = db.Column(db.BigInteger)
    transaction_id = db.Column(db.BigInteger)
    purchase_date = db.Column(db.DateTime())
    original_purchase_date = db.Column(db.DateTime())
    status = db.Column(db.String)
    receipt_data = deferred(db.Column(db.Text), group='iapdetails')
    receipt = deferred(db.Column(db.Text), group='iapdetails')

    is_processed = db.Column(db.Boolean, default=False)
    renewal_status = db.Column(db.Integer, default=1)
    receipt_version = db.Column(db.Integer, default=6)

    def __repr__(self):
        return '<id : %s>' % (self.id)

    def save(self, session=None):
        session = session or db.session
        try:
            session.add(self)
            session.commit()
            return {'invalid': False, 'message': "Payment successfully added", 'id': self.id}
        except Exception as e:
            session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Payment successfully deleted"}
        except Exception as e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['payment_id'] = self.payment_id
        rv['original_transaction_id'] = self.original_transaction_code
        rv['expires_date'] = self.expires_date
        rv['web_order_line_item_id'] = self.web_order_line_item_id
        rv['transaction_id'] = self.transaction_id
        rv['purchase_date'] = self.purchase_date
        rv['original_purchase_date'] = self.original_purchase_date
        rv['receipt_data'] = self.receipt_data
        rv['receipt'] = self.receipt
        rv['status'] = self.status
        rv['is_processed'] = self.is_processed
        rv['renewal_status'] = self.renewal_status

        return rv

    def get_expired_receipt_data(self):
        now_date = datetime.utcnow().date() - timedelta(days=1)
        next_date = datetime.utcnow().date()
        start_date = '%s 17:00:00' % (now_date)
        end_date = '%s 16:59:59' % (next_date)
        pa = AppleReceiptIdentifiers.query.filter(
            AppleReceiptIdentifiers.expired_date > start_date,
            AppleReceiptIdentifiers.expired_date < end_date,
            AppleReceiptIdentifiers.is_processed == false()
        )
        return pa

    def get_receipt_data(self):
        receipt = AppleReceiptIdentifiers.query.filter_by(
            original_transaction_id = str(self.original_transaction_id),
            itunes_product_type = 2,
            is_processed = False,
        ).order_by(desc(AppleReceiptIdentifiers.expired_date)).first()
        return receipt


class AppleReceiptIdentifiers(db.Model, TimestampMixin):

    __tablename__ = 'core_applereceiptidentifiers'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer)
    bundle_id = db.Column(db.String)
    original_transaction_id = db.Column(db.String)
    web_order_line_id = db.Column(db.String)
    order_id = db.Column(db.Integer)
    itunes_product_type = db.Column(db.Integer)
    client_id = db.Column(db.Integer)
    purchase_date = db.Column(db.DateTime())
    expired_date = db.Column(db.DateTime())
    status = db.Column(db.String)
    is_active = db.Column(db.Boolean, default=True)
    is_test_payment = db.Column(db.Boolean, default=False)
    is_trial = db.Column(db.Boolean, default=False)
    is_processed = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<id : %s>' % (self.id)

    def save(self, session=None):
        session = session or db.session
        try:
            session.add(self)
            session.commit()
            return {'invalid': False, 'message': "record successfully added"}
        except Exception as e:
            session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "record successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = dict()

        rv['id'] = self.id
        rv['user_id'] = self.user_id
        rv['bundle_id'] = self.bundle_id
        rv['original_transaction_id'] = self.original_transaction_id
        rv['web_order_line_id'] = self.web_order_line_id
        rv['order_id'] = self.order_id
        rv['itunes_product_type'] = self.itunes_product_type
        rv['client_id'] = self.client_id
        rv['purchase_date'] = self.purchase_date
        rv['expired_date'] = self.expired_date
        rv['status'] = self.status
        rv['is_active'] = self.is_active
        rv['is_test_payment'] = self.is_test_payment
        rv['is_processed'] = self.is_processed
        rv['is_trial'] = self.is_trial

        return rv

class RenewalLogProcessings(db.Model, TimestampMixin):

    __tablename__ = 'core_logs_iaprenewal'

    id = db.Column(db.Integer, primary_key=True)
    original_transaction_id = db.Column(db.BigInteger)
    web_order_line_id = db.Column(db.BigInteger)
    start_process = db.Column(db.DateTime())
    end_process = db.Column(db.DateTime())
    error = db.Column(db.Text)
    apple_receipt = db.Column(db.Text)
    apple_response = db.Column(db.Text)
    is_patch = db.Column(db.Boolean, default=False)
    is_notice = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<id : %s>' % (self.id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "record successfully added"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "record successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['original_transaction_id'] = self.original_transaction_id
        rv['web_order_line_id'] = self.web_order_line_id
        rv['start_process'] = self.start_process
        rv['end_process'] = self.end_process
        rv['error'] = self.error
        rv['apple_receipt'] = self.apple_receipt
        rv['apple_response'] = self.apple_response
        rv['is_patch'] = self.is_patch
        rv['is_notice'] = self.is_notice

        return rv
