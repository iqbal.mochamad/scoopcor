'''
Itunes product type:
0 Single Consumable
1 Single Non-Consumable
2 Subscription
'''

SINGLE_CONSUMABLE = 0
SINGLE_NON_CONSUMABLE = 1
SUBSCRIPTION = 2
