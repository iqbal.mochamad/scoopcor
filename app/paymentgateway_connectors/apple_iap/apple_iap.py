import httplib

from app import app
from app.helpers import payment_logs

from . import iap_status, itunes_product_type

from app.orders.models import Order
from app import constants
from app.payments import payment_status
from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP, AppleReceiptIdentifiers
from app.paymentgateway_connectors.apple_iap.helpers import AppleIAPHelpers
from app.paymentgateway_connectors.helpers import TransactionValidation


class AppleIAPCheckout():
    """
    This API used for validating IAP purchase. It will call Itunes server and
    check whether the receipt is valid purchase. It will response couples of data
    to indicate the result of validation.
    """

    def __init__(self, receipt_data=None, payment_id=None, order_id=None,
                 args=None, is_sandbox=False, user_id=None):
        """
        :param is_sandbox: REQUIRED
        :param receipt-data: REQUIRED
        """
        self.receipt_data = receipt_data
        self.payment_id = payment_id
        self.order_id = order_id
        self.args = args
        self.is_sandbox = is_sandbox
        self.is_trial = False
        self.user_id = user_id

        self.message = ""
        self.status_code = None
        self.apple_response = None
        self.original_transaction_id = None
        self.web_order_line_id = None
        self.product_sku = None
        self.web_order_line_item_id = None
        self.transaction_id = None
        self.purchase_date = None
        self.original_purchase_date = None
        self.expires_date = None
        self.itunes_product_type = None
        self.payment_gateway_id = None
        self.client_id = None
        self.currency_code = None
        self.final_amount = None
        self.temp_order_id = None
        self.order_number = None

    def update_status_code(self):
        """
        function to update IAP status code
        """
        pa = PaymentAppleIAP.query.filter_by(payment_id=self.payment_id).first()
        pa.status = self.status_code
        pa_rslt = pa.save()

        if pa_rslt.get('invalid', False):
            return False, payment_logs(args=self.args, error_message=pa_rslt.get('message', ''))

        return True

    def update_pg_connector_db(self):
        cip_status, cip_rslt = self.construct_iap_save_db_params()
        if not cip_status:
            return False, cip_rslt

        pa = PaymentAppleIAP.query.filter_by(payment_id=self.payment_id).first()
        pa.product_id = self.apple_response.get('receipt').get('product_id')
        pa.original_transaction_id = self.original_transaction_id
        pa.expires_date = self.expires_date
        pa.web_order_line_item_id = self.web_order_line_item_id
        pa.transaction_id = self.transaction_id
        pa.purchase_date = self.purchase_date
        pa.original_purchase_date = self.original_purchase_date
        pa.receipt = str(self.apple_response)
        pa_rslt = pa.save()

        if pa_rslt.get('invalid', False):
            return False, pa_rslt.get('message', '')

        return True, 'OK'

    def save_to_pg_connector_db(self):
        pa_params = {
            "payment_id": self.payment_id,
            "receipt_data": self.receipt_data
        }

        pa = PaymentAppleIAP(**pa_params).save()
        if pa.get('invalid', False):
            return False, pa.get('message', '')

        return True, 'OK'

    def construct_iap_save_db_params(self):
        try:
            op = self.apple_response.get('receipt').get('original_purchase_date')
            op = op.split(' ')
            self.original_purchase_date = "%s %s" % (op[0],op[1])

            ep = self.apple_response.get('receipt').get('expires_date_formatted', None)
            if ep is not None:
                ep = ep.split(' ')
                self.expires_date = "%s %s" % (ep[0], ep[1])
            else:
                self.expires_date = None

            ap = self.apple_response.get('receipt').get('purchase_date', None)
            if ap is not None:
                ap = ap.split(' ')
                self.purchase_date = "%s %s" % (ap[0], ap[1])
            else:
                self.purchase_date = None

            self.original_transaction_id = self.apple_response.get('receipt').get('original_transaction_id', None)
            self.web_order_line_id = self.apple_response.get('receipt').get('web_order_line_item_id', None)
            self.product_sku = self.apple_response.get('receipt').get('product_id', None)
            self.web_order_line_item_id = self.apple_response.get('receipt').get('web_order_line_item_id', None)
            self.transaction_id = self.apple_response.get('receipt').get('transaction_id', None)

            return True, 'OK'

        except Exception, e:
            return False, str(e)

    def get_client_id(self):
        order = Order.query.filter_by(id=self.order_id).first()
        client_id = order.client_id

        return client_id

    def master_order(self):
        order = Order.query.filter_by(id=self.order_id).first()
        return order

    def set_transaction_status_message(self):
        if self.status_code == iap_status.INVALID_RECEIPT_PRODUCTION:
            self.message = "Invalid receipt given"

        if self.status_code == iap_status.INVALID_RECEIPT_SANDBOX:
            self.message = "Invalid receipt given"

        if self.status_code == iap_status.SUBSCRIPTION_EXPIRED:
            self.message = "This receipt is valid but the subscription has expired."

        if self.status_code == iap_status.SERVER_NOT_AVAILABLE:
            self.message = "The receipt server is not currently available."

        if self.status_code == iap_status.SHARED_SECRET_NOT_MATCH:
            self.message = "The shared secret you provided does not match the shared secret on file for your account."

        if self.status_code == iap_status.AUTHENTICATION_FAILED:
            self.message = "The receipt could not be authenticated."

        if self.status_code == iap_status.RECEIPT_MALFORMED:
            self.message = "The data in the receipt-data property was malformed."

        if self.status_code == iap_status.INVALID_JSON_OBJECT:
            self.message = "The App Store could not read the JSON object you provided."

        if self.status_code == iap_status.ACCEPTED:
            self.message = "accepted"

    def verify_payment_iap(self):
        """
        This is main function IAP validation.
        """

        try:
            save_to_db_status, save_to_db_rslt = self.save_to_pg_connector_db()
            if not save_to_db_status:
                response_form = {
                    "args": self.args,
                    "error_message": save_to_db_rslt,
                    "error_code": constants.CLIENT_500241,
                    "user_message": "Failed to save transaction data to DB"}

                return False, response_form

            # this is for verify to apple if sandbox or not.
            verify_status, verify_rslt = AppleIAPHelpers(
                receipt_data=self.receipt_data).verify_to_apple()

            if not verify_status:
                response_form = {
                    "args": self.args,
                    "error_message": verify_rslt,
                    "error_code": constants.CLIENT_400240,
                    "user_message": "Verification to apple failed"}

                return False, response_form

            self.apple_response = verify_rslt.get('verification_rslt')
            self.is_sandbox = verify_rslt.get('is_sandbox')
            self.is_trial = verify_rslt.get('is_trial')

            # this if for validate data transaction with data orders.
            client_id = self.get_client_id()
            tv_status, tv_rslt = TransactionValidation(
                order_id=self.order_id,
                platform_id=1,
                client_id=client_id,
                purchase_item_tier_code=self.apple_response.get('receipt').get('product_id', None)
            ).validate()

            if not tv_status:
                response_form = {
                    "args": self.args,
                    "error_message": tv_rslt,
                    "error_code": constants.CLIENT_400241,
                    "user_message": "Transaction verification failed."}

                return False, response_form

            self.status_code = self.apple_response.get('status')
            self.update_status_code()
            self.set_transaction_status_message()

            if self.status_code != 0:
                response_form = {
                    "args": self.args,
                    "error_message": "%s %s" % (self.status_code, self.message),
                    "error_code": constants.CLIENT_400242,
                    "user_message": "Validation return unsuccessful status",
                    "status_code": self.status_code}

                return False, response_form

            update_db_status, update_db_rslt = self.update_pg_connector_db()
            if not update_db_status:
                response_form = {
                    "args": self.args,
                    "error_message": update_db_rslt,
                    "error_code": constants.CLIENT_500241,
                    "user_message": "Failed to save transaction data to DB"}

                return False, response_form

            if self.web_order_line_id is not None:
                self.itunes_product_type = itunes_product_type.SUBSCRIPTION
            else:
                if self.product_sku.find(app.config['CONSUMABLE_MARK']) != -1:
                    self.itunes_product_type = itunes_product_type.SINGLE_CONSUMABLE
                else:
                    self.itunes_product_type = itunes_product_type.SINGLE_NON_CONSUMABLE

            check_new_receipt_params = {
                "client_id": client_id,
                "web_order_line_id": self.web_order_line_id,
                "itunes_product_type_id": self.itunes_product_type,
                "original_transaction_id": self.original_transaction_id
            }
            args = {
                "order_id": self.order_id
            }

            new_receipt = AppleIAPHelpers().check_new_receipt(**check_new_receipt_params)

            if not new_receipt:
                response_form = dict()
                response_form['original_transaction_id'] = self.original_transaction_id
                response_form['expires_date'] = self.expires_date
                response_form['status_code'] = payment_status.PAYMENT_RESTORED
                response_form['purchase_date'] = self.purchase_date
                response_form['is_sandbox'] = self.is_sandbox
                return True, response_form

            arc_params = {
                "bundle_id": self.apple_response.get('receipt').get('bid'),
                "original_transaction_id": self.original_transaction_id,
                "web_order_line_id": self.web_order_line_id,
                "itunes_product_type_id": self.itunes_product_type,
                "order_id": self.order_id,
                "purchase_date": self.purchase_date,
                "expired_date": self.expires_date,
                "trx_status": self.apple_response.get('status'),
                "client_id": client_id,
                "is_sandbox": self.is_sandbox,
                "user_id": self.user_id,
                "is_trial": self.is_trial
            }

            ari_status, ari_rslt = AppleIAPHelpers(**arc_params).save_to_applereceiptidentifiers()
            if not ari_status:
                response_form = {
                    "args": self.args,
                    "error_message": ari_rslt,
                    "error_code": constants.CLIENT_500242,
                    "user_message": "Failed to save receipt data to DB"}
                return False, response_form

            response_form = dict()
            response_form['receipt'] = self.apple_response
            response_form['status_code'] = self.status_code
            response_form['original_transaction_id'] = self.original_transaction_id
            response_form['expires_date'] = self.expires_date
            response_form['is_trial'] = self.is_trial

            # If non-consumable the purchase_date is original_purchase_date
            if self.itunes_product_type == itunes_product_type.SINGLE_NON_CONSUMABLE:
                response_form['purchase_date'] = self.original_purchase_date
            else:
                response_form['purchase_date'] = self.purchase_date

            response_form['is_sandbox'] = self.is_sandbox
            return True, response_form

        except Exception, e:
            response_form = {
                    "args": self.args,
                    "error_message": str(e),
                    "error_code": constants.CLIENT_500240,
                    "user_message": "Internal server error occured",
                    "status_code": httplib.INTERNAL_SERVER_ERROR}
            return False, response_form

