import httplib

from flask import g

from app import constants
from app.master.choices import PlatformType, IOS
from app.orders.order_restore import OrderRestore
from .helpers import AppleIAPHelpers
from . import iap_status


class AppleIAPRestore(object):
    """ This API class is for restoring paid items.

    :param args:
    """
    def __init__(self, args=None):
        self.user_id = args.get('user_id', g.current_user.id)
        self.platform_id = args.get('platform_id', IOS)
        self.receipt_data = args.get('receipt_data', None)
        self.purchase_date = None
        self.original_purchase_date = None
        self.receipt = None
        self.tier_code = None
        self.args = args
        self.web_order_line_id = None

        self.verification_rslt = None
        self.format_rslt = None
        self.restore_rslt = None

    def restore_verification(self):
        """ Verify the receipt to apple before doing restore.

        :return: Boolean
        :rtype: bool
        """
        if self.platform_id == PlatformType.ios:
            apple_verify_status, apple_verify_rslt = \
                AppleIAPHelpers(
                    receipt_data=self.receipt_data).verify_to_apple()

            apple_response = apple_verify_rslt['verification_rslt']

            if not apple_verify_status or apple_response['status'] \
                    not in (iap_status.ACCEPTED, iap_status.SUBSCRIPTION_EXPIRED):
                response_form = {
                    "args": self.args,
                    "error_message": apple_response,
                    "error_code": constants.CLIENT_400240,
                    "user_message": "Verification to apple failed"}

                self.verification_rslt = response_form
                return False

            self.verification_rslt = apple_response
        return True

    def data_format(self):
        """
        Formatting data required for restore_data function

        :return: Boolean
        """
        try:
            if self.platform_id == PlatformType.ios:
                self.receipt = self.verification_rslt.get('receipt', None)
                self.original_transaction_id = self.receipt.get('original_transaction_id', None)

                original_purchase_date = self.receipt.get('original_purchase_date', None)
                self.original_purchase_date = original_purchase_date.split(" ")[0]

                purchase_date = self.receipt.get('purchase_date', None)
                self.purchase_date = purchase_date.split(" ")[0]

                self.web_order_line_id = self.receipt.get('web_order_line_item_id', None)

                if self.web_order_line_id is None:
                    origin_tier_code = self.receipt.get('product_id', None)
                    splitted_tier_code = origin_tier_code.split(".")
                    self.tier_code = ".%s" % splitted_tier_code[3]
                else:
                    origin_tier_code = self.receipt.get('product_id', None)
                    splitted_tier_code = origin_tier_code.split(".")
                    self.tier_code = ".%s.%s" % (splitted_tier_code[3], splitted_tier_code[4])

                self.format_rslt = 'OK'
                return True

        except Exception, e:
            self.format_rslt = "Error data_format restore: %s" % e
            return False

    def restore_data(self):
        """
        Restoring the items, and calling OrderRestore class to save the items
        to library.

        :return: Boolean
        """
        orderrestore_params = dict()
        orderrestore_params['user_id']=self.user_id
        orderrestore_params['tier_code']=self.tier_code
        orderrestore_params['platform_id']=self.platform_id
        orderrestore_params['original_transaction_id']=self.original_transaction_id

        if self.web_order_line_id is not None:  # SUBSCRIPTION
            orderrestore_params['original_purchase_date'] = self.purchase_date
            or_status, or_rslt = OrderRestore(**orderrestore_params).construct()

        else:  # SINGLE
            orderrestore_params['original_purchase_date'] = self.original_purchase_date
            or_status, or_rslt = OrderRestore(**orderrestore_params).construct()

        if or_status != httplib.OK:
            json_form = {
                "args": self.args,
                "error_message": or_rslt,
                "error_code": constants.CLIENT_400244,
                "user_message": "Failed to restore items"}
            self.restore_rslt = json_form
            return False

        json_form = {
            'restored_items': or_rslt
        }
        self.restore_rslt = json_form
        return True

    def restore(self):
        """ Main function of restore

        :return: Boolean
        :return: restore_rslt
        """
        try:
            verified = self.restore_verification()
            if not verified:
                return False, self.verification_rslt

            formatted = self.data_format()
            if not formatted:
                return False, self.format_rslt

            data_restored = self.restore_data()
            if not data_restored:
                return False, self.restore_rslt

            return True, self.restore_rslt

        except Exception as e:
            self.restore_rslt = {
                "args": self.args,
                "error_message": str(e),
                "error_code": constants.CLIENT_400244,
                "user_message": "Failed to restore items"}
            return False, self.restore_rslt
