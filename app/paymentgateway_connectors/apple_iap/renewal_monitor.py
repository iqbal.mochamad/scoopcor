import requests
from sqlalchemy import desc

from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP, AppleReceiptIdentifiers
from app.paymentgateway_connectors.apple_iap.apple_iap_renewal import AppleIAPRenewal
from app.paymentgateway_connectors.choices.renewal_status import *

def reset_receipt(data_original=None):

    # reset all receipt to originals

    try:
        for i in data_original:
            payments = PaymentAppleIAP.query.filter_by(
                original_transaction_id=i).order_by(desc(PaymentAppleIAP.id)).all()

            if len(payments) > 1:
                first_payments = payments[:1]
                others_payments = payments[1:]

                for ipo in first_payments:
                    ipo.renewal_status = NEW
                    ipo.is_processed = False
                    ipo.save()

                for ivo in others_payments:
                    ivo.renewal_status = COMPLETE
                    ivo.is_processed = True
                    ivo.save()
            else:
                # reset
                payments[0].renewal_status = NEW
                payments[0].is_processed = False
                payments[0].save()

            receipts = AppleReceiptIdentifiers.query.filter_by(
                original_transaction_id=str(i)).order_by(desc(AppleReceiptIdentifiers.id)).all()

            if len(receipts) > 1:
                first_receipts = receipts[:1]
                others_receipts = receipts[1:]

                for ixo in first_receipts:
                    ixo.is_processed = False
                    ixo.save()

                for imo in others_receipts:
                    imo.is_processed = True
                    imo.save()

            else:
                receipts[0].is_processed = False
                receipts[0].save()

        return True, 'OK'

    except Exception as e:
        return False, 'Error %s' % e

def reset_patch(data_original=None):

    try:
        #url_data = 'https://scoopadm.apps-foundry.com/scoopcor/api/v1/payments/appleiaps/renewal/patch/%s'
        #for idata in data_original:
        #    api_data = url_data % idata
        #    response = requests.get(api_data)
        #    print response.json()

        monitor_renewal = []
        for idata in data_original:
            data_renewal = AppleIAPRenewal(original_transaction_id=idata).construct()
            monitor_renewal.append(data_renewal.data)
            # print data_renewal.data

        return True, monitor_renewal

    except Exception as e:
        return False, 'Error %s' % e

