'''
Verify Receipt Status
---------------
0 - Success
21000 - The App Store could not read the JSON object you provided.
21002 - The data in the receipt-data property was malformed.
21003 - The receipt could not be authenticated.
21004 - The shared secret you provided does not match the shared secret on file for your account.
21005 - The receipt server is not currently available.
21006 - This receipt is valid but the subscription has expired.
21007 - Sandbox receipt sent to production
21008 - production receipt sent to sandbox
'''

INVALID_RECEIPT_PRODUCTION = 21007
INVALID_RECEIPT_SANDBOX = 21008
SUBSCRIPTION_EXPIRED = 21006
SERVER_NOT_AVAILABLE = 21005
SHARED_SECRET_NOT_MATCH = 21004
AUTHENTICATION_FAILED = 21003
RECEIPT_MALFORMED = 21002
INVALID_JSON_OBJECT = 21000
ACCEPTED = 0

UNSUCCESSFUL_CODE_LIST = [
    INVALID_JSON_OBJECT,
    RECEIPT_MALFORMED,
    AUTHENTICATION_FAILED,
    SHARED_SECRET_NOT_MATCH,
    SERVER_NOT_AVAILABLE,
    SUBSCRIPTION_EXPIRED,
    INVALID_RECEIPT_SANDBOX,
    INVALID_RECEIPT_PRODUCTION]