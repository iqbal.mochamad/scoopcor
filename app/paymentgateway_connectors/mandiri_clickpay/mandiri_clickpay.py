import ast
from xml.etree import ElementTree as ET
import datetime
import requests
import xmltodict
import json

from app import app
from app.paymentgateway_connectors.mandiri_clickpay.models import MandiriClickpay, MandiriClickpayDetail
from app.helpers import conflict_message, bad_request_message, internal_server_message
from app.helpers import internal_server_message, payment_logs


class MandiriClickpayCheckout():
    def __init__(self, card_no=None, amount=None, transaction_id=None,
                 invoice_no=None, bank_id=None, token_response=None,
                 user_id=None, items=None, payment_id=None, challenge_code3=None,
                 purchase_date=None, args=None):

        self.card_no = card_no
        self.amount = amount
        self.transaction_id = transaction_id
        self.invoice_no = invoice_no
        self.bank_id = 1  # default_value
        self.items = items
        self.token_response = token_response
        self.user_id = user_id
        self.payment_id = payment_id
        self.challenge_code3 = challenge_code3
        self.purchase_date = purchase_date
        self.args = args

    def create_new_transaction(self):
        try:
            user_id = app.config['MANDIRI_CLICKPAY_USER']
            password = app.config['MANDIRI_CLICKPAY_PASSWORD']
            items = self.items
            items_count = str(len(items))
            root_el = ET.Element("payment_request")
            user_id_el = ET.SubElement(root_el, "user_id")
            user_id_el.text = str(user_id)
            password_el = ET.SubElement(root_el, "password")
            password_el.text = str(password)
            card_no_el = ET.SubElement(root_el, "card_no")
            card_no_el.text = str(self.card_no)
            amount_el = ET.SubElement(root_el, "amount")
            amount_el.text = str(int(self.amount))
            transaction_id_el = ET.SubElement(root_el, "transaction_id")
            transaction_id_el.text = str(self.transaction_id)
            data_field1_el = ET.SubElement(root_el, "data_field1")
            data_field1_el.text = str(self.card_no)[-10:]
            data_field2_el = ET.SubElement(root_el, "data_field2")
            data_field2_el.text = str(int(self.amount))
            data_field3_el = ET.SubElement(root_el, "data_field3")
            data_field3_el.text = str(self.challenge_code3)
            token_response_el = ET.SubElement(root_el, "token_response")
            token_response_el.text = str(self.token_response)
            date_time_el = ET.SubElement(root_el, "date_time")
            date_time_el.text = str(self.purchase_date)
            #date_time_el.text = str(datetime.datetime.now())
            bank_id_el = ET.SubElement(root_el, "bank_id")
            bank_id_el.text = str(self.bank_id)
            items_el = ET.SubElement(root_el, "items", count=str(items_count))

            for idx, val in enumerate(self.items):
                idxstr = "%s" % (str(idx+1))
                item_el = ET.SubElement(
                    items_el,
                    "item",
                    no=str(idxstr),
                    name=str(val['name']),
                    price=str(int(val['price'])),
                    qty=str(val['quantity'])
                )

            data = ET.tostring(root_el)
            headers = {'Content-Type': 'application/xml'}

            trx_response = requests.post(app.config['MANDIRI_SITE'], data=data, headers=headers)

            if trx_response.status_code == 200 and trx_response.text != '':
                response_raw = trx_response.text
                response_raw = response_raw.replace("\n", "")
                response_parsed = xmltodict.parse(response_raw)
                payment_response = response_parsed.get('payment_response', None)
                response_description = payment_response.get('response_desc', None)
                response_receipt_code = payment_response.get('receipt_code', None)
                response_code = str(payment_response.get('response_code', None))

                transaction_data = {
                    "payment_id": self.payment_id,
                    "invoice_id": self.invoice_no,
                    "transaction_id": self.transaction_id,
                    "card_no": self.card_no,
                    "amount": self.amount,
                    "token_response": self.token_response,
                    "response_description": response_description,
                    "status": response_code
                }

                trx = MandiriClickpay(**transaction_data).save()

                if trx.get('invalid', False):
                    return False, trx.get('message', '')

                transaction_detail = {
                    "paymentclickpay_id": trx.get('id', None),
                    "request_uncontrolled": "",
                    "response_type": "",
                    "response_uncontrolled": "",
                    "response_receipt_code": response_receipt_code,
                    "capture_request": str(data),
                    "capture_response": str(response_raw),
                    "payment_id": self.payment_id
                }

                trx_detail = MandiriClickpayDetail(**transaction_detail).save()

                if trx_detail.get('invalid', False):
                    return False, trx_detail.get('message', '')

                if response_code != '0000':
                    return False, "ERROR clickpay response code: %s" % str(response_code)

                return True, trx_response.text

            else:
                response_json = {
                    'message': 'Clickpay server not giving any respond',
                    'result': trx_response.text
                }

                return False, json.dumps(response_json)
        except Exception, e:
            return False, str(e)
