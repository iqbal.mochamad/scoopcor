from flask_appsfoundry.models import TimestampMixin

from app import db


class MandiriClickpay(db.Model, TimestampMixin):
    __tablename__ = 'core_paymentclickpays'

    id = db.Column(db.Integer, primary_key=True)
    payment_id = (db.Column(db.Integer, db.ForeignKey('core_payments.id')))
    invoice_id = db.Column(db.BigInteger)

    transaction_id = db.Column(db.String(30), nullable=False)
    card_no = db.Column(db.String(16))
    amount = db.Column(db.String(10))
    token_response = db.Column(db.String(6))
    response_description = db.Column(db.String(255))
    status = db.Column(db.String(10))

    def __repr__(self):
        return '<id : %s >' % self.id

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully added", 'id': self.id}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['payment_id'] = self.payment_id
        rv['invoice_id'] = self.invoice_id

        rv['transaction_id'] = self.transaction_id
        rv['card_no'] = self.card_no
        rv['amount'] = self.amount
        rv['token_response'] = self.token_response
        rv['response_description'] = self.response_description
        rv['status'] = self.status

        return rv


class MandiriClickpayDetail(db.Model, TimestampMixin):
    __tablename__ = 'core_paymentclickpaydetails'

    id = db.Column(db.Integer, primary_key=True)
    paymentclickpay_id = db.Column(db.Integer, db.ForeignKey('core_paymentclickpays.id'), nullable=False)
    payment_id = (db.Column(db.Integer, db.ForeignKey('core_payments.id')))

    request_uncontrolled = db.Column(db.Text)
    response_type = db.Column(db.String(1))
    response_uncontrolled = db.Column(db.Text)
    response_receipt_code = db.Column(db.String(255))
    capture_request = db.Column(db.Text)
    capture_response = db.Column(db.Text)

    def __repr__(self):
        return '<id : %s >' % self.id

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully added"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['paymentclickpay_id'] = self.paymentclickpay_id
        rv['payment_id'] = self.payment_id

        rv['request_uncontrolled'] = self.request_uncontrolled
        rv['response_type'] = self.response_type
        rv['response_uncontrolled'] = self.response_uncontrolled
        rv['response_receipt_code'] = self.response_receipt_code
        rv['capture_request'] = self.capture_request
        rv['capture_response'] = self.capture_response

        return rv
