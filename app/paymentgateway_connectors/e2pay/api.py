import httplib

import requests

from flask import Blueprint, jsonify, request

from app import app, db
from app.orders.helpers import update_order_payments
from app.orders.models import Order
from app.orders.order_complete import OrderCompleteConstruct
from app.paymentgateway_connectors.e2pay.choices import E2PAY_STATUS, E2PAY_STATUS_SUCCESS, E2PAY_STATUS_FAIL
from app.paymentgateway_connectors.e2pay.models import PaymentE2payData

blueprint = Blueprint('e2pay', __name__, url_prefix='/v1/payments')


def generate_e2pay_requiry_request(order=None):
    merchant_code = order.paymentgateway.merchant_code

    return "Amount={amount}&MerchantCode={merchant}&RefNo={refno}".format(
        amount="{}00".format(int(order.final_amount)),
        merchant=merchant_code if merchant_code else app.config['E2PAY_MERCHANT_CODE'],
        refno=order.id)


def update_e2pay_status(exist_order, status):
    exist_data = PaymentE2payData.query.filter_by(order_id=exist_order.id).first()
    if exist_data:
        exist_data.status = E2PAY_STATUS[status]
        db.session.add(exist_data)
        db.session.commit()


@blueprint.route('/e2pay-complete-transactions', methods=['POST', ])
def complete_transaction_e2pay():

    # Sample request data from e2pay
    # MerchantCode=IF00291&PaymentId=10&RefNo=1926137&Amount=10000&Currency=IDR&Remark=&TransId=T0031985400&
    # AuthCode=grame20061812102642235&Status=1&ErrDesc=&Signature=pL0GPr4mqaPpN9419CtBD8qE1z4=

    try:
        request_data = request.get_data()
        data_partner = request_data.split('&')

        data_json = {}
        for idata in data_partner:
            temp_data = idata.split('=')
            data_json[temp_data[0]] = temp_data[1]

        # do requery check validity of e2pay.
        order_id = data_json.get('RefNo', 0)

        exist_order = Order.query.filter_by(id=order_id).first()

        if exist_order:
            requery_request = generate_e2pay_requiry_request(order=exist_order)
            headers = {'Content-Type': 'application/x-www-form-urlencoded'}

            response_charge = requests.post(app.config['E2PAY_API_RE_QUERY'], data=requery_request, headers=headers)

            if response_charge.status_code in [httplib.OK, httplib.CREATED]:
                if response_charge.text == '00':
                    OrderCompleteConstruct(user_id=exist_order.user_id, payment_gateway_id=exist_order.paymentgateway_id,
                                           order_id=exist_order.id, order_number=exist_order.order_number,
                                           repurchased=False).construct()
                    update_order_payments(order_id=exist_order.id)
                    update_e2pay_status(exist_order=exist_order, status=E2PAY_STATUS_SUCCESS)
                else:
                    update_e2pay_status(exist_order=exist_order, status=E2PAY_STATUS_FAIL)

    except:
        pass

    # always return OK here, no html tag or any JSON formats.
    # E2pay Request.
    resp = jsonify('OK')
    resp.status_code = httplib.OK
    return resp
