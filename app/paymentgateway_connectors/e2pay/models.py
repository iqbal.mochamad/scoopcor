from app import db
from flask_appsfoundry.models import TimestampMixin
from sqlalchemy.dialects.postgresql import ENUM, JSONB

from app.paymentgateway_connectors.e2pay.choices import E2PAY_STATUS


class PaymentE2payData(db.Model, TimestampMixin):

    __tablename__ = 'core_paymente2pay'

    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('core_orders.id'), nullable=False)
    order = db.relationship('Order')
    paymentgateway_id = db.Column(db.Integer)
    status = db.Column(ENUM(*E2PAY_STATUS.values(), name='e2pay_status'), nullable=False)
    status_url = db.Column(db.String(250))
    data_request = db.Column(JSONB)
    data_response = db.Column(JSONB)
