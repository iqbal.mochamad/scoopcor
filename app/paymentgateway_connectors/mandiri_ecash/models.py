from flask_appsfoundry.models import TimestampMixin

from app import db


class MandiriEcash(db.Model, TimestampMixin):
    __tablename__ = 'core_paymentecashs'

    id = db.Column(db.Integer, primary_key=True)
    payment_id = (db.Column(db.Integer, db.ForeignKey('core_payments.id')))

    payment_code = db.Column(db.String(100))
    transaction_id = db.Column(db.String(50))
    status = db.Column(db.String(50))

    def __repr__(self):
        return '<ecash id : %s >' % self.id

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully added", 'id': self.id}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['payment_id'] = self.transaction_code
        rv['payment_code'] = self.payment_code
        rv['transaction_id'] = self.transaction_id
        rv['status'] = self.status

        return rv


class MandiriEcashDetail(db.Model, TimestampMixin):
    __tablename__ = 'core_paymentecashdetails'

    id = db.Column(db.Integer, primary_key=True)
    paymentecash_id = (db.Column(db.Integer, db.ForeignKey('core_paymentecashs.id'), nullable=False))
    payment_id = db.Column(db.Integer, nullable=False)

    authorize_request = db.Column(db.Text())
    authorize_response = db.Column(db.Text())
    capture_request = db.Column(db.Text())
    capture_response = db.Column(db.Text())
    ph_no = db.Column(db.String(30))

    def __repr__(self):
        return '<ecash detail id : %s >' % self.id

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully added"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Transaction successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['authorize_request'] = self.authorize_request
        rv['authorize_response'] = self.authorize_response
        rv['capture_request'] = self.capture_request
        rv['capture_response'] = self.capture_response
        rv['paymentecash_id'] = self.paymentecash_id
        rv['payment_id'] = self.payment_id
        rv['ph_no'] = self.ph_no

        return rv
