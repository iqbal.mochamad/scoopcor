from xml.etree import ElementTree as ET
import datetime
import requests
import hashlib
import base64

from app import app
from app.paymentgateway_connectors.mandiri_ecash.models import MandiriEcash, MandiriEcashDetail
from app.helpers import conflict_message, bad_request_message, internal_server_message
from app.helpers import internal_server_message, payment_logs


class MandiriEcashCheckout():
    def __init__(self, trx_amount=None, client_ip=None, transaction_id=None,
                 ecash_trx_id=None, payment_id=None, return_url=None, args=None):

        self.client_ip = client_ip
        self.trx_amount = trx_amount
        self.transaction_id = transaction_id
        self.ecash_trx_id = ecash_trx_id
        self.payment_id = payment_id
        self.return_url = return_url
        self.args = args

    def create_new_transaction(self):
        to_username = app.config['ECASH_USERNAME']

        password = app.config['ECASH_PASSWORD']
        auth_code = base64.encodestring('%s:%s' % (to_username, password)).replace('\n', '')

        password = "%s%s%s" % (to_username.upper(), self.trx_amount, self.client_ip)
        createSha1 = hashlib.sha1(password)
        hash_parameter = createSha1.hexdigest()

        root_el = ET.Element("soapenv:Envelope")
        root_el.set("xmlns:soapenv", "http://schemas.xmlsoap.org/soap/envelope/")
        root_el.set("xmlns:ws", "http://ws.service.gateway.ecomm.ptdam.com/")
        header_el = ET.SubElement(root_el, "soapenv:Header")
        body_el = ET.SubElement(root_el, "soapenv:Body")
        generate_el = ET.SubElement(body_el, "ws:generate")
        params_el = ET.SubElement(generate_el, "params")
        amount_el = ET.SubElement(params_el, "amount")
        amount_el.text = self.trx_amount
        clientAddress_el = ET.SubElement(params_el, "clientAddress")
        clientAddress_el.text = self.client_ip
        description_el = ET.SubElement(params_el, "description")
        description_el.text = "Pembelian buku"
        memberaddress_el = ET.SubElement(params_el, "memberAddress")
        memberaddress_el.text = app.config['SERVER_ADDRESS']
        return_url_el = ET.SubElement(params_el, "returnUrl")
        return_url_el.text = self.return_url
        tousername_el = ET.SubElement(params_el, "toUsername")
        tousername_el.text = to_username
        trx_id_el = ET.SubElement(params_el, "trxid")
        trx_id_el.text = self.transaction_id
        hash_el = ET.SubElement(params_el, "hash")
        hash_el.text = hash_parameter

        data = ET.tostring(root_el, encoding="UTF-8")
        headers = {
            'Content-Type': 'application/xml',
            'Authorization': 'Basic %s' % (auth_code)
            }
        url = "%s%s" % (app.config['ECASH_SITE'], app.config['ECASH_NEW_TRANSACTION_URL'])

        xml_response = requests.post(url, data=data, headers=headers)

        if xml_response.status_code == 200 and xml_response.text != '':
            root = ET.fromstring(xml_response.text)
            ecash_trx_id = root[0][0][0].text

            ecash_trx = {
                "payment_id": self.payment_id,
                "transaction_id": self.transaction_id,
                "payment_code": ecash_trx_id
            }
            pe = MandiriEcash(**ecash_trx).save()

            if pe.get('invalid', False):
                return False, internal_server_message(
                    modul="ecash",
                    method="POST",
                    error=pe.get('message', ''),
                    req=str(ecash_trx)
                )

            ecash_trx_detail = {
                "authorize_request": str(data),
                "authorize_response": str(xml_response),
                "paymentecash_id": pe.get('id', False),
                "payment_id": self.payment_id
            }

            ped = MandiriEcashDetail(**ecash_trx_detail).save()
            if ped.get('invalid', False):
                return False, internal_server_message(
                    modul="ecash",
                    method="POST",
                    error=ped.get('message', ''),
                    req=str(ecash_trx_detail)
                )

            return True, ecash_trx_id
        else:
            return False, xml_response.text

    def validate_transaction(self):
        url = "%s%s" % (app.config['ECASH_SITE'], app.config['ECASH_VALIDATION_URL'])
        headers = {
            "Content-Type": "application/x-www-form-urlencoded"
        }
        data = {
            "id": self.ecash_trx_id
        }

        response = requests.post(url, data=data, headers=headers)

        if response.status_code == 200:
            validation_result = str(response.text)
            validation_result_list = validation_result.split(',')

            me = MandiriEcash.query.filter_by(payment_id=self.payment_id).first()
            med = MandiriEcashDetail.query.filter_by(payment_id=self.payment_id).first()

            if me is not None and med is not None:
                me.status = str(validation_result_list[4]).replace('\n', '')
                me.payment_code = validation_result_list[0]
                me.save()

                med.capture_request = str(data)
                med.capture_response = validation_result
                med.ph_no = validation_result_list[2]
                med.save()

                trx_status = str(validation_result_list[4]).replace('\n', '')

                return True, response.text, trx_status

            else:
                return False, \
                       payment_logs(args=self.payment_id, error_message="payment_id not found"), \
                       None

        else:
            return False, \
                   payment_logs(args=response.status_code, error_message="Transaction unsuccessfully completed"), \
                   None
