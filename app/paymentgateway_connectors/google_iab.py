import httplib2
import datetime
import requests

from apiclient.discovery import build
from flask import current_app
from oauth2client.file import Storage

from app import app
import gflags

FLAGS = gflags.FLAGS

from . import _log


# duplicate class with app.paymentgateway_connectors.inapbilling.billing.GoogleIABCheckout???
class GoogleIABCheckout(object):

    def __init__(self, payload=None, product_sku=None, purchase_token=None):
        if not product_sku is None:
            sku = product_sku.split(".")
            del sku[-1]
            del sku[-1]
            del sku[-1]
            del sku[-1]
            dot = "."
            package_name = dot.join(sku)
            self.package_name = package_name

        self.payload = payload
        self.purchase_token = purchase_token
        self.product_sku = product_sku

    def generate_payload(self):
        '''
            To generate developer payload token from the backend.
            return: payload token ex: 46068fb881db37fd8937f14042014171549_413569

        '''
        token = '%s_%s' % (datetime.datetime.now().strftime('%d%m%Y%H%M%S'), str(datetime.datetime.now().microsecond))
        developer_payload = '%s%s' % ('46068fb881db37fd8937f', token)

        return developer_payload

    def verify_payment_IAB(self):
        '''
            To verify IAB payment whether it is a valid transaction that has been recorded by google
            or not.
            return: boolean, True or False
        '''
        response_google = self.confirm_google_payment()

        if response_google is not None:
            response_google_devpayload = response_google['developerPayload']
            isPurchased = response_google['purchaseState']

            if self.payload == response_google_devpayload and isPurchased == 0:
                return True
            else:
                return False
        else:
            return False

    def confirm_google_payment(self):
        '''
            To get information about the transaction from google using google's IAB API.
            return: json response,
            ex:
            {
                "kind":"androidpublisher#inapppurchase",
                "purchaseTime": {long},
                "purchaseState": {integer},
                "consumptionState":{integer},
                "developerPayload":{string}
            }
            Note:
            purchaseState:
                0 = Purchased
                1 = Cancelled
            consumptionState:
                0 = Consumed
                1 = Yet to be consumed
        '''
        storage = Storage(current_app.config['GOOGLE_OAUTH_CREDENTIALS_FILE'])
        credentials = storage.get()
        response = None
        access_token = credentials.access_token

        http = httplib2.Http()

        if not self.check_token_from_google(access_token):
            credentials.refresh(http)
            storage.put(credentials)

        http = credentials.authorize(http)
        service = build('androidpublisher', 'v1.1', http=http)
        inapppurcase = service.inapppurchases().get(packageName=self.package_name,
                                    productId=self.product_sku, token=self.purchase_token)
        response = inapppurcase.execute()

        return response

    def check_token_from_google(self, token):
        '''
            To check whether the token we use to call the google's API is still valid or expired.
        '''
        token_info = requests.get('{}?access_token={}'.format(
            app.config['GOOGLE_TOKEN_INFO_API'], token))

        return token_info.ok and token_info.json()['expires_in'] > 100

