from flask import Blueprint
from flask.ext import restful

from app.paymentgateway_connectors.creditcard.api import PaymentCreditCardRenewalDeleteApi
from .finnet195.api import FinnetPaymentGatewayAPI
from .tcash.api import TcashPaymentGatewayAPI, PaymentTcashApi
from .apple_iap.api import AppleIAPRestoreAPI, AppleIAPTranslationAPI
from .apple_iap.api import AppleIAPRenewalAPI, AppleIAPRenewalTempAPI, MonitoringRenewalAPI

from .faspay.api import FaspayCheckAuthkeySignatureAPI, FaspayPaymentNotificationAPI, FaspayTransactionStatusAPI
from .creditcard.api import PaymentCreditCardRenewalListApi, PaymentCreditCardRenewalCancelApi

finnet_blueprint = Blueprint('finnet195_payments', __name__, url_prefix='/v1/payments/finnet195')
finnet_api = restful.Api(finnet_blueprint)
finnet_api.add_resource(FinnetPaymentGatewayAPI, '/return')

tcash_blueprint = Blueprint('tcash_payments', __name__, url_prefix='/v1/payments/tcash')
tcash_api = restful.Api(tcash_blueprint)
tcash_api.add_resource(TcashPaymentGatewayAPI, '/return')
tcash_api.add_resource(PaymentTcashApi, '/<string:tcash_column>/<string:tcash_value>')

# new api for Apple receipt IOS 7+
blueprint_apple = Blueprint('payments_apple_iap', __name__, url_prefix='/v1/payments/apple-iap')

# previous api for Apple receipt IOS 6-
apple_blueprint = Blueprint('apple_payments', __name__, url_prefix='/v1/payments/appleiaps')
apple_api = restful.Api(apple_blueprint)
apple_api.add_resource(AppleIAPRestoreAPI, '/restore')
apple_api.add_resource(AppleIAPRenewalAPI, '/renewal/<int:divider>')
apple_api.add_resource(MonitoringRenewalAPI, '/monitor_renewal')

# this api for temporary backup only
apple_api.add_resource(AppleIAPRenewalTempAPI, '/old-renewal/<int:divider>')
apple_api.add_resource(AppleIAPTranslationAPI, '/translation')


faspay_blueprint = Blueprint('faspay', __name__, url_prefix='/v1/payments/faspay')
api = restful.Api(faspay_blueprint)
api.add_resource(FaspayPaymentNotificationAPI, '/payment_notification', endpoint='payment_notification')

faspay_blueprint.add_url_rule(rule='/check_authkeysignature', view_func=FaspayCheckAuthkeySignatureAPI.as_view('validate_authkey_signature'))

#api.add_resource(FaspayCheckAuthkeySignatureAPI, '/check_authkeysignature', endpoint='validate_authkey_signature')
api.add_resource(FaspayTransactionStatusAPI, '/check_trx_status', endpoint='check_transaction_status')

cc_blueprint = Blueprint('creditcard', __name__, url_prefix='/v1/payments/creditcard')
creditcard_api = restful.Api(cc_blueprint)
creditcard_api.add_resource(PaymentCreditCardRenewalListApi, '/renewal', endpoint='cc-renewal')
creditcard_api.add_resource(PaymentCreditCardRenewalCancelApi, '/renewal-cancel', endpoint='cc-renewal-cancel')
creditcard_api.add_resource(PaymentCreditCardRenewalDeleteApi, '/profile/<string:masked_credit_card>', endpoint='cc-profile-token')

