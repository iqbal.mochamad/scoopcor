from flask_appsfoundry.models import TimestampMixin

from app import db

"""
    LEGEND:
        STATUS:
            1. TEMP
            2. CHARGED
            3. CHARGED & VALIDATED
            4. POSSIBLE FRAUD
"""


class PaymentInappBilling(db.Model, TimestampMixin):

    __tablename__ = 'core_paymentinappbillings'

    id = db.Column(db.Integer, primary_key=True)
    payment_id = (db.Column(db.Integer, db.ForeignKey('core_payments.id')))
    payload_key = db.Column(db.String(250))
    purchase_token = db.Column(db.String(500))
    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    user = db.relationship('User')
    order_id = db.Column(db.Integer, db.ForeignKey('core_orders.id'))
    order = db.relationship('Order')
    status = db.Column(db.Integer)  # READ LEGEND : STATUS
    iab_order_id = db.Column(db.String(250))

    def __repr__(self):
        return '<order_id : %s payload_key: %s>' % (self.order_id, self.payload_key)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "PaymentInappBilling successfully added", 'id': self.id}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "PaymentInappBilling successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['payment_id'] = self.payment_id
        rv['payload_key'] = self.payload_key
        rv['google_key'] = self.google_key
        rv['user_id'] = self.user_id
        rv['order_id'] = self.order_id
        rv['iab_order_id'] = self.iab_order_id

        return rv

class PaymentInappBillingDetail(db.Model, TimestampMixin):

    __tablename__ = 'core_paymentinappbillingdetails'

    id = db.Column(db.Integer, primary_key=True)

    paymentinappbilling_id = (db.Column(db.Integer, db.ForeignKey('core_paymentinappbillings.id'), nullable=False))
    payment_id = db.Column(db.Integer, nullable=False)

    response_iab = db.Column(db.Text())

    def __repr__(self):
        return '<order_id : %s payload_key: %s>' % (self.id, self.paymentinappbilling_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Paymentinappbillingdetails successfully added"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "PaymentInappBillingDetails successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['paymentinappbilling_id'] = self.paymentinappbilling_id
        rv['payment_id'] = self.payment_id
        rv['response_iab'] = self.response_iab

        return rv
