import httplib, hashlib, json, requests, gflags

from flask import Response, current_app, g
from datetime import datetime

from app.paymentgateway_connectors.inapbilling.models import PaymentInappBilling, PaymentInappBillingDetail
from app.orders.models import Order
from app.helpers import payment_logs
from app.paymentgateway_connectors.inapbilling.inappbilling_status import GOOGLE_PURCHASED, PAYMENT_INAPP_NEW, \
    PAYMENT_INAPP_PROCESSING, PAYMENT_INAPP_ACCEPT, PAYMENT_INAPP_FRAUD

from app import app, db
FLAGS = gflags.FLAGS


"""
    V3 Google DOCS REFERENCE : 
        - https://developer.android.com/google/play/billing/billing_reference
        - https://developers.google.com/android-publisher/api-ref/purchases/products

"""


class GoogleIABCheckout(object):

    def __init__(self, order_id=None, developer_payload=None,
            purchase_token=None, args=None, method=None, payment_id=None,
            product_sku=None, iab_order_id=None):

        self.order_id = order_id
        self.developer_payload = developer_payload
        self.purchase_token = purchase_token
        self.payment_id = payment_id
        self.product_sku = product_sku
        self.iab_order_id = iab_order_id

        self.args = args
        self.user_id = args.get('user_id', None)
        self.method = method
        self.hash_key = app.config['PAYMENT_SALT']

        self.master_order = None

    def new_transactions(self):
        try:
            developer_payload = self.generate_payload_key()
            new_paymentiab = PaymentInappBilling(
                payload_key=developer_payload,
                user_id=self.user_id,
                order_id=self.order_id,
                status=PAYMENT_INAPP_NEW,
                payment_id=self.payment_id,
                purchase_token=self.purchase_token
            )
            db.session.add(new_paymentiab)
            db.session.commit()

            payment_iab_detail = PaymentInappBillingDetail(
                paymentinappbilling_id=new_paymentiab.id,
                payment_id=new_paymentiab.payment_id)
            db.session.add(payment_iab_detail)
            db.session.commit()

            return True, Response(
                json.dumps({
                    'developer_payload': developer_payload,
                    'product_code': self.master_order.tier_code,
                    'payment_id': new_paymentiab.payment_id
                }),
                status=httplib.CREATED, mimetype='application/json')

        except Exception, e:
            return False, payment_logs(args=self.args, error_message='Google New Transaction Error {}'.format(e))

    def new_charges(self):
        try:
            exist_paymentiab = PaymentInappBilling.query.filter_by(order_id=self.order_id,
                payload_key=self.developer_payload, status=PAYMENT_INAPP_NEW).first()

            if not exist_paymentiab:
                return False, "Invalid payment, Order_id or payment has been paid"

            exist_paymentiab.purchase_token = self.purchase_token
            exist_paymentiab.status = PAYMENT_INAPP_PROCESSING
            exist_paymentiab.iab_order_id = self.iab_order_id
            db.session.add(exist_paymentiab)
            db.session.commit()

            # Validate purchase token to google
            purchase_status, purchase_data = self.confirm_google_payment()

            if not purchase_status:
                exist_paymentiab.status = PAYMENT_INAPP_FRAUD
            else:
                exist_paymentiab.status = PAYMENT_INAPP_ACCEPT

            db.session.add(exist_paymentiab)
            db.session.commit()
            return purchase_status, purchase_data

        except Exception as e:
            return False, "Google Charges Failed {}".format(e)


    def confirm_google_payment(self):
        try:
            if hasattr(g, 'current_client') and g.current_client.product_id:
                package_name = g.current_client.product_id
            else:
                package_name = '.'.join(self.product_sku.split(".")[:-4])

            access_token, scope = self.retrieve_access_token()
            if access_token:
                purchase_api = app.config['GOOGLE_API_PURCHASE'].format(package_name, self.product_sku,
                    self.purchase_token, access_token)
                request_purchase = requests.get(purchase_api)

                if request_purchase.status_code == httplib.OK:
                    purchase_response =request_purchase.json()
                    google_order_id = purchase_response.get('orderId', 'unknown-order')
                    google_purchase_status = purchase_response.get('purchaseState', '-1')

                    if google_order_id == self.iab_order_id and google_purchase_status == GOOGLE_PURCHASED:
                        return True, 'Transaction Success'
                    else:
                        return False, 'Possible fraud transaction'
                else:
                    return False, 'Cant retrieve purchase google'
            else:
                return False, 'Cant retrieve access token'
        except Exception as e:
            return False, 'Failed for processing {}'.format(e)

    def generate_payload_key(self):
        data_key = '{}{}{}'.format(self.hash_key, datetime.now().strftime('%d%m%Y%H%M%S'), str(datetime.now().microsecond))
        payload_key = hashlib.sha256('{}{}'.format(self.order_id, data_key)).hexdigest()
        return payload_key

    def retrieve_access_token(self):
        # always get new token for this.
        access_token, scope = None, None
        request_data = {
            "grant_type":"refresh_token",
            "refresh_token": app.config['GOOGLE_TOKEN'],
            "client_id": app.config['GOOGLE_CLIENT_ID'],
            "client_secret": app.config['GOOGLE_CLIENT_SECRET']
        }
        retrieve_access = requests.post(app.config['GOOGLE_TOKEN_AUTH'], request_data)
        if retrieve_access.status_code == httplib.OK:
            access_token = retrieve_access.json().get('access_token', None)
            scope = retrieve_access.json().get('scope', None)
        return access_token, scope

    def get_orders(self):
        order = Order.query.filter_by(id=self.order_id).first()
        if not order: return payment_logs(args=self.args, error_message="Invalid OrderId")
        self.master_order = order

    def google_transactions(self):
        self.get_orders()
        return self.new_transactions()

    def google_validate(self):
        return self.new_charges()
