GOOGLE_PURCHASED = 0
GOOGLE_CANCELLED = 1

PURCHASE_TYPES = {
    GOOGLE_PURCHASED: 'purchased',
    GOOGLE_CANCELLED: 'cancelled',
}


PURCHASE_TEST = 0 #Test (i.e. purchased from a license testing account)
PURCHASE_PROMO = 1 #(i.e. purchased using a promo code)
PURCHASE_REWARDED = 2 #(i.e. from watching a video ad instead of paying)

PURCHASE_TYPES = {
    PURCHASE_TEST : 'Test',
    PURCHASE_PROMO : 'Promo',
    PURCHASE_REWARDED : 'Reward'
}


PAYMENT_INAPP_NEW = 1
PAYMENT_INAPP_PROCESSING = 2
PAYMENT_INAPP_ACCEPT = 3
PAYMENT_INAPP_FRAUD = 4
