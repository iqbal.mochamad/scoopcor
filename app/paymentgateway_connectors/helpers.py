from app import app
from app.auth.models import Client

from app.orders.models import Order
from app.services.scoopcas import ScoopCas


class TransactionValidation(object):
    def __init__(self, order_id=None, platform_id=None, client_id=None,
                 purchase_item_tier_code=None, purchase_item_price=None):
        """ This function used to validate all transactions.

        This function will validate whether data from the payment gateway server returned are matched with our db.

        :param `int` platform_id: REQUIRED
        :param order_id: REQUIRED
        :param purchase_item_tier_code:
        :param purchase_item_price:

        """
        self.purchase_item_price = purchase_item_price
        self.purchase_item_tier_code = purchase_item_tier_code
        self.item_price = None
        self.item_tier_code = None
        self.order_id = order_id
        self.orderline = None
        self.platform_id = platform_id
        self.client_id = client_id
        self.offer = None

    def master_order(self):
        order = Order.query.filter_by(id=self.order_id).first()

        if not order:
            raise Exception("Invalid order id")

        return order

    def compare_data(self):

        if None in [self.item_tier_code, self.purchase_item_tier_code]:
            raise Exception('Invalid tier code')
        elif self.item_tier_code != self.purchase_item_tier_code:
            raise Exception('Item tier codes are not matched')

        return True

    def extract_db_data(self, order):

        item_data = dict()
        item_data['item_tier_code'] = order.tier_code
        item_data['item_price'] = order.final_amount
        self.item_price = item_data['item_price']

        # update this old methods, our db cas and core already merge, no need anymore.

        #client = ScoopCas(base_url=app.config['SERVER_SCOOPCAS'], auth_token=app.config['AUTH_HEADER']
        #                  ).get_client(client_id=self.client_id)
        #client_id = client.get('client_id', None)
        #product_id = client.get('product_id', None)

        client = Client.query.get(self.client_id)

        if client:
            client_id = client.id
            product_id = client.product_id
        else:
            raise Exception('Invalid Client Objects Not Founds.')

        if client_id is None:
            raise Exception('Invalid Client id')

        if product_id is None:
            raise Exception('Invalid Product id')

        #self.item_tier_code = "{}{}".format(client.get('product_id'), item_data['item_tier_code'])
        self.item_tier_code = "{}{}".format(product_id, item_data['item_tier_code'])

        return True

    def validate(self):
        try:
            order = self.master_order()
            self.extract_db_data(order)
            self.compare_data()

            return True, 'Matched'

        except Exception as e:
            return False, str(e)

def authorize_formatting():
    return {
        'virtual_account_number': '',
        'bank': '',
        'gopay_qr_code_url': '',
        'gopay_deeplink_url': '',
        'payment_type': '',
    }
