from datetime import datetime

import requests, json, base64, httplib
from flask import Response
from sqlalchemy import desc
from unidecode import unidecode

from app import app, db
from app.helpers import err_response
from app.orders.models import Order, OrderLine
from app.paymentgateway_connectors.atm_transfer.midtrans_status import *
from app.paymentgateway_connectors.gopay.models import PaymentGopayAccount
from app.paymentgateway_connectors.helpers import authorize_formatting
from app.payments.choices import WAITING_FOR_PAYMENT, PAYMENT_GATEWAYS
from app.payments.choices.gateways import GOPAY
from app.payments.models import Payment
from app.users.users import User

MIDTRANS_HEADER = {
    'Content-Type':'application/json',
    'Accept':'application/json',
    'Authorization':base64.b64encode(app.config['MIDTRANS_SERVER_KEY'])
}


class GopayAuthorize():

    def __init__(self, order_id=None):
        self.order_id = order_id
        self.data_gopay = None

    def request_gopay(self):
        data_orderline = OrderLine.query.filter_by(order_id=self.order.id).order_by(desc(OrderLine.id)).all()

        return {
            "payment_type": "gopay",
            "transaction_details": {
                "order_id": str(self.order.id),
                "gross_amount": int(self.order.final_amount)
            },
            "item_details":
                [{
                    "id": str(iorder.id),
                    "price": int(iorder.final_price),
                    "quantity": iorder.quantity,
                    "name": "{}".format(unidecode(unicode(iorder.name))[:30])
                } for iorder in data_orderline],
            "customer_details": {
                "first_name": self.user.first_name if self.user.first_name else "",
                "last_name": self.user.last_name if self.user.last_name else "",
                "email": self.user.email if self.user.email else "",
                "phone": self.user.phone_number if self.user.phone_number else "1234567"
            },
            "gopay": {
                "enable_callback": True
            }
        }

    def create_payments(self):
        exist_payment = Payment.query.filter_by(order_id=self.order.id).first()
        if not exist_payment:
            new_payment = Payment(
                order_id=self.order.id,
                user_id=self.order.user_id,
                paymentgateway_id=self.order.paymentgateway_id,
                currency_code=self.order.currency_code,
                amount=self.order.final_amount,
                payment_status=WAITING_FOR_PAYMENT,
                is_active=True,
                is_test_payment=True if app.config['TESTING'] else False,
                is_trial=False,
                payment_datetime=datetime.now(),
                financial_archive_date=None,
                merchant_params=None
            )
            db.session.add(new_payment)
            db.session.commit()

    def create_gopay_data(self, data_request, data_response):
        exist_gopay_data = PaymentGopayAccount.query.filter_by(order_id=self.order.id).first()

        if not exist_gopay_data:
            new_gopay_data = PaymentGopayAccount(
                order_id=self.order.id,
                status=STATUS_PENDING,
                data_request=data_request,
                data_response=data_response
            )
            db.session.add(new_gopay_data)
            db.session.commit()
            self.data_gopay = new_gopay_data

    def charge_gopay(self):
        try:
            data_request = self.request_gopay()
            go_charge = requests.post(app.config['MIDTRANS_API_CHARGE'], json.dumps(data_request), headers=MIDTRANS_HEADER)
            go_response = go_charge.json()

            # create log
            self.create_payments()
            self.create_gopay_data(data_request, go_response)

            if go_charge.status_code in [httplib.OK, httplib.CREATED]:
                fraud_status = go_response.get('fraud_status', '')
                transaction_status = go_response.get('transaction_status', '')
                gopay_actions = go_response.get('actions', [])

                if fraud_status == 'accept' and transaction_status == STATUS_PENDING:
                    data_response = authorize_formatting()
                    for gopay_data in gopay_actions:
                        action_name = gopay_data.get('name', '')
                        data_response['payment_type'] = PAYMENT_GATEWAYS[GOPAY]

                        if action_name == 'generate-qr-code':
                            data_response['gopay_qr_code_url'] = gopay_data.get('url', '')
                        if action_name == 'deeplink-redirect':
                            data_response['gopay_deeplink_url'] = gopay_data.get('url', '')
                        if action_name == 'get-status':
                            self.data_gopay.status_url = gopay_data.get('url', '')
                            db.session.add(self.data_gopay)
                            db.session.commit()
                    return True, data_response
                else:
                    return False, go_response
            else:
                return False, 'GOPAY Cant reach midtrans API {}'.format(go_response)

        except Exception as e:
            return False, 'Internal Error {}'.format(e)

    def authorize(self):
        self.order = Order.query.filter_by(id=self.order_id).first()
        self.user = User.query.filter_by(id=self.order.user_id).first()

        # charge midtrans
        status, response = self.charge_gopay()

        if status:
            return Response(json.dumps(response), status=httplib.OK, mimetype='application/json')
        else:
            return err_response(status=httplib.BAD_REQUEST, error_code=httplib.BAD_REQUEST,
                                developer_message=response, user_message="Payment Invalid.")
