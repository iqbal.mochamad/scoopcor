from sqlalchemy.dialects.postgresql import ENUM, JSONB
from flask_appsfoundry.models import TimestampMixin

from app import db
from app.paymentgateway_connectors.atm_transfer.midtrans_status import MIDTRANS_STATUS


class PaymentGopayAccount(db.Model, TimestampMixin):

    __tablename__ = 'core_paymentgopays'

    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('core_orders.id'), nullable=False)
    order = db.relationship('Order')
    status = db.Column(ENUM(*MIDTRANS_STATUS.values(), name='status'), nullable=False)
    status_url = db.Column(db.Text)
    data_request = db.Column(JSONB)
    data_response = db.Column(JSONB)
