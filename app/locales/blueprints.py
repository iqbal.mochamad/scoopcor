from flask import Blueprint
from flask_appsfoundry import Api

from .api import LanguageCodesToNameApi, CountryCodesToNameApi

blueprint = Blueprint('locales_blueprint', __name__, url_prefix='/v2')

api = Api(blueprint)
api.add_resource(LanguageCodesToNameApi, '/languages/codes-to-names')
api.add_resource(CountryCodesToNameApi, '/countries/codes-to-names')
