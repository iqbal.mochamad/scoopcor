from httplib import BAD_REQUEST

import pycountry
import gettext

from flask_appsfoundry import Resource
from flask_appsfoundry.controllers import ErrorHandlingApiMixin

from flask import request
from werkzeug.exceptions import BadRequest

from app.locales.choices import COUNTRIES_ENABLED, LANGUAGES_ENABLED


class LanguageCodesToNameApi(ErrorHandlingApiMixin, Resource):

    def get(self):

        resp_lang = request.args.get('lang')
        # english is the default, so we need to skip translation of english
        # but we still want to allow the front end to request it for
        # consistency
        if resp_lang and resp_lang != 'eng':

            try:
                # we need alpha2 for translation.. however, our app is requiring
                # alpha3, so to keep things consistent on the front end, we're
                # requesting alpha3
                resp_lang = pycountry.languages.get(terminology=resp_lang).alpha2

                lang = gettext.translation('iso639',
                                           pycountry.LOCALES_DIR,
                                           languages=[resp_lang])
                lang.install()

            except KeyError:
                raise BadRequest("Invalid 'lang' {}".format(resp_lang))

            response = {language.terminology: lang.ugettext(language.name) for language in pycountry.languages}
        else:
            response = {language.terminology: language.name
                        for language in pycountry.languages}

        if request.args.get('active_only'):
            for lc in [code for code in response if code not in LANGUAGES_ENABLED]:
                del response[lc]

        return response

    def _handle_400(self, exception):

        response = {
            "status": BAD_REQUEST,
            "error_code": 400,
            "user_message": exception.description,
            "developer_message": "?lang parameter must be a valid ISO639-2/T code"
        }

        return response, BAD_REQUEST


class CountryCodesToNameApi(ErrorHandlingApiMixin, Resource):

    def get(self):

        resp_lang = request.args.get('lang')
        if resp_lang and resp_lang != 'eng':

            try:
                resp_lang = pycountry.languages.get(terminology=resp_lang).alpha2

                lang = gettext.translation('iso3166',
                                           pycountry.LOCALES_DIR,
                                           languages=[resp_lang])
                lang.install()
            except KeyError:
                raise BadRequest("Invalid 'lang' {}".format(resp_lang))

            response = {country.alpha2.lower(): lang.ugettext(country.name)
                        for country in pycountry.countries}
        else:
            response = {country.alpha2.lower(): country.name
                        for country in pycountry.countries}

        if request.args.get('active_only'):
            for cc in [code for code in response
                       if code not in COUNTRIES_ENABLED]:
                del response[cc]

        return response

    def _handle_400(self, exception):

        response = {
            "status": BAD_REQUEST,
            "error_code": 400,
            "user_message": exception.description,
            "developer_message": "?lang parameter must be a valid ISO639-2/T code"
        }

        return response, BAD_REQUEST
