from sqlalchemy import desc
from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP, AppleReceiptIdentifiers
from app.paymentgateway_connectors.choices.renewal_status import *


def itunes_reset(data_original=None):

    '''reset all receipt itunes to latest originals'''

    try:
        for idata in data_original:
            payments = PaymentAppleIAP.query.filter_by(
                original_transaction_id=idata).order_by(desc(PaymentAppleIAP.id)).all()

            if len(payments) > 1:
                first_payments = payments[:1]
                others_payments = payments[1:]

                for ifirst in first_payments:
                    ifirst.renewal_status = NEW
                    ifirst.is_processed = False
                    ifirst.save()

                for iother in others_payments:
                    iother.renewal_status = COMPLETE
                    iother.is_processed = True
                    iother.save()
            else:
                payments[0].renewal_status = NEW
                payments[0].is_processed = False
                payments[0].save()

            receipts = AppleReceiptIdentifiers.query.filter_by(original_transaction_id=str(idata)).order_by(
                desc(AppleReceiptIdentifiers.id)).all()

            if receipts:
                if len(receipts) > 1:
                    first_receipts = receipts[:1]
                    others_receipts = receipts[1:]

                    for ifirst in first_receipts:
                        ifirst.is_processed = False
                        ifirst.save()

                    for iother in others_receipts:
                        iother.is_processed = True
                        iother.save()

                else:
                    receipts[0].is_processed = False
                    receipts[0].save()
            print 'Reset Success {} {}'.format(payments[0].id, idata)

        return 'Reset Success'

    except Exception as e:
        return False, 'itunes_reset Error {}'.format(e)
