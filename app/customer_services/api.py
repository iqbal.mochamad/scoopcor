import httplib, json, base64, requests

from datetime import datetime, timedelta
from flask import Response, request
from sqlalchemy import desc

from app import db, app
from app.auth.decorators import token_required
from flask_restful import Resource

from app.constants import RoleType
from app.customer_services.sql_data import sql_data
from app.helpers import err_response
from app.orders.choices import COMPLETE
from app.orders.helpers import update_order_payments
from app.orders.models import Order, OrderLine
from app.orders.order_complete import OrderCompleteConstruct
from app.paymentgateway_connectors.atm_transfer.midtrans_status import FRAUD_ACCEPT, STATUS_CAPTURE, STATUS_EXPIRED
from app.paymentgateway_connectors.atm_transfer.models import PaymentVirtualAccount
from app.paymentgateway_connectors.inapbilling.models import PaymentInappBilling
from app.payments.choices.gateways import IAB, GOPAY
from app.payments.signature_generator import GenerateSignature
from app.users.users import User, Role
from app.utils.marshmallow_helpers import schema_load
from .schema import *

def user_response_data(exist_user):
    response_json = {}
    if exist_user:
        response_json = {
            "id": exist_user.id,
            "email": exist_user.email,
            "first_name": exist_user.first_name if exist_user.first_name else "",
            "last_name": exist_user.last_name if exist_user.last_name else "",
            "role": [{'id': i.id, 'name': i.name} for i in exist_user.roles],
            "organization": [{'id': i.id, 'name': i.name} for i in exist_user.organizations],
            "excluded_from_reporting": exist_user.excluded_from_reporting,
            "phone_number": exist_user.phone_number if exist_user.phone_number else None,
            "is_verified": exist_user.is_verified}
    return response_json

class RemoveAccessApi(Resource):

    @token_required('can_read_write_global_all')
    def get(self):
        args = request.args.to_dict()
        exist_user = User.query.filter_by(email=args.get('email', 'anynomous@g.com')).first()
        if not exist_user:
            return Response(status=httplib.NOT_FOUND)

        response_json = user_response_data(exist_user)
        return Response(json.dumps({'data': response_json}), status=httplib.OK, mimetype='application/json')

    @token_required('can_read_write_global_all')
    def post(self):
        data = schema_load(schema=RemoveAccessSchema())
        exist_user = User.query.filter_by(email=data.get('email', 'anynomous@g.com')).first()
        if not exist_user:
            return Response(status=httplib.NOT_FOUND)

        new_role = Role.query.filter_by(id=RoleType.verified_user.value).first()

        #update user data
        exist_user.excluded_from_reporting = False

        for old_role in exist_user.roles:
            exist_user.roles.remove(old_role)
        exist_user.roles.append(new_role)

        for old_org in exist_user.organizations:
            exist_user.organizations.remove(old_org)

        db.session.add(exist_user)
        db.session.commit()

        response_json = user_response_data(exist_user)
        return Response(json.dumps({'data': response_json}), status=httplib.OK, mimetype='application/json')

class GoogleInappCsData(Resource):

    @token_required('can_read_write_global_all, can_read_global_cs, can_read_write_patch_cs')
    def post(self):
        data = schema_load(schema=GoogleInappCsDataSchema())
        email = data.get('email', 'anynomous@g.com')
        iab_order_id = data.get('iab_order_id', 'xx')

        is_fraud, fraud_message, google_data, order_response, raw_sql = False, None, {}, [], None
        exist_user = User.query.filter_by(email=email).first()

        exist_data_google = PaymentInappBilling.query.filter(PaymentInappBilling.iab_order_id == iab_order_id).first()
        if exist_data_google:
            fraud_message, is_fraud = check_complete_order(exist_data_google.order, exist_user, fraud_message, is_fraud)
            fraud_message, is_fraud, real_user = check_real_user(exist_data_google.order, exist_user, fraud_message, is_fraud)

            google_data = {
                'is_paid': exist_data_google.order.order_status == COMPLETE,
                'iab_order_id': exist_data_google.iab_order_id,
                'order_id': exist_data_google.order.id,
                'order_number': exist_data_google.order.order_number,
                'order_date': datetime.strftime(exist_data_google.order.created + timedelta(hours=7), '%Y-%m-%d %H:%M:%S'),
                'email': exist_data_google.user.email,
                'full_name': '{} {}'.format(exist_data_google.user.first_name, exist_data_google.user.last_name)}
            raw_sql = sql_data(exist_data_google.user.email, IAB, datetime.now().year)

        if exist_user:
            raw_sql = sql_data(exist_user.email, IAB, datetime.now().year)

        if raw_sql:
            data_raw = db.engine.execute(raw_sql)
            order_response = [{"created": idata[0].strftime("%Y-%m-%d %H:%M:%S"), "name": idata[1], "offer_id": idata[2],
                "order_id": idata[3], "order_status": idata[4], "order_number": idata[5], "final_amount": "%.2f" % idata[6],
                "payment_gateway_id": idata[7], "payment_status": idata[8], "currency_code": idata[9],
                "is_paid": idata[4] == COMPLETE
            } for idata in data_raw.fetchall()]

        if not raw_sql:
            fraud_message = "Invalid input data Email or Google receipt are not Registered in our system!!"

        if raw_sql and not order_response:
            fraud_message = "No transaction history purchase with google for this account, make sure this reporting are google transaction!"

        response_json = {
            "user_data": user_response_data(exist_user),
            "order_data": order_response,
            "is_fraud": True if fraud_message else False,
            "fraud_message": fraud_message,
            "google_data": google_data}
        return Response(json.dumps(response_json), status=httplib.OK, mimetype='application/json')

class CsPatchData(Resource):

    @token_required('can_read_write_global_all, can_read_global_cs, can_read_write_patch_cs')
    def post(self):
        data = schema_load(schema=CsPatchDataSchema())
        order_id = data.get('order_id', 1)
        signature = data.get('signature', 'unknown')
        paymentgateway_id = data.get('paymentgateway_id', 100)
        iab_order_id = data.get('iab_order_id', 'unknown')

        exist_signature = GenerateSignature(order_id=order_id).construct()

        if exist_signature != signature:
            return err_response(status=httplib.BAD_REQUEST, error_code=httplib.BAD_REQUEST,
                developer_message='Failed, Invalid Signature', user_message='Failed, Invalid Signature')

        exist_order = Order.query.filter_by(id=order_id).first()
        if not exist_order:
            return err_response(status=httplib.BAD_REQUEST, error_code=httplib.BAD_REQUEST,
                developer_message='Failed, Invalid Order ID', user_message='Failed, Invalid Order ID')

        if exist_order.order_status == COMPLETE:
            return err_response(status=httplib.BAD_REQUEST, error_code=httplib.BAD_REQUEST,
                                developer_message='Failed, Order Already Completed', user_message='Failed, Order Already Completed')

        if int(paymentgateway_id) == IAB:
            exist_data_google_iab_order_id = PaymentInappBilling.query.filter(PaymentInappBilling.iab_order_id == iab_order_id).first()
            if exist_data_google_iab_order_id:
                exist_data_google_iab_order_id.order_id = order_id
                db.session.add(exist_data_google_iab_order_id)
                db.session.commit()
            else:
                exist_data_google = PaymentInappBilling.query.filter(PaymentInappBilling.order_id == order_id).first()
                if exist_data_google:
                    exist_data_google.iab_order_id = iab_order_id
                    db.session.add(exist_data_google)
                    db.session.commit()

        update_order_payments(exist_order.id)
        return OrderCompleteConstruct(user_id=exist_order.user_id, payment_gateway_id=exist_order.paymentgateway_id,
            order_id=exist_order.id, order_number=exist_order.order_number, repurchased=True).construct()

def get_midtrans_status(order):
    MIDTRANS_HEADER = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': base64.b64encode(app.config['MIDTRANS_SERVER_KEY'])
    }

    midtrans_order_api = '{}/v2/{}/status'.format(app.config['MIDTRANS_API'], order.id)
    midtrans_order = requests.get(midtrans_order_api, headers=MIDTRANS_HEADER)

    if midtrans_order.status_code in [httplib.OK, httplib.CREATED]:
        return midtrans_order.json()
    return None

def check_complete_order(exist_order, exist_user, fraud_message, is_fraud):
    if exist_order.order_status == COMPLETE:
        is_fraud = True
        if exist_user:
            fraud_message = "This Order already PAID with account : {} at {} with order number {} ".format(
                exist_user.email,
                datetime.strftime(exist_order.created + timedelta(hours=7), '%Y-%m-%d %H:%M:%S'),
                exist_order.order_number)
        else:
            user_found = User.query.filter_by(id=exist_order.user_id).first()
            fraud_message = "This Order already PAID with account : {} at {} with order number {} ".format(
                user_found.email, datetime.strftime(exist_order.created + timedelta(hours=7), '%Y-%m-%d %H:%M:%S'),
                exist_order.order_number)
    return fraud_message, is_fraud

def check_real_user(exist_order, exist_user, fraud_message, is_fraud):
    if exist_user:
        if exist_order.user_id != exist_user.id:
            real_user = User.query.filter_by(id=exist_order.user_id).first()
            is_fraud = True
            fraud_message = "Invalid user account reporting!! This order purchased with account : {}".format(
                real_user.email)
        else:
            real_user = exist_user
    else:
        real_user = User.query.filter_by(id=exist_order.user_id).first()
        fraud_message = "This order purchased with account : {}".format(real_user.email)
    return fraud_message, is_fraud, real_user

class GopayCsData(Resource):

    @token_required('can_read_write_global_all, can_read_global_cs, can_read_write_patch_cs')
    def post(self):
        data = schema_load(schema=GopayDataSchema())
        email = data.get('email', 'anynomous@g.com')
        order_id = data.get('order_id', 1)
        order_data, midtrans_response, is_fraud, fraud_message = None, None, False, None

        exist_user = User.query.filter_by(email=email).first()
        exist_order = Order.query.filter_by(id=order_id, paymentgateway_id=GOPAY).first()

        if exist_order:
            fraud_message, is_fraud = check_complete_order(exist_order, exist_user, fraud_message, is_fraud)
            fraud_message, is_fraud, real_user = check_real_user(exist_order, exist_user, fraud_message, is_fraud)
            orderlines_data = OrderLine.query.filter_by(order_id=exist_order.id).all()

            order_data = [{
                "user_id": iorderline.order.user_id,
                "is_paid": iorderline.order.order_status == COMPLETE,
                "email": real_user.email,
                "full_name": '{} {}'.format(real_user.first_name, real_user.last_name),
                "order_id": iorderline.order.id,
                "order_number": iorderline.order.order_number,
                "final_amount": "%.2f" % iorderline.order.final_amount,
                "name": iorderline.name,
                "created": iorderline.order.created.strftime("%Y-%m-%d %H:%M:%S"),
                "order_status": iorderline.order.order_status,
                "payment_gateway_id": iorderline.order.paymentgateway_id
            } for iorderline in orderlines_data]

            midtrans_response = get_midtrans_status(exist_order)
            if midtrans_response:
                fraud_status = midtrans_response.get('fraud_status', '')
                transaction_status = midtrans_response.get('transaction_status', '')

                if midtrans_response.get('payment_type') != 'gopay':
                    fraud_message = "This order not register as GOPAY in midtrans, please ask developer for further investigating.!"

                if fraud_status != FRAUD_ACCEPT and transaction_status != STATUS_CAPTURE:
                    fraud_message = "This order are mark FAILURE in midtrans.!"

                if transaction_status == STATUS_EXPIRED:
                    fraud_message = "This Order already EXPIRED, REFUND to MIDTRANS if user already cutted."
        else:
            fraud_message = "Invalid Order ID"

        response_json = {
            "user_data": user_response_data(exist_user),
            "order_data": order_data,
            "is_fraud": True if fraud_message else False,
            "midtrans_status": midtrans_response,
            "fraud_message": fraud_message
        }
        return Response(json.dumps(response_json), status=httplib.OK, mimetype='application/json')

class VirtualAccountCsData(Resource):

    @token_required('can_read_write_global_all, can_read_global_cs, can_read_write_patch_cs')
    def post(self):
        data = schema_load(schema=VirtualAccountDataSchema())
        email = data.get('email', 'anynomous@g.com')
        virtual_number = data.get('virtual_number', 1)
        order_data, midtrans_response, is_fraud, fraud_message = None, None, False, None

        exist_user = User.query.filter_by(email=email).first()
        exist_virtual_data = PaymentVirtualAccount.query.filter_by(virtual_number=virtual_number).order_by(
            desc(PaymentVirtualAccount.id)).first()

        if exist_virtual_data:
            fraud_message, is_fraud = check_complete_order(exist_virtual_data.order, exist_user, fraud_message, is_fraud)
            fraud_message, is_fraud, real_user = check_real_user(exist_virtual_data.order, exist_user, fraud_message, is_fraud)

            orderlines_data = OrderLine.query.filter_by(order_id=exist_virtual_data.order.id).all()
            order_data = [{
                "user_id": iorderline.order.user_id,
                "is_paid": iorderline.order.order_status == COMPLETE,
                "email": real_user.email,
                "full_name": '{} {}'.format(real_user.first_name, real_user.last_name),
                "order_id": iorderline.order.id,
                "order_number": iorderline.order.order_number,
                "final_amount": "%.2f" % iorderline.order.final_amount,
                "name": iorderline.name,
                "created": iorderline.order.created.strftime("%Y-%m-%d %H:%M:%S"),
                "order_status": iorderline.order.order_status,
                "payment_gateway_id": iorderline.order.paymentgateway_id
            } for iorderline in orderlines_data]

            midtrans_response = get_midtrans_status(exist_virtual_data.order)
            if midtrans_response:
                fraud_status = midtrans_response.get('fraud_status', '')
                transaction_status = midtrans_response.get('transaction_status', '')

                if midtrans_response.get('payment_type') not in ['echannel', 'bank_transfer']:
                    fraud_message = "This order are not register as Virtual Account in midtrans.! please ask developer for further investigating."

                if fraud_status != FRAUD_ACCEPT and transaction_status != STATUS_CAPTURE:
                    fraud_message = "This order are mark FAILURE in midtrans.!"

                if transaction_status == STATUS_EXPIRED:
                    fraud_message = "This Order already EXPIRED, REFUND to MIDTRANS if user already transfered."
        else:
            fraud_message = "Invalid Virtual Number, Not Found!"

        response_json = {
            "user_data": user_response_data(exist_user),
            "order_data": order_data,
            "is_fraud": True if fraud_message else False,
            "midtrans_status": midtrans_response,
            "fraud_message": fraud_message}

        return Response(json.dumps(response_json), status=httplib.OK, mimetype='application/json')
