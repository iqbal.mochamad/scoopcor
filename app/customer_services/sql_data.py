
def sql_data(email, paymentgateway_id, year):
    return """
        select
            core_orderlines.created,
            core_orderlines.name,
            core_orderlines.offer_id,
            core_orders.id as order_id,
            core_orders.order_status,
            core_orders.order_number,
            core_orders.final_amount,
            core_paymentgateways.id,
            core_payments.payment_status,
            core_orderlines.localized_currency_code
        from 
            core_orderlines
            join core_orders on core_orderlines.order_id = core_orders.id
            join core_payments on core_orderlines.order_id = core_payments.order_id
            join core_paymentgateways on core_orders.paymentgateway_id = core_paymentgateways.id
        where
            core_orderlines.user_id in (
                select id from cas_users where email='{email}'
            ) and core_paymentgateways.id = {paymentgateway_id}
            and extract(year from core_orderlines.created) = {year}
        order by core_orderlines.id desc;
    """.format(email=email, paymentgateway_id=paymentgateway_id, year=year)
