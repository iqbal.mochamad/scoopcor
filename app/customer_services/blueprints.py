from flask import Blueprint
from flask_appsfoundry import Api

from .api import RemoveAccessApi, GoogleInappCsData, CsPatchData, GopayCsData, VirtualAccountCsData

blueprint = Blueprint('customer-service', __name__, url_prefix='/v1/cs')
access_api = Api(blueprint)
access_api.add_resource(RemoveAccessApi, '/remove-access', endpoint='remove-access')
access_api.add_resource(GoogleInappCsData, '/google-iab', endpoint='google-iab-access')
access_api.add_resource(GopayCsData, '/gopay', endpoint='gopay-access')
access_api.add_resource(VirtualAccountCsData, '/virtual-account', endpoint='va-access')

access_api.add_resource(CsPatchData, '/patch-data', endpoint='cs-patch')

