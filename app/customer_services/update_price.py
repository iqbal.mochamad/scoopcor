import decimal
from datetime import datetime
from sqlalchemy import desc

from app.discounts.choices.discount_type import DISCOUNT_OFFER
from app.helpers import purge_redis_cache
from app.master.choices import ANDROID, IOS
from app.offers.choices.offer_type import SINGLE
from app.offers.models import Offer, OfferPlatform
from app.tiers.models import TierAndroid, TierIos
from app.discounts.models import Discount
from app import db

"""
    this for batch update price of offers and recalculate the promos.
"""

def update_price_offers(offer_ids=None, price_idr=None, price_usd=None):
    data_offers = list(set(offer_ids))
    offer_obj = Offer.query.filter(Offer.id.in_(data_offers), Offer.offer_type_id==SINGLE).order_by(desc(Offer.id)).all()

    try:
        for ioffer in offer_obj:
            old_price_idr = ioffer.price_idr

            ioffer.modified = datetime.utcnow()
            ioffer.price_usd = price_usd
            ioffer.price_idr = price_idr
            db.session.add(ioffer)
            db.session.commit()
            print 'price offers successfully updated {} - {} to {}'.format(ioffer.id, old_price_idr, price_idr)

            if ioffer.discount_id:
                offer_valid_discount = Discount.query.filter(
                    Discount.id.in_(ioffer.discount_id), Discount.valid_to > datetime.now(),
                    Discount.discount_type == DISCOUNT_OFFER).order_by(desc(Discount.id)).all()
                latest_discount = offer_valid_discount[0] if offer_valid_discount else None

                if latest_discount:
                    disc_price_usd = latest_discount.calculate_offer_discount(ioffer.price_usd, 'USD')
                    disc_price_idr = latest_discount.calculate_offer_discount(ioffer.price_idr, 'IDR')
                    disc_price_point = latest_discount.calculate_offer_discount(ioffer.price_point, 'PTS')

                    ioffer.discount_tag = latest_discount.tag_name
                    ioffer.discount_name = latest_discount.name
                    ioffer.discount_price_usd = disc_price_usd
                    ioffer.discount_price_idr = disc_price_idr
                    ioffer.discount_price_point = disc_price_point
                    ioffer.is_discount = True

                    db.session.add(ioffer)
                    db.session.commit()
                    print 'Discount Offers successfully updated {}'.format(ioffer.discount_id)

            if ioffer.items:
                for data_item in ioffer.items:
                    purge_redis_cache('items-details-', data_item.id)

    except Exception as e:
        print 'update_price_offers failed {}'.format(e)


def update_price_platform(offer_ids=None, price_idr=None, price_usd=None):
    data_offers = list(set(offer_ids))
    offer_obj = OfferPlatform.query.filter(OfferPlatform.offer_id.in_(data_offers),
        OfferPlatform.platform_id.in_([ANDROID, IOS])).order_by(desc(OfferPlatform.id)).all()

    try:
        for ioffer in offer_obj:
            old_price_usd = ioffer.price_usd
            old_price_idr = ioffer.price_idr

            # price
            ioffer.price_idr = price_idr

            tier_android = TierAndroid.query.filter_by(tier_price=price_usd, tier_type=2).order_by(TierAndroid.id).first()
            tier_ios = TierIos.query.filter_by(tier_price=price_usd, tier_type=2).first()

            if ioffer.platform_id == ANDROID:
                ioffer.price_usd = tier_android.tier_price
                # ioffer.price_idr = tier_android.price_idr
                ioffer.tier_code = tier_android.tier_code
                ioffer.tier_id = tier_android.id

            if ioffer.platform_id == IOS:
                # ioffer.price_idr = tier_ios.price_idr
                ioffer.price_usd = tier_ios.tier_price
                ioffer.tier_code = tier_ios.tier_code
                ioffer.tier_id = tier_ios.id

            db.session.add(ioffer)
            db.session.commit()
            print 'price Platform successfully updated {} - ({}){} to ({}){}'.format(ioffer.offer_id, old_price_usd,
                    old_price_idr, ioffer.price_usd, price_idr)

            if ioffer.discount_id:
                offer_valid_discount = Discount.query.filter(
                    Discount.id.in_(ioffer.discount_id), Discount.valid_to > datetime.now(),
                    Discount.discount_type == DISCOUNT_OFFER).order_by(desc(Discount.id)).all()

                latest_discount = offer_valid_discount[0] if offer_valid_discount else None
                if latest_discount:
                    disc_price_usd = latest_discount.calculate_offer_discount(ioffer.price_usd, 'USD')
                    disc_price_idr = latest_discount.calculate_offer_discount(ioffer.price_idr, 'IDR')
                    disc_price_point = latest_discount.calculate_offer_discount(ioffer.price_point, 'PTS')

                    if ioffer.platform_id == IOS:
                        # tier code always same.
                        ioffer.discount_tier_id = ioffer.tier_id
                        ioffer.discount_tier_code = ioffer.tier_code
                        ioffer.discount_tag = latest_discount.tag_name
                        ioffer.discount_name = latest_discount.name
                        ioffer.discount_tier_price = ioffer.price_usd

                        price = str(disc_price_usd).split('.')
                        final = decimal.Decimal('%s.%s' % (price[0], 99))
                        ioffer.discount_price_usd = final

                        ioffer.discount_price_idr = disc_price_idr
                        ioffer.discount_price_point = disc_price_point

                    if ioffer.platform_id == ANDROID:
                        tier = TierAndroid.query.filter(TierAndroid.price_idr >= disc_price_idr).order_by(
                            TierAndroid.price_idr).first()

                        ioffer.discount_tier_id = tier.id
                        ioffer.discount_tier_code = tier.tier_code
                        ioffer.discount_tag = latest_discount.tag_name
                        ioffer.discount_name = latest_discount.name
                        ioffer.discount_tier_price = disc_price_usd
                        ioffer.discount_price_usd = disc_price_usd
                        ioffer.discount_price_idr = disc_price_idr
                        ioffer.discount_price_point = disc_price_point

                    db.session.add(ioffer)
                    db.session.commit()
                    print 'Discount Platform Offers successfully updated {}'.format(ioffer.discount_id)

    except Exception as e:
        print 'update_price_platform failed {}'.format(e)


