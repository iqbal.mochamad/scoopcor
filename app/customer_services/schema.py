from marshmallow import fields
from app import ma

class GoogleInappCsDataSchema(ma.Schema):
    email = fields.String(required=True)
    iab_order_id = fields.String(required=True)

class CsPatchDataSchema(ma.Schema):
    order_id = fields.Integer(required=True)
    signature = fields.String(required=True)
    paymentgateway_id = fields.Integer(required=False)
    iab_order_id = fields.String(required=False)

class RemoveAccessSchema(ma.Schema):
    email = fields.String(required=True)

class GopayDataSchema(ma.Schema):
    email = fields.String(required=True)
    order_id = fields.Integer(required=True)

class VirtualAccountDataSchema(ma.Schema):
    email = fields.String(required=True)
    virtual_number = fields.String(required=True)
