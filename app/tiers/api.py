import json, httplib

from flask_restful import Resource, reqparse
from flask import Response, request

from app.auth.decorators import token_required
from app.helpers import conflict_message, bad_request_message, internal_server_message
from app.tiers.models import TierAndroid, TierIos, TierWp

from app.master.models import Currencies
from app.custom_get import CustomGet


class TiersIosListApi(Resource):

    ''' Shows a list of Tiersios and do POST / GET '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('tier_price', type=str, required=True, location='json')
        self.reqparser.add_argument('tier_order', type=int, required=True, location='json')
        self.reqparser.add_argument('currency_id', type=int, required=True, location='json')
        self.reqparser.add_argument('tier_type', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')

        #optional parameters
        self.reqparser.add_argument('tier_code', type=unicode, location='json')
        self.reqparser.add_argument('price_idr', type=str, location='json')

        super(TiersIosListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_ios_tier')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = CustomGet(param, 'ios_tiers', 'POST').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="ios_tiers", method="POST",
                error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_global_ios_tier')
    def post(self):
        args = self.reqparser.parse_args()

        if args['tier_type'] in [1,2]:
            code = args['tier_code']
            if code:
                pass
            else:
                return bad_request_message(modul="tiers_ios", method="POST",
                    param="tier_code", not_found='EMPTY INVALID', req=args)

            po = TierIos.query.filter_by(tier_code=args['tier_code']).first()
            if po:
                return conflict_message(modul="tiers_ios", method="POST",
                    conflict=args['tier_code'], req=args)

        #construct in same dict
        kwargs = {}
        for item in args:
            if args[item] == None:
                pass
            else:
                kwargs[item] = args[item]

        if args['currency_id']:
            currency = Currencies.query.filter_by(id=args['currency_id']).first()
            if not currency:
                return bad_request_message(modul="tiers_ios", method="POST",
                    param="currency_id", not_found=args['currency_id'], req=args)
            kwargs['currency_id'] = currency.id

        try:
            m = TierIos(**kwargs)
            sv = m.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="tiers_ios", method="POST",
                    error=sv.get('message', ''), req=str(args))
            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.CREATED,
                    mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="tiers_ios", method="POST",
                error=str(e), req=str(args))

class TiersIosApi(Resource):

    ''' GET/PUT/DELETE Tier Ios '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('tier_price', type=str, required=True, location='json')
        self.reqparser.add_argument('tier_order', type=int, required=True, location='json')
        self.reqparser.add_argument('currency_id', type=int, required=True, location='json')
        self.reqparser.add_argument('tier_type', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')

        #optional parameters
        self.reqparser.add_argument('tier_code', type=unicode, location='json')
        self.reqparser.add_argument('price_idr', type=str, location='json')

        super(TiersIosApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_ios_tier')
    def get(self, tier_id=None):
        m = TierIos.query.filter_by(id=tier_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_ios_tier')
    def put(self, tier_id=None):

        m = TierIos.query.filter_by(id=tier_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        args = self.reqparser.parse_args()

        if args['tier_type'] in [1,2]:
            code = args['tier_code']
            if code:
                pass
            else:
                return bad_request_message(modul="tiers_ios", method="POST",
                    param="tier_code", not_found='EMPTY INVALID', req=args)

            #checking if conflict when edit
            if args['tier_code'] != m.tier_code:
                u = TierIos.query.filter_by(tier_code=args['tier_code']).first()
                if u:
                    return conflict_message(modul="tiers_ios", method="PUT",
                        conflict=args['tier_code'], req=args)

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        if args['currency_id']:
            currency = Currencies.query.filter_by(id=args['currency_id']).first()
            if not currency:
                return bad_request_message(modul="tiers_ios", method="POST",
                    param="currency_id", not_found=args['currency_id'], req=args)
            kwargs['currency_id'] = currency.id

        try:
            f = TierIos.query.get(tier_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="tiers_ios", method="PUT",
                    error=sv.get('message', ''), req=str(args))

            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="tiers_ios", method="PUT",
                error=e, req=str(args))


    @token_required('can_read_write_global_all')
    def delete(self, tier_id=None):
        f = TierIos.query.filter_by(id=tier_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=httplib.NOT_FOUND)


class TiersAndroidListApi(Resource):

    ''' Shows a list of Tiersios and do POST / GET '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('tier_price', type=str, required=True, location='json')
        self.reqparser.add_argument('tier_order', type=int, required=True, location='json')
        self.reqparser.add_argument('currency_id', type=int, required=True, location='json')
        self.reqparser.add_argument('tier_type', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')

        #optional parameters
        self.reqparser.add_argument('tier_code', type=unicode, location='json')
        self.reqparser.add_argument('price_idr', type=str, location='json')

        super(TiersAndroidListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_android_tier')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = CustomGet(param, 'android_tiers', 'POST').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="android_tiers", method="POST",
                error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_global_android_tier')
    def post(self):
        args = self.reqparser.parse_args()

        if args['tier_type'] in [1,2]:
            code = args['tier_code']
            if code:
                pass
            else:
                return bad_request_message(modul="tiers_android", method="POST",
                    param="tier_code", not_found='EMPTY INVALID', req=args)

            po = TierAndroid.query.filter_by(tier_code=args['tier_code']).first()
            if po:
                return conflict_message(modul="tiers_android", method="POST",
                    conflict=args['tier_code'], req=args)

        #construct in same dict
        kwargs = {}
        for item in args:
            if args[item] == None:
                pass
            else:
                kwargs[item] = args[item]

        if args['currency_id']:
            currency = Currencies.query.filter_by(id=args['currency_id']).first()
            if not currency:
                return bad_request_message(modul="tiers_android", method="POST",
                    param="currency_id", not_found=args['currency_id'], req=args)
            kwargs['currency_id'] = currency.id

        try:
            m = TierAndroid(**kwargs)
            sv = m.save()

            if sv.get('invalid', False):
                return internal_server_message(modul="tiers_android", method="POST",
                    error=sv.get('message', ''), req=str(args))
            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.CREATED,
                    mimetype='application/json')

        except Exception, e:
                return internal_server_message(modul="tiers_android", method="POST",
                error=str(e), req=str(args))

class TiersAndroidApi(Resource):

    ''' GET/PUT/DELETE Tier Ios '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('tier_price', type=str, required=True, location='json')
        self.reqparser.add_argument('tier_order', type=int, required=True, location='json')
        self.reqparser.add_argument('currency_id', type=int, required=True, location='json')
        self.reqparser.add_argument('tier_type', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')

        #optional parameters
        self.reqparser.add_argument('tier_code', type=unicode, location='json')
        self.reqparser.add_argument('price_idr', type=str, location='json')

        super(TiersAndroidApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_android_tier')
    def get(self, tier_id=None):
        m = TierAndroid.query.filter_by(id=tier_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_android_tier')
    def put(self, tier_id=None):

        m = TierAndroid.query.filter_by(id=tier_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        args = self.reqparser.parse_args()

        if args['tier_type'] in [1,2]:
            code = args['tier_code']
            if code:
                pass
            else:
                return bad_request_message(modul="tiers_android", method="POST",
                    param="tier_code", not_found='EMPTY INVALID', req=args)

            #checking if conflict when edit
            if args['tier_code'] != m.tier_code:
                u = TierAndroid.query.filter_by(tier_code=args['tier_code']).first()
                if u:
                    return conflict_message(modul="tiers_android", method="PUT",
                        conflict=args['tier_code'], req=args)

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        if args['currency_id']:
            currency = Currencies.query.filter_by(id=args['currency_id']).first()
            if not currency:
                return bad_request_message(modul="tiers_android", method="POST",
                    param="currency_id", not_found=args['currency_id'], req=args)
            kwargs['currency_id'] = currency.id

        try:
            f = TierAndroid.query.get(tier_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="tiers_android", method="PUT",
                    error=sv.get('message', ''), req=str(args))

            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="tiers_android", method="PUT",
                error=e, req=str(args))


    @token_required('can_read_write_global_all')
    def delete(self, tier_id=None):
        f = TierAndroid.query.filter_by(id=tier_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=httplib.NOT_FOUND)


class TiersWpListApi(Resource):

    ''' Shows a list of Tiersios and do POST / GET '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('tier_price', type=str, required=True, location='json')
        self.reqparser.add_argument('tier_order', type=int, required=True, location='json')
        self.reqparser.add_argument('currency_id', type=int, required=True, location='json')
        self.reqparser.add_argument('tier_type', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')

        #optional parameters
        self.reqparser.add_argument('tier_code', type=unicode, location='json')

        super(TiersWpListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_windows_tier')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = CustomGet(param, 'wp_tiers', 'POST').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="tiers_wp", method="POST",
                error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_global_windows_tier')
    def post(self):
        args = self.reqparser.parse_args()

        if args['tier_type'] in [1,2]:
            code = args['tier_code']
            if code:
                pass
            else:
                return bad_request_message(modul="tiers_wp", method="POST",
                    param="tier_code", not_found='EMPTY INVALID', req=args)

            po = TierWp.query.filter_by(tier_code=args['tier_code']).first()
            if po:
                return conflict_message(modul="tiers_wp", method="POST",
                    conflict=args['tier_code'], req=args)

        #construct in same dict
        kwargs = {}
        for item in args:
            if args[item] == None:
                pass
            else:
                kwargs[item] = args[item]

        if args['currency_id']:
            currency = Currencies.query.filter_by(id=args['currency_id']).first()
            if not currency:
                return bad_request_message(modul="tiers_wp", method="POST",
                    param="currency_id", not_found=args['currency_id'], req=args)
            kwargs['currency_id'] = currency.id

        try:
            m = TierWp(**kwargs)
            sv = m.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="tiers_wp", method="POST",
                    error=sv.get('message', ''), req=str(args))
            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.CREATED,
                    mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="tiers_wp", method="POST",
                error=str(e), req=str(args))

class TiersWpApi(Resource):

    ''' GET/PUT/DELETE Tier Ios '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('tier_price', type=str, required=True, location='json')
        self.reqparser.add_argument('tier_order', type=int, required=True, location='json')
        self.reqparser.add_argument('currency_id', type=int, required=True, location='json')
        self.reqparser.add_argument('tier_type', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')

        #optional parameters
        self.reqparser.add_argument('tier_code', type=unicode, location='json')

        super(TiersWpApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_windows_tier')
    def get(self, tier_id=None):
        m = TierWp.query.filter_by(id=tier_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_windows_tier')
    def put(self, tier_id=None):

        m = TierWp.query.filter_by(id=tier_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        args = self.reqparser.parse_args()

        if args['tier_type'] in [1,2]:
            code = args['tier_code']
            if code:
                pass
            else:
                return bad_request_message(modul="tiers_wp", method="POST",
                    param="tier_code", not_found='EMPTY INVALID', req=args)

            #checking if conflict when edit
            if args['tier_code'] != m.tier_code:
                u = TierWp.query.filter_by(tier_code=args['tier_code']).first()
                if u:
                    return conflict_message(modul="tiers_wp", method="PUT",
                        conflict=args['tier_code'], req=args)

        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        if args['currency_id']:
            currency = Currencies.query.filter_by(id=args['currency_id']).first()
            if not currency:
                return bad_request_message(modul="tiers_wp", method="POST",
                    param="currency_id", not_found=args['currency_id'], req=args)
            kwargs['currency_id'] = currency.id

        try:
            f = TierWp.query.get(tier_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="tiers_wp", method="PUT",
                    error=sv.get('message', ''), req=str(args))

            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="tiers_wp", method="PUT",
                error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, tier_id=None):
        f = TierWp.query.filter_by(id=tier_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=httplib.NOT_FOUND)
