import csv
from collections import namedtuple

from app.payments.choices.gateways import APPLE
from app.tiers.models import TierMatrix

from app import app


def migrate_csv_matrix(fp=None, paymentgateway_id=None):

    reader = csv.reader(open(fp, 'rU'), delimiter=';')
    raw_label = reader.next()
    label_name = raw_label[0].split(',')

    data_final = []
    for b in reader:
        price_data = [i.split(',') for i in b]
        rw = zip(label_name, price_data[0])
        data_final.append(dict(rw))

    for data in data_final:
        matrix = TierMatrix(
            paymentgateway_id = paymentgateway_id,
            tier_price = data['USD'],
            is_alternate = data['ALTERNATE']
        )

        # delete USD values
        del data['ALTERNATE']

        if paymentgateway_id == APPLE:
            matrix.localize_tier = data['Tier']
            del data['Tier']

        matrix.rate_price = data
        matrix.save()

        print 'OK', matrix
