from app import db
from flask_appsfoundry.models import TimestampMixin

from app.master.models import Currencies
from sqlalchemy.dialects.postgresql import JSON

"""
    LEGEND
        TIER TYPE
        1. NON-CONSUMABLE
        2. CONSUMABLE
"""


class TierIos(db.Model, TimestampMixin):

    __tablename__ = 'core_iostiers'

    id = db.Column(db.Integer, primary_key=True)
    tier_price = db.Column(db.Numeric(10, 2), nullable=False)
    price_idr = db.Column(db.Numeric(10, 2), default=0)
    tier_code = db.Column(db.String(250))
    tier_order = db.Column(db.Integer)
    currency_id = db.Column(db.Integer, db.ForeignKey('core_currencies.id'), nullable=False)
    tier_type = db.Column(db.Integer) #READ LEGEND : TIER TYPE
    is_active = db.Column(db.Boolean(), nullable=False)


    def __repr__(self):
        return '<tier_code : %s>' % self.tier_code

    def get_currency_iso(self):
        iso = ''
        currency = Currencies.query.filter_by(id=self.currency_id).first()
        if currency:
            iso = currency.iso4217_code
        return iso

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "TierIos success add"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "TierIos success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}
        rv['id'] = self.id
        if self.tier_price: rv['tier_price'] = '%.2f' % self.tier_price
        else: rv['tier_price'] = 0

        if self.tier_price: rv['price_idr'] = '%.2f' % self.price_idr
        else: rv['price_idr'] = 0

        rv['tier_code'] = self.tier_code
        rv['tier_order'] = self.tier_order
        rv['currency_id'] = self.currency_id
        rv['currency_code'] = self.get_currency_iso()
        rv['tier_type'] = self.tier_type
        rv['is_active'] = self.is_active

        return rv

class TierAndroid(db.Model, TimestampMixin):

    __tablename__ = 'core_androidtiers'

    id = db.Column(db.Integer, primary_key=True)
    tier_price = db.Column(db.Numeric(10, 2), nullable=False)
    price_idr = db.Column(db.Numeric(10, 2), default=0)
    tier_code = db.Column(db.String(250))
    tier_order = db.Column(db.Integer)
    currency_id = db.Column(db.Integer, db.ForeignKey('core_currencies.id'), nullable=False)
    tier_type = db.Column(db.Integer) #READ LEGEND : TIER TYPE
    is_active = db.Column(db.Boolean(), nullable=False)

    def __repr__(self):
        return '<tier_code : %s>' % self.tier_code

    def get_currency_iso(self):
        iso = ''
        currency = Currencies.query.filter_by(id=self.currency_id).first()
        if currency:
            iso = currency.iso4217_code
        return iso

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "TierAndroid success add"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "TierAndroid success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}
        rv['id'] = self.id

        if self.tier_price:
            rv['tier_price'] = '%.2f' % self.tier_price
        else:
            rv['tier_price'] = 0

        if self.tier_price:
            rv['price_idr'] = '%.2f' % self.price_idr
        else:
            rv['price_idr'] = 0


        rv['tier_code'] = self.tier_code
        rv['tier_order'] = self.tier_order
        rv['currency_id'] = self.currency_id
        rv['currency_code'] = self.get_currency_iso()
        rv['tier_type'] = self.tier_type
        rv['is_active'] = self.is_active

        return rv

class TierWp(db.Model, TimestampMixin):

    __tablename__ = 'core_windowsphonetiers'

    id = db.Column(db.Integer, primary_key=True)
    tier_price = db.Column(db.Numeric(10, 2), nullable=False)
    price_idr = db.Column(db.Numeric(10, 2), default=0)
    tier_code = db.Column(db.String(250))
    tier_order = db.Column(db.Integer)
    currency_id = db.Column(db.Integer, db.ForeignKey('core_currencies.id'), nullable=False)
    tier_type = db.Column(db.Integer) #READ LEGEND : TIER TYPE
    is_active = db.Column(db.Boolean(), nullable=False)


    def __repr__(self):
        return '<tier_code : %s>' % self.tier_code

    def get_currency_iso(self):
        iso = ''
        currency = Currencies.query.filter_by(id=self.currency_id).first()
        if currency:
            iso = currency.iso4217_code
        return iso

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "TierWp success add"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "TierWp success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}
        rv['id'] = self.id
        rv['tier_price'] = '%.2f' % self.tier_price
        rv['price_idr'] = '%.2f' % self.price_idr
        rv['tier_code'] = self.tier_code
        rv['tier_order'] = self.tier_order
        rv['currency_id'] = self.currency_id
        rv['currency_code'] = self.get_currency_iso()
        rv['tier_type'] = self.tier_type
        rv['is_active'] = self.is_active

        return rv

class TierMatrix(db.Model, TimestampMixin):

    __tablename__ = 'core_matrixtiers'

    id = db.Column(db.Integer, primary_key=True)
    paymentgateway_id = db.Column(db.Integer)
    price_idr = db.Column(db.Numeric(10, 2), default=0)
    tier_price = db.Column(db.Numeric(10, 2))
    rate_price = db.Column(JSON(db.Text))
    is_alternate = db.Column(db.Boolean(), default=False)
    localize_tier = db.Column(db.String(250))

    valid_to = db.Column(db.DateTime)
    valid_from = db.Column(db.DateTime)

    def __repr__(self):
        return '<paymentgateway_id : %s>' % self.paymentgateway_id

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Tier Matrix success add"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}
        rv['id'] = self.id

        rv['paymentgateway_id'] = self.paymentgateway_id
        rv['tier_price'] = '%.2f' % self.tier_price
        rv['rate_price'] = self.rate_price
        rv['is_alternate'] = self.is_alternate
        rv['localize_tier'] = self.localize_tier

        return rv


