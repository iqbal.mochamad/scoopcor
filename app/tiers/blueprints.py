from flask import Blueprint
from flask.ext import restful

from .api import TiersIosApi, TiersIosListApi, TiersAndroidApi, \
    TiersAndroidListApi, TiersWpApi, TiersWpListApi


iostiers_blueprint = Blueprint('ios_tiers', __name__, url_prefix='/v1/ios_tiers')
iostiers_api = restful.Api(iostiers_blueprint)
iostiers_api.add_resource(TiersIosListApi, '')
iostiers_api.add_resource(TiersIosApi, '/<int:tier_id>', endpoint='tier_ios')

androidtiers_blueprint = Blueprint('android_tiers', __name__, url_prefix='/v1/android_tiers')
androidtiers_api = restful.Api(androidtiers_blueprint)
androidtiers_api.add_resource(TiersAndroidListApi, '')
androidtiers_api.add_resource(TiersAndroidApi, '/<int:tier_id>', endpoint='tiers_android')

wptiers_blueprint = Blueprint('wp_tiers', __name__, url_prefix='/v1/wp_tiers')
wptiers_api = restful.Api(wptiers_blueprint)
wptiers_api.add_resource(TiersWpListApi, '')
wptiers_api.add_resource(TiersWpApi, '/<int:tier_id>', endpoint='tiers_wp')

