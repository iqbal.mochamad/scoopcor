from __future__ import unicode_literals

import random

from nose.tools import istest

from app import db
from app.payment_gateways.models import PaymentGateway
from app.users.tests.fixtures import OrganizationFactory
from app.utils.testcases import CrudBase
from tests.fixtures.sqlalchemy.payments.gateways import PaymentGatewayFactory


@istest
class PaymentGatewayCrudTests(CrudBase):

    request_url = '/v1/payment_gateways'
    fixture_factory = PaymentGatewayFactory
    model = PaymentGateway
    list_key = 'payment_gateways'

    @classmethod
    def setUpClass(cls):
        super(PaymentGatewayCrudTests, cls).setUpClass()
        cls.org = OrganizationFactory()
        # clear all already created data from generate_static_sql_fixtures(session)
        db.engine.execute("delete from core_paymentgateways;")
        cls.session.commit()

    def assert_saved_data(self, saved_entity, request_body):
        # because some value was converted from string to int and vice versa, we need to override this method
        copy_req_body = request_body.copy()
        # assert and remove (cause already asserted)
        self.assertEqual('{0:.2f}'.format(saved_entity.minimal_amount), copy_req_body.pop('minimal_amount'))
        # assert the remaining fields
        super(PaymentGatewayCrudTests, self).assert_saved_data(saved_entity, copy_req_body)

    def get_request_body(self):
        with self.on_session(self.org):
            return {
                "name": self.fake.name(),
                "payment_flow_type": random.choice(list(PaymentGateway.Type)).value,
                "sort_priority": self.fake.pyint(),
                "is_active": True,
                "base_currency_id": 2,
                "clients": ['1', '2', '3', '4'],
                "description": self.fake.bs(),
                "organization_id": self.org.id,
                "mnc": self.fake.bs()[0:10],
                "mcc": self.fake.bs()[0:10],
                "sms_number": '{}'.format(self.fake.pyint())[0:9],
                "slug": self.fake.slug(),
                "meta": self.fake.bs(),
                "icon_image_normal": self.fake.url(),
                "icon_image_highres": self.fake.url(),
                "minimal_amount": '{0:.2f}'.format(self.fake.pyint()),
            }

    def get_expected_response(self, entity):

        expected = {
            "id": entity.id,
            "icon_image_normal": entity.icon_image_normal,
            "organization_id": entity.organization_id,
            "sort_priority": entity.sort_priority,
            "sms_number": entity.sms_number,
            "name": entity.name,
            "payment_flow_type": entity.payment_flow_type,
            "clients": ['{}'.format(client) for client in entity.clients] if entity.clients else None,
            "mcc": entity.mcc,
            "payment_api_url": entity.payment_api_url,
            "slug": entity.slug,
            "is_active": entity.is_active,
            "currency": {
                "id": entity.base_currency.id,
                "iso4217_code": entity.base_currency.iso4217_code,
            },
            "meta": entity.meta,
            "mnc": entity.mnc,
            "icon_image_highres": entity.icon_image_highres,
            "minimal_amount": str(entity.minimal_amount) if entity.minimal_amount else None,
            "description": entity.description,
        }
        return expected
