from flask import Blueprint
from flask_appsfoundry import Api

from .api import PaymentGatewayListApi, PaymentGatewayApi


blueprint = Blueprint('payment_gateways', __name__, url_prefix='/v1/payment_gateways')

api = Api(blueprint)

api.add_resource(PaymentGatewayListApi, '')
api.add_resource(PaymentGatewayApi, '/<int:gateway_id>', endpoint='payment_gateway')
