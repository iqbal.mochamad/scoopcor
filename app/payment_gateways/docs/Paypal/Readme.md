# Paypal

Online Documentation: [developer.paypal.com](http://)

### Sandbox

**API Settings**

    Merchant ID: willson-facilitator@apps-foundry.com
    Endpoint: api.sandbox.paypal.com
    Client ID: AZmlhxBWpmfSbeNrDI7FMM_52tR6nXwr9skVEbzLvbmpE2rXq972e0m8GFi5
    Secret: EOPGEhBj0AfpVMrQZGXvlQ1lHy9KlqV1Za0E12uClBazqjlJBY5Ikv4dfDRq
    
**Test Account**

    Email: paypal-sandbox@apps-foundry.com
    Password: Passw0rd

### Production

**API Settings**

    Merchant ID: willson@apps-foundry.com
    Endpoint: api.paypal.com
    Client ID: AUa1ohChuaacKJXHY2_bk2d0qZqKtxm4ZuCj-4ETQ7P3cOipetP3FJ56ipBE
    Secret: ELzAFRD_wwhIvEJjLGaEVpBnjqSNHNSeMwhrAeQ5G5IyIHNEDTiEEHgeqWSe

**Test Account**

    - not available, use private one -

