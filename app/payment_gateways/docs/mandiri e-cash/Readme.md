# mandiri e-cash

### Global

**Portal**

    Branchless Banking Agen: [https://www.mandiriecash.com/agent](http://)

    Merchant: [http://pms.mandiriecash.co.id/login](http://)

    Username: 49801000012

    Password: Appsfoundry555

### Sandbox

**API Settings**

    Merchant ID: scoopindonesia

    Password: 123456

**Test Account**

    Phone No.: 085267878089 (Brian), 08159139085 (Johnsons)

    PIN: 123456

### Production

**API Settings**

    Merchant ID: 49801000012

    Password: 156294be2926

**Test Account**

    Phone No.: 081293789904

    PIN: 123321

### Supports

**Adhi**

Email: adhi@ptdam.com

Phone: +6281210854566

Skype: adhiwicaksono.ptdam

YM: wyrm_moonsong@yahoo.com

**Ricky**

Email: ricky@ptdam.com

Phone: +62812-86441100, +62818-08661100, +62815-17794900
