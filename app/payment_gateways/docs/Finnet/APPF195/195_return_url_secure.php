<?php
require_once("195_config.php");
require_once("195_function.php");

//IMPORTANT!! This is to tell the engine 195 that the sent data has been accepted by the merchant
//PENTING!! Ini adalah untuk memberitahu mesin 195 bahwa data yang dikirim telah diterima oleh pedagang
echo '00';

//TO WRITE LOG POST FROM 195 RESPON
$log = '
';
foreach($_POST as $name=>$value){
	$_POST[$name]=htmlspecialchars(strip_tags(trim($value)));
$log .= $name.' : '.htmlspecialchars(strip_tags(trim($value))).'
';
}

//EXTRACT POST TO VARIABLE
extract($_POST);

//REQEUST CODE 195
if($_POST["trax_type"]=="195Code"){
	$log = '
RESPON REQUEST '.date("Y-m-d h:i:s").' ENGINE 195
'.$log;
	writeLog($log);
	$mer_signature = $_POST["mer_signature"];
	unset($_POST["mer_signature"]);
	unset($_POST["amount"]);
	unset($_POST["paid"]);
	if(check_mer_signature($mer_signature,$_POST,$PASS_195)){ //SECURE DATA
		//DO ACTION WITH YOUR CONDITION
		$sql = 'UPDATE 195_transaction SET trax_type="'.$trax_type.'",payment_code="'.$payment_code.'" WHERE invoice="'.$invoice.'"';
		mysql_query($sql);
	}
}

//PAYMENT SUCCESS RESULT CODE 00
if($_POST["trax_type"]=="Payment" and $_POST["result_code"]=="00" ){
	$log = '
RESPON PAYMENT SUCCESS '.date("Y-m-d h:i:s").' ENGINE 195
'.$log;
	writeLog($log);
	$mer_signature = $_POST["mer_signature"];
	unset($_POST["mer_signature"]);
	unset($_POST["amount"]);
	unset($_POST["paid"]);
	if(check_mer_signature($mer_signature,$_POST,$PASS_195)){ //SECURE DATA
		//DO ACTION WITH YOUR CONDITION
		$sql = 'UPDATE 195_transaction SET trax_type="'.$trax_type.'",result_code="'.$result_code.'",result_desc="'.$result_desc.'",log_no="'.$log_no.'",payment_source="'.$payment_source.'"  WHERE payment_code="'.$payment_code.'"';
		mysql_query($sql);
	}
}

//PAYMENT EXPIRED RESULT CODE 05
if($_POST["trax_type"]=="Payment" and $_POST["result_code"]=="05" ){
	$log = '
RESPON PAYMENT EXPIRED  '.date("Y-m-d h:i:s").' ENGINE 195
'.$log;
	writeLog($log);
	$mer_signature = $_POST["mer_signature"];
	unset($_POST["mer_signature"]);
	unset($_POST["amount"]);
	unset($_POST["paid"]);
	if(check_mer_signature($mer_signature,$_POST,$PASS_195)){ //SECURE DATA
		//DO ACTION WITH YOUR CONDITION
		$sql = 'UPDATE 195_transaction SET trax_type="'.$trax_type.'",result_code="'.$result_code.'",result_desc="'.$result_desc.'",log_no="'.$log_no.'",payment_source="'.$payment_source.'"  WHERE payment_code="'.$payment_code.'"';
		mysql_query($sql);
	}
}

//CHECK STATUS TRANSACTION
if($_POST["trax_type"]=="195Status"){
	$log = '
CHECK STATUS '.date("Y-m-d h:i:s").' ENGINE 195
'.$log;
	writeLog($log);
	$mer_signature = $_POST["mer_signature"];
	unset($_POST["mer_signature"]);
	unset($_POST["amount"]);
	unset($_POST["paid"]);
	if(check_mer_signature($mer_signature,$_POST,$PASS_195)){ //SECURE DATA
		if($_POST["result_code"]=="00"){ //PAID
			//DO ACTION WITH YOUR CONDITION
		}else if($_POST["result_code"]=="04"){ //UNPAID
			//DO ACTION WITH YOUR CONDITION
		}else if($_POST["result_code"]=="05"){ //EXPIRED
			//DO ACTION WITH YOUR CONDITION
		}else if($_POST["result_code"]=="14"){ //NOT FOUND
			//DO ACTION WITH YOUR CONDITION
		}
	}
}
?>