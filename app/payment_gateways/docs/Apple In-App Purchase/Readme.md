# Apple In-App Purchase

Online Documentation: [https://developer.apple.com/in-app-purchase/](http://)

### Global

**API Settings**

    Shared Secret: cc34aa7f178042d4aa8879e91a52e3c0

### Sandbox

**API Settings**

    Receipt Verification URL: https://sandbox.itunes.apple.com/verifyReceipt

**Test Account 1**

    Username: op3@as.com

    Password: Passw0rd123

**Test Account 2**

    Username: op2.us@as.com

    Password: Passw0rd123

### Production

**API Settings**

    Receipt Verification URL: https://buy.itunes.apple.com/verifyReceipt