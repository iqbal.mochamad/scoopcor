import os
from enum import IntEnum
from flask import current_app
from flask_appsfoundry import models
from sqlalchemy.dialects.postgresql import ARRAY, ENUM

from app import db, app
from app.master.models import Currencies

GROUP_MIDTRANS = 'midtrans'
GROUP_E2PAY = 'e2pay'
GROUP_OTHERS = 'others'

PAYMENTGATEWAY_GROUP = {
    GROUP_MIDTRANS: 'midtrans',
    GROUP_E2PAY: 'e2pay',
    GROUP_OTHERS: 'others'
}


class PaymentGateway(models.TimestampMixin, db.Model):

    class Type(IntEnum):
        apple = 1
        paypal = 2
        free = 3
        finnet = 5
        tcash = 6
        point = 9
        clickpay = 11
        veritrans = 12
        xl = 13
        ecash = 14
        iab = 15
        iap = 16
        klikbca = 17
        briepay = 18
        brimocash = 19
        cimbclicks = 21
        elevenia = 22
        gramedia = 23
        wallet = 24
        direct_payment = 25
        indomaret = 26
        kredivo = 27
        va_permata = 28
        va_bca = 29
        va_mandiri = 30
        va_bni = 31
        gopay = 32
        link_aja = 33
        ovo = 34
        cimb_click = 35
        cimb_go_mobile = 36
        cimb_rekpon = 37
        shopee_pay = 38

    class Flow(IntEnum):
        not_applicable = 1
        manual = 2
        authorize_validate = 3
        charged = 4
        validate = 5
        authorize_capture = 6

    name = db.Column(db.String(100), nullable=False)
    mnc = db.Column(db.String(50), doc='mobile only')
    mcc = db.Column(db.String(50), doc='mobile only')
    sms_number = db.Column(db.String(10), doc='mobile only')
    payment_flow_type = db.Column(db.SmallInteger, nullable=True, default=5) # READ LEGEND PAYMENT TYPE

    description = db.Column(db.Text)
    slug = db.Column(db.String(250))
    meta = db.Column(db.Text)
    icon_image_normal = db.Column(db.String(100))
    icon_image_highres = db.Column(db.String(100))
    sort_priority = db.Column(db.SmallInteger, nullable=True, default=1)
    is_active = db.Column(db.Boolean(), default=False)

    # this can't enforce it's key because it's in CAS
    organization_id = db.Column(db.Integer, nullable=True)
    clients = db.Column(ARRAY(db.String))

    base_currency_id = db.Column(db.Integer, db.ForeignKey('core_currencies.id'), nullable=False)
    base_currency = db.relationship('Currencies')

    minimal_amount = db.Column(db.Numeric(10, 2))
    lowest_supported_version = db.Column(db.String(20))

    payment_group = db.Column(ENUM(*PAYMENTGATEWAY_GROUP.values(), name='paymentgateway_group'), nullable=False)
    merchant_code = db.Column(db.String(250))
    merchant_key = db.Column(db.String(250))

    is_renewal = db.Column(db.Boolean(), default=False)

    @property
    def payment_api_url(self):
        """ return api url from payment gateway vendor's for front end (to hit in payment process)
        (better provided from back end/COR instead of hard coded in Front End code)
        :return:
        """
        api_urls = {
            PaymentGateway.Type.ecash: current_app.config.get('ECASH_SITE')
        }
        return api_urls.get(PaymentGateway.Type.ecash, None)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "PaymentGateway success add"}
        except Exception as e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "PaymentGateway success deleted"}
        except Exception as e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def get_currencies(self):
        tmp = {}
        tmp['iso4217_code'] = ''

        currency = Currencies.query.filter_by(id=self.base_currency_id).first()
        if currency:
            tmp['iso4217_code'] = currency.iso4217_code
            tmp['id'] = currency.id

        return tmp

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['name'] = self.name
        rv['mnc'] = self.mnc if self.mnc else ""
        rv['mcc'] = self.mcc if self.mcc else ""
        rv['sms_number'] = self.sms_number if self.sms_number else ""
        rv['payment_flow_type'] = self.payment_flow_type
        rv['description'] = self.description if self.description else ""
        rv['slug'] = self.slug
        rv['meta'] = self.meta if self.meta else ""
        rv['sort_priority'] = self.sort_priority
        rv['is_active'] = self.is_active
        rv['currency'] = self.get_currencies()
        rv['organization_id'] = self.organization_id
        rv['clients'] = self.clients if self.clients else ""
        rv['payment_api_url'] = self.payment_api_url
        rv['images'] = os.path.join(
            app.config['BOTO3_NGINX_STATICS'],
            'payment-gateways',
            self.icon_image_highres if self.icon_image_highres else ''
        )
        rv['minimal_amount'] = '%.2f' % (self.minimal_amount) if self.minimal_amount else ""
        rv['lowest_supported_version'] = self.lowest_supported_version if self.lowest_supported_version else ""

        return rv
