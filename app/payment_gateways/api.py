import json, httplib, os

from flask_appsfoundry import Resource
from flask import Response, request, jsonify
from flask_appsfoundry.parsers.converters import ImageFieldParser

from app import db, ma, app
from app.auth.decorators import token_required
from app.custom_get import CustomGet
from app.helpers import internal_server_message, regenerate_slug, generate_slug
from app.uploads.aws.helpers import aws_upload_file
from app.users.organizations import Organization
from app.utils.marshmallow_helpers import schema_load
from .models import PaymentGateway
from marshmallow import fields


class PaymentGatewaySchema(ma.Schema):
    name = fields.Str(required=True)
    base_currency_id = fields.Int(required=True, default=1)
    is_active = fields.Bool(required=False, default=True)
    minimal_amount = fields.Int(required=False, default=0)
    images = fields.Str(required=False)

def upload_image_paymentgateway(image=None):
    save_directory = os.path.join('/tmp', 'payment-gateways')
    if not os.path.exists(save_directory):
        os.makedirs(save_directory)

    p = ImageFieldParser(save_directory)
    files = p.from_base64(image)

    raw_files = os.path.join(save_directory, files)
    aws_upload_file(bucket_name=app.config['BOTO3_GRAMEDIA_STATIC_BUCKET'],
                    bucket_folder='payment-gateways/',
                    transfer_file=raw_files,
                    output_file=files,
                    is_public_access=True)
    # remove files..

    return files


class PaymentGatewayListApi(Resource):
    ''' Shows a list of PaymentGateway and do POST / GET
    '''

    @token_required('can_read_write_global_all, can_read_write_public,'
                    'can_read_write_public_ext, can_read_global_payment_gateway')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = CustomGet(param, 'payment_gateways', 'POST').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="payment_gateway", method="POST",
                                           error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_global_payment_gateway')
    def post(self):
        schema = schema_load(PaymentGatewaySchema())
        payment_exist = PaymentGateway.query.filter_by(name=schema.get('name')).first()
        if payment_exist:
            resp = jsonify('Payment {} is exist'.format(payment_exist.name))
            resp.status_code = httplib.CONFLICT
        else:
            new_files = upload_image_paymentgateway(schema.get('images'))
            schema['icon_image_normal'] = new_files
            schema['icon_image_highres'] = new_files
            del schema['images']

            new_record = PaymentGateway(**schema)
            db.session.add(new_record)
            db.session.commit()

            # generate slug.
            slug = generate_slug(new_record.name)
            exist_slug = PaymentGateway.query.filter(PaymentGateway.slug == slug).first()
            if not exist_slug:
                new_record.slug = slug
            else:
                new_slug = regenerate_slug(PaymentGateway, slug, exist_slug)
                new_record.slug = new_slug

            resp = jsonify(new_record.values())
            resp.status_code = httplib.CREATED

        return resp


class PaymentGatewayApi(Resource):
    ''' GET/PUT/DELETE PaymentGateway '''

    @token_required('can_read_write_global_all, can_read_write_public,'
                    'can_read_write_public_ext, can_read_global_payment_gateway')
    def get(self, gateway_id=None):
        m = PaymentGateway.query.filter_by(id=gateway_id).first()
        if m:
            resp = get_response_body(m)
            return Response(resp, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_payment_gateway')
    def put(self, gateway_id=None):
        schema = schema_load(PaymentGatewaySchema())
        exist_data = PaymentGateway.query.filter_by(id=gateway_id).first()

        if not exist_data:
            return Response(status=httplib.NOT_FOUND)

        payment_exist = PaymentGateway.query.filter_by(name=schema.get('name')).first()

        if payment_exist:
            resp = jsonify('Payment {} is exist'.format(payment_exist.name))
            resp.status_code = httplib.CONFLICT
        else:
            if schema.get('images', None) in (None, ""):
                pass
            else:
                new_files = upload_image_paymentgateway(schema.get('images'))
                schema['icon_image_normal'] = new_files
                schema['icon_image_highres'] = new_files
                del schema['images']

            for k, v in schema.iteritems():
                setattr(exist_data, k, v)

            db.session.add(exist_data)
            db.session.commit()
            resp = jsonify(exist_data.values())
            resp.status_code = httplib.CREATED

        return resp

    @token_required('can_read_write_global_all')
    def delete(self, gateway_id=None):
        f = PaymentGateway.query.filter_by(id=gateway_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=httplib.NOT_FOUND)


def get_response_body(payment_gateway):
    rv = payment_gateway.values()
    org = db.session.query(Organization).get(payment_gateway.organization_id) if payment_gateway.organization_id \
        else None
    rv['organization'] = {
        "id": org.id,
        "name": org.name,
        "href": org.api_url,
    } if org else None
    return json.dumps(rv)
