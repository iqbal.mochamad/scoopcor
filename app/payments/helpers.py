import json
import httplib
import datetime
from datetime import timedelta

from flask import Response, request, g
from flask import make_response
from six import string_types
from babel.dates import format_date, format_time

from app import app, db
from flask_appsfoundry.exceptions import BadRequest, Conflict, UnprocessableEntity, InternalServerError

from app.eperpus.models import CatalogItem
from app.helpers import payment_logs
from app.orders.choices import NEW
from app.orders.models import Order
from app.orders.order_complete import OrderCompleteConstruct
from app.orders.order_points import PointsPayment
from app.orders.helpers import order_status
from app.paymentgateway_connectors.apple_iap.v3.helpers import GatewayTimeout, AppleRequestError, AppleConnectionError
from app.paymentgateway_connectors.apple_iap.v3.payment_validation import apple_payment_receipt_validation
from app.paymentgateway_connectors.atm_transfer.charge import MidtransVirtualAccount
from app.paymentgateway_connectors.creditcard.charge import CreditCardCapture
from app.paymentgateway_connectors.gopay.charge import GopayAuthorize
from app.payments.choices.gateways import *
from app.payments.models import Payment, PaymentDetail
from app.paymentgateway_connectors.apple_iap import iap_status
from app.paymentgateway_connectors.paypal.models import PaymentPaypal
from app.paymentgateway_connectors.paypal.paypal import PaypalCheckout
from app.paymentgateway_connectors.apple_iap.apple_iap import AppleIAPCheckout
from app.paymentgateway_connectors.finnet195.finnet195 import Finnet195Checkout
from app.paymentgateway_connectors.inapbilling.billing import GoogleIABCheckout
from app.paymentgateway_connectors.mandiri_clickpay.mandiri_clickpay import MandiriClickpayCheckout
from app.paymentgateway_connectors.mandiri_ecash.mandiri_ecash import MandiriEcashCheckout
from app.paymentgateway_connectors.tcash.tcash import TcashCheckout
from app.paymentgateway_connectors.faspay.helpers import FaspayCheckout
from app.paymentgateway_connectors.faspay.faspay_config import FASPAY_CONFIG as fc
from app.paymentgateway_connectors.faspay.faspaypg.choices import payment_status as faspay_ps
from app.paymentgateway_connectors.faspay.faspaypg import response_status as faspay_rs
from app.users.users import User
from app.utils.datetimes import datetime_to_isoformat, WIB, get_utc_nieve, get_local
from app.messaging.email import SmtpServer, HtmlEmail, get_smtp_server
from app.services.kredivo.helpers import build_kredivo_entity
from app.services.kredivo.helpers import build_kredivo_gateway
from app.services.kredivo.kredivo_gateway import KredivoUnexpectedError, KredivoCheckoutError, KredivoTransactionStatus, \
    KredivoFraudStatus
from app.users.wallet import wallet_payment_transaction

from . import _log


TRANSLATION_TO_SC2 = 1
TRANSLATION_TO_SC1 = 2


class PaymentConstruct(object):

    def __init__(self, args=None):

        self.session = db.session()
        # payment and order will retrieve later
        self.payment = None
        self.order = None

        self.order_id = args.get('order_id', None)
        self.payment_code = args.get('payment_code', None)
        self.return_url = args.get('return_url', None)
        self.cancel_url = args.get('cancel_url', None)
        self.payment_id = args.get('payment_id', None)
        self.payer_id = args.get('payer_id', None)
        self.payment_gateway_id = args.get('payment_gateway_id', None)
        self.payment_status = None
        self.order_rslt = None
        self.order_number = None
        self.payment_datetime = datetime.datetime.utcnow()
        self.repurchased = args.get('repurchased', False)

        # Faspay
        self.faspay_trx_data = args.get('faspay_trx_data', None)
        self.faspay_payment_status = args.get('faspay_payment_status', None)


        # APPLE
        self.receipt_data = args.get('receipt_data', None)
        self.unified_receipt_data = args.get('unified_receipt_data', None)
        self.is_sandbox = args.get('is_sandbox', False)

        # INAPP BILLING REQUEST
        self.purchase_token = args.get('purchase_token', None)
        self.developer_payload = args.get('developer_payload', None)
        self.product_sku = args.get('product_sku', None)
        self.iab_order_id = args.get('iab_order_id', None)

        # Clickpay
        self.card_no = args.get('card_no', None)
        self.response_token = args.get('response_token', None)
        self.challenge_code3 = args.get('challenge_code3', None)

        # Ecash
        self.client_ip = args.get('client_ip', None)
        self.ecash_trx_id = args.get('ecash_trx_id', None)

        # Veritrans
        self.billing_address = args.get('billing_address', None)
        self.save_token = args.get('save_token', False)

        # Veritrans - Indomaret
        self.veritrans_cstore_status = args.get('veritrans_cstore_status', None)

        # Finnet
        self.finnet_result_code = args.get('finnet_result_code', None)

        # Tcash
        self.tcash_status_code = args.get('tcash_status_code', None)

        # kredivo
        self.transaction_id = args.get('transaction_id', None)
        self.transaction_status = args.get('transaction_status', None)
        self.fraud_status = args.get('fraud_status', None)
        self.signature_key = args.get('signature_key', None)

        self.args = args
        self.api = args.get('api', '')
        self.method = args.get('method', '')
        try:
            self.ip_address = request.remote_addr
        except Exception as e:
            self.ip_address = None

        self.free_not = []

        # Payment status constants
        self.WAITING_FOR_PAYMENT = 20001
        self.PAYMENT_IN_PROCESS = 20002
        self.PAYMENT_BILLED = 20003
        self.PAYMENT_RESTORED = 20004
        self.CANCELLED = 50000
        self.PAYMENT_ERROR = 50001

    def initialize_transaction(self):
        """
        Initializing transaction, fetching orders and checking free
        transaction. allocating flow transaction according to their payment
        gateway id
        """

        fetch_order_sts, order_rslt = self.fetch_order()

        if not fetch_order_sts:
            _log.debug("Order not valid.")
            return order_rslt
        else:
            self.order_rslt = order_rslt
            _log.debug("Order is valid.")

        free_check_sts, free_checkout_rslt = self.free_checkout_check(self.order_rslt)
        if not free_check_sts:
            _log.debug("Not a free checkout.  Bailing out.")
            return free_checkout_rslt

        # Free checkout will calling charge transaction no matter
        # what payment API called
        free_checkout = free_checkout_rslt
        if free_checkout:
            _log.debug("Charging free checkout")
            return self.charge_transaction()

        # Other than Free checkout, transaction flow will be directed
        # according to the payment type
        if self.api == 'v1/payments/authorize':
            return self.authorize_transaction()
        elif self.api in ['v1/payments/capture',
                          'v2/payments/capture',
                          ]:
            return self.capture_transaction()
        elif self.api == 'v1/payments/charge':
            return self.charge_transaction()
        elif self.api == 'v1/payments/validate':
            return self.validate_transaction()
        else:
            _log.debug("Unknown payment API requested: {}".format(self.api))
            return payment_logs(args=self.args, error_message="Invalid payment api")

    def authorize_transaction(self):
        try:
            json_form = {}

            payment_gateway_id = self.order_rslt['payment_gateway_id']
            amount = self.order_rslt['amount']
            currency = self.order_rslt['currency']
            items = self.order_rslt['items']
            user_id = self.order_rslt['user_id']
            order_number = self.order_rslt['order_number']
            amount = self.order_rslt['amount']

            return_url = self.return_url
            cancel_url = self.cancel_url
            order_id = self.order_id
            client_ip = self.client_ip
            payer_id = self.payer_id if self.payer_id else g.current_user.email if g.current_user else None
            faspay_trx_data = self.faspay_trx_data
            payment_channel = None

            if payment_gateway_id == FREE:
                return self.charge_transaction()

            # Paypal Transaction Processing #
            if payment_gateway_id == PAYPAL:
                payment_status, payment_rslt = self.save_to_payment(payment_status=self.WAITING_FOR_PAYMENT)
                if not payment_status:
                    return payment_rslt

                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.WAITING_FOR_PAYMENT)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                payment_id = payment_rslt

                paypal_transaction = {
                    "return_url": return_url,
                    "cancel_url": cancel_url,
                    "amount": amount,
                    "currency": currency,
                    "items": items,
                    "transaction_id": order_id,
                    "payment_id": payment_id,
                    "args": self.args
                }
                pc_status, pc_rslt = PaypalCheckout(**paypal_transaction).authorize()
                if not pc_status:
                    return pc_rslt

                json_form = {
                    "approval_url": pc_rslt['approval_url'],
                    "payment_id": payment_id
                }

            elif payment_gateway_id == KREDIVO:
                return self.authorize_kredivo_transaction(order_id, items, currency, amount)

            # Veritrans CStore - Indomaret Transaction Processing - submit order-payment to veritrans
            elif payment_gateway_id == INDOMARET:

                payment_code, response = self.authorize_indomaret(
                    order_id, self.billing_address, amount, items, payer_id)

                if payment_code:
                    json_form = response
                else:
                    # failed/error, return error response
                    return response

            # Finnet Transaction Processing #
            elif payment_gateway_id == FINNET:
                payment_status, payment_rslt = self.save_to_payment(payment_status=self.WAITING_FOR_PAYMENT)
                if not payment_status:
                    return payment_rslt

                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.WAITING_FOR_PAYMENT)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                payment_id = payment_rslt

                # NEED ATTENTION ON THIS PARAMETERS
                # self.payer_id = "rs@apps-foundry.com"  # Testing Purposes
                finnet_transaction = {
                    # "user_id": user_id,
                    "invoice": int(order_id),
                    "amount": int(amount),
                    "payer_id": payer_id,
                    "content_id": "99",
                    "code": str(payment_id),
                    "payment_id": payment_id
                }

                fc_status, fc_rslt = Finnet195Checkout(**finnet_transaction).create_new_transaction()

                if not fc_status:
                    return payment_logs(args=self.args, error_message=fc_rslt)

                authorize_send_email_finnet(amount, items, payment_code=fc_rslt,
                                            order_id=order_id, payer_id=payer_id)

                json_form = {
                    "finnet_payment_code": fc_rslt
                }

            # Mandiri Ecash Transaction Processing #
            elif payment_gateway_id == ECASH:

                payment_status, payment_rslt = self.save_to_payment(payment_status=self.WAITING_FOR_PAYMENT)
                if not payment_status:
                    return payment_rslt

                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.WAITING_FOR_PAYMENT)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                self.payment_id = payment_rslt

                ecash_transaction = {
                    "trx_amount": str(amount),
                    "client_ip": str(client_ip),
                    "transaction_id": str(order_id),
                    "payment_id": self.payment_id,
                    "return_url": return_url,
                    "args": self.args
                }
                et_status, et_rslt = MandiriEcashCheckout(**ecash_transaction).create_new_transaction()
                if not et_status:
                    return payment_logs(args=self.args, error_message=str(et_rslt))

                ecash_trx_id = et_rslt
                json_form = {
                    "ecash_trx_id": ecash_trx_id,
                    "payment_id": self.payment_id
                }

            elif payment_gateway_id == TCASH:
                payment_status, payment_rslt = self.save_to_payment(payment_status=self.WAITING_FOR_PAYMENT)
                if not payment_status:
                    return payment_rslt

                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.WAITING_FOR_PAYMENT)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                self.payment_id = payment_rslt

                tcash_transaction = {
                    "amount": str(amount),
                    "transaction_id": str(order_id),
                    "payment_id": self.payment_id
                }
                tt_status, tt_rslt = TcashCheckout(**tcash_transaction).create_new_transaction()
                if not tt_status:
                    return payment_logs(args=self.args, error_message=str(tt_rslt))

                json_form = {
                    "payment_id": self.payment_id,
                    "order_number": order_number,
                    "message": "Payment tcash authorized"
                }

                return Response(
                    json.dumps(json_form),
                    status=httplib.OK,
                    mimetype='application/json'
                )

            # InAPP Billing (GOOGLE) #
            elif payment_gateway_id == IAB:
                payment_status, payment_rslt = self.save_to_payment(payment_status=self.WAITING_FOR_PAYMENT)
                if not payment_status:
                    return payment_rslt

                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.WAITING_FOR_PAYMENT)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                payment_id = payment_rslt

                inapp_status, inapp_rslt = GoogleIABCheckout(
                    order_id=self.order_id,
                    args=self.args,
                    payment_id=payment_id
                ).google_transactions()

                if inapp_status:
                    return inapp_rslt
                else:
                    return payment_logs(args=self.args, error_message=str("Invalid payment gateway id"))

            # FASPAY
            elif payment_gateway_id in [KLIKBCA, BRIMOCASH, BRIEPAY, XL, CIMBCLICKS]:

                _log.debug("Charging Faspay Transaction "
                           "for channel: {}".format(payment_gateway_id))

                for payment in fc:
                    if payment.get('paymentgateway_id') == int(payment_gateway_id):
                        payment_channel = payment.get('channel')

                payment_status, payment_rslt = self.save_to_payment(payment_status=self.WAITING_FOR_PAYMENT)
                if not payment_status:
                    return payment_rslt

                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.WAITING_FOR_PAYMENT)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                self.payment_id = payment_rslt

                faspay_param = {
                    "order_id": order_id,
                    "customer": faspay_trx_data.get('customer', dict()),
                    "payment_channel": str(payment_channel),
                    "pay_type": str(faspay_trx_data['pay_type']),
                    "bank_userid": "",
                    "msisdn": "",
                    "email": str(faspay_trx_data.get('email', '')),
                    "terminal": str(faspay_trx_data['terminal']),
                    # "receiver_name_for_shipping": str(faspay_trx_data['receiver_name_for_shipping']),
                    "items": items,
                    "payment_id": self.payment_id,
                    "callback": app.config['BCA_CALLBACK']
                }

                faspay_transaction = FaspayCheckout(**faspay_param).authorize()
                if type(faspay_transaction) == dict:
                    faspay_transaction['payment_id'] = self.payment_id

                return Response(response=json.dumps(faspay_transaction), status=httplib.OK)

            elif payment_gateway_id in [VA_PERMATA, VA_BCA, VA_MANDIRI, VA_BNI]:
                return MidtransVirtualAccount(order_id).authorize()

            elif payment_gateway_id in [GOPAY]:
                return GopayAuthorize(order_id).authorize()

            else:
                _log.debug("Unknown payment gateway ID: {}.  Cannot authorize transaction.".format(payment_gateway_id))
                return payment_logs(args=self.args, error_message=str("Invalid payment gateway id"))

            _log.debug("Payment authorized successfully.")
            return Response(
                json.dumps(json_form),
                status=httplib.OK,
                mimetype='application/json'
            )
        except Exception as e:
            _log.exception("An unknown error occurred authorizing the payment.")
            return payment_logs(args=self.args, error_message=str(e))

    def authorize_indomaret(self, order_id, billing_address, amount, items,
                            payer_id):
        payment_status, payment_rslt = self.save_to_payment(payment_status=self.WAITING_FOR_PAYMENT)
        if not payment_status:
            return None, payment_rslt

        ord_status = order_status(order=self.order, session=self.session,
                                  order_id=order_id, order_status=self.WAITING_FOR_PAYMENT)
        if ord_status != 'OK':
            return None, payment_logs(args=self.args, error_message="Order id is not found")

        self.payment_id = payment_rslt

        formatted_billing_address = {
            "city": billing_address.get('city', '') if billing_address else '',
            "first_name": billing_address.get('first_name', '') if billing_address else payer_id,
            "last_name": billing_address.get('last_name', '') if billing_address else '',
            "postal_code": billing_address.get('postal_code', '') if billing_address else '12345',
            "phone": billing_address.get('phone', '') if billing_address else '1234567',
            "address1": billing_address.get('address1', '') if billing_address else '',
            "address2": billing_address.get('address2', '') if billing_address else '',
        }

        vc_transaction = {
            "token": None,
            "amount": amount,
            "items": items,
            "customer_email": payer_id,
            "payment_id": self.payment_id,
            "billing_address": formatted_billing_address,
            "order_id": order_id,
            "message": "Scoop Order Id {}".format(self.order_id),
            "payment_args": self.args
        }
        response_status, response = False, {}
        if response_status:
            # payment submitted to veritrans successfully, waiting customer pay to indomaret
            # return and email payment code
            payment_code = response.get('payment_code', None)

            # format expired date for email
            expired_date = format_date(response.get('expired_date'), format='full', locale='id') + \
                           ', ' + format_time(response.get('expired_date'), "HH:mm:ss")

            authorize_send_email_indomaret(amount, expired_date, items, payment_code, order_id, payer_id)

            response_json_form = {"payment_code": payment_code,
                                  "order_id": order_id,
                                  "payment_id": self.payment_id,
                                  "expired_date": datetime_to_isoformat(response.get("expired_date"))}
            return payment_code, response_json_form
        else:
            update_status = self.change_payment_status(status=self.PAYMENT_ERROR,
                                                       payment_datetime=self.payment_datetime)
            ord_status = order_status(order=self.order, session=self.session,
                                      order_id=order_id, order_status=self.PAYMENT_ERROR)
            return None, response

    def authorize_kredivo_transaction(self, order_id, items, currency, amount, return_url=None, cancel_url=None):
        payment_status, payment_rslt = self.save_to_payment(payment_status=self.WAITING_FOR_PAYMENT)
        if not payment_status:
            return payment_rslt

        ord_status = order_status(order=self.order, session=self.session,
                                  order_id=order_id, order_status=self.WAITING_FOR_PAYMENT)
        if ord_status != 'OK':
            return payment_logs(args=self.args, error_message="Order id is not found")

        payment_id = payment_rslt

        kredivo_entity = build_kredivo_entity(order_id, items, amount,
                                              back_to_store_uri=self.return_url)
        kredivo_gateway = build_kredivo_gateway()
        try:
            _log.info("Kredivo: data before request was: {}".format(json.dumps(kredivo_entity.serialize())))
            response = kredivo_gateway.checkout(kredivo_entity.serialize())
            _log.info("Kredivo: response was: {}".format(response))
            json_form = {
                "redirect_url": response.redirect_url,
                "status": response.status.value,
                "message": response.message,
                "payment_id": payment_id,
                "order_id": order_id
            }
            return make_response(
                json.dumps(json_form),
                httplib.OK,
                {'Content-type': 'application/json'}
            )
        except KredivoCheckoutError as e:
            _log.debug("Kredivo Checkout error: {}".format(str(e)))
            return payment_logs(args=self.args, error_message=str(e))
        except KredivoUnexpectedError as e:
            _log.debug("Kredivo Unexpected error: {}".format(str(e)))
            return payment_logs(args=self.args, error_message=str(e), status_code=e.status_code)
        except Exception as e:
            _log.debug("Kredivo - Unknown Error: {}".format(e))
            return payment_logs(args=self.args, error_message=str(e))

    def charge_transaction(self):
        try:
            payment_gateway_id = self.order_rslt['payment_gateway_id']
            user_id = self.order_rslt['user_id']
            order_number = self.order_rslt['order_number']
            order_id = self.order_id

            if payment_gateway_id not in (FREE, POINT, WALLET, DIRECT_PAYMENT):
                _log.debug("Unknown payment gateway id: {}.  Cannot charge transaction.".format(payment_gateway_id))
                return payment_logs(args=self.args, error_message="Invalid payment gateway id")

            # check and validate order
            check_new_transaction_status, check_new_transaction_rslt = self.check_new_transaction()
            if not check_new_transaction_status:
                return check_new_transaction_rslt

            self.create_payment(order_id, status=self.PAYMENT_IN_PROCESS)

            if payment_gateway_id == POINT:
                payment_status, payment_result = PointsPayment(
                    user_id=user_id,
                    payment_gateway_id=payment_gateway_id,
                    order_id=order_id,
                    order_number=order_number,
                    args=self.args
                ).construct()
                if payment_status != 'OK':
                    self.update_payment_order_status(order_id=order_id, status=self.PAYMENT_ERROR)
                    return payment_result

            elif payment_gateway_id == WALLET:
                # validate order vs wallet balance then save wallet transaction
                #    self.order_rslt['amount'] from float(order.final_amount)
                try:
                    wallet_payment_transaction(session=db.session(),
                                               order_id=order_id,
                                               order_amount=self.order_rslt['amount'],
                                               party_id=user_id)
                except (BadRequest, Conflict) as e:
                    self.update_payment_order_status(order_id=order_id, status=self.PAYMENT_ERROR)
                    raise e

            # for Free and Direct Payment, we don't to validate or process anything, just accept the payment

            # update payment status to billed
            # payment only, don't need to update order, order will be update via order complete construct
            self.update_payment_order_status(order_id=None, status=self.PAYMENT_BILLED)

            # update order to completed, and build return value for success payment response
            order_construct = OrderCompleteConstruct(
                user_id=user_id,
                payment_gateway_id=payment_gateway_id,
                order_id=order_id,
                order_number=order_number,
                catalog_id=self.args.get('catalog_id', None)
            ).construct()

            if order_construct.status_code not in [200, 201]:
                self.update_payment_order_status(order_id=None, status=self.PAYMENT_ERROR)
            return order_construct

        except Exception as e:
            _log.exception("An unknown error occurred charging the transaction")
            return payment_logs(args=self.args, error_message=str(e))

    def create_payment(self, order_id, status):
        payment_status, payment_rslt = self.save_to_payment(payment_status=status)
        if not payment_status:
            return payment_rslt
        ord_status = order_status(order=self.order, session=self.session, order_id=order_id, order_status=status)
        if ord_status != 'OK':
            return payment_logs(args=self.args, error_message="Order id is not found")
        self.payment_id = payment_rslt

    def update_payment_order_status(self, order_id, status):
        # self.update_payment_status(order_id, status=self.PAYMENT_ERROR)
        # self.update_payment_status(order_id, status=self.PAYMENT_BILLED)
        update_status, update_result = self.change_payment_status(status=status,
                                                                  payment_datetime=self.payment_datetime)
        if not update_status:
            return update_result

        if order_id:
            ord_status = order_status(order=self.order, session=self.session, order_id=order_id, order_status=status)
            if ord_status != 'OK':
                return payment_logs(args=self.args, error_message="Order id is not found")

    def capture_kredivo_confirmation(self, transaction_id, transaction_status, fraud_status, signature_key):

        merchant_params = {"transaction_id": transaction_id, "signature_key": signature_key,
                           "fraud_status": fraud_status, "transaction_status": transaction_status}
        transaction_status = transaction_status.upper()
        fraud_status = fraud_status.upper()
        if transaction_status == KredivoTransactionStatus.settlement.value \
                and fraud_status == KredivoFraudStatus.accept.value:

            # this payment status already re-validate/double checked in:
            #   CaptureKredivoPaymentApi.validate_kredivo_payment
            #       that calls this method
            #  here we only need to update the status of payment and order and finalize the order

            update_status, payment_log = self.change_payment_status(
                status=self.PAYMENT_BILLED,
                payment_datetime=get_utc_nieve(),
                merchant_params=merchant_params)
            if not update_status:
                return payment_log
            ord_status = order_status(order=self.order, session=self.session,
                                      order_id=self.order_id, order_status=self.PAYMENT_BILLED)
            if ord_status != 'OK':
                return payment_logs(args=self.args, error_message="Order id is not found")

            _log.debug("Kredivo Capture: payment billed")
            # complete final step: ex: deliver item to owned items, etc
            return self.execute_complete_checkout()

        elif transaction_status == KredivoTransactionStatus.pending.value:
            # status in request body != pending but confirmed status from kredivo still pending, do nothing
            return make_response(json.dumps({
                    "status": "OK",
                    "message": ""
                }), httplib.OK, {'Content-type': 'application/json'})
        elif transaction_status in [KredivoTransactionStatus.deny.value,
                                    KredivoTransactionStatus.cancel.value,
                                    KredivoTransactionStatus.expire.value] or (
                transaction_status == KredivoTransactionStatus.settlement.value \
                and fraud_status != KredivoFraudStatus.accept.value
        ):
            update_status, payment_log = self.change_payment_status(
                status=self.CANCELLED,
                payment_datetime=get_utc_nieve(),
                merchant_params=merchant_params)
            ord_status = order_status(order=self.order, session=self.session,
                                      order_id=self.order_id, order_status=self.CANCELLED)
            if ord_status != 'OK':
                return payment_logs(args=self.args, error_message="Order id is not found")
            # return payment_logs(args=self.args, error_message="Payment Cancelled or interrupted")
            return make_response(json.dumps({
                    "status": "OK",
                    "message": "Payment Cancelled or interrupted"
                }), httplib.OK, {'Content-type': 'application/json'})
        else:
            return payment_logs(args=self.args, error_message="Unknown transcation status for kredivo")

    def capture_transaction(self):
        try:
            payment_gateway_id = self.order_rslt['payment_gateway_id']
            items = self.order_rslt['items']
            amount = self.order_rslt['amount']
            order_id = self.order_id
            payer_id = self.payer_id
            card_no = self.card_no
            response_token = self.response_token

            if payment_gateway_id == KREDIVO:
                return self.capture_kredivo_confirmation(
                    self.transaction_id, self.transaction_status, self.fraud_status, self.signature_key)

            elif payment_gateway_id == CREDITCARD:
                if self.api == 'v1/payments/capture':
                    return payment_logs(args=self.args, error_message="Payment invalid, Please update your apps with the latest.")
                else:
                    return CreditCardCapture(master_order=self.order, billing_address=self.billing_address,
                        save_token=self.payment_code, args=self.args).capture()

            elif payment_gateway_id == PAYPAL:

                update_status = self.change_payment_status(status=self.PAYMENT_IN_PROCESS)
                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.PAYMENT_IN_PROCESS)

                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                pp = PaymentPaypal.query.filter_by(payment_id=self.payment_id).first()
                if pp is None:
                    return payment_logs(args=self.args, error_message="Payment_id not found")

                payment_code = pp.payment_code

                capture_status, capture_rslt = PaypalCheckout(
                    payment_code=payment_code,
                    payer_id=payer_id,
                    args=self.args
                ).capture()

                self.payment_datetime = capture_rslt['create_time']

                if not capture_status:
                    update_status = self.change_payment_status(
                        status=self.PAYMENT_ERROR, payment_datetime=self.payment_datetime)
                    ord_status = order_status(order=self.order, session=self.session,
                                              order_id=order_id, order_status=self.PAYMENT_ERROR)
                    if ord_status != 'OK':
                        return payment_logs(args=self.args, error_message="Order id is not found")
                    return capture_rslt

                update_status = self.change_payment_status(
                    status=self.PAYMENT_BILLED, payment_datetime=self.payment_datetime)
                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.PAYMENT_BILLED)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                return self.execute_complete_checkout()

            elif payment_gateway_id == CLICKPAY:

                if card_no is None or response_token is None:
                    return payment_logs(args=self.args, error_message="Card number or token response is None")

                payment_status, payment_rslt = self.save_to_payment(payment_status=self.PAYMENT_IN_PROCESS)
                if not payment_status:
                    return payment_rslt

                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.PAYMENT_IN_PROCESS)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                self.payment_id = payment_rslt
                self.payment_datetime_format = "{0:%Y%m%d%H%M%S}".format(self.payment_datetime)

                # Begin processing to clickpay and saving transaction in clickpay table #
                clickpay_transaction = {
                    "card_no": str(card_no),
                    "amount": amount,
                    "transaction_id": int(order_id),
                    "invoice_no": int(order_id),
                    "items": items,
                    "token_response": str(response_token),
                    "payment_id": self.payment_id,
                    "challenge_code3": self.challenge_code3,
                    "purchase_date": self.payment_datetime_format,
                    "args": self.args
                }
                transaction_status, transaction_rslt = MandiriClickpayCheckout(**clickpay_transaction).create_new_transaction()

                if transaction_status:
                    update_status = self.change_payment_status(
                        status=self.PAYMENT_BILLED, payment_datetime=self.payment_datetime)
                    ord_status = order_status(order=self.order, session=self.session,
                                              order_id=order_id, order_status=self.PAYMENT_BILLED)
                    if ord_status != 'OK':
                        return payment_logs(args=self.args, error_message="Order id is not found")
                    return self.execute_complete_checkout()
                else:
                    update_status = self.change_payment_status(
                        status=self.PAYMENT_ERROR, payment_datetime=self.payment_datetime)
                    ord_status = order_status(order=self.order, session=self.session,
                                              order_id=order_id, order_status=self.PAYMENT_ERROR)

                    if ord_status != 'OK':
                        return payment_logs(args=self.args, error_message="Order id is not found")

                    return payment_logs(args=self.args, error_message=str(transaction_rslt))

            else:
                return payment_logs(args=self.args, error_message=str("Invalid payment gateway id"))

        except Exception as e:
            return False, payment_logs(args=self.args, error_message=str(e))

    def validate_transaction(self):
        try:
            payment_gateway_id = self.order_rslt['payment_gateway_id']
            amount = self.order_rslt['amount']
            currency = self.order_rslt['currency']
            items = self.order_rslt['items']
            user_id = self.order_rslt['user_id']
            order_number = self.order_rslt['order_number']
            amount = self.order_rslt['amount']

            return_url = self.return_url
            cancel_url = self.cancel_url
            order_id = self.order_id
            client_ip = self.client_ip
            ecash_trx_id = self.ecash_trx_id
            is_sandbox = self.is_sandbox

            if payment_gateway_id == INDOMARET:

                return self.validate_indomaret()

            elif payment_gateway_id == FINNET:
                # -=================== FINNET PAYMENT
                update_status = self.change_payment_status(status=self.PAYMENT_IN_PROCESS)
                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.PAYMENT_IN_PROCESS)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                if self.finnet_result_code == '00':
                    update_status = self.change_payment_status(
                        status=self.PAYMENT_BILLED, payment_datetime=self.payment_datetime)
                    ord_status = order_status(order=self.order, session=self.session,
                                              order_id=order_id, order_status=self.PAYMENT_BILLED)
                    if ord_status != 'OK':
                        return payment_logs(args=self.args, error_message="Order id is not found")

                    return self.execute_complete_checkout()
                else:
                    update_status = self.change_payment_status(
                        status=self.PAYMENT_ERROR, payment_datetime=self.payment_datetime)
                    ord_status = order_status(order=self.order, session=self.session,
                                              order_id=order_id, order_status=self.PAYMENT_ERROR)
                    if ord_status != 'OK':
                        return payment_logs(args=self.args, error_message="Order id is not found")
                    return payment_logs(args=self.args, error_message="Payment Cancelled or interrupted")

            # -=================== TCASH PAYMENT
            if payment_gateway_id == TCASH:

                update_status = self.change_payment_status(status=self.PAYMENT_IN_PROCESS)
                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.PAYMENT_IN_PROCESS)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                if self.tcash_status_code == "00":
                    update_status = self.change_payment_status(
                        status=self.PAYMENT_BILLED, payment_datetime=self.payment_datetime)
                    ord_status = order_status(order=self.order, session=self.session,
                                              order_id=order_id, order_status=self.PAYMENT_BILLED)
                    if ord_status != 'OK':
                        return payment_logs(args=self.args, error_message="Order id is not found")

                    return self.execute_complete_checkout()
                else:
                    update_status = self.change_payment_status(
                        status=self.PAYMENT_ERROR, payment_datetime=self.payment_datetime)
                    ord_status = order_status(order=self.order, session=self.session,
                                              order_id=order_id, order_status=self.PAYMENT_ERROR)
                    if ord_status != 'OK':
                        return payment_logs(args=self.args, error_message="Order id is not found")
                    return payment_logs(args=self.args, error_message="Payment Cancelled or interrupted")

            # -=================== ECASH PAYMENT
            if payment_gateway_id == ECASH:
                update_status = self.change_payment_status(status=self.PAYMENT_IN_PROCESS)
                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.PAYMENT_IN_PROCESS)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                capture_status, capture_rslt, trx_status = MandiriEcashCheckout(
                    ecash_trx_id=ecash_trx_id,
                    payment_id=self.payment_id
                ).validate_transaction()

                if not capture_status:
                    update_status = self.change_payment_status(
                        status=self.PAYMENT_ERROR, payment_datetime=self.payment_datetime)
                    ord_status = order_status(order=self.order, session=self.session,
                                              order_id=order_id, order_status=self.PAYMENT_ERROR)
                    if ord_status != 'OK':
                        return payment_logs(args=self.args, error_message="Order id is not found")
                    return capture_rslt

                if str(trx_status) != "SUCCESS":
                    update_status = self.change_payment_status(
                        status=self.PAYMENT_ERROR, payment_datetime=self.payment_datetime)
                    ord_status = order_status(order=self.order, session=self.session,
                                              order_id=order_id, order_status=self.PAYMENT_ERROR)
                    if ord_status != 'OK':
                        return payment_logs(args=self.args, error_message="Order id is not found")
                    return payment_logs(args=self.args, error_message="Payment Cancelled or interrupted")

                update_status = self.change_payment_status(
                    status=self.PAYMENT_BILLED, payment_datetime=self.payment_datetime)
                ord_status = order_status(order=self.order, session=self.session,
                                          order_id=order_id, order_status=self.PAYMENT_BILLED)
                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                return self.execute_complete_checkout()

            # -=================== APPLE PAYMENT
            if payment_gateway_id == APPLE:

                return self.validate_apple(order_id, user_id)

            # -=================== INAPP BILLING PAYMENT
            if payment_gateway_id == IAB:

                if self.payment_id is None:
                    return payment_logs(args=self.args, error_message=str("Invalid payment_id"))

                self.change_payment_status(status=self.PAYMENT_IN_PROCESS)

                ord_status = order_status(order=self.order, session=self.session,
                        order_id=order_id, order_status=self.PAYMENT_IN_PROCESS)

                if ord_status != 'OK':
                    return payment_logs(args=self.args, error_message="Order id is not found")

                iab_status, iab_rslt = GoogleIABCheckout(
                    order_id=self.order_id,
                    args=self.args,
                    purchase_token=self.purchase_token,
                    developer_payload=self.developer_payload,
                    product_sku=self.product_sku,
                    iab_order_id=self.iab_order_id
                ).google_validate()
                print '-------------------------------', iab_status, iab_rslt

                if not iab_status:
                    self.change_payment_status(status=self.PAYMENT_ERROR, payment_datetime=self.payment_datetime)
                    order_status(order=self.order, session=self.session, order_id=order_id, order_status=self.PAYMENT_ERROR)
                    return payment_logs(args=self.args, error_message=iab_rslt)
                else:
                    # all is well
                    self.change_payment_status(status=self.PAYMENT_BILLED, payment_datetime=self.payment_datetime)
                    order_status(order=self.order, session=self.session, order_id=order_id, order_status=self.PAYMENT_BILLED)
                    return self.execute_complete_checkout()


            # -=================== FASPAY
            if payment_gateway_id in [KLIKBCA, BRIMOCASH, BRIEPAY, XL, CIMBCLICKS]:

                if self.payment_id is None:
                    return payment_logs(
                        args=self.args,
                        error_message=str("Invalid payment_id"))

                self.change_payment_status(status=self.PAYMENT_IN_PROCESS)
                ord_status = order_status(
                    order=self.order, session=self.session,
                    order_id=order_id,
                    order_status=self.PAYMENT_IN_PROCESS)

                if ord_status != 'OK':
                    return payment_logs(
                        args=self.args,
                        error_message="Order id is not found")

                response_code, payment_stts, payment_date = FaspayCheckout(
                    order_id=order_id,
                    order_number=order_number,
                    payment_id=self.payment_id,
                    paymentgateway_id=payment_gateway_id
                ).validate()


                # HACK: Faspay's data comes to us in WIB (+7 GMT), not in UTC
                # Unfortunately, we don't actually convert this to a datetime
                # in our code (as far as I can tell) so we have to do that first
                # Response format it: 2015-08-20 14:46:12
                # Then we can convert to UTC
                if isinstance(payment_date, string_types):
                    payment_date = datetime.datetime.strptime(
                        payment_date, "%Y-%m-%d %H:%M:%S")

                payment_date -= timedelta(hours=7)

                if response_code == faspay_rs.PAYMENT_SUCCESS and \
                        payment_stts == faspay_ps.PAYMENT_SUCCESS:
                    self.change_payment_status(status=self.PAYMENT_BILLED, payment_datetime=self.payment_datetime)
                    ord_status = order_status(
                        order=self.order, session=self.session,
                        order_id=order_id,
                        order_status=self.PAYMENT_BILLED)

                    if ord_status != 'OK':
                        return payment_logs(
                            args=self.args,
                            error_message="Order id is not found")

                    return self.execute_complete_checkout()
                else:
                    self.change_payment_status(status=self.PAYMENT_ERROR, payment_datetime=self.payment_datetime)
                    ord_status = order_status(
                        order=self.order, session=self.session,
                        order_id=order_id,
                        order_status=self.PAYMENT_ERROR)

                    if ord_status != 'OK':
                        return payment_logs(
                            args=self.args,
                            error_message="Order id is not found")

                    return payment_logs(
                        args=self.args,
                        error_message="Payment Failed. response code: {} payment status: {}".format(
                            response_code, payment_stts))

            return payment_logs(
                args=self.args,
                error_message=str("Invalid payment gateway id"))

        except Exception as e:
            return payment_logs(args=self.args, error_message=str(e))

    def validate_apple(self, order_id, user_id):

        if not self.receipt_data and not self.unified_receipt_data:
            return payment_logs(args=self.args, error_message="Receipt data is empty!")

        payment_status, payment_rslt = self.save_to_payment(
            payment_status=self.WAITING_FOR_PAYMENT,
            merchant_params={
                "receipt_data": self.receipt_data,
                "unified_receipt_data": '{}...'.format(
                    self.unified_receipt_data[0:100]) if self.unified_receipt_data else None,
            })
        if not payment_status:
            return payment_rslt

        ord_status = order_status(order=self.order, session=self.session,
                                  order_id=order_id, order_status=self.WAITING_FOR_PAYMENT)
        if ord_status != 'OK':
            return payment_logs(args=self.args, error_message="Order id is not found")

        self.payment_id = payment_rslt

        if self.receipt_data:
            # use old receipt data format (IOS6-)
            return self.validate_apple_single_receipt(self.receipt_data, order_id, user_id)
        elif self.unified_receipt_data:
            # validate new IOS7+ Unified Receipt Data
            return self.validate_apple_unified_receipt(
                self.unified_receipt_data, user_id, self.order, self.payment)

    def validate_apple_unified_receipt(self, unified_receipt_data,
                                       user_id, order, payment):
        """ validate apple payment with new IOS7+ Unified Receipt Data

        :param unified_receipt_data:
        :param user_id:
        :param order:
        :param payment:
        :return:
        """
        #
        order_id = order.id
        try:

            # validate order vs unified receipt data
            validation_result = apple_payment_receipt_validation(
                unified_receipt_data, user_id, order, payment)

            # if no error raised, validation success, update payment and order status
            if validation_result.get("is_restore", False):
                status = self.PAYMENT_RESTORED
            else:
                status = self.PAYMENT_BILLED

            self.change_payment_status(
                status=status, payment_datetime=validation_result['purchase_date'],
                is_trial=validation_result.get("is_trial", False),
                is_test_payment=validation_result.get("is_sandbox", False)
            )
            order_status(order=self.order, session=self.session,
                         order_id=order_id, order_status=status)

            # finalized order, created user owned item/subscriptions etc
            return self.execute_complete_checkout(
                original_transaction_id=validation_result.get('original_transaction_id', None),
                expires_date=validation_result.get('expires_date', None),
                is_trial=validation_result.get('is_trial', False),
                is_restore=validation_result.get('is_restore', False),
                repurchased=self.repurchased
            )
        except (UnprocessableEntity, GatewayTimeout, AppleRequestError, AppleConnectionError, InternalServerError) as e:
            self.change_payment_status(status=self.PAYMENT_ERROR)
            order_status(order=self.order, session=self.session,
                         order_id=order_id, order_status=self.PAYMENT_ERROR)
            return payment_logs(
                args=self.args,
                status_code=e.code,
                error_message=e.developer_message,
                error_code=e.error_code,
                user_message=e.user_message)

    def validate_apple_single_receipt(self, receipt_data, order_id, user_id):
        """ validate apple payment with old receipt data format (IOS6-)

        :param receipt_data: old receipt data (from IOS6-)
        :param order_id:
        :param user_id:
        :return:
        """
        va_status, va_rslt = AppleIAPCheckout(
            receipt_data=receipt_data,
            payment_id=self.payment_id,
            order_id=order_id,
            args=self.args,
            user_id=user_id
        ).verify_payment_iap()

        if not va_status:
            update_status = self.change_payment_status(status=self.PAYMENT_ERROR)
            ord_status = order_status(order=self.order, session=self.session,
                                      order_id=order_id, order_status=self.PAYMENT_ERROR)
            if ord_status != 'OK':
                return payment_logs(args=self.args, error_message="Order id is not found")
            return payment_logs(**va_rslt)

        if va_rslt['is_sandbox']:
            self.update_payment_to_sandbox()

        if va_rslt.get('is_trial', False):
            self.update_payment_to_trial()

        if va_rslt['status_code'] == 0:
            translation_params = {
                'user_id': user_id,
                'translation_type': TRANSLATION_TO_SC1,
                'order_id': order_id,
                'receipt': va_rslt['receipt']
            }

            # TEMPORARY DISABLE THIS TRANSLATIONS, WAITING FOR BENNY TO FIX SC1
            # try:
            #    apple_trx_translator = AppleIAPTranslation(**translation_params).translate()
            # except Exception as e:
            #    content = "Payment IAP translation to SC1 failed for order_id: %s, Error : %s" % (str(order_id), e)
            #    subject = "%s IAP Payment translation to SC1 failed" % app.config['ADMIN_SERVER']
            #
            #    try:
            #        sendmail_aws(subject, 'no-reply@gramedia.com', ['ah@apps-foundry.com','bg@apps-foundry.com'], content)
            #    except Exception as e:
            #        try:
            #            sendmail_smtp(subject, 'no-reply@gramedia.com', ['ah@apps-foundry.com','bg@apps-foundry.com'], content)
            #        except Exception as e:
            #            pass
            #    pass

            update_status = self.change_payment_status(
                status=self.PAYMENT_BILLED, payment_datetime=va_rslt['purchase_date'])
            ord_status = order_status(order=self.order, session=self.session,
                                      order_id=order_id, order_status=self.PAYMENT_BILLED)
            if ord_status != 'OK':
                return payment_logs(args=self.args, error_message="Order id is not found")
            return self.execute_complete_checkout(
                original_transaction_id=va_rslt.get('original_transaction_id', None),
                expires_date=va_rslt.get('expires_date', None),
                is_trial=va_rslt.get('is_trial', False)
            )

        if va_rslt['status_code'] == self.PAYMENT_RESTORED:
            update_status = self.change_payment_status(
                status=self.PAYMENT_RESTORED, payment_datetime=va_rslt['purchase_date'])
            ord_status = order_status(order=self.order, session=self.session,
                                      order_id=order_id, order_status=self.PAYMENT_RESTORED)
            if ord_status != 'OK':
                return payment_logs(args=self.args, error_message="Order id is not found")
            return self.execute_complete_checkout(
                original_transaction_id=va_rslt.get('original_transaction_id', None),
                expires_date=va_rslt.get('expires_date', None),
                is_trial=va_rslt.get('is_trial', False)
            )

        elif va_rslt['status_code'] in iap_status.UNSUCCESSFUL_CODE_LIST:
            update_status = self.change_payment_status(status=self.PAYMENT_ERROR)
            ord_status = order_status(order=self.order, session=self.session,
                                      order_id=order_id, order_status=self.PAYMENT_ERROR)
            if ord_status != 'OK':
                return payment_logs(args=self.args, error_message="Order id is not found")

            return payment_logs(
                args=self.args,
                error_message=va_rslt['error_message'],
                error_code=va_rslt['error_code'],
                user_message=va_rslt['user_message'])

    def validate_indomaret(self):
        confirmed_payment_status = self.veritrans_cstore_status

        if confirmed_payment_status == 'settlement':

            # this payment status already re-validate/double checked in:
            #   ValidateIndomaretPaymentApi.validate_indomaret_payment
            #       that calls this method
            #  here we only need to update the status of payment and order

            update_status, payment_log = self.change_payment_status(
                status=self.PAYMENT_BILLED,
                payment_datetime=get_utc_nieve())
            if not update_status:
                return payment_log
            ord_status = order_status(order=self.order, session=self.session,
                                      order_id=self.order_id, order_status=self.PAYMENT_BILLED)
            if ord_status != 'OK':
                return payment_logs(args=self.args, error_message="Order id is not found")

            return self.execute_complete_checkout()

        elif confirmed_payment_status == 'pending':
            # status in request body != pending but confirmed status still pending, do nothing
            return make_response('', httplib.OK, {'Content-type': 'application/json'})
        elif confirmed_payment_status in ['expire', 'cancel']:
            # cancelled, failed
            update_status, payment_log = self.change_payment_status(
                status=self.CANCELLED,
                payment_datetime=get_utc_nieve())
            ord_status = order_status(order=self.order, session=self.session,
                                      order_id=self.order_id, order_status=self.CANCELLED)
            if ord_status != 'OK':
                return payment_logs(args=self.args, error_message="Order id is not found")
            else:
                return make_response('', httplib.OK, {'Content-type': 'application/json'})
        else:
            return payment_logs(args=self.args, error_message="Invalid payment status")

    def save_to_payment(self, payment_status=None, payment_datetime=None, merchant_params=None):

        _log.debug("Payment for Order {order_id}, payment gateway id {pg_id}. "
                   "Create/Update Payment with status: {status}".format(
                    order_id=self.order_id, pg_id=self.order_rslt['payment_gateway_id'], status=payment_status))

        payment_gateway_id = self.order_rslt['payment_gateway_id']
        currency = self.order_rslt['currency']
        user_id = self.order_rslt['user_id']
        amount = self.order_rslt['amount']
        order_id = self.order_id

        self.payment = self.session.query(Payment).filter_by(
            order_id=order_id, paymentgateway_id=payment_gateway_id
        ).first()
        if not self.payment:
            self.payment = Payment(paymentgateway_id=payment_gateway_id, order_id=order_id)

        self.payment.user_id = user_id
        self.payment.is_test_payment = self.is_sandbox
        if self.payment.payment_status != self.PAYMENT_BILLED:
            self.payment.payment_status = payment_status
        self.payment.amount = amount
        self.payment.currency_code = currency

        if merchant_params:
            self.payment.merchant_params = merchant_params

        if payment_datetime and self.payment.payment_datetime != payment_datetime:
            self.payment.payment_datetime = payment_datetime

        sv = self.payment.save(self.session)

        self.payment_id = self.payment.id

        if sv.get('invalid', False):
            return False, payment_logs(args=self.args, error_message=sv.get('message', ''))

        if self.ip_address is None:
            self.ip_address = ''

        pd = self.session.query(PaymentDetail).filter_by(payment_id=self.payment_id).first()
        if not pd:
            payment_det = {
                "payment_id": self.payment_id,
                "ip_address": self.ip_address
            }

            pd = PaymentDetail(**payment_det)

            spd = pd.save(self.session)

            if spd.get('invalid', False):
                return False, payment_logs(args=self.args, error_message=sv.get('message', ''))

        return True, self.payment_id

    def execute_complete_checkout(self, original_transaction_id=None,
            expires_date=None, is_trial=None, is_restore=None, repurchased=False):

        payment_gateway_id = self.order_rslt['payment_gateway_id']
        amount = self.order_rslt['amount']
        currency = self.order_rslt['currency']
        user_id = self.order_rslt['user_id']
        amount = self.order_rslt['amount']
        order_id = self.order_id
        order_number = self.order_rslt['order_number']

        order_complete_params = dict()
        order_complete_params['user_id'] = user_id
        order_complete_params['payment_gateway_id'] = payment_gateway_id
        order_complete_params['order_id'] = order_id
        order_complete_params['order_number'] = order_number
        order_complete_params['is_trial'] = is_trial
        order_complete_params['repurchased'] = repurchased

        if payment_gateway_id == APPLE:
            order_complete_params['original_transaction_id'] = original_transaction_id
            order_complete_params['expires_date'] = expires_date
            order_complete_params['is_restore'] = is_restore

        oc = OrderCompleteConstruct(**order_complete_params).construct()

        return oc

    def change_payment_status(self, status=None, payment_datetime=None, merchant_params=None,
                              is_trial=None, is_test_payment=None):
        _log.debug("Attempting to change payment status.")
        try:
            if not self.payment:
                self.payment = self.session.query(Payment).get(self.payment_id)

            if self.payment:
                # if status in (self.PAYMENT_BILLED, self.PAYMENT_ERROR):
                #    payment.payment_datetime = self.payment_datetime
                if self.payment.payment_status != self.PAYMENT_BILLED:
                    self.payment.payment_status = status
                if payment_datetime:
                    self.payment.payment_datetime = payment_datetime
                if merchant_params:
                    self.payment.merchant_params = merchant_params

                if is_trial is not None:
                    self.payment.is_trial = is_trial
                if is_test_payment is not None:
                    self.payment.is_test_payment = is_test_payment

                self.payment.save()
            else:
                return False, payment_logs(args=self.args, error_message="payment_id not found")
            return True, 'OK'

        except Exception as e:
            _log.exception("An unknown error occured changing payment status")
            return False, payment_logs(args=self.args, error_message=str(e))

    def save_payment_date(self, payment_date=None):
        _log.debug("Attempting to save payment date.")
        try:
            if not self.payment:
                self.payment = self.session.query(Payment).get(self.payment_id)

            if self.payment:
                self.payment.payment_datetime = payment_date
                self.payment.save(self.session)
            else:
                return False, payment_logs(args=self.args, error_message="payment_id not found")
            return True, 'OK'

        except Exception as e:
            _log.debug("An unknown error occured saving payment date")
            return False, payment_logs(args=self.args, error_message=str(e))

    def fetch_order(self):
        _log.debug("Attempting to fetch order")
        self.order = self.session.query(Order).get(self.order_id)

        if self.order is None:
            return False, payment_logs(args=self.args, error_message='Order not found')
        else:
            # check order data valid or not with request
            if self.order.paymentgateway_id != self.payment_gateway_id:
                _log.info("The Order's paymentgateway {}, "
                          "does not match the request's gateway id: {}".format(
                                self.order.paymentgateway_id, self.payment_gateway_id))
                return False, payment_logs(args=self.args, error_message='Payment gateway_id Invalid')

            if self.order.order_status in [90000, 20003]:  # already purchased
                _log.info("Could not fetch order: Already purchased")
                return False, payment_logs(args=self.args, error_message='Payment Status Invalid (Not NEW)')

        if not self.payer_id:
            # if payer email is empty,
            # get email from the user in order
            user = self.session.query(User).get(self.order.user_id)
            self.payer_id = user.email if user else g.current_user.email

        orderline = self.order.get_order_line()
        items = []
        item_name = []
        for i in orderline:
            item = {}
            if self.payment_gateway_id == PAYPAL:
                item['price'] = "%.2f" % (i.final_price)
            else:
                item['price'] = float(i.final_price)
            if self.payment_gateway_id == KREDIVO:
                # existing code for paypal processing will error if items included offer_id, per 14 sep 2016
                # don't know for another payment gateways,
                # since as of now it's only need in kredivo, and in order to prevent error in other payment gateways
                #  so it only assigned here
                item['offer_id'] = i.offer_id
            item['currency'] = i.currency_code
            item['quantity'] = i.quantity
            # item_name = re.sub(r'[^\w]', '', i.name)
            # item_name = i.name
            item['name'] = i.name  # item_name.encode('utf-8')
            item['is_free'] = i.is_free
            items.append(item)

        json_form = {
            "payment_gateway_id": self.order.paymentgateway_id,
            "amount": float(self.order.final_amount),
            "currency": self.order.currency_code,
            "items": items,
            "user_id": self.order.user_id,
            "order_number": self.order.order_number
        }

        return True, json_form

    def free_checkout_check(self, order_container=None):
        # checking free checkout
        _log.debug("Checking for free checkout.")
        try:
            for free_item in order_container['items']:
                is_free = free_item.get('is_free', None)
                self.free_not.append(is_free)

            if False not in self.free_not:
                free_checkout = True

            if False in self.free_not:
                free_checkout = False

            return True, free_checkout

        except Exception as e:
            _log.exception("An unknown error occurred when checking for free checkout.")
            return False, payment_logs(args=self.args, error_message=str(e))

    def check_new_transaction(self):
        _log.debug("Checking new transaction")
        try:
            if not self.order:
                self.order = self.session.query(Order).get(self.order_id)

            if (not self.order or self.order.user_id != self.order_rslt['user_id'] or
                    self.order.paymentgateway_id != self.payment_gateway_id or
                    self.order.order_status != NEW):
                _log.info("This transaction cannot be processed. Already paid or invalid payment status.")
                return False, payment_logs(
                    args=self.args,
                    error_message="This transaction cannot be processed. Already paid or invalid payment status."
                )

            return True, 'OK'
        except Exception as e:
            _log.exception("An unknown error occured checking new transaction.")
            return False, payment_logs(args=self.args, error_message=str(e))

    def update_payment_to_sandbox(self):
        if not self.payment:
            self.payment = self.session.query(Payment).get(self.payment_id)

        if self.payment:
            self.payment.is_test_payment = True
            self.payment.save(self.session)
        else:
            _log.debug("Error updating payment status to sandbox. "
                       "Payment ID was not found (I'm gonna throw an exception now, ok?)")
            raise Exception("payment_id not found")
        return True

    def update_payment_to_trial(self):
        if not self.payment:
            self.payment = self.session.query(Payment).get(self.payment_id)

        if self.payment:
            self.payment.is_trial = True
            self.payment.save(self.session)
        else:
            _log.debug("Error updating payment status to sandbox. "
                       "Payment ID was not found (I'm gonna throw an exception now, ok?)")
            raise Exception("payment_id not found")
        return True


def authorize_send_email_finnet(amount, items, payment_code, order_id, payer_id):
    try:
        # format expired date for email
        expired_date = get_local() + timedelta(days=1)
        expired_date = format_date(expired_date, format='full', locale='id') + \
                       ', ' + format_time(expired_date, "HH:mm:ss")

        server = get_smtp_server()
        msg = HtmlEmail(
            'Gramedia Digital <no-reply@gramedia.com>',
            [payer_id, ],
            'Gramedia Digital: Finnet ATM Transfer Payment Code',
            'email/atm_transfer/payment_flow.html',
            'email/atm_transfer/payment_flow.txt',
            payment_code=payment_code,
            amount=amount,
            items=items,
            expired_date=expired_date[:-3])

        server.send_message(msg)

    except Exception as e:
        _log.exception("Failed to send payment notification."
                       "order id {order_id}, payment code {payment_code}, to {payer_id}. "
                       "Error: {error_message}".format(
                            order_id=order_id, payment_code=payment_code,
                            error_message=e.message, payer_id=payer_id))


def authorize_send_email_indomaret(amount, expired_date, items, payment_code, order_id, payer_id):
    try:

        server = get_smtp_server()
        msg = HtmlEmail(
            'Gramedia Digital <no-reply@gramedia.com>',
            [payer_id, ],
            'Gramedia Digital - Pembayaran Indomaret',
            'email/indomaret/payment_flow.html',
            'email/indomaret/payment_flow.txt',
            payment_code=payment_code,
            amount=amount,
            items=items,
            expired_date=expired_date[:-3])

        server.send_message(msg)

    except Exception as e:
        _log.exception("Failed to send payment notification."
                       "order id {order_id}, payment code {payment_code}, to {payer_id}. "
                       "Error: {error_message}".format(
            order_id=order_id, payment_code=payment_code,
            error_message=e.message, payer_id=payer_id))

