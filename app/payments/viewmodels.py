from datetime import timedelta
from app.paymentgateway_connectors.atm_transfer.models import PaymentVirtualAccount
from .choices import gateways

class PaymentPendingViewModel(object):
    def __init__(self, pending_transactions, total, offset, limit):
        self.pending_transactions = [
            {
                'payment_id': transaction.id,
                'payment_code': self.get_payment_code(transaction),
                'order_id': transaction.order_id,
                'payment_status': transaction.payment_status,
                'payment_gateway_id': transaction.paymentgateway_id,
                'transaction_date': (transaction.created + timedelta(hours=7)).isoformat(), #set to WIB
                'expired_date': self.get_expired_date(transaction)
            }
            for transaction in pending_transactions]
        self.metadata = { 'resultset' : {'count': total, 'offset': offset, 'limit': limit }}

    @staticmethod
    def get_virtualaccount_number(order_id):
        virtual_account = PaymentVirtualAccount.query.filter_by(order_id=order_id).first()
        return virtual_account.virtual_number if virtual_account else None

    def get_payment_code(self, transaction):
        if transaction.paymentgateway_id in (gateways.VA_BCA, gateways.VA_BNI, gateways.VA_MANDIRI, gateways.VA_PERMATA):
            return self.get_virtualaccount_number(transaction.order_id)
        else:
            return None

    def get_expired_date(self, transaction):
        VA_EXPIRED = 24 #24 hours.
        GOPAY_EXPIRED = 15 #15 minutes
        CIMB_EXPIRED = 24 #24 hours, prepare for CIMB Rekening Ponsel.

        if transaction.paymentgateway_id in (gateways.VA_BCA, gateways.VA_BNI, gateways.VA_MANDIRI, gateways.VA_PERMATA):
            return ((transaction.created + timedelta(hours=VA_EXPIRED) + timedelta(hours=7)).isoformat())

        if transaction.paymentgateway_id in (gateways.GOPAY):
            return ((transaction.created + timedelta(minutes=GOPAY_EXPIRED) + timedelta(hours=7)).isoformat())

