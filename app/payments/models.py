from functools import partial

from enum import IntEnum
from app.logs.helpers import record_primary_entity_inserted, \
    record_primary_entity_updated
from flask_appsfoundry.models import BaseDatamodelMixin, TimestampMixin
from sqlalchemy import Column, String, Integer, DateTime, Boolean, event
from sqlalchemy.dialects import postgresql

from app import db
from app.payment_gateways.models import PaymentGateway
from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP

from . import _log
from .choices import PAYMENT_GATEWAYS


class Payment(TimestampMixin, db.Model):
    __tablename__ = 'core_payments'

    class Status(IntEnum):
        waiting = 20001
        in_process = 20002
        billed = 20003
        restored = 20004
        cancelled = 50000
        error = 50001

    order_id = db.Column(db.Integer, db.ForeignKey('core_orders.id'))
    order = db.relationship("Order", backref=db.backref('payments', lazy='dynamic'))

    # todo: this should be dropped.  user is an attribute of ORDER
    user_id = db.Column(db.Integer)

    paymentgateway_id = db.Column(db.Integer, db.ForeignKey('core_paymentgateways.id'))
    paymentgateway = db.relationship("PaymentGateway")

    currency_code = db.Column(db.String(3))
    amount = db.Column(db.Numeric(12, 2))
    payment_status = db.Column(db.Integer)
    is_active = db.Column(db.Boolean(), default=True)
    is_test_payment = db.Column(db.Boolean(), default=False)
    is_trial = db.Column(db.Boolean(), default=False)
    payment_datetime = db.Column(db.DateTime)
    financial_archive_date = db.Column(db.DateTime)

    merchant_params = db.Column(db.JSON)

    def __repr__(self):
        return '<id : %s>' % (self.id)

    def save(self, session=None):
        session = session or db.session()
        try:
            session.add(self)
            session.commit()
            return {'invalid': False, 'message': "Payment successfully added", 'id': self.id}
        except Exception as e:
            session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Payment successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['paymentgateway_id'] = self.paymentgateway_id
        rv['amount'] = self.amount
        rv['currency_code'] = self.currency_code
        rv['payment_status'] = self.payment_status
        rv['user_id'] = self.user_id
        rv['order_id'] = self.order_id
        rv['payment_datetime'] = self.payment_datetime
        rv['financial_archive_date'] = self.financial_archive_date
        rv['is_test_payment'] = self.is_test_payment
        rv['is_active'] = self.is_active
        rv['is_trial'] = self.is_trial
        rv['created'] = self.created.isoformat()
        rv['modified'] = self.modified.isoformat()

        return rv

    def get_receipt_data(self):
        apple_payment = PaymentAppleIAP.query.filter_by(payment_id=self.id).first()
        return apple_payment.receipt_data

    def mirror_values(self):
        """
            This is used for display values, self.values() can't custom cos rassel forgot
            use values object or not in payment processings
        """
        rv = {}

        rv['id'] = self.id
        rv['paymentgateway_id'] = self.paymentgateway_id
        rv['amount'] = '%.2f' % self.amount
        rv['currency_code'] = self.currency_code
        rv['payment_status'] = self.payment_status
        rv['user_id'] = self.user_id
        rv['order_id'] = self.order_id

        if self.payment_datetime:
            rv['payment_datetime'] = self.payment_datetime.isoformat()
        else:
            rv['payment_datetime'] = None

        rv['financial_archive_date'] = self.financial_archive_date
        rv['created'] = self.created.isoformat()
        rv['modified'] = self.modified.isoformat()

        return rv


class PaymentDetail(TimestampMixin, db.Model):
    __tablename__ = 'core_paymentdetails'

    payment_id = db.Column(db.Integer, db.ForeignKey('core_payments.id'), nullable=False)
    ip_address = db.Column(db.String(32))

    def __repr__(self):
        return '<id : %s>' % (self.id)

    def save(self, session):
        session = session or db.session()
        try:
            session.add(self)
            session.commit()
            return {'invalid': False, 'message': "Payment details successfully added"}
        except Exception as e:
            session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Payment details successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['payment_id'] = self.payment_id
        rv['ip_address'] = self.ip_address

        return rv


class PaymentLog(TimestampMixin, db.Model):

    __tablename__ = 'core_logs_payments'

    id = db.Column(db.Integer, primary_key=True)
    paymentgateway_id = db.Column(db.Integer)
    user_id = db.Column(db.Integer)
    order_id = db.Column(db.Integer)
    request = db.Column(db.Text)
    api = db.Column(db.String(100))
    method = db.Column(db.String(10))
    error_message = db.Column(db.Text)
    checked = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return '<id : %s>' % (self.id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "PaymentLog successfully added"}
        except Exception, e:
            db.session.rollback()
            #db.session.delete(self)
            #db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "PaymentLog successfully deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def get_payment_gateways(self):
        pymnt_gtwy = PaymentGateway.query.filter_by(id=self.paymentgateway_id).first()
        if pymnt_gtwy:
            return [{"id": pymnt_gtwy.id, "name": pymnt_gtwy.name}]
        return []

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def extension_values(self, extension=None, custom=None, platform_id=None):
        data_values = self.values()

        for param in extension:
            if param == 'payment_gateways':
                data_values['payment_gateways'] = self.get_payment_gateways()

            if param == 'ext':
                data_values['payment_gateways'] = self.get_payment_gateways()

        if custom:
            original_values = data_values

            kwargs = {}
            fields_required = custom.split(',')
            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
            return kwargs

        return data_values

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['paymentgateway_id'] = self.paymentgateway_id
        rv['user_id'] = self.user_id
        rv['order_id'] = self.order_id
        rv['request'] = self.request
        rv['error_message'] = self.error_message
        rv['api'] = self.api
        rv['method'] = self.method
        rv['checked'] = self.checked

        return rv


class PaymentToken(TimestampMixin, BaseDatamodelMixin, db.Model):
    """
    Represents a reusable payment token in the database.  These are used with
    payment gateways that allow us to resubmit the same token multiple times
    (effectively storing the user's credit card data for the next purchase),
    but keeps their credit card data off our server.
    """
    user_id = Column(Integer, index=True, nullable=False)
    token = Column(String(250), index=True)
    masked_credit_card = Column(String(25))
    expiration = Column(DateTime)
    gateway = Column(postgresql.ENUM(*PAYMENT_GATEWAYS.values(), name='payment_gateways'), nullable=False)
    billing_address = Column(postgresql.JSON)
    shipping_address = Column(postgresql.JSON)
    first_name = Column(String(60))
    last_name = Column(String(60))
    is_active = Column(Boolean, default=True, nullable=False)


# Hookup the CRUD logger
__log_crud__ = [
    Payment,
    PaymentDetail,
    PaymentToken,
]

for model_class in __log_crud__:
    event.listen(
        model_class,
        'after_insert',
        partial(record_primary_entity_inserted, logger=_log)
    )
    event.listen(
        model_class,
        'after_update',
        partial(record_primary_entity_updated, logger=_log)
    )
