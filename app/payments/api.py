import httplib
import json
from datetime import datetime, timedelta

from flask import Response, request, make_response, current_app, g, jsonify
from flask_appsfoundry import controllers
from flask_appsfoundry.parsers import converters
from flask_restful import Resource, reqparse
from flask.views import MethodView
from requests.exceptions import HTTPError
from sqlalchemy.sql import and_, or_, desc
from werkzeug.exceptions import BadRequest

from app import app
from app.auth.decorators import token_required
from flask_appsfoundry.exceptions import UnprocessableEntity
from app.helpers import payment_logs
from app.orders.models import Order
from app.paymentgateway_connectors.atm_transfer.complete import VirtualAccountProcess
from app.paymentgateway_connectors.gopay.complete import GopayAccountProcess
from app.paymentgateway_connectors.creditcard.models import PaymentVeritrans, PaymentCreditCardToken
from app.payments.payment_status import PAYMENT_BILLED
from app.payments.signature_generator import GenerateSignature
from app.payments.helpers import PaymentConstruct
from app.payments.models import Payment
from app.payments.viewmodels import PaymentPendingViewModel
from app.services.kredivo.helpers import build_kredivo_gateway
from app.services.kredivo.kredivo_gateway import KredivoTransactionStatus

from app.payments.choices.gateways import INDOMARET, KREDIVO, VA_PERMATA, VA_BCA, VA_MANDIRI, VA_BNI, GOPAY

from . import parsers, models, serializers, _log
from .custom_get import PaymentList
from .choices import payment_statuses, PAYMENT_BILLED


class PaymentListApi(Resource):

    ''' Shows a list of Order Payment and do POST / GET '''

    def __init__(self):
        self.data = request.args.to_dict()

        super(PaymentListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def get(self):

        try:
            param = self.data
            param['api'] = '/v1/payments'
            param['method'] = 'GET'

            scene = PaymentList(param, 'payments').construct()
            return scene
        except Exception as e:

            _log.exception("Failed fetching payment list")

            json_form = {
                "message": str(e)
            }
            return Response(
                json_form,
                status=httplib.INTERNAL_SERVER_ERROR,
                mimetype='application/json'
            )

            return payment_logs(args=param, error_message=str(e))

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def post(self):
        pass


class PaymentAuthorizeApi(Resource):
    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required parameters
        self.reqparser.add_argument('order_id', type=int, required=True, location='json')
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('signature', type=str, required=True, location='json')
        self.reqparser.add_argument('payment_gateway_id', type=int, required=True, location='json')

        # optional parameters
        self.reqparser.add_argument('payment_code', type=str, location='json')
        self.reqparser.add_argument('return_url', type=str, location='json')
        self.reqparser.add_argument('cancel_url', type=str, location='json')
        self.reqparser.add_argument('payer_id', type=str, location='json')

        self.reqparser.add_argument('faspay_trx_data', type=dict, location='json')

        super(PaymentAuthorizeApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def post(self):
        _log.debug("Beginning payment authorize.")
        args = self.reqparser.parse_args()
        args['api'] = 'v1/payments/authorize'
        args['method'] = 'POST'


        # validate signature
        key_valid = GenerateSignature(order_id=args['order_id']).construct()
        if str(key_valid) != args['signature']:
            _log.info("Unable to authorize payment.  Signature invalid: {}".format(args['signature']))
            return payment_logs(args=args,
                error_message='Invalid signature : %s' % args['signature'])

        try:
            # construct items
            order = PaymentConstruct(args).initialize_transaction()
            return order

        except Exception as e:
            _log.exception("Unable to authorize payment.")
            return payment_logs(args=args, error_message=str(e))


class PaymentCaptureApi(Resource):
    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required parameters
        self.reqparser.add_argument('order_id', type=int, required=True, location='json')
        self.reqparser.add_argument('signature', type=str, required=True, location='json')
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('payment_gateway_id', type=int, required=True, location='json')

        # for payment gateway using only capture as the flow
        self.reqparser.add_argument('payer_id', type=str, location='json')
        self.reqparser.add_argument('payment_code', type=str, location='json')
        self.reqparser.add_argument('payment_id', type=int, location='json')
        self.reqparser.add_argument('billing_address', type=dict, location='json')

        # Clickpay
        self.reqparser.add_argument('card_no', type=str, location='json')
        self.reqparser.add_argument('response_token', type=str, location='json')
        self.reqparser.add_argument('challenge_code3', type=int, location='json')

        super(PaymentCaptureApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def post(self):
        _log.debug("Beginning payment capture")

        args = self.reqparser.parse_args()
        args['api'] = 'v1/payments/capture'
        args['method'] = 'POST'

        # validate signature
        key_valid = GenerateSignature(order_id=args['order_id']).construct()
        if str(key_valid) != args['signature']:
            _log.debug("Capture Failure.  Invalid payment signature: {}".format(args['signature']))
            return payment_logs(args=args,
                error_message='Invalid signature : %s' % args['signature'])

        try:
            # construct items
            order = PaymentConstruct(args).initialize_transaction()
            _log.debug("Capture Successful")
            return order
        except Exception as e:
            _log.exception("Capture Failure")
            return payment_logs(args=args, error_message=str(e))


class PaymentCaptureApiV2(Resource):
    '''
    This is a 100% copy of the version 1 code,
    but we're assigning the args['api'] value to v2 instead of v1
    '''
    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required parameters
        self.reqparser.add_argument('order_id', type=int, required=True, location='json')
        self.reqparser.add_argument('signature', type=str, required=True, location='json')
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('payment_gateway_id', type=int, required=True, location='json')
        self.reqparser.add_argument('save_token', type=converters.boolean)

        # for payment gateway using only capture as the flow
        self.reqparser.add_argument('payer_id', type=str, location='json')
        self.reqparser.add_argument('payment_code', type=str, location='json')
        self.reqparser.add_argument('payment_id', type=int, location='json')
        self.reqparser.add_argument('billing_address', type=dict, location='json')

        # Clickpay
        self.reqparser.add_argument('card_no', type=str, location='json')
        self.reqparser.add_argument('response_token', type=str, location='json')
        self.reqparser.add_argument('challenge_code3', type=int, location='json')

        super(PaymentCaptureApiV2, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def post(self):
        _log.debug("Starting payment capture v2 (VERITRANS ONLY)")
        args = self.reqparser.parse_args()
        args['api'] = 'v2/payments/capture'
        args['method'] = 'POST'

        # validate signature
        key_valid = GenerateSignature(order_id=args['order_id']).construct()
        if str(key_valid) != args['signature']:
            _log.debug("Capture Failure.  Invalid payment signature: {}".format(args['signature']))
            return payment_logs(args=args,
                error_message='Invalid signature : %s' % args['signature'])

        try:
            # construct items
            order = PaymentConstruct(args).initialize_transaction()
            return order
        except Exception as e:
            _log.exception("Capture Failed.  An unknown error occurred.")
            return payment_logs(args=args, error_message=str(e))


class PaymentValidateApi(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required parameters
        self.reqparser.add_argument('order_id', type=int, required=True, location='json')
        self.reqparser.add_argument('signature', type=str, required=True, location='json')
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('payment_gateway_id', type=int, required=True, location='json')
        self.reqparser.add_argument('payment_id', type=int, location='json')

        # ecash only
        self.reqparser.add_argument('ecash_trx_id', type=str, location='json')

        # optional APPLE
        self.reqparser.add_argument('receipt_data', type=str, location='json')
        self.reqparser.add_argument('unified_receipt_data', type=str, location='json')

        # optional INAPPBILLING (GOOGLE)
        self.reqparser.add_argument('purchase_token', type=str, location='json')
        self.reqparser.add_argument('developer_payload', type=str, location='json')
        self.reqparser.add_argument('product_sku', type=str, location='json')
        self.reqparser.add_argument('iab_order_id', type=str, location='json')

        # optional FASPAY
        self.reqparser.add_argument('order_number', type=str, location='json')
        self.reqparser.add_argument('repurchased', type=bool, location='json')
        super(PaymentValidateApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def post(self):
        _log.debug("Beginning payment authorization.")
        args = self.reqparser.parse_args()
        args['api'] = 'v1/payments/validate'
        args['method'] = 'POST'

        # validate signature
        key_valid = GenerateSignature(order_id=args['order_id']).construct()
        if str(key_valid) != args['signature']:
            return payment_logs(args=args,
                error_message='Invalid signature : %s' % args['signature'])

        try:
            # construct items
            order = PaymentConstruct(args).initialize_transaction()
            return order

        except Exception as e:
            _log.exception("Payment Validate Failed.  An unknown error occurred.")
            return payment_logs(args=args, error_message=str(e))


class PaymentChargeApi(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required parameters
        self.reqparser.add_argument('order_id', type=int, required=True, location='json')
        self.reqparser.add_argument('signature', type=str, required=True, location='json')
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('payment_gateway_id', type=int, required=True, location='json')
        self.reqparser.add_argument('catalog_id', type=int, location='json')

        super(PaymentChargeApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def post(self):
        _log.debug("Beginning payment charge.")
        args = self.reqparser.parse_args()
        args['api'] = 'v1/payments/charge'
        args['method'] = 'POST'

        # validate signature
        key_valid = GenerateSignature(order_id=args['order_id']).construct()
        if str(key_valid) != args['signature']:
            _log.debug("Invalid signature.")
            return payment_logs(
                args=args,
                error_message='Invalid signature : %s' % args['signature']
            )

        try:
            # construct items
            order = PaymentConstruct(args).initialize_transaction()
            return order

        except Exception as e:
            _log.exception("Payment Charge failed.  An unknown error occurred.")
            return payment_logs(args=args, error_message=str(e))


class PaymentPendingDetailApi(controllers.DetailApiResource):

    def build_query(self, base_query, input_args):
        query = base_query

        now_date = datetime.utcnow()
        date_filter_pending = now_date - timedelta(days=1)
        date_filter_cancelled = now_date - timedelta(days=1)

        # apply ordering
        query = query.distinct(Payment.order_id)

        query = query.filter(
            Payment.paymentgateway_id.in_(input_args['payment_gateway']),
            or_(and_(Payment.payment_status == payment_statuses.WAITING_FOR_PAYMENT,
                     Payment.modified >= date_filter_pending,
                     Payment.modified <= now_date),
                and_(Payment.payment_status == payment_statuses.PAYMENT_CANCELLED,
                     Payment.modified >= date_filter_cancelled,
                     Payment.modified <= now_date)))

        query = query.filter_by(user_id=input_args['user_id'])

        # filter by is_active
        query = query.filter_by(is_active=input_args['is_active'])

        # filter by client_id
        # this if special for whitelabel android only.
        if input_args['client_id']:
            query = query.filter(Payment.order.has(client_id=input_args['client_id']))

        query = query.order_by(desc(Payment.order_id))

        # pagination
        query_count = query.count()
        query = query.limit(input_args['limit']).offset(input_args['offset']).all()
        query = {'query':query, 'count':query_count}
        return query

    def get(self):

        session = models.Payment.query.session

        # 1.) grab our input
        parser = parsers.PendingArgsParser()
        args = parser.parse_args()


        try:
            # 2.) build our query
            query = self.build_query(base_query=models.Payment.query, input_args=args)

            # 3.) Build our result proxy
            view_model = PaymentPendingViewModel(query['query'], query['count'], args['offset'], args['limit'])
            view_model = {'pending_transactions' : view_model.pending_transactions,
                          'metadata': view_model.metadata }
            # 4.) Serialize and return response
            return view_model, 200

        except Exception:
            _log.exception("Error fetching payment details")
            raise
        finally:
            # 5.) CLEAN UP
            session.rollback()

# This function is not secure,
# filter always need ?user_id=.. this must change retrieve from g user by JWT
# No, PUT and POST for credit card token api. only GET

# class TokenListCreateApi(controllers.ListCreateApiResource):
#     query_set = PaymentCreditCardToken.query.filter(PaymentCreditCardToken.expiration >= datetime.now())
#     default_ordering = (desc(PaymentCreditCardToken.expiration),)
#     create_request_parser = parsers.PaymentTokenArgsParser
#     list_request_parser = parsers.PaymentTokenListArgsParser
#     response_serializer = serializers.TokenSerializer()
#     serialized_list_name = 'tokens'
#     get_security_tokens = ('can_read_write_global_all', 'can_read_write_public_ext', 'can_read_write_public',)
#     post_security_tokens = ('can_read_write_global_all', 'can_read_write_public_ext',)
#
#
# class TokenDetailApi(controllers.DetailApiResource):
#     query_set = PaymentCreditCardToken.query
#     request_parser = parsers.PaymentTokenArgsParser()
#     response_serializer = serializers.TokenSerializer()
#     get_security_tokens = ('can_read_write_global_all', 'can_read_write_public_ext', 'can_read_write_public',)
#     put_security_tokens = ('can_read_write_global_all', 'can_read_write_public_ext',)
#     delete_security_tokens = ('can_read_write_global_all',)

class TokenListCreateApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_write_public')
    def get(self):
        user_id = g.current_user.id
        args = request.args.to_dict()
        limit, offset = args.get('limit', 20), args.get('offset', 0)

        cc_token = PaymentCreditCardToken.query.filter(PaymentCreditCardToken.user_id == user_id,
            PaymentCreditCardToken.is_active == True).order_by(desc(PaymentCreditCardToken.id))
        total_count = cc_token.count()

        if limit:  cc_token = cc_token.limit(limit).offset(offset)
        data_token = [data.values() for data in cc_token.all()] if total_count else []
        response = jsonify({'tokens': data_token,
                            'metadata': {'resultset': {'offset': offset, 'limit': limit, 'count': total_count}}})
        response.status_code = httplib.OK
        return response

class ValidateIndomaretPaymentApi(MethodView):
    """ API to retrieve callback from Veritrans.

    the main purpose for this API is to validate indomaret payment.
        if customer paid in indomaret, veritrans will hit this API,
        sending payment code and payment status.

    because they way it configure in veritrans (in VT-WEB notification),
      Veritrans Notification will also hit this API too
        ex: from Capture/Charga credit card transaction notification.
            for this, we just need to return 200/OK response.
    """

    def post(self):
        request_body = json.loads(request.data)

        payment_code = request_body.get('payment_code', '')
        order_id = int(request_body.get('order_id', 0))
        payment_status = request_body.get('transaction_status', '').lower()
        return self.validate_indomaret_payment(payment_code, order_id, payment_status)

    @staticmethod
    def validate_indomaret_payment(payment_code, order_id, payment_status):

        if payment_status == 'pending' or not payment_code:
            # notification with status still pending, do nothing
            # or request with no payment code, just return OK
            #   in capture (charge credit card): veritrans will hit this api too with empty payment code
            #   do nothing here, because order already validate in CAPTURE api
            return make_response('', httplib.OK, {'Content-type': 'application/json'})

        payment_veritrans = PaymentVeritrans.query.filter_by(
            payment_code=payment_code
        ).order_by(desc(PaymentVeritrans.id)).first()

        if not payment_veritrans and current_app.config['DEBUG']:
            # for sand box, ignore it, just send ok, to prevent too many request from veritrans api to our sandbox
            return make_response('', httplib.OK, {'Content-type': 'application/json'})
            # else, don't throw error here yet, let it handled by PaymentConstruct initialize_transaction process

        saved_payment_id = payment_veritrans.payment_id if payment_veritrans else 0

        # create req body args, for payment logs and payment construct
        api = 'v1/payments/validate'
        req_body_args = {
            "api": api,
            "payment_gateway_id": INDOMARET,
            "order_id": order_id,
            "payment_code": payment_code,
            "payment_id": saved_payment_id
        }

        # authenticate the callback that hits this API
        # reconfirm payment status, hit veritrans csstore api to verify status
        resp = {}
        req_body_args['veritrans_cstore_status'] = resp.transaction_status
        if not resp.transaction_status:
            # raise error failed to hit veritrans API
            return payment_logs(args=req_body_args, error_message=str(resp),
                                status_code=resp.status_code if resp and resp.status_code else None)

        if resp.transaction_status in ['settlement', 'capture'] and \
                payment_veritrans and payment_veritrans.payment.payment_status == PAYMENT_BILLED:
            # notification of settlement but the payment already billed, return OK to veritrans
            return make_response('', httplib.OK, {'Content-type': 'application/json'})

        try:
            #
            return PaymentConstruct(args=req_body_args).initialize_transaction()
        except Exception as e:
            return payment_logs(args=req_body_args, error_message=str(e))


class CaptureKredivoPaymentApi(MethodView):
    """ API to retrieve callback/push notification from Kredivo

    """

    def post(self):
        req_body = json.loads(request.data)
        _log.info("Kredivo Capture: request data: {}".format(request.data))
        order_id = req_body.get('order_id')
        transaction_id = req_body.get('transaction_id', '')
        transaction_status = req_body.get('transaction_status', '')
        signature_key = req_body.get('signature_key', '')
        return self.validate_kredivo_payment(order_id, transaction_id, transaction_status, signature_key)

    def validate_kredivo_payment(self, order_id, transaction_id, transaction_status, signature_key):
        response_body = {
            "status": "OK",
            "message": ""
        }
        payment = Payment.query.filter_by(order_id=order_id).first()

        if not payment and current_app.config['DEBUG']:
            # if order not found and it's in sand box, ignore it, just send ok,
            #   to prevent too many request from kredivo api to our sandbox
            return make_response(json.dumps(response_body), httplib.OK, {'Content-type': 'application/json'})
            # else, don't throw error here yet, let it handled by PaymentConstruct initialize_transaction process

        saved_payment_id = payment.id if payment else None

        # create req body args, for payment logs and payment construct
        api = 'v1/payments/capture'
        req_body_args = {
            "api": api,
            "payment_gateway_id": KREDIVO,
            "order_id": order_id,
            "transaction_id": transaction_id,
            "payment_id": saved_payment_id,
            "signature_key": signature_key,
            "transaction_status": transaction_status
        }

        # authenticate the callback that hits this API
        # reconfirm payment status, hit kredivo api to verify status
        kredivo_gateway = build_kredivo_gateway()
        try:
            resp = kredivo_gateway.get_payment_status(transaction_id=transaction_id, signature_key=signature_key)
            _log.info("Kredivo Capture: recheck response data: {}".format(json.dumps(resp.serialize())))
            req_body_args['transaction_status'] = resp.transaction_status
            req_body_args['fraud_status'] = resp.fraud_status
        except HTTPError as e:
            # raise error failed to hit veritrans API
            return payment_logs(args=req_body_args, error_message=str(resp),
                                status_code=resp.status_code if resp and resp.status_code else None)

        if resp.transaction_status.upper() in [KredivoTransactionStatus.settlement.value,
                                               KredivoTransactionStatus.capture.value] and \
            payment and payment.payment_status == PAYMENT_BILLED:
            # notification of settlement but the payment already billed, return OK to veritrans
            response_body['order_id'] = payment.order_id
            response_body['payment_id'] = payment.id
            return make_response(json.dumps(response_body), httplib.OK, {'Content-type': 'application/json'})

        try:
            # do further process: if settlement, deliver the item to user owned items, etc.
            return PaymentConstruct(args=req_body_args).initialize_transaction()
        except Exception as e:
            _log.exception("Kredivo Capture: error: {}".format(str(e)))
            return payment_logs(args=req_body_args, error_message=str(e))


class MidtransAccountNotifyApi(MethodView):
    """
        This handling midtrans notify callback urls.
    """

    def post(self):
        request_body = json.loads(request.data)
        order_id = request_body.get('order_id', None)
        data_order = Order.query.filter_by(id=order_id).first()
        message = 'Transaction Success!'

        if data_order:
            if data_order.paymentgateway_id in [VA_PERMATA, VA_BCA, VA_MANDIRI, VA_BNI]:
                status, message = VirtualAccountProcess(request_body, data_order).complete()
            if data_order.paymentgateway_id in [GOPAY]:
                status, message = GopayAccountProcess(request_body, data_order).complete()
        return Response(json.dumps({'status': message}), status=httplib.OK, mimetype='application/json')

