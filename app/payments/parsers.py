import json

from flask_appsfoundry import parsers
from flask_appsfoundry.parsers import standardinputs as si, filterinputs as fi, converters, InputField

from flask_restful.inputs import boolean
from app.paymentgateway_connectors.creditcard.models import PaymentCreditCardToken

from .choices import PAYMENT_GATEWAYS
from .choices.gateways import TCASH, FINNET

def payment_gateway_id(input):
    gateway_ids = [int(gid) for gid in input.split(',')]
    for gateway_id in gateway_ids:
        # just make sure it exists -- so we throw a key error if it doesn't
        PAYMENT_GATEWAYS[gateway_id]
    return gateway_ids

class PendingArgsParser(parsers.ParserBase):
    payment_gateway = InputField(type=payment_gateway_id,
        default=[FINNET, TCASH])
    user_id = InputField(type=int, required=True)
    is_active = InputField(type=boolean, default=True)
    limit = InputField(type=int, default=20)
    offset = InputField(type=int, default=0)
    client_id = InputField(type=int)

class PaymentTokenArgsParser(parsers.ParserBase):

    user_id = si.InputField(type=int, required=True)
    last_name = si.InputField()
    first_name = si.InputField()
    masked_credit_card = si.InputField()
    token=si.InputField()
    expiration = si.InputField(type=converters.naive_datetime_from_iso8601)
    billing_address = si.InputField(type=lambda val: json.dumps(val))
    bin_bank = si.InputField()
    bin_bank_code = si.InputField()
    bin_class = si.InputField()
    bin_types = si.InputField()
    bin_brand = si.InputField()
    bin_country_code = si.InputField()


class PaymentTokenListArgsParser(parsers.SqlAlchemyFilterParser):

    __model__ = PaymentCreditCardToken

    id = fi.IntegerFilter()
    user_id = fi.IntegerFilter()
    last_name = fi.StringFilter()
    first_name = fi.StringFilter()
    masked_credit_card = fi.StringFilter()
    token=fi.StringFilter()
    expiration = fi.DateTimeFilter()
    is_active = fi.BooleanFilter()
    billing_address = fi.StringFilter()
    bin_bank = fi.StringFilter()
    bin_bank_code = fi.StringFilter()
    bin_class = fi.StringFilter()
    bin_types = fi.StringFilter()
    bin_brand = fi.StringFilter()
    bin_country_code = fi.StringFilter()
