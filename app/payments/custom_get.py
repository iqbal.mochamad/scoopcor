import json
import httplib

from flask_restful import Resource, Api, reqparse
from flask_restful import Api, reqparse
from flask import abort, Response, jsonify
from datetime import date, timedelta
import datetime

from app import app, db

from sqlalchemy import func, distinct
from sqlalchemy.sql import and_, or_, desc

from app.payments.models import Payment, PaymentDetail, PaymentLog
from app.orders.models import Order, OrderLine, OrderLineDiscount, OrderDetail
from app.offers.models import Offer, OfferPlatform
from app.items.models import Item, UserItem, UserSubscription

from app.paymentgateway_connectors.finnet195.models import PaymentFinnet195, PaymentFinnet195Detail
from app.paymentgateway_connectors.tcash.models import PaymentTcash, PaymentTcashDetail
from app.helpers import payment_logs

from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME

class PaymentList():
    def __init__(self, param=None, module=None):
        self.module = module
        self.param = param

        self.payment_gateway_ids = param.get("payment_gateway_id", None)
        self.order = param.get("order", None)
        self.limit = param.get("limit", 20)
        self.offset = param.get("offset", 0)
        self.modified__gte = param.get("modified__gte", None)
        self.modified__lte = param.get("modified__lte", None)
        self.payment_status = param.get("payment_status", None)
        self.user_id = param.get("user_id", None)
        self.order_id = param.get("order_id", None)
        self.tcash_no_ref = param.get("tcash_no_ref", None)
        self.expand = param.get("expand", None)
        self.payment_id = param.get("payment_id", None)

        self.master = ''
        self.payment_gateway_list = []
        self.payments = []
        self.total = 0

    def json_success(self, data_payments=None, offset=None, limit=None):

        if self.total <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            for transaction in data_payments:
                try:
                    self.order_id = transaction.values().get('order_id', None)

                    payment = {}
                    payment_gateway = transaction.values().get('paymentgateway_id', None)

                    payment['order_id'] = self.order_id
                    payment['payment_status'] = transaction.values().get('payment_status', None)
                    payment['transaction_date'] = transaction.values().get('modified', None)
                    payment['payment_id'] = transaction.values().get('id', None)
                    payment['payment_gateway_id'] = transaction.values().get('paymentgateway_id', None)

                    if self.expand == "items":
                        payment['orderlines'] = self.get_items()

                    self.payments.append(payment)
                except Exception, e:
                    payment_logs(args=self.param, error_message=str(e))

            temp = {}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}

            rv = json.dumps({self.module:  self.payments, "metadata": tempx})
            return Response(rv, status=httplib.OK, mimetype='application/json')

    def get_items(self):
        orderlines = self.order_master().get_order_line()

        items = []
        for dataline in orderlines:
            temp = {}
            temp['items'] = self.json_items(dataline)
            items.append(temp)

        return items

    def json_items(self, orderline=None):
        data_item = []

        offer = orderline.get_offer()
        if offer:
            if offer.offer_type_id == 2:
                brands = offer.get_brands_id()

                for ibrands in brands:
                    item = {}

                    item_brands = Item.query.filter_by(brand_id=ibrands, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).order_by(
                        desc(Item.release_date)).limit(1).all()

                    if item_brands:
                        ms_it = item_brands[0]
                        item['id'] = ms_it.id
                        item['name'] = ms_it.name
                        item['edition_code'] = ms_it.edition_code
                        item['item_status'] = {v:k for k,v in STATUS_TYPES.items()}[ms_it.item_status]
                        item['release_date'] = ms_it.release_date.isoformat()
                        item['is_featured'] = ms_it.is_featured
                        item['vendor'] = ms_it.get_vendor()
                        item['languages'] = ms_it.get_languages()
                        item['countries'] = ms_it.get_country()
                        item['authors'] = ms_it.get_authors()
                        data_item.append(item)

            else:
                for xo in offer.get_items():
                    item = {}
                    ms_it = self.get_master_item(xo)
                    item['name'] = ms_it.name
                    item['id'] = ms_it.id
                    item['edition_code'] = ms_it.edition_code
                    item['item_status'] = {v:k for k,v in STATUS_TYPES.items()}[ms_it.item_status]
                    item['release_date'] = ms_it.release_date.isoformat()
                    item['is_featured'] = ms_it.is_featured
                    item['vendor'] = ms_it.get_vendor()
                    item['languages'] = ms_it.get_languages()
                    item['countries'] = ms_it.get_country()
                    item['authors'] = ms_it.get_authors()
                    data_item.append(item)

        return data_item

    def get_master_item(self, item_id=None):
        """
            Get ITEM MASTER DATA
        """
        data_item = None

        #ITEM READY FOR CONSUME
        item = Item.query.filter_by(id=item_id, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).first()
        if item:
            data_item = item
        return data_item

    def order_master(self):
        order = Order.query.filter_by(id=self.order_id).first()
        if not order:
            payment_logs(args=self.param, error_message="invalid order id")
        return order

    def payment_master(self):
        return Payment.query

    def construct(self):
        try:
            self.master = self.payment_master()

            #if self.tcash_no_ref:
            #    tcash_trx = PaymentTcash.query.filter_by(tcash_trx_id=self.tcash_no_ref).first()
            #    if tcash_trx:
            #        self.payment_id = tcash_trx.values().get('payment_id')
            #else:
            #    if self.user_id is None and self.payment_gateway_list == []:
            #        payment_logs(args=self.param, error_message="invalid user_id or payment gateway")

            if self.payment_id:
                self.master = self.master.filter_by(id=int(self.payment_id))

            if self.payment_status:
                self.master = self.master.filter_by(payment_status=int(self.payment_status))

            if self.user_id:
                self.master = self.master.filter_by(user_id=int(self.user_id))

            if self.order_id:
                self.master = self.master.filter_by(order_id=int(self.order_id))

            if self.payment_gateway_ids:
                self.payment_gateway_ids = self.payment_gateway_ids.split(',')
                for payment_gateway in self.payment_gateway_ids:
                    self.payment_gateway_list.append(int(payment_gateway))
                self.master = self.master.filter(Payment.paymentgateway_id.in_(self.payment_gateway_list))

            if self.modified__gte:
                self.master = self.master.filter(Payment.modified >= self.modified__gte)

            if self.modified__lte:
                self.master = self.master.filter(Payment.modified <= self.modified__lte)

            if self.order:
                order_data = self.order.split(',')
                for orderx in order_data:
                    if orderx[0] == '-': #descending
                        order_param = '%s %s' % (orderx[1:], 'desc')
                        self.master = self.master.order_by(order_param)
                    else:
                        self.master = self.master.order_by(orderx)

            self.master = self.master.limit(self.limit).offset(self.offset).all()
            self.total = len(self.master)

            return self.json_success(self.master, self.offset, self.limit)
        except Exception, e:
            return payment_logs(args=self.param, error_message=str(e))


class PaymentLogMonitor(object):

    def __init__(self, param=None, module=None):

        self.limit = param.get('limit', 20)
        self.offset = param.get('offset', 0)
        self.fields = param.get('fields', None)

        self.paymentgateway_id = param.get('paymentgateway_id', None)
        self.user_id = param.get('user_id', None)
        self.order_id = param.get('order_id', None)
        self.is_checked = param.get('is_checked', False)

        self.extend = param.get('expand', None)
        self.module = module
        self.master = ''
        self.total = 0

    def json_success(self, data_items=None, offset=None, limit=None):
        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:

            if self.fields and self.extend:
                extends = self.extend.split(',')
                cur_values = [u.extension_values(extends, self.fields) for u in data_items]

            elif self.extend:
                extends = self.extend.split(',')
                cur_values = [u.extension_values(extends) for u in data_items]

            elif self.fields:
                cur_values = [u.custom_values(self.fields) for u in data_items]
            else:
                cur_values = [u.values() for u in data_items]

            temp={}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}

            if self.module in ['LOGMONITOR_DETAILS']:
                rv = json.dumps(cur_values[0])
            else:
                rv = json.dumps({'orders': cur_values, "metadata": tempx})

            return Response(rv, status=httplib.OK, mimetype='application/json')

    def payment_log_master(self):
        return PaymentLog.query

    def filter_custom(self):
        self.master = self.payment_log_master()

        if self.paymentgateway_id:
            self.master = self.master.filter_by(paymentgateway_id=self.paymentgateway_id)

        if self.user_id:
            self.master = self.master.filter_by(user_id=self.user_id)

        if self.order_id:
            self.master = self.master.filter_by(order_id=self.order_id)

        if self.is_checked:
            self.master = self.master.filter_by(checked=self.is_checked)

        self.master = self.master.limit(self.limit).offset(self.offset).all()
        self.total = len(self.master)

        return self.json_success(self.master, self.offset, self.limit)
