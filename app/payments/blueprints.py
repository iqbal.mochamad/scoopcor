from flask import Blueprint
from flask_appsfoundry import Api


from .api import PaymentAuthorizeApi, PaymentCaptureApi, PaymentValidateApi, \
    PaymentChargeApi, PaymentListApi, \
    TokenListCreateApi, PaymentCaptureApiV2, ValidateIndomaretPaymentApi,\
    PaymentPendingDetailApi, CaptureKredivoPaymentApi, MidtransAccountNotifyApi

#,PaymentPendingApi

payments_v1_blueprint = Blueprint('payments_v1', __name__, url_prefix='/v1/payments')
payments_v1_api = Api(payments_v1_blueprint)

payments_v1_api.add_resource(PaymentAuthorizeApi, '/authorize')
payments_v1_api.add_resource(PaymentCaptureApi, '/capture')
payments_v1_api.add_resource(PaymentValidateApi, '/validate')
payments_v1_api.add_resource(PaymentChargeApi, '/charge')
payments_v1_api.add_resource(PaymentListApi, '')
payments_v1_api.add_resource(PaymentPendingDetailApi, '/pending')
#payments_v1_api.add_resource(PaymentPendingApi, '/pending')
payments_v1_api.add_resource(TokenListCreateApi, "/tokens")
# payments_v1_api.add_resource(TokenDetailApi, "/tokens/<int:db_id>")
payments_v1_api.add_resource(MidtransAccountNotifyApi, '/va-complete')

payments_v1_blueprint.add_url_rule(
    '/kredivo/capture', view_func=CaptureKredivoPaymentApi.as_view('kredivo_payment_capture')
)


# veritrans indomaret callback api -- when customer paid, veritrans will hit this api to validate payment in our system
payments_v1_blueprint.add_url_rule(
    '/veritrans/cstore/validation',
    view_func=ValidateIndomaretPaymentApi.as_view('veritrans_indomaret_payment_validation'))


payments_v2_blueprint = Blueprint('payments_v2', __name__, url_prefix='/v2/payments')
payments_v2_api = Api(payments_v2_blueprint)
payments_v2_api.add_resource(PaymentCaptureApiV2, '/capture')
