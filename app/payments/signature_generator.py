#FOR PAYMENT SIGNATURE DECODER
import hashlib

from app import app


class GenerateSignature():

    def __init__(self, order_id=None):

        self.order_id = str(order_id)
        self.shared_key = app.config['PAYMENT_SALT']
        self.hash_key = ''

    def generate_signature(self):

        #loop, (N + order_id[:2])
        max_loop = self.order_id[-2:]
        loop_range = range(1, int(max_loop) + 1) #RULE : ALL + 1 FOR AVOID ORDER_ID : 00

        # hash(<order_id> + <shared_secret>)
        # create first key
        self.hash_key = hashlib.sha256('%s%s' % (self.order_id, self.shared_key)).hexdigest()

        for iloop in loop_range:
            self.hash_key = hashlib.sha256(self.hash_key + self.shared_key).hexdigest()
        return self.hash_key

    def construct(self):
        #return self.generate_signature()
        return self.generate_signature()

