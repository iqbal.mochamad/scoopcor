from __future__ import unicode_literals

APPLE = 1
PAYPAL = 2
FREE = 3
FINNET = 5
TCASH = 6
POINT = 9
CLICKPAY = 11
CREDITCARD = 12
OLD_VERITRANS = 12 #this still need for enum models, rename to credit_card so not confuse
XL = 13
ECASH = 14
IAB = 15
IAP = 16
KLIKBCA = 17
BRIEPAY = 18
BRIMOCASH = 19
CIMBCLICKS = 21
ELEVENIA = 22
GRAMEDIA = 23
WALLET = 24
DIRECT_PAYMENT = 25
INDOMARET = 26
KREDIVO = 27
VA_PERMATA = 28
VA_BCA = 29
VA_MANDIRI = 30
VA_BNI = 31
GOPAY = 32
LINK_AJA = 33
OVO = 34
CIMB_CLICK = 35
CIMB_GO_MOBILE = 36
CIMB_REKPON = 37
SHOPEE_PAY = 38


PAYMENT_GATEWAYS = {
    APPLE: 'apple in-app purchase',
    PAYPAL: 'paypal',
    FREE: 'free purchase',
    FINNET: 'finnet atm transfer',
    TCASH: 't-cash',
    POINT: 'gd point',
    CLICKPAY: 'mandiri clickpay',
    CREDITCARD: 'creditcard',
    XL: 'xl payment',
    ECASH: 'mandiri e-cash',
    IAB: 'google in-app billing',
    IAP: 'windows phone in-app purchase',
    ELEVENIA: 'elevenia',
    GRAMEDIA: 'gramedia',
    INDOMARET: 'indomaret',
    KREDIVO: 'kredivo',
    VA_PERMATA : 'permata',
    VA_BCA : 'bca',
    VA_MANDIRI : 'mandiri',
    VA_BNI : 'bni',
    GOPAY : 'gopay',
    OLD_VERITRANS: 'veritrans',
    LINK_AJA: 'link aja',
    OVO: 'ovo',
    CIMB_CLICK : 'CIMB Clicks',
    CIMB_GO_MOBILE : 'CIMB GoMobile',
    CIMB_REKPON : 'CIMB RekPon',
    SHOPEE_PAY : 'Shopee Pay'
}


# E2PAY ID PAYMENTGATEWAYS
E2PAY_CREDIT_CARD = 1
E2PAY_CIMB = 7
E2PAY_XL_TUNAI = 9
E2PAY_TCASH = 10 # this id used for TCASH and LinkAja
E2PAY_IB_MUAMALAT = 11
E2PAY_EPAY_BRI = 12
E2PAY_DANAMON_ONLINE_BANKING = 13
E2PAY_DOMPETKU_INDOSAT = 14
E2PAY_ONLINE_SBI = 16
E2PAY_BCA_KLIKPAY = 18
E2PAY_QNB = 19
E2PAY_VA_MAYBANK = 20
E2PAY_BTN = 22
E2PAY_MANDIRI_CLICKPAY = 24
E2PAY_MBAYAR = 25
E2PAY_INDOMARET = 26
E2PAY_VA_PERMATA = 32
E2PAY_OVO = 34
E2PAY_SHOPEE = 40


# This is for mapping our ID payment with E2Pay Payment Gateway.
E2PAY_GATEWAYS = {
    OVO: E2PAY_OVO,
    LINK_AJA: E2PAY_TCASH,
    CIMB_CLICK: E2PAY_CIMB,
    CIMB_GO_MOBILE: E2PAY_CIMB,
    CIMB_REKPON: E2PAY_CIMB,
    SHOPEE_PAY: E2PAY_SHOPEE
}
