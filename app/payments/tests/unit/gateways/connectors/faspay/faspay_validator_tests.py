import unittest
from app.paymentgateway_connectors.faspay.faspaypg.validators import \
    RequiredValidator, ValidationError

from mock import MagicMock
from faker import Factory
import nose


class FaspayValidatorTests(unittest.TestCase):
    def test_required_validator(self):
        validator = RequiredValidator()
        self.assertRaises(ValidationError, lambda: validator.validate(None))