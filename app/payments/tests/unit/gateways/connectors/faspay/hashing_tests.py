"""
Faspay Hashing Tests
====================

Check that our utility classes generate their hashing data properly.

All of these unit tests, have a test for a 'generate' method.  The returned
value here will be identical to the
"""
from datetime import datetime
import unittest

from app.paymentgateway_connectors.faspay.faspaypg.hashing import \
    BCASignatureGen


class BCASigntaureGenTests(unittest.TestCase):

    def setUp(self):

        self.hash_gen = BCASignatureGen(
            klikpaycode=123,
            transaction_no=456,
            currency="IDR",
            keyid="12345678901234561234567890123456",
            transaction_date=datetime(
                year=2010, month=1, day=20, hour=1, minute=1, second=1
            ).strftime('%d/%m/%Y %H:%M:%S'),
            total_amount=1500500.00
        )

    def test_first_step(self):

        expected = ("123456IDR12345678901234561234567890123456",
                    str(20012010 + 1500500))

        actual = self.hash_gen._first_step()

        self.assertEqual(actual, expected)

    def test_get_hash(self):
        """ Verify our hashing function works according to KlikPay's
        documentation.
        """
        fixtures = {
            '21512510': -951649475,
            '123456IDR12345678901234561234567890123456': 2063172304,
        }

        for given, expectation in fixtures.items():
            actual = self.hash_gen._get_hash(given)
            self.assertEquals(actual, expectation)

    def test_get_hash2(self):
        """ Verify our hashing function works according to KlikPay's
        documentation.
        """
        fixtures = {
            '27096567': -57232946,
            '07APLI03723114040500027656IDR306A3479334B504C4979345030656C72': 1998862596,
        }

        for given, expectation in fixtures.items():
            actual = self.hash_gen._get_hash(given)
            self.assertEquals(actual, expectation)

    def test_third_step(self):

        first_values = self.hash_gen._first_step()
        hashed_values = [self.hash_gen._get_hash(tv) for tv in first_values]
        expected = 1111522829

        self.assertEqual(self.hash_gen._third_step(*hashed_values), expected)

    def test_generate(self):
        expected = 1111522829

        self.assertEqual(self.hash_gen.generate(), expected)

