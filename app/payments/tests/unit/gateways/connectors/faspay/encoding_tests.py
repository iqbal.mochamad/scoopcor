# coding=utf-8
from __future__ import unicode_literals

from unittest import TestCase

from app.paymentgateway_connectors.faspay.faspaypg.request import PostDataResponse

class PostDataResponseSerializerTests(TestCase):

    def test_unicode_parsing(self):

        fixture = known_failure
        resp = PostDataResponse(postdataxml=fixture.encode('utf-8'))
        resp_as_dict = resp.serialize()




known_failure = """<?xml version="1.0" encoding="utf-8"?>
<faspay>
 <response>Transmisi Info Detil Pembelian</response>
 <trx_id>3114040500000380</trx_id>
 <merchant_id>31140</merchant_id>
 <merchant>SCOOPONLINE</merchant>
 <bill_no>76289295549417</bill_no>
 <bill_items>
   <product>MOTOR PLUS / ED 860 AUG 2015</product>
   <payment_plan>01</payment_plan>
   <qty>1</qty>
   <amount>1000000</amount>
   <tenor>00</tenor>
   <merchant_id>31140</merchant_id>
 </bill_items>
 <bill_items>
   <product>nakita / ED 856 2015</product>
   <payment_plan>01</payment_plan>
   <qty>1</qty>
   <amount>1000000</amount>
   <tenor>00</tenor>
   <merchant_id>31140</merchant_id>
 </bill_items>
 <bill_items>
   <product>C&amp;R / 26â01 SEP 2015</product>
   <payment_plan>01</payment_plan>
   <qty>1</qty>
   <amount>900000</amount>
   <tenor>00</tenor>
   <merchant_id>31140</merchant_id>
 </bill_items>
 <response_code>00</response_code>
 <response_desc>Sukses</response_desc>
</faspay>
"""
