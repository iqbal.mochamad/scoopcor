from datetime import timedelta

from app import db
from app.orders.choices import COMPLETE, NEW, DELIVERY_IN_PROCESS
from app.orders.helpers import order_status
from app.orders.models import Order
from app.payments.helpers import PaymentConstruct
from app.payments.payment_status import WAITING_FOR_PAYMENT, PAYMENT_IN_PROCESS
from app.utils.datetimes import get_local, get_local_nieve
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory


class HelpersTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(HelpersTests, cls).setUpClass()
        cls.session2 = db.session()
        cls.session2.add(cls.regular_user)
        cls.order = OrderFactory(user_id=cls.regular_user.id, order_status=NEW,
                                 total_amount=5500, final_amount=5000)
        order_line = OrderLineFactory(order=cls.order, orderline_status=NEW,
                                      price=5500, final_price=5000)
        cls.order2 = OrderFactory(user_id=cls.regular_user.id, order_status=NEW,
                                  total_amount=5500, final_amount=5000)
        cls.session2.add(order_line)
        cls.session2.add(cls.order)
        cls.session2.add(cls.order2)
        cls.session2.commit()

        cls.pc = PaymentConstruct(args={"dummy": 123})
        cls.pc.order_id = cls.order.id
        cls.pc.payment_gateway_id = cls.order.paymentgateway_id
        cls.pc.is_sandbox = True
        cls.pc.order_rslt = {
            "user_id": cls.order.user_id,
            "amount": cls.order.final_amount,
            "payment_gateway_id": cls.order.paymentgateway_id,
            "currency": cls.order.currency_code,
        }
        cls.session.commit()

    def test_save_to_payment(self):
        # test save_to_payment method
        expected_date = get_local_nieve() - timedelta(days=1)
        result, payment_id = self.pc.save_to_payment(payment_status=WAITING_FOR_PAYMENT, payment_datetime=expected_date)
        self.assertTrue(result)
        self.assertGreater(payment_id, 0)
        self.assertEqual(self.pc.payment.payment_status, WAITING_FOR_PAYMENT)
        self.assertAlmostEqual(self.pc.payment.payment_datetime, expected_date)

        if not self.pc.payment_id:
            self.pc.payment_id = self.pc.payment.id

        # emptied pc.payment, just for test
        self.pc.payment = None
        # test change_payment_status method
        expected_date = get_local_nieve()
        expected_merch_param = {"payment_code": "dsakdfsalkfdsaf"}
        result, info = self.pc.change_payment_status(
            status=PAYMENT_IN_PROCESS, payment_datetime=expected_date,
            merchant_params=expected_merch_param)
        self.assertTrue(result)
        self.assertEqual(self.pc.payment.payment_status, PAYMENT_IN_PROCESS)
        self.assertEqual(self.pc.payment.merchant_params, expected_merch_param)
        self.assertAlmostEqual(self.pc.payment.payment_datetime, expected_date)

        # emptied pc.payment, just for test
        # test save_payment_date method
        self.pc.payment = None
        expected_date = get_local_nieve() + timedelta(days=1)
        result, info = self.pc.save_payment_date(payment_date=expected_date)
        self.assertTrue(result)
        self.assertAlmostEqual(self.pc.payment.payment_datetime, expected_date)

    def test_fetch_order(self):
        # set/prepare data order
        with self.on_session(self.order):
            self.order.order_status = Order.Status.new.value
            self.pc.order_id = self.order.id
            self.session.add(self.order)

            items = [{
                    "name": lineitem.name,
                    "is_free": lineitem.is_free,
                } for lineitem in self.order.order_lines
            ]

            self.session.commit()

        result, json_data = self.pc.fetch_order()
        if not result:
            self.fail('Result not True, error {0}, info {0.data}'.format(json_data))

        actual_items = json_data.pop("items")
        expected_json = {
            "payment_gateway_id": self.order.paymentgateway_id,
            "amount": float(self.order.final_amount),
            "currency": self.order.currency_code,
            "user_id": self.order.user_id,
            "order_number": self.order.order_number
        }
        self.assertDictContainsSubset(expected_json, json_data)
        self.assertDictContainsSubset(items[0], actual_items[0])

    def test_check_new_transaction(self):
        result, info = self.pc.check_new_transaction()
        self.assertTrue(result)

        # test failed (change check parameter):
        self.pc.order_rslt['user_id'] = self.order.id + 1234
        result, info = self.pc.check_new_transaction()
        # restore data
        self.pc.order_rslt['user_id'] = self.order.user_id
        self.assertFalse(result)

        # test wrong payment gateway
        self.pc.payment_gateway_id = -1234
        result, info = self.pc.check_new_transaction()
        # restore data
        self.pc.payment_gateway_id = self.order.paymentgateway_id
        self.assertFalse(result)

        # test wrong order status
        original_order_status = self.pc.order.order_status
        self.pc.order.order_status = COMPLETE
        result, info = self.pc.check_new_transaction()
        # restore data
        self.pc.order.order_status = original_order_status
        self.assertFalse(result)

    def test_order_status(self):
        # this order_status is on other modules, but since it used heavily in this helper class,
        # we add a functional test for this method here
        with self.on_session(self.order2):
            original_order_status = self.order2.order_status

            info = order_status(order=self.order2, session=self.session2,
                                order_id=self.order2.id, order_status=DELIVERY_IN_PROCESS,
                                )
            self.assertEqual(info, 'OK')
            self.assertEqual(self.order2.order_status, DELIVERY_IN_PROCESS)

            info = order_status(order_id=self.order2.id, order_status=COMPLETE)
            self.assertEqual(info, 'OK')
            saved_order = self.session2.query(Order).get(self.order2.id)
            self.assertEqual(saved_order.order_status, COMPLETE)
