from __future__ import absolute_import, unicode_literals

from unittest import TestCase

from babel.dates import format_time, format_date

from app import app
from app.payments.helpers import authorize_send_email_finnet, authorize_send_email_indomaret
from app.utils.datetimes import get_local
from tests.fixtures.helpers import mock_smtplib


class SendEmailTests(TestCase):

    def test_send_email_finnet(self):
        with app.test_request_context('/'):
            # comment this mock_stmplib if u want to really test sending email to mailtrap.io
            # or other smtp configured in config.py/local_config.py
            mock_smtplib()

            actual = authorize_send_email_finnet(
                amount=50000,
                items=[
                    {
                        "name": "Item ABC",
                        "price": 30000
                    },
                    {
                        "name": "Item CDE",
                        "price": 20000
                    }],
                payment_code="sdfsadf12323213dffsd",
                order_id=100,
                payer_id='test@customer-email.com'
            )
            self.assertIsNone(actual)

    def test_send_email_indomaret(self):
        # comment this mock_stmplib if u want to really test sending email to mailtrap.io
        # or other smtp configured in config.py/local_config.py
        mock_smtplib()

        with app.test_request_context('/'):
            expired_date = format_date(get_local(), format='full', locale='id') + \
                           ', ' + format_time(get_local(), "HH:mm:ss")

            actual = authorize_send_email_indomaret(
                amount=50000,
                items=[
                    {
                        "name": "Item ABC",
                        "price": 30000
                    },
                    {
                        "name": "Item CDE",
                        "price": 20000
                    }],
                expired_date=expired_date,
                payment_code="sdfsadf12323213dffsd",
                order_id=100,
                payer_id='test@customer-email.com'
            )
            self.assertIsNone(actual)
