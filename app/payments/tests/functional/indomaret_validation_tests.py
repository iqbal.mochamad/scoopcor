from datetime import datetime
from httplib import OK
from unittest import SkipTest
from unittest import TestCase

import mock
from faker import Factory
from flask import g
from nose.tools import nottest, istest

from app import db, app, set_request_id
from app.auth.models import UserInfo
from app.items.choices import STATUS_READY_FOR_CONSUME
from app.items.choices import STATUS_TYPES
from app.offers.models import Offer
from app.offers.models import OfferType
from app.orders.choices import COMPLETE
from app.orders.choices import NEW
from app.orders.models import Order
from app.payment_gateways.models import PaymentGateway
from app.payments.api import ValidateIndomaretPaymentApi
from app.payments.choices import AUTHORIZE_VALIDATE
from app.payments.choices.gateways import INDOMARET
from app.payments.models import Payment
from app.payments.payment_status import PAYMENT_BILLED
from app.users.users import User
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import ItemFactory
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.master import CurrencyFactory, BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.offers import OfferTypeFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, PaymentGatewayFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.payments import PaymentFactory, PaymentVeritransFactory


@nottest
def mocked_get_csstore_transaction_status(*args, **kwargs):
    class MockedResponse(object):

        def __init__(self, status):
            self.transaction_status = status
            self.status_code = 200

    return MockedResponse('settlement')


class ValidateIndomaretPaymentApiTests(TestBase):
    """ Functional Test for Veritrans Indomaret Payment Gateway
    Callback from veritrans: when customer paid, veritrans will hit this api to validate the payment in our system
    """

    @classmethod
    @mock.patch('app.payments.api.get_csstore_transaction_status',
                mock.Mock(side_effect=mocked_get_csstore_transaction_status))
    def setUpClass(cls):
        super(ValidateIndomaretPaymentApiTests, cls).setUpClass()
        cls.init_data(cls.session)

        # VERITRANS_V2_SERVER_KEY = 'VT-server-W9y3smYZf3cRhIxJL5WkY0Tj' (set in local_config.py)

        # execute method to test
        g.current_user = cls.session.query(User).get(USER_ID)
        cls.response = ValidateIndomaretPaymentApi().validate_indomaret_payment(
            payment_code=cls.payment_code,
            order_id=cls.order_id,
            payment_status='settlement'
        )
        # is_mock_functional_test=True: this is functional test, so we don't really need to submit to veristrans api
        # mock it, set is_mock_functional_test to True

    def test_response_status(self):
        self.assertEqual(self.response.status_code, OK,
                         'Response status code is not {status}. Response: {response}'.format(
                             status=OK, response=str(self.response)))

    def test_updated_payment_status(self):
        session = db.session()
        payment = session.query(Payment).get(self.order_id)
        self.assertEquals(payment.payment_status, PAYMENT_BILLED)

    def test_updated_order_status(self):
        session = db.session()
        order = session.query(Order).get(self.order_id)
        self.assertEquals(order.order_status, COMPLETE)

    @classmethod
    def init_data(cls, session):
        pg_gateway = session.query(PaymentGateway).get(INDOMARET)
        offer_type = session.query(OfferType).get(Offer.Type.single.value)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        brand = BrandFactory()
        item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)
        cls.user = UserFactory(id=USER_ID, username='cortesting@apps-foundry.com', email='cortesting@apps-foundry.com')

        # generate unique order id to prevent error from veritrans in end to end test
        cls.order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        order = OrderFactory(id=cls.order_id, paymentgateway=pg_gateway,
                             total_amount=50000, final_amount=50000,
                             user_id=USER_ID, party=cls.user,
                             order_status=NEW)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=USER_ID, currency_code='IDR',
                                      price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                      orderline_status=NEW)
        # give payment id same as order id, just for test purposes
        payment = PaymentFactory(id=cls.order_id, order=order, user_id=order.user_id)
        cls.payment_code = '21234'
        payment_veritrans = PaymentVeritransFactory(payment=payment,
                                                    transaction_id=cls.order_id, payment_code=cls.payment_code)
        session.commit()

USER_ID = 12345
_fake = Factory.create()
