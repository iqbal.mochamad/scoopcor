from unittest import TestCase




class PostDataRequestSerializationTests(TestCase):
    pass



# Fixtures
single_item_postdata_request = """
<faspay>
  <merchant>SCOOPONLINE</merchant>
  <bill_no>8879107313104266</bill_no>
  <bill_currency>IDR</bill_currency>
  <pay_type>01</pay_type>
  <bill_tax>0</bill_tax>
  <bill_expired>2015-08-22 16:12:04.381434</bill_expired>
  <bill_total>1900000</bill_total>
  <terminal>10</terminal>
  <item>
    <product>KOKI / ED 311 AUG 2015</product>
    <payment_plan>01</payment_plan>
    <qty>1</qty>
    <amount>900000</amount>
    <tenor>00</tenor>
    <merchant_id>31140</merchant_id>
  </item>


  <payment_channel>303</payment_channel>
  <bill_desc>Pembelian di SCOOP</bill_desc>
  <bill_miscfee>0</bill_miscfee>
  <signature>58d42e15a5ef7f46986cfd59c6ab6fe745420df7</signature>
  <bill_reff>8879107313104266</bill_reff>
  <merchant_id>31140</merchant_id>
  <bill_gross>1900000</bill_gross>
  <email />
  <bill_date>2015-08-21 16:12:04.381424</bill_date>
</faspay>
"""

multi_item_postdata_request = """
<faspay>
  <merchant>SCOOPONLINE</merchant>
  <bill_no>8879107313104266</bill_no>
  <bill_currency>IDR</bill_currency>
  <pay_type>01</pay_type>
  <bill_tax>0</bill_tax>
  <bill_expired>2015-08-22 16:12:04.381434</bill_expired>
  <bill_total>1900000</bill_total>
  <terminal>10</terminal>
  <item>
    <product>KOKI / ED 311 AUG 2015</product>
    <payment_plan>01</payment_plan>
    <qty>1</qty>
    <amount>900000</amount>
    <tenor>00</tenor>
    <merchant_id>31140</merchant_id>
  </item>
  <item>
    <product>OTOPLUS / ED 08 2015</product>
    <payment_plan>01</payment_plan>
    <qty>1</qty>
    <amount>1000000</amount>
    <tenor>00</tenor>
    <merchant_id>31140</merchant_id>
  </item>


  <payment_channel>303</payment_channel>
  <bill_desc>Pembelian di SCOOP</bill_desc>
  <bill_miscfee>0</bill_miscfee>
  <signature>58d42e15a5ef7f46986cfd59c6ab6fe745420df7</signature>
  <bill_reff>8879107313104266</bill_reff>
  <merchant_id>31140</merchant_id>
  <bill_gross>1900000</bill_gross>
  <email />
  <bill_date>2015-08-21 16:12:04.381424</bill_date>
</faspay>

"""
