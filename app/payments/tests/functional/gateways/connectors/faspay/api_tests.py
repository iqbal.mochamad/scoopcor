from datetime import datetime
from httplib import OK
import unittest

from flask import Flask, url_for, g
from mock import patch

from app import app, db, set_request_id
from tests.fixtures.helpers import generate_static_sql_fixtures

from tests.fixtures.sqlalchemy.payments.gateways.connectors import PaymentBCAKlikpayFactory
from tests.fixtures.sqlalchemy.payments import PaymentFactory


class ScoopDatabaseTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.original_funcs = app.before_request_funcs
        app.before_request_funcs = {
            None: [
                set_request_id,
            ]
        }

    @classmethod
    def tearDownClass(cls):
        app.before_request_funcs = cls.original_funcs


class CheckSignatureTests(ScoopDatabaseTestCase):

    @classmethod
    def _get_url(cls):
        with app.test_request_context(""):
            url = url_for('faspay.validate_authkey_signature')
        return url

    client = app.test_client(use_cookies=False)

    # self.hash_gen = BCASignatureGen(
    #         klikpaycode=123,
    #         transaction_no=456,
    #         currency="IDR",
    #         keyid="12345678901234561234567890123456",
    #         transaction_date=datetime(
    #             year=2010, month=1, day=20, hour=1, minute=1, second=1
    #         ).strftime('%d/%m/%Y %H:%M:%S'),
    #         total_amount=1500500.00
    #     )

    @classmethod
    @patch('app.paymentgateway_connectors.faspay.api.current_app', spec=Flask)
    def setUpClass(cls, mock_app):
        app.before_request_funcs = {
            None: [
                set_request_id,
            ]
        }

        super(CheckSignatureTests, cls).setUpClass()

        # try:
        # Test settings are pulled exactly from the documentation for FASPAY.
        mock_app.config = {
            'KLIKPAYCODE': 123,
            'CLEAR_KEY': '12345678901234561234567890123456'
        }

        payment = PaymentFactory(
            amount=1500500.00,
            currency_code="IDR",
            payment_datetime=datetime(year=2010, month=1, day=20,
                                      hour=1, minute=1, second=1)
        )
        payment_detail = PaymentBCAKlikpayFactory(
            mi_trx_id=456,
            purchase_date=payment.payment_datetime,
            payment=payment
        )

        s = db.session()
        s.add(payment_detail)
        s.commit()

        cls.response = cls.client.get(
            cls._get_url() + "?signature=1913180747&trx_id=456"
        )
        # except Exception as e:

    def test_response_code(self):
        self.assertEqual(OK, self.response.status_code)

    def test_content_type(self):
        self.assertEqual('text/plain', self.response.content_type)

    def test_response_body(self):
        self.assertEqual("1", self.response.data)


class CheckAuthkeyTests(unittest.TestCase):
    pass


class CheckAuthkeySignatureSADTests(unittest.TestCase):
    pass


original_before_request_funcs = app.before_request_funcs


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()
    session = db.session()
    generate_static_sql_fixtures(session)
    session.commit()


def teardown_module():
    app.before_request_funcs = original_before_request_funcs
    db.session().close_all()
    db.drop_all()

