import json
import os
from httplib import BAD_REQUEST

from unittest import TestCase

from datetime import datetime

from googleapiclient.errors import HttpError
from mock import patch, Mock
from nose.tools import nottest

from app import app
from app.paymentgateway_connectors.inapbilling.billing import GoogleIABCheckout
from app.payments.choices.gateways import IAB
from app.payments.signature_generator import GenerateSignature

USER_ID = 12345


class GoogleIabTests(TestCase):
    """
    Google docs: https://developers.google.com/android-publisher/api-ref/purchases/products/get
    """
    @classmethod
    def setUpClass(cls):
        # generate unique order id to prevent error from veritrans in end to end test
        # to prevent error 406 already exists from veritrans
        cls.order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        cls.developer_payload = '7c43d0614e2872d62f69c359b10338eeb1dbe61cec902fc79f6ff587b49d84b6'
        cls.purchase_token = 'plfaibkadeblebpbgnnkokij.AO-J1OyMsmaTWyoyyrtT0C0Lav7e3PFNfNOf_A0vl4MCgcX0' \
                         '3G_GqyrqGP6wNPqVEDhJYblGQFYRjcCnb_HhF0eldwqeqSGx_6TUNUqot13lHFxvFh3AGuxWSn' \
                         'RfqM6wUrTdrO9I9Tl571uSBHvKaZacILb1AG4L_Q'
        cls.package_name = 'com.appsfoundry.scoop'
        cls.product_sku = 'com.appsfoundry.scoop.c.usd.2.99'
        cls.args = {
            "order_id": cls.order_id,
            "signature": GenerateSignature(order_id=cls.order_id).construct(),
            "user_id": USER_ID,
            "payment_gateway_id": IAB,
            "payment_id": cls.order_id,
            "purchase_token": cls.purchase_token,
            "developer_payload": cls.developer_payload,
            "product_sku": cls.product_sku,
        }

        with app.test_request_context('/'):
            file = app.config['GOOGLE_OAUTH_CREDENTIALS_FILE']
            if not os.path.isfile(file):
                f = open(file, 'w')
                f.write('{"_module": "oauth2client.client", "scopes": [], "token_expiry": "2016-10-31T05:34:58Z", "id_token": null, "access_token": "ya29.CiqKA1jbcHaN-EejB-ejMm9D-tmtk5e_6TCtHpRYwSwNJFD3wqgjb3dqH4Q", "token_uri": "https://accounts.google.com/o/oauth2/token", "invalid": false, "token_response": {"access_token": "ya29.CiqKA1jbcHaN-EejB-ejMm9D-tmtk5e_6TCtHpRYwSwNJFD3wqgjb3dqH4Q", "token_type": "Bearer", "expires_in": 3600}, "client_id": "1082882231704-5gl237c0gtumat7qkc2u71t74o3qfhil.apps.googleusercontent.com", "token_info_uri": null, "client_secret": "hXC9Y41ZTWap10VE6WyhHHmm", "revoke_uri": "https://accounts.google.com/o/oauth2/revoke", "_class": "OAuth2Credentials", "refresh_token": "1/5IizWAHMW7tQNFbJV4y2W5YBvKU5V65C6FbaGY85iLQ", "user_agent": null}')
                f.close()

        cls.google_iab = GoogleIABCheckout(
            order_id=cls.order_id,
            payment_id=cls.order_id,
            args=cls.args,
            developer_payload=cls.developer_payload,
            purchase_token=cls.purchase_token,
            product_sku=cls.product_sku,
        )

    @patch('app.paymentgateway_connectors.inapbilling.billing.'
           'GoogleIABCheckout.check_token_from_google')
    @patch('googleapiclient.http.HttpRequest.execute')
    def test_confirm_google_payment(self, mocked_google_http_request_execute,
                                    mocked_check_token_from_google):
        expected_google_response = {
            u'consumptionState': 1,
            u'purchaseState': 0,
            u'kind': u'androidpublisher#inappPurchase',
            u'developerPayload': u'7c43d0614e2872d62f69c359b10338eeb1dbe61cec902fc79f6ff587b49d84b6',
            u'purchaseTime': u'1477653656970'}
        mocked_check_token_from_google.return_value = True
        mocked_google_http_request_execute.return_value = expected_google_response

        with app.test_request_context('/'):
            status, response = self.google_iab.confirm_google_payment()
        self.assertEqual(status, True)
        self.assertDictEqual(response, expected_google_response)

    def test_confirm_google_payment_json_file_not_found(self):
        with app.test_request_context('/'):
            original_file_path = app.config['GOOGLE_OAUTH_CREDENTIALS_FILE']
            app.config['GOOGLE_OAUTH_CREDENTIALS_FILE'] = 'wrongfile.json'

            status, response = self.google_iab.confirm_google_payment()

            # restore original path for other tests
            app.config['GOOGLE_OAUTH_CREDENTIALS_FILE'] = original_file_path

            self.assertEqual(status, False)
            self.assertEqual(response.status_code, BAD_REQUEST)
            self.assertIn("Failed to get Google OAuth Credential from File",
                          json.loads(response.data).get('developer_message', None))

    @patch('app.paymentgateway_connectors.inapbilling.billing.'
           'GoogleIABCheckout.check_token_from_google')
    @patch('googleapiclient.http.HttpRequest.execute')
    def test_confirm_google_payment_failed(self, mocked_google_http_request_execute,
                                    mocked_check_token_from_google):
        content_error = {
            u'error': {
                u'code': 400,
                u'errors': [{
                    u'domain': u'androidpublisher',
                    u'location': u'token',
                    u'locationType': u'parameter',
                    u'message': u'The purchase token does not match the product ID.',
                    u'reason': u'purchaseTokenDoesNotMatchProductId'}],
                u'message': u'The purchase token does not match the product ID.'}}
        uri = u'https://www.googleapis.com/androidpublisher/v1.1/applications/com.appsfoundry.scoop/' \
              u'inapp/android.test.purchased/purchases/plfaibkadeblebpbgnnkokij.AO-J1OyMsmaTWyoyyrtT0' \
              u'C0Lav7e3PFNfNOf_A0vl4MCgcX03G_GqyrqGP6wNPqVEDhJYblGQFYRjcCnb_HhF0eldwqeqSGx_' \
              u'6TUNUqot13lHFxvFh3AGuxWSnRfqM6wUrTdrO9I9Tl571uSBHvKaZacILb1AG4L_Q?alt=json'
        mock_response_error = Mock()
        mock_response_error.reason = None
        mock_response_error.status = 400
        http_error = HttpError(
            resp=mock_response_error, content=json.dumps(content_error), uri=uri)
        mocked_google_http_request_execute.side_effect = http_error
        mocked_check_token_from_google.return_value = True

        with app.test_request_context('/'):
            status, response = self.google_iab.confirm_google_payment()
            self.assertEqual(status, False)
            self.assertEqual(response.status_code, BAD_REQUEST)
            self.assertIn("Failed to get Confirmation from Google",
                          json.loads(response.data).get('developer_message', None))
