import json
from httplib import OK
from unittest import TestCase

from datetime import datetime

import httpretty
import requests
from flask import g
from mock import patch
from nose.tools import nottest

from app import db, app, set_request_id
from app.auth.models import UserInfo
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.orders.choices import NEW
from app.payments.choices.gateways import POINT, KREDIVO
from app.payments.signature_generator import GenerateSignature
from app.services.kredivo.kredivo_gateway import Kredivo
from app.users.users import User
from app.payments.choices import CHARGED
from tests.fixtures.sqlalchemy import ItemFactory
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.master import CurrencyFactory, BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferTypeFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.payments.gateways import PaymentGatewayFactory


class KredivoPaymentTests(TestCase):
    """Functional Test for Payment with Kredivo - Authorize
    Submit order payment to kredivo (kredivo checkout)

    """

    original_before_request_funcs = None
    maxDiff = None

    @classmethod
    @httpretty.activate
    def get_api_response(cls):
        # prepare httppretty to mock kredivo checkout response
        #   (mock request.post response from kredivo)
        krevido = Kredivo('dfkljsklfjs', requests, True)
        httpretty.register_uri(httpretty.POST, krevido._build_uri("/v2/checkout_url"),
                               body=cls.get_mocked_kredivo_checkout_response(),
                               status=200)

        header_data = {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'Content-Type': 'application/json',
        }
        client = app.test_client(use_cookies=False)
        request_body = cls.get_request_body_payment()
        return client.post('/v1/payments/authorize',
                           data=json.dumps(request_body),
                           headers=header_data)

    @classmethod
    def setUpClass(cls):
        db.drop_all()
        db.create_all()

        cls.init_data()
        cls.original_before_request_funcs = app.before_request_funcs
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }

        cls.response = cls.get_api_response()
        cls.response_dict = json.loads(cls.response.data)

        """Response example:
        {
          "order_id": 817464,
          "payment_id": 753685,
          "status": "OK",
          "redirect_url": "https://api.kredivo.com/kredivo/signin?tr_id=XXX"
        }
        """

    @classmethod
    def tearDownClass(cls):
        app.before_request_funcs = cls.original_before_request_funcs
        db.session().close_all()
        db.drop_all()

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, OK,
                         'Response status is not OK. Response {}'.format(self.response.data))

    def test_response_body_status(self):
        self.assertEqual(self.response_dict.get('status', None), "OK")

    def test_response_payment_id(self):
        self.assertIsNotNone(self.response_dict.get('payment_id', None))

    def test_response_redirect_url(self):
        self.assertIsNotNone(self.response_dict.get('redirect_url', None))

    def test_response_order_id(self):
        self.assertEqual(self.order_id, self.response_dict.get('order_id', None))

    @staticmethod
    def get_mocked_kredivo_checkout_response():
        return json.dumps({
            "status": "OK",
            "message": "Message if any",
            "redirect_url": "https://api.kredivo.com/kredivo/signin?tr_id=XXX"
        })

    @classmethod
    def get_request_body_payment(cls):
        return {
            "order_id": cls.order_id,
            "signature": GenerateSignature(order_id=cls.order_id).construct(),
            "user_id": USER_ID,
            "payment_gateway_id": KREDIVO
        }

    @classmethod
    def init_data(cls):
        s = db.session()
        s.expire_on_commit = False

        cur_idr = CurrencyFactory(id=2, iso4217_code='IDR')
        cur_pts = CurrencyFactory(id=3, iso4217_code='PTS')
        pg_indomaret = PaymentGatewayFactory(id=KREDIVO, base_currency=cur_idr, payment_flow_type=CHARGED,
                                             minimal_amount=1)
        pg_point = PaymentGatewayFactory(id=POINT, base_currency=cur_pts, payment_flow_type=CHARGED,
                                         minimal_amount=1)

        offer_type = OfferTypeFactory(id=1)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        brand = BrandFactory()
        item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)

        cls.user = UserFactory(id=USER_ID, username='cortesting@apps-foundry.com', email='cortesting@apps-foundry.com')
        # generate unique order id to prevent error from veritrans in end to end test
        # to prevent error 406 already exists from veritrans
        cls.order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        order = OrderFactory(id=cls.order_id, paymentgateway=pg_indomaret,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             user_id=USER_ID, party=cls.user,
                             order_status=NEW)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=USER_ID, currency_code='IDR',
                                      price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                      orderline_status=NEW)
        s.commit()


USER_ID = 12345


def init_g_current_user():
    session = db.session()
    g.current_user = session.query(User).get(USER_ID)
    g.user = UserInfo(username=g.current_user.username, user_id=USER_ID, token='abcd', perm=['can_read_write_global_all',])
    session.close()
