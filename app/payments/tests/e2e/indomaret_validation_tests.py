import json
from datetime import datetime
from httplib import OK

import mock
from flask import g
from nose.tools import nottest

from app import db, app
from app.auth.models import UserInfo
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.items.models import UserItem
from app.offers.models import Offer
from app.offers.models import OfferType
from app.orders.choices import NEW, COMPLETE
from app.orders.models import Order
from app.payment_gateways.models import PaymentGateway
from app.payments.choices.gateways import POINT, INDOMARET
from app.payments.models import Payment
from app.payments.payment_status import PAYMENT_BILLED, WAITING_FOR_PAYMENT
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from app.utils.datetimes import datetime_to_isoformat
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.payments import PaymentFactory
from tests.fixtures.sqlalchemy.payments import PaymentVeritransFactory


class MockedResponse(object):
    def __init__(self, status):
        self.transaction_status = status
        self.status_code = 200


@nottest
def mocked_get_csstore_transaction_status(*args, **kwargs):

    return MockedResponse('settlement')


@nottest
def mocked_get_csstore_transaction_status_pending(*args, **kwargs):

    return MockedResponse('pending')


@nottest
def mocked_get_csstore_transaction_status_expire(*args, **kwargs):

    return MockedResponse('expire')


class ValidateIndomaretPaymentApiBase(TestBase):

    @classmethod
    def init_data(cls, s):
        pg_indomaret = s.query(PaymentGateway).get(INDOMARET)
        pg_point = s.query(PaymentGateway).get(POINT)
        offer_type = s.query(OfferType).get(Offer.Type.single.value)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        brand = BrandFactory()
        cls.item_id = 1
        item1 = ItemFactory(id=cls.item_id, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)

        cls.user = UserFactory(id=USER_ID, username='cortesting@apps-foundry.com', email='cortesting@apps-foundry.com')
        # generate unique order id to prevent error from veritrans in end to end test
        # to prevent error 406 already exists from veritrans
        cls.order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        order = OrderFactory(id=cls.order_id, paymentgateway=pg_indomaret,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             user_id=USER_ID, party=cls.user,
                             order_status=WAITING_FOR_PAYMENT, currency_code=pg_indomaret.base_currency.iso4217_code)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=USER_ID,
                                      currency_code=pg_indomaret.base_currency.iso4217_code,
                                      price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                      orderline_status=NEW)
        # give payment id same as order id, just for test purposes
        payment = PaymentFactory(id=cls.order_id, order=order, user_id=order.user_id,
                                 payment_status=WAITING_FOR_PAYMENT)
        cls.payment_code = '21234'
        payment_veritrans = PaymentVeritransFactory(payment=payment,
                                                    transaction_id=cls.order_id, payment_code=cls.payment_code)

        s.commit()

    @classmethod
    def get_request_body_payment(cls, transaction_status='settlement'):
        return {
            "status_code": "200",
            "status_message": "Veritrans payment notification",
            "transaction_id": "93c6a0ff-b270-4b00-adba-f1738ff90261",
            "order_id": str(cls.order_id),
            "payment_type": "cstore",
            "transaction_time": datetime_to_isoformat(datetime.now()),
            "transaction_status": transaction_status,
            "payment_code": cls.payment_code,
            "signature_key": "4533cbaf612267f5da81a17ff8ab8b86d39de8a1877e6125f39782baac3fe211469532f20e7bf5cc88eb51dfc36eca098578503c3cc5a880a0245e733579afc3",
            "gross_amount": 50000
        }


class ValidateIndomaretPaymentApiE2ETests(ValidateIndomaretPaymentApiBase):
    """End to End test for Payment with Indomaret (Veritrans) - Validation
    After customer paid in indomaret, veritrans will hit this API.
    'v1/payments/veritrans/cstore/validation'
    here we test it

    make sure app.config['VERITRANS_V2_SERVER_KEY'] contain key from vertirans sandbox
    """

    maxDiff = None

    @classmethod
    @mock.patch('app.payments.api.get_csstore_transaction_status',
                mock.Mock(side_effect=mocked_get_csstore_transaction_status))
    def setUpClass(cls):
        super(ValidateIndomaretPaymentApiE2ETests, cls).setUpClass()
        mock_smtplib()
        cls.init_data(cls.session)

        cls.set_api_authorized_user(cls.super_admin)
        request_body = cls.get_request_body_payment()
        cls.response = cls.api_client.post('/v1/payments/veritrans/cstore/validation',
                                           data=json.dumps(request_body),
                                           headers=cls.build_headers(has_body=True))
        cls.response_dict = json.loads(cls.response.data)

    def test_response_status(self):
        self.assertEqual(self.response.status_code, OK,
                         'Response status code {0.status_code} != 200(OK). Response {0.data}'.format(self.response))

    def test_response_order_id(self):
        self.assertEqual(self.order_id, self.response_dict.get('order_id', None))

    def test_order_status(self):
        # assert order status and order line status
        order = Order.query.get(self.order_id)
        self.assertEqual(COMPLETE, order.order_status)
        for line in order.order_lines.all():
            self.assertEqual(
                COMPLETE, line.orderline_status,
                'Order line status not completed. Line id: {0.id}, status: {0.orderline_status}'.format(
                    line)
            )

    def test_updated_payment_status(self):
        session = db.session()
        payment = session.query(Payment).get(self.order_id)
        self.assertEquals(payment.payment_status, PAYMENT_BILLED)

    def test_updated_order_status(self):
        session = db.session()
        order = session.query(Order).get(self.order_id)
        self.assertEquals(order.order_status, COMPLETE)

    def test_saved_user_owned_item(self):
        session = db.session()
        user_owned_item = session.query(UserItem).filter_by(user_id=USER_ID, item_id=self.item_id).first()
        self.assertEquals(user_owned_item.user_id, USER_ID)


class NotificationPendingTests(ValidateIndomaretPaymentApiBase):
    """End to End test for Payment with Indomaret (Veritrans) - Validation
    After we submit order into indomaret, veritrans will hit this API to send notification.
    'v1/payments/veritrans/cstore/validation'
    here we test it
        Request Body from Veritrans: Status order is still "pending"
    make sure app.config['VERITRANS_V2_SERVER_KEY'] contain key from vertirans sandbox
    """

    maxDiff = None

    @classmethod
    @mock.patch('app.payments.api.get_csstore_transaction_status',
                mock.Mock(side_effect=mocked_get_csstore_transaction_status_pending))
    def setUpClass(cls):
        super(NotificationPendingTests, cls).setUpClass()
        mock_smtplib()

        cls.init_data(cls.session)

        cls.set_api_authorized_user(cls.super_admin)
        request_body = cls.get_request_body_payment(transaction_status='pending')
        cls.response = cls.api_client.post('/v1/payments/veritrans/cstore/validation',
                                           data=json.dumps(request_body),
                                           headers=cls.build_headers(has_body=True))

    def test_response_status(self):
        self.assertEqual(self.response.status_code, OK,
                         'Response status code {0.status_code} != 200(OK). Response {0.data}'.format(self.response))


class NotificationExpireTests(ValidateIndomaretPaymentApiBase):
    """End to End test for Payment with Indomaret (Veritrans) - Validation
    After we submit order into indomaret, veritrans will hit this API to send notification.
    'v1/payments/veritrans/cstore/validation'
    here we test it
        Request Body from Veritrans: Status order is "expire"

    make sure app.config['VERITRANS_V2_SERVER_KEY'] contain key from vertirans sandbox
    """

    maxDiff = None

    @classmethod
    @mock.patch('app.payments.api.get_csstore_transaction_status',
                mock.Mock(side_effect=mocked_get_csstore_transaction_status_expire))
    def setUpClass(cls):
        super(NotificationExpireTests, cls).setUpClass()
        mock_smtplib()

        cls.init_data(cls.session)

        cls.set_api_authorized_user(cls.super_admin)
        request_body = cls.get_request_body_payment(transaction_status='expire')
        cls.response = cls.api_client.post('/v1/payments/veritrans/cstore/validation',
                                           data=json.dumps(request_body),
                                           headers=cls.build_headers(has_body=True))

    def test_response_status(self):
        self.assertEqual(self.response.status_code, OK,
                         'Response status code {0.status_code} != 200(OK). Response {0.data}'.format(self.response))


USER_ID = 12345
