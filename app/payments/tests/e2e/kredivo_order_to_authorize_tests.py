import json
from datetime import timedelta, datetime
from httplib import OK, CREATED

import httpretty

from app import db, app
from app.eperpus.tests.fixtures import SharedLibraryFactory
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.offers.models import Offer
from app.offers.models import OfferType
from app.payment_gateways.models import PaymentGateway
from app.payments.choices.gateways import FREE, POINT, KREDIVO
from app.payments.signature_generator import GenerateSignature
from app.services.kredivo.helpers import build_kredivo_gateway
from app.users.tests.fixtures import UserFactory
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory


class OrderPaymentWithKredivoTests(TestBase):
    """ end to end test for Order API until payment with Kredivo-Authorize API
    From order checkout api - order confirm api to payment authorize api with Kredivo as payment gateway

    we mocked the response when doing checkout to kredivo api

    for full test to kredivo api, see integration tests folder
    """
    @classmethod
    def setUpClass(cls):
        super(OrderPaymentWithKredivoTests, cls).setUpClass()
        cls.init_data(cls.session)

    @classmethod
    def init_data(cls, s):
        pg_free = s.query(PaymentGateway).get(FREE)
        pg_point = s.query(PaymentGateway).get(POINT)
        pg_kredivo = s.query(PaymentGateway).get(KREDIVO)
        offer_type = s.query(OfferType).get(Offer.Type.single.value)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        brand = BrandFactory()
        item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)
        cls.library_org = SharedLibraryFactory(
            id=11,
            user_concurrent_borrowing_limit=10,
            reborrowing_cooldown=timedelta(days=0),
            borrowing_time_limit=timedelta(days=7))
        cls.user = UserFactory(id=12345)

        # set last sequence id for Order to unique number
        # to prevent error 406 already exists from kredivo
        last_order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        db.engine.execute("select setval('core_orders_id_seq', {}, true)".format(last_order_id))
        db.engine.execute("select setval('core_payments_id_seq', {}, true)".format(last_order_id))

        s.commit()

    @httpretty.activate
    def test_order_payment_with_kredivo(self):
        mock_smtplib()

        # define mocked patch:
        with app.test_request_context('/'):
            # here we mock the response from kredivo when we hit POST checkout API
            matched_url_to_mock = build_kredivo_gateway()._build_uri('/v2/checkout_url')
            httpretty.register_uri(
                httpretty.POST,
                matched_url_to_mock,
                body=json.dumps({
                    "status": "OK",
                    "message": "mocked response",
                    "redirect_url": "https://api.kredivo.com/kredivo/signin?tr_id=XXX"
                }))

            # get request body for order
            request_body = self.get_request_body_checkout()
            # first, do order checkout
            self.set_api_authorized_user(self.user)
            response = self.api_client.post('/v1/orders/checkout',
                                            data=json.dumps(request_body),
                                            headers=self.build_headers(has_body=True))
            self.assertEqual(response.status_code, CREATED)
            if response.status_code == CREATED:
                response_checkout = json.loads(response.data)
                self.assertIsNotNone(response_checkout.get('order_number', None))
                self.assertIsNotNone(response_checkout.get('temp_order_id', None))

                # next, confirm order
                request_body = self.get_request_body_confirm(response_checkout)
                self.set_api_authorized_user(self.user)
                response = self.api_client.post('/v1/orders/confirm',
                                                data=json.dumps(request_body),
                                                headers=self.build_headers(has_body=True))
                self.assertEqual(response.status_code, CREATED)
                if response.status_code == CREATED:
                    response_confirm = json.loads(response.data)
                    order_id = response_confirm.get('order_id', None)
                    self.assertIsNotNone(order_id)

                    # finally, process payment (authorize payment)
                    request_body = self.get_request_body_payment(response_confirm)
                    self.set_api_authorized_user(self.user)
                    response = self.api_client.post('/v1/payments/authorize',
                                                    data=json.dumps(request_body),
                                                    headers=self.build_headers(has_body=True))
                    self.assertEqual(response.status_code, OK, response)
                    if response.status_code == OK:
                        response_charge = json.loads(response.data)
                        self.assertIsNotNone(response_charge)
                        self.assertIsNotNone(response_charge.get('payment_id', None))
                        self.assertIsNotNone(response_charge.get('redirect_url', None))

    @staticmethod
    def get_request_body_payment(response_confirm):
        return {
            "order_id": response_confirm.get('order_id', None),
            "signature": GenerateSignature(order_id=response_confirm.get('order_id', None)).construct(),
            "user_id": response_confirm.get('user_id', None),
            "payment_gateway_id": response_confirm.get('payment_gateway_id', None),
            "return_url": "http://getscoop.com/orders",
            "cancel_url": "http://getscoop.com/orders/cancel"
        }

    @staticmethod
    def get_request_body_confirm(response_checkout):
        return {
            "user_id": response_checkout.get('user_id', None),
            "payment_gateway_id": response_checkout.get('payment_gateway_id', None),
            "client_id": response_checkout.get('client_id', None),
            "currency_code": response_checkout.get('currency_code', None),
            "final_amount": response_checkout.get('final_amount', None),
            "order_number": response_checkout.get('order_number', None),
            "temp_order_id": response_checkout.get('temp_order_id', None),
        }

    def get_request_body_checkout(self):
        return {
            "user_id": self.user.id,
            "payment_gateway_id": KREDIVO,
            "client_id": 7,
            "platform_id": 4,
            "order_lines": [
                {
                    "offer_id": self.offer1.id,
                    "quantity": 1,
                    "name": self.offer1.name
                },
            ],
            "user_email": "ahmad.syafrudin@apps-foundry.com",
            "user_name": "",
            "user_street_address": "",
            "user_city": "Jakarta",
            "user_zipcode": "",
            "user_state": "",
            "user_country": "Indonesia",
            "latitude": -6.1744,
            "longitude": 106.8294,
            "ip_address": "202.179.188.106",
            "os_version": "Mac OS X",
            "device_model": "Chrome 51.0.2704.84",
            "client_version": "3.10.1"
        }

    @staticmethod
    def expected_checkout_response_example():
        return {
            "payment_gateway_name": "Free Purchase",
            "user_id": 11,
            "total_amount": "0.00",
            "partner_id": None,
            "is_active": True,
            "final_amount": "0.00",
            "payment_gateway_id": 3,
            "temp_order_id": 1,
            "client_id": 7,
            "order_status": 10000,
            "order_number": 78171068314826,
            "currency_code": "IDR",
            "order_lines": [
                {
                    "name": "streamline ubiquitous metrics",
                    "items": [
                        {
                            "is_featured": False,
                            "vendor": {
                                "slug": "amet-unde-odio",
                                "id": 1,
                                "name": "strategize cross-media e-commerce"
                            },
                            "name": "re-intermediate turn-key users",
                            "countries": [
                                {
                                    "name": "Indonesia",
                                    "id": 1
                                }
                            ],
                            "release_date": "1990-05-03T20:20:39",
                            "item_status": 9,
                            "languages": [
                                {
                                    "id": 1,
                                    "name": "English"
                                }
                            ],
                            "authors": [],
                            "id": 1,
                            "edition_code": "provident-repellat"
                        }
                    ],
                    "offer_id": 1,
                    "is_free": True,
                    "raw_price": "0.00",
                    "currency_code": "IDR",
                    "final_price": "0.00"
                }
            ]
        }
