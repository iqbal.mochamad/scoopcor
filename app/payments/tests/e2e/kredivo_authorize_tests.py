import json
from httplib import OK, CREATED
from unittest import TestCase

from datetime import timedelta, datetime

import httpretty
from flask import g
from nose.tools import nottest

from app import db, app, set_request_id
from app.auth.models import UserInfo
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.offers.models import Offer, OfferType
from app.orders.choices import NEW
from app.payment_gateways.models import PaymentGateway
from app.payments.choices.gateways import FREE, POINT, KREDIVO
from app.payments.signature_generator import GenerateSignature
from app.services.kredivo.helpers import build_kredivo_gateway
from app.users.users import User
from app.payments.choices import CHARGED, AUTHORIZE_CAPTURE
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from app.eperpus.tests.fixtures import SharedLibraryFactory
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.master import CurrencyFactory, BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferTypeFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.payments.gateways import PaymentGatewayFactory


USER_ID = 12345


class PaymentAuthorizeWithKredivoTests(TestBase):
    """ end to end test for Payment Authorized API with KREDIVO as payment gateway

    we mocked the response when doing checkout to kredivo api

    for full test to kredivo api, see integration tests folder
    """
    @classmethod
    def setUpClass(cls):
        super(PaymentAuthorizeWithKredivoTests, cls).setUpClass()
        cls.init_data(cls.session)

        cls.response = cls.get_api_response()

    @classmethod
    def init_data(cls, s):
        offer_type = s.query(OfferType).get(Offer.Type.single.value)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        brand = BrandFactory()
        item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)
        cls.library_org = SharedLibraryFactory(
            id=11,
            user_concurrent_borrowing_limit=10,
            reborrowing_cooldown=timedelta(days=0),
            borrowing_time_limit=timedelta(days=7))
        cls.user = UserFactory(id=USER_ID)

        pg_kredivo = s.query(PaymentGateway).get(KREDIVO)

        # set last sequence id for Order to unique number
        # to prevent error 406 already exists from kredivo
        last_order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        db.engine.execute("select setval('core_orders_id_seq', {}, true)".format(last_order_id))
        db.engine.execute("select setval('core_payments_id_seq', {}, true)".format(last_order_id))
        cls.order_id = last_order_id-1
        order = OrderFactory(id=cls.order_id, paymentgateway=pg_kredivo,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             user_id=USER_ID, party=cls.user,
                             order_status=NEW)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=USER_ID, currency_code='IDR',
                                      price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                      orderline_status=NEW)
        s.commit()

    @classmethod
    @httpretty.activate
    def get_api_response(cls):
        mock_smtplib()
        # define mocked patch:
        # here we mock the response from kredivo when we hit POST checkout API
        matched_url_to_mock = build_kredivo_gateway()._build_uri('/v2/checkout_url')
        httpretty.register_uri(
            httpretty.POST,
            matched_url_to_mock,
            body=json.dumps({
                "status": "OK",
                "message": "mocked response",
                "redirect_url": "https://api.kredivo.com/kredivo/signin?tr_id=XXX"
            }))
        cls.set_api_authorized_user(cls.user)
        # process payment (authorize payment to kredivo api)
        request_body = cls.get_request_body_payment(cls.order_id)
        return cls.api_client.post('/v1/payments/authorize',
                                   data=json.dumps(request_body),
                                   headers=cls.build_headers(has_body=True))

    def test_status_code(self):
        self.assertEqual(self.response.status_code, OK, self.response)

    def test_response_body(self):
        response = json.loads(self.response.data)
        self.assertIsNotNone(response)
        self.assertIsNotNone(response.get('payment_id', None))
        self.assertIsNotNone(response.get('redirect_url', None))

    @staticmethod
    def get_request_body_payment(order_id):
        return {
            "order_id": order_id,
            "signature": GenerateSignature(order_id=order_id).construct(),
            "user_id": USER_ID,
            "payment_gateway_id": KREDIVO,
            "return_url": "http://getscoop.com/orders",
            "cancel_url": "http://getscoop.com/orders/cancel"
        }
