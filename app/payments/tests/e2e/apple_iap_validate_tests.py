from __future__ import  absolute_import, unicode_literals

import json
from datetime import datetime, timedelta
from httplib import OK

import httpretty
from mock import patch

from app import db, app
from app.auth.models import Client
from app.items.choices import STATUS_READY_FOR_CONSUME
from app.items.choices import STATUS_TYPES
from app.items.models import UserItem
from app.master.models import Platform
from app.offers.models import OfferType, Offer
from app.orders.choices import COMPLETE
from app.orders.models import Order
from app.payment_gateways.models import PaymentGateway
from app.paymentgateway_connectors.apple_iap import iap_status
from app.paymentgateway_connectors.apple_iap.v3.helpers import get_apple_url
from app.paymentgateway_connectors.choices.renewal_status import NEW
from app.payments.choices import WAITING_FOR_PAYMENT, payment_statuses
from app.payments.choices.gateways import APPLE
from app.payments.models import Payment
from app.payments.signature_generator import GenerateSignature
from app.users.tests.fixtures import UserFactory
from app.utils.datetimes import get_local
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferBuffetFactory, OfferPlatformFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory


class AppleIapValidateTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(AppleIapValidateTests, cls).setUpClass()
        cls.init_data(cls.session)

        mock_smtplib()

    @classmethod
    def init_data(cls, s):
        pg = s.query(PaymentGateway).get(APPLE)
        offer_type = s.query(OfferType).get(Offer.Type.single.value)
        cls.offer1 = OfferFactory(id=43103, name='Scoop Premium',
                                  offer_type=offer_type, is_free=False, price_idr=89000, price_usd=6.99,
                                  price_point=1)
        brand = BrandFactory()
        cls.item_id = 1
        item1 = ItemFactory(id=cls.item_id, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)
        cls.offer1.brands.append(brand)
        offer_buffet = OfferBuffetFactory(
            available_from=datetime(2016, 10, 1),
            available_until=datetime.now() + timedelta(days=330),
            buffet_duration=timedelta(days=30),
            offer=cls.offer1
        )
        offer_platform = OfferPlatformFactory(
            offer=cls.offer1,
            platform=s.query(Platform).get(1),
            tier_id=1,
            tier_code='c.fake.code',
            currency='USD',
            price_usd=6.99,
            price_idr=89000,
            price_point=1,
            discount_price_usd=6.99,
            discount_price_idr=89000,
            discount_price_point=1
        )
        cls.client = s.query(Client).get(1)

        cls.user2 = UserFactory()
        s.add(cls.regular_user)
        s.add(cls.user2)
        s.add(cls.super_admin)
        s.commit()

        # generate unique order id to prevent error
        # order id for old receipt (IOS6)
        cls.order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        cls.order_id2 = int((datetime.now() - datetime(1970, 1, 1)).total_seconds()) + 1

        # create order for IOS6- validation process
        cls.create_order(pg, cls.order_id, cls.regular_user)
        cls.create_order(pg, cls.order_id2, cls.user2)
        # order id for testing new receipt (IOS7+, unified receipt)
        cls.order_id_unified_receipt = int((datetime.now() - datetime(1970, 1, 1)).total_seconds()) + 2
        # create order for testing new receipt (IOS7+, unified receipt) - validation process
        cls.order_unified = cls.create_order(pg, cls.order_id_unified_receipt, cls.super_admin)

        s.commit()

    @classmethod
    def create_order(cls, pg, order_id, user):
        order = OrderFactory(id=order_id, paymentgateway=pg,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             user_id=user.id, party=user,
                             tier_code='.c.usd.1.99',
                             order_status=WAITING_FOR_PAYMENT,
                             currency_code=pg.base_currency.iso4217_code,
                             client_id=cls.client.id)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=user.id,
                                      currency_code=pg.base_currency.iso4217_code,
                                      price=cls.offer1.price_usd, final_price=cls.offer1.price_usd,
                                      orderline_status=NEW)
        return order

    @httpretty.activate
    def test_validate_ios6_receipt_data(self):
        self.set_api_authorized_user(self.regular_user)

        request_body = {
            "is_sandbox": True,
            "order_id": self.order_id,
            "signature": GenerateSignature(order_id=self.order_id).construct(),
            "user_id": self.regular_user.id,
            "payment_gateway_id": APPLE,
            "unified_receipt_data": "",
            "receipt_data": "GHKR5ZWK45GII93X9CMN939123A5EYRUVVM",
        }

        # here we mock the response from apple ios when we hit their API (GET payment status api)
        matched_url_to_mock = "{}{}".format(app.config['APPLE_IAP_HOST_LIVE'],
                                            app.config['URL_VERIFY_RECEIPT'])

        httpretty.register_uri(
            httpretty.POST,
            matched_url_to_mock,
            body=self.get_mocked_ios6_apple_response())

        # run apple payment validate to test
        response = self.api_client.post(
            '/v1/payments/validate', data=json.dumps(request_body),
            headers=self.build_headers(has_body=True))
        httpretty.disable()
        httpretty.reset()
        session = db.session()
        self.assertEqual(response.status_code, OK, 'status_code != OK, response: {}'.format(response.data))
        self._validate_db_data(session, user=self.regular_user, order_id=self.order_id)

    @httpretty.activate
    def test_validate_ios6_receipt_data_twice(self):
        """ test validate for the same order id twice, the first validate error,
        then try again should success

        :return:
        """
        self.set_api_authorized_user(self.user2)

        request_body = {
            "is_sandbox": True,
            "order_id": self.order_id2,
            "signature": GenerateSignature(order_id=self.order_id2).construct(),
            "user_id": self.user2.id,
            "payment_gateway_id": APPLE,
            "unified_receipt_data": "",
            "receipt_data": "GHKR5ZWK45GII93X9CMN939123A5EYRUVVM",
        }

        # here we mock the response from apple ios when we hit their API (GET payment status api)
        matched_url_to_mock = "{}{}".format(app.config['APPLE_IAP_HOST_LIVE'],
                                            app.config['URL_VERIFY_RECEIPT'])

        original_transaction_id = "8881122334455"
        httpretty.register_uri(
            httpretty.POST,
            matched_url_to_mock,
            body=self.get_mocked_ios6_apple_response(
                status=iap_status.INVALID_JSON_OBJECT,
                original_transaction_id=original_transaction_id))

        # run apple payment validate for the first time and error
        response = self.api_client.post(
            '/v1/payments/validate', data=json.dumps(request_body),
            headers=self.build_headers(has_body=True))

        httpretty.register_uri(
            httpretty.POST,
            matched_url_to_mock,
            body=self.get_mocked_ios6_apple_response(original_transaction_id=original_transaction_id))

        # run apple payment validate again
        response = self.api_client.post(
            '/v1/payments/validate', data=json.dumps(request_body),
            headers=self.build_headers(has_body=True))
        session = db.session()
        httpretty.disable()
        httpretty.reset()
        self.assertEqual(response.status_code, OK, 'status_code != OK, response: {}'.format(response.data))
        self._validate_db_data(session, user=self.user2, order_id=self.order_id2)

    # @httpretty.activate
    @patch('app.paymentgateway_connectors.apple_iap.v3.helpers.get_receipt_from_itunes')
    def test_validate_ios7_unified_receipt_data(self, mock_get_receipt_from_itunes):
        # IOS7+
        self.set_api_authorized_user(self.super_admin)

        mock_smtplib()

        request_body = {
            "is_sandbox": True,
            "order_id": self.order_id_unified_receipt,
            "signature": GenerateSignature(order_id=self.order_id_unified_receipt).construct(),
            "user_id": self.super_admin.id,
            "payment_gateway_id": APPLE,
            "unified_receipt_data": "GHKR5ZWK45GII93X9CMN939123A5EYRUVVM",
            "receipt_data": ""
        }

        with self.on_session(self.client, self.order_unified):
            mock_ios7_receipt = self.get_mock_valid_ios7_unified_receipt_json(
                self.client,
                self.order_unified,
                dummy_trx_id="9912312312312")
        # here we mock the response from apple ios when we hit their API (GET payment status api)
        # matched_url_to_mock = "{}{}".format(app.config['APPLE_IAP_HOST_LIVE'],
        #                                     app.config['URL_VERIFY_RECEIPT'])
        # matched_url_to_mock = "{}{}".format(get_apple_url(), app.config['URL_VERIFY_RECEIPT'])
        # httpretty.register_uri(
        #     httpretty.POST,
        #     matched_url_to_mock,
        #     content_type='application/json',
        #     body=mock_ios7_receipt)
        mock_get_receipt_from_itunes.return_value = (mock_ios7_receipt, True)

        # run apple payment validate to test
        response = self.api_client.post(
            '/v1/payments/validate', data=json.dumps(request_body),
            headers=self.build_headers(has_body=True))
        httpretty.disable()
        httpretty.reset()

        self.assertEqual(response.status_code, OK)
        session = db.session()
        self._validate_db_data(session, user=self.super_admin, order_id=self.order_id_unified_receipt)

    def _validate_db_data(self, session, user, order_id):
        actual_owned_item = session.query(UserItem).filter_by(
            user_id=user.id, item_id=self.item_id
        ).first()
        actual_order = session.query(Order).get(order_id)
        actual_payment = session.query(Payment).filter_by(order_id=order_id).first()
        self.assertIsNotNone(actual_owned_item)
        self.assertEqual(actual_order.order_status, COMPLETE)
        self.assertEqual(actual_payment.payment_status, payment_statuses.PAYMENT_BILLED)

    @staticmethod
    def get_mocked_ios6_apple_response(status=iap_status.ACCEPTED, original_transaction_id=123345, is_trial=False):
        # ex: "2016-12-14 02:10:22"
        purchase_date = get_local().strftime('%Y-%m-%d %H:%M:%S')
        expired_date = (get_local() + timedelta(days=30)).strftime('%Y-%m-%d %H:%M:%S')
        return json.dumps({
            'status': status,
            'verification_rslt': {
                "status": status,
            },
            "receipt": {
                "is_trial_period": is_trial,
                "product_id": 'com.appsfoundry.scoop.c.usd.1.99',
                'original_purchase_date': purchase_date,
                'expires_date_formatted': expired_date,
                'purchase_date': purchase_date,
                'original_transaction_id': original_transaction_id,
                'web_order_line_item_id': "11{}".format(original_transaction_id),
                'transaction_id': 12345
            },
            "latest_receipt_info": {
                "is_trial_period": is_trial,
                "product_id": 'com.appsfoundry.scoop.c.usd.1.99',
                'original_purchase_date': purchase_date,
                'expires_date_formatted': expired_date,
                'purchase_date': purchase_date,
                'original_transaction_id': original_transaction_id,
                'web_order_line_item_id': "11{}".format(original_transaction_id),
                'transaction_id': 12345
            },
            "latest_receipt": "dkfjlkafjalsdkfsadf.sdfdslfkjsadlfsdalfjdslkfjdlksfsdklfjdklsfs"

        })

    def get_mock_valid_ios7_unified_receipt_json(self, client, order, dummy_trx_id="1231231321"):
        return {
            "status": 0,
            "environment": "Sandbox",
            "receipt": {
                "in_app": [
                    {
                        # "product_id": "com.appsfoundry.com.c.0.99.usd",
                        "product_id": "{}{}".format(
                            client.product_id,
                            order.tier_code
                        ),
                        "transaction_id": dummy_trx_id,
                        "original_transaction_id": dummy_trx_id,
                        "web_order_line_item_id": "{}123".format(dummy_trx_id),
                        "purchase_date": get_local().strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
                        "original_purchase_date": get_local().strftime("%Y-%m-%d %H:%M:%S Etc/GMT"),
                    },
                    # some dummy data
                    {
                        "quantity": "1",
                        "product_id": "com.appsfoundry.scoop.ars.SUB_1003MTH",
                        "transaction_id": "1000000166563547",
                        "original_transaction_id": "1000000166563403",
                        "purchase_date": "2015-08-06 07:14:31 Etc/GMT",
                        "purchase_date_ms": "1438845271000",
                        "purchase_date_pst": "2015-08-06 00:14:31 America/Los_Angeles",
                        "original_purchase_date": "2015-08-06 07:10:52 Etc/GMT",
                        "original_purchase_date_ms": "1438845052000",
                        "original_purchase_date_pst": "2015-08-06 00:10:52 America/Los_Angeles",
                        "expires_date": "2015-08-06 07:29:31 Etc/GMT",
                        "expires_date_ms": "1438846171000",
                        "expires_date_pst": "2015-08-06 00:29:31 America/Los_Angeles",
                        "web_order_line_item_id": "1000000030259814",
                        "is_trial_period": "false"
                    },
                    {
                        "quantity": "1",
                        "product_id": "com.appsfoundry.scoop.ID_MIIN2015MTH07DT0612",
                        "transaction_id": "1000000166162028",
                        "original_transaction_id": "1000000166162028",
                        "purchase_date": "2015-08-04 04:29:24 Etc/GMT",
                        "purchase_date_ms": "1438662564000",
                        "purchase_date_pst": "2015-08-03 21:29:24 America/Los_Angeles",
                        "original_purchase_date": "2015-08-04 04:29:24 Etc/GMT",
                        "original_purchase_date_ms": "1438662564000",
                        "original_purchase_date_pst": "2015-08-03 21:29:24 America/Los_Angeles",
                        "is_trial_period": "false"
                    },
                    {
                        "quantity": "1",
                        "product_id": "com.appsfoundry.scoop.c.usd.6.99",
                        "transaction_id": "1000000185071679",
                        "original_transaction_id": "1000000185071679",
                        "purchase_date": "2015-12-15 07:02:09 Etc/GMT",
                        "purchase_date_ms": "1450162929000",
                        "purchase_date_pst": "2015-12-14 23:02:09 America/Los_Angeles",
                        "original_purchase_date": "2015-12-15 07:02:09 Etc/GMT",
                        "original_purchase_date_ms": "1450162929000",
                        "original_purchase_date_pst": "2015-12-14 23:02:09 America/Los_Angeles",
                        "is_trial_period": "false"
                    },
                ],
            }
        }


original_apple_iap = app.config['APPLE_IAP_HOST_LIVE']


def setup_module():
    app.config['APPLE_IAP_HOST_LIVE'] = 'http://buy.itunes.apple.com'


def teardown_module():
    app.config['APPLE_IAP_HOST_LIVE'] = original_apple_iap
