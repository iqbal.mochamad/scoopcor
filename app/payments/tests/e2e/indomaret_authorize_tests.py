import json
from httplib import OK

from datetime import datetime, timedelta

from mock import patch

from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.offers.choices.offer_type import SINGLE
from app.offers.models import OfferType
from app.orders.choices import NEW
from app.payment_gateways.models import PaymentGateway
from app.paymentgateway_connectors.veritrans.veritrans import VeritransCStore
from app.payments.choices.gateways import INDOMARET
from app.payments.signature_generator import GenerateSignature
from app.utils.datetimes import datetime_to_isoformat
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory


def mock_veritrans_response_success(submit_to_veritrans=True):
    submit_status = True
    payment_code = 'dummytest1234'
    formatted_billing_address = {
        "city": '',
        "first_name": 'email@gmail.com',
        "last_name": '',
        "postal_code": '12345',
        "phone": '1234567',
        "address1": '',
        "address2": '',
    }
    vc_transaction = {
        "token": None,
        "amount": 12321321,
        "items": [],
        "customer_email": 'email@gmail.com',
        "payment_id": 123,
        "billing_address": formatted_billing_address,
        "order_id": ORDER_ID,
        "message": "Scoop Order Id {}".format(ORDER_ID),
        "payment_args": {}
    }
    vc = VeritransCStore(vc_transaction)
    response = vc._dummy_veritrans_response(payment_code=payment_code)
    return submit_status, response


class IndomaretPaymentTests(TestBase):
    """End to End test for Payment with Indomaret (Veritrans) - Authorize
    Submit order payment to indomaret veritrans

    make sure app.config['VERITRANS_V2_SERVER_KEY'] contain key from vertirans sandbox
    """

    original_before_req_funcs = None
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(IndomaretPaymentTests, cls).setUpClass()
        cls.init_data(cls.session)

        mock_smtplib()

    @patch('app.paymentgateway_connectors.veritrans.veritrans.VeritransCStore.charge_transaction')
    def test_authorize_indomaret(self, mock_veritrans_response):
        # mock valid response from veritrans
        mock_veritrans_response.return_value = (
            True, {
                "approval_code": None,
                "fraud_status": None,
                "gross_amount": self.order.final_amount,
                "order_id": str(self.order_id),
                "payment_code": "12312312rer24",
                "payment_type": u'cstore',
                "status_code": 201,
                "status_message": 'Success, cstore transaction is successful',
                "transaction_id": '52bd1121-9ae5-4aa7-a9bf-7c92629ca233',
                "transaction_status": 'pending',
                "transaction_time": datetime_to_isoformat(datetime.now()),
                "expired_date": datetime.now() + timedelta(days=1)
            }
        )

        self.set_api_authorized_user(self.user)
        request_body = self.get_request_body_payment()
        self.response = self.api_client.post(
            '/v1/payments/authorize',
            data=json.dumps(request_body),
            headers=self.build_headers(has_body=True))
        self.response_dict = json.loads(self.response.data)

        """Response example:
        {
          "order_id": 817464,
          "payment_id": 753685,
          "payment_code": "1320001234567",
          "expired_date": "2016-08-11T09:57:43+00:00"
        }
        """
        self.assertEqual(self.response.status_code, OK,
                         'Response status code {0.status_code} != 200(OK). Response {0.data}'.format(self.response))

        self.assertIsNotNone(self.response_dict.get('payment_code', None))

        self.assertIsNotNone(self.response_dict.get('expired_date', None))

        self.assertEqual(self.order_id, self.response_dict.get('order_id', None))

    @classmethod
    def get_request_body_payment(cls):
        return {
            "order_id": cls.order_id,
            "signature": GenerateSignature(order_id=cls.order_id).construct(),
            "user_id": USER_ID,
            "payment_gateway_id": INDOMARET
        }

    @classmethod
    def init_data(cls, s):
        # generate_static_sql_fixtures(session)
        pg_indomaret = s.query(PaymentGateway).get(INDOMARET)

        offer_type = s.query(OfferType).get(SINGLE)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        brand = BrandFactory()
        item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)

        cls.user = UserFactory(id=USER_ID, username='cortesting@apps-foundry.com', email='cortesting@apps-foundry.com')
        # generate unique order id to prevent error from veritrans in end to end test
        # to prevent error 406 already exists from veritrans
        cls.order_id = ORDER_ID
        cls.order = OrderFactory(
            id=cls.order_id, paymentgateway=pg_indomaret,
            total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
            user_id=USER_ID, party=cls.user,
            order_status=NEW)
        order_line = OrderLineFactory(order=cls.order, offer=cls.offer1, user_id=USER_ID, currency_code='IDR',
                                      price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                      orderline_status=NEW)
        s.commit()


USER_ID = 12345
ORDER_ID = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())

