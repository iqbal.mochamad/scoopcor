import json
from datetime import datetime
from httplib import OK

import mock
from nose.tools import nottest

from app import db
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.items.models import UserItem
from app.offers.models import Offer
from app.offers.models import OfferType
from app.orders.choices import NEW, COMPLETE
from app.orders.models import Order
from app.payment_gateways.models import PaymentGateway
from app.paymentgateway_connectors.inapbilling.models import PaymentInappBilling, PaymentInappBillingDetail
from app.payments.choices.gateways import IAB
from app.payments.models import Payment
from app.payments.payment_status import PAYMENT_BILLED, WAITING_FOR_PAYMENT
from app.payments.signature_generator import GenerateSignature
from app.users.tests.fixtures import UserFactory
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.payments import PaymentFactory

"""
# Make sure python library install correctly:
pip install --upgrade google-api-python-client

"""

FAKE_GOOGLE_TIER_CODE = '.c.usd.3.99'


@nottest
def mocked_google_response(*args, **kwargs):
    class MockedResponse(object):

        def __init__(self, status):
            self.purchaseState = status
            self.consumptionState = 0
            self.developerPayload

    return MockedResponse('settlement')


class ValidatePaymentApiBase(TestBase):
    """Base Class for End to End test for Payment with Google In-App Billing (IAB) - Validate
        with MOCKED response from Google IAB..
    """

    @classmethod
    def init_data(cls, s):
        payment_gateway = s.query(PaymentGateway).get(IAB)
        offer_type = s.query(OfferType).get(Offer.Type.single.value)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        brand = BrandFactory()
        cls.item_id = 1
        item1 = ItemFactory(id=cls.item_id, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)

        cls.user = UserFactory(id=USER_ID, username='cortesting@apps-foundry.com', email='cortesting@apps-foundry.com')
        # generate unique order id to prevent error from veritrans in end to end test
        # to prevent error 406 already exists from veritrans
        cls.order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        order = OrderFactory(id=cls.order_id, paymentgateway=payment_gateway,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             user_id=USER_ID, party=cls.user,
                             order_status=WAITING_FOR_PAYMENT, tier_code=FAKE_GOOGLE_TIER_CODE)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=USER_ID, currency_code='IDR',
                                      price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                      orderline_status=NEW)
        # give payment id same as order id, just for test purposes
        payment = PaymentFactory(id=cls.order_id, order=order, user_id=order.user_id,
                                 payment_status=WAITING_FOR_PAYMENT)
        s.commit()
        cls.fake_developer_payload = '7c43d0614e2872d62f69c359b10338eeb1dbe61cec902fc79f6ff587b49d84b6'
        cls.fake_purchase_token = 'plfaibkadeblebpbgnnkokij.AO-J1OyMsmaTWyoyyrtT0C0Lav7e3PFNfNOf_A0vl4MCgcX0' \
                                  '3G_GqyrqGP6wNPqVEDhJYblGQFYRjcCnb_HhF0eldwqeqSGx_6TUNUqot13lHFxvFh3AGuxWSn' \
                                  'RfqM6wUrTdrO9I9Tl571uSBHvKaZacILb1AG4L_Q'
        payment_iab = PaymentInappBilling(
            payload_key=cls.fake_developer_payload,
            user_id=USER_ID,
            order_id=cls.order_id,
            status=1,
            payment_id=cls.order_id,
            purchase_token=cls.fake_purchase_token
        )
        s.add(payment_iab)
        s.commit()
        payment_iab_detail = PaymentInappBillingDetail(
            paymentinappbilling_id=payment_iab.id,
            payment_id=cls.order_id
        )
        s.add(payment_iab_detail)
        s.commit()

    @classmethod
    def get_request_body_payment(cls):
        return {
            "order_id": cls.order_id,
            "signature": GenerateSignature(order_id=cls.order_id).construct(),
            "user_id": USER_ID,
            "payment_gateway_id": IAB,
            "payment_id": cls.order_id,
            "purchase_token": cls.fake_purchase_token,
            "developer_payload": cls.fake_developer_payload,
            "product_sku": 'com.appsfoundry.scoop.c.usd.1.99',
        }


class ValidatePaymentApiE2ETest(ValidatePaymentApiBase):
    """End to End test for Payment with Google In-App Billing (IAB) - Validate
    with MOCKED response from Google IAB..
    """

    maxDiff = None

    @classmethod
    @mock.patch('app.paymentgateway_connectors.inapbilling.billing.'
                'GoogleIABCheckout.confirm_google_payment')
    def setUpClass(cls, mocked_google_response):
        super(ValidatePaymentApiE2ETest, cls).setUpClass()
        cls.init_data(cls.session)

        """ Mock Google IAB Checkout Response
            purchaseState:
                0 = Purchased
                1 = Cancelled
            consumptionState:
                0 = Consumed
                1 = Yet to be consumed
        :param mocked_google_response:
        :return:
        """
        mocked_google_response.return_value = (True, {
            "kind": "androidpublisher#inapppurchase",
            "purchaseTime": 123123123,
            "purchaseState": 0,
            "consumptionState": 0,
            "developerPayload": cls.fake_developer_payload
        })

        mock_smtplib()

        cls.set_api_authorized_user(cls.super_admin)
        request_body = cls.get_request_body_payment()
        headers = cls.build_headers(has_body=True)
        headers['User-Agent'] = "scoop_android/v1.2.3"
        cls.response = cls.api_client.post(
            '/v1/payments/validate',
            data=json.dumps(request_body),
            headers=headers)
        cls.response_dict = json.loads(cls.response.data)

        # example response:

    def test_response_status(self):
        self.assertEqual(self.response.status_code, OK,
                         'Response status code {0.status_code} != 200(OK). Response {0.data}'.format(self.response))

    def test_response_order_id(self):
        self.assertEqual(self.order_id, self.response_dict.get('order_id', None))

    def test_updated_payment_status(self):
        session = db.session()
        payment = session.query(Payment).get(self.order_id)
        self.assertEquals(payment.payment_status, PAYMENT_BILLED)

    def test_updated_order_status(self):
        session = db.session()
        order = session.query(Order).get(self.order_id)
        self.assertEquals(order.order_status, COMPLETE)

    def test_saved_user_owned_item(self):
        session = db.session()
        user_owned_item = session.query(UserItem).filter_by(user_id=USER_ID, item_id=self.item_id).first()
        self.assertEquals(user_owned_item.user_id, USER_ID)


USER_ID = 12345


# example request body from mobile apps / front end:
# API /validate
# Body Payment Google in-app-billing :
# {
#   "product_sku": "com.appsfoundry.scoop.c.usd.1.99",
#   "user_id": "442007",
#   "developer_payload": "7c43d0614e2872d62f69c359b10338eeb1dbe61cec902fc79f6ff587b49d84b6",
#   "payment_id": 872061,
#   "order_id": 946321,
#   "purchase_token": "plfaibkadeblebpbgnnkokij.AO-J1OyMsmaTWyoyyrtT0C0Lav7e3PFNfNOf_A0vl4MCgcX03G_GqyrqGP6wNPqVEDhJYblGQFYRjcCnb_HhF0eldwqeqSGx_6TUNUqot13lHFxvFh3AGuxWSnRfqM6wUrTdrO9I9Tl571uSBHvKaZacILb1AG4L_Q",
#   "signature": "1372b27bc049c7fa40ed069a3ccf255c78e3c007b7143b116a886615a3a00d69",
#   "payment_gateway_id": 15
# }

# example success response from payment completed checkout
# {
#    "payment_gateway_name":"enhance granular relationships",
#    "user_id":12345,
#    "total_amount":"10000.00",
#    "payment_gateway_id":15,
#    "order_id":1477677798,
#    "partner_id":5605,
#    "is_active":true,
#    "final_amount":"10000.00",
#    "platform_id":2800,
#    "tier_code":".c.usd.3.99",
#    "temp_order_id":5698,
#    "client_id":4723,
#    "point_reward":6991,
#    "order_status":90000,
#    "order_number":7746,
#    "currency_code":"NZD",
#    "order_lines":[
#       {
#          "name":"whiteboard sexy info-mediaries",
#          "items":[
#             {
#                "is_featured":true,
#                "vendor":{
#                   "slug":"pariatur-et-magnam",
#                   "id":1,
#                   "name":"productize distributed supply-chains"
#                },
#                "name":"exploit rich infrastructures",
#                "countries":[
#                   {
#                      "name":"Indonesia",
#                      "id":1
#                   }
#                ],
#                "release_date":"2014-07-10T08:37:53",
#                "item_status":9,
#                "languages":[
#                   {
#                      "id":2,
#                      "name":"Indonesian"
#                   }
#                ],
#                "authors":[
#
#                ],
#                "id":1,
#                "edition_code":"voluptatibus"
#             }
#          ],
#          "offer_id":1,
#          "offer_type_id":1,
#          "is_free":false,
#          "final_price":"10000.00",
#          "currency_code":"IDR",
#          "raw_price":"10000.00"
#       }
#    ]
# }
