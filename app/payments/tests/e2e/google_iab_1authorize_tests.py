import json
from datetime import datetime
from httplib import CREATED

from app import db, app
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.master.models import Currencies
from app.offers.choices.offer_type import SINGLE
from app.offers.models import OfferType, Offer
from app.orders.choices import NEW
from app.payment_gateways.models import PaymentGateway
from app.payments.choices.gateways import IAB
from app.payments.signature_generator import GenerateSignature
from app.users.tests.fixtures import UserFactory
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory

FAKE_GOOGLE_TIER_CODE = '.c.usd.3.99'


class MockedPaymentTests(TestBase):
    """End to End test for Payment with Google In-App Billing (IAB) - Authorize
    """
    @classmethod
    def setUpClass(cls):
        super(MockedPaymentTests, cls).setUpClass()
        cls.init_data(cls.session)

        mock_smtplib()

        cls.set_api_authorized_user(cls.user)
        request_body = cls.get_request_body_payment()
        cls.response = cls.api_client.post('/v1/payments/authorize',
                                           data=json.dumps(request_body),
                                           headers=cls.build_headers(cls))
        cls.response_dict = json.loads(cls.response.data)

        """Response example:
        {
          "order_id": 817464,
          "payment_id": 753685,
          "payment_code": "1320001234567",
          "expired_date": "2016-08-11T09:57:43+00:00"
        }
        """

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, CREATED,
                         'Response status is not CREATED. Response {}'.format(self.response.data))

    def test_response_payment_id(self):
        self.assertIsNotNone(self.response_dict.get('payment_id', None))

    def test_response_developer_payload(self):
        self.assertIsNotNone(self.response_dict.get('developer_payload', None))

    def test_response_product_code(self):
        self.assertEqual(FAKE_GOOGLE_TIER_CODE, self.response_dict.get('product_code', None))

    @classmethod
    def get_request_body_payment(cls):
        return {
            "order_id": cls.order_id,
            "signature": GenerateSignature(order_id=cls.order_id).construct(),
            "user_id": USER_ID,
            "payment_gateway_id": IAB
        }

    @classmethod
    def init_data(cls, s):
        pg_test = s.query(PaymentGateway).get(IAB)

        offer_type = s.query(OfferType).get(Offer.Type.single.value)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        brand = BrandFactory()
        item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)

        cls.user = UserFactory(id=USER_ID, username='cortesting@apps-foundry.com', email='cortesting@apps-foundry.com')
        # generate unique order id to prevent error from veritrans in end to end test
        # to prevent error 406 already exists from veritrans
        cls.order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        order = OrderFactory(id=cls.order_id, paymentgateway=pg_test,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             user_id=USER_ID, party=cls.user,
                             order_status=NEW, tier_code=FAKE_GOOGLE_TIER_CODE)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=USER_ID, currency_code='IDR',
                                      price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                      orderline_status=NEW)
        s.commit()


USER_ID = 12345

