import json
from datetime import datetime
from httplib import OK

import httpretty
from nose.tools import istest

from app import db, app
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.items.models import UserItem
from app.offers.models import Offer, OfferType
from app.orders.choices import NEW, COMPLETE
from app.orders.models import Order
from app.payment_gateways.models import PaymentGateway
from app.payments.choices.gateways import KREDIVO
from app.payments.models import Payment
from app.payments.payment_status import PAYMENT_BILLED, WAITING_FOR_PAYMENT, CANCELLED
from app.services.kredivo.helpers import build_kredivo_gateway
from app.users.tests.fixtures import UserFactory
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.payments import PaymentFactory


class CaptureKredivoPaymentApiBase(TestBase):


    transaction_id = "bb0380d7-2688-4e73-a668-997f3fc5cbec"
    signature_key = "5f1fa4a524f5a8201a3a373d67b5796840bed1598b2bb816"

    @classmethod
    def init_data(cls, new_id, s):
        pg_kredivo = s.query(PaymentGateway).get(KREDIVO)
        offer_type = s.query(OfferType).get(Offer.Type.single.value)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                              price_point=10)
        brand = BrandFactory()
        item_id = 1
        item1 = ItemFactory(id=item_id, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)

        cls.user = UserFactory(id=USER_ID, username='cortesting@apps-foundry.com', email='cortesting@apps-foundry.com')
        s.commit()
        # generate unique order id to prevent error from kredivo in end to end test
        # to prevent error 406 already exists from kredivo
        cls.order_id = new_id
        order = OrderFactory(id=cls.order_id, paymentgateway=pg_kredivo,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             user_id=USER_ID, party=cls.user,
                             order_status=WAITING_FOR_PAYMENT)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=USER_ID, currency_code='IDR',
                                      price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                      orderline_status=NEW)
        # give payment id same as order id, just for test purposes
        payment = PaymentFactory(id=cls.order_id, order=order, user_id=order.user_id,
                                 payment_status=WAITING_FOR_PAYMENT)

        s.commit()

    @classmethod
    @httpretty.activate
    def get_api_response(cls,
                         request_body_trx_status="SETTLEMENT",
                         mocked_response_trx_status='SETTLEMENT',
                         mocked_fraud_status="ACCEPT"):

        mock_smtplib()
        # here we mock the response from kredivo when we hit their API (GET payment status api)
        matched_url_to_mock = build_kredivo_gateway()._build_uri(
            "/v2/update?transaction_id={}&signature_key={}".format(
                cls.transaction_id, cls.signature_key))
        httpretty.register_uri(
            httpretty.GET,
            matched_url_to_mock,
            body=cls.get_mocked_kredivo_response(mocked_response_trx_status, mocked_fraud_status))

        cls.set_api_authorized_user(cls.user)
        request_body = cls.get_request_body_payment(transaction_status=request_body_trx_status)
        client = app.test_client(use_cookies=False)
        cls.response = client.post('/v1/payments/kredivo/capture',
                                   data=json.dumps(request_body),
                                   headers=cls.build_headers(has_body=True))
        cls.response_dict = json.loads(cls.response.data)

    @classmethod
    def get_mocked_kredivo_response(cls, mocked_response_trx_status='SETTLEMENT',
                                    mocked_fraud_status="ACCEPT"):
        return json.dumps({
            "status": "OK",
            "message": "Mocked Confirmed order status.",
            "payment_type": "30_days",
            "transaction_id": cls.transaction_id,
            "transaction_status": mocked_response_trx_status,
            "transaction_time": 1469613243,
            "order_id": "KD125262",
            "amount": 125000,
            "fraud_status": mocked_fraud_status
        })

    @classmethod
    def get_request_body_payment(cls, transaction_status='SETTLEMENT'):
        return {
            "status": "OK",
            "message": "Confirmed order status.",
            "payment_type": "30_days",
            "transaction_id": cls.transaction_id,
            "transaction_status": transaction_status,
            "transaction_time": 1469613243,
            "order_id": cls.order_id,
            "amount": 125000,
            "signature_key": cls.signature_key,
            "shipping_address": {
                "first_name": "Oemang",
                "last_name": "Tandra",
                "address": "Jalan Teknologi Indonesia No. 25",
                "city": "Jakarta",
                "postal_code": "12960",
                "phone": "081513114262",
                "country_code": "IDN"
            }
        }


@istest
class CaptureKredivoPaymentApiTests(CaptureKredivoPaymentApiBase):
    """End to End test for Payment with Kredivo - Capture Post Notification with Status=SETTLEMENT
    After customer validated in kredivo, Kredivo will hit this API.
    'v1/payments/kredivo/capture'
    here we test it

    """

    @classmethod
    def setUpClass(cls):
        super(CaptureKredivoPaymentApiTests, cls).setUpClass()
        new_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        cls.init_data(new_id, cls.session)
        cls.get_api_response()

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, OK)

    def test_response_status(self):
        self.assertEqual(self.response_dict.get('status', None), "OK")

    def test_response_order_id(self):
        self.assertEqual(self.order_id, self.response_dict.get('order_id', None))

    def test_updated_payment_status(self):
        session = db.session()
        payment = session.query(Payment).get(self.order_id)
        self.assertEquals(payment.payment_status, PAYMENT_BILLED)

    def test_updated_order_status(self):
        session = db.session()
        # in this test with dummy data, payment id = order id = self.order_id
        order = session.query(Order).get(self.order_id)
        self.assertEquals(order.order_status, COMPLETE)

    def test_saved_user_owned_item(self):
        session = db.session()
        user_owned_item = session.query(UserItem).filter_by(user_id=USER_ID, item_id=1).first()
        self.assertEquals(user_owned_item.user_id, USER_ID)


@istest
class NotificationCancelTests(CaptureKredivoPaymentApiBase):
    """End to End test for Payment with Kredivo  - Capture Post Notification with Status=Cancel
    After we submit order into Kredivo, kredivo will hit this API to send notification.
    'v1/payments/kredivo/capture'
    here we test it

    """

    @classmethod
    def setUpClass(cls):
        super(NotificationCancelTests, cls).setUpClass()
        new_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds()) + 1
        cls.init_data(new_id, cls.session)
        cls.get_api_response(request_body_trx_status="CANCEL", mocked_response_trx_status="CANCEL")

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, OK)

    def test_response_status(self):
        self.assertEqual(self.response_dict.get('status', None), "OK")

    def test_updated_payment_status(self):
        session = db.session()
        # in this test with dummy data, payment id = order id = self.order_id
        payment = session.query(Payment).get(self.order_id)
        self.assertEquals(payment.payment_status, CANCELLED)


@istest
class NotificationPendingTests(CaptureKredivoPaymentApiBase):
    """End to End test for Payment with Kredivo  - Capture Post Notification with Status=Pending
    After we submit order into Kredivo, kredivo will hit this API to send notification.
    'v1/payments/kredivo/capture'
    here we test it

    """

    @classmethod
    def setUpClass(cls):
        super(NotificationPendingTests, cls).setUpClass()
        new_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds()) + 2
        cls.init_data(new_id, cls.session)
        cls.get_api_response(request_body_trx_status="PENDING", mocked_response_trx_status="PENDING")

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, OK)

    def test_response_status(self):
        self.assertEqual(self.response_dict.get('status', None), "OK")


@istest
class NotificationSettlementButActuallyPendingTests(CaptureKredivoPaymentApiBase):
    """End to End test for Payment with Kredivo - Capture
    Test for case: Post Notification with Status=Settlement, but confirmation data is still in pending
    After we submit order into Kredivo, kredivo will hit this API to send notification.
    'v1/payments/kredivo/capture'
    here we test it

    """

    @classmethod
    def setUpClass(cls):
        super(NotificationSettlementButActuallyPendingTests, cls).setUpClass()
        new_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds()) + 3
        cls.init_data(new_id, cls.session)
        cls.get_api_response(request_body_trx_status="SETTLEMENT", mocked_response_trx_status="PENDING")

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, OK)

    def test_response_status(self):
        self.assertEqual(self.response_dict.get('status', None), "OK")


USER_ID = 12345
