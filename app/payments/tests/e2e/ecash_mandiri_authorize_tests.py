import json
from datetime import datetime
from httplib import OK

from httpretty import httpretty

from app import db, app
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.offers.models import OfferType, Offer
from app.orders.choices import NEW
from app.payment_gateways.models import PaymentGateway
from app.payments.choices.gateways import ECASH
from app.payments.models import Payment
from app.payments.signature_generator import GenerateSignature
from app.users.tests.fixtures import UserFactory
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory


class EcashPaymentTests(TestBase):
    """End to End test for Payment with Indomaret (Veritrans) - Authorize
    Submit order payment to indomaret veritrans

    make sure app.config['VERITRANS_V2_SERVER_KEY'] contain key from vertirans sandbox
    """

    original_before_req_funcs = None
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(EcashPaymentTests, cls).setUpClass()
        cls.init_data(cls.session)

        mock_smtplib()

        httpretty.enable()
        httpretty.register_uri(
            httpretty.POST,
            "{}{}".format(app.config['ECASH_SITE'], app.config['ECASH_NEW_TRANSACTION_URL']),
            body=cls._get_mock_ecash_response(),
            content_type="application/xml"
        )
        cls.set_api_authorized_user(cls.super_admin)
        request_body = cls.get_request_body_payment()
        cls.response = cls.api_client.post(
            '/v1/payments/authorize',
            data=json.dumps(request_body),
            headers=cls.build_headers(has_body=True))
        cls.response_dict = json.loads(cls.response.data)

        """Response example:
        {
          "order_id": 817464,
          "payment_id": 753685,
          "payment_code": "1320001234567",
          "expired_date": "2016-08-11T09:57:43+00:00"
        }
        """

    @classmethod
    def tearDownClass(cls):
        super(EcashPaymentTests, cls).tearDownClass()

    @classmethod
    def _get_mock_ecash_response(cls):
        return '''<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
<soap:Body>
<ns2:generateResponse xmlns:ns2="http://ws.service.gateway.ecomm.ptdam.com/" xmlns:ns3="http://ws.service.ecomm.e-cash.ptdam.com/">
<return>K7P79BIHOXBCCGDBRIDA0IGXQHENWLJZ{}</return> </ns2:generateResponse>
</soap:Body> </soap:Envelope>'''.format(cls.order_id)

    def test_response_status_code(self):
        self.assertEqual(self.response.status_code, OK,
                         'Response status code {0.status_code} != 200(OK). Response {0.data}'.format(self.response))

    def test_response_payment_code(self):
        self.assertIsNotNone(self.response_dict.get('ecash_trx_id', None))

    def test_response_payment(self):
        actual_payment_id = self.response_dict.get('payment_id', None)
        self.assertIsNotNone(actual_payment_id)
        session = db.session()
        actual_payment = session.query(Payment).get(actual_payment_id)
        self.assertIsNotNone(actual_payment)
        self.assertEqual(actual_payment.order_id, self.order_id)
        self.assertEqual(actual_payment.amount, self.offer1.price_idr)

    @classmethod
    def get_request_body_payment(cls):
        return {
            "order_id": cls.order_id,
            "signature": GenerateSignature(order_id=cls.order_id).construct(),
            "user_id": USER_ID,
            "payment_gateway_id": ECASH,
            "client_ip": "192.168.1.1",
            "return_url": "https://merchant.com/return.html"
        }

    @classmethod
    def init_data(cls, s):
        pg_test = s.query(PaymentGateway).get(ECASH)

        offer_type = s.query(OfferType).get(Offer.Type.single.value)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        brand = BrandFactory()
        item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)

        cls.user = UserFactory(id=USER_ID, username='cortesting@apps-foundry.com', email='cortesting@apps-foundry.com')
        # generate unique order id to prevent error from veritrans in end to end test
        # to prevent error 406 already exists from veritrans
        cls.order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        order = OrderFactory(id=cls.order_id, paymentgateway=pg_test,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             user_id=USER_ID, party=cls.user,
                             order_status=NEW)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=USER_ID, currency_code='IDR',
                                      price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                      orderline_status=NEW)
        s.commit()


USER_ID = 12345
