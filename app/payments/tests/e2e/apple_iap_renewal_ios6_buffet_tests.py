from datetime import datetime, timedelta

from flask import json
import httpretty

from app import app
from app.items.choices import STATUS_READY_FOR_CONSUME
from app.items.choices import STATUS_TYPES
from app.master.models import Platform
from app.offers.choices.offer_type import BUFFET
from app.offers.models import OfferType
from app.payment_gateways.models import PaymentGateway
from app.paymentgateway_connectors.apple_iap import iap_status
from app.paymentgateway_connectors.apple_iap.apple_iap_renewal import AppleIAPRenewal
from app.paymentgateway_connectors.apple_iap.models import PaymentAppleIAP, AppleReceiptIdentifiers
from app.paymentgateway_connectors.choices.renewal_status import NEW
from app.payments.choices import WAITING_FOR_PAYMENT
from app.payments.choices.gateways import APPLE
from app.users.tests.fixtures import UserFactory
from app.utils.datetimes import get_local
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferBuffetFactory, OfferPlatformFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory
from tests.fixtures.sqlalchemy.payments import PaymentFactory


class AppleIapRenewal(TestBase):

    @classmethod
    def setUpClass(cls):
        super(AppleIapRenewal, cls).setUpClass()
        cls.init_data(cls.session)

    @classmethod
    def init_data(cls, s):
        pg = s.query(PaymentGateway).get(APPLE)

        offer_type = s.query(OfferType).get(BUFFET)
        cls.offer1 = OfferFactory(id=43103, name='Scoop Premium',
                                  offer_type=offer_type, is_free=False, price_idr=89000, price_usd=6.99,
                                  price_point=1)
        brand = BrandFactory()
        cls.item_id = 1
        item1 = ItemFactory(id=cls.item_id, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)
        cls.offer1.brands.append(brand)
        offer_buffet = OfferBuffetFactory(
            available_from=get_local(),
            available_until=datetime.now() + timedelta(days=330),
            buffet_duration=timedelta(days=30),
            offer=cls.offer1
        )
        cls.tier_code = '.c.usd.1.99'
        offer_platform = OfferPlatformFactory(
            offer=cls.offer1,
            platform=s.query(Platform).get(1),
            tier_id=1,
            tier_code=cls.tier_code,
            currency='USD',
            price_usd=6.99,
            price_idr=89000,
            price_point=1,
            discount_price_usd=6.99,
            discount_price_idr=89000,
            discount_price_point=1
        )

        cls.user = UserFactory(id=USER_ID, username='cortesting@apps-foundry.com', email='cortesting@apps-foundry.com')
        # generate unique order id to prevent error
        cls.order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        # create first order
        order = OrderFactory(id=cls.order_id, paymentgateway=pg,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             user_id=USER_ID, party=cls.user,
                             order_status=WAITING_FOR_PAYMENT, currency_code=pg.base_currency.iso4217_code,
                             tier_code=cls.tier_code
                             )
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=USER_ID,
                                      currency_code=pg.base_currency.iso4217_code,
                                      price=cls.offer1.price_usd, final_price=cls.offer1.price_usd,
                                      orderline_status=NEW)
        # set payment id same as order id, just for test purposes
        payment = PaymentFactory(id=cls.order_id, order=order, user_id=order.user_id,
                                 payment_status=WAITING_FOR_PAYMENT)
        s.commit()
        cls.original_transaction_id = 21223

        cls.payment_iap = PaymentAppleIAP(
            id=100,
            payment_id=cls.order_id,
            product_id='dummy_id',
            original_transaction_id=cls.original_transaction_id,
            expires_date=datetime.now() + timedelta(days=30),
            web_order_line_item_id=1,
            transaction_id=331223,
            purchase_date=datetime(2016, 10, 1),
            original_purchase_date=datetime(2016, 10, 1),
            status='new',
            receipt_data='dlfkasjdlkfsajf',
            receipt='kldjfaslkjfdsaf',
            is_processed=False,
            renewal_status=NEW,
        )
        cls.iap_receipt = AppleReceiptIdentifiers(
            user_id=USER_ID,
            client_id=1,
            original_transaction_id=cls.original_transaction_id,
            itunes_product_type=2,
            is_processed=False,
            expired_date=datetime.now() + timedelta(days=30),
        )
        s.add(cls.payment_iap)
        s.add(cls.iap_receipt)
        s.commit()

    def get_mocked_ios6_apple_response(self, status=iap_status.ACCEPTED, original_transaction_id=123345, is_trial=False):
        # ex: "2016-12-14 02:10:22"
        purchase_date = get_local().strftime('%Y-%m-%d %H:%M:%S')
        expired_date = (get_local() + timedelta(days=30)).strftime('%Y-%m-%d %H:%M:%S')
        return json.dumps({
            'status': status,
            'verification_rslt': {
                "status": status,
            },
            "receipt": {
                "is_trial_period": is_trial,
                "product_id": 'com.appsfoundry.scoop{}'.format(self.tier_code),
                'original_purchase_date': purchase_date,
                'expires_date_formatted': expired_date,
                'purchase_date': purchase_date,
                'original_transaction_id': original_transaction_id,
                'web_order_line_item_id': "11{}".format(original_transaction_id),
                'transaction_id': 12345
            },
        })

    @httpretty.activate
    def test_auto_purchase(self):

        mock_smtplib()

        self.session.add(self.payment_iap)
        self.session.add(self.iap_receipt)

        # here we mock the response from apple ios when we hit their API (GET payment status api)
        matched_url_to_mock = "{}{}".format(app.config['APPLE_IAP_HOST_LIVE'],
                                            app.config['URL_VERIFY_RECEIPT'])
        httpretty.register_uri(
            httpretty.POST,
            matched_url_to_mock,
            body=self.get_mocked_ios6_apple_response(original_transaction_id=self.original_transaction_id))

        apple_renewal = AppleIAPRenewal(divider=10)
        apple_renewal.new_receipt_data = "dummy_ecrypted_receipt_data_dfaljfalfja"
        apple_renewal.orderlines_data = [{
            'offer_id': self.offer1.id,
            'quantity': 1
        }]

        result, info = apple_renewal.auto_purchase(renewal=self.payment_iap, active_renewal=self.iap_receipt)
        self.assertTrue(result, 'Auto purchase result is not True (Failed). \nerror {}'.format(str(info)))
        self.assertEqual(info, 'OK')


USER_ID = 12345
