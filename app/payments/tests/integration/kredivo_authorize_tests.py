import json
from datetime import timedelta, datetime
from httplib import OK

from nose.tools import nottest

from app import db
from app.eperpus.tests.fixtures import SharedLibraryFactory
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.offers.models import Offer
from app.offers.models import OfferType
from app.orders.choices import NEW
from app.payment_gateways.models import PaymentGateway
from app.payments.choices.gateways import KREDIVO
from app.payments.signature_generator import GenerateSignature
from app.users.tests.fixtures import UserFactory
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory

USER_ID = 12345


@nottest
class PaymentAuthorizeWithKredivoTests(TestBase):
    """ integration for Payment Authorized API with KREDIVO as payment gateway

    marked as nottest (so as to prevent external factor error for CI)
    run this tests manually on demand (if needed)
    """

    original_before_req_funcs = None
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        super(PaymentAuthorizeWithKredivoTests, cls).setUpClass()
        cls.init_data(cls.session)

        cls.response = cls.get_api_response()

    @classmethod
    def init_data(cls, s):
        pg_kredivo = s.query(PaymentGateway).get(KREDIVO)
        offer_type = s.query(OfferType).get(Offer.Type.single.value)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        brand = BrandFactory()
        item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)
        cls.library_org = SharedLibraryFactory(
            id=11,
            user_concurrent_borrowing_limit=10,
            reborrowing_cooldown=timedelta(days=0),
            borrowing_time_limit=timedelta(days=7))
        cls.user = UserFactory(id=USER_ID)

        # set last sequence id for Order to unique number
        # to prevent error 406 already exists from kredivo
        last_order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        db.engine.execute("select setval('core_orders_id_seq', {}, true)".format(last_order_id))
        db.engine.execute("select setval('core_payments_id_seq', {}, true)".format(last_order_id))
        cls.order_id = last_order_id-1
        order = OrderFactory(id=cls.order_id, paymentgateway=pg_kredivo,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             user_id=USER_ID, party=cls.user,
                             order_status=NEW)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=USER_ID, currency_code='IDR',
                                      price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                      orderline_status=NEW)
        s.commit()

    @classmethod
    def get_api_response(cls):
        # define mocked patch:

        mock_smtplib()

        cls.set_api_authorized_user(cls.user)

        # process payment (authorize payment to kredivo api)
        request_body = cls.get_request_body_payment(cls.order_id)
        return cls.api_client.post('/v1/payments/authorize',
                                   data=json.dumps(request_body),
                                   headers=cls.build_headers(has_body=True))

    def test_status_code(self):
        self.assertEqual(self.response.status_code, OK,
                         'Response status code {0.status_code} != 200(OK). Response {0.data}'.format(self.response))

    def test_response_body(self):
        response = json.loads(self.response.data)
        self.assertIsNotNone(response)
        self.assertIsNotNone(response.get('payment_id', None))
        self.assertIsNotNone(response.get('redirect_url', None))

    @staticmethod
    def get_request_body_payment(order_id):
        return {
            "order_id": order_id,
            "signature": GenerateSignature(order_id=order_id).construct(),
            "user_id": USER_ID,
            "payment_gateway_id": KREDIVO,
            "return_url": "http://getscoop.com/orders",
            "cancel_url": "http://getscoop.com/orders/cancel"
        }
