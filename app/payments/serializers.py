from flask_restful import fields
from flask_appsfoundry import serializers
from flask_appsfoundry.serializers.nested import AddressSerializer


class TokenSerializer(serializers.SerializerBase):
    id = fields.Integer
    user_id = fields.Integer
    last_name = fields.String
    first_name = fields.String
    token=fields.String
    masked_credit_card = fields.String
    expiration = fields.DateTime('iso8601')
    billing_address = fields.Nested(AddressSerializer())
    is_active = fields.Boolean
    bin_bank = fields.String
    bin_bank_code = fields.String
    bin_class = fields.String
    bin_types = fields.String
    bin_brand = fields.String
    bin_country_code = fields.String
