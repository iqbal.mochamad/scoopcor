import flask_restful as restful
from flask import Blueprint

from app.shared_client.api import SharedPartnerLibrary, SharedPartnerLibraryDetail, SharedPartnerRegistration, \
    SharedPartnerPremium, SharePartnerLoginApi, SharePartnerLinkedListApi

partners_shared = Blueprint('partner-shared-lib', __name__, url_prefix='/v1/partners')
partners_shared_api = restful.Api(partners_shared)

# this current api only KPG used for displaying our katalog inside their web!
partners_shared_api.add_resource(SharedPartnerLibrary, '/shared-library', endpoint='partners-shared')
partners_shared_api.add_resource(SharedPartnerLibraryDetail, '/shared-library/<slug>', endpoint='partners-shared-detail')


# this current api still KOMPAS.id used for registration their user to gdn and check premium statuses
partners_shared_api.add_resource(SharedPartnerRegistration, '/registration', endpoint='partners-registration')
partners_shared_api.add_resource(SharedPartnerPremium, '/premium-status', endpoint='partners-premium-status')

# this for bound account to any
partners_shared_api.add_resource(SharePartnerLoginApi, '/access-login', endpoint='partners-access-login')
partners_shared_api.add_resource(SharePartnerLinkedListApi, '/shared-link', endpoint='partners-shared-link')



