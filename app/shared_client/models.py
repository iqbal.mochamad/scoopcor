from flask_appsfoundry.models import TimestampMixin
from sqlalchemy.dialects.postgresql import JSONB

from app import db


class PartnerAuth(db.Model, TimestampMixin):

    __tablename__ = 'core_partners_users'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'))
    users = db.relationship('User')
    partner_id = db.Column(db.Integer)
    partner_account = db.Column(db.String(250), nullable=False)
    expired_date = db.Column(db.DateTime, nullable=True)
    response_data = db.Column(JSONB)
    order_id = db.Column(db.Integer)
    client_id = db.Column(db.Integer)

    def __repr__(self):
        return '<user_id : %s>' % self.user_id
