import json

import requests

from datetime import datetime
from hashlib import sha256
from httplib import OK, CREATED

from sqlalchemy import desc

from app.helpers import generate_invoice
from app.master.choices import ANDROID, IOS, GETSCOOP
from app.offers.choices.offer_type import BUFFET
from app.offers.choices.status_type import READY_FOR_SALE
from app.offers.models import Offer, OfferBuffet
from app.master.models import Brand, Partner

from app import app, db
from app.orders.choices import COMPLETE
from app.orders.models import Order, OrderLine
from app.payments.choices import PAYMENT_BILLED
from app.payments.choices.gateways import FREE
from app.payments.models import Payment
from app.users.buffets import UserBuffet


def generate_kompas_hash(email=None, password=None, exist_partner=None):
    date_format = datetime.strftime(datetime.now(), '%Y-%m-%d')
    signature = '{date_format}{email}{salt}{date_format2}{password}'.format(
        date_format=date_format, email=email, salt=exist_partner.auth_key,
        date_format2=date_format, password=password)
    return sha256(signature).hexdigest()


def get_active_kompas_subs(email=None, partner_active=None):

    expired_date, partner_unique_code = None, None
    try:
        kompas_partner = '{}?email={}'.format(app.config['KOMPASID_API_SUBS'], email.strip())
        header = {'Authorization': app.config['KOMPASID_AUTH_KEY'], 'Content-Type': 'application/json'}
        partner_data = requests.get(kompas_partner, data=json.dumps({'email': email}), headers=header)

        if partner_data.status_code in [OK, CREATED]:
            data_response = partner_data.json()
            partner_active.response_data = data_response
            memberships = data_response.get('result', {}).get('memberships', [])

            expires_data, temp_code = [], {}
            for data in memberships:
                expires_data.append(data.get('membership_expired', None))
                temp_code.update({data.get('membership_expired', None): data.get('membership_slug', 'Unknown')})

            # get the latest and long expired times
            expires_data.sort()
            final_expired_date = expires_data[-1:]

            if final_expired_date:
                expired_date = datetime.strptime(final_expired_date[0], '%Y-%m-%d %H:%M:%S').replace(microsecond=1)
                partner_unique_code = temp_code[final_expired_date[0]]
                partner_active.expired_date = expired_date

            db.session.add(partner_active)
            db.session.commit()

    except:
        pass

    return expired_date, partner_unique_code


def linked_partner_account(exist_partner=None, email=None, password=None):

    partner_data = None
    try:
        signature = generate_kompas_hash(email=email, password=password, exist_partner=exist_partner)
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        data_request = {"email": email, "password": password, "hash": signature}

        partner_login = requests.post(exist_partner.auth_url, data=data_request, headers=headers)
        if partner_login.status_code in [OK, CREATED]:
            data_response = partner_login.json()
            partner_data = data_response.get('data', {}).get('user', {})
    except:
        pass

    return partner_data


def create_order(offer_premium=None, partner_active=None):
    if partner_active.client_id not in [ANDROID, IOS]:
        platform_id = GETSCOOP
    else:
        platform_id = partner_active.client_id

    new_order = Order(
        order_number=generate_invoice(),
        total_amount=offer_premium.price_idr,
        final_amount=offer_premium.price_idr,
        user_id=partner_active.user_id,
        client_id=partner_active.client_id,
        partner_id=partner_active.partner_id,
        order_status=COMPLETE,
        platform_id=platform_id,
        is_active=True,
        point_reward=0,
        currency_code='IDR',
        paymentgateway_id=FREE,
        temporder_id=0,
        is_renewal=False
    )
    db.session.add(new_order)
    db.session.commit()
    return new_order


def create_orderline(offer_premium=None, new_order=None):
    new_orderline = OrderLine(
        name=offer_premium.long_name,
        offer_id=offer_premium.id,
        is_active=True,
        is_free=False,
        is_discount=False,
        user_id=new_order.user_id,
        campaign_id=None,
        order_id=new_order.id,
        quantity=1,
        orderline_status=COMPLETE,
        currency_code=new_order.currency_code,
        final_price=new_order.final_amount,
        price=new_order.total_amount,
        localized_currency_code='IDR',
        localized_final_price=new_order.final_amount
    )
    db.session.add(new_orderline)
    db.session.commit()
    return new_orderline


def create_payment(new_order=None):
    new_payment = Payment(
        order_id=new_order.id,
        user_id=new_order.user_id,
        paymentgateway_id=new_order.paymentgateway_id,
        currency_code=new_order.currency_code,
        amount=new_order.final_amount,
        payment_status=PAYMENT_BILLED,
        is_active=True,
        payment_datetime=datetime.now()
    )
    db.session.add(new_payment)
    db.session.commit()
    return new_payment


def create_userbuffet(new_orderline=None, partner_active=None):

    orderline_offers = new_orderline.get_offer()
    buffet = orderline_offers.buffet[0]

    new_userbuffet = UserBuffet(
        user_id=new_orderline.user_id,
        orderline_id=new_orderline.id,
        offerbuffet_id=buffet.id,
        offerbuffet=buffet,
        is_restore=False,
        is_trial=False,
        valid_to=partner_active.expired_date
    )
    db.session.add(new_userbuffet)
    db.session.commit()
    return new_userbuffet


def deliver_subs_order(partner_active=None, partner_unique_code=None):

    exist_partner = Partner.query.filter_by(id=partner_active.partner_id).first()
    offer_mapping = exist_partner.reward_offers.get(partner_unique_code, None)
    offer_premium = Offer.query.filter_by(id=offer_mapping).first()

    try:
        if offer_premium:
            new_order = create_order(offer_premium=offer_premium, partner_active=partner_active)

            partner_active.order_id = new_order.id
            db.session.add(partner_active)
            db.session.commit()

            new_orderline = create_orderline(offer_premium=offer_premium, new_order=new_order)
            new_payment = create_payment(new_order=new_order)

            new_userbuffet = create_userbuffet(new_orderline=new_orderline, partner_active=partner_active)

    except Exception as e:
        pass
