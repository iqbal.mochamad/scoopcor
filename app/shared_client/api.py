import iso639, json

from flask import request, jsonify, g
from hashlib import sha1
from sqlalchemy import desc
from datetime import datetime
from httplib import OK, NO_CONTENT, BAD_REQUEST, INTERNAL_SERVER_ERROR, NOT_FOUND, CONFLICT
from unidecode import unidecode

from app.auth.models import Client
from app.master.choices import GETSCOOP
from app.offers.models import Offer, OfferBuffet
from app.shared_client.models import PartnerAuth
from app.users.buffets import UserBuffet
from app.users.organizations import OrganizationUser
from app.master.models import Brand, Vendor, Partner
from app.items.models import Item

from flask_appsfoundry import Resource
from app.auth.decorators import token_required, user_is_authenticated
from app.auth.helpers import get_user
from app.auth.api import get_login_response
from app.helpers import err_response
from app.items.choices.status import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.items.previews import preview_s3_covers, preview_s3_urls
from app.users.users import User, Role
from app.constants import RoleType

from .helpers import linked_partner_account, get_active_kompas_subs, deliver_subs_order

from app import app, db


def get_partner_brand_ids(user_id):
    """For list all brands by organizations"""
    exist_organization = OrganizationUser.query.filter_by(user_id=user_id).first()
    exist_brand = Brand.query.filter(Brand.vendor_id == Vendor.query.filter(
        Vendor.organization_id == exist_organization.organization_id).first().id).all()

    return [brand.id for brand in exist_brand]


def validate_partner_user_agent(request):
    """ Validate all registered user agent."""
    user_agent = request.headers.get('User-Agent', 'Unknown').split('/')[0].lower().strip().replace(" ", "_")
    exist_user_agent = Client.query.filter_by(app_name=user_agent).first()

    if not exist_user_agent:
        return None, "Invalid UserAgent. user agent is not registered!"
    return exist_user_agent, None


def format_response_data(partner_items):
    response_data = []
    for item in partner_items:
        response_data.append({
            'name': unidecode(unicode(item.name)),
            'release_date': item.release_date.isoformat(),
            'slug': item.slug,
            'cover': preview_s3_covers(item, 'image_highres', is_full_path=True),
            "languages": [iso639.find(lang_)['name'] for lang_ in item.languages] if item.languages else [],
            "vendor": item.brand.vendor.name if item.brand.vendor else "-",
            "total_page": item.page_count if item.page_count else 0,
            "authors": [author_.name for author_ in item.authors] if item.authors else [],
            "previews": preview_s3_urls(item)
        })
    return response_data


def error_response(status_code=None, message=None):
    return err_response(status=status_code, error_code=status_code, developer_message=message, user_message=message)


class SharedPartnerLibrary(Resource):

    @token_required('can_read_write_global_all, can_read_ebook_library')
    def get(self):
        """
            List all item related with vendor by user-agent

            NOTE : why not used regular api item list for our client/partner ?
                - this api detect user agent + limited JWT access, if someday agreement stop. we can just drop JWT and
                  user agent. our regular api item list intact.
        """
        args = request.args.to_dict()
        keyword_search, limit, offset = args.get('search', None), int(args.get('limit', 20)), int(args.get('offset', 0))
        user_id = g.current_user.id

        user_agent, error_message = validate_partner_user_agent(request)
        if error_message:
            return err_response(status=BAD_REQUEST, error_code=BAD_REQUEST,
                                developer_message=error_message, user_message=error_message)
        try:
            # retrieve all items by brand_id
            partner_items = Item.query.filter(Item.brand_id.in_(get_partner_brand_ids(user_id)),
                                              Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME])

            # Get all item related by catalog share, by search or limit offset
            if keyword_search:
                partner_items = partner_items.filter(Item.name.ilike('%{}%'.format(keyword_search)))

            partner_items = partner_items.order_by(desc('release_date'))
            total_count = partner_items.count()
            response_data = format_response_data(partner_items.limit(limit).offset(offset).all())

            response = jsonify({
                'data': response_data,
                'metadata': {'resultset': {'offset': offset, 'limit': limit, 'count': total_count}}})

            response.status_code = OK if len(response_data) > 0 else NO_CONTENT
            return response

        except Exception as e:
            return error_response(status_code=INTERNAL_SERVER_ERROR,
                                  message='Unable to retrieve data partner items {}'.format(e))


class SharedPartnerLibraryDetail(Resource):
    """ Detail items related by user agent vendors"""

    @token_required('can_read_write_global_all, can_read_ebook_library')
    def get(self, slug):
        user_id = g.current_user.id

        # Crosscheck user agent!
        user_agent, error_message = validate_partner_user_agent(request)
        if error_message:
            return err_response(status=BAD_REQUEST, error_code=BAD_REQUEST,
                                developer_message=error_message, user_message=error_message)

        # prevent if slug hacked.. must check with brand_ids related.
        brand_id = get_partner_brand_ids(user_id)
        exist_item = Item.query.filter(Item.slug == slug, Item.brand_id.in_(brand_id)).all()
        if not exist_item:
            return error_response(status_code=INTERNAL_SERVER_ERROR, message='Item you looking are not founds.')

        response_data = format_response_data(exist_item)[0]
        response = jsonify(response_data)
        response.status_code = OK
        return response


class SharedPartnerRegistration(Resource):

    @token_required('can_read_write_global_all, can_read_ebook_library')
    def post(self):
        """
            Registration by client

            NOTE : why not used regular api registration for our client/partner ?
                - regular api registration, have salt credential who cant exposed to client/partner.
                - this api detect user agent + limited JWT access, if someday agreement stop. we can just drop JWT and
                  user agent only. our regular api registration intact.
        :param
            - email
            - password

        :return:
            - JWT response
        """

        session = db.session

        # validate user agent if not register
        user_agent, error_message = validate_partner_user_agent(request)
        if error_message:
            return error_response(status_code=BAD_REQUEST, message=error_message)

        request_data = json.loads(request.data)
        email = request_data.get('email', None)
        password = request_data.get('password', None)

        if not email or not password:
            return error_response(status_code=BAD_REQUEST, message='Please define username and password')

        if len(password) < 6:
            return error_response(status_code=BAD_REQUEST, message='Password length, must minimum 6 characters')

        ebook_user = get_user(email, session)
        if ebook_user:
            return error_response(status_code=CONFLICT, message='{} already registered'.format(email))

        else:
            new_ebook_user = User(
                username=email.strip().lower(),
                email=email.strip().lower(),
                first_name=request_data.get('first_name', None),
                last_name=request_data.get('last_name', None),
                last_login=datetime.utcnow(),
                is_verified=False,
                excluded_from_reporting=False,
                origin_client_id=user_agent.id,
                created=datetime.utcnow()
            )

            verified_role = Role.query.get(RoleType.unverified_user.value)
            password_sha = sha1(app.config['PASSWORD_HASH'] + password.strip().lower()).hexdigest()
            new_ebook_user.set_password(password_sha)
            new_ebook_user.roles.append(verified_role)
            session.add(new_ebook_user)
            session.commit()

            device_id = request_data.get('device_id', None)
            return get_login_response(new_ebook_user, device_id)


class SharedPartnerPremium(Resource):

    @token_required('can_read_write_global_all, can_read_ebook_library')
    def post(self):
        """
            Check premium subs by client/partner

            NOTE : why not used regular api owned_buffet for our client/partner ?
                - this api detect user agent + limited JWT access, if someday agreement stop. we can just drop JWT and
                  user agent only. our regular api owned_buffet intact.
        :param
            - email

        :return:
            - package
            - expired_date
        """

        session = db.session

        # validate user agent if not register
        user_agent, error_message = validate_partner_user_agent(request)
        if error_message:
            return err_response(status=BAD_REQUEST, error_code=BAD_REQUEST,
                                developer_message=error_message, user_message=error_message)

        request_data = json.loads(request.data)
        email = request_data.get('email', None)
        if not email:
            return error_response(status_code=BAD_REQUEST, message='Email is required')

        ebook_user = get_user(email, session)
        if not ebook_user:
            return error_response(status_code=NOT_FOUND, message='Email has not registered')

        exist_partner = Partner.query.filter(Partner.clients.any(id=int(user_agent.id))).first()

        active_buffet = None
        if exist_partner:
            # get id of buffet
            buffet_offer = OfferBuffet.query.filter(OfferBuffet.offer_id.in_(exist_partner.display_offers)).all()
            buffet_ids = [buffet.id for buffet in buffet_offer]

            # get longest premium.
            active_buffet = UserBuffet.query.filter(
                UserBuffet.user_id == ebook_user.id, UserBuffet.offerbuffet_id.in_(buffet_ids),
                UserBuffet.valid_to >= datetime.utcnow()
            ).order_by(desc('valid_to')).first()

        response = jsonify({
            'offer_code': active_buffet.offerbuffet.offer.offer_code if active_buffet else None,
            'package': active_buffet.offerbuffet.offer.name if active_buffet else None,
            'expired_date': active_buffet.valid_to.replace(microsecond=0).isoformat() if active_buffet else None
        })
        response.status_code = OK
        return response


class SharePartnerLinkedListApi(Resource):

    @user_is_authenticated
    @token_required('can_read_write_global_all, can_read_write_public, can_read_write_public_ext')
    def get(self):
        user_id = g.current_user.id
        partner_data = Partner.query.filter_by(is_active=True).order_by(desc(Partner.id)).all()

        data_partner = []
        for partner in partner_data:
            user_connected = PartnerAuth.query.filter_by(user_id=user_id, partner_id=partner.id).first()

            data_partner.append(
                {
                    'partner_id': partner.id,
                    'name': partner.name,
                    'image_url': partner.image_url(),
                    'is_connected': True if user_connected else False,
                    'is_displayed': partner.is_displayed,
                    'web_success_page': partner.web_success_page,
                    'web_oauth_page': partner.web_oauth_page
                }
            )

        response = jsonify({'partners': data_partner})
        response.status_code = OK
        return response


class SharePartnerLoginApi(Resource):
    """
        Prepare this api for other partners who has same bounding account!
        winter is coming
    """

    @user_is_authenticated
    @token_required('can_read_write_global_all, can_read_write_public, can_read_write_public_ext')
    def post(self):
        try:
            request_data = json.loads(request.data)
            current_user_id = g.current_user.id
            current_client_id = g.current_client.id if getattr(g, 'current_client', GETSCOOP) \
                                                       and g.current_client else GETSCOOP

            email = request_data.get('email', None)
            password = request_data.get('password', None)
            partner_id = request_data.get('partner_id', 0)

            if not email or not password:
                return error_response(status_code=BAD_REQUEST, message='Email or Password is required')

            exist_partner = Partner.query.filter_by(id=partner_id).first()
            if not exist_partner:
                return error_response(status_code=BAD_REQUEST, message='Unregistered Partner')

            existing_linked_user = PartnerAuth.query.filter(PartnerAuth.partner_account == email.lower(),
                                                            PartnerAuth.partner_id == exist_partner.id).first()

            if existing_linked_user:
                return error_response(status_code=BAD_REQUEST,
                                      message='Account already linked with other ebooks account!')

            partner_data = linked_partner_account(exist_partner=exist_partner, email=email.lower().strip(),
                                                  password=password.strip())

            if partner_data:
                # added record link partner and ebook account!
                new_record = PartnerAuth(user_id=current_user_id, partner_account=email.lower(),
                                         partner_id=exist_partner.id, client_id=current_client_id)
                db.session.add(new_record)
                db.session.commit()

                # retrieve subs of users
                expired_subs, partner_unique_code = get_active_kompas_subs(email=email, partner_active=new_record)

                if expired_subs:
                    deliver_subs_order(partner_active=new_record, partner_unique_code=partner_unique_code)

                response = jsonify({'message': 'Account successfully linked'})
                response.status_code = OK
                return response
            else:
                return error_response(status_code=BAD_REQUEST, message='The username or password entered is incorrect')

        except Exception as e:
            return error_response(status_code=INTERNAL_SERVER_ERROR,
                                  message='Oops something wrong {}'.format(e))
