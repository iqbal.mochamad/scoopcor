from flask_appsfoundry.serializers import SerializerBase, fields


class GeneralSerializer(SerializerBase):
    api_status = fields.String
    server_timestamp = fields.DateTime('iso8601')
    api_version = fields.String
    links = fields.List(fields.String)
    client = fields.Raw
