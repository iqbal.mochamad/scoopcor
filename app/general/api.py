from datetime import datetime

from flask import g, url_for, request
from flask_appsfoundry import Resource, marshal_with

from app import __version__
from app.general import serializers
from app.utils.geo_info import get_geo_info


class GeneralApi(Resource):
    @marshal_with(serializers.GeneralSerializer())
    def get(self):

        link_urls = []
        if any([(user_perm in ['can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext',
                               'can_read_global_banner', ]) for user_perm in g.user.perm]):
            link_urls.append(url_for('banners.search'))

        geo_info = get_geo_info()

        return {
            'server_timestamp': datetime.utcnow(),
            'api_status': "Normal",
            'api_version': __version__,
            'links': link_urls,
            "client": {
                "ip": geo_info.client_ip,
                "country_code": geo_info.country_code,
                "location": {
                    "coordinates": [
                        geo_info.latitude,
                        geo_info.longitude
                    ],
                    "type": "POINT"
                }
            }
        }

