from flask_appsfoundry.models import TimestampMixin

from app import db


class Address(db.Model, TimestampMixin):
    """ Helper model that represents a postal address.
    """
    __tablename__ = 'cas_addresses'

    street1 = db.Column(db.String(250))
    street2 = db.Column(db.String(250))
    city = db.Column(db.String(20))
    postal_code = db.Column(db.String(10))
    province = db.Column(db.String(20))
    country_code = db.Column(db.String(2))

    # def __repr__(self):
    #     return self.id
