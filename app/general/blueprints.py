from flask import Blueprint
from flask_appsfoundry import Api

from app.general.api import GeneralApi


blueprint = Blueprint('general', __name__, url_prefix='/v1/')

api = Api(blueprint)

api.add_resource(GeneralApi, '')

