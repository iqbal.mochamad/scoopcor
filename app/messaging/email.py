from __future__ import unicode_literals, print_function, absolute_import
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib

from app.utils.solr_gateway import EmailLog, EmailLogEntity
from flask import render_template, current_app

from . import _log

__all__ = ('get_smtp_server', 'SmtpServer', 'HtmlEmail', )


def get_smtp_server():
    """ Helper function to fetch a configured SMTP server object.
    There MUST be a flask app context available or this will raise
    a RuntimeError.
    :return: A fully-configured SmtpServer object.
    :rtype: SmtpServer
    """
    try:
        return SmtpServer(
            host=current_app.config['EMAIL_HOST'],
            user=current_app.config['EMAIL_USER'],
            password=current_app.config['EMAIL_PASS'],
            port=current_app.config['EMAIL_PORT'],
            use_tls=current_app.config['EMAIL_USE_TLS'])
    except KeyError as e:
        _log.exception("Scoopcor e-mail settings are misconfigured!!!")
        raise e


class SmtpServer(object):
    """ A class-based way to represent a server
    """
    def __init__(self, host, user, password, port=25, use_tls=False):
        self.host = host
        self.user = user
        self.password = password
        self.port = port
        self.use_tls = use_tls

    def send_message(self, message, bcc=None, log_to_solr=True):
        email_log = EmailLog(current_app.config.get('SOLR_URL'))
        server = smtplib.SMTP(self.host, self.port)

        try:
            if self.use_tls:
                server.starttls()

            server.ehlo()

            if current_app.config['EMAIL_LOGIN']:
                server.login(self.user, self.password)

            to = message.to + bcc if bcc else message.to
            server.sendmail(message.sender,  to, message.rendered_content.as_string())
            email_log_entity = EmailLogEntity(
                sender=message.sender,
                target=message.to,
                html_template=message.html_template,
                is_error=False,
                log_info=""
            )

        except smtplib.SMTPException as e:
            _log.exception("Scoopcor send email failed. Error: {}".format(e.message))
            email_log_entity = EmailLogEntity(
                sender=message.sender,
                target=message.to,
                html_template=message.html_template,
                is_error=True,
                log_info=str(e)
            )

        server.quit()
        try:
            if log_to_solr:
                email_log.add(email_log_entity)
        except Exception as e:
            _log.exception("Scoopcor send email failed to add log. Error: {}".format(str(e)))


class HtmlEmail(object):
    """ An HTML multipart e-mail message.
    """
    def __init__(self, sender, to, subject, html_template, plaintext_template, **kwargs):
        """
        :param `six.string_types` sender: String-encoded e-mail address for the 'FROM' field.
        :param `list` to: Iterable of string-encoded e-mail address to send to.
        :param `six.string_types` subject: The subject of the e-mail
        :param `six.string_types` html_template: Name of the JINJA2 template to use for
            the e-mail message's text/html content.
        :param `six.string_types` plaintext_template: name of the JINJA2 template to use for
            the e-mail message's text/plain content.
        :param kwargs: Context arguments that will be passed to the
            JINJA2 template when rendering.
        :return:
        """
        self.sender = sender
        self.to = to
        self.subject = subject
        self.html_template = html_template
        self.plaintext_template = plaintext_template
        self.message_context = kwargs

    @property
    def rendered_content(self):
        """

        :return:
        """
        # Construct email
        msg = MIMEMultipart('alternative')
        msg.set_charset('utf8')

        msg['To'] = ', '.join(self.to)
        msg['From'] = self.sender
        msg['Subject'] = self.subject

        # Create the body of the message (a plain-text and an HTML version).
        text = render_template(self.plaintext_template, **self.message_context)
        msg.attach(MIMEText(text, 'plain', _charset="UTF-8"))

        html = render_template(self.html_template, **self.message_context)
        msg.attach(MIMEText(html, 'html', _charset="UTF-8"))

        return msg

