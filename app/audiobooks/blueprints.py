from flask import Blueprint
import flask_restful as restful

from .api import AudioMetaApi, AudioDownloadApi

audio_blueprint = Blueprint('audio-items', __name__, url_prefix='/v1/items')
audio_api = restful.Api(audio_blueprint)

audio_api.add_resource(AudioMetaApi, '/<item:item>/download/<int:chapter>/audio-meta', endpoint='audio-meta')
audio_api.add_resource(AudioDownloadApi, '/<item:item>/download/<int:chapter>/audio', endpoint='audio-download')
