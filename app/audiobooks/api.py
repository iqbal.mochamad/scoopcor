from httplib import BAD_REQUEST, OK

from flask import g, jsonify, make_response
from flask_restful import Resource
from flask_appsfoundry.exceptions import Unauthorized

from app.auth.decorators import user_is_authenticated
from app.helpers import err_response
from app.items.choices import AUDIOBOOK, ITEM_TYPES
from app.items.helpers import construct_nginx_internal_download_url
from app.items.models import UserItem
from app.users.users import is_apps_foundry_users

from app.uploads.aws.helpers import aws_shared_link

from app import app

def user_owned_audio(exist_user, item, chapter):
    ''' Validate ownership or access for audiobooks '''

    if not exist_user: raise Unauthorized
    meta_display, error_message = False, None

    # check if user has this items or not.
    owned_item = UserItem.query.filter(UserItem.user_id == exist_user.id, UserItem.item_id == item.id,
        UserItem.itemtype_id == AUDIOBOOK, UserItem.is_active == True).first()
    if owned_item: meta_display = True

    # check if user has super admin or employee access.
    if ('can_read_write_global_all' in g.user.perm or is_apps_foundry_users(g.current_user)) and not meta_display:
        meta_display = True

    if not meta_display: error_message = "You dont have any access for download this Item"
    if not item.audiobooks.count(): error_message = "This item are not audiobooks."

    data_audio = item.audiobooks.filter_by(is_active=True, chapter=chapter).first()
    if not data_audio: error_message = "This item has no chapter : {}".format(chapter)
    return error_message, data_audio


class AudioMetaApi(Resource):

    @user_is_authenticated
    def get(self, item, chapter):
        exist_user = g.current_user
        owned_audio, data_audio = user_owned_audio(exist_user, item, chapter)

        if owned_audio:
            return err_response(status=BAD_REQUEST, error_code=BAD_REQUEST,
                                developer_message=owned_audio, user_message=owned_audio)

        # Haiyo, something not right with android/ios library encryption.
        # Extract encrypted zip use old style SC1 key, read encrypted PDF/epub or audio file use Key.
        # this two step should be use a same param KEY only.
        from app.items.helpers import EncryptData
        old_style_key = EncryptData(key=data_audio.hash_key, item_id=item.id).construct()

        response_data = {'file_time': data_audio.file_time, 'chapter': data_audio.chapter,
            'file_type': ITEM_TYPES[AUDIOBOOK], 'file_pattern': data_audio.file_pattern,
            'key': data_audio.hash_key, 'old_style_key': old_style_key}
        response_data.update(data_audio.metainfo)

        resp = jsonify(response_data)
        resp.status_code = OK
        return resp


class AudioDownloadApi(Resource):

    @user_is_authenticated
    def get(self, item, chapter):
        exist_user = g.current_user
        owned_audio, data_audio = user_owned_audio(exist_user, item, chapter)

        if owned_audio:
            return err_response(status=BAD_REQUEST, error_code=BAD_REQUEST, developer_message=owned_audio, user_message=owned_audio)

        bucket = app.config['BOTO3_GRAMEDIA_AUDIO_MOBILE_BUCKET']
        file_bucket_path = '{}/{}'.format(item.id, data_audio.file_name)
        shared_link = aws_shared_link(bucket, file_bucket_path)

        if shared_link:
            link_url = shared_link.replace('https://ebook-audiobook-bundle.s3.amazonaws.com/', '')
            url_link = '{}/{}'.format(app.config['NGINX_S3_AUDIO_MOBILE_BUNDLES'], link_url)
        else:
            url_link = construct_nginx_internal_download_url(item)

        response = make_response("", OK, {'Content-Type': "application/zip", 'X-Accel-Redirect': url_link})
        return response
