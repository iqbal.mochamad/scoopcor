from app import db
from flask_appsfoundry.models import TimestampMixin
from sqlalchemy.dialects.postgresql import ARRAY, JSONB
from humanize import naturalsize

class AudioBook(db.Model, TimestampMixin):

    __tablename__ = 'core_audiobooks'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(250), nullable=False)
    subtitle = db.Column(db.String(100), nullable=False)
    chapter = db.Column(db.Integer)
    file_name = db.Column(db.String(100))
    file_size = db.Column(db.Integer)
    file_time = db.Column(db.String(25))
    file_pattern = db.Column(ARRAY(db.String))
    item_id = db.Column(db.Integer, db.ForeignKey('core_items.id'))
    item = db.relationship('Item')
    description = db.Column(db.Text)
    hash_key = db.Column(db.String(250))
    metainfo = db.Column(JSONB)
    is_active = db.Column(db.Boolean(), default=True)


    def values(self):
        return {
            'title': self.title,
            'subtitle': '{} {}'.format(self.subtitle, self.chapter),
            'file_size': naturalsize(self.file_size) if self.file_size > 0 else '- MB',
            'file_pattern': self.file_pattern,
            'file_name': self.file_name,
            'file_time': self.file_time,
            'chapter': self.chapter
        }
