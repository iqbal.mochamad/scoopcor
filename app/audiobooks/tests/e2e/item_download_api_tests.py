from __future__ import unicode_literals, absolute_import

from httplib import OK, FORBIDDEN
from flask import g
from nose.tools import nottest, istest

from app import app, db, set_request_id
from app.auth.models import UserInfo, ANONYMOUS_USER
from app.audiobooks.models import AudioBook
from app.audiobooks.tests.functional.fixtures.sqlalchemy.items import AudioBookItemFactory
from app.users.users import User
from tests import testcases


@nottest
class ItemDownloadAudioApiTestsBase(testcases.DetailTestCase):
    """ Base class for testing valid response from api """

    fixture_factory = AudioBookItemFactory
    request_url = 'items/{0.id}/download/1/audio'
    request_headers = {'Accept': 'application/zip'}

    @classmethod
    def mocked_g_user(cls):
        pass

    @classmethod
    def setUpClass(cls):
        ctx = app.test_request_context('/')
        ctx.push()
        cls.mocked_g_user()
        super(ItemDownloadAudioApiTestsBase, cls).setUpClass()

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        s.expire_on_commit = False
        cls.item = s.query(AudioBook).first()
        return cls.item

    def test_response_status_code(self):
        self.assertEqual(OK, self.response.status_code)

    def test_response_content_type(self):
        self.assertEqual(self.response.content_type, 'application/zip')

    def test_x_accel_redirect(self):
        self.assertIn('x-accel-redirect', self.response.headers)
        actual_download_url = self.response.headers.get('x-accel-redirect')

        expected_url = '{amazon_url}/{ebook_bundle}/{chapter}.zip'.format(
            amazon_url='https://ebook-audiobook-bundle.s3.amazonaws.com/',
            ebook_bundle=app.config['BOTO3_GRAMEDIA_AUDIO_MOBILE_BUCKET'],
            chapter=self.item.chapter)
        self.assertEqual(actual_download_url, expected_url)

    def test_content_length(self):
        self.assertEqual(self.response.content_length, 0)

    @classmethod
    def tearDownClass(cls):
        if getattr(g, 'current_user', None):
            db.session.expunge(g.current_user)
        db.session.expunge(cls.item)
        db.session.expunge_all()


def get_user(session, user_id):
    return session.query(User).get(user_id)

def not_authorized_user(*args, **kwargs):
    g.user = ANONYMOUS_USER
    g.current_user = None

def super_admin_user(*args, **kwargs):
    g.user = UserInfo(username="kiratakada@gmail.com", user_id=1234, token='abcd',
                      perm=['can_read_write_global_all', 'b', 'c', ])


@nottest
class InvalidUserDownloadTestsBase(testcases.DetailTestCase):
    """ Tests forbidden/invalid user item download """

    fixture_factory = AudioBookItemFactory
    request_url = 'items/{0.id}/download/1/audio'
    request_headers = {'Accept': 'application/zip'}

    @classmethod
    def mocked_g_user(cls):
        app.before_request_funcs = {None: [set_request_id, not_authorized_user,]}

    @classmethod
    def setUpClass(cls):
        ctx = app.test_request_context('/')
        ctx.push()
        cls.mocked_g_user()
        super(InvalidUserDownloadTestsBase, cls).setUpClass()

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        s.expire_on_commit = False
        cls.item = s.query(AudioBook).first()
        return cls.item

    def test_response_code(self):
        self.assertEqual(FORBIDDEN, self.response.status_code)

    @classmethod
    def tearDownClass(cls):
        if getattr(g, 'current_user', None):
            db.session.expunge(g.current_user)
        db.session.expunge(cls.item)
        db.session.expunge_all()


@nottest
class UserAdminDownloadTestsBase(testcases.DetailTestCase):
    """ Tests SUPER USER / Company access can download """

    fixture_factory = AudioBookItemFactory
    request_url = 'items/{0.id}/download/1/audio'
    request_headers = {'Accept': 'application/zip'}

    @classmethod
    def mocked_g_user(cls):
        app.before_request_funcs = {None: [set_request_id, super_admin_user,]}

    @classmethod
    def setUpClass(cls):
        ctx = app.test_request_context('/')
        ctx.push()
        cls.mocked_g_user()
        super(InvalidUserDownloadTestsBase, cls).setUpClass()

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        s.expire_on_commit = False
        cls.item = s.query(AudioBook).first()
        return cls.item

    def test_response_code(self):
        self.assertEqual(OK, self.response.status_code)

    @classmethod
    def tearDownClass(cls):
        if getattr(g, 'current_user', None):
            db.session.expunge(g.current_user)
        db.session.expunge(cls.item)
        db.session.expunge_all()
