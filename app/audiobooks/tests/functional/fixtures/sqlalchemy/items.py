from factory import Faker
from factory.alchemy import SQLAlchemyModelFactory
from app.audiobooks.models import AudioBook

from app import db

class AudioBookItemFactory(SQLAlchemyModelFactory):
    title = Faker('text')
    subtitle = Faker('text')
    chapter = 1
    file_name = 'Bab-1.zip'
    file_size = '6468501'
    file_time = '07:04'
    hash_key = '44a428ef70b764b6ac1dc399b34b87e6652ac494407fbd454b5ae580c2777704'
    metainfo = {"file_second": 424.273560090703,
                "zip_checksum": "760ca49fb40d0a29a24b378a46b28dad",
                "file_checksum": "a9174dc0b30ae35e135a408356c287b7"
                }

    class Meta:
        sqlalchemy_session = db.session
        model = AudioBook
