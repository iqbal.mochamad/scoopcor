import os, random, subprocess, hashlib, string

from app import app, db
from app.audiobooks.models import AudioBook
from datetime import datetime
from app.items.models import Item, UserItem
from pydub import AudioSegment

from app.users.users import User
from app.utils.shims import item_types

SPLIT_CHUNKED = 60000
CONVERT_EXT = "mp3"
FILE_DIRECTORY = '/tmp/audiobooks'


class AudioConverter():

    def __init__(self, item_id, file_name, file_format, args):
        self.args = args
        self.item = Item.query.filter_by(id=item_id).first()
        self.file_name = file_name
        self.file_format = file_format
        self.metainfo = {}
        self.file_chunks = []
        self.file_pattern = []
        self.file_time = None
        self.master_audio = None

    def create_audiobook(self):
        ''' Temporary create audiobook files with status FALSE, this change True if all process Complete.! '''
        new_audiobook = AudioBook(
            title = self.args.get('title', 'Unknown'),
            subtitle= self.args.get('subtitle', 'Unknown'),
            chapter=int(self.args.get('chapter', 1)),
            is_active=False,
            item_id=self.item.id)
        db.session.add(new_audiobook)
        db.session.commit()
        self.master_audio = new_audiobook

        # Bond to item parent.
        self.item.audiobooks.append(new_audiobook)
        db.session.add(self.item)
        db.session.commit()

    def reformat_zero(self, data):
        return '0{data}'.format(data=data) if data < 10 else data

    def seconds_totime(self, time):
        ''' meta mp3 have no info for duration in string HH:MM, only microseconds.
            this func allow you to convert microseconds to HH:MM formats. '''

        time = time % (24 * 3600)
        hour = time // 3600
        time %= 3600
        minutes = int(time // 60)
        time %= 60
        seconds = int(time) + 1

        self.file_time = '{minutes}:{seconds}'.format(minutes=self.reformat_zero(minutes), seconds=self.reformat_zero(seconds))
        return hour, minutes, seconds

    def generate_file_naming(self, size=60, chars=string.ascii_uppercase + string.digits):
        ''' randomize file naming for chunks file. '''
        return ''.join(random.choice(chars) for _ in range(size)).lower()

    def generate_file_version(self):
        ''' Generate file version, have no idea this Formula where is come. from SC1. ^^ '''

        file_version1 = '{item_id}{edition_code}{custom_date}{release_date}'.format(item_id=self.item.id,
            edition_code=self.item.edition_code, custom_date=datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            release_date=self.item.release_date)

        file_version2 = '{brand_id}{custom_price}{custom_date}'.format(brand_id=self.item.brand_id, custom_price='0.99',
            custom_date=datetime.now().strftime('%Y-%m-%d %H:%M:%S'))

        version1 = hashlib.md5(file_version1).hexdigest()
        version2 = hashlib.md5(file_version2).hexdigest()
        file_version = '{version1}{version2}'.format(version1=version1, version2=version2)

        # This format come from SC1 formula
        pass_zip = "{item_id}{custom_str}{zip_salt}{custom_str2}{brand_id}{custom_str3}{file_version}".format(
            item_id=self.item.id, custom_str='~#%&', zip_salt=app.config['GARAMZIP'], custom_str2='*&#@!',
            brand_id=self.item.brand_id, custom_str3='&%^)', file_version=file_version)

        custom_zip_password = hashlib.sha256(pass_zip).hexdigest()
        custom_file_version = file_version
        return custom_zip_password, custom_file_version

    def reformat_audiobooks(self):
        ''' Create chunks with randomize name of files. '''
        try:
            file_path = os.path.join(FILE_DIRECTORY, self.file_name)
            if self.item and os.path.exists(file_path):
                audiofile = AudioSegment.from_file(file_path, format=self.file_format)
                self.metainfo['file_second'] = audiofile.duration_seconds
                self.metainfo['file_checksum'] = hashlib.md5(file_path).hexdigest()

                self.seconds_totime(audiofile.duration_seconds)

                for idata, chunk in enumerate(audiofile[::SPLIT_CHUNKED]):
                    named_files = self.generate_file_naming()
                    new_names = "{}.{}".format(named_files, CONVERT_EXT)

                    self.file_pattern.append(new_names)
                    new_files = os.path.join(FILE_DIRECTORY, new_names)
                    with open(new_files, "wb") as f:
                        chunk.export(f, format=CONVERT_EXT.format(named_files))

            return True, 'Successfully!'

        except Exception as e:
            return False, 'reformat_audiobooks Error {}'.format(e)

    def create_zip_files(self):
        ''' Create zip and update metadata of audio'''
        try:
            zip_password, zip_file_version = self.generate_file_version()
            new_zip_name = '{}-{}.zip'.format(self.master_audio.subtitle, self.master_audio.chapter)
            zip_path = os.path.join(FILE_DIRECTORY, new_zip_name)

            zip_args = ['7za', 'a', '-p' + zip_password] + [zip_path] + [FILE_DIRECTORY + '/{}'.format(i) for i in self.file_pattern]
            # zip_args = ['7za', 'a'] + [zip_path] + [FILE_DIRECTORY + '/{}'.format(i) for i in self.file_pattern]
            subprocess.call(zip_args)

            args_command = ['du', zip_path]
            subprocess.Popen(args_command, stdout=subprocess.PIPE)

            if os.path.exists(zip_path):
                self.metainfo['zip_checksum'] = hashlib.md5(zip_path).hexdigest()

                # update meta audiofiles.
                self.master_audio.file_size = os.path.getsize(zip_path)
                self.master_audio.file_time = self.file_time
                self.master_audio.file_name = new_zip_name
                self.master_audio.file_pattern = self.file_pattern
                self.master_audio.hash_key = zip_file_version
                self.master_audio.is_active = True
                self.master_audio.metainfo = self.metainfo

                db.session.add(self.master_audio)
                db.session.commit()

            clear_args = ['rm', '-r'] + [FILE_DIRECTORY + '/{}'.format(i) for i in self.file_pattern]
            subprocess.call(clear_args)
            return True, 'Success'

        except Exception as e:
            return False, 'create_zip_files Failed {}'.format(e)

    def process(self):
        self.create_audiobook()
        self.reformat_audiobooks()
        self.create_zip_files()


def deliver_audibooks(users=None, audio_id=None):
    """ Small script only for patch audiobooks item to specified users cloud """

    for user in users:
        exist_user = User.query.filter_by(email=user).first()
        exist_audio = Item.query.filter_by(id=audio_id).first()

        if exist_user and exist_audio:
            exist_cloud = UserItem.query.filter_by(user_id=exist_user.id, item_id=exist_audio.id).first()

            if not exist_cloud:
                new_user_item = UserItem(
                    user_id=exist_user.id,
                    item_id=exist_audio.id,
                    edition_code=exist_audio.edition_code,
                    orderline_id=0,
                    is_active=True,
                    itemtype_id=item_types.enum_to_id(exist_audio.item_type),
                    brand_id=exist_audio.brand_id,
                    vendor_id=exist_audio.brand.vendor_id
                )
                db.session.add(new_user_item)
                db.session.commit()
                print 'update success patch {}'.format(exist_user.email)
            else:
                print 'user already have audioboks'
        else:
            print '---------------------- User not found {}'.format(user)
