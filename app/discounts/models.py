from datetime import datetime

from app import db
from flask_appsfoundry.models import TimestampMixin
from sqlalchemy.dialects.postgresql import ARRAY

from app.campaigns.models import Campaign

"""
    OFFER DISCOUNT RULE:
        1. BY AMOUNT
        2. BY PERCENTAGE
        3. BY TO AMOUNT #flush all price, with discount price

    DISCOUNT TYPE:
        1.  OFFER DISCOUNT
        2.  ORDER DISCOUNT
        3.  OFFER PAYMENT GATEWAY DISCOUNT
        4.  ORDER PAYMENT GATEWAY DISCOUNT
        5.  DISCOUNT CODE

    DISCOUNT STATUS:
        1. NEW
        2. ONGOING
        3. EXPIRED

    SCHEDULE TYPE:
        1. IMMEDIATE
        2. SCHEDULED

    DISCOUNT CODE TYPE
        1. OVERIDE OFFER DISCOUNT
        2. CAN'T OVERIDE OFFER DISCOUNT

    PREDEFINED GROUP :
        1. ALL MAGAZINES ( SUBS + SINGLE )
        2. ALL BOOKS ( SINGLE )
        3. ALL NEWSPAPERS ( SUBS )
        4. ALL OFFERS

        # ADDITIONAL FOR DISCOUNT CODES
        5. ALL SINGLE ( MAGAZINES + BOOK )
        6. ALL SUBSCRIPTIONS ( MAGAZINES + NEWSPAPER SUBS)

        # MAGAZINE ONLY
        7. MAGAZINE SINGLE.
        8. MAGAZINE SUBSCRIPTIONS.
        9. ALL MAGAZINE ( SUBS + SINGLE ) + ALL NEWSPAPERS ( SUBS )

"""

#junk table discount --> offer
discount_offer = db.Table(
    'core_discounts_offers',
    db.Column('discount_id', db.Integer, db.ForeignKey('core_discounts.id')),
    db.Column('offer_id', db.Integer, db.ForeignKey('core_offers.id'))
)

# discount x, only valid for items/offers that belong to these vendor only
discount_vendor = db.Table(
    'core_discounts_vendors',
    db.Column('discount_id', db.Integer, db.ForeignKey('core_discounts.id')),
    db.Column('vendor_id', db.Integer, db.ForeignKey('core_vendors.id'))
)

#junk table discount --> platforms
discount_platform = db.Table(
    'core_discounts_platforms',
    db.Column('discount_id', db.Integer, db.ForeignKey('core_discounts.id')),
    db.Column('platform_id', db.Integer, db.ForeignKey('core_platforms.id'))
)

#junk table discount --> paymentgateway
discount_payment = db.Table(
    'core_discounts_paymentgateways',
    db.Column('discount_id', db.Integer, db.ForeignKey('core_discounts.id')),
    db.Column('paymentgateway_id', db.Integer, db.ForeignKey('core_paymentgateways.id'))
)

#junk table discount --> user
discount_code_user = db.Table(
    'core_discountcodes_users',
    db.Column('discountcode_id', db.Integer, db.ForeignKey('core_discountcodes.id')),
    db.Column('user_id', db.Integer, db.ForeignKey('cas_users.id'))
)

class Discount(db.Model, TimestampMixin):

    __tablename__ = 'core_discounts'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250), nullable=False)
    tag_name = db.Column(db.String(250))
    description = db.Column(db.Text)
    campaign_id = db.Column(db.Integer, db.ForeignKey('core_campaigns.id'), nullable=False)
    campaign = db.relationship(Campaign, backref=db.backref("discounts", lazy="dynamic"))

    valid_to = db.Column(db.DateTime, nullable=False)
    valid_from = db.Column(db.DateTime, nullable=False)

    #status-status-an
    discount_rule = db.Column(db.Integer) #READ LEGEND : OFFER DISCOUNT RULE
    discount_type = db.Column(db.Integer) #READ LEGEND : DISCOUNT TYPE
    discount_status = db.Column(db.Integer) #READ LEGEND : DISCOUNT STATUS
    discount_schedule_type = db.Column(db.Integer) #READ LEGEND : DISCOUNT SCHEDULE TYPE
    is_active = db.Column(db.Boolean(), default=False)

    discount_usd = db.Column(db.Numeric(10, 2), nullable=False)
    discount_idr = db.Column(db.Numeric(10, 2), nullable=False)
    discount_point = db.Column(db.Integer)

    min_usd_order_price = db.Column(db.Numeric(10, 2))
    max_usd_order_price = db.Column(db.Numeric(10, 2))
    min_idr_order_price = db.Column(db.Numeric(10, 2))
    max_idr_order_price = db.Column(db.Numeric(10, 2))

    predefined_group = db.Column(db.Integer)

    vendor_participation = db.Column(db.Integer)
    partner_participation = db.Column(db.Integer)
    sales_recognition = db.Column(db.Integer)

    # this only for BIN CC Promotions
    bin_codes = db.Column(ARRAY(db.String), nullable=True)
    trial_time = db.Column(db.Interval)

    offers = db.relationship(
        'Offer',
        secondary=discount_offer,
        backref=db.backref('core_discounts', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    vendors = db.relationship(
        'Vendor',
        secondary=discount_vendor,
        backref=db.backref('discounts', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    platforms = db.relationship(
        'Platform',
        secondary=discount_platform,
        backref=db.backref('core_discounts', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    paymentgateways = db.relationship(
        'PaymentGateway',
        secondary=discount_payment,
        backref=db.backref('core_discounts', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    def __repr__(self):
        return '<name : %s>' % self.name

    def offer_id(self):
        return [offer.id for offer in self.offers]

    def platform_id(self):
        return [plat.id for plat in self.platforms]

    def paymentgateway_id(self):
        return [payment.id for payment in self.paymentgateways]

    def discount_valid(self):
        valid = False

        if datetime.now() >= self.valid_from and datetime.now() <= self.valid_to:
            valid = True

        return valid

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Discount success add"}
        except Exception, e:
            db.session.rollback()
            #db.session.delete(self)
            #db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Discount success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def calculate_offer_discount(self, raw_price=None, currency=None):
        """
            OFFER DISCOUNT RULE:
            1. BY AMOUNT
            2. BY PERCENTAGE
            3. BY TO AMOUNT #flush all price, with discount price
        """
        final_price = 0
        if currency == 'USD':
            if self.discount_usd:
                if self.discount_rule == 1: #by amount
                    final_price = raw_price - self.discount_usd

                if self.discount_rule == 2: #by percentage
                    raw_price = float(raw_price)
                    discount_price = raw_price * (float(self.discount_usd) * 0.01)

                    # get only two digits from . points
                    # dont use round.!!! its gonna make mess
                    data = str(discount_price).split('.')
                    discount_price = float('%s.%s' % (data[0], data[1][:2]))

                    final_price = raw_price - discount_price

                if self.discount_rule == 3: #flush price
                    final_price = self.discount_usd

                if final_price <= 0:
                    final_price = 0
            else:
                # this case if self.discount_usd set 0.00, return raw price
                final_price = raw_price

        if currency == 'IDR':
            if self.discount_idr:
                if self.discount_rule == 1: #by amount
                    final_price = raw_price - self.discount_idr

                if self.discount_rule == 2: #by percentage
                    raw_price = float(raw_price)
                    discount_price = raw_price * (float(self.discount_idr) * 0.01)
                    final_price = raw_price - discount_price

                if self.discount_rule == 3: #flush price
                    final_price = self.discount_idr

                if final_price <= 0:
                    final_price = 0
            else:
                # this case if self.discount_idr set 0.00, return raw price
                final_price = raw_price

        if currency == 'PTS':
            if self.discount_point:
                if self.discount_rule == 1: #by amount
                    final_price = raw_price - self.discount_point

                if self.discount_rule == 2: #by percentage
                    discount_price = raw_price * (self.discount_point * 0.01)
                    final_price = raw_price - discount_price

                if self.discount_rule == 3: #flush price
                    final_price = self.discount_point

                if final_price <= 0:
                    final_price = 0
            else:
                # this case if self.discount_idr set 0.00, return raw price
                final_price = raw_price

        return final_price

    def custom_values(self, custom=None):
        #customize values
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}
        rv['id'] = self.id

        rv['name'] = self.name
        rv['tag_name'] = self.tag_name
        rv['description'] = self.description
        rv['campaign_id'] = self.campaign_id
        rv['valid_to'] = self.valid_to.isoformat()
        rv['valid_from'] = self.valid_from.isoformat()
        rv['discount_rule'] = self.discount_rule
        rv['discount_type'] = self.discount_type
        rv['is_active'] = self.is_active
        rv['discount_usd'] = '%.2f' % self.discount_usd
        rv['discount_idr'] = '%.2f' % self.discount_idr
        rv['discount_point'] = self.discount_point
        rv['discount_status'] = self.discount_status
        rv['discount_schedule_type'] = self.discount_schedule_type
        rv['predefined_group'] = self.predefined_group
        rv['vendor_participation'] = self.vendor_participation
        rv['partner_participation'] = self.partner_participation
        rv['sales_recognition'] = self.sales_recognition
        rv['bin_codes'] = self.bin_codes

        if self.offers:
            rv['offers'] = [{'id': i.id, 'name': i.name } for i in self.offers]
        else:
            rv['offers'] = []

        if self.min_usd_order_price:
            rv['min_usd_order_price'] = '%.2f' % self.min_usd_order_price
        if self.max_usd_order_price:
            rv['max_usd_order_price'] = '%.2f' % self.max_usd_order_price
        if self.min_idr_order_price:
            rv['min_idr_order_price'] = '%.2f' % self.min_idr_order_price
        if self.max_idr_order_price:
            rv['max_idr_order_price'] = '%.2f' % self.max_idr_order_price

        return rv


class DiscountCode(db.Model, TimestampMixin):

    __tablename__ = 'core_discountcodes'

    id = db.Column(db.Integer, primary_key=True)

    code = db.Column(db.String(250), nullable=False, unique=True)
    max_uses = db.Column(db.Integer)
    current_uses = db.Column(db.Integer)
    is_active = db.Column(db.Boolean(), default=False)
    discount_type = db.Column(db.Integer) #READ LEGEND : DISCOUNT CODE TYPE
    discount_id = db.Column(db.Integer, db.ForeignKey('core_discounts.id'), nullable=False)
    discount = db.relationship('Discount')
    is_for_new_user = db.Column(db.Boolean(), default=False)

    users = db.relationship(
        'User',
        secondary=discount_code_user,
        backref=db.backref('core_discountcodes', lazy='dynamic'),
        lazy='dynamic',
        uselist=True
    )

    def __repr__(self):
        return '<code : %s>' % (self.code)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "DiscountCode success add"}
        except Exception, e:
            db.session.rollback()
            #db.session.delete(self)
            #db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "DiscountCode success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def coupon_can_used(self):
        if self.current_uses >= self.max_uses:
            return False
        else:
            return True

    def get_discount(self):
        return Discount.query.filter_by(id=self.discount_id).first() if self.discount_id else None

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        return {
            'id': self.id,
            'discount_code': self.code,
            'max_uses': self.max_uses,
            'current_uses': self.current_uses if self.current_uses else 0,
            'is_active': self.is_active,
            'discount_type': self.discount_type,
            'discount_id': self.discount_id,
            'discount': {'id': self.discount.id, 'name': self.discount.name}
        }


class DiscountCodePermittedUsers(db.Model):
    """ Many-to-many mapping used to restrict redemption of discount codes to
    only a certain subset of Users.

    .. note::

        At the current time, the 'user_id' constraint cannot be enforced because
        it lives in the scoopcas database.
    """
    __tablename__ = 'core_discountcodes_permitted_users'
    discount_code_id = db.Column(
        db.Integer,
        db.ForeignKey('core_discountcodes.id'),
        nullable=False,
        primary_key=True)
    discount_code = db.relationship('DiscountCode')
    user_id = db.Column(
        db.Integer, nullable=False, doc="ScoopCas user.id", primary_key=True)


class DiscountCodeLog(db.Model, TimestampMixin):

    __tablename__ = 'core_logs_discountcodes'

    id = db.Column(db.Integer, primary_key=True)

    code = db.Column(db.String(250), nullable=False)
    max_uses = db.Column(db.Integer)
    current_uses = db.Column(db.Integer)
    is_active = db.Column(db.Boolean(), default=False)
    discount_type = db.Column(db.Integer) #READ LEGEND : DISCOUNT CODE TYPE
    discount_id = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return '<code : %s>' % (self.code)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "DiscountCode success add"}
        except Exception, e:
            db.session.rollback()
            db.session.delete(self)
            db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "DiscountCode success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def get_discount(self):
        dsc = ''
        discount = Discount.query.filter_by(id=self.discount_id).first()
        if discount:
            dsc = discount
        return dsc

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['discount_code'] = self.code
        rv['max_uses'] = self.max_uses

        if self.current_uses:
            rv['current_uses'] = self.current_uses
        else:
            rv['current_uses'] = 0

        rv['is_active'] = self.is_active
        rv['discount_id'] = self.discount_id
        rv['discount_type'] = self.discount_type

        return rv
