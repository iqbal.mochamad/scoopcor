import json, httplib, random, string

from datetime import datetime

from flask_restful import Resource, Api, reqparse
from flask_restful import Api, reqparse
from flask import abort, Response, jsonify, request

from app import app, db
from app.constants import *
from app.auth.decorators import token_required

from app.helpers import log_api, assertion_error, message_json
from app.helpers import dtserializer, err_response, date_validator
from app.helpers import conflict_message, bad_request_message, internal_server_message

from sqlalchemy.sql import and_, or_, desc

from app.discounts.models import Discount, DiscountCode
from app.offers.models import Offer, OfferPlatform
from app.master.models import Platform
from app.campaigns.models import Campaign
from app.payment_gateways.models import PaymentGateway
from app.tiers.models import TierIos, TierAndroid, TierWp

from app.items.custom_get import CustomGet
from app.items.models import Item
from app.items.choices.item_type import ITEM_TYPES, BOOK, MAGAZINE, NEWSPAPER
from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME

from app.discounts.choices.predefined_type import ALL_MAGAZINE, ALL_BOOK, ALL_NEWSPAPERS
from app.discounts.choices.predefined_type import ALL_OFFERS, ALL_SINGLE, ALL_SUBSCRIPTIONS
from app.discounts.choices.predefined_type import MAGAZINE_SINGLE, MAGAZINE_SUBSCRIPTIONS, MAGAZINE_NEWSPAPER


class DiscountConstruct():

    def __init__(self, args=None, method=None, old_data=None):
        self.args = args
        self.method = method
        self.old_data = old_data

        self.campaign_id = args.get('campaign_id', None)
        self.valid_from = args.get('valid_from', '')
        self.valid_to = args.get('valid_to', '')

        self.discount_type = args.get('discount_type', None)
        self.discount_schedule_type = args.get('discount_schedule_type', None)

        self.discount_usd = args.get('discount_usd', None)
        self.discount_idr = args.get('discount_idr', None)

        self.min_usd_order_price = args.get('min_usd_order_price', None)
        self.max_usd_order_price = args.get('max_usd_order_price', None)
        self.min_idr_order_price = args.get('min_idr_order_price', None)
        self.max_idr_order_price = args.get('max_idr_order_price', None)

        self.offers = args.get('offers', None)
        self.platforms = args.get('platforms', None)
        self.paymentgateways = args.get('paymentgateways', None)

        self.discount_plat = []
        self.discount_off = []
        self.discounts_ids = []

        self.predefined_group = args.get('predefined_group', 0)

        self.master_discounts = ''

    def bad_request(self, bad=None, msg=None):
        return err_response(status=httplib.BAD_REQUEST,
                            error_code=CLIENT_400101,
                            developer_message="%s" % (msg),
                            user_message="Bad Request for %s" % bad)

    def invalid_check(self, data=None, module=None):
        """
            FUNCTION FOR CHECKING DATA IS NULL/ZERO VALUE
        """
        if (data == 0 or str(data) == "0" or data == ""):
            return 'FAILED', self.bad_request(module,
                                              "%s can't 0 or NONE" % module)
        elif data is None:
            return 'FAILED', self.bad_request(module,
                                              "%s can't 0 or NONE" % module)
        else:
            return 'OK', 'OK'

    def check_campaign(self):
        try:
            campaign = Campaign.query.filter_by(id=self.campaign_id).first()
            if not campaign:
                return 'FAILED', bad_request_message(modul="discount 1", method=self.method,
                                                     param="campaign_id", not_found=self.campaign_id, req=self.args)
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="discount 1", method=self.method,
                                                     error=e, req=str(self.args))

    def check_date(self):
        """
            CHECKING DATE FORMAT YYYY-MM-DD
            INVALID IF VALID_TO < VALID_FROM
        """
        try:
            valid_from = date_validator(self.valid_from, 'custom')
            if valid_from.get('invalid', False):
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                                              error_code=httplib.BAD_REQUEST,
                                              developer_message="Invalid valid from",
                                              user_message="valid from invalid")

            valid_to = date_validator(self.valid_to, 'custom')
            if valid_to.get('invalid', False):
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                                              error_code=httplib.BAD_REQUEST,
                                              developer_message="Invalid valid to",
                                              user_message="valid to invalid")

            if self.valid_to <= self.valid_from:
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                                              error_code=httplib.BAD_REQUEST,
                                              developer_message="Invalid valid to must greater than valid from",
                                              user_message="valid to invalid")

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="discount 2", method=self.method,
                                                     error=e, req=str(self.args))

    def check_order_discounts(self):
        """
            IF DISCOUNT TYPE DISCOUNT ORDER (PAYMENT ORDER, OFFER ORDER) TYPE 2,4
            MIN_ AND MAX_ PRICE CAN'T EMPTY.
        """
        try:
            if self.discount_type in [2, 4]:
                xo = self.invalid_check(self.min_usd_order_price, 'min_usd_order_price')
                if xo[0] != 'OK':
                    return "FAILED", xo[1]

                xo = self.invalid_check(self.max_usd_order_price, 'max_usd_order_price')
                if xo[0] != 'OK':
                    return "FAILED", xo[1]

                xo = self.invalid_check(self.min_idr_order_price, 'min_idr_order_price')
                if xo[0] != 'OK':
                    return "FAILED", xo[1]

                xo = self.invalid_check(self.max_idr_order_price, 'max_idr_order_price')
                if xo[0] != 'OK':
                    return "FAILED", xo[1]

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="discount 3", method=self.method,
                                                     error=e, req=str(self.args))

    def check_offer(self):
        """
            1. CHECKING OFFER EXIST/NOT EXIST
            2. CHECKING OFFER IF ALREADY HAVE DISCOUNT OR NOT (OFFER DISCOUNT TYPE)
        """
        try:
            data_offer = []
            for ioffer in self.offers:
                offer = Offer.query.filter_by(id=ioffer).first()
                if not offer:
                    return 'FAILED', bad_request_message(modul="discount 4", method=self.method,
                                                         param="offers", not_found=ioffer, req=self.args)

                if (offer.is_free or offer.price_usd <= 0):
                    return 'FAILED', self.bad_request(bad='offer_id: %s' % ioffer,
                                                      msg="offer %s is FREE, can't assign discount" % ioffer)

                # CHECKING OFFER DISCOUNT, 1 OFFER vs 1 DISCOUNT TYPE
                # COMMENT OUT COS, 1 OFFER CAN ALLOW 2 DISCOUNT OFFER NOW.
                # if offer.is_discount:
                #    for xio in offer.discount_id:
                #        discount = Discount.query.filter_by(id=xio).first()
                #        if discount:
                #            if discount.discount_type == 5:
                #                pass
                #            else:
                #                if (discount.discount_type == self.discount_type and discount.discount_valid()):
                #                    return 'FAILED', self.bad_request(bad='offer_id: %s' % ioffer,
                #                        msg="offer %s, Still active with promo : %s" % (ioffer, discount.name))

                data_offer.append(offer)
            self.offers = data_offer
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="discount 4", method=self.method,
                                                     error=e, req=str(self.args))

    def check_platforms(self):
        """
            CHECK PLATFORM EXIST/NOT EXIST
        """
        try:
            data_platform = []
            for iplat in self.platforms:
                plat = Platform.query.filter_by(id=iplat).first()
                if not plat:
                    return 'FAILED', bad_request_message(modul="discount 5", method=self.method,
                                                         param="platforms", not_found=iplat, req=self.args)
                data_platform.append(plat)

            self.platforms = data_platform
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="discount 5", method=self.method,
                                                     error=e, req=str(self.args))

    def check_payment_gateways(self):
        """
            CHECK PAYMENT GATEWAY EXIST/NOT EXIST
        """
        try:
            data_payment = []
            for ipay in self.paymentgateways:
                pay = PaymentGateway.query.filter_by(id=ipay).first()
                if not pay:
                    return 'FAILED', bad_request_message(modul="discount 6", method=self.method,
                                                         param="paymentgateways", not_found=ipay, req=self.args)
                data_payment.append(pay)

            self.paymentgateways = data_payment
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="discount 6", method=self.method,
                                                     error=e, req=str(self.args))

    def check_discount_payment(self):
        """
            CHECKING DISCOUNT PAYMENT GATEWAY, DISCOUNT ORDER ONLY AVAILABLE 1
        """
        try:
            discount = Discount.query.filter_by(discount_type=4).all()
            if discount:
                for ipox in discount:
                    reg_payment = ipox.paymentgateway_id()

                    for pymnt in self.paymentgateways:
                        if pymnt.id in reg_payment and ipox.discount_valid():
                            return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                                                          error_code=httplib.BAD_REQUEST,
                                                          developer_message="Discount Order payment gateway only 1 can exist,"
                                                                            "please un-activated the old one",
                                                          user_message="Invalid Discount")
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="discount 7", method=self.method,
                                                     error=e, req=str(self.args))

    # -==================== post ================================================
    def post_discounts(self):
        try:
            kwargs = {}
            for item in self.args:
                if self.args[item] is None:
                    pass
                else:
                    kwargs[item] = self.args[item]

            if self.offers:
                del kwargs['offers']

            if self.platforms:
                del kwargs['platforms']

            if self.paymentgateways:
                del kwargs['paymentgateways']

            # overwrite discount status, regarding discount_schedule_type
            if self.discount_schedule_type == 1:  # IMMEDIATE STATUS
                kwargs['discount_status'] = 1  # NEW STATUS
                kwargs['is_active'] = True
            if self.discount_schedule_type == 2:  # SCHEDULED STATUS
                kwargs['discount_status'] = 2  # SCHEDULED USED CRON JOB,
                kwargs['is_active'] = False

            m = Discount(**kwargs)

            if self.offers:
                for i in self.offers:
                    m.offers.append(i)

            if self.platforms:
                for z in self.platforms:
                    m.platforms.append(z)

            if self.paymentgateways:
                for x in self.paymentgateways:
                    m.paymentgateways.append(x)

            sv = m.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="discount 8",
                                                         method=self.method, error=sv.get('message', ''),
                                                         req=str(self.args))
            else:
                rv = m.values()
                self.master_discounts = m
                return 'OK', Response(json.dumps(rv), status=httplib.CREATED,
                                      mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="discount 8", method=self.method,
                                                     error=e, req=str(self.args))

    def post_discount_offer(self):
        """
            UPDATE OFFER PRICES
        """

        try:
            # IF DISCOUNT TYPE 1 (OFFER DISCOUNT), UPDATE OFFER PRICE
            self.discounts_ids = []
            platforms_id = [i.id for i in self.platforms]

            if self.discount_type == 1:
                for offer in self.offers:
                    if offer.is_free:
                        offer.discount_usd = 0
                        offer.discount_idr = 0
                        offer.discount_point = 0
                    else:
                        usd_price = self.master_discounts.calculate_offer_discount(
                            offer.price_usd, 'USD')
                        idr_price = self.master_discounts.calculate_offer_discount(
                            offer.price_idr, 'IDR')
                        point_price = self.master_discounts.calculate_offer_discount(
                            offer.price_point, 'PTS')

                        offer.discount_price_usd = usd_price
                        offer.discount_price_idr = idr_price
                        offer.discount_price_point = point_price
                        offer.discount_tag = self.master_discounts.tag_name
                        offer.discount_name = self.master_discounts.name
                        offer.is_discount = True

                        # update discount_id
                        if offer.discount_id:
                            discount_ids = list(set(offer.discount_id))

                            self.discounts_ids = discount_ids
                            if self.master_discounts.id not in self.discounts_ids:
                                self.discounts_ids.append(self.master_discounts.id)
                            offer.discount_id = self.discounts_ids

                        else:
                            offer.discount_id = [self.master_discounts.id]

                    sv = offer.save()
                    if sv.get('invalid', False):
                        return 'FAILED', internal_server_message(modul="discount 9",
                                                                 method=self.method, error=sv.get('message', ''),
                                                                 req=str(self.args))

                    xo = self.post_discount_platform_offer(offer)
                    if xo[0] != 'OK':
                        return 'FAILED', xo[1]

            # ANOTHER DISCOUNT NOT CALCULATE OFFER PRICE, JUST UPDATE DISCOUNT_ID DATA
            if self.discount_type in [2, 3, 4, 5]:
                for offer in self.offers:

                    # UPDATE OFFER BASE DISCOUNT_ID ONLY
                    # temp_discount = []
                    if offer.is_discount:

                        discount_ids = list(set(offer.discount_id))
                        self.discounts_ids = discount_ids

                        if self.master_discounts.id not in self.discounts_ids:
                            self.discounts_ids.append(self.master_discounts.id)
                        offer.discount_id = self.discounts_ids

                        # for ix in offer.discount_id:
                        #    temp_discount.append(ix)
                        # temp_discount.append(self.master_discounts.id)
                        # offer.discount_id = temp_discount

                    else:
                        offer.discount_id = [self.master_discounts.id]

                    sv = offer.save()
                    if sv.get('invalid', False):
                        return 'FAILED', internal_server_message(modul="discount 9",
                                                                 method=self.method, error=sv.get('message', ''),
                                                                 req=str(self.args))

                    # UPDATE OFFER PLATFORMS DISCOUNT_ID ONLY
                    for iplat in platforms_id:
                        offplatform = OfferPlatform.query.filter_by(offer_id=offer.id, platform_id=iplat).all()

                        for dsc in offplatform:
                            if dsc.discount_id:
                                discount_ids = list(set(dsc.discount_id))
                                self.discounts_ids = discount_ids

                                if self.master_discounts.id not in self.discounts_ids:
                                    self.discounts_ids.append(self.master_discounts.id)
                                dsc.discount_id = self.discounts_ids

                                # self.discount_plat = [i for i in dsc.discount_id]
                                # for i in dsc.discount_id:
                                #    if self.master_discounts.id not in self.discount_plat:
                                #        self.discount_plat.append(self.master_discounts.id)
                                # dsc.discount_id = self.discount_plat
                            else:
                                dsc.discount_id = [self.master_discounts.id]
                            so = dsc.save()

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="discount 9", method=self.method,
                                                     error=e, req=str(self.args))

    def post_discount_platform_offer(self, offer=None):
        """
            UPDATE PLATFORM DISCOUNT OF OFFERS
        """
        try:
            self.discounts_ids = []
            platforms = [i.id for i in self.platforms]

            for iplat in platforms:
                offer_platform = OfferPlatform.query.filter_by(offer_id=offer.id, platform_id=iplat).all()

                for dsc in offer_platform:
                    # calculate discounts
                    if dsc.price_usd:
                        usd_price = self.master_discounts.calculate_offer_discount(
                            dsc.price_usd, 'USD')
                        dsc.discount_price_usd = usd_price

                    if dsc.price_idr:
                        idr_price = self.master_discounts.calculate_offer_discount(
                            dsc.price_idr, 'IDR')
                        dsc.discount_price_idr = idr_price

                    if dsc.price_point:
                        point_price = self.master_discounts.calculate_offer_discount(
                            dsc.price_point, 'PTS')
                        dsc.discount_price_point = point_price

                    # update platform offer discount info
                    dsc.discount_tag = self.master_discounts.tag_name
                    dsc.discount_name = self.master_discounts.name

                    # update discount_id
                    if dsc.discount_id:
                        discount_ids = list(set(dsc.discount_id))
                        self.discounts_ids = discount_ids

                        if self.master_discounts.id not in self.discounts_ids:
                            self.discounts_ids.append(self.master_discounts.id)
                        dsc.discount_id = self.discounts_ids

                        # self.discount_plat = [i for i in dsc.discount_id]
                        # for i in dsc.discount_id:
                        #    if self.master_discounts.id not in self.discount_plat:
                        #        self.discount_plat.append(self.master_discounts.id)
                        # dsc.discount_id = self.discount_plat
                    else:
                        dsc.discount_id = [self.master_discounts.id]

                    # Non consumable type 2 subscriptions,
                    # discount_tier_id == tier_id
                    if offer.offer_type_id == 2:
                        dsc.discount_tier_id = dsc.tier_id

                        # update price
                        price = str(usd_price).split('.')
                        final = float('%s.%s' % (price[0], 99))
                        dsc.discount_tier_price = final

                        dsc.tier_code = dsc.get_tier_code()
                        dsc.discount_tier_code = dsc.get_tier_code()

                    # Consumable
                    # discount_tier_id != tier_id, base USD VALUE
                    if offer.offer_type_id in [1, 3]:
                        price = str(usd_price).split('.')
                        final = float('%s.%s' % (price[0], 99))

                        dsc.tier_code = dsc.get_tier_code()

                        if dsc.platform_id == 1:
                            data = TierIos.query.filter_by(tier_price=final, tier_type=2).first()
                        if dsc.platform_id == 2:
                            data = TierAndroid.query.filter_by(tier_price=final).first()
                        if dsc.platform_id == 3:
                            data = TierWp.query.filter_by(tier_price=final).first()

                        if data:
                            dsc.discount_tier_id = data.id
                            dsc.discount_tier_price = data.tier_price
                            dsc.discount_tier_code = data.tier_code

                    sv = dsc.save()
                    if sv.get('invalid', False):
                        return 'FAILED', internal_server_message(modul="discount 10",
                                                                 method=self.method, error=sv.get('message', ''),
                                                                 req=str(self.args))

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="discount 10", method=self.method,
                                                     error=e, req=str(self.args))

    def put_discounts(self):
        try:
            kwargs = {}
            for item in self.args:
                if self.args[item] is None:
                    pass
                else:
                    kwargs[item] = self.args[item]

            if self.offers:
                del kwargs['offers']

            if self.platforms:
                del kwargs['platforms']

            if self.paymentgateways:
                del kwargs['paymentgateways']

            if self.offers:
                for old_offer in self.old_data.offers:
                    self.old_data.offers.remove(old_offer)

                for new_offer in self.offers:
                    self.old_data.offers.append(new_offer)

            if self.platforms:
                for old_platform in self.old_data.platforms:
                    self.old_data.platforms.remove(old_platform)

                for new_platform in self.platforms:
                    self.old_data.platforms.append(new_platform)

            if self.paymentgateways:
                for old_pay in self.old_data.paymentgateways:
                    self.old_data.paymentgateways.remove(old_pay)

                for new_pay in self.paymentgateways:
                    self.old_data.paymentgateways.append(new_pay)

            for k, v in kwargs.iteritems():
                setattr(self.old_data, k, v)

            sv = self.old_data.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="item", method=self.method,
                                                         error=sv.get('message', ''), req=str(self.args))

            return "OK", Response(json.dumps(self.old_data.values()), status=httplib.CREATED,
                                  mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="discount 11", method=self.method,
                                                     error=e, req=str(self.args))

    def construct(self):
        date = self.check_date()
        if date[0] != 'OK':
            return date[1]

        campaign = self.check_campaign()
        if campaign[0] != 'OK':
            return campaign[1]

        check_order = self.check_order_discounts()
        if check_order[0] != 'OK':
            return check_order[1]

        if not self.predefined_group:
            check_offers = self.check_offer()
            if check_offers[0] != 'OK':
                return check_offers[1]

        check_platforms = self.check_platforms()
        if check_platforms[0] != 'OK':
            return check_platforms[1]

        payment = self.check_payment_gateways()
        if payment[0] != 'OK':
            return payment[1]

        if self.discount_type == 4:
            pg = self.check_discount_payment()
            if pg[0] != 'OK':
                return pg[1]

        if self.method == 'POST':
            discount = self.post_discounts()
            if discount[0] != 'OK':
                return discount[1]

            if not self.predefined_group:
                discount_offer = self.post_discount_offer()
                if discount_offer[0] != 'OK':
                    return discount_offer[1]

        if self.method == 'PUT':
            if not self.predefined_group:
                discount = self.put_discounts()
                if discount[0] != 'OK':
                    return discount[1]

        return discount[1]


class DiscountCodeConstruct():

    def __init__(self, args={}):
        self.args = args

    def generate_invoice(self, size=5, chars=string.ascii_uppercase + string.digits):
        data = ''.join(random.choice(chars) for _ in range(size))
        return data.strip().lower()

    def generate_code(self, total=1):
        coupon_code_list, i = [], 0
        while i != total:
            code = '{}{}'.format(self.args.get('discount_prefix', ''), self.generate_invoice())
            if code not in coupon_code_list:
                coupon_code_list.append(code)
                i += 1

        data_coupon = []
        for disc_code in coupon_code_list:
            new_code = DiscountCode(
                discount_id=self.args.get('discount_id', None),
                discount_type=self.args.get('discount_type', 1),
                max_uses=self.args.get('max_uses', 1),
                is_active=True,
                code=disc_code)
            db.session.add(new_code)
            db.session.commit()

            data_coupon.append(new_code.code)
        return data_coupon

    def create(self):
        if self.args is None:
            return []

        return self.generate_code(self.args.get('total_coupon', 1))


class DiscountCodeItems():

    def __init__(self, discount=None, limit=None, offset=None, fields=None):
        self.discount = discount
        self.item_ids = []
        self.master = Item.query
        self.limit = limit
        self.limit_predefined = 100
        self.offset = offset
        self.fields = fields

        self.mag, self.book, self.new = '', '', ''

    def filter_items(self):
        param, scene = {}, []

        if self.discount.predefined_group:

            if self.discount.predefined_group == ALL_MAGAZINE:
                self.master = self.master.filter_by(
                                        item_type=ITEM_TYPES[MAGAZINE],
                                        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                        is_active=True).order_by(desc(Item.release_date)).limit(self.limit_predefined)\
                                        .offset(self.offset)

            if self.discount.predefined_group == ALL_BOOK:
                self.master = self.master.filter_by(
                                        item_type=ITEM_TYPES[BOOK],
                                        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                        is_active=True).order_by(desc(Item.release_date)).limit(self.limit_predefined)\
                                        .offset(self.offset)

            if self.discount.predefined_group == ALL_NEWSPAPERS:
                self.master = self.master.filter_by(
                                        item_type=ITEM_TYPES[NEWSPAPER],
                                        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                        is_active=True).order_by(desc(Item.release_date)).limit(self.limit_predefined)\
                                        .offset(self.offset)

            if self.discount.predefined_group == ALL_SINGLE:
                self.mag = self.master.filter_by(
                                        item_type=ITEM_TYPES[MAGAZINE],
                                        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                        is_active=True).order_by(desc(Item.release_date)).limit(self.limit_predefined)\
                                        .offset(self.offset)

                self.book = self.master.filter_by(
                                        item_type=ITEM_TYPES[BOOK],
                                        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                        is_active=True).order_by(desc(Item.release_date)) .limit(self.limit_predefined)\
                                        .offset(self.offset)

                self.master = self.mag.all() + self.book.all()

            if self.discount.predefined_group in [ALL_SUBSCRIPTIONS, MAGAZINE_NEWSPAPER]:
                self.mag = self.master.filter_by(
                                        item_type=ITEM_TYPES[MAGAZINE],
                                        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                        is_active=True).order_by(desc(Item.release_date)).limit(self.limit_predefined)\
                                        .offset(self.offset)

                self.new = self.master.filter_by(
                                        item_type=ITEM_TYPES[NEWSPAPER],
                                        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                        is_active=True).order_by(desc(Item.release_date)).limit(self.limit_predefined)\
                    .offset(self.offset)

                self.master = self.mag.all() + self.new.all()

            if self.discount.predefined_group in [MAGAZINE_SUBSCRIPTIONS, MAGAZINE_SINGLE]:
                self.master = self.master.filter_by(
                                        item_type=ITEM_TYPES[MAGAZINE],
                                        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                        is_active=True).order_by(desc(Item.release_date)).limit(self.limit_predefined)\
                                        .offset(self.offset)

            if self.discount.predefined_group in [ALL_MAGAZINE, ALL_BOOK,
                                                  ALL_NEWSPAPERS, MAGAZINE_SINGLE, MAGAZINE_SUBSCRIPTIONS]:
                self.item_ids = [str(i.id) for i in self.master.all()]

            if self.discount.predefined_group in [ALL_SINGLE, ALL_SUBSCRIPTIONS, MAGAZINE_NEWSPAPER]:
                self.item_ids = [str(i.id) for i in self.master]

        else:
            offer_ids = self.discount.offer_id()
            offer = list(set(offer_ids))

            for ioffer in offer:
                offer_data = Offer.query.filter_by(id=ioffer, offer_status=7, is_active=True).first()

                # single
                if offer_data.offer_type_id == 1:
                    self.item_ids.append(str(offer_data.get_items()[0]))

                # subscriptions
                if offer_data.offer_type_id in [2, 4]:
                    brands = offer_data.get_brands_id()

                    for ibrand in brands:
                        item = Item.query.filter_by(
                                            brand_id=ibrand,
                                            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                                            is_active=True).order_by(desc(Item.release_date)).first()

                        if item:
                            self.item_ids.append(str(item.id))

        item_ids = ','.join(self.item_ids)

        param['item_id'] = item_ids
        param['expand'] = 'authors,brand,display_offers'
        param['order'] = '-release_date'
        param['fields'] = self.fields
        param['limit'] = self.limit
        param['offset'] = self.offset

        scene = CustomGet(param, 'ITEMS').filter_custom()

        return scene
