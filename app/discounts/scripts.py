import decimal
from sqlalchemy import desc

from app.discounts.choices.discount_type import DISCOUNT_CODE, DISCOUNT_OFFER
from app.master.choices import ANDROID, IOS, WINDOWS_PHONE
from app.offers.models import Offer, OfferPlatform
from app.tiers.models import TierAndroid
from app.discounts.models import Discount
from datetime import datetime
from app import db, app

'''
    this functionality prepared for CS portal promo clean for offers
'''

class DiscountClear():

    def __init__(self, discount_id=None):

        if discount_id:
            self.discounts = Discount.query.filter_by(id=discount_id).all()
        else:
            start_date = datetime.now().date().replace(month=1, day=1)
            self.discounts = Discount.query.filter(Discount.valid_from > start_date,
                Discount.valid_to < datetime.now(), Discount.discount_type == DISCOUNT_OFFER).order_by(desc(Discount.id)).all()

    def clean_offers(self, latest_discount, offer, discount_valid_ids):
        if latest_discount:
            disc_price_usd = latest_discount.calculate_offer_discount(offer.price_usd, 'USD')
            disc_price_idr = latest_discount.calculate_offer_discount(offer.price_idr, 'IDR')
            disc_price_point = latest_discount.calculate_offer_discount(offer.price_point, 'PTS')

            offer.discount_id = discount_valid_ids
            offer.discount_tag = latest_discount.tag_name
            offer.discount_name = latest_discount.name
            offer.discount_price_usd = disc_price_usd
            offer.discount_price_idr = disc_price_idr
            offer.discount_price_point = disc_price_point
            offer.is_discount = True
        else:
            offer.discount_id = discount_valid_ids
            offer.discount_tag = None
            offer.discount_name = None
            offer.discount_price_usd = None
            offer.discount_price_idr = None
            offer.discount_price_point = None
            offer.is_discount = False

        db.session.add(offer)
        db.session.commit()

        # clean details items
        for data_item in offer.items:
            redis_key = 'items-details-{}*'.format(data_item.id)
            for data_redis in app.kvs.keys(redis_key):
                app.kvs.delete(data_redis)

    def clean_platform_offers(self, latest_discount, offer, discount_valid_ids):

        if latest_discount:
            disc_price_usd = latest_discount.calculate_offer_discount(offer.price_usd, 'USD')
            disc_price_idr = latest_discount.calculate_offer_discount(offer.price_idr, 'IDR')
            disc_price_point = latest_discount.calculate_offer_discount(offer.price_point, 'PTS')

            if offer.platform_id == IOS:
                # tier code always same.
                offer.discount_id = discount_valid_ids
                offer.discount_tier_id = offer.tier_id
                offer.discount_tier_code = offer.tier_code
                offer.discount_tag = latest_discount.tag_name
                offer.discount_name = latest_discount.name
                offer.discount_tier_price = offer.price_usd

                price = str(disc_price_usd).split('.')
                final = decimal.Decimal('%s.%s' % (price[0], 99))
                offer.discount_price_usd = final

                offer.discount_price_idr = disc_price_idr
                offer.discount_price_point = disc_price_point

            if offer.platform_id == ANDROID:
                tier = TierAndroid.query.filter(TierAndroid.price_idr >= disc_price_idr).order_by(
                    TierAndroid.price_idr).first()

                offer.discount_id = discount_valid_ids
                offer.discount_tier_id = tier.id
                offer.discount_tier_code = tier.tier_code
                offer.discount_tag = latest_discount.tag_name
                offer.discount_name = latest_discount.name
                offer.discount_tier_price = disc_price_usd
                offer.discount_price_usd = disc_price_usd
                offer.discount_price_idr = disc_price_idr
                offer.discount_price_point = disc_price_point

        else:
            offer.discount_id = discount_valid_ids
            offer.discount_tier_id = None
            offer.discount_tier_code = None
            offer.discount_tag = None
            offer.discount_name = None
            offer.discount_tier_price = None
            offer.discount_price_usd = None
            offer.discount_price_idr = None
            offer.discount_price_point = None

        db.session.add(offer)
        db.session.commit()

    def clean_promo_offers(self):
        try:
            for discount in self.discounts:
                for offer in discount.offers:
                    if offer.discount_id:
                        old_discount_id = offer.discount_id
                        coupon_valid_discount = Discount.query.filter(
                            Discount.id.in_(offer.discount_id), Discount.valid_to > datetime.now(),
                            Discount.discount_type==DISCOUNT_CODE).order_by(desc(Discount.id)).all()

                        offer_valid_discount = Discount.query.filter(
                            Discount.id.in_(offer.discount_id), Discount.valid_to > datetime.now(),
                            Discount.discount_type == DISCOUNT_OFFER).order_by(
                            desc(Discount.id)).all()

                        latest_offer_discount = offer_valid_discount[0] if offer_valid_discount else None
                        discount_valid = coupon_valid_discount + offer_valid_discount
                        discount_valid_ids = [i.id for i in discount_valid] if discount_valid else []

                        self.clean_offers(latest_offer_discount, offer, discount_valid_ids)
                        print 'Offer Clear up {} - {} to {}'.format(offer.id, old_discount_id, discount_valid_ids)
                    else:
                        pass

        except Exception as e:
            print 'clean_promo_offers Error {}'.format(e)

    def clean_promo_platform_offers(self):
        try:
            for discount in self.discounts:
                for offer in discount.offers:
                    offer_platforms = OfferPlatform.query.filter_by(offer_id=offer.id).order_by(desc(OfferPlatform.id)).all()

                    for pl_offer in offer_platforms:
                        if pl_offer.discount_id:
                            old_discount_id = pl_offer.discount_id
                            coupon_valid_discount = Discount.query.filter(
                                Discount.id.in_(offer.discount_id), Discount.valid_to > datetime.now(),
                                Discount.discount_type == DISCOUNT_CODE).order_by(desc(Discount.id)).all()

                            offer_valid_discount = Discount.query.filter(
                                Discount.id.in_(offer.discount_id), Discount.valid_to > datetime.now(),
                                Discount.discount_type == DISCOUNT_OFFER).order_by(desc(Discount.id)).all()

                            latest_offer_discount = offer_valid_discount[0] if offer_valid_discount else None
                            discount_valid = coupon_valid_discount + offer_valid_discount
                            discount_valid_ids = [i.id for i in discount_valid] if discount_valid else []

                            self.clean_platform_offers(latest_offer_discount, pl_offer, discount_valid_ids)
                            print 'Offer Platform Clear up {} - {} to {}'.format(pl_offer.offer_id, old_discount_id, discount_valid_ids)
                        else:
                            pass

        except Exception as e:
            print 'clean_promo_platform_offers Error {}'.format(e)

    def clean_up(self):
        self.clean_promo_platform_offers()
        self.clean_promo_offers()
