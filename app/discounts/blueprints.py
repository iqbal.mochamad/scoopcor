from flask import Blueprint
from flask.ext import restful

from .api import DiscountListApi, DiscountApi, DiscountCodeListApi, \
    DiscountCodeApi, DiscountCodeValid, DiscountCodeItem, \
    MigrateDiscount

#
# api.add_resource(DiscountListApi, '/v1/discounts')
# api.add_resource(DiscountApi, '/v1/discounts/<int:discount_id>', endpoint='discounts')
discounts_blueprint = Blueprint('discounts', __name__, url_prefix='/v1/discounts')
discounts_api = restful.Api(discounts_blueprint)
discounts_api.add_resource(DiscountListApi, '')
discounts_api.add_resource(DiscountApi, '/<int:discount_id>', endpoint='discounts')


# api.add_resource(DiscountCodeListApi, '/v1/discount_codes')
# api.add_resource(DiscountCodeApi, '/v1/discount_codes/<int:discount_id>', endpoint='discount_codes')
# api.add_resource(DiscountCodeValid, '/v1/discount_codes/code/<discount_code>', endpoint='discount_codes_search')
# api.add_resource(DiscountCodeItem, '/v1/discount_codes/code/<discount_code>/items', endpoint='discount_codes_item')
discount_codes_blueprint = Blueprint('discount_codes', __name__, url_prefix='/v1/discount_codes')
discount_codes_api = restful.Api(discount_codes_blueprint)
discount_codes_api.add_resource(DiscountCodeListApi, '')
discount_codes_api.add_resource(DiscountCodeApi, '/<int:discount_id>', endpoint='discount_codes')
discount_codes_api.add_resource(DiscountCodeValid, '/code/<discount_code>', endpoint='discount_codes_search')
discount_codes_api.add_resource(DiscountCodeItem, '/code/<discount_code>/items', endpoint='discount_codes_item')


# #api.add_resource(LinkDiscountCode, '/v1/link_discount_codes')
# api.add_resource(MigrateDiscount, '/v1/migrate_discountoffers')
discount_misc_blueprint = Blueprint('discount_misc', __name__, url_prefix='/v1')
discount_misc_api = restful.Api(discount_misc_blueprint)
# discount_misc_api.add_resource(LinkDiscountCode, '/link_discount_codes')
discount_misc_api.add_resource(MigrateDiscount, '/migrate_discountoffers')
