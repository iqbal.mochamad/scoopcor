import json, httplib

from flask import Response
from datetime import datetime
from sqlalchemy.sql import desc
from app.discounts.models import Discount, DiscountCode


class CustomGet():

    def __init__(self, param=None, module=None, method=None):

        self.limit = param.get('limit', 20)
        self.offset = param.get('offset', 0)
        self.fields = param.get('fields', None)
        self.active_only = param.get('active_only', False)
        self.discount_type = param.get('discount_type', False)
        self.q = param.get('q', None)

        self.module = module
        self.method = method
        self.master = ''
        self.total = 0

    def json_success(self, data_items=None, offset=None, limit=None):

        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            if self.fields:
                cur_values = [u.custom_values(self.fields) for u in data_items]
            else:
                cur_values = [u.values() for u in data_items]

            temp={}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}
            rv = json.dumps({self.module: cur_values, "metadata": tempx})
            return Response(rv, status=httplib.OK, mimetype='application/json')

    def discount_master(self):
        self.total = Discount.query.count()
        return Discount.query

    def discount_code_master(self):
        self.total = DiscountCode.query.count()
        return DiscountCode.query

    def construct(self):

        if self.q:
            self.q = "%s%s%s" % ('%', self.q, '%')

        if self.module == 'discounts':
            self.master = self.discount_master()

            # have no idea why frontend ios/android send 1 or 0 and portal send True/False and web send 'true'/'false' ^^.
            if self.active_only in (True, 1, 'true'):
                self.master = self.master.filter(Discount.valid_to >= datetime.now())

            if self.q:
                self.master = self.master.filter(Discount.name.ilike(self.q))

            if self.discount_type:
                self.master = self.master.filter_by(discount_type=self.discount_type)
            self.master = self.master.order_by(desc(Discount.id))

        if self.module == 'discount_codes':
            self.master = self.discount_code_master()

            if self.q:
                self.master = self.master.filter(DiscountCode.code.ilike(self.q))
            self.master = self.master.order_by(desc(DiscountCode.id))

        self.total = self.master.count()
        self.master = self.master.limit(self.limit).offset(self.offset).all()
        return self.json_success(self.master, self.offset, self.limit)

