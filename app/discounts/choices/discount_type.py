DISCOUNT_OFFER = 1
DISCOUNT_ORDER = 2
DISCOUNT_PG_OFFER = 3
DISCOUNT_PG_ORDER = 4
DISCOUNT_CODE = 5

DISCOUNT_TYPES = {
    DISCOUNT_OFFER : 'discount offers',
    DISCOUNT_ORDER : 'discount order',
    DISCOUNT_PG_OFFER : 'discount payment gateway offer',
    DISCOUNT_PG_ORDER : 'discount paytment gateway order',
    DISCOUNT_CODE : 'discount code'
}
