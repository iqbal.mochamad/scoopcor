ALL_MAGAZINE = 1 #(subs & single)
ALL_BOOK = 2 #(single)
ALL_NEWSPAPERS = 3 #(subs)
ALL_OFFERS = 4 #(all magazine, book, newspaper)
ALL_SINGLE = 5 #(single magazine and book)
ALL_SUBSCRIPTIONS = 6 #(subscriptions magazine and newspapers)

MAGAZINE_SINGLE = 7 #(magazine single only)
MAGAZINE_SUBSCRIPTIONS = 8 #(magazine subs only)
MAGAZINE_NEWSPAPER = 9 #(all magazine single/subs + all newspaper single/subs)

HARPER_COLLINS = 467


DISCOUNT_PREDEFINED_GROUP = {
    ALL_MAGAZINE: 'All Magazine Subscriptions and Single Offer',
    ALL_BOOK: 'All Book Single Offer',
    ALL_NEWSPAPERS: 'All Newspaper Subscriptions Offers',
    ALL_OFFERS: 'All Item Offers',
    ALL_SINGLE: 'All Magazine and Book Single Offers',
    ALL_SUBSCRIPTIONS: 'All Magazine and Newspapers Subscription Offers',
    MAGAZINE_SINGLE: 'All Magazine Single Offers',
    MAGAZINE_SUBSCRIPTIONS: 'All Magazine Subscription Offers',
    MAGAZINE_NEWSPAPER: 'All Magazine and Newspapers Single/Subscription Offers',
    HARPER_COLLINS: 'Harper Collins Item Only'
}
