"""
    PATCH RETENTION CODES
"""
import string
import random

from datetime import datetime, timedelta

from app.campaigns.models import Campaign
from app.discounts.models import Discount, DiscountCode
from app.master.models import Platform

from app.payment_gateways.models import PaymentGateway


class RetentionCode():

    def __init__(self, campaign_id=None, start_date=None):
        self.campaign_id = campaign_id
        self.start_date = start_date

        self.master_campaign = None
        self.master_discount = None

        self.master_platform = []
        self.master_payment = []

    def master_data(self):
        try:
            # >>> CAMPAIGN
            # use exist campaign id
            if self.campaign_id:
                campaign = Campaign.query.filter_by(id=self.campaign_id).first()
                if campaign:
                    self.master_campaign = campaign
            else:
                campaign = Campaign(
                    name = "Retentions code promo",
                    description = "Daily retention code, start : 12 March 2015. end: 30 April 2015",
                    is_active = True
                )
                campaign.save()
                self.master_campaign = campaign

            # >>>>> PLATFORMS
            platforms_id = [4]
            for i in platforms_id:
                platform = Platform.query.filter_by(id=i).first()
                self.master_platform.append(platform)

            # >>>> PAYMENTS
            payment_id = [2,5,6,11,12,14,15,16]
            for i in payment_id:
                payment = PaymentGateway.query.filter_by(id=i).first()
                self.master_payment.append(payment)

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', e

    def id_generator(self, size=2, chars=string.ascii_uppercase):
        return ''.join(random.choice(chars) for _ in range(size))

    def create_discounts(self):

        try:
            valid_from = datetime.strptime(self.start_date, '%Y-%m-%d')
            valid_to = valid_from + timedelta(days=2)

            midle_code = self.id_generator()

            raw_coupon = 'SCOOP-%s-%s' % (midle_code,
                datetime.strftime(valid_from, '%d%m%Y'))
            coupon_code = raw_coupon.lower()

            discounts = Discount(
                name = 'Retention code - %s' % coupon_code,
                tag_name = 'Retention code',
                description = "Daily retention code, start : 12 March 2015. end: 30 April 2015",
                campaign_id = self.master_campaign.id,

                valid_to = valid_to,
                valid_from = valid_from,

                discount_rule = 2,
                discount_type = 5,
                discount_status = 1,
                discount_schedule_type = 1,
                is_active = True,

                discount_usd = 20,
                discount_idr = 20,
                discount_point = 0,

                predefined_group = 9
            )

            for i in self.master_platform:
                discounts.platforms.append(i)

            for j in self.master_payment:
                discounts.paymentgateways.append(j)

            discounts.save()

            # create coupon_codes
            discount_code = DiscountCode(
                code = coupon_code,
                max_uses = 1000000,
                current_uses = 1,
                is_active = True,
                discount_type = 1,
                discount_id = discounts.id
            )
            discount_code.save()

            #print discounts
            print 'date : %s coupon-code : %s ' % (datetime.strftime(valid_from, '%d-%m-%Y'), coupon_code)
            return 'OK', 'OK'

        except Exception, e:
            print 'ERRROOOOR', e
            return 'FAILED', e

    def construct(self):

        master_data = self.master_data()
        if master_data[0] != 'OK':
            return master_data[1]

        discounts = self.create_discounts()
        if discounts[0] != 'OK':
            return discounts[1]

        return 'OK'
