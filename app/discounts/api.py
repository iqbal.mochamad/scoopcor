import ast
import json
import httplib

from flask_restful import Resource, Api, reqparse
from flask_restful import Api, reqparse
from flask import abort, Response, jsonify, request
from marshmallow import fields

from app import app, db, ma
from app.constants import *
from app.auth.decorators import token_required

from app.helpers import log_api, assertion_error, message_json, err_response
from app.helpers import dtserializer, err_response, date_validator
from app.helpers import conflict_message, bad_request_message, internal_server_message

from sqlalchemy.sql import and_, or_, desc

from app.discounts.models import Discount, DiscountCode, DiscountCodeLog
from app.discounts.custom_get import CustomGet

from app.discounts.helpers import DiscountConstruct, DiscountCodeConstruct, DiscountCodeItems
from app.offers.models import Offer, OfferPlatform
from app.payments.choices.gateways import POINT
from app.tiers.models import TierAndroid, TierIos, TierWp
from app.utils.marshmallow_helpers import schema_load


class DiscountListApi(Resource):
    ''' Shows a list of Discount and do POST / GET '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('name', type=unicode, required=True, location='json')
        self.reqparser.add_argument('tag_name', type=str, required=True, location='json')
        self.reqparser.add_argument('campaign_id', type=int, required=True, location='json')
        self.reqparser.add_argument('valid_to', type=str, required=True, location='json')
        self.reqparser.add_argument('valid_from', type=str, required=True, location='json')
        self.reqparser.add_argument('discount_rule', type=int, required=True, location='json')
        self.reqparser.add_argument('discount_type', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')
        self.reqparser.add_argument('discount_usd', type=str, required=True, location='json')
        self.reqparser.add_argument('discount_idr', type=str, required=True, location='json')
        self.reqparser.add_argument('platforms', type=list, required=True, location='json')
        self.reqparser.add_argument('paymentgateways', type=list, required=True, location='json')
        self.reqparser.add_argument('discount_status', type=int, required=True, location='json')
        self.reqparser.add_argument('discount_schedule_type', type=int, required=True, location='json')

        self.reqparser.add_argument('discount_point', type=int, location='json')
        self.reqparser.add_argument('min_usd_order_price', type=str, location='json')
        self.reqparser.add_argument('max_usd_order_price', type=str, location='json')
        self.reqparser.add_argument('min_idr_order_price', type=str, location='json')
        self.reqparser.add_argument('max_idr_order_price', type=str, location='json')
        self.reqparser.add_argument('description', type=unicode, location='json')

        self.reqparser.add_argument('vendor_participation', type=int, required=True, location='json')
        self.reqparser.add_argument('partner_participation', type=int, required=True, location='json')
        self.reqparser.add_argument('sales_recognition', type=int, required=True, location='json')

        self.reqparser.add_argument('offers', type=list, location='json')
        self.reqparser.add_argument('predefined_group', type=int, location='json')

        super(DiscountListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_discount')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = CustomGet(param, 'discounts', 'GET').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="discounts", method="GET",
                                           error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_global_discount')
    def post(self):

        args = self.reqparser.parse_args()

        # check if discount already exist with same name
        po = Discount.query.filter_by(name=args['name']).first()
        if po:
            return conflict_message(modul="discount", method="POST",
                                    conflict=args['name'], req=args)
        else:
            scene = DiscountConstruct(args, "POST", '').construct()
            return scene


class DiscountApi(Resource):
    ''' GET/PUT/DELETE Discount '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('name', type=unicode, required=True, location='json')
        self.reqparser.add_argument('tag_name', type=str, required=True, location='json')
        self.reqparser.add_argument('campaign_id', type=int, required=True, location='json')
        self.reqparser.add_argument('valid_to', type=str, required=True, location='json')
        self.reqparser.add_argument('valid_from', type=str, required=True, location='json')
        self.reqparser.add_argument('discount_rule', type=int, required=True, location='json')
        self.reqparser.add_argument('discount_type', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')
        self.reqparser.add_argument('discount_usd', type=str, required=True, location='json')
        self.reqparser.add_argument('discount_idr', type=str, required=True, location='json')
        self.reqparser.add_argument('platforms', type=list, required=True, location='json')
        self.reqparser.add_argument('paymentgateways', type=list, required=True, location='json')
        self.reqparser.add_argument('discount_status', type=int, required=True, location='json')
        self.reqparser.add_argument('discount_schedule_type', type=int, required=True, location='json')

        self.reqparser.add_argument('discount_point', type=int, location='json')
        self.reqparser.add_argument('min_usd_order_price', type=str, location='json')
        self.reqparser.add_argument('max_usd_order_price', type=str, location='json')
        self.reqparser.add_argument('min_idr_order_price', type=str, location='json')
        self.reqparser.add_argument('max_idr_order_price', type=str, location='json')
        self.reqparser.add_argument('description', type=unicode, location='json')

        self.reqparser.add_argument('vendor_participation', type=int, required=True, location='json')
        self.reqparser.add_argument('partner_participation', type=int, required=True, location='json')
        self.reqparser.add_argument('sales_recognition', type=int, required=True, location='json')

        self.reqparser.add_argument('offers', type=list, location='json')
        self.reqparser.add_argument('predefined_group', type=int, location='json')

        super(DiscountApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_discount')
    def get(self, discount_id=None):
        m = Discount.query.filter_by(id=discount_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all')
    def delete(self, discount_id=None):
        f = Discount.query.filter_by(id=discount_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')

        else:
            return Response(status=httplib.NOT_FOUND)


class DiscountCodeSchema(ma.Schema):
    discount_id = fields.Int(required=True, default=1)
    discount_prefix = fields.Str(required=True, default='')
    discount_type = fields.Int(required=True)
    max_uses = fields.Int(required=True, default=1)
    total_coupon = fields.Int(required=True)


class DiscountCodeListApi(Resource):
    ''' Shows a list of DiscountCode and do POST / GET '''

    @token_required('can_read_write_global_all, can_read_global_discount_code')
    def get(self):
        args = request.args.to_dict()
        limit, offset, q = args.get('limit', 20), args.get('offset', 0), args.get('q', None)
        data_discount = DiscountCode.query
        if q:
            q = '%{}%'.format(q)
            data_discount = data_discount.join(Discount).filter(
                or_(DiscountCode.code.ilike(q),
                    Discount.name.ilike(q)))
        total_count = data_discount.count()
        data_discount = data_discount.order_by(desc(DiscountCode.id)).limit(limit).offset(offset).all()

        rebuild = {
            "discount_codes": [data.values() for data in data_discount],
            'metadata': {
                'resultset': {
                    'offset': offset,
                    'limit': limit,
                    'count': total_count
                },
            }
        }
        data = jsonify(rebuild)
        data.status_code = httplib.OK
        return data

    @token_required('can_read_write_global_all, can_read_write_global_discount_code')
    def post(self):
        schema_data = schema_load(DiscountCodeSchema())
        data_coupon = DiscountCodeConstruct(schema_data).create()
        resp = jsonify({'code': data_coupon})
        resp.status_code = httplib.OK
        return resp


class DiscountCodeApi(Resource):
    ''' GET/PUT/DELETE DiscountCode '''

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('discount_id', type=int, required=True, location='json')
        self.reqparser.add_argument('is_active', type=bool, required=True, location='json')
        self.reqparser.add_argument('discount_type', type=int, required=True, location='json')
        self.reqparser.add_argument('discount_code', type=str, required=True, location='json')
        self.reqparser.add_argument('max_uses', type=int, required=True, location='json')

        super(DiscountCodeApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_global_discount_code')
    def get(self, discount_id=None):
        m = DiscountCode.query.filter_by(id=discount_id).first()
        if m:
            rv = json.dumps(m.values())
            return Response(rv, status=httplib.OK, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_global_discount_code')
    def put(self, discount_id=None):

        m = DiscountCode.query.filter_by(id=discount_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)

        args = self.reqparser.parse_args()
        kwargs = {}
        for item in args:
            if args[item] is None:
                pass
            else:
                kwargs[item] = args[item]

        if args['discount_code'] != m.code:
            po = DiscountCode.query.filter_by(
                discount_id=args['discount_id'],
                code=args['discount_code'].lower()).first()
            if po:
                return conflict_message(modul="discount code", method="PUT",
                                        conflict=args['discount_code'], req=args)

        if args['discount_id']:
            discount = Discount.query.filter_by(id=args['discount_id']).first()
            if not discount:
                return bad_request_message(modul="discount code", method="PUT",
                                           param="discount_id", not_found=args['discount_id'], req=args)

            if discount.discount_type != 5:
                return err_response(status=httplib.BAD_REQUEST,
                                    error_code=httplib.BAD_REQUEST,
                                    developer_message="Invalid Discount Type, Not Discount Code type",
                                    user_message="Invalid Discount")

            kwargs['discount_id'] = discount.id

        kwargs['code'] = kwargs['discount_code'].lower()
        del kwargs['discount_code']

        try:
            f = DiscountCode.query.get(discount_id)
            for k, v in kwargs.iteritems():
                setattr(f, k, v)

            sv = f.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="discount code", method="PUT",
                                               error=sv.get('message', ''), req=str(args))
            else:
                rv = m.values()
                return Response(json.dumps(rv), status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="discount code", method="PUT",
                                           error=e, req=str(args))

    @token_required('can_read_write_global_all')
    def delete(self, discount_id=None):
        f = DiscountCode.query.filter_by(id=discount_id).first()
        if f:
            # set is_active --> False
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)


class MigrateDiscount(Resource):
    """
        This API for operator, adding new offer id to discounts
    """

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        # required for database whos can't null
        self.reqparser.add_argument('discount_id', type=int, required=True, location='json')
        self.reqparser.add_argument('offer_id', type=list, required=True, location='json')

        super(MigrateDiscount, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def post(self):
        data_offer = []
        args = self.reqparser.parse_args()

        def platforms_forms(platform_list=None, discounts=None, offer=None):
            # patch data platforms
            for iplat in platform:

                if iplat in [1, 2, 3]:
                    offer_platforms = OfferPlatform.query.filter_by(
                        offer_id=offer.id, platform_id=iplat).first()

                    if offer_platforms.discount_id:
                        discount_ids = [i for i in offer_platforms.discount_id]
                        if str(discounts.id) in discount_ids:
                            pass
                        else:
                            discount_ids.append(discounts.id)
                            offer_platforms.discount_id = discount_ids
                    else:
                        offer_platforms.discount_id = [discounts.id]

                    price_usd = offer_platforms.price_usd
                    price_idr = offer_platforms.price_idr

                    dsc_usd = discounts.calculate_offer_discount(price_usd, 'USD')
                    dsc_idr = discounts.calculate_offer_discount(price_idr, 'IDR')

                    offer_platforms.discount_tag = discounts.tag_name
                    offer_platforms.discount_name = discounts.name

                    # Non consumable type 2 subscriptions,
                    # discount_tier_id == tier_id
                    if offer.offer_type_id == 2:
                        offer_platforms.discount_tier_id = offer_platforms.tier_id

                        # update price
                        price = str(dsc_usd).split('.')
                        final = float('%s.%s' % (price[0], 99))

                        offer_platforms.discount_tier_price = final
                        offer_platforms.discount_price_usd = final
                        offer_platforms.discount_price_idr = dsc_idr

                        offer_platforms.tier_code = offer_platforms.get_tier_code()
                        offer_platforms.discount_tier_code = offer_platforms.get_tier_code()

                    # Consumable
                    if offer.offer_type_id in [1, 3]:
                        price = str(dsc_usd).split('.')
                        final = float('%s.%s' % (price[0], 99))

                        offer_platforms.tier_code = offer_platforms.get_tier_code()

                        if iplat == 1:
                            data = TierIos.query.filter_by(tier_price=final, tier_type=2).first()
                        if iplat == 2:
                            data = TierAndroid.query.filter_by(tier_price=final).first()
                        if iplat == 3:
                            data = TierWp.query.filter_by(tier_price=final).first()

                        if data:
                            offer_platforms.discount_price_usd = final
                            offer_platforms.discount_price_idr = dsc_idr

                            offer_platforms.discount_tier_id = data.id
                            offer_platforms.discount_tier_price = data.tier_price
                            offer_platforms.discount_tier_code = data.tier_code

                    ## point
                    payment_gateway = [i.id for i in discounts.paymentgateways]
                    if POINT in payment_gateway:
                        price_points = offer.price_point
                        dsc_point = discounts.calculate_offer_discount(price_points, 'PTS')
                        offer_platforms.discount_price_point = dsc_point

                    offer_platforms.save()

        try:
            discounts = Discount.query.filter_by(id=args['discount_id']).first()
            if not discounts:
                return bad_request_message(modul="hack discount", method="POST",
                                           param="discount_id", not_found=args['discount_id'], req=args)

            offer_list = args['offer_id']
            for ioffer in offer_list:
                rv = {}
                offer = Offer.query.filter_by(id=ioffer).first()

                if discounts.discount_type == 1:
                    payment_gateway = [i.id for i in discounts.paymentgateways]
                    platform = [i.id for i in discounts.platforms]

                    if POINT in payment_gateway:
                        price_points = offer.price_point
                        dsc_point = discounts.calculate_offer_discount(price_points, 'PTS')
                        offer.discount_price_point = dsc_point
                        rv['price_point'] = '%s --> %s' % (price_points, dsc_point)

                    price_usd = offer.price_usd
                    price_idr = offer.price_idr

                    dsc_usd = discounts.calculate_offer_discount(price_usd, 'USD')
                    dsc_idr = discounts.calculate_offer_discount(price_idr, 'IDR')

                    if offer.discount_id:
                        discount_ids = [i for i in offer.discount_id]

                        if str(discounts.id) in discount_ids:
                            pass
                        else:
                            discount_ids.append(discounts.id)
                            offer.discount_id = discount_ids
                    else:
                        offer.discount_id = [discounts.id]

                    offer.discount_tag = discounts.tag_name
                    offer.discount_name = discounts.name
                    offer.discount_price_usd = dsc_usd
                    offer.discount_price_idr = dsc_idr
                    offer.is_discount = True
                    offer.save()

                    discounts.offers.append(offer)
                    discounts.save()

                    platforms_forms(platform, discounts, offer)

                    rv['status'] = 'OK'
                    rv['offer_id'] = offer.id
                    rv['price_usd'] = '%s --> %s' % (price_usd, dsc_usd)
                    rv['price_idr'] = '%s --> %s' % (price_idr, dsc_idr)
                    data_offer.append(rv)

                else:
                    if offer.discount_id:
                        discount_ids = [i for i in offer.discount_id]
                        discount_ids.append(discounts.id)
                        offer.discount_id = discount_ids
                    else:
                        offer.discount_id = [discounts.id]

                    rv['status'] = 'OK'
                    rv['offer_id'] = offer.id
                    rv['discounts_name'] = discounts.name
                    offer.save()

                    discounts.offers.append(offer)
                    discounts.save()

                    data_offer.append(rv)

            return Response(json.dumps(data_offer), status=httplib.OK, mimetype='application/json')

        except Exception, e:
            return internal_server_message(modul="hack discount ", method="POST",
                                           error=e, req=str(args))


class DiscountCodeValid(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_write_public')
    def get(self, discount_code=None):

        m = DiscountCode.query.filter_by(code=discount_code.lower()).first()
        print m
        if m:
            disounts = m.get_discount()

            if disounts.discount_valid():
                discounts_data = []
                rv = {}
                rv['id'] = disounts.id
                rv['name'] = disounts.name
                rv['valid_from'] = disounts.valid_from.isoformat()
                rv['valid_to'] = disounts.valid_to.isoformat()
                rv['campaign_id'] = disounts.campaign_id

                rv['discount_rule'] = disounts.discount_rule
                rv['discount_usd'] = '%.2f' % disounts.discount_usd
                rv['discount_idr'] = '%.2f' % disounts.discount_idr

                rv['platforms'] = disounts.platform_id()
                rv['payment_gateways'] = disounts.paymentgateway_id()

                if disounts.min_usd_order_price:
                    rv['min_usd_order_price'] = '%.2f' % disounts.min_usd_order_price
                else:
                    rv['min_usd_order_price'] = 0

                if disounts.max_usd_order_price:
                    rv['max_usd_order_price'] = '%.2f' % disounts.max_usd_order_price
                else:
                    rv['max_usd_order_price'] = 0

                if disounts.min_idr_order_price:
                    rv['min_idr_order_price'] = '%.2f' % disounts.min_idr_order_price
                else:
                    rv['min_idr_order_price'] = 0

                if disounts.max_idr_order_price:
                    rv['max_idr_order_price'] = '%.2f' % disounts.max_idr_order_price
                else:
                    rv['max_idr_order_price'] = 0

                discounts_data.append(rv)

                rd_data = {}
                rd_data['discount_code'] = m.code
                rd_data['max_uses'] = m.max_uses
                rd_data['current_uses'] = m.current_uses

                rd_data['discounts'] = discounts_data
                return Response(json.dumps(rd_data), status=httplib.OK, mimetype='application/json')

            else:
                return err_response(status=httplib.BAD_REQUEST,
                                    error_code=400101,
                                    developer_message="discount code %s, promo expired",
                                    user_message="discount code %s expired" % discount_code)

        else:
            # if coupon not exist, recheck log.
            log_disc = DiscountCodeLog.query.filter_by(code=discount_code.lower()).first()
            if log_disc:
                return err_response(status=httplib.BAD_REQUEST,
                                    error_code=400101,
                                    developer_message="discount code %s, max-used: %s, current-used : %s" % (
                                        discount_code, log_disc.max_uses, log_disc.current_uses),
                                    user_message="discount code %s quota is used up" % discount_code)
            else:
                return Response(status=httplib.NO_CONTENT)


class DiscountCodeItem(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_write_public')
    def get(self, discount_code=None):

        m = DiscountCode.query.filter_by(code=discount_code.lower()).first()
        param = request.args.to_dict()

        if m:
            disounts = m.get_discount()
            if disounts.discount_valid():
                limit = param.get('limit', 20)
                offset = param.get('offset', 0)
                fields = param.get('fields', '')
                data = DiscountCodeItems(disounts, limit, offset, fields).filter_items()
                return data

            else:
                return err_response(status=httplib.BAD_REQUEST,
                                    error_code=400101,
                                    developer_message="discount code %s, promo expired",
                                    user_message="discount code %s expired" % discount_code)
        else:
            # if coupon not exist, recheck log.
            log_disc = DiscountCodeLog.query.filter_by(code=discount_code.lower()).first()
            if log_disc:
                return err_response(status=httplib.BAD_REQUEST,
                                    error_code=400101,
                                    developer_message="discount code %s, max-used: %s, current-used : %s" % (
                                        discount_code, log_disc.max_uses, log_disc.current_uses),
                                    user_message="discount code %s quota is used up" % discount_code)
            else:
                return Response(status=httplib.NO_CONTENT)
