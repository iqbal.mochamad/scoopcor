from flask_appsfoundry.serializers import SerializerBase, fields


class RevenueFormulaSerializer(SerializerBase):
    id = fields.Integer
    platform_id = fields.Integer
    fixed_amount = fields.Integer
    percentage_amount = fields.Float


class RevenueSharesSerializerBase(SerializerBase):
    id = fields.Integer
    name = fields.String
    description = fields.String
    created = fields.DateTime('iso8601')
    modified = fields.DateTime('iso8601')
    is_active = fields.Boolean()
    default_fixed_amount = fields.Integer
    default_percentage_amount = fields.Float
    formulas = fields.List(fields.Nested(RevenueFormulaSerializer()))


class PaymentGatewayFormulaSerializer(SerializerBase):
    id = fields.Integer
    card_type = fields.String
    paymentgateway_id = fields.Integer
    max_amount = fields.Float
    min_amount = fields.Float
    fixed_amount = fields.Float
    percentage_amount = fields.Float


class PaymentGatewayCostSerializerBase(SerializerBase):
    id = fields.Integer
    name = fields.String
    description = fields.String
    created = fields.DateTime('iso8601')
    modified = fields.DateTime('iso8601')
    is_active = fields.Boolean()

    default_fixed_amount = fields.Float
    default_percentage_amount = fields.Float

    formulas = fields.List(fields.Nested(PaymentGatewayFormulaSerializer()))


class VendorPaymentGatewayCostSerializerBase(SerializerBase):
    id = fields.Integer
    vendor_id = fields.Integer
    paymentgatewaycost_id = fields.Integer
    valid_from = fields.DateTime('iso8601')
    valid_to = fields.DateTime('iso8601')
    created = fields.DateTime('iso8601')
    modified = fields.DateTime('iso8601')
    is_active = fields.Boolean()
