from werkzeug.exceptions import BadRequest
from flask_appsfoundry import parsers
from flask_appsfoundry.parsers import (
    InputField,ParserBase, SqlAlchemyFilterParser)
from flask_appsfoundry.parsers.filterinputs import (
    IntegerFilter, StringFilter, BooleanFilter, DateTimeFilter)
from flask_restful import inputs
from flask_appsfoundry.parsers.converters import naive_datetime_from_iso8601

from decimal import Decimal


def positive_only(input):
    if Decimal(input) > 0:
        return Decimal(input)
    else:
        raise BadRequest(description="{} FOR NOT ALLOWED".format(input))

from . import models
def fields_column(input):
    return input.split(',')


def formula_element(input):
    """ Formats an input json document with the expected input data for a
    revenue share formula.  This is solely to be used with the revenue-shares parser.

    :param `dict` input: An input dictionary (assumed to by from JSON)
    :return: A formatted *copy* of the provided input dictionary.
    :rtype: `dict`
    """
    result = {
        'platform_id': int(input['platform_id']),
        'fixed_amount': Decimal(input['fixed_amount']),
        'percentage_amount': Decimal(input['percentage_amount'])
    }
    return result

def payment_element(input):
    """ Formats an input json document with the expected input data for a
    payment gateway formula.  This is solely to be used with the paymentgateway-costs parser.

    :param `dict` input: An input dictionary (assumed to by from JSON)
    :return: A formatted *copy* of the provided input dictionary.
    :rtype: `dict`
    """
    result = {
        'card_type' : input['card_type'],
        'paymentgateway_id': int(input['paymentgateway_id']),
        'max_amount': positive_only(input.get('max_amount')),
        'min_amount': positive_only(input.get('min_amount')),
        'fixed_amount': positive_only(input.get('fixed_amount')),
        'percentage_amount': positive_only(input.get('percentage_amount'))
    }
    return result



class RevenueSharesArgsParserList(parsers.ParserBase):
    name = InputField()
    description = InputField()
    is_active = InputField(type=inputs.boolean, default=True)
    default_fixed_amount = InputField(type=Decimal)
    default_percentage_amount = InputField(type=Decimal)

    formulas = InputField(type=formula_element, action='append', default=[])

    offset = InputField(type=int, default=0)
    limit = InputField(type=int, default=20)
    fields = InputField(type=fields_column,
                        default=['all'])
    q = InputField()
    order = InputField()

class RevenueSharesArgsParser(ParserBase):
    name = InputField(required=True)
    description = InputField()
    is_active = InputField(type=inputs.boolean)
    default_fixed_amount = InputField(type=Decimal, required=True)
    default_percentage_amount = InputField(type=Decimal, required=True)
    formulas = InputField(type=formula_element, action='append', default=[])

class PaymentGatewayCostsArgsParser(ParserBase):
    name = InputField(required=True)
    description = InputField()
    is_active = InputField(type=inputs.boolean, default=True)
    formulas = InputField(type=payment_element, action='append', default=[])
    default_fixed_amount = InputField(type=positive_only)
    default_percentage_amount = InputField(type=positive_only)

class PaymentGatewayCostsListArgsParser(SqlAlchemyFilterParser):
    __model__ = models.PaymentGatewayCost
    id = IntegerFilter()
    name = StringFilter()
    description = StringFilter()

    payment_gateway_id = IntegerFilter()
    max_amount = IntegerFilter()
    min_amount = IntegerFilter()
    fixed_amount = IntegerFilter()
    percentage_amount = IntegerFilter()

    is_active = BooleanFilter(default=True)

class VendorPaymentGatewayCostsArgsParser(ParserBase):
    vendor_id = InputField(type=int, required=True)
    paymentgatewaycost_id = InputField(type=int, required=True)
    valid_from = InputField(type=naive_datetime_from_iso8601, required=True)
    valid_to = InputField(type=naive_datetime_from_iso8601, required=True)

class VendorPaymentGatewayCostsListArgsParser(SqlAlchemyFilterParser):
    __model__ = models.VendorPaymentGatewayCost
    id = IntegerFilter()
    vendor_id = IntegerFilter()
    paymentgatewaycost_id = IntegerFilter()
    valid_from = DateTimeFilter()
    valid_to = DateTimeFilter()

    is_active = BooleanFilter(default=True)
