from flask import Blueprint

from flask_appsfoundry import Api


from .api import RevenueSharesListApi, RevenueSharesDetailApi
from .paymentgateway_costs import PaymentGatewayCostListApi, PaymentGatewayCostsDetailApi
from .vendor_paymentgatewaycosts import VendorPaymentGatewayCostsDetailApi, VendorPaymentGatewayCostsListApi



revenue_share_blueprint = Blueprint('revenue_share', __name__, url_prefix='/v1/revenue_share')

revenue_share_api = Api(revenue_share_blueprint)
revenue_share_api.add_resource(RevenueSharesListApi, '')
revenue_share_api.add_resource(RevenueSharesDetailApi, '/<int:db_id>', endpoint='revenue_share')

paymentgateway_cost_blueprint = Blueprint('paymentgatewaycosts', __name__, url_prefix='/v1/paymentgatewaycosts')

paymentgateway_cost_api = Api(paymentgateway_cost_blueprint)
paymentgateway_cost_api.add_resource(PaymentGatewayCostListApi, '')
paymentgateway_cost_api.add_resource(PaymentGatewayCostsDetailApi, '/<int:db_id>', endpoint='paymentgatewaycosts')

publisher_paymentgateway_cost_blueprint = Blueprint('publisher-paymentgatewaycosts', __name__, url_prefix='/v1/vendor_payment_gateway_costs')
publisher_paymentgateway_cost_api = Api(publisher_paymentgateway_cost_blueprint)
publisher_paymentgateway_cost_api.add_resource(VendorPaymentGatewayCostsListApi, '')
publisher_paymentgateway_cost_api.add_resource(VendorPaymentGatewayCostsDetailApi, '/<int:db_id>', endpoint='vendor_payment_gateway_costs')
