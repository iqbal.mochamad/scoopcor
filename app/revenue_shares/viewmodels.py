from .helpers import build_query

class RevenueSharesListViewModel(object):
    def __init__(self, query, args):
        query = build_query(query, args)
        self.revenue = query['query']

        self.metadata = {'resultset':
                             {'count': query['query_count'],
                              'offset': args['offset'],
                              'limit': args['limit']}}
