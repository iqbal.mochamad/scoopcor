"""
Revenue Shares API
=================

Endpoints for posting and retrieving analytics data from the front end apps.
"""
from __future__ import absolute_import
from httplib import CREATED, OK, NOT_FOUND, NO_CONTENT

from flask import Response
from flask_appsfoundry import controllers, marshal
from flask_appsfoundry.controllers import ErrorHandlingApiMixin
from flask_restful import Resource

from app.auth.decorators import token_required
from . import models, parsers, serializers
from .models import RevenueShares
from .viewmodels import RevenueSharesListViewModel


class RevenueSharesListApi(controllers.ListCreateApiResource):
    serialized_list_name = 'revenue_shares'
    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext',
                           'can_read_global_revenue')

    @token_required('can_read_write_global_all, can_read_write_global_revenue')
    def post(self):
        session = models.RevenueShares.query.session

        parser = parsers.RevenueSharesArgsParser()
        args = parser.parse_args()

        try:

            revenue_share = models.RevenueShares(
                **{k: args[k] if k != 'formulas'
                        else [models.RevenueFormulas(**formula) for formula in args[k]]
                   for k in args}
            )

            session.add(revenue_share)
            session.commit()

            return marshal(revenue_share, serializers.RevenueSharesSerializerBase()), CREATED

        except Exception:
            session.rollback()
            raise

    def get(self):
        session = models.RevenueShares.query.session
        parser = parsers.RevenueSharesArgsParserList()
        args = parser.parse_args()

        try:
            view_model = RevenueSharesListViewModel(models.RevenueShares.query, args)

            serializer = serializers.RevenueSharesSerializerBase()

            if 'all' not in args['fields']:
                # create a temporary list of attribute names
                all_fields = [key for key, value in serializer.items()]
                for attr_name in all_fields:
                    if attr_name not in args['fields'] and attr_name != 'formulas':
                        delattr(serializer, attr_name)

            view_model = {'revenue_shares': marshal(view_model.revenue, serializer),
                          'metadata': view_model.metadata}

            # 4.) Serialize and return response
            return view_model, OK

        except Exception:
            raise
        finally:
            # 5.) CLEAN UP
            session.rollback()



class RevenueSharesDetailApi(ErrorHandlingApiMixin, Resource):

    @token_required('can_read_write_global_all, can_read_write_public,'
        'can_read_write_public_ext, can_read_global_revenue')
    def get(self, db_id=None):
        session = models.RevenueShares.query.session

        try:
            revenueshares = RevenueShares.query.filter(RevenueShares.id == db_id).first()
            serializer = serializers.RevenueSharesSerializerBase()

            if revenueshares:
                view_model = marshal(revenueshares, serializer)
                return view_model, 200

            else:
                return Response(status=NOT_FOUND)

        except Exception:
            raise

        finally:
            # 5.) CLEAN UP
            session.rollback()

    @token_required('can_read_write_global_all, can_read_write_global_revenue')
    def put(self, db_id=None):
        pass

        revenueshares = RevenueShares.query.filter(RevenueShares.id == db_id).first()

        if not revenueshares:
            return Response(status=NOT_FOUND)

        session = RevenueShares.query.session

        # 1.) grab our input
        parser = parsers.RevenueSharesArgsParser()
        args = parser.parse_args()

        try:
            for old in revenueshares.formulas:
                revenueshares.formulas.remove(old)

            revenueshares.name = args['name']
            revenueshares.description = args['description']
            revenueshares.is_active = args['is_active']
            revenueshares.default_percentage_amount = args['default_percentage_amount']
            revenueshares.default_fixed_amount = args['default_fixed_amount']

            for formula in args['formulas']:
                revenue_formula = models.RevenueFormulas(**formula)
                session.add(revenue_formula)
                revenueshares.formulas.append(revenue_formula)

            session.commit()
            revenueshares = RevenueShares.query.filter(RevenueShares.id == db_id).first()
            return marshal(revenueshares, serializers.RevenueSharesSerializerBase()), OK

        except Exception as e:
            # potentially log some stuff to say that shit blew up.
            session.rollback()
            raise e

    @token_required('can_read_write_global_all')
    def delete(self, db_id=None):
        session = models.RevenueShares.query.session
        revenueshares = RevenueShares.query.filter(RevenueShares.id == db_id).first()

        if revenueshares:
            try:
                revenueshares.is_active = False
                session.commit()

            except:
                raise

            finally:
                session.rollback()

            return None, NO_CONTENT

        else:
            return None, NO_CONTENT
