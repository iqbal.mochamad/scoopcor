from flask_appsfoundry.models import TimestampMixin, SoftDeletableMixin

from app.revenue_shares.choices import PAYMENT_CARD_TYPES
from app import db
from sqlalchemy.dialects import postgresql


revenueshares_formulas = db.Table(
    'core_revenueshares_formulas',
    db.Column('revenueshare_id', db.Integer, db.ForeignKey('core_revenueshares.id')),
    db.Column('revenueformula_id', db.Integer, db.ForeignKey('core_revenuesformulas.id')),
    db.PrimaryKeyConstraint('revenueshare_id', 'revenueformula_id')
)

paymentgatewaycosts_formulas = db.Table(
    'core_paymentgatewaycosts_formulas',
    db.Column('paymentgatewaycost_id', db.Integer, db.ForeignKey('core_paymentgatewaycosts.id')),
    db.Column('paymentgatewayformula_id', db.Integer, db.ForeignKey('core_paymentgatewayformulas.id')),
    db.PrimaryKeyConstraint('paymentgatewaycost_id', 'paymentgatewayformula_id')
)


class RevenueShares(TimestampMixin, db.Model):

    __tablename__ = 'core_revenueshares'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250))
    description = db.Column(db.Text)
    is_active = db.Column(db.Boolean(), default=False)
    default_fixed_amount = db.Column(db.DECIMAL(10, 2))
    default_percentage_amount = db.Column(db.DECIMAL(10, 2))

    formulas = db.relationship(
        'RevenueFormulas',
        secondary=revenueshares_formulas,
        backref=db.backref('core_revenueshares', lazy='dynamic'),
        lazy='dynamic'
    )

class RevenueFormulas(TimestampMixin, db.Model):

    __tablename__ = 'core_revenuesformulas'

    id = db.Column(db.Integer, primary_key=True)
    platform_id = db.Column(db.Integer)
    fixed_amount = db.Column(db.DECIMAL(10, 2))
    percentage_amount = db.Column(db.DECIMAL(10, 2))



class PaymentGatewayCost(TimestampMixin, SoftDeletableMixin, db.Model):
    name = db.Column(db.CHAR(250))
    description = db.Column(db.Text)
    default_fixed_amount = db.Column(db.DECIMAL(10, 4), default=0)
    default_percentage_amount = db.Column(db.DECIMAL(10, 4), default=0)
    formulas = db.relationship(
        'PaymentGatewayFormula',
        secondary=paymentgatewaycosts_formulas,
        backref=db.backref('payment_gateway_costs', lazy='dynamic'),
        lazy='dynamic'
    )


class PaymentGatewayFormula(TimestampMixin, db.Model):
    paymentgateway_id = db.Column(db.Integer, db.ForeignKey('core_paymentgateways.id'))
    card_type = db.Column(postgresql.ENUM(*PAYMENT_CARD_TYPES.values(),
                                         name='payment_card_types'),
                         nullable=True)
    max_amount = db.Column(db.DECIMAL(10, 4), default=0)
    min_amount = db.Column(db.DECIMAL(10, 4), default=0)
    fixed_amount = db.Column(db.DECIMAL(10, 4), default=0)
    percentage_amount = db.Column(db.DECIMAL(10, 4), default=0)


class VendorPaymentGatewayCost(TimestampMixin,SoftDeletableMixin, db.Model):
    __tablename__ = 'core_vendors_paymentgatewaycosts'
    vendor_id = db.Column(db.Integer, db.ForeignKey('core_vendors.id'), nullable=False)
    #vendor = db.relationship('Vendor', backref=db.backref('publisher_payment_gateway', uselist=False))
    paymentgatewaycost_id = db.Column(db.Integer, db.ForeignKey('core_paymentgatewaycosts.id'), nullable=False)
    #paymentgatewaycost = db.relationship('PaymentGatewayCost', backref=db.backref('publisher_payment_gateway_cost', uselist=False))
    valid_from = db.Column(db.DateTime, nullable=False)
    valid_to = db.Column(db.DateTime, nullable=False)









