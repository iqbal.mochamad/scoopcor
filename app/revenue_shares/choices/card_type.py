CREDIT_CARD = u'credit'
DEBIT_CARD = u'debit'

PAYMENT_CARD_TYPES = {
    CREDIT_CARD: u'credit',
    DEBIT_CARD: u'debit'
}
