from __future__ import absolute_import
from httplib import OK, CREATED

from flask_appsfoundry import controllers, marshal
from sqlalchemy import desc
from werkzeug.exceptions import NotFound

from app.auth.decorators import token_required
from . import models, parsers, serializers


class PaymentGatewayCostListApi(controllers.ListCreateApiResource):

    query_set = models.PaymentGatewayCost.query

    default_ordering = (desc(models.PaymentGatewayCost.id),
                        )
    create_request_parser = parsers.PaymentGatewayCostsArgsParser

    list_request_parser = parsers.PaymentGatewayCostsListArgsParser

    response_serializer = serializers.PaymentGatewayCostSerializerBase

    serialized_list_name = u'paymentgatewaycosts'

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext',
                           'can_read_global_paymentgatewaycosts',
                           )

    post_security_tokens = ('can_read_write_global_all',
                            'can_read_write_global_paymentgatewaycosts',
                            )

    @token_required('can_read_write_global_all, can_read_write_global_paymentgatewaycosts')
    def post(self):
        session = models.PaymentGatewayCost.query.session

        parser = parsers.PaymentGatewayCostsArgsParser()
        args = parser.parse_args()

        try:
            paymentgatewaycosts = models.PaymentGatewayCost(
                **{k: args[k] if k != 'formulas'
                        else [models.PaymentGatewayFormula(**formula) for formula in args[k]]
                   for k in args}
            )

            session.add(paymentgatewaycosts)
            session.commit()

            return marshal(paymentgatewaycosts, serializers.PaymentGatewayCostSerializerBase()), CREATED

        except Exception:
            session.rollback()
            raise


class PaymentGatewayCostsDetailApi(controllers.DetailApiResource):

    query_set = models.PaymentGatewayCost.query

    update_request_parser = parsers.PaymentGatewayCostsArgsParser

    response_serializer = serializers.PaymentGatewayCostSerializerBase

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext',
                           'can_read_write_global_paymentgatewaycosts',
                           )

    put_security_tokens = ('can_read_write_global_all',
                           'can_read_write_global_paymentgatewaycosts',
                           )

    delete_security_tokens = ('can_read_write_global_all',
                              )

    @token_required('can_read_write_global_all, can_read_write_global_revenue')
    def put(self, db_id):

        paymentgatewaycosts = models.PaymentGatewayCost.query.get(db_id)

        if not paymentgatewaycosts:
            raise NotFound

        session = models.PaymentGatewayCost.query.session

        # 1.) grab our input
        parser = parsers.PaymentGatewayCostsArgsParser()
        args = parser.parse_args()

        try:
            for old in paymentgatewaycosts.formulas:
                paymentgatewaycosts.formulas.remove(old)

            paymentgatewaycosts.name = args['name']
            paymentgatewaycosts.description = args['description']
            paymentgatewaycosts.is_active = args['is_active']
            paymentgatewaycosts.default_fixed_amount = args['default_fixed_amount']
            paymentgatewaycosts.default_percentage_amount = args['default_percentage_amount']


            for formula in args['formulas']:
                gateway_formula = models.PaymentGatewayFormula(**formula)
                session.add(gateway_formula)
                paymentgatewaycosts.formulas.append(gateway_formula)

            session.commit()
            return marshal(paymentgatewaycosts, serializers.PaymentGatewayCostSerializerBase()), OK

        except Exception:
            # potentially log some stuff to say that shit blew up.
            session.rollback()
            raise
