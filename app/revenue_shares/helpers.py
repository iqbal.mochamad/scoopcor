from sqlalchemy.sql import or_


from flask_appsfoundry.parsers import specialized

from models import RevenueShares

def build_query(base_query, args):

        query = base_query
        query = query.filter_by(is_active=args['is_active'])

        if args['q']:
            query = query.filter(RevenueShares.name.ilike('%' + args['q'] + '%'))

        ordering = specialized.SqlAlchemyOrderingParser().parse_args().get('order')
        for order_opt in ordering:
            query = query.order_by(order_opt)

        query_count = query.count()
        query = query.limit(args['limit']).offset(args['offset']).all()
        query = {'query': query, 'query_count': query_count}

        return query
