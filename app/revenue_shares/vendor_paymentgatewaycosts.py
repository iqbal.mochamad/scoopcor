from __future__ import absolute_import

from flask_appsfoundry import controllers
from sqlalchemy import desc

from . import models, parsers, serializers


class VendorPaymentGatewayCostsListApi(controllers.ListCreateApiResource):

    query_set = models.VendorPaymentGatewayCost.query

    default_ordering = (desc(models.VendorPaymentGatewayCost.id),
                        )
    create_request_parser = parsers.VendorPaymentGatewayCostsArgsParser

    list_request_parser = parsers.VendorPaymentGatewayCostsListArgsParser

    response_serializer = serializers.VendorPaymentGatewayCostSerializerBase

    serialized_list_name = u'vendors_paymentgatewaycosts'

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext'
                           )

    post_security_tokens = ('can_read_write_global_all',
                            'can_read_write_global_vendor_paymentgatewaycosts',
                            )


class VendorPaymentGatewayCostsDetailApi(controllers.DetailApiResource):

    query_set = models.VendorPaymentGatewayCost.query

    update_request_parser = parsers.VendorPaymentGatewayCostsArgsParser

    response_serializer = serializers.VendorPaymentGatewayCostSerializerBase

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext',
                           'can_read_write_global_paymentgatewaycosts',
                           )

    put_security_tokens = ('can_read_write_global_all',
                           'can_read_write_global_paymentgatewaycosts',
                           )

    delete_security_tokens = ('can_read_write_global_all',
                              )
