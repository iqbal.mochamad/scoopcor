from enum import IntEnum

MIME_TYPE_V3_JSON = 'application/vnd.scoop.v3+json'

CLIENT_200101 = 200101

CLIENT_400101 = 400101
CLIENT_400102 = 400102
CLIENT_400103 = 400103
CLIENT_400104 = 400104
CLIENT_400105 = 400105
CLIENT_400106 = 400106
CLIENT_400107 = 400107
CLIENT_400108 = 400108
CLIENT_400109 = 400109
CLIENT_400110 = 400110
CLIENT_400111 = 400111
CLIENT_400112 = 400112
CLIENT_400113 = 400113
CLIENT_400114 = 400114
CLIENT_400115 = 400115
CLIENT_400116 = 400116

# Payment Status Code Blocks

# -- general payment status codes | 400200 - 400219
CLIENT_400200 = 400200  # unreserved
CLIENT_400201 = 400201  # unreserved
CLIENT_400202 = 400202  # unreserved
CLIENT_400203 = 400203  # unreserved
CLIENT_400204 = 400204  # unreserved
CLIENT_400205 = 400205  # unreserved

# -- android playstore status codes | 400220 - 400239
CLIENT_400220 = 400220  # unreserved
CLIENT_400221 = 400221  # unreserved
CLIENT_400222 = 400222  # unreserved
CLIENT_400223 = 400223  # unreserved
CLIENT_400224 = 400224  # unreserved
CLIENT_400225 = 400225  # unreserved

# -- apple itunes status codes | 400240 - 400259
CLIENT_400240 = 400240  # Invalid apple itunes verification
CLIENT_400241 = 400241  # Invalid transaction validation (apple)
CLIENT_400242 = 400242  # Validation return unsuccessful status
CLIENT_400243 = 400243  # The receipt is exist, the item is already bought
CLIENT_400244 = 400244  # Order restore failed
CLIENT_400245 = 400245  # unreserved


CLIENT_400301 = 400301
CLIENT_400401 = 400401
CLIENT_400501 = 400501

CLIENT_401101 = 401101
CLIENT_401102 = 401102
CLIENT_401103 = 401103
CLIENT_401104 = 401104

CLIENT_403101 = 403101
CLIENT_403102 = 403102

CLIENT_405101 = 405101

CLIENT_409101 = 409101

CLIENT_500101 = 500101

# -- general payment status codes | 400200 - 400219
CLIENT_500200 = 500200  # unreserved
CLIENT_500201 = 500201  # unreserved
CLIENT_500202 = 500202  # unreserved
CLIENT_500203 = 500203  # unreserved
CLIENT_500204 = 500204  # unreserved
CLIENT_500205 = 500205  # unreserved

# -- apple itunes status codes | 500240 - 500159
CLIENT_500240 = 500240  # General apple itunes error
CLIENT_500241 = 500241  # Failed to save transaction data to DB
CLIENT_500242 = 500242  # Failed to save receipt data to DB
CLIENT_500243 = 500243  # Failed to save receipt data from SC 1 (translation)
CLIENT_500244 = 500244  # unreserved
CLIENT_500245 = 500245  # unreserved

####
CLIENT_400901 = 400901


class RoleType(IntEnum):
    """ Mapping of Role primary keys.
    """
    super_admin = 1
    internal_admin = 2
    internal_marketing = 3
    internal_finance = 4
    organization_specific_admin = 5
    organization_specific_marketing = 6
    organization_specific_finance = 7
    guest_user = 8
    unverified_user = 9
    verified_user = 10
    sandbox_user = 11
    public_user = 12
    resource_server = 13
    internal_data_entry = 14
    organization_specific_data_entry = 16
    internal_developer = 17
    cafe_user = 18
    banned_user = 19
    sales_promotion_girl = 20
    internal_admin_eperpus = 21
    organization_admin_eperpus = 22
    customer_support = 23
    admin_customer_support = 24
    publisher_eperpus = 25
    publisher_ebooks = 26
