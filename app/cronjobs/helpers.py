import requests
from datetime import datetime

from sqlalchemy import desc

from app import app, db
from app.items.choices.item_type import ITEM_TYPES, NEWSPAPER
from app.items.models import Item, UserSubscription, ItemUploadProcess
from app.master.models import Brand
from app.logs.models import ApiLog, ItemProcessing
from app.send_mail import sendmail_smtp, sendmail_aws
from app.migrate_data.items_subscriptions import DurationMigrate, EditionMigrate

from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME, \
    STATUS_UPLOADED, STATUS_PREPARE_FOR_CONSUME

DELIVER_NEW = 1
DELIVER_PROCESSING = 2
DELIVER_COMPLETE = 3
DELIVER_ERROR = 4

EDITION = 1
MONTHLY = 4


class PostItemSubs():

    def __init__(self, module=None, item_id=None):
        self.module = module
        self.item_id = item_id
        self.user_subs = []
        self.admin = app.config['ADMIN_EMAILS']
        self.server = app.config['ADMIN_SERVER']
        self.process = False

    def cron_log(self, name=None, error=None):
        # log = ApiLog(
        #     name=name,
        #     http_method='POST',
        #     user_id=1,
        #     role_id=1,
        #     request='cron',
        #     respons=str(error),
        #     response_code=500,
        #     err_message=str(error),
        #     ip_address='cron',
        #     headers='cron'
        # )
        # log.save()
        pass

    def go_post_items(self):
        try:
            # processing item
            if self.item_id:
                item_ready = ItemProcessing.query.filter_by(item_id=self.item_id, status=1).order_by(desc(ItemProcessing.id)).all()
            else:
                item_ready = ItemProcessing.query.filter_by(status=1).order_by(desc(ItemProcessing.id)).all()

            for data_item in item_ready:

                # get item objects
                exist_item = Item.query.filter(
                    Item.id==data_item.item_id,
                    Item.item_status.in_([
                        STATUS_TYPES[STATUS_UPLOADED],
                        STATUS_TYPES[STATUS_PREPARE_FOR_CONSUME],
                        STATUS_TYPES[STATUS_READY_FOR_CONSUME]]),
                    Item.is_active == True
                ).first()

                upload_file = ItemUploadProcess.query.filter(ItemUploadProcess.item_id == data_item.item_id,
                    ItemUploadProcess.status == 10, ItemUploadProcess.error == None).all()

                # check if file uploaded already processing successfully.
                if exist_item and upload_file:
                    # split processing Edition and Duration.
                    subs_sample = UserSubscription.query.filter_by(brand_id=exist_item.brand_id).order_by(
                        desc(UserSubscription.id)).first()

                    if not subs_sample:
                        if exist_item.release_schedule <= datetime.now():
                            # for book or item who doesnt have subs
                            exist_item.item_status = STATUS_TYPES[STATUS_READY_FOR_CONSUME]
                            data_item.status = DELIVER_COMPLETE

                            db.session.add(data_item)
                            db.session.add(exist_item)
                            db.session.commit()
                        continue

                    migrate_edition, migrate_data = EditionMigrate(item=exist_item).construct()
                    migrate_duration, migrate_data = DurationMigrate(item=exist_item).construct()

                    if migrate_edition or migrate_duration:
                        # check if theres release_schedule
                        if (exist_item.release_schedule and exist_item.release_schedule <= datetime.now()):
                            exist_item.item_status = STATUS_TYPES[STATUS_READY_FOR_CONSUME]
                            data_item.status = DELIVER_COMPLETE

                            db.session.add(exist_item)
                            db.session.add(data_item)
                            db.session.commit()
                    else:
                        # this mean something error when deliver items..
                        data_item.status = DELIVER_ERROR
                        db.session.add(data_item)
                        db.session.commit()

        except Exception, e:
            self.cron_log('CRON go_post_items', e)

    def construct(self):
        self.go_post_items()


class PostMonitorSubs():

    def __init__(self, module=None):
        self.module = module
        self.data_item = ''
        self.admin = app.config['ADMIN_EMAILS']
        self.server = app.config['ADMIN_SERVER']

    def cron_log(self, name=None, error=None):
        # log = ApiLog(
        #     name=name,
        #     http_method='POST',
        #     user_id=1,
        #     role_id=1,
        #     request='cron',
        #     respons=str(error),
        #     response_code=500,
        #     err_message=str(error),
        #     ip_address='cron',
        #     headers='cron'
        # )
        # log.save()
        pass

    def send_mail_error(self, items=None, status=None):
        try:
            data_item = Item.query.filter_by(id=items.item_id).first()
            brand = Brand.query.filter_by(id=items.brand_id).first()

            if status == 2:
                mark_status = 'PROCESSING to LONG > 5 MINUTES'
                error = 'Processing to long'
            else:
                mark_status = 'FAILED TO PROCESS.'
                error = items.error

            if data_item:

                content = """
                    Hi Admin, <br><br>

                    Warning, monitoring item..<br><br>

                    Processing_id: %s <br>
                    Item_id : %s - [ %s ] <br>
                    Brand_id : %s - [ %s ]<br>
                    Status : %s <br>
                    Warning : %s <br><br>

                    Please check., <br>
                    MAY FORCE BE WITH YOU <br>
                """ % (
                    items.id,
                    items.item_id, data_item.name,
                    items.brand_id, brand.name,
                    mark_status,
                    error
                )

                subject = 'DELIVER CRON FILE FAILED (%s)' % (self.server)
                try:
                    #print 'send aws..'
                    sendmail_aws(subject, 'Gramedia Digital Monitoring <no-reply@gramedia.com>', self.admin, content)
                except:
                    #print 'send smtp..'
                    sendmail_smtp(subject, 'Gramedia Digital Monitoring <no-reply@gramedia.com>', self.admin, content)

                items.status=5
                items.is_notified = True
                items.save()

        except Exception as e:
            print e


    def go_monitor_item_failed(self):
        try:
            item_ready = ItemProcessing.query.filter_by(status=4).all()
            for itm in item_ready:
                range_time = datetime.now() - itm.process_ends
                range_hours = range_time.seconds / 1800

                if range_hours >= 1:
                    #send email to admin, processing failed
                    self.send_mail_error(itm, 4)

        except Exception, e:
            self.cron_log('CRON go_monitor_item_failed', e)

    def go_monitor_item_processing(self):
        try:
            item_ready = ItemProcessing.query.filter_by(status=2).all()

            for itm in item_ready:
                range_time = datetime.now() - itm.process_ends
                range_hours = range_time.seconds / 1800

                if range_hours >= 1:
                    #send email to admin, processing failed
                    self.send_mail_error(itm, 2)

        except Exception, e:
            self.cron_log('CRON go_monitor_item_processing', e)

    def construct(self):
        self.go_monitor_item_failed()
        self.go_monitor_item_processing()
