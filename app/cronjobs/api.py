# TUTORIAL :
# https://pythonhosted.org/APScheduler/
# https://pythonhosted.org/APScheduler/cronschedule.html
# http://stackoverflow.com/questions/18394605/how-to-set-cron-schedule-and-interval-job-with-apscheduler

from apscheduler.scheduler import Scheduler
from app.discounts.models import Discount, DiscountCode, DiscountCodeLog
from app.cronjobs.helpers import PostItemSubs

sched = Scheduler()
sched.start()

def discount_code():
    """ FIND ALL DISCOUNT CODE, WHERE MAX USE > CURRENT USE
    FOR AVOID DISCOUNT CODE USE MISS WHEN PURCHASE ORDER
    """

    try:
        discount_code = DiscountCode.query.all()
        if discount_code:
            for code in discount_code:
                if code.current_uses >= code.max_uses:
                    log = DiscountCodeLog(
                        code = code.code,
                        max_uses = code.max_uses,
                        current_uses = code.current_uses,
                        is_active = code.is_active,
                        discount_type = code.discount_type,
                        discount_id = code.discount_id
                    )
                    sv = log.save()
                    if sv.get('invalid', False):
                        pass

                    code.delete()

    except Exception, e:
        pass

def discount_code_invalid():
    """ FIND DISCOUNT WITH DISCOUNT CODE TYPE,
    IF VALID TO < DATETIME NOW, ALL DISCOUNT CODE MOVE TO LOG
    """
    try:
        discount = Discount.query.filter_by(discount_type=5).all()
        for idisc in discount:

            #find expired discounts
            if not idisc.discount_valid():
                discount_code = DiscountCode.query.filter_by(discount_id=idisc.id).all()
                for code in discount_code:
                    log = DiscountCodeLog(
                        code = code.code,
                        max_uses = code.max_uses,
                        current_uses = code.current_uses,
                        is_active = code.is_active,
                        discount_type = code.discount_type,
                        discount_id = code.discount_id
                    )
                    sv = log.save()
                    if sv.get('invalid', False):
                        pass

                    code.delete()

    except Exception, e:
        pass



