from flask_appsfoundry.parsers import SqlAlchemyFilterParser, BooleanFilter

from app.auth.models import Client


class ClientArgsParser(SqlAlchemyFilterParser):
    __model__ = Client

    is_active = BooleanFilter()
