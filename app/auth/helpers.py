"""
Auth Helpers
============

Simple helper functions to help with Authentication/Authorization for users
of ScoopCOR API.
"""
from __future__ import unicode_literals

import ast, base64, json, httplib, smtplib, jwt, requests,os

from datetime import timedelta, datetime
from hashlib import sha1

from enum import Enum
from flask import request, g, current_app
from jinja2 import TemplateNotFound
from jwt import InvalidSignatureError
from requests.exceptions import HTTPError

from app.constants import RoleType
from flask_appsfoundry.exceptions import Unauthorized, BadRequest, InternalServerError

from app.master.choices import PlatformType
from app.messaging.email import get_smtp_server, HtmlEmail
from app.auth.models import Token
from app.utils.datetimes import get_utc_nieve

from .exceptions import UserNotFound, UnauthorizedError
from .models import ANONYMOUS_USER, UserInfo
from . import role_perms, _log, constants

from app import app

SCOOP_PORTAL = 67


def get_expire_time():
    # generate new JSON Web Token
    if getattr(g, 'current_client', None):
        if g.current_client.id == SCOOP_PORTAL:
            return timedelta(minutes=5)
        elif g.current_client.platform_id in (PlatformType.ios.value, PlatformType.android.value):
            # login from android and ios devices: set expires to None/never expire
            return None
        else:
            # for login from getscoop, set jwt token expiry to:
            return timedelta(hours=24)
    else:
        return timedelta(hours=24)


def jwt_decode(encoded):
    if getattr(g, 'current_client', None) and \
        g.current_client.id == SCOOP_PORTAL:
        leeway = timedelta(minutes=5)
    else:
        # for login from getscoop/mobile apps, set leeway/grace period to:
        leeway = timedelta(hours=24)
    secret = current_app.config['JWT_SECRET']
    try:
        jwt.decode(encoded, secret, algorithm='HS256', leeway=leeway)
    except InvalidSignatureError:
        secret = current_app.config['GRAMEDIA_SECRET_KEY']

    return jwt.decode(encoded, secret, algorithm='HS256', leeway=leeway)


def jwt_encode(payload):
    secret = current_app.config['JWT_SECRET']
    return jwt.encode(payload, secret, algorithm='HS256')


def jwt_encode_from_user(user, device_id=None, expire_time=timedelta(minutes=5)):
    """ Generate JSON Web Token for a user object

    # example of usage to generate JSON Web Token for certain user
    from app import app
    from app.users.models import User
    from app.auth.helpers import *

    user = User.query.get(12345)
    with app.test_request_context('/'):
        # generate jwt token with 1 year expire time
        print jwt_encode_from_user(user, None, expire_time='1Y')


    :param `app.users.models.User` user:
    :param `string` device_id:
    :param `datetime.timedelta` expire_time:
    :return `string` JSON Web Token:
    """
    payload = get_user_payload(user, device_id, expire_time)
    return jwt_encode(payload)


def get_user_payload(user, device_id, expire_time):
    payload = {
        'user_id': user.id,
        'user_name': user.username,
        'email': user.email,
        'roles': [role.id for role in user.roles],
        'organizations': [org.id for org in user.organizations],
        'device_id': device_id,
        'sig': generate_signature(user.password),
        'iss': constants.TOKEN_ISSUER
    }
    if expire_time:
        # set jwt expires time:
        payload['exp'] = get_utc_nieve() + expire_time
        payload['expire_timedelta'] = expire_time.seconds
    return payload


def generate_signature(key):
    return sha1(app.config['PASSWORD_HASH'] + key).hexdigest() if key else None


def jwt_refresh(encoded):
    # check expired, if not, extend expired time
    # try:
    payload = jwt_decode(encoded)
    expire_time = get_expire_time()
    new_expired_date = get_utc_nieve() + expire_time if expire_time else None
    current_expired = payload.get('exp', None)
    if current_expired and new_expired_date and \
        current_expired < int((new_expired_date - datetime(1970, 1, 1)).total_seconds()):
        #  if current_expired is not set --> non expired token.. we don't need to extend anything
        # else: set new expired date
        payload['expire_timedelta'] = expire_time.seconds,
        payload['exp'] = new_expired_date
        encoded = jwt_encode(payload)
    return encoded
    # except jwt.ExpiredSignatureError as e:
    #     raise e


def get_reset_password_token(user):
    # generate reset password token, it will expired in 3 days
    payload = {
        "user_id": user.id,
        "sub": constants.TOKEN_RESET_PASSWORD_SUBJECT,
        'iss': constants.TOKEN_ISSUER,
        "exp": get_utc_nieve() + timedelta(days=3)
    }
    return jwt_encode(payload=payload)


def get_user_from_reset_password_token(token, session):
    from app.users.users import User
    try:
        payload = jwt_decode(token)
    except jwt.ExpiredSignatureError:
        raise Unauthorized(user_message='Your token has expired, retry reset password again',
                           developer_message='Token expired')
    except (jwt.DecodeError, jwt.InvalidTokenError) as e:
        raise Unauthorized(user_message='Token invalid',
                           developer_message='Token invalid, decode error: {}'.format(str(e)))
    except Exception as e:
        raise Unauthorized(user_message='Failed to authenticate token',
                           developer_message=str(e))

    if payload.get('sub', None) != constants.TOKEN_RESET_PASSWORD_SUBJECT \
        or payload.get('iss', None) != constants.TOKEN_ISSUER:
        raise Unauthorized(user_message='Token invalid',
                           developer_message='Token invalid, subject not match')

    user_id = payload.get('user_id', None) if payload else None

    user = session.query(User).get(user_id) if user_id else None
    if not user:
        raise BadRequest(user_message='User not found',
                         developer_message='user with id {} not found!'.format(user_id))
    return user


def prehash_user_password(plaintext_password):
    """ Frontend currently pre-hashes the user password before sending to backend.

    .. warning::

        For consistency, we need to mimic this functionality, however it would be fantastic to remove
        this requirement later as this adds absolutely no security benefits.

    :param `six.string_types` plaintext_password:
    :return: A password hashed according to the frontend rules.
    """
    return sha1(app.config['PASSWORD_HASH'] + plaintext_password).hexdigest()

def assert_user_password(username, password, session, user=None):

    if not username or not password:
        raise BadRequest(user_message='Please define username and password',
                         developer_message='username or password is empty')
    valid = False
    ebook_user = get_user(username, session)
    if ebook_user:
        # croscek EBOOK SIDES.
        user = ebook_user

        banned_roles = user.roles.filter_by(id=RoleType.banned_user.value).first()
        if banned_roles:
            from app.reports.blacklist import check_user_banned
            message, response = check_user_banned(user.id)
            raise BadRequest(developer_message=message, user_message=message)

        if user and user.salt is None:
            user.set_password(password)
            session.add(user)
            session.commit()
        valid = verify_hash_password(password, user)
    else:
        from app.users.users import User, Role
        verified_role = Role.query.get(RoleType.verified_user.value)
        gramedia_user = get_user_from_gramedia_api(username, password)

        if not gramedia_user:
            # we cannot solved this prehash password things.
            # CS will aproach user to reset their password by apps.
            pass

        if gramedia_user and not ebook_user:
            # create new user from gramedia.com
            new_ebook_user = User(
                username=username.strip().lower(),
                email=username.strip().lower(),
                first_name=gramedia_user.get('first_name', None),
                last_name=gramedia_user.get('last_name', None),
                last_login=datetime.utcnow(),
                is_verified=True,
                excluded_from_reporting=False,
                level='GRAMEDIA',
                created=datetime.utcnow()
            )
            from app import app
            password_sha = sha1(app.config['PASSWORD_HASH'] + password.strip().lower()).hexdigest()
            new_ebook_user.set_password(password_sha)
            new_ebook_user.roles.append(verified_role)
            session.add(new_ebook_user)
            session.commit()
            user = new_ebook_user
            valid=True

        if gramedia_user and ebook_user:
            # always return JWT from ebook.
            user = ebook_user
            valid=True

    if valid:
        return user
    else:
        raise BadRequest(
            developer_message="The username or password entered is incorrect",
            user_message="The username or password you entered is incorrect.")


def verify_hash_password(password, user):
    valid = False
    if password == user.password:
        valid = True
    else:
        hashed_password = user.hash_password(password)
        if hashed_password == user.password:
            valid = True
        else:
            hashed_password = user.hash_password(prehash_user_password(password))
            if hashed_password == user.password:
                valid = True
    return valid


def get_user(username, session):
    from app.users.users import User
    user = session.query(User).filter_by(username=username.lower()).first()
    if not user:
        user = session.query(User).filter_by(email=username.lower()).first()
    if not user:
        user = session.query(User).filter_by(email=username).first()
    return user


def get_user_from_gramedia_api(username, password):
    gramedia_url = current_app.config['GRAMEDIA_COM_API_URL']
    gramedia_auth_path = current_app.config['GRAMEDIA_COM_AUTH_API_PATH']
    import requests
    # try to find user from gramedia.com
    resp = requests.post('{base_url}/{auth_path}'.format(base_url=gramedia_url, auth_path=gramedia_auth_path),
                         json={'email': username, 'password': password})
    data = None
    if resp.status_code == httplib.OK:
        token = resp.json().get('token')
        data = jwt_decode(token)
        data['is_active'] = True
    return data


def get_user_from_gramedia(username, password):
    from app.users.users import get_user_from_gramedia_db
    data = get_user_from_gramedia_api(username, password)
    email = data.get('email')
    user = get_user_from_gramedia_db(email)

    return user


def send_reset_password_token_email(user, token, user_agent='ebooks'):
    if user_agent in ['scoop_ios', 'scoop_android', 'scoop_web', 'ebooks']:
        message = HtmlEmail(
            sender='no-reply@gramedia.com',
            to=[user.email, ],
            subject='Gramedia Digital - Reset Password',
            html_template='email/reset-password/password_reset.html',
            plaintext_template='email/reset-password/password_reset.txt',
            reset_url="{int_base}/reset-password/{reset_token}".format(
                int_base=current_app.config['INT_BASE_URL'],
                reset_token=token))
    else:
        from app.eperpus.constants import PUBLIC_LIBRARY_CLIENT_ID
        from app.users.organizations import Organization
        from app.auth.models import Token, Client

        # Find organization parent by user agent.
        exist_client = Client.query.filter_by(app_name=user_agent).first()
        exist_organization_parent = Organization.query.filter(Organization.clients.any(id=exist_client.id),
                                                              Organization.parent_organization_id == None).first()

        if exist_organization_parent:
            organization_name = exist_organization_parent.app_name

            # for handling eperpus public who has more than 1 clients!
            if exist_client.id in PUBLIC_LIBRARY_CLIENT_ID:
                organization_name = 'Eperpus'

            if exist_organization_parent.logo_url:
                image_logo = exist_organization_parent.logo_url.split('/')[-1:][0]
                logo_client = os.path.join(app.config['OFFERS_BASE_URL'], 'eperpus-logo',
                                           str(exist_organization_parent.id), image_logo)
            else:
                # Default logo.. if None in database.
                logo_client = os.path.join(app.config['OFFERS_BASE_URL'], 'eperpus-logo', 'eperpus.png')

            data_token = '{}@@{}'.format(token, logo_client)
            encode_token = base64.b64encode(data_token)

            message = HtmlEmail(
                sender='no-reply@gramedia.com',
                to=[user.email, ],
                subject='{} - Reset Password'.format(organization_name),
                logo_client=logo_client,
                organization_name=organization_name,
                html_template='email/reset-password/eperpus_password_reset.html',
                plaintext_template='email/reset-password/eperpus_password_reset.txt',
                reset_url="{int_base}/reset-password/{reset_token}".format(
                    int_base=current_app.config['EPERPUS_URL_BASE'],
                    reset_token=encode_token))

    server = get_smtp_server()

    try:
        # m = message.rendered_content
        server.send_message(message)
    except (smtplib.SMTPException, TemplateNotFound) as e:
        _log.exception(
            "Failed to send password reset token for user with username = {} and email {}. "
            "Error: {}".format(user.username, user.email, str(e)))
        error_msg = "Failed to send password reset token for user with username = {} and email {}".format(
            user.username, user.email
        )
        raise InternalServerError(
            user_message=error_msg, developer_message=error_msg)


def assert_user_has_any_permission(required_permissions, user_info):
    """ Check if the current user any of the required permissions.

    If the user has any of the permissions, this check will pass and return
    nothing.

    When this application is running in 'test' mode, these checks will be
    disabled and a warning will be issued.

    :param iterable of strings required_permissions: List of permissions that
        the user's permissions will be checked against.
    :param `UserInfo` user_info: The user information to use for Authorization.
    :return: None if the check passes.
    :raises: :class:`UnauthorizedError` if the user does
        not have any of the required permissions.
    """
    if any([req_perm in user_info.perm for req_perm in required_permissions]):
        pass
    else:
        raise UnauthorizedError("Token {}, Invalid Permissions".format(
            "Invalid" if user_info is ANONYMOUS_USER else "Valid"
        ))


def assert_user_has_all_permissions(required_permissions, user_info):
    if all([(req_perm in user_info.perm) for req_perm in required_permissions]):
        pass
    else:
        raise UnauthorizedError("Token {}, Invalid Permissions".format(
            "Invalid" if user_info is ANONYMOUS_USER else "Valid"
        ))


def fetch_user_from_redis(redis_server, prefix, user_id, password):
    """ Attempts lookup user information from a Redis server.

    :param :class:`redis.StrictRedis` redis_server: The redis server instance
        used to query for user information.
    :param `str` prefix: A string-based token prefix (including the colon), that
        will be appended onto the front of the user's id for a redis query.
    :param `int` user_id: A unique identifier
    :param `str` password: The user's hashed password
        (should match their redis token)
    :return: UserInfo object representing the user.
    :raises: :class:`UnauthorizedError` If the user's information was found in
        redis, but we're unable to parse it.
    :raises: :class:`UserNotFound` If the user's information is not present
        in the specified Redis server.
    """
    lookup_key = "{prefix}:{key}".format(prefix=prefix.rstrip(":"), key=user_id)
    # _log.debug("Fetching user from REDIS with key: {}".format(lookup_key))

    user_info_dict = redis_server.get(lookup_key)

    if not user_info_dict:
        raise UserNotFound
    else:
        try:
            user_info = UserInfo(**ast.literal_eval(user_info_dict))

            if user_info.token == password:
                return user_info
            else:
                # _log.debug(
                #    "User Redis Token {} did not match password {}".format(
                #        user_info.token, password))
                raise UnauthorizedError("Password did not match redis.")

        except (ValueError, SyntaxError, IndexError) as e:
            # _log.debug("Error decoding data {}".format(e))
            raise UnauthorizedError(
                "An error happened decoding data from Redis: {0}".format(e)
            )


def fetch_user_from_scoopcas(user_id, password):
    """ Attempts to fetch user information from ScoopCAS

    :param `str` user_id: The user ID
    :param `str` password: The user's hashed password.
    :return: The user info of the user that was authenticated.
    :raises: :class:`UnauthorizedError`
    :raises: `HTTPError`
    """
    headers = current_app.config['JSON_HEADERS']
    headers['Authorization'] = current_app.config['AUTH_HEADER']
    json_form = {'user_id': user_id, 'access_token': password}

    try:
        resp = requests.post(
            current_app.config['VERIFY_TOKEN'],
            data=json.dumps(json_form),
            headers=headers)

        if resp.status_code == httplib.OK:

            return UserInfo(
                user_id=resp.json()['user_id'],
                username=resp.json()['username'],
                token=password,
                perm=resp.json()['scope'].split(" "))

        else:
            raise HTTPError("Non-200 HTTP status code from ScoopCAS: {}".format(
                resp.status_code))

    except HTTPError:
        raise UnauthorizedError("Token Invalid, Invalid Permissions")


def credentials_from_bearer_in_request(req=request):
    """ Fetch the Username and (hashed) Password from Oauth2-style headers.

    If the Authorization: Bearer xyz header is missing, or we're unable to
    to parse the header, than an exception will be raised.

    :param :py:class:`flask.Request` req: Current request object.
    :return: Username, Password in a tuple from Auth header
    :rtype: tuple(string, string)
    :raises: :class:`UnauthorizedError` if unable to decode
        Authorization header, or headers are missing, or in an incorrect format.
    """
    try:
        auth_header = req.headers['Authorization']

        (bearer, token) = auth_header.split(' ')

        # sanity check -- we always expect the auth header to include 'Bearer'
        if not bearer == 'Bearer':
            raise UnauthorizedError("Incorrect Authorization header")

        # Decode the token passed in the Bearer portion
        decoded = base64.b64decode(token)
        (username, password) = decoded.split(":")

        # Make sure either the username or password is not blank
        if '' in (username, password):
            raise UnauthorizedError("Invalid Bearer token")

        # validate token in data:
        user_token = Token.query.filter_by(
            token=password,
            user_id=username,
            is_active=True
        ).first()
        if user_token:
            return username, password
        else:
            return None, None

    except KeyError:
        raise UnauthorizedError("Missing Authorization Header")

    except (ValueError, TypeError):
        raise UnauthorizedError("Bearer decode failed")


def get_user_from_request(req):
    """ Get the user credential for the current request.

    If the Authorization: JWT abc123xyz header is exists, we decode the token using JWT
    else if Authorization: Bearer abc123xyzber header is exists, we decode in credentials_from_request

    :return: user_id, token in a tuple from Auth header
    :raises: :class:`UnauthorizedError` if JWT Token exists and it's expired or unable to decode it.
        if Bearer token is invalid, we ignore it, return None value (it will become anonymous user later)
        if authorization header is missing, we don't raise any exception, just return None value
    """
    user_id = None
    realm = None
    payload = None
    token = None
    auth_header = req.headers.get('Authorization', None)
    try:
        if auth_header:
            (key, token) = auth_header.split()
        else:
            # if authorization header is missing, we don't raise any exception, just return anonymous user later
            return CredentialData(user_id, token, payload, realm)

        if key == RealmEnum.jwt.value:
            # new authentication using JSON Web Token
            payload = jwt_decode(token)

            issuer = payload.get('iss')
            user_id = payload.get('user_id', None)
            realm = RealmEnum.jwt

            if issuer == 'GRAMEDIA':
                from app.users.users import User
                from app import db
                session = db.session()
                user = session.query(User).filter_by(email=payload.get('email').lower()).first()

                if user:
                    user_id = user.id
                else:
                    # try to get user from gramedia
                    from app.users.users import get_user_from_gramedia_db
                    user = get_user_from_gramedia_db(payload.get('email'))
                    user_id = user.id

        elif key == RealmEnum.bearer.value:
            # if Authorization: JWT abc123xyz not exists, try authentication using Bearer
            user_id, token = credentials_from_bearer_in_request(req=req)
            realm = RealmEnum.bearer

        return CredentialData(user_id, token, payload, realm)

    except jwt.ExpiredSignatureError:
        raise Unauthorized(user_message='Your session has expired',
                           developer_message='Your session/token has expired')
    except (jwt.DecodeError, jwt.InvalidTokenError) as e:
        raise Unauthorized(user_message='Token invalid',
                           developer_message='Token invalid'.format(str(e)))
    except KeyError:
        # Missing Authorization Header
        return CredentialData(user_id, token, payload, realm)
    except (ValueError, TypeError):
        raise UnauthorizedError("Bearer decode failed")
    except Exception as e:
        return CredentialData(user_id, token, payload, realm)


class RealmEnum(Enum):
    jwt = 'JWT'
    bearer = 'Bearer'


class CredentialData(object):

    def __init__(self, user_id, token, payload=None, realm=None):
        self.user_id = user_id
        self.token = token
        self.payload = payload
        self.realm = realm


def authenticate_user_on_session():
    """ Authenticates the user credentials for the current request.

    .. warning::

        This method has side-effects.

    After the user credentials have been authenticated, they are stored in
    the flask global object.  If authentication fails (ie, not provided
    or invalid), the ANONYMOUS_USER will be stored.

    This method is not concerned with Authorization, only Authentication,
    and will swallow all errors.
    """
    from app.users.users import User
    credential_data = get_user_from_request(req=request)

    g.user = ANONYMOUS_USER
    g.current_user = None
    g.current_user_roles = None
    if credential_data and credential_data.user_id:
        try:
            g.current_user = User.query.get(credential_data.user_id)
        except:
            pass

        # validate user data in token/jwt payload vs existing data
        assert_user_payload(g.current_user, credential_data)

        if g.current_user and g.current_user.is_active:
            # stored list of role_id's and organization_id's in g
            #  (to prevent query to db multiple times just to get user role id's)
            g.current_user_roles = [role.id for role in g.current_user.roles.all()]
            # stored it in g.user object
            g.user = UserInfo(
                user_id=g.current_user.id,
                username=g.current_user.username,
                token=credential_data.token,
                perm=get_permissions(g.current_user_roles))


def assert_user_payload(user, credential_data):
    if user and credential_data and credential_data.realm == RealmEnum.jwt:
        # validate user signature
        assert_user_signature_in_payload(user, credential_data)

        # re-validate device id in this JWT Payload vs registered devices..
        # if it's removed/revoked, not in list of user devices anymore -> raise 401 error -> user should re login!
        if credential_data.payload.get('device_id', None):
            from app.auth.user_devices import UserDevices
            UserDevices(
                user_id=user.id
            ).assert_device_still_registered(
                device_id=credential_data.payload.get('device_id', None)
            )


def assert_user_signature_in_payload(user, credential_data):
    """ Validate user signature in JWT Payload
    if user already change the password, the old JWT should be invalid!
    user should re-login and get new valid JWT
    :param user:
    :param credential_data:
    :return:
    """
    if user and credential_data and credential_data.payload:
        signature = credential_data.payload.get('sig', None)
        expected_signature = generate_signature(user.password)
        if signature and signature != expected_signature:
            raise Unauthorized(
                user_message='Token signature is invalid, please login again',
                developer_message='JWT Token signature is invalid, password already changed, '
                                  'login again to get new valid token')


def get_permissions(user_roles):
    # get list of permission from roles then remove duplicate element from perm list
    return list(set([v for role_id in user_roles for v in role_perms.PERMS.get(role_id, None)]))


def get_current_user_roles():
    """ to prevent query to db multiple times just to get user role id's, we provide this method

    :return: list of role_id's from current user
    """
    return g.current_user_roles if getattr(g, 'current_user_roles', None) and g.current_user_roles else None


def get_user_role_ids(user=None):
    """

    :param `app.models.User` user: (optional) An instance of User
    :return:
    """
    current_user_id = g.current_user.id if getattr(g, 'current_user', None) else None
    if user and user.id != current_user_id:
        user_role_ids = [role.id for role in user.roles.all()]
    else:
        user_role_ids = get_current_user_roles()
    return user_role_ids


def is_superuser(user):
    """ Returns a boolean indication whether the user has the Superuser token.

    .. warning::

        This is as fragile as your database state.  We make assumptions based
        on a set of staticly-defined primary key values for cas_roles.id

    :param `app.models.User` user: A user to test for the Superuser Token.
    :rtype: bool
    :return: True if the user is a superuser.  False in any other circumstance.
    """
    if not user:
        return False
    return any(r.id == RoleType.super_admin for r in user.roles)


def reformat_unauthorized_error(response):
    if response and response.status_code == 401 and response.data:
        import json
        try:
            data = json.loads(response.data)
        except:
            # response data is not a correct json format
            data = None
        error_message = data.get('message', None) if data else None
        if error_message and data and not data.get('user_message', None):
            response.data = json.dumps({
                'status': response.status_code,
                'error_code': response.status_code,
                'user_message': error_message,
                'developer_message': error_message
            })
    return response
