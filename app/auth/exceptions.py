"""
Auth Exceptions
===============

Specialized exception types that can be raised by our authentication system.
These should all be compatible with Werkzeug exceptions (therefore subclass
authorization errors).
"""
from flask_appsfoundry.exceptions import Unauthorized


class UserNotFound(Unauthorized):
    """ Raised when a remote service is checked for a user, but none was found.
    """
    def __init__(self, *args, **kwargs):
        super(UserNotFound, self).__init__(*args, **kwargs)


class UnauthorizedError(Unauthorized):

    def __init__(self, message):
        super(UnauthorizedError, self).__init__(
            user_message=message, developer_message=message)
