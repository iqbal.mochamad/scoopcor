from __future__ import unicode_literals, absolute_import

from unittest import TestCase
import json

from app import app, db
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import ClientFactory


class CurrentClientApiTests(TestCase):

    @classmethod
    def setUpClass(cls):
        db.drop_all()
        db.create_all()
        session = db.session()
        generate_static_sql_fixtures(session)
        ClientFactory(id=1000, app_name='a_valid_client', platform_id=1)
        session.commit()

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.drop_all()

    def test_undetected_client(self):
        c = app.test_client(use_cookies=False)
        resp = c.get('/v1/clients/current-client',
                     headers={
                         'Accept': 'application/vnd.scoop.v3+json',
                         'User-Agent': 'Some BS Client/3.0 (That we should not detect)'
                     })

        self.assertEqual(resp.status_code, 404)

    def test_detected_client(self):
        c = app.test_client(use_cookies=False)
        resp = c.get('/v1/clients/current-client',
                     headers={
                         'Accept': 'application/vnd.scoop.v3+json',
                         'User-Agent': 'A Valid Client/1.0 (We should detect this one)'
                     })

        self.assertEqual(resp.status_code, 200)
        self.assertDictEqual(
            json.loads(resp.data),
            {
                'app_name': 'a_valid_client'
            }
        )
