import json
from base64 import b64encode

from flask import url_for

from app.auth.helpers import jwt_decode
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import TokenFactory


class TokenBearerToJwtTests(TestBase):

    def tests_convert_bearer_to_jwt(self):
        with self.on_session(self.regular_user):
            user_id = self.regular_user.id
            password = self.fake.password()
            # prepare old bearer
            old_bearer = b64encode("{user_id}:{password}".format(
                user_id=user_id, password=password)
            )
            token = TokenFactory(token=password, user_id=user_id, is_active=True,
                                 perm_list=['can_read_write_global_all', ])
            self.session.add(token)
            self.session.commit()

            headers = self.build_headers(has_body=True)
            headers['Authorization'] = "Bearer {}".format(old_bearer)
            resp = self.api_client.post(
                'v1/auth/token/bearer-to-jwt',
                data=json.dumps({
                    "device_id": self.fake.bs()
                }),
                headers=headers,
            )
            self.assert_status_code(resp, 200)
            self.assert_response_body_expectation(resp, self.regular_user)

            # empty device id, error raised
            headers['Authorization'] = "Bearer {}".format(old_bearer)
            resp = self.api_client.post(
                'v1/auth/token/bearer-to-jwt',
                data=json.dumps({
                    "device_id": None
                }),
                headers=headers,
            )
            self.assert_status_code(resp, 400)

    def test_invalid_bearer(self):
        # with invalid bearer
        old_bearer_invalid = b64encode("{user_id}:{password}".format(
            user_id=123, password="dasfkdlfjaskdf")
        )

        headers = self.build_headers(has_body=True)
        headers['Authorization'] = "Bearer {}".format(old_bearer_invalid)
        resp = self.api_client.post(
            'v1/auth/token/bearer-to-jwt',
            data=json.dumps({
                "device_id": self.fake.bs()
            }),
            headers=headers,
        )
        self.assert_status_code(resp, 401)

    def assert_response_body_expectation(self, response, user=None):

        user = user or self.regular_user

        # we can't directly validate token in dict because of time differents, we decode it later to validate it
        actual = json.loads(response.get_data())
        token = actual.pop('token')

        payload = jwt_decode(token)
        expected = {
            'id': user.id,
            'href': url_for('users.details', user=user, _external=True),
            'realm': 'JWT',
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'is_verified': True,
            'expire_timedelta': payload.get('expire_timedelta', None),
            'expires': payload.get('exp', None),
        }
        self.assertDictContainsSubset(expected, actual)
        self.assertEqual(user.id, payload.get('user_id'))
        self.assertEqual(user.username, payload.get('user_name'))
        self.assertEqual(user.email, payload.get('email'))
