from __future__ import unicode_literals, absolute_import

import httplib
import json
from unittest import TestCase

from app import app, db
from app.auth.helpers import jwt_encode_from_user
from app.constants import RoleType
from app.users.users import User
from tests.fixtures.helpers import generate_static_sql_fixtures, create_user_and_token
from app.users.tests.fixtures import OrganizationFactory


class ApiTests(TestCase):

    maxDiff = None
    request_url = "/v1/auth/change-password"
    request_headers = {
        'Accept': "application/json",
        'Accept-Charset': 'utf-8',
        'X-Scoop-Client': 'scoop web/1.0.0',
        'User-Agent': 'scoop web/1.0.0'
    }
    client = app.test_client(use_cookies=False)

    def test_all_is_well(self):
        with app.test_request_context('/'):
            session = db.session()
            self.user = session.query(User).get(USER_ID)
            self.request_headers['Authorization'] = 'JWT {}'.format(
                jwt_encode_from_user(self.user, device_id='testdevice213'))
            new_password = 'new_password'
            request_body = {
                'password': PASSWORD,
                'new_password': new_password
            }
            # change password via put method
            response = self.client.put(
                self.request_url,
                data=json.dumps(request_body),
                headers=self.request_headers)
            self.assertEqual(response.status_code, httplib.NO_CONTENT)

            # check database, new password should be updated
            user = session.query(User).get(USER_ID)
            self.assertEqual(user.password, user.hash_password(new_password))

            # test login with new password
            request_body = {
                'username': self.user.username,
                'password': new_password,
                'device_id': 'dummy-device-id-12335'
            }
            self.request_headers.pop('Authorization', None)
            response = self.client.post('/v1/auth/login',
                                        data=json.dumps(request_body),
                                        headers=self.request_headers)
            self.assertEqual(response.status_code, httplib.OK,
                             'Failed to login with new password, response {} != 200 (OK)'.format(
                                 response.status_code
                             ))

    def test_user_not_found(self):
        # no authorization header defined
        request_body = {
            'password': PASSWORD,
            'new_password': 'dfsdfsadf'
        }
        # remove authorization header to simulate the error
        self.request_headers.pop('Authorization', None)
        # test url with invalid user id
        response = self.client.put(
            self.request_url,
            data=json.dumps(request_body),
            headers=self.request_headers)
        self.assertEqual(response.status_code, httplib.UNAUTHORIZED)

    def test_password_invalid(self):
        request_body = {
            'password': 'invalid-password',
            'new_password': 'dfsdfsadf'
        }
        session = db.session()
        self.user = session.query(User).get(USER_ID)
        with app.test_request_context('/'):
            self.request_headers['Authorization'] = 'JWT {}'.format(
                jwt_encode_from_user(self.user, device_id='testdevice213'))
            response = self.client.put(
                self.request_url.format(USER_ID),
                data=json.dumps(request_body),
                headers=self.request_headers)
            self.assertEqual(response.status_code, httplib.UNAUTHORIZED)


USER_ID = 112233
USERNAME = 'test-user'
EMAIL = 'test-user@email.com'
PASSWORD = 'we4scoop'
user_test = None


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()
    session = db.session()
    generate_static_sql_fixtures(session)
    org = OrganizationFactory(id=1023)
    global user_test
    user_test, token = create_user_and_token(
        session,
        RoleType.super_admin,
        id=USER_ID,
        username=USERNAME,
        email=EMAIL,
        organizations=[org, ])
    #
    session.commit()


def teardown_module():
    db.session().close_all()
    db.drop_all()
