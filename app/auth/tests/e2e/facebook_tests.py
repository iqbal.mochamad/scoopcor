import json
import facebook
from httplib import OK, UNPROCESSABLE_ENTITY
from unittest import TestCase
from faker import Factory

from flask import url_for
from mock import patch

from app import db, app
from app.auth.helpers import jwt_decode
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from tests.fixtures.helpers import generate_static_sql_fixtures


class FacebookLoginTests(TestCase):
    headers = {
        'Accept': 'application/vnd.scoop.v3+json',
        'Content-Type': 'application/json',
        'Accept-Charset': 'utf-8',
        'X-Scoop-Client': 'scoop web/1.0.0'
    }

    @classmethod
    def setUpClass(cls):
        db.session().close_all()
        db.drop_all()
        db.create_all()
        session = db.session()
        generate_static_sql_fixtures(session)
        session.commit()
        cls.api_client = app.test_client(use_cookies=False)
        cls.user = None

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.drop_all()

    @patch.object(facebook.GraphAPI, 'get_object', autospec=True)
    def test_login_with_facebook(self, mock_fb_get_object):
        fb_email = 'fb_email1@testemail.com'
        mock_fb_get_object.return_value = {
            "id": _fake.pyint(),
            "email": fb_email,
            "first_name": _fake.name(),
            "last_name": _fake.name(),
            "birthday": "12/24/1977",
            "gender": "male",
        }

        url = '/v1/auth/facebook'
        body = {
            "fb_access_token": "CAAKvgAX7BZCUBAAnqDP3mMh6iF4GZBMSJVTXHe5aCxa1eqVp7yxZBp91xFUZCShhHT4QZAK4ZBcq7BSSCDN9zlLC2Eq5VyHxDSkwnSMHxxYU6YFkecUI9j9EH8kdcPPgRcKSV3tZBPgJKKhZB0VcZA0PlmnF3llgSOuCwrIfXDN0FRwRsr3A4y9bHpRrcQYaSmp4ZD",
            "client_id": 1,
            "device_id": "XHe5aCxa1eqVp7yxZBp91xFUZCShhHT4QZAK4ZBcq7BSSCD",
            # "device_id": None,
            "scopes": "can_read_write_public can_read_write_public_ext",
            "origin_device_model": "HTC-X",
            "origin_partner_id": 2
        }
        response = self.api_client.post(url,
                                        data=json.dumps(body),
                                        headers=self.headers)
        self.assertEqual(response.status_code, OK,
                      "Response status code is not OK, response {}".format(response.data))
        session = db.session()
        user = session.query(User).filter_by(email=fb_email).first()
        self.assert_response_body_expectation(response, user=user)

        # # old facebook login response from cas documentation
        # expected = {
        #     "first_name": "Benny",
        #     "last_name": "Gotama",
        #     "user_id": 5,
        #     "expires_in": 86400,
        #     "access_token": "22a7e722c9540ef07aa8df698186f33274d8cb6588d224fb0f6abf59ae180a990a25dc4d0cd4f9f8",
        #     "title": 1,
        #     "token_type": "Bearer",
        #     "scope": "can_read_write_public can_read_write_public_ext",
        #     "email": "gotama.benny@gmail.com",
        #     "is_verified": True,
        #     "refresh_token": "43f7cbca144901b96e48b95a19f30a8af8898ce4e85bceb25114376569f9166331eb1bbda9fcf3ac"
        # }

    def assert_response_body_expectation(self, response, user=None):
        with app.test_request_context('/'):

            user = user or self.user
            # we can't directly validate token in dict because of time differents, we decode it later to validate it
            actual = json.loads(response.get_data())
            token = actual.pop('token')

            payload = jwt_decode(token)
            expected = {
                'id': user.id,
                'href': url_for('users.details', user=user, _external=True),
                'realm': 'JWT',
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                'is_verified': True,
                'expire_timedelta': payload.get('expire_timedelta', None),
                'expires': payload.get('exp', None),
            }
            self.assertDictContainsSubset(expected, actual)
            self.assertEqual(user.id, payload.get('user_id'))
            self.assertEqual(user.username, payload.get('user_name'))
            self.assertEqual(user.email, payload.get('email'))
            self.assertEqual(user.is_verified, True)

    def test_login_with_facebook_wrong_body(self):
        body = {
            "wrong_body_param": "CAAKvgAX7BZCUBAAnqDP3mMh6iF4GZBMSJVTXHe5aCxa1eqVp7yxZBp91xFUZCShhHT4QZAK4ZBcq7BSSCDN9zlLC2Eq5VyHxDSkwnSMHxxYU6YFkecUI9j9EH8kdcPPgRcKSV3tZBPgJKKhZB0VcZA0PlmnF3llgSOuCwrIfXDN0FRwRsr3A4y9bHpRrcQYaSmp4ZD",
            "wrong_body_param2": 1,
        }
        url = '/v1/auth/facebook'

        response = self.api_client.post(url,
                                        data=json.dumps(body),
                                        headers=self.headers)
        self.assertEqual(response.status_code, UNPROCESSABLE_ENTITY,
                         "Response status code is not 422 (UNPROCESSABLE_ENTITY), response {}".format(response.data))

    @patch.object(facebook.GraphAPI, 'get_object', autospec=True)
    def test_deauthorize_with_facebook(self, mock_fb_get_object):
        email_deauthorized = 'fb_email_deauthorize@testmail.com'
        session = db.session()
        user_deauthorize = UserFactory(id=1023232, email=email_deauthorized)
        session.commit()

        mock_fb_get_object.return_value = {
            "id": _fake.pyint(),
            "email": email_deauthorized,
            "first_name": _fake.name(),
            "last_name": _fake.name(),
            "birthday": "12/24/1977",
            "gender": "male",
        }

        body = {
            "fb_access_token": "CAAKvgAX7BZCUBAAnqDP3mMh6iF4GZBMSJVTXHe5aCxa1eqVp7yxZBp91xFUZCShhHT4QZAK4ZBcq7BSSCDN9zlLC2Eq5VyHxDSkwnSMHxxYU6YFkecUI9j9EH8kdcPPgRcKSV3tZBPgJKKhZB0VcZA0PlmnF3llgSOuCwrIfXDN0FRwRsr3A4y9bHpRrcQYaSmp4ZD",
            "client_id": 1
        }
        url = '/v1/auth/facebook-deauthorize'
        response = self.api_client.post(url,
                                        data=json.dumps(body),
                                        headers=self.headers)
        self.assertEqual(response.status_code, OK,
                         "Response status code is not OK, response {}".format(response.data))

        url = '/v1/auth/facebook-deauthorize-all'
        response = self.api_client.post(url,
                                        data=json.dumps(body),
                                        headers=self.headers)
        self.assertEqual(response.status_code, OK,
                         "Response status code is not OK, response {}".format(response.data))


_fake = Factory.create()
