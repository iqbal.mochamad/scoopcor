from __future__ import unicode_literals, absolute_import
import json
import random
from httplib import UNAUTHORIZED, OK
from unittest import TestCase

from nose.tools import istest

from app import db, app
from app.auth.helpers import prehash_user_password
from app.auth.models import Application, Client
from app.auth.user_devices import UserDevices, UserDeviceData
from app.constants import RoleType
from app.users.users import User
from app.utils.testcases import TestBase
from tests.fixtures.helpers import generate_static_sql_fixtures, create_user_and_token
from app.users.tests.fixtures import OrganizationFactory


@istest
class DeauthorizeTests(TestBase):
    """ Test Deauthorize API to deauthorize the oldest device from user-devices list """

    maxDiff = None
    request_url = "/v1/auth/deauthorize"
    client = app.test_client(use_cookies=False)
    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'X-Scoop-Client': 'scoop ios/1.0.0'
    }

    @classmethod
    def setUpClass(cls):
        super(DeauthorizeTests, cls).setUpClass()

        cls.max_devices_count = 5
        cls.user = cls.session.query(User).get(cls.regular_user.id)
        cls.password = "we4scoop"

        cls.user_devices = UserDevices(user_id=cls.user.id)
        # clean up cached user device data
        cls.user_devices.clear_cache()
        # re-create some dummy user devices data into redis
        count = 1
        while count <= cls.max_devices_count:
            data = UserDeviceData(client_id=1, device_id='kkyy::{}'.format(count),
                                  application_id=1)
            cls.user_devices.save(data=data)
            count += 1

        # add device from different application (should not be removed, different app)
        cls.user_devices.save(UserDeviceData(
            client_id=555, device_id='kkyy::{}'.format(555),
            application_id=4))

        # deauthorize device
        request_body = {
            'username': cls.user.username,
            'password': cls.password
        }
        cls.response = cls.client.post(cls.request_url,
                                       data=json.dumps(request_body),
                                       headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(OK, self.response.status_code, "Status_code != 200, {}".format(self.response.data))

    def test_with_invalid_user_password(self):
        # create a post request for deauthorize device with wrong password
        with self.on_session(self.user):
            request_body = {
                'username': self.user.username,
                'password': 'invalid-password',
            }
            self.response = self.client.post(self.request_url,
                                             data=json.dumps(request_body),
                                             headers=self.request_headers)
            self.assertEqual(self.response.status_code, UNAUTHORIZED)


@istest
class DeauthorizeAllTests(DeauthorizeTests):

    request_url = "/v1/auth/deauthorize-all"

    def test_login_then_deauthorized(self):
        """ Test: old JWT with device id that already revoked/deauthorized should be invalid

        :return:
        """
        # login with a device id and get JWT
        request_body = {
            'username': self.user.username,
            'password': self.password,
            'device_id': 'dummy-device-id-11889935'
        }
        self.response = self.client.post('v1/auth/login',
                                         data=json.dumps(request_body),
                                         headers=self.request_headers)
        self.assertEqual(self.response.status_code, OK)
        # get JWT
        resp = json.loads(self.response.data)
        headers = self.request_headers.copy()
        auth_token = "{} {}".format(resp.get('realm'), resp.get('token'))
        headers['Authorization'] = auth_token

        # test to access some API (get) --> should be ok with the same token
        resp_get = self.client.get('v1/banners',
                                   data=json.dumps(request_body),
                                   headers=headers)
        self.assertEqual(resp_get.status_code, OK)

        # deauthorize all device
        request_body = {
            'username': self.user.username,
            'password': self.password
        }
        self.response = self.client.post(self.request_url,
                                         data=json.dumps(request_body),
                                         headers=self.request_headers)

        # test access some API again (get) --> should be 401 unauthorized
        resp_get = self.client.get('v1/banners',
                                   data=json.dumps(request_body),
                                   headers=headers)
        self.assertEqual(resp_get.status_code, UNAUTHORIZED)

        # next -- test login again.. get new JWT, should be valid -- start
        request_body = {
            'username': self.user.username,
            'password': self.password,
            'device_id': 'dummy-device-id-11889935-xz'
        }
        self.response = self.client.post('v1/auth/login',
                                         data=json.dumps(request_body),
                                         headers=self.request_headers)
        self.assertEqual(self.response.status_code, OK)
        # get JWT
        resp = json.loads(self.response.data)
        headers = self.request_headers.copy()
        auth_token = "{} {}".format(resp.get('realm'), resp.get('token'))
        headers['Authorization'] = auth_token

        # test to access some API (get) --> should be ok with the same token
        resp_get = self.client.get('v1/banners',
                                   data=json.dumps(request_body),
                                   headers=headers)
        self.assertEqual(resp_get.status_code, OK)
        # next -- test login again.. get new JWT, should be valid -- end

#
# def setup_module():
#     db.drop_all()
#     db.create_all()
#     session = db.session()
#     generate_static_sql_fixtures(session)
#     org = OrganizationFactory(id=1023)
#     user, token = create_user_and_token(session, RoleType.super_admin, organizations=[org, ])
#     user.set_password(prehash_user_password(PASSWORD))
#     session.add(user)
#     global user_id
#     user_id = user.id
#     session.commit()
#
#
# def teardown_module():
#     db.session().close_all()
#     db.drop_all()
