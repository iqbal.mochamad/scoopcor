from __future__ import unicode_literals, absolute_import

from factory import alchemy, Faker
from nose.tools import istest

from app import db
from app.auth.models import Client, Application
from app.utils.testcases import CrudBase


class ApplicationFactory(alchemy.SQLAlchemyModelFactory):

    max_devices = Faker('random_number')
    name = Faker('first_name')
    enable_unlimited_devices = Faker('boolean')

    class Meta:
        sqlalchemy_session = db.session
        model = Application


@istest
class ApplicationCrudTests(CrudBase):

    request_url = '/v1/applications'
    fixture_factory = ApplicationFactory
    model = Application
    list_key = 'data'

    @classmethod
    def setUpClass(cls):
        super(ApplicationCrudTests, cls).setUpClass()
        db.engine.execute("select setval('applications_id_seq', 100);")

    def get_request_body(self):
        request_body = {
            "name": self.fake.name(),
            "max_devices": self.fake.pyint(),
            "enable_unlimited_devices": self.fake.pybool(),
            'is_active': self.fake.pybool(),
        }
        return request_body

    def get_expected_response(self, entity):
        expected = {
            "id": entity.id,
            "name": entity.name,
            "max_devices": entity.max_devices,
            'is_active': entity.is_active,
            "enable_unlimited_devices": entity.enable_unlimited_devices,
            "href": entity.api_url,
        }
        return expected

    # def test_format_response(self):
    #     schema = load_json_schema("auth", "v2/client.list")
    #
    #     validate_scoop_jsonschema(json.loads(self.response.data), schema)
