from __future__ import unicode_literals, absolute_import
import httplib
import json
import smtplib
from unittest import TestCase

from mock import patch

from app import db, app
from app.auth import helpers
from app.constants import RoleType
from app.users.users import User
from tests.fixtures.helpers import generate_static_sql_fixtures, create_user_and_token
from app.users.tests.fixtures import OrganizationFactory


def mock_send_email_success(message):
    return None


def mock_send_email_failed(message):
    raise smtplib.SMTPException()


class GetTokenTests(TestCase):

    maxDiff = None
    request_url = "/v1/auth/reset-password"
    request_headers = {
        'Accept': "application/json",
        'Accept-Charset': 'utf-8',
        'X-Scoop-Client': 'scoop web/1.0.0',
        'User-Agent': 'scoop web/1.0.0'
    }
    client = app.test_client(use_cookies=False)

    @patch('app.messaging.email.SmtpServer.send_message', side_effect=mock_send_email_success)
    def test_all_is_well(self, mock_send_email):
        request_body = {
            'username': USERNAME,
        }
        response = self.client.post(
            self.request_url,
            data=json.dumps(request_body),
            headers=self.request_headers)
        self.assertEqual(response.status_code, httplib.OK)
        user = User.query.get(USER_ID)
        token = user.reset_password_token
        self.assertIsNotNone(token)
        with app.test_request_context('/'):
            decoded_token = helpers.jwt_decode(token)
            self.assertEqual(decoded_token.get('sub'), 'reset password')
            self.assertEqual(decoded_token.get('user_id'), user_test.id)

    @patch('app.messaging.email.SmtpServer.send_message', side_effect=mock_send_email_success)
    def test_get_token_with_email(self, mock_send_email):
        request_body = {
            'username': EMAIL,
        }
        response = self.client.post(
            self.request_url,
            data=json.dumps(request_body),
            headers=self.request_headers)
        self.assertEqual(response.status_code, httplib.OK)
        user = User.query.get(USER_ID)
        token = user.reset_password_token
        self.assertIsNotNone(token)
        with app.test_request_context('/'):
            decoded_token = helpers.jwt_decode(token)
            self.assertEqual(decoded_token.get('sub'), 'reset password')
            self.assertEqual(decoded_token.get('user_id'), user_test.id)

    @patch('app.messaging.email.SmtpServer.send_message', side_effect=mock_send_email_failed)
    def test_send_email_failed(self, mock_send_email):
        request_body = {
            'username': USERNAME,
        }
        response = self.client.post(
            self.request_url,
            data=json.dumps(request_body),
            headers=self.request_headers)
        self.assertEqual(response.status_code, httplib.INTERNAL_SERVER_ERROR)

    def test_username_not_defined(self):
        """username is required, we tests if its not defined correctly"""
        request_body = {
            'username-not-defined': USERNAME,
        }
        response = self.client.post(
            self.request_url,
            data=json.dumps(request_body),
            headers=self.request_headers)
        self.assertEqual(response.status_code, httplib.BAD_REQUEST)

    def test_username_not_found(self):
        request_body = {
            'username': 'invalid-user-name',
        }
        response = self.client.post(
            self.request_url,
            data=json.dumps(request_body),
            headers=self.request_headers)
        self.assertEqual(response.status_code, httplib.BAD_REQUEST)


class ResetPasswordApiTests(TestCase):

    maxDiff = None
    request_url = "/v1/auth/reset-password"
    request_headers = {
        'Accept': "application/json",
        'Accept-Charset': 'utf-8',
        'X-Scoop-Client': 'scoop web/1.0.0',
        'User-Agent': 'scoop web/1.0.0'
    }
    client = app.test_client(use_cookies=False)

    def test_all_is_well(self):
        with app.test_request_context('/'):
            session = db.session()
            user = session.query(User).get(USER_ID)
            # get valid token for password reset
            token = helpers.get_reset_password_token(user)
            new_password = 'new_password'
            request_body = {
                'reset_password_token': token,
                'new_password': new_password
            }
            # reset password via put method
            response = self.client.put(
                self.request_url,
                data=json.dumps(request_body),
                headers=self.request_headers)
            self.assertEqual(response.status_code, httplib.NO_CONTENT)

            # check database, new password should be updated
            user = session.query(User).get(USER_ID)
            self.assertEqual(user.password, user.hash_password(new_password))

    def test_invalid_token(self):
        with app.test_request_context('/'):
            request_body = {
                'reset_password_token': 'wrongtoken.dfsafsaf.dfsafsdfas',
                'new_password': 'dfkslfjslfjsf'
            }
            # reset password via put method
            response = self.client.put(
                self.request_url,
                data=json.dumps(request_body),
                headers=self.request_headers)
            self.assertEqual(response.status_code, httplib.UNAUTHORIZED)


USER_ID = 112233
USERNAME = 'test-reset'
EMAIL = 'test-reset@email.com'
user_test = None


def setup_module():
    db.drop_all()
    db.create_all()
    session = db.session()
    generate_static_sql_fixtures(session)
    org = OrganizationFactory(id=1023)
    global user_test
    user_test, token = create_user_and_token(
        session,
        RoleType.super_admin,
        id = USER_ID,
        username=USERNAME,
        email=EMAIL,
        organizations=[org, ])
    # password = 'we4scoop'
    session.commit()


def teardown_module():
    db.session().close_all()
    db.drop_all()
