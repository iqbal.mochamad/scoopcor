from __future__ import unicode_literals, absolute_import

import random

from nose.tools import istest

from app import db
from app.auth.models import Client, Application, ClientVersion
from app.master.models import Platform
from app.utils.testcases import CrudBase
from tests.fixtures.sqlalchemy.users import ClientFactory, ClientVersionFactory


@istest
class ClientCrudTests(CrudBase):

    request_url = '/v1/clients'
    fixture_factory = ClientFactory
    model = Client
    list_key = 'clients'

    @classmethod
    def setUpClass(cls):
        super(ClientCrudTests, cls).setUpClass()
        db.engine.execute("select setval('cas_clients_id_seq', 100);")

    def set_up_fixtures(self):
        entity = super(ClientCrudTests, self).set_up_fixtures()
        version = ClientVersionFactory(client=entity)
        self.session.add(version)
        self.session.commit()
        self.session.expunge(entity)
        return entity

    def get_request_body(self):
        platform_id = random.choice([1, 2, 4])
        platform = self.session.query(Platform).get(platform_id)
        application = self.session.query(Application).get(random.choice([1, 2]))
        request_body = {
            "name": self.fake.name(),
            # "slug": self.fake.pystr(),
            "app_name": self.fake.name(),
            "redirect_url": self.fake.url(),
            # "client_type": self.fake.pystr(),
            # "flurry_key": self.fake.pystr(),
            "gai_key": self.fake.pystr(),
            "version": self.fake.pystr(),
            "lowest_supported_version": self.fake.pystr(),
            "lowest_os_version": self.fake.pystr(),
            # "scopes": [],
            # "user_id": self.fake.pyint(),
            "description": self.fake.bs(),
            "external_bundle_name": self.fake.pystr(),
            "external_identifier": self.fake.pyint(),
            "url": self.fake.url(),

            # "ip_address": self.fake.pystr(),
            # "meta": self.fake.pystr(),
            "product_id": self.fake.pystr(),
            "allow_age_restricted_content": self.fake.pybool(),
            "platform": {
                "id": platform.id,
                "name": platform.name,
            },
            "application": {
                "id": application.id,
                "name": application.name,
            },
            "firebase_server_key": self.fake.pystr(),
            'is_active': self.fake.pybool(),
            # 'start_date': isoformat(get_local() - timedelta(days=2)),
            # 'end_date': isoformat(get_local() + timedelta(days=3)),
            # 'total_cost': self.fake.pyfloat(left_digits=8, right_digits=4),
        }
        return request_body

    def get_expected_response(self, entity):
        versions = entity.versions.filter_by(is_active=True).all()
        expected = {
            "id": entity.id,
            "name": entity.name,
            "description": entity.description,
            'is_active': entity.is_active,
            "slug": entity.slug,
            "app_name": entity.app_name,
            "redirect_url": entity.redirect_url,
            "client_type": entity.client_type,
            "flurry_key": entity.flurry_key,
            "gai_key": entity.gai_key,
            "version": entity.version,
            "lowest_supported_version": entity.lowest_supported_version,
            "lowest_os_version": entity.lowest_os_version,
            # "scopes": entity.scopes,
            # "user_id": entity.user_id,
            "external_bundle_name": entity.external_bundle_name,
            "external_identifier": entity.external_identifier,
            "url": entity.url,

            # "ip_address": entity.ip_address,
            # "meta": entity.meta,
            "product_id": entity.product_id,
            "allow_age_restricted_content": entity.allow_age_restricted_content,
            "platform": {
                "id": entity.platform.id,
                "name": entity.platform.name,
            } if entity.platform else None,
            "application": {
                "id": entity.application.id,
                "name": entity.application.name,
            } if entity.application else None,
            "firebase_server_key": entity.firebase_server_key,
            "client_versions": [{
                "id": v.id,
                "version": v.version,
                "is_active": v.is_active,
                "enable_scoop_point": v.enable_scoop_point,
                "enable_referral": v.enable_referral,
            } for v in versions] if versions else None,
        }
        return expected

    # def test_format_response(self):
    #     schema = load_json_schema("auth", "v2/client.list")
    #
    #     validate_scoop_jsonschema(json.loads(self.response.data), schema)

    # def test_update_again(self):
    #     entity_to_update = self.set_up_fixtures()
    #     with self.on_session(entity_to_update):
    #         self.set_api_authorized_user(self.super_admin)
    #
    #         # execute api and get response
    #         request_body = {
    #             "allow_age_restricted_content": True, "app_name": "client_name_89_baru_ios",
    #             "application": {"href": "http://localhost:8080/scoopcor/api/v1/applications/1", "id": 1,
    #                             "name": "SCOOP mobile apps"}, "client_type": "",
    #             "created": "2017-06-19T06:11:39.709569+00:00",
    #             "description": "dklsjflskda fsf\ndsfdlskfjsl f\n\ndklfajsdlkfs\n89",
    #             "external_bundle_name": "bundle_name_bar89", "external_identifier": None,
    #             "firebase_server_key": None, "flurry_key": "", "gai_key": "123123.123123.89",
    #             "href": "http://localhost:8080/scoopcor/api/v1/clients/89", "id": 89, "ip_address": "",
    #             "is_active": True, "lowest_os_version": "8.7.6-89", "lowest_supported_version": "0.0.2",
    #             "meta": "", "modified": "2017-06-19T06:11:39.716967+00:00", "name": "client name 89 baru ios",
    #             "organizations": [], "platform": {"id": 1, "name": "iOS"}, "product_id": "com.name_bar.app-89",
    #             "redirect_url": "http://utl-dff/kkk-89", "scopes": None, "slug": "client-name-3-baru-ios",
    #             "url": "http://utl-dff-89", "user_id": None, "version": "0.0.3", "versions": []}
    #         # request_body = self.get_request_body()
    #         response = self.api_client.put(
    #             '{base}/{id}'.format(base=self.request_url, id=entity_to_update.id),
    #             data=json.dumps(request_body),
    #             headers=self.build_headers(has_body=True))
    #
    #         self.assert_status_code(response, OK)
    #
    #         request_body.pop('slug')
    #         request_body.pop('created')
    #         request_body.pop('modified')
    #         # reload saved/updated data
    #         self.session.refresh(entity_to_update)
    #         # assert response body vs expected response
    #         expected_response = self.get_expected_response(entity_to_update)
    #         response_json = self._get_response_json(response)
    #         self.assert_response_body(response_json, expected_response, entity_to_update,
    #                                   msg="\nFail on Test Update API."
    #                                       "\nExpected {expected}\nActual {actual}\n".format(
    #                                         expected=expected_response, actual=response_json
    #                                         ))
    #         # assert saved data vs request body
    #         self.assert_saved_data(entity_to_update, request_body)


@istest
class ClientVersionCrudTests(CrudBase):

    request_url = '/v1/client-versions'
    fixture_factory = ClientVersionFactory
    model = ClientVersion
    list_key = 'client_versions'

    @classmethod
    def setUpClass(cls):
        super(ClientVersionCrudTests, cls).setUpClass()
        db.engine.execute("select setval('cas_clients_id_seq', 200);")
        cls.client_id = 1123
        cls.client = ClientFactory(id=cls.client_id, client_id=cls.client_id, is_active=True)
        cls.session.add(cls.client)
        cls.session.commit()
        cls.session.expunge(cls.client)

    def get_request_body(self):
        with self.on_session(self.client):
            request_body = {
                "version": self.fake.ean(length=8),
                'enable_scoop_point': self.fake.pybool(),
                'enable_referral': self.fake.pybool(),
                # "client_id": self.client_id,
                "client": {
                    "id": self.client_id,
                    "name": self.client.name
                },
            }
        return request_body

    def get_expected_response(self, entity):
        expected = {
            "id": entity.id,
            "version": entity.version,
            'is_active': entity.is_active,
            "enable_scoop_point": entity.enable_scoop_point,
            "enable_referral": entity.enable_referral,
            "client": {
                "id": entity.client.id,
                "name": entity.client.name,
                "app_name": entity.client.app_name
            } if entity.client else None,
        }
        return expected
