from __future__ import unicode_literals, absolute_import
import json
from httplib import UNAUTHORIZED, BAD_REQUEST, OK

from datetime import timedelta

from app import db, app
from app.auth.helpers import jwt_encode_from_user, jwt_decode, jwt_encode
from app.auth.models import Client
from app.auth.user_devices import UserDevices, UserDeviceData
from app.constants import RoleType
from app.utils.datetimes import get_utc_nieve
from tests.fixtures.helpers import generate_static_sql_fixtures, create_user_and_token
from app.users.tests.fixtures import OrganizationFactory
from tests import testcases


class RefreshTokenTests(testcases.DetailTestCase):

    maxDiff = None
    request_url = "/v1/auth/refresh-token"
    # fixture_factory = LayoutBannerFactory

    request_headers = {
        'Accept': "application/json",
        'Accept-Charset': 'utf-8',
        'X-Scoop-Client': 'scoop ios/1.0.0'
    }
    device_id = 'testdevice213'

    @classmethod
    def set_up_fixtures(cls):
        session = db.session()
        generate_static_sql_fixtures(session)
        org = OrganizationFactory(id=1023)
        cls.user, cls.token = create_user_and_token(session, RoleType.super_admin,
                                                    organizations=[org, ])
        cls.password = 'we4scoop'

        # # add device to user device data (it should be added in user login process automatically)
        # #   here we only test refresh api, as if login already execute previously
        # app_id = 1
        # application = Application(id=app_id, max_devices=5,
        #                           enable_unlimited_devices=True)
        client = Client.query.filter(Client.name.ilike('%scoop%ios%')).first()
        # client.application = application
        # session.add(client)
        # session.commit()
        with app.test_request_context('/'):
            data = UserDeviceData(client_id=client.id, device_id=cls.device_id,
                                  application_id=client.application.id)
            UserDevices(cls.user.id).save(data)
        return cls.user

    @classmethod
    def remove_fixtures_from_session(cls):
        pass

    @classmethod
    def get_api_response(cls):
        with app.test_request_context('/'):
            cls.request_headers['Authorization'] = 'JWT {}'.format(
                jwt_encode_from_user(cls.user, device_id='testdevice213'))
            return cls.client.get(cls.request_url,
                                  headers=cls.request_headers)

    def test_response_body(self):
        self.assert_response_body_expectation(self.response)

    def assert_response_body_expectation(self, response):
        with app.test_request_context('/'):
            actual = json.loads(response.get_data())
            # we can't directly validate token in dict because of time differents, we decode it later to validate it
            token = actual.get('token', None)
            self.assertIsNotNone(token)
            self.assertEqual(actual.get('realm', None), 'JWT')
            payload = jwt_decode(token)
            self.assertEqual(self.user.id, payload.get('user_id'))
            self.assertEqual(self.user.username, payload.get('user_name'))
            self.assertEqual(self.user.email, payload.get('email'))

    def test_bad_token(self):
        self.request_headers['Authorization'] = 'JWT fkjdsalfjdsalfsafslkjSAdf12321213123'
        response = self.client.get(self.request_url,
                                   headers=self.request_headers)
        self.assertEqual(response.status_code, UNAUTHORIZED, response.data)

    def test_expired_token(self):
        with app.test_request_context('/'):
            # generate expired token
            payload = {
                'user_id': self.user.id,
                'user_name': self.user.username,
                'email': self.user.email,
                'exp': get_utc_nieve() - timedelta(days=10),
                'device_id': self.device_id
            }
            encoded_token = jwt_encode(payload)
            self.request_headers['Authorization'] = 'JWT {}'.format(encoded_token)
            # test refresh token api for this expired token
            response = self.client.get(self.request_url,
                                       headers=self.request_headers)
            self.assertEqual(response.status_code, UNAUTHORIZED)

    def test_refreshed_token_with_no_expiry_defined(self):
        with app.test_request_context('/'):
            # generate almost expired token
            payload = {
                'user_id': self.user.id,
                'user_name': self.user.username,
                'email': self.user.email,
                'device_id': self.device_id
            }
            encoded_token = jwt_encode(payload)
            self.request_headers['Authorization'] = 'JWT {}'.format(encoded_token)
            # get new refreshed token api
            response = self.client.get(self.request_url,
                                       headers=self.request_headers)
            self.assertEqual(response.status_code, OK, response.data)

    def test_refreshed_token(self):
        with app.test_request_context('/'):
            # generate almost expired token
            payload = {
                'user_id': self.user.id,
                'user_name': self.user.username,
                'email': self.user.email,
                'exp': get_utc_nieve() + timedelta(seconds=30)
            }
            encoded_token = jwt_encode(payload)
            self.request_headers['Authorization'] = 'JWT {}'.format(encoded_token)
            # get new refreshed token api
            response = self.client.get(self.request_url,
                                       headers=self.request_headers)
            self.assertEqual(response.status_code, OK, response.data)
            data = json.loads(response.data)
            new_token = data.get('token', None)
            self.request_headers['Authorization'] = 'JWT {}'.format(encoded_token)
            # test new token api again
            new_response = self.client.get(self.request_url,
                                           headers=self.request_headers)
            self.assertEqual(new_response.status_code, OK, new_response.data)

    def test_bad_header(self):
        self.request_headers.pop('Authorization')
        response = self.client.get(self.request_url,
                                   headers=self.request_headers)
        self.assertEqual(response.status_code, BAD_REQUEST)

    def test_device_in_token_already_removed(self):
        with app.test_request_context('/'):
            headers = self.request_headers.copy()
            headers['X-Scoop-Client'] = 'scoop ios/1.0.0'
            # login with token that contain invalid device
            # (device id not registered in login process or already revoked/removed)
            headers['Authorization'] = 'JWT {}'.format(
                jwt_encode_from_user(self.user, device_id='invalid_device_id_12345'))
            response = self.client.get(self.request_url,
                                       headers=headers)
            self.assertEqual(response.status_code, UNAUTHORIZED, response.data)

    def test_device_in_token_still_valid(self):
        with app.test_request_context('/'):
            headers = self.request_headers.copy()
            headers['X-Scoop-Client'] = 'scoop ios/1.0.0'
            # login with token that contain valid device id
            # (device id still exists in user device data that stored in redis cache)
            headers['Authorization'] = 'JWT {}'.format(
                jwt_encode_from_user(self.user, device_id=self.device_id))
            response = self.client.get(self.request_url,
                                       headers=headers)
            self.assertEqual(response.status_code, OK, response.data)


def setup_module():
    db.drop_all()
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
