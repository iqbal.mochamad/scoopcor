import json
import uuid

from flask import url_for

from app.auth.helpers import jwt_decode
from app.constants import RoleType
from app.users.users import User
from app.utils.testcases import TestBase


class UserLoginGuestTests(TestBase):

    request_url = "/v1/auth/login-guest"

    @classmethod
    def setUpClass(cls):
        super(UserLoginGuestTests, cls).setUpClass()

        headers = cls.build_headers(has_body=True)
        headers['X-Scoop-Client'] = 'scoop ios/1.0.0'
        cls.username = str(uuid.uuid4()).replace('-', '')
        cls.device_id = 'dummy-device-id-12'
        request_body = {
            'unique_id': cls.username,
            'device_id': cls.device_id
        }
        cls.response = cls.api_client.post(
            cls.request_url, data=json.dumps(request_body), headers=headers)

    def test_response_status_code(self):
        self.assert_status_code(self.response, 200)

    def test_response_body(self):
        user = self.session.query(User).filter(
            User.username.ilike('%{}%'.format(self.username))).first()
        self.assert_response_body_expectation(self.response, user)

        # assert saved user data
        self.assertEqual(user.origin_client_id, 1)
        self.assertEqual(user.username, '{}@guest.scoop'.format(self.username))
        self.assertEqual(user.email, '{}@guest.scoop'.format(self.username))
        self.assertIn(RoleType.guest_user.value, [r.id for r in user.roles])

    def assert_response_body_expectation(self, response, user=None):

        # we can't directly validate token in dict because of time differents, we decode it later to validate it
        actual = json.loads(response.get_data())
        token = actual.pop('token')

        payload = jwt_decode(token)
        expected = {
            'id': user.id,
            'href': url_for('users.details', user=user, _external=True),
            'realm': 'JWT',
            'first_name': user.first_name,
            'last_name': user.last_name,
            'email': user.email,
            'is_verified': True,
            'expire_timedelta': payload.get('expire_timedelta', None),
            'expires': payload.get('exp', None),
        }
        self.assertDictContainsSubset(expected, actual)
        # validate token payload
        self.assertEqual(user.id, payload.get('user_id'))
        self.assertEqual(self.device_id, payload.get('device_id'))
        self.assertEqual(user.username, payload.get('user_name'))
        self.assertEqual(user.email, payload.get('email'))
