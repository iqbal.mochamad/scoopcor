from __future__ import unicode_literals, absolute_import

import json
import os
from httplib import UNAUTHORIZED
from unittest import TestCase

from datetime import timedelta
from faker import Factory
from nose.tools import istest, nottest

from app import db, app
from app.auth.helpers import jwt_encode
from app.constants import RoleType
from app.utils.datetimes import get_utc_nieve
from tests.fixtures.helpers import create_user_and_token, generate_static_sql_fixtures


USER_ID = 12345


def get_jwt_valid_token(expired=None):
    with app.test_request_context('/'):
        if not expired:
            expired = get_utc_nieve() + timedelta(minutes=10)
        return jwt_encode(payload={"user_id": USER_ID, "exp": expired})


@istest
class JwtTokenExpiredTests(TestCase):
    """ Test access end point list api with expired jwt token """

    maxDiff = None
    request_url = "/v1/items"
    client = app.test_client(use_cookies=False)

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'X-Scoop-Client': 'scoop web/1.0.0',
        'Authorization': 'JWT {}'.format(
            get_jwt_valid_token(expired=get_utc_nieve() - timedelta(days=110)))
    }

    @classmethod
    def setUpClass(cls):
        super(JwtTokenExpiredTests, cls).setUpClass()
        cls.response = cls.client.get(cls.request_url, headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(UNAUTHORIZED, self.response.status_code)

    def test_response_body(self):
        # error raised on before request.. so it will handled in "after request func", user_message=developer_message
        expected = {
            'status': 401,
            'error_code': 401,
            'user_message': "Your session/token has expired",
            'developer_message': "Your session/token has expired"
        }
        actual = json.loads(self.response.data)
        self.assertDictEqual(expected, actual)


@istest
class JwtTokenExpiredOnRefreshTokenTests(TestCase):
    """ Test access end point list api with expired jwt token """

    maxDiff = None
    request_url = "/v1/auth/refresh-token"
    client = app.test_client(use_cookies=False)

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'X-Scoop-Client': 'scoop web/1.0.0',
        'Authorization': 'JWT {}'.format(
            get_jwt_valid_token(expired=get_utc_nieve() - timedelta(days=100)))
    }

    @classmethod
    def setUpClass(cls):
        super(JwtTokenExpiredOnRefreshTokenTests, cls).setUpClass()
        cls.response = cls.client.get(cls.request_url, headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(UNAUTHORIZED, self.response.status_code)

    def test_response_body(self):
        # CorApiErrorHandlingMixin implemented, latest correct errror generated, user_message != developer_message
        expected = {
            'status': 401,
            'error_code': 401,
            'user_message': "Your session has expired",
            'developer_message': "Token expired"
        }
        actual = json.loads(self.response.data)
        self.assertEqual(expected['status'], actual.get('status', None))
        self.assertEqual(expected['error_code'], actual.get('error_code', None))
        self.assertEqual(expected['user_message'], actual.get('user_message', None))


@istest
class JwtTokenInvalidOnOldApiTests(TestCase):
    """ Test access end point list api with expired jwt token """

    maxDiff = None
    request_url = "/v1/items"
    client = app.test_client(use_cookies=False)

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'X-Scoop-Client': 'scoop web/1.0.0',
        'Authorization': 'JWT absdfsfdsfsdf'
    }

    @classmethod
    def setUpClass(cls):
        super(JwtTokenInvalidOnOldApiTests, cls).setUpClass()
        cls.response = cls.client.get(cls.request_url, headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(UNAUTHORIZED, self.response.status_code)

    def test_response_body(self):
        # old api: error raised on before request
        # so it will handled in "after request func", user_message=developer_message
        expected = {
            'status': 401,
            'error_code': 401,
            'user_message': "Token invalid",
            'developer_message': "Token invalid"
        }
        actual = json.loads(self.response.data)
        self.assertDictEqual(expected, actual)


@istest
class JwtTokenInvalid(TestCase):
    """ Test access end point list api with expired jwt token """

    maxDiff = None
    request_url = "/v1/auth/refresh-token"
    client = app.test_client(use_cookies=False)

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'X-Scoop-Client': 'scoop web/1.0.0',
        'Authorization': 'JWT dsjlkfjsdflsdlkfsfkljsaf'
    }

    @classmethod
    def setUpClass(cls):
        super(JwtTokenInvalid, cls).setUpClass()
        cls.response = cls.client.get(cls.request_url, headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(UNAUTHORIZED, self.response.status_code)

    def test_response_body(self):
        # CorApiErrorHandlingMixin implemented, latest correct errror generated, user_message != developer_message
        expected = {
            'status': 401,
            'error_code': 401,
            'user_message': "Token invalid",
            'developer_message': "Token invalid, decode error: Not enough segments"
        }
        actual = json.loads(self.response.data)
        self.assertEqual(expected['status'], actual.get('status', None))
        self.assertEqual(expected['error_code'], actual.get('error_code', None))
        self.assertEqual(expected['user_message'], actual.get('user_message', None))


_fake = Factory.create()
ENV_TESTING = os.environ.get('TESTING', None)


def setup_module():
    db.drop_all()
    db.create_all()
    # remove testing from environment so we can test the method
    os.environ.pop('TESTING', None)
    session = db.session()
    generate_static_sql_fixtures(session)
    user, token = create_user_and_token(
        session, RoleType.internal_admin,
        id=USER_ID
    )
    session.commit()


def teardown_module():
    # restore the testing value in environment
    if ENV_TESTING:
        os.environ['TESTING'] = ENV_TESTING

    db.session().close_all()
    db.drop_all()
