import json

from app.auth.user_devices import UserDevices, UserDeviceData
from app.utils.testcases import TestBase


class CurrentDevicesTests(TestBase):
    url = '/v1/auth/current-devices'

    @classmethod
    def setUpClass(cls):
        super(CurrentDevicesTests, cls).setUpClass()
        # create some user devices data
        user_devices = UserDevices(cls.regular_user.id)
        # ios devices
        count = 1
        while count <= 4:
            user_devices.save(
                data=UserDeviceData(client_id=1, device_id='my_ios_device::{}'.format(count),
                                    application_id=1))
            count += 1
        # eperpus devices
        count = 1
        while count <= 5:
            user_devices.save(
                data=UserDeviceData(client_id=85, device_id='my_device::{}'.format(count),
                                    application_id=3))
            count += 1

    def test_current_devices(self):
        with self.on_session(self.super_admin):
            self.set_api_authorized_user(self.super_admin)

            resp = self.api_client.get(self.url, headers=self.build_headers())
            self.assert_status_code(resp, 200)
            actual = json.loads(resp.data)
            # no device registered, content 0 application data
            self.assertEqual(len(actual), 0)

        with self.on_session(self.regular_user):
            self.set_api_authorized_user(self.regular_user)
            resp = self.api_client.get(self.url, headers=self.build_headers())
            self._assert_user_devices_response(resp)

    def test_current_devices_by_user_id(self):
        with self.on_session(self.super_admin, self.regular_user):
            # set JWT Token to super admin
            self.set_api_authorized_user(self.super_admin)
            # query devices for user "regular user"
            resp = self.api_client.get('{}/{}'.format(self.url, self.regular_user.id), headers=self.build_headers())
            self._assert_user_devices_response(resp)

    def _assert_user_devices_response(self, resp):
            self.assert_status_code(resp, 200)
            actual = json.loads(resp.data)
            # content 2 application data
            self.assertEqual(len(actual), 2)
            ios_devices = filter(lambda e: e['application_id'] == 1, actual)
            self.assertGreater(len(ios_devices), 0)
            self.assertEqual(ios_devices[0].get("device_count"), 4)
            self.assertEqual(len(ios_devices[0].get("devices")), 4)
            eperpus_devices = filter(lambda e: e['application_id'] == 3, actual)
            self.assertGreater(len(eperpus_devices), 0)
            self.assertEqual(eperpus_devices[0].get("device_count"), 5)
            self.assertEqual(len(eperpus_devices[0].get("devices")), 5)
