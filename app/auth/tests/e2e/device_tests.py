from httplib import CREATED

from flask import json

from app.utils.testcases import TestBase


class DeviceTests(TestBase):

    def test_post(self):
        request_body = {
            "device_imei": "358860060103228",
            "device_mid": "24edfd99accad16",
            "device_model": "EVERCOSS A65"
        }
        response = self.api_client.post(
            '/v1/auth/device',
            data=json.dumps(request_body),
            headers=self.build_headers(has_body=True))

        self.assertEqual(response.status_code, CREATED, response.data)
        actual = json.loads(response.data)
        self.assertIsNotNone(actual.get('unique_id'))
        self.assertDictContainsSubset(request_body, actual)

    def test_post_ios(self):
        request_body = {
            "device_imei": None,
            "device_mid": None,
            "device_model": None
        }
        response = self.api_client.post(
            '/v1/auth/device',
            data=json.dumps(request_body),
            headers=self.build_headers(has_body=True))

        self.assertEqual(response.status_code, CREATED, response.data)
        actual = json.loads(response.data)
        self.assertIsNotNone(actual.get('unique_id'))
        self.assertDictContainsSubset(request_body, actual)

        # register again from another ios
        request_body = {
            "device_imei": None,
            "device_mid": None,
            "device_model": None
        }
        response = self.api_client.post(
            '/v1/auth/device',
            data=json.dumps(request_body),
            headers=self.build_headers(has_body=True))

        self.assertEqual(response.status_code, CREATED, response.data)
        actual = json.loads(response.data)
        self.assertIsNotNone(actual.get('unique_id'))
        self.assertDictContainsSubset(request_body, actual)
