from __future__ import unicode_literals, absolute_import

import json
import os
from base64 import b64encode
from httplib import UNAUTHORIZED, OK
from unittest import TestCase

from faker import Factory
from flask import url_for, current_app
from sqlalchemy import desc

from app import db, app
from app.auth.helpers import jwt_encode
from app.constants import RoleType
from app.layouts.layout_type import LayoutTypes
from app.layouts.models import LayoutControl
from app.auth.models import Token
from tests.fixtures.helpers import create_user_and_token, generate_static_sql_fixtures
from tests.fixtures.sqlalchemy.layouts import LayoutFactory
from tests import testcases


USER_ID = 12345


def get_jwt_valid_token():
    with app.test_request_context('/'):
        return jwt_encode(payload={"user_id": USER_ID})


class ListWithValidJwtTokenTests(testcases.ListTestCase):
    maxDiff = None
    request_url = "/v1/banners"
    list_key = 'banners'
    fixture_factory = LayoutFactory

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'Authorization': 'JWT {}'.format(get_jwt_valid_token())
    }

    @classmethod
    def set_up_fixtures(cls):
        session = db.session()
        session.expire_on_commit = False
        cls.init_layout_data(session)
        layouts = session.query(LayoutControl).order_by(desc(LayoutControl.sort_priority)).all()
        return layouts

    @classmethod
    def init_layout_data(cls, session):
        layout = LayoutFactory(id=1000, layout_type=LayoutTypes.latest_book, sort_priority=20)
        layout2 = LayoutFactory(id=1002, layout_type=LayoutTypes.popular_all, sort_priority=10)
        session.commit()

    def test_response_body(self):
        response_json = json.loads(self.response.data)
        i = 0
        # make sure the priority is the same in response (sort by sort_priority desc)
        for entity_json in response_json[self.list_key]:
            entity = self.fixtures[i]
            self.assert_entity_detail_expectation(entity, entity_json)
            i += 1

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        with app.test_request_context('/'):
            expected = {
                'id': fixture.id,
                'title': fixture.name,
                'layout_type': fixture.layout_type.value,
                'href': url_for('layouts.details', db_id=fixture.id, _external=True)
            }
        self.assertDictEqual(fixture_json, expected)


class ListWithAnonymousUserTests(TestCase):

    maxDiff = None
    request_url = "/v1/banners"
    client = app.test_client(use_cookies=False)

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'Authorization': 'JWT wrongtokeninvalidtoken'
    }

    @classmethod
    def setUpClass(cls):
        super(ListWithAnonymousUserTests, cls).setUpClass()
        cls.response = cls.client.get(cls.request_url, headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(UNAUTHORIZED, self.response.status_code)


class ListWithValidBearerTests(TestCase):
    """ Test access end point list api with valid Bearer token """

    maxDiff = None
    request_url = "/v1/banners"
    client = app.test_client(use_cookies=False)

    @classmethod
    def setUpClass(cls):
        s = db.session()
        token = s.query(Token).filter_by(user_id=USER_ID).first().token
        s.close_all()
        request_headers = {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'Cache-Control': 'no-cache',
            'Authorization': 'Bearer {}'.format(b64encode('{}:{}'.format(
                USER_ID, token
            )))
        }
        super(ListWithValidBearerTests, cls).setUpClass()
        cls.response = cls.client.get(cls.request_url, headers=request_headers)

    def test_response_code(self):
        self.assertEqual(OK, self.response.status_code)


class ListWithInvalidBearerTests(TestCase):
    """ Test access end point list api with invalid Bearer token """

    maxDiff = None
    request_url = "/v1/banners"
    client = app.test_client(use_cookies=False)

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Cache-Control': 'no-cache',
        'Authorization': 'Bearer {}'.format(b64encode('{}:{}'.format(
            USER_ID, 'wrongbearertoken'
        )))
    }

    @classmethod
    def setUpClass(cls):
        super(ListWithInvalidBearerTests, cls).setUpClass()
        cls.response = cls.client.get(cls.request_url, headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(UNAUTHORIZED, self.response.status_code)


_fake = Factory.create()
ENV_TESTING = os.environ.get('TESTING', None)


def setup_module():
    db.drop_all()
    db.create_all()
    # remove testing from environment so we can test the method
    os.environ.pop('TESTING', None)
    app.testing = False
    session = db.session()
    generate_static_sql_fixtures(session)
    user, token = create_user_and_token(
        session, RoleType.internal_admin,
        id=USER_ID
    )
    session.commit()


def teardown_module():
    # restore the testing value in environment
    if ENV_TESTING:
        os.environ['TESTING'] = ENV_TESTING
    app.testing = ENV_TESTING
    db.session().close_all()
    db.drop_all()
