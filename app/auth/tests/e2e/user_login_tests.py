from __future__ import unicode_literals, absolute_import
import json
import random
from httplib import UNAUTHORIZED, BAD_REQUEST, OK, NO_CONTENT, FORBIDDEN

from flask import url_for
from nose.tools import istest

from app import db, app
from app.auth.helpers import jwt_decode, prehash_user_password
from app.auth.models import Client
from app.auth.user_devices import UserDevices, UserDeviceData
from app.eperpus.constants import PUBLIC_LIBRARY_CLIENT_ID
from app.eperpus.tests.fixtures import SharedLibraryFactory
from app.users.users import APPS_FOUNDRY_ORG, User
from app.constants import RoleType
from tests.fixtures.helpers import generate_static_sql_fixtures, create_user_and_token
from app.users.tests.fixtures import OrganizationFactory
from tests import testcases


class LoginTests(testcases.DetailTestCase):

    maxDiff = None
    request_url = "/v1/auth/login"
    # fixture_factory = LayoutBannerFactory
    request_headers = {
        'Content-Type': "application/json",
        'Accept': "application/json",
        'Accept-Charset': 'utf-8',
        'X-Scoop-Client': 'scoop web/1.0.0'
    }

    @classmethod
    def set_up_fixtures(cls):
        session = db.session()
        generate_static_sql_fixtures(session)
        org = OrganizationFactory(id=1023)
        org_foundry = OrganizationFactory(id=APPS_FOUNDRY_ORG)
        cls.user, cls.token = create_user_and_token(
            session, RoleType.super_admin,
            organizations=[org, ],
            is_verified=True, is_active=True)
        cls.user_apps_foundry, cls.token = create_user_and_token(
            session, RoleType.super_admin, organizations=[org_foundry, ])
        cls.user_unverified, cls.token_unverified = create_user_and_token(
            session, RoleType.unverified_user,
            organizations=[org, ],
            is_verified=False, is_active=True)
        cls.password = 'we4scoop'

        session.commit()
        with app.test_request_context('/'):
            # clean up cached user device data
            UserDevices(user_id=cls.user.id).clear_cache()

        return cls.user

    @classmethod
    def remove_fixtures_from_session(cls):
        pass

    @classmethod
    def get_api_response(cls):
        request_body = {
            'username': cls.user.username,
            'password': cls.password,
            'device_id': 'dummy-device-id-12\n335\n'
        }
        return cls.client.post(cls.request_url,
                               data=json.dumps(request_body),
                               headers=cls.request_headers)

    def test_response_body(self):
        pass

    def test_login_with_username(self):
        self.assert_response_body_expectation(self.response)

    def assert_response_body_expectation(self, response, user=None):
        with app.test_request_context('/'):

            user = user or self.user

            # we can't directly validate token in dict because of time differents, we decode it later to validate it
            actual = json.loads(response.get_data())
            token = actual.pop('token')

            payload = jwt_decode(token)
            expected = {
                'id': user.id,
                'href': url_for('users.details', user=user, _external=True),
                'realm': 'JWT',
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                'is_verified': True,
                'expire_timedelta': payload.get('expire_timedelta', None),
                'expires': payload.get('exp', None),
            }
            self.assertDictContainsSubset(expected, actual)
            self.assertEqual(user.id, payload.get('user_id'))
            self.assertEqual(user.username, payload.get('user_name'))
            self.assertEqual(user.email, payload.get('email'))

    def test_login_with_prehased_password(self):
        request_body = {
            'username': self.user.username,
            'password': prehash_user_password(self.password)
        }
        response = self.client.post(self.request_url,
                                    data=json.dumps(request_body),
                                    headers=self.request_headers)
        self.assert_response_body_expectation(response)

    def test_login_with_email(self):
        request_body = {
            'username': self.user.email,
            'password': self.password
        }
        response = self.client.post(self.request_url,
                                    data=json.dumps(request_body),
                                    headers=self.request_headers)
        self.assert_response_body_expectation(response)

    def test_login_with_wrong_username(self):
        request_body = {
            'username': 'wrongusername',
            'password': self.password
        }
        response = self.client.post(self.request_url,
                                    data=json.dumps(request_body),
                                    headers=self.request_headers)
        self.assertEqual(response.status_code, UNAUTHORIZED)

    def test_login_with_wrong_password(self):
        request_body = {
            'username': self.user.username,
            'password': 'wrongpassword'
        }
        response = self.client.post(self.request_url,
                                    data=json.dumps(request_body),
                                    headers=self.request_headers)
        self.assertEqual(response.status_code, UNAUTHORIZED)

    def test_login_with_empty_username_or_password(self):
        request_body = {
            'password': 'wrongpassword'
        }
        response = self.client.post(self.request_url,
                                    data=json.dumps(request_body),
                                    headers=self.request_headers)
        self.assertEqual(response.status_code, BAD_REQUEST)

        request_body = {
            'username': 'kfjalfdf'
        }
        response = self.client.post(self.request_url,
                                    data=json.dumps(request_body),
                                    headers=self.request_headers)
        self.assertEqual(response.status_code, BAD_REQUEST)

    def test_verify_user_password(self):
        request_body = {
            'username': self.user.email,
            'password': self.password
        }
        response = self.client.post('v1/auth/verify-password',
                                    data=json.dumps(request_body),
                                    headers=self.request_headers)
        self.assertEqual(response.status_code, NO_CONTENT)

    def test_user_device_limit_reached(self):
        with app.test_request_context('/'):
            # add other client+device data

            user_devices = UserDevices(self.user.id)
            user_devices.clear_cache()
            maximum = 5
            count = 1
            while count <= maximum+1:
                data = UserDeviceData(client_id=random.choice([1, 2]),
                                      device_id='kkyy::{}'.format(count),
                                      application_id=1)
                user_devices.save(data=data)
                count += 1

            headers = self.request_headers.copy()
            headers['X-Scoop-Client'] = 'scoop ios/1.0.0'
            request_body = {
                'username': self.user.username,
                'password': self.password,
                'device_id': 'dummy-device-id-4455'
            }
            response = self.client.post(self.request_url,
                                        data=json.dumps(request_body),
                                        headers=headers)
            self.assertEqual(response.status_code, 420)

    def test_user_device_still_available(self):
        with app.test_request_context('/'):

            user_devices = UserDevices(self.user.id)
            user_devices.clear_cache()
            # add some user device into redis
            maximum = 5
            count = 1
            while count < maximum-1:
                data = UserDeviceData(client_id=count, device_id='kkyy::{}'.format(count),
                                      application_id=1)
                user_devices.save(data=data)
                count += 1

            headers = self.request_headers.copy()
            headers['User-Agent'] = 'scoop ios/1.0.0'
            request_body = {
                'username': self.user.username,
                'password': self.password,
                'device_id': 'dummy-device-id-8899'
            }
            response = self.client.post(self.request_url,
                                        data=json.dumps(request_body),
                                        headers=headers)
            self.assertEqual(response.status_code, OK, response)
            self.assert_response_body_expectation(response)

    def test_user_device_user_apps_foundry(self):
        with app.test_request_context('/'):
            user_devices = UserDevices(self.user_apps_foundry.id)
            user_devices.clear_cache()
            # add some user device into redis
            maximum = 5
            count = 1
            while count < maximum + 1:
                data = UserDeviceData(client_id=count, device_id='kkyy::{}'.format(count),
                                      application_id=1)
                user_devices.save(data=data)
                count += 1

            headers = self.request_headers.copy()
            headers['User-Agent'] = 'scoop ios/1.0.0'
            request_body = {
                'username': self.user_apps_foundry.username,
                'password': self.password,
                'device_id': 'dummy-device-id-8899'
            }
            response = self.client.post(self.request_url,
                                        data=json.dumps(request_body),
                                        headers=headers)
            self.assertEqual(response.status_code, OK, response)
            self.assert_response_body_expectation(response, user=self.user_apps_foundry)

    def test_eperpus_general_login(self):
        """ when user sign in in ePerpus General,
        user should be given access to all public librarry
        :return:
        """

        # setup data eperpus public:
        session = db.session()
        public_library1 = SharedLibraryFactory(id=1000001, public_library=True)
        public_library2 = SharedLibraryFactory(id=1000002, public_library=True)
        client_eperpus = session.query(Client).get(PUBLIC_LIBRARY_CLIENT_ID[0])
        client_eperpus.organizations.append(public_library1)
        client_eperpus.organizations.append(public_library2)
        session.add(client_eperpus)
        session.commit()
        session.expunge(client_eperpus)

        session.add(self.user)
        # set login from client application ePerpus General & Public Library
        headers = self.request_headers.copy()
        headers['X-Scoop-Client'] = 'eperpus ios/1.0.0'

        request_body = {
            'username': self.user.username,
            'password': prehash_user_password(self.password)
        }
        response = self.client.post(self.request_url,
                                    data=json.dumps(request_body),
                                    headers=headers)
        session.add(self.user)
        self.assertEqual(response.status_code, OK, response.data)
        self.assert_response_body_expectation(response, user=self.user)

        # validate user - registered to eperpus library 1 and 2
        saved_user = session.query(User).get(self.user.id)
        actual_org_id = [org.id for org in saved_user.organizations]
        self.assertIn(1000001, actual_org_id)
        self.assertIn(1000002, actual_org_id)

    def test_eperpus_general_vs_unverified_user(self):
        # test unverified user! on eperpus general is unauthorized. user must verified
        headers = self.request_headers.copy()
        headers['X-Scoop-Client'] = 'eperpus ios/1.0.0'
        request_body = {
            'username': self.user_unverified.username,
            'password': prehash_user_password(self.password)
        }
        response2 = self.client.post(self.request_url,
                                     data=json.dumps(request_body),
                                     headers=headers)
        self.assertEqual(response2.status_code, FORBIDDEN)

    def test_unverified_user(self):
        # test unverified user! on regular apps (not eperpus client), it's allowed
        headers = self.request_headers.copy()
        request_body = {
            'username': self.user_unverified.username,
            'password': prehash_user_password(self.password)
        }
        response2 = self.client.post(self.request_url,
                                     data=json.dumps(request_body),
                                     headers=headers)
        self.assertEqual(response2.status_code, OK)


def setup_module():
    db.drop_all()
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
