"""
Unit tests for our Auth helper functions.

*see*: :mod:`app.auth.helpers`
"""
from __future__ import unicode_literals

import os
from base64 import b64encode
import json
import random
from string import letters
import unittest
import jwt

from datetime import timedelta, datetime
from faker import Factory
from flask import Request, Flask, g
from flask_appsfoundry.auth import helpers as appsfoundry_helpers
from hashlib import sha1
from mockredis import mock_strict_redis_client
from nose.tools import raises, nottest, istest
from mock import MagicMock, patch, create_autospec
from redis import StrictRedis
import requests
from requests.exceptions import HTTPError

from app import app, db
from app.auth import constants
from app.auth import role_perms
from app.auth.exceptions import UserNotFound, Unauthorized
from app.auth.helpers import (
    credentials_from_bearer_in_request, assert_user_has_all_permissions, fetch_user_from_redis,
    fetch_user_from_scoopcas, authenticate_user_on_session, is_superuser, jwt_encode, jwt_decode, jwt_refresh,
    jwt_encode_from_user, get_user_from_request, get_permissions, assert_user_has_any_permission,
    get_reset_password_token, get_user_from_reset_password_token, get_current_user_roles, generate_signature, RealmEnum,
    CredentialData, assert_user_signature_in_payload, get_user_payload, get_expire_time, SCOOP_PORTAL)
from app.auth.models import UserInfo, ANONYMOUS_USER, Token, Client
from app.auth.user_devices import UserDeviceNotFound
from app.constants import RoleType
from flask_appsfoundry.exceptions import BadRequest

from app.master.choices import PlatformType
from app.users.organizations import Organization
from app.users.users import User, Role
from app.utils.datetimes import get_utc_nieve
from app.utils.testcases import TestBase


class JwtHelpersTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(JwtHelpersTests, cls).setUpClass()
        cls.user1 = User(id=99109, password=_fake.password(), username=_fake.name(), email=_fake.email())
        cls.user1.roles = [
            Role(id=RoleType.internal_marketing.value), Role(id=RoleType.internal_developer.value)
        ]
        cls.user1.organizations = [Organization(id=11), ]
        ctx = app.test_request_context('/')
        ctx.push()

    def tearDown(self):
        g.current_user = None
        super(JwtHelpersTests, self).tearDown()

    def test_encode_decode(self):
        payload = {
            'user_id': _fake.pyint(),
            'user_name': _fake.name(),
            'email': _fake.email(),
            'exp': get_utc_nieve() + timedelta(minutes=5)
        }
        encoded_token = jwt_encode(payload)
        self.assertIsNotNone(encoded_token)

        expected = jwt_decode(encoded_token)
        self.assertDictEqual(expected, payload)

    def test_jwt_refresh_success(self):
        payload = {
            'user_id': _fake.pyint(),
            'user_name': _fake.name(),
            'email': _fake.email(),
            'exp': get_utc_nieve()
        }
        encoded_token = jwt_encode(payload)
        self.assertIsNotNone(encoded_token)
        new_encoded_token = jwt_refresh(encoded_token)
        self.assertIsNotNone(new_encoded_token)

    def test_jwt_refresh_but_expired(self):
        payload = {
            'user_id': _fake.pyint(),
            'user_name': _fake.name(),
            'email': _fake.email(),
            'exp': get_utc_nieve() - timedelta(days=10)
        }
        encoded_token = jwt_encode(payload)
        self.assertIsNotNone(encoded_token)
        self.assertRaises(jwt.ExpiredSignatureError, jwt_refresh, encoded_token)

    def test_jwt_encode_user(self):
        user = User(id=123456, username=_fake.email(), email=_fake.email(), is_active=True, password="1231313dfasfd")
        user.roles = [
            Role(id=RoleType.internal_marketing.value), Role(id=RoleType.internal_developer.value)
        ]
        user.organizations = [Organization(id=10), ]

        device_id = 'asfsdfsdf123123'
        encoded = jwt_encode_from_user(user, device_id=device_id)
        self.assertIsNotNone(encoded)

        actual = jwt_decode(encoded)
        expired = actual.pop('exp')
        expected = {
            'user_id': user.id,
            'user_name': user.username,
            'email': user.email,
            'roles': [role.id for role in user.roles],
            'organizations': [org.id for org in user.organizations],
            'device_id': device_id,
            'expire_timedelta': timedelta(minutes=5).seconds
        }
        self.assertDictContainsSubset(expected, actual)
        now = int((get_utc_nieve() - datetime(1970, 1, 1)).total_seconds())
        expired_expected = int((get_utc_nieve() + timedelta(minutes=5) - datetime(1970, 1, 1)).total_seconds())
        self.assertGreater(expired, now)
        self.assertGreaterEqual(expired_expected, expired,
                                'expired time {} is not greater or equal to expected value {}'.format(
                                    expired, expired_expected
                                ))

    def test_generate_signature(self):
        key = "fjskafslkdfjlsafjlskadjf"
        expected = sha1(app.config['JWT_SECRET'] + key).hexdigest()
        actual = generate_signature(key)
        self.assertEqual(expected, actual)

    def test_assert_user_signature_in_payload(self):
        # valid jwt token data signature
        payload = get_user_payload(self.user1, "abc123", timedelta(minutes=5))
        token_data = CredentialData(
            user_id=self.user1.id,
            token="dfsfksadfldsakfsdf",
            payload=payload,
            realm=RealmEnum.jwt,
        )
        actual = assert_user_signature_in_payload(self.user1, token_data)
        self.assertIsNone(actual)

        # valid bearer token data (no signature, no need to check further)
        token_data_bearer = CredentialData(
            user_id=self.user1.id,
            token="dfsfksadfldsakfsdf",
            payload=None,
            realm=RealmEnum.bearer,
        )
        actual = assert_user_signature_in_payload(self.user1, token_data_bearer)
        self.assertIsNone(actual)

        # test signature is different/invalid
        payload['sig'] = "faslkfjasldfjlasjfas"
        token_data_invalid = CredentialData(
            user_id=self.user1.id,
            token="dfsfksadfldsakfsdf",
            payload=payload,
            realm=RealmEnum.jwt,
        )
        self.assertRaises(
            Unauthorized,
            assert_user_signature_in_payload, self.user1, token_data_invalid,
        )

    def test_get_user_payload(self):
        expire_time = timedelta(minutes=5)
        device_id = "abc123"
        user = self.user1
        expected = {
            'user_id': user.id,
            'user_name': user.username,
            'email': user.email,
            'roles': [role.id for role in user.roles],
            'organizations': [org.id for org in user.organizations],
            'device_id': device_id,
            # 'expire_timedelta': expire_time.seconds,
            'sig': generate_signature(user.password),
            'iss': constants.TOKEN_ISSUER
        }
        actual = get_user_payload(self.user1, device_id, expire_time)
        self.assertDictContainsSubset(expected, actual)
        self.assertLessEqual(actual.get('exp'), get_utc_nieve() + expire_time, )

    def test_get_expire_time(self):
        client = Client(id=8878, client_id=8878, platform_id=PlatformType.ios.value)
        g.current_client = client
        actual = get_expire_time()
        # for platform ios --> expires time is None
        self.assertIsNone(actual)

        g.current_client.platform_id = PlatformType.android.value
        actual = get_expire_time()
        # for platform android --> expires time is None
        self.assertIsNone(actual)

        g.current_client.platform_id = PlatformType.web.value
        actual = get_expire_time()
        # for client portal scoop--> expires = 5 minutes
        self.assertEqual(actual, timedelta(hours=24))

        g.current_client.id = SCOOP_PORTAL
        actual = get_expire_time()
        g.current_client = None
        # for client portal scoop--> expires = 5 minutes
        self.assertEqual(actual, timedelta(minutes=5))


class ResetPasswordHelpersTests(unittest.TestCase):

    def test_get_reset_password_token(self):
        user = User(
            id=_fake.pyint(),
            username=_fake.name(),
            email=_fake.email())
        with app.test_request_context('/'):
            reset_pwd_token = get_reset_password_token(user)
            self.assertIsNotNone(reset_pwd_token)
            expected = {
                "user_id": user.id,
                "sub": "reset password",
                'iss': 'SCOOP'
            }
            actual = jwt_decode(reset_pwd_token)
            expired = actual.pop('exp', None)
            self.assertDictContainsSubset(expected, actual)
            now = int((get_utc_nieve() - datetime(1970, 1, 1)).total_seconds())
            expired_expected = int((get_utc_nieve() + timedelta(days=3) - datetime(1970, 1, 1)).total_seconds())
            self.assertGreater(expired, now)
            self.assertGreaterEqual(expired_expected, expired,
                                    'expired time {} is not greater or equal to expected value {}'.format(
                                        expired, expired_expected
                                    ))

    @patch("flask_sqlalchemy.orm.query.Query.get", autospec=True)
    def test_get_user_from_reset_password_token(self, mocked_session_get):
        session = db.session()
        with app.test_request_context('/'):
            user = create_autospec(User)
            user.id = _fake.pyint()
            user.username = _fake.name()
            user.email = _fake.email()
            # create valid token
            reset_pwd_token = get_reset_password_token(user)

            # mock query get user return valid user
            mocked_session_get.return_value = user

            actual_user = get_user_from_reset_password_token(reset_pwd_token, session)
            self.assertIsNotNone(actual_user)
            self.assertIsNotNone(actual_user.id, user.id)

    @raises(BadRequest)
    @patch("flask_sqlalchemy.orm.query.Query.get", autospec=True)
    def test_user_not_found(self, mocked_session_get):
        """Test method: get_user_from_reset_password_token and user not found! """
        session = db.session()
        with app.test_request_context('/'):
            user = create_autospec(User)
            user.id = _fake.pyint()
            reset_pwd_token = get_reset_password_token(user)

            # mock user not found!
            mocked_session_get.return_value = None

            # execute function to tests!
            get_user_from_reset_password_token(reset_pwd_token, session)

    @raises(Unauthorized)
    def test_reset_token_invalid(self):
        """Test method: get_user_from_reset_password_token and token invalid! """
        session = db.session()
        with app.test_request_context('/'):
            reset_pwd_token = 'afsflksdfsd.dfsafsdafdsaf.dfdsfdasfasd'

            # execute function to tests!
            get_user_from_reset_password_token(reset_pwd_token, session)

    @raises(Unauthorized)
    def test_token_payload_invalid(self):
        """Test method: get_user_from_reset_password_token
        and token issuer/subject is invalid! """
        session = db.session()
        with app.test_request_context('/'):
            payload = {
                "user_id": 1234,
                "sub": "invalid-subject",
                'iss': "invalid-issuer",
                "exp": get_utc_nieve() + timedelta(minutes=60)
            }
            reset_pwd_token = jwt_encode(payload=payload)

            # execute function to tests!
            get_user_from_reset_password_token(reset_pwd_token, session)

    @raises(Unauthorized)
    def test_token_expired(self):
        """Test method: get_user_from_reset_password_token and token expired """
        session = db.session()
        with app.test_request_context('/'):
            payload = {
                "user_id": 1234,
                "sub": "invalid-subject",
                'iss': "invalid-issuer",
                "exp": get_utc_nieve() - timedelta(minutes=120)
            }
            reset_pwd_token = jwt_encode(payload=payload)

            # execute function to tests!
            get_user_from_reset_password_token(reset_pwd_token, session)


@istest
class AssertUserHasAnyPermissionTests(unittest.TestCase):

    def test_user_permissions_valid(self):
        """ Method should return None if the user has valid permissions.
        """
        required_perms = ['perm1', 'perm2', 'perm3']
        user = UserInfo(username=_fake.user_name(), token=_fake.password(), user_id=_fake.pyint(), perm=['perm2'])
        self.assertIsNone(assert_user_has_any_permission(required_perms, user))

    @raises(Unauthorized)
    def test_user_permissions_invalid(self):
        required_perms = ['perm1', 'perm2', 'perm3']
        user = UserInfo(username=_fake.user_name(), token=_fake.password(), user_id=_fake.pyint(), perm=['perm4'])
        assert_user_has_any_permission(required_perms, user)

    @raises(Unauthorized)
    def test_anonymous_user(self):
        required_perms = ['perm1', 'perm2', 'perm3']
        assert_user_has_any_permission(required_perms, ANONYMOUS_USER)


class ScoopFlaskAssertUserHasAnyPermissionTests(unittest.TestCase):

    env_testing = os.environ.get('TESTING', None)

    @classmethod
    def setUpClass(cls):
        # remove testing from environment so we can test the method
        os.environ.pop('TESTING', None)

    @classmethod
    def tearDownClass(cls):
        # restore the condition
        if cls.env_testing:
            os.environ['TESTING'] = cls.env_testing

    def test_user_permission_valid(self):
        """test method assert_user_has_any_permission in scoop flask"""
        with app.test_request_context('/'):
            required_perms = ['perm1', 'perm2', 'perm3']
            g.user = UserInfo(username=_fake.user_name(), token=_fake.password(), user_id=_fake.pyint(), perm=['perm2'])
            self.assertIsNone(appsfoundry_helpers.assert_user_has_any_permission(
                required_perms, redis=None, app_conf={}))

    @raises(appsfoundry_helpers.UnauthorizedError)
    def test_user_permissions_invalid(self):
        with app.test_request_context('/'):
            required_perms = ['perm1', 'perm2', 'perm3']
            g.user = UserInfo(username=_fake.user_name(), token=_fake.password(), user_id=_fake.pyint(), perm=['perm4'])
            appsfoundry_helpers.assert_user_has_any_permission(required_perms, redis=None, app_conf={})

    @raises(appsfoundry_helpers.UnauthorizedError)
    def test_anonymous_user(self):
        with app.test_request_context('/'):
            required_perms = ['perm1', 'perm2', 'perm3']
            g.user = ANONYMOUS_USER
            appsfoundry_helpers.assert_user_has_any_permission(required_perms, redis=None, app_conf={})


@istest
class AssertUserHasAllPermissions(unittest.TestCase):

    def test_user_permissions_valid(self):
        required_perms = ['perm1', 'perm2', 'perm3']
        user = UserInfo(username=_fake.user_name(), token=_fake.password(), user_id=_fake.pyint(), perm=required_perms)
        self.assertIsNone(assert_user_has_all_permissions(required_perms, user))

    @raises(Unauthorized)
    def test_user_permissions_invalid(self):
        required_perms = ['perm1', 'perm2', 'perm3']
        user = UserInfo(username=_fake.user_name(), token=_fake.password(), user_id=_fake.pyint(), perm=['perm2'])
        self.assertIsNone(assert_user_has_all_permissions(required_perms, user))

    @raises(Unauthorized)
    def test_anonymous_user(self):
        required_perms = ['perm1', 'perm2', 'perm3']
        assert_user_has_all_permissions(required_perms, ANONYMOUS_USER)


@istest
class FetchUserFromRedisTests(unittest.TestCase):

    def test_lookup_key_formed(self):
        """ Redis must be queried with a key formed with an arbitrary prefix
        combined with the user's database ID.
        """
        mock_redis = MagicMock(spec=StrictRedis)

        prefix = _fake.word()
        user_id = _fake.pyint()

        try:
            fetch_user_from_redis(mock_redis, prefix, user_id, None)
        except Exception:
            # we don't care about any exceptions
            pass

        expected_call = "{}:{}".format(prefix, user_id)
        mock_redis.get.assert_called_once_with(expected_call)

    def test_lookup_key_prefix_trailing_colon_stripped(self):
        """ Lookup key formation should should handle an extra trailing slash
        on the key prefix.
        """
        mock_redis = MagicMock(spec=StrictRedis)

        prefix = _fake.word()
        user_id = _fake.pyint()

        try:
            fetch_user_from_redis(mock_redis, prefix + ":", user_id, None)
        except Exception:
            # we don't care about any exceptions
            pass

        expected_call = "{}:{}".format(prefix, user_id)
        mock_redis.get.assert_called_once_with(expected_call)

    @raises(Unauthorized)
    def test_user_not_found(self):
        mock_redis = MagicMock(spec=StrictRedis)
        mock_redis.get.return_value = None

        prefix = _fake.word()
        user_id = _fake.pyint()

        fetch_user_from_redis(mock_redis, prefix, user_id, None)

    @raises(Unauthorized)
    def test_nonmatching_password(self):

        prefix = _fake.word()
        user_id = _fake.pyint()

        mock_redis = MagicMock(spec=StrictRedis)
        mock_redis.get.return_value = str(dict(
            username=_fake.user_name(),
            user_id=user_id,
            token="password_1",
            perm=[]
        ))

        fetch_user_from_redis(mock_redis, prefix, user_id, "password_2")

    def test_valid_user(self):
        prefix = _fake.word()
        user_id = _fake.pyint()
        password = _fake.password()

        mock_redis = MagicMock()
        mock_redis.get.return_value = str(dict(
            username=_fake.user_name(),
            user_id=user_id,
            token=password,
            perm=[]
        ))

        fetch_user_from_redis(mock_redis, prefix, user_id, password)


@istest
class FetchUserFromScoopcasTests(unittest.TestCase):

    @patch('app.auth.helpers.requests.post', spec=requests.Request)
    @patch('app.auth.helpers.current_app', spec=Flask)
    def test_request_is_properly_formed(self, mock_flask, mock_post_req):

        mock_flask.config = {
            'JSON_HEADERS': {
                'Content-Type': _fake.mime_type(category='application')
            },
            'AUTH_HEADER': 'Bearer: {}'.format(
                b64encode("{}:{}".format(_fake.user_name(), _fake.password()))),
            'VERIFY_TOKEN': _fake.url()
        }

        user_id, password = _fake.pyint(), _fake.password()

        try:
            fetch_user_from_scoopcas(user_id, password)
        except Exception:
            # don't care about any exceptions thrown -- we're verifying the
            # http call only
            pass

        expected_headers = mock_flask.config['JSON_HEADERS']
        expected_headers['Authorization'] = mock_flask.config['AUTH_HEADER']

        mock_post_req.assert_called_once_with(
            mock_flask.config['VERIFY_TOKEN'],
            data=json.dumps({'user_id': user_id, 'access_token': password}),
            headers=expected_headers
        )

    @raises(Unauthorized)
    @patch('app.auth.helpers.requests.post', spec=requests.Request)
    @patch('app.auth.helpers.current_app', spec=Flask)
    def test_non_200_raises_unauthorized(self, mock_flask, mock_post_req):
        mock_flask.config = {
            'JSON_HEADERS': {
                'Content-Type': _fake.mime_type(category='application')
            },
            'AUTH_HEADER': 'Bearer: {}'.format(
                b64encode("{}:{}".format(_fake.user_name(), _fake.password()))),
            'VERIFY_TOKEN': _fake.url()
        }

        user_id, password = _fake.pyint(), _fake.password()
        mock_post_req.return_value.status_code = random.randint(201, 599)
        fetch_user_from_scoopcas(user_id, password)

    @raises(Unauthorized)
    @patch('app.auth.helpers.requests.post', spec=requests.Request, side_effect=HTTPError())
    @patch('app.auth.helpers.current_app', spec=Flask)
    def test_http_error_raises_unauthorized(self, mock_flask, mock_post_req):
        mock_flask.config = {
            'JSON_HEADERS': {
                'Content-Type': _fake.mime_type(category='application')
            },
            'AUTH_HEADER': 'Bearer: {}'.format(
                b64encode("{}:{}".format(_fake.user_name(), _fake.password()))),
            'VERIFY_TOKEN': _fake.url()
        }
        fetch_user_from_scoopcas(_fake.pyint(), _fake.password())

    @patch('app.auth.helpers.requests.post', spec=requests.Request)
    @patch('app.auth.helpers.current_app', spec=Flask)
    def test_successful_user_info_returned(self, mock_flask, mock_post_req):

        user_id = _fake.pyint()
        username = _fake.user_name()
        password = _fake.password()
        tokens = _fake.pylist(10, True, _fake.word)

        mock_flask.config = {
            'JSON_HEADERS': {
                'Content-Type': _fake.mime_type(category='application')
            },
            'AUTH_HEADER': 'Bearer: {}'.format(
                b64encode("{}:{}".format(username, password))),
            'VERIFY_TOKEN': _fake.url()
        }

        mock_resp = MagicMock(spec=requests.Response)
        mock_resp.status_code = 200
        mock_resp.json = lambda: {
            'user_id': user_id,
            'username': username,
            # this comes back from CAS as a space-separated string, not a list.
            'scope': " ".join(tokens)
        }

        mock_post_req.return_value = mock_resp

        user_info = fetch_user_from_scoopcas(user_id, password)

        self.assertIsInstance(user_info, UserInfo)

        self.assertEqual(user_info.user_id, user_id)
        self.assertEqual(user_info.username, username)
        self.assertEqual(user_info.token, password)
        self.assertListEqual(user_info.perm, tokens)


@istest
class CredentialsFromRequestTests(unittest.TestCase):
    """ Tests to make sure our Bearer credentials can be extracted from request

    *see*: :func:`app.auth.helpers.credentials_from_bearer_in_request`
    """
    @patch("flask_sqlalchemy.orm.query.Query.first", autospec=True)
    def test_valid_with_bearer(self, mocked_get_user_token):
        """ Make sure the correct username/password are returned. """
        username = _fake.user_name()
        password = _fake.password()
        encoded = b64encode("{username}:{password}".format(
            username=username, password=password)
        )
        req = MagicMock(spec=Request)
        req.headers = {
            "Authorization": "Bearer {}".format(encoded),
        }
        # mock get user token method, return fake data
        token = create_autospec(Token)
        token.token = password
        token.user_id = username
        token.is_active = True
        mocked_get_user_token.return_value = token

        # execute function
        decoded_username, decoded_pass = credentials_from_bearer_in_request(req=req)

        self.assertEqual(username, decoded_username)
        self.assertEqual(password, decoded_pass)

    @patch("flask_sqlalchemy.orm.query.Query.first", autospec=True)
    def test_valid_bearer_but_wrong_token_password(self, mocked_get_user_token):
        username = _fake.pyint()
        password = _fake.password()
        encoded = b64encode("{username}:{password}".format(
            username=username, password=password)
        )

        req = MagicMock(spec=Request)
        req.headers = {
            "Authorization": "Bearer {}".format(encoded),
        }
        # mock get user token method, as if token password not found in database
        mocked_get_user_token.return_value = None
        decoded_username, decoded_pass = credentials_from_bearer_in_request(req=req)

        self.assertIsNone(decoded_username)
        self.assertIsNone(decoded_pass)

    @raises(Unauthorized)
    def test_missing_auth_header(self):
        """ Unauthorized if the auth header is missing completely. """
        req = MagicMock(spec=Request)
        req.headers = _fake.pydict()
        credentials_from_bearer_in_request(req=req)

    @raises(Unauthorized)
    def test_non_bearer_header(self):
        """ We don't allow basic auth on our API calls. """
        encoded = b64encode("{username}:{password}".format(
            username=_fake.user_name(), password=_fake.password()
        ))
        req = MagicMock(spec=Request)
        req.headers = {
            "Authorization": "Basic {}".format(encoded)
        }
        credentials_from_bearer_in_request(req=req)

    @raises(Unauthorized)
    def test_unparsable_header(self):
        """ Check with an invalidly formatted (but b64 encoded) auth header """
        bad = b64encode(
            "".join([random.choice(letters) for _ in range(40)]) + ":"
        )
        req = MagicMock(spec=Request)
        req.headers = {
            "Authorization": "Bearer {}".format(bad)
        }
        credentials_from_bearer_in_request(req=req)


@istest
class GetUserFromRequestTests(unittest.TestCase):
    """ Tests to make sure our JWT/Bearer credentials can be extracted from request

    *see*: :func:`app.auth.helpers.get_user_from_request`
    """
    def test_jwt_token_valid(self):
        """ Make sure the correct username/password are returned. """
        user_id = _fake.pyint()
        with app.test_request_context('/'):
            payload = {"user_id": user_id, "exp": get_utc_nieve() + timedelta(minutes=10)}
            encoded = jwt_encode(payload)
            req = MagicMock(spec=Request)
            req.headers = {
                "Authorization": "JWT {}".format(encoded),
            }
            token_data = get_user_from_request(req=req)

            self.assertEqual(token_data.user_id, user_id)
            self.assertEqual(token_data.token, encoded)
            self.assertEqual(token_data.realm, RealmEnum.jwt)
            self.assertEqual(token_data.payload, payload)

    @raises(Unauthorized)
    def test_jwt_token_expired(self):
        """ Make sure the correct username/password are returned. """
        user_id = _fake.pyint()
        with app.test_request_context('/'):
            encoded = jwt_encode({
                "user_id": user_id,
                "exp": get_utc_nieve() - timedelta(days=10)
            })
            req = MagicMock(spec=Request)
            req.headers = {
                "Authorization": "JWT {}".format(encoded),
            }
            token_data = get_user_from_request(req=req)

    @raises(Unauthorized)
    def test_jwt_token_invalid(self):
        """ Make sure the correct username/password are returned. """
        user_id = _fake.pyint()
        with app.test_request_context('/'):
            encoded = jwt.encode({
                "user_id": user_id,
                "exp": get_utc_nieve() + timedelta(minutes=10)
            }, 'wrongsecret')
            req = MagicMock(spec=Request)
            req.headers = {
                "Authorization": "JWT {}".format(encoded),
            }
            token_data = get_user_from_request(req=req)

    def test_missing_auth_header(self):
        """ if the auth header is missing completely:
         No error raised, decoded_user_id is none."""
        with app.test_request_context('/'):
            req = MagicMock(spec=Request)
            req.headers = _fake.pydict()
            token_data = get_user_from_request(req=req)
            self.assertIsNone(token_data.user_id)
            self.assertIsNone(token_data.token)
            self.assertIsNone(token_data.realm)
            self.assertIsNone(token_data.payload)

    @patch("flask_sqlalchemy.orm.query.Query.first", autospec=True)
    def test_with_valid_bearer(self, mocked_get_user_token):
        """ Make sure the correct username/password are returned. """
        user_id = _fake.pyint()
        password = _fake.password()
        encoded = b64encode("{username}:{password}".format(
            username=user_id, password=password)
        )

        req = MagicMock(spec=Request)
        req.headers = {
            "Authorization": "Bearer {}".format(encoded),
        }
        with app.test_request_context('/'):
            # mock get user token method, return fake data
            mocked_get_user_token.return_value = Token(token=password, user_id=user_id, is_active=True)
            token_data = get_user_from_request(req=req)

            self.assertEqual(user_id, int(token_data.user_id))
            self.assertEqual(password, token_data.token)
            self.assertEqual(token_data.realm, RealmEnum.bearer)
            self.assertIsNone(token_data.payload)

    @patch("flask_sqlalchemy.orm.query.Query.first", autospec=True)
    def test_with_invalid_bearer(self, mocked_get_user_token):
        """ Make sure the correct username/password are returned. """
        user_id = _fake.pyint()
        password = _fake.password()
        encoded = b64encode("{username}:{password}".format(
            username=user_id, password=password)
        )

        req = MagicMock(spec=Request)
        req.headers = {
            "Authorization": "Bearer {}".format(encoded),
        }
        with app.test_request_context('/'):
            # mock get user token method, return None as if token password is invalid (data not found)
            mocked_get_user_token.return_value = None
            token_data = get_user_from_request(req=req)

            self.assertIsNone(token_data.user_id)
            self.assertIsNone(token_data.token)

    @raises(Unauthorized)
    def test_bad_auth_header(self):
        with app.test_request_context('/'):
            req = MagicMock(spec=Request)
            req.headers = {
                "Authorization": "kdfjlsdfjdslkfjsdlfjsdf",
            }
            token_data = get_user_from_request(req=req)
            self.assertIsNone(token_data.user_id)
            self.assertIsNone(token_data.token)


@istest
class GetPermissionsTests(unittest.TestCase):

    def test_get_permissions(self):
        # test for method: get_permissions
        expected_perms = role_perms.PERMS.get(RoleType.internal_marketing)
        expected_perms.extend(role_perms.PERMS.get(RoleType.internal_developer))
        # remove duplicate values
        expected_perms = list(set(expected_perms))
        # compare list of permissions from this user roles
        user = mocked_get_user_from_db(12345)
        user_roles = [role.id for role in user.roles.all()]
        self.assertEqual(get_permissions(user_roles), expected_perms)


@istest
class GetUserInfoTests(unittest.TestCase):

    def test_get_current_user_roles(self):
        expected_role_ids = [RoleType.internal_marketing.value, RoleType.internal_developer.value, ]
        user = mocked_get_user_from_db(12345)
        with app.test_request_context('/'):
            g.current_user_roles = [role.id for role in user.roles.all()]
            self.assertEqual(get_current_user_roles(), expected_role_ids)


def mocked_get_user_from_request(req):
    user = mocked_get_user_from_db(12345)
    return CredentialData(
                12345, 'kdsjfladjfdkslafjsdlf123123lkjdfkljsf',
                payload=get_user_payload(user, "abc123", timedelta(days=10)),
                realm=RealmEnum.jwt
            )


def mocked_get_user_from_db(user_id):
    user = User(id=user_id, is_active=True, username=_fake.email(), email=_fake.email())
    user.roles = [
        Role(id=RoleType.internal_marketing.value), Role(id=RoleType.internal_developer.value)
    ]
    user.organizations = [
        Organization(id=9910),
    ]
    return user


@istest
class AuthenticateUserOnSessionTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(AuthenticateUserOnSessionTests, cls).setUpClass()

    @patch('app.auth.helpers.get_user_from_request')
    @patch("flask_sqlalchemy.orm.query.Query.get")
    def test_valid_token_and_user(self, mock_get_user_from_db, mock_get_user_from_req):
        """ valid user
        """
        expected_user_id = 12345
        user = mocked_get_user_from_db(expected_user_id)
        mock_get_user_from_db.return_value = user
        mock_get_user_from_req.return_value = CredentialData(
            12345, 'kdsjfladjfdkslafjsdlf123123lkjdfkljsf',
            payload=get_user_payload(user, device_id=None, expire_time=timedelta(days=10)),
            realm=RealmEnum.jwt
        )
        # test execute the function!
        authenticate_user_on_session()

        # assert values
        self.assertIsNotNone(g.current_user)
        self.assertEqual(g.current_user.id, expected_user_id)
        self.assertIsNotNone(g.user)
        self.assertEqual(g.user.user_id, expected_user_id)
        expected_perms = role_perms.PERMS.get(RoleType.internal_marketing)
        expected_perms.extend(role_perms.PERMS.get(RoleType.internal_developer))
        # remove duplicate values
        expected_perms = list(set(expected_perms))
        # compare list of permissions from this user roles
        self.assertEqual(g.user.perm, expected_perms)

    @patch('app.auth.helpers.get_user_from_request')
    @patch("flask_sqlalchemy.orm.query.Query.get")
    def test_unregistered_device(self, mock_get_user_from_db, mock_get_user_from_req):
        """ valid user but device is unregistered (already revoked/deauthorized),
        if device already deauthorized, token should be invalid!
        """
        expected_user_id = 12345
        user = mocked_get_user_from_db(expected_user_id)
        mock_get_user_from_db.return_value = user
        mock_get_user_from_req.return_value = CredentialData(
            12345, 'kdsjfladjfdkslafjsdlf123123lkjdfkljsf',
            payload=get_user_payload(user, device_id="unregisterd_device", expire_time=timedelta(days=10)),
            realm=RealmEnum.jwt
        )
        # test execute the function!
        self.assertRaises(
            UserDeviceNotFound,
            authenticate_user_on_session
        )

    @patch('app.auth.helpers.get_user_from_request')
    def test_not_found_sets_anonymous_user(self, mocked_get_user_none):
        """ Failed to decode authorization token should return ANONYMOUS_USER.
        """
        mocked_get_user_none.return_value = CredentialData(None, None)
        authenticate_user_on_session()
        self.assertIs(g.user, ANONYMOUS_USER)
        self.assertIs(g.current_user, None)

    @raises(Unauthorized)
    @patch('app.auth.helpers.get_user_from_request', side_effect=Unauthorized)
    def test_unauthorized_invalid_token(self, mocked):
        authenticate_user_on_session()

    @patch('app.auth.helpers.credentials_from_bearer_in_request',
           side_effect=random.choice([KeyError, ValueError, Exception]))
    def test_all_other_exceptions_set_anonymous_user(self, mock_creds_from_request):
        """ All unexpected errors should simply set the g.user instance to
        ANONYMOUS_USER
        """
        authenticate_user_on_session()
        self.assertIs(g.user, ANONYMOUS_USER)
        self.assertIs(g.current_user, None)


@istest
class IsSuperuserTests(unittest.TestCase):
    """ Verified is_superuser helper properly detects superuser/non-superuser.
    """

    def test_unauthenticated_user_returns_false(self):
        self.assertFalse(is_superuser(None))

    def test_superuser_returns_true(self):
        u = User()
        u.roles = [
            Role(id=RoleType.super_admin.value)
        ]
        self.assertTrue(is_superuser(u))

    def test_nonsuperuser_returns_false(self):
        u = User()
        u.roles = [
            Role(id=RoleType.public_user.value)
        ]
        self.assertFalse(is_superuser(u))


_fake = Factory.create()
