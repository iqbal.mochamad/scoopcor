from __future__ import unicode_literals, absolute_import

from flask import g
from flask_appsfoundry.exceptions import Forbidden

from app import app
from app.auth.eperpus import assign_user_to_eperpus_public, assert_user_eperpus_general
from app.eperpus.constants import PUBLIC_LIBRARY_CLIENT_ID
from app.auth.models import Client
from app.eperpus.tests.fixtures import SharedLibraryFactory
from app.users.tests.fixtures import UserFactory
from app.utils.testcases import TestBase


class EperpusTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(EperpusTests, cls).setUpClass()
        cls.public_library1 = SharedLibraryFactory(id=1000001, public_library=True)
        cls.public_library2 = SharedLibraryFactory(id=1000002, public_library=True)
        cls.client_ios = cls.session.query(Client).get(1)
        cls.client_eperpus = cls.session.query(Client).get(PUBLIC_LIBRARY_CLIENT_ID[0])
        cls.client_eperpus.organizations.append(cls.public_library1)
        cls.client_eperpus.organizations.append(cls.public_library2)
        cls.user = UserFactory()
        cls.user2 = UserFactory()
        cls.user_unverified = UserFactory(is_verified=False)
        cls.session.add(cls.client_eperpus)
        cls.session.commit()

    # @classmethod
    # def tearDownClass(cls):
    #     cls.session.expunge_all()
    #     super(EperpusTests, cls).tearDownClass()

    def test_assign_user_eperpus_general_public(self):

        with self.on_session(self.user, self.client_eperpus, self.public_library1, self.public_library2):
            public_lib_id1 = self.public_library1.id
            public_lib_id2 = self.public_library2.id
            # test with eperpus client, user should auto added to eperpus public
            # g.current_client = client_eperpus
            assign_user_to_eperpus_public(self.user, self.client_eperpus, self.session)
            self.session.refresh(self.user)
            actual_org_id = [org.id for org in self.user.organizations]
            self.assertIn(public_lib_id1, actual_org_id)
            self.assertIn(public_lib_id2, actual_org_id)

        user2 = UserFactory()
        self.session.commit()
        self.session.add(user2)
        self.session.add(self.client_ios)
        # test with non eperpus client, user should not auto added to eperpus public
        assign_user_to_eperpus_public(user2, self.client_ios, self.session)
        self.session.refresh(user2)
        actual_org_id = [org.id for org in user2.organizations]
        self.session.expunge(user2)
        self.assertNotIn(public_lib_id1, actual_org_id)
        self.assertNotIn(public_lib_id2, actual_org_id)

    def test_assert_user_eperpus_general(self):
        # user verified vs client "eperpus general", no error raised
        self.session.add(self.user2)
        actual = assert_user_eperpus_general(self.session, self.user2, self.client_eperpus)
        self.assertTrue(actual)

        # for unverified user vs client "eperpus general" -> Forbidden raised
        self.assertRaises(
            Forbidden,
            assert_user_eperpus_general, self.session, self.user_unverified, self.client_eperpus)

        # user unverified vs client not an eperpus --> no error raised
        actual = assert_user_eperpus_general(self.session, self.user2, self.client_ios)
        self.assertTrue(actual)
