from __future__ import unicode_literals, absolute_import

import json
import unittest

from flask import g
from mockredis import mock_strict_redis_client

from app import app, db
from app.auth.user_devices import UserDevices, ErrorMaximumDeviceReached, UserDeviceNotFound, \
    UserDeviceData, UserDeviceStorage, migrate_from_redis_to_db
from app.constants import RoleType
from app.users.tests.fixtures import UserFactory, OrganizationFactory
from app.auth.models import Client, Application
from app.users.user_devices import UserDevice
from app.users.users import Role, is_apps_foundry_users, APPS_FOUNDRY_ORG
from app.utils.redis_cache import RedisCache
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import ClientFactory


class UserDevicesRedisTests(TestBase):

    mock_redis = mock_strict_redis_client()
    redis_cache = RedisCache(redis=mock_redis)
    storage = UserDeviceStorage.redis

    @classmethod
    def setUpClass(cls):
        super(UserDevicesRedisTests, cls).setUpClass()
        role = cls.session.query(Role).get(int(RoleType.internal_admin))
        cls.user = UserFactory(id=12345, roles=[role, ])

        cls.session.commit()

    def tearDown(self):
        super(UserDevicesRedisTests, self).tearDown()
        self.mock_redis.flushdb()

    def test_get_user_device_data(self):
        db.session().add(self.user)
        user_devices = UserDevices(self.user.id, self.redis_cache, storage=self.storage)
        key = user_devices.key
        expected = [{"kkk": 123, "bbb": "kdfjslkfdsklfdskf"}, {"kkk": 456, "bbb": "yyyhhhkkkk"}]
        self.redis_cache.write(key=key, data=expected)
        actual = user_devices._get_cached_data()
        self.assertEqual(actual, expected)

    def test_save_user_device_data(self):
        user_devices = UserDevices(self.user.id, self.redis_cache, storage=self.storage)
        # clean up previous data if any
        user_devices.clear_cache()
        data = UserDeviceData(client_id=1111, device_id='da791a4b7e36aeed6e7dce6f186d6421',
                              application_id=123)
        user_devices.save(data)
        cached_data = self.redis_cache.get(key=user_devices.key)
        actual = json.loads(cached_data) if cached_data else []
        expected = {'client_id': 1111, 'device_id': 'da791a4b7e36aeed6e7dce6f186d6421',
                    'application_id': 123}
        self.assertIn(expected, actual)

    def test_assert_unlimited_devices(self):
        session = db.session()
        user = UserFactory(id=30)
        app_id = 1
        maximum = 2
        session.commit()
        application = session.query(Application).get(app_id)
        application.max_devices = 0
        application.enable_unlimited_devices = True
        session.add(application)
        client = ClientFactory(id=10022, application=application, platform_id=1)
        session.commit()
        g.current_client = client

        # initiate user device class
        user_devices = UserDevices(user.id, self.redis_cache, storage=self.storage)
        user_devices.clear_cache()

        # add saved device data
        count = 1
        while count <= maximum + 3:
            data = UserDeviceData(client_id=count, device_id='kkyy::{}'.format(count),
                                  application_id=app_id)
            user_devices.save(data=data)
            count += 1
        # assert new device, still allowed, enable_unlimited_devices=True
        self.assertIsNone(user_devices._assert_devices_per_application(device_id='kkkk2355'))

    def test_assert_devices_per_application(self):
        user = UserFactory(id=31)
        db.session().commit()
        app_id = 100
        maximum = 2
        session = db.session()
        application = Application(id=app_id, max_devices=maximum)
        session.add(application)
        session.commit()
        client = ClientFactory(id=10001, application=application)
        session.commit()
        g.current_client = session.query(Client).get(10001)

        # initiate user device class
        user_devices = UserDevices(user.id, self.redis_cache, storage=self.storage)
        user_devices.clear_cache()

        # add elibrary device data! it's different application, so it should not counted
        user_devices.save(UserDeviceData(client_id=83, device_id='aabb::{}'.format(123),
                                         application_id=884))

        # add saved device data
        count = 1
        while count <= maximum-1:
            data = UserDeviceData(client_id=10001, device_id='kkyy::{}'.format(count),
                                  application_id=app_id)
            user_devices.save(data=data)
            count += 1
        # assert new device, still allowed, maximum device not reached yet
        self.assertIsNone(user_devices._assert_devices_per_application(device_id='kkkk2355'))

        # increase saved device data, so maximum reached
        data = UserDeviceData(client_id=g.current_client.id, device_id='kkyy::{}'.format(count),
                              application_id=app_id)
        user_devices.save(data)
        # assert new device, maximum device reached, error raised
        self.assertRaises(ErrorMaximumDeviceReached, user_devices._assert_devices_per_application,
                          'kkkk2355')

        # assert for device already registered, no error raised
        self.assertIsNone(user_devices._assert_devices_per_application(device_id='kkyy::1'),
                          'Device already registered, no error should returned')

    def test_assert_user_device_limit(self):
        session = db.session()
        user = UserFactory(id=32)

        app_id = 1
        maximum = 2
        application = session.query(Application).get(app_id)
        application.max_devices = maximum
        application.enable_unlimited_devices = False
        session.add(application)
        dummy_client = ClientFactory(id=100, application=application, platform_id=1)
        session.commit()
        g.current_client = dummy_client

        # initiate user device class
        user_devices = UserDevices(user.id, self.redis_cache, storage=self.storage)
        user_devices.clear_cache()

        # add saved device data
        count = 1
        while count <= maximum - 1:
            data = UserDeviceData(client_id=dummy_client.id, device_id='kkyy::{}'.format(count),
                                  application_id=app_id)
            user_devices.save(data=data)
            count += 1

        # still allowed, maximum device not reached yet
        self.assertIsNone(user_devices.assert_user_device_limit(device_id='kkkk2355'))

        # increase saved device data, so maximum reached
        data = UserDeviceData(client_id=g.current_client.id, device_id='kkyy::{}'.format(count),
                              application_id=app_id)
        user_devices.save(data)

        # maximum device reached, error raised
        self.assertRaises(ErrorMaximumDeviceReached, user_devices.assert_user_device_limit,
                          'abckkkk2355')

        # for device already registered, no error raised
        self.assertIsNone(user_devices.assert_user_device_limit(
            device_id='kkyy::1'),
            'Device already registered, no error should returned')

    def test_assert_device_still_registered(self):
        g.current_client = Client.query.get(1)
        user = UserFactory(id=33)
        db.session().commit()
        user_devices = UserDevices(user.id, self.redis_cache, storage=self.storage)
        user_devices.clear_cache()
        # add some devices
        count = 1
        while count <= 4:
            data = UserDeviceData(client_id=count, device_id='kkyy::{}'.format(count),
                                  application_id=123)
            user_devices.save(data=data)
            count += 1
        # device not found!, error raised
        self.assertRaises(UserDeviceNotFound, user_devices.assert_device_still_registered,
                          'abckkkk2355')
        # for device already registered, no error raised
        self.assertIsNone(
            user_devices.assert_device_still_registered(device_id='kkyy::1'),
            'Device already registered, no error should returned')

    def test_clear_cache(self):
        g.current_client = Client.query.get(1)
        user = UserFactory(id=45)
        db.session().commit()
        user_devices = UserDevices(user.id, self.redis_cache, storage=self.storage)
        user_devices.clear_cache()
        # add some devices

        # device from different app
        different_app = UserDeviceData(
            client_id=555, device_id='kkyy::{}'.format(555),
            application_id=551)
        user_devices.save(different_app)
        count = 1
        # device from the same app
        while count <= 4:
            user_devices.save(
                data=UserDeviceData(client_id=count, device_id='kkyy::{}'.format(count),
                                    application_id=g.current_client.application_id))
            count += 1
        user_devices.clear_cache()
        actual = user_devices.data
        self.assertEqual(actual, [])

    def test_remove_all(self):
        g.current_client = Client.query.get(1)
        user = UserFactory(id=44)
        db.session().commit()
        user_devices = UserDevices(user.id, self.redis_cache, storage=self.storage)
        user_devices.clear_cache()
        # add some devices

        # device from different app, should not removed
        different_app = UserDeviceData(
            client_id=555, device_id='kkyy::{}'.format(555),
            application_id=551)
        user_devices.save(different_app)
        count = 1
        # device from the same app, should be removed
        while count <= 4:
            user_devices.save(
                data=UserDeviceData(client_id=g.current_client.id, device_id='kkyy::{}'.format(count),
                                    application_id=g.current_client.application_id))
            count += 1
        user_devices.remove_all()
        if user_devices.storage == UserDeviceStorage.redis:
            actual = user_devices._get_cached_data()
            self.assertEqual(len(actual), 1)
            self.assertDictEqual(actual[0], different_app.serialize())
        else:
            actual = self.session.query(UserDevice).filter(UserDevice.user_id == user.id).all()
            self.assertEqual(len(actual), 1)
            self.assertEqual(actual[0].device_id[0], different_app.device_id)

    def test_remove_oldest_device(self):
        g.current_client = Client.query.get(1)
        user = UserFactory(id=55)
        db.session().commit()
        user_devices = UserDevices(user.id, self.redis_cache, storage=self.storage)
        user_devices.clear_cache()
        # add some devices
        oldest_data = UserDeviceData(client_id=1, device_id='kkyy::{}'.format(1),
                                     application_id=g.current_client.application_id)
        user_devices.save(data=oldest_data)
        count = 2
        while count <= 4:
            data = UserDeviceData(client_id=1, device_id='kkyy::{}'.format(count),
                                  application_id=g.current_client.application_id)
            user_devices.save(data=data)
            count += 1
        user_devices.remove_oldest_device()
        self.assertNotIn(oldest_data.device_id, [d.get('device_id') for d in user_devices._get_cached_data()])

    def test_is_apps_foundry_users(self):
        org_apps_foundry = OrganizationFactory(id=APPS_FOUNDRY_ORG, name='apps foundry')
        org_other1 = OrganizationFactory(id=9911, name='apa aja bro')
        org_other2 = OrganizationFactory(id=9912, name='apa aja bro 2')
        user_apps_foundry = UserFactory(id=56)
        user_apps_foundry.organizations.append(org_apps_foundry)
        user_apps_foundry.organizations.append(org_other1)
        db.session().commit()
        actual = is_apps_foundry_users(user_apps_foundry)
        self.assertTrue(actual)

        user_other = UserFactory(id=66)
        user_other.organizations.append(org_other2)
        db.session().commit()
        actual = is_apps_foundry_users(user_other)
        self.assertFalse(actual)


class UserDevicesFromDbTests(UserDevicesRedisTests):

    storage = UserDeviceStorage.db

    def test_get_user_device_data(self):
        with self.on_session(self.user):
            user_id = self.user.id

        g.current_client = self.session.query(Client).get(1)

        user_devices = UserDevices(user_id, self.redis_cache, storage=UserDeviceStorage.db)

        user_devices.clear_db_data_all()
        # make user device data already cleared
        prev_data = self.session.query(UserDevice).filter_by(user_id=user_id).all()
        self.assertEqual(len(prev_data), 0)
        ud = UserDevice(user_id=user_id, client_id='1', device_id=['abc123', 'abc245', ])
        self.session.add(ud)
        ud2 = UserDevice(user_id=user_id, client_id='2', device_id=['xyc123', 'xyz245', ])
        self.session.add(ud2)
        ud3 = UserDevice(user_id=user_id, client_id='20', device_id=['20xyc123', '20xyz245', ])
        self.session.add(ud3)
        self.session.commit()

        expected = [
            {'client_id': 1, 'device_id': 'abc123', 'application_id': 1},
            {'client_id': 1, 'device_id': 'abc245', 'application_id': 1},
            {'client_id': 2, 'device_id': 'xyc123', 'application_id': 1},
            {'client_id': 2, 'device_id': 'xyz245', 'application_id': 1},
        ]

        actual_from_main_method = user_devices._get_cached_data()
        self.assertEqual(len(actual_from_main_method), len(expected))
        self._assert_list_equal(actual_from_main_method, expected)

        actual = user_devices._get_cached_data_from_db(user_id)
        self.assertEqual(len(actual), len(expected))
        self._assert_list_equal(actual, expected)

    def test_save_user_device_data(self):
        user_devices = UserDevices(self.user.id, self.redis_cache, storage=self.storage)
        # clean up previous data if any
        user_devices.clear_cache()
        g.current_client = self.session.query(Client).get(1)
        data = UserDeviceData(client_id=1, device_id='da791a4b7e36aeed6e7dce6f186d6421zz',
                              application_id=1)
        user_devices.save(data)
        saved_data = self.session.query(UserDevice).filter_by(
            user_id=self.user.id,
            client_id='1',
        ).first()
        self.assertIsNotNone(saved_data)
        self.assertIn('da791a4b7e36aeed6e7dce6f186d6421zz', saved_data.device_id)

        # save another id
        data = UserDeviceData(client_id=1, device_id='kkyy1123455',
                              application_id=1)
        user_devices.save(data)
        saved_data2 = self.session.query(UserDevice).filter_by(
            user_id=self.user.id,
            client_id='1',
        ).first()
        self.assertIsNotNone(saved_data2)
        self.assertEqual(len(saved_data2.device_id), 2)
        self.assertIn('kkyy1123455', saved_data2.device_id)

        # save the same id again, it should not duplicate
        data = UserDeviceData(client_id=1, device_id='kkyy1123455',
                              application_id=1)
        user_devices.save(data)
        saved_data3 = self.session.query(UserDevice).filter_by(
            user_id=self.user.id,
            client_id='1',
        ).first()
        self.assertIsNotNone(saved_data3)
        self.assertEqual(len(saved_data3.device_id), 2)
        self.assertIn('kkyy1123455', saved_data3.device_id)

    def test_migrate_from_redis_to_db(self):
        # create some dummy data on redis
        self.mock_redis.flushdb()
        user_devices = UserDevices(self.user.id, self.redis_cache, storage=UserDeviceStorage.redis)
        count = 1
        while count <= 4:
            user_devices.save(
                data=UserDeviceData(client_id=1, device_id='kkyy::{}'.format(count),
                                    application_id=1))
            count += 1
        user_devices = UserDevices(self.regular_user.id, self.redis_cache, storage=UserDeviceStorage.redis)
        count = 11
        while count <= 15:
            user_devices.save(
                data=UserDeviceData(client_id=2, device_id='kkyy::{}'.format(count),
                                    application_id=1))
            count += 1

        user_devices_db = UserDevices(self.user.id, self.redis_cache, storage=UserDeviceStorage.db)
        # clean up previous data if any
        user_devices_db.clear_cache()

        # do migration process
        migrate_from_redis_to_db(self.redis_cache)

        # assert result
        saved_data = self.session.query(UserDevice).filter_by(
            user_id=self.user.id,
            client_id='1',
        ).first()
        self.assertIsNotNone(saved_data)
        self.assertEqual(len(saved_data.device_id), 4)
        self.assertIn('kkyy::3', saved_data.device_id)

        saved_data = self.session.query(UserDevice).filter_by(
            user_id=self.regular_user.id,
            client_id='2',
        ).first()
        self.assertIsNotNone(saved_data)
        self.assertEqual(len(saved_data.device_id), 5)
        self.assertIn('kkyy::13', saved_data.device_id)



