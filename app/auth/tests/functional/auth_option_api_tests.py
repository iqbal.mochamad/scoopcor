from unittest import TestCase

from app import app


class ApiWithOptionTests(TestCase):

    def test_for_some_api(self):
        c = app.test_client(use_cookies=False)

        # access without authentication, only GET allowed, PUT not allowed
        response = c.options('/v1/layouts')
        allowed_header = response.headers.get('allow', '').split(',')

        expected = ['OPTIONS', 'GET', 'HEAD']
        self.assertItemsEqual(expected, allowed_header,
                              'Expected {} vs actual {}'.format(expected, allowed_header))
