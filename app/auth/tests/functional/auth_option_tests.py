from __future__ import unicode_literals
from unittest import TestCase

from flask import g

from flask_appsfoundry.controllers import ListCreateApiResource
from nose.tools import nottest

from app import app, db
from app.auth.models import UserInfo
from app.users.organizations import Organization
from app.utils.controllers import CorAuthMixin
from app.users.tests.fixtures import RoleFactory, UserFactory


@nottest
class DummyClass(ListCreateApiResource, CorAuthMixin):
    get_security_tokens = ['allow get',]
    post_security_tokens = ['allow post']
    put_security_tokens = ['allow put']
    delete_security_tokens = ['allow delete']
    allowed_options = ['OPTIONS', 'GET', 'PUT', 'DELETE', 'POST', ]

    def get(self):
        pass

    def post(self):
        pass

    def put(self):
        pass

    def delete(self):
        pass


class OptionTests(TestCase):

    @classmethod
    def setUpClass(cls):
        db.session().close_all()
        db.drop_all()
        db.create_all()
        cls.init_data()

    @classmethod
    def tearDownClass(cls):
        db.session().expunge_all()
        db.session().close_all()
        db.drop_all()

    @classmethod
    def init_data(cls):
        session = db.session()
        org = Organization(id=40, name='FHM AFRIKA')

        role_super_user = RoleFactory(id=1, name='Super user')
        role = RoleFactory(id=11, name='normal user')

        cls.super_user = UserFactory(id=12345, username="Jojon")
        cls.super_user.roles.append(role_super_user)

        cls.super_user_info = UserInfo(cls.super_user.username,
                        "LUARBIASAIMNOTATOKENFORREAL",
                        cls.super_user.email,
                        ['can_read_write_global_all'])

        cls.user1 = UserFactory(id=22345, username="Doyok")
        cls.user1.roles.append(role)

        session.commit()

    def test_super_user(self):
        with app.test_request_context('/'):
            session = db.session()
            session.add(self.super_user)
            g.user = self.super_user_info
            g.current_user = self.super_user

            # initiate and reset dummy class
            dummy = DummyClass()
            dummy.allowed_options = ['OPTIONS', 'GET', 'PUT', 'DELETE', 'POST', ]
            # get allowed options in header
            response = dummy.options()
            allowed_header = response.headers.get('allow', '')
            session.expunge(self.super_user)
            self.assertIn('POST', allowed_header)
            self.assertIn('GET', allowed_header)
            self.assertIn('PUT', allowed_header)
            self.assertIn('DELETE', allowed_header)

    def test_super_user_with_limited_options(self):
        with app.test_request_context('/'):
            session = db.session()
            session.add(self.super_user)
            g.user = self.super_user_info
            g.current_user = self.super_user

            # initiate and reset dummy class
            dummy = DummyClass()
            dummy.allowed_options = ['OPTIONS', 'GET', 'PUT', 'WRONG_METHOD_NAME']
            # get allowed options in header
            response = dummy.options()
            allowed_header = response.headers.get('allow', '').split(',')
            expected = ['GET', 'HEAD', 'OPTIONS', 'PUT']
            session.expunge(self.super_user)
            self.assertItemsEqual(expected, allowed_header,
                                  'Expected {} vs actual {}'.format(expected, allowed_header))

    def test_allow_get(self):
        with app.test_request_context('/'):
            session = db.session()
            session.add(self.user1)
            g.current_user = self.user1
            g.user = UserInfo(self.user1.username,
                              "LUARBIASAIMNOTATOKENFORREAL",
                              self.user1.email,
                              ['allow get', ])

            # initiate and reset dummy class
            dummy = DummyClass()
            dummy.allowed_options = ['OPTIONS', 'GET', 'PUT', 'DELETE', 'POST', ]
            # get allowed options in header
            response = dummy.options()
            allowed_header = response.headers.get('allow', '').split(',')
            expected = ['GET', 'HEAD', 'OPTIONS', ]
            self.assertItemsEqual(expected, allowed_header,
                                  'Expected {} vs actual {}'.format(expected, allowed_header))

    def test_allow_get_empty_permission(self):
        with app.test_request_context('/'):
            # initiate and reset dummy class
            session = db.session()
            session.add(self.user1)
            g.current_user = self.user1
            dummy = DummyClass()
            dummy.allowed_options = ['OPTIONS', 'GET', 'PUT', 'DELETE', 'POST', ]
            # set get permission to empty (allowed for everyone)
            dummy.get_security_tokens = []
            # create user dont have permission, but will still allowed cause of empty get permission aboved
            g.user = UserInfo(self.user1.username,
                              "LUARBIASAIMNOTATOKENFORREAL",
                              self.user1.email,
                              ['allow ', ])
            # get allowed options in header
            response = dummy.options()
            allowed_header = response.headers.get('allow', '').split(',')
            expected = ['GET', 'HEAD', 'OPTIONS', ]
            self.assertItemsEqual(expected, allowed_header,
                                  'Expected {} vs actual {}'.format(expected, allowed_header))

    def test_allow_delete(self):
        with app.test_request_context('/'):
            session = db.session()
            session.add(self.user1)
            g.current_user = self.user1
            g.user = UserInfo(self.user1.username,
                              "LUARBIASAIMNOTATOKENFORREAL",
                              self.user1.email,
                              ['allow delete', ])

            # initiate and reset dummy class
            dummy = DummyClass()
            dummy.allowed_options = ['OPTIONS', 'GET', 'PUT', 'DELETE', 'POST', ]
            # get allowed options in header
            response = dummy.options()
            allowed_header = response.headers.get('allow', '').split(',')
            expected = ['DELETE', 'OPTIONS', ]
            self.assertItemsEqual(expected, allowed_header,
                                  'Expected {} vs actual {}'.format(expected, allowed_header))

    def test_allow_post(self):
        with app.test_request_context('/'):
            session = db.session()
            session.add(self.user1)
            g.current_user = self.user1
            g.user = UserInfo(self.user1.username,
                              "LUARBIASAIMNOTATOKENFORREAL",
                              self.user1.email,
                              ['allow post', ])

            # initiate and reset dummy class
            dummy = DummyClass()
            dummy.allowed_options = ['OPTIONS', 'GET', 'PUT', 'DELETE', 'POST', ]
            # get allowed options in header
            response = dummy.options()
            allowed_header = response.headers.get('allow', '').split(',')
            expected = ['POST', 'OPTIONS', ]
            self.assertItemsEqual(expected, allowed_header,
                                  'Expected {} vs actual {}'.format(expected, allowed_header))

    def test_allow_put(self):
        with app.test_request_context('/'):
            session = db.session()
            session.add(self.user1)
            g.current_user = self.user1
            g.user = UserInfo(self.user1.username,
                              "LUARBIASAIMNOTATOKENFORREAL",
                              self.user1.email,
                              ['allow put', ])

            # initiate and reset dummy class
            dummy = DummyClass()
            dummy.allowed_options = ['OPTIONS', 'GET', 'PUT', 'DELETE', 'POST', ]
            # get allowed options in header
            response = dummy.options()
            allowed_header = response.headers.get('allow', '').split(',')
            expected = ['PUT', 'OPTIONS', ]
            self.assertItemsEqual(expected, allowed_header,
                                  'Expected {} vs actual {}'.format(expected, allowed_header))
