from __future__ import unicode_literals, absolute_import

import json
from httplib import NO_CONTENT

import jwt
from flask import request
from flask import url_for
from flask import views, g, jsonify
from flask_appsfoundry.exceptions import Unauthorized, BadRequest, NotFound

from app import db
from app.auth.decorators import user_has_any_tokens
from app.auth.eperpus import assert_user_eperpus_general
from app.auth.helpers import verify_hash_password
from app.constants import RoleType
from app.users.users import User, Role
from app.utils.controllers import CorApiErrorHandlingMixin
from app.utils.datetimes import get_utc_nieve
from . import helpers, user_devices

class LoginApi(CorApiErrorHandlingMixin, views.MethodView):

    def post(self):
        request_data = json.loads(request.data)
        username = request_data.get('username', None)
        password = request_data.get('password', None)

        session = db.session()
        # verify user and password, if failed, raise Unauthorized error
        user = helpers.assert_user_password(username, password, session)
        assert_user_eperpus_general(session, user, client=getattr(g, 'current_client', None))

        # update user last login info
        user.last_login = get_utc_nieve()
        session.add(user)
        session.commit()

        device_id = request_data.get('device_id', None)
        return get_login_response(user, device_id)


class LoginGuestApi(CorApiErrorHandlingMixin, views.MethodView):

    @user_has_any_tokens('can_read_write_global_all', 'can_read_write_public_ext')
    def post(self):
        request_data = json.loads(request.data)
        if not request_data.get('unique_id', None):
            raise BadRequest(user_message='Please define unique guest id for username',
                             developer_message='Unique id is empty')

        username = email = '{}@guest.scoop'.format(request_data.get('unique_id', None))

        session = db.session()
        # check if user with this unique id already exists?
        user = helpers.get_user(username, session)
        if not user:
            # if not yet exists, create a new one
            user = User(
                username=username,
                email=email,
                is_active=True,
                is_verified=True,
                origin_client_id=g.current_client.id if hasattr(g, 'current_client') else None
            )
            user.set_password('22d7fe8c185003c98f97e5d6ced420c7')
            guest_role = session.query(Role).get(RoleType.guest_user.value)
            user.roles.append(guest_role)

        # update user last login info
        user.last_login = get_utc_nieve()
        session.add(user)
        session.commit()

        device_id = request_data.get('device_id', None)
        return get_login_response(user, device_id)


class VerifyUserPasswordApi(CorApiErrorHandlingMixin, views.MethodView):
    """ API use to verify user + password is correct, or user is valid
    example usage: used by device to change parental control --> ask password --> hit this api

    e2e test in: user_login_tests.py

    """
    def post(self):
        request_data = json.loads(request.data)
        username = request_data.get('username', None)
        password = request_data.get('password', None)

        session = db.session()
        # verify user and password, if failed, raise Unauthorized error
        helpers.assert_user_password(username, password, session)

        response = jsonify({})
        response.status_code = NO_CONTENT
        return response


def get_login_response(user, device_id):
    # check user-device count vs limit, if failed, raise BadRequest error, else register device
    device_id = device_id.strip() if device_id else None
    user_devices.UserDevices(user_id=user.id).assert_user_device_limit(
        device_id=device_id)

    # generate new JSON Web Token
    jwt_token = helpers.jwt_encode_from_user(
        user,
        device_id=device_id,
        expire_time=helpers.get_expire_time())
    payload = helpers.jwt_decode(jwt_token)
    return jsonify({
        'id': user.id,
        'token': jwt_token,
        'realm': 'JWT',
        'first_name': user.first_name,
        'last_name': user.last_name,
        'email': user.email,
        'expire_timedelta': payload.get('expire_timedelta', None),
        'href': url_for('users.details', user=user, _external=True),
        'expires': payload.get('exp', None),
        'is_verified': user.is_verified,
        'country_code': request.headers.get('GEOIP_COUNTRY_CODE', 'ID')
    })


class ConvertTokenBearerToJwt(CorApiErrorHandlingMixin, views.MethodView):
    """ Convert old bearer to JWT (with device_id data)
    use by front end to convert apps bearer with new JWT

    """

    @user_has_any_tokens('can_read_write_public', 'can_read_write_public_ext')
    def post(self):
        if not getattr(g, 'current_user', None):
            raise Unauthorized()
        request_data = json.loads(request.data)
        device_id = request_data.get('device_id', None)
        if not device_id:
            raise BadRequest(user_message='Device id is required',
                             developer_message='Device id is required')
        return get_login_response(g.current_user, device_id)


class RefreshTokenApi(CorApiErrorHandlingMixin, views.MethodView):

    def get(self):
        try:
            authorization = request.headers.get('Authorization', None)
            encoded_token = authorization.split()[1]

            new_token = helpers.jwt_refresh(encoded_token)
            payload = helpers.jwt_decode(new_token)

            # revalidate device id in this token vs registered devices..
            # if it's removed/revoked, not in list of user devices anymore -> raise error -> user should re login!
            user_devices.UserDevices(
                user_id=payload.get('user_id', None)
            ).assert_device_still_registered(
                device_id=payload.get('device_id', None)
            )

            return jsonify({
                'token': new_token,
                'expire_timedelta': payload.get('expire_timedelta', None),
                'expires': payload.get('exp', None),
                'realm': 'JWT',
            })
        except user_devices.UserDeviceNotFound:
            raise
        except jwt.ExpiredSignatureError:
            raise Unauthorized(user_message='Your session has expired',
                               developer_message='Token expired')
        except (jwt.DecodeError, jwt.InvalidTokenError) as e:
            raise Unauthorized(user_message='Token invalid',
                               developer_message='Token invalid, decode error: {}'.format(str(e)))
        except Exception as e:
            raise BadRequest(user_message='Failed to get authorization from request header',
                             developer_message=str(e))


class ChangePasswordApi(CorApiErrorHandlingMixin, views.MethodView):
    def put(self):
        request_data = json.loads(request.data)
        session = db.session()

        user = getattr(g, 'current_user', None)
        password = request_data.get('password', None)
        new_password = request_data.get('new_password', None)

        if not user:
            raise Unauthorized(user_message='Change password temporary not available, please try again later.',
                               developer_message='User not found, probably g service down.')

        valid = verify_hash_password(password, user)
        if not valid:
            raise BadRequest(user_message='The password you entered is incorrect',
                             developer_message='The password you entered is incorrect')

        if not new_password:
            raise BadRequest(user_message='Please define new password', developer_message='New password empty')

        user.set_password(new_password)
        session.add(user)
        session.commit()

        # remove all device from user device list, so user must logged in with new password
        user_devices.UserDevices(user_id=user.id).remove_all()
        return jsonify({}), NO_CONTENT


class ResetPasswordApi(CorApiErrorHandlingMixin, views.MethodView):

    def post(self):
        """Generate a reset password token based on user name/email"""
        request_data = json.loads(request.data)
        username = request_data.get('username', None)
        if not username:
            raise BadRequest(user_message='User name is required',
                             developer_message='username not found in request body')

        session = db.session()
        user = helpers.get_user(username, session)
        if not user:
            raise BadRequest(
                developer_message="Username not found",
                user_message="The username or email you entered is incorrect.")

        # generate reset password token, it will expired in 3 days
        token = helpers.get_reset_password_token(user=user)
        # save token to database just for our notes,
        #   in case user complain not able to reset, we can give them the reset url easily
        user.reset_password_token = token
        session.add(user)
        session.commit()

        x_user_agent = request.headers.get('X-User-Agent', None)
        if x_user_agent:
            # x-user-agent only coming from web reader and portal eperpus.
            # both of this doesn't have client record, so it needed to be public user agent
            user_agent = "eperpus_android"
        else:
            user_agent = request.headers.get('User-Agent', 'ebooks / 1.0').split('/')[0]

        helpers.send_reset_password_token_email(user, token, user_agent=user_agent.lower().strip().replace(" ", "_"))

        return jsonify({})

    def put(self):
        """ reset password with provided token """
        request_data = json.loads(request.data)
        token = request_data.get('reset_password_token', None)
        if not token:
            raise BadRequest(user_message='Reset password token is required',
                             developer_message='reset_password_token not found in request body')
        new_password = request_data.get('new_password', None)
        if not new_password:
            raise BadRequest(user_message='New password is required',
                             developer_message='new_password not found in request body')

        session = db.session()
        # validate token and get user from token, raise error if failed
        user = helpers.get_user_from_reset_password_token(token, session)

        # token valid, update user password
        user.password = user.hash_password(new_password)
        user.reset_password_token = None
        session.add(user)
        session.commit()

        # remove all device from user device list, so user must logged in with new password
        user_devices.UserDevices(user_id=user.id).remove_all()

        return jsonify({}), NO_CONTENT


class DeauthorizeApi(CorApiErrorHandlingMixin, views.MethodView):
    """ deauthorize / remove the oldest device from user devices list """

    def post(self):
        request_data = json.loads(request.data)
        username = request_data.get('username', None)
        password = request_data.get('password', None)

        session = db.session()
        # verify user and password, if failed, raise Unauthorized error
        user = helpers.assert_user_password(username, password, session)

        # remove the oldest device from user device list
        user_devices.UserDevices(user_id=user.id).remove_oldest_device()

        return jsonify({})


class DeauthorizeAllApi(CorApiErrorHandlingMixin, views.MethodView):
    """ deauthorize / remove All device from user devices list """

    def post(self):
        request_data = json.loads(request.data)
        username = request_data.get('username', None)
        password = request_data.get('password', None)

        session = db.session()
        # verify user and password, if failed, raise Unauthorized error
        user = helpers.assert_user_password(username, password, session)

        # remove all device from user device list
        user_devices.UserDevices(user_id=user.id).remove_all()

        return jsonify({})


class CurrentDevices(CorApiErrorHandlingMixin, views.MethodView):
    """ show current user devices based on user Token

    """

    @user_has_any_tokens('can_read_write_public', 'can_read_write_public_ext')
    def get(self):
        if not getattr(g, 'current_user', None):
            raise Unauthorized()
        data = user_devices.UserDevices(user_id=g.current_user.id).get_user_devices()
        return jsonify(data)


class GetUserDevices(CorApiErrorHandlingMixin, views.MethodView):
    """ show current user devices based on user id

    """

    @user_has_any_tokens('can_read_write_public', 'can_read_write_public_ext')
    def get(self, user_id):
        user = User.query.get(user_id)
        if not user:
            raise NotFound()
        data = user_devices.UserDevices(user_id=user.id).get_user_devices()
        return jsonify(data)
