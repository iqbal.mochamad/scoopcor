from __future__ import unicode_literals, absolute_import

import json
import random

import facebook
from flask import Blueprint, request, jsonify
from flask import g
from flask_appsfoundry.exceptions import UnprocessableEntity, Unauthorized
from marshmallow import fields

from app import ma, db
from app.auth.api import get_login_response
from app.constants import RoleType
from app.users.users import User, Role, Profile
from app.utils.marshmallow_helpers import schema_load
from . import user_devices, _log

blueprint = Blueprint('facebook', __name__, url_prefix='/v1')


class FacebookSchema(ma.Schema):
    fb_access_token = fields.Str(required=True)

    device_id = fields.Str(allow_none=True)
    origin_device_model = fields.Str(allow_none=True)
    origin_partner_id = fields.Int(allow_none=True)
    scope = fields.Str(allow_none=True)


@blueprint.route('/auth/facebook', methods=["POST", ])
def login_with_facebook():
    email, json_data, fb_profile = validate_fb_token()

    session = db.session()
    user = session.query(User).filter_by(email=email).first()
    if not user:
        user = User(
            username=email,
            email=email,
        )
        user.set_password(''.join([str(random.randint(1, 11)) for x in range(8)]))
        user.origin_client_id = g.current_client.id if getattr(g, 'current_client', None) else None

    if not user.fb_id:
        user.fb_id = fb_profile['id']
    if not user.is_verified:
        user.is_verified = True
        user_roles = [role.id for role in user.roles]
        if RoleType.unverified_user.value in user_roles:
            unverified_role = session.query(Role).get(RoleType.unverified_user.value)
            if unverified_role:
                unverified_role.users.remove(user)
        if RoleType.verified_user.value not in user_roles:
            verified_role = session.query(Role).get(RoleType.verified_user.value)
            if verified_role:
                verified_role.users.append(user)

    user.first_name = fb_profile.get('first_name', None) if not user.first_name else user.first_name
    user.last_name = fb_profile.get('last_name', None) if not user.last_name else user.last_name

    user_profile = session.query(Profile).filter_by(user_id=user.id).first()
    if not user_profile:
        user_profile = Profile(user_id=user.id)
        # mark source type = 2 from facebook
        user.source_type = 2
        user_profile.origin_device_model = json_data.get('origin_device_model')
        user_profile.origin_partner_id = json_data.get('origin_partner_id')

    if not user_profile.gender:
        user_profile.gender = 1 if fb_profile.get('gender', '') == 'male' else 2
    if not user_profile.birthdate:
        birthday = fb_profile.get('birthday', None)
        if birthday is not None:
            birthday = fb_profile['birthday'].split('/')
            birthday = '%s-%s-%s' % (birthday[2], birthday[0], birthday[1])
        user_profile.birthdate = birthday

    session.add(user)
    session.add(user_profile)
    session.commit()

    return get_login_response(user, device_id=json_data.get('device_id', None))


def validate_fb_token():
    json_data = schema_load(schema=FacebookSchema())

    fb_graph = facebook.GraphAPI(access_token=json_data.get('fb_access_token'), version="2.7")
    fb_profile = fb_graph.get_object("me", fields='id, name, email')
    email = fb_profile.get('email', None)
    if not email:
        raise UnprocessableEntity(
            user_message='Invalid facebook scope',
            developer_message='Email is not found using this scope')
    return email, json_data, fb_profile


@blueprint.route('/auth/facebook-deauthorize', methods=["POST", ])
def deauthorize_oldest_device():
    user = get_user_from_fb_token()

    user_devices.UserDevices(user_id=user.id).remove_oldest_device()

    return jsonify({})


@blueprint.route('/auth/facebook-deauthorize-all', methods=["POST", ])
def deauthorize_all():
    user = get_user_from_fb_token()

    # remove all device from user device list
    user_devices.UserDevices(user_id=user.id).remove_all()

    return jsonify({})


def get_user_from_fb_token():
    email, json_data, fb_profile = validate_fb_token()
    session = db.session()
    user = session.query(User).filter_by(email=email).first()
    if not user:
        raise Unauthorized(developer_message='Email for this facebook token is not found in user data')

    return user

