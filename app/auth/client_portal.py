import httplib
import json

from flask import request, Response
from flask_restful import Resource
from flask_appsfoundry.exceptions import NotFound, BadRequest
from marshmallow import fields
from sqlalchemy import desc, or_

from app import ma, db
from app.auth.models import Client
from app.auth.decorators import token_required
from app.helpers import generate_slug, regenerate_slug, err_response
from app.utils.marshmallow_helpers import schema_load


class ClientPortalSchema(ma.Schema):
    name = fields.String(required=True)
    app_name = fields.String(required=True)
    product_id = fields.String(required=True)

    flurry_key = fields.String(required=False)
    gai_key = fields.String(required=False)
    version = fields.String(required=False)
    lowest_supported_version = fields.String(required=False)
    lowest_os_version = fields.String(required=False)
    external_bundle_name = fields.String(required=False)
    firebase_server_key = fields.String(required=False)
    platform_id = fields.Integer(required=False)
    allow_age_restricted_content = fields.Boolean(default=False)
    is_active = fields.Boolean(default=True)

def slug_generator(data, name):
    slug = generate_slug(name)
    exist_data = Client.query.filter(Client.slug == slug).first()
    if not exist_data:
        data['slug'] = slug
    else:
        new_slug = regenerate_slug(Client, slug)
        data['slug'] = new_slug
    return data

class ClientPortalListApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_global_client, can_read_write_global_client')
    def get(self):
        args = request.args.to_dict()
        limit, offset, q, client_data = args.get('limit', 20), args.get('offset', 0), args.get('q', None), Client.query
        total_count = Client.query.count()

        if q:
            q = '{}{}{}'.format('%', q, '%')
            client_data = client_data.filter(Client.name.ilike(q))

        client_data = client_data.order_by(desc(Client.id)).limit(limit).offset(offset).all()
        json_response = {
            'clients': [u.revalues() for u in client_data],
            'metadata': {'resultset': {'offset': int(offset), 'limit': int(limit), 'count': total_count}}}

        return Response(json.dumps(json_response), status=httplib.OK, mimetype='application/json')

    @token_required('can_read_write_global_all, can_read_write_global_client')
    def post(self):
        data = schema_load(schema=ClientPortalSchema())
        name = data.get("name", "unknown")
        app_name = data.get("app_name", "unknown")
        firebase_server_key = data.get('firebase_server_key', None)

        exist_data = Client.query.filter(or_(Client.name == name, Client.app_name == app_name)).first()
        if exist_data:
            message = 'Invalid name or app-name already exist!.'
            return err_response(status=httplib.BAD_REQUEST, error_code=400101, developer_message=message,
                                user_message=message)

        data['client_id'] = 9999
        if firebase_server_key: data['firebase_server_key'] = 'key={}'.format(firebase_server_key)
        data = slug_generator(data, name)

        new_client = Client(**data)
        db.session.add(new_client)
        db.session.commit()

        # update client_id, this is fuck from legacy!
        new_client.client_id = new_client.id
        db.session.add(new_client)
        db.session.commit()

        return Response(json.dumps(new_client.revalues()), status=httplib.CREATED, mimetype='application/json')


class ClientPortalApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_global_client, can_read_write_global_client')
    def get(self, client_id=None):
        exist_data = Client.query.filter_by(id=client_id).first()
        if not exist_data: raise NotFound
        return Response(json.dumps(exist_data.revalues()), status=httplib.OK, mimetype='application/json')

    @token_required('can_read_write_global_all, can_read_write_global_client')
    def put(self, client_id):
        data = schema_load(schema=ClientPortalSchema())
        name = data.get("name", "unknown")
        app_name = data.get("app_name", "unknown")
        cl_exist_data_app, cl_exist_data_name = None, None

        exist_data = Client.query.filter_by(id=client_id).first()
        if not exist_data: raise NotFound

        if exist_data.name != name:
            cl_exist_data_name = Client.query.filter(Client.name == name).first()

        if exist_data.app_name != app_name:
            cl_exist_data_app = Client.query.filter(Client.app_name == app_name).first()

        if cl_exist_data_name or cl_exist_data_app:
            message = 'Invalid name or app-name already exist!.'
            return err_response(status=httplib.BAD_REQUEST, error_code=400101, developer_message=message,
                                user_message=message)

        for k, v in data.iteritems():
            setattr(exist_data, k, v)
        db.session.add(exist_data)
        db.session.commit()
        return Response(json.dumps(exist_data.revalues()), status=httplib.OK, mimetype='application/json')
