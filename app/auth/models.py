"""
Models
======

Some limited information to hold data about the current request user.
"""
from collections import namedtuple
from datetime import datetime, timedelta
from enum import IntEnum

from flask import url_for
from flask_appsfoundry.models import SoftDeletableMixin, TimestampMixin
from sqlalchemy import Column, DateTime
from sqlalchemy.dialects.postgresql import ARRAY

from app import db
from app.auth.constants import TOKEN_TTL
from app.utils.models import url_mapped


UserInfo = namedtuple('UserInfo', ['username', 'token', 'user_id', 'perm'])
""" Holds some basic information about a user that is logged into the system.
"""


ANONYMOUS_USER = UserInfo(username='anonymous', token=None, user_id=0, perm=[])
""" Represents a user that has not been authenticated.

There should only ever be a single instance of this user, so identity comparison
can be used.
"""


class Application(db.Model, SoftDeletableMixin, TimestampMixin):
    __tablename__ = 'applications'
    name = db.Column(db.String(250))
    max_devices = db.Column(db.Integer)
    enable_unlimited_devices = db.Column(db.Boolean, default=False)

    @property
    def api_url(self):
        return url_for('applications.details', entity_id=self.id, _external=True)


class Client(SoftDeletableMixin, TimestampMixin, db.Model):
    """ A single application client that accesses ScoopCOR APIs.
    ex: Scoop Apps IOS, Scoop Apps Android
    """
    __tablename__ = 'cas_clients'

    class Platform(IntEnum):
        ios = 1
        windows = 3
        android = 2
        web = 4

    client_id = db.Column(db.Integer, nullable=False)
    app_name = db.Column(db.String(250))
    client_secret = db.Column(db.String(80), doc='Generate automatic, use same method to generate token')
    redirect_url = db.Column(db.String(250))
    client_type = db.Column(db.String(80))
    flurry_key = db.Column(db.String(30))
    gai_key = db.Column(db.String(30))
    version = db.Column(db.String(20))
    lowest_supported_version = db.Column(db.String(20))
    lowest_os_version = db.Column(db.String(20))
    scopes = db.Column(ARRAY(db.Text))
    user_id = db.Column(db.Integer)
    name = db.Column(db.String(150))
    description = db.Column(db.String(250))
    external_bundle_name = db.Column(db.String(250))
    external_identifier = db.Column(db.Integer)
    url = db.Column(db.String(250))
    ip_address = db.Column(db.String(30), doc='ip address of a server (if only client is a server)')
    slug = db.Column(db.String(250), nullable=False)
    meta = db.Column(db.Text)
    product_id = db.Column(db.String(100))
    allow_age_restricted_content = db.Column(db.Boolean, default=True, nullable=False)
    platform_id = db.Column(db.Integer, db.ForeignKey('core_platforms.id'),
                            nullable=False, default=4)
    platform = db.relationship('Platform', backref='clients')
    application_id = db.Column(db.Integer, db.ForeignKey('applications.id'))
    application = db.relationship('Application', backref='clients')
    firebase_server_key = db.Column(db.Text)

    @property
    def api_url(self):
        return url_for('clients.details', entity_id=self.id, _external=True)

    def revalues(self):
        return {
            'id': self.client_id,
            'client_id': self.client_id,
            'name': self.name,
            'app_name': self.app_name,
            'flurry_key': self.flurry_key,
            'gai_key': self.gai_key,
            'version': self.version,
            'lowest_supported_version': self.lowest_supported_version,
            'lowest_os_version': self.lowest_os_version,
            'external_bundle_name': self.external_bundle_name,
            'is_active': self.is_active,
            'product_id': self.product_id,
            'allow_age_restricted_content': self.allow_age_restricted_content,
            'platform_id': self.platform_id,
            'firebase_server_key': self.firebase_server_key,
            'slug': self.slug
        }

class ClientVersion(db.Model, SoftDeletableMixin, TimestampMixin):
    """ A single release version of a Client application.
    """
    __tablename__ = 'cas_clientversions'

    client_id = db.Column(db.Integer, db.ForeignKey('cas_clients.id'), nullable=False)
    client = db.relationship('Client', backref=db.backref('versions', lazy='dynamic'))
    version = db.Column(db.String(10))
    enable_scoop_point = db.Column(db.Boolean)
    enable_referral = db.Column(db.Boolean, default=True)


@url_mapped('permissions.details', (('db_id', 'id'), ))
class Perm(TimestampMixin, db.Model):
    """ A single permission.

    Permissions are assigned to `Roles`, which are assigned to `Users`, which are used
    to determine if a `User` has sufficient rights to perform an action through the web API.
    """
    __tablename__ = 'cas_perms'
    name = db.Column(db.String(80), unique=True, nullable=False)
    description = db.Column(db.Text)


class Token(SoftDeletableMixin, db.Model):

    __tablename__ = 'cas_tokens'

    token = db.Column(db.String(250), nullable=False, unique=True)
    refresh_token = db.Column(db.String(250), nullable=False, unique=True)

    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    user = db.relationship('User', backref='tokens')

    valid_from = Column(DateTime, default=datetime.now)
    valid_to = Column(DateTime, default=lambda: datetime.now() + timedelta(seconds=TOKEN_TTL))

    perm_list = db.Column(ARRAY(db.Text), nullable=False)

