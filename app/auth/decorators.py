from __future__ import absolute_import, unicode_literals

import httplib
from functools import wraps
from warnings import warn

from flask import make_response, current_app, g
from flask_appsfoundry.exceptions import Unauthorized, Forbidden, BadRequest

from app.auth.exceptions import UnauthorizedError
from .helpers import assert_user_has_any_permission
from .models import ANONYMOUS_USER


SUPERUSER_TOKEN = 'can_read_write_global_all'


def user_is_authenticated(func):
    """ Raises 401 if the user is not authenticated.

    Decorates a method view function.

    :param `callable` func: The function that's being wrapped.
    :return: The wrapped function.
    """
    @wraps(func)
    def inner(*args, **kwargs):
        if g.user is ANONYMOUS_USER:
            raise Unauthorized
        return func(*args, **kwargs)

    return inner


def user_is_superuser(func):
    """ Raises 403 if the authenticated user is not a 'super user'

    This is based on having the 'can_read_write_global_all' perm.

    Raises a 401 if the user has not authenticated yet.

    :param `callable` func: The function that's being wrapped.
    :return: The wrapped function.
    """
    @user_is_authenticated
    @wraps(func)
    def inner(*args, **kwargs):
        if not SUPERUSER_TOKEN in g.user.perm:
            raise Forbidden
        return func(*args, **kwargs)

    return inner


def custom_headers(headers={}):
    """This decorator adds the headers passed in to the response"""
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            resp = make_response(f(*args, **kwargs))
            h = resp.headers
            for header, value in headers.items():
                h[header] = value
            return resp
        return decorated_function
    return decorator


def banned_permissions(perm):
    from app.helpers import log_api, err_response

    def decorated(func):

        def check_perm(*args, **kwargs):

            if current_app.testing:
                warn("App is in TEST mode. Auth checks disabled")
                return func(*args, **kwargs)

            banned_perms = [p.strip() for p in perm.split(',')]
            if any([(user_perm in banned_perms) for user_perm in g.user.perm]):
                raise BadRequest(user_message='You dont have have access',
                                 developer_message="Token {}, Invalid Permissions, banned user detected".format(
                                     "Invalid" if g.user is ANONYMOUS_USER else "Valid"))
            #
            # # split Bearer with token
            # authorization = request.headers.get('Authorization', '')
            # data_auth = authorization.split(' ')
            # user_id = base64.b64decode(data_auth[1]).split(':')[0]
            #
            # # get token user from redis
            # token = current_app.kvs.get(TOKEN_PREFIX + user_id)
            #
            # if token:
            #     # get name of api requests
            #     base_url = str(request.url.split('/')[::-1][0])
            #
            #     current_token = ast.literal_eval(token)
            #     token_permissions = current_token['perm']
            #
            #     static_perm = [i.strip() for i in perm.split(',')]
            #     for iperm in static_perm:
            #         if str(iperm) in token_permissions:
            #
            #             message = 'You dont have have access.'
            #
            #             if base_url in ['checkout', 'confirm']:
            #                 message = """
            #                         Sorry, you can't purchase the item with
            #                         this account. Please register or
            #                         sign in to your SCOOP ID account """
            #
            #             if base_url in ['restore']:
            #                 message = """The temporary account doesn't
            #                         allow you to restore items on your device."""
            #
            #             return err_response(status=httplib.BAD_REQUEST,
            #                 error_code=CLIENT_400101,
            #                 developer_message=message,
            #                 user_message=message)
            # else:
            #     return err_response(status=httplib.BAD_REQUEST,
            #         error_code=CLIENT_400101,
            #         developer_message="Can't retrieve token from redis",
            #         user_message="You dont have have access.")
            return func(*args, **kwargs)
        return check_perm
    return decorated


def token_required(perms):

    def decorated(func):
        @wraps(func)
        def inner(*args, **kwargs):

            # Major Blocked.!! due violation regulations.
            if 'cant_access_protocol' in g.user.perm:
                from app.reports.blacklist import check_user_banned
                message, response = check_user_banned(g.user.user_id)
                return response

            if current_app.testing:
                warn("App is in TEST mode. Auth checks disabled")
            else:
                required_permissions = [perm.strip() for perm in perms.split(',')]
                assert_user_has_any_permission(required_permissions=required_permissions, user_info=g.user)
            return func(*args, **kwargs)
        return inner
    return decorated


def user_has_any_tokens(*tokens):
    """ Decorator that asserts the user has at least one of the listed tokens.

    :param `str` tokens: The token strings to check.
    :return: Wrapped function.
    :raises: BadRequest
    """
    def decorated(func):
        @wraps(func)
        def inner(*args, **kwargs):

            if current_app.testing:
                warn("App is in TEST mode.  Auth checks disabled")
            else:
                assert_user_has_any_permission(
                    required_permissions=tokens,
                    user_info=g.user
                )
            return func(*args, **kwargs)
        return inner

    return decorated


def blank_test(req, user, *args, **kwargs):
    pass


def user_passes_tests(*tests):

    def wrapper(func):
        @wraps(func)
        def inner(*args, **kwargs):
            return func(*args, **kwargs)
        return inner
    return wrapper

