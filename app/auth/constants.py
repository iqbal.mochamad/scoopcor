from __future__ import unicode_literals


TOKEN_ISSUER = "SCOOP"
TOKEN_RESET_PASSWORD_SUBJECT = "reset password"

# TTL of token in seconds
TOKEN_TTL = 86400
TOKEN_BYTES = 40
TOKEN_PREFIX = 'tok:'

REFRESH_TTL = 86400
REFRESH_PREFIX = 'rsh:'

SECRET_TTL = 86400
SECRET_BYTES = 32
SECRET_PREFIX = 'scr:'

SCOPE_DEFAULT = ['can_read_write_public']

# Length of scrypt hash of passwords
PW_HASH_BYTES = 64

# Length of salt
PW_SALT_BYTES = 32

MR = 1
MRS = 2
MISS = 3
MDM = 4
DR = 5


TITLE_TYPE = {
    MR: 'Mr.',
    MRS: 'Mrs.',
    MISS: 'Ms',
    MDM: 'Mdm.',
    DR: 'Dr.'
}

