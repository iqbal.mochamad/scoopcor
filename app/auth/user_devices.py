from __future__ import unicode_literals, absolute_import

import hashlib
import json
import random
from datetime import datetime
from httplib import CREATED

from enum import Enum
from flask import current_app, g, jsonify, views
from flask_appsfoundry.exceptions import ScoopApiException, Unauthorized, UnprocessableEntity
from marshmallow import fields

from app import db, ma
from app.auth import _log
from app.auth.models import Client, Application
from app.master.choices import IOS, ANDROID
from app.notification.ebooks.helpers import deauthorization_remove_notification_devices
from app.notification.helpers import remove_notification_devices
from app.users.user_devices import Device, UserDevice
from app.users.users import User, is_apps_foundry_users
from app.utils.controllers import CorApiErrorHandlingMixin
from app.utils.datetimes import get_local_nieve
from app.utils.marshmallow_helpers import schema_load
from app.utils.redis_cache import RedisCache

UNLIMITED_DEVICE_USERS = [
    'apple.review@noemail.scoop.com', 'apple.review1@noemail.scoop.com',
    'apple.review2@noemail.scoop.com', 'apple.review3@noemail.scoop.com',
    'scoopios@client.scoop', 'scoopandroid@client.scoop', 'smartlibrarysd@gmail.com',
    'smartlibrarysmp@gmail.com', 'smartlibrarysma@gmail.com', 'smartlibrarysmk@gmail.com',
    'marketingscoopcoretest@gmail.com', 'scoopcoretest@gmail.com'
]


class UserDeviceStorage(Enum):
    redis = 'redis'
    db = 'db'


# before changing this value, make sure to transfer data from previous storage to new storage
# run method migrate_from_redis_to_db  (to migrate data from redis to database)
#  User Device Storage in Redis as of now has a problem: data is gone/wiped out sometime (intermittent)
USER_DEVICE_STORAGE = UserDeviceStorage.db


class UserDevices(object):

    def __init__(self, user_id, redis_cache=None, storage=None):
        self.storage = storage or USER_DEVICE_STORAGE
        self.redis_cache = redis_cache if redis_cache else RedisCache(current_app.kvs, expiry=None)
        # sometime g has no attribute current client, we fix this here (set to None if not found)
        self.current_client = getattr(g, 'current_client', None)
        self.session = db.session()
        if self.current_client:
            self.session.add(self.current_client)
        if user_id and (not getattr(g, 'current_user', None) or g.current_user.id != user_id):
            self.user = self.session.query(User).get(user_id)
        else:
            self.user = g.current_user
        self.current_client_id = self.current_client.id if self.current_client else None
        self.current_application = self.current_client.application if self.current_client else None

        self.key = self._get_key(user_id=user_id)
        self._data = None

    @property
    def data(self):
        if not self._data:
            self._data = self._get_cached_data()
        return self._data

    @staticmethod
    def _get_key(user_id):
        return 'user_devices:{user_id}'.format(user_id=user_id)

    def assert_user_device_limit(self, device_id):
        """ assert new user device vs limit (already registered device), and register this new device if applicable.
        used in user login api to validate user device against limit.


        :param `string` device_id:
        :return `app.exceptions.BadRequest` if user device exceed limit
            return None if limit not exceeded
        """
        if not self.current_client:
            return

        if is_apps_foundry_users(user=self.user) or self.user.email.lower() in UNLIMITED_DEVICE_USERS:
            # for user registered in Apps foundry organization, unlimited device allowed
            #  or user for Apple Reviewer --> ignore device count
            return

        if self.current_application and self.current_application.enable_unlimited_devices:
            # login from Unlimited Device Apps (eg: scoop_web, and scoopportal device) not count
            return

        device_id = device_id.strip() if device_id else None
        if g.current_client and g.current_client.slug == 'web-reader':
            # if user from web-reader
            pass
        else:
            self._assert_devices_per_application(device_id)

        self.save(data=self._get_current_data(device_id=device_id))

    def assert_device_still_registered(self, device_id):
        if not device_id or is_apps_foundry_users(user=self.user) or self.user.email.lower() in UNLIMITED_DEVICE_USERS:
            # for user registered in Apps foundry organization, unlimited device allowed
            return

        device_id = device_id.strip()

        if self.current_application and self.current_application.enable_unlimited_devices:
            # login from Unlimited Device Apps (eg: scoop_web, and scoopportal device) not count
            return

        device_valid = False
        if self.storage == UserDeviceStorage.db:
            existing_devices = self.session.query(UserDevice).filter(
                UserDevice.user_id == self.user.id,
                UserDevice.device_id.any(device_id))
            if self.current_client_id:
                # make sure device id exists in this Client (from user agent)
                existing_devices = existing_devices.filter(
                    UserDevice.client_id == str(self.current_client_id)
                )
            existing_devices = existing_devices.all()
            if existing_devices:
                device_valid = True
        else:
            current_devices = [x.get('device_id', None) for x in self.data]
            if device_id in current_devices:
                device_valid = True

        if not device_valid:
            # raise UserDeviceNotFound(developer_message="device id {device_id} not exists {client_info}".format(
            #     device_id=device_id,
            #     client_info="in client id {}".format(self.current_client_id) if self.current_client_id else None
            # ))

            # try to re-register/re-save user device data (if max device not limited yet) -- start
            #   this is temporary patch, until all redis/missing data is re-saved to database
            #       (maybe one-two week's, after that remove this part of code)
            #       this to prevent case of forced log out for eperpus apps.
            if getattr(g, 'current_client', None) and g.current_client.id not in (IOS, ANDROID):
                    # and datetime.now() < datetime(2017, 10, 1):
                try:
                    self.assert_user_device_limit(device_id)
                except ErrorMaximumDeviceReached as ex:
                    raise UserDeviceNotFound
                except Exception:
                    raise UserDeviceNotFound
            else:
                pass
                # raise UserDeviceNotFound(developer_message="device id {device_id} not exists {client_info}".format(
                #     device_id=device_id,
                #     client_info="in client id {}".format(self.current_client_id) if self.current_client_id else None
                # ))
            # try to re-register/re-save user device data (if max device not limited yet) -- end

    def remove_oldest_device(self):
        # remove all device in one go -- start
        # this is temporary patch, until all redis/missing data is re-saved to database
        #   so user don't need to remove device multiple times.
        # remove this code after not used anymore
        if getattr(g, 'current_client', None) and g.current_client.id not in (IOS, ANDROID) \
                and datetime.now() < datetime(2017, 10, 1):
            self.remove_all()
            return
        # remove all device in one go -- end

        # remove oldest device from the same application (based on g.current_client)
        if self.data:
            application_id = self.current_application.id if self.current_application else None
            current_devices_per_app = [
                x for x in self.data
                if x and x.get('application_id', '') == application_id]
            if current_devices_per_app:
                self._data.remove(current_devices_per_app[0])
                if self.storage == UserDeviceStorage.redis:
                    self.redis_cache.write(self.key, self._data)
                else:
                    self._remove_oldest_device_from_db(application_id)

    def _remove_oldest_device_from_db(self, application_id):
        user_devices_data = self._user_device_db_query(self.user.id, application_id)
        for ud in user_devices_data:
            if ud.device_id:
                # must create a new list by copying from the old data, if not, save will failed
                device_ids = list(ud.device_id)
                device_ids.remove(ud.device_id[0])
                ud.device_id = device_ids
                if ud.device_id:
                    self.session.add(ud)
                else:
                    # device id on this user + client id is empty
                    self.session.delete(ud)
                self.session.commit()

    def remove_all(self):
        """
        remove all device from the SAME application GROUP (based on g.current_client.application_id)
        only for one user
         to remove ALL device for ALL application, use: CLEAR_CACHE method below

        :return:
        """

        if self.data:
            # remove from self.data attribute
            application_id = self.current_application.id if self.current_application else None
            current_devices_per_app = [
                x for x in self.data
                if x and x.get('application_id', '') == application_id]
            for device_per_app in current_devices_per_app:
                self._data.remove(device_per_app)

            # remove from storage
            if self.storage == UserDeviceStorage.redis:
                self.redis_cache.write(self.key, self._data)
            else:
                self._remove_all_from_db(application_id)

            try:
                remove_notification_devices(self.user.id, self.current_application)

                if self.current_client_id in [ANDROID, IOS]:
                    deauthorization_remove_notification_devices(self.user.id)

            except Exception:
                _log.exception("Failed to remove notification devices")
                pass

    def _remove_all_from_db(self, application_id):
        """
        remove data from database
        remove all device from the SAME application GROUP (based on g.current_client.application_id)
        only for one user
         to remove ALL device for ALL application, use: CLEAR_CACHE method below

        :param `int` application_id
        :return:
        """
        client_ids = self.get_client_ids(application_id)
        if client_ids:
            sql = "delete From cas_userdevices where user_id = {user_id} and client_id in ('{client_id}');".format(
                user_id=self.user.id,
                client_id="','".join(client_ids)
            )
            db.engine.execute(sql)
        # user_devices_data = self._user_device_db_query(self.user.id, application_id)
        # for ud in user_devices_data:
        #     self.session.delete(ud)
        #     self.session.commit()

    def clear_cache(self):
        # remove all device data for this user (for ALL Application Group)
        if self.storage == UserDeviceStorage.redis:
            self.redis_cache.delete(self.key)
        else:
            self.clear_db_data_all()
        # refresh data
        self._data = self._get_cached_data()

    def clear_db_data_all(self):
        """ remove all device data of a user in database

        :return:
        """
        self.session.query(UserDevice).filter_by(user_id=self.user.id).delete()
        self.session.commit()

    def save(self, data):
        """ Store user device data to redis cache or db

        :param `UserDeviceData` data:
        """
        # save client and device data to redis
        current_value = data.serialize()
        if current_value not in self.data:
            self._data.append(current_value)
            if self.storage == UserDeviceStorage.redis:
                self.redis_cache.write(self.key, self._data)
            else:
                self._save_to_db(data)

    def _save_to_db(self, data):
        """ Store user device data to db
            User Device data structure is still the same with CAS (UserDevice.client_id is a string),
                not enough time is available to change UserDevice.client_id to int (no time to change CAS too)
                so we follow old code behaviour here
        :param `UserDeviceData` data:
        """
        user_device_data = self.session.query(UserDevice).filter(
            UserDevice.user_id == self.user.id,
            UserDevice.client_id == str(data.client_id) if data.client_id else None
        ).order_by(UserDevice.id).first()
        if user_device_data:
            if g.current_client and g.current_client.slug == 'web-reader':
                # replace old device id using new one
                device_ids = [data.device_id]
                user_device_data.device_id = device_ids
                self.session.add(user_device_data)
                self.session.commit()
            # must create a new list by copying from the old data, if not, save will failed
            else:
                device_ids = list(user_device_data.device_id)
                if data.device_id not in device_ids:
                    # if not yet exists, save to db
                    device_ids.append(data.device_id)
                    user_device_data.device_id = device_ids
                    self.session.add(user_device_data)
                    self.session.commit()
        else:
            # create new line for this user + client id
            ud = UserDevice(
                user_id=self.user.id,
                client_id=str(data.client_id),
                device_id=[data.device_id, ]
            )
            self.session.add(ud)
            self.session.commit()

    def _get_current_data(self, device_id):
        return UserDeviceData(
            client_id=self.current_client_id,
            device_id=device_id,
            application_id=self.current_application.id if self.current_application else None,
        )

    def _get_cached_data(self):
        if self.storage == UserDeviceStorage.redis:
            cached_data = self.redis_cache.get(self.key)
            return json.loads(cached_data) if cached_data else []
        else:
            return self._get_cached_data_from_db(self.user.id)

    def _get_cached_data_from_db(self, user_id):
        """ get user devices data from database (convert from old format to current format (list of UserDeviceData))

        :param user_id:
        :return:
        """
        application_id = self.current_application.id if self.current_application else None
        user_devices = self._user_device_db_query(user_id, application_id)
        return self._db_to_list_user_devices(user_devices, application_id)

    def _db_to_list_user_devices(self, user_devices, application_id=None):
        data = []
        if user_devices:
            for ud in user_devices:
                if ud.client_id == str(self.current_client_id):
                    self.current_client_devices_in_db = ud.device_id
                if application_id:
                    # data source already filter by application id, we don't need to re-query application id from Client
                    client_application_id = application_id
                else:
                    client = self.session.query(Client).get(int(ud.client_id))
                    client_application_id = client.application_id
                for d in ud.device_id:
                    data.append(UserDeviceData(
                        client_id=int(ud.client_id) if ud.client_id else None,
                        device_id=d,
                        application_id=client_application_id
                    ).serialize())
            return data
        else:
            return []

    def get_client_ids(self, application_id):
        clients = self.session.query(Client).filter_by(application_id=application_id).all() if application_id else None
        if clients:
            return ['{}'.format(c.id) for c in clients]
        else:
            return self.current_client_id

    def _user_device_db_query(self, user_id, application_id):
        """ get user devices data for the same application id and user_id

        :param user_id:
        :return:
        """
        client_ids = self.get_client_ids(application_id)
        if client_ids:
            return self.session.query(UserDevice).filter(
                UserDevice.user_id == user_id,
                UserDevice.client_id.in_(client_ids)).all()
        else:
            return None

    def _assert_devices_per_application(self, device_id):
        current_value = self._get_current_data(device_id=device_id)

        if current_value.serialize() not in self.data:
            current_devices_per_app = [
                x.get('device_id', None) for x in self.data
                if x and x.get('application_id', '') == current_value.application_id]

            if self.current_application:
                enable_unlimited_devices = self.current_application.enable_unlimited_devices
                max_devices = self.current_application.max_devices
            else:
                enable_unlimited_devices = False
                max_devices = None

            if not max_devices:
                # do not joined this to above else if..
                # it's a last resorst in case max_device in current_application is not maintained properly
                # (self.current_application.max_devices == null/None)
                max_devices = current_app.config.get('MAX_DEVICE_ALLOWED', 5)

            if not enable_unlimited_devices and len(current_devices_per_app) >= max_devices:
                raise ErrorMaximumDeviceReached(
                    developer_message='Maximum number of device is {}'.format(
                        max_devices
                    )
                )

    def get_user_devices(self):
        # get all devices data for a user (for all client/application)
        if self.storage == UserDeviceStorage.redis:
            cached_data = self.redis_cache.get(self.key)
            user_devices = json.loads(cached_data) if cached_data else []
        else:
            user_devices_db = self.session.query(UserDevice).filter(
                UserDevice.user_id == self.user.id, ).order_by(UserDevice.client_id).all()
            user_devices = self._db_to_list_user_devices(user_devices_db, application_id=None)
        data_list = []
        if user_devices:
            for ud in user_devices:
                data = filter(lambda e: e['application_id'] == ud.get('application_id'), data_list)
                if not data:
                    # not found, add new
                    application = self.session.query(Application).get(ud.get('application_id'))
                    data_list.append({
                        "application_id": ud.get('application_id'),
                        "name": application.name,
                        "max_devices": application.max_devices,
                        "enable_unlimited_devices": application.enable_unlimited_devices,
                        "clients": [{
                            "id": c.id,
                            "name": c.name,
                        } for c in application.clients
                        ],
                        "device_count": 1,
                        "devices": [ud, ]
                    })
                else:
                    app_device = data[0]
                    app_device['device_count'] += 1
                    app_device['devices'].append(ud)
        return data_list


class UserDeviceData(object):

    def __init__(self, client_id, device_id, application_id):
        """

        :param `int` client_id:
        :param `six.text_type` device_id:
        :param `int` application_id: application id
        """
        self.client_id = client_id
        self.application_id = application_id
        self.device_id = device_id

    def serialize(self):
        return self.__dict__


class DeviceApi(CorApiErrorHandlingMixin, views.MethodView):

    def post(self):
        device_schema = DeviceSchema()
        entity = schema_load(schema=device_schema)

        device_imei = entity.get('device_imei', None)
        if device_imei and len(device_imei) > 20:
            raise UnprocessableEntity(
                user_message='Device imei more than 20 digits',
                developer_message='Register device failed. Device imei more than 20 digits')

        device_model = entity.get('device_model', None)
        device_mid = entity.get('device_mid', None)
        session = db.session()
        device = None
        if device_imei:
            device = session.query(Device).filter_by(
                device_imei=device_imei).first()
            if not device and device_mid and device_model:
                device = session.query(Device).filter_by(
                    device_mid=device_mid, device_model=device_model).first()

        if not device:
            unique_id = self._generate_uid(device_imei)
            device = Device(
                unique_id=unique_id,
                device_imei=device_imei,
                device_model=device_model,
                device_mid=device_mid
            )
            session.add(device)
            session.commit()
        resp = jsonify(device_schema.dump(device).data)
        resp.status_code = CREATED
        return resp

    @staticmethod
    def _generate_uid(device_imei):
        random_key = ['6shd93hd83h8fjdoeuejfueijdidi',
                      'asldnakjshfahsd883kf0hwe0rejd',
                      'i7sdyfKUHKUH8fioshfiowfHUHUcf',
                      'ak72usi819iasjkmbhdl3IO920lsk',
                      'oaly2Uijsn8Ikxnl0lakmnGtsjklu']
        token_key = hashlib.md5("{}{}{}".format(
            current_app.config['SECRET_GENERATE_TOKEN_KEY'],
            str(get_local_nieve()),
            device_imei
        )).hexdigest()
        return '{}{}'.format(random.choice(random_key), token_key)


class DeviceSchema(ma.Schema):
    unique_id = fields.Str(dump_only=True)
    device_mid = fields.Str(required=False, allow_none=True)
    device_imei = fields.Str(required=False, allow_none=True)
    device_model = fields.Str(required=False, allow_none=True)


class ErrorMaximumDeviceReached(ScoopApiException):

    def __init__(self, **kwargs):
        super(ErrorMaximumDeviceReached, self).__init__(
            code=420,
            error_code=420,
            user_message=kwargs.get(
                "user_message",
                'You have reached maximum number of authorized devices.\n'
                'Please deauthorized first then re-login'),
            developer_message=kwargs.get(
                "developer_message",
                'You have reached maximum number of authorized devices.\n'
                'Please deauthorized first then re-login')
        )


class UserDeviceNotFound(Unauthorized):

    def __init__(self, **kwargs):
        super(UserDeviceNotFound, self).__init__(
            user_message=kwargs.get(
                "user_message",
                'Your access in this device has been revoked, please login again'),
            developer_message=kwargs.get(
                "developer_message",
                'Your access in this device has been revoked, please login again')
        )


def migrate_from_redis_to_db(redis_cache=None):
    redis_cache = redis_cache or RedisCache(current_app.kvs, expiry=None)
    key = 'user_devices'
    for cached_key in redis_cache.redis.scan_iter('{}*'.format(key)):
        user_id = cached_key.split(':')[1]
        user_id = int(user_id)

        cached_data = redis_cache.get(cached_key)
        data_devices = json.loads(cached_data)
        ud_db = UserDevices(user_id=user_id, redis_cache=redis_cache, storage=UserDeviceStorage.db)
        for data in data_devices:
            ud_db.save(UserDeviceData(
                client_id=data.get('client_id'),
                device_id=data.get('device_id'),
                application_id=data.get('application_id'), ))
