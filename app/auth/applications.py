from flask.ext.appsfoundry.parsers import SqlAlchemyFilterParser
from marshmallow import fields
from marshmallow_sqlalchemy import ModelSchema

from app import db
from app.auth.models import Application
from app.utils.marshmallow_helpers import MaGetEditBase, MaListResponse
from app.utils.parser_fields import WildcardFilter


class ApplicationSchema(ModelSchema):

    class Meta:
        model = Application
        sqla_session = db.session
        exclude = []

    # title = fields.Str(attribute='name')
    href = fields.Str(attribute='api_url', dump_only=True)


class ApplicationSimpleSchema(ModelSchema):

    class Meta:
        model = Application
        sqla_session = db.session
        fields = ['id', 'name', 'href']

    name = fields.Str(dump_only=True)
    href = fields.Str(attribute='api_url', dump_only=True)


LIST_TOKENS = ['can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext',
               'can_read_write_global_client', 'can_read_global_client']
MODIFY_TOKENS = ['can_read_write_global_all', 'can_read_write_global_client']


class DefinitionMixin(object):
    model = Application
    schema = ApplicationSchema()

    get_security_tokens = LIST_TOKENS
    edit_security_tokens = MODIFY_TOKENS


class ApplicationApi(DefinitionMixin, MaGetEditBase):
    """ GET API (Get single), PUT API (Update), DELETE API
    """


class ApplicationListArgsParserLists(SqlAlchemyFilterParser):
    __model__ = Application
    q = WildcardFilter(dest='name')


class ApplicationListApi(DefinitionMixin, MaListResponse):
    """ LIST API (search), POST API (Create)
        """
    key_name = "data"
    filter_parsers = ApplicationListArgsParserLists()
