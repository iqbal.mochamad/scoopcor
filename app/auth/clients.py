from flask import g
from flask import views, jsonify
from flask_appsfoundry.exceptions import NotFound
from flask_appsfoundry.parsers import SqlAlchemyFilterParser
from marshmallow import fields, post_dump, post_load
from marshmallow import pre_load
from marshmallow_sqlalchemy import ModelSchema
from sqlalchemy import desc

from app import db, ma
from app.auth.applications import ApplicationSimpleSchema
from app.auth.models import Client, ClientVersion
from app.master.models import Platform
from app.users.organizations import Organization
from app.utils.marshmallow_helpers import MaGetEditBase, MaListResponse, create_slug, SimpleNameHrefMixin
from app.utils.shims.parsers import WildcardFilter


class PlatformSimpleSchema(SimpleNameHrefMixin, ModelSchema):
    class Meta:
        model = Platform
        sqla_session = db.session
        fields = ['id', 'name', 'href']


class ClientVersionSchema(ModelSchema):
    class Meta:
        model = ClientVersion
        sqla_session = db.session

    # title = fields.Str(attribute='name')
    client_id = fields.Int(load_only=True)
    client = fields.Nested('ClientSchema', only=('id', 'name', 'app_name'))

    created = fields.DateTime(dump_only=True)
    modified = fields.DateTime(dump_only=True)

    @pre_load
    def pre_load(self, data):
        data['client_id'] = data.get('client').get('id', data.get('client_id', None)) if data.get('client') else None
        data.pop('client', None)
        return data


class OrganizationSimpleSchema(ModelSchema):
    class Meta:
        model = Organization
        sqla_session = db.session
        fields = ('id', 'name', 'type')


class ClientSchema(ModelSchema):

    class Meta:
        model = Client
        sqla_session = db.session
        exclude = ['client_id', 'platform_id', 'application_id', 'client_secret']

    # title = fields.Str(attribute='name')
    slug = fields.Str(dump_only=True)
    href = fields.Str(attribute="api_url", dump_only=True)
    client_secret = fields.Str(dump_only=True)
    application = fields.Nested(ApplicationSimpleSchema)
    platform = fields.Nested(PlatformSimpleSchema)
    organizations = fields.Nested(OrganizationSimpleSchema, dump_only=True, many=True)

    # client_versions = fields.Nested(ClientVersionSchema, dump_only=True, many=True,
    #                          only=('id', 'version', 'enable_scoop_point', 'enable_referral'))
    created = fields.DateTime(dump_only=True)
    modified = fields.DateTime(dump_only=True)

    # TODO: Process client_secret?
    @pre_load
    def preprocess_data(self, data):
        # if not getattr(data, 'client_id', None):
        #     data['client_id'] =
        pass

    @post_dump
    def post_dump(self, data):
        versions = ClientVersion.query.filter_by(
            client_id=data.get('id'), is_active=True).order_by(desc(ClientVersion.id)).all()
        version_schema = ClientVersionSchema()
        version_schema.exclude = ['client']
        # data['client_versions'] = [version_schema.dump(v).data for v in versions] if versions else None
        data['client_versions'] = version_schema.dump(versions, many=True).data if versions else None


LIST_TOKENS = ['can_read_write_global_all', 'can_read_write_public', 'can_read_write_public_ext',
               'can_read_write_global_client', 'can_read_global_client']
MODIFY_TOKENS = ['can_read_write_global_all', 'can_read_write_global_client']


class DefinitionMixin(object):
    model = Client
    schema = ClientSchema()

    get_security_tokens = LIST_TOKENS
    edit_security_tokens = MODIFY_TOKENS


class ClientApi(DefinitionMixin, MaGetEditBase):
    """ GET API (Get single), PUT API (Update), DELETE API
    """

    def _before_commit(self, data, session, **kwargs):
        if not data.slug:
            data.slug = create_slug(data.name, Client, session)
        if not data.client_id:
            data.client_id = data.id


class ClientArgsParserLists(SqlAlchemyFilterParser):
    __model__ = Client
    q = WildcardFilter(dest='name')


class ClientListPostApi(DefinitionMixin, MaListResponse):
    """ LIST API (search), POST API (Create)
        """
    key_name = "clients"
    filter_parsers = ClientArgsParserLists()
    # no token required to access list API
    get_security_tokens = None

    def _before_commit(self, data, session, **kwargs):
        if not data.slug:
            data.slug = create_slug(data.name, Client, session)
        # work arround, since client_id must be same with id and not allowed null
        data.client_id = -999
        session.commit()
        session.refresh(data)
        data.client_id = data.id
        session.add(data)


class CurrentClientApi(views.MethodView):
    """ Returns metadata about the user's client application.

    If the API hasn't been able to lookup the user's current client application, a 404 will be returned.
    """
    def get(self):
        if getattr(g, 'current_client', None) is None:
            raise NotFound

        return jsonify({
            'app_name': g.current_client.app_name
        })


class ClientVersionDefinitionMixin(object):
    model = ClientVersion
    schema = ClientVersionSchema()

    get_security_tokens = LIST_TOKENS
    edit_security_tokens = MODIFY_TOKENS


class ClientVersionApi(ClientVersionDefinitionMixin, MaGetEditBase):
    """ GET API (Get single), PUT API (Update), DELETE API
    """


class ClientVersionArgsParserLists(SqlAlchemyFilterParser):
    __model__ = ClientVersion
    q = WildcardFilter(dest='version')


class ClientVersionListPostApi(ClientVersionDefinitionMixin, MaListResponse):
    """ LIST API (search), POST API (Create)
        """
    key_name = "client_versions"
    filter_parsers = ClientVersionArgsParserLists()
    # no token required to access list API
    get_security_tokens = None
