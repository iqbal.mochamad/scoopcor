from __future__ import absolute_import
from flask import Blueprint

from app.auth import applications
from . import api, user_devices
from . import clients

from .client_portal import ClientPortalListApi, ClientPortalApi

from flask_appsfoundry import Api

client_portal_blueprint = Blueprint('portal-clients', __name__, url_prefix='/v1/portal-clients')
portal_clients_api = Api(client_portal_blueprint)
portal_clients_api.add_resource(ClientPortalListApi, '', endpoint='portal-clients-list')
portal_clients_api.add_resource(ClientPortalApi, '/<int:client_id>')


client_blueprint = Blueprint('clients', __name__, url_prefix='/v1/clients')
client_blueprint.add_url_rule('', view_func=clients.ClientListPostApi.as_view('list'))
client_blueprint.add_url_rule('/<int:entity_id>', view_func=clients.ClientApi.as_view('details'))
client_blueprint.add_url_rule('/current-client', view_func=clients.CurrentClientApi.as_view('current'))

client_version_blueprint = Blueprint('client_versions', __name__, url_prefix='/v1/client-versions')
client_version_blueprint.add_url_rule('', view_func=clients.ClientVersionListPostApi.as_view('list'))
client_version_blueprint.add_url_rule('/<int:entity_id>', view_func=clients.ClientVersionApi.as_view('details'))

auth_blueprint = Blueprint('auth', __name__, url_prefix='/v1/auth')
auth_blueprint.add_url_rule('/login', view_func=api.LoginApi.as_view('login'))
auth_blueprint.add_url_rule('/login-guest', view_func=api.LoginGuestApi.as_view('login-guest'))
auth_blueprint.add_url_rule('/verify-password', view_func=api.VerifyUserPasswordApi.as_view('verify-password'))
auth_blueprint.add_url_rule('/refresh-token', view_func=api.RefreshTokenApi.as_view('refresh-token'))

auth_blueprint.add_url_rule('/token/bearer-to-jwt',
                            view_func=api.ConvertTokenBearerToJwt.as_view('token-bearer-to-jwt'))

auth_blueprint.add_url_rule('/reset-password', view_func=api.ResetPasswordApi.as_view('reset-password'))
auth_blueprint.add_url_rule('/change-password',
                            view_func=api.ChangePasswordApi.as_view('change-password'))

auth_blueprint.add_url_rule('/deauthorize', view_func=api.DeauthorizeApi.as_view('deauthorize'))
auth_blueprint.add_url_rule('/deauthorize-all', view_func=api.DeauthorizeAllApi.as_view('deauthorize-all'))

# create, register, and get a new device unique id
auth_blueprint.add_url_rule('/device', view_func=user_devices.DeviceApi.as_view('device'))
# list of current user devices
auth_blueprint.add_url_rule('/current-devices', view_func=api.CurrentDevices.as_view('current-devices'))
auth_blueprint.add_url_rule('/current-devices/<int:user_id>',
                            view_func=api.GetUserDevices.as_view('current-devices-by-user-id'))

# User Register API: v1/users method POST (app.users.users.py method: create_user)

application_blueprint = Blueprint('applications', __name__, url_prefix='/v1/applications')
application_blueprint.add_url_rule('', view_func=applications.ApplicationListApi.as_view('list'))
application_blueprint.add_url_rule('/<int:entity_id>', view_func=applications.ApplicationApi.as_view('details'))
