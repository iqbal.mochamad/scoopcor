from flask_appsfoundry.serializers import fields

from app.utils.serializers import ImplicitRelHref


class ClientImplicitHref(ImplicitRelHref):
    title = fields.String(attribute='name')
    href = fields.String(attribute='api_url')
