from __future__ import unicode_literals, absolute_import

from datetime import datetime
from flask import g
from flask_appsfoundry.exceptions import Forbidden

from app import db
from app.eperpus.constants import PUBLIC_LIBRARY_CLIENT_ID, TELKOMSEL_ORGANIAZATION_ID


def assert_user_eperpus_general(session, user, client=None):
    # cant import this on top, un-loop import user vs organization
    from app.users.organizations import Organization, OrganizationType

    client = client or getattr(g, 'current_client', None)

    if client:
        # check if client are related to eperpus?
        exist_organization = Organization.query.filter(
            Organization.clients.any(id=client.id),
            Organization.type == OrganizationType.shared_library.value,
            Organization.parent_organization_id == None).first()

        if client.id in PUBLIC_LIBRARY_CLIENT_ID:
            assign_user_to_eperpus_public(user, client, session)

        if exist_organization and not user.is_verified:
            raise Forbidden(
                user_message="Login failed, please verify your email account",
                developer_message="Login failed, user email account not verified"
            )

    return True


def assign_user_to_telkomsel(user_schema, session=None):
    from sqlalchemy import or_
    from app.users.organizations import Organization
    from app.users.users import User, Role, Profile
    from app.utils.marshmallow_helpers import schema_load
    from app.auth.role_perms import RoleType

    entity, error_message = schema_load(user_schema), None

    if not entity.get('origin_client_id', None):
        entity['origin_client_id'] = g.current_client.id

    profile_entity = entity.pop('profile', None)
    user_exists = User.query.filter(or_(
        User.username == entity.get("username", 'unknown'),
        User.email == entity.get("email", 'unknown@gramedia.com'))).first()

    if user_exists:
        error_message = "Registration successful. You may use your Gramedia Digital's password."

        # update data users
        user_exists.phone_number = entity.get('phone_number', None)
        user_exists.is_verified = True

        if user_exists.first_name is None:
            user_exists.first_name = entity.get('first_name', None)
            user_exists.last_name = entity.get('last_name', None)

        exist_roles = [role_.id for role_ in user_exists.roles]
        if RoleType.unverified_user.value in exist_roles:
            # this is if user unverified_role, remove this roles immediately!
            user_exists.roles.remove(session.query(Role).get(RoleType.unverified_user.value))

        if RoleType.verified_user.value not in exist_roles:
            user_exists.roles.append(session.query(Role).get(RoleType.verified_user.value))

        # auto add organizations telkomsel for exist users!
        exists_organization = [org_.id for org_ in user_exists.organizations]
        if TELKOMSEL_ORGANIAZATION_ID not in exists_organization:
            user_exists.organizations.append(session.query(Organization).get(TELKOMSEL_ORGANIAZATION_ID))
        else:
            error_message = 'Email {} had been registered.'.format(user_exists.email)

        session.add(user_exists)
        session.commit()
        return error_message, user_exists

    else:
        new_user = User()
        for key, value in entity.items():
            if hasattr(new_user, key):
                setattr(new_user, key, value)

        new_user.set_password(new_user.password)
        new_user.roles.append(session.query(Role).get(RoleType.verified_user.value))
        new_user.organizations.append(session.query(Organization).get(TELKOMSEL_ORGANIAZATION_ID))
        new_user.is_verified = True
        new_user.is_active = True
        new_user.last_login = datetime.now()

        if profile_entity:
            profile = Profile()
            for key, value in profile_entity.items():
                if hasattr(profile, key):
                    setattr(profile, key, value)
            profile.user = new_user
            session.add(profile)

        session.add(new_user)
        session.commit()
        return error_message, new_user


def assign_user_to_eperpus_public(user, client=None, session=None):
    """ special rules for application ePerpus General & Public:
    if user client is ePerpus General & Public:
        automatically add user to public library

    :param user:
    :param client:
    :param session:
    :return:
    """
    session = session or db.session()
    client_id = client.id if client else 1

    # assign to eperpus public
    if client_id in PUBLIC_LIBRARY_CLIENT_ID:
        from app.eperpus.libraries import SharedLibrary
        eperpus_public = session.query(SharedLibrary).filter_by(public_library=True).all()
        user_orgs = [org.id for org in user.organizations]
        for eperpus in eperpus_public:
            if eperpus.id not in user_orgs:
                user.organizations.append(eperpus)
        session.add(user)
        session.commit()
    else:
        # cant import this on top, un-loop import user vs organization
        from app.users.organizations import Organization, OrganizationType
        from app.users.users import Role
        from app.auth.role_perms import RoleType

        user.is_verified = False

        # added Branded e-perpus clients!
        exist_organization = Organization.query.filter(
            Organization.clients.any(id=client_id),
            Organization.type == OrganizationType.shared_library.value,
            Organization.parent_organization_id == None).first()

        if exist_organization:
            user.organizations.append(exist_organization)
            session.add(user)

        # remove old roles
        for prev_role in user.roles:
            user.roles.remove(prev_role)

        # set user unverified role
        user.roles.append(session.query(Role).get(RoleType.unverified_user.value))
        session.commit()
