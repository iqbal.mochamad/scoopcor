from flask_appsfoundry.models import SoftDeletableMixin, TimestampMixin
from sqlalchemy.dialects import postgresql

from app import db


class CurrencyQuote(TimestampMixin, SoftDeletableMixin, db.Model):
    base_currency = db.Column(db.Text, nullable=False)
    counter_currency = db.Column(db.Text, nullable=False)
    quote = db.Column(db.DECIMAL)
    valid_from = db.Column(db.DateTime, nullable=False)
    valid_to = db.Column(db.DateTime, nullable=False)
    source = db.Column(db.Text, nullable=False)
