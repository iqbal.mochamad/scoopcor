from flask_restful import fields
from flask_appsfoundry import serializers


class CurrencyQuotesSerializer(serializers.SerializerBase):
    id = fields.Integer
    base_currency = fields.String
    counter_currency = fields.String
    quote = fields.Float
    is_active = fields.Boolean
    valid_from = fields.DateTime('iso8601')
    valid_to = fields.DateTime('iso8601')
    created = fields.DateTime('iso8601')
    modified = fields.DateTime('iso8601')
    source = fields.String
