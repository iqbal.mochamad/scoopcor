from decimal import Decimal

from flask_appsfoundry.parsers import converters as inputs
from flask_appsfoundry.parsers import (
    ParserBase, InputField, SqlAlchemyFilterParser, )
from flask_appsfoundry.parsers.filterinputs import (
    IntegerFilter, StringFilter, DateTimeFilter, BooleanFilter)
from werkzeug.exceptions import BadRequest

from . import models


def positive_only(input):
    if Decimal(input) > 0:
        return Decimal(input)
    else:
        raise BadRequest(description="CURRENCY QUOTE MUST BE > 0")


class CurrencyQuoteArgsParser(ParserBase):

    base_currency = InputField(required=True)
    counter_currency = InputField(required=True)
    quote = InputField(type=positive_only, required=True)
    valid_from = InputField(type=inputs.naive_datetime_from_iso8601,
                            required=True)
    valid_to = InputField(type=inputs.naive_datetime_from_iso8601,
                           required=True)
    source = InputField(required=True)
    is_active = InputField(type=inputs.boolean)


class CurrencyQuoteListArgsParser(SqlAlchemyFilterParser):
    __model__ = models.CurrencyQuote
    id = IntegerFilter()
    base_currency = StringFilter()
    counter_currency = StringFilter()
    quote = IntegerFilter()
    valid_from = DateTimeFilter()
    valid_to = DateTimeFilter()
    source = StringFilter()
    is_active = BooleanFilter()
