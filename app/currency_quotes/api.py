from flask_appsfoundry import controllers
from sqlalchemy import desc
from werkzeug.exceptions import BadRequest

from . import models, parsers, serializers


class CurrencyQuotesListApi(controllers.ListCreateApiResource):

    query_set = models.CurrencyQuote.query

    default_ordering = (desc(models.CurrencyQuote.id),
                        )
    create_request_parser = parsers.CurrencyQuoteArgsParser

    list_request_parser = parsers.CurrencyQuoteListArgsParser

    response_serializer = serializers.CurrencyQuotesSerializer

    serialized_list_name = u'currency_quotes'

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext',
                           'can_read_global_currencyquotes',
                           )

    post_security_tokens = ('can_read_write_global_all',
                            'can_read_write_global_currencyquotes',
                            )

    def _before_insert(self, model, req_data):
        if req_data['base_currency'] == req_data['counter_currency']:
            raise BadRequest(description="set base or counter to different currency")


class CurrencyQuotesDetailApi(controllers.DetailApiResource):

    query_set = models.CurrencyQuote.query

    update_request_parser = parsers.CurrencyQuoteArgsParser

    response_serializer = serializers.CurrencyQuotesSerializer

    get_security_tokens = ('can_read_write_global_all',
                           'can_read_write_public',
                           'can_read_write_public_ext',
                           'can_read_global_currencyquotes',
                           )

    put_security_tokens = ('can_read_write_global_all',
                           'can_read_write_global_currencyquotes',
                           )

    delete_security_tokens = ('can_read_write_global_all',
                              )

    def _before_update(self, model, req_args):
        if req_args['base_currency'] == req_args['counter_currency']:
            raise BadRequest(description="set base or counter to different currency")