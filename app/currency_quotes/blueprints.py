from flask import Blueprint
from flask_appsfoundry import Api

from .api import CurrencyQuotesListApi, CurrencyQuotesDetailApi


blueprint = Blueprint('currencyquotes', __name__, url_prefix='/v1/currencyquotes')

api = Api(blueprint)

api.add_resource(CurrencyQuotesListApi, '')
api.add_resource(CurrencyQuotesDetailApi, '/<int:db_id>', endpoint='currencyquotes')
