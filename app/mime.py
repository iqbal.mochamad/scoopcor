"""
Application Mime Types
======================

Contains constants for all of the Mime-Types that we use within ScoopCOR.
"""
from __future__ import absolute_import, unicode_literals


SCOOP_V3_JSON = 'application/vnd.scoop.v3+json'
APPLICATION_JSON = 'application/json'
APPLICATION_XML = 'application/xml'
