from __future__ import unicode_literals, absolute_import

from flask_appsfoundry.models import TimestampMixin
from sqlalchemy import Column
from sqlalchemy.dialects.postgresql import JSONB

from app import db

class UserDownload(db.Model):
    __tablename__ = 'user_download'
    __table_args__ = {'schema': 'analytics'}
    schema = 'remote_banks'

    id = db.Column(db.Integer, primary_key=True)
    user_activity = Column(JSONB)


class UserPageview(db.Model):
    __tablename__ = 'user_page_view'
    __table_args__ = {'schema': 'analytics'}
    id = db.Column(db.Integer, primary_key=True)
    user_activity = Column(JSONB)


class UserSearch(db.Model):
    __tablename__ = 'user_search'
    __table_args__ = {'schema': 'analytics'}
    id = db.Column(db.Integer, primary_key=True)
    user_activity = Column(JSONB)


class GuestLogin(db.Model):
    __tablename__ = 'user_guest_login'
    __table_args__ = {'schema': 'analytics'}
    id = db.Column(db.Integer, primary_key=True)
    user_activity = Column(JSONB)


class AppOpen(db.Model):
    __tablename__ = 'user_app_open'
    __table_args__ = {'schema': 'analytics'}

    id = db.Column(db.Integer, primary_key=True)
    user_activity = Column(JSONB)


class ItemViews(db.Model):
    """Table for recording activity when user view items which triggered by

    this endpoint:
    --- v1/items?item=12354
    --- v1/items/12354
    """

    __tablename__ = 'user_item_view'
    __table_args__ = {'schema': 'analytics'}

    id = db.Column(db.Integer, primary_key=True)
    user_activity = Column(JSONB)




