from __future__ import unicode_literals

from enum import Enum


PAGEVIEW = u'pageview'
DOWNLOAD = u'download'
APPOPEN = u'app_open'
MIGRATIONGUESTLOGIN = u'migration_guest_login'
USER_QUERY = u'user_query'

ACTIVITY_TYPES = [
    PAGEVIEW,
    DOWNLOAD,
    APPOPEN,
    MIGRATIONGUESTLOGIN,
    USER_QUERY,
]


class ActivityType(Enum):
    pageview = 'pageview'
    download = 'download'
    app_open = 'app_open'
    migration_guest_login = 'migration_guest_login'
    user_query = 'user_query'
