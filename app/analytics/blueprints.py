from flask import Blueprint
from flask_appsfoundry import Api

from .api import BulkLogListApi


blueprint = Blueprint('analytics', __name__, url_prefix='/v1/analytics')

api = Api(blueprint)
api.add_resource(BulkLogListApi, '', endpoint='bulk')

