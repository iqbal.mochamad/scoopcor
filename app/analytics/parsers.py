from __future__ import absolute_import, unicode_literals

from flask_appsfoundry.parsers import ParserBase, InputField
from flask_appsfoundry.parsers.converters import naive_datetime_from_iso8601
from datetime import datetime


def pageview_element(input):
    """ Formats an input json document with the expected input data for a
    Pageview log.  This is solely to be used with the bulk-analytics parser.

    :param `dict` input: An input dictionary (assumed to by from JSON)
    :return: A formatted *copy* of the provided input dictionary.
    :rtype: `dict`
    """
    if input.get('max_duration'):
        max_duration = float(input['max_duration'])
    else:
        max_duration = float(input['duration']) if input.get('duration') else 0
    result = {
        'device_id': input.get('device_id'),
        'user_id': int(input['user_id']) if input.get('user_id') else None,
        'item_id': int(input['item_id']) if input.get('item_id') else None,
        'session_name': input.get('session_name'),
        'ip_address': input.get('ip_address'),
        'page_orientation': input.get('page_orientation'),
        'page_number': [int(pgnum) for pgnum in input.get('page_number', [])],
        'duration': float(input['duration']) if input.get('duration') else 0,
        'max_duration': max_duration,
        'online_status': input.get('online_status'),
        'location':
            {"type": "POINT",
             "coordinates": [float(input.get('location').split()[0]),
                             float(input.get('location').split()[1])]}
            if input.get('location')
            else None,
        'client_version': input.get('client_version'),
        'client_id':
            int(input['client_id'])
            if 'client_id' in input
            else None,
        'os_version': input.get('os_version'),
        'device_model': input.get('device_model'),
        'datetime':
            naive_datetime_from_iso8601(input['datetime'])
            if 'datetime' in input
            else datetime.now(),
        'activity_type': 'pageview',
        'chapter': input.get('chapter') if input.get('chapter') else None,
        'catalog_id': int(input['catalog_id']) if input.get('catalog_id') else None,
        'organization_id': int(input['organization_id']) if input.get('organization_id') else None,
    }
    return result


def download_element(input):
    """ Formats an input json document with the expected input data for a
    Download log.  This is solely to be used with the bulk-analytics parser.

    :param `dict` input: An input dictionary (assumed to by from JSON)
    :return: A formatted *copy* of the provided input dictionary.
    :rtype: `dict`
    """
    result = {
        'device_id': input.get('device_id'),
        'user_id': int(input['user_id']) if input.get('user_id') else None,
        'item_id': int(input['item_id']) if input.get('item_id') else None,
        'session_name': input.get('session_name'),
        'ip_address': input.get('ip_address'),
        'download_status': input.get('download_status'),
        'location':
            {"type": "POINT",
             "coordinates": [float(input.get('location').split()[0]),
                             float(input.get('location').split()[1])]}
            if input.get('location')
            else None,
        'client_version': input.get('client_version'),
        'client_id':
            int(input.get('client_id'))
            if 'client_id' in input
            else None,
        'os_version': input.get('os_version'),
        'device_model': input.get('device_model'),
        'datetime':
            naive_datetime_from_iso8601(input['datetime'])
            if 'datetime' in input
            else datetime.now(),
        'activity_type': 'download',
        'catalog_id': int(input['catalog_id']) if input.get('catalog_id') else None,
        'organization_id': int(input['organization_id']) if input.get('organization_id') else None,
    }
    return result


def appopen_element(input):
    result = {
        'device_id': input.get('device_id'),
        'user_id': int(input['user_id']) if input.get('user_id') else None,
        'session_name': input.get('session_name'),
        'ip_address': input.get('ip_address'),
        'location':
            {"type": "POINT",
             "coordinates": [float(input.get('location').split()[0]),
                             float(input.get('location').split()[1])]}
            if input.get('location')
            else None,
        'client_version': input.get('client_version'),
        'client_id':
            int(input.get('client_id'))
            if 'client_id' in input
            else None,
        'os_version': input.get('os_version'),
        'device_model': input.get('device_model'),
        'datetime':
            naive_datetime_from_iso8601(input['datetime'])
            if 'datetime' in input
            else datetime.now(),
        'activity_type': 'app_open'
    }
    return result


def migration_element(input):
    result = {
        'old_device_id': input.get('old_device_id'),
        'device_id': input.get('device_id'),
        'user_id': int(input['user_id']) if input.get('user_id') else None,
        'session_name': input.get('session_name'),
        'ip_address': input.get('ip_address'),
        'location':
            {"type": "POINT",
             "coordinates": [float(input.get('location').split()[0]),
                             float(input.get('location').split()[1])]}
            if input.get('location')
            else None,
        'client_version': input.get('client_version'),
        'client_id':
            int(input.get('client_id'))
            if 'client_id' in input
            else None,
        'os_version': input.get('os_version'),
        'device_model': input.get('device_model'),
        'datetime':
            naive_datetime_from_iso8601(input['datetime'])
            if 'datetime' in input
            else datetime.now(),
        'activity_type': 'migration_guest_login'
    }
    return result


class BulkAnalyticsParser(ParserBase):
    """ Allows posting of the same data as below, but in a nested format so
    that many records can be posted by the apps in the same HTTP request.
    """
    pageviews = InputField(type=pageview_element, action='append', default=[])
    downloads = InputField(type=download_element, action='append', default=[])
    app_opens = InputField(type=appopen_element, action='append', default=[])
    migration_guest_logins = InputField(type=migration_element, action='append', default=[])

