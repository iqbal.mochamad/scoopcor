import re

from flask_appsfoundry.serializers import SerializerBase, fields

from app import db


class GeometryPoint(fields.Raw):
    """ Special purpose field for fetching a PostGIS POINT objects

    Converts from the binary data that is retrieved by default, and then
    extracting the coordinates (it comes out as something like
    `POINT(24.35, -2.12)`, so we're just interested in the numeric
    coordinates for displaying).
    """
    def format(self, value):
        if value is not None:
            point = db.session.scalar(value.ST_AsText())
            m = re.findall(r'^POINT\((?P<point_text>.+)\)$', point)
            point = m.pop() if m else None
        else:
            point = None
        return point


class AnalyticsSerializerBase(SerializerBase):
    id = fields.Integer
    activity_type = fields.String
    device_id = fields.String
    user_id = fields.Integer
    ip_address = fields.String
    location = GeometryPoint
    client_version = fields.String
    client_id = fields.Integer
    session_name = fields.String
    os_version = fields.String
    device_model = fields.String
    datetime = fields.DateTime('iso8601')
    created = fields.DateTime('iso8601')


class AppOpenSerializer(AnalyticsSerializerBase):
    pass


class MigrationGuestLoginSerializer(AnalyticsSerializerBase):
    old_device_id = fields.String


class PageViewSerializer(AnalyticsSerializerBase):
    item_id = fields.Integer
    page_orientation = fields.String
    page_number = fields.List(fields.Integer)
    chapter = fields.String
    duration = fields.Float
    max_duration = fields.Float
    online_status = fields.String


class DownloadSerializer(AnalyticsSerializerBase):
    item_id = fields.Integer
    download_status = fields.String



class BulkAnalyticsSerializer(PageViewSerializer, DownloadSerializer):
    """ Just merges both formats together in a general 'analytic log' format.
    """
    pass
