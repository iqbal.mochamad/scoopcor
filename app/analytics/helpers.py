import calendar
import json

import logging
import requests
from flask import request, jsonify
from requests import codes

from app import db, app
from app.analytics.models import UserSearch, ItemViews
from flask_appsfoundry.exceptions import NotFound

from app.utils.datetimes import datetime_to_isoformat, get_local
from app.utils.exceptions import BadGateway
from app.utils.geo_info import get_geo_info
from . import models, parsers, serializers
from .choices.activity import PAGEVIEW, DOWNLOAD, APPOPEN, MIGRATIONGUESTLOGIN, USER_QUERY


log = logging.getLogger('scoopcor.errors')


def log_query_set_factory(controller):
    if controller.set_id == PAGEVIEW:
        return models.UserPageview.query
    elif controller.set_id == DOWNLOAD:
        return models.UserDownload.query
    elif controller.set_id == APPOPEN:
        return models.AppOpen.query
    elif controller.set_id == MIGRATIONGUESTLOGIN:
        return models.GuestLogin.query
    else:
        raise NotFound


def log_serializer_factory(controller):
    if controller.set_id == PAGEVIEW:
        return serializers.PageViewSerializer
    elif controller.set_id == DOWNLOAD:
        return serializers.DownloadSerializer
    elif controller.set_id == APPOPEN:
        return serializers.AppOpenSerializer
    elif controller.set_id == MIGRATIONGUESTLOGIN:
        return serializers.MigrationGuestLoginSerializer
    else:
        raise NotFound


def user_activity_to_json(activity_args, created_date=None):
    if not created_date:
        created_date = get_local()
    activity_args["created"] = float(calendar.timegm(created_date.timetuple()))

    if activity_args.get("datetime", None):
        activity_date = activity_args["datetime"]
        if not -30 < (activity_date.date() - get_local().date()).days < 2:
            activity_date = get_local()
        activity_args["datetime"] = datetime_to_isoformat(activity_date)

        activity_args["datetime_epoch"] = float(calendar.timegm(activity_date.timetuple()))

    return activity_args


def save_user_search_activity(user_id, query_text, query_results, query_facets=None):

    data = {
        'activity_type': USER_QUERY,
        'device_id': 'scoop-api-server',
        'user_id': user_id,
        'session_name': 'scoop-api-server',
        'datetime': datetime_to_isoformat(get_local()),
        'query_text': query_text,
        'query_results': query_results,
        'query_facets': query_facets
    }
    data = set_geo_info(data)
    user_activity = {'user_search': [data]}
    return send_to_analytic_api(data=json.dumps(user_activity))


def set_geo_info(data):
    geo_info = get_geo_info()
    if geo_info and geo_info.client_ip:
        data['ip_address'] = geo_info.client_ip
    if geo_info and geo_info.latitude:
        data['location'] = "{} {}".format(geo_info.latitude, geo_info.longitude)
    return data


def insert_user_item_view_data(user_id, item_id, client_id, log=None):
    try:
        if item_id:
            created = get_local()
            data = {
                "user_id": user_id,
                "item_id": item_id,
                "client_id": client_id,
                "datetime": datetime_to_isoformat(created)
            }
            data = set_geo_info(data)
            user_activity = {'item_views': [data]}
            # item_view = ItemViews(user_activity=user_activity)
            return send_to_analytic_api(data=json.dumps(user_activity))
    except Exception as ex:
        if log:
            log.exception('Failed to post log data to analytics item view, error {}'.format(str(ex)))
        pass


def send_to_analytic_api(data):
    """ resend data to analytics API

    :param `str` data: string dump from a dictionary
    :return:
    """
    base_url = app.config.get('ANALYTICS_API_BASE_URL', None)
    try:
        headers = {
            'Accept': 'application/vnd.scoop.v3+json',
            'Content-Type': 'application/json',
            'User-Agent': request.headers.get('User-Agent', None),
            'X-COR-Forward': '1',
        }
        if request.headers.get('Authorization', None):
            headers['Authorization'] = request.headers.get('Authorization', None)

        response = requests.post(base_url, data=data, headers=headers)
        # print('aaa - {} - {} - {}'.format(base_url, response.status_code, response))
        if (response.status_code in (codes.ok, codes.created) and response.text) \
                or (400 <= response.status_code < 500):
            resp = jsonify(response.json())
            resp.status_code = response.status_code
            return resp
        else:
            raise BadGateway(
                user_message="Failed to get data from Analytics API",
                developer_message="Response: {status_code} - response body: {body}.\n Url: {url}".format(
                    status_code=response.status_code,
                    body=response.text if response.text else "Empty Response",
                    url=base_url)
            )
    except BadGateway as ex:
        log.exception("When post to API Url: {url} got Error {ex.developer_message}".format(ex=ex, url=base_url))
        raise ex
    except Exception as ex:
        log.exception("When post to API Url: {url} got Error {ex}".format(
            ex=str(ex), url=base_url))
        raise BadGateway(
            user_message="Failed to get data from Analytics API",
            developer_message="When post to API Url: {url} got Error {ex}".format(ex=str(ex), url=base_url)
        )
