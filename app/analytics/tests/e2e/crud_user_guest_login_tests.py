from __future__ import unicode_literals

import json

import httpretty
from faker import Factory
from nose.tools import istest
from requests import codes

from app import app
from app.utils.testcases import TestBase


@istest
class PageViewTests(TestBase):

    request_url = '/v1/analytics'
    fixture_factory = None

    @classmethod
    def setUpClass(cls):
        super(PageViewTests, cls).setUpClass()

    @httpretty.activate
    def test_post_data(self):
        expected = {"recorded": {u'migration_guest_logins': 2}}

        # mock_response_from_analytics_api
        analytics_api_url = app.config['ANALYTICS_API_BASE_URL']
        httpretty.register_uri(httpretty.POST, analytics_api_url,
                               body=json.dumps(expected),
                               status=201)

        self.req_body = self.get_api_request_body()
        self.set_api_authorized_user(self.super_admin)
        response = self.api_client.post(
            self.request_url,
            data=json.dumps(self.req_body),
            headers=self.build_headers(has_body=True))
        self.assert_status_code(response, codes.created)

        actual = json.loads(response.data)
        self.assertIsNotNone(actual.get('recorded'))
        # expected = {"recorded": {u'migration_guest_logins': 0, u'downloads': 0, u'pageviews': 2, u'app_opens': 0}}
        expected = {"recorded": {u'migration_guest_logins': 2}}
        self.assertDictContainsSubset(
            expected=expected.get('recorded'),
            actual=actual.get('recorded'))

    def get_api_request_body(self):
        return {
            'migration_guest_logins': [{
                "device_id": "123",
                "old_device_id": "jflddfsl.dflaj",
                "user_id": 1,
                "session_name": "some session",
                "ip_address": "127.0.0.1",
                "download_status": "success",
                "location": "64.54 32.64",
                "client_version": "1.2.3",
                "os_version": "abc",
                "device_model": "abc",
                "datetime": "2015-03-23T10:11:30.121365"
                },
                {
                "device_id": "123",
                "old_device_id": "jflddfsl.dflaj",
                "user_id": 1,
                "session_name": "some session",
                "ip_address": "127.0.0.1",
                "download_status": "success",
                "location": "64.54 32.64",
                "client_version": "1.2.3",
                "os_version": "abc",
                "device_model": "abc",
                "datetime": "2015-03-23T10:11:30.121365"
                }
            ]
        }


_fake = Factory.create()
