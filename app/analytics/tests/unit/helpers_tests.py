import json
import random

import httpretty
import requests
from faker import Factory
from marshmallow.utils import isoformat
from requests import codes

from app import app
from app.analytics import helpers
from app.analytics.choices import ONLINE_TYPES, ORIENTATION_TYPES
from app.utils.exceptions import BadGateway
from app.utils.testcases import TestBase


# create a callback body that raises an exception when opened
def mockExceptionCallback(request, uri, headers):

    # raise your favourite exception here, e.g. requests.ConnectionError or requests.Timeout
    raise requests.Timeout('Connection timed out.')


class HelpersTests(TestBase):

    @httpretty.activate
    def test_send_to_analytic_api(self):
        expected = {"recorded": {u'pageviews': 2}}
        # mock_response_from_analytics_api
        analytics_api_url = app.config['ANALYTICS_API_BASE_URL']
        httpretty.register_uri(httpretty.POST, analytics_api_url,
                               body=json.dumps(expected),
                               status=codes.created)
        data = self.get_api_request_body()
        resp = helpers.send_to_analytic_api(json.dumps(data))
        self._assert_response(expected, resp)

    @httpretty.activate
    def test_send_to_analytic_api_error_raised(self):
        # mock_response_from_analytics_api
        #  test got response 501 from Analytics API
        analytics_api_url = app.config['ANALYTICS_API_BASE_URL']
        httpretty.register_uri(httpretty.POST, analytics_api_url,
                               body=json.dumps({}),
                               status=501)
        self.assertRaises(BadGateway, helpers.send_to_analytic_api, "")
        # mock_response_from_analytics_api
        #  test error time out
        httpretty.register_uri(httpretty.POST, analytics_api_url,
                               body=mockExceptionCallback,
                               status=200)
        self.assertRaises(BadGateway, helpers.send_to_analytic_api, "")

    @httpretty.activate
    def test_insert_user_item_view_data(self):
        expected = {"recorded": {u'item_views': 1}}
        # mock_response_from_analytics_api
        analytics_api_url = app.config['ANALYTICS_API_BASE_URL']
        httpretty.register_uri(httpretty.POST, analytics_api_url,
                               body=json.dumps(expected),
                               status=codes.created)

        resp = helpers.insert_user_item_view_data(
            user_id=12345,
            item_id=3322,
            client_id=1,
        )
        self._assert_response(expected, resp)

    @httpretty.activate
    def test_save_user_search_activity(self):
        expected = {"recorded": {u'user_search': 1}}
        # mock_response_from_analytics_api
        analytics_api_url = app.config['ANALYTICS_API_BASE_URL']
        httpretty.register_uri(httpretty.POST, analytics_api_url,
                               body=json.dumps(expected),
                               status=codes.created)

        resp = helpers.save_user_search_activity(
            user_id=12345,
            query_text="brand_name:{}".format(_fake.word()),
            query_results=self.fake.pyint(),
            query_facets=1,
        )
        self._assert_response(expected, resp)

    def _assert_response(self, expected, response):
        self.assert_status_code(response, codes.created)

        actual = json.loads(response.data)
        self.assertIsNotNone(actual.get('recorded'))
        # expected = {"recorded": {u'migration_guest_logins': 0, u'downloads': 0, u'pageviews': 2, u'app_opens': 0}}

        self.assertDictContainsSubset(
            expected=expected.get('recorded'),
            actual=actual.get('recorded'))

    @staticmethod
    def get_api_request_body():
        return {
            'pageviews': [{
                'device_id': _fake.bs(),
                'user_id': _fake.pyint(),
                "item_id": _fake.pyint(),
                'session_name': _fake.name(),
                "ip_address": _fake.ipv4(),
                "page_orientation": random.choice(ORIENTATION_TYPES),
                "page_number": [random.randint(0, 100), random.randint(0, 100)],
                "chapter": "1.abc",
                "duration": random.randint(0, 100),
                "max_duration": "{}".format(_fake.pydecimal(positive=True)),
                "online_status": random.choice(ONLINE_TYPES),
                "location": "{} {}".format(
                    _fake.geo_coordinate(),
                    _fake.geo_coordinate()),
                "client_version": _fake.word(),
                "os_version": _fake.word(),
                "device_model": _fake.word(),
                "datetime": isoformat(_fake.date_time_this_year()),
                "catalog_id": _fake.pyint(),
            },
                {
                    'device_id': _fake.bs(),
                    'user_id': _fake.pyint(),
                    "item_id": _fake.pyint(),
                    'session_name': _fake.name(),
                    "ip_address": _fake.ipv4(),
                    "page_orientation": random.choice(ORIENTATION_TYPES),
                    "page_number": [random.randint(0, 100), random.randint(0, 100)],
                    "duration": random.randint(0, 100),
                    "online_status": random.choice(ONLINE_TYPES),
                    "location": "{} {}".format(
                        _fake.geo_coordinate(),
                        _fake.geo_coordinate()),
                    "client_version": _fake.word(),
                    "os_version": _fake.word(),
                    "device_model": _fake.word(),
                    "datetime": isoformat(_fake.date_time_this_year()),
                }
            ]
        }


_fake = Factory.create()
