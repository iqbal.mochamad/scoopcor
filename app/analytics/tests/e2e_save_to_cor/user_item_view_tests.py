from __future__ import unicode_literals, absolute_import
from unittest import TestCase
from nose.tools import istest, nottest
from httplib import OK

from flask import g

from app import app, set_request_id, db
from app.auth.models import UserInfo
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy import ItemFactory
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from app.analytics.models import ItemViews
from tests.fixtures.sqlalchemy.items import get_parental_control


@nottest
class HelpersTests(TestCase):

    maxDiff = None

    @classmethod
    def setUpClass(cls):

        db.drop_all()
        db.create_all()

        cls.init_data()
        cls.original_before_request_funcs = app.before_request_funcs
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }

    @classmethod
    def init_data(cls):
        s = db.session()
        generate_static_sql_fixtures(s)
        cls.user = UserFactory(id=12345)
        p = get_parental_control()
        cls.item1 = ItemFactory(id=1, parentalcontrol_id=p)
        cls.item2 = ItemFactory(id=2)
        # item 3, item not borrowed yet
        cls.item3 = ItemFactory(id=3)
        # item 4, item is borrowed and already returned, should not shown in list of borrowed items
        cls.item4 = ItemFactory(id=4)

        s.commit()

    @classmethod
    def tearDownClass(cls):
        app.before_request_funcs = cls.original_before_request_funcs

    def tearDown(self):
        session = db.session()
        session.query(ItemViews).delete()
        session.commit()

    def test_response_code(self):
        client = app.test_client(use_cookies=False)
        response = client.get('/v1/items?item_id=1')

        self.assertEqual(response.status_code, OK)
        session = db.session()
        content = session.query(ItemViews).all()

        self.assertEqual(len(content), 1)

    def test_list_more_than_one(self):
        client = app.test_client(use_cookies=False)
        response = client.get('/v1/items?item_id=1,2')
        self.assertEqual(response.status_code, OK)
        session = db.session()
        content = session.query(ItemViews).all()
        self.assertEqual(len(content), 0)

    def test_detail_item_view(self):
        client = app.test_client(use_cookies=False)
        response = client.get('/v1/items/1')
        self.assertEqual(response.status_code, OK)
        session = db.session()
        content = session.query(ItemViews).all()
        self.assertEqual(len(content), 1)
        self.assertEqual(content[0].user_activity.get('item_id'), 1)
        self.assertEqual(content[0].user_activity.get('user_id'), 12345)


def init_g_current_user():
    g.user = UserInfo(username='dfdsafsdf', user_id=1234, token='abcd', perm=['can_read_write_global_all',])
    session = db.session()
    g.current_user = session.query(User).get(12345)
    g.current_client = None
    session.close()
