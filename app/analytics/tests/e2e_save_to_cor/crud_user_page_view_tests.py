from __future__ import unicode_literals

import json
import random

from faker import Factory
from nose.tools import istest, nottest
from requests import codes

from app.analytics.choices import ORIENTATION_TYPES, ONLINE_TYPES
from app.utils.datetimes import datetime_to_isoformat
from app.utils.testcases import TestBase


@nottest
class PageViewTests(TestBase):

    request_url = '/v1/analytics'
    fixture_factory = None

    @classmethod
    def setUpClass(cls):
        super(PageViewTests, cls).setUpClass()

    def test_post_data(self):
        self.req_body = self.get_api_request_body()
        self.set_api_authorized_user(self.super_admin)
        response = self.api_client.post(
            self.request_url,
            data=json.dumps(self.req_body),
            headers=self.build_headers(has_body=True))
        self.assert_status_code(response, codes.created)

        actual = json.loads(response.data)
        self.assertIsNotNone(actual.get('recorded'))
        # expected = {"recorded": {u'migration_guest_logins': 0, u'downloads': 0, u'pageviews': 2, u'app_opens': 0}}
        expected = {"recorded": {u'pageviews': 2}}
        self.assertDictContainsSubset(
            expected=expected.get('recorded'),
            actual=actual.get('recorded'))

    def get_api_request_body(self):
        return {
            'pageviews': [{
                'device_id': _fake.bs(),
                'user_id': _fake.pyint(),
                "item_id": _fake.pyint(),
                'session_name': _fake.name(),
                "ip_address": _fake.ipv4(),
                "page_orientation": random.choice(ORIENTATION_TYPES),
                "page_number": [random.randint(0, 100), random.randint(0, 100)],
                "chapter": "1.abc",
                "duration": random.randint(0, 100),
                "max_duration": "{}".format(_fake.pydecimal(positive=True)),
                "online_status": random.choice(ONLINE_TYPES),
                "location": "{} {}".format(
                    _fake.geo_coordinate(),
                    _fake.geo_coordinate()),
                "client_version": _fake.word(),
                "os_version": _fake.word(),
                "device_model": _fake.word(),
                "datetime": datetime_to_isoformat(_fake.date_time_this_year()),
                "catalog_id": _fake.pyint(),
            },
                {
                    'device_id': _fake.bs(),
                    'user_id': _fake.pyint(),
                    "item_id": _fake.pyint(),
                    'session_name': _fake.name(),
                    "ip_address": _fake.ipv4(),
                    "page_orientation": random.choice(ORIENTATION_TYPES),
                    "page_number": [random.randint(0, 100), random.randint(0, 100)],
                    "duration": random.randint(0, 100),
                    "online_status": random.choice(ONLINE_TYPES),
                    "location": "{} {}".format(
                        _fake.geo_coordinate(),
                        _fake.geo_coordinate()),
                    "client_version": _fake.word(),
                    "os_version": _fake.word(),
                    "device_model": _fake.word(),
                    "datetime": datetime_to_isoformat(_fake.date_time_this_year()),
                }
            ]
        }


_fake = Factory.create()
