"""
Analytics Log API
=================

Endpoints for posting and retrieving analytics data from the front end apps.
"""
from __future__ import unicode_literals, absolute_import

import httplib

from flask import request

from app import db
from app.analytics.helpers import user_activity_to_json, send_to_analytic_api
from app.utils.datetimes import get_local
from app.utils.marshmallow_helpers import MaListResponse
from . import models, parsers


def check_existing_activity(activity_param):
    """
    :param activity_param: parameter for activity object
    :return: Boolean
    """

    if activity_param['activity_type'] == "pageview":
        sql_source = "select id from analytics.user_page_view"
    elif activity_param['activity_type'] == "download":
        sql_source = "select id from analytics.user_download"
    elif activity_param['activity_type'] == "app_open":
        sql_source = "select id from analytics.app_open"
    else:
        sql_source = "select id from analytics.user_guest_login"

    sql = """ {sql_source}
            where user_activity #> '{{"user_id"}}' = '{user_id}'
            and cast(user_activity #>> '{{"datetime_epoch"}}' as float) = '{datetime_epoch}'::float
            """.format(sql_source=sql_source,
                       user_id=activity_param['user_id'],
                       datetime_epoch=activity_param['datetime_epoch'])

    result = db.engine.execute(sql)
    return result is None


class BulkLogListApi(MaListResponse):
    """ Allows for bulk insertion and retrieval of analytics data.  The format
    is similar to the analytics data in the TypedLogListApi, except the data
    from GET is not specific to the log type (may contain extra null fields)
    and POST allows multiple logs to be posted at once.
    """
    model = models.UserPageview
    edit_security_tokens = [
        'can_read_write_global_all',
        'can_read_write_global_analytic',
        'can_read_write_public',
        'can_read_write_public_ext']

    def get(self):
        raise NotImplementedError()

    def post(self):
        """
        Records the posted log data, then returns the number of logs stored
        in the database.

        :return: A tuple, first element containing the number of logs stored
            after the post was processed, and the second value of 201 (HTTP
            CREATED)
        :rtype: `tuple`
        """
        # return self._save_to_cor_db()
        self._authorize_request(self.edit_security_tokens)
        return send_to_analytic_api(data=request.data)

    def _save_to_cor_db(self):
        """ this is old code, save data to table in analytics schema

        :return:
        """
        session = db.session

        parser = parsers.BulkAnalyticsParser()
        args = parser.parse_args()

        try:

            created_date = get_local()

            for activity_args in args['pageviews']:
                activity = models.UserPageview(
                    user_activity=user_activity_to_json(activity_args, created_date))
                # if check_existing_activity(activity_args):
                session.add(activity)
            for activity_args in args['downloads']:
                activity = models.UserDownload(
                    user_activity=user_activity_to_json(activity_args, created_date))
                # if check_existing_activity(activity_args):
                session.add(activity)
            for activity_args in args['app_opens']:
                activity = models.AppOpen(
                    user_activity=user_activity_to_json(activity_args, created_date))
                # if check_existing_activity(activity_args):
                session.add(activity)
            for activity_args in args['migration_guest_logins']:
                activity = models.GuestLogin(user_activity=user_activity_to_json(activity_args, created_date))
                # if check_existing_activity(activity_args):
                session.add(activity)

            session.commit()

            return {"recorded": {k: len(args[k]) for k in args}}, httplib.CREATED

        except Exception:
            session.rollback()
            raise

