import httplib
from datetime import datetime, timedelta
from sqlalchemy.sql import and_, or_, desc
from dateutil.relativedelta import relativedelta

from app import app

from app.tiers.models import TierIos, TierAndroid, TierWp
from app.items.models import Item, UserItem
from app.offers.models import Offer, OfferPlatform
from app.offers.choices.offer_type import BUFFET, SINGLE, SUBSCRIPTIONS, BUNDLE

from app.items.choices.item_type import ITEM_TYPES, BOOK, MAGAZINE, NEWSPAPER
from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME

from app.users.buffets import UserBuffet
from app.utils.shims import item_types

class OrderRestore():

    def __init__(self, tier_code=None, user_id=None, platform_id=None,
            original_purchase_date=None, original_transaction_id=None):

        self.tier_code = tier_code
        self.user_id = user_id
        self.platform_id = platform_id
        self.original_purchase_date = original_purchase_date
        self.original_transaction_id = original_transaction_id

        self.tier_data = None
        self.offer_data = []

        self.restored_items = []
        # array of restored items

    def json_success(self, item=None):

        # TODO: What is item?
        data_detail = {}
        if item.item_type == ITEM_TYPES[MAGAZINE]:
            data_detail = item.get_magazine()

        if item.item_type == ITEM_TYPES[BOOK]:
            data_detail = item.get_books()

        if item.item_type == ITEM_TYPES[NEWSPAPER]:
            data_detail = item.get_news()

        json_form = dict()
        json_form['vendor'] = item.get_vendor()
        json_form['brand'] = item.get_brand()
        json_form['parental_control'] = item.get_parental_control()
        json_form['issue_number'] = data_detail['issue_number']
        json_form['name'] = item.name
        json_form['item_type_id'] = {v:k for k,v in ITEM_TYPES.items()}[item.item_type] # convert item_type back to item
        json_form['thumb_image_highres'] = data_detail['thumb_image_highres']
        json_form['thumb_image_normal'] = data_detail['thumb_image_normal']
        json_form['image_highres'] = data_detail['image_highres']
        json_form['image_normal'] = data_detail['image_normal']
        json_form['media_base_url'] = app.config['DIGITAL_MAGAZINE']
        json_form['id'] = item.id
        json_form['authors'] = item.get_authors()

        return json_form

    def check_platforms(self):
        # FOR NOW ONLY ALLOW FOR IOS PLATFORM
        if self.platform_id != 1:
            return httplib.BAD_REQUEST, "Platform id not IOS platform"
        return httplib.OK, 'OK'

    def check_tier_code(self):
        try:
            if self.platform_id == 1:
                sku = TierIos.query.filter_by(tier_code=self.tier_code).first()

            if self.platform_id == 2:
                sku = TierAndroid.query.filter_by(tier_code=self.tier_code).first()

            if self.platform_id == 3:
                sku = TierWp.query.filter_by(tier_code=self.tier_code).first()

            if not sku:
                return httplib.BAD_REQUEST, "tier : %s not found" % self.tier_code

            self.tier_data = sku
            return httplib.OK, 'OK'

        except Exception, e:
            return httplib.BAD_REQUEST, 'ERROR check_tier_code : %s' % str(e)

    def get_offer_by_tier_code(self):

        try:
            offer_plt = OfferPlatform.query.filter_by(tier_code=self.tier_code,
                tier_id = self.tier_data.id, platform_id=self.platform_id).all()

            if not offer_plt:
                return (httplib.BAD_REQUEST,
                    "Offer Platform data not founds, tier_code : %s" % self.tier_code)

            for ioffer in offer_plt:
                offer = Offer.query.filter_by(id=ioffer.offer_id).first()
                self.offer_data.append(offer)

            return httplib.OK, 'OK'

        except Exception, e:
            return httplib.BAD_REQUEST, 'ERROR get_offer_by_tier_code : %s' % str(e)

    def save_user_library(self, data_items=None):
        try:
            for xitem in data_items:
                self.restored_items.append(self.json_success(xitem))
                us_lib = UserItem.query.filter_by(user_id=self.user_id, item_id=xitem.id).first()

                if not us_lib:

                    try:
                        vendors = xitem.get_vendor()
                        vendor_id = vendors.get('id', 0)
                    except:
                        vendor_id = 0

                    mv = UserItem(
                        user_id=self.user_id,
                        item_id=xitem.id,
                        edition_code=xitem.edition_code,
                        is_active=True,
                        orderline_id=None,
                        is_restore=True,
                        brand_id=xitem.brand_id,
                        itemtype_id=item_types.enum_to_id(xitem.item_type),
                        vendor_id=vendor_id
                    )
                    sv = mv.save()
                    if sv.get('invalid', False):
                        return httplib.BAD_REQUEST, sv.get('message', '')

            return httplib.OK, 'OK'
        except Exception, e:
            return httplib.BAD_REQUEST, 'ERROR save_user_library : %s' % str(e)

    def deliver_item_single(self, offer=None):
        try:
            item_ids = offer.get_items()
            for xitem in item_ids:
                item = Item.query.filter_by(id=xitem).all()
                if item:
                    sv = self.save_user_library(item)
                    if sv[0] != httplib.OK:
                        return sv

            return httplib.OK, 'OK'

        except Exception, e:
            return httplib.BAD_REQUEST, 'ERROR deliver_item_single : %s' % str(e)

    def deliver_item_subscription(self, offer=None):
        item, backward_items = [], []
        rl_valid_to, end_date = None, None
        try:
            brands = offer.get_brands_id()
            quantity, quantity_unit, allow_backward, backward_quantity, backward_quantity_unit = offer.get_subscrip_quantity()

            for ibrand in brands:
                date_data = datetime.strptime(self.original_purchase_date, '%Y-%m-%d')

                # DELIVER SUBS BY EDITION TYPE
                if int(quantity_unit) == 1:
                    item = Item.query.filter(
                        Item.release_date >= date_data).filter_by(
                        brand_id=ibrand).limit(quantity).all()


                # DELIVER SUBS BY DURATION TYPE
                if int(quantity_unit) in [2,3,4]:

                    if quantity_unit == 2: #DAY
                        end_date = date_data + timedelta(days=quantity)

                    if quantity_unit == 3: #WEEK
                        end_date = date_data + timedelta(weeks=quantity)

                    if quantity_unit == 4: #MONTH
                        end_date = date_data + relativedelta(months=quantity)

                    item = Item.query.filter(
                        Item.release_date >= date_data).filter(
                        Item.release_date <= end_date).filter_by(
                        brand_id=ibrand).all()

                # IF ITEM NOT EXIST DELIVER THE LATEST ITEMS
                if not item:
                    item_latest = Item.query.filter_by(brand_id=ibrand).order_by(desc(Item.release_date)).first()
                    item = [item_latest]

                # DELIVER ITEM BACKWARD
                if allow_backward and backward_quantity > 0:

                    # UNIT
                    if backward_quantity_unit == 1:
                        backward_items = Item.query.filter(Item.release_date <= date_data).filter_by(
                            brand_id=ibrand).order_by(desc(Item.release_date)).limit(backward_quantity+1).all()
                        if backward_items:
                            backward_items = backward_items[1:]

                    #days
                    if backward_quantity_unit == 2:
                        rl_valid_to = date_data - timedelta(days=backward_quantity+1)

                    #weeks
                    if backward_quantity_unit == 3:
                        rl_valid_to = date_data - timedelta(weeks=backward_quantity+1)

                    #month
                    if backward_quantity_unit == 4:
                        new_valid = date_data - relativedelta(months=backward_quantity+1)
                        rl_new_form = '%s-%s-%s' % (new_valid.year, new_valid.month, 1)
                        rl_valid_to = datetime.strptime(rl_new_form, '%Y-%m-%d')

                    if rl_valid_to:
                        backward_items = Item.query.filter(Item.release_date >= rl_valid_to).filter_by(
                            brand_id=ibrand, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).order_by(desc(Item.release_date)).all()

                #  Merged item and item backward
                item_data = item + backward_items

                sv = self.save_user_library(item_data)

                if sv[0] != httplib.OK:
                    return sv

            return httplib.OK, 'OK'

        except Exception, e:
            return httplib.BAD_REQUEST, 'ERROR deliver_item_subscription : %s' % str(e)

    def deliver_item_buffet(self):
        try:
            user_restore = UserBuffet.query.filter_by(
                original_transaction_id=self.original_transaction_id,
                is_restore=False).order_by(
                desc(UserBuffet.valid_to)).first()

            user_data = UserBuffet.query.filter_by(
                original_transaction_id=self.original_transaction_id,
                user_id=self.user_id).order_by(desc(UserBuffet.valid_to)).first()

            # asume self,original_transaction_id not changes
            # avoid duplicates records
            if user_restore:
                if user_data:
                    if user_data.orderline_id != user_restore.orderline_id:
                        new_data = UserBuffet(
                            user_id=self.user_id,
                            orderline_id=user_restore.orderline_id,
                            offerbuffet_id=user_restore.offerbuffet_id,
                            offerbuffet=user_restore.offerbuffet,
                            valid_to=user_restore.valid_to,
                            is_restore=True,
                            is_trial=user_restore.is_trial,
                            original_transaction_id=self.original_transaction_id
                        )
                        new_data.save()
                else:
                    # create new records..
                    new_data = UserBuffet(
                        user_id=self.user_id,
                        orderline_id=user_restore.orderline_id,
                        offerbuffet_id=user_restore.offerbuffet_id,
                        offerbuffet=user_restore.offerbuffet,
                        valid_to=user_restore.valid_to,
                        is_restore=True,
                        is_trial=user_restore.is_trial,
                        original_transaction_id=self.original_transaction_id
                    )
                    new_data.save()

            return httplib.OK, 'OK'

        except Exception, e:
            return httplib.BAD_REQUEST, 'ERROR deliver_item_buffet : %s' % str(e)

    def deliver_item_by_offer(self):
        try:
            for ioffer in self.offer_data:

                # single offer deliver items
                if ioffer.offer_type_id in [SINGLE, BUNDLE]:
                    self.deliver_item_single(ioffer)

                # subscriptions offer deliver items
                if ioffer.offer_type_id == SUBSCRIPTIONS:
                    self.deliver_item_subscription(ioffer)

                # buffet offer deliver items
                if (ioffer.offer_type_id == BUFFET and self.original_transaction_id):
                    self.deliver_item_buffet()
                    return httplib.NO_CONTENT, "BUFFET ITEMS"

            return httplib.OK, 'OK'

        except Exception, e:
            return httplib.BAD_REQUEST, 'ERROR deliver_item_by_offer : %s' % str(e)

    def construct(self):
        chk_platforms = self.check_platforms()
        if chk_platforms[0] != httplib.OK:
            return chk_platforms

        chk_tier_code = self.check_tier_code()
        if chk_tier_code[0] != httplib.OK:
            return chk_tier_code

        chk_offer_tier = self.get_offer_by_tier_code()
        if chk_offer_tier[0] != httplib.OK:
            return chk_offer_tier

        deliver_items = self.deliver_item_by_offer()
        if deliver_items[0] != httplib.OK:
            return deliver_items

        return httplib.OK, self.restored_items



