import decimal, json, httplib, os

from flask_appsfoundry.exceptions import BadRequest
from flask import Response
from unidecode import unidecode

from app import db, app
from app.constants import *
from app.discounts.choices.predefined_type import HARPER_COLLINS
from app.discounts.choices.discount_type import DISCOUNT_OFFER
from app.orders.helpers import retrieve_promotions_checkout, check_coupon_error, check_trial_promotions
from app.orders.helpers import order_signature_encrypt
from app.utils.datetimes import get_local

from app.helpers import err_response, generate_invoice
from app.helpers import bad_request_message, internal_server_message

from sqlalchemy.sql import desc

from app.orders.models import OrderDetail
from app.orders.models import OrderTemp, OrderLineTemp, OrderLineDiscountTemp
from app.orders.models import OrderLog

from app.offers.models import Offer, OfferBuffet
from app.payments.choices.gateways import POINT, FREE, WALLET, CREDITCARD, PAYMENT_GATEWAYS
from app.users.buffets import UserBuffet
from app.offers.choices.offer_type import BUFFET, SUBSCRIPTIONS, BUNDLE, SINGLE
from app.items.choices.item_type import ITEM_TYPES, BOOK, MAGAZINE, NEWSPAPER
from app.items.models import Item

from app.master.models import Platform, Partner
from app.discounts.models import Discount, DiscountCode
from app.discounts.choices.discount_code_type import CANT_OVERIDE_OFFER_DISCOUNT
from app.payment_gateways.models import PaymentGateway, PAYMENTGATEWAY_GROUP, GROUP_E2PAY
from app.points.models import PointUser

from datetime import datetime
from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME, STATUS_MCGRAWHILL_CONTENT
from app.users.wallet import WALLET_OFFER_TYPE_ID, assert_wallet_balance_by_party_id, assert_offer_for_wallet

from .exceptions import PaymentBelowGatewayMinimumError


'''
    NOTE CAUTION:
        1. checkout for renewal, not cut by promo. hold by self.is_renewal
        2. buffet premium can't cut by predefined group discounts
'''

class OrderCheckoutConstructBase(object):
    """
        RULE:

        1. calculate offer, (discount offer)
        2. calculate coupon code
        3. calculate order
        4. calculate payment gateway
    """

    def __init__(self, args=None, method=None):
        self.args = args
        self.method = method

        #set all info data
        self.platform = 4 # Default WEB is 4

        self.payment_gateway = args.get('payment_gateway_id', None)
        self.user_id = args.get('user_id', 0)
        self.partner_id = args.get('partner_id', 0)
        self.client_id = args.get('client_id', 0)
        self.is_renewal = args.get('is_renewal', False)
        self.cc_token_id = args.get('cc_token_id', None)

        self.order_id = args.get('order_id', None)
        self.order_lines = args.get('order_lines', [])

        self.coupon_data = []
        self.base_currency= None
        self.single_tier_code = None

        self.data_price = []
        self.order_number = ""

        self.su_price_usd, self.fi_price_usd = [], []
        self.su_price_idr, self.fi_price_idr = [], []
        self.su_price_pts, self.fi_price_pts = [], []
        self.discount_data_valid = []

        self.temp_discount_master = None

        self.coupon_can_used = False
        self.can_calculate = False
        self.discount_go_cut = False
        self.coupon_list_used = {}

        self.last_discount = False
        self.last_usd = 0
        self.last_idr = 0
        self.last_pts = 0

        self.order_details = ['user_email', 'user_name', 'user_street_address',
            'user_city', 'user_zipcode', 'user_state', 'user_country', 'latitude',
            'longitude', 'ip_address', 'os_version', 'client_version', 'device_model']
        self.order_details_dict = {}
        self.is_top_up_wallet = False
        self.country_code = args.get('country_code', None)

    def check_orderlines_offer(self):
        """
            CHECKING OFFER IF VALID/NOT
        """
        try:
            for ioffer in self.order_lines:
                offer_id = ioffer.get('offer_id', None)
                offer = Offer.query.filter_by(id=offer_id).first()
                if not offer:
                    return 'FAILED', bad_request_message(modul="order checkout 1",
                        method=self.method, param="offer_id",
                        not_found=offer_id, req=self.args)

                # check restricted countries.
                status, item_name = offer.is_restricted_offers(self.country_code)
                if status:
                    msg = "{} cannot be purchase because it is restricted from your current country.".format(item_name)
                    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                                                error_code=CLIENT_400101,
                                                developer_message=msg,
                                                user_message=msg)

                # buffet cant purchase with scoop points
                if offer.offer_type_id == BUFFET:
                    # if self.payment_gateway == POINT:
                    #     return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    #         error_code=CLIENT_400101,
                    #         developer_message="Payment Invalid, point cant used for buffet",
                    #         user_message="{} is not available for purchasing premium package.".format(
                    #             PAYMENT_GATEWAYS[POINT].title()))

                    offer_buffet = OfferBuffet.query.filter_by(offer_id=offer.id).first()
                    data_buffet = UserBuffet.query.filter(UserBuffet.user_id==self.user_id,
                        UserBuffet.valid_to >= datetime.utcnow(), UserBuffet.offerbuffet_id==offer_buffet.id).order_by(
                        desc(UserBuffet.valid_to)).first()

                    if data_buffet:
                        data_time = datetime.strftime(data_buffet.valid_to, '%Y-%m-%d %H:%M:%S')
                        return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                            error_code=CLIENT_400101,
                            developer_message="Payment Invalid, have active buffet",
                            user_message="Your {} subscription is still active until {}".format(
                                offer.long_name, data_time))

                # This is for checking breachings hit api,
                # check if payment_gateway FREE vs offer.is_free ?
                if self.payment_gateway == FREE:

                    if not offer.is_free and offer.discount_id or offer.is_free:
                        # this mean price already set to 0 from discounts
                        error_message = None
                    else:
                        error_message = "Payment Invalid, not FREE content"

                    if error_message:
                        return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                            error_code=CLIENT_400101,
                            developer_message=error_message,
                            user_message=error_message)

                # Check if renewal cc still active, not allowed user to purchase!
                if self.payment_gateway == CREDITCARD:
                    from app.paymentgateway_connectors.creditcard.models import PaymentCreditCardRenewal
                    from app.paymentgateway_connectors.choices.renewal_status import RENEWAL_STATUS, CANCEL, NEW

                    active_renewal = PaymentCreditCardRenewal.query.filter(
                        PaymentCreditCardRenewal.offer_id == offer.id,
                        PaymentCreditCardRenewal.user_id == self.user_id,
                        PaymentCreditCardRenewal.expired_date >= datetime.now(),
                        PaymentCreditCardRenewal.renewal_status.in_([RENEWAL_STATUS[NEW], RENEWAL_STATUS[CANCEL]])
                    ).first()

                    if active_renewal:
                        return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                              error_code=CLIENT_400101,
                              developer_message="Your susbcription with auto renewal is still active.",
                              user_message="Your susbcription with auto renewal is still active.")

            return 'OK', 'OK'

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 1",
                method=self.method, error=e, req=str(self.args))

    def check_coupon_code(self):
        """
            General checking coupon code for all transactions
        """
        try:
            new_orderlines = []
            for ioffer in self.order_lines:
                offer_id = ioffer.get('offer_id', 0)
                coupon = ioffer.get('discount_code', None)

                error_message = None
                self.coupon_list_used.update({offer_id: coupon})

                if coupon:
                    if coupon in self.coupon_data:
                        error_message = "Cant used same coupon in one cart!"
                        return 'FAILED', err_response(status=httplib.BAD_REQUEST, error_code=CLIENT_400101,
                                                      developer_message=error_message, user_message=error_message)
                    else:
                        self.coupon_data.append(coupon)

                    # Checking max uses
                    coupon_code = DiscountCode.query.filter_by(code=coupon.lower()).first()
                    offer = Offer.query.filter_by(id=offer_id).first()

                    if coupon_code and offer:
                        discounts = coupon_code.get_discount()
                        error_message = check_coupon_error(coupon_code=coupon_code, discounts=discounts,
                                                                offer=offer, payment_gateway=self.payment_gateway,
                                                                user_id=self.user_id)
                    else:
                        error_message = "this coupon is invalid."

                if error_message:
                    # Fixed issue Frontend still send discount code while already return 400 from BE!
                    ioffer['discount_code'] = None
                    new_orderlines.append(ioffer)
                    self.order_lines = new_orderlines

                    return 'FAILED', err_response(status=httplib.BAD_REQUEST, error_code=CLIENT_400101,
                                                  developer_message=error_message, user_message=error_message)

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="order checkout 2",
                method=self.method, error=e, req=str(self.args))

    def check_payment_gateway(self):
        """
            CHECKING PAYMENT GATEWAY EXIST, GET CURRENCY BASE
        """
        try:
            payment = PaymentGateway.query.filter_by(id=self.payment_gateway).first()
            if not payment:
                return 'FAILED', bad_request_message(modul="order checkout 3",
                    method=self.method, param="payment_gateway",
                    not_found=self.payment_gateway, req=str(self.args))
            else:
                self.payment_gateway = payment
                self.base_currency = payment.get_currencies()['iso4217_code']
            return 'OK', 'OK'

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 3",
                method=self.method, error=e, req=str(self.args))

    def check_partner_id(self):
        try:
            partner = Partner.query.filter_by(id=self.partner_id).first()
            if not partner:
                return 'FAILED', bad_request_message(modul="order checkout 3",
                    method=self.method, param="partner_id",
                    not_found=self.partner_id, req=self.args)

            self.partner_id = partner
            return 'OK', 'OK'
        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 3",
                method=self.method, error=e, req=str(self.args))

    def construct_details(self):
        try:
            for item in self.args:
                if self.args[item] == None:
                    pass
                else:
                    if item in self.order_details:
                        self.order_details_dict[item] = self.args[item]
            return 'OK', 'OK'

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 3b",
                method=self.method, error=e, req=str(self.args))

    def checkout_order_details(self):
        try:
            self.order_details_dict['temporder_id'] = self.master_order.id
            order_detail = OrderDetail(**self.order_details_dict)
            sv = order_detail.save()

            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="order checkout 3c",
                    method=self.method, error=sv.get('message', ''), req=str(self.args))
            return 'OK', 'OK'

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 3c",
                method=self.method, error=e, req=str(self.args))

    # CALCULATION CHECKOUT FUNCTIONS ============================------------
    def calc_usd(self, discount=None, raw_price=None):
        usd_price = 0
        try:
            usd_price = discount.calculate_offer_discount(raw_price, 'USD')
            if usd_price <= 0:
                usd_price = 0

            self.su_price_usd.append(raw_price)
            if not self.platform: #later for, find tiers
                self.fi_price_usd.append(usd_price)

        except Exception as e:
            pass
        return usd_price

    def calc_idr(self, discount=None, raw_price=None):

        idr_price = 0
        try:
            idr_price = discount.calculate_offer_discount(raw_price, 'IDR')
            if idr_price <= 0:
                idr_price = 0

            self.su_price_idr.append(raw_price)
            self.fi_price_idr.append(idr_price)

        except Exception as e:
            pass

        return idr_price

    def calc_pts(self, discount=None, raw_price=None):
        pts_price = 0
        try:
            pts_price = discount.calculate_offer_discount(raw_price, 'PTS')
            if pts_price <= 0:
                pts_price = 0

            self.su_price_pts.append(raw_price)
            self.fi_price_pts.append(pts_price)

        except Exception as e:
            pass
        return pts_price

    def retrieve_bin_token(self):
        from app.paymentgateway_connectors.creditcard.models import PaymentCreditCardToken
        cc_token, bin_number = None, None
        cc_token = PaymentCreditCardToken.query.filter(PaymentCreditCardToken.id == self.cc_token_id,
                                                       PaymentCreditCardToken.user_id == self.user_id,
                                                       PaymentCreditCardToken.expiration >= datetime.now()).first()
        if cc_token:
            bin_number = cc_token.masked_credit_card.split('-')[0].strip()

        return cc_token, bin_number

    def allow_promotion_cutted(self, discount=None, offer=None):

        # first check if this promo allowed by normal promotion.
        if not discount.bin_codes:
            return (discount.discount_type == DISCOUNT_OFFER and discount.discount_valid() and
                    self.payment_gateway.id in discount.paymentgateway_id() and
                    self.platform in discount.platform_id() and offer.id in discount.offer_id())

        if discount.bin_codes and self.cc_token_id:
            cc_token, bin_number = self.retrieve_bin_token()

            # this if user buyed with cc already saved in profile user
            if cc_token:
                return (discount.discount_type == DISCOUNT_OFFER and discount.discount_valid() and
                        self.payment_gateway.id in discount.paymentgateway_id() and
                        self.platform in discount.platform_id() and offer.id in discount.offer_id() and
                        bin_number in discount.bin_codes)

        return False

    def allow_promotion_coupon_cutted(self, discount):

        # check normal promotion
        if not discount.bin_codes:
            return self.platform in discount.platform_id() and self.payment_gateway.id in discount.paymentgateway_id()

        # this for bin promotions
        if discount.bin_codes and self.cc_token_id:
            cc_token, bin_number = self.retrieve_bin_token()

            # this if user buyed with cc already saved in profile user
            if cc_token:
                return (self.platform in discount.platform_id() and
                        self.payment_gateway.id in discount.paymentgateway_id() and
                        bin_number in discount.bin_codes)
        return False

    # START CHECKOUT FUNCTIONS ===============================----------------
    def checkout_order_base(self):
        """
            CALCULATE PRICES, ( BASE PRICE, --> OFFER TABLE)
        """

        try:
            data_price = []

            #create order temp
            order_temp = self.insert_order()
            if order_temp[0] != 'OK':
                return 'FAILED', order_temp[1]

            for ioffer in self.order_lines:
                temp = {}
                offer_id = ioffer.get('offer_id', None)
                coupon_code = ioffer.get('discount_code', '')
                quantity = ioffer.get('quantity', None)
                offer_name = ioffer.get('name', None)
                top_up_amount = ioffer.get('top_up_amount', 0)

                if top_up_amount > 0:
                    self.is_top_up_wallet = True
                    coupon_code = ''

                offer = Offer.query.filter_by(id=offer_id).first()
                if offer:
                    if self.is_top_up_wallet:
                        # for top up wallet trx, assert, if offer type not wallet offer, raise error
                        assert_offer_for_wallet(offer)

                    #create temporary orderline
                    line_temp = self.insert_order_line(offer, quantity, offer_name, top_up_amount)
                    if line_temp[0] != 'OK':
                        return line_temp
                    else:
                        line_temp = line_temp[1]

                    #temporary store id in json for next step coupon_code
                    if coupon_code:
                        temp['orderline_id'] = line_temp.id

                    temp['status'] = httplib.CREATED
                    temp['offer_id'] = offer_id
                    temp['discount_code'] = coupon_code

                    if offer.price_usd:
                        temp['usd_price'] = '%.2f' % offer.price_usd
                    if offer.price_idr:
                        temp['idr_price'] = '%.2f' % offer.price_idr if not self.is_top_up_wallet else top_up_amount
                    if offer.price_point:
                        temp['pts_price'] = offer.price_point

                    # CHECK DISCOUNT DATA =============================== [ NORMAL DISCOUNTS ]
                    if offer.is_discount and not self.is_top_up_wallet and not self.is_renewal:
                        discount_data = Discount.query.filter(Discount.id.in_(offer.discount_id)).filter_by(
                            discount_type=DISCOUNT_OFFER, is_active=True).order_by(desc(Discount.id)).all()

                        for discount in discount_data:
                            if discount.discount_valid():
                                self.discount_go_cut = True
                                self.discount_data_valid.append(discount)

                    if self.discount_go_cut:
                        if self.discount_data_valid:
                            #Retrieve only OFFER DISCOUNTS
                            #DISCOUNT OFFER TYPE, DISCOUNT TIME STILL VALID,
                            #PAYMENT GATEWAY
                            self.discount_data_valid = list(set(self.discount_data_valid))

                            for discount in self.discount_data_valid:

                                check_promotion_cut = self.allow_promotion_cutted(discount=discount, offer=offer)

                                if check_promotion_cut:
                                    is_trial = True if discount.trial_time.days else False
                                    if is_trial:
                                        exist_trial = check_trial_promotions(user_id=line_temp.user_id,
                                                                             offer_id=line_temp.offer_id)
                                        if exist_trial:
                                            return 'FAILED', err_response(
                                                status=httplib.BAD_REQUEST, error_code=CLIENT_400101,
                                                developer_message=exist_trial, user_message=exist_trial)

                                    line_temp.is_discount = True
                                    line_temp.campaign_id = discount.campaign_id

                                    #write discount temp
                                    temp_discounts = self.insert_order_discounts(line_temp, discount)

                                    if temp_discounts[0] != 'OK':
                                        return temp_discounts[1]
                                    else:
                                        temp_discounts = temp_discounts[1]
                                    temp['discount_tag'] = discount.tag_name

                                    if offer.is_free:
                                        usd_price, idr_price, point_price = 0, 0, 0
                                        temp_discounts.price_usd = offer.price_usd
                                        temp_discounts.price_idr = offer.price_idr
                                        temp_discounts.price_point = offer.price_point

                                        temp_discounts.final_price_usd = usd_price
                                        temp_discounts.final_price_idr = idr_price
                                        temp_discounts.final_price_point = point_price

                                    #calculate discounts price
                                    if offer.price_usd:
                                        usd_price = self.calc_usd(discount,offer.price_usd)
                                        temp['final_usd_price'] = '%.2f' % usd_price

                                        line_temp.price_usd = offer.price_usd
                                        line_temp.final_price_usd = usd_price
                                        temp_discounts.price_usd = offer.price_usd
                                        temp_discounts.final_price_usd = usd_price

                                    if offer.price_idr:
                                        idr_price = self.calc_idr(discount, offer.price_idr)
                                        temp['final_idr_price'] = '%.2f' % idr_price

                                        temp_discounts.price_idr = offer.price_idr
                                        temp_discounts.final_price_idr = idr_price
                                        line_temp.price_idr = offer.price_idr
                                        line_temp.final_price_idr = idr_price

                                    if offer.price_point:
                                        point_price = self.calc_pts(discount, offer.price_point)
                                        temp['final_pts_price'] = point_price

                                        temp_discounts.price_point = offer.price_point
                                        temp_discounts.final_price_point = point_price
                                        line_temp.price_point = offer.price_point
                                        line_temp.final_price_point = point_price

                                    db.session.add(temp_discounts)
                                    break
                                else:
                                    xo = self.update_order_line_temp(offer, line_temp)
                                    temp.update(xo)
                        else:
                            xo = self.update_order_line_temp(offer, line_temp)
                            temp.update(xo)

                    elif not self.is_top_up_wallet:

                        ## CHECK DISCOUNT DATA =============================== [ PREDEFINED GROUP, SUCKS ]
                        group_predefined = []
                        offer_type = offer.get_item_objects()

                        if offer_type:

                            # SINGLE
                            item_data = Item.query.filter(Item.id.in_(
                                offer.get_items())).order_by(desc(Item.id)).first()
                        else:
                            # SUBSCRIPTIONS
                            item_data = Item.query.filter(Item.brand_id.in_(
                                offer.get_brands_id())).order_by(desc(Item.release_date)).first()

                        # HOTFIX : avoid promo for premium not cut from predefined group promos
                        if (item_data and offer.offer_type_id != BUFFET):

                            # MAGAZINE
                            if item_data.item_type == ITEM_TYPES[MAGAZINE]:

                                if offer.get_items():
                                    # MAGAZINE SINGLE
                                    group_predefined = [4,1,5,7]
                                else:
                                    # MAGAZINE SUBS
                                    group_predefined = [4,1,6,8]

                            # BOOK
                            if item_data.item_type == ITEM_TYPES[BOOK]:
                                # HARPER COLLINS 5%
                                if item_data.brand.vendor_id == HARPER_COLLINS:
                                    group_predefined = [HARPER_COLLINS]
                                else:
                                    group_predefined = [4,2,5]

                            # NEWSPAPERS
                            if item_data.item_type == ITEM_TYPES[NEWSPAPER]:
                                group_predefined = [4,3,6]

                            discount = Discount.query.filter(
                                Discount.predefined_group.in_(group_predefined),
                                Discount.valid_to >= get_local()
                            ).filter_by(
                                    discount_type=1, is_active=True,
                            ).join(Discount.paymentgateways).filter(
                                PaymentGateway.id == self.payment_gateway.id
                            ).join(Discount.platforms).filter(
                                Platform.id == self.platform
                            ).order_by(
                                desc(Discount.id)).first()

                        else:
                            discount = []

                        if discount:
                            #if (discount.discount_valid() and
                            #    self.payment_gateway.id in discount.paymentgateway_id() and
                            #    self.platform in discount.platform_id()):

                            line_temp.is_discount = True
                            line_temp.campaign_id = discount.campaign_id

                            #write discount temp
                            temp_discounts = self.insert_order_discounts(line_temp, discount)

                            if temp_discounts[0] != 'OK':
                                return temp_discounts[1]
                            else:
                                temp_discounts = temp_discounts[1]

                            temp['discount_tag'] = discount.tag_name

                            if offer.is_free:
                                usd_price, idr_price, point_price = 0, 0, 0
                                temp_discounts.price_usd = offer.price_usd
                                temp_discounts.price_idr = offer.price_idr
                                temp_discounts.price_point = offer.price_point

                                temp_discounts.final_price_usd = usd_price
                                temp_discounts.final_price_idr = idr_price
                                temp_discounts.final_price_point = point_price

                            #calculate discounts price
                            if offer.price_usd:
                                #if self.last_discount:
                                #    usd_price = self.calc_usd(discount, self.last_usd)
                                #else:
                                usd_price = self.calc_usd(discount,offer.price_usd)
                                temp['final_usd_price'] = '%.2f' % usd_price

                                line_temp.price_usd = offer.price_usd
                                line_temp.final_price_usd = usd_price
                                temp_discounts.price_usd = offer.price_usd
                                temp_discounts.final_price_usd = usd_price

                            if offer.price_idr:
                                #if self.last_discount:
                                #    idr_price = self.calc_idr(discount, self.last_idr)
                                #else:
                                idr_price = self.calc_idr(discount, offer.price_idr)
                                temp['final_idr_price'] = '%.2f' % idr_price

                                temp_discounts.price_idr = offer.price_idr
                                temp_discounts.final_price_idr = idr_price
                                line_temp.price_idr = offer.price_idr
                                line_temp.final_price_idr = idr_price

                            if offer.price_point:
                                #if self.last_discount:
                                #    point_price = self.calc_pts(discount, self.last_pts)
                                #else:
                                point_price = self.calc_pts(discount, offer.price_point)
                                temp['final_pts_price'] = point_price

                                temp_discounts.price_point = offer.price_point
                                temp_discounts.final_price_point = point_price
                                line_temp.price_point = offer.price_point
                                line_temp.final_price_point = point_price

                            #adding to session db
                            db.session.add(temp_discounts)

                        else:
                            #NO Discounts, same as price normal
                            xo = self.update_order_line_temp(offer, line_temp)
                            temp.update(xo)

                    #adding to session db
                    db.session.add(line_temp)
                else:
                    temp['status'] = httplib.NOT_FOUND
                    temp['error'] = "Invalid Offer"
                    temp['offer_id'] = offer_id

                    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                        error_code=CLIENT_400101,
                        developer_message="No Offer for platform : %s, offer_id : %s" % (self.platform, offer_id),
                        user_message="Payment Invalid")

                data_price.append(temp)
                self.data_price = data_price

            db.session.commit()
            self.update_order_master()
            return 'OK', 'OK'

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 4",
                method=self.method, error=e, req=str(self.args))

    def validate_max_min(self, disc=None, raw_usd=None, raw_idr=None):
        if str(self.base_currency) in ['USD', 'IDR']:
            if disc.min_idr_order_price and disc.max_idr_order_price:

                if (float(raw_idr) >= float(disc.min_idr_order_price) and float(raw_idr) <= float(disc.max_idr_order_price) ):
                    self.coupon_can_used = True

            elif disc.min_usd_order_price and disc.max_usd_order_price:
                if (float(raw_usd) >= float(disc.min_usd_order_price) and float(raw_usd) <= float(disc.max_usd_order_price)):
                    self.coupon_can_used = True

            elif disc.min_idr_order_price:
                if float(raw_idr) >= float(disc.min_idr_order_price):
                    self.coupon_can_used = True

            elif disc.min_usd_order_price:
                if float(raw_usd) >= float(disc.min_usd_order_price):
                    self.coupon_can_used = True
            else:
                self.coupon_can_used = True

    def validate_coupon_code(self, discounts=None, offers=None, raw_usd=None, raw_idr=None):
        try:

            # check coupon code by BIN PROMOTION
            allow_coupon_code_cut = self.allow_promotion_coupon_cutted(discounts)
            if allow_coupon_code_cut:

                # check if discounts is all offers
                # HOTFIX to avoid buffet premium not get discounts predefined
                if (discounts.predefined_group and offers.offer_type_id != BUFFET):

                    offer_type = offers.get_item_objects()

                    if offer_type:
                        # SINGLE
                        item_data = Item.query.filter(Item.id.in_(
                            offers.get_items())).order_by(desc(Item.id)).first()
                    else:
                        # SUBSCRIPTIONS
                        item_data = Item.query.filter(Item.brand_id.in_(
                            offers.get_brands_id())).order_by(desc(Item.release_date)).first()

                    if item_data:

                        # MAGAZINE
                        if item_data.item_type == ITEM_TYPES[MAGAZINE]:

                            if offers.get_items():
                                # MAGAZINE SINGLE
                                if discounts.predefined_group in [4,1,5,7,9]:
                                    self.validate_max_min(discounts, raw_usd, raw_idr)
                            else:
                                # MAGAZINE SUBS
                                if discounts.predefined_group in [4,1,6,8,9]:
                                    self.validate_max_min(discounts, raw_usd, raw_idr)

                        # BOOK
                        if item_data.item_type == ITEM_TYPES[BOOK]:
                            # BOOK SINGLE
                            if discounts.predefined_group in [4,2,5]:
                               self.validate_max_min(discounts, raw_usd, raw_idr)

                        # NEWSPAPER
                        if item_data.item_type == ITEM_TYPES[NEWSPAPER]:
                            if discounts.predefined_group in [4,3,6,9]:
                                self.validate_max_min(discounts, raw_usd, raw_idr)

                    else:
                        self.coupon_can_used = False

                # SPECIAL CASE FOR NOT PREDEFINED GROUP
                if not discounts.predefined_group:

                    # CHECKING OFFER INSIDE DISCOUNT OFFERS
                    if (int(offers.id) in discounts.offer_id()):
                        self.coupon_can_used = True

                    # CHECK IF MIN ORDER
                    if str(self.base_currency) in ['USD', 'IDR']:
                        if discounts.min_idr_order_price and discounts.max_idr_order_price:
                            if (float(raw_idr) >= float(discounts.min_idr_order_price) and float(raw_idr) <= float(discounts.max_idr_order_price) ):
                                self.coupon_can_used = True

                        elif discounts.min_usd_order_price and discounts.max_usd_order_price:
                            if (float(raw_usd) >= float(discounts.min_usd_order_price) and float(raw_usd) <= float(discounts.max_usd_order_price)):
                                self.coupon_can_used = True

                        elif discounts.min_idr_order_price:
                            if float(raw_idr) >= float(discounts.min_idr_order_price):
                                self.coupon_can_used = True

                        elif discounts.min_usd_order_price:
                            if float(raw_usd) >= float(discounts.min_usd_order_price):
                                self.coupon_can_used = True
            else:
                self.coupon_can_used = False

        except Exception as e:
            self.coupon_can_used = False

    def checkout_coupon_code(self):
        """
            CALCULATE COUPON CODE, ( BASE PRICE, --> OFFER TABLE)
        """
        try:
            #Reset all sum price
            self.su_price_usd, self.fi_price_usd = [], []
            self.su_price_idr, self.fi_price_idr = [], []
            self.su_price_pts, self.fi_price_pts = [], []

            data_price = []

            for iprice in self.data_price:
                temp = {}
                coupon = iprice.get('discount_code', '')
                status = iprice.get('status', 204)
                offer_id = iprice.get('offer_id', 0)

                tag = iprice.get('discount_tag', None)
                raw_usd = iprice.get('final_usd_price', None)
                raw_idr = iprice.get('final_idr_price', None)
                raw_pts = iprice.get('final_pts_price', None)

                orderline_id = iprice.get('orderline_id', 0)

                temp['discount_code'] = coupon
                temp['offer_id'] = offer_id
                temp['status'] = httplib.CREATED

                if tag:
                    temp['discount_tag'] = tag

                if status != 204 and coupon:
                    offer = Offer.query.filter_by(id=offer_id).first()
                    coupon_code = DiscountCode.query.filter_by(code=coupon.lower()).first()

                    if coupon_code and offer:
                        xdiscount = coupon_code.get_discount()

                        if coupon_code.coupon_can_used() and xdiscount.discount_valid():

                            # check if coupon code can overide the others promo?
                            # if cant, calculate from base prices
                            # if can, (stack promo calculations )
                            if coupon_code.discount_type == CANT_OVERIDE_OFFER_DISCOUNT:
                                raw_usd = offer.price_usd
                                raw_idr = offer.price_idr

                            # validate coupon code if can used or not
                            self.validate_coupon_code(xdiscount, offer, raw_usd, raw_idr)

                        if self.coupon_can_used:
                            #can cut price, even offer already has discount offer
                            discount = coupon_code.get_discount()

                            temp['coupon_code_tag'] = discount.tag_name

                            if raw_usd:
                                raw_usd = decimal.Decimal(raw_usd)
                                price_usd = self.calc_usd(discount, raw_usd)
                            else:
                                price_usd = 0

                            if raw_idr:
                                raw_idr = decimal.Decimal(raw_idr)
                                price_idr = self.calc_idr(discount, raw_idr)
                            else:
                                price_idr = 0

                            if raw_pts:
                                raw_pts = int(raw_pts)
                                price_pts = self.calc_pts(discount, raw_pts)
                            else:
                                price_pts = 0

                            temp['final_usd_price'] = '%.2f' % price_usd
                            temp['usd_price'] = '%.2f' % raw_usd

                            temp['final_idr_price'] = '%.2f' % price_idr
                            temp['idr_price'] = '%.2f' % raw_idr

                            temp['final_pts_price'] = price_pts
                            temp['pts_price'] = raw_pts

                            orderline, exist_trial = self.update_order_line_temp_coupon(orderline_id=orderline_id,
                                    raw_usd=raw_usd, final_usd=price_usd, raw_idr=raw_idr,
                                    final_idr=price_idr, raw_pts=raw_pts, final_pts=price_pts,
                                    discount=discount, coupon=coupon)

                            if exist_trial:
                                return 'FAILED', err_response(status=httplib.BAD_REQUEST, error_code=CLIENT_400101,
                                                              developer_message=exist_trial, user_message=exist_trial)

                            # Reset coupon can used
                            self.coupon_can_used = False

                        else:
                            normal = self.coupon_code_normal(raw_usd, raw_idr, raw_pts, offer)
                            temp.update(normal)

                    else:
                        normal = self.coupon_code_normal(raw_usd, raw_idr, raw_pts, offer)
                        temp.update(normal)

                else:
                    temp['status'] = httplib.NOT_FOUND
                    temp['error'] = "Invalid Offer"
                    temp['offer_id'] = offer_id

                self.update_order_master()
                data_price.append(temp)

            self.data_price = data_price
            return 'OK', 'OK'

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 9",
                method=self.method, error=e, req=str(self.args))

    def checkout_payments_gateways(self):
        try:
            data_price = []
            for iprice in self.data_price:
                temp = {}

                status = iprice.get('status', 204)
                offer_id = iprice.get('offer_id', 0)

                tag = iprice.get('discount_tag', None)
                raw_usd = iprice.get('final_usd_price', None)
                raw_idr = iprice.get('final_idr_price', None)
                raw_pts = iprice.get('final_pts_price', None)

                offer = Offer.query.filter_by(id=offer_id).first()
                line_temp = OrderLineTemp.query.filter_by(offer_id=offer_id, order_id=self.master_order.id).first()

                #get discounts payment gateways
                discount_data = Discount.query.filter_by(discount_type=3).all()

                if discount_data:
                    for discount in discount_data:
                        if (discount.discount_valid() and self.payment_gateway.id in discount.paymentgateway_id()
                            and offer.id in discount.offer_id()):

                            temp['coupon_code_tag'] = discount.tag_name

                            if raw_usd:
                                raw_usd = decimal.Decimal(raw_usd)
                                price_usd = self.calc_usd(discount, raw_usd)
                            else:
                                price_usd = 0

                            if raw_idr:
                                raw_idr = decimal.Decimal(raw_idr)
                                price_idr = self.calc_idr(discount, raw_idr)
                            else:
                                price_idr = 0

                            if raw_pts:
                                raw_pts = int(raw_pts)
                                price_pts = self.calc_pts(discount, raw_pts)
                            else:
                                price_pts = 0

                            temp['final_usd_price'] = '%.2f' % price_usd
                            temp['usd_price'] = '%.2f' % raw_usd

                            temp['final_idr_price'] = '%.2f' % price_idr
                            temp['idr_price'] = '%.2f' % raw_idr

                            temp['final_pts_price'] = price_pts
                            temp['pts_price'] = raw_pts

                            self.update_order_line_temp_coupon(orderline_id=line_temp.id,
                                raw_usd=raw_usd, final_usd=price_usd, raw_idr=raw_idr,
                                final_idr=price_idr, raw_pts=raw_pts, final_pts=price_pts,
                                discount=discount, coupon='')

                        else:
                            normal = self.coupon_code_normal(raw_usd, raw_idr, raw_pts, offer)
                            temp.update(normal)

            self.update_order_master()
            return 'OK', 'OK'

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 15",
                method=self.method, error=e, req=str(self.args))

    # END CHECKOUT FUNCTIONS ===============================----------------
    def insert_order(self):
        try:
            order_number = generate_invoice()
            temp = OrderTemp(
                order_number = order_number,
                user_id = self.user_id,
                order_status = 10000,
                is_active = True,
                platform_id = self.platform,
                is_renewal = self.is_renewal
            )
            self.order_number = order_number

            if self.payment_gateway:
                temp.paymentgateway_id = self.payment_gateway.id

            if self.partner_id:
                temp.partner_id = self.partner_id.id

            if self.client_id:
                temp.client_id = self.client_id

            sv = temp.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="order checkout 5",
                    method=self.method, error=sv.get('message', ''),
                    req=str(self.args))

            self.master_order = temp

            try:
                #log records db args
                log_order = OrderLog(
                    user_id = self.user_id,
                    order_temp_id = temp.id,
                    platform_id = self.platform,
                    request = str(self.args),
                    api = 'checkout'
                )
                log_order.save()
            except Exception as e:
                pass

            return 'OK', 'OK'

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 5",
                method=self.method, error=e, req=str(self.args))

    def insert_order_line(self, offer=None, quantity=None, name=None, top_up_amount=None):
        try:
            temp = OrderLineTemp(
                offer_id=offer.id,
                is_active=True,
                is_free=offer.is_free,
                user_id=self.user_id,
                orderline_status=10000,
                order_id=self.master_order.id,
            )

            #SCOOP-2150
            if quantity is not None and quantity <= 0:
                raise BadRequest(developer_message='Quantity order <= 0 not allowed')
            elif quantity:
                temp.quantity = quantity
            else:
                # mobile apps didn't send quantity, so it's quantity = None, which should translate to 1
                temp.quantity = 1

            if top_up_amount:
                temp.price_idr = top_up_amount
                temp.final_price_idr = top_up_amount

            if name:
                temp.name = name
            else:
                brand = offer.get_brand_objects()
                item = offer.get_item_objects()

                #if subscriptions format name [ brand ] offer-name
                if offer.offer_type_id in [SUBSCRIPTIONS, BUNDLE]:
                    if brand:
                        name_insert = '%s - %s' % (brand[0].get('name', ''), offer.name)
                    else:
                        name_insert = offer.name

                if offer.offer_type_id in [SINGLE, BUFFET]:
                    if item:
                        if len(item[0].get('name', '')) > 100:
                            name_insert = '%s' % (item[0].get('name', '')[:100])
                        else:
                            name_insert = '%s' % (item[0].get('name', ''))
                    else:
                        name_insert = offer.name

                temp.name = name_insert

            sv = temp.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="order checkout 6",
                    method=self.method, error=sv.get('message', ''),
                    req=str(self.args))

            self.master_order_line = temp
            return 'OK', temp

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 6",
                method=self.method, error=e, req=str(self.args))

    def insert_order_discounts(self, order_line=None, discount=None):
        try:
            temp = OrderLineDiscountTemp(
                order_id = self.master_order.id,
                orderline_id = order_line.id,
                discount_id = discount.id,
                discount_name = discount.name,
                discount_type = discount.discount_type,
                discount_usd = discount.discount_usd,
                discount_idr = discount.discount_idr,
                discount_point = discount.discount_point
            )

            sv = temp.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="order checkout 7",
                    method=self.method, error=sv.get('message', ''),
                    req=str(self.args))
            return 'OK', temp

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 7",
                method=self.method, error=e, req=str(self.args))

    def update_order_master(self):
        """ Update order database """
        self.master_order.sums_data_order_line()

    def update_order_line_temp(self, offer=None, line_temp=None):
        """
            Update order_line_temp price only from offer base or offer platform
        """
        temp = {}
        line_temp.is_discount = False

        if offer.is_free:
            line_temp.price_usd = 0
            line_temp.final_price_usd = 0
            line_temp.price_idr = 0
            line_temp.final_price_idr = 0
            line_temp.price_point = 0
            line_temp.final_price_point = 0

            temp['final_usd_price'], temp['usd_price'] = 0, 0
            temp['final_idr_price'], temp['idr_price'] = 0, 0
            temp['final_pts_price'], temp['pts_price'] = 0, 0

            self.su_price_usd.append(0)
            self.fi_price_usd.append(0)
            self.su_price_idr.append(0)
            self.fi_price_idr.append(0)
            self.su_price_pts.append(0)
            self.fi_price_pts.append(0)

        elif offer.offer_type_id != WALLET_OFFER_TYPE_ID:

            #TODO: REFACTOR THIS WHEN MIGRATE TO PYTHON 3.0
            if offer.price_usd:
                line_temp.price_usd = offer.price_usd
                line_temp.final_price_usd = offer.price_usd * line_temp.quantity

                self.su_price_usd.append(offer.price_usd)
                self.fi_price_usd.append(offer.price_usd * line_temp.quantity)

                temp['final_usd_price'] = '%.2f' % (offer.price_usd * line_temp.quantity)

            if offer.price_idr:
                line_temp.price_idr = offer.price_idr
                line_temp.final_price_idr = offer.price_idr * line_temp.quantity

                self.su_price_idr.append(offer.price_idr)
                self.fi_price_idr.append(offer.price_idr * line_temp.quantity)

                temp['final_idr_price'] = '%.2f' % (offer.price_idr * line_temp.quantity)

            if offer.price_point:
                line_temp.price_point = offer.price_point
                line_temp.final_price_point = offer.price_point * line_temp.quantity

                self.su_price_pts.append(offer.price_point)
                self.fi_price_pts.append(offer.price_point * line_temp.quantity)

                temp['final_pts_price'] = (offer.price_point * line_temp.quantity)

        db.session.add(line_temp)
        return temp

    def update_order_line_temp_coupon(self, orderline_id=None,
        raw_usd=0, final_usd=0, raw_idr=0, final_idr=0,
        raw_pts=0, final_pts=0, discount=None, coupon=None):

        """
            Update orderline temp via coupon code,
            create record discount_temp for info cut
        """
        orderline = OrderLineTemp.query.filter_by(id=orderline_id).first()
        if orderline:
            ords = OrderLineDiscountTemp(
                order_id = self.master_order.id,
                orderline_id = orderline_id,
                discount_id = discount.id,
                discount_name = discount.name,
                discount_type = discount.discount_type,
                discount_usd = discount.discount_usd,
                discount_idr = discount.discount_idr,
                discount_point = discount.discount_point,
                discount_code = coupon
            )

            orderline.is_discount = True
            orderline.is_trial = True if discount.trial_time.days else False

            #orderline.price_usd = raw_usd
            orderline.final_price_usd = final_usd
            ords.price_usd = raw_usd
            ords.final_price_usd = final_usd

            #orderline.price_idr = raw_idr
            orderline.final_price_idr = final_idr
            ords.price_idr = raw_idr
            ords.final_price_idr = final_idr

            #orderline.price_point = raw_pts
            orderline.final_price_point = final_pts
            orderline.campaign_id = discount.campaign_id

            ords.price_point = raw_pts
            ords.final_price_point = final_pts

            db.session.add(orderline)
            db.session.add(ords)
            db.session.commit()

            if orderline.is_trial:
                exist_trial = check_trial_promotions(user_id=orderline.user_id, offer_id=orderline.offer_id)
                if exist_trial:
                    return orderline, exist_trial

        return None, None

    def coupon_code_normal(self, final_usd=None, final_idr=None, final_pts=None, offer=None):
        """
            Just for displaying normal value, from coupon code no update
        """
        temp = {}

        if offer.is_free:
            temp['final_usd_price'], temp['usd_price'] = 0, 0
            temp['final_idr_price'], temp['idr_price'] = 0, 0
            temp['final_pts_price'], temp['pts_price'] = 0, 0

            self.su_price_usd.append(0)
            self.fi_price_usd.append(0)
            self.su_price_idr.append(0)
            self.fi_price_idr.append(0)
            self.su_price_pts.append(0)
            self.fi_price_pts.append(0)

        else:
            #if not offer include inside discount, return normal price
            if final_usd:
                temp['final_usd_price'] = final_usd
                temp['usd_price'] = final_usd

                self.su_price_usd.append(decimal.Decimal(final_usd))
                self.fi_price_usd.append(decimal.Decimal(final_usd))

            if final_idr:
                temp['final_idr_price'] = final_idr
                temp['idr_price'] = final_idr

                self.su_price_idr.append(decimal.Decimal(final_idr))
                self.fi_price_idr.append(decimal.Decimal(final_idr))

            if not final_pts:
                temp['final_pts_price'] = 0
                temp['pts_price'] = 0
            else:
                temp['final_pts_price'] = final_pts
                temp['pts_price'] = final_pts

                self.su_price_pts.append(int(final_pts))
                self.fi_price_pts.append(int(final_pts))

        return temp

    def get_master_item(self, item_id=None):
        """
            Get ITEM MASTER DATA
        """
        data_item = None

        #ITEM READY FOR CONSUME
        allowed_item_status = [STATUS_TYPES[STATUS_READY_FOR_CONSUME], STATUS_TYPES[STATUS_MCGRAWHILL_CONTENT]]
        item = Item.query.filter(Item.id == item_id, Item.item_status.in_(allowed_item_status)).first()
        if item:
            data_item = item
        return data_item

    def json_items(self, orderline=None):
        data_item = []

        offer = orderline.get_offer()
        if offer:
            if offer.offer_type_id in [SUBSCRIPTIONS, BUFFET]:
                if offer.offer_type_id == BUFFET:
                    # get only 1 brands, to prevent error because of too many brands in a buffet
                    brands = [offer.brands[0].id, ] if offer.brands[0] else []
                else:
                    brands = offer.get_brands_id()

                for ibrands in brands:
                    item = {}

                    item_brands = Item.query.filter_by(brand_id=ibrands, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).order_by(
                        desc(Item.release_date)).limit(1).all()

                    if item_brands:
                        ms_it = item_brands[0]
                        item['name'] = unidecode(unicode(ms_it.name))
                        item['edition_code'] = ms_it.edition_code
                        item['item_status'] = {v:k for k,v in STATUS_TYPES.items()}[ms_it.item_status]
                        item['release_date'] = ms_it.release_date.isoformat()
                        item['is_featured'] = ms_it.is_featured
                        item['vendor'] = ms_it.get_vendor()
                        item['languages'] = ms_it.get_languages()
                        item['countries'] = ms_it.get_country()
                        item['authors'] = ms_it.get_authors()
                        data_item.append(item)

                if offer.offer_type_id == 4:
                    data_item = data_item[0]
                    data_item['name'] = orderline.name
                    data_item = [data_item]

            else:
                for xo in offer.get_items():
                    item = {}
                    ms_it = self.get_master_item(xo)
                    item['name'] = unidecode(unicode(ms_it.name))
                    item['id'] = ms_it.id
                    item['edition_code'] = ms_it.edition_code
                    item['item_status'] = {v:k for k,v in STATUS_TYPES.items()}[ms_it.item_status]
                    item['release_date'] = ms_it.release_date.isoformat()
                    item['is_featured'] = ms_it.is_featured
                    item['vendor'] = ms_it.get_vendor()
                    item['languages'] = ms_it.get_languages()
                    item['countries'] = ms_it.get_country()
                    item['authors'] = ms_it.get_authors()
                    data_item.append(item)

        return data_item

    def json_orderlines(self):
        orderlines = self.master_order.get_orderline_temp()

        order = []
        for dataline in orderlines:

            temp = {}
            temp['name'] = dataline.name
            temp['is_free'] = dataline.is_free
            temp['is_trial'] = dataline.is_trial
            temp['offer_id'] = dataline.offer_id

            if self.payment_gateway:
                cur = self.payment_gateway.get_currencies()
                currency = cur.get('iso4217_code', '')

                if (currency == 'USD'):
                    temp['final_price'] = '%.2f' % dataline.final_price_usd
                    temp['raw_price'] = '%.2f' % dataline.price_usd
                    temp['currency_code'] = currency
                    #temp['payment_gateway'] = self.payment_gateway.name

                if (currency == 'IDR'):
                    temp['final_price'] = '%.2f' % dataline.final_price_idr
                    temp['raw_price'] = '%.2f' % dataline.price_idr
                    temp['currency_code'] = currency
                    #temp['payment_gateway'] = self.payment_gateway.name

                if (currency == 'PTS'):
                    temp['final_price'] = dataline.price_point
                    temp['raw_price'] = dataline.final_price_point
                    temp['currency_code'] = currency
                    #temp['payment_gateway'] = self.payment_gateway.name
            else:
                temp['final_amount_usd'] = '%.2f' % dataline.final_price_usd
                temp['total_amount_usd'] = '%.2f' % dataline.price_usd

                temp['final_amount_idr'] = '%.2f' % dataline.final_price_idr
                temp['total_amount_idr'] = '%.2f' % dataline.price_idr

                temp['final_amount_point'] = dataline.price_point
                temp['total_amount_point'] = dataline.final_price_point

            temp['discount_code'] = self.coupon_list_used.get(dataline.offer_id, None)
            temp['items'] = self.json_items(dataline)
            order.append(temp)
        return order

    def json_success(self):
        temp = {}
        temp['user_id'] = self.user_id

        if self.payment_gateway:
            temp['payment_gateway_id'] = self.payment_gateway.id
            cur = self.payment_gateway.get_currencies()
            currency = cur.get('iso4217_code', '')

            if (currency == 'USD'):
                temp['final_amount'] = '%.2f' % self.master_order.final_price_usd
                temp['total_amount'] = '%.2f' % self.master_order.price_usd
                temp['currency_code'] = currency

                if (0 == self.master_order.final_price_usd):
                    # HACK FOR COUPON CODES IF 0.00
                    self.master_order.paymentgateway_id = FREE
                    self.master_order.save()

                    temp['payment_gateway_name'] = "Free Purchase"
                    temp['payment_gateway_id'] = FREE
                else:
                    temp['payment_gateway_name'] = self.payment_gateway.name

            if (currency == 'IDR'):
                temp['final_amount'] = '%.2f' % self.master_order.final_price_idr
                temp['total_amount'] = '%.2f' % self.master_order.price_idr
                temp['currency_code'] = currency

                if (0 == self.master_order.final_price_idr):
                    # HACK FOR COUPON CODES IF 0.00
                    self.master_order.paymentgateway_id = FREE
                    self.master_order.save()

                    temp['payment_gateway_name'] = "Free Purchase"
                    temp['payment_gateway_id'] = FREE
                else:
                    temp['payment_gateway_name'] = self.payment_gateway.name

            if (currency == 'PTS'):
                temp['final_amount'] = self.master_order.final_price_point
                temp['total_amount'] = self.master_order.price_point
                temp['currency_code'] = currency
                temp['payment_gateway_name'] = self.payment_gateway.name

        else:
            if self.su_price_usd:
                temp['final_amount_usd'] = '%.2f' % sum(self.fi_price_usd)
                temp['total_amount_usd'] = '%.2f' % sum(self.su_price_usd)
            if self.su_price_idr:
                temp['final_amount_idr'] = '%.2f' % sum(self.fi_price_idr)
                temp['total_amount_idr'] = '%.2f' % sum(self.su_price_idr)
            if self.su_price_pts:
                temp['final_amount_point'] = sum(self.fi_price_pts)
                temp['total_amount_point'] = sum(self.su_price_pts)

        temp['temp_order_id'] = self.master_order.id
        temp['order_number'] = self.order_number
        temp['order_status'] = self.master_order.order_status
        temp['order_lines'] = self.json_orderlines()

        temp['is_active'] = self.master_order.is_active
        temp['client_id'] = self.master_order.client_id
        temp['partner_id'] = self.master_order.partner_id

        promo_sum, promo_detail = retrieve_promotions_checkout(order=self.master_order)
        temp['discount_details'] = promo_detail
        temp['discount_summary'] = promo_sum

        if self.payment_gateway.payment_group == PAYMENTGATEWAY_GROUP[GROUP_E2PAY]:
            signature = order_signature_encrypt(value=self.master_order.id)
            temp['confirm_page'] = os.path.join(app.config['EBOOKS_URL_BASE'], 'payments/e2pay/?s={}'.format(signature))
            temp['confirm_success_page'] = os.path.join(app.config['EBOOKS_URL_BASE'],
                                                        'pay/success/?s={}'.format(signature))
        else:
            temp['confirm_page'] = ""
            temp['confirm_success_page'] = ""

        return Response(json.dumps(temp), status=httplib.CREATED, mimetype='application/json')

    def check_points_users(self):
        try:
            pts_user = PointUser.query.filter_by(user_id=self.user_id).first()
            if pts_user:
                point_user = int(pts_user.point_amount)
            else:
                point_user = 0

            buying_pts = int(self.master_order.final_price_point)
            if point_user >= buying_pts:
                return 'OK', 'OK'
            else:
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    error_code=CLIENT_400101,
                    developer_message="Payment Invalid, Points not enough",
                    user_message="Payment Invalid")

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order checkout 17",
                method=self.method, error=e, req=str(self.args))

    def assert_order_meets_gateway_minimum(self, order_temp, payment_gateway):
        """
        :param `app.orders.models.OrderTemp` order_temp:
        :param `app.payment_gateways.models.PaymentGateway` payment_gateway:
        :return:
        """
        if payment_gateway.minimal_amount:
            if payment_gateway.minimal_amount > order_temp.final_price and \
                not (order_temp.final_price_usd or order_temp.final_price_idr) == 0:

                raise PaymentBelowGatewayMinimumError(
                    order_amount=order_temp.final_price,
                    payment_gateway=payment_gateway
                )

    def construct(self):
        orderlines = self.check_orderlines_offer()
        if orderlines[0] != 'OK':
            return orderlines[1]

        if self.partner_id:
            part = self.check_partner_id()
            if part[0] != 'OK':
                return part[1]

        cpn = self.check_coupon_code()
        if cpn[0] != 'OK':
            return cpn[1]

        odr_detail = self.construct_details()
        if odr_detail[0] != 'OK':
            return odr_detail[1]

        if self.payment_gateway:
            payment = self.check_payment_gateway()
            if payment[0] != 'OK':
                return payment[1]

        #1 FIRST : CHECKING OFFER DISCOUNT VALUE
        order = self.checkout_order_base()
        if order[0] != 'OK':
            return order[1]

        #1.b INSERT ORDER DETAILS
        order_detail = self.checkout_order_details()
        if order_detail[0] != 'OK':
            return order_detail[1]

        #2 SECOND : CHECKING COUPON CODE VALUE PER-OFFER
        coupon = self.checkout_coupon_code()
        if coupon[0] != 'OK':
            return coupon[1]

        #3. THIRD : CHECKING PAYMENT GATEWAY
        pgmt = self.checkout_payments_gateways()
        if pgmt[0] != 'OK':
            return pgmt[1]

        #4. FOURTH : CHECKING ORDER DISCOUNTS

        if self.payment_gateway.id == POINT: #SPECIAL CASE FOR POINTS
            pts_chk = self.check_points_users()
            if pts_chk[0] != 'OK':
                return pts_chk[1]
        elif self.payment_gateway.id == WALLET:
            assert_wallet_balance_by_party_id(
                session=db.session(),
                party_id=self.master_order.user_id,
                order_amount=self.master_order.final_price_idr
            )

        # special case for checking minimum amount paymentgateways
        self.assert_order_meets_gateway_minimum(
            order_temp=self.master_order,
            payment_gateway=self.payment_gateway
        )

        return self.json_success()
