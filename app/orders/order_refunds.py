import ast
import json
import httplib
import decimal

from datetime import datetime

from flask_restful import Resource, Api, reqparse
from flask_restful import Api, reqparse
from flask import abort, Response, jsonify, request

from app import app, db
from app.constants import *
from app.auth.decorators import token_required, custom_headers

from app.helpers import log_api, assertion_error, message_json
from app.helpers import dtserializer, err_response, date_validator
from app.helpers import conflict_message, bad_request_message, internal_server_message

from sqlalchemy.sql import and_, or_, desc

from app.orders.models import OrderRefund, Order, OrderLine
from app.payments.models import Payment
from app.master.models import Currencies


class OrderProcess():

    def __init__(self, args=None, method=None, old_data=None):
        self.args = args
        self.method = method
        self.old_data = old_data

        self.user_id = args.get('user_id', None)
        self.origin_payment_id = args.get('origin_payment_id', None)
        self.origin_order_id = args.get('origin_order_id', None)
        self.origin_orderline_id = args.get('origin_orderline_id', None)

        self.refund_amount = args.get('refund_amount', None)
        self.refund_currency = args.get('refund_currency_id', 0)

        self.refund_datetime = args.get('refund_datetime', '')

        self.refund_dict_master = ['pic_id', 'user_id', 'origin_payment_id',
            'origin_order_id', 'origin_orderline_id', 'refund_amount',
            'refund_datetime', 'refund_reason', 'finance_status',
            'ticket_number', 'note', 'is_active', 'refund_currency_id',]
        self.refund_master = {}

    def recheck_refund(self):

        try:
            order_data = Order.query.filter_by(id=self.origin_order_id).first()
            payment_data = Payment.query.filter_by(id=self.origin_payment_id).first()
            currency = Currencies.query.filter_by(id=self.refund_currency).first()

            if not order_data:
                return 'FAILED', bad_request_message(modul="refund 1", method=self.method,
                    param="Origin order id", not_found=self.origin_order_id, req=self.args)

            elif not payment_data:
                return 'FAILED', bad_request_message(modul="refund 1", method=self.method,
                    param="Origin payment id", not_found=self.origin_payment_id, req=self.args)

            elif not currency:
                return 'FAILED', bad_request_message(modul="refund 1", method=self.method,
                    param="Refund currency id", not_found=self.refund_currency, req=self.args)

            else:

                # recheck orderline same order_id?
                order_line = order_data.get_order_line()
                line_id = [ i.id for i in order_line]

                if self.origin_orderline_id not in line_id:
                    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                        error_code=400101,
                        developer_message="Invalid Origin orderline Id, no relation with Order Record",
                        user_message="Invalid Orderline Id")

                # recheck order with payment records
                if self.origin_order_id != payment_data.order_id:
                    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                        error_code=400101,
                        developer_message="Invalid Origin payment Id, no relation with Order Record",
                        user_message="Invalid Payment Id")

                # recheck user_id inside order_id?
                if self.user_id != order_data.user_id:
                    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                        error_code=400101,
                        developer_message="Invalid Pic, no relation with Order user id",
                        user_message="Invalid Pic")

                ## recheck if refund > more than purchase price
                #if decimal.Decimal(self.refund_amount) > decimal.Decimal(order_data.final_amount):
                #    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                #        error_code=400101,
                #        developer_message="Invalid refund amount, amount execeded order final amount",
                #        user_message="Invalid refund amount")

                ## recheck currency vs orders currency
                #if currency.iso4217_code != order_data.currency_code:
                #    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                #        error_code=400101,
                #        developer_message="Invalid currency, currency order data : %s" % order_data.currency_code,
                #        user_message="Invalid refund currency")

                # recheck date refund formats
                refund_date = date_validator(self.refund_datetime, 'custom')
                if refund_date.get('invalid', False):
                    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                        error_code=400101,
                        developer_message="Invalid refund Date",
                        user_message="Invalid refund date")

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="refund 1", method=self.method,
                error=e, req=str(self.args))

    def construct_parameters(self):
        try:
            for item in self.args:
                if self.args[item] == None:
                    pass
                else:
                    if item in self.refund_dict_master:
                        self.refund_master[item] = self.args[item]
            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="refund 2", method=self.method,
                error=e, req=str(self.args))

    def post_data(self):
        try:

            m = OrderRefund(**self.refund_master)
            sv = m.save()

            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="refund 3",
                    method=self.method, error=sv.get('message', ''), req=str(self.args))
            else:
                rv = m.values()
                return 'OK', Response(json.dumps(rv), status=httplib.CREATED,
                    mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="refund 3", method=self.method,
                error=e, req=str(self.args))

    def put_data(self):
        try:

            for k, v in self.refund_master.iteritems():
                setattr(self.old_data, k, v)

            sv = self.old_data.save()
            if sv.get('invalid', False):
                return 'FAILED', internal_server_message(modul="refund 4", method=self.method,
                    error=sv.get('message', ''), req=str(self.args))
            else:
                rv = self.old_data.values()
                return 'OK', Response(json.dumps(rv), status=httplib.OK,
                    mimetype='application/json')

        except Exception, e:
            return 'FAILED', internal_server_message(modul="refund 4", method=self.method,
                error=e, req=str(self.args))

    def construct(self):
        order = self.recheck_refund()
        if order[0] != 'OK':
            return order[1]

        param = self.construct_parameters()
        if param[0] != 'OK':
            return param[1]

        if self.method == 'POST':
            post_data = self.post_data()
            return post_data[1]

        if self.method == 'PUT':
            put_data = self.put_data()
            return put_data[1]



