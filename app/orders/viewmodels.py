from flask_appsfoundry.viewmodels import SaViewmodelBase


class RemoteOrderViewmodel(SaViewmodelBase):

    @property
    def order_lines_list(self):
        return [ol for ol in self._model_instance.order_lines.all()]
