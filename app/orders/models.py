from app import db
from datetime import timedelta

from enum import IntEnum
from flask_appsfoundry.models import TimestampMixin
from sqlalchemy.sql import desc

from app.offers.models import Offer
from app.master.models import Platform
from app.payments.models import Payment
from app.payment_gateways.models import PaymentGateway

from app.items.models import UserSubscription, Item
from app.users.base import Party
from app.users.users import User
from app.utils.models import url_mapped

"""
    LEGEND
        ORDER STATUS
            10000: TEMP
            10001: NEW
            20001: WAITING FOR PAYMENT
            20002: PAYMENT IN PROCESS
            20003: PAYMENT BILLED
            30001: WAITING FOR DELIVERY
            30002: DELIVERY IN PROCRESS
            30003: DELIVERED
            40001: WAITING FOR REFUND
            40002: REFUND IN PROCESS
            40003: REFUNDED
            50000: CANCELLED
            50001: PAYMENT ERROR
            50002: DELIVERY ERROR
            50003: REFUND ERROR
            90000: COMPLETE

        ORDER LINE STATUS
            10000: TEMP
            10001: NEW
            30003: DELIVERED
            40003: REFUNDED
            50000: CANCELLED
            50002: DELIVERY ERROR
            50003: REFUND ERROR
            90000: COMPLETE

"""


@url_mapped('orders.orders', (('order_id', 'id'),))
class Order(db.Model, TimestampMixin):

    __tablename__ = 'core_orders'

    class Status(IntEnum):
        temp = 10000
        new = 10001
        waiting_for_payment = 20001
        payment_in_process = 20002
        payment_billed = 20003
        waiting_for_delivery = 30001
        delivery_in_process = 30002
        delivered = 30003
        waiting_for_refund = 40001
        refund_in_process = 40002
        refunded = 40003
        cancelled = 50000
        payment_error = 50001
        delivery_error = 50002
        refund_error = 50003
        complete = 90000

    id = db.Column(db.Integer, primary_key=True)

    order_number = db.Column(db.BigInteger, nullable=False)
    total_amount = db.Column(db.Numeric(10, 2), nullable=False) #(before order discount)
    final_amount = db.Column(db.Numeric(10, 2), nullable=False) #sum(after all cut discount)

    user_id = db.Column(db.Integer, db.ForeignKey('parties.id'), nullable=False)
    party = db.relationship(Party, backref=db.backref('orders', lazy='dynamic'))

    client_id = db.Column(db.Integer)
    partner_id = db.Column(db.Integer) #request
    order_status = db.Column(db.Integer) #READ ORDER STATUS LEGEND
    is_active = db.Column(db.Boolean(), default=False)
    point_reward = db.Column(db.Integer)
    currency_code = db.Column(db.String(5), nullable=False) #paymentgateway

    paymentgateway_id = db.Column(db.Integer, db.ForeignKey('core_paymentgateways.id'), nullable=False)
    paymentgateway = db.relationship('PaymentGateway')

    tier_code = db.Column(db.String(100))
    platform_id = db.Column(db.Integer)
    temporder_id = db.Column(db.Integer)

    # remote_order_number is a field to store Unique Order ID from partner
    # (ex for remote order checkout: gramedia.com, send via RemoteCheckoutApi)
    remote_order_number = db.Column(db.String(50))
    is_renewal = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return '<user_id : %s total_amount: %s>' % (self.user_id, self.total_amount)

    def get_order_user(self):
        return User.query.filter_by(id=self.user_id).first()

    def save(self, session=None):
        session = session or db.session()
        try:
            session.add(self)
            session.commit()
            return {'invalid': False, 'message': "Order success add"}
        except Exception, e:
            session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Order success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def get_quantity(self):
        orderlines = OrderLine.query.filter_by(order_id=self.id).all()

        total_orderlines = len(orderlines)
        total_quantity = sum([i.quantity for i in orderlines])
        return total_orderlines, total_quantity

    def get_order_line(self):
        return OrderLine.query.filter_by(order_id=self.id).all()

    def get_platform(self):
        data = {}
        plat = Platform.query.filter_by(id=self.platform_id).first()
        if plat:
            data['id'] = plat.id
            data['name'] = plat.name
        else:
            data = None
        return data

    def get_payment_gateway(self):
        data = {}
        plat = PaymentGateway.query.filter_by(id=self.paymentgateway_id).first()
        if plat:
            data['id'] = plat.id
            data['name'] = plat.name
        return data

    def get_data_order_line(self):
        data = []
        order_line = OrderLine.query.filter_by(order_id=self.id).all()

        if order_line:
            for i in order_line:
                #offer = i.get_offer()
                data_order = i.values()

                #data_order['items'] = offer.get_item_for_orders()
                data.append(data_order)

        return data

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def get_payments(self):
        payment_data = Payment.query.filter_by(order_id=self.id).first()
        if payment_data:
            return payment_data.id
        else:
            return 0

    def extension_values(self, extension=None, custom=None):
        data_values = self.values()

        for param in extension:
            if param == 'order_line':
                data_values['order_lines'] = self.get_data_order_line()

            if param == 'platforms':
                data_values['platform'] = self.get_platform()

            if param == 'payment_gateway':
                data_values['payment_gateways'] = self.get_payment_gateway()

            if param == 'ext':
                data_values['order_lines'] = self.get_data_order_line()
                data_values['platform'] = self.get_platform()
                data_values['payment_gateways'] = self.get_payment_gateway()

        if custom:
            original_values = data_values

            kwargs = {}
            fields_required = custom.split(',')
            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
            return kwargs

        return data_values

    def get_temporder(self):
        return OrderTemp.query.filter_by(id=self.temporder_id).first()

    def values(self):
        rv = {}

        rv['order_id'] = self.id
        rv['temporder_id'] = self.temporder_id
        rv['order_number'] = self.order_number
        rv['total_amount'] = '%.2f' % self.total_amount
        rv['final_amount'] = '%.2f' % self.final_amount
        rv['user_id'] = self.user_id
        rv['client_id'] = self.client_id
        rv['partner_id'] = self.partner_id
        rv['order_status'] = self.order_status
        rv['is_active'] = self.is_active
        rv['point_reward'] = self.point_reward
        rv['currency_code'] = self.currency_code
        rv['payment_gateway_id'] = self.paymentgateway_id
        rv['payment_gateway_name'] = self.paymentgateway.name
        rv['tier_code'] = self.tier_code
        rv['platform_id'] = self.platform_id
        rv['payment_id'] = self.get_payments()
        rv['remote_order_number'] = self.remote_order_number

        return rv

    def transactions_orderline(self):
        data = []

        order_line = OrderLine.query.filter_by(order_id=self.id).all()
        if order_line:
            for iorder in order_line:
                tmp = {}
                offer = Offer.query.filter_by(id=iorder.offer_id).first()
                tmp['id'] = iorder.id
                tmp['offer_id'] = offer.id
                tmp['offer_name'] = iorder.name
                tmp['offer_type_id'] = offer.offer_type_id
                tmp['price'] = '%.2f' % iorder.price
                tmp['final_price'] = '%.2f' % iorder.final_price

                if iorder.is_discount and iorder.campaign_id:
                    discount_data = OrderLineDiscount.query.filter_by(
                        orderline_id=iorder.id, order_id=self.id).first()

                    if discount_data.discount_code:
                        tmp['discount_code'] = discount_data.discount_code

                if offer.offer_type_id == 1: #single
                    tmp['item_ids'] = offer.get_items()
                    tmp['offer_quantity'] = 1
                    tmp['offer_quantity_unit'] = 1
                    tmp['on_going'] = 0

                if offer.offer_type_id == 2: #subscriptions
                    subs = UserSubscription.query.filter_by(orderline_id=iorder.id).first()
                    if subs:
                        tmp['offer_quantity_unit'] = subs.quantity_unit
                        tmp['offer_quantity'] = subs.quantity
                        tmp['on_going'] = subs.current_quantity
                        tmp['valid_to'] = subs.valid_to.isoformat()
                        tmp['valid_from'] = subs.valid_from.isoformat()

                        item = Item.query.filter(Item.release_date >= subs.valid_from.date()
                            ).filter_by(brand_id=subs.brand_id).order_by(Item.id).first()

                        # this is can be release_date subs not same as purchase date
                        if not item:
                            item = Item.query.filter_by(brand_id=subs.brand_id
                                ).order_by(desc(Item.id)).first()

                        if item:
                            tmp['item_ids'] = [item.id]
                        else:
                            tmp['item_ids'] = []

                if offer.offer_type_id == 3: #bundle
                    tmp['item_ids'] = offer.get_items()
                    tmp['offer_quantity'] = len(offer.get_items())
                    tmp['offer_quantity_unit'] = 1
                    tmp['on_going'] = 0

                if offer.offer_type_id == 4: #buffets
                    buffets = iorder.get_buffets()
                    if buffets:
                        #tmp['item_ids'] = []
                        #tmp['offer_quantity_unit'] = 1
                        #tmp['offer_quantity'] = 1
                        #tmp['on_going'] = 1
                        tmp['valid_to'] = buffets.valid_to.isoformat()

                data.append(tmp)
        return data

    def transactions_values(self):
        rv = {}

        rv['order_id'] = self.id
        rv['point_reward'] = self.point_reward
        rv['payment_gateway_id'] = self.paymentgateway_id
        rv['payment_gateway_name'] = self.paymentgateway.name
        rv['platform_id'] = self.platform_id
        rv['order_status'] = self.order_status
        rv['user_id'] = self.user_id
        rv['total_amount'] = '%.2f' % self.total_amount
        rv['final_amount'] = '%.2f' % self.final_amount
        rv['order_number'] = self.order_number
        rv['currency_code'] = self.currency_code
        rv['order_lines'] = self.transactions_orderline()
        rv['created'] = (self.created + timedelta(hours=7)).isoformat()

        return rv


class OrderLine(db.Model, TimestampMixin):

    class Status(IntEnum):
        temp = 10000
        new = 10001
        delivered = 30003
        refunded = 40003
        cancelled = 50000
        delivery_error = 50002
        refund_error = 50003
        complete = 90000

    __tablename__ = 'core_orderlines'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(250))
    #paymentgateway_id = db.Column(db.Integer, db.ForeignKey('core_paymentgateways.id'), nullable=False)

    offer = db.relationship('Offer', backref=db.backref('orderlines'))
    offer_id = db.Column(db.Integer, db.ForeignKey('core_offers.id'), nullable=False)

    is_active = db.Column(db.Boolean(), default=False)
    is_free = db.Column(db.Boolean(), default=False)
    is_discount = db.Column(db.Boolean(), default=False)
    is_trial = db.Column(db.Boolean(), default=False)

    user_id = db.Column(db.Integer)
    campaign_id = db.Column(db.Integer)
    order_id = db.Column(db.Integer, db.ForeignKey('core_orders.id'), nullable=False)
    order = db.relationship(Order, backref=db.backref("order_lines", lazy='dynamic'))
        # todo: renamed order_lines to line_items .. look for refs to this
    quantity = db.Column(db.Integer)
    orderline_status = db.Column(db.Integer) #READ ORDER STATUS LEGEND

    # todo: this should probably go on the order
    currency_code = db.Column(db.String(5))
    price = db.Column(db.Numeric(10, 2))
    final_price = db.Column(db.Numeric(10, 2))

    # todo: this should probably go with the order
    localized_currency_code = db.Column(db.String(20))

    # this stays here
    localized_final_price = db.Column(db.Numeric(10, 2))

    def __repr__(self):
        return '<name : %s final_price: %s>' % (self.name, self.final_price)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderLine success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderLine success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def get_offer(self):
        off = None
        offer = Offer.query.filter_by(id=self.offer_id).first()
        if offer:
            off = offer
        return off

    def get_paymentgatewayid(self):
        order = Order.query.filter_by(id=self.order_id).first()
        if order:
            return order.paymentgateway_id
        else:
            return 0

    def get_buffets(self):
        # Circular reference needs to die
        buffet = None
        from app.users.buffets import UserBuffet
        buffet_data = UserBuffet.query.filter_by(orderline_id=self.id).first()
        if buffet_data:
            buffet = buffet_data
        return buffet

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['name'] = self.name
        rv['offer_id'] = self.offer_id
        rv['is_active'] = self.is_active
        rv['is_free'] = self.is_free
        rv['is_discount'] = self.is_discount
        rv['user_id'] = self.user_id
        rv['campaign_id'] = self.campaign_id
        rv['order_id'] = self.order_id

        if self.quantity:
            rv['quantity'] = self.quantity
        else:
            rv['quantity'] = 1

        rv['order_line_status'] = self.orderline_status
        rv['currency_code'] = self.currency_code
        rv['final_price'] = '%.2f' % self.final_price
        rv['price'] = '%.2f' % self.price

        #rv['localized_currency_code'] = self.localized_currency_code
        #rv['localized_final_price'] = '%.2f' % self.localized_final_price

        return rv


class OrderDetail(db.Model, TimestampMixin):

    __tablename__ = 'core_orderdetails'

    id = db.Column(db.Integer, primary_key=True)

    order_id = db.Column(db.Integer, db.ForeignKey("core_orders.id"))
    order = db.relationship("Order", backref=db.backref("order_detail"))

    temporder_id = db.Column(db.Integer)

    user_email = db.Column(db.String(100))
    user_name = db.Column(db.String(250))
    user_street_address = db.Column(db.String(250))
    user_city = db.Column(db.String(32))
    user_zipcode = db.Column(db.String(10))
    user_state = db.Column(db.String(32))
    user_country = db.Column(db.String(32))

    latitude = db.Column(db.String(50))
    longitude = db.Column(db.String(50))
    note = db.Column(db.String(250))
    ip_address = db.Column(db.String(50))
    os_version = db.Column(db.String(50))
    client_version = db.Column(db.String(50))
    device_model = db.Column(db.String(50))

    def __repr__(self):
        return '<user_email : %s order_id: %s>' % (
            self.user_email, self.order_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderDetail success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderDetail success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}


    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['order_id'] = self.order_id
        rv['temporder_id'] = self.temporder_id
        rv['user_email'] = self.user_email
        rv['user_name'] = self.user_name
        rv['user_street_address'] = self.user_street_address
        rv['user_city'] = self.user_city
        rv['user_zipcode'] = self.user_zipcode
        rv['user_state'] = self.user_state
        rv['user_country'] = self.user_country
        rv['latitude'] = self.latitude
        rv['longitude'] = self.longitude
        rv['note'] = self.note
        rv['ip_address'] = self.ip_address
        rv['os_version'] = self.os_version
        rv['client_version'] = self.client_version
        rv['device_model'] = self.device_model

        return rv


class OrderLineDiscount(db.Model, TimestampMixin):

    __tablename__ = 'core_orderlinediscounts'

    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('core_orders.id'), nullable=False)
    order = db.relationship(Order, backref=db.backref("order_line_discounts"))
    orderline_id = db.Column(db.Integer, db.ForeignKey('core_orderlines.id'), nullable=False)
    order_line = db.relationship(OrderLine, backref=db.backref("order_line_discount"))
    discount_id = db.Column(db.Integer, db.ForeignKey('core_discounts.id'), nullable=False)

    discount_name = db.Column(db.String(150))
    currency_code = db.Column(db.String(5))
    discount_code = db.Column(db.String(250))
    discount_type = db.Column(db.Integer)

    discount_value = db.Column(db.Numeric(10, 2))
    raw_price = db.Column(db.Numeric(10, 2))
    final_price = db.Column(db.Numeric(10, 2))

    def __repr__(self):
        return '<order_id : %s discount_id: %s>' % (self.order_id, self.discount_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderDiscount success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderDiscount success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['order_id'] = self.order_id
        rv['orderline_id'] = self.orderline_id
        rv['discount_id'] = self.discount_id
        rv['discount_name'] = self.discount_name
        rv['discount_code'] = self.discount_code
        rv['currency_code'] = self.currency_code
        rv['discount_type'] = self.discount_type
        rv['discount_value'] = self.discount_value
        rv['raw_price'] = self.raw_price
        rv['final_price'] = self.final_price
        return rv


class OrderLogDispatch(db.Model, TimestampMixin):

    """
        THIS TABLE FOR TRACE ERROR WHEN ORDER COMPLETE ONLY
    """

    __tablename__ = 'core_logs_orders'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    order_id = db.Column(db.Integer)
    order_number = db.Column(db.BigInteger)
    error_status = db.Column(db.Text)
    data_order = db.Column(db.Text)

    def __repr__(self):
        return '<user_id : %s order_id: %s>' % (self.user_id, self.order_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderLogDispatch success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderLogDispatch success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['user_id'] = self.user_id
        rv['order_id']= self.order_id
        rv['order_number'] = self.order_number
        rv['error_status'] = self.error_status
        rv['data_order'] = self.data_order
        return rv


class OrderLog(db.Model, TimestampMixin):

    __tablename__ = 'core_logs_orderstransactions'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, nullable=False)
    order_id = db.Column(db.Integer)
    platform_id = db.Column(db.Integer)
    order_temp_id = db.Column(db.Integer)
    request = db.Column(db.Text)
    api = db.Column(db.String(10))

    def __repr__(self):
        return '<user_id : %s order_id: %s>' % (self.user_id, self.order_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderlogTransactions success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderlogTransactions success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['user_id'] = self.user_id
        rv['order_id']= self.order_id
        rv['order_temp_id'] = self.order_temp_id
        rv['platform_id'] = self.platform_id
        rv['request'] = self.request
        rv['api'] = self.api
        return rv


class OrderTemp(db.Model, TimestampMixin):
    __tablename__ = 'core_ordertemps'

    id = db.Column(db.Integer, primary_key=True)

    order_number = db.Column(db.BigInteger)
    user_id = db.Column(db.Integer)
    client_id = db.Column(db.Integer)
    partner_id = db.Column(db.Integer)
    order_status = db.Column(db.Integer) #READ ORDER STATUS LEGEND
    cart_id = db.Column(db.Integer)
    is_active = db.Column(db.Boolean(), default=False)

    paymentgateway_id = db.Column(db.Integer, db.ForeignKey('core_paymentgateways.id'))
    paymentgateway = db.relationship('PaymentGateway')

    tier_code = db.Column(db.String(100))
    platform_id = db.Column(db.Integer)

    price_usd = db.Column(db.Numeric(10, 2), default=0)
    final_price_usd = db.Column(db.Numeric(10, 2), default=0)

    price_idr = db.Column(db.Numeric(10, 2), default=0)
    final_price_idr = db.Column(db.Numeric(10, 2), default=0)

    price_point = db.Column(db.Integer, default=0)
    final_price_point = db.Column(db.Integer, default=0)

    is_renewal = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return '<user_id : %s order_status: %s>' % (self.user_id, self.order_status)

    def __str__(self):
        return "<OrderTemp(id={0.id})>".format(self)

    def get_order_user(self):
        return User.query.filter_by(id=self.user_id).first()

    @property
    def final_price(self):
        """ Determines the OrderTemp's final price, based on it's PaymentGateway's base_currency.

        :return: The amount the user must pay in a given currency.
        :rtype: :class:`decimal.Decimal`
        :raises: :class:`ValueError` if paymentgateway_id attribute has not yet been set.
        :raises: :class:`ValueError` if paymentgateway.base_currency has an unexpected currency code.
        """
        if self.paymentgateway is None:
            raise ValueError("This object {0} must set paymentgateway_id before fetching final_price.".format(self))

        if 'USD' == self.paymentgateway.base_currency.iso4217_code.upper():
            total_amount = self.final_price_usd
        elif 'IDR' == self.paymentgateway.base_currency.iso4217_code.upper():
            total_amount = self.final_price_idr
        elif 'PTS' == self.paymentgateway.base_currency.iso4217_code.upper():
            total_amount = self.final_price_point
        else:
            raise ValueError("{0} uses an unknown currency.".format(self.paymentgateway))

        return total_amount

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderTemp success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderTemp success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def get_orderline_temp(self):
        order_line = None
        ords = OrderLineTemp.query.filter_by(order_id=self.id).all()
        if ords:
            order_line = ords
        return order_line

    def get_platform(self):
        data = {}
        plat = Platform.query.filter_by(id=self.platform).first()
        if plat:
            data['id'] = plat.id
            data['name'] = plat.name
        else:
            data = None
        return data

    def get_payment_gateway(self):
        data = {}
        plat = PaymentGateway.query.filter_by(id=self.paymentgateway_id).first()
        if plat:
            data['id'] = plat.id
            data['name'] = plat.name
        return data

    def get_data_order_lineobject(self):
        order_line = OrderLineTemp.query.filter_by(order_id=self.id).all()
        return order_line

    def get_data_order_line(self):
        data = []
        order_line = OrderLineTemp.query.filter_by(order_id=self.id).all()

        if order_line:
            for i in order_line:
                #offer = i.get_offer()
                data_order = i.values()

                #data_order['items'] = offer.get_item_for_orders()
                data.append(data_order)

        return data

    def sums_data_order_line(self):
        data = []
        order_line = OrderLineTemp.query.filter_by(order_id=self.id).all()

        if order_line:
            raw_usd_price, final_usd_price = [], []
            raw_idr_price, final_idr_price = [], []
            raw_pts_price, final_pts_price = [], []

            for i in order_line:
                raw_usd_price.append(i.price_usd)
                final_usd_price.append(i.final_price_usd)

                raw_idr_price.append(i.price_idr)
                final_idr_price.append(i.final_price_idr)

                raw_pts_price.append(i.price_point)
                final_pts_price.append(i.final_price_point)

            self.price_usd = sum(raw_usd_price)
            self.final_price_usd = sum(final_usd_price)

            self.price_idr = sum(raw_idr_price)
            self.final_price_idr = sum(final_idr_price)

            self.price_point = sum(raw_pts_price)
            self.final_price_point = sum(final_pts_price)

            self.save()

        return 'OK'

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def extension_values(self, extension=None, custom=None):
        data_values = self.values()

        for param in extension:
            if param == 'order_line':
                data_values['order_lines'] = self.get_data_order_line()

            if param == 'platforms':
                data_values['platform'] = self.get_platform()

            if param == 'payment_gateway':
                data_values['payment_gateways'] = self.get_payment_gateway()

            if param == 'ext':
                data_values['order_lines'] = self.get_data_order_line()
                data_values['platform'] = self.get_platform()
                data_values['payment_gateways'] = self.get_payment_gateway()

        if custom:
            original_values = data_values

            kwargs = {}
            fields_required = custom.split(',')
            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
            return kwargs

        return data_values

    def values(self):
        rv = {}

        rv['temp_order_id'] = self.id
        rv['order_number'] = self.order_number
        rv['user_id'] = self.user_id
        rv['client_id'] = self.client_id
        rv['partner_id'] = self.partner_id
        rv['order_status'] = self.order_status
        rv['cart_id'] = self.cart_id
        rv['is_active'] = self.is_active
        rv['payment_gateway_id'] = self.paymentgateway_id

        rv['price_usd'] = '%.2f' % self.price_usd
        rv['price_idr'] = '%.2f' % self.price_idr
        rv['price_point'] = self.price_point

        rv['final_price_usd'] = '%.2f' % self.final_price_usd
        rv['final_price_idr'] = '%.2f' % self.final_price_idr
        rv['final_price_point'] = self.final_price_point

        rv['tier_code'] = self.tier_code
        rv['platform_id'] = self.platform_id

        return rv


class OrderLineTemp(db.Model, TimestampMixin):

    __tablename__ = 'core_orderlinetemps'

    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(250))
    offer_id = db.Column(db.Integer, db.ForeignKey('core_offers.id'))
    offer = db.relationship('Offer')

    is_active = db.Column(db.Boolean(), default=False)
    is_free = db.Column(db.Boolean(), default=False)
    is_discount = db.Column(db.Boolean(), default=False)
    is_trial = db.Column(db.Boolean(), default=False)

    user_id = db.Column(db.Integer)
    campaign_id = db.Column(db.Integer)
    order_id = db.Column(db.Integer)
    quantity = db.Column(db.Integer)
    orderline_status = db.Column(db.Integer)

    price_usd = db.Column(db.Numeric(10, 2), default=0)
    final_price_usd = db.Column(db.Numeric(10, 2), default=0)

    price_idr = db.Column(db.Numeric(10, 2), default=0)
    final_price_idr = db.Column(db.Numeric(10, 2), default=0)

    price_point = db.Column(db.Integer, default=0)
    final_price_point = db.Column(db.Integer, default=0)

    def __repr__(self):
        return '<name : %s quantity: %s>' % (self.name, self.quantity)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderLineTemp success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderLineTemp success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def get_offer(self):
        off = None
        offer = Offer.query.filter_by(id=self.offer_id).first()
        if offer:
            off = offer
        return off

    def get_orderline_discounts(self):
        order_dsc = None
        ords = OrderLineDiscountTemp.query.filter_by(
            orderline_id=self.id, order_id=self.order_id).all()

        if ords:
            order_dsc = ords
        return order_dsc

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['name'] = self.name
        rv['offer_id'] = self.offer_id
        rv['is_active'] = self.is_active
        rv['is_free'] = self.is_free
        rv['is_discount'] = self.is_discount
        rv['user_id'] = self.user_id
        rv['campaign_id'] = self.campaign_id
        rv['order_id'] = self.order_id
        rv['quantity'] = self.quantity
        rv['orderline_status'] = self.orderline_status

        rv['price_usd'] = '%.2f' % self.price_usd
        rv['price_idr'] = '%.2f' % self.price_idr
        rv['price_point'] = self.price_point

        rv['final_price_usd'] = '%.2f' % self.final_price_usd
        rv['final_price_idr'] = '%.2f' % self.final_price_idr
        rv['final_price_point'] = self.final_price_point

        return rv


class OrderLineDiscountTemp(db.Model, TimestampMixin):

    __tablename__ = 'core_orderlinediscounttemp'

    id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer)
    orderline_id = db.Column(db.Integer)
    discount_id = db.Column(db.Integer)

    discount_name = db.Column(db.String(150))
    discount_code = db.Column(db.String(250))
    discount_type = db.Column(db.Integer)

    price_usd = db.Column(db.Numeric(10, 2))
    discount_usd = db.Column(db.Numeric(10, 2))
    final_price_usd = db.Column(db.Numeric(10, 2)) #after discount cut

    price_idr = db.Column(db.Numeric(10, 2))
    discount_idr = db.Column(db.Numeric(10, 2))
    final_price_idr = db.Column(db.Numeric(10, 2)) #after discount cut

    price_point = db.Column(db.Integer)
    discount_point = db.Column(db.Integer)
    final_price_point = db.Column(db.Integer) #after discount cut

    def __repr__(self):
        return '<order_id : %s discount_id: %s>' % (self.order_id, self.discount_id)

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderDiscountTemp success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "OrderDiscountTemp success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}


    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['order_id'] = self.order_id
        rv['orderline_id'] = self.orderline_id
        rv['discount_id'] = self.discount_id
        rv['discount_name'] = self.discount_name
        rv['discount_code'] = self.discount_code
        rv['discount_type'] = self.discount_type

        rv['discount_usd'] = self.discount_usd
        rv['discount_idr'] = self.discount_idr
        rv['discount_point'] = self.discount_point

        rv['price_usd'] = self.price_usd
        rv['final_price_usd'] = self.final_price_usd
        rv['price_idr'] = self.price_idr
        rv['final_price_idr'] = self.final_price_idr
        rv['price_point'] = self.price_point
        rv['final_price_point'] = self.final_price_point

        return rv


class OrderRefund(db.Model, TimestampMixin):

    """
        FINANCE STATUS
            1. OK ( Already processed by finance and refund success )
            2. NOT OK ( still processed by finance )

        REFUND REASON
            1. Duplicate Payment
            2. Fraud
            3. Error Complaint
            4. User Wrong Purchase
            5. Offer Discontinued
            6. Promo Issue
            7. Others

    """

    __tablename__ = 'core_refunds'

    id = db.Column(db.Integer, primary_key=True)

    pic_id = db.Column(db.Integer) #user login from scoopportal
    user_id = db.Column(db.Integer) #scoop id
    origin_payment_id = db.Column(db.Integer)
    origin_order_id = db.Column(db.Integer)
    origin_orderline_id = db.Column(db.Integer)
    refund_amount = db.Column(db.Numeric(10, 2))
    refund_currency_id = db.Column(db.Integer)
    refund_datetime = db.Column(db.DateTime, nullable=False)
    refund_reason = db.Column(db.Integer) # Read Legend : Refund Reason
    ticket_number = db.Column(db.String(50))
    note = db.Column(db.Text)
    finance_status = db.Column(db.Integer) # Read Legend : Finance status
    is_active = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        return '<user_id : %s>' % self.user_id

    def save(self):
        try:
            db.session.add(self)
            db.session.commit()
            return {'invalid': False, 'message': "Order Refund success add"}
        except Exception, e:
            db.session.rollback()
            # db.session.delete(self)
            # db.session.commit()
            return {'invalid': True, 'message': str(e)}

    def delete(self):
        try:
            db.session.delete(self)
            db.session.commit()
            return {'invalid': False, 'message': "Order Refund success deleted"}
        except Exception, e:
            db.session.rollback()
            return {'invalid': True, 'message': str(e)}

    def custom_values(self, custom=None):
        original_values = self.values()

        kwargs = {}
        if custom:
            fields_required = custom.split(',')

            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
        else:
            kwargs = original_values
        return kwargs

    def get_data_payment(self):
        payment_data = []
        payment = Payment.query.filter_by(id=self.origin_payment_id).first()
        if payment:
            payment_data = payment.mirror_values()
        return payment_data

    def get_data_order(self):
        order_data = []
        order = Order.query.filter_by(id=self.origin_order_id).first()
        if order:
            order_data = order.values()
        return order_data

    def get_data_orderline(self):
        order_data = []
        order = OrderLine.query.filter_by(id=self.origin_orderline_id).first()
        if order:
            order_data = order.values()
        return order_data

    def extension_values(self, extension=None, custom=None):
        data_values = self.values()

        for param in extension:

            if param == 'payment':
                data_values['payment'] = self.get_data_payment()

            if param == 'order':
                data_values['order'] = self.get_data_order()

            if param == 'order_line':
                data_values['order_line'] = self.get_data_orderline()

            if param == 'ext':
                data_values['payment'] = self.get_data_payment()
                data_values['order'] = self.get_data_order()
                data_values['order_line'] = self.get_data_orderline()

        if custom:
            original_values = data_values

            kwargs = {}
            fields_required = custom.split(',')
            for item in original_values:
                key_item = item.title().lower()
                if key_item in fields_required:
                    kwargs[key_item] = original_values[key_item]
            return kwargs

        return data_values

    def values(self):
        rv = {}

        rv['id'] = self.id
        rv['pic_id'] = self.pic_id
        rv['user_id'] = self.user_id
        rv['origin_payment_id'] = self.origin_payment_id
        rv['origin_order_id'] = self.origin_order_id
        rv['origin_orderline_id'] = self.origin_orderline_id
        rv['refund_amount'] = '%.2f' % self.refund_amount
        rv['refund_datetime'] = self.refund_datetime.isoformat()
        rv['refund_reason'] = self.refund_reason
        rv['refund_currency_id'] = self.refund_currency_id
        rv['ticket_number'] = self.ticket_number
        rv['note'] = self.note
        rv['finance_status'] = self.finance_status
        rv['is_active'] = self.is_active

        return rv
