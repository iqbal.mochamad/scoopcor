"""
API
===

HTTP Endpoints for this package.
"""
import httplib

from flask import Response, request, views
from flask_appsfoundry import Resource, controllers, marshal
from flask_appsfoundry.parsers import reqparse

from app.constants import RoleType
from app.master.choices import GETSCOOP
from app.orders.choices import COMPLETE
from app.orders.helpers import (
    construct_order_from_remote_request, construct_useritems_from_order, assert_remote_order_not_exists
)

from app.auth.decorators import token_required, banned_permissions
from flask_appsfoundry.exceptions import ScoopApiException
from app.helpers import internal_server_message
from app.users.users import User, Role
from app.utils.geo_info import get_geo_info

from . import _log
from .custom_get import CustomGet, RefundCustomGet, OrderTransactionGet
from .exceptions import PaymentBelowGatewayMinimumError, DuplicateRemoteOrderException
from .helpers import construct_payment_from_order
from .models import Order, OrderRefund, OrderTemp
from .order_checkout_base import OrderCheckoutConstructBase
from .order_checkout_platform import OrderCheckoutConstructPlatform
from .order_confirm import OrderConfirmConstruct
from .order_complete import OrderCompleteConstruct
from .order_refunds import OrderProcess
from .parsers import RemoteCheckoutArgsParser
from .serializers import OrderSerializer


class OrderCheckout(views.MethodView):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required parameters
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('order_lines', type=list, required=True, location='json')

        #optional parameters
        self.reqparser.add_argument('platform_id', type=int, location='json')
        self.reqparser.add_argument('client_id', type=int, location='json')
        self.reqparser.add_argument('partner_id', type=int, location='json')
        self.reqparser.add_argument('payment_gateway_id', type=int, location='json')
        self.reqparser.add_argument('order_info', type=list, location='json')
        self.reqparser.add_argument('order_id', type=int, location='json')
        self.reqparser.add_argument('cc_token_id', type=int, location='json')

        self.reqparser.add_argument('quantity', type=int, location='json')
        self.reqparser.add_argument('name', type=str, location='json')

        # HOTFIXED: Renewal cant cut with discounts
        self.reqparser.add_argument('is_renewal', type=bool, location='json')

        # INFO parameters
        self.reqparser.add_argument('user_email', type=unicode, location='json')
        self.reqparser.add_argument('user_name', type=unicode, location='json')
        self.reqparser.add_argument('user_street_address', type=str, location='json')
        self.reqparser.add_argument('user_city', type=str, location='json')
        self.reqparser.add_argument('user_zipcode', type=str, location='json')
        self.reqparser.add_argument('user_state', type=str, location='json')
        self.reqparser.add_argument('user_country', type=str, location='json')
        self.reqparser.add_argument('latitude', type=str, location='json')
        self.reqparser.add_argument('longitude', type=str, location='json')
        self.reqparser.add_argument('ip_address', type=str, location='json')
        self.reqparser.add_argument('os_version', type=str, location='json')
        self.reqparser.add_argument('client_version', type=str, location='json')
        self.reqparser.add_argument('device_model', type=str, location='json')

        super(OrderCheckout, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    @banned_permissions('can_read_write_public_cafe_partnership, cant_access_protocol')
    def post(self):
        args = self.reqparser.parse_args()
        geo_info = get_geo_info()
        args['country_code'] = geo_info.country_code if geo_info.country_code else None

        try:
            platform_id = args.get('platform_id', 0)

            if platform_id == GETSCOOP:
                scene = OrderCheckoutConstructBase(args, 'POST').construct()
            else:
                scene = OrderCheckoutConstructPlatform(args, 'POST').construct()
            return scene

        except ScoopApiException as e:
            _log.info("Unable to complete the order checkout: {}".format(e))

            if isinstance(e, PaymentBelowGatewayMinimumError):
                # TODO: temp database recording of error messages
                internal_server_message(modul="order checkout 18",
                                        method=request.method,
                                        error=e,
                                        req=str(args))
            raise
        except Exception as e:
            _log.exception("An unexpected error occurred during order checkout.")
            return internal_server_message(modul="order checkout",
                method='POST', error=e, req=str(args))


class OrderConfirm(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required parameters
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('payment_gateway_id', type=int, required=True, location='json')
        self.reqparser.add_argument('currency_code', type=str, required=True, location='json')
        self.reqparser.add_argument('final_amount', type=str, required=True, location='json')
        self.reqparser.add_argument('order_number', type=int, required=True, location='json')
        self.reqparser.add_argument('temp_order_id', type=int, required=True, location='json')
        self.reqparser.add_argument('client_id', type=int, required=True, location='json')

        self.reqparser.add_argument('order_lines', type=list, location='json')

        super(OrderConfirm, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    @banned_permissions('can_read_write_public_cafe_partnership, cant_access_protocol')
    def post(self):
        args = self.reqparser.parse_args()
        geo_info = get_geo_info()
        args['country_code'] = geo_info.country_code if geo_info.country_code else None

        try:
            #construct items
            scene = OrderConfirmConstruct(args, 'POST').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="order confirm",
                method='POST', error=e, req=str(args))


class OrderComplete(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required parameters
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('payment_gateway_id', type=int, required=True, location='json')
        self.reqparser.add_argument('order_id', type=int, required=True, location='json')
        self.reqparser.add_argument('order_number', type=int, required=True, location='json')
        self.reqparser.add_argument('original_transaction_id', type=str, location='json')
        self.reqparser.add_argument('catalog_id', type=int, location='json')
        self.reqparser.add_argument('repurchased', type=bool, location='json')

        super(OrderComplete, self).__init__()

    @token_required('can_read_write_global_all')
    def post(self):
        args = self.reqparser.parse_args()
        try:
            #construct items
            user_id = args.get('user_id', 0)
            payment_gateway_id = args.get('payment_gateway_id', 0)
            order_id = args.get('order_id', 0)
            order_number = args.get('order_number', 0)
            original_transaction_id = args.get('original_transaction_id', None)
            catalog_id = args.get('catalog_id', None)
            repurchased = args.get('repurchased', False)

            scene = OrderCompleteConstruct(user_id=user_id, payment_gateway_id=payment_gateway_id,
                order_id=order_id, order_number=order_number, original_transaction_id=original_transaction_id,
                catalog_id=catalog_id, repurchased=repurchased).construct()

            return scene

        except Exception, e:
            return internal_server_message(modul="order confirm",
                method='POST', error=e, req=str(args))


class OrderApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_global_order')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = CustomGet(param, 'ORDER').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="Order", method="POST",
                error=str(e), req=str(param))


class OrderListApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext, can_read_global_orderline')
    def get(self, order_id=None):
        m = Order.query.filter_by(id=order_id).first()
        if m:
            param = request.args.to_dict()
            param['order_id'] = str(order_id)
            param['expand'] = 'ext'

            scene = CustomGet(param, 'ORDER_DETAILS').construct()
            return scene
        else:
            return Response(status=httplib.NOT_FOUND)


class OrderNumberApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def get(self, order_number=None):
        try:
            order = Order.query.filter_by(order_number=order_number).first()
            if order:
                param = {}
                param['order_id'] = str(order.id)
                param['expand'] = 'ext'
                scene = CustomGet(param, 'ORDER_DETAILS').construct()
                return scene
            else:
                return Response(status=httplib.NOT_FOUND)
        except Exception, e:
            return internal_server_message(modul="OrderNumb", method="POST",
                error=str(e), req=str(param))


class OrderTempApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = CustomGet(param, 'ORDER_TEMP').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="Ordertemp", method="POST",
                error=str(e), req=str(param))


class OrderListTempApi(Resource):

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def get(self, order_id=None):
        m = OrderTemp.query.filter_by(id=order_id).first()
        if m:
            param = request.args.to_dict()
            param['order_id'] = str(order_id)

            scene = CustomGet(param, 'ORDER_TEMP').construct()
            return scene
        else:
            return Response(status=httplib.NOT_FOUND)


class OrderRefundListApi(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('pic_id', type=int, required=True, location='json')
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('origin_payment_id', type=int, required=True, location='json')
        self.reqparser.add_argument('origin_order_id', type=int, required=True, location='json')
        self.reqparser.add_argument('origin_order_line_id', type=int, required=True, location='json')
        self.reqparser.add_argument('refund_amount', type=str, required=True, location='json')
        self.reqparser.add_argument('refund_currency_id', type=int, required=True, location='json')
        self.reqparser.add_argument('refund_datetime', type=str, required=True, location='json')
        self.reqparser.add_argument('refund_reason', type=int, required=True, location='json')
        self.reqparser.add_argument('finance_status', type=int, required=True, location='json')

        #optional parameters
        self.reqparser.add_argument('ticket_number', type=str, location='json')
        self.reqparser.add_argument('note', type=str, location='json')
        self.reqparser.add_argument('is_active', type=bool, location='json')

        super(OrderRefundListApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_refund')
    def get(self):
        try:
            param = request.args.to_dict()
            scene = RefundCustomGet(param, 'REFUND').construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="order refund", method="POST",
                error=str(e), req=str(param))

    @token_required('can_read_write_global_all, can_read_write_refund')
    def post(self):
        try:
            args = self.reqparser.parse_args()
            args['origin_orderline_id'] = args['origin_order_line_id']
            del args['origin_order_line_id']

            scene = OrderProcess(args=args, method="POST").construct()
            return scene
        except Exception, e:
            return internal_server_message(modul="order refund", method="POST",
                error=str(e), req=str(args))


class OrderRefundApi(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        #required for database whos can't null
        self.reqparser.add_argument('pic_id', type=int, required=True, location='json')
        self.reqparser.add_argument('user_id', type=int, required=True, location='json')
        self.reqparser.add_argument('origin_payment_id', type=int, required=True, location='json')
        self.reqparser.add_argument('origin_order_id', type=int, required=True, location='json')
        self.reqparser.add_argument('origin_order_line_id', type=int, required=True, location='json')
        self.reqparser.add_argument('refund_amount', type=str, required=True, location='json')
        self.reqparser.add_argument('refund_currency_id', type=int, required=True, location='json')
        self.reqparser.add_argument('refund_datetime', type=str, required=True, location='json')
        self.reqparser.add_argument('refund_reason', type=int, required=True, location='json')
        self.reqparser.add_argument('finance_status', type=int, required=True, location='json')

        #optional parameters
        self.reqparser.add_argument('ticket_number', type=str, location='json')
        self.reqparser.add_argument('note', type=str, location='json')
        self.reqparser.add_argument('is_active', type=bool, location='json')

        super(OrderRefundApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_refund')
    def get(self, refund_id=None):
        m = OrderRefund.query.filter_by(id=refund_id).first()
        if m:
            param = request.args.to_dict()
            param['refund_id'] = str(refund_id)
            param['expand'] = 'ext'

            scene = RefundCustomGet(param, 'REFUND_DETAILS').construct()
            return scene
        else:
            return Response(status=httplib.NOT_FOUND)

    @token_required('can_read_write_global_all, can_read_write_refund')
    def put(self, refund_id=None):
        args = self.reqparser.parse_args()

        args['origin_orderline_id'] = args['origin_order_line_id']
        del args['origin_order_line_id']

        m = OrderRefund.query.filter_by(id=refund_id).first()
        if not m:
            return Response(status=httplib.NOT_FOUND)
        else:
            scene = OrderProcess(args, 'PUT', m).construct()
            return scene

    @token_required('can_read_write_global_all')
    def delete(self, refund_id=None):
        f = OrderRefund.query.filter_by(id=refund_id).first()
        if f:
            f.is_active = False
            f.save()
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            return Response(status=httplib.NOT_FOUND)


class OrderTransactionApi(Resource):

    def __init__(self):
        self.reqparser = reqparse.RequestParser()

        super(OrderTransactionApi, self).__init__()

    @token_required('can_read_write_global_all, can_read_write_public_ext')
    def get(self, user_id=None):
        m = Order.query.filter_by(user_id=user_id).all()
        if not m:
            return Response(status=httplib.NO_CONTENT)

        else:
            param = request.args.to_dict()
            scene = OrderTransactionGet(param, user_id).construct()
            return scene


class RemoteCheckoutApi(controllers.ErrorHandlingApiMixin, Resource):
    """ API endpoint for recording purchases processed on a 3rd party system.

    .. note::

        This is currently only functional for 'elevenia', however it's meant
        to be kept in a generic state so it's expendable to other services
        in the future.
    """
    def post(self, remote_service_name):
        """ Records a purchase that occurred in a 3rd party system.

        The user's auth token is validated against the provider (we're assuming
        one valid token per remote_service_name), unless the system is in
        DEBUG (where any token is acceptable).

        .. warning::

            We have no way of validating the purchase pricing sent to us,
            because it happens all on a remote system, so we accept any data
            that is provided for pricing.

        :return: A tuple containing the response data (to be serialized to
            JSON) as the first element, and the http status code as the second.
        :rtype: tuple(OrderedDictionary, int)
        """
        return self.post_remote_checkout(remote_service_name)

    def post_remote_checkout(self, remote_service_name):
        from datetime import datetime
        from hashlib import sha1
        from app import app, db

        parser = RemoteCheckoutArgsParser()
        args = parser.parse_args(req=request)
        session = Order.query.session

        remote_order_log = remote_service_name
        verified_role = session.query(Role).get(RoleType.verified_user.value)

        try:
            remote_order_number = args.get('remote_order_number', None)
            if remote_order_number:
                assert_remote_order_not_exists(remote_order_number, session)
                remote_order_log = '{remote_service_name} remote order no: {remote_order_number}'.format(
                    remote_service_name=remote_service_name,
                    remote_order_number=remote_order_number)

            customer_user_id = args.get('user_id', None)
            customer_user_email = args.get('receiver_email', '').lower().strip()
            cust_by_id = session.query(User).get(customer_user_id)

            user = cust_by_id if cust_by_id else None
            if not cust_by_id:
                cust_by_email = session.query(User).filter(User.email==customer_user_email).first()
                user = cust_by_email if cust_by_email else None

            # create new user
            if not user:
                user = User(
                    username=customer_user_email,
                    email=customer_user_email,
                    last_login=datetime.utcnow(),
                    is_verified=True,
                    excluded_from_reporting=False,
                    created=datetime.utcnow()
                )
                password_sha = sha1(app.config['PASSWORD_HASH'] + 'unknownpassword').hexdigest()
                user.set_password(password_sha)
                user.roles.append(verified_role)
                db.session.add(user)
                db.session.commit()

            order = db.session.query(Order).filter(Order.remote_order_number == remote_order_number).first()
            if order:
                _log.exception(
                    "Failed {remote_service_name} checkout for {remote_order_log}. Error message: {err_msg}".format(
                        remote_service_name=remote_service_name, remote_order_log=remote_order_log,
                        err_msg="already exists"))
                session.rollback()
                raise DuplicateRemoteOrderException(remote_order_number)
            else:
                order = construct_order_from_remote_request(args, session, user)
                session.add(order)

                if order.order_status == COMPLETE:
                    # generate a payment for our order
                    payment = construct_payment_from_order(order)
                    session.add(payment)

                    # add all the items on the order the user's items.
                    for user_item in construct_useritems_from_order(order, remote_service_name):
                        session.add(user_item)

                session.commit()
                session.flush()

                _log.info("Remote checkout {status}. "
                          "New Order(id={order_id}) created for {remote_order_log}.".format(
                            status='success' if order.order_status == COMPLETE else 'failed',
                            order_id=order.id,
                            remote_order_log=remote_order_log))

            return marshal(order, OrderSerializer()), httplib.CREATED

        except Exception as e:
            _log.exception("Failed {remote_service_name} checkout for {remote_order_log}. Error message: {err_msg}".format(
                remote_service_name=remote_service_name, remote_order_log=remote_order_log, err_msg=e.message))
            session.rollback()
            raise

    def _handle_UserNotFoundException(self, exception):
        return {
            "status": httplib.BAD_REQUEST,
            "error_code": 400102,
            "developer_message": "'receiver_email' does not exist.  "
                                 "Account creation not allowed.",
            "user_message":
                "Sorry, we don't have a user with the user id/e-mail address:"
                " {}.  Please sign up for an account with Scoop.".format(
                    exception),
        }, 400, {'Content-Type': 'application/json'}

    def _handle_400(self, exception):

        data = getattr(exception, 'data', {})

        response = {
            "status": httplib.BAD_REQUEST,
            "error_code": 400101,
            "user_message": data.get('user_message', str(exception)),
            "developer_message": data.get('developer_message') or getattr(exception, 'description') or "No developer message"
        }
        return response, 400

    def _handle_422(self, exception):
        data = getattr(exception, 'data', {})

        response = {
            "status": 422,
            "error_code": 422101,
            "user_message": data.get('user_message', str(exception)),
            "developer_message": data.get('developer_message') or getattr(exception, 'description') or "No developer message"
        }
        return response, 422
