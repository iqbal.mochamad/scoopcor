import json, pickle
from datetime import timedelta

from babel import numbers
from dateutil.relativedelta import relativedelta
from flask import current_app, g
from sqlalchemy import desc

from app.items.models import UserSubscription, Item, UserItem
from app.items.previews import preview_s3_covers
from app.messaging.email import HtmlEmail
from app.messaging.email import get_smtp_server
from app.offers.models import OfferSubscription, Offer
from app.payments.choices.gateways import WALLET
from app.users.buffets import UserBuffet
from app.utils.shims import item_types
from app import app

from . import _log
from ..users.users import User


def deliver_user_buffet(user_id, order_line_id, offer,
                        expires_date, is_trial, original_transaction_id, session):
    buffet = offer.buffet[0]
    if buffet:
        new_data = UserBuffet(
            user_id=user_id,
            orderline_id=order_line_id,
            offerbuffet_id=buffet.id,
            offerbuffet=buffet,
            valid_to=expires_date,
            is_restore=False,
            is_trial=is_trial,
            original_transaction_id=original_transaction_id
        )
        session.add(new_data)
        return new_data


def deliver_user_subscription(order, order_line, offer, purchase_date, session):
    base_purchase_date = purchase_date.replace(hour=0, minute=0, second=0, microsecond=0)
    offer_subscription = offer.subscription.first()
    offer_brands = offer.brands.all()

    items = []
    for brand in offer_brands:
        brand_id = brand.id
        # get current item and backward items for this subscription
        items = get_items_from_subscription(
            session, offer_subscription, purchase_date, brand_id, items)

        if offer_subscription.quantity_unit == OfferSubscription.QuantityUnit.unit.value:
            new_release_items = get_new_release_items(items, base_purchase_date) if items else []
            current_quantity = offer_subscription.quantity - len(new_release_items)
            current_quantity = current_quantity if current_quantity >= 0 else 0
            valid_to = purchase_date
        else:
            current_quantity = offer_subscription.quantity
            valid_to = get_subs_end_date(purchase_date, current_quantity, offer_subscription.quantity_unit)

        # add user subscription data
        user_subs = UserSubscription(
            user_id=order.user_id,
            quantity=offer_subscription.quantity,
            current_quantity=current_quantity,
            quantity_unit=offer_subscription.quantity_unit,
            valid_from=purchase_date,
            valid_to=valid_to,
            brand_id=brand_id,
            subscription_code=offer.offer_code,
            orderline_id=order_line.id,
            allow_backward=offer_subscription.allow_backward,
            backward_quantity=offer_subscription.backward_quantity,
            backward_quantity_unit=offer_subscription.backward_quantity_unit,
            is_active=True
        )
        session.add(user_subs)
        session.commit()

        latest_item = get_latest_item(items) if items else None
        # deliver user items (that already released) from this subscriptions
        if items:
            deliver_owned_items(items, order.user_id, session, order_line=order_line, is_restore=False)

        # deliver extra items data based on current latest item
        if latest_item and latest_item.extra_items:
            extra_items = []
            for item_id in latest_item.extra_items:
                item = session.query(Item).get(item_id)
                if item:
                    extra_items.append(item)
            deliver_owned_items(extra_items, order.user_id, session, order_line=order_line, is_restore=False)


def get_new_release_items(list_items, base_purchase_date):
    new_items = []
    for item in list_items:
        if item.release_date >= base_purchase_date:
            new_items.append(item)
    return new_items


def get_latest_item(list_items):
    latest_item = list_items[0]
    for item in list_items:
        if item.release_date > latest_item.release_date:
            latest_item = item
    return latest_item


def deliver_owned_items(items, user_id, session, order_line=None, is_restore=True):

    for item in items:
        user_item = session.query(UserItem).filter_by(
            item_id=item.id,
            user_id=user_id,
            user_buffet_id=None
        ).first()
        if not user_item:
            user_item = UserItem(
                user_id=user_id,
                item_id=item.id,
                edition_code=item.edition_code,
                is_active=True,
                orderline_id=order_line.id if order_line else None,
                is_restore=is_restore,
                brand_id=item.brand_id,
                itemtype_id=item_types.enum_to_id(item.item_type),
                vendor_id=item.brand.vendor_id
            )
            session.add(user_item)
            session.commit()


def get_items_from_subscription(session, offer_subscription, base_purchase_date, brand_id, items):
    base_purchase_date = base_purchase_date.replace(hour=0, minute=0, second=0, microsecond=0)
    if offer_subscription.quantity_unit == OfferSubscription.QuantityUnit.unit.value:
        # Subscription by unit/edition
        items = session.query(Item).filter(
            Item.brand_id == brand_id,
            Item.release_date >= base_purchase_date,
            Item.item_status == Item.Statuses.ready_for_consume.value
        ).order_by(Item.release_date).limit(offer_subscription.quantity).all()

        if offer_subscription.allow_backward and offer_subscription.backward_quantity > 0:
            backward_items = session.query(Item).filter(
                Item.brand_id == brand_id,
                Item.release_date < base_purchase_date,
                Item.item_status == Item.Statuses.ready_for_consume.value
            ).order_by(
                desc(Item.release_date)
            ).limit(offer_subscription.backward_quantity + 1).all()

            items = items + backward_items

    elif offer_subscription.quantity_unit in [
            OfferSubscription.QuantityUnit.day.value,
            OfferSubscription.QuantityUnit.week.value,
            OfferSubscription.QuantityUnit.month.value]:

        end_date = get_subs_end_date(
            base_purchase_date, offer_subscription.quantity, offer_subscription.quantity_unit)

        items = session.query(Item).filter(
            Item.brand_id == brand_id,
            Item.release_date >= base_purchase_date,
            Item.release_date <= end_date,
            Item.item_status == Item.Statuses.ready_for_consume.value
        ).all()

        if offer_subscription.allow_backward and offer_subscription.backward_quantity > 0:

            start_date = get_backward_start_date(
                base_purchase_date, offer_subscription.quantity, offer_subscription.quantity_unit)

            backward_items = session.query(Item).filter(
                Item.brand_id == brand_id,
                Item.release_date >= start_date,
                Item.release_date < base_purchase_date,
                Item.item_status == Item.Statuses.ready_for_consume.value
            ).order_by(desc(Item.release_date)).all()

            items = items + backward_items

    items = list(set(items))
    return items


def get_subs_end_date(base_purchase_date, quantity, quantity_unit):
    # Subscriptons by Days (2) / Weeks (3) / Months (4)
    if quantity_unit == OfferSubscription.QuantityUnit.day.value:
        end_date = base_purchase_date + timedelta(days=quantity)
    elif quantity_unit == OfferSubscription.QuantityUnit.week.value:
        end_date = base_purchase_date + timedelta(weeks=quantity)
    elif quantity_unit == OfferSubscription.QuantityUnit.month.value:
        end_date = base_purchase_date + relativedelta(months=quantity)
    return  end_date


def get_backward_start_date(base_purchase_date, quantity, quantity_unit):
    if quantity_unit == OfferSubscription.QuantityUnit.day.value:
        start_date = base_purchase_date - timedelta(days=quantity)
    elif quantity_unit == OfferSubscription.QuantityUnit.week.value:
        start_date = base_purchase_date - timedelta(weeks=quantity)
    elif quantity_unit == OfferSubscription.QuantityUnit.month.value:
        start_date = base_purchase_date - relativedelta(months=quantity)
    return start_date


def get_eperpus_useremail(organization_id=None, customer_email=None):
    from app.users.organizations import OrganizationUser
    org_users = OrganizationUser.query.filter_by(organization_id=organization_id, is_manager=True).all()
    return [o.user.email for o in org_users if customer_email != o.user.email and o.user.is_active == True] \
        if org_users else []

def delivery_async_email():
    try:
        server = get_smtp_server()

        # retrieve all email keys
        data_redis = app.kvs.keys('eperpus:*')

        for data_content in data_redis:
            data = app.kvs.get(data_content)

            # restruct data string to objects by pickle
            object_data = pickle.loads(data)
            content, email_to = object_data.get('content', None), object_data.get('to', [])

            for email in email_to:
                content.to = [email, ]
                server.send_message(content)

            # clear up
            app.kvs.delete(data_content)

    except Exception as e:
        print e


def send_receipt_mail(order=None, customer_email=None, catalog_id=None):
    """

    :param `six.text_type` platform:
    :param `app.orders.models.Order` order:
    :param `six.text_type` customer_email: The customer's e-mail address.  This is an unfortunately-named parameter ;_;
    :return:
    """
    try:
        if order.platform_id == 1:
            platform = "Gramedia Digital (iOS)"
        elif order.platform_id == 2:
            platform = "Gramedia Digital (Android)"
        elif order.platform_id == 4:
            platform = "Gramedia Digital (ebooks.gramedia.com)"
        else:
            platform = "Gramedia Digital (ebooks.gramedia.com)"

        if not customer_email:
            # order for an organization
            if order.paymentgateway_id == WALLET:
                customer_email = g.current_user.email
                bcc = get_eperpus_useremail(order.user_id, customer_email)

                email_template = 'email/eperpus/complete.html'
                email_content_template = 'email/eperpus/complete.txt'
                subject = 'Pembelian Berhasil - {}'.format(order.order_number)
            else:
                return False
        else:
            # regular order from a user
            email_template = 'email/orders/complete.html'
            email_content_template = 'email/orders/complete.txt'
            subject = 'Your receipt from Gramedia Digital'
            bcc = []

        server = get_smtp_server()

        reply_email = 'Gramedia Digital <no-reply@gramedia.com>'
        if order.paymentgateway_id == WALLET:
            reply_email = 'ePerpus <noreply@eperpus.com>'

        msg = HtmlEmail(
            reply_email,
            [customer_email, ],
            subject,
            email_template,
            email_content_template,
            context=OrderCompleteEmailViewmodel(platform=platform, order=order,
                customer_email=customer_email)
        )

        if not catalog_id:
            # send normally for Gramedia Digital
            server.send_message(msg)
            for email in bcc:
                msg.to = [email, ]
                server.send_message(msg)
        else:
            data = {}
            data['to'] = msg.to + bcc
            data['content'] = msg
            app.kvs.set('eperpus:{}'.format(order.order_number), pickle.dumps(data))

        return True

    except Exception as e:
        _log.exception(
            "Failed to send payment notification."
            "order id {order_id}, order number {order_number}, to {customer}. "
            "Error: {error_message}".format(
                order_id=order.id, order_number=order.order_number,
                error_message=e.message, customer=customer_email))
        return False


class OrderCompleteEmailViewmodel(object):
    def __init__(self, platform=None, order=None, customer_email=None, quantity=None, renewal_eperpus=None, reason_fail_text=None):
        self.platform = platform
        self.order = order
        self.customer_email = customer_email
        self.renewal_eperpus = renewal_eperpus
        self.reason_fail_text = reason_fail_text

    @property
    def customer_main_email(self):
        return self.customer_email

    @property
    def order_lines_total(self):
        return self.order.get_quantity()[0]

    @property
    def order_lines_quantity(self):
        return self.order.get_quantity()[1]

    @property
    def order_lines(self):
        return self.order.order_lines.all()

    @property
    def payment(self):
        return self.order.paymentgateway.name

    @property
    def discount(self):
        return self.order.total_amount - self.order.final_amount

    @property
    def transaction_date(self):
        return self.order.created.strftime('%d %b %Y')

    @property
    def eperpus_transaction_date(self):
        return (self.order.created + timedelta(hours=7)).strftime('%d %b %Y | %H:%M WIB')

    def get_thumbnail_url(self, order_line):
        if order_line.offer.offer_type.id in [Offer.Type.bundle, Offer.Type.buffet]:
            return "{}{}".format(current_app.config.get("OFFERS_BASE_URL"),
                                 order_line.offer.image_normal)

        elif order_line.offer.offer_type.id == Offer.Type.subscription:
            item = order_line.offer.brands.first().items.order_by(desc(Item.release_date)).first()
            return preview_s3_covers(item, 'thumb_image_normal', True)
        else:
            return preview_s3_covers(order_line.offer.items.first(), 'thumb_image_normal', True)

    @property
    def expired_renewal_date(self):
        return self.renewal_eperpus.valid_to.strftime('%d %b %Y')

    @property
    def renewal_transaction_date(self):
        return (self.renewal_eperpus.order.created + timedelta(hours=7)).strftime('%d %b %Y | %H:%M WIB')

    @property
    def fail_renewal_note(self):
        return self.reason_fail_text

    @property
    def display_offer_name(self):
        display_offer = '{} {}'.format(self.renewal_eperpus.brand.name, self.renewal_eperpus.offer.get_display_offer_name(loc='id'))
        return display_offer

    @staticmethod
    def format_currency(currency_code=None, amount=None):

        if currency_code:
            if currency_code == 'IDR':
                # expect: 'Rp 112.312.321'
                return numbers.format_decimal(amount, format='Rp #,##0;-#', locale='id')
            elif currency_code == 'PTS':
                # expect: '1.234.567 PTS'
                return numbers.format_decimal(amount, format='#,##0 Pts;-#', locale='id')
            elif currency_code == 'USD':
                # expect: 'US$ 1,234,567.34'
                return numbers.format_decimal(amount, format='US$ #,##0.##;-#', locale='en')
            else:
                # expect: $1,500.00
                return numbers.format_currency(amount, currency_code, locale='en')
        else:
            return numbers.format_decimal(amount, format='#,##0;-#', locale='id')


def send_auto_renewal_email(exist_org_brand=None, is_renewal=None):
    # Avoid for looping import -_-"
    from app.eperpus.helpers import eperpus_organization_admins

    try:
        pic_email = g.current_user.email
        data_admin = eperpus_organization_admins(org_id=exist_org_brand.organization_id)

        # send email to pic + all admin related to organizations
        admin_emails = list(set([data.email for data in data_admin] + [pic_email]))

        for email in admin_emails:
            email_template = 'email/eperpus/enable_renewal.html' if is_renewal else 'email/eperpus/disable_renewal.html'
            email_content_template = 'email/eperpus/enable_renewal.txt' if is_renewal else 'email/eperpus/disable_renewal.txt'
            subject = 'Perpanjangan Otomatis Berhasil!' if is_renewal else 'Berhenti Perpanjangan Otomatis Berhasil!'

            server = get_smtp_server()
            reply_email = 'ePerpus <noreply@eperpus.com>'
            msg = HtmlEmail(reply_email, [email, ], subject, email_template, email_content_template,
                            context=OrderCompleteEmailViewmodel(customer_email=pic_email, renewal_eperpus=exist_org_brand))
            server.send_message(msg)

        return True

    except Exception as e:
        _log.exception(
            "Failed to send auto renewal notification."
            "order id {order_id}, to {customer}. "
            "Error: {error_message}".format(order_id=exist_org_brand.order_id, error_message=e.message,
                                            customer=pic_email))
        return False


def send_auto_renewal_email_transaction(exist_org=None, status_renewal=None, reason_fail=None, user_pic=None):
    # Avoid for looping import -_-"
    from app.eperpus.helpers import eperpus_organization_admins

    try:
        user_pic = User.query.filter_by(id=user_pic).first()
        data_admin = eperpus_organization_admins(org_id=exist_org.organization_id)

        pic_email = user_pic.email
        # send email to pic + all admin related to organizations
        admin_emails = list(set([data.email for data in data_admin] + [pic_email]))

        for email in admin_emails:
            email_template = 'email/eperpus/success_renewal.html' if status_renewal else 'email/eperpus/fail_renewal.html'
            email_content_template = 'email/eperpus/success_renewal.txt' if status_renewal else 'email/eperpus/fail_renewal.txt'
            subject = 'Perpanjangan Otomatis Langganan Anda Berhasil!' if status_renewal else 'Perpanjangan Otomatis Langganan Gagal'

            server = get_smtp_server()
            reply_email = 'ePerpus <noreply@eperpus.com>'
            msg = HtmlEmail(reply_email, [email, ], subject, email_template, email_content_template,
                            context=OrderCompleteEmailViewmodel(
                                customer_email=pic_email, renewal_eperpus=exist_org, reason_fail_text=reason_fail))
            server.send_message(msg)
        return True

    except Exception as e:
        _log.exception(
            "Failed to send auto renewal notification."
            "order id {order_id}, to {customer}. "
            "Error: {error_message}".format(order_id=exist_org.order_id, error_message=e.message, customer=pic_email))
        return False
