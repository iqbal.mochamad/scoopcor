from decimal import Decimal
from flask_appsfoundry import parsers
from flask_appsfoundry.parsers import InputField, StringField, IntegerField

from app.remote_publishing.choices import REMOTE_DATA_LOCATIONS


def line_item_field(input):
    """ Nested parser for remote checkout line items data.

    :param `dict` input: Line item representations for each item purchased
        on a given order.

        id
            the scoopcor item id.
        sub_total
            the subtotal amount for this line item.
        grand_total
            the total amount paid for this item.
    :return: The same input dictionary that was provided.
    :rtype: `dict`
    """
    return {
        'id': input['id'],
        'discount_id': input.get('discount_id', None),
        'sub_total': input['sub_total'],
        'grand_total': input['grand_total'],
    }


class RemoteCheckoutArgsParser(parsers.ParserBase):
    sub_total = InputField(type=Decimal, required=True)
    grand_total = InputField(type=Decimal, required=True)

    # remote checkout system can send email or Scoop User Id
    sender_email = StringField(required=False)
    receiver_email = StringField(required=False)
    user_id = IntegerField(required=False)

    remote_service_name = StringField(choices=REMOTE_DATA_LOCATIONS,
                                      default='elevenia')
    currency_code = StringField(required=True,
                                choices=[u'idr', u'usd'],
                                type=lambda input: input.lower())
    items = InputField(type=line_item_field, required=True, action='append')
    user_message = StringField()
    remote_order_number = StringField(required=False)
