import json
import httplib

from flask_restful import Resource, Api, reqparse
from flask_restful import Api, reqparse
from flask import abort, Response, jsonify

from app import app, db

from sqlalchemy.sql import and_, or_, desc

from app.orders.models import Order, OrderLine, OrderLineDiscount, OrderRefund
from app.orders.models import OrderTemp, OrderLineTemp, OrderLineDiscountTemp


class CustomGet():

    def __init__(self, param=None, module=None):

        self.limit = param.get('limit', 20)
        self.offset = param.get('offset', 0)
        self.fields = param.get('fields', None)
        self.q = param.get('q', None)

        self.user_id = param.get('user_id', None)
        self.order_id = param.get('order_id', None)
        self.order_number = param.get('order_number', None)
        self.remote_order_number = param.get('remote_order_number', None)
        self.payment_gateway = param.get('payment_gateways', None)
        self.order_status = param.get('order_status', None)
        self.currency = param.get('currency_code', None)
        self.platform = param.get('platforms', None)
        self.extend = param.get('expand', None)

        self.module = module
        self.master = ''
        self.total = 0

    def json_success(self, data_items=None, offset=None, limit=None):
        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:

            if self.fields and self.extend:
                extends = self.extend.split(',')
                cur_values = [u.extension_values(extends, self.fields) for u in data_items]

            elif self.extend:
                extends = self.extend.split(',')
                cur_values = [u.extension_values(extends) for u in data_items]

            elif self.fields:
                cur_values = [u.custom_values(self.fields) for u in data_items]
            else:
                cur_values = [u.values() for u in data_items]

            temp={}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}

            if self.module in ['ORDER_DETAILS']:
                rv = json.dumps(cur_values[0])
            else:
                rv = json.dumps({'orders': cur_values, "metadata": tempx})

            return Response(rv, status=httplib.OK, mimetype='application/json')

    def order_master(self):
        self.total = Order.query.count()
        return Order.query

    def ordertemp_master(self):
        self.total = OrderTemp.query.count()
        return OrderTemp.query

    def construct(self):
        if self.module in ['ORDER', 'ORDER_DETAILS']:
            self.master = self.order_master()

        if self.module == 'ORDER_TEMP':
            self.master = self.ordertemp_master()

        if self.user_id:
            user_id = self.user_id.split(',')
            if self.module in ['ORDER', 'ORDER_DETAILS']:
                self.master = self.master.filter(Order.user_id.in_(user_id))
            if self.module == 'ORDER_TEMP':
                self.master = self.master.filter(OrderTemp.user_id.in_(user_id))

        if self.order_id:
            order_id = self.order_id.split(',')
            if self.module in ['ORDER', 'ORDER_DETAILS']:
                self.master = self.master.filter(Order.id.in_(order_id))
            if self.module == 'ORDER_TEMP':
                self.master = self.master.filter(OrderTemp.id.in_(order_id))

        if self.order_number:
            order_num = self.order_number.split(',')

            if self.module in ['ORDER', 'ORDER_DETAILS']:
                self.master = self.master.filter(Order.order_number.in_(order_num))
            if self.module == 'ORDER_TEMP':
                self.master = self.master.filter(OrderTemp.order_number.in_(order_num))
            # self.master = self.master.filter_by(order_number=self.order_number)

        if self.remote_order_number:
            order_num = self.remote_order_number.split(',')

            if self.module in ['ORDER', 'ORDER_DETAILS']:
                self.master = self.master.filter(Order.remote_order_number.in_(order_num))

        if self.payment_gateway:
            payment_id = self.payment_gateway.split(',')
            if self.module in ['ORDER', 'ORDER_DETAILS']:
                self.master = self.master.filter(Order.paymentgateway_id.in_(payment_id))
            if self.module == 'ORDER_TEMP':
                self.master = self.master.filter(OrderTemp.paymentgateway_id.in_(payment_id))

        if self.order_status:
            self.master = self.master.filter_by(order_status=self.order_status)

        if self.currency:
            if self.module in ['ORDER', 'ORDER_DETAILS']:
                self.master = self.master.filter_by(currency_code=self.currency)

        if self.platform:
            platform_id = self.platform.split(',')
            if self.module in ['ORDER', 'ORDER_DETAILS']:
                self.master = self.master.filter(Order.platform_id.in_(platform_id))
            if self.module == 'ORDER_TEMP':
                self.master = self.master.filter(OrderTemp.platform_id.in_(platform_id))

        self.total = self.master.count()
        self.master = self.master.limit(self.limit).offset(self.offset).all()
        return self.json_success(self.master, self.offset, self.limit)


class RefundCustomGet():

    def __init__(self, param=None, module=None):

        self.limit = param.get('limit', 20)
        self.offset = param.get('offset', 0)
        self.fields = param.get('fields', None)

        self.refund_id = param.get('refund_id', None)
        self.pic_id = param.get('pic_id', None)
        self.user_id = param.get('user_id', None)
        self.origin_payment_id = param.get('origin_payment_id', None)
        self.origin_order_id = param.get('origin_order_id', None)
        self.origin_orderline_id = param.get('origin_order_line_id', None)
        self.refund_amount = param.get('refund_amount', None)
        self.refund_currency_id = param.get('refund_currency_id', None)
        self.finance_status = param.get('finance_status', None)
        self.is_active = param.get('is_active', None)
        self.extend = param.get('expand', None)
        self.order = param.get('order', None)

        self.refund_datetime_lt = param.get('refund_datetime_lt', None)
        self.refund_datetime_lte = param.get('refund_datetime_lte', None)
        self.refund_datetime_gt = param.get('refund_datetime_gt', None)
        self.refund_datetime_gte = param.get('refund_datetime_gte', None)

        self.module = module
        self.master = ''
        self.total = 0

    def json_success(self, data_items=None, offset=None, limit=None):
        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:

            if self.fields and self.extend:
                extends = self.extend.split(',')
                cur_values = [u.extension_values(extends, self.fields) for u in data_items]

            elif self.extend:
                extends = self.extend.split(',')
                cur_values = [u.extension_values(extends) for u in data_items]

            elif self.fields:
                cur_values = [u.custom_values(self.fields) for u in data_items]
            else:
                cur_values = [u.values() for u in data_items]

            temp={}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}

            if self.module in ['REFUND_DETAILS']:
                rv = json.dumps(cur_values[0])
            else:
                rv = json.dumps({'refunds': cur_values, "metadata": tempx})

            return Response(rv, status=httplib.OK, mimetype='application/json')

    def refund_master(self):
        self.total = OrderRefund.query.count()
        return OrderRefund.query

    def construct(self):

        self.master = self.refund_master()

        if self.pic_id:
            idx = self.pic_id.split(',')
            self.master = self.master.filter(OrderRefund.pic_id.in_(idx))

        if self.user_id:
            idy = self.user_id.split(',')
            self.master = self.master.filter(OrderRefund.user_id.in_(idy))

        if self.refund_id:
            idz = self.refund_id.split(',')
            self.master = self.master.filter(OrderRefund.id.in_(idz))

        if self.origin_payment_id:
            self.master = self.master.filter_by(origin_payment_id=self.origin_payment_id)

        if self.origin_order_id:
            self.master = self.master.filter_by(origin_order_id=self.origin_order_id)

        if self.origin_orderline_id:
            self.master = self.master.filter_by(origin_orderline_id=self.origin_orderline_id)

        if self.refund_amount:
            self.master = self.master.filter_by(refund_amount=self.refund_amount)

        if self.refund_currency_id:
            self.master = self.master.filter_by(refund_currency_id=self.refund_currency_id)

        if self.finance_status:
            self.master = self.master.filter_by(finance_status=self.finance_status)

        if self.refund_datetime_lt:
            self.master = self.master.filter(OrderRefund.refund_datetime < self.refund_datetime_lt)

        if self.refund_datetime_lte:
            self.master = self.master.filter(OrderRefund.refund_datetime <= self.refund_datetime_lte)

        if self.refund_datetime_gt:
            self.master = self.master.filter(OrderRefund.refund_datetime > self.refund_datetime_gt)

        if self.refund_datetime_gte:
            self.master = self.master.filter(OrderRefund.refund_datetime >= self.refund_datetime_gte)

        if self.is_active:
            self.master = self.master.filter_by(is_active=self.is_active)

        if self.order:
            order_data = self.order.split(',')
            for orderx in order_data:
                if orderx[0] == '-': #descending
                    order_param = '%s %s' % (orderx[1:], 'desc')
                    self.master = self.master.order_by(order_param)
                else:
                    self.master = self.master.order_by(orderx)

        self.total = self.master.count()
        self.master = self.master.limit(self.limit).offset(self.offset).all()
        return self.json_success(self.master, self.offset, self.limit)


class OrderTransactionGet():

    def __init__(self, param=None, user_id=None):
        self.args = param

        self.limit = int(param.get('limit', 20))
        self.offset = int(param.get('offset', 0))

        self.fields = param.get('fields', None)
        self.extend = param.get('expand', None)

        self.order_status = param.get('order_status', None)
        self.order_number = param.get('order_number', None)
        self.order_id = param.get('order_id', None)
        self.payment_gateway_id = param.get('payment_gateway_id', None)

        self.user_id = user_id
        self.master = ''

    def json_success(self, data_items=None, offset=None, limit=None):
        if len(data_items) <= 0:
            return Response(None, status=httplib.NO_CONTENT, mimetype='application/json')
        else:
            cur_values = [u.transactions_values() for u in data_items]

            temp={}
            temp['count'] = self.total
            if offset:
                temp['offset'] = int(offset)
            else:
                temp['offset'] = 0

            if limit:
                temp['limit'] = int(limit)
            else:
                temp['limit'] = 0

            tempx = {'resultset': temp}

            rv = json.dumps({'transactions': cur_values, "metadata": tempx})
            return Response(rv, status=httplib.OK, mimetype='application/json')

    def master_orders(self):
        return Order.query.filter_by(user_id=self.user_id).order_by(desc(Order.id))

    def construct(self):
        self.master = self.master_orders()

        if self.order_status:
            self.master = self.master.filter_by(order_status=self.order_status)

        if self.order_number:
            self.master = self.master.filter_by(order_number=self.order_number)

        if self.payment_gateway_id:
            self.master = self.master.filter_by(paymentgateway_id=self.payment_gateway_id)

        if self.order_id:
            self.master = self.master.filter_by(id=self.order_id)

        self.total = self.master.count()
        self.master = self.master.limit(self.limit).offset(self.offset).all()

        return self.json_success(self.master, self.offset, self.limit)
