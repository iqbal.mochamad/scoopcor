import random
from flask_appsfoundry.serializers import SerializerBase, fields


class OrderSerializer(SerializerBase):
    payment_gateway_name = fields.String(attribute='paymentgateway.name')
    payment_gateway_id = fields.Integer(attribute='paymentgateway_id')
    user_id = fields.Integer()
    total_amount = fields.Integer
    final_amount = fields.Integer
    order_id = fields.Integer(attribute='id')
    order_status = fields.Integer()
    order_number = fields.Integer()
    currency_code = fields.String()
    order_lines = fields.List(fields.Nested(
        {'name': fields.String,
         'items': fields.List(fields.Nested({'name': fields.String, 'id': fields.Integer}), attribute='offer.items'),
         'offer_id': fields.Integer(random.randint(1,10000)),
         'final_price': fields.Integer,
         'currency_code': fields.String(random.choice(['idr', 'usd']))
         }
        ))
    remote_order_number = fields.String()


