import requests
import json
import httplib
from datetime import datetime, timedelta

from dateutil.relativedelta import relativedelta
from flask import current_app, Response

from sqlalchemy.sql import desc

from app import app, db
from app.constants import *
from app.helpers import err_response, internal_server_message
from app.offers.choices.quantity_unit import UNIT, DAILY, WEEKLY, MONTHLY
from app.orders.deliver_items import send_receipt_mail

from app.payments.choices.gateways import KREDIVO
from app.points.models import PointUser

from app.orders.models import Order, OrderLineDiscount, OrderLogDispatch
from app.eperpus.helpers import update_shared_library_item_from_order

from app.users.buffets import UserBuffet
from app.eperpus.libraries import SharedLibrary
from app.items.models import Item, UserItem, UserSubscription, UserSubscriptionItem

from app.master.choices import ANDROID, GETSCOOP, IOS
from app.discounts.models import DiscountCode, DiscountCodeLog, Discount
from app.payment_gateways.models import PaymentGateway

from app.send_mail import sendmail_smtp, sendmail_aws

from app.utils.shims import item_types

from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME, STATUS_MCGRAWHILL_CONTENT
from app.offers.choices.offer_type import SINGLE, BUNDLE, BUFFET, SUBSCRIPTIONS
from app.users.wallet import WALLET_OFFER_TYPE_ID, wallet_top_up_transaction

from . import helpers, _log
from .choices import COMPLETE, DELIVERY_ERROR, DELIVERY_IN_PROCESS


class OrderCompleteConstruct(object):

    def __init__(self, user_id=None, payment_gateway_id=None, order_id=None,
            order_number=None, original_transaction_id=None, expires_date=None,
            is_trial=None, is_restore=None, catalog_id=None, repurchased=False):

        self.user_id = user_id
        self.payment_gateway_id = payment_gateway_id
        self.order_id = order_id
        self.order_number = order_number
        self.original_transaction_id = original_transaction_id
        self.expires_date = expires_date
        self.repurchased = repurchased

        self.method = 'GET'
        self.args = {'user_id': user_id, 'payment_id': payment_gateway_id,
                     'order_id': order_id, 'order_number': order_number}

        self.master_order = None
        self.master_payment = None
        self.content_orderline = []
        self.admin = app.config['ADMIN_EMAILS']
        self.is_top_up_wallet_transaction = False

        if is_trial: self.is_trial = is_trial
        else: self.is_trial = False
        self.is_restore = is_restore

        # for eperpus purchase -- auto add to catalog item
        self.catalog_id = catalog_id

    def check_order(self):
        try:
            order = Order.query.filter_by(
                id=self.order_id,
                order_number=self.order_number,
                user_id=self.user_id,
                paymentgateway_id=self.payment_gateway_id).first()

            if not order:
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    error_code=CLIENT_400101,
                    developer_message="Payment Invalid, Invalid Data Payment",
                    user_message="Payment Invalid")

            self.master_order = order
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="order complete 1",
                method=self.method, error=e, req=str(self.args))

    def check_payment(self):
        try:
            payment = PaymentGateway.query.filter_by(id=self.master_order.paymentgateway_id).first()
            if payment:
                self.master_payment = payment
            return "OK"
        except Exception, e:
            internal_server_message(modul="order complete 1a", method=self.method, error=e, req=str(self.args))
            pass

    def invalid_order_status(self, msg=None):
        self.master_order.order_status = 50002 #DELIVERY ERROR STATUS

        logs = OrderLogDispatch(
            user_id = self.user_id,
            order_id = self.order_id,
            order_number = self.order_number,
            error_status = msg,
            data_order = str(self.args)
        )

        db.session.add(logs)
        db.session.add(self.master_order)
        db.session.commit()

    def success_order_status(self):
        self.master_order.order_status = COMPLETE #COMPLETE STATUS
        db.session.add(self.master_order)
        db.session.commit()

    def admin_email_format(self, item_id=None, message=None, mod=None):

        #get payment gateway
        pymnt = ''
        payment = PaymentGateway.query.filter_by(id=self.master_order.paymentgateway_id).first()
        if payment:
            pymnt = payment.name

        if mod == 'single':
            content = """
                Dear Admin, <br><br>

                Order complete failed: ( %s )<br><br>

                ItemId: %s <br>
                UserId : %s <br>
                Invoice Number : %s <br>
                OrderId : %s <br>
                PaymentGateway : %s <br><br>

                Regards, <br>
                Backend team - SALAM KOMPAK!! <br>
                MAY FORCE BE WITH YOU <br>
            """

        if mod == 'subs':
            content = """
                Dear Admin, <br><br>

                Order complete failed: ( %s )<br><br>

                BrandId: %s <br>
                UserId : %s <br>
                Invoice Number : %s <br>
                OrderId : %s <br>
                PaymentGateway : %s <br><br>

                Regards, <br>
                Backend team - SALAM KOMPAK!! <br>
                MAY FORCE BE WITH YOU <br>

            """

        content = content % (message,
            item_id,
            self.master_order.user_id,
            self.master_order.order_number,
            self.master_order.id, pymnt)

        return content

    def send_mail_admin(self, content=None):
        content = content
        try:
            sendmail_aws('ORDER COMPLETE FAILED - MISSING FILE', 'no-reply@gramedia.com', self.admin, content)
        except Exception, e:
            try:
                sendmail_smtp('ORDER COMPLETE FAILED - MISSING FILE',
                    'no-reply@gramedia.com', self.admin, content)
            except Exception, e:
                pass

    def get_master_item(self, item_id=None):
        """
            Get ITEM MASTER DATA
        """
        data_item = None

        # ITEM READY FOR CONSUME
        allowed_item_status = [STATUS_TYPES[STATUS_READY_FOR_CONSUME], STATUS_TYPES[STATUS_MCGRAWHILL_CONTENT]]
        item = Item.query.filter(Item.id == item_id, Item.item_status.in_(allowed_item_status)).first()

        if item:
            data_item = item
        return data_item

    def get_master_extra_item(self, item_id=None):
        # this is only case for extra items only
        # for extra items not checked item status.
        return Item.query.filter_by(id=item_id, is_active=True).first()

    def get_offer_items(self):
        """
            GET OFFER AND ITEM LIST OF ITEMS
        """
        try:
            orderline = self.master_order.get_order_line()
            if orderline:
                for dataline in orderline:
                    offer = dataline.get_offer()
                    if offer:
                        #subscriptions
                        if offer.offer_type_id == 2:
                            self.deliver_subscriptions(offer, dataline)
                        else:
                            #single/bundle
                            data_item = offer.get_items()
                            self.deliver_single_bundle(data_item, dataline)

            return 'OK', 'OK'

        except Exception, e:
            self.invalid_order_status(str(e))
            return 'FAILED', internal_server_message(modul="order complete 2",
                    method=self.method, error=e, req=str(self.args))

    def save_user_item(self, item_id=None, order_line=None, mod=None, subs=None, extra_items=False):
        # store to UserItem library
        try:
            if extra_items:
                pitem = self.get_master_extra_item(item_id)
            else:
                pitem = self.get_master_item(item_id)

            vendor_id = 0
            if pitem:

                # find user item not buffets
                item_user = UserItem.query.filter(
                    UserItem.user_buffet_id == None).filter_by(
                    user_id=self.user_id, item_id=item_id).first()

                if item_user:
                    # JIRA
                    if not item_user.is_active:
                        item_user.is_active = True
                        item_user.save()
                    item_delivered = True
                else:
                    try:
                        vendors = pitem.get_vendor()
                        vendor_id = vendors.get('id', 0)
                    except:
                        vendor_id = 0

                    xitem = UserItem(
                        user_id=self.user_id,
                        item_id=item_id,
                        edition_code=pitem.edition_code,
                        orderline_id=order_line.id,
                        is_active=True,
                        itemtype_id=item_types.enum_to_id(pitem.item_type),
                        brand_id=pitem.brand_id,
                        vendor_id=vendor_id
                    )
                    xitem.save()
                    sv = xitem.save()

                    if sv.get('invalid', False):
                        msg = "%s FAILED TO DELIVER, item_id : %s" % (mod, item_id)
                        self.invalid_order_status(msg)

                        order_line.orderline_status = 50002
                        order_line.save()
                    else:
                        item_delivered = True
                        # Record data junk table only for subscriptions
                        if mod in ['SUBSCRIPTIONS', 'EXTRA SUBSCRIPTIONS']:
                            mo = UserSubscriptionItem(
                                usersubscription_id=subs.id,
                                useritem_id=xitem.id,
                                item_id=item_id,
                                user_id=self.user_id,
                                note='ADD FROM /v1/order'
                            )
                            mo.save()

                if item_delivered:
                    order_line.orderline_status = COMPLETE
                    order_line.save()

            else:
                return 'FAILED'

            return 'OK'
        except Exception as e:
            return 'FAILED'

    def deliver_single_bundle(self, items=None, order_line=None):
        """
            STORE SINGLE / BUNDLE ITEMS / BUFFET ITEMS
        """
        try:
            offers = order_line.get_offer()

            if offers.offer_type_id in [BUNDLE, SINGLE]:
                for data in items:
                    item = self.get_master_item(data)

                    if item:
                        #EXTRA ITEMS INCLUDE
                        if item.get_extra_items():
                            for ixtra in item.get_extra_items():
                                pitem = self.get_master_extra_item(ixtra)

                                if pitem:
                                    self.save_user_item(item_id=ixtra, order_line=order_line, mod='EXTRA SINGLE',
                                        extra_items=True)
                                else:
                                    self.invalid_order_status('EXTRA ITEMS NOT FOUNDS, item-id:%s' % ixtra)
                                    # content = self.admin_email_format(ixtra, 'EXTRA ITEM LOST', 'single')
                                    # self.send_mail_admin(content)

                        #store to UserItem library
                        self.save_user_item(data, order_line, 'SINGLE')

                    else:
                        #TODO : SEND EMAIL TO ADMIN
                        self.invalid_order_status('Item Not Founds : %s' % data)
                        content = self.admin_email_format(data, 'ITEM LOST', 'single')
                        self.send_mail_admin(content)

            if offers.offer_type_id == BUFFET:
                buffet = offers.buffet[0]
                if buffet:
                    buffet_data = UserBuffet(
                        user_id=self.user_id,
                        orderline_id=order_line.id,
                        offerbuffet_id=buffet.id,
                        offerbuffet=buffet,
                        is_restore=False,
                        is_trial=self.is_trial
                    )

                    if self.master_order.platform_id in [ANDROID, GETSCOOP]:
                        # for android and getscoop, expired buffet get from offer valid
                        # buffet_data.original_transaction_id = self.master_order.order_number

                        off_buf = offers.get_buffet_objects()
                        if off_buf:
                            buffet_data.valid_to = datetime.utcnow() + off_buf.buffet_duration
                        else:
                            #if buffet objects not exist + 1 month defaults
                            buffet_data.valid_to = datetime.utcnow() + relativedelta(months=+1)

                        if order_line.is_trial and order_line.campaign_id:
                            exist_trial_discount = Discount.query.filter_by(campaign_id=order_line.campaign_id).first()
                            buffet_data.valid_to = datetime.utcnow() + exist_trial_discount.trial_time

                    if self.master_order.platform_id in [IOS]:
                        # for ios, expired buffet get from itunes.
                        # syncronize for renewal itunes

                        if self.original_transaction_id:
                            buffet_data.original_transaction_id = self.original_transaction_id

                        if self.expires_date:
                            # temporary for testing only
                            buffet_data.valid_to = self.expires_date

                    # complete buffet restore itunes_transaction_id
                    buffet_data.save()

                    # update orderline status
                    order_line.orderline_status = COMPLETE
                    order_line.save()

            self.success_order_status()

        except Exception as e:
            self.invalid_order_status('%s-%s' % ('order_complete3', str(e)))
            return 'FAILED', internal_server_message(modul="order complete 3",
                    method=self.method, error=e, req=str(self.args))

    def deliver_subscriptions(self, offer=None, order_line=None):
        try:
            brands = offer.get_brands_id()
            quantity, quantity_unit, allow_backward, \
            backward_quantity, backward_quantity_unit = offer.get_subscrip_quantity()

            for ibrands in brands:
                subs_data = None
                rl_valid_to = None

                #find item READY FOR CONSUME
                if allow_backward and backward_quantity > 0:

                    #original items
                    original_item = Item.query.filter_by(
                        brand_id=ibrands, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).order_by(
                        desc(Item.release_date)).limit(1).all()

                    # UNIT
                    if backward_quantity_unit == 1:
                        backward_items = Item.query.filter_by(
                            brand_id=ibrands, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).order_by(
                            desc(Item.release_date)).limit(backward_quantity+1).all()

                    #days
                    if backward_quantity_unit == 2:
                        rl_valid_to = datetime.now() - timedelta(days=backward_quantity+1)

                    #weeks
                    elif backward_quantity_unit == 3:
                        rl_valid_to = datetime.now() - timedelta(weeks=backward_quantity+1)

                    #month
                    elif backward_quantity_unit == 4:
                        new_valid = datetime.now() - relativedelta(months=backward_quantity+1)
                        rl_new_form = '%s-%s-%s' % (new_valid.year, new_valid.month, 1)
                        rl_valid_to = datetime.strptime(rl_new_form, '%Y-%m-%d')

                    if rl_valid_to:
                        backward_items = Item.query.filter(Item.release_date >= rl_valid_to).filter_by(
                            brand_id=ibrands, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).order_by(
                            desc(Item.release_date)).all()

                    item_brands = original_item + backward_items

                else:
                    item_brands = Item.query.filter_by(
                        brand_id=ibrands, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).order_by(
                        desc(Item.release_date)).limit(1).all()

                if item_brands:
                    item = item_brands[0]

                    exist_subs = UserSubscription.query.filter_by(user_id=self.master_order.user_id,
                                                                  orderline_id=order_line.id).first()

                    # Edition Subscriptions
                    if (quantity_unit == UNIT and not exist_subs):

                        if order_line.is_trial:
                            # user get 1 item when clicked buy + 1 trial edition
                            quantity = 2

                        sub_unit = UserSubscription(
                            user_id=self.master_order.user_id,
                            quantity=quantity,
                            current_quantity=quantity - 1,
                            quantity_unit=quantity_unit,
                            valid_from=datetime.now(),
                            valid_to=datetime.now(),
                            brand_id=ibrands,
                            subscription_code=offer.offer_code,
                            orderline_id=order_line.id,
                            allow_backward=allow_backward,
                            backward_quantity=backward_quantity,
                            backward_quantity_unit=backward_quantity_unit,
                            is_active=True
                        )

                        mo = sub_unit.save()
                        sv = mo.get('invalid', False)
                        if not sv:
                            # Deliver item to user library
                            subs_data = sub_unit

                            order_line.orderline_status = COMPLETE
                            order_line.save()

                            for xitem in item_brands:
                                self.save_user_item(xitem.id, order_line, 'SUBSCRIPTIONS', subs_data)

                    # Duration Subscriptions
                    if (quantity_unit in [DAILY, WEEKLY, MONTHLY] and not exist_subs):

                        #days
                        if quantity_unit == DAILY:
                            valid_to = datetime.now() + timedelta(days=quantity)

                        # weeks
                        if quantity_unit == WEEKLY:
                            valid_to = datetime.now() + timedelta(weeks=quantity)

                        # month
                        if quantity_unit == MONTHLY:
                            valid_to = datetime.now() + relativedelta(months=quantity)

                        if order_line.is_trial and order_line.campaign_id:
                            exist_trial_discount = Discount.query.filter_by(campaign_id=order_line.campaign_id).first()
                            valid_to = datetime.now() + exist_trial_discount.trial_time

                        sub_unit = UserSubscription(
                            user_id=self.master_order.user_id,
                            quantity=quantity,
                            current_quantity=quantity,
                            quantity_unit=quantity_unit,
                            valid_from=datetime.now(),
                            valid_to=valid_to,
                            brand_id=ibrands,
                            subscription_code=offer.offer_code,
                            orderline_id=order_line.id,
                            allow_backward=allow_backward,
                            backward_quantity=backward_quantity,
                            backward_quantity_unit=backward_quantity_unit,
                            is_active=True
                        )

                        mo = sub_unit.save()
                        sv = mo.get('invalid', False)

                        if not sv:
                            # Deliver item to user library
                            subs_data = sub_unit

                            order_line.orderline_status = COMPLETE
                            order_line.save()

                            for xitem in item_brands:
                                self.save_user_item(xitem.id, order_line, 'SUBSCRIPTIONS', subs_data)

                    ### INSERT EXTRA ITEMS -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
                    if item.get_extra_items() and subs_data:
                        for ixtra in item.get_extra_items():
                            # pitem = self.get_master_item(ixtra)
                            pitem = self.get_master_extra_item(ixtra)
                            if pitem:
                                # Deliver item to user library
                                self.save_user_item(item_id=ixtra, order_line=order_line, mod='EXTRA SUBSCRIPTIONS',
                                    extra_items=True)
                            else:
                                self.invalid_order_status('EXTRA ITEMS NOT FOUNDS, item-id:%s' % ixtra)
                                # content = self.admin_email_format(ixtra, 'EXTRA ITEMS NOT FOUNDS - SUBSCRIPTION',
                                #                                   'subs')
                                # self.send_mail_admin(content)

                else:
                    # TODO : SEND EMAIL TO ADMIN
                    self.invalid_order_status('Item SUBSRCIPTIONS Not Founds, Brands: %s' % ibrands)
                    content = self.admin_email_format(ibrands, 'ITEM LOST - SUBSCRIPTIONS', 'subs')
                    self.send_mail_admin(content)

            self.success_order_status()
            return 'OK', 'OK'
        except Exception as e:
            self.invalid_order_status('%s-%s' % ('order_complete4', str(e)))
            return 'FAILED', internal_server_message(modul="order complete 4",
                    method=self.method, error=e, req=str(self.args))

    def deliver_points(self):

        try:
            point_rule = app.config['POINT_RULE_BY']
            point_expired = app.config['POINT_RULE_EXPIRED']

            if point_rule == 'DAY':
                valid_to = datetime.utcnow() + timedelta(days=point_expired)
                exact_cut_date = datetime.utcnow() + timedelta(days=app.config['POINT_CUT_DURATION'])

            if point_rule == 'MONTH':
                valid_to = datetime.utcnow() + relativedelta(months=point_expired)
                exact_cut_date = datetime.utcnow() + relativedelta(months=app.config['POINT_CUT_DURATION'])

            if point_rule == 'YEAR':
                valid_to = datetime.utcnow() + relativedelta(years=point_expired)
                exact_cut_date = datetime.utcnow() + relativedelta(years=app.config['POINT_CUT_DURATION'])

            #Check if point already recorded
            pts = PointUser.query.filter_by(user_id=self.master_order.user_id).first()

            if pts:
                new_pts = pts.point_amount + self.master_order.point_reward
                pts.point_amount = new_pts
                # every time user purchased, always update expired time
                pts.acquire_datetime = datetime.utcnow()
                pts.expired_datetime = valid_to
                pts.cut_datetime = exact_cut_date
                pts.notify_expired = False

            else:
                pts = PointUser(
                    user_id=self.master_order.user_id,
                    client_id=self.master_order.client_id,
                    point_amount=self.master_order.point_reward,
                    point_used=0,
                    acquire_datetime=datetime.utcnow(),
                    expired_datetime=valid_to,
                    cut_datetime = exact_cut_date,
                    is_active=True,
                    notify_expired = False
                )
            pts.save()
            return 'OK', 'OK'

        except Exception as e:
            print "Error kah???...", e
            return 'FAILED', internal_server_message(modul="order complete 4",
                method=self.method, error=e, req=str(self.args))

    def deliver_coupon_code(self):
        try:
            #get discount code only
            order_dsc = OrderLineDiscount.query.filter_by(
                order_id=self.master_order.id, discount_type=5).all()

            if order_dsc:
                for dsc_name in order_dsc:
                    dsc_code = DiscountCode.query.filter_by(code=dsc_name.discount_code.lower()).first()

                    if dsc_code:
                        current = dsc_code.current_uses + 1

                        # if current use + 1 >= max_uses, store coupon to LOG
                        if current >= dsc_code.max_uses:
                            cot = DiscountCodeLog(
                                code=dsc_code.code,
                                max_uses=dsc_code.max_uses,
                                current_uses=current,
                                is_active=True,
                                discount_type=dsc_code.discount_type,
                                discount_id=dsc_code.discount_id
                            )
                            # save log discount code
                            cot.save()

                            # remove discount code
                            dsc_code.delete()

                        dsc_code.current_uses = current
                        dsc_code.save()

                    #SC1 UPDATE TABLE PURCHASE
                    #self.update_sc1_purchas(coupon_code=dsc_code)

            return 'OK', 'OK'

        except Exception as e:
            return 'FAILED', internal_server_message(modul="order complete 6",
                method=self.method, error=e, req=str(self.args))

    def get_useremail(self):
        from app.users.users import User
        user = User.query.get(self.user_id)
        return user.email if user and user.email else None

    def deliver_receipt_emails(self):
        try:
            email_user = self.get_useremail()
            send_receipt_mail(order=self.master_order, customer_email=email_user, catalog_id=self.catalog_id)
            return "OK"

        except Exception as e:
            internal_server_message(modul="order complete send receipt emails",
                method=self.method, error=e, req=str(self.args))

    def json_items(self, orderline=None):
        data_item = []

        offer = orderline.get_offer()
        if offer:
            if offer.offer_type_id in [2,4]:
                if offer.offer_type_id == 4:
                    # get only 1 brands, to prevent error because of too many brands in a buffet
                    brands = [offer.brands[0].id, ] if offer.brands[0] else []
                else:
                    brands = offer.get_brands_id()

                for ibrands in brands:
                    item = {}

                    item_brands = Item.query.filter_by(brand_id=ibrands, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).order_by(
                        desc(Item.release_date)).limit(1).all()

                    if item_brands:
                        ms_it = item_brands[0]
                        item['name'] = ms_it.name
                        item['id'] = ms_it.id
                        item['edition_code'] = ms_it.edition_code
                        item['item_status'] = {v:k for k,v in STATUS_TYPES.items()}[ms_it.item_status]
                        item['release_date'] = ms_it.release_date.isoformat()
                        item['is_featured'] = ms_it.is_featured
                        item['vendor'] = ms_it.get_vendor()
                        item['languages'] = ms_it.get_languages()
                        item['countries'] = ms_it.get_country()
                        item['authors'] = ms_it.get_authors()
                        data_item.append(item)

                if offer.offer_type_id == 4:
                    data_item = data_item[0]
                    data_item['name'] = orderline.name
                    data_item = [data_item]

            else:
                for xo in offer.get_items():
                    item = {}
                    ms_it = self.get_master_item(xo)
                    item['name'] = ms_it.name
                    item['id'] = ms_it.id
                    item['edition_code'] = ms_it.edition_code
                    item['item_status'] = {v:k for k,v in STATUS_TYPES.items()}[ms_it.item_status]
                    item['release_date'] = ms_it.release_date.isoformat()
                    item['is_featured'] = ms_it.is_featured
                    item['vendor'] = ms_it.get_vendor()
                    item['languages'] = ms_it.get_languages()
                    item['countries'] = ms_it.get_country()
                    item['authors'] = ms_it.get_authors()
                    data_item.append(item)

        return data_item

    def json_orderlines(self):
        orderlines = self.master_order.get_order_line()

        order = []
        for dataline in orderlines:
            temp = {}

            offers = dataline.get_offer()

            temp['name'] = dataline.name
            temp['is_free'] = dataline.is_free
            temp['offer_id'] = dataline.offer_id
            temp['raw_price'] = '%.2f' % dataline.price
            temp['final_price'] = '%.2f' % dataline.final_price
            temp['currency_code'] = dataline.currency_code
            temp['offer_type_id'] = offers.offer_type_id

            temp['items'] = self.json_items(dataline)

            if offers.offer_type_id == BUFFET:
                buffet_data = dataline.get_buffets()
                if buffet_data:
                    temp['valid_to'] = buffet_data.valid_to.isoformat()

            order.append(temp)
        return order

    def json_success(self):
        temp = {}
        order = self.master_order

        pymnt = ''
        payment = PaymentGateway.query.filter_by(id=order.paymentgateway_id).first()
        if payment:
            pymnt = payment.name

        if order.paymentgateway_id == KREDIVO:
            # add some response required by kredivo
            temp['status'] = "OK"
            temp['message'] = ""

        temp['user_id'] = order.user_id
        if order.tier_code:
            temp['tier_code'] = order.tier_code
        temp['currency_code'] = order.currency_code
        temp['order_id'] = order.id
        temp['temp_order_id'] = self.master_order.temporder_id
        temp['payment_gateway_id'] = order.paymentgateway_id
        temp['final_amount'] = '%.2f' % order.final_amount
        temp['total_amount'] = '%.2f' % order.total_amount
        if order.platform_id:
            temp['platform_id'] = order.platform_id
        temp['order_number'] = order.order_number

        temp['payment_gateway_name'] = pymnt
        temp['order_status'] = order.order_status
        temp['order_lines'] = self.json_orderlines()

        temp['is_active'] = self.master_order.is_active
        temp['client_id'] = self.master_order.client_id
        temp['point_reward'] = self.master_order.point_reward
        temp['partner_id'] = self.master_order.partner_id

        return Response(json.dumps(temp), status=httplib.OK, mimetype='application/json')

    def order_change_status(self):
        """
            CHECKING STATUS ORDERLINE, AND UPDATE ORDER STATUS
            CASE 1 ORDER 3 ORDERLINE.
                3 ORDERLINE: 90000, ---------------------------------------> ORDER : 90000
                2 ORDERLINE: 90000, 1 ORDERLINE 30002 ---------------------> ORDER : 30002
                1 ORDERLINE: 90000, 1 ORDERLINE 30002 1 ORDERLINE 50002 ---> ORDER : 50002

        """
        orderline = self.master_order.get_order_line()

        session = db.session()
        shared_library = session.query(SharedLibrary).get(self.master_order.user_id)

        if orderline:
            dat_status = []
            for dataline in orderline:
                dat_status.append(dataline.orderline_status)

                if dataline.offer.offer_type_id == WALLET_OFFER_TYPE_ID:
                    self.is_top_up_wallet_transaction = True

                if shared_library:
                    # add inventory to shared library/organization - items
                    update_shared_library_item_from_order(
                        session,
                        shared_library,
                        offer=dataline.offer,
                        quantity_buy=dataline.quantity,
                        catalog_id=self.catalog_id
                    )

            # TODO : if self.is_restore self.master_order.order_status = PAYMENT_RESTORED

            if DELIVERY_ERROR in dat_status:
                self.master_order.order_status = DELIVERY_ERROR
            elif DELIVERY_IN_PROCESS in dat_status:
                self.master_order.order_status = DELIVERY_IN_PROCESS
            else:
                self.master_order.order_status = COMPLETE

                if self.is_top_up_wallet_transaction:
                    wallet_top_up_transaction(self.master_order, session)

            self.master_order.save(session)

            if shared_library:
                session.commit()
                # import delta data to SOLR (update shared library item information in SOLR)
                try:
                    url = '{}/scoop-sharedlib-all/dataimport'.format(current_app.config['SOLR_BASE_URL'])
                    requests.post(url, data={'command': 'delta-import', 'wt': 'json'})
                except:
                    pass

    def construct(self):
        check_order = self.check_order()
        if check_order[0] != 'OK':
            return check_order[1]

        check_payment = self.check_payment()

        # deliver to owned_items/ owned_subscriptions / owned_buffet
        offer = self.get_offer_items()
        if offer[0] != 'OK':
            return offer[1]

        coupon_code = self.deliver_coupon_code()
        if coupon_code[0] != 'OK':
            pass

        # PAYMENT : POINT & FREE PURCHASE NOT RECEIVE POINT REWARD
        if helpers.user_allowed_to_get_point(user_id=self.master_order.user_id,
                                     payment_gateway_id=self.payment_gateway_id,
                                     is_top_up_wallet_transaction=self.is_top_up_wallet_transaction):
            store_point = self.deliver_points()
            # No need return error if insert to points.

        # checking status orderlines..
        self.order_change_status()

        # sending receipts emails to users
        if not self.repurchased:
            self.deliver_receipt_emails()

        return self.json_success()


