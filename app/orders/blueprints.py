from flask import Blueprint
import flask_restful as restful

from .api import OrderCheckout, OrderConfirm, OrderComplete, OrderApi, \
    OrderListApi, OrderNumberApi, OrderTempApi, OrderListTempApi, \
    OrderRefundListApi, OrderRefundApi, RemoteCheckoutApi, \
    OrderRefundListApi, OrderRefundApi, OrderTransactionApi


blueprint = Blueprint('orders', __name__, url_prefix='/v1/orders')
blueprint.add_url_rule('/checkout', view_func=OrderCheckout.as_view('checkout'))


api = restful.Api(blueprint)
api.add_resource(OrderConfirm, '/confirm')
api.add_resource(OrderComplete, '/complete')

api.add_resource(OrderApi, '')
api.add_resource(OrderListApi, '/<int:order_id>', endpoint='orders')
api.add_resource(OrderNumberApi, '/order_number/<order_number>', endpoint='orders_numbers' )

api.add_resource(OrderTempApi, '/temp')
api.add_resource(OrderListTempApi, '/temp/<int:order_id>', endpoint='orders_temp')

api.add_resource(OrderRefundListApi, '/refunds')
api.add_resource(OrderRefundApi, '/refunds/<int:refund_id>', endpoint='refunddetail')

api.add_resource(RemoteCheckoutApi, '/remote-checkout/<remote_service_name>')

api.add_resource(OrderTransactionApi, '/transactions/<int:user_id>', endpoint='transactions')
