"""
Helpers
=======

Simple helper functions used in this module.
"""
import logging
import os
from datetime import datetime, timedelta

from dateutil.relativedelta import relativedelta
from flask import current_app, g
from sqlalchemy import desc
from werkzeug.exceptions import BadRequest, Forbidden, UnprocessableEntity, Conflict

from app import db
from app.constants import RoleType
from app.discounts.choices.predefined_type import ALL_SUBSCRIPTIONS, ALL_MAGAZINE, ALL_NEWSPAPERS, \
    MAGAZINE_SINGLE, MAGAZINE_SUBSCRIPTIONS, MAGAZINE_NEWSPAPER, ALL_OFFERS, DISCOUNT_PREDEFINED_GROUP
from app.discounts.choices.predefined_type import ALL_SINGLE, ALL_BOOK
from app.discounts.models import Discount
from app.helpers import generate_invoice
from app.items.choices import BOOK, MAGAZINE, NEWSPAPER, ITEM_TYPES
from app.items.models import UserItem, UserSubscription, Item
from app.master.choices import GETSCOOP
from app.offers.models import Offer
from app.orders.choices import CANCELLED, NEW, WAITING_FOR_PAYMENT, PAYMENT_IN_PROCESS
from app.orders.models import OrderLineDiscountTemp
from app.payment_gateways.models import PaymentGateway
from app.remote_publishing.choices import ELEVENIA, GRAMEDIA
from app.utils.shims import item_types
from app.users.users import User
from app.users.buffets import UserBuffet
from app.payments.models import Payment
from app.payments.choices.gateways import ELEVENIA as ELEVENIA_PAYMENT_GATEWAY, \
    GRAMEDIA as GRAMEDIA_PAYMENT_GATEWAY, FREE, POINT
from app.payments.payment_status import PAYMENT_BILLED

from . import _log
from .choices import COMPLETE
from .models import OrderDetail, OrderLine, Order, OrderLineDiscount

from app.offers.choices.offer_type import BUFFET, SINGLE, SUBSCRIPTIONS


_log_audit = logging.getLogger('scoopcor.remote_checkout')

def update_order_payments(order_id=None):
    from unidecode import unidecode
    order = Order.query.filter_by(id=order_id).first()

    if order:
        order.order_status = COMPLETE
        db.session.add(order)
        print 'success.. update order : {} - {}'.format(order.id, order.user_id)

        orderlines = OrderLine.query.filter_by(order_id=order.id).all()
        for iorder in orderlines:
            iorder.orderline_status = COMPLETE
            db.session.add(iorder)
            print 'success.. update orderline : {} - {}'.format(iorder.id, unidecode(iorder.name))

        payment = Payment.query.filter_by(order_id=order.id).order_by(desc(Payment.id)).first()
        if payment:
            payment.payment_status = PAYMENT_BILLED
            db.session.add(payment)
            print 'success.. update payment : {} - {} - {}'.format(payment.id, payment.user_id, payment.order_id)
    db.session.commit()


# this for CS team api portal later.
def update_order_data(order_id):
    from app.orders.order_complete import OrderCompleteConstruct

    order = Order.query.filter_by(id=order_id).first()
    if order:
        update_order_payments(order.id)
        scene = OrderCompleteConstruct(user_id=order.user_id, payment_gateway_id=order.paymentgateway_id,
                                       order_id=order.id, order_number=order.order_number, repurchased=True).construct()
        return scene.data
    else:
        return {}


def order_status(order_id=None, order_status=None, order=None, session=None):

    session = session or db.session()
    if not order:
        order = session.query(Order).get(order_id)

    if not order:
        return 'FAILED'
    else:
        order.order_status = order_status
        order.save(session)
        return 'OK'


def assert_remote_ordering_allowed(remote_service, user_id):
    """ Verify that the provided user_id is permitted to use remote checkout.

    The authorization checks will be different for each service, however

    :param `str` remote_service: The remote service identifier
    :param `int` user_id: The remote user identity.
    :raises: :class:`Forbidden` if authorization fails.
    :raises: :class:`BadRequest` if invalid remote service requested.
    :returns: None if the authorization checks succeed.
    """

    if remote_service == ELEVENIA:
        # allowed_users = [436786, ] if current_app.debug else [436785, ]
        allowed_users = []
    elif remote_service == GRAMEDIA:
        # 523861 gramedia-live@merchant.scoop
        # 523862 gramedia-dev@merchant.scoop
        allowed_users = [523862, ] if current_app.debug else [523861, ]
    else:
        raise BadRequest("Invalid Remote Service")

    _log.debug("Allowed users for {remote_service} checkout: {user_id}".format(
        user_id=",".join(str(i) for i in allowed_users),
        remote_service=remote_service))

    try:
        if user_id not in allowed_users:
            raise Forbidden
        _log.info(
            "User(id={user_id}) authorized for {remote_service} checkout.".format(
                user_id=user_id,
                remote_service=remote_service))
    except Forbidden:
        if current_app.debug and g.user and 'can_read_write_global_all' in g.user.perm:
            _log.debug("User(id={user_id}) not authorized for checkout, but "
                       "permitted because app is in DEBUG mode.".format(user_id=user_id))
        else:
            _log.info("User(id={user_id}) failed authorization for "
                      "{remote_service}  checkout".format(
                            user_id=user_id,
                            remote_service=remote_service))
            raise Forbidden


def get_paymentgateway_for_remote_service(remote_service):
    """ Fetches the paymengateway.id for a given remote service.

    This payment gateway is generally a 'fake' that does not actually do any
    payment processing.

    :param `basestring` remote_service: A unique identifier for a remote
        service.
    :return: The `app.payment_gateways.models.PaymentGateway` id that should be
        used for the provided remote_service identifier.
    :rtype: int
    """

    if remote_service == ELEVENIA:
        payment_gateway_id = ELEVENIA_PAYMENT_GATEWAY
    elif remote_service == GRAMEDIA:
        payment_gateway_id = GRAMEDIA_PAYMENT_GATEWAY
    else:
        payment_gateway_id = None

    if payment_gateway_id:
        s = db.session()
        payment_gateway = s.query(PaymentGateway).get(payment_gateway_id)
        if payment_gateway is None:
            raise Conflict(
                "Could not find PaymentGateway(id={})".format(payment_gateway_id))
    else:
        raise BadRequest("Invalid Remote Service")

    return payment_gateway


def get_client_id_for_remote_service(remote_service):
    """ Returns a CAS client.id that is valid for orders with a remote service.

    :param `basestring` remote_service: A unique identifier for a remote
        service.
    :return: A CAS client.id that should be used when registering new
        orders from a remote service.
    :rtype: int
    """
    if remote_service == ELEVENIA:
        return 80
    elif remote_service == GRAMEDIA:
        return 82
    raise BadRequest("Invalid Remote Service")


def construct_order_from_remote_request(args, session, user):
    """ Creates an Order object with data provided as a callback from the
        Elevenia API.

    :param `dict` args: Dictionary representation of JSON data posted from
        the elevenia API.
    :param `sqlalchemy.orm.session.Session` session: Currently-active database
        session object.
    :param `app.users.models.User` user:  an instance of User as Customer
    :return: Returns a fully-configured Order object based on arguments
        expected from a callback from the Elevenia API.
    :rtype: :class:`app.orders.models.Order`
    """
    # noinspection PyArgumentList
    with session.no_autoflush:
        remote_service_name = args.get('remote_service_name', None)
        remote_order_number = args.get('remote_order_number', None)
        order = Order(
            order_number=generate_invoice(),
            total_amount=args['sub_total'],
            final_amount=args['grand_total'],
            user_id=user.id,
            client_id=get_client_id_for_remote_service(remote_service_name),
            partner_id=None,
            order_status=COMPLETE,
            currency_code=(args['currency_code']).upper(),
            paymentgateway=get_paymentgateway_for_remote_service(remote_service_name),
            tier_code=None,
            platform_id=GETSCOOP,
            temporder_id=None,
            order_detail=[OrderDetail(
                temporder_id=None,
                user_email=args['receiver_email'],
                user_name=args['receiver_email'],
            )],
            is_active=True,
            remote_order_number=remote_order_number,
            point_reward=0,
        )

        # add the line items to the order information, and then also
        # deliver the items to the user's UserItems as the same time
        total_base_price = 0
        for line_info in args['items']:

            scoop_offer_id_with_prefix = line_info['id']
            scoop_offer_id = int(scoop_offer_id_with_prefix.lstrip(u"SC00P"))

            offer = session.query(Offer).get(scoop_offer_id)

            base_price = offer.price_usd if (args['currency_code']).upper() == 'USD' else offer.price_idr
            total_base_price += base_price

            if not offer:
                raise BadRequest("Invalid Product with id {}".format(scoop_offer_id))

            if offer.offer_type.id == SINGLE:
                item = offer.items.first()
                name = item.name
            else:
                name = offer.long_name

            # noinspection PyArgumentList
            ol = OrderLine(
                name=name,
                offer=offer,
                is_active=True,
                is_free=False,
                is_discount=False,
                user_id=user.id,
                quantity=1,
                orderline_status=COMPLETE,
                currency_code=(args['currency_code']).upper(),
                price=base_price,
                final_price=line_info['grand_total'],
                localized_currency_code='IDR',
                localized_final_price=line_info['grand_total'],
            )

            order.order_lines.append(ol)

            # add order line discounts if applicable
            try:
                ol_discount = add_order_line_discount(session, offer, line_info, args,
                                                      remote_service_name, remote_order_number,
                                                      ol)
                if ol_discount:
                    order.order_line_discounts.append(ol_discount)
            except DiscountNotValidForOfferInRemoteCheckout as e:
                # if there's invalid discount, mark it as CANCELLED (50000)
                order.order_status = CANCELLED
                ol.orderline_status = CANCELLED
                _log.exception(e.message)

        order.total_amount = total_base_price

        return order


def add_order_line_discount(session, offer, line_info, args, remote_service_name, remote_order_number, order_line):

    discount = get_discount_for_remote_offer(session, offer, line_info, remote_service_name, remote_order_number)

    if discount:
        ol_discount = OrderLineDiscount(
            order_line=order_line,
            discount_id=discount.id,
            discount_name=discount.name,
            currency_code=(args['currency_code']).upper(),
            discount_code=None,
            discount_type=discount.discount_type,
            discount_value=discount.discount_idr,
            raw_price=offer.price_idr,
            final_price=line_info['grand_total']
        )
        order_line.campaign_id = discount.campaign_id
        order_line.is_discount = True
        return ol_discount
    else:
        return None


def get_discount_for_remote_offer(session, offer, line_info, remote_service_name, remote_order_number):
    discount_id = line_info.get('discount_id', None)
    discount = None
    if discount_id:
        # if remote service already send discount id, validate it
        discount = session.query(Discount).get(discount_id)
        assert_discount_from_remote_checkout(discount, offer, remote_service_name, remote_order_number)
        return discount
    else:
        pass
        # get default for general discount
        # if remote_service_name == GRAMEDIA:
            # get general discount for gramedia, based on the offer
            # for elevenia, no discount defined yet
        #    discount = get_general_discount_for_gramedia(session, offer)

    return discount


def assert_discount_from_remote_checkout(discount, offer, remote_service_name, remote_order_number):
    if not discount:
        err_msg = "Discount id {discount_id} not found for " \
                  "remote order no {no} from {serice_name}".format(
                        discount_id=discount.id,
                        no=remote_order_number,
                        serice_name=remote_service_name
                    )
        _log_audit.info(err_msg)
        raise DiscountNotValidForOfferInRemoteCheckout(err_msg)
    else:
        # discount found, next validate it:

        # offer.items could be empty for offer subscription or buffet (get item from offer.brands later)
        item = offer.items.first()
        brand = item.brand if item else offer.brands.first()
        if not item and brand:
            item = brand.items.first()

        offer_vendor_id = brand.vendor_id if brand else None
        discount_valid = True

        # Three level discount validation for remote checkout

        if discount.vendors.all() and offer_vendor_id not in [vendor.id for vendor in discount.vendors.all()]:
            # vendor of the item in offer not exists for this discount, discount invalid!
            discount_valid = False

        if discount.offers.all():
            offer_in_discount = discount. \
                offers. \
                filter(Offer.id == offer.id) \
                .first()
            if not offer_in_discount:
                discount_valid = False

        if discount.predefined_group:
            if not item:
                discount_valid = False
            elif discount.predefined_group == ALL_BOOK and item.item_type != BOOK:
                discount_valid = False
            elif discount.predefined_group == ALL_MAGAZINE and item.item_type != MAGAZINE:
                discount_valid = False
            elif discount.predefined_group == ALL_NEWSPAPERS and item.item_type != NEWSPAPER:
                discount_valid = False
            elif discount.predefined_group == ALL_SINGLE and offer.offer_type_id != SINGLE:
                discount_valid = False
            elif discount.predefined_group == ALL_SUBSCRIPTIONS and offer.offer_type_id != SUBSCRIPTIONS:
                discount_valid = False
            elif discount.predefined_group == MAGAZINE_SINGLE \
                    and not (offer.offer_type_id == SINGLE and item.item_type == MAGAZINE):
                discount_valid = False
            elif discount.predefined_group == MAGAZINE_SUBSCRIPTIONS \
                    and not (offer.offer_type_id == SUBSCRIPTIONS and item.item_type == MAGAZINE):
                discount_valid = False
            elif discount.predefined_group == MAGAZINE_NEWSPAPER \
                    and (item.item_type != NEWSPAPER and item.item_type != MAGAZINE):
                discount_valid = False

        if not discount_valid:
            err_msg = "Offer Id {offer_id} is invalid for Discount with Id {discount_id}." \
                      "Remote checkout not allowed".format(
                            offer_id=offer.id,
                            discount_id=discount.id
                        )
            _log_audit.info(err_msg)
            raise DiscountNotValidForOfferInRemoteCheckout(err_msg)
        else:
            return None


class DiscountNotValidForOfferInRemoteCheckout(Exception):
    pass


def construct_payment_from_order(order):
    """

    :param `app.orders.models.Order` order: The order instance to construct
        this payment for.
    :return:
    :rtype: :class:`app.payments.models.Payment`
    """
    # noinspection PyArgumentList
    return Payment(
        order=order,
        user_id=order.user_id,
        paymentgateway_id=order.paymentgateway.id,
        currency_code=order.paymentgateway.base_currency.iso4217_code,
        amount=order.final_amount,
        payment_status=PAYMENT_BILLED,
        payment_datetime=datetime.utcnow()
    )


def construct_useritems_from_order(order, remote_service_name):
    """

    :param `app.orders.models.Order` order:
    :return: List of :class:`app.items.models.UserItem`
    :rtype: list
    """
    user_items = []

    order_lines = [ol for ol in order.order_lines]

    for order_line in order_lines:
        offer = order_line.offer

        if offer.offer_type.id == SINGLE:
            for item in offer.items:

                # make sure the user doesn't already own this item for elevenia
                user_doesnt_already_own = assert_user_doesnt_already_own(
                    order.user_id, item.id, remote_service_name, order.remote_order_number)

                # noinspection PyArgumentList
                if user_doesnt_already_own:
                    user_items.append(
                        UserItem(user_id=order.user_id,
                                 item_id=item.id,
                                 orderline=ol,
                                 itemtype_id=item_types.enum_to_id(item.item_type),
                                 edition_code=item.edition_code,
                                 is_active=True,
                                 is_restore=False,
                                 vendor_id=item.brand.vendor.id,
                                 brand_id=item.brand_id))

        elif offer.offer_type.id == BUFFET:

            # noinspection PyArgumentList
            user_items.append(
                UserBuffet(user_id=order.user_id,
                           orderline=ol,
                           offerbuffet=offer.buffet[0],
                           valid_to=datetime.utcnow() + offer.buffet[0].buffet_duration,
                           is_restore=False,
                           is_trial=False)
            )

        elif offer.offer_type.id == SUBSCRIPTIONS:

            user_items = []
            order_lines = [ol for ol in order.order_lines]

            for order_line in order_lines:

                offer = order_line.offer
                for brand in offer.brands:

                    latest_item = brand.items.filter(
                        Item.is_active == True,
                        Item.release_date <= datetime.now(),
                        Item.item_status == 'ready for consume'
                    ).order_by(desc(Item.created)).first()

                    # noinspection PyArgumentList
                    user_items.append(
                        UserItem(user_id=order.user_id,
                                 item_id=latest_item.id,
                                 orderline=order_line,
                                 is_active=True,
                                 vendor=brand.vendor,
                                 brand=brand))

                    if offer.subscription.first().quantity_unit == 1:
                        valid_to = datetime.now()
                    elif offer.subscription.first().quantity_unit == 2:
                        valid_to = datetime.now() + timedelta(days=offer.subscription.first().quantity)
                    elif offer.subscription.first().quantity_unit == 3:
                        valid_to = datetime.now() + timedelta(weeks=offer.subscription.first().quantity)
                    elif offer.subscription.first().quantity_unit == 4:
                        valid_to = datetime.now() + relativedelta(months=offer.subscription.first().quantity)
                    else:
                        raise ValueError("Unknown quantity_unit: {}".format(offer.subscription.first().quantity_unit))

                    # noinspection PyArgumentList
                    user_items.append(
                        UserSubscription(
                            user_id=order.user_id,
                            quantity=offer.subscription.first().quantity,
                            quantity_unit=offer.subscription.first().quantity_unit,
                            current_quantity=offer.subscription.first().quantity-1,
                            valid_from=datetime.now(),
                            valid_to=valid_to,
                            brand=offer.brands.first(),
                            subscription_code=offer.offer_code,
                            orderline=order_line,
                            is_active=True,
                            allow_backward=offer.subscription.first().allow_backward,
                            backward_quantity_unit=offer.subscription.first().backward_quantity,
                            backward_quantity=offer.subscription.first().backward_quantity_unit
                        )
                    )

    return user_items


def assert_user_doesnt_already_own(user_id, item_id, remote_service_name, remote_order_number=None):
    """ Check user owned item (outright, not including buffet items).
    For Elevenia: Raises an exception if the user already owns the Item
    For Gramedia.com: don't raise error, just don't add it to user owned item.

    :param `int` user_id:
    :param `int` item_id: Primary key of app.items.models.Item
    :param 'app.remote_publishing.choices' remote_service_name: ELEVENIA or GRAMEDIA
    :param 'str' remote_order_number: remote order no
    :return: 'bool' True or False
    """
    user_items = UserItem.query.\
        filter(UserItem.item_id == item_id,
               UserItem.user_id == user_id,
               UserItem.user_buffet_id == None).\
        order_by(UserItem.id.desc()).count()

    if user_items:
        if remote_service_name == ELEVENIA:
            # user already owned the item, raise error to stop process
            # so user don't order the same item
            # in elevenia, this process before checkout confirm, so it's ok
            raise UnprocessableEntity(
                "User(id={user_id}) already owns Item(id={item_id})".format(
                    user_id=user_id,
                    item_id=item_id))
        elif remote_service_name == GRAMEDIA:
            # user already owned the item
            # don't raise error, just don't add it to owned item
            _log_audit.info("User(id={user_id}) already owns Item(id={item_id}), "
                       "remote order no {no} from {serice_name}".format(
                            user_id=user_id,
                            item_id=item_id,
                            no=remote_order_number,
                            serice_name=remote_service_name
            ))
            return False
    else:
        return True


def assert_remote_order_not_exists(remote_order_number, session):
    existing_remote_order = session.query(Order).filter(Order.remote_order_number == remote_order_number).first()
    if existing_remote_order:
        raise Conflict("Order with remote order number {} already exists!".format(remote_order_number))


def user_allowed_to_get_point(user_id, payment_gateway_id, is_top_up_wallet_transaction):
    is_allowed = False
    if payment_gateway_id not in [FREE, POINT] and not is_top_up_wallet_transaction:
        user_point = User.query.get(user_id)
        if user_point:
            # point only delivered if the orders is from a user (for organization -> no point)
            roles = list(set([role.id for role in user_point.roles.all()]))
            # user with guest role doesn't get points
            if not (len(roles) == 1 and RoleType.guest_user in roles):
                is_allowed = True
    return is_allowed


def retrieve_promotions_checkout(order):
    sum_data, details_data = [], []

    exist_promotions = OrderLineDiscountTemp.query.filter_by(order_id=order.id).order_by(OrderLineDiscountTemp.id).all()
    base_currencies = order.paymentgateway.base_currency.iso4217_code

    for data_promo in exist_promotions:
        promo_name, promo_cut = data_promo.discount_name, 0

        if data_promo.discount_code:
            promo_name = 'Coupon : {}'.format(data_promo.discount_code)

        if base_currencies == 'IDR':
            promo_cut = data_promo.price_idr - data_promo.final_price_idr
            sum_data.append(promo_cut)

        if base_currencies == 'USD':
            promo_cut = data_promo.price_usd - data_promo.final_price_usd
            sum_data.append(promo_cut)

        if base_currencies == 'PTS':
            promo_cut = data_promo.price_point - data_promo.final_price_point
            sum_data.append(promo_cut)

        if promo_cut > 0:
            details_data.append({'promo_name': promo_name, 'currency_code': base_currencies, 'total_amount': '%.2f' % promo_cut})

    sum_data = {'total_amount': '%.2f' % sum(sum_data) if sum_data else 0, 'currency_code': base_currencies} \
        if details_data else {}

    return sum_data, details_data


def retrieve_promotions_confirm(order):
    sum_data, details_data = [], []
    exist_promotions = OrderLineDiscount.query.filter_by(order_id=order.id).order_by(OrderLineDiscount.id).all()

    for data_promo in exist_promotions:
        promo_name, promo_cut = data_promo.discount_name, 0

        if data_promo.discount_code:
            promo_name = 'Coupon : {}'.format(data_promo.discount_code)

        promo_cut = data_promo.raw_price - data_promo.final_price
        sum_data.append(promo_cut)

        if promo_cut > 0:
            details_data.append(
                {'promo_name': promo_name, 'currency_code': data_promo.currency_code, 'total_amount': '%.2f' % promo_cut})

    sum_data = {'total_amount': '%.2f' % sum(sum_data) if sum_data else 0,
                'currency_code': order.paymentgateway.base_currency.iso4217_code} if details_data else {}

    return sum_data, details_data


def order_signature_encrypt(value):
    encrypt_data = "{}#{}".format(value, os.urandom(32).encode('hex'))
    return encrypt_data.encode('hex')


def check_coupon_error(coupon_code, discounts, offer, payment_gateway, user_id):
    from app.users.users import User

    error_message = None

    # check coupon exceded!
    if not coupon_code.coupon_can_used():
        error_message = "Coupon invalid, has exceeded the maximum uses."

    # check coupon predefined group
    if discounts.predefined_group:
        offer_type = offer.get_item_objects()
        allowed_predefined = []

        if offer_type:
            # SINGLE / BUNDLING
            item_data = Item.query.filter(Item.id.in_(offer.get_items())).order_by(desc(Item.id)).first()
        else:
            # SUBSCRIPTIONS / PREMIUM
            item_data = Item.query.filter(Item.brand_id.in_(
                offer.get_brands_id())).order_by(desc(Item.release_date)).limit(2).first()

        # MAGAZINE
        if item_data.item_type == ITEM_TYPES[MAGAZINE]:

            if offer.get_items():
                # MAGAZINE SINGLE
                allowed_predefined = [ALL_OFFERS, ALL_MAGAZINE, ALL_SINGLE, MAGAZINE_SINGLE]
            else:
                # MAGAZINE SUBS
                allowed_predefined = [ALL_OFFERS, ALL_MAGAZINE, ALL_SUBSCRIPTIONS, MAGAZINE_SUBSCRIPTIONS]

        # BOOK
        if item_data.item_type == ITEM_TYPES[BOOK]:
            allowed_predefined = [ALL_OFFERS, ALL_BOOK, ALL_SINGLE]

        # NEWSPAPERS
        if item_data.item_type == ITEM_TYPES[NEWSPAPER]:
            allowed_predefined = [ALL_OFFERS, ALL_NEWSPAPERS, ALL_SUBSCRIPTIONS]

        if discounts.predefined_group not in allowed_predefined:
            error_message = "This coupon can be exchanged only for {}".format(
                DISCOUNT_PREDEFINED_GROUP[discounts.predefined_group])

        if discounts.predefined_group and offer.offer_type_id == BUFFET:
            error_message = "This coupon can be exchanged only for {}".format(
                DISCOUNT_PREDEFINED_GROUP[discounts.predefined_group])

    # check coupon can used in offer.
    if not discounts.predefined_group and offer.offer_type_id != BUFFET:
        if str(coupon_code.discount_id) not in offer.discount_id:
            error_message = "This coupon not available for this offer."

    # check if paymentgateways not included.
    if payment_gateway != FREE and payment_gateway not in discounts.paymentgateway_id():
        data_payments = PaymentGateway.query.filter(PaymentGateway.id.in_(discounts.paymentgateway_id())).all()

        valid_payment = ' / '.join([i.name for i in data_payments])
        error_message = "This coupon can be used only for : {} payments".format(valid_payment)

    # check for specified user.
    coupon_users = [i.id for i in coupon_code.users]
    if coupon_users and not user_id in coupon_users:
        error_message = "Only the intended account can use this coupon"

    # check for new user.
    if coupon_code.is_for_new_user:
        exist_user = User.query.filter_by(id=user_id).first()
        user_register_valid = exist_user.created >= discounts.valid_from and exist_user.created <= discounts.valid_to

        if exist_user and not user_register_valid:
            error_message = 'This coupon only valid for new user that register between ' \
                            '{} to {}'.format(datetime.strftime(discounts.valid_from, '%d %B %Y'),
                                              datetime.strftime(discounts.valid_to, '%d %B %Y'))

    # check validity of promotions?
    if not discounts.discount_valid() and discounts.valid_to < datetime.now():
        error_message = "Your coupon already expired at {}".format(
            datetime.strftime(discounts.valid_to, '%d %B %Y'))

    if not discounts.discount_valid() and discounts.valid_from > datetime.now():
        error_message = "This coupon can used start from : {}".format(
            datetime.strftime(discounts.valid_from, '%d %B %Y'))

    # to minimize query, selected from created of coupon!
    coupon_create = coupon_code.created - timedelta(days=2)
    coupon_used = OrderLineDiscount.query.filter(OrderLineDiscount.discount_code == coupon_code.code,
                                                 OrderLineDiscount.created >= coupon_create).all()

    order_statuses = [data.order.order_status for data in coupon_used]
    order_pending = [NEW, WAITING_FOR_PAYMENT, PAYMENT_IN_PROCESS]

    # only check for original coupon, coupon general must minimum > 10 uses
    if coupon_used and coupon_code.max_uses <= 10:
        for failed_status in order_pending:
            if failed_status in order_statuses:
                error_message = "This coupon code is already used. Please pay your pending transaction."
                break

    return error_message


def check_trial_promotions(user_id=None, offer_id=None):
    exist_orderline_trial = OrderLine.query.filter(OrderLine.user_id == user_id, OrderLine.offer_id == offer_id,
                                                   OrderLine.orderline_status == COMPLETE,
                                                   OrderLine.is_trial == True).first()
    return 'You already tried this package.' if exist_orderline_trial else None
