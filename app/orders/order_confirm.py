import json, httplib

from flask import Response

from app import app
from app.constants import *
from datetime import datetime

from app.helpers import err_response
from app.helpers import bad_request_message, internal_server_message
from app.helpers import generate_point_idr, generate_point_usd

from sqlalchemy.sql import desc

from app.orders.choices import COMPLETE
from app.orders.helpers import retrieve_promotions_confirm
from app.orders.models import Order, OrderLine, OrderLineDiscount, OrderDetail, OrderTemp, OrderLog
from app.orders.receipt_emails import matrix_not_founds, matrix_not_match, matrix_bad_request
from app.payments.choices.gateways import IAB, APPLE, PAYPAL, FREE

from app.items.models import Item
from app.payment_gateways.models import PaymentGateway

from app.tiers.models import TierMatrix

from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME, STATUS_MCGRAWHILL_CONTENT
from app.master.choices import ANDROID, GETSCOOP, IOS


class OrderConfirmConstruct():

    def __init__(self, args=None, method=None):
        self.args = args
        self.method = method

        self.user_id = args.get('user_id', None)
        self.payment_gateway = args.get('payment_gateway_id', None)
        self.currency = args.get('currency', None)
        self.final_price = args.get('final_amount', None)
        self.order_number = args.get('order_number', None)
        self.order_id = args.get('temp_order_id', None)
        self.client_id = args.get('client_id', None)
        self.orderlines = args.get('order_lines', None)
        self.coupon_list_used = {}

        self.order_temp = None
        self.master_order = None
        self.master_orderlines = []

        self.admin_email = app.config['ADMIN_EMAILS']
        self.operator_email = app.config['OPERATOR_EMAILS']
        self.monitoring_email = app.config['MONITORING_EMAILS']
        self.country_code = args.get('country_code', None)

        self.exist = False

    def check_payment_gateway(self):
        """
            CHECKING PAYMENT GATEWAY ID, VALID/NOT
        """
        try:
            payment = PaymentGateway.query.filter_by(id=self.payment_gateway).first()
            if not payment:
                return 'FAILED', bad_request_message(modul="order confirm 1",
                    method=self.method, param="payment_gateway",
                    not_found=self.payment_gateway, req=self.args)

            self.payment_gateway = payment
            self.currency = payment.get_currencies()['iso4217_code']
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="order confirm 1",
                method=self.method, error=e, req=str(self.args))

    def check_temp_order(self):
        """
            CHECKING ORDER TEMP FROM DATA REQUEST
        """
        try:
            order_temp = OrderTemp.query.filter_by(
                id=self.order_id,
                order_number=self.order_number,
                user_id=self.user_id,
                paymentgateway_id=self.payment_gateway.id).first()

            if not order_temp:
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    error_code=CLIENT_400101,
                    developer_message="Payment Invalid, Invalid Parameters Value",
                    user_message="Payment Invalid")

            self.order_temp = order_temp
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="order confirm 2",
                method=self.method, error=e, req=str(self.args))

    def check_order(self):
        try:
            order = Order.query.filter_by(
                temporder_id=self.order_id,
                order_number=self.order_number,
                user_id=self.user_id,
                paymentgateway_id=self.payment_gateway.id).first()

            if order:
                self.exist = True
                self.master_order = order

            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="order confirm 1",
                method=self.method, error=e, req=str(self.args))

    def post_data_order(self):
        """
            MOVE ORDER TEMP --> ORDER STATUS : 1 (NEW)
        """
        try:
            price_points = []
            data_orderline = self.order_temp.get_orderline_temp()

            if self.currency == 'USD':
                total_amount = self.order_temp.price_usd
                final_amount = self.order_temp.final_price_usd

                # special for offer buffets, no point rewards.
                for orderline in data_orderline:
                    offer = orderline.get_offer()
                    if offer.offer_type_id == 4:
                        pass
                    else:
                        price_points.append(orderline.final_price_usd)

                price_points = sum(price_points)
                point_reward = generate_point_usd(float(price_points))

            if self.currency == 'IDR':
                total_amount = self.order_temp.price_idr
                final_amount = self.order_temp.final_price_idr

                # special for offer buffets, no point rewards.
                for orderline in data_orderline:
                    offer = orderline.get_offer()
                    if offer.offer_type_id == 4:
                        pass
                    else:
                        price_points.append(orderline.final_price_idr)

                price_points = sum(price_points)
                point_reward = generate_point_idr(float(price_points))

            if self.currency == 'PTS':
                total_amount = self.order_temp.price_point
                final_amount = self.order_temp.final_price_point
                point_reward = 0

            # check if already FREE from temps
            if self.payment_gateway.id == FREE:
                total_amount = 0
                final_amount = 0
                point_reward = 0

            order = Order(
                order_number = self.order_temp.order_number,
                total_amount = total_amount,
                final_amount = final_amount,
                user_id = self.order_temp.user_id,
                client_id = self.order_temp.client_id,
                partner_id = self.order_temp.partner_id,
                order_status = 10001,
                is_active = self.order_temp.is_active,
                point_reward = point_reward,
                currency_code = self.currency,
                paymentgateway_id = self.payment_gateway.id,
                temporder_id = self.order_id,
                is_renewal = self.order_temp.is_renewal
            )

            if self.order_temp.tier_code:
                order.tier_code = self.order_temp.tier_code

            if self.order_temp.platform_id:
                order.platform_id = self.order_temp.platform_id

            sv = order.save()
            if sv.get('invalid', False):
                return internal_server_message(modul="order confirm 3", method=self.method,
                    error=sv.get('message', ''), req=str(self.args))

            self.master_order = order

            try:
                # update data log checkout
                log_order = OrderLog.query.filter_by(order_temp_id=self.order_id).first()
                if log_order:
                    log_order.order_id = order.id
                    log_order.save()

                # record confirm request from frontend
                confirm_order = OrderLog(
                    user_id = order.user_id,
                    order_temp_id = order.temporder_id,
                    platform_id = order.platform_id,
                    request = str(self.args),
                    order_id = order.id,
                    api = 'confirm'
                )
                confirm_order.save()

            except:
                pass

            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="order confirm 3",
                method=self.method, error=e, req=str(self.args))

    def post_data_orderline(self):
        """
            MOVE ORDERLINE TEMP --> ORDERLINE
        """

        try:
            line_temp = self.order_temp.get_orderline_temp()
            if line_temp:
                for data in line_temp:
                    if self.currency == 'USD':
                        final_price = data.final_price_usd
                        price = data.price_usd

                    if self.currency == 'IDR':
                        final_price = data.final_price_idr
                        price = data.price_idr

                    if self.currency == 'PTS':
                        final_price = data.final_price_point
                        price = data.price_point

                    # check if already FREE from temps
                    if self.payment_gateway.id == 3:
                        final_price = 0

                    orderline = OrderLine(
                        name=data.name,
                        offer_id=data.offer_id,
                        is_active=data.is_active,
                        is_free=data.is_free,
                        is_discount=data.is_discount,
                        is_trial=data.is_trial,
                        user_id=data.user_id,
                        campaign_id=data.campaign_id,
                        order_id=self.master_order.id,
                        quantity=data.quantity,
                        orderline_status=10001,
                        currency_code=self.currency,
                        final_price=final_price,
                        price=price
                    )

                    sv = orderline.save()
                    if sv.get('invalid', False):
                        return internal_server_message(modul="order confirm 4", method=self.method,
                            error=sv.get('message', ''), req=str(self.args))

                    if data.is_discount:
                        #move all data discount temp
                        dscont = self.post_order_discounts(orderline, data)
                        if dscont[0] != 'OK':
                            return 'FAILED', dscont[1]

                    # if data.is_trial:
                    #     exist_trial = OrderLine.query.filter(OrderLine.is_trial == True,
                    #         OrderLine.offer_id == data.offer_id, OrderLine.user_id == data.user_id,
                    #         OrderLine.orderline_status == COMPLETE).first()
                    #
                    #     if exist_trial:
                    #         return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    #             error_code=CLIENT_400101,
                    #             developer_message="You already tried this package.",
                    #             user_message="You already tried this package.")

                    self.master_orderlines.append(orderline)

            return 'OK', 'OK'
        except Exception, e:
            return 'FAILED', internal_server_message(modul="order confirm 4",
                method=self.method, error=e, req=str(self.args))

    def post_order_discounts(self, line_new=None, line_tmp=None):
        try:
            discount_tmp = line_tmp.get_orderline_discounts()
            for data in discount_tmp:
                if self.currency == 'USD':
                    final_price = data.final_price_usd
                    price = data.price_usd
                    discount_val = data.discount_usd

                if self.currency == 'IDR':
                    final_price = data.final_price_idr
                    price = data.price_idr
                    discount_val = data.discount_idr

                if self.currency == 'PTS':
                    final_price = data.final_price_point
                    price = data.price_point
                    discount_val = data.discount_point

                mo = OrderLineDiscount(
                    order_id=self.master_order.id,
                    orderline_id=line_new.id,
                    discount_id=data.discount_id,

                    discount_name=data.discount_name,
                    currency_code=self.currency,
                    discount_code=data.discount_code,
                    discount_type=data.discount_type,

                    discount_value=discount_val,
                    raw_price=price,
                    final_price=final_price
                )

                sv = mo.save()
                if sv.get('invalid', False):
                    return 'FAILED', internal_server_message(
                        modul="order confirm 5",
                        method=self.method, error=sv.get('message', ''),
                        req=str(self.args))

            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="order confirm 5",
                method=self.method, error=e, req=str(self.args))

    def post_data_details(self):
        try:
            detail = OrderDetail.query.filter_by(temporder_id=self.master_order.temporder_id).first()

            if detail:
                detail.order_id = self.master_order.id
                detail.save()
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="order confirm 5",
                method=self.method, error=e, req=str(self.args))

    def update_localize(self, orderline=None):
        for iloc in orderline:
            iloc.localized_currency_code = self.master_order.currency_code
            iloc.localized_final_price = iloc.final_price
            iloc.save()

    def get_localize_matrix(self, price=None, paymentgateway_id=None):

        # this is for case base currency IDR for IAB
        if paymentgateway_id == IAB:

            if self.master_order.currency_code == 'IDR':
                final_matrix = TierMatrix.query.filter(
                    TierMatrix.price_idr >= self.master_order.final_amount,
                    TierMatrix.paymentgateway_id == paymentgateway_id,
                    TierMatrix.valid_to >= datetime.now()
                ).order_by(TierMatrix.price_idr).first()
            else:
                final_matrix = TierMatrix.query.filter(
                    TierMatrix.tier_price == price,
                    TierMatrix.paymentgateway_id == paymentgateway_id,
                    TierMatrix.is_alternate == False,
                    TierMatrix.valid_to >= datetime.now()
                ).order_by(desc(TierMatrix.id)).first()

        if paymentgateway_id == APPLE:
            final_matrix = TierMatrix.query.filter(
                TierMatrix.tier_price == price,
                TierMatrix.paymentgateway_id == paymentgateway_id,
                TierMatrix.is_alternate == False,
                TierMatrix.valid_to >= datetime.now()
            ).order_by(desc(TierMatrix.id)).first()

        return final_matrix

    def post_localize_details(self):
        try:
            content, subject, admin_email = None, 'Confirm...', []

            # get orderlines
            orderline = self.master_order.get_order_line()

            if self.master_order.platform_id == GETSCOOP:
                # getscoop no tiers so all localized restore with normal values pack
                self.update_localize(orderline)
            else:
                if self.master_order.platform_id == ANDROID:
                    valid_payment_gateway = IAB

                if self.master_order.platform_id == IOS:
                    valid_payment_gateway = APPLE

                # check tier ios paymnet only
                if self.master_order.paymentgateway_id in (IAB, APPLE, PAYPAL):

                    # support new version ios/android, with orderlines request
                    if self.orderlines:
                        for iordline in self.orderlines:

                            localize_price = iordline.get('localized_final_price', None)
                            localize_currency = iordline.get('localized_currency_code', None)
                            offer_id = iordline.get('offer_id', None)
                            coupon = iordline.get('discount_code', None)
                            self.coupon_list_used.update({offer_id: coupon})

                            if (localize_price is None or localize_currency is None):
                                # send email to backend
                                content = matrix_bad_request() % (
                                    self.master_order.paymentgateway_id,
                                    self.args
                                )

                                subject = 'WARNING: (%s) Localize Confirm Bad Request Format' % (
                                    app.config['ADMIN_SERVER'])
                                admin_email = self.admin_email + self.monitoring_email

                            else:
                                data_matrix = self.get_localize_matrix(
                                    self.master_order.final_amount,
                                    valid_payment_gateway)

                                if not data_matrix:
                                    # send data email to backend
                                    content = matrix_not_founds() % (
                                        self.master_order.final_amount,
                                        self.master_order.paymentgateway_id,
                                        str(self.args)
                                    )

                                    subject = 'WARNING: (%s) Data Matrix %s Not Found' % (
                                        app.config['ADMIN_SERVER'], self.master_order.final_amount)
                                    admin_email = self.admin_email + self.monitoring_email

                                else:
                                    orderlines_data = OrderLine.query.filter_by(
                                        order_id = self.master_order.id,
                                        offer_id = int(offer_id)
                                    ).first()

                                    if orderlines_data:
                                        localize_currency = str(localize_currency).upper()

                                        # recheck if currency in our matrix exist? store frontend price
                                        rate_price = data_matrix.rate_price.get(localize_currency, None)
                                        if not rate_price:
                                            rate_price = localize_price
                                        else:
                                            # compare data matrix with localize price
                                            # this is gonna send an email to backend, in bottom..
                                            if rate_price != localize_price:
                                                rate_price = localize_price

                                        orderlines_data.localized_currency_code = localize_currency
                                        orderlines_data.localized_final_price = rate_price
                                        orderlines_data.save()

                                    # compare data frontend with our matrix db.
                                    # if not same per currency codes, send an email
                                    matrix_price = data_matrix.rate_price.get(localize_currency, 0)
                                    if matrix_price != localize_price:
                                        # send an email to backends
                                        orderlines_json = self.args.get('order_lines')[0]

                                        content = matrix_not_match() % (
                                            self.payment_gateway.name,
                                            self.payment_gateway.name,
                                            self.payment_gateway.name,
                                            orderlines_json.get('offer_id'),
                                            orderlines_json.get('name'),

                                            orderlines_data.final_price,
                                            orderlines_json.get('localized_final_price'),
                                            orderlines_json.get('localized_currency_code'),

                                            orderlines_data.final_price,
                                            matrix_price,
                                            localize_currency.upper(),

                                            self.payment_gateway.name,
                                            app.config['ADMIN_SERVER'],
                                            datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
                                        )

                                        subject = "WARNING: (%s) Client Localized Price Doesn't Matched with Backend %s Tier Matrix" % (
                                            app.config['ADMIN_SERVER'], self.payment_gateway.name)
                                        admin_email = self.admin_email + self.monitoring_email + self.operator_email

                    else:
                        # support old version ios/android, not send orderlines request,
                        # convert USD final price to default to localize IDR
                        for iloc in orderline:
                            data_matrix = self.get_localize_matrix(
                                iloc.final_price, valid_payment_gateway)

                            if not data_matrix:
                                # send data email to backend
                                content = matrix_not_founds() % (
                                    self.master_order.final_amount,
                                    valid_payment_gateway,
                                    self.args
                                )
                                subject = 'WARNING: (%s) Data Matrix %s Not Found' % (
                                    app.config['ADMIN_SERVER'], self.master_order.final_amount)
                                admin_email = self.admin_email + self.monitoring_email

                            else:
                                iloc.localized_currency_code = 'IDR'
                                iloc.localized_final_price = data_matrix.rate_price.get('IDR', 0)
                                iloc.save()
                else:
                    self.update_localize(orderline)
                    for iordline in self.orderlines:
                        offer_id = iordline.get('offer_id', None)
                        coupon = iordline.get('discount_code', None)
                        self.coupon_list_used.update({offer_id: coupon})
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', internal_server_message(modul="order confirm 5.1",
                method=self.method, error=e, req=str(self.args))

    def check_valid_restriction(self):
        for orderline in self.master_orderlines:
            status, item_name = orderline.offer.is_restricted_offers(self.country_code)
            if status:
                msg = "{} cannot be purchase because it is restricted from your current country.".format(item_name)
                return False, err_response(status=httplib.BAD_REQUEST, error_code=CLIENT_400101,
                                           developer_message=msg, user_message=msg)
        return True, 'OK'

    def get_master_item(self, item_id=None):
        """
            Get ITEM MASTER DATA
        """
        data_item = None

        # ITEM READY FOR CONSUME
        allowed_item_status = [STATUS_TYPES[STATUS_READY_FOR_CONSUME], STATUS_TYPES[STATUS_MCGRAWHILL_CONTENT]]
        item = Item.query.filter(Item.id == item_id, Item.item_status.in_(allowed_item_status)).first()

        if item:
            data_item = item
        return data_item

    def json_items(self, orderline=None):
        data_item = []

        offer = orderline.get_offer()
        if offer:
            if offer.offer_type_id in [2,4]:
                if offer.offer_type_id == 4:
                    # get only 1 brands, to prevent error because of too many brands in a buffet
                    brands = [offer.brands[0].id, ] if offer.brands[0] else []
                else:
                    brands = offer.get_brands_id()

                for ibrands in brands:
                    item = {}

                    item_brands = Item.query.filter_by(brand_id=ibrands, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME]).order_by(
                        desc(Item.release_date)).limit(1).all()

                    if item_brands:
                        ms_it = item_brands[0]
                        item['name'] = ms_it.name
                        item['edition_code'] = ms_it.edition_code
                        item['item_status'] = {v:k for k,v in STATUS_TYPES.items()}[ms_it.item_status]
                        item['release_date'] = ms_it.release_date.isoformat()
                        item['is_featured'] = ms_it.is_featured
                        item['vendor'] = ms_it.get_vendor()
                        item['languages'] = ms_it.get_languages()
                        item['countries'] = ms_it.get_country()
                        item['authors'] = ms_it.get_authors()
                        data_item.append(item)

                if offer.offer_type_id == 4:
                    data_item = data_item[0]
                    data_item['name'] = orderline.name
                    data_item = [data_item]

            else:
                for xo in offer.get_items():
                    item = {}
                    ms_it = self.get_master_item(xo)
                    item['name'] = ms_it.name
                    item['id'] = ms_it.id
                    item['edition_code'] = ms_it.edition_code
                    item['item_status'] = {v:k for k,v in STATUS_TYPES.items()}[ms_it.item_status]
                    item['release_date'] = ms_it.release_date.isoformat()
                    item['is_featured'] = ms_it.is_featured
                    item['vendor'] = ms_it.get_vendor()
                    item['languages'] = ms_it.get_languages()
                    item['countries'] = ms_it.get_country()
                    item['authors'] = ms_it.get_authors()
                    data_item.append(item)

        return data_item

    def json_orderlines(self):
        orderlines = self.master_order.get_order_line()

        order = []
        for dataline in orderlines:
            temp = {}
            temp['name'] = dataline.name
            temp['offer_name'] = dataline.offer.name
            temp['is_free'] = dataline.is_free
            temp['is_trial'] = dataline.is_trial
            temp['offer_id'] = dataline.offer_id
            temp['raw_price'] = '%.2f' % dataline.price
            temp['final_price'] = '%.2f' % dataline.final_price
            temp['currency_code'] = dataline.currency_code
            temp['discount_code'] = self.coupon_list_used.get(dataline.offer_id, None)

            if dataline.localized_final_price is not None:
                temp['localized_final_price'] = '%.2f' % dataline.localized_final_price
                temp['localized_currency_code'] = dataline.localized_currency_code
            else:
                temp['localized_final_price'] = None
                temp['localized_currency_code'] = None

            temp['items'] = self.json_items(dataline)
            order.append(temp)
        return order

    def json_success(self):
        temp = {}
        if self.exist:
            status = httplib.OK
        else:
            status = httplib.CREATED

        if self.master_order.platform_id:
            temp['platform_id'] = self.master_order.platform_id

        if self.master_order.tier_code:
            temp['tier_code'] = self.master_order.tier_code

        temp['order_id'] = self.master_order.id
        temp['temp_order_id'] = self.master_order.temporder_id
        temp['user_id'] = self.master_order.user_id
        temp['currency_code'] = self.master_order.currency_code
        temp['payment_gateway_id'] = self.master_order.paymentgateway_id
        temp['final_amount'] = str(self.master_order.final_amount)
        temp['total_amount'] = str(self.master_order.total_amount)
        temp['order_number'] = self.master_order.order_number
        temp['payment_gateway_name'] = self.payment_gateway.name
        temp['order_status'] = self.master_order.order_status
        temp['order_lines'] = self.json_orderlines()

        temp['is_active'] = self.master_order.is_active
        temp['client_id'] = self.master_order.client_id
        temp['point_reward'] = self.master_order.point_reward
        temp['partner_id'] = self.master_order.partner_id

        promo_sum, promo_detail = retrieve_promotions_confirm(order=self.master_order)
        temp['discount_details'] = promo_detail
        temp['discount_summary'] = promo_sum


        return Response(json.dumps(temp), status=status,
            mimetype='application/json')

    def construct(self):
        payment = self.check_payment_gateway()
        if payment[0] != 'OK':
            return payment[1]

        order = self.check_temp_order()
        if order[0] != 'OK':
            return order[1]

        self.check_order()

        if not self.exist:
            post_order = self.post_data_order()
            if post_order[0] != 'OK':
                return post_order[1]

            post_orderline = self.post_data_orderline()
            if post_orderline[0] != 'OK':
                return post_orderline[1]

            # Update order details.
            self.post_data_details()
            self.post_localize_details()

            # check geo_ip restrictions.
            valid_restriction_status, valid_restriction_data = self.check_valid_restriction()
            if not valid_restriction_status:
                return valid_restriction_data

        return self.json_success()
