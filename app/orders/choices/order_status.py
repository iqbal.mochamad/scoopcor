TEMP = 10000
NEW = 10001
WAITING_FOR_PAYMENT = 20001
PAYMENT_IN_PROCESS = 20002
PAYMENT_BILLED = 20003
WAITING_FOR_DELIVERY = 30001
DELIVERY_IN_PROCESS = 30002
DELIVERED = 30003
WAITING_FOR_REFUND = 40001
REFUND_IN_PROCESS = 40002
REFUNDED = 40003
CANCELLED = 50000
PAYMENT_ERROR = 50001
DELIVERY_ERROR = 50002
REFUND_ERROR = 50003
EXPIRED = 50004
COMPLETE = 90000

ORDER_STATUS_TYPES = (
    TEMP,
    NEW,
    WAITING_FOR_PAYMENT,
    PAYMENT_IN_PROCESS,
    PAYMENT_BILLED,
    WAITING_FOR_DELIVERY,
    DELIVERY_IN_PROCESS,
    DELIVERED,
    WAITING_FOR_REFUND,
    REFUND_IN_PROCESS,
    REFUNDED,
    CANCELLED,
    PAYMENT_ERROR,
    DELIVERY_ERROR,
    REFUND_ERROR,
    EXPIRED,
    COMPLETE,
)

ORDER_LINE_STATUS_TYPES = (
    TEMP,
    NEW,
    DELIVERED,
    REFUNDED,
    CANCELLED,
    DELIVERY_ERROR,
    REFUND_ERROR,
    EXPIRED,
    COMPLETE,
)
