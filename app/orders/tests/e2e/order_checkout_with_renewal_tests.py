from __future__ import unicode_literals, absolute_import

import json
from unittest import TestCase, SkipTest

from httplib import CREATED, OK

from datetime import datetime, timedelta
from nose.tools import istest, nottest

from app import app, db
from app.auth.helpers import jwt_encode_from_user
from app.items.choices import STATUS_TYPES, STATUS_READY_FOR_CONSUME
from app.items.models import UserSubscription
from app.master.choices import IOS, GETSCOOP
from app.offers.choices.offer_type import SUBSCRIPTIONS, BUFFET
from app.offers.models import Offer
from app.orders.choices import COMPLETE
from app.orders.models import OrderLineTemp, OrderLine, Order, OrderTemp
from app.payments.choices import CHARGED
from app.payments.choices.gateways import DIRECT_PAYMENT
from app.payments.signature_generator import GenerateSignature
from app.users.buffets import UserBuffet
from app.users.users import User
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.master import BrandFactory, CurrencyFactory, PlatformFactory
from tests.fixtures.sqlalchemy.offers import OfferTypeFactory, OfferFactory, OfferPlatformFactory, OfferBuffetFactory, \
    OfferSubscriptionFactory
from tests.fixtures.sqlalchemy.orders import PaymentGatewayFactory


@nottest
class OrderCheckoutToCompleteBase(TestCase):
    """ Base class to test Order with Auto Renew turned On (or Off, based on expect_is_renewal variable)
    test from: Order Checkout -> Order Confirm -> Payment (with direct payment here) -> Order Complete

    """
    db = db
    maxDiff = None

    client = app.test_client(use_cookies=False)

    session = db.session()

    expect_is_renewal = True

    @classmethod
    def setUpClass(cls):
        super(OrderCheckoutToCompleteBase, cls).setUpClass()
        mock_smtplib()

    @property
    def headers(self):
        with app.test_request_context('/'):
            return {
                'Accept': 'application/json',
                'Accept-Charset': 'utf-8',
                'Content-Type': 'application/json',
                'Authorization': 'JWT {}'.format(jwt_encode_from_user(self.user))
            }

    def _assert_order_checkout(self, json_body):
        response = self.client.post('/v1/orders/checkout',
                                    data=json.dumps(json_body),
                                    headers=self.headers)

        self.assertEqual(response.status_code, CREATED, response.data)
        response_checkout = json.loads(response.data)
        temp_order_id = response_checkout.get('temp_order_id', None)
        self._assert_is_renewal_checkout(temp_order_id)
        return response_checkout

    def _assert_is_renewal_checkout(self, temp_order_id):
        temp_order = self.session.query(OrderTemp).get(temp_order_id)
        self.assertIsNotNone(temp_order)
        self.assertEqual(self.expect_is_renewal, temp_order.is_renewal)

    def _assert_order_confirm(self, response_checkout):
        request_body = get_request_body_confirm(response_checkout)
        response = self.client.post('/v1/orders/confirm',
                                    data=json.dumps(request_body),
                                    headers=self.headers)
        self.assertEqual(response.status_code, CREATED, response.data)
        response_confirm = json.loads(response.data)
        order_id = response_confirm.get('order_id', None)
        self.assertIsNotNone(order_id)
        order = self.session.query(Order).get(order_id)
        self.assertIsNotNone(order)
        self.assertEqual(self.expect_is_renewal, order.is_renewal)

        return response_confirm

    def _assert_payment_and_order_complete(self, response_confirm):
        request_body = get_request_body_payment(response_confirm)
        response = self.client.post(
            '/v1/payments/charge',
            data=json.dumps(request_body),
            headers=self.headers)
        self.assertEqual(response.status_code, OK, response.data)
        response_payment = json.loads(response.data)
        self.assertIsNotNone(response_payment)
        order_id = response_payment.get('order_id', None)
        self.assertEqual(COMPLETE, response_payment.get('order_status', None))

        # check saved order data in database
        order = self.session.query(Order).get(order_id)
        self.assertEqual(COMPLETE, order.order_status)
        self.assertEqual(
            self.expect_is_renewal,
            order.is_renewal,
            'is_renewal Status in Order is not {}'.format(self.expect_is_renewal)
        )
        for line in order.order_lines.all():
            self.assertEqual(
                COMPLETE, line.orderline_status,
                'Order line status not completed. Line id: {0.id}, status: {0.orderline_status}'.format(
                    line)
            )
            self._assert_user_buffet_or_subscription(line)

    def _assert_user_buffet_or_subscription(self, line):
        if line.offer_id == BUFFET:
            user_buffet = self.session.query(UserBuffet).filter_by(
                user_id=self.user.id,
                orderline_id=line.id
            ).first()
            self.assertIsNotNone(user_buffet)
            # self.assertEqual(self.expect_is_renewal, user_buffet.is_renewal)
        elif line.offer_id == SUBSCRIPTIONS:
            user_subscription = self.session.query(UserSubscription).filter_by(
                user_id=self.user.id,
                orderline_id=line.id
            ).first()
            self.assertIsNotNone(user_subscription)
            # self.assertEqual(self.expect_is_renewal, user_subscription.is_renewal)


@istest
class OrderOfferSubscriptionTests(OrderCheckoutToCompleteBase):

    def test_post_order_IOS(self):
        user_id = SUPER_USER_ID
        self.user = self.session.query(User).get(user_id)
        offer = self.session.query(Offer).get(SUBSCRIPTIONS)
        json_body = get_request_body_checkout(user_id, offer, payment_gateway_id=DIRECT_PAYMENT, platform_id=IOS)
        self.session.close()

        response_checkout = self._assert_order_checkout(json_body)

        response_confirm = self._assert_order_confirm(response_checkout)

        self._assert_payment_and_order_complete(response_confirm)

    def test_post_order_web_getscoop(self):
        user_id = 200
        self.user = self.session.query(User).get(user_id)
        offer = self.session.query(Offer).get(SUBSCRIPTIONS)
        json_body = get_request_body_checkout(user_id, offer, payment_gateway_id=DIRECT_PAYMENT, platform_id=GETSCOOP)
        self.session.close()

        response_checkout = self._assert_order_checkout(json_body)

        response_confirm = self._assert_order_confirm(response_checkout)

        self._assert_payment_and_order_complete(response_confirm)


@istest
class OrderBuffetPremiumTests(OrderCheckoutToCompleteBase):

    def test_post_order_IOS(self):
        user_id = 300
        self.user = self.session.query(User).get(user_id)

        offer = self.session.query(Offer).get(BUFFET)
        json_body = get_request_body_checkout(user_id, offer, payment_gateway_id=DIRECT_PAYMENT, platform_id=IOS)
        self.session.close()

        response_checkout = self._assert_order_checkout(json_body)

        response_confirm = self._assert_order_confirm(response_checkout)

        self._assert_payment_and_order_complete(response_confirm)

    def test_post_order_web_getscoop(self):
        user_id = 400
        self.user = self.session.query(User).get(user_id)

        offer = self.session.query(Offer).get(BUFFET)
        json_body = get_request_body_checkout(user_id, offer, payment_gateway_id=DIRECT_PAYMENT, platform_id=GETSCOOP)
        self.session.close()

        response_checkout = self._assert_order_checkout(json_body)

        response_confirm = self._assert_order_confirm(response_checkout)

        self._assert_payment_and_order_complete(response_confirm)


@istest
class OrderBuffetNotAutoRenewTests(OrderCheckoutToCompleteBase):

    expect_is_renewal = False

    def test_post_order_IOS(self):
        user_id = 500
        self.user = self.session.query(User).get(user_id)

        offer = self.session.query(Offer).get(BUFFET)
        json_body = get_request_body_checkout(user_id, offer, payment_gateway_id=DIRECT_PAYMENT, platform_id=IOS,
                                              is_renewal=self.expect_is_renewal)
        self.session.close()

        response_checkout = self._assert_order_checkout(json_body)

        response_confirm = self._assert_order_confirm(response_checkout)

        self._assert_payment_and_order_complete(response_confirm)

    def test_post_order_web_getscoop(self):
        user_id = 600
        self.user = self.session.query(User).get(user_id)

        offer = self.session.query(Offer).get(BUFFET)
        json_body = get_request_body_checkout(user_id, offer, payment_gateway_id=DIRECT_PAYMENT, platform_id=GETSCOOP,
                                              is_renewal=self.expect_is_renewal)
        self.session.close()

        response_checkout = self._assert_order_checkout(json_body)

        response_confirm = self._assert_order_confirm(response_checkout)

        self._assert_payment_and_order_complete(response_confirm)


def get_request_body_checkout(user_id, offer, payment_gateway_id, platform_id, is_renewal=True):
    return {
        "os_version": "9.3",
        "order_lines": [
            {
                "offer_id": offer.id,
                "name": offer.long_name,
                "localized_currency_code": "IDR",
                "localized_final_price": str(offer.price_idr),
            }
        ],
        "is_renewal": is_renewal,
        "longitude": "106.8294",
        "user_country": "Indonesia",
        "latitude": "-6.1744",
        "ip_address": "120.164.44.125",
        "device_model": "x86_64",
        "client_version": "4.7.1.0",
        "client_id": "1",
        "user_id": user_id,
        "user_city": "Jakarta",
        "platform_id": platform_id,
        "payment_gateway_id": payment_gateway_id
    }


def get_request_body_confirm(response_checkout):
    return {
        "user_id": response_checkout.get('user_id', None),
        "payment_gateway_id": response_checkout.get('payment_gateway_id', None),
        "client_id": response_checkout.get('client_id', None),
        "currency_code": response_checkout.get('currency_code', None),
        "final_amount": response_checkout.get('final_amount', None),
        "order_number": response_checkout.get('order_number', None),
        "temp_order_id": response_checkout.get('temp_order_id', None),
    }


def get_request_body_payment(response_confirm):
    return {
        "order_id": response_confirm.get('order_id', None),
        "signature": GenerateSignature(order_id=response_confirm.get('order_id', None)).construct(),
        "user_id": response_confirm.get('user_id', None),
        "payment_gateway_id": response_confirm.get('payment_gateway_id', None)
    }


def create_init_data():
    session = db.session

    cur_idr = CurrencyFactory(id=2, iso4217_code='IDR')
    cur_pts = CurrencyFactory(id=3, iso4217_code='PTS')
    payment_gateway = PaymentGatewayFactory(
        id=DIRECT_PAYMENT, base_currency=cur_idr, payment_flow_type=CHARGED,
        minimal_amount=0.001)

    user = UserFactory(id=SUPER_USER_ID)
    user2 = UserFactory(id=200)
    user3 = UserFactory(id=300)
    user4 = UserFactory(id=400)
    user4 = UserFactory(id=500)
    user6 = UserFactory(id=600)

    offer_type_subs = OfferTypeFactory(id=SUBSCRIPTIONS)
    offer_type_buffet = OfferTypeFactory(id=BUFFET)
    offer_subscription = OfferFactory(id=SUBSCRIPTIONS, offer_type=offer_type_subs, is_free=False,
                                price_idr=10000, price_usd=10, price_point=10)
    offer_buffet_premium = OfferFactory(id=BUFFET, offer_type=offer_type_buffet, is_free=False,
                                        price_idr=10000, price_usd=10, price_point=10)
    platform_ios = PlatformFactory(id=IOS)
    platform_web = PlatformFactory(id=GETSCOOP)
    offer_subscription_platform_ios = OfferPlatformFactory(
        offer=offer_subscription, platform=platform_ios, price_idr=1000,
        discount_price_usd=1000)
    offer_buffer_platform_ios = OfferPlatformFactory(offer=offer_buffet_premium,
                                                     platform=platform_ios, price_idr=1000, discount_price_usd=1000)
    brand = BrandFactory()
    item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand,
                        languages=['ind'],
                        countries=['id'])
    offer_subscription.items.append(item1)
    offer_subscription.brands.append(brand)

    offer_buffet_premium.brands.append(brand)
    offer_buffet = OfferBuffetFactory(offer=offer_buffet_premium,
                                      available_from=datetime.now(),
                                      available_until=datetime.now() + timedelta(days=2))

    offer_subscription_detail = OfferSubscriptionFactory(offer=offer_subscription)

    session.commit()


# original_before_request_funcs = None


def setup_module():
    # make sure the test mode flag is set, or bail out
    if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
        raise SkipTest("COR is not in testing mode.  Cannot continue.")

    db.session.close_all()
    db.engine.dispose()
    db.drop_all()
    db.create_all()

    # global original_before_request_funcs
    # original_before_request_funcs = app.before_request_funcs
    # app.before_request_funcs = {
    #     None: [
    #         set_request_id,
    #         init_g_current_user,
    #     ]
    # }

    create_init_data()


def teardown_module():
    # app.before_request_funcs = original_before_request_funcs
    db.session.close_all()
    db.engine.dispose()
    db.drop_all()


SUPER_USER_ID = 430450


# def init_g_current_user():
#     session = db.session()
#     g.current_user = session.query(User).get(SUPER_USER_ID)
#     g.user = UserInfo(username=g.current_user.username, user_id=SUPER_USER_ID, token='abcd',
#                       perm=['can_read_write_global_all', ])
#     session.close()

