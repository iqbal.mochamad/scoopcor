from datetime import datetime
from flask import g

from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.offers.choices.offer_type import SINGLE
from app.offers.models import OfferType
from app.orders.choices import COMPLETE
from app.orders.models import Order
from app.orders.deliver_items import send_receipt_mail, OrderCompleteEmailViewmodel, get_eperpus_useremail
from app.payment_gateways.models import PaymentGateway
from app.payments.choices.gateways import POINT, WALLET
from app.utils.testcases import TestBase
from tests.fixtures.helpers import mock_smtplib
from tests.fixtures.sqlalchemy import ItemFactory
from app.users.tests.fixtures import UserFactory, OrganizationUserFactory, OrganizationFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory, OrderLineFactory

USER_ID = 12345


class SendEmail(TestBase):
    @classmethod
    def setUpClass(cls):
        super(SendEmail, cls).setUpClass()
        cls.init_data(cls.session)

    @classmethod
    def init_data(cls, s):
        pg_point = s.query(PaymentGateway).get(POINT)
        pg_wallet = s.query(PaymentGateway).get(WALLET)

        offer_type = s.query(OfferType).get(SINGLE)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        offer2 = OfferFactory(id=2, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                              price_point=10)
        brand = BrandFactory()
        cls.item_id = 1
        item1 = ItemFactory(id=cls.item_id, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand,
                            thumb_image_normal='images/1/5574/general_small_covers/ID_GAB2014MTH09ED09_S.jpg')
        cls.offer1.items.append(item1)
        item2 = ItemFactory(id=21233, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand,
                            thumb_image_normal='images/1/36281/general_small_covers/ID_PSB2017MTH03DBE_S_.jpg')
        offer2.items.append(item2)

        cls.user = UserFactory(id=USER_ID, username='cortesting@apps-foundry.com', email='cortesting@apps-foundry.com')
        cls.user2 = UserFactory(username='cortesting_test@apps-foundry.com', email='cortesting_test@apps-foundry.com')

        # generate unique order id to prevent error from veritrans in end to end test
        # to prevent error 406 already exists from veritrans
        cls.order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())

        order = OrderFactory(id=cls.order_id, paymentgateway=pg_point,
                             total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                             party=cls.user,
                             order_status=COMPLETE, currency_code=pg_point.base_currency.iso4217_code)
        order_line = OrderLineFactory(order=order, offer=cls.offer1, user_id=USER_ID,
                                      currency_code=pg_point.base_currency.iso4217_code,
                                      price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                      orderline_status=COMPLETE)
        order_line2 = OrderLineFactory(order=order, offer=offer2, user_id=USER_ID,
                                       currency_code=pg_point.base_currency.iso4217_code,
                                       price=offer2.price_idr, final_price=offer2.price_idr,
                                       orderline_status=COMPLETE)

        # eperpus
        cls.organization = OrganizationFactory()
        s.commit()

        cls.user_org1 = OrganizationUserFactory(user_id=cls.user.id, organization_id=cls.organization.id,
                                                is_manager=True)
        cls.user_org2 = OrganizationUserFactory(user_id=cls.user2.id, organization_id=cls.organization.id,
                                                is_manager=True)

        cls.order_eperpus = OrderFactory(paymentgateway=pg_wallet,
                                         total_amount=cls.offer1.price_idr, final_amount=cls.offer1.price_idr,
                                         party=cls.organization,
                                         order_status=COMPLETE, currency_code=pg_wallet.base_currency.iso4217_code)

        order_line_eperpus = OrderLineFactory(order=cls.order_eperpus, offer=cls.offer1, user_id=cls.organization.id,
                                              currency_code=pg_wallet.base_currency.iso4217_code,
                                              price=cls.offer1.price_idr, final_price=cls.offer1.price_idr,
                                              orderline_status=COMPLETE)

        s.commit()

    def test_get_eperpus_useremail(self):
        with self.on_session(self.organization, self.user, self.user2):
            actual = get_eperpus_useremail(self.organization.id, self.user.email)
            self.assertEqual(actual, [self.user2.email, ])

    def test_send_receipt_email(self):
        customer_email = 'fake.user@fakemail.com'
        order = self.session.query(Order).get(self.order_id)

        # # comment out this mock_stmplib if u want to test sending email to mailtrap.io
        # # or other smtp configured in config.py/local_config.py
        # from tests.fixtures.helpers import mock_smtplib
        # mock_smtplib()

        actual = send_receipt_mail(order, customer_email)
        # check the email in ur mailtrap.io account (set on local_config.py)
        self.assertTrue(actual)

    def test_send_receipt_email_eperpus(self):
        with self.on_session(self.order_eperpus, self.user):
            g.current_user = self.user

            actual = send_receipt_mail(self.order_eperpus, None)
            g.current_user = None

            # check the email in ur mailtrap.io account (set on local_config.py)
            self.assertTrue(actual)

    def test_format_currency(self):
        order = Order.query.get(self.order_id)
        ocemail = OrderCompleteEmailViewmodel('SCOOP (iOS)', order, 'fake@email.com')
        actual = ocemail.format_currency('IDR', 112312321.2345)
        self.assertEqual('Rp 112.312.321', actual)

        actual = ocemail.format_currency('PTS', 1234567.343)
        self.assertEqual('1.234.567 Pts', actual)

        actual = ocemail.format_currency('USD', 1234567.343)
        self.assertEqual('US$ 1,234,567.34', actual)
