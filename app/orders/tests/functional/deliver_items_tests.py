from datetime import timedelta, datetime

from app import db
from app.items.models import Item, UserSubscription, UserItem
from app.master.choices import IOS
from app.master.models import Platform
from app.offers.models import Offer, OfferSubscription
from app.offers.models import OfferType
from app.orders.deliver_items import get_items_from_subscription, get_backward_start_date, get_subs_end_date, \
    get_new_release_items, get_latest_item, deliver_user_buffet, deliver_user_subscription, deliver_owned_items
from app.users.tests.fixtures import UserFactory
from app.utils.datetimes import get_local, get_local_nieve
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferSubscriptionFactory, OfferPlatformFactory, \
    OfferBuffetFactory
from tests.fixtures.sqlalchemy.orders import OrderLineFactory, OrderFactory


class DeliverItemsTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(DeliverItemsTests, cls).setUpClass()
        session = db.session()

        # Offer Subscriptions data
        cls.brand_sub_1 = BrandFactory()

        # create item backward
        item_sub_31 = ItemFactory(
            id=31,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=cls.brand_sub_1,
            release_date=get_local() - timedelta(days=100)
        )
        cls.item_sub_32 = ItemFactory(
            id=32,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=cls.brand_sub_1,
            release_date=get_local() - timedelta(days=15)
        )
        cls.item_sub_33 = ItemFactory(
            id=33,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=cls.brand_sub_1,
            release_date=get_local() - timedelta(days=10)
        )

        # create normal item
        cls.item_sub_100 = ItemFactory(
            id=100,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=cls.brand_sub_1,
            release_date=get_local_nieve()
        )
        cls.item_sub_101 = ItemFactory(
            id=101,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=cls.brand_sub_1,
            release_date=get_local_nieve() + timedelta(days=10)
        )
        item_sub_102 = ItemFactory(
            id=102,
            item_status=Item.Statuses.ready_for_consume.value,
            brand=cls.brand_sub_1,
            release_date=get_local_nieve() + timedelta(days=100)
        )
        offer_type_sub = session.query(OfferType).get(Offer.Type.subscription.value)
        cls.offer_sub_1 = OfferFactory(offer_type=offer_type_sub)
        session.commit()
        cls.offer_sub_1.brands.append(cls.brand_sub_1)
        cls.offer_subscription_1 = OfferSubscriptionFactory(
            offer=cls.offer_sub_1,
            quantity_unit=1,
            quantity=2,

        )
        platform = session.query(Platform).get(IOS)
        cls.offer_platform_sub_1 = OfferPlatformFactory(
            offer=cls.offer_sub_1,
            platform=platform,
            tier_code='.ARS.ID_11A22CCDFDF'
        )
        session.commit()

        cls.purchase_date = get_local()

    def test_sub_unit_per_edition(self):
        self.offer_subscription_1.quantity_unit = OfferSubscription.QuantityUnit.unit.value
        self.offer_subscription_1.quantity = 2
        self.offer_subscription_1.allow_backward = False
        self.session.add(self.offer_subscription_1)
        self.session.commit()
        with self.on_session(self.brand_sub_1, self.offer_sub_1, self.item_sub_100, self.item_sub_101):
            actual = get_items_from_subscription(
                session=self.session,
                offer_subscription=self.offer_subscription_1,
                base_purchase_date=self.purchase_date,
                brand_id=self.brand_sub_1.id,
                items=[]
            )
            expected = [self.item_sub_100, self.item_sub_101, ]
            self.assertEqual(len(actual), len(expected))
            self.assertItemsEqual(actual, expected)

    def test_sub_unit_per_edition_with_backward(self):
        self.offer_subscription_1.quantity_unit = OfferSubscription.QuantityUnit.unit.value
        self.offer_subscription_1.quantity = 2
        self.offer_subscription_1.allow_backward = True
        self.offer_subscription_1.backward_quantity = 1
        self.session.add(self.offer_subscription_1)
        self.session.commit()
        with self.on_session(
            self.brand_sub_1,
            self.offer_sub_1, self.item_sub_100, self.item_sub_101,
            self.item_sub_32, self.item_sub_33
        ):
            actual = get_items_from_subscription(
                session=self.session,
                offer_subscription=self.offer_subscription_1,
                base_purchase_date=self.purchase_date,
                brand_id=self.brand_sub_1.id,
                items=[]
            )
            expected = [self.item_sub_100, self.item_sub_101, self.item_sub_33, self.item_sub_32, ]
            self.assertEqual(len(actual), len(expected))
            self.assertItemsEqual(actual, expected)

    def test_sub_unit_per_month(self):
        self.offer_subscription_1.quantity_unit = OfferSubscription.QuantityUnit.month.value
        self.offer_subscription_1.quantity = 2
        self.offer_subscription_1.allow_backward = False
        self.session.add(self.offer_subscription_1)
        with self.on_session(
            self.brand_sub_1, self.offer_sub_1, self.item_sub_100, self.item_sub_101
        ):
            actual = get_items_from_subscription(
                session=self.session,
                offer_subscription=self.offer_subscription_1,
                base_purchase_date=self.purchase_date,
                brand_id=self.brand_sub_1.id,
                items=[]
            )
            expected = [self.item_sub_100, self.item_sub_101, ]
            self.assertEqual(len(actual), len(expected))
            self.assertItemsEqual(actual, expected)

    def test_sub_unit_per_month_with_backward(self):
        self.offer_subscription_1.quantity_unit = OfferSubscription.QuantityUnit.month.value
        self.offer_subscription_1.quantity = 2
        self.offer_subscription_1.allow_backward = True
        self.offer_subscription_1.backward_quantity = 1
        self.session.add(self.offer_subscription_1)
        self.session.commit()
        with self.on_session(
            self.brand_sub_1, self.offer_sub_1, self.item_sub_100, self.item_sub_101,
            self.item_sub_32, self.item_sub_33
        ):
            actual = get_items_from_subscription(
                session=self.session,
                offer_subscription=self.offer_subscription_1,
                base_purchase_date=self.purchase_date,
                brand_id=self.brand_sub_1.id,
                items=[]
            )
            expected = [self.item_sub_100, self.item_sub_101, self.item_sub_33, self.item_sub_32, ]
            self.assertEqual(len(actual), len(expected))
            self.assertItemsEqual(actual, expected)

    def test_get_backward_start_date(self):
        base_purchase_date = datetime(2016, 10, 10)
        actual = get_backward_start_date(base_purchase_date, 2, OfferSubscription.QuantityUnit.day)
        self.assertEqual(actual, datetime(2016, 10, 8))

        actual = get_backward_start_date(base_purchase_date, 1, OfferSubscription.QuantityUnit.week)
        self.assertEqual(actual, datetime(2016, 10, 3))

        actual = get_backward_start_date(base_purchase_date, 2, OfferSubscription.QuantityUnit.month)
        self.assertEqual(actual, datetime(2016, 8, 10))

    def test_get_subs_end_date(self):
        base_purchase_date = datetime(2016, 10, 10)
        actual = get_subs_end_date(base_purchase_date, 2, OfferSubscription.QuantityUnit.day)
        self.assertEqual(actual, datetime(2016, 10, 12))

        actual = get_subs_end_date(base_purchase_date, 1, OfferSubscription.QuantityUnit.week)
        self.assertEqual(actual, datetime(2016, 10, 17))

        actual = get_subs_end_date(base_purchase_date, 2, OfferSubscription.QuantityUnit.month)
        self.assertEqual(actual, datetime(2016, 12, 10))

    def test_get_new_released_items(self):
        with self.on_session(
            self.item_sub_100, self.item_sub_101,
            self.item_sub_32, self.item_sub_33
        ):
            items = [self.item_sub_100, self.item_sub_101,
                     self.item_sub_32, self.item_sub_33, ]
            base_date = get_local_nieve().replace(hour=0, minute=0, second=0, microsecond=0)
            new_release_items = get_new_release_items(items, base_date)
            self.assertItemsEqual(new_release_items, [self.item_sub_100, self.item_sub_101, ])

    def test_get_latest_item(self):
        with self.on_session(
            self.item_sub_100, self.item_sub_101,
            self.item_sub_32, self.item_sub_33
        ):
            items = [self.item_sub_100, self.item_sub_101,
                     self.item_sub_32, self.item_sub_33, ]
            latest_item = get_latest_item(items)
            self.assertEqual(latest_item.id, self.item_sub_101.id)

    def test_send_receipt_mail(self):
        """ test already exists in test_send_receipt_email.py

        :return:
        """
        pass

    def test_deliver_user_buffet(self):
        offer_buffet = OfferBuffetFactory()
        order = OrderFactory(party=self.super_admin)
        order_line = OrderLineFactory(offer=offer_buffet.offer)
        self.session.commit()
        expires_date = get_local_nieve() + timedelta(days=30)
        actual = deliver_user_buffet(
            user_id=self.super_admin.id,
            order_line_id=order_line.id,
            offer=offer_buffet.offer,
            expires_date=expires_date,
            is_trial=False,
            original_transaction_id="1123123123",
            session=self.session
        )
        self.assertEqual(actual.offerbuffet, offer_buffet)
        self.assertEqual(actual.valid_to, expires_date)
        self.assertEqual(actual.user_id, self.super_admin.id)

    def test_deliver_user_subscription(self):
        # prepare data
        self.offer_subscription_1.quantity_unit = OfferSubscription.QuantityUnit.month.value
        self.offer_subscription_1.quantity = 1
        self.offer_subscription_1.allow_backward = False
        self.offer_subscription_1.backward_quantity = 0
        self.session.add(self.offer_subscription_1)
        self.session.commit()
        self.session.expunge_all()
        with self.on_session(
            self.brand_sub_1, self.offer_sub_1, self.offer_subscription_1,
            self.item_sub_100, self.item_sub_101,
            self.item_sub_32, self.item_sub_33
        ):
            self.session.add(self.offer_sub_1)
            order = OrderFactory(party=self.regular_user)
            order_line = OrderLineFactory(offer=self.offer_sub_1, order=order)
            self.session.commit()

            # test deliver_user_subscription
            deliver_user_subscription(
                order=order,
                order_line=order_line,
                offer=self.offer_sub_1,
                purchase_date=get_local_nieve(),
                session=self.session
            )

            saved_user_subs = self.session.query(UserSubscription).filter_by(
                user_id=self.regular_user.id,
                orderline_id=order_line.id
            ).first()
            self.assertIsNotNone(saved_user_subs)
            self.assertEqual(saved_user_subs.user_id, self.regular_user.id)
            self.assertEqual(saved_user_subs.quantity, self.offer_subscription_1.quantity)
            self.assertEqual(saved_user_subs.brand_id, self.brand_sub_1.id)

            saved_user_items = self.session.query(UserItem).filter_by(
                user_id=self.regular_user.id,
                orderline_id=order_line.id
            ).all()
            self.assertIsNotNone(saved_user_items)
            expected_item_ids = [self.item_sub_100.id, self.item_sub_101.id]
            actual_item_ids = [user_item.item.id for user_item in saved_user_items]
            self.assertItemsEqual(expected_item_ids, actual_item_ids)

    def test_deliver_owned_items(self):
        user = UserFactory(id=9911)
        self.session.commit()
        with self.on_session(user, self.item_sub_32, self.item_sub_33):
            items = [self.item_sub_32, self.item_sub_33, ]
            deliver_owned_items(
                items=items,
                user_id=user.id,
                session=self.session,
                order_line=None,
                is_restore=False,
            )
            saved_user_items = self.session.query(UserItem).filter_by(
                user_id=user.id,
            ).all()
            self.assertIsNotNone(saved_user_items)
            expected_item_ids = [item.id for item in items]
            actual_item_ids = [user_item.item.id for user_item in saved_user_items]
            self.assertItemsEqual(expected_item_ids, actual_item_ids)
