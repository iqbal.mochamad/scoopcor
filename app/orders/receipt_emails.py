def content_receipt_emails():

    content = """
        <div style="font-family: Arial; padding: 10px;">
            <table style="width: 100%;" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                        <img src="https://ebooks.gramedia.com/css/images/logo-gramedia-digital.png">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 20px; color: #3D3D3D; line-height: 150%;">
                        <strong>Your Order Confirmation</strong>
                        <div style="margin-top: 10px; padding-top: 10px; border-top: 1pt solid #c7c9c9">
                            <strong>Thank you for purchasing from <span style="color: #1498d8">%s</span></strong><br/>
                            Order Number : %s<br/>
                            Date: %s <br/>
                            Payment: %s
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 20px; color: #3D3D3D; border-collapse: collapse; line-height: 150%;">
                        <strong>Order Details</strong>
                        <table style="width: 100%; margin-top: 5px; border-bottom: 2pt solid #c7c9c9" cellspacing="0">
                            <tr style="background-color: #1498d8;">
                                <td></td>
                                <td style="padding: 10px; color: #FFF;">ITEM</td>
                                <td style="padding: 10px; color: #FFF; text-align: right; width: 120px;">PRICE</td>
                            </tr>
                            <!-- loop item start -->

                            %s

                            <!-- loop item end -->
                            <tr>
                                <td style="padding: 10px 0; border-top: 1pt solid #c7c9c9; text-align: right;" colspan="2">Order Total</td>
                                <td align="right" style="padding: 10px; border-top: 1pt solid #c7c9c9;"><b> %s </b></td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 0; text-align: right;" colspan="2">Discount</td>
                                <td align="right" style="padding: 10px;"><b> %s </b></td>
                            </tr>
                            <tr>
                                <td style="padding: 10px 0; border-top: 1pt solid #c7c9c9; text-align: right;" colspan="2">Grand Total</td>
                                <td align="right" style="padding: 10px; border-top: 1pt solid #c7c9c9;"><b> %s </b></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 40px; color: #3D3D3D; line-height: 150%;">
                        <strong>Need Help?</strong>
                        <div style="margin-top: 10px; padding-top: 10px; border-top: 1pt solid #c7c9c9; font-size: 14px;">
                            Tell us your problem at email customer care<br/>
                            <a href="mailto:customercare@gramedia.com" style="color: #1498d8; text-decoration: none;">customercare@gramedia.com</a><br/>
                            or by phone at <span style="color: #1498d8;">(021) 29675836 / 37 </span><br/><br/>
                            or check our <a href="http://ebooks.gramedia.com/id/faq" style="color: #1498d8; text-decoration: none;">FAQ page</a>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-top: 40px; color: #3D3D3D;">
                    </td>
                </tr>
            </table>
        </div>
    """

    return content

def matrix_not_founds():

    content = """
        Dear Admin, <br><br>

        Matrix Not Founds in Our database <br>
        please recheck <br><br>

        Price : %s <br>
        Paymentgateway_id : %s <br><br>
        Request : %s <br><br>

        Regards, <br>
        Backend team - SALAM KOMPAK!! <br>
        MAY FORCE BE WITH YOU <br>
    """

    return content

def matrix_not_match():

    content = """
        Dear Admin, <br><br>

        Client Localized Price Doesn't Matched with Backend %s Tier Matrix. <br><br>

        If below cases occured: <br><br>
        - Incorrect pricing between Backend's offer or tier price with %s product price. Please proceed to fix the data <br>
        - Invalid Backend stored %s matrix records, if matrix updated. Please proceed to update the Backend stored matrix <br>
        - Invalid or fraud request from client side. Please proceed to mitigate the fraud <br><br>

        Offer ID: %s <br>
        Offer Name: %s <br><br>

        Client Base Price : %s <br>
        Client Localized Price: %s <br>
        Client Localized Currency: %s <br><br>

        Backend Base Price : %s <br>
        Backend Localized Price: %s <br>
        Backend Localized Currency: %s <br><br>

        Payment Name: %s <br>
        Server: %s <br>
        Server Timestamp: %s <br><br>

        Regards, <br>
        Backend team - SALAM KOMPAK!! <br>
        MAY FORCE BE WITH YOU <br>
    """

    return content

def matrix_bad_request():

    content = """
        Dear Admin, <br><br>

        Frontend request for localize Invalid <br>
        Missing localized_final_price or localized_currency_code <br>

        please inform... <br><br>

        Payment gateway : %s <br>
        Frontend request for confirm : <br><br>

        %s <br><br>

        Regards, <br>
        Backend team - SALAM KOMPAK!! <br>
        MAY FORCE BE WITH YOU <br>
    """

    return content
