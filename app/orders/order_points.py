from __future__ import unicode_literals
import httplib

from datetime import datetime

from app import app, db
from app.constants import *

from app.helpers import err_response
from app.helpers import payment_logs
from app.orders.models import Order
from app.orders.models import OrderLogDispatch

from app.points.models import PointUser, PointUse
from app.users.users import User


class PointsPayment(object):

    def __init__(self, user_id=None, payment_gateway_id=None, order_id=None, order_number=None, args=None):

        self.user_id = user_id
        self.payment_gateway_id = payment_gateway_id
        self.order_id = order_id
        self.order_number = order_number

        #self.args = {'user_id': user_id, 'payment_id': payment_gateway_id,
        #    'order_id': order_id, 'order_number': order_number}
        self.args = args
        self.method = 'POST'
        self.master_order = None
        self.PAYMENT_IN_PROCESS = 20002

    def invalid_order_status(self, msg=None):
        logs = OrderLogDispatch(
            user_id = self.user_id,
            order_id = self.order_id,
            order_number = self.order_number,
            error_status = msg,
            data_order = str(self.args)
        )
        db.session.add(logs)
        db.session.commit()

    def check_order(self):
        try:
            order = Order.query.filter_by(
                id=self.order_id,
                order_number=self.order_number,
                user_id=self.user_id,
                paymentgateway_id=self.payment_gateway_id).first()

            if not order:
                return 'FAILED', payment_logs(args=self.args, error_message="Order Not Founds")

            if order.order_status != self.PAYMENT_IN_PROCESS:
                return 'FAILED', payment_logs(args=self.args, error_message="Order Status Invalid")

            self.master_order = order
            return 'OK', 'OK'

        except Exception, e:
            return 'FAILED', payment_logs(args=self.args, error_message=str(e))

    def check_points(self):
        try:
            user = User.query.get(self.master_order.user_id)
            if not user.is_verified:
                self.invalid_order_status("User have not been verified.")
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    error_code=CLIENT_400101,
                    developer_message="Payment Invalid, User have not been verified.",
                    user_message="Email has not been verified, please kindly verify it before use GD Point."
                )

            user_pts = PointUser.query.filter_by(user_id=self.master_order.user_id).first()
            if user_pts:
                final_price = int(self.master_order.final_amount)

                if final_price <= int(user_pts.point_amount):

                    #depleted user points
                    user_pts.point_amount = (user_pts.point_amount - final_price)
                    user_pts.point_used = (user_pts.point_used + final_price)
                    user_pts.save()

                    #record points use
                    pts_used = PointUse(
                        client_id = self.master_order.client_id,
                        user_id = self.master_order.user_id,
                        use_datetime = datetime.now(),
                        amount = final_price,
                        is_active = True,
                        order_id = self.master_order.id
                    )
                    pts_used.save()

                    #pts_details = PointUsedDetail(
                    #    pointuse_id = pts_used.id,
                    #    detail = 'PAYMENT COMPLETE for order_id : %s' % self.master_order.id
                    #)
                    #pts_details.save()

                    return 'OK', 'OK'

                else:
                    self.invalid_order_status("Not enough points")
                    return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                        error_code=CLIENT_400101,
                        developer_message="Payment Invalid, Points not enough",
                        user_message="Payment Invalid")
            else:
                self.invalid_order_status("Not enough points")
                return 'FAILED', err_response(status=httplib.BAD_REQUEST,
                    error_code=CLIENT_400101,
                    developer_message="Payment Invalid, Points not enough",
                    user_message="Payment Invalid")

        except Exception, e:
            return 'FAILED', payment_logs(args=self.args, error_message=str(e))

    def construct(self):
        chck_order = self.check_order()
        if chck_order[0] != 'OK':
            return chck_order

        chck_points = self.check_points()
        return chck_points
