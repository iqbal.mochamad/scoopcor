from __future__ import unicode_literals

from flask_appsfoundry.exceptions import ScoopApiException, Conflict
from httplib import UNPROCESSABLE_ENTITY, CONFLICT


class UserNotFoundException(Exception):
    """ Special purpose exception for the Remote Checkout API.

    Raised whenever the user specified in the remote checkout request body
    (currently only by e-mail address from elevenia), does not exist in CAS.
    """
    pass


class DuplicateRemoteOrderException(Conflict):
    def __init__(self, remote_order_number):
        self.remote_order_number = remote_order_number

        super(DuplicateRemoteOrderException, self).__init__(
            code=CONFLICT,
            error_code=CONFLICT,
            user_message="Duplicate Remote Order Number for : {}".format(remote_order_number),
            developer_message=str(self),
        )

    def __str__(self):
        return ("<DuplicateRemoteOrderException("
                    "remote_order_number={0.remote_order_number})>"
                ).format(self)


class PaymentBelowGatewayMinimumError(ScoopApiException):
    """ Raised when the payment for an Order is insufficient for the
    payment gateway's minimum_amount.
    """
    def __init__(self, order_amount, payment_gateway):
        """
        :param `decimal.Decimal` order_amount: The total amount that's
            being charged to the payment gateway.
        :param `app.payment_gateways.models.PaymentGateway` payment_gateway:
        """
        self.order_amount = order_amount
        self.payment_gateway = payment_gateway

        super(PaymentBelowGatewayMinimumError, self).__init__(
            code=UNPROCESSABLE_ENTITY,
            error_code=UNPROCESSABLE_ENTITY,
            user_message="Sorry, your selected payment method requires a "
                         "minimum purchase of: {0.minimal_amount} "
                         "{0.base_currency.iso4217_code}".format(payment_gateway),
            developer_message=str(self),
        )

    def __str__(self):
        return ("<PaymentBelowGatewayMinimumError("
                    "order_amount={0.order_amount}, "
                    "gateway_id={0.payment_gateway.id}, "
                    "minimal_amount={0.payment_gateway.minimal_amount})>"
                ).format(self)

