from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib

from app import app


def sendmail_smtp(subject=None, addr_from=None, addr_to=None, msg_string=None):
    """
        Send email using by localhost smtp,
        secondary send email if sendmail_aws failed
    """
    smtp = smtplib.SMTP('localhost')
    try:
        msg = MIMEMultipart()
        msg['Subject'] = subject
        msg.attach(MIMEText(msg_string))

        smtp.sendmail(addr_from, addr_to, msg.as_string())
    except smtplib.SMTPException:
        app.logger.exception("Couldn't send the e-mail.")
    finally:
        smtp.close()


def sendmail_aws(subject=None, addr_from=None, addr_to=None, msg_string=None, msg_text=None):
    smtp_server = app.config['EMAIL_HOST']
    smtp_username = app.config['EMAIL_USER']
    smtp_password = app.config['EMAIL_PASS']
    smtp_port = app.config['EMAIL_PORT']
    smtp_do_tls = app.config['EMAIL_USE_TLS']

    server = smtplib.SMTP(smtp_server, smtp_port)

    if smtp_do_tls:
        server.starttls()

    server.ehlo()
    if app.config['EMAIL_LOGIN']:
        server.login(smtp_username, smtp_password)

    # Construct email
    msg = MIMEMultipart('alternative')
    #msg['To'] = addr_to
    msg['To'] = ', '.join(addr_to)
    msg['From'] = addr_from
    msg['Subject'] = subject

    # Create the body of the message (a plain-text and an HTML version).
    text = msg_text
    html = msg_string

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Send the message via an SMTP server
    server.sendmail(addr_from, addr_to, msg.as_string())
    server.quit()

