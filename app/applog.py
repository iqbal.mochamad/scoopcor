from __future__ import unicode_literals

import logging
from logging.config import dictConfig
from sys import exc_info

from flask import g, request
from werkzeug.exceptions import Unauthorized


class ScoopLogFormatFilter(logging.Filter):
    def filter(self, record):
        try:
            record.username = g.user.username
        except (RuntimeError, AttributeError):
            record.username = ""

        try:
            record.request_info = "[{0.method}] {0.url}".format(request)
        except Exception:
            record.request_info = ""

        try:
            record.request_id = g.request_id
        except RuntimeError:
            record.request_id = ""
        except AttributeError:
            record.request_id = ""
        return True


class AuthFailureFilter(logging.Filter):
    """ Make sure auth failures aren't logged in our app log.
    """

    def filter(self, record):
        try:
            if any(exc_info()):
                exception_class = exc_info()[0]
                return 0 if issubclass(exception_class, Unauthorized) else True
            else:
                return True
        except RuntimeError:
            return True


def initialize_logging(flask_app):
    flask_app.logger.debug("I'm a hack -- ignore me")
    dictConfig(flask_app.config['LOGGING_CONFIG'])
    flask_app.logger.warning("Application Started")


def initialize_package_logger(package_name):
    logger = logging.getLogger(package_name.replace('app.', 'scoopcor.'))
    return logger


class RequestDataFilter(logging.Filter):

    def filter(self, record):
        if hasattr(request, 'json'):
            data = request.json
            if data is not None and 'password' in data:
                del data['password']
            request_data = data
            record.request_data = request_data
        if hasattr(request, 'method'):
            record.method = request.method
        if hasattr(request, 'path'):
            record.request_path = request.path
        if hasattr(request, 'user_agent'):
            record.user_agent = request.user_agent.string

        return True
