from __future__ import unicode_literals, absolute_import, print_function

from datetime import datetime

from sqlalchemy import select, desc

from app.items.choices import SubscriptionType
from app.items.models import UserSubscription


def get_user_ids_with_subscriptions(session):
    """ Returns the user_id of every user ever with a subscription.

    :param `sqlalchemy.orm.Session` session:
    :return:
    :rtype: sqlalchemy.engine.result.ResultProxy
    """
    return session.bind.execute(select([UserSubscription.user_id])
                                .distinct())


def get_user_subscribed_brands(user_id, session):
    """ Fetches all brands -> subscription types that a user has.

    :param `int` user_id:
    :param `sqlalchemy.orm.Session` session:
    :return:
    :rtype: sqlalchemy.engine.result.ResultProxy
    """
    return session.bind.execute(
        select([UserSubscription.brand_id, UserSubscription.quantity_unit])
        .where(UserSubscription.user_id == user_id)
        .distinct())


def get_latest_user_subscription(user_id, brand_id, subscription_type, session):
    return session\
        .query(UserSubscription)\
        .filter(
            (UserSubscription.user_id == user_id) &
            (UserSubscription.brand_id == brand_id) &
            (UserSubscription.quantity_unit == subscription_type))\
        .order_by(desc(UserSubscription.modified))\
        .first()


def get_expired_subscriptions(session, expired_between):
    """ Yields all UserSubscriptions that have expired between certain dates.

    Subscriptions that have been renewed are NOT included.

    :param session:
    :param expired_between:
    :return:
    """
    for sub in (SingleUserSubscriptionsReport(user_id, session) for user_id in get_user_ids_with_subscriptions(session)):

        if len([x for x in sub.get_expired_subscriptions(expired_between)]):
            yield sub


class SingleUserSubscriptionsReport(object):

    def __init__(self, user_id, session):
        self.user_id = user_id
        self.session = session
        self.latest_subscriptions = []

        for row in get_user_subscribed_brands(self.user_id, self.session):
            self.latest_subscriptions.append(
                get_latest_user_subscription(
                    user_id=self.user_id,
                    brand_id=row.brand_id,
                    subscription_type=row.quantity_unit,
                    session=self.session))

    def get_expired_subscriptions(self, expired_between):

        for sub in self.latest_subscriptions:
            if sub.quantity_unit == SubscriptionType.monthly:

                if sub.valid_to <= datetime.now() and \
                        (expired_between.min_date <= sub.valid_to <= expired_between.max_date):
                    yield sub

            elif sub.quantity_unit == SubscriptionType.per_edition:
                if sub.current_quantity == 0 and \
                        (expired_between.min_date <= sub.modified <= expired_between.max_date):
                    yield sub
            else:
                raise Exception("Unprocessable type")


