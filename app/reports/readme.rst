Reports
=======

This package contains only API endpoints that are meant for displaying 'report' or
'chart' data from ScoopPortal.

Nothing should ever import code from within this package.

