"""
Eperpus SQL
===========

These are straight up, raw SQL literals used for generating the report data returned
from the EPERPUS module.

.. note::

    Variable interpolation is done with PYSCOPG2 escape sequences.  Short explanation is,  use the old
    python format string format, with EVERYTHING identified as a string (psycopg2 should handle the conversion
    automatically).
"""
from __future__ import absolute_import, unicode_literals

# ok 6 secs
# TOP_READING_TIMES = '''
#     SELECT
#         top_readers.user_id,
#         top_readers.reading_seconds,
#         (SELECT u.username || ' - ' || coalesce(u.first_name, '') || ' ' || coalesce(u.last_name, '')
#          FROM cas_users AS u WHERE u.id = top_readers.user_id LIMIT 1) AS username
#     FROM (
#         SELECT
#             CAST(user_activity->>'user_id' AS INT) AS user_id,
#             SUM(CAST(user_activity->>'duration' AS FLOAT)) AS reading_seconds
#         FROM
#             analytics.user_page_view a
#         WHERE
#             CAST(user_activity->>'client_id' AS INT) = ANY(%(client_ids)s)
#             {where_clause_user_org}
#         GROUP BY
#             user_activity->>'user_id'
#         ORDER BY
#             SUM(CAST(user_activity->>'duration' AS FLOAT)) DESC
#         LIMIT %(limit)s
#     ) AS top_readers;
# '''
TOP_READING_TIMES = '''
    SELECT
        top_readers.user_id,
        top_readers.reading_seconds,
        (SELECT u.username || ' - ' || coalesce(u.first_name, '') || ' ' || coalesce(u.last_name, '')
         FROM cas_users AS u WHERE u.id = top_readers.user_id LIMIT 1) AS username
    FROM (
        SELECT
            user_id,
            SUM(reading_seconds) AS reading_seconds
        FROM
            utility.user_page_views a
        WHERE
            client_id = ANY(%(client_ids)s)
            {where_clause_user_org}
        GROUP BY
            user_id
        ORDER BY
            SUM(reading_seconds) DESC
        LIMIT %(limit)s
    ) AS top_readers;
'''

# ok 3 secs
TOP_DOWNLOADERS_REPORT = '''
    SELECT
        top_downloaders.user_id,
        top_downloaders.download_count,
        (SELECT u.username || ' - ' || coalesce(u.first_name, '') || ' ' || coalesce(u.last_name, '')
        FROM cas_users AS u WHERE u.id = top_downloaders.user_id LIMIT 1) as username
    FROM (
        SELECT
            CAST(user_activity->>'user_id' AS INT) AS user_id,
            COUNT(*) AS download_count
        FROM analytics.user_download a
        WHERE CAST(user_activity->>'client_id' AS INT) = ANY(%(client_ids)s)
            {where_clause_user_org}
        GROUP BY
            user_activity->>'user_id'
        ORDER BY
            COUNT(*) DESC
        LIMIT %(limit)s
    ) AS top_downloaders;
'''

WHERE_CLAUSE_USER_ORGANIZATIONS_USER_ANALYTICS = '''
    AND EXISTS(
                SELECT 1 FROM
                public.cas_users_organizations uo
                WHERE uo.organization_id = {org_id}
                 AND uo.user_id = CAST(a.user_activity->>'user_id' AS INT)
            )
'''

WHERE_CLAUSE_USER_ORGANIZATIONS = '''
    AND EXISTS(
                SELECT 1 FROM
                public.cas_users_organizations uo
                WHERE uo.organization_id = {org_id}
                 AND uo.user_id = a.user_id
            )
'''

# should be fine.
ITEM_BORROWING_COUNT_FOR_TIMESPAN = '''
    SELECT
        COUNT(*) AS count
    FROM core_user_borrowed_items
    WHERE
        catalog_item_org_id = ANY(%(organization_ids)s)
        AND catalog_item_item_id = %(item_id)s
        AND borrowing_start_time BETWEEN %(start_time)s AND %(end_time)s;
'''

# should be fine
TOTAL_USER_BORROWS = '''
    SELECT
        COUNT(*) AS count
    FROM core_user_borrowed_items
    WHERE
        user_id = %(user_id)s
        AND catalog_item_org_id = ANY(%(organization_ids)s)
        AND borrowing_start_time BETWEEN %(start_time)s AND %(end_time)s;
        ;
'''


ITEM_BORROWED_CHART = '''
    SELECT
        0 as id,
        to_char(borrowing_start_time, 'YYYY-MM-DD') as group1,
        COUNT(*) AS value
    FROM core_user_borrowed_items
    WHERE catalog_item_org_id = %(organization_id)s
        AND borrowing_start_time BETWEEN %(start_time)s AND %(end_time)s
    GROUP BY  to_char(borrowing_start_time, 'YYYY-MM-DD')
    ORDER BY group1;
'''


ITEM_BORROWED_TOP_N = '''
    SELECT
        ci.id as id,
        ci.name as group1,
        r.count as value
    FROM (
        SELECT
            i.catalog_item_item_id as item_id,
            COUNT(1) AS count
        FROM core_user_borrowed_items i
        WHERE catalog_item_org_id = %(organization_id)s
            AND borrowing_start_time BETWEEN %(start_time)s AND %(end_time)s
        GROUP BY  i.catalog_item_item_id
        order by count(1) desc limit %(limit)s
    ) r
        LEFT JOIN core_items ci on ci.id = r.item_id
    order by r.count desc;
'''


ITEM_BORROWED_SUMMARY = '''
    SELECT
        COUNT(1) AS count
    FROM core_user_borrowed_items i
    WHERE catalog_item_org_id = %(organization_id)s
        AND borrowing_start_time BETWEEN %(start_time)s AND %(end_time)s
    ;
'''


# ok 1.2 seconds.
MOST_DOWNLOADED_ITEMS = '''
    SELECT
        most_downloaded.item_id,
        items.name AS item_name,
        most_downloaded.count
    FROM (
        SELECT
            CAST(user_activity->>'item_id' AS INT) AS item_id,
            COUNT(*) AS count
        FROM analytics.user_download a
        WHERE
            CAST(user_activity->>'client_id' AS INT) = ANY(%(client_ids)s)
            {where_clause_user_org}
        GROUP BY
            CAST(user_activity->>'item_id' AS INT)
        ORDER BY
            COUNT(*) DESC
        LIMIT %(limit)s
    ) AS most_downloaded
    LEFT JOIN core_items AS items
        ON most_downloaded.item_id = items.id;
'''


MOST_BORROWED_ITEMS = '''
    SELECT
        most_downloaded.item_id,
        items.name AS item_name,
        most_downloaded.count
    FROM (
        SELECT catalog_item_item_id as item_id,
            COUNT(1) as count
        FROM core_user_borrowed_items a
        WHERE
            catalog_item_org_id = ANY(%(organization_ids)s)
            AND borrowing_start_time BETWEEN %(start_time)s AND %(end_time)s
            {where_clause_user_org}
        GROUP BY catalog_item_item_id
        ORDER BY
            COUNT(*) DESC
    ) AS most_downloaded
    LEFT JOIN core_items AS items
        ON most_downloaded.item_id = items.id
    OFFSET %(offset)s
    LIMIT %(limit)s;
'''

MOST_BORROWED = '''
    SELECT catalog_item_item_id as item_id,
        COUNT(1) as count
    FROM core_user_borrowed_items a
    WHERE
        catalog_item_org_id = ANY(%(organization_ids)s)
        AND borrowing_start_time BETWEEN %(start_time)s AND %(end_time)s
        {where_clause_user_org}
    GROUP BY catalog_item_item_id
    ORDER BY
        COUNT(*) DESC

'''

# should be OK, have to keep track @ production
BORROWED_ITEMS_COUNT = '''
    SELECT
        COUNT(*)
    FROM core_user_borrowed_items
    WHERE
        catalog_item_org_id = ANY(%(organization_ids)s)
        AND returned_time IS NULL;
'''

# ok.  5.67 MS
USER_DAILY_READING_AVERAGE = '''
    SELECT AVG(daily_reading_time.reading_time) AS reading_time
    FROM (
        SELECT
            SUM(CAST(user_activity->>'duration' AS FLOAT)) AS reading_time
        FROM remote_user_page_view
        WHERE
            CAST(user_activity->>'user_id' AS INT) = %(user_id)s
            AND CAST(user_activity->>'client_id' AS INT) = ANY(%(client_ids)s)
        GROUP BY
            DATE(user_activity->>'datetime')
    ) AS daily_reading_time;
'''


# READING_STATISTICS = '''
#     SELECT
#         -- extracting 'HOUR' from ::INTERVAL will 'floor' it's value.  We want ceiling, so + 1
#         EXTRACT(HOUR FROM rd.duration*'1 second'::INTERVAL)+1 hour_group,
#         COUNT(*) as count
#     FROM (
#         -- get the user_id and sum of their reading time
#         SELECT
#             CAST(user_activity->>'user_id' AS INT) AS user_id,
#             SUM(CAST(user_activity->>'duration' AS FLOAT)) AS duration
#         FROM analytics.user_page_view
#         WHERE
#             CAST(user_activity->>'datetime_epoch' AS FLOAT)
#                 BETWEEN
#                     EXTRACT(EPOCH FROM (NOW() - %(start_time)s::INTERVAL)) AND
#                     EXTRACT(EPOCH FROM (NOW() - %(end_time)s::INTERVAL))
#             AND (CAST(user_activity->>'client_id' AS INT) = ANY(%(client_ids)s))
#         GROUP BY
#             CAST(user_activity->>'user_id' AS INT)
#     ) AS rd
#     GROUP BY
#         EXTRACT(HOUR FROM rd.duration*'1 second'::INTERVAL);
# '''
READING_STATISTICS = '''
    SELECT
        -- extracting 'HOUR' from ::INTERVAL will 'floor' it's value.  We want ceiling, so + 1
        EXTRACT(HOUR FROM rd.duration*'1 second'::INTERVAL)+1 hour_group,
        COUNT(*) as count
    FROM (
        -- get the user_id and sum of their reading time
        SELECT
            user_id,
            SUM(reading_seconds) AS duration
        FROM utility.user_page_views a
        WHERE
            client_id = ANY(%(client_ids)s)
        GROUP BY
            user_id
    ) AS rd
    GROUP BY
        EXTRACT(HOUR FROM rd.duration*'1 second'::INTERVAL);
'''


# USER_TOTAL_READING_DURATION_FOR_CLIENTS = '''
#     SELECT
#         COUNT(DISTINCT rd.user_id) AS count
#     FROM (
#         SELECT
#             CAST(user_activity->>'user_id' AS INT) AS user_id,
#             SUM(CAST(user_activity->>'duration' AS FLOAT)) AS duration
#         FROM analytics.user_page_view
#         WHERE
#             CAST(user_activity->>'datetime_epoch' AS FLOAT) > EXTRACT(EPOCH FROM (NOW()-%(since)s::INTERVAL))
#             AND CAST(user_activity->>'client_id' AS INT) = ANY(%(client_ids)s)
#         GROUP BY
#             CAST(user_activity->>'user_id' AS INT)
#     ) AS rd
#     WHERE
#         rd.duration > EXTRACT(EPOCH FROM %(duration)s::INTERVAL);
# '''
USER_TOTAL_READING_DURATION_FOR_CLIENTS = '''
    SELECT
        COUNT(DISTINCT rd.user_id) AS count
    FROM utility.user_page_views rd
    WHERE
        rd.client_id = ANY(%(client_ids)s) AND
        rd.reading_seconds > EXTRACT(EPOCH FROM %(duration)s::INTERVAL);
'''


NEW_USER_COUNT = '''
    SELECT COUNT(distinct uo.user_id) AS count
    FROM cas_users_organizations uo
    LEFT JOIN cas_users u
        ON uo.user_id = u.id
    WHERE
        organization_id = ANY(%(organization_ids)s)
        AND u.created > NOW() - %(since)s::INTERVAL
'''


NEW_USER_SUMMARY = '''
    SELECT
        users.id AS user_id,
        users.email AS user_email
    FROM cas_users_organizations AS user_orgs
    LEFT JOIN cas_users AS users
        ON user_orgs.user_id = users.id
    WHERE
        organization_id = ANY(%(organization_ids)s)
        AND users.created > NOW() - %(since)s::INTERVAL;
'''


ACTIVE_USER_COUNT = '''
    SELECT
        COUNT(DISTINCT u.user_id) AS count
    FROM analytics.user_page_view s
    LEFT JOIN cas_users_organizations u
        ON u.user_id = CAST(user_activity->>'user_id' AS INT)
        AND u.organization_id = ANY(%(organization_ids)s)
    WHERE
        CAST(user_activity->>'datetime_epoch' AS FLOAT) > EXTRACT(EPOCH FROM (NOW() - %(since)s::INTERVAL))
        AND u.user_id IS NOT NULL
'''

OWNED_ITEM_PER_CATEGORY_SUMMARY = """
    select c.id, c.name
    , (100 * (count(1) / summary.total::float))::numeric(10,2) as percentage
    , count(1) as count, summary.total as total
    from core_organization_items oi
      left join core_items_categories ic on ic.item_id = oi.item_id
      left join core_categories c on c.id = ic.category_id
      left join (
        select %(organization_id)s as organization_id, count(1) as total
        from core_organization_items oi where organization_id = %(organization_id)s and oi.is_active = TRUE
                ) summary on summary.organization_id = oi.organization_id
    where oi.organization_id = %(organization_id)s and oi.is_active = TRUE
    group by c.id, c.name, summary.total
    order by percentage desc;
"""
