import httplib, json

from dateutil.relativedelta import relativedelta
from flask_appsfoundry.exceptions import BadRequest
from marshmallow import fields
from sqlalchemy import desc
from unidecode import unidecode

from app import db, ma, app
from datetime import datetime, timedelta

from sqlalchemy.dialects.postgresql import ENUM
from flask import Blueprint, request, jsonify
from flask_appsfoundry.models import TimestampMixin

from app.auth.user_devices import UserDevices
from app.constants import RoleType
from app.users.users import User, Role
from app.helpers import err_response
from app.utils.marshmallow_helpers import schema_load


VIOLATION_SCREEN = 'screenshots violation'
VIOLATION_HACK = 'hackers violation'
VIOLATION_SPAM = 'spamming violation'
VIOLATION_SARA = 'sara violation'
VIOLATION_REFERRAL = 'referral violation'

VIOLATION_STATUS = {
    VIOLATION_SCREEN : 'screenshots violation',
    VIOLATION_HACK : 'hackers violation',
    VIOLATION_SPAM : 'spamming violation',
    VIOLATION_SARA : 'sara violation',
    VIOLATION_REFERRAL : 'referral violation'
}


class ScreenshotsSchema(ma.Schema):
    email = fields.Str(required=True)
    violation_status = fields.Str(required=True)
    is_active = fields.Bool(required=False, default=True)


class BlacklistReportScreenshot(db.Model, TimestampMixin):

    __tablename__ = 'core_report_screenshots'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    user = db.relationship('User')
    item_id = db.Column(db.Integer, db.ForeignKey('core_items.id'), nullable=False)
    item = db.relationship('Item')
    screenshot_date = db.Column(db.DateTime, nullable=False)
    is_active = db.Column(db.Boolean(), default=True)


class BlacklistReportUser(db.Model, TimestampMixin):

    __tablename__ = 'core_report_userblacklists'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('cas_users.id'), nullable=False)
    user = db.relationship('User')
    violation_status = db.Column(ENUM(*VIOLATION_STATUS.values(), name='bank_account'), nullable=False)
    expired_date = db.Column(db.DateTime, default=datetime.utcnow)
    is_active = db.Column(db.Boolean(), default=True)

    def get_screenshots_report(self):
        exist_data = BlacklistReportScreenshot.query.filter_by(user_id=self.user_id).order_by(desc(
            BlacklistReportScreenshot.screenshot_date)).all()
        if exist_data:
            return [{
                'item_id': data_screen.item.id,
                'item_name': unidecode(unicode(data_screen.item.name)),
                'screenshots_date': data_screen.screenshot_date.isoformat()
            } for data_screen in exist_data]
        return []

    def values(self):
        return {
            'id': self.id,
            'user_id': self.user_id,
            'user': {'id': self.user.id, 'email': self.user.email, 'first_name': self.user.first_name,
                     'last_name': self.user.last_name},
            'violation_status': self.violation_status,
            'is_active': self.is_active,
            'screenshots_report': self.get_screenshots_report(),
            'hackers_report': [], # prepare for next reporting product
            'spamming_report': [], # prepare for next reporting product
            'sara_report': [], # prepare for next reporting product
            'referral_report': [] # prepare for next reporting product
        }


def update_user_access(user_data, role_access=RoleType.banned_user.value):
    # update role user to banned roles
    for old_role in user_data.roles:
        user_data.roles.remove(old_role)
    banned_role = Role.query.get(role_access)
    user_data.roles.append(banned_role)
    db.session.add(user_data)
    db.session.commit()

    # automaticly force logout all device login from ebooks and eperpus!!
    UserDevices(user_id=user_data.id).remove_all()


blueprint = Blueprint('ebooks_violation_report', __name__, url_prefix='/v1')


@blueprint.route('/reports/screenshots', methods=['GET', 'POST'])
def screenshots_reports():

    if request.method == 'GET':
        args = request.args.to_dict()
        limit, offset, q = args.get('limit', 20), args.get('offset', 0), args.get('q', None)
        screenshots_query = BlacklistReportUser.query
        if q:
            screenshots_query = screenshots_query.join(User).filter(User.email.ilike('%{search}%'.format(search=q)))

        total_count = screenshots_query.count()
        screenshots_query = screenshots_query.order_by(desc(BlacklistReportUser.id)).limit(limit).offset(offset).all()

        screenshots_data = [{
            "id": data.id,
            "user_id": data.user_id,
            "email": data.user.email if data.user else None,
            "status": data.violation_status,
            "is_active": data.is_active,
            "report_date": data.created.isoformat()
        } for data in screenshots_query]

        rebuild = {
            "screenshots_report": screenshots_data,
            'metadata': {'resultset': {'offset': offset, 'limit': limit, 'count': total_count}}
        }
        data = jsonify(rebuild)
        data.status_code = httplib.OK
        return data

    if request.method == 'POST':
        try:
            request_data = json.loads(request.data)
            violation_data = request_data.get('screenshots', [])

            user_ids = []
            for data in violation_data:
                user_id = data.get('user_id', 1)
                exist_user = User.query.filter_by(id=user_id).first()

                if exist_user:
                    # input data to screenshot reporting.
                    screen_violation = BlacklistReportScreenshot(user_id=exist_user.id,
                                                                 screenshot_date=data.get('date', datetime.now()),
                                                                 item_id=data.get('item_id', 1))
                    db.session.add(screen_violation)
                    db.session.commit()
                    user_ids.append(exist_user)

            # checking if data already limit acceptance.
            for user_data in list(set(user_ids)):
                violation_exist = BlacklistReportScreenshot.query.filter_by(user_id=user_data.id, is_active=True).all()

                if violation_exist and len(violation_exist) >= app.config['SCREENSHOT_VIOLATION_REPORT'] and \
                    app.config['SCREENSHOT_VIOLATION_ACTIVE']:

                    # banned user!
                    update_user_access(user_data, RoleType.banned_user.value)

                    exist_banned = BlacklistReportUser.query.filter_by(
                        user_id=user_data.id, violation_status=VIOLATION_SCREEN, is_active=True).first()

                    if not exist_banned:
                        # always created records black list user and update expired_date 30 days from created
                        exist_user_violate = BlacklistReportUser(user_id=user_data.id,
                                                                 violation_status=VIOLATION_SCREEN, is_active=True)
                        exist_user_violate.expired_date = datetime.now() + timedelta(days=app.config['SCREENSHOT_VIOLATION_EXPIRED_DURATION'])
                        db.session.add(exist_user_violate)
                        db.session.commit()

        except Exception as e:
            print e

        resp = jsonify({'message': 'violation has been recorded!'})
        resp.status_code = httplib.OK
        return resp


def check_user_banned(user_id):

    # find banned user
    banned_user = BlacklistReportUser.query.filter_by(user_id=user_id, is_active=True).first()
    message = 'Your account has been suspended.'

    # check if user already excedeed quota permanent banned
    all_record_banned = BlacklistReportUser.query.filter_by(user_id=user_id).all()

    if len(all_record_banned) >= app.config['SCREENSHOT_VIOLATION_UNLOCK']:
        # overwrite at here if PM want change suspended permanent!
        # message = 'Your account has been permanent suspended.'
        message = message
    else:
        # count waiting time
        wait_time = relativedelta(banned_user.expired_date, datetime.now())

        # General message for soft blocking
        if wait_time.days > 0:
            message = 'Your account has been suspended for {} days'.format(wait_time.days)
        else:
            # interval -H1 always return 0.days 23 hours. we cant said account suspended 0.days
            if wait_time.hours:
                message = 'Your account has been suspended for {} hours'.format(wait_time.hours)

    '''
        TODO: unlock this code if product want specified reason for user blocking when login!!

        exist_blacklist = BlacklistReportUser.query.filter_by(user_id=user_id, is_active=True).first()
        if exist_blacklist and exist_blacklist.violation_status == VIOLATION_SCREEN:
            message = 'Your account has been suspended due screenshot violation!'
    '''
    return message, err_response(status=httplib.UNAUTHORIZED, error_code=httplib.UNAUTHORIZED,
                                 developer_message=message, user_message=message)


@blueprint.route('/reports/portal/screenshots/<blacklist_id>', methods=['GET', 'DELETE'])
def portal_screenshots_report_detail(blacklist_id):
    screenshots_data = BlacklistReportUser.query.filter_by(id=blacklist_id).first()
    if not screenshots_data:
        resp = jsonify()
        resp.status_code = httplib.NOT_FOUND
        return resp

    if request.method == 'GET':
        resp = jsonify(screenshots_data.values())
        resp.status_code = httplib.OK
        return resp

    if request.method == 'DELETE':
        ''' This method for unblock user '''
        all_data_report_user = BlacklistReportUser.query.filter_by(user_id=screenshots_data.user_id).all()

        for data_user in all_data_report_user:
            data_user.is_active = False
            db.session.add(data_user)
            db.session.commit()

            if data_user.user.email.split('@')[1] == 'guest.scoop':
                update_user_access(data_user.user, RoleType.verified_user.value)
            else:
                update_user_access(data_user.user, RoleType.unverified_user.value)

        resp = jsonify({'message': 'User has been reactivate'})
        resp.status_code = httplib.NO_CONTENT
        return resp


@blueprint.route('/reports/portal/screenshots', methods=['POST'])
def portal_screenshots_post_report():
    screenshots_schema = schema_load(ScreenshotsSchema())
    user_exist = User.query.filter_by(email=screenshots_schema.get('email')).first()
    if not user_exist:
        return BadRequest(user_message='User not found',
                         developer_message='user with email {} not found!'.format(screenshots_schema.get('email')))

    new_record = BlacklistReportUser(user_id= user_exist.id, violation_status=screenshots_schema.get('violation_status'))
    db.session.add(new_record)
    db.session.commit()
    update_user_access(user_exist, RoleType.banned_user.value)

    resp = jsonify(new_record.values())
    resp.status_code = httplib.CREATED
    return resp


