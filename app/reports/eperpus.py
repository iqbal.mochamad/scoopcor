"""
JSON data for Eperpus Portal
============================

.. warning::

    In our modelling, we refer to something as a SharedLibrary.   Marketing has begun calling these
    'eperpus'.  Just note that both names refer to the same thing.

APIs that return JSON data for populating graphs and widgets on the 'eperpus.com/portal' web CMS.
"""
from __future__ import absolute_import, unicode_literals

import httplib
import io
import json
from datetime import datetime, date
from functools import partial
from httplib import NO_CONTENT

import requests
from dateutil.relativedelta import relativedelta
from flask import Blueprint, jsonify, request, g, current_app, send_file
from flask_appsfoundry.exceptions import Unauthorized, Forbidden, NotFound, BadRequest
from marshmallow.utils import isoformat
from xlsxwriter import Workbook

from app import db, app
from app.constants import RoleType
from app.eperpus.members import get_stock_holding_org
from app.items.models import Item
from app.reports import eperpus_dw_api
from app.users.organizations import Organization, OrganizationUser, get_org_and_child_org_ids
from app.users.users import User
from app.utils.exceptions import BadGateway
from app.utils.redis_cache import RedisCache
from . import eperpus_sql as sql

blueprint = Blueprint('eperpus_reports', __name__, url_prefix='/v1/reports')

ONE_DAY = 86400


@blueprint.before_request
def authorize_user():
    """ Checks that the user is logged in, and a manager of the given organization.
    """
    if request.method != 'OPTIONS':
        if g.current_user is None:
            raise Unauthorized()

        allowed, s = False, db.session()

        # bypass internal admin.
        current_user = s.query(User).filter_by(id=g.current_user.id).first()
        if not current_user:
            raise Forbidden(
                developer_message='User must be a manager of this organization'
            )

        current_role = [role.id for role in current_user.roles]
        if RoleType.internal_admin_eperpus.value in current_role or RoleType.super_admin.value in current_role:
            allowed = True
        else:
            allowed = bool(
                s.query(OrganizationUser).filter_by(
                    organization_id=int(request.view_args['organization_id']),
                    user_id=g.current_user.id,
                    is_manager=True
                ).count()
            )

        if not allowed:
            raise Forbidden(
                developer_message='User must be a manager of this organization'
            )


@blueprint.before_request
def validate_eperpus():
    """ Makes sure that a request is for an 'eperpus'-type organization, and the organization is a top-level org.
    """
    if request.method != 'options':
        organization = db.session.query(Organization).get(request.view_args['organization_id'])
        if not organization:
            raise NotFound(
                developer_message='Could not found organization '
                                  'with id {}'.format(request.view_args['organization_id']))
        if not organization.parent_organization:
            pass
        else:
            raise NotFound(
                developer_message=('{org.name} ({org.id}) is a child of '
                                   '{org.parent_organization.name} ({org.parent_organization.id}).  '
                                   'Reports must be run for top-level organizations. '
                                   'Use Query Param org-id for child report').format(org=organization)
            )
        if organization.type != 'shared library':
            raise NotFound(
                developer_message='{org.name} ({org.id}) type is not shared library'.format(org=organization)
            )


def required_user_id_param(req):
    try:
        user_id = req.args.get('user_id', type=int)
        if not user_id:
            raise BadRequest(developer_message='user_id is a required query param')
    except ValueError:
        raise BadRequest(
            developer_message='user_id ({}) query param is not a valid integer.'.format(req.args.get('user_id'))
        )
    return (user_id,)


def required_item_id_param(req):
    try:
        user_id = req.args.get('item_id', type=int)
        if not user_id:
            raise BadRequest(developer_message='item_id is a required query param')
    except ValueError:
        raise BadRequest(
            developer_message='item_id ({}) query param is not a valid integer.'.format(req.args.get('item_id'))
        )
    return (user_id,)


def limit_param(default, req):
    return (req.args.get('limit', default, type=int),)


def string_param(default, param_name, req):
    return (req.args.get(param_name, default),)


def limit_with_org_param(limit_default, req):
    return (req.args.get('limit', limit_default, type=int), req.args.get('org_id', type=int),)


def required_duration_since_param(req):
    try:
        return (req.args['duration'], req.args['since'])
    except KeyError:
        return BadRequest(
            developer_message="'duration' and 'since' are required query params and must be ISO8601 duration formatted."
        )


def no_param(req):
    return ()


class ReportCache(object):
    def __init__(self, redis, expiry=ONE_DAY):
        self.redis = redis
        self.expiry = expiry

    def write(self, key, data):
        cache_data = json.dumps(data, sort_keys=True)
        self.redis.setex(key, self.expiry, cache_data)

    def get(self, key):
        return self.redis.get(key)


CACHE_KEY = 'reports:eperpus'


class ReportBase(object):
    def __init__(self, engine, organization, cache, session, main_org=None):
        self.engine = engine
        self.organization = organization
        self.main_org = main_org or organization
        self.cache = cache
        self.session = session

    @staticmethod
    def _bypass_cache():
        return request.headers.get('Cache-Control', '').lower() == 'no-cache'

    def build_cache_key(self, *args):
        params = [self.main_org.id, ]
        if args:
            for p in args:
                params.append(p if not isinstance(p, datetime) else isoformat(p))
        query_string_param = request.args.to_dict()
        if query_string_param:
            params = {
                "params": params,
                "query_params": query_string_param
            }
        hash_key = hash(json.dumps(params, sort_keys=True))
        return "{}:{}:{}".format(CACHE_KEY, self.report_name, hash_key)

    def _fetch_from_db(self, *args):
        raise NotImplemented

    def get(self, *args):
        results = None
        key = self.build_cache_key(*args)
        if not self._bypass_cache():
            cached_response = self.cache.get(key)
            if cached_response:
                results = json.loads(cached_response)

        if not results:
            results = self._fetch_from_db(*args)
            self.cache.write(key, results)
        return results

    def download(self, *args):
        raise NotImplemented


@blueprint.route('/eperpus/<int:organization_id>/<report_id>')
def report_api(organization_id, report_id):
    try:
        report_builder, params_builder = lookup_report[report_id]
    except KeyError:
        raise NotFound

    session = db.session

    org_id = int(request.args.get('org_id', organization_id))
    current_org = session.query(Organization).get(org_id)
    if org_id != organization_id:
        # validate org id in request query string args vs organization_id,
        # make sure it's a child of organization_id
        main_org = session.query(Organization).get(organization_id)
        child_orgs = get_org_and_child_org_ids(main_org)
        if org_id not in child_orgs:
            raise NotFound(
                developer_message='Organization {org_id} is not a child of {org.name} ({org.id})'.format(
                    org_id=org_id, org=main_org)
            )
    else:
        main_org = current_org

    report_builder = report_builder(
        engine=db.engine,
        organization=current_org,
        cache=ReportCache(current_app.kvs),
        session=session,
        main_org=main_org
    )

    report_data = report_builder.get(*params_builder(req=request))
    return jsonify(report_data)


@blueprint.route('/eperpus/<int:organization_id>/download/<report_id>')
def report_api_download(organization_id, report_id):
    try:
        report_builder, params_builder = lookup_report[report_id]
    except KeyError:
        raise NotFound

    session = db.session

    org_id = int(request.args.get('org_id', organization_id))
    current_org = session.query(Organization).get(org_id)
    if org_id != organization_id:
        # validate org id in request query string args vs organization_id,
        # make sure it's a child of organization_id
        main_org = session.query(Organization).get(organization_id)
        child_orgs = get_org_and_child_org_ids(main_org)
        if org_id not in child_orgs:
            raise NotFound(
                developer_message='Organization {org_id} is not a child of {org.name} ({org.id})'.format(
                    org_id=org_id, org=main_org)
            )
    else:
        main_org = current_org

    report_builder = report_builder(
        engine=db.engine,
        organization=current_org,
        cache=ReportCache(current_app.kvs),
        session=session,
        main_org=main_org
    )

    response = report_builder.download(*params_builder(req=request))

    return response


@blueprint.route('/eperpus/<int:organization_id>', methods=['PURGE', ])
def purge_cache(organization_id):
    redis_cache = RedisCache(current_app.kvs)
    redis_cache.delete_with_key_prefix(CACHE_KEY)
    resp = jsonify('')
    resp.status_code = NO_CONTENT
    return resp


def get_where_clause_by_user_organizations(organization, user_analytic=True):
    if organization.parent_organization:
        # this is a child organizations (have a parent org),
        # add filter by user - organizations
        #   create additional where clause
        if user_analytic:
            return sql.WHERE_CLAUSE_USER_ORGANIZATIONS_USER_ANALYTICS.format(
                org_id=organization.id)
        else:
            return sql.WHERE_CLAUSE_USER_ORGANIZATIONS.format(
                org_id=organization.id)
    else:
        # for parent organizations, no need addition where clause
        return ""


class ItemBorrowingCountPerWeekReport(ReportBase):
    report_name = 'item-borrowing-count'

    def _fetch_from_db(self, item_id):
        results = {}
        for wk_start in range(1, 13):
            results[wk_start] = self.engine.execute(
                sql.ITEM_BORROWING_COUNT_FOR_TIMESPAN,
                {
                    'organization_ids': get_org_and_child_org_ids(self.organization),
                    'item_id': item_id,
                    'start_time': datetime.utcnow() - relativedelta(weeks=wk_start),
                    'end_time': datetime.utcnow() - relativedelta(weeks=wk_start - 1)

                }).scalar()
        return {'data': results}

    def get(self, item_id):
        return super(ItemBorrowingCountPerWeekReport, self).get(item_id)


def list_to_string(value):
    return ','.join(map(str, value))


class MostDownloadedItemsReport(ReportBase):
    report_name = 'most-downloaded-items'

    def _fetch_from_db(self, args=None):
        limit, offset = args.get('limit', 25), args.get('offset', 0)
        start_date = args.get('start_date', datetime.utcnow() - relativedelta(days=14))
        end_date = args.get('end_date', datetime.utcnow() + relativedelta(days=1))
        organization_id = args.get('org_id', 0)
        total_count, data_download = 0, []
        catalog_id = args.get('catalog_id', 0)

        base_url = '{}/kettle/executeTrans/?rep=scoopdwext_etl&trans={}&start_date={}&end_date={}&organization_id={}' \
                   '&limit={}&offset={}&catalog_id={}'.format(app.config['DW_API_BASE_URL'], 'eperpus_API_top_borrowed_item_3',
                                                start_date, end_date, organization_id, limit, offset, catalog_id)

        response = requests.get(base_url, auth=(app.config['DW_API_USER'], app.config['DW_API_PASSWORD']))

        if response.status_code == httplib.OK and response.text:
            data_response = response.json().get('data', [])

            # DW api return index [0] only for total_row.. DAM
            total_count = data_response[0].get('total_row', 0)
            del data_response[0]

            for idata in data_response:
                item_id = idata.get('item_id', 0)
                data_item = self.session.query(Item).filter_by(id=item_id).first()
                if data_item:
                    item_info = {
                        'item_id': item_id,
                        'item_name': idata.get('item_name', None),
                        'count': idata.get('value', None),
                        'authors': ', '.join(
                            [author.name for author in data_item.authors]) if data_item else None,
                        'category': [{'id': cat.id, 'name': cat.name} for cat in
                                     data_item.categories] if data_item else None
                    }
                else:
                    item_info = {
                        'item_id': item_id,
                        'item_name': idata.get('item_name', None),
                        'count': idata.get('value', None),
                        'authors': '',
                        'category': ''
                    }
                data_download.append(item_info)
        else:
            raise BadGateway(
                user_message="Failed to get data from data warehouse API",
                developer_message="DW Response: {status_code} - response body: {body}.\n Url: {url}".format(
                    status_code=response.status_code,
                    body=response.text if response.text else "Empty Response",
                    url=base_url)
            )

        temp, resultset = {}, {}
        temp['count'] = total_count
        temp['limit'] = int(limit)
        temp['offset'] = int(offset)
        resultset['resultset'] = temp
        return {'data': data_download, 'metadata': resultset}

    def get(self, limit, org_id=None):
        args = request.args.to_dict()
        return super(MostDownloadedItemsReport, self).get(args)


class TotalBorrowsForUserReport(ReportBase):
    """ micro report: Total Peminjaman (total buku yg dipinjam user 123)

    """
    report_name = 'user-total-borrowing-count'

    def _fetch_from_db(self, user_id):
        result = self.engine.execute(sql.TOTAL_USER_BORROWS, {
            'user_id': user_id,
            'organization_ids': get_org_and_child_org_ids(self.organization),
            'start_time': datetime.utcnow() - relativedelta(days=14),
            'end_time': datetime.utcnow() + relativedelta(days=1),
        }).scalar()
        return {'count': result}

    def get(self, user_id):
        return super(TotalBorrowsForUserReport, self).get(user_id)


class UserAverageDailyReadingTime(ReportBase):
    report_name = 'user-average-daily-reading-time'

    def _fetch_from_db(self, user_id):
        return {
            'count': db.engine.execute(
                sql.USER_DAILY_READING_AVERAGE,
                {
                    'client_ids': client_ids_from_organization_id(self.organization),
                    'user_id': user_id
                }).scalar()
        }

    def get(self, user_id):
        return super(UserAverageDailyReadingTime, self).get(user_id)


class TopReadingTimes(ReportBase):
    report_name = 'top-reading-times'

    def _fetch_from_db(self, limit, org_id=None):
        rows = db.engine.execute(
            sql.TOP_READING_TIMES.format(
                where_clause_user_org=get_where_clause_by_user_organizations(self.organization, user_analytic=False)
            ),
            {
                'limit': limit,
                'client_ids': client_ids_from_organization_id(self.organization)
            })

        return {'data': [
            {
                'user_id': r.user_id,
                'reading_seconds': r.reading_seconds,
                'username': r.username,
            } for r in rows.fetchall()
        ]}


class TopDownloadersReport(ReportBase):
    report_name = 'top-downloaders'

    def _fetch_from_db(self, limit):
        rows = db.engine.execute(
            sql.TOP_DOWNLOADERS_REPORT.format(
                where_clause_user_org=get_where_clause_by_user_organizations(self.organization)
            ),
            {
                'limit': limit,
                'client_ids': client_ids_from_organization_id(self.organization)
            })

        return {'data': [
            {
                'user_id': r.user_id,
                'download_count': r.download_count,
                'username': r.username
            } for r in rows.fetchall()
        ]}


class CurrentBorrowedItemsCountReport(ReportBase):
    report_name = 'current-borrowed-items'

    def _fetch_from_db(self):
        count = db.engine.execute(
            sql.BORROWED_ITEMS_COUNT,
            {'organization_ids': get_org_and_child_org_ids(self.organization)})

        return {'count': count.scalar()}


class EperpusReadingStatistics(ReportBase):
    report_name = 'user-read-statistics'

    def _fetch_from_db(self):
        """ Returns a report that answers the question:

        How many users read for how many hours?  .. this week .. last week .. and today.

        Eg, "This week 2 users read for 1 hour;  12 users read for 2 hours;  1 user read for 3 hours; ..and so on"

        This API will return 3 group_names: seven_days, seven_to_fourteen_days, twentyfour_hours
        These are based on the timespan they represent in their query (from the current time).

        This API takes no query parameters.
        """

        def fetch_report_data(group_name, start_time, end_time):
            return {
                group_name: [{
                    'hours': row.hour_group,
                    'count': row.count
                } for row in db.engine.execute(sql.READING_STATISTICS, {
                    'client_ids': client_ids_from_organization_id(self.organization),
                    'start_time': start_time,
                    'end_time': end_time
                })
                ]
            }

        all_reports = [
            fetch_report_data('last_24_hours', 'P1D', 'P0D'),
            fetch_report_data('one_week_ago', 'P7D', 'P0D'),
            fetch_report_data('two_weeks_ago', 'P14D', 'P7D'),
        ]

        data = {}
        for rpt in all_reports:
            data.update(rpt)

        return {"data": data}


class EperpusUsersOverReadingThreshold(ReportBase):
    # total users with a minimum reading duration, including a particular time period
    report_name = 'users-count-over-reading-duration'

    def _fetch_from_db(self, duration, since):
        result = db.engine.execute(
            sql.USER_TOTAL_READING_DURATION_FOR_CLIENTS,
            {
                'client_ids': client_ids_from_organization_id(self.organization),
                'duration': duration,
                'since': since
            })

        return {'count': result.scalar() if result else 0}


class EperpusNewUserCountReport(ReportBase):
    # count of new user signups after a certain date
    report_name = 'new-user-count'

    def _fetch_from_db(self, since):
        result = db.engine.execute(
            sql.NEW_USER_COUNT,
            {
                'organization_ids': get_org_and_child_org_ids(self.organization),
                'since': since
            })

        return {'count': result.scalar() if result else 0}


class EperpusNewUserSummaryReport(ReportBase):
    report_name = 'new-user-summary'

    def _fetch_from_db(self, since):
        result = db.engine.execute(
            sql.NEW_USER_SUMMARY,
            {
                'organization_ids': get_org_and_child_org_ids(self.organization),
                'since': since
            }
        )
        return {'data': [{'user_id': row.user_id, 'user_name': row.user_name} for row in result]}


class EperpusActiveUserCountReport(ReportBase):
    # users that have an activity part a certain date
    report_name = 'current-active-users'

    def _fetch_from_db(self, since):
        result = db.engine.execute(
            sql.ACTIVE_USER_COUNT,
            {
                'organization_ids': get_org_and_child_org_ids(self.organization),
                'since': since
            })

        return {'count': result.scalar() if result else 0}


class OwnedItemPerCategorySummary(ReportBase):
    report_name = 'owned-item-per-category-summary'

    def _fetch_from_db(self):
        parent_org = get_stock_holding_org(self.organization)
        results = self.engine.execute(
            sql.OWNED_ITEM_PER_CATEGORY_SUMMARY,
            {
                'organization_id': parent_org.id if parent_org else 0,
            })
        data = []
        for row in results:
            data.append({
                'id': row[0],
                'name': row[1],
                'percentage': float(row[2]),
                'count': row[3],
            })
        return {'data': data}

    def download(self, *args):
        created = date.today().strftime('%Y-%m-%d')
        report_title = ['Kategori', 'Persentase (%)', 'Jumlah Judul']
        report_key = ['name', 'percentage', 'count']
        results = self.get(*args)

        output = io.BytesIO()
        workbook = Workbook(output)
        worksheet = workbook.add_worksheet()
        worksheet.set_column(0, 0, 40)
        worksheet.set_column(1, 1, 16)
        worksheet.set_column(2, 2, 12)
        bold = workbook.add_format({'bold': True})
        for index, title in enumerate(report_title):
            worksheet.write(0, index, title, bold)

        for index, data in enumerate(results.get('data')):
            for i, value in enumerate(report_key):
                worksheet.write(index + 1, i, data.get(value))

        workbook.close()
        output.seek(0)
        filename = "{filename}-{org_id}-{date}.xlsx".format(
            filename=self.report_name, org_id=self.organization.id, date=created)
        return send_file(output, attachment_filename=filename, as_attachment=True)


class EperpusUsers(ReportBase):
    report_name = 'eperpus-users'

    def download(self, *args):
        created = date.today().strftime('%Y-%m-%d')
        identity = '&organization_id={org_id}&j_username={user}&j_password={password}&output=xlsx'.format(
            org_id=self.organization.id, user=app.config['DW_JASPER_USER'], password=app.config['DW_JASPER_PASS'])
        query_params = '_flowId=viewReportFlow&_flowId=viewReportFlow&&reportUnit=/EPerpus/user_detail_organization&'
        download_url = "{base_url}{query_params}{identity}".format(
            base_url=app.config['DW_JASPER_URL'], query_params=query_params, identity=identity)

        output = io.BytesIO()
        resp = requests.get(download_url, stream=True)

        output.write(resp.content)
        output.seek(0)

        filename = "{filename}-{org_id}-{date}.xlsx".format(
            filename=self.report_name, org_id=self.organization.id, date=created)

        return send_file(output, attachment_filename=filename, as_attachment=True)


class EperpusContent(ReportBase):
    report_name = 'eperpus-content'

    def download(self, *args):
        created = date.today().strftime('%Y-%m-%d')
        identity = '&organization_id={org_id}&j_username={user}&j_password={password}&output=xlsx'.format(
            org_id=self.organization.id, user=app.config['DW_JASPER_USER'], password=app.config['DW_JASPER_PASS'])
        query_params = '_flowId=viewReportFlow&_flowId=viewReportFlow&&reportUnit=/EPerpus/purchased_contents&'
        download_url = "{base_url}{query_params}{identity}".format(
            base_url=app.config['DW_JASPER_URL'], query_params=query_params, identity=identity)

        output = io.BytesIO()
        resp = requests.get(download_url, stream=True)

        output.write(resp.content)
        output.seek(0)

        filename = "{filename}-{org_id}-{date}.xlsx".format(
            filename=self.report_name, org_id=self.organization.id, date=created)

        return send_file(output, attachment_filename=filename, as_attachment=True)


class EperpusContentInternal(ReportBase):
    report_name = 'eperpus-content-internal'

    def download(self, *args):
        created = date.today().strftime('%Y-%m-%d')
        identity = '&organization_id={org_id}&j_username={user}&j_password={password}&output=xlsx'.format(
            org_id=self.organization.id, user=app.config['DW_JASPER_USER'], password=app.config['DW_JASPER_PASS'])
        query_params = '_flowId=viewReportFlow&_flowId=viewReportFlow&&reportUnit=/EPerpus/internal_contents&'
        download_url = "{base_url}{query_params}{identity}".format(
            base_url=app.config['DW_JASPER_URL'], query_params=query_params, identity=identity)

        output = io.BytesIO()
        resp = requests.get(download_url, stream=True)

        output.write(resp.content)
        output.seek(0)

        filename = "{filename}-{org_id}-{date}.xlsx".format(
            filename=self.report_name, org_id=self.organization.id, date=created)

        return send_file(output, attachment_filename=filename, as_attachment=True)


# register class report to list of available reports:
#       url example: /v1/reports/eperpus/711/item-borrowed-chart

__all_reports = [
    # /item-borrowing-count .. ok 200-300MS
    (ItemBorrowingCountPerWeekReport, required_item_id_param),
    (MostDownloadedItemsReport, partial(limit_with_org_param, limit_default=10)),
    # /most-downloaded-items .. ok 1 second
    (TotalBorrowsForUserReport, required_user_id_param),  # /user-total-borrowing-count .. ok 273ms
    (UserAverageDailyReadingTime, required_user_id_param),  # /user-average-daily-reading-time .. ok 438ms
    (TopReadingTimes, partial(limit_with_org_param, limit_default=10)),  # /top-reading-times .. ok 941 ms
    (TopDownloadersReport, partial(limit_param, default=10)),  # /top-downloaders .. ok 459ms
    (CurrentBorrowedItemsCountReport, no_param),  # /current-borrowed-items .. ok 287ms
    # missing: average reading rate per 15 minutes
    (EperpusReadingStatistics, no_param),  # /user-read-statistics
    (EperpusUsersOverReadingThreshold, required_duration_since_param),  # / users-count-over-reading-duration
    (EperpusNewUserCountReport, partial(string_param, default='P7D', param_name='since')),
    # /new-user-count .. ok 1.57 second
    (EperpusActiveUserCountReport, partial(string_param, default='P7D', param_name='since')),
    # **NOT OK** /current-active-users
    (EperpusNewUserSummaryReport, partial(string_param, default='P7D', param_name='since')),
    # /owned-item-per-category-summary
    (OwnedItemPerCategorySummary, no_param),
    # /item-borrowed-chart
    (eperpus_dw_api.ItemBorrowedChart, eperpus_dw_api.date_range_compare),
    # item-borrowed-top-n
    (eperpus_dw_api.ItemBorrowedTopN, eperpus_dw_api.date_range_compare),
    # item-borrowed-summary
    (eperpus_dw_api.ItemBorrowedSummary, eperpus_dw_api.date_range_compare),
    # /v1/reports/eperpus/<eperpus_id>/active-users-summary
    (eperpus_dw_api.ActiveUsersSummary, eperpus_dw_api.date_range_compare),
    # /v1/reports/eperpus/<eperpus_id>/active-users-chart
    (eperpus_dw_api.ActiveUsersChart, eperpus_dw_api.date_range_compare),
    # /v1/reports/eperpus/<eperpus_id>/active-users-top-n
    (eperpus_dw_api.ActiveUsersTopN, eperpus_dw_api.date_range_compare),
    # /v1/reports/eperpus/<eperpus_id>/active-users-summary-avg-reading
    (eperpus_dw_api.ActiveUsersSummaryAvgReadingDuration, eperpus_dw_api.date_range_compare),
    # /v1/reports/eperpus/<eperpus_id>/active-users-summary-avg-session
    (eperpus_dw_api.ActiveUsersSummaryAvgReadingSession, eperpus_dw_api.date_range_compare),
    (eperpus_dw_api.UserAvgReadingTime, eperpus_dw_api.date_range_compare),
    (EperpusUsers, no_param),
    (EperpusContent, no_param),
    (EperpusContentInternal, no_param),
]

lookup_report = {rpt[0].report_name: rpt for rpt in __all_reports}


def client_ids_from_organization_id(organization):
    """ Helper function that gets the associated client_ids from a given organization id.

    The idea with this is that the Eperpus orgs all have unique client ids assigned to them
    (currently 2, one for iOS and one for Android).

    :param organization: an instance of Organization, if none provided, it will retrieve organization based on org id
    :return: A list of `Client`.id's that are associated with a given organization.
    :rtype: list
    """
    try:
        if not organization:
            raise KeyError
        parent_org = get_stock_holding_org(organization)
        client_ids = [client.id for client in parent_org.clients]

        # todo: only going one level deep.  need to extend to an indefinite
        for child_org in parent_org.child_organizations:
            client_ids.extend([c.id for c in child_org.clients])

        return list(set(client_ids))
    except KeyError:
        raise NotFound(
            user_message="Not found",
            developer_message=
            "Org {} does not have a client_ids mapping configured.  Talk to backend.".format(organization.id)
        )
