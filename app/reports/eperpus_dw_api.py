import json
import requests
from datetime import datetime, timedelta
from urllib import quote_plus

from babel import numbers
from flask import request
from flask_appsfoundry.exceptions import BadRequest
from marshmallow.utils import isoformat, from_datestring
from requests import codes

from app import app
from app.utils.exceptions import BadGateway

ONE_DAY = 86400
SMART_LIB_ORGANIZATION = [1100813, 1100814, 1100815, 1100816, 1330090, 1100823, 1331792]

def encode_date(value):
    if isinstance(value, str):
        # make sure it's a valid date
        value = from_datestring(value)
    return quote_plus(isoformat(value, localtime=True))


def data_warehouse_api(dw_trans_name='', start_date=None, end_date=None,
                       organization_id=None, limit=None, organization_child_id=None, user_id=None):
    """

    :param `string` dw_trans_name:
    :param `datetime` start_date: must be a datetime data (use from_datestring method to convert string to datetime)
    :param `datetime` end_date: must be a datetime data (use from_datestring method to convert string to datetime)
    :param organization_id:
    :param limit:
    :param organization_child_id:
    :return:
    """
    start_date_for_dw = encode_date(start_date)
    end_date_for_dw = encode_date(end_date)

    if int(organization_id) in SMART_LIB_ORGANIZATION: dw_trans_name = '{}_smart'.format(dw_trans_name)
    else: dw_trans_name = dw_trans_name

    base_url = '%s/kettle/executeTrans/?' \
               'rep=scoopdwext_etl&' \
               'trans=%s&start_date=%s&end_date=%s&organization_id=%s' % (
                   app.config['DW_API_BASE_URL'], dw_trans_name,
                   start_date_for_dw, end_date_for_dw, organization_id)

    if limit:
        base_url = '{}&limit={}'.format(base_url, limit)

    base_url = '{}&organization_child_id={}&user_id={}'.format(
        base_url, organization_child_id if organization_child_id and organization_child_id != organization_id else 0,
        user_id if user_id else 0)

    try:
        response = requests.get(base_url, auth=(app.config['DW_API_USER'], app.config['DW_API_PASSWORD']))

        # print('aaa - {} - {} - {}'.format(base_url, response.status_code, response))
        if response.status_code == codes.ok and response.text:
            return response.json().get('data', [])
        else:
            raise BadGateway(
                user_message="Failed to get data from data warehouse API",
                developer_message="DW Response: {status_code} - response body: {body}.\n Url: {url}".format(
                    status_code=response.status_code,
                    body=response.text if response.text else "Empty Response",
                    url=base_url)
            )
    except BadGateway as ex:
        raise ex
    except Exception as ex:
        raise BadGateway(
            user_message="Failed to get data from data warehouse API",
            developer_message="Error: {ex}\n. Url: {url}".format(ex=str(ex), url=base_url)
        )


def date_range_compare(req):

    try:
        start_date = from_datestring(req.args.get('start-date', default='', type=str).replace('"', ''))
        end_date = from_datestring(req.args.get('end-date', default='', type=str).replace('"', ''))
        if not start_date or not end_date:
            raise BadRequest(developer_message='start date and end date is a required query param')
    except (ValueError, AttributeError):
        raise BadRequest(
            developer_message='start date ({}) or end date ({}) query param is not a valid date.'.format(
                req.args.get('start-date'), req.args.get('end-date'))
        )
    start_date2 = None
    end_date2 = None
    try:
        is_compare = req.args.get('is-compare', default=False)
        if is_compare in [True, 'true', 1, '1']:
            start_date2 = from_datestring(req.args.get('start-date2', default='', type=str).replace('"', ''))
            end_date2 = from_datestring(req.args.get('end-date2', default='', type=str).replace('"', ''))
            if not start_date or not end_date:
                raise BadRequest(developer_message='Compare start date and end date is a required query param')
    except (ValueError, AttributeError):
        raise BadRequest(
            developer_message='Compare start date ({}) or end date ({}) query param is not a valid date.'.format(
                req.args.get('start-date2'), req.args.get('end-date2'))
        )
    limit = req.args.get('limit', default=None, type=int)
    user_id = req.args.get('user-id', default=None, type=int)

    return start_date, end_date, is_compare, start_date2, end_date2, limit, user_id


class ReportBase(object):
    def __init__(self, engine, organization, cache, session, main_org=None):
        self.engine = engine
        self.organization = organization
        self.main_org = main_org or organization
        self.cache = cache
        self.session = session

    @staticmethod
    def _bypass_cache():
        return request.headers.get('Cache-Control', '').lower() == 'no-cache'

    def build_cache_key(self, *args):
        params = [self.main_org.id, ]
        if args:
            for p in args:
                params.append(p if not isinstance(p, datetime) else isoformat(p))
        query_string_param = request.args.to_dict()
        if query_string_param:
            params = {
                "params": params,
                "query_params": query_string_param
            }
        hash_key = hash(json.dumps(params, sort_keys=True))
        return "{}:{}:{}".format(CACHE_KEY, self.report_name, hash_key)

    def _fetch_from_db(self, *args):
        raise NotImplemented

    def get(self, *args):
        results = None
        key = self.build_cache_key(*args)
        if self.cache and not self._bypass_cache():
            cached_response = self.cache.get(key)
            if cached_response:
                results = json.loads(cached_response)

        if not results:
            results = self._fetch_from_db(*args)
            if self.cache:
                self.cache.write(key, results)
        return results


class ReportWithCompareBase(ReportBase):

    report_name = ''
    # define sql syntax if from scoopcor db
    sql_syntax = None
    # define api url + user + password if from DW API
    dw_trans_name = None

    default_limit = None

    def _fetch_from_db(self, start_date, end_date, is_compare, start_date2, end_date2, limit=None, user_id=None):
        limit = limit or self.default_limit
        rows = self.get_report_data(
            organization_id=self.main_org.id, start_date=start_date, end_date=end_date, limit=limit,
            organization_child_id=self.organization.id,
            user_id=user_id
        )
        data = self.serialize_data(rows)
        result = {'data': data}
        if is_compare:
            rows2 = self.get_report_data(
                organization_id=self.main_org.id, start_date=start_date2, end_date=end_date2, limit=limit,
                organization_child_id=self.organization.id,
                user_id=user_id
            )
            result['data_compare'] = self.serialize_data(rows2)
        return result

    def serialize_data(self, rows):
        # override this method if needed
        data = []
        if rows:
            for r in rows:
                data.append({
                    "id": r.get('id') if self.dw_trans_name else r.id,
                    'group1': r.get('group1') if self.dw_trans_name else r.group1,
                    'value': r.get('value') if self.dw_trans_name else r.value
                })
        return data

    def get_report_data(self, organization_id, start_date, end_date, limit=None, organization_child_id=None,
                        user_id=None):
        if self.sql_syntax:
            sql_param = {
                'organization_id': organization_id,
                'start_time': start_date,
                'end_time': end_date,
            }
            if limit:
                sql_param['limit'] = limit

            rows = self.engine.execute(self.sql_syntax, sql_param).fetchall()

        elif self.dw_trans_name:
            rows = data_warehouse_api(self.dw_trans_name, start_date, end_date,
                                      organization_id, limit, organization_child_id, user_id)

        return rows


class ReportChartWithCompareBase(ReportWithCompareBase):

    def serialize_data(self, rows):
        # override this method if needed
        data = []
        if rows:
            for r in rows:
                data.append({
                    'group1': r.get('date') if self.dw_trans_name else r.group1,
                    'value': r.get('count') if self.dw_trans_name else r.value
                })
        return data


class ReportSummaryWithCompareBase(ReportWithCompareBase):

    def _fetch_from_db(self, start_date, end_date, is_compare, start_date2, end_date2, limit=None, user_id=None):
        limit = limit or self.default_limit
        result = self.get_report_data(
            organization_id=self.main_org.id, start_date=start_date, end_date=end_date, limit=limit,
            organization_child_id=self.organization.id,
            user_id=user_id
        )
        result2 = None
        if is_compare:
            result2 = self.get_report_data(
                organization_id=self.main_org.id, start_date=start_date2, end_date=end_date2, limit=limit,
                organization_child_id=self.organization.id,
                user_id=user_id
            )
        data = self._get_summary_response(result, result2, is_compare)
        return data

    def get_report_data(self, organization_id, start_date, end_date, limit=None,
                        organization_child_id=None, user_id=None):
        summary = None
        if self.sql_syntax:
            sql_param = {
                'organization_id': organization_id,
                'start_time': start_date,
                'end_time': end_date,
            }

            # since it's summary report, get with scalar method (single value)
            summary = self.engine.execute(self.sql_syntax, sql_param).scalar()

        elif self.dw_trans_name:
            # get data from Data Warehouse API
            dw_data = data_warehouse_api(self.dw_trans_name, start_date, end_date,
                                         organization_id, limit, organization_child_id, user_id)
            if dw_data:
                summary = dw_data[0].get('count', None)

        return summary

    @staticmethod
    def _get_summary_response(result, result2, is_compare):
        if is_compare:
            if not result2:
                main_value = "N/A"
            else:
                main_value = (float(result) - float(result2)) / float(result2)
                main_value = numbers.format_decimal(main_value, format='#,##0.## %;-# %', locale='id')
            data = {
                'main_value': main_value,
                'sub_value': numbers.format_decimal(result, format='#,##0.##;-#', locale='id') if result else None,
                'sub_compare_value': numbers.format_decimal(result2, format='#,##0.##;-#', locale='id') if result2
                    else None,
            }
        else:
            data = {'main_value': numbers.format_decimal(result, format='#,##0.##;-#', locale='id'), }
        return data


class ItemBorrowedChart(ReportChartWithCompareBase):

    report_name = 'item-borrowed-chart'
    # sql_syntax = sql.ITEM_BORROWED_CHART
    sql_syntax = None
    dw_trans_name = 'eperpus_API_total_borrowed_item'
    default_limit = None

    # for parameter parsing from query parameter: see method date_range_compare()

    # url example: = '/v1/organizations/711/item-borrowed-chart?'
    #     'start-date=2016-03-01T00:00:00Z'
    #     '&end-date=2016-03-10T00:00:00Z'
    #     '&is-compare=1'
    #     '&start-date2=2016-02-01T00:00:00Z'
    #     '&end-date2=2016-02-10T00:00:00Z',


class ItemBorrowedTopN(ReportWithCompareBase):

    report_name = 'item-borrowed-top-n'
    # sql_syntax = sql.ITEM_BORROWED_TOP_N
    sql_syntax = None
    default_limit = 10
    dw_trans_name = "eperpus_API_top_borrowed_item"


class ItemBorrowedSummary(ReportSummaryWithCompareBase):

    report_name = 'item-borrowed-summary'
    # sql_syntax = sql.ITEM_BORROWED_SUMMARY
    dw_trans_name = 'eperpus_API_total_borrowed'
    sql_syntax = None
    default_limit = None


class ActiveUsersSummary(ReportSummaryWithCompareBase):

    report_name = 'active-users-summary'
    dw_trans_name = 'eperpus_API_total_reader'
    sql_syntax = None
    default_limit = None


class ActiveUsersSummaryAvgReadingDuration(ReportSummaryWithCompareBase):

    report_name = 'active-users-summary-avg-reading'
    dw_trans_name = 'eperpus_API_avg_reading'
    sql_syntax = None
    default_limit = None

    @staticmethod
    def _get_summary_response(result, result2, is_compare):
        if is_compare:
            if not result2:
                main_value = "N/A"
            else:
                value1 = interval_to_seconds(result)
                value2 = interval_to_seconds(result2)
                main_value = (float(value1) - float(value2)) / float(value2)
                main_value = numbers.format_decimal(main_value, format='#,##0.## %;-# %', locale='id')
            data = {
                'main_value': main_value,
                'sub_value': remove_microseconds(result),
                'sub_compare_value': remove_microseconds(result2)
            }
        else:
            data = {'main_value': remove_microseconds(result)}
        return data


def interval_to_seconds(interval_value):
    try:
        interval_value = remove_microseconds(interval_value)
        # t = datetime.strptime(interval_value, "%H:%M:%S")
        t = interval_value.split(':')
        delta = timedelta(hours=int(t[0]), minutes=int(t[1]), seconds=int(t[2]))
        return delta.seconds
    except:
        return 0


def remove_microseconds(timestamp):
    parts = str(timestamp).split('.') if '.' in str(timestamp) else [timestamp, '0']
    return parts[0]


class ActiveUsersSummaryAvgReadingSession(ReportSummaryWithCompareBase):
    # /v1/reports/eperpus/<eperpus_id>/active-users-summary-avg-session
    report_name = 'active-users-summary-avg-session'
    dw_trans_name = 'eperpus_API_avg_session'
    sql_syntax = None
    default_limit = None

    @staticmethod
    def _get_summary_response(result, result2, is_compare):
        if is_compare:
            if not result2:
                main_value = "N/A"
            else:
                main_value = (float(result) - float(result2)) / float(result2)
                main_value = numbers.format_decimal(main_value, format='#,##0.## %;-# %', locale='id')
            data = {
                'main_value': main_value,
                'sub_value': numbers.format_decimal(result, format='#,##0.## kali;-# kali', locale='id'),
                'sub_compare_value': numbers.format_decimal(result2, format='#,##0.## kali;-# kali', locale='id'),
            }
        else:
            data = {'main_value': numbers.format_decimal(result, format='#,##0.## kali;-# kali', locale='id')}
        return data


class ActiveUsersChart(ReportChartWithCompareBase):

    report_name = 'active-users-chart'
    sql_syntax = None
    default_limit = None
    dw_trans_name = 'eperpus_API_total_active_user'


class ActiveUsersTopN(ReportWithCompareBase):

    report_name = 'active-users-top-n'
    sql_syntax = None
    default_limit = 20
    dw_trans_name = 'eperpus_API_top_reader'

    def serialize_data(self, rows):
        # override this method if needed
        data = []
        if rows:
            for r in rows:
                group1 = '{} ({})'.format(r.get('group1'), r.get('username')) if self.dw_trans_name else r.group1
                data.append({
                    "id": r.get('id') if self.dw_trans_name else r.id,
                    'group1': group1,
                    'value': remove_microseconds(r.get('value')) if self.dw_trans_name else r.value
                })
        return data


class UserAvgReadingTime(ReportWithCompareBase):

    report_name = 'user-average-reading-time'
    sql_syntax = None
    default_limit = 1
    dw_trans_name = 'eperpus_API_top_reader'

    def _fetch_from_db(self, start_date, end_date, is_compare, start_date2, end_date2, limit=None, user_id=None):
        limit = limit or self.default_limit
        rows = self.get_report_data(
            organization_id=self.main_org.id, start_date=start_date, end_date=end_date, limit=limit,
            organization_child_id=self.organization.id,
            user_id=user_id
        )
        data = self.serialize_data(rows, start_date, end_date)
        result = {'count': data}
        return result

    def calculate_average(self, value, start_date, end_date):
        time_count = (end_date - start_date)
        value_minutes = (interval_to_seconds(value) / 60) / time_count.days
        return value_minutes

    def serialize_data(self, rows, start_date, end_date):
        # override this method if needed
        data = "0"
        if rows:
            for r in rows:
                value = remove_microseconds(r.get('value')) if self.dw_trans_name else r.value
                data = self.calculate_average(value, start_date, end_date)
        return data

class ReportCache(object):
    def __init__(self, redis, expiry=ONE_DAY):
        self.redis = redis
        self.expiry = expiry

    def write(self, key, data):
        cache_data = json.dumps(data, sort_keys=True)
        self.redis.setex(key, self.expiry, cache_data)

    def get(self, key):
        return self.redis.get(key)


CACHE_KEY = 'reports:eperpus'
