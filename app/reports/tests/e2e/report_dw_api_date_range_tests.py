import json
from datetime import datetime, timedelta
from httplib import OK

from mock import patch

from app import db
from app.auth.models import Client
from app.eperpus.tests.fixtures import UserBorrowedItemFactory, SharedLibraryFactory
from app.reports.eperpus_dw_api import encode_date, interval_to_seconds, remove_microseconds
from app.users.organizations import OrganizationUser
from app.users.tests.fixtures import UserFactory
from app.utils.datetimes import get_local, WIB
from app.utils.testcases import TestBase


def mock_get_from_dw_api(package_name='', start_date=None, end_date=None,
                         organization_id=None, limit=None, organization_child_id=None, user_id=None):
    # mock response from Datawarehouse API (content inside "data" attribute)
    if package_name == 'eperpus_API_total_borrowed':
        # item-borrowed-summary
        if start_date.month == 3:
            return [{"count": 6}]
        else:
            return [{"count": 6}]
    elif package_name == 'eperpus_API_total_borrowed_item':
        # item-borrowed-chart
        if start_date.month == 3:
            return [
                {"date": '2016-03-04', "count": 3},
                {"date": '2016-03-05', "count": 1},
                {"date": '2016-03-06', "count": 2},
            ]
        else:
            return [
                {"date": '2016-02-02', "count": 2},
                {"date": '2016-02-03', "count": 1},
                {"date": '2016-02-04', "count": 3},
            ]
    elif package_name == 'eperpus_API_top_borrowed_item':
        # item-borrowed-top-n
        if start_date.month == 3:
            return [
                {"id": 444, "group1": 'up to u', "value": 33},
                {"id": 555, "group1": 'just random', "value": 22},
            ]
        else:
            return [
                {"id": 123, "group1": 'nama apa aja', "value": 3},
                {"id": 333, "group1": 'ga ada nama', "value": 2},
            ]
    elif package_name == 'eperpus_API_total_active_user':
        if start_date.month == 3:
            # active user chart
            return [
                {"date": '2016-03-04', "count": 3},
                {"date": '2016-03-05', "count": 1},
                {"date": '2016-03-06', "count": 2},
                {"date": '2016-03-07', "count": 3},
                {"date": '2016-03-08', "count": 4},
                {"date": '2016-03-09', "count": 6},
                {"date": '2016-03-10', "count": 6},
            ]
        else:
            return [
                {"date": '2016-02-04', "count": 1},
                {"date": '2016-02-05', "count": 3},
                {"date": '2016-02-06', "count": 5},
                {"date": '2016-02-07', "count": 2},
                {"date": '2016-02-08', "count": 3},
                {"date": '2016-02-09', "count": 1},
                {"date": '2016-02-10', "count": 2},
            ]
    elif package_name == 'eperpus_API_total_reader':
        # active user summary
        if start_date.month == 3:
            return [{"count": 12345.334455}]
        else:
            return [{"count": 11123.2345}]
    elif package_name == 'eperpus_API_avg_reading':
        # active-users-summary-avg-reading duration
        if start_date.month == 3:
            return [{"count": '10:52:43.302'}]
        else:
            return [{"count": '04:32:58.692'}]
    elif package_name == 'eperpus_API_avg_session':
        # active-users-summary-avg-session
        if start_date.month == 3:
            return [{"count": 50.23245}]
        else:
            return [{"count": 100.23455}]
    elif package_name == 'eperpus_API_top_reader':
        # active-users-top-n
        if user_id:
            # user-average-reading-time
            return [
                {"id": 1, "value": "10:11:04.394", "username": "xxx@g.com", "group1": "xxxx"}
            ]
        elif start_date.month == 3:
            # active-users-top-n
            return [
                {"id": 1, "username": "a", "group1": 'user a', "value": 13},
                {"id": 2, "username": "b", "group1": 'user b', "value": 12},
                {"id": 3, "username": "ab", "group1": 'user ab', "value": 10},
                {"id": 4, "username": "bce", "group1": 'user bce', "value": 9},
                {"id": 5, "username": "abc", "group1": 'user abc', "value": 7},
            ]
        else:
            # active-users-top-n
            return [
                {"id": 5, "username": "abc", "group1": 'user abc', "value": 15},
                {"id": 11, "username": "bdd", "group1": 'user bdd', "value": 14},
                {"id": 12, "username": "abe", "group1": 'user abe', "value": 10},
                {"id": 13, "username": "bee", "group1": 'user bee', "value": 8},
            ]
    elif package_name == 'eperpus_API_top_reader':
        return [
            {"id": 1, "value": "10:11:04.394", "username": "xxx@g.com", "group1": "xxxx"}
        ]


class ReportWithDateRangeTests(TestBase):

    max_borrowed_time = timedelta(days=7)

    @classmethod
    def setUpClass(cls):
        super(ReportWithDateRangeTests, cls).setUpClass()
        cls.init_data()

    @classmethod
    def init_data(cls):
        s = db.session
        client = s.query(Client).get(83)
        cls.parent_org_id = 711
        cls.child_org_id = 712
        cls.library_org = SharedLibraryFactory(
            id=cls.parent_org_id,
            user_concurrent_borrowing_limit=10,
            reborrowing_cooldown=timedelta(days=0),
            borrowing_time_limit=cls.max_borrowed_time)
        cls.library_org.clients.append(client)
        cls.child_org = SharedLibraryFactory(
            id=cls.child_org_id,
            parent_organization=cls.library_org)
        cls.child_org2 = SharedLibraryFactory(
            id=713,
            parent_organization=cls.library_org)
        cls.user = UserFactory(id=12345, created=get_local())
        cls.user2 = UserFactory(id=10346, created=datetime(2015, 1, 1))
        cls.user3 = UserFactory(id=103467, created=datetime(2015, 1, 1))
        s.add(
            OrganizationUser(
                organization_id=cls.library_org.id,
                user_id=cls.super_admin.id,
                is_manager=True
            ))
        s.add(
            OrganizationUser(
                organization_id=cls.library_org.id,
                user_id=cls.user.id,
                is_manager=True
            ))

        s.commit()

    @classmethod
    def create_user_borrowed_item(cls, user, catalog_item1, borrow_date):
        return UserBorrowedItemFactory(
            user=user,
            catalog_item=catalog_item1,
            borrowing_start_time=borrow_date
        )

    def assert_response(self, url, expected):
        self.set_api_authorized_user(self.super_admin)
        headers = self.build_headers()
        headers['Cache-Control'] = 'no-cache'
        response = self.api_client.get(url, headers=headers)
        self.assertEqual(response.status_code, OK, "Response status code != 200(OK). response: {}".format(response.data))
        actual = json.loads(response.data)
        self._assert_dict_contain_subset(expected, actual)

    @patch('app.reports.eperpus_dw_api.data_warehouse_api', side_effect=mock_get_from_dw_api)
    def test_item_borrowed_chart(self, mock_dw):
        expected = {
            "data": [
                {"group1": '2016-03-04', "value": 3},
                {"group1": '2016-03-05', "value": 1},
                {"group1": '2016-03-06', "value": 2},
            ],
            "data_compare": [
                {"group1": '2016-02-02', "value": 2},
                {"group1": '2016-02-03', "value": 1},
                {"group1": '2016-02-04', "value": 3},
            ]
        }
        self.assert_response(
            url='/v1/reports/eperpus/711/item-borrowed-chart?'
                'start-date=2016-03-01T00:00:00Z&end-date=2016-03-10T00:00:00Z'
                '&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z&end-date2=2016-02-10T00:00:00Z',
            expected=expected)

    @patch('app.reports.eperpus_dw_api.data_warehouse_api', side_effect=mock_get_from_dw_api)
    def test_item_borrowed_summary(self, mock_dw):
        expected = {"main_value": "0 %", "sub_compare_value": "6", "sub_value": "6"}
        self.assert_response(
            url='/v1/reports/eperpus/711/item-borrowed-summary?'
                'start-date=2016-03-01T00:00:00Z&end-date=2016-03-10T00:00:00Z'
                '&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z&end-date2=2016-02-10T00:00:00Z',
            expected=expected)

    @patch('app.reports.eperpus_dw_api.data_warehouse_api', side_effect=mock_get_from_dw_api)
    def test_item_borrowed_top_n(self, mock_dw):
        # with self.on_session(self.item1, self.item2):
        expected = {
            "data": [
                # {"id": self.item1.id, "group1": self.item1.name, "value": 3},
                # {"id": self.item2.id, "group1": self.item2.name, "value": 2},
                {"id": 123, "group1": "nama apa aja", "value": 3},
                {"id": 333, "group1": "ga ada nama", "value": 2},
            ]
        }
        expected_with_compare = {
            "data": [
                {"id": 444, "group1": "up to u", "value": 33},
                {"id": 555, "group1": "just random", "value": 22},
            ],
            "data_compare": [
                {"id": 123, "group1": "nama apa aja", "value": 3},
                {"id": 333, "group1": "ga ada nama", "value": 2},
            ]
        }
        # test select top N , N = 2
        self.assert_response(
            url='/v1/reports/eperpus/711/item-borrowed-top-n?start-date=2016-02-01T00:00:00Z&'
                'end-date=2016-02-10T00:00:00Z&limit=2',
            expected=expected)
        # test select top N , N = 2, with compare
        self.assert_response(
            url='/v1/reports/eperpus/711/item-borrowed-top-n?'
                'start-date=2016-03-01T00:00:00Z&end-date=2016-03-10T00:00:00Z'
                '&limit=2&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z&end-date2=2016-02-10T00:00:00Z',
            expected=expected_with_compare)

    @patch('app.reports.eperpus_dw_api.data_warehouse_api', side_effect=mock_get_from_dw_api)
    def test_active_users_summary(self, mock_dw):
        sub_compare_value = 11123.2345
        sub_value = 12345.334455
        expected = {
            'main_value': "10,99 %",
            'sub_value': "12.345,33",
            'sub_compare_value': "11.123,23"
        }
        self.assert_response(
            url='/v1/reports/eperpus/711/active-users-summary?'
                'start-date=2016-03-01T00:00:00Z'
                '&end-date=2016-03-10T00:00:00Z'
                '&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z'
                '&end-date2=2016-02-10T00:00:00Z',
            expected=expected)

    def test_interval_to_seconds(self):
        sub_value = '10:52:43.302'
        value1 = interval_to_seconds(sub_value)
        self.assertEqual(value1, 39163)

    def test_remove_microseconds(self):
        sub_value = '10:52:43.302'
        value1 = remove_microseconds(sub_value)
        self.assertEqual(value1, '10:52:43')

    @patch('app.reports.eperpus_dw_api.data_warehouse_api', side_effect=mock_get_from_dw_api)
    def test_active_users_summary_avg_reading(self, mock_dw):
        sub_compare_value = '04:32:58.692'
        value2 = interval_to_seconds(sub_compare_value)
        sub_value = '10:52:43.302'
        value1 = interval_to_seconds(sub_value)

        expected = {
            'main_value': "139,12 %",
            'sub_value': remove_microseconds(sub_value),
            'sub_compare_value': remove_microseconds(sub_compare_value)
        }
        self.assert_response(
            url='/v1/reports/eperpus/711/active-users-summary-avg-reading?'
                'start-date=2016-03-01T00:00:00Z'
                '&end-date=2016-03-10T00:00:00Z'
                '&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z'
                '&end-date2=2016-02-10T00:00:00Z',
            expected=expected)

    @patch('app.reports.eperpus_dw_api.data_warehouse_api', side_effect=mock_get_from_dw_api)
    def test_active_users_summary_avg_session(self, mock_dw):
        sub_compare_value = 100.23455
        sub_value = 50.23245

        expected = {
            'main_value': "-49,89 %",
            'sub_value': '50,23 kali'.format(sub_value),
            'sub_compare_value': '100,23 kali'.format(sub_compare_value)
        }
        self.assert_response(
            url='/v1/reports/eperpus/711/active-users-summary-avg-session?'
                'start-date=2016-03-01T00:00:00Z'
                '&end-date=2016-03-10T00:00:00Z'
                '&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z'
                '&end-date2=2016-02-10T00:00:00Z',
            expected=expected)

    @patch('app.reports.eperpus_dw_api.data_warehouse_api', side_effect=mock_get_from_dw_api)
    def test_active_users_chart(self, mock_dw):
        expected = {
            "data": [
                {"group1": '2016-03-04', "value": 3},
                {"group1": '2016-03-05', "value": 1},
                {"group1": '2016-03-06', "value": 2},
                {"group1": '2016-03-07', "value": 3},
                {"group1": '2016-03-08', "value": 4},
                {"group1": '2016-03-09', "value": 6},
                {"group1": '2016-03-10', "value": 6},
            ],
            "data_compare": [
                {"group1": '2016-02-04', "value": 1},
                {"group1": '2016-02-05', "value": 3},
                {"group1": '2016-02-06', "value": 5},
                {"group1": '2016-02-07', "value": 2},
                {"group1": '2016-02-08', "value": 3},
                {"group1": '2016-02-09', "value": 1},
                {"group1": '2016-02-10', "value": 2},
            ]
        }

        self.assert_response(
            url='/v1/reports/eperpus/711/active-users-chart?'
                'start-date=2016-03-01T00:00:00Z'
                '&end-date=2016-03-10T00:00:00Z'
                '&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z'
                '&end-date2=2016-02-10T00:00:00Z',
            expected=expected)

    @patch('app.reports.eperpus_dw_api.data_warehouse_api', side_effect=mock_get_from_dw_api)
    def test_active_users_top_n(self, mock_dw):
        expected = {
            "data": [
                {"id": 1, "group1": 'user a (a)', "value": 13},
                {"id": 2, "group1": 'user b (b)', "value": 12},
                {"id": 3, "group1": 'user ab (ab)', "value": 10},
                {"id": 4, "group1": 'user bce (bce)', "value": 9},
                {"id": 5, "group1": 'user abc (abc)', "value": 7},
            ],
            "data_compare": [
                {"id": 5, "group1": 'user abc (abc)', "value": 15},
                {"id": 11, "group1": 'user bdd (bdd)', "value": 14},
                {"id": 12, "group1": 'user abe (abe)', "value": 10},
                {"id": 13, "group1": 'user bee (bee)', "value": 8},
            ]
        }
        self.assert_response(
            url='/v1/reports/eperpus/711/active-users-top-n?'
                'start-date=2016-03-01T00:00:00Z'
                '&end-date=2016-03-10T00:00:00Z'
                '&limit=2&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z'
                '&end-date2=2016-02-10T00:00:00Z',
            expected=expected)

    @patch('app.reports.eperpus_dw_api.data_warehouse_api', side_effect=mock_get_from_dw_api)
    def test_user_avg_reading_time(self, mock_dw):
        expected = {
            "count": 67
        }

        self.assert_response(
            url='/v1/reports/eperpus/711/user-average-reading-time?'
                'start-date=2016-03-01T00:00:00Z'
                '&end-date=2016-03-10T00:00:00Z'
                '&user-id=1', expected=expected)

    def test_encode_date(self):
        string_date = '2017-11-08T17:00:00+07:00'
        actual = encode_date(string_date)
        # : -> %3A, + -> %2B
        expected = '2017-11-08T17%3A00%3A00%2B07%3A00'
        self.assertEqual(expected, actual)
        date = datetime(2017, 11, 8, 17, 0, 0, tzinfo=WIB())
        actual2 = encode_date(date)
        self.assertEqual(expected, actual2)
