from __future__ import unicode_literals

import json
import random
import time

from httplib import OK, NO_CONTENT
from unittest import TestCase

import mock
from faker import Factory

from datetime import timedelta, datetime
from flask import g, current_app
from marshmallow.utils import isoformat
from mockredis import mock_strict_redis_client
from nose.tools import istest, nottest

from app import app, set_request_id, db
from app.analytics.choices import ORIENTATION_TYPES, ONLINE_TYPES
from app.analytics.models import UserPageview, UserDownload
from app.auth.models import UserInfo, Client
from app.eperpus.libraries import SharedLibraryItem
from app.eperpus.tests.fixtures import CatalogFactory, UserBorrowedItemFactory
from app.eperpus.tests.fixtures import CatalogItemFactory
from app.eperpus.tests.fixtures import SharedLibraryFactory
from app.eperpus.tests.fixtures import SharedLibraryItemFactory
from app.users.organizations import Organization, OrganizationUser
from app.users.tests.fixtures import UserFactory
from app.users.users import User
from app.utils.datetimes import datetime_to_isoformat, get_local
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import AuthorFactory
from tests.fixtures.sqlalchemy import ItemFactory


@nottest
def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    # if args[0] == '/v1/reports/eperpus/711/users'':
    return MockResponse(get_mocked_user_request_get(711), 200)


@istest
class SharedLibraryMicroReportTest(TestBase):

    maxDiff = None
    max_borrowed_time = timedelta(days=7)

    @classmethod
    def setUpClass(cls):
        db.engine.execute("DROP MATERIALIZED VIEW IF EXISTS utility.user_page_views;")
        super(SharedLibraryMicroReportTest, cls).setUpClass()

        cls.init_data()
        db.engine.execute("""CREATE MATERIALIZED VIEW utility.user_page_views AS
        SELECT
            CAST(user_activity->>'user_id' AS INT) AS user_id,
            CAST(user_activity->>'client_id' AS INT) as client_id,
            SUM(CAST(user_activity->>'duration' AS FLOAT)) AS reading_seconds
        FROM
            analytics.user_page_view a
        GROUP BY
            CAST(user_activity->>'user_id' AS INT),
            CAST(user_activity->>'client_id' AS INT);
        CREATE UNIQUE INDEX idx_materialized_view_user_page_views_client_user_id on utility.user_page_views ("client_id", "user_id");
        """)
        # refresh materialized view after data initiation if needed
        # db.engine.execute('REFRESH MATERIALIZED VIEW CONCURRENTLY utility.user_page_views;')
        cls.mock_redis = mock_strict_redis_client()

        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }

    @classmethod
    def tearDownClass(cls):
        db.engine.execute("DROP MATERIALIZED VIEW IF EXISTS utility.user_page_views;")
        super(SharedLibraryMicroReportTest, cls).tearDownClass()

    @classmethod
    def init_data(cls):
        s = db.session()
        client = s.query(Client).get(83)
        cls.parent_org_id = 711
        cls.child_org_id = 712
        cls.library_org = SharedLibraryFactory(
            id=cls.parent_org_id,
            user_concurrent_borrowing_limit=10,
            reborrowing_cooldown=timedelta(days=0),
            borrowing_time_limit=cls.max_borrowed_time)
        cls.library_org.clients.append(client)
        cls.child_org = SharedLibraryFactory(
            id=cls.child_org_id,
            parent_organization=cls.library_org)
        cls.child_org2 = SharedLibraryFactory(
            id=713,
            parent_organization=cls.library_org)
        user_manager = UserFactory(id=12340, created=get_local())
        cls.user = UserFactory(id=12345, created=get_local())
        cls.user2 = UserFactory(id=10346, created=datetime(2015, 1, 1))
        cls.user3 = UserFactory(id=103467, created=datetime(2015, 1, 1))
        s.add(
            OrganizationUser(
                organization_id=cls.library_org.id,
                user_id=user_manager.id,
                is_manager=True
            ))
        s.add(
            OrganizationUser(
                organization_id=cls.library_org.id,
                user_id=cls.user.id,
                is_manager=True
            ))

        s.commit()

        cls.child_org.users.append(cls.user)
        cls.child_org.users.append(cls.user2)
        cls.child_org2.users.append(cls.user3)

        cls.item1 = ItemFactory(id=1)
        author = AuthorFactory()
        cls.item1.authors.append(author)
        author = AuthorFactory()
        cls.item1.authors.append(author)
        cls.item2 = ItemFactory(id=2)
        # item 3, item not borrowed yet
        cls.item3 = ItemFactory(id=3)
        # item 4, item is borrowed and already returned, should not shown in list of borrowed items
        cls.item4 = ItemFactory(id=4)

        shared_lib_item1 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item1,
            quantity=10,
            quantity_available=10,
            item_name_override='{} additional title'.format(cls.item1.name)
        )
        shared_lib_item2 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item2,
            quantity=10,
            quantity_available=7,
            item_name_override='{} additional title'.format(cls.item2.name)
        )
        shared_lib_item3 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item3,
            quantity=10,
            quantity_available=8,
            item_name_override='{} additional title'.format(cls.item3.name)
        )
        shared_lib_item4 = SharedLibraryItemFactory(
            shared_library=cls.library_org,
            item=cls.item4,
            quantity=10,
            quantity_available=4,
            item_name_override='{} additional title'.format(cls.item4.name)
        )
        cls.catalog = CatalogFactory(id=1,shared_library=cls.library_org,)
        catalog_item1 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item1, )
        catalog_item2 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item2, )
        catalog_item3 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item3, )
        catalog_item4 = CatalogItemFactory(catalog=cls.catalog, shared_library_item=shared_lib_item4, )

        page_view1 = cls.create_page_view(cls.user.id, cls.item1.id, duration=100 * cls.user.id)
        page_view2 = cls.create_page_view(cls.user2.id, cls.item1.id, duration=100 * cls.user2.id)
        page_view3 = cls.create_page_view(cls.user3.id, cls.item1.id, duration=100 * cls.user3.id)
        s.add(page_view1)
        s.add(page_view2)
        s.add(page_view3)

        user_download1 = cls.create_user_download(cls.user.id, cls.item1.id)
        user_download2 = cls.create_user_download(cls.user2.id, cls.item2.id)
        # this should not get selected on filter by child org, cause belong to different child org
        user_download3 = cls.create_user_download(cls.user3.id, cls.item3.id)
        s.add(user_download1)
        s.add(user_download2)
        s.add(user_download3)

        user_borrowed1 = cls.create_user_borrowed_item(cls.user, catalog_item1)
        user_borrowed2= cls.create_user_borrowed_item(cls.user2, catalog_item2)
        # this should not get selected on filter by child org, cause belong to different child org
        user_borrowed3 = cls.create_user_borrowed_item(cls.user3, catalog_item3)

        s.commit()

    @classmethod
    def create_user_borrowed_item(cls, user, catalog_item1):
        return UserBorrowedItemFactory(
            user=user,
            catalog_item=catalog_item1,
            borrowing_start_time=datetime.utcnow() - timedelta(days=3)
        )

    @classmethod
    def create_page_view(cls, user_id, item_id, duration):
        return UserPageview(user_activity={
            'device_id': _fake.pyint(),
            'user_id': user_id,
            "item_id": item_id,
            'session_name': _fake.name(),
            "ip_address": _fake.ipv4(),
            "page_orientation": random.choice(ORIENTATION_TYPES),
            "page_number": [random.randint(0, 100), random.randint(0, 100)],
            "duration": duration,
            "client_id": 83,
            "online_status": random.choice(ONLINE_TYPES),
            "location": "{} {}".format(
                _fake.geo_coordinate(),
                _fake.geo_coordinate()),
            "client_version": _fake.word(),
            "os_version": _fake.word(),
            "device_model": _fake.word(),
            "datetime": datetime_to_isoformat(datetime.now() - timedelta(days=1)),
            "datetime_epoch": time.mktime((datetime.now() - timedelta(days=1)).timetuple()),
        })

    @classmethod
    def create_user_download(self, user_id, item_id):
        return UserDownload(user_activity={
            'device_id': _fake.pyint(),
            'user_id': user_id,
            "item_id": item_id,
            'session_name': _fake.name(),
            "ip_address": _fake.ipv4(),
            "client_id": 83,
            "online_status": random.choice(ONLINE_TYPES),
            "location": "{} {}".format(
                _fake.geo_coordinate(),
                _fake.geo_coordinate()),
            "client_version": _fake.word(),
            "os_version": _fake.word(),
            "device_model": _fake.word(),
            "datetime": datetime_to_isoformat(datetime.now() - timedelta(days=1)),
            "datetime_epoch": time.mktime((datetime.now() - timedelta(days=1)).timetuple()),
        })

    @mock.patch('app.users.helpers.requests.get', mock.Mock(side_effect=mocked_requests_get))
    def test_get_shared_library_users(self):
        expected = get_expected_user_response(711)

        with app.app_context():
            current_app.kvs = self.mock_redis
            client = app.test_client(use_cookies=False)
            response = client.get('/v1/organizations/711/users?limit=20&offset=0')
            if response.status_code == OK:
                actual = json.loads(response.data)
                self._assert_list_equal(expected, actual)
            self.assertEqual(response.status_code, OK)

    def test_get_average_user_read_duration_per_day(self):
        expected = {"count": 1234500}
        with app.app_context():
            current_app.kvs = self.mock_redis
            client = app.test_client(use_cookies=False)
            response = client.get(
                '/v1/reports/eperpus/711/user-average-daily-reading-time?'
                'user_id={user_id}&since=P30D&duration=P0Y0M0DT0H15M0S&client_id=83&client_id=84'.format(
                    user_id=self.user.id)
            )
            if response.status_code == OK:
                actual = json.loads(response.data)
                self.assertDictEqual(actual, expected)
            self.assertEqual(response.status_code, OK, response.data)

    def test_get_total_user_read_based_on_duration(self):
        expected = {"count": 3}
        with app.app_context():
            current_app.kvs = self.mock_redis
            client = app.test_client(use_cookies=False)
            response = client.get('/v1/reports/eperpus/711/users-count-over-reading-duration?'
                                  'since=P30D&duration=P0Y0M0DT0H15M0S&client_id=83&client_id=84')
            if response.status_code == OK:
                actual = json.loads(response.data)
                self.assertDictEqual(actual, expected)
            self.assertEqual(response.status_code, OK)

    def test_get_total_new_user(self):
        expected = {"count": 2}
        with app.app_context():
            current_app.kvs = self.mock_redis
            client = app.test_client(use_cookies=False)
            response = client.get('/v1/reports/eperpus/711/new-user-count?created_since=P7D')
            if response.status_code == OK:
                actual = json.loads(response.data)
                self.assertDictEqual(actual, expected)
            self.assertEqual(response.status_code, OK)

    def test_get_total_current_active_user(self):
        expected = {"count": 3}
        with app.app_context():
            current_app.kvs = self.mock_redis
            client = app.test_client(use_cookies=False)
            response = client.get('/v1/reports/eperpus/711/current-active-users?active_since=P7D')
            if response.status_code == OK:
                actual = json.loads(response.data)
                self.assertDictEqual(actual, expected)
            self.assertEqual(response.status_code, OK)

    def test_get_user_read_statistic(self):
        expected = {u'data': {
                        u'last_24_hours': [
                            {u'count': 1, u'hours': 343.0},
                            {u'count': 1, u'hours': 288.0},
                            {u'count': 1, u'hours': 2875.0}
                        ],
                        u'one_week_ago': [
                            {u'count': 1, u'hours': 343.0},
                            {u'count': 1, u'hours': 288.0},
                            {u'count': 1, u'hours': 2875.0}
                        ],
                        u'two_weeks_ago': [
                            {u'count': 1, u'hours': 343.0},
                            {u'count': 1, u'hours': 288.0},
                            {u'count': 1, u'hours': 2875.0}
                        ]}
        }
        with app.app_context():
            current_app.kvs = self.mock_redis
            client = app.test_client(use_cookies=False)
            response = client.get('/v1/reports/eperpus/711/user-read-statistics')
            if response.status_code == OK:
                actual = json.loads(response.data)
                self.assertDictEqual(actual, expected)
            self.assertEqual(response.status_code, OK)

    def test_most_download_items(self):
        with self.on_session(self.library_org, self.item1, self.item2, self.item3):
            session = db.session()
            expected_items = [self.item1, self.item2, self.item3]
            expected = self.expected_most_download_item_response(expected_items, session)
            expected_items_by_child_org = [self.item1, self.item2]
            expected_by_child_org = self.expected_most_download_item_response(expected_items_by_child_org, session)

        with app.app_context():
            current_app.kvs = self.mock_redis
            client = app.test_client(use_cookies=False)
            # get most downloaded items for parent organization
            response = client.get('/v1/reports/eperpus/{}/most-downloaded-items'.format(
                self.parent_org_id
            ))
            self.assertEqual(response.status_code, OK, response.data)
            actual = json.loads(response.data)
            self.assertEqual(len(actual.get('data')), len(expected_items))
            self.assertUnorderedListEqual(actual.get('data'), expected.get('data'), 'item_id')

            # get most downloaded items only for user that registered in child organization 712
            response = client.get('/v1/reports/eperpus/{main_id}/most-downloaded-items?org_id={child_id}'.format(
                main_id=self.parent_org_id,
                child_id=self.child_org_id
            ))
            self.assertEqual(response.status_code, OK, response.data)
            actual2 = json.loads(response.data)
            self.assertEqual(len(actual2.get('data')), len(expected_items_by_child_org))
            self.assertUnorderedListEqual(actual2.get('data'), expected_by_child_org.get('data'), 'item_id')

    def test_top_downloaders(self):
        with self.on_session(self.user, self.user2, self.user3):
            def get_expected(users):
                return {
                    'data': [{
                        "user_id": user.id,
                        'download_count': 1,
                        'username': user.username + ' - ' + user.first_name + ' ' + user.last_name
                    } for user in users]
                }

            expected_by_parent_org = get_expected([self.user, self.user2, self.user3, ])
            expected_by_child_org = get_expected([self.user, self.user2, ])

        with app.app_context():
            current_app.kvs = self.mock_redis
            client = app.test_client(use_cookies=False)
            # test get top downloaders by parent organization
            response = client.get('/v1/reports/eperpus/{}/top-downloaders'.format(
                self.parent_org_id
            ))
            self.assertEqual(response.status_code, OK, response.data)
            actual = json.loads(response.data)
            self.assertEqual(len(actual.get('data')), len(expected_by_parent_org.get('data')))
            self.assertUnorderedListEqual(actual.get('data'), expected_by_parent_org.get('data'), 'user_id')

            # test get top downloaders by child organization
            response = client.get('/v1/reports/eperpus/{}/top-downloaders?limit=10&org_id={}'.format(
                self.parent_org_id, self.child_org_id
            ))
            self.assertEqual(response.status_code, OK, response.data)
            actual = json.loads(response.data)
            self.assertEqual(len(actual.get('data')), len(expected_by_child_org.get('data')))
            self.assertUnorderedListEqual(actual.get('data'), expected_by_child_org.get('data'), 'user_id')

            # run again with the same query, should get from cache
            response = client.get('/v1/reports/eperpus/{}/top-downloaders?limit=10&org_id={}'.format(
                self.parent_org_id, self.child_org_id
            ))
            self.assertEqual(response.status_code, OK, response.data)
            actual = json.loads(response.data)
            self.assertEqual(len(actual.get('data')), len(expected_by_child_org.get('data')))

    def test_user_total_borrowing_count(self):
        # v1/reports/eperpus/723/user-total-borrowing-count?user_id=5
        with self.on_session(self.user):
            user_id = self.user.id
        with app.app_context():
            current_app.kvs = self.mock_redis
            client = app.test_client(use_cookies=False)
            # get top reading times by parent organization
            response = client.get('/v1/reports/eperpus/{}/user-total-borrowing-count?user_id={}'.format(
                self.parent_org_id, user_id
            ))
            self.assertEqual(response.status_code, OK, response.data)

    def test_top_reading_times(self):
        with self.on_session(self.user, self.user2, self.user3):
            def get_expected(users):
                return {
                    'data': [{
                        "user_id": user.id,
                        'reading_seconds': 100 * user.id,
                        'username': user.username + ' - ' + user.first_name + ' ' + user.last_name
                    } for user in users]
                }

            expected_by_parent_org = get_expected([self.user, self.user2, self.user3, ])
            expected_by_child_org = get_expected([self.user, self.user2, ])

        with app.app_context():
            current_app.kvs = self.mock_redis
            client = app.test_client(use_cookies=False)
            # get top reading times by parent organization
            response = client.get('/v1/reports/eperpus/{}/top-reading-times'.format(
                self.parent_org_id
            ))
            self.assertEqual(response.status_code, OK, response.data)
            actual = json.loads(response.data)
            self.assertEqual(len(actual.get('data')), len(expected_by_parent_org.get('data')))
            self.assertUnorderedListEqual(actual.get('data'), expected_by_parent_org.get('data'), 'user_id')

            # get top reading times by child organization
            response = client.get('/v1/reports/eperpus/{}/top-reading-times?org_id={}'.format(
                self.parent_org_id,
                self.child_org_id
            ))
            self.assertEqual(response.status_code, OK, response.data)
            actual = json.loads(response.data)
            self.assertEqual(len(actual.get('data')), len(expected_by_child_org.get('data')))
            self.assertUnorderedListEqual(actual.get('data'), expected_by_child_org.get('data'), 'user_id')

    def assertUnorderedListEqual(self, actual, expected, key):
        for actual_dict in actual:
            for expected_dict in expected:
                if actual_dict[key] == expected_dict[key]:
                    self.assertDictContainsSubset(expected_dict, actual_dict)

    def expected_most_download_item_response(self, expected_items, session):
        org_items = session.query(SharedLibraryItem).filter(
            SharedLibraryItem.organization_id == self.library_org.id,
            SharedLibraryItem.item_id.in_([item.id for item in expected_items])).all()
        data = []
        for org_item in org_items:
            data.append({
                u'item_id': org_item.item.id,
                u'item_name': org_item.item.name,
                u'count': 1,
                'authors': ', '.join([author.name for author in org_item.item.authors]),
                'quantity': org_item.quantity if org_item else None,
                'quantity_available': org_item.quantity_available if org_item else None
            })
        expected = {u'data': data}
        return expected

    def test_purge_cache(self):
        client = app.test_client(use_cookies=False)
        response = client.open(
            '/v1/reports/eperpus/{}'.format(self.parent_org_id), method='PURGE')
        self.assertEqual(response.status_code, NO_CONTENT,
                         'Response != 204 (NO CONTENT). Response: {}'.format(response.data))


def get_mocked_user_request_get(org_id):
    users = []
    session = db.session()
    org = session.query(Organization).get(org_id)
    for user in org.users:
        users.append(get_user_response(user))
    # for user in self.child_org.users.all():
    #     users.append(get_user_response(user))
    response = {
        "response": {
            "docs": users,
            "numFound": len(users),
        },
        "facet_counts": {
            "facet_queries": {},
            "facet_fields": {
                "is_active": [
                    True,
                    2,
                    False,
                    0
                ]
            },
            "facet_ranges": {},
            "facet_intervals": {},
            "facet_heatmaps": {}
        },
        "spellcheck": {
            "suggestions": [
                "spell_test", {
                    "numFound": 2,
                    "startOffset": 0,
                    "endOffset": 7,
                    "origFreq": 0,
                    "suggestion": [
                        {
                            "word": "spell_test1",
                            "freq": 8
                        },
                        {
                            "word": "spell_test2",
                            "freq": 1
                        }
                    ]
                }
            ],
            "correctlySpelled": 'false'
        }
    }
    session.close()
    return response


def get_expected_user_response(org_id):
    users = []
    session = db.session()
    org = session.query(Organization).get(org_id)
    for user in org.users:
        users.append(get_user_response(user))
    # for user in self.child_org.users.all():
    #     users.append(get_user_response(user))
    expected = {
        "users": users,
        "metadata": {
            "resultset": {
                "count": len(users),
                "limit": 20,
                "offset": 0
            },
            "facets": [{
                "field_name": "is_active",
                "values": [
                    {"value": True,
                     "count": 2
                    },
                    {"value": False,
                     "count": 0
                     }
                ]
                },
            ],
            "spelling_suggestions": [
                {
                    "value": "spell_test1",
                    "count": 8,
                },
                {
                    "value": "spell_test2",
                    "count": 1,
                }
            ]
        },
    }
    session.close()
    return expected


def get_user_response(user):
    return {
        "id": user.id,
        "username": user.username,
        "email": user.email,
        "first_name": user.first_name,
        "last_name": user.last_name,
        "is_active": user.is_active,
        "note": user.note,
        "signup_date": str(isoformat(user.created)),
        # "organizations": [
        #     {
        #         "id": org.id,
        #         "name": org.name,
        #     } for org in user.organizations.all()]
    }


def get_item_response(local_app, item, org_item):
    return {
        "id": item.id,
        "title": org_item.item_name_override if org_item.item_name_override else item.name,
        "details": {
            "href": "http://localhost/v1/items/{}".format(item.id),
            "title": "Detil Item"
        },
        "cover_image": {
            "href": local_app.config['MEDIA_BASE_URL'] + item.image_normal,
            "title": "Gambar Sampul"
        },
        "currently_available": org_item.quantity_available,
        "total_in_collection": org_item.quantity,
        "max_borrow_time": "P0Y0M7DT0H0M0S",
        # "max_borrow_time": "P{0.years}Y{0.months}M{0.days}DT{0.hours}H{0.minutes}M{0.seconds}S".format(
        #     org_item.shared_library.borrowing_time_limit),
        "vendor": {
            "href": "http://localhost/v1/vendors/{}".format(item.brand.vendor.id),
            "title": item.brand.vendor.name
            }
    }


def init_g_current_user():
    g.user = UserInfo(username='dfdsafsdf', user_id=12345, token='abcd', perm=['can_read_write_global_all',])
    session = db.session()
    g.current_user = session.query(User).get(12345)
    session.close()


_fake = Factory.create()

