import json
from datetime import timedelta, datetime

from nose.tools import nottest
from requests import codes

from app import db
from app.auth.models import Client
from app.eperpus.tests.fixtures import SharedLibraryFactory
from app.users.organizations import OrganizationUser
from app.users.tests.fixtures import UserFactory
from app.utils.datetimes import get_local
from app.utils.testcases import TestBase


@nottest
class ReportWithDateRangeTests(TestBase):

    max_borrowed_time = timedelta(days=7)

    @classmethod
    def setUpClass(cls):
        super(ReportWithDateRangeTests, cls).setUpClass()
        cls.init_data()

    @classmethod
    def init_data(cls):
        s = db.session
        client = s.query(Client).get(83)
        cls.parent_org_id = 711
        cls.child_org_id = 712
        cls.library_org = SharedLibraryFactory(
            id=cls.parent_org_id,
            user_concurrent_borrowing_limit=10,
            reborrowing_cooldown=timedelta(days=0),
            borrowing_time_limit=cls.max_borrowed_time)
        cls.library_org.clients.append(client)
        cls.child_org = SharedLibraryFactory(
            id=cls.child_org_id,
            parent_organization=cls.library_org)
        cls.child_org2 = SharedLibraryFactory(
            id=713,
            parent_organization=cls.library_org)
        cls.user = UserFactory(id=12345, created=get_local())
        cls.user2 = UserFactory(id=10346, created=datetime(2015, 1, 1))
        cls.user3 = UserFactory(id=103467, created=datetime(2015, 1, 1))
        s.add(
            OrganizationUser(
                organization_id=cls.library_org.id,
                user_id=cls.super_admin.id,
                is_manager=True
            ))
        s.add(
            OrganizationUser(
                organization_id=cls.library_org.id,
                user_id=cls.user.id,
                is_manager=True
            ))

        s.commit()

    def assert_response(self, url, expected):
        self.set_api_authorized_user(self.super_admin)
        headers = self.build_headers()
        headers['Cache-Control'] = 'no-cache'
        response = self.api_client.get(url, headers=headers)
        self.assertEqual(response.status_code, codes.ok, "Response status code {} != 200(OK). response: {}".format(
            response.status_code, response.data))
        if expected:
            actual = json.loads(response.data)
            self._assert_dict_contain_subset(expected, actual)

    def test_item_borrowed_chart(self):
        expected = {
            "data": [
                {"group1": '2016-03-04', "value": 3},
                {"group1": '2016-03-05', "value": 1},
                {"group1": '2016-03-06', "value": 2},
            ],
            "data_compare": [
                {"group1": '2016-02-02', "value": 2},
                {"group1": '2016-02-03', "value": 1},
                {"group1": '2016-02-04', "value": 3},
            ]
        }
        self.assert_response(
            url='/v1/reports/eperpus/711/item-borrowed-chart?'
                'start-date=2016-03-01T00:00:00Z&end-date=2016-03-10T00:00:00Z'
                '&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z&end-date2=2016-02-10T00:00:00Z',
            expected=None)

    def test_item_borrowed_top_n(self):
        expected = {
            "data": [
                {"id": 1, "group1": 'item a', "value": 13},
                {"id": 2, "group1": 'item b', "value": 12},
                {"id": 3, "group1": 'item ab', "value": 10},
                {"id": 4, "group1": 'item bce', "value": 9},
                {"id": 5, "group1": 'item abc', "value": 7},
                {"id": 6, "group1": 'item bdd', "value": 5},
                {"id": 7, "group1": 'item aee', "value": 4},
                {"id": 8, "group1": 'item bkk', "value": 2},
            ],
            "data_compare": [
                {"id": 5, "group1": 'item abc', "value": 15},
                {"id": 11, "group1": 'item bdd', "value": 14},
                {"id": 12, "group1": 'item abe', "value": 10},
                {"id": 13, "group1": 'item bee', "value": 8},
                {"id": 14, "group1": 'item ak', "value": 6},
                {"id": 15, "group1": 'item be', "value": 4},
                {"id": 17, "group1": 'item aefe', "value": 2},
                {"id": 19, "group1": 'item bkk', "value": 2},
            ]
        }
        # test select top N , N = 2
        self.assert_response(
            url='/v1/reports/eperpus/711/item-borrowed-top-n?start-date=2016-02-01T00:00:00Z&'
                'end-date=2016-02-10T00:00:00Z&limit=2',
            expected=None)
        # test select top N , N = 2, with compare
        self.assert_response(
            url='/v1/reports/eperpus/711/item-borrowed-top-n?'
                'start-date=2016-03-01T00:00:00Z&end-date=2016-03-10T00:00:00Z'
                '&limit=2&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z&end-date2=2016-02-10T00:00:00Z',
            expected=None)

    def test_active_users_summary(self):
        sub_compare_value = 11123
        sub_value = 12345
        expected = {
            'main_value': '{0:.2f} %'.format(((float(sub_value) - float(sub_compare_value))
                                              / float(sub_compare_value)) * 100),
            'sub_value': '{:,}'.format(sub_value),
            'sub_compare_value': '{:,}'.format(sub_compare_value)
        }
        self.assert_response(
            url='/v1/reports/eperpus/711/active-users-summary?'
                'start-date=2016-03-01T00:00:00Z'
                '&end-date=2016-03-10T00:00:00Z'
                '&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z'
                '&end-date2=2016-02-10T00:00:00Z',
            expected=None)

    def test_active_users_summary_avg_reading(self):
        sub_compare_value = 21123
        sub_value = 22345
        expected = {
            'main_value': '{0:.2f} %'.format(((float(sub_value) - float(sub_compare_value))
                                              / float(sub_compare_value)) * 100),
            'sub_value': '{:,}'.format(sub_value),
            'sub_compare_value': '{:,}'.format(sub_compare_value)
        }
        self.assert_response(
            url='/v1/reports/eperpus/711/active-users-summary-avg-reading?'
                'start-date=2016-03-01T00:00:00Z'
                '&end-date=2016-03-10T00:00:00Z'
                '&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z'
                '&end-date2=2016-02-10T00:00:00Z',
            expected=None)

    def test_active_users_chart(self):
        expected = {
            "data": [
                {"group1": '2016-03-04', "value": 3},
                {"group1": '2016-03-05', "value": 1},
                {"group1": '2016-03-06', "value": 2},
                {"group1": '2016-03-07', "value": 3},
                {"group1": '2016-03-08', "value": 4},
                {"group1": '2016-03-09', "value": 6},
                {"group1": '2016-03-10', "value": 6},
            ],
            "data_compare": [
                {"group1": '2016-02-04', "value": 1},
                {"group1": '2016-02-05', "value": 3},
                {"group1": '2016-02-06', "value": 5},
                {"group1": '2016-02-07', "value": 2},
                {"group1": '2016-02-08', "value": 3},
                {"group1": '2016-02-09', "value": 1},
                {"group1": '2016-02-10', "value": 2},
            ]
        }

        self.assert_response(
            url='/v1/reports/eperpus/711/active-users-chart?'
                'start-date=2016-03-01T00:00:00Z'
                '&end-date=2016-03-10T00:00:00Z'
                '&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z'
                '&end-date2=2016-02-10T00:00:00Z',
            expected=None)

    def test_active_users_top_n(self):
        expected = {
            "data": [
                {"id": 1, "group1": 'user a', "value": 13},
                {"id": 2, "group1": 'user b', "value": 12},
                {"id": 3, "group1": 'user ab', "value": 10},
                {"id": 4, "group1": 'user bce', "value": 9},
                {"id": 5, "group1": 'user abc', "value": 7},
                {"id": 6, "group1": 'user bdd', "value": 5},
                {"id": 7, "group1": 'user aee', "value": 4},
                {"id": 8, "group1": 'user bkk', "value": 2},
            ],
            "data_compare": [
                {"id": 5, "group1": 'user abc', "value": 15},
                {"id": 11, "group1": 'user bdd', "value": 14},
                {"id": 12, "group1": 'user abe', "value": 10},
                {"id": 13, "group1": 'user bee', "value": 8},
                {"id": 14, "group1": 'user ak', "value": 6},
                {"id": 15, "group1": 'user be', "value": 4},
                {"id": 17, "group1": 'user aefe', "value": 2},
                {"id": 19, "group1": 'user bkk', "value": 2},
            ]
        }
        self.assert_response(
            url='/v1/reports/eperpus/711/active-users-top-n?'
                'start-date=2016-03-01T00:00:00Z'
                '&end-date=2016-03-10T00:00:00Z'
                '&limit=2&is-compare=1'
                '&start-date2=2016-02-01T00:00:00Z'
                '&end-date2=2016-02-10T00:00:00Z',
            expected=None)

