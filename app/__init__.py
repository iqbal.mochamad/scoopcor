from __future__ import absolute_import, unicode_literals

import logging
import os
from base64 import b64encode
from httplib import BAD_REQUEST
from traceback import format_exc

import redis
from flask import Flask, g, request, jsonify
from flask_babel import Babel
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import MetaData
from werkzeug.exceptions import HTTPException

from elasticapm.contrib.flask import ElasticAPM
from elasticapm.handlers.logging import LoggingHandler

from .applog import initialize_logging
from .startup_checks import startup_checks
from .utils.blueprint_loader import BlueprintLoader
from .utils.postgresql import relativedelta_interval
from .utils.sqlalchemy import INDEX_NAMING_CONVENTIONS
from .utils.useragent_helpers import UserAgentMeta

if os.path.isfile('/srv/scoopcor/newrelic.ini'):
    import newrelic.agent

    newrelic.agent.initialize('/srv/scoopcor/newrelic.ini')

__version__ = '2.38.11'

# Before starting up the application, we need to register our psycopg2 types
relativedelta_interval.register()

app = Flask(__name__)
app.config.from_object('config')

# flask extensions
babel = Babel(app)
db = SQLAlchemy(app, metadata=MetaData(naming_convention=INDEX_NAMING_CONVENTIONS))

ma = Marshmallow(app)

try:
    app.kvs = redis.StrictRedis(
        host=app.config['REDIS_HOST'],
        port=app.config['REDIS_PORT'],
        db=app.config['REDIS_DB'],
        password=app.config['REDIS_DB_PASS']
    )
except redis.exceptions.RedisError:
    app.logger.exception("Could not connect to redis.")
    raise

apm = ElasticAPM(app)

@app.before_request
def set_request_id():
    """ Randomly generates an int and base64 encodes it on our global request context.

    This is only so we can grep an individual request through our logfiles
    (while multiple are happening at the same time).
    """
    try:
        g.request_id = b64encode(os.urandom(4))
    except RuntimeError:
        pass


@app.before_request
def detect_client():
    """ Detects the Client that was used to make this request.

    If client detection fails, current_client will be set to None.
    """
    from app.auth.models import Client
    from app.utils.useragent_helpers import construct_useragent_meta, UnparsableUserAgent

    session = db.session()

    try:

        ua = request.headers.get('X-Scoop-Client') or request.headers.get('X-User-Agent') or request.user_agent.string
        g.client = construct_useragent_meta(ua)

        req_client = (
            session.query(Client).filter_by(app_name=g.client.app_name).first())

        if not req_client:
            raise UnparsableUserAgent(ua)

        session.expunge(req_client)

        g.current_client = req_client

    except UnparsableUserAgent:
        g.client = getattr(g, 'client', None)
        g.current_client = getattr(g, 'current_client', None)


@app.before_request
def auth_user():
    from app.auth.helpers import authenticate_user_on_session
    authenticate_user_on_session()


@babel.localeselector
def get_locale():
    # if a user is logged in, use the locale from the user settings
    # user = getattr(g, 'current_user', None)
    # if user is not None:
    #     return user.locale

    # otherwise try to guess the language from the user accept header
    #  the browser transmits.
    # We support id/en in this app. The best match wins.
    # translations = [str(translation) for translation in babel.list_translations()]
    translations = ['id', 'en']
    selected_language = request.accept_languages.best_match(translations)
    return selected_language if selected_language else 'en'


@app.after_request
def hack_ios_payment_validate_error_format(response):
    client_info = getattr(g, 'client', None)
    if client_info:
        if client_info.app_name == "scoop_ios" and \
            client_info.version == '4.8.0' and \
            request.path == '/v1/payments/validate' and \
                response.status_code == BAD_REQUEST:
            import json
            import six
            response_json = json.loads(response.data)
            response_json['error_code'] = six.text_type(response_json['error_code'])
            response.data = json.dumps(response_json)
    return response


@app.after_request
def handle_unauthorized_error(response):
    from app.auth.helpers import reformat_unauthorized_error
    return reformat_unauthorized_error(response)


@app.after_request
def after_request(response):
    # commits any pending transactions
    db.session.commit()
    return response


@app.after_request
def log_request(response):
    logger = logging.getLogger('eperpus')

    message = "{method} {path} {status}".format(method=request.method, path=request.path, status=response.status_code)

    logger.info(message)
    return response


@app.errorhandler(Exception)
def on_any_error(error):
    # rollback and pending transactions
    db.session().rollback()

    error_logger = logging.getLogger('scoopcor.errors')

    if isinstance(error, HTTPException):
        error_logger.debug('Raised an HTTP error.  This is probably normal.', exc_info=1)
        return error.get_response()
    else:
        error_body = {
            'user_message': 'Sorry, something went wrong!',
            'developer_message':
                'Scoopcor encountered some unknown error.  Give this to backend team: {}'.format(error.message),
            'status': 500,
            'error_code': 500
        }

        if app.debug:
            error_body['traceback'] = format_exc()

        error_response = jsonify(error_body)
        error_response.status_code = 500
        error_logger.exception('Something went badly wrong')
        # ElasticAPM(app, logging=logging.ERROR)
        return error_response


if not app.testing:
    initialize_logging(app)

loader = BlueprintLoader(__package__)
blueprint_modules = [

    'app.depreciated.blueprints',
    'app.discounts.blueprints',
    'app.general.blueprints',
    'app.locales.blueprints',
    'app.parental_controls.blueprints',
    'app.customer_services.blueprints',

    'app.paymentgateway_connectors.blueprints',
    'app.paymentgateway_connectors.apple_iap.v3.restore',
    'app.payments.blueprints',
    'app.orders.blueprints',
    'app.items.blueprints',
    'app.items.files',
    'app.master.blueprints',
    'app.migrate_data.blueprints',

    'app.payment_gateways.blueprints',
    'app.points.blueprints',

    'app.revenue_shares.blueprints',
    'app.tiers.blueprints',
    'app.analytics.blueprints',
    'app.remote_publishing.blueprints',
    'app.offers.blueprints',
    'app.currency_quotes.blueprints',

    'app.users.user_level',
    'app.users.users',
    'app.users.signup_checks',
    'app.users.organizations',
    'app.users.buffets',
    'app.users.wallet',
    'app.users.items',
    'app.users.roles',
    'app.users.api',

    'app.eperpus.libraries',
    'app.eperpus.catalogs',
    'app.eperpus.members',
    'app.eperpus.scripts',
    'app.eperpus.watch_list',
    'app.eperpus.clients',
    'app.eperpus.categories',
    'app.eperpus.transactions',
    'app.eperpus.client_library',

    'app.auth.blueprints',
    'app.uploads.blueprints',
    'app.facebook_login_data.blueprints',
    'app.campaigns.blueprints',
    'app.logs.blueprints',

    'app.reports.eperpus',
    'app.reports.blacklist',

    'app.layouts.blueprints',
    'app.layouts.banners',
    'app.auth.facebook',

    'app.notification.api',
    'app.notification.ebooks.api',
    'app.audiobooks.blueprints',
    'app.eperpus.landing_page',
    'app.shared_client.blueprints',
    'app.paymentgateway_connectors.e2pay.api'
]

for module_name in blueprint_modules:
    mod = loader.load_blueprint_module(module_name)
    bps = loader.discover_blueprint_attributes(mod)
    for bp in bps:
        app.register_blueprint(bp)

startup_checks(app)

handler = LoggingHandler(client=apm.client)
handler.setLevel(logging.ERROR)
app.logger.addHandler(handler)
