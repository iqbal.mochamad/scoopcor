import os

from . import base, serializers

from flask_appsfoundry.parsers.converters import naive_datetime_from_iso8601

from datetime import datetime

_shim_data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')


class BaseQuery(object):

    def __init__(self, model_class):
        pass

    def all(self):
        pass

class Country(object):

    data_location = os.path.join(_shim_data_dir, 'core_countries.json')

    def __init__(self, **kwargs):
        self.id = kwargs.get('id')
        self.name = kwargs.get('name')
        self.description = kwargs.get('description')
        self.meta = kwargs.get('meta')
        self.slug = kwargs.get('slug')
        self.code = kwargs.get('code')
        self.icon_image_normal = kwargs.get('icon_image_normal')
        self.icon_image_highres = kwargs.get('icon_image_highres')
        self.sort_priority = kwargs.get('sort_priority')
        self.is_active = kwargs.get('is_active')
        self.created = naive_datetime_from_iso8601(kwargs.get('created', datetime.now().isoformat()))
        self.modified = naive_datetime_from_iso8601(kwargs.get('modified', datetime.now().isoformat()))

    @classmethod
    def query(cls):
        pass
