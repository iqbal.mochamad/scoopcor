import re

from flask_appsfoundry.parsers import ParserBase
from flask import request

class TupleOrderingOptionsParser(object):
    """
    Parses the ordering options and returns a list of tuples
    (field_name, 'asc'|'desc')

    NOTE: We cannot use anything derived from RequestParser or bad things
    will happen because the front end is sending Content-Type headers on GET
    requests.  Instead we're doing a little hacking here.
    """
    # def __init__(self, *args, **kwargs):
    #
    #     super(TupleOrderingOptionsParser, self).__init__(*args, **kwargs)
    #
    #     self.add_argument('order',
    #                       type=self.parse_ordering_opt,
    #                       store_missing=False,
    #                       action='append',)

    def parse_args(self, req=None):

        req = req or request

        order = req.args.getlist('order',
                                 type=self.parse_ordering_opt)

        return {'order': order} if order else { }

    def parse_ordering_opt(self, input):
        regex = re.compile(r"^(?P<desc>-)?(?P<field_name>.+)$")

        is_desc, field_name = regex.match(input).groups()

        return field_name, 'desc' if is_desc else 'asc'
