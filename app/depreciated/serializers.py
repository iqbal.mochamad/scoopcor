from flask import current_app

from flask_appsfoundry.serializers import SerializerBase, fields


class CountrySerializer(SerializerBase):

    def __init__(self, media_base_url=None, *args, **kwargs):

        super(CountrySerializer, self).__init__(*args, **kwargs)
        self.__serializer_fields__['media_base_url'] = fields.String(
            default=media_base_url or current_app.config['MEDIA_BASE_URL'])

    id = fields.Integer
    name = fields.String
    slug = fields.String
    sort_priority = fields.Integer
    code = fields.String
    description = fields.String
    meta = fields.String
    is_active = fields.Boolean
    icon_image_normal = fields.String
    icon_image_highres = fields.String


class ItemTypeSerializer(SerializerBase):

    id = fields.Integer
    name = fields.String
    meta = fields.String
    slug = fields.String
    description = fields.String
    sort_priority = fields.Integer
    is_active = fields.Boolean


class LanguageSerializer(SerializerBase):

    def __init__(self, media_base_url=None, *args, **kwargs):
        super(LanguageSerializer, self).__init__(*args, **kwargs)
        self.__serializer_fields__['media_base_url'] = fields.String(
        default=media_base_url or current_app.config['MEDIA_BASE_URL'])

    id = fields.Integer
    name = fields.String
    slug = fields.String
    sort_priority = fields.Integer
    is_active = fields.Boolean
    description = fields.String
    meta = fields.String
    icon_image_normal = fields.String
    icon_image_highres = fields.String
