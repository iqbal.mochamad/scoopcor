from flask import Blueprint
import flask_restful as restful


from .api import CountryListApi, CountryDetailApi, ItemTypeListApi, ItemTypeDetailApi, LanguageListApi, \
    LanguageDetailApi

country_blueprint = Blueprint('country', __name__, url_prefix='/v1/countries')
country_api = restful.Api(country_blueprint)
country_api.add_resource(CountryListApi, '')
country_api.add_resource(CountryDetailApi, '/<int:db_id>')


item_type_blueprint = Blueprint('item_type', __name__, url_prefix='/v1/item_types')
item_type_api = restful.Api(item_type_blueprint)
item_type_api.add_resource(ItemTypeListApi, '')
item_type_api.add_resource(ItemTypeDetailApi, '/<int:db_id>')


language_blueprint = Blueprint('language', __name__, url_prefix='/v1/languages')
language_api = restful.Api(language_blueprint)
language_api.add_resource(LanguageListApi, '')
language_api.add_resource(LanguageDetailApi, '/<int:db_id>')
