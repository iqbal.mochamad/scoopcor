import json
from copy import copy

from flask import request
from flask.views import MethodView
from flask_restful import Resource, fields, marshal
from flask_appsfoundry import controllers

from werkzeug.exceptions import NotFound, NotImplemented


from . import viewmodels, parsers


class ShimDetailsApiBase(controllers.ErrorHandlingApiMixin, Resource):

    methods = ('get', 'put', 'delete', 'head', 'options', )

    DATA_LOCATION = None

    SERIALIZER = None

    _data = None

    @property
    def data(self):
        if self._data is None:
            self._data = json.load(open(self.DATA_LOCATION))
        return self._data

    def _get_serializer(self):

        serializer = self.SERIALIZER

        if isinstance(self.SERIALIZER, type):
            serializer = self.SERIALIZER()

        return serializer

    def get(self, db_id):
        record = filter(lambda row: row['id'] == db_id, self.data)

        if not record:
            raise NotFound

        return marshal(record[0], self._get_serializer())

    def put(self, db_id):
        raise NotImplemented(description="This API endpoint has been depreciated")

    def delete(self, db_id):
        raise NotImplemented(description="This API endpoint has been depreciated")


class ShimListApiBase(controllers.ErrorHandlingApiMixin, MethodView):

    methods = ('get', 'post', 'head', 'options', )

    DATA_LOCATION = None

    SERIALIZER = None

    SERIALIZED_LIST_NAME = 'objects'

    _data = None

    @property
    def data(self):
        if self._data is None:
            self._data = json.load(open(self.DATA_LOCATION))
        return self._data

    def _get_list_viewmodel(self, list_name, elements):
        vm = viewmodels.ShimPaginatedViewmodel(elements, list_name)
        return vm

    def _get_list_serializer(self, list_name, element_serializer):
        """
        Returns the listview serializer that will be used for marshalling
        our return data in the expected format.

        :param str list_name: The name that the results will be stored under
        :param element_serializer: A serializer that will be used for the
            individual elements in the results.
        :return: A dictionary serializer for use with flask-restful's marshal
        :rtype: dict
        """
        serializer = {
            'metadata': fields.Nested({
                'resultset': fields.Nested({
                    'offset': fields.Integer,
                    'limit': fields.Integer,
                    'count': fields.Integer,
                })
            }),
            list_name: fields.List(fields.Nested(element_serializer)),
        }

        return serializer

    def _get_serializer(self):

        serializer = self.SERIALIZER

        if isinstance(self.SERIALIZER, type):
            serializer = self.SERIALIZER()

        return serializer

    def get(self):

        element_serializer = self._get_serializer()

        list_serializer = \
            self._get_list_serializer(self.SERIALIZED_LIST_NAME,
                                      element_serializer)

        displayed_data = list(copy(self.data))

        order_parser = parsers.TupleOrderingOptionsParser()
        ordering = order_parser.parse_args(req=request)
        if ordering:
            # only supporting ordering by a single field..
            # this is a depreciated api, so if we need more, we'll add it..
            sort_field, sort_order = ordering['order'][0]

            displayed_data.sort(
                cmp=lambda a, b: cmp(a[sort_field], b[sort_field]),
                reverse=True if sort_order == 'desc' else False)

        # note: the only filtering option the front end is using is 'is_active'
        # if we need more, we'll add them
        if request.args.get('is_active'):
            displayed_data = filter(lambda a: a['is_active'], displayed_data)

        if request.args.get('q'):
            search_data = request.args.get('q', 'kira here').title()
            displayed_data = filter(lambda a: a['name'] == search_data, displayed_data)

        list_vm = self._get_list_viewmodel(self.SERIALIZED_LIST_NAME, displayed_data)

        return marshal(list_vm, list_serializer)

    def post(self):
        raise NotImplemented(description="This API endpoint has been depreciated")
