"""
These are a simple set of Shim APIs that maintain interface compatibility
after dropping countries, items types and languages from our database.
"""
import os

from app.utils import controllers
from . import base, serializers

_shim_data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'data')


class CountryListApi(base.ShimListApiBase, controllers.ListAuthMixin):

    DATA_LOCATION = os.path.join(_shim_data_dir, 'core_countries.json')

    SERIALIZER = serializers.CountrySerializer

    SERIALIZED_LIST_NAME = 'countries'


class CountryDetailApi(base.ShimDetailsApiBase, controllers.DetailAuthMixin):

    DATA_LOCATION = os.path.join(_shim_data_dir, 'core_countries.json')

    SERIALIZER = serializers.CountrySerializer


class ItemTypeListApi(base.ShimListApiBase):

    DATA_LOCATION = os.path.join(_shim_data_dir, 'core_itemtypes.json')

    SERIALIZER = serializers.ItemTypeSerializer

    SERIALIZED_LIST_NAME = 'item_types'


class ItemTypeDetailApi(base.ShimDetailsApiBase):

    DATA_LOCATION = os.path.join(_shim_data_dir, 'core_itemtypes.json')

    SERIALIZER = serializers.ItemTypeSerializer


class LanguageListApi(base.ShimListApiBase):

    DATA_LOCATION = os.path.join(_shim_data_dir, 'core_languages.json')

    SERIALIZER = serializers.LanguageSerializer

    SERIALIZED_LIST_NAME = 'languages'


class LanguageDetailApi(base.ShimDetailsApiBase):

    DATA_LOCATION = os.path.join(_shim_data_dir, 'core_languages.json')

    SERIALIZER = serializers.LanguageSerializer

