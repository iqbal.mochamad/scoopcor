
class ShimPaginatedViewmodel(object):
    def __init__(self, results, results_attribute_name):


        self.metadata = {
            'resultset': {
                'limit': 20,
                'offset': 0,
                'count': len(results),
            }
        }

        setattr(self, results_attribute_name, results)

