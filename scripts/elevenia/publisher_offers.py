#!/usr/bin/env python
"""
Elevenia Items Publish Script
=============================

Synopsis
--------

This script will iterate over all the Items currently for sale in
our database, and publish all the Books + Magazines with single offers
that have NOT already been uploaded to the Elevenia API.

If the upload is successful, the results will be stored in the
core_remote_offers table.
"""
from __future__ import unicode_literals

import os
import sys
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir, os.pardir))

from collections import namedtuple
import logging
from datetime import datetime
from requests.exceptions import BaseHTTPError

from app import app, db
from app.items.models import Item
from app.items.choices.status import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.offers.models import Offer

from app.remote_publishing.models import RemoteOffer
from app.remote_publishing.choices.remote_data_location import ELEVENIA

from app.services.elevenia import Elevenia
from app.services.elevenia.helpers import get_delivery_template, \
    build_elevenia_product_from_scoop_offer
from app.services.elevenia.exceptions import NoMappableCategoriesError
from app.services.elevenia.entities.exceptions import ApiOperationFailure


def main():

    _log.warning("Started Elevenia Publishing Script")

    gateway = Elevenia(app.config["ELEVENIA_API_KEY"])
    delivery_template = get_delivery_template(gateway)

    session = db.session()

    publish_single_offers(delivery_template, gateway, session)

    publish_subscription_offers(delivery_template, gateway, session)


def publish_single_offers(delivery_template, gateway, session):
    for single_offer in get_eligible_single_offers(session):

        try:
            assert_offer_has_available_item(single_offer)

            remote_offer = filter(
                lambda ri: ri.remote_service == ELEVENIA,
                single_offer.remote_offers
            )

            if not remote_offer:
                # first time we're uploading this offer to elevenia
                elevenia_product = build_elevenia_product_from_scoop_offer(
                    single_offer, delivery_template, gateway
                )

                elevenia_product.save()

                record_remote_publishing(single_offer, elevenia_product)
            else:
                continue
                elevenia_product = build_elevenia_product_from_scoop_offer(
                    single_offer, delivery_template, gateway
                )
                elevenia_product.id = remote_offer[0].remote_id

                elevenia_product.save()
                elevenia_product.deactivate()

                record_remote_publishing(single_offer, elevenia_product)

        except ApiOperationFailure as e:
            # TODO: change this.. it's actually a problem.
            log_skipped_item(e, single_offer)
        except NoAvailableItemsForOffer as e:
            log_skipped_item(e, single_offer)
        except (NoMappableCategoriesError, BaseHTTPError,
                ValueError, Exception) as e:
            log_skipped_item(e, single_offer)


def get_eligible_single_offers(session):

    return session.query(Offer).filter(
        (Offer.offer_type_id == SINGLE_OFFER) &
        (Offer.offer_status == OFFER_READY_FOR_SALE))


def assert_offer_has_available_item(offer):

    item = offer.items.filter(
           (Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME]) &
           (Item.item_type.in_([u'magazine', u'book'])) &
           (Item.is_active == True)).first()

    if not item:
        raise NoAvailableItemsForOffer(
            "No valid items for offer ({0.id}) -- skipping".format(offer))


class NoAvailableItemsForOffer(Exception):
    pass


def publish_subscription_offers(delivery_template, gateway, session):
    for (subscription_offer, meta) in get_elibile_subscription_offers(session):

        try:
            remote_offer = filter(
                lambda ri: ri.remote_service == ELEVENIA,
                subscription_offer.remote_offers
            )

            if not remote_offer:
                elevenia_product = build_elevenia_product_from_scoop_offer(
                    subscription_offer, delivery_template, gateway
                )

                elevenia_product.save()

                record_remote_publishing(subscription_offer, elevenia_product)

        except ApiOperationFailure as e:
            log_skipped_item(e, subscription_offer)
        except NoAvailableItemsForOffer as e:
            log_skipped_item(e, subscription_offer)
        except (NoMappableCategoriesError, BaseHTTPError,
                ValueError, Exception) as e:
            log_skipped_item(e, subscription_offer)


def get_elibile_subscription_offers(session):

    EligibleSubOffer = namedtuple('EligibleSubOffer', ['friendly_name',
                                                       'offer_id'])

    eligible_offers_meta = [
        # Koran
        EligibleSubOffer('KOMPAS 3 Bulan', 486),
        EligibleSubOffer('Bisnis Indonesia 3 Bulan', 796),
        EligibleSubOffer('Koran TEMPO 3 Bulan', 830),
        EligibleSubOffer('Rakyat Merdeka 3 Bulan', 206),
        EligibleSubOffer('Jawa Pos 3 Bulan', 34996),
        EligibleSubOffer('Koran Kontan 3 Bulan', 35614),
        EligibleSubOffer('Pikiran Rakyat 3 Bulan', 483),
        EligibleSubOffer('Top Skor 3 Bulan', 41139),
        EligibleSubOffer('INVESTOR DAILY 3 Bulan', 42372),
        EligibleSubOffer('Media Indonesia 3 Bulan', 643),

        # Majalah
        EligibleSubOffer('POPULAR 6 Edisi', 634),
        EligibleSubOffer('FHM Indonesia 6 Edisi', 112),
        EligibleSubOffer('OTOMOTIF 24 Edisi', 845),
        EligibleSubOffer('Auto Bild 12 Edisi', 836),
        EligibleSubOffer('SWA 14 Edisi', 102),
        EligibleSubOffer('GATRA 26 Edisi', 477),
        EligibleSubOffer('Sinyal 14 Edisi', 449),
        EligibleSubOffer('Tabloid Rumah 14 Edisi', 451),
        EligibleSubOffer('MAXIM Indonesia 6 Edisi', 579),
        EligibleSubOffer('CHIP 7 Edisi', 457),
    ]

    return [(session.query(Offer).get(meta.offer_id), meta)
            for meta in eligible_offers_meta]


def log_skipped_item(exception, scoop_offer):
    if scoop_offer.offer_type_id == 1:
        if isinstance(exception, NoMappableCategoriesError):
            _log.warn(
                u"Could not publish Scoop Offer for Item: "
                u"{0.name} ({0.id}).  No valid category mappings!".format(
                    scoop_offer.items.first())
            )
        if isinstance(exception, ApiOperationFailure):
            _log.warn(
                "Could not publish Scoop Offer for Item: "
                "{0.name} ({0.id}) -- Elevenia Message: {1.message}, {1.code}".format(scoop_offer.items.first(), exception))
        else:
            _log.exception("Unknown exception type encountered")
    else:
        if isinstance(exception, NoMappableCategoriesError):
            _log.warn(
                u"Could not publish Scoop Offer for Offer: "
                u"{0.name} ({0.id}).  No valid category mappings!".format(
                    scoop_offer)
            )
        if isinstance(exception, ApiOperationFailure):
            _log.warn(
                "Could not publish Scoop Offer for Offer: "
                "{0.name} ({0.id}) -- Elevenia Message: {1.message}, {1.code}".format(scoop_offer, exception))
        else:
            _log.exception("Unknown exception type encountered")


def log_successful_publish(scoop_offer, elevenia_product):
    _log.info(
        u"Published Scoop Offer: {0.name} ({0.id}) -> {1.id}".format(
            scoop_offer, elevenia_product)
    )


def record_remote_publishing(scoop_offer, elevenia_product, remote_offer=None):
    if not remote_offer:
        scoop_offer.remote_offers.append(RemoteOffer(
            remote_service=ELEVENIA,
            remote_id=elevenia_product.id,
            last_checked=datetime.utcnow(),
            last_posted=datetime.utcnow()
        ))
    else:
        remote_offer.last_checked=datetime.utcnow()
        remote_offer.last_posted=datetime.utcnow()

    Offer.query.session.commit()

    log_successful_publish(scoop_offer, elevenia_product)


WEB_PLATFORM = 4
OFFER_READY_FOR_SALE = 7
SINGLE_OFFER = 1
SUBSCRIPTION_OFFER = 2

_log = logging.getLogger("scoopcor.script.elevenia")


if __name__ == '__main__':

    ctx = app.app_context()

    try:
        ctx.push()
        main()

    except Exception as e:
        _log.exception("Elevenia Publishing Failed")
        raise

    finally:
        ctx.pop()

