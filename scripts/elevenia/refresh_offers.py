"""
Elevenia Items Refresh Script
=============================

This script will request every Item that we have published to Elevenia.
It will then sync our local remote publishing database table with the
correct information.
"""
from datetime import datetime

from app import app
from app.services.elevenia import Elevenia, Product
from app.remote_publishing.models import RemoteOffer
from app.remote_publishing.choices.remote_data_location import ELEVENIA


def main():
    """ Syncronizes our local remote_items with Elevenia

    .. warning::

        This is a slow process and should likely be run as some kind of
        daily cron job task.
    """
    gateway = Elevenia(app.config['ELEVENIA_API_KEY'])

    page_num = 0
    known_items = []

    # Add all missing scoop ids to the database
    while True:
        page_num += 1
        products = gateway.resource(Product).list(page_num)
        if not products:
            break

        session = RemoteOffer.query.session

        for product in products:

                scoop_item_id = strip_elevenia_prefix(product.scoop_item_id)

                # find if we have a mapping
                remote_offer = session.query(RemoteOffer).filter(
                    RemoteOffer.remote_id == product.id,
                    RemoteOffer.remote_service == ELEVENIA
                ).first()

                if not remote_offer:
                    remote_offer = RemoteOffer(
                        item_id=scoop_item_id,
                        remote_service=ELEVENIA,
                        remote_id=product.id,
                        last_checked=datetime.utcnow(),
                        last_posted=datetime.utcnow()
                    )
                    session.add(remote_offer)

                else:
                    remote_offer.item_id = scoop_item_id
                    remote_offer.last_checked = datetime.utcnow()
                    remote_offer.last_posted = datetime.utcnow()

                known_items.append(scoop_item_id)

        # remove all unknown items from the database
        unknown_remote_offers = session.query(RemoteOffer).filter(RemoteOffer.item_id.notin_(known_items)).all()
        for unknown in unknown_remote_offers:
            session.delete(unknown)

        # commit our changes to the database
        session.commit()


def strip_elevenia_prefix(elevenia_scoop_id):
    """ Strips the SC00P prefix from Elevenia's IDs and converts to an integer.

    Our 'scoop_item_id' property in elevenia actually needs some work to
    get a usable 'scoop'.'core_items'.'id'.  Our IDs in their system are all
    represented as strings, which include our Item IDs.

    :param `str` elevenia_scoop_id: The scoop_item_id returned from the
        Elevenia API.
    :return: An integer that should map to the primary key of an Item
        in core_items
    :rtype: `int`
    """
    elevenia_scoop_id.replace("SC00P", "")
    return int(elevenia_scoop_id)


if __name__ == '__main__':
    main()
