#!/usr/bin/env python
"""
Clear Local Cache Files
=======================

Remove files from local that have already been uploaded to S3 and meet
certain conditions (older than 3 months, and only of types magazines/newspapers.
"""
from __future__ import unicode_literals

from argparse import ArgumentParser
from datetime import datetime, timedelta
import logging
from httplib import NOT_FOUND
from logging.config import dictConfig
import sys
import os

import boto3
from botocore.exceptions import ClientError

# ADD cor to search path
sys.path.insert(0, os.path.join(os.path.dirname(__file__), os.pardir))
from app import db
from app.items.models import ItemFile


def main(s3client, base_dir, file_ext):

    file_ext = normalize_file_extension(file_ext)

    _log.info("Started -- looking for {ext} in {dir}".format(ext=file_ext, dir=base_dir))

    for filename, dirpath in recursively_find_files_with_extension(base_dir, file_ext):

            full_filename = os.path.join(dirpath, filename)
            s3_key = normalize_s3_key(full_filename)

            try:
                assert_s3_filesize_matches_local(
                    file_size=os.stat(full_filename).st_size,
                    full_filename=full_filename,
                    s3_key=s3_key,
                    s3client=s3client)

                remove_file_if_graceperiod_expired(
                    full_filename=full_filename,
                    s3_key=s3_key,
                    grace_period=timedelta(days=90))

            except (FileSizeMismatchError, UnknownItemFile):
                _log.exception("Could not remove file")
            except ClientError as exc:
                _log.info(
                    "Skipping File not yet on s3: {}".format(s3_key)
                    if int(exc.response['Error']['Code']) == NOT_FOUND else
                    "S3 returned an unknown error code")
            except Exception:
                _log.exception("An unknown error occurred.")

    _log.info("Finished")


def recursively_find_files_with_extension(base_dir, file_ext):
    for (dirpath, dirnames, filenames) in os.walk(base_dir):
        for filename in [f for f in filenames if f.endswith(file_ext)]:
            yield (filename, dirpath)


def assert_s3_filesize_matches_local(file_size, full_filename, s3_key, s3client):
    head = s3client.head_object(Bucket='item-file-archives', Key=s3_key)
    expected_size = head['ContentLength']
    if file_size != expected_size:
        raise FileSizeMismatchError(full_filename, file_size, expected_size)


def remove_file_if_graceperiod_expired(full_filename, s3_key, grace_period):

    db_session = db.session()
    try:
        item_file = db_session.query(ItemFile).filter(ItemFile.file_name == s3_key).first()
        if item_file is None:
            raise UnknownItemFile
        if item_file.item.item_type in ['newspaper', 'magazine'] and \
                        item_file.item.release_date <= datetime.now() - grace_period:
            os.unlink(full_filename)
            _log.info("File: {filename} removed.".format(filename=full_filename))
        else:
            _log.info("Skipping File: {filename}.  Item is book or "
                      "within grace period".format(filename=full_filename))
    finally:
        db_session.close()


def normalize_s3_key(full_filename):
    """ Fix the S3 key prefix so it works on 65, 62 or Prod 4 (paths are different)

    :param `str` full_filename:
    :return:
    """
    if "/var/scoop65/img" in full_filename:
        s3_key = full_filename.replace("/var/scoop65/img/", "images")
    elif "/var/python/www/scoopcor/static/scoop62" in full_filename:
        s3_key = full_filename.replace("/var/python/www/scoopcor/static/scoop62/", "")
    elif "/var/python/www/scoopcor/static/scoop62/images/" in full_filename:
        s3_key = full_filename.replace("/var/python/www/scoopcor/static/scoop62/", "images")
    else:
        s3_key = full_filename.replace("/var/scoop62/", "")
    return s3_key


def normalize_file_extension(file_ext):
    """ Assure our input 'file_extension' always starts with '.'

    :param `str` file_ext:
    :return: File extensions starting with '.'
    """
    if not file_ext.startswith('.'):
        file_ext = "." + file_ext
    return file_ext


class UnknownItemFile(Exception):
    pass


class FileSizeMismatchError(Exception):
    def __init__(self, full_filename, file_size, expected_size):
        self.full_filename = full_filename
        self.file_size = file_size
        self.expected_size = expected_size
        super(FileSizeMismatchError, self).__init__(str(self))

    def __str__(self):
        return "File: {filename} on disk ({ondisk}) does not match " \
            "size in S3 ({s3size}).  Skipping".format(
                filename=self.full_filename,
                ondisk=self.file_size,
                s3size=self.expected_size)


def initialize_logging():

    dictConfig({
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'script_format': {
                'format': '%(asctime)s - %(levelname)s - %(message)s'
            }
        },
        'handlers': {
            's3_clear_debug_log_file': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'formatter': 'script_format',
                'filename': 'clear-local-contentfile-cache.script.debug.log'
            },
            's3_clear_log_file': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'formatter': 'script_format',
                'filename': 'clear-local-contentfile-cache.script.log'
            },
        },
        'loggers': {
            'script.s3.clear-local': {
                'handlers': ['s3_clear_log_file', 's3_clear_debug_log_file', ],
                'propagate': False,
                'level': 'DEBUG',
            },
        }})


_log = logging.getLogger('script.s3.clear-local')


if __name__ == '__main__':

    initialize_logging()

    parser = ArgumentParser()

    parser.add_argument('--base-dir', dest='base_dir', required=True)
    parser.add_argument('--file-ext', dest='file_ext', required=True)

    s3client = boto3.client(
        's3', 'ap-southeast-1',
        aws_access_key_id='AKIAIWWZDMPWMHIQWBDQ',
        aws_secret_access_key='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')


    main(s3client, **vars(parser.parse_args()))


