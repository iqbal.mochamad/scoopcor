import os, sys

from sqlalchemy import desc

from app.items.choices import NEWSPAPER, ITEM_TYPES

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files
from app.logs.models import ItemProcessing
from app.items.models import Item
from app import app, db

BUCKET_NAME = app.config['BOTO3_GRAMEDIA_COVERS_BUCKET']


def send_s3_cover(aws_server=None, item=None, iprocess=None):
    cover_dir = ['general_small_covers', 'thumb_image_highres', 'image_normal', 'image_highres']
    for cover_path in cover_dir:
        brand_folder = aws_check_files(aws_server, BUCKET_NAME, '{}/{}/'.format(item.brand_id, cover_path))

        if not brand_folder:
            aws_server.put_object(Bucket=BUCKET_NAME, Key='{}/{}/'.format(item.brand_id, cover_path), ACL='public-read')

        # check files..
        if cover_path == 'general_small_covers': image_files = item.thumb_image_normal
        if cover_path == 'thumb_image_highres': image_files = item.thumb_image_highres
        if cover_path == 'image_normal': image_files = item.image_normal
        if cover_path == 'image_highres': image_files = item.image_highres

        file_path = os.path.join('/var/python/www/scoopcore/magazine_static/', str(image_files))
        if os.path.exists(file_path):
            new_name = str(os.path.basename(file_path))

            aws_server.upload_file(file_path, BUCKET_NAME, '{}/{}/{}'.format(item.brand_id, cover_path, new_name),
                                   ExtraArgs={'ContentType': 'image/jpeg', 'ACL': 'public-read'})

            new_path = 'images/1/{}/{}/{}'.format(item.brand_id, cover_path, new_name)

            if cover_path == 'general_small_covers': item.thumb_image_normal = new_path
            if cover_path == 'thumb_image_highres': item.thumb_image_highres = new_path
            if cover_path == 'image_normal': item.image_normal = new_path
            if cover_path == 'image_highres': item.image_highres = new_path

            db.session.add(item)
            db.session.commit()
            print 'success transfering files to s3, {} {}'.format(item.brand_id, image_files)
        else:
            if iprocess:
                iprocess.status = 4
                db.session.add(iprocess)
                db.session.commit()
            print 'No file are founds..', item.id, image_files


def get_item_by_cover(item_id=None):
    aws_server = connect_gramedia_s3()

    if item_id:
        item = Item.query.filter_by(id=item_id).first()
        send_s3_cover(aws_server, item, None)
    else:
        item_process = ItemProcessing.query.filter_by(status=3).order_by(desc(ItemProcessing.id)).all()
        for iprocess in item_process:
            # update status
            iprocess.status = 5
            db.session.add(iprocess)
            db.session.commit()

            item = Item.query.filter_by(id=iprocess.item_id).first()
            print item.item_type, ITEM_TYPES[NEWSPAPER]
            if item.item_type == ITEM_TYPES[NEWSPAPER]:
                send_s3_cover(aws_server, item, iprocess)
            else:
                # for book and magazines already used new portal.
                print 'item_id {} is not newspaper.. skipp'.format(item.id)


if __name__ == '__main__':
    get_item_by_cover()
