import os, sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.uploads.api import upload_process


def main():
    upload_process()

if __name__ == '__main__':
    main()
