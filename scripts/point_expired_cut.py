from app.points.api import pointexpiredcut
from app import app

def main():
    ctx = app.test_request_context('/')
    ctx.push()

    pointexpiredcut()

    ctx.pop()

if __name__ == '__main__':
    main()
