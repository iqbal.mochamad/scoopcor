#!/usr/bin/env python
"""
User Buffet/Subscription Expiry Notice
======================================

sending emails to users that theirs buffet and subscription has expired
"""
from __future__ import unicode_literals, absolute_import

import sys
from argparse import ArgumentParser
from logging import getLogger
from smtplib import SMTPException

import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))
from app import app, db
from app.helpers import DateRange
from app.items.helpers import get_latest_items_for_brand
from app.messaging.email import SmtpServer, HtmlEmail
from app.services.scoopcas import ScoopCas
from app.reports.user_subscriptions import \
    SingleUserSubscriptionsReport, get_user_ids_with_subscriptions


_log = getLogger("")


def main(session, min_age, max_age):

    with app.app_context():

        server = get_smtp_server()

        cas = ScoopCas(app.config['SERVER_SCOOPCAS'], app.config['AUTH_HEADER'], testing=app.debug)

        for user_subscription_report in (SingleUserSubscriptionsReport(user_id, session) for user_id in get_user_ids_with_subscriptions(session)):

            user = cas.get_user(user_subscription_report.user_id)

            for expired_sub in user_subscription_report.get_expired_subscriptions(DateRange(min_age, max_age)):

                latest_editions = get_latest_items_for_brand(session, expired_sub.brand_id, max_results=3)
                if latest_editions:
                    try:
                        msg = construct_email(user, latest_editions, expired_sub)
                        server.send_message(msg)

                    except SMTPException:
                        _log.exception("Failed to send subscription renewal "
                                       "notice to: {0.email}".format(user))


def get_smtp_server():
    server = SmtpServer(
        app.config['SMTP_HOST'], app.config['SMTP_USER'],
        app.config['SMTP_PASS'], app.config['SMTP_PORT'],
        app.config['SMTP_USE_TLS'])
    return server


def construct_email(user, latest_editions, expired_sub):
    return HtmlEmail(
        'noreply@getscoop.com',
        [user.email, ],
        'Scoop - Your Subscription Has Expired!',
        'email/user_reminders/subscription_expired.html',
        'email/user_reminders/subscription_expired.txt',
        user=user,
        items=latest_editions,
        subscription_offer=expired_sub.offer)


if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument(
        '--min-expired-age',
        dest='min_age',
        required=True)

    parser.add_argument(
        '--max-expired-age',
        dest='max_age',
        required=True)

    main(db.session(), **vars(parser.parse_args()))
