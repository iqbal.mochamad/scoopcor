import os, shutil, sys

from six import text_type
from sqlalchemy import desc

from app import app, db
from app.eperpus.models import WebReaderLog
from app.items.choices import ItemTypes
from app.items.files.pdfs import web_reader_batch_extractor_factory, thumbnail_batch_extractor_factory
from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))


class UploadWebReaderProcessing(object):

    def __init__(self, reader_data=None, redis_key=None):
        self.item = reader_data.item
        self.reader_data = reader_data
        self.redis_keys = redis_key
        self.original_bucket = app.config['BOTO3_GRAMEDIA_ORIGINAL_BUCKET']

        self.directory = os.path.join(app.config['UPLOAD_BASE_DIR'], 'web-reader')
        self.thumbnails_directory = os.path.join(app.config['UPLOAD_BASE_DIR'], 'thumbnails')
        self.temp_path = os.path.join('/tmp/', '{}.pdf'.format(self.item.edition_code))
        self.output_dir = os.path.join(self.directory, '{}.pdf'.format(self.item.edition_code))

        self.original_brand_path = os.path.join(self.directory, str(self.item.brand_id))
        self.original_thumb_path = os.path.join(self.thumbnails_directory, str(self.item.brand_id))

        self.s3_server = connect_gramedia_s3()
        self.mod = 0777

    def retrieve_files(self):
        try:
            if not os.path.exists(self.output_dir):
                # download from S3
                bucket_file = '{}/{}.pdf'.format(self.item.brand_id, self.item.edition_code)
                check_files = aws_check_files(self.s3_server, self.original_bucket, bucket_file)

                if not check_files:
                    return False, 'Files {} not exist on bucket {}'.format(bucket_file, self.original_bucket)
                else:
                    try:
                        if not os.path.isdir(self.directory):
                            os.makedirs(self.directory, self.mod)
                        if not os.path.isdir(self.thumbnails_directory):
                            os.makedirs(self.thumbnails_directory, self.mod)

                        if not os.path.exists(self.original_brand_path):
                            os.makedirs(self.original_brand_path, self.mod)
                        if not os.path.exists(self.original_thumb_path):
                            os.makedirs(self.original_thumb_path, self.mod)

                        self.s3_server.download_file(Bucket=self.original_bucket, Key=bucket_file, Filename=self.temp_path)
                    except:
                        pass

                    if os.path.exists(self.temp_path):
                        shutil.copy(self.temp_path, self.original_brand_path)
                        shutil.copy(self.temp_path, self.original_thumb_path)

                        # remove files
                        try: os.remove(self.temp_path)
                        except: pass
                        return True, 'Done retrieve_files for item {}'.format(self.item.id)
                    else:
                        return False, 'Cant process file in {}'.format(self.output_dir)

        except Exception as e:
            return False, 'retrieve_files Failed {}'.format(e)

    def extract_web_reader(self):
        try:
            high_res = self.item.item_type == ItemTypes.newspaper.value
            web_reader_batch_extractor_factory(self.item, self.directory, high_res).process()
            return True, 'Done extract_web_reader for item {}'.format(self.item.id)

        except Exception as e:
            return False, 'extract_web_reader Failed {}'.format(e)

    def extract_thumbnails_reader(self):
        try:
            thumbnail_batch_extractor_factory(self.item, self.thumbnails_directory).process()
            return True, 'Done extract_thumbnails_reader for item {}'.format(self.item.id)
        except Exception as e:
            return False, 'extract_thumbnails_reader Failed {}'.format(e)

    def transfer_web_reader_s3(self, bucket=None, thumbnail=None):
        try:
            bucket_name = bucket

            # check brand..
            brand_folder = aws_check_files(self.s3_server, bucket_name, '{}/'.format(self.item.brand_id))
            if not brand_folder:
                self.s3_server.put_object(Bucket=bucket_name, Key='{}/'.format(self.item.brand_id))

            # check brand/item_id
            item_folder = aws_check_files(self.s3_server, bucket_name,
                                          '{}/{}/'.format(self.item.brand_id, self.item.id))
            if not item_folder:
                self.s3_server.put_object(Bucket=bucket_name, Key='{}/{}/'.format(self.item.brand_id, self.item.id))

            if thumbnail:
                images_files = os.path.join(app.static_folder, 'item_content_files', 'thumbnails',
                                            text_type(self.item.brand_id), text_type(self.item.id))
            else:
                images_files = os.path.join(app.static_folder, 'item_content_files', 'web_reader',
                                            text_type(self.item.brand_id), text_type(self.item.id))

            for image in os.listdir(images_files):
                # files_ = os.path.join(images_files, image)
                # process_call = 'aws s3 cp {} s3://{}/{}/{}/'.format(files_, bucket, self.item.brand_id, self.item.id)
                # subprocess.call(process_call, shell=True)

                files_ = os.path.join(images_files, image)
                self.s3_server.upload_file(files_, bucket_name, '{}/{}/{}'.format(self.item.brand_id,
                    self.item.id, image), ExtraArgs={'ContentType': 'image/jpeg'})
                os.remove(files_)
            return True, 'Done transfer_web_reader_s3 for item {}'.format(self.item.id)

        except Exception as e:
            return False, 'transfer_web_reader Failed {}'.format(e)

    def process(self):
        bucket_name = app.config['BOTO3_GRAMEDIA_WEBREADER_BUCKET']
        file_exist = aws_check_files(self.s3_server, bucket_name, '{}/{}/{}'.format(self.item.brand_id, self.item.id, '1.jpg'))

        if not file_exist:
            print 'retrieve_files for item {}'.format(self.item.id)
            status, message = self.retrieve_files()
            if not status:
                return False, message
            else:
                print 'extract_web_reader for item {}'.format(self.item.id)
                web_status, web_message = self.extract_web_reader()

                print 'extract_thumbnails for item {}'.format(self.item.id)
                thumb_status, thumb_message = self.extract_thumbnails_reader()

                if not web_status or not thumb_status:
                    if not web_status: return web_status, web_message
                    else: return thumb_status, thumb_message
                else:
                    print 'transfer_web_reader_s3 for item {}'.format(self.item.id)
                    web_tr_status, web_tr_message = self.transfer_web_reader_s3(app.config['BOTO3_GRAMEDIA_WEBREADER_BUCKET'], False)
                    if not web_tr_status: return web_tr_status, web_tr_message

                    print 'transfer_thumb_web_reader_s3 for item {}'.format(self.item.id)
                    thumb_tr_status, thumb_tr_message = self.transfer_web_reader_s3(app.config['BOTO3_GRAMEDIA_THUMBNAILS_BUCKET'], True)
                    if not thumb_tr_status: return thumb_tr_status, thumb_tr_message

                    print 'deleting pdf source folder for item {}, source {}'.format(self.item.id, self.original_brand_path)
                    shutil.rmtree(self.original_brand_path, ignore_errors=True)
                    shutil.rmtree(self.original_thumb_path, ignore_errors=True)

                    # delete processing..
                    app.kvs.delete(self.redis_keys)
                    return True, 'Done'

        else:
            # mark if this item already have web_reader.
            self.reader_data.is_web_reader = True
            db.session.add(self.reader_data)
            db.session.commit()
            app.kvs.delete(self.redis_keys)

            shutil.rmtree(self.original_brand_path, ignore_errors=True)
            shutil.rmtree(self.original_thumb_path, ignore_errors=True)

            return False, 'Content already exist for item {}, brand_id {}'.format(self.item.id, self.item.brand_id)


def upload_webreader(reader_data=None, redis_key=None):
    try:
        if reader_data and reader_data.item.content_type == 'pdf':
            print 'Processing PDF WEB READER START item_id {item}, brand_id {brand}'.format(
                item=reader_data.item.id, brand=reader_data.item.brand_id)
            status, message = UploadWebReaderProcessing(reader_data, redis_key).process()
            return status, message

    except Exception as e:
        print False, "Upload webreader failed {}".format(e)


def main():
    ctx = app.test_request_context('/')
    ctx.push()

    ratio_data = [('magazine', 5), ('book', 7), ('newspaper', 5)]
    data_reader = []
    for iratio in ratio_data:
        web_data = WebReaderLog.query.filter_by(is_processed=False).join(WebReaderLog.item, aliased=True).filter_by(
            item_type=iratio[0]).order_by(desc(WebReaderLog.item_id)).limit(iratio[1]).all()
        data_reader = data_reader + web_data

    key_data = range(1, len(data_reader)+1)
    map_reader = zip(key_data, data_reader)

    try:
        for idata in map_reader:
            redis_keys = 'web-reader:data{}'.format(idata[0])
            reader_data = idata[1]

            if app.kvs.get(redis_keys):
                pass
            else:
                app.kvs.set(redis_keys, 'processed {}'.format(reader_data.item_id), ex=3600)
                reader_data.is_processed = True
                db.session.add(reader_data)
                db.session.commit()

                status, message = upload_webreader(reader_data, redis_keys)
                if not status:
                    reader_data.error = message
                    db.session.add(reader_data)
                else:
                    reader_data.is_web_reader = True
                    reader_data.item.is_webreader = True
                    db.session.add(reader_data)
                db.session.commit()

    except Exception as e:
        print 'Errror {}'.format(e)

    ctx.pop()

if __name__ == '__main__':
    main()
