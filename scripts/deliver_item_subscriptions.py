import os, sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.cronjobs.helpers import PostItemSubs

def deliver_item_subs():
    PostItemSubs().construct()

if __name__ == '__main__':
    deliver_item_subs()
