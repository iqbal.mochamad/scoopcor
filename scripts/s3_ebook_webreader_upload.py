import os, sys, subprocess


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.uploads.aws.ebook_webreaders import transfer_files_webreader
from app import app


BIZNET_PATH = app.config['BOTO3_BIZNET_WEBREADER']
BUCKET_NAME = app.config['BOTO3_GRAMEDIA_WEBREADER_BUCKET']

def get_latest_modified_folders():

    # get list of full folders.
    # sorted all folder by modified times, and get 25 top folder of brand_id
    # why 30? dari febri dalam sejam itu paling dia top max upload content 20 files ( this mean 20 brand)
    process = subprocess.Popen(['ls', '-t', BIZNET_PATH], stdout=subprocess.PIPE)
    stdout, stderr = process.communicate()
    brand_id_list = stdout.decode('ascii').splitlines()[:30]
    return brand_id_list


def upload_specified_files(brand_id_list):

    for brand_id in brand_id_list:
        folder_brand = os.path.join(BIZNET_PATH, brand_id)

        if os.path.isdir(folder_brand):
            process = subprocess.Popen(['ls', '-t', folder_brand], stdout=subprocess.PIPE)
            stdout, stderr = process.communicate()
            item_id_list = stdout.decode('ascii').splitlines()[:5]
            transfer_files_webreader(directory_path=BIZNET_PATH, brand_id=brand_id, bucket_name=BUCKET_NAME,
                item_id_list=item_id_list)

# def upload_bulk_files(brand_id_list):
#     for brand_id in brand_id_list:
#         transfer_files_webreader(str(brand_id))


if __name__ == '__main__':
    brand_id_list = get_latest_modified_folders()

    # uncomment this for upload latest modified file inside brand folder
    upload_specified_files(brand_id_list)

    # uncomment this for upload bulk files inside brand_id.
    # upload_bulk_files(brand_id_list)
