#!/usr/bin/env python
"""
Upload Unpurchased Files to Amazon S3
=====================================

"""
from argparse import ArgumentParser
from datetime import timedelta, datetime
import logging
import os
import multiprocessing
import threading
import shutil
import sys

import boto3
from boto3.s3.transfer import S3Transfer

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))
from app import db, app
from app.items.models import Item
from app.items.choices.item_type import MAGAZINE, NEWSPAPER, ITEM_TYPES

MAGAZINE_STR = ITEM_TYPES[MAGAZINE]
NEWSPAPER_STR = ITEM_TYPES[NEWSPAPER]


_log = logging.getLogger('scoopcor.script.s3')


def main(s3, grace_period_days=None, brand_id=None, all_of_type=None, upload_only=False, dry_run=False):

    if None not in [all_of_type, grace_period_days]:

        _log.info("Fetching {} older than {} day(s) for archiving.".format(all_of_type, grace_period_days))

        all_candidates = [ac.files for ac in find_items_by_type(timedelta(days=grace_period_days), item_type=all_of_type)]

        upload_candidate_count = len(all_candidates)
        thread_count = 1
        items_per_thread = (upload_candidate_count/thread_count)

        _log.info("Preparing to archive {} on {} threads.".format(upload_candidate_count, thread_count))

        i = 0
        jobs = []
        while i < upload_candidate_count:

            p = multiprocessing.Process(target=main_archive_by_item_type,
                                        kwargs={
                                            'dry_run': dry_run,
                                            's3': s3,
                                            'upload_only': upload_only,
                                            'archive_candidates': all_candidates[i:items_per_thread]
                                        })
            jobs.append(p)
            p.start()
            i += items_per_thread

        for job in jobs:
            job.join()


        _log.info("Job Done.")

    # elif grace_period_days is not None:
    #
    #     _log.info("Fetching candidates for archiving")
    #
    #     all_candiates = [ac.files for ac in find_items_to_archive(timedelta(days=grace_period_days))]
    #
    #     upload_candidate_count = len(all_candiates)
    #     thread_count = 30
    #     items_per_thread = (upload_candidate_count / thread_count)
    #
    #     _log.info("Preparing to archive {} on {} threads.".format(upload_candidate_count, thread_count))
    #
    #     i = 0
    #     jobs = []
    #     while i < upload_candidate_count:
    #         p = multiprocessing.Process(target=main_archive_unowned,
    #                                     kwargs={
    #                                         'dry_run': dry_run,
    #                                         's3': s3,
    #                                         'upload_only': upload_only,
    #                                         'archive_candidates': all_candiates[i:items_per_thread]
    #                                     })
    #         jobs.append(p)
    #         p.start()
    #         i += items_per_thread
    #
    #     for job in jobs:
    #         job.join()
    #
    #
    #     _log.info("Job Done.")
    #


    else:
        raise RuntimeError("--grace-period-days or --brand-id is required.")


def main_archive_by_item_type(dry_run, s3, upload_only, archive_candidates):
    for archive_candidate in archive_candidates:

        for item_file in archive_candidate:

            # if dry run, we just report the filename and nothing else
            item_file_full_path = os.path.join(app.static_folder, app.config['BOTO3_DIR_DOWNLOAD'],
                                               item_file.file_name)

            if dry_run:
                _log.info("Dry run -> {}".format(item_file_full_path))
                continue

            _log.info("Uploading file: {}".format(item_file_full_path))

            if os.path.exists(item_file_full_path):
                try:
                    s3.upload_file(
                        filename=item_file_full_path,
                        bucket=app.config['BOTO3_BUCKET_NAME'],
                        key=item_file.file_name)
                    _log.info("File uploaded.")
                    # if upload only, then we skip removing the file from local
                    if upload_only:
                        _log.info("Uploading only.. not deleting locally.")
                        continue
                    else:
                        _log.info("Moving to temp storage")
                        #os.makedirs('/tmp/s3-archive', exist_ok=True)
                        shutil.move(
                            item_file_full_path,
                            '/home/derek.curtis/s3-archive/' + item_file.file_name.replace('/', '_'))

                except Exception:
                    _log.exception("Couldn't upload file")
            else:
                _log.warning("File does not exist locally: {}".format(item_file_full_path))


def main_archive_unowned(dry_run, s3, upload_only, archive_candidates):

    for archive_candidate in archive_candidates:

        for item_file in archive_candidate:

            # if dry run, we just report the filename and nothing else
            item_file_full_path = os.path.join(app.static_folder, app.config['BOTO3_DIR_DOWNLOAD'],
                                               item_file.file_name)

            if dry_run:
                _log.info("Dry run -> {}".format(item_file_full_path))
                continue

            _log.info("Uploading file: {}".format(item_file_full_path))

            if os.path.exists(item_file_full_path):
                try:
                    s3.upload_file(
                        filename=item_file_full_path,
                        bucket=app.config['BOTO3_BUCKET_NAME'],
                        key=item_file.file_name)
                    _log.info("File uploaded.")
                    # if upload only, then we skip removing the file from local
                    if upload_only:
                        _log.info("Uploading only.. not deleting locally.")
                        continue
                    else:
                        _log.info("Moving to temp storage")
                        os.makedirs('/tmp/s3-archive', exist_ok=True)
                        shutil.move(
                            item_file_full_path,
                            '/tmp/s3-archive/' + item_file.replace('/', '_'))
                        # os.unlink(item_file_full_path)

                except Exception:
                    _log.exception("Couldn't upload file")
            else:
                _log.warning("File does not exist locally: {}".format(item_file_full_path))


def main_archive_by_brand_id(brand_id, dry_run, upload_only):
    _log.info("Running for brand_id: {}".format(brand_id))

    for archive_candidate in find_items_by_brand(brand_id):

        for item_file in archive_candidate.files:

            item_file_full_path = os.path.join(app.static_folder, app.config['BOTO3_DIR_DOWNLOAD'], item_file.file_name)
            if dry_run:
                _log.info("Dry run -> {}".format(item_file_full_path))
                continue


            if os.path.exists(item_file_full_path):
                try:
                    s3.upload_file(
                        filename=item_file_full_path,
                        bucket=app.config['BOTO3_BUCKET_NAME'],
                        key=item_file.file_name)

                    print item_file_full_path

                    # if upload only, then we skip removing the file from local
                    if upload_only:
                        continue
                    else:
                        print "Deleting locally is not enabled"
                        # raise Warning("Deleting locally is not enabled.")
                        # os.remove(item_file_full_path)

                except Exception:
                    raise
            else:
                _log.warning("File does not exist: {}".format(item_file_full_path))


def find_items_to_archive(grace_period):
    """ Returns a generator expression that will fetch all the items that should
    be archived.  Does not do any checking to see if the item files actually
    exist on the filesystem or not.

    :param `datetime.timedelta` grace_period: Anything falling between now and
        the grace_period will not be flagged as requiring a fix.
    :return: A generator expression that will iterate all the items that fulfill
        our archiving criteria
    """
    s = db.session()

    return (entity for entity in s.query(Item).filter(
        (Item.release_date < (datetime.utcnow() - grace_period)),
    ).limit(10000) if len(entity.user_items) == 0)


def find_items_by_type(grace_period, item_type):
    s = db.session()

    return (entity for entity in s.query(Item).filter(
        (Item.release_date < (datetime.utcnow() - grace_period)),
        (Item.item_type == item_type)))


def find_items_by_brand(brand_id):
    s = db.session()

    return (entity for entity in s.query(Item).filter(
        Item.brand_id == brand_id
    ))

if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument(
        '--grace-period-days',
        dest='grace_period_days',
        type=int,
        required=False,
        help="The number of days to wait after an Item's 'release_date' before "
             "it is archived to Amazone S3."
    )
    parser.add_argument(
        '--upload-only',
        dest='upload_only',
        action='store_true',
        default=False,
        help="If provided, only upload files--don't delete local copies"
    )
    parser.add_argument(
        '--dry-run',
        dest='dry_run',
        action='store_true',
        default=False,
        help="If provided, only report the names of the files that would be"
             "uploaded, but don't actually do anything."
    )
    parser.add_argument(
        '--brand-id',
        dest='brand_id',
        type=int,
        required=False,
    )
    parser.add_argument(
        '--all-of-type',
        dest='all_of_type',
        required=False,
        choices=(MAGAZINE_STR, NEWSPAPER_STR,)
    )

    s3 = S3Transfer(boto3.client('s3',
        app.config['BOTO3_AWS_REGION'],
        aws_access_key_id=app.config['BOTO3_AWS_APP_ID'],
        aws_secret_access_key=app.config['BOTO3_AWS_APP_SECRET']))

    main(s3, **vars(parser.parse_args()))

