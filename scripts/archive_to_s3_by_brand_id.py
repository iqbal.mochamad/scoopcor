#!/usr/bin/env python
"""
Upload Unpurchased Files to Amazon S3
=====================================

"""
from argparse import ArgumentParser
from datetime import datetime
import logging
import os
import sys
import threading

import botocore
import boto3
from boto3.s3.transfer import S3Transfer
from s3transfer.subscribers import BaseSubscriber

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))
from app import db, app
from app.items.models import Item
from app.items.choices.item_type import MAGAZINE, NEWSPAPER, ITEM_TYPES

MAGAZINE_STR = ITEM_TYPES[MAGAZINE]
NEWSPAPER_STR = ITEM_TYPES[NEWSPAPER]


_log = logging.getLogger('scoopcor.script.s3')


def main_archive_by_brand_id(s3, brand_id=None):
    _log.info("Running for brand_id: {}".format(brand_id))

    for archive_candidate in find_items_by_brand(brand_id):

        for item_file in archive_candidate.files:
            item_file_full_path = os.path.join(app.static_folder, app.config['BOTO3_DIR_DOWNLOAD'], item_file.file_name.replace(
                'images/1/', ''))

            if file_already_exists_in_s3(app.config['BOTO3_BUCKET_NAME'], item_file.file_name):
                _log.warning("File Already in S3 : {} . Do Nothing, Go to next item.".format(item_file_full_path))
                break

            if os.path.exists(item_file_full_path):
                try:
                    _log.info("Starting upload {} ....".format(item_file.item.edition_code))

                    s3.upload_file(
                        filename=item_file_full_path,
                        bucket=app.config['BOTO3_BUCKET_NAME'],
                        key=item_file.file_name,
                        callback=ProgressPercentage(item_file_full_path))

                    print item_file_full_path+"\n==========="

                    # # if upload only, then we skip removing the file from local
                    # if upload_only:
                    #     continue
                    # else:
                    #     print "Deleting locally is not enabled"
                    #     # raise Warning("Deleting locally is not enabled.")
                    #     # os.remove(item_file_full_path)

                except Exception:
                    raise
            else:
                _log.warning("File does not exist: {}".format(item_file_full_path))


def find_items_to_archive(grace_period):
    """ Returns a generator expression that will fetch all the items that should
    be archived.  Does not do any checking to see if the item files actually
    exist on the filesystem or not.

    :param `datetime.timedelta` grace_period: Anything falling between now and
        the grace_period will not be flagged as requiring a fix.
    :return: A generator expression that will iterate all the items that fulfill
        our archiving criteria
    """
    s = db.session()

    return (entity for entity in s.query(Item).filter(
        (Item.release_date < (datetime.utcnow() - grace_period)),
    ).limit(10000) if len(entity.user_items) == 0)


def find_items_by_type(grace_period, item_type):
    s = db.session()

    return (entity for entity in s.query(Item).filter(
        (Item.release_date < (datetime.utcnow() - grace_period)),
        (Item.item_type == item_type)))


def find_items_by_brand(brand_id):
    s = db.session()

    return (entity for entity in s.query(Item).filter(
        Item.brand_id == brand_id
    ))


class ProgressPercentage(object):
    def __init__(self, filename):
        self._filename = filename
        self._size = float(os.path.getsize(filename))
        self._seen_so_far = 0
        self._lock = threading.Lock()

    def __call__(self, bytes_amount):
        # To simplify we'll assume this is hooked up
        # to a single filename.
        with self._lock:
            self._seen_so_far += bytes_amount
            percentage = (self._seen_so_far / self._size) * 100
            sys.stdout.write(
                "\r {}  {} / {}  ({:10.2f}%)" .format(
                    self._filename, self._seen_so_far, self._size,
                    percentage))
            sys.stdout.flush()


def file_already_exists_in_s3(bucket_name, filename):
    conn = boto3.resource('s3',
                          app.config['BOTO3_AWS_REGION'],
                          aws_access_key_id=app.config['BOTO3_AWS_APP_ID'],
                          aws_secret_access_key=app.config['BOTO3_AWS_APP_SECRET'])
    try:
        conn.Object(bucket_name, filename).load()

    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            return False
        else:
            raise
    else:
        return True


class ProgressCallbackInvoker(BaseSubscriber):
    """A back-compat wrapper to invoke a provided callback via a subscriber

    :param callback: A callable that takes a single positional argument for
        how many bytes were transferred.
    """
    def __init__(self, callback):
        self._callback = callback

    def on_progress(self, bytes_transferred, **kwargs):
        self._callback(bytes_transferred)


if __name__ == '__main__':

    parser = ArgumentParser()

    parser.add_argument(
        '--brand-id',
        dest='brand_id',
        type=int,
        required=False,
    )

    s3 = S3Transfer(boto3.client('s3',
        app.config['BOTO3_AWS_REGION'],
        aws_access_key_id=app.config['BOTO3_AWS_APP_ID'],
        aws_secret_access_key=app.config['BOTO3_AWS_APP_SECRET']))

    main_archive_by_brand_id(s3, **vars(parser.parse_args()))
