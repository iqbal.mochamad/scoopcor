import os, sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.items.choices.status import STATUS_TYPES, STATUS_READY_FOR_CONSUME, STATUS_NOT_FOR_CONSUME

from datetime import datetime, timedelta
from app.items.models import ItemUploadProcess


def uploader_clean_upfiles():
    start_date = datetime.now() - timedelta(days=7)
    data_upload = ItemUploadProcess.query.filter(
        ItemUploadProcess.created >= start_date
    ).all()

    for idata in data_upload:
        raw_files = os.path.join(idata.file_dir, idata.file_name)

        try:
            if idata.item.item_status in (STATUS_TYPES[STATUS_READY_FOR_CONSUME], STATUS_TYPES[STATUS_NOT_FOR_CONSUME]):
                if os.path.isfile(raw_files):
                    os.remove(raw_files)
                    print 'Successfully delete raw files {}'.format(raw_files)
        except Exception as e:
            print "Cant delete raw files {} - {} Error {}".format(idata.id, raw_files, e)

if __name__ == '__main__':
    uploader_clean_upfiles()
