#!/usr/bin/env python
import os, sys
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app import db
import logging


FORMAT = '%(asctime)s - SCRIPT - INFO - %(message)s'
logging.basicConfig(format=FORMAT)

def patch_parental_control():
    conn = db.session.bind
    logging.warning('patch parental control to 17+')
    for brand in get_age_restricted_brands(conn):
        age_restrict_items(conn, items=conn.execute(core_items.select().where(
            core_items.c.brand_id == brand.id)).fetchall())
        age_restrict_brand_by_default(conn, brand)
    logging.warning('patch parental control to all ages')
    for brand in get_all_ages_brands(conn):
        all_age_brand_by_default(conn, brand)
    logging.warning('patch parental control by categories for book')
    for row in sort_items_categories(conn):
        age_restrict_for_books(conn, row)

MAGAZINE = 1
BOOK = 2
NEWSPAPER = 3
BONUS_ITEM = 4

ITEM_TYPES = {MAGAZINE: 'magazine',
              BOOK: 'book',
              NEWSPAPER: 'newspaper',
              BONUS_ITEM: 'bonus item',
              }

meta = sa.MetaData()
core_brands = sa.Table(
    'core_brands',
    meta,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('name', sa.Text),
    sa.Column('default_parentalcontrol_id', sa.Integer,
              sa.ForeignKey('core_parentalcontrols')),
    sa.Column('is_active', sa.Boolean)
)


core_items = sa.Table(
    'core_items',
    meta,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('name', sa.Text),
    sa.Column('is_active', sa.Boolean),
    sa.Column('brand_id', sa.Integer, sa.ForeignKey('core_brands.id')),
    sa.Column('parentalcontrol_id', sa.Integer,
              sa.ForeignKey('core_parentalcontrols'))
)
core_items_categories = sa.Table(
    'core_items_categories',
    meta,
    sa.Column('category_id', sa.Integer, sa.ForeignKey('core_categories.id')),
    sa.Column('item_id', sa.Integer, sa.ForeignKey('core_items.id'))
)


core_categories = sa.Table(
    'core_categories',
    meta,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('name', sa.Text),
    sa.Column('item_type', ENUM(*ITEM_TYPES.values(), name='item_type'), nullable=False),
    sa.Column('is_active', sa.Boolean)
)

core_brands_defaultparentalcontrols = sa.Table(
    'core_brands_defaultparentalcontrols',
    meta,
    sa.Column('brand_id', sa.Integer,
              sa.ForeignKey('core_brands')),
    sa.Column('parental_id', sa.Integer,
              sa.ForeignKey('core_parentalcontrols'))
)

core_parentalcontrol = sa.Table(
    'core_parentalcontrols',
    meta,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('name', sa.Text),
)

ALL_AGES = 1
ADULT_ONLY = 2


def get_age_restricted_brands(conn):
    adult_brands = conn.execute(core_brands.select().where(sa.and_(
                                core_brands.c.id.in_(brand_id_17),
                                core_brands.c.is_active==True)
                            )).fetchall()
    return adult_brands


def get_all_ages_brands(conn):
    all_ages_brands = conn.execute(core_brands.select().where(sa.and_(
                                ~core_brands.c.id.in_(brand_id_17),
                                core_brands.c.is_active==True)
                            )).fetchall()
    return all_ages_brands


def age_restrict_items(conn, items):

    for item in items:
        conn.execute(core_items.update().where(
                                    core_items.c.id==item.id).values(
                                    parentalcontrol_id=ADULT_ONLY))


def all_age_brand_by_default(conn, brand):
    """
    :type brand: sql alchemy query object
    """
    conn.execute(core_brands.update().where(
                core_brands.c.id == brand.id).values(
                                            default_parentalcontrol_id=ALL_AGES))
    conn.execute(core_brands_defaultparentalcontrols.insert().values(
                                            parental_id=ALL_AGES,
                                            brand_id=brand.id))


def age_restrict_brand_by_default(conn, brand):
    """
    :type brand: sql alchemy query object
    """
    conn.execute(core_brands.update().where(
                core_brands.c.id == brand.id).values(
                                            default_parentalcontrol_id=ADULT_ONLY))
    conn.execute(core_brands_defaultparentalcontrols.insert().values(
                                            parental_id=ADULT_ONLY,
                                            brand_id=brand.id))


def sort_items_categories(conn):
    array_query = sa.func.array(
        sa.select([core_items_categories.c.category_id]).where(
        core_items_categories.c.item_id == core_items.c.id).label("categories"))

    query_ = conn.execute(sa.select(
        [core_items.c.id, array_query, core_items.c.name]).where(
        core_items.c.is_active==True))

    item_has_category = query_.fetchall()

    return item_has_category


brand_id_17 = (5, 6, 7, 9, 16, 17, 25, 28, 30, 33, 35, 38, 40, 42, 45, 46, 47,
               48, 50, 51, 52, 53, 61, 64, 67, 69, 76, 79, 80, 85, 88, 92, 94,
               95, 99, 100, 112, 113, 114, 115, 119, 120, 124, 126, 127, 138,
               139, 142, 143, 145, 155, 157, 161, 163, 165, 167, 179, 197, 199,
               200, 202, 209, 230, 287, 288, 290, 301, 335, 353, 364, 365, 369,
               378, 412, 447, 453, 454, 462, 463, 471, 480, 486, 487, 513, 515,
               550, 568, 659, 660, 661, 665, 667, 668, 669, 675, 676, 679, 682,
               754, 784, 814, 895, 932, 942, 943, 975, 1016, 1035, 1049, 1172,
               1190, 1367, 1601, 2080, 2547, 2559, 2565, 2588, 2600, 2697, 2698,
               2699, 2702, 2703, 2704, 2705, 2707, 2718, 2723, 2725, 2726, 2727,
               2731, 2732, 2733, 2735, 2736, 2739, 2740, 2741, 2742, 2743, 2745,
               2779, 2862, 3184, 3186, 3187, 3397, 3486, 4059, 4119, 4137, 4247,
               4381, 4387, 4501, 4582, 5003, 5170, 5172, 5173, 5182, 5253, 5268,
               5271, 5285, 5408, 5567, 5568, 5610, 5612, 5615, 5617, 5618, 5619,
               5851, 5939, 6171, 6677, 6683, 6684, 6752, 6753, 7039, 7127, 7515,
               7516, 7520, 7522, 7523, 7555, 7556, 7557, 7720, 7761, 7762, 7806,
               7814, 7867, 7904, 8150, 8401, 8581, 8583, 8587, 8666, 8748, 8818,
               8875, 8972, 9004, 9023, 9060, 9090, 9106, 9250, 9251, 9314, 9628,
               9676, 9815, 9866, 10095, 13131, 13288, 13444, 14282, 14283,
               14444,
               14862, 15085, 15538, 16064, 16148, 16349, 16478, 16643, 17314,
               17576,
               18084, 18316, 18335, 18698, 19211, 19232, 19947, 21588, 22140,
               22147,
               22409, 22430, 22760, 22857, 22863, 22865, 23095, 23217, 23268,
               24010,
               24068, 24446, 24462, 24495, 24672, 24696, 24844, 25189, 25549,
               26177,
               26398, 26705, 26739, 27207, )

category_17_book = [17, 21, 24 ,25, 30, 32, 33, 37, 84, 85, 87, ]

all_ages_book = [18, 19, 20, 22, 23, 26, 27, 28, 29,  31,  34, 35,
                                    36,  38, 39, 40, 74, 78, 86, ]

def age_restrict_for_books(conn, rows):
        if any(x in category_17_book for x in rows[1]):
            conn.execute(core_items.update().where(core_items.c.id==rows[0]).values(parentalcontrol_id=ADULT_ONLY))

        elif all(x in all_ages_book for x in rows[1]):
            conn.execute(core_items.update().where(core_items.c.id==rows[0]).values(parentalcontrol_id=ALL_AGES))

if __name__ == "__main__":
    patch_parental_control()
