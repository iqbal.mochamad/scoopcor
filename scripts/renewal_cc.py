import os, sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.paymentgateway_connectors.creditcard.auto_renew import CreditCardRenew
from app import app

def main():
    CreditCardRenew().renewal()

if __name__ == '__main__':
    ctx = app.app_context()

    try:
        ctx.push()
        main()
    except:
        pass

    finally:
        ctx.pop()
