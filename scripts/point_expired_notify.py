from app.points.api import pointexpirednotification
from app import app

def main():
    ctx = app.test_request_context('/')
    ctx.push()

    pointexpirednotification()

    ctx.pop()

if __name__ == '__main__':
    main()
