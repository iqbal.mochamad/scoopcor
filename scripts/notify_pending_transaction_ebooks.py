import os, sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.notification.ebooks.helpers import notification_pending_transaction

def main():
    notification_pending_transaction()

if __name__ == '__main__':
    main()
