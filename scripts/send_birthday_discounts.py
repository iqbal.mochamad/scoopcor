#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
Send Birthday Discounts
=======================

E-mail script that looks up any users whose birthday occurs today.

The template used for the e-mail is different based upon the user's gender,
and the discount code that is included is a single-use code that can only be
redeemed for that user's account.
"""
from __future__ import unicode_literals

import argparse
from datetime import date, datetime, timedelta
from decimal import Decimal
from logging import getLogger
import os
from smtplib import SMTPException

from app import app, db
from app.discounts.helpers import generate_personalized_discount_code
from app.discounts.models import Discount, DiscountCode, DiscountCodePermittedUsers
from app.master.models import Campaign
from app.messaging.email import SmtpServer, HtmlEmail
from app.services.scoopcas import ScoopCas
from app.services.scoopcas.entities import User


GENDER_MALE = 1
GENDER_FEMALE = 2

_log = getLogger('scoopcor.script.birthday')

smtp = SmtpServer(host=app.config[''], user=app.config[''],
                      password=app.config[''], port=app.config[''],
                      use_tls=app.config[''])


def main(ref_date, campaign_id, discount_valid_length):
    """ Main program loop to generate all

    :param `datetime.date` ref_date:
    :param `int` campaign_id: The campaign ID this discount should be
        registered to.
    :param `datetime.timedelta` discount_valid_length: The number of days a
        user has to redeem their discount before it expires.
    :return: Nothing
    :rtype: None
    """
    s = db.session()

    valid_from = datetime.utcnow()
    valid_to = valid_from + discount_valid_length

    campaign = s.query(Campaign).get(campaign_id)
    if not campaign or not campaign.is_active:
        raise RuntimeError("Campaign ID {} does not exist or is inactive.  "
                           "Cannot run script.".format(campaign_id))

    try:

        for user in get_users_with_birthdays(ref_date):

            user_amount_discount, user_magbook_discount = \
                construct_discount_models(user, valid_from, valid_to)

            email_message = construct_email_body(
                bookmag_discount_code=user_magbook_discount.discount_code_personalized.code,
                amount_discount_code=user_amount_discount.discount_code_personalized.code,
                gender=user.gender)

            smtp.send_message(email_message)

            # only save after successfully sending the user's e-mail
            s.add(user_magbook_discount)
            s.commit()

    except SMTPException:
        _log.exception("Failed sending e-mail to user: {0.email}".format(user))
    except Exception:
        _log.exception("An unexpected error occurred -- bailing out!")
        raise


def construct_discount_models(user, valid_from, valid_to):
    user_magbook_discount = Discount(
        name="Birthday Discount for {}".format(user.user_id),
        description="Magazine/Book Coupon",
        valid_from=valid_from,
        valid_to=valid_to,

        discount_rule=1,
        discount_type=DISCOUNT_CODE_PERSONALIZED,
        discount_subtype=BIRTHDAY_DISCOUNT,
        discount_status=None,
        discount_schedule_type=None,

        is_active=False,
        discount_usd=None,
        discount_idr=None,
        discount_point=None,

        min_usd_order_price=None,
        max_usd_order_price=None,
        min_idr_order_price=None,
        max_idr_order_price=None,

        predefined_group=None,

        vendor_participation=None,
        partner_participation=None,
        sales_recogniation=None,

        # TODO: this needs to be a discount code instead
        discount_code_personalized=DiscountCodePersonalized(
            code=generate_personalized_discount_code(),
            user_id=user.id,
            note="Auto generated on {} by birthday emailer script".format(
                datetime.utcnow().isoformat())
        )
    )
    user_amount_discount = Discount(
        name="Birthday Discount for {}".format(user.user_id),
        description="$4 USD Discount",
        valid_from=valid_from,
        valid_to=valid_to,

        discount_rule=1,
        discount_type=DISCOUNT_CODE_PERSONALIZED,
        discount_subtype=BIRTHDAY_DISCOUNT,
        discount_status=None,
        discount_schedule_type=None,

        is_active=False,
        discount_usd=Decimal(4.00),
        discount_idr=None,
        discount_point=None,

        min_usd_order_price=Decimal(4.00),
        max_usd_order_price=None,
        min_idr_order_price=None,
        max_idr_order_price=None,

        predefined_group=None,

        vendor_participation=None,
        partner_participation=None,
        sales_recogniation=None,

        discount_code_personalized=DiscountCodePersonalized(
            code=generate_personalized_discount_code(),
            user_id=user.id,
            note="Auto generated on {} by birthday emailer script".format(
                valid_from.isoformat())
        )
    )
    return user_amount_discount, user_magbook_discount


def get_users_with_birthdays(ref_date):
    cas = ScoopCas(
        base_url=app.config['SERVER_SCOOPCAS'],
        auth_token=app.config['AUTH_HEADER'])

    users = cas.resource(User).search(
        User.birthdate.match_partial(
            month=ref_date.month,
            day=ref_date.day))

    return users


def construct_email_body(bookmag_discount_code, amount_discount_code, gender=None):

    if gender == GENDER_MALE:
        html_template = os.path.join('emails', 'user-birthday', 'male.html')
        text_template = os.path.join('emails', 'user-birthday', 'male.txt')
    elif gender == GENDER_FEMALE:
        html_template = os.path.join('emails', 'user-birthday', 'female.html')
        text_template = os.path.join('emails', 'user-birthday', 'female.txt')
    else:
        html_template = os.path.join('emails', 'user-birthday', 'male.html')
        text_template = os.path.join('emails', 'user-birthday', 'male.txt')

    return HtmlEmail(
        sender=app.config[''],
        to=[],
        subject='Happy Birthday! From Scoop',
        html_template=html_template,
        text_template=text_template,
        debug_mode=app.debug or app.testing,
        bookmag_discount_code=bookmag_discount_code,
        amount_discount_code=amount_discount_code)


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--ref-date',
        type=lambda inputstr: datetime.strptime(inputstr, '%Y-%m-%d').date(),
        default=date.today(),
        dest='ref_date',
        help='YYYY-MM-DD formatted date.')
    parser.add_argument(
        '--offer-id',
        type=int,
        required=True,
        dest='offer_id',
        help="The scoopcor_db's core_offers.id for the offer to be included."
    )
    parser.add_argument(
        '--campaign-id',
        type=int,
        required=True,
        dest='campaign_id',
        help="The scoopcor_db's core_campaigns.id for the discount to be included."
    )
    parser.add_argument(
        '--discount-valid-days',
        type=lambda x: timedelta(days=x),
        dest='discount_valid_length',
        default=timedelta(days=7),
        help="The number of days, within which the user must redeem their discount"
    )

    main(**vars(parser.parse_args()))
