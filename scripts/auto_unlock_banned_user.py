import os, sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.constants import RoleType
from app.reports.blacklist import BlacklistReportUser, update_user_access, BlacklistReportScreenshot

from datetime import datetime
from app import app, db


def unlock_banned_user():
    ctx = app.test_request_context('/')
    ctx.push()

    exist_user_banned = BlacklistReportUser.query.filter(
        BlacklistReportUser.is_active == True, datetime.now() >= BlacklistReportUser.expired_date).all()

    # exist_banned_user yang aktif dan expired
    for banned_user in exist_user_banned:

        # retrieve all blacklist report by user!
        exist_banned_user = BlacklistReportUser.query.filter_by(user_id=banned_user.user_id).all()

        if len(exist_banned_user) >= app.config['SCREENSHOT_VIOLATION_UNLOCK']:
            # if user exceeded from quota unlock, do nothing USER ALREADY PERMANENT LOCK
            pass
        else:
            # clean all reporting screenshots by user
            active_report_screenshots = BlacklistReportScreenshot.query.filter_by(
                user_id=banned_user.user_id, is_active=True).all()

            for report in active_report_screenshots:
                report.is_active = False
                db.session.add(report)

            banned_user.is_active = False
            db.session.add(banned_user)
            db.session.commit()

            if '@guest.scoop' in banned_user.user.email:
                update_user_access(banned_user.user, RoleType.verified_user.value)
            else:
                update_user_access(banned_user.user, RoleType.unverified_user.value)

    ctx.pop()


if __name__ == '__main__':
    unlock_banned_user()
