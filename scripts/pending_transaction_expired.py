import os, sys

from sqlalchemy import desc


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app import app, db
from datetime import datetime, timedelta
from app.orders.models import Order
from app.orders.choices import NEW, WAITING_FOR_PAYMENT, PAYMENT_IN_PROCESS, EXPIRED
from app.payments.models import Payment


def main():
    """
        This cron for collect all pending transactions order : Waiting for payment or new or in process more than 24H
        then set all to Expired statuses
    """

    # retrieve data 7 days and -H1 from now()
    pending_from = datetime.now() - timedelta(days=7)
    pending_to = datetime.now() - timedelta(hours=24)

    pending_order = Order.query.filter(
        Order.created >= pending_from, Order.created <= pending_to,
        Order.order_status.in_([NEW, WAITING_FOR_PAYMENT, PAYMENT_IN_PROCESS])).order_by(desc(Order.id)).all()

    for order in pending_order:
        order.order_status = EXPIRED
        db.session.add(order)

        orderlines = order.get_order_line()
        for orderline in orderlines:
            orderline.orderline_status = EXPIRED
            db.session.add(orderline)

        exist_payment = Payment.query.filter_by(order_id=order.id).first()
        if exist_payment:
            exist_payment.payment_status = EXPIRED
            db.session.add(exist_payment)

        db.session.commit()
        print 'Success update order status : {} to Expired'.format(order.id)


if __name__ == '__main__':
    ctx = app.app_context()

    try:
        ctx.push()
        main()
    except:
        pass

    finally:
        ctx.pop()
