from app.orders.deliver_items import delivery_async_email
from app import app

def main():
    ctx = app.test_request_context('/')
    ctx.push()

    delivery_async_email()

    ctx.pop()

if __name__ == '__main__':
    main()
