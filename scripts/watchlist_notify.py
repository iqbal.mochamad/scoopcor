from app.notification.api import watchlist_notifications

def main():
    return watchlist_notifications()

if __name__ == '__main__':
    main()
