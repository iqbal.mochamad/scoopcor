import logging
import os
import sys

from datetime import datetime
from sqlalchemy import desc

from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.items.helpers import get_item_request_body, get_item_cache_key
from app.utils.redis_cache import RedisCache

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app import db, app
from app.items.models import Item


_log = logging.getLogger('scoopcor.script.generate_item_api_cache')


def main():
    """ Generate cache data for item api (Latest Item API)

    :return:
    """
    session = db.session()
    items = session.query(Item).filter(
        Item.is_active == True,
        Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME]
    ).all()

    start = datetime.now()
    _log.info('Generate cache data, started at: {}'.format(start))
    redis_cache = RedisCache(redis=app.kvs)
    try:
        for item in items:
            key = get_item_cache_key(item_id=item.id, platform_id=1)
            item_data = get_item_request_body(item, platform_id=1)
            redis_cache.write(key, item_data)
            key = get_item_cache_key(item_id=item.id, platform_id=2)
            item_data2 = get_item_request_body(item, platform_id=2)
            redis_cache.write(key, item_data2)
            key = get_item_cache_key(item_id=item.id, platform_id=3)
            item_data4 = get_item_request_body(item, platform_id=4)
            redis_cache.write(key, item_data4)
    except Exception as e:
        _log.exception(str(e))

    end = datetime.now()
    _log.info('Generate cache data, ended at {}, duration {}s'.format(end, (end-start).seconds))


if __name__ == '__main__':
    main()
