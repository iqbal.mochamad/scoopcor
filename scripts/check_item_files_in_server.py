#!/usr/bin/env python
"""
Item Files Check Script
====================

this script check item files in NAS attached to static folder in COR

Example Usage
-------------

.. code-block:: bash

    $ python scripts/check_item_files_in_server.py --book-min-age=4000 --magazine-min-age=30 --newspaper-min-age=30

the output file is: check_item_files.script.log
"""
from argparse import ArgumentParser

import boto3
import os
import sys

from datetime import timedelta, datetime

from flask import logging

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from logging.config import dictConfig
from app import app, db
from app.items.models import Item, ItemFile

FILE_PATH = os.path.join(app.static_folder, 'item_content_files', 'processed')
# '/srv/scoopcor/www/app/static/item_content_files/processed'

_log = logging.getLogger('scoopcor.script.itemfiles')


def main(book_min_age, magazine_min_age, newspaper_min_age):

    _log.info('Check item files in server\n'
              'File Path: {path}\n'
              'Job Start at {timestart}\n\n'
              'item_id, item_type, error_seq, nas_status, '
              's3_status, nas_file_size, s3_file_size, release_date, content_type, brand_id, file_name'.format(timestart=datetime.now(), path=FILE_PATH))

    schedule_check_item_files('book', datetime.now() - book_min_age)
    schedule_check_item_files('magazine', datetime.now() - magazine_min_age)
    schedule_check_item_files('newspaper', datetime.now() - newspaper_min_age)

    _log.info("\nJob Done at {}.".format(datetime.now()))


def schedule_check_item_files(item_type, minimum_release_date):
    """

    :param `string` item_type:
    :param `datetime` minimum_release_date:
    :return:
    """
    s = db.session()

    result_set = s.query(Item).filter(
        Item.item_type == item_type,
        Item.release_date >= minimum_release_date,
        Item.is_active == True,
        Item.item_status == 'ready for consume')

    s3 = boto3.client(
            's3',
            app.config['BOTO3_AWS_REGION'],
            aws_access_key_id=app.config['BOTO3_AWS_APP_ID'],
            aws_secret_access_key=app.config['BOTO3_AWS_APP_SECRET']
        )

    for item in result_set:
        try:
            # check if item_file data exists
            item_file = s.query(ItemFile).filter(ItemFile.item_id == item.id).first()

            if not item_file:
                _log.info("{item_id}, 1, 'Not found in core_itemfiles', , ".format(item_id=item.id))
            else:

                file_name = item_file.file_name.replace('images/1/', '')
                item_file_full_path = os.path.join(FILE_PATH, file_name)

                nas_status = ""
                nas_file_size = 0
                # check in NAS
                if not os.path.isfile(item_file_full_path):
                    nas_status = 'Not found in NAS'
                else:
                    nas_file_size = os.path.getsize(item_file_full_path)

                # check in amazon s3
                try:
                    s3_status = ""
                    s3_file_size = 0
                    s3_file = s3.head_object(Bucket=app.config['BOTO3_BUCKET_NAME'],
                                                Key=item_file.file_name)
                    s3_file_size = s3_file.get('ContentLength', 0) if s3_file else 0
                except Exception as e:
                    s3_status = "Not found in S3"

                if s3_status or nas_status or nas_file_size != s3_file_size:
                    _log.info("{item_id}, '{item_type}', 2, '{nas_status}', '{s3_status}'"
                              ", '{nas_file_size}', '{s3_file_size}'"
                              ", '{release_date}', '{content_type}'"
                              ", {brand_id}, '{file_name}'".format(
                                item_id=item.id,
                                item_type=item_type,
                                nas_status=nas_status,
                                s3_status=s3_status,
                                s3_file_size=s3_file_size,
                                nas_file_size=nas_file_size,
                                brand_id=item.brand_id,
                                file_name=file_name,
                                release_date=item.release_date,
                                content_type=item.content_type,
                              ))

        except Exception as e:
            _log.error("{item_id}, 3, '{err}', ''".format(
                item_id=item.id,
                err=str(e),
                ))

        finally:
            s.expunge_all()


def initialize_logging():

    dictConfig({
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'script_format': {
                'format': '%(message)s'
            }
        },
        'handlers': {
            'item_files_log_file': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'formatter': 'script_format',
                'filename': 'check_item_files.script.log'
            },
        },
        'loggers': {
            'scoopcor.script.itemfiles': {
                'handlers': ['item_files_log_file', ],
                'propagate': False,
                'level': 'INFO',
            },
        }})


if __name__ == "__main__":

    initialize_logging()

    def timedelta_from_int(value):
        value = int(value)
        return timedelta(days=value)

    parser = ArgumentParser()
    parser.add_argument(
        '--book-min-age',
        dest='book_min_age',
        type=timedelta_from_int,
        required=False,
        default=timedelta(days=4650),
    )
    parser.add_argument(
        '--magazine-min-age',
        dest='magazine_min_age',
        type=timedelta_from_int,
        required=True
    )
    parser.add_argument(
        '--newspaper-min-age',
        dest='newspaper_min_age',
        type=timedelta_from_int,
        required=True,
    )

    main(**vars(parser.parse_args()))
