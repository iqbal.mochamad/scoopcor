import os, sys


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from datetime import datetime, timedelta
from app import app, db
from app.eperpus.libraries import SharedLibraryBrand, process_renewal_eperpus, process_wallet_eperpus, \
    create_new_org_brand_renewal, transaction_renewal_note
from app.users.wallet import Wallet
from app.orders.deliver_items import send_auto_renewal_email_transaction
from app.orders.helpers import update_order_payments
from app.offers.choices.status_type import READY_FOR_SALE
from sqlalchemy import desc, and_, or_


def auto_renewal_eperpus():
    ctx = app.test_request_context('/')
    ctx.push()

    end_renewal_time = (datetime.now() + timedelta(days=3)).replace(hour=0, minute=0, second=0, microsecond=0)
    start_renewal_time = datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)

    # OBJECT OF ORGANIZATION BRAND , RENEWAL FOR DURATION AND EDITION
    exist_renewal_obj = SharedLibraryBrand.query.filter(or_(and_(SharedLibraryBrand.current_offer_stock == 0,
                                                                 SharedLibraryBrand.current_offer_type == 'edition',
                                                                 SharedLibraryBrand.is_active == True,
                                                                 SharedLibraryBrand.renewal_status == 'new',
                                                                 SharedLibraryBrand.is_renewal == True),
                                                            and_(SharedLibraryBrand.valid_to <= end_renewal_time,
                                                                 SharedLibraryBrand.valid_to >= start_renewal_time,
                                                                  SharedLibraryBrand.current_offer_type == 'duration',
                                                                  SharedLibraryBrand.is_active == True,
                                                                  SharedLibraryBrand.renewal_status == 'new',
                                                                  SharedLibraryBrand.is_renewal == True))).order_by(desc(SharedLibraryBrand.id)).all()

    for exist_renewal in exist_renewal_obj:
        auto_renewal_process(exist_renewal_eperpus=exist_renewal)

    ctx.pop()


def auto_renewal_process(exist_renewal_eperpus):

    check_wallet = Wallet.query.filter_by(party_id=exist_renewal_eperpus.organization_id).first()

    if exist_renewal_eperpus.offer.offer_status != READY_FOR_SALE:
        # SEND EMAIL REGARDING OFFER NOT VALID
        renewal_note = {
            'failed_trx': 'Perpanjangan Otomatis Langganan Gagal pada {} karena Offer tidak valid'.format(
                datetime.now().isoformat())}

        transaction_renewal_note(exist_renewal_eperpus=exist_renewal_eperpus,
                                 renewal_note=renewal_note, renewal_status='fail', is_active=True)

        send_auto_renewal_email_transaction(exist_org=exist_renewal_eperpus,
                                            status_renewal=False,
                                            reason_fail='Offer tidak valid',
                                            user_pic=exist_renewal_eperpus.renewal_note['user_id_activate'])
    else:
        if check_wallet.balance >= exist_renewal_eperpus.offer.price_idr:
            try:
                # process order, orderlines, payment
                new_order, new_orderlines, new_payment = process_renewal_eperpus(exist_renewal_eperpus=exist_renewal_eperpus)

                # process wallet
                exist_wallet, wallet_trx = process_wallet_eperpus(exist_renewal_eperpus=exist_renewal_eperpus,
                                                                  new_order=new_order)

                # UPDATE RENEWAL TRANSACTION STATUS
                renewal_note = {'success_trx': 'Perpanjangan Otomatis Berhasil pada {}'.format(datetime.now().isoformat())}
                transaction_renewal_note(exist_renewal_eperpus=exist_renewal_eperpus,
                                         renewal_note=renewal_note,
                                         renewal_status='complete',
                                         is_active=True)

                # new org brand
                new_lib_brand = create_new_org_brand_renewal(exist_renewal_eperpus=exist_renewal_eperpus,
                                                             new_order=new_order)

                # UPDATE PAYMENT JADI COMPLETE
                update_order_payments(order_id=wallet_trx.order_id)

                db.session.add(exist_wallet)
                db.session.add(wallet_trx)
                db.session.add(new_lib_brand)
                db.session.commit()

                # SEND EMAIL AUTO RENEWAL SUCCESS
                send_auto_renewal_email_transaction(exist_org=new_lib_brand, status_renewal=True,
                                                    user_pic=exist_renewal_eperpus.renewal_note['user_id_activate'])
            except Exception as e:
                pass
        else:

            # SEND EMAIL REGARDING TO : WALLET NOT ENOUGH
            renewal_note = {'failed_trx': 'Perpanjangan Otomatis Langganan Gagal pada {} karena Deposit tidak cukup'.format(
                datetime.now().isoformat())}

            transaction_renewal_note(exist_renewal_eperpus=exist_renewal_eperpus, renewal_note=renewal_note,
                                     renewal_status='fail',
                                     is_active=True)

            send_auto_renewal_email_transaction(exist_org=exist_renewal_eperpus,
                                                status_renewal=False,
                                                reason_fail='Deposit tidak cukup',
                                                user_pic=exist_renewal_eperpus.renewal_note['user_id_activate'])


if __name__ == '__main__':
    auto_renewal_eperpus()
