import os, sys

from datetime import datetime
from sqlalchemy import desc

from app.uploads.upload_pdf import UploadPDFProcessing

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.uploads.aws.ebook_previews import transfer_files_previews
from app import app, db
from app.items.models import Item, ItemUploadProcess
from app.items.previews import preview_rule_factory
from app.logs.models import ItemProcessing

from app.items.choices import NEWSPAPER, ITEM_TYPES

BIZNET_PATH = app.config['BOTO3_BIZNET_WEBREADER']

# def get_items(brand_id=None, item_id=None):
#
#     if item_id:
#         item = Item.query.filter(Item.id == item_id, Item.page_count != None).all()
#     else:
#         item = Item.query.filter(Item.brand_id == brand_id, Item.page_count != None).order_by(Item.id).all()
#
#     for exist_item in item:
#         rules = preview_rule_factory(exist_item)
#         previews = ['{}.jpg'.format(i) for i in rules.get_page_numbers()]
#         transfer_files_previews(directory_path=BIZNET_PATH, brand_id=str(exist_item.brand_id),
#                                 item_id=str(exist_item.id), bucket_name=app.config['BOTO3_GRAMEDIA_PREVIEW_BUCKET'],
#                                 preview_files=previews)

def update_error(item_proc=None, error=None):
    item_proc.error = error
    item_proc.status = 9
    db.session.add(item_proc)
    db.session.commit()

def upload_covers_previews(item_id=None):

    # THIS IS TEMPORARY FOR CREATED COVER AND PREVIEWS NEWSPAPER,COS NEWSPAPER STILL ON BIZNET UPLOADER
    if item_id:
        item_process = ItemProcessing.query.filter_by(item_id=item_id).order_by(desc(ItemProcessing.id)).all()
    else:
        item_process = ItemProcessing.query.filter_by(status=3).order_by(desc(ItemProcessing.id)).all()

    for item_data in item_process:
        master_item = Item.query.filter_by(id=item_data.item_id).first()

        try:
            if master_item.item_type.strip() != ITEM_TYPES[NEWSPAPER]:
                item_data.status = 5
                db.session.add(item_data)
                db.session.commit()
                print "Item are not Newspapers", master_item.id
            else:
                item_upload = ItemUploadProcess.query.filter_by(item_id=master_item.id, status=12).first()
                if not item_upload:
                    item_upload = ItemUploadProcess.query.filter_by(item_id=master_item.id).first()
                    if not item_upload:
                        item_upload = ItemUploadProcess(
                            pic_id = 1,
                            pic_name = 'otomate@cron',
                            role_id = 1,
                            item_id = master_item.id,
                            release_schedule = master_item.release_schedule,
                            start_process_time = datetime.now(),
                            file_name = '{}.pdf'.format(master_item.edition_code),
                            file_dir = '/tmp/original_local/{}'.format(master_item.brand_id),
                            file_size = 0,
                            status = 10,
                            file_check = 0,
                            error=None
                        )
                        db.session.add(item_upload)
                        db.session.commit()

                    scene = UploadPDFProcessing(item_upload.item, item_upload)
                    status, message = scene.validate_size_raw(skip_check=True)
                    if not status:
                        print message
                    else:
                        print 'processing...', master_item.id, item_upload.item.brand_id

                    status_preview, message_preview = scene.create_preview()
                    if not status_preview:
                        update_error(item_upload, message_preview)
                    print message_preview

                    status_cover, message_cover = scene.create_cover()
                    if not status_cover:
                        update_error(item_upload, message_cover)
                    print message_cover
                    filpath = os.path.join(item_upload.file_dir, item_upload.file_name)

                    if status_cover and status_preview:
                        item_upload.status=12
                        db.session.add(item_upload)

                        item_data.status = 5
                        db.session.add(item_data)
                        db.session.commit()

                        os.remove(filpath)
                        #break
                    else:
                        os.remove(filpath)
                else:
                    item_data.status = 5
                    db.session.add(item_data)
                    db.session.commit()
        except Exception as e:
            print e

if __name__ == '__main__':
    # uncomment this for upload bulk files inside brand_id.
    upload_covers_previews()
