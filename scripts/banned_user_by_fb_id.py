import os
# import logging

from sqlalchemy import create_engine
from app import app

cor = app.config['SQLALCHEMY_DATABASE_URI']

SQLALCHEMY_DATABASE_URI_CAS = \
    'postgresql://{user}:{password}@{host}:{port}/{db_name}'.format(
        user=os.getenv("COR_DB_USER", "scoopcor"),
        password=os.getenv("COR_DB_PASS", "hobbes"),
        host=os.getenv("COR_DB_HOST", "localhost"),
        port=os.getenv("COR_DB_PORT", "5432"),
        db_name=os.getenv("CAS_DB_NAME", "scoopcas_db"))



# logging.basicConfig()
# logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

engine_cor = create_engine(cor)

engine_cas = create_engine(SQLALCHEMY_DATABASE_URI_CAS)

SUSPECT_FB_ID = ['100000083737945', '100000545861960', '1003384124', '10153085269962956', '1040736106', '1091575705',
                 '1103211253', '1336504515', '1337375309', '1405533883', '1645706252', '571464044', '597673072',
                 '648479159', '739593204', '837577859640849', ]

CAS_TABLE_HAS_USER_ID = ['cas_clients', 'cas_log_deauthorized',
                         'cas_log_devicehistory', 'cas_points', 'cas_profiles', 'cas_refreshtokens',
                         'cas_tokens', 'cas_userdevices', 'cas_users_roles', ]

IMPORTANT_TABLE = ['core_userbuffets', 'core_useritems', 'core_orders', 'core_orderdetails', 'core_itemdownloads',
                   'core_usersubscriptions']

COR_TABLE_HAS_USER_ID = ['core_applereceiptidentifiers', 'core_buffetpageviews', 'core_discountcodes_permitted_users',
                         'core_iapreceiptdata', 'core_itemdownloads', 'core_itemlogs', 'core_logs', 'core_logs_iab',
                         'core_logs_orders', 'core_logs_orderstransactions', 'core_logs_payments', 'core_orderdetails',
                         'core_orderlines', 'core_orderlinetemps', 'core_orders', 'core_ordertemps',
                         'core_paymentinappbillings', 'core_payments', 'core_paymenttokens', 'core_pointprofiles',
                         'core_pointuses', 'core_refunds', 'core_userbuffets', 'core_useritems',
                         'core_userpoints', 'core_usersubscriptions', 'core_usersubscriptions_items', ]

UNECESSARY = ['core_useractivities','cas_log_apilog', 'cas_cafes_passwords',]


def get_latest_id(fb_user_id):
    con = engine_cas.connect()
    user_data = con.execute("SELECT id FROM cas_users WHERE fb_id='{}'".format(fb_user_id))
    all_id = [row[0] for row in user_data]
    max_ = max(all_id)
    return {'active_id': max_, 'set_inactive_id': [id for id in all_id if id != max_]}


def get_user_id_in_table(table_name, user_id, con):
    data_ = []
    for user in user_id['set_inactive_id']:
        data_exist = con.execute("SELECT user_id FROM {} WHERE user_id='{}'".format(table_name, user))
        data_.append(data_exist)

    return data_


def update_user_id_in_table(table_name, banned_user_id, latest_user_id, con):
    con = engine_cor.connect()
    for banned_user in banned_user_id:
        con.execute("UPDATE {} SET user_id={} WHERE user_id='{}'".format(table_name, latest_user_id, banned_user))
        con.commit()
        con.close()
        print("updating {} to {} from {}".format(banned_user_id, latest_user_id, table_name))


def update_per_table(user_ids, con, list_table):
    for table in list_table:
        print(table)
        if get_user_id_in_table(table, user_ids, con):
            update_user_id_in_table(table, user_ids['set_inactive_id'], user_ids['active_id'], con)
            print("UPDATE USER_ID {}".format(user_ids))
        else:
            update_user_id_in_table(table, user_ids['set_inactive_id'], user_ids['active_id'], con)
            print("UPDATE USER_ID {}".format(user_ids))


def set_user_inactive(banned_user_id, con):
    con.execute("UPDATE cas_users set is_active = false, role_id = 19 where user_id ={}".format(banned_user_id))
    con.commit()
    con.close()


def update_data_in_cor():
    for fb_id in SUSPECT_FB_ID:
        current_id = get_latest_id(fb_id)
        update_per_table(user_ids=current_id, con=engine_cor.connect(), list_table=COR_TABLE_HAS_USER_ID)


def update_data_in_cas():
    for fb_id in SUSPECT_FB_ID:
        current_id = get_latest_id(fb_id)
        update_per_table(user_ids=current_id, con=engine_cor.connect(), list_table=CAS_TABLE_HAS_USER_ID)

#
# def update_important_table():
#     for table_name in IMPORTANT_TABLE:

