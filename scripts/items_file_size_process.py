from argparse import ArgumentParser
from sqlalchemy import desc

from app import app, db
from app.items.models import Item, ItemFile
from app.uploads.aws.helpers import connect_gramedia_s3, aws_check_files


def update_item_files(max_record):
    server = connect_gramedia_s3()
    bucket = app.config['BOTO3_GRAMEDIA_MOBILE_BUCKET']
    items = Item.query.filter_by(file_size=None).limit(max_record).all()
    for item in items:
        brand_id = item.brand_id
        item_file = ItemFile.query.filter_by(item_id=item.id).order_by(desc(ItemFile.id)).first()
        if item_file:
            file_name = item_file.file_name.split('/')[-1].strip()
            location = '{brand_id}/{file_name}'.format(brand_id=brand_id, file_name=file_name)
        else:
            location = '{brand_id}/{file_name}.zip'.format(brand_id=brand_id, file_name=item.edition_code)
        check_file = aws_check_files(server, bucket, location)
        item.file_size = check_file.get('ContentLength') if check_file is not None else 0
        db.session.add(item)
        db.session.commit()


def main(max_record):
    update_item_files(max_record)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        '--max-record',
        dest='max_record',
        type=int,
        required=False,
        default=100,
    )
    main(**vars(parser.parse_args()))
