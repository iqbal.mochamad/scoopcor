import os, sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app import db, app

def main():

    refresh_sql = [
        "REFRESH MATERIALIZED VIEW CONCURRENTLY utility.item_later_editions;",
        "REFRESH MATERIALIZED VIEW CONCURRENTLY utility.item_downloads;",
        "REFRESH MATERIALIZED VIEW CONCURRENTLY utility.item_popular;"]

    for raw_sql in refresh_sql:
        try:
            print('start execute raw sql : {}'.format(raw_sql))
            db.engine.execute(raw_sql)
            print('finish execute raw sql : {}'.format(raw_sql))

        except Exception as e:
            print("Error when execute raw sql {} Error {}".format(raw_sql, e))

    try:
        print('start redis clear')
        for data in app.kvs.keys('LATEST:*'):
            app.kvs.delete(data)
        print('end redis clear')
    except Exception as e:
        print("Error when execute clear redis Error {}".format(e))

if __name__ == '__main__':
    main()
