import argparse
import logging
import os
import sys

# add the parent directory to the python path so we can
# get at our models
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app import app
from app.paymentgateway_connectors.apple_iap.apple_iap_renewal import AppleIAPRenewal


_log = logging.getLogger("scoopcor.script.apple_iap_renewal_script")


def renewal_construct(limit=100, backdays=14):
    _log.info("Run Apple IAP Renewal Process with limit {}, and back days {} -- start.".format(limit, backdays))
    renewal = AppleIAPRenewal(limit, None, backdays).construct()
    _log.info("Result: {}".format(renewal.data))
    _log.info("Run Apple IAP Renewal Process with limit {}, and back days {} -- end.".format(limit, backdays))
    return renewal


def get_command_line_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument(
       "--limit",
       dest="limit",
       type=int,
       required=False,
       default=100,
       help="Maksimum transaction that can be executed (default 100)")

    parser.add_argument(
        "--backdays",
        dest="backdays",
        type=int,
        required=False,
        default=14,
        help="Number of back days to get expired transaction (default 14 days)")

    command_line_args = vars(parser.parse_args())

    return command_line_args


if __name__ == "__main__":

    try:

        with app.test_request_context('/'):
            args = get_command_line_arguments()
            sys.exit(renewal_construct(**args))

    except Exception as e:
        _log.exception("Script failed with an unknown error")
        raise
