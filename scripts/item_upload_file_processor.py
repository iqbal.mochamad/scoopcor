from __future__ import unicode_literals

from app import app, db
from app.items.models import ItemUploadProcess
from app.items.choices import ItemUploadProcessStatus
from app.uploads import _log, helpers


def main(session):
    """

    :param `app.items.models.Item` item:
        The item for which we need to process it's files.
    :return:
    """
    for item_upload in get_all_pending_uploads(session):
        try:
            pass
        except Exception as e:
            pass


def get_all_pending_uploads(session):
    return session.query(ItemUploadProcess).filter_by(
        status=ItemUploadProcessStatus.new.value)



if __name__ == '__main__':
    session = db.session()
    main(session)
