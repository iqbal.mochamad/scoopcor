"""
Item Files Upload to S3 Script
====================

This script upload item files to s3 from list of item id's.


Example Usage
-------------

.. code-block:: bash

    $ python scripts/item_files_upload_to_s3.py --item_id=96031

sample log info:
Upload API response: {'results': [
{'item_id': '96031',
'upload_status': 'File not found in local server',
'item_file': u'images/1/30116/JP_DCI2016MTH01WEMB135791113151719.zip'}]}

"""
from argparse import ArgumentParser
import os
import sys
import logging

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app import app, db
from app.items.api_v1 import ItemFileUploadToServerApi


_log = logging.getLogger('scoopcor.script.s3')


def main(item_id):

    if item_id:
        _log.info("Uploading file for item_id {}".format(item_id))

        upload_api = ItemFileUploadToServerApi()
        response = upload_api.post_item_to_s3_from_script(item_id=item_id)

        _log.info("Upload API response: {}".format(str(response)))


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument(
        '--item_id',
        dest='item_id',
        required=True,
        help="item_id is required.",
    )

    main(**vars(parser.parse_args()))
