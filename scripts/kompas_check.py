import os, sys
from datetime import datetime
from sqlalchemy import desc

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.shared_client.models import PartnerAuth
from app.shared_client.helpers import get_active_kompas_subs, deliver_subs_order


def main():
    existing_linked_user = PartnerAuth.query.filter(PartnerAuth.order_id == None).order_by(PartnerAuth.id).all()

    for data_exist in existing_linked_user:
        expired_subs, partner_unique_code = get_active_kompas_subs(email=data_exist.partner_account,
                                                                   partner_active=data_exist)

        if expired_subs:
            deliver_subs_order(partner_active=data_exist, partner_unique_code=partner_unique_code)


if __name__ == '__main__':
    main()


