"""
New active newspapers count notification
========================================

Send email notifications about how many active newspapers available today
"""
from __future__ import unicode_literals, absolute_import
import os

import sys
from logging import getLogger
from smtplib import SMTPException

from app import db, app
from app.items.choices import ItemTypes, STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.items.models import Item
from app.messaging.email import get_smtp_server, HtmlEmail
from app.utils.datetimes import get_local

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))


_log = getLogger("")


def main(session=None):
    session = session or db.session()

    # get new active item
    _log.info("Start, Get new active newspaper")
    item_count = get_item_count(session)

    # get list of emails
    recipients = [
        "bg@apps-foundry.com",
        "fa@apps-foundry.com",
        "ah@apps-foundry.com",
        "calvin@apps-foundry.com",
        "rudi.satria@apps-foundry.com",
        "anwari.rahman@apps-foundry.com"]

    # send email notifications
    _log.info("Sending info about new active newspaper for today release date, item count = {}".format(
        item_count
    ))
    with app.app_context():
        send_email_notifications(recipients, item_count)
    _log.info("End")


def get_item_count(session):
    date_filter = get_local().replace(hour=0, minute=0, second=0, microsecond=0)
    item_count = session.query(Item.id).filter(
        Item.release_date >= date_filter,
        Item.item_type == ItemTypes.newspaper.value,
        Item.item_status == STATUS_TYPES[STATUS_READY_FOR_CONSUME],
        Item.is_active == True,
    ).count()
    return item_count


def send_email_notifications(recipients, item_count):
    try:
        smtp_server = get_smtp_server()
        msg = construct_email(recipients, item_count)
        smtp_server.send_message(msg)

    except SMTPException:
        _log.exception("Failed to send [new active newspaper] notifications"
                       " to: {}".format(recipients))
        return False


def construct_email(recipients, item_count):
    today = get_local().strftime("%d %b %Y")
    level = 'INFO' if item_count else 'CRITICAL'
    subject = '{}: {} newspaper active on {}'.format(level, item_count, today)
    return HtmlEmail(
        'SCOOP@apps-foundry.com',
        recipients,
        subject,
        'email/staff_notifications/new_active_newspaper.html',
        'email/staff_notifications/new_active_newspaper.txt',
        level=level,
        item_count=item_count,
        today=today)


if __name__ == '__main__':

    try:
        main()
    except Exception as e:
        _log.exception("Script failed with an unknown error")
        raise
