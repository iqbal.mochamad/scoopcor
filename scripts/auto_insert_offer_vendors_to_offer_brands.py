import os
import sys
from logging import getLogger

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app import db


_log = getLogger("scoopcor.scripts.auto_insert_offer_vendor_to_offer_brands")


def main():
    """ script to auto insert data to core_offers_brands based on offer-vendors relationship.
    (insert all the brand from vendor that registered in core_offers_vendors
        and not yet exists in core_offers_brands).

    ex: for Offer Premium Buffet that all item from vendor X is available.

    :return:
    """
    connection = db.engine.connect()

    trans = connection.begin()
    try:
        connection.execute("""
insert into core_offers_brands (offer_id, brand_id)
select distinct s.offer_id, b.id as brand_id
from core_offers_vendors s
    left join core_offers o on o.id = s.offer_id
    left join core_vendors v on v.id = s.vendor_id
    left join core_brands b on b.vendor_id = v.id
    left join core_offers_brands t on t.offer_id = o.id and t.brand_id = b.id
    left join core_items i on i.brand_id = b.id
where o.is_active = true
    and v.is_active = true
    and b.is_active = true
    and t.offer_id is null
    and i.is_active = true and i.item_status = 'ready for consume';
        """)
        trans.commit()
        _log.info('Auto insert Offer Vendors to Offer Brands, done.')
    except Exception as e:
        _log.exception('Error: Auto insert Offer Vendors to Offer Brands, error details: {}'.format(e))
        trans.rollback()


if __name__ == '__main__':
    main()
