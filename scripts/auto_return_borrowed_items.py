import os
import sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

import requests

from app import app, db


def main():
    connection = db.engine.connect()
    # url = 'http://dev6.apps-foundry.com:8983/solr/scoop-sharedlib-all/dataimport'
    url = '{}/scoop-sharedlib-all/dataimport'.format(app.config['SOLR_BASE_URL'])

    try:
        trans = connection.begin()

        # auto return overdue for specified items
        connection.execute("SELECT utility.return_overdue_borrowed_specified_items();")

        # auto return overdue item per content types
        connection.execute("SELECT utility.return_overdue_borrowed_items();")
        trans.commit()
    except:
        trans.rollback()

    # try:
    #     # insert new item/edition per brands from organization brands to organization items
    #     #     and from catalog-brands to catalog-items
    #     trans = connection.begin()
    #     connection.execute("SELECT utility.organization_brands_to_organization_item();")
    #     trans.commit()
    # except:
    #     trans.rollback()

    # synchronize org-item data + stock available info from postgre to solr
    requests.post(url, data={'command': 'delta-import', 'wt': 'json'})

if __name__ == '__main__':
    main()

