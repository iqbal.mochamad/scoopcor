import os, sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from sqlalchemy import or_

from app.items.choices import ItemUploadProcessStatus, STATUS_TYPES, STATUS_UPLOADED
from app.items.models import ItemUploadProcess
from app.logs.models import ItemProcessing

from app import db

def uploader_failed_monitor():

    try:
        upload_data = ItemUploadProcess.query.filter(or_(
            ItemUploadProcess.status == ItemUploadProcessStatus.failed.value,
            ItemUploadProcess.error != '')).all()

        for data_item in upload_data:

            # all reset
            data_item.item.item_status = STATUS_TYPES[STATUS_UPLOADED]
            db.session.add(data_item.item)

            item_processing = ItemProcessing.query.filter_by(item_id=data_item.item_id).all()
            for iprocess in item_processing:
                db.session.delete(iprocess)
                db.session.commit()

            data_item.status = ItemUploadProcessStatus.new.value
            data_item.error = None
            db.session.add(data_item)
            db.session.commit()

            path_file = os.path.join(data_item.file_dir, data_item.file_name)
            print os.path.exists(path_file)
            if os.path.exists(path_file):
                raw_size = os.path.getsize(path_file)
                if raw_size != data_item.file_size:
                    os.remove(path_file)

            print 'success reset uploader data', data_item.item_id

    except Exception as e:
        print 'Failed.. {}'.format(e)

if __name__ == '__main__':
    uploader_failed_monitor()


