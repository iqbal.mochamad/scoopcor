#!/usr/bin/env python
"""
Popular Items Script
====================

This script creates/rebuilds a materialized view in postgres that contains
our 'popular items' data.


Example Usage
-------------

.. code-block:: bash

    $ python scripts/popular_generator.py

"""
from argparse import ArgumentParser
import os
import sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app import db


def main():
    """ Creates the popular_items materialized view, or refeshes it's data.
    """
    if view_exists(db.engine, 'item_popular', schema='utility'):
        db.engine.execute(SQL_STATEMENTS['refresh'])
    else:
        db.engine.execute(SQL_STATEMENTS['create'])

def read_sql_file(filename):
    """ Reading sql files from sql directory

    :param `string` filename: name of sql file
    :return: content of sql file
    :rtype: `unicode`
    """
    file_path = os.path.join(BASE_DIR, 'sql', filename)
    with open(file_path, 'r') as f:
        sql = f.read()

    return sql


def view_exists(engine, viewname, schema):
    """ Does a table/view with viewname exist in a schema?

    :param `sqlalchemy.engine.Engine` engine:
    :param `str` viewname: Name of the view
    :param `str` schema: The postgresql schema name to check for `viewname`
    :return: Boolean indiciation of whether the requested table/view exists.
    :rtype: bool
    """
    query_text = (
        "SELECT EXISTS ( "
        "  SELECT 1 FROM pg_catalog.pg_class c "
        "  JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace"
        "  WHERE n.nspname = '{schema}' "
        "      AND c.relname = '{name}');".format(
            schema=schema, name=viewname))

    return engine.execute(query_text).scalar()


SQL_STATEMENTS = {
    'create':
        read_sql_file('item_downloads.create.sql') +
        read_sql_file('item_later_edition.create.sql') +
        read_sql_file('non_free_item.create.sql') +
        read_sql_file('item_is_in_buffet.sql') +
        read_sql_file('item_popular.create.sql'),
    'refresh':
        read_sql_file('refresh_materialized.sql')
}


if __name__ == '__main__':
    main()
