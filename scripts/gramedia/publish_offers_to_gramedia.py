#!/usr/bin/env python
"""
Publish Offers to Gramedia.com Product Catalog Script
============================================================

this script will send Scoop Offers to Gramedia.com Product Catalog

Example Usage
-------------

.. code-block:: bash

    $ python scripts/publish_offers_to_gramedia.py --max-records=500

the log file is: publish_offer_to_gramedia.script.log
"""
from argparse import ArgumentParser

import boto3
import os
import sys

from datetime import timedelta, datetime

from flask import logging

from app.services.gramedia.helpers import init_and_publish_offer_to_gramedia

BASE_DIR = os.path.dirname(os.path.abspath(__file__)).replace('/scripts', '')
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from logging.config import dictConfig


_log = logging.getLogger('scoopcor.script.publish_offer_to_gramedia')


def main(max_records):
    """

    :param 'int' max_records: how many records of Offers that will processed at one go
    :return:
    """

    start_time = datetime.now()
    _log.info('Publish Offers to Gramedia.com Product Catalog\n'
              'Job Start at {start_time}\n\n'.format(start_time=start_time))

    init_and_publish_offer_to_gramedia(
        log=_log,
        max_records=max_records,
        is_test_mode=False)

    end_time = datetime.now()
    delta = end_time - start_time
    processing_time = round(delta.seconds / 60.0, 2)

    _log.info("\nJob Done at {end_time}. Total processing time: {processing_time} min".format(
        end_time=end_time, processing_time=processing_time))


def initialize_logging():

    dictConfig({
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'script_format': {
                'format': '%(message)s'
            }
        },
        'handlers': {
            'publish_offer_to_gramedia_log_file': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'formatter': 'script_format',
                'filename': os.path.join(BASE_DIR, os.pardir, 'logs', 'publish_offer_to_gramedia.script.log')
            },
        },
        'loggers': {
            'scoopcor.script.publish_offer_to_gramedia': {
                'handlers': ['publish_offer_to_gramedia_log_file', ],
                'propagate': False,
                'level': 'INFO',
            },
        }})


if __name__ == "__main__":

    initialize_logging()

    parser = ArgumentParser()
    parser.add_argument(
        '--max-records',
        dest='max_records',
        type=int,
        required=False,
        default=500,
    )

    main(**vars(parser.parse_args()))
