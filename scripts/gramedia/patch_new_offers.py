import os, sys, csv, httplib
from unidecode import unidecode

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from sqlalchemy.sql import desc

from app.offers.models import Offer, OfferPlatform
from app.master.choices import ANDROID, IOS, WINDOWS_PHONE

from app.master.models import Brand, Vendor
from app.tiers.models import TierMatrix, TierAndroid, TierIos, TierWp
from app.items.models import Item

from app.offers.choices.offer_type import SINGLE
from app.offers.choices.status_type import READY_FOR_SALE, NOT_FOR_SALE

from sqlalchemy import desc
from app import app, db


class GramediaOffers(object):

    def __init__(self, vendor_id=None, limit=None, offset=None):
        self.vendor_id = vendor_id
        self.limit = limit
        self.offset = offset

        self.master_vendors = None
        self.master_brands = None
        self.master_items = []

        self.master_gramedia = []
        self.new_master_gramedia = []
        self.master_tiermatrix = []
        self.master_idrmatrix = []
        self.csv_master = []

    def get_vendors_brands(self):
        try:
            vendor = Vendor.query.filter_by(id=self.vendor_id, is_active=True).first()

            if vendor:
                self.master_vendors = vendor
                brands = Brand.query.filter_by(vendor_id=vendor.id).order_by(
                    desc(Brand.id)).limit(self.limit).offset(self.offset).all()
                self.master_brands = brands
                print '===== Found %s brands for %s' % (len(brands), self.master_vendors.name)
                return True, httplib.OK
            else:
                return False, 'ERROR Not found vendors'

        except Exception as e:
            return False, 'ERROR get_vendors_brands ERROR %s' % e

    def get_items(self):
        try:
            for ibrand in self.master_brands:
                item = Item.query.filter_by(brand_id=ibrand.id).all()
                self.master_items = self.master_items + item
            print '===== Found %s items for %s' % (len(self.master_items), self.master_vendors.name)
            return True, httplib.OK

        except Exception as e:
            return False, 'ERROR get_items ERROR %s' % e

    def get_offers(self):
        try:
            for ixtem in self.master_items:
                offer_single = Offer.query.join(Item.core_offers).filter(
                    Item.id.in_([ixtem.id])).filter_by(offer_type_id=SINGLE
                    ).order_by(Offer.id).all()

                # asumtion theres 2 offer single for 1 items
                for ioffer in offer_single:
                    temp = {}
                    # base idr price get from getscoop, not from platforms offers
                    temp['base_offer'] = ioffer
                    temp['base_item'] = ixtem
                    temp['item_id'] = ixtem.id

                    platform_offers = OfferPlatform.query.filter_by(offer_id=ioffer.id).all()
                    temp['base_platforms'] = platform_offers

                    self.master_gramedia.append(temp)
            return True, httplib.OK

        except Exception as e:
            return False, 'ERROR get_offers %s' % e

    def get_tiers_matrix(self):
        try:

            # get the latest valid from
            latest_data = TierMatrix.query.filter_by(
                paymentgateway_id=IOS, is_alternate=False
            ).order_by(desc(TierMatrix.id)).first()

            # get all tier with a same datetime valid
            tier_matrix = TierMatrix.query.filter_by(
                valid_from = latest_data.valid_from,
                paymentgateway_id=IOS, is_alternate=False
            ).order_by(desc(TierMatrix.id)).all()

            self.master_tiermatrix = [(i.tier_price, i.rate_price['IDR']) for i in tier_matrix]
            self.master_idrmatrix = [(int(i.rate_price['IDR'])) for i in tier_matrix]

            return True, httplib.OK

        except Exception as e:
            return False, 'ERROR get_tiers_matrix %s' % e

    def compare_nearest_price(self):
        try:
            take_closest = lambda num, collection:min(collection,key=lambda x:abs(x-num))

            for igramed in self.master_gramedia:
                items = igramed.get('base_item', None)
                old_offers = igramed.get('base_offer', None)
                old_idr = int(old_offers.price_idr)

                if items and old_offers:

                    # closest range
                    closest_price = take_closest(old_idr, self.master_idrmatrix)

                    # get the usd prices
                    for iusd in self.master_tiermatrix:
                        if closest_price == int(iusd[1]):
                            self.return_construct(self.create_new_offers(old_offers, iusd[0], items, igramed))

            return True, httplib.OK

        except Exception as e:
            return False, 'ERROR compare_nearest_price %s' % e

    def get_tiers_code(self, platform_id=None, price_usd=None):

        if platform_id == IOS:
            tier_data = TierIos.query.filter_by(tier_price=price_usd, tier_type=2).first()

        if platform_id == ANDROID:
            tier_data = TierAndroid.query.filter_by(tier_price=price_usd, tier_type=2).first()

        if platform_id == WINDOWS_PHONE:
            tier_data = TierWp.query.filter_by(tier_price=price_usd, tier_type=2).first()

        return tier_data

    def create_new_offers_platforms(self, new_offers=None, old_offers=None, platform_id=None):
        try:
            # get discounts data
            old_platforms = OfferPlatform.query.filter_by(offer_id=old_offers.id,
                platform_id=platform_id).first()

            # retrieve data tiers by platforms
            tier_code = self.get_tiers_code(platform_id, new_offers.price_usd)

            # IDR ios get from matrix data
            if platform_id == IOS:
                for ios_mat in self.master_tiermatrix:
                    if str(new_offers.price_usd) == str(ios_mat[0]):
                        new_price_idr = ios_mat[1]
            else:
                new_price_idr = new_offers.price_idr

            # create dataplatforms
            new_offer_platforms = OfferPlatform(
                offer_id = new_offers.id,
                offer = new_offers,
                platform_id = old_platforms.platform_id,
                platform = old_platforms.platform,
                tier_id = tier_code.id,
                tier_code = tier_code.tier_code,
                currency = old_platforms.currency,

                price_usd = new_offers.price_usd,
                price_idr = new_price_idr,
                price_point = new_offers.price_point,

                #discount data
                discount_id = old_platforms.discount_id,
                discount_tier_id = old_platforms.discount_tier_id,
                discount_tier_code = old_platforms.discount_tier_code,
                discount_tag = old_platforms.discount_tag,
                discount_name = old_platforms.discount_name,
                discount_tier_price = old_platforms.discount_tier_price,

                discount_price_usd = old_platforms.discount_price_usd,
                discount_price_idr = old_platforms.discount_price_idr,
                discount_price_point = old_platforms.discount_price_point
            )
            db.session.add(new_offer_platforms)
            db.session.commit()
            print 'Old Platform Offers %s IDR : %s USD : %s tier : %s' % (old_platforms.id, old_platforms.price_idr, old_platforms.price_usd, old_platforms.tier_code)
            print 'New Platform Offers %s IDR : %s USD : %s tier : %s' % (new_offer_platforms.id, new_offer_platforms.price_idr, new_offer_platforms.price_usd, new_offer_platforms.tier_code)

            return True, httplib.OK


        except Exception as e:
            return False, 'ERROR create_new_offers_platforms %s' % e

    def create_new_offers(self, old_offers=None, new_usd=None, items=None, igramed=None):
        try:

            temp = {}
            # create new offers:
            new_offers = Offer(
                name = old_offers.name,
                long_name = old_offers.long_name,
                offer_status = old_offers.offer_status,
                sort_priority = old_offers.sort_priority,
                is_active = old_offers.is_active,
                offer_type = old_offers.offer_type,
                offer_type_id = old_offers.offer_type_id,
                exclusive_clients = old_offers.exclusive_clients,
                is_free = old_offers.is_free,
                offer_code = old_offers.offer_code,
                item_code = old_offers.item_code,
                price_usd = new_usd, # -----> newwwwww
                price_idr = old_offers.price_idr,
                price_point = old_offers.price_point,
                vendor_price_usd = new_usd, # -----> newwwwww
                vendor_price_idr = old_offers.vendor_price_idr,
                vendor_price_point = old_offers.vendor_price_point,
                discount_id = old_offers.discount_id,
                discount_tag = old_offers.discount_tag,
                discount_name = old_offers.discount_name,
                discount_price_usd = old_offers.discount_price_usd,
                discount_price_idr = old_offers.discount_price_idr,
                discount_price_point = old_offers.discount_price_point,
                is_discount = old_offers.is_discount,
                image_normal = old_offers.image_normal,
                image_highres = old_offers.image_highres
            )

            # add items
            new_offers.items.append(items)

            ## add brands
            new_offers.brands.append(items.brand)

            old_brands = [i for i in old_offers.brands]
            if not old_brands:
                print 'fixing old offers has no brands..'
                old_offers.brands.append(items.brand)
                db.session.add(old_offers)

            db.session.add(new_offers)
            db.session.commit()

            print '======================= BASE OFFER ======================='
            print 'Item name : %s' % items.name
            print 'Old Offers %s IDR : %s USD : %s' % (old_offers.id, old_offers.price_idr, old_offers.price_usd)
            print 'New Offers %s IDR : %s USD : %s' % (new_offers.id, new_offers.price_idr, new_offers.price_usd)

            # create new offer_platforms
            print '======================= BASE PLATFORMS OFFER IOS ======================='
            self.return_construct(self.create_new_offers_platforms(new_offers, old_offers, IOS))

            print '======================= BASE PLATFORMS OFFER ANDROID ======================='
            self.return_construct(self.create_new_offers_platforms(new_offers, old_offers, ANDROID))

            print '======================= BASE PLATFORMS OFFER WINDOWS PHONE ======================='
            self.return_construct(self.create_new_offers_platforms(new_offers, old_offers, WINDOWS_PHONE))

            # deactivate old offers
            print '======================= DEACTIVATE OLD OFFER [ FINISH ] ======================='
            old_offers.is_active = False
            old_offers.offer_status = NOT_FOR_SALE
            db.session.add(old_offers)
            db.session.commit()

            temp['new_base_offer'] = new_offers
            temp['new_items_name'] = unidecode(unicode(items.name))
            temp['new_items_id'] = items.id
            temp['new_items_vendor'] = items.brand.vendor_id

            platform_offers = OfferPlatform.query.filter_by(offer_id=new_offers.id).all()
            temp['new_base_platforms'] = platform_offers
            igramed.update(temp)

            self.new_master_gramedia.append(igramed)

            return True, httplib.OK

        except Exception as e:
            return False, 'ERROR create_new_offers %s' % e

    def construct_csv_files(self):
        try:
            print '======================= CREATE CSV FILES [ START ] ======================='
            file_name = '%s_%s_%s.csv' % (self.master_vendors.name, self.limit, self.offset)
            file_csv = os.path.join('/tmp/matrix', file_name)

            fp = open(file_csv, 'w')

            w = csv.writer(fp)
            w.writerow([
                'ITEM',
                'ITEM ID',
                'VENDOR ID',
                'OLD OFFER ID',
                'NEW OFFER ID',

                'IDR',
                'USD',
                'POINT',

                'IDR',
                'USD',
                'POINT',

                'IDR',
                'USD',
                'POINT',
                'TIER CODE',

                'IDR',
                'USD',
                'POINT',
                'TIER CODE',

                'IDR',
                'USD',
                'POINT',
                'TIER CODE',

                'IDR',
                'USD',
                'POINT',
                'TIER CODE'
            ])

            for igaram in self.new_master_gramedia:
                tmp = {}

                old_offer = igaram.get('base_offer', None)
                new_offer = igaram.get('new_base_offer', None)

                old_plat = igaram.get('base_platforms', None)
                new_plat = igaram.get('new_base_platforms', None)

                tmp['item_name'] = igaram.get('new_items_name', None)
                tmp['item_id'] = igaram.get('new_items_id', None)
                tmp['vendor_id'] = igaram.get('new_items_vendor', None)

                tmp['old_get_idr'] = '%.2f' % old_offer.price_idr
                tmp['old_get_usd'] = '%.2f' % old_offer.price_usd
                tmp['old_get_pts'] = old_offer.price_point
                tmp['old_get_id'] = old_offer.id

                tmp['new_get_idr'] = '%.2f' % new_offer.price_idr
                tmp['new_get_usd'] = '%.2f' % new_offer.price_usd
                tmp['new_get_pts'] = new_offer.price_point
                tmp['new_get_id'] = new_offer.id

                for ix in old_plat:
                    if ix.platform_id == IOS:
                        tmp['old_ios_usd'] = '%.2f' % ix.price_usd
                        tmp['old_ios_idr'] = '%.2f' % ix.price_idr
                        tmp['old_ios_pts'] = int(ix.price_point)
                        tmp['old_ios_tier'] = ix.tier_code
                    if ix.platform_id == ANDROID:
                        tmp['old_android_usd'] = '%.2f' % ix.price_usd
                        tmp['old_android_idr'] = '%.2f' % ix.price_idr
                        tmp['old_android_pts'] = int(ix.price_point)
                        tmp['old_android_tier'] = ix.tier_code
                    if ix.platform_id == WINDOWS_PHONE:
                        tmp['old_wp_usd'] = '%.2f' % ix.price_usd
                        tmp['old_wp_idr'] = '%.2f' % ix.price_idr
                        tmp['old_wp_pts'] = int(ix.price_point)
                        tmp['old_wp_tier'] = ix.tier_code

                for iy in new_plat:
                    if iy.platform_id == IOS:
                        tmp['new_ios_usd'] = '%.2f' % iy.price_usd
                        tmp['new_ios_idr'] = '%.2f' % iy.price_idr
                        tmp['new_ios_pts'] = int(iy.price_point)
                        tmp['new_ios_tier'] = iy.tier_code
                    if iy.platform_id == ANDROID:
                        tmp['new_android_usd'] = '%.2f' % iy.price_usd
                        tmp['new_android_idr'] = '%.2f' % iy.price_idr
                        tmp['new_android_pts'] = int(iy.price_point)
                        tmp['new_android_tier'] = iy.tier_code
                    if iy.platform_id == WINDOWS_PHONE:
                        tmp['new_wp_usd'] = '%.2f' % iy.price_usd
                        tmp['new_wp_idr'] = '%.2f' % iy.price_idr
                        tmp['new_wp_pts'] = int(iy.price_point)
                        tmp['new_wp_tier'] = iy.tier_code

                self.csv_master.append(tmp)
            print '======================= CREATE CSV FILES [ CONSTRUCT DONE ] ======================='

            for data_csv in self.csv_master:
                w.writerow([
                    data_csv.get('item_name', None),
                    data_csv.get('item_id', None),
                    data_csv.get('vendor_id', None),
                    data_csv.get('old_get_id', None),
                    data_csv.get('new_get_id', None),

                    data_csv.get('old_get_idr', None),
                    data_csv.get('old_get_usd', None),
                    data_csv.get('old_get_pts', None),
                    data_csv.get('new_get_idr', None),
                    data_csv.get('new_get_usd', None),
                    data_csv.get('new_get_pts', None),

                    data_csv.get('old_ios_idr', None),
                    data_csv.get('old_ios_usd', None),
                    data_csv.get('old_ios_pts', None),
                    data_csv.get('old_ios_tier', None),
                    data_csv.get('new_ios_idr', None),
                    data_csv.get('new_ios_usd', None),
                    data_csv.get('new_ios_pts', None),
                    data_csv.get('new_ios_tier', None),

                    data_csv.get('old_android_idr', None),
                    data_csv.get('old_android_usd', None),
                    data_csv.get('old_android_pts', None),
                    data_csv.get('old_android_tier', None),
                    data_csv.get('new_android_idr', None),
                    data_csv.get('new_android_usd', None),
                    data_csv.get('new_android_pts', None),
                    data_csv.get('new_android_tier', None)

                    #data_csv.get('old_wp_idr', None),
                    #data_csv.get('old_wp_usd', None),
                    #data_csv.get('old_wp_pts', None),
                    #data_csv.get('old_wp_tier', None),
                    #data_csv.get('new_wp_idr', None),
                    #data_csv.get('new_wp_usd', None),
                    #data_csv.get('new_wp_pts', None),
                    #data_csv.get('new_wp_tier', None)
                ])


            fp.close()
            print '======================= CREATE CSV FILES [ FINISH ] ======================='
            return True, httplib.OK

        except Exception as e:
            print e
            return False, 'ERROR construct_csv_files %s' % e

    def return_construct(self, construct_data=None):
        status_construct, data_construct = construct_data
        if not status_construct:
            return data_construct

    def construct(self):
        self.return_construct(self.get_vendors_brands())
        self.return_construct(self.get_items())
        self.return_construct(self.get_offers())
        self.return_construct(self.get_tiers_matrix())
        self.return_construct(self.compare_nearest_price())
        self.return_construct(self.construct_csv_files())
