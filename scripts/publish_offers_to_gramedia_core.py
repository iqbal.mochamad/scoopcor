import os
import sys
from argparse import ArgumentParser
from logging.config import dictConfig

from flask import logging

from app.services.gramedia.gramedia_export import init_and_publish_offer_to_gramedia

BASE_DIR = os.path.dirname(os.path.abspath(__file__)).replace('/scripts', '')
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

_log = logging.getLogger('scoopcor.script.publish_offer_to_gramedia')


def main(max_records, offset, item_type):
    init_and_publish_offer_to_gramedia(log=_log,
                                       max_records=max_records,
                                       offset=offset, item_type=item_type)


def initialize_logging():
    dictConfig({
        'version': 1,
        'disable_existing_loggers': True,
        'formatters': {
            'script_format': {
                'format': '%(message)s'
            }
        },
        'handlers': {
            'publish_offer_to_gramedia_log_file': {
                'level': 'INFO',
                'class': 'logging.FileHandler',
                'formatter': 'script_format',
                'filename': os.path.join(BASE_DIR, 'logs', 'publish_offer_to_gramedia.script.log')
            },
        },
        'loggers': {
            'scoopcor.script.publish_offer_to_gramedia': {
                'handlers': ['publish_offer_to_gramedia_log_file', ],
                'propagate': False,
                'level': 'INFO',
            },
        }})


if __name__ == "__main__":
    initialize_logging()

    parser = ArgumentParser()
    parser.add_argument(
        '--max-records',
        dest='max_records',
        type=int,
        required=False,
        default=500,
    )
    parser.add_argument(
        '--offset',
        dest='offset',
        type=int,
        required=False,
        default=0,
    )

    parser.add_argument(
        '--item-type',
        dest='item_type',
        required=False,
        default=None,
    )

    main(**vars(parser.parse_args()))
