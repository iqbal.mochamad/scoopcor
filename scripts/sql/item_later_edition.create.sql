CREATE MATERIALIZED VIEW utility.item_later_editions AS
    SELECT MAX(item.id) AS item_id,
        item.brand_id,
        (SELECT count(*) AS count
            FROM core_items ci2
            WHERE ci2.brand_id = item.brand_id
                  AND ci2.is_active = true
                  AND ci2.item_status = 'ready for consume'::item_status) AS later_issue_count
       FROM core_items item GROUP BY item.brand_id
    WITH DATA;

CREATE UNIQUE INDEX idx_materialized_view_item_later_editions_item_id on utility.item_later_editions ("item_id");
