CREATE OR REPLACE VIEW public.notify_brand AS
select distinct on(brand_id) brand_id,
       item_id,
       brand_name,
       organization_id,
       status,
       item_name,
       release_date,
       release_period
from (select max(core_items.id) as item_id,
             core_items.brand_id as brand_id,
             core_vendors.name as brand_name,
             core_items.name as item_name,
             core_vendors.organization_id as organization_id,
             core_items.release_date as release_date,
             core_brands.release_period as release_period,
             CASE
               WHEN core_brands.release_period = 'weekly' AND core_items.release_date <= now() - interval '1 week'
               THEN 'late'
               WHEN core_brands.release_period = 'biweekly' AND core_items.release_date <= now() - interval '2 week'
               THEN 'late'
               WHEN core_brands.release_period = 'monthly' AND core_items.release_date <= now() - interval '1 month'
               THEN 'late'
               ELSE 'not-late'
             END as "status"
      from core_brands
             join core_items on core_items.brand_id = core_brands.id
             join core_vendors on core_brands.vendor_id = core_vendors.id
      where core_brands.default_item_type in ('magazine', 'newspaper')
        and core_brands.is_active = true
        and core_items.item_status = 'ready for consume'
      group by core_items.brand_id,
               core_items.release_date,
               core_vendors.name,
               core_vendors.organization_id,
               core_brands.release_period,
               core_items.name) as "data"
where data.status = 'late'
order by brand_id desc, item_id desc;
