CREATE OR REPLACE VIEW utility.non_free_item as
        SELECT
            item_id
            ,brand_id
            ,(
            '{' ||
            STRING_AGG(
            CASE WHEN is_free > 0 THEN platform_id::TEXT END, ', ' ORDER BY platform_id)
            || '}'
            )::INT[] AS platform_id
        FROM (
            SELECT
                core_items.id AS item_id
                ,core_items.brand_id
                ,'4' AS platform_id
                ,SUM(CASE WHEN core_offers.is_free = FALSE THEN 1 ELSE 0 END) AS is_free
            FROM
                core_offers
                LEFT JOIN core_offers_items ON core_offers_items.offer_id = core_offers.id
                LEFT JOIN core_offers_brands ON core_offers_brands.offer_id = core_offers.id
                JOIN core_items ON core_items.brand_id = core_offers_brands.brand_id OR core_items.id = core_offers_items.item_id
            WHERE
                core_offers.offer_type_id IN (1, 2)
            GROUP BY
                core_items.id
                ,core_items.brand_id

            UNION ALL
            SELECT
                core_items.id AS item_id
                ,core_items.brand_id
                ,core_platforms_offers.platform_id
                ,SUM(CASE WHEN core_platforms_offers.price_usd > 0 OR core_platforms_offers.price_idr > 0 OR core_platforms_offers.price_point > 0 THEN 1 ELSE 0 END) AS is_free
            FROM
                core_platforms_offers
                JOIN core_offers ON core_offers.id = core_platforms_offers.offer_id
                LEFT JOIN core_offers_items ON core_offers_items.offer_id = core_offers.id
                LEFT JOIN core_offers_brands ON core_offers_brands.offer_id = core_offers.id
                JOIN core_items ON core_items.brand_id = core_offers_brands.brand_id OR core_items.id = core_offers_items.item_id
            WHERE
                core_offers.offer_type_id IN (1, 2)
                AND core_platforms_offers.platform_id <> 3
            GROUP BY
                core_items.id
                ,core_items.brand_id
                ,core_platforms_offers.platform_id
        ) data_1
        GROUP BY
            item_id
            ,brand_id;