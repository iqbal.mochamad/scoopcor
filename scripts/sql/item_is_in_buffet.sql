CREATE OR REPLACE FUNCTION utility.item_is_in_buffet(INT)
  RETURNS BOOLEAN
AS
$BODY$
  DECLARE
  desired_item_id INT = $1;
  count_by_brands INT;
  count_by_items INT;
BEGIN

    count_by_brands = (
        SELECT COUNT(*) FROM core_offers_brands AS ob
           JOIN core_items AS item ON ob.brand_id = item.brand_id
           JOIN core_offers AS o ON ob.offer_id = o.id
        WHERE item.id = desired_item_id
           AND o.offer_type_id = 4
           AND o.is_active = True);

    count_by_items = (
        SELECT COUNT(*) FROM core_offers_items oi
            JOIN core_offers o ON o.id = oi.offer_id
        WHERE oi.item_id = desired_item_id
            AND o.offer_type_id = 4
            AND o.is_active = True);


    RETURN CASE
      WHEN count_by_brands > 0 THEN
        TRUE
      WHEN count_by_items > 0 THEN
        TRUE
      ELSE
        FALSE
    END;
END;
$BODY$
LANGUAGE plpgsql VOLATILE;