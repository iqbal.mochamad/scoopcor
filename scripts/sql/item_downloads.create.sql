CREATE MATERIALIZED VIEW utility.item_downloads AS

SELECT
    item.brand_id, count(*) AS download_count
FROM
    (SELECT distinct user_activity->>'item_id' as item_id, user_activity->>'user_id' as user_id
FROM analytics.user_download
WHERE user_activity->>'activity_type' = 'download' AND user_activity->>'download_status'= 'success'
AND to_timestamp(user_activity->>'datetime', 'YYYY-MM-DD HH24:MI:SS') >= current_date - INTERVAL '2 month') as download_data
  JOIN core_items item ON item.id = CAST(download_data.item_id as INTEGER)
WHERE item_status = 'ready for consume' and is_active = TRUE
GROUP BY
    item.brand_id
    with data;

CREATE UNIQUE INDEX idx_materialized_view_item_downloads_brand_id on utility.item_downloads ("brand_id");