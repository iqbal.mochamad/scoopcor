CREATE MATERIALIZED VIEW utility.user_page_views AS

SELECT
    CAST(user_activity->>'user_id' AS INT) AS user_id,
    CAST(user_activity->>'client_id' AS INT) as client_id,
    SUM(CAST(user_activity->>'duration' AS FLOAT)) AS reading_seconds
FROM
    analytics.user_page_view a
GROUP BY
    CAST(user_activity->>'user_id' AS INT),
    CAST(user_activity->>'client_id' AS INT);


CREATE UNIQUE INDEX idx_materialized_view_user_page_views_client_user_id on utility.user_page_views ("client_id", "user_id");
