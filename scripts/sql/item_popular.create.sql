CREATE MATERIALIZED VIEW utility.item_popular AS
 SELECT item.id,
    item.is_active,
    item.item_status,
    download.download_count,
    item.item_type,
    brand.vendor_id,
    item.languages::text[] AS languages,
    item.countries::text[] AS countries,
    non_free_item.platform_id AS non_free_platforms,
    utility.item_is_in_buffet(item.id) AS is_buffet,
    ARRAY( SELECT category.category_id
           FROM core_items_categories category
          WHERE category.item_id = item.id) AS category_id,
        CASE
            WHEN later_edition.later_issue_count > 0 THEN true
            ELSE false
        END AS is_latest
   FROM utility.item_later_editions later_edition
     JOIN utility.item_downloads download ON download.brand_id = later_edition.brand_id
     JOIN core_items item ON item.id = later_edition.item_id
     JOIN core_brands brand ON brand.id = item.brand_id
     JOIN utility.non_free_item non_free_item ON non_free_item.item_id = item.id
  ORDER BY download.download_count DESC
WITH DATA;

CREATE UNIQUE INDEX idx_materialized_view_item_popular_brand_id on utility.item_popular ("id");
CREATE INDEX idx_materialized_view_item_popular_category_id on utility.item_popular ("category_id");
