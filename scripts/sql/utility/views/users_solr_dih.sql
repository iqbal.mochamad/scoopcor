CREATE OR REPLACE VIEW utility.users_solr_dih AS
SELECT cas_users.id,
    cas_users.username,
    cas_users.first_name,
    cas_users.last_name,
    cas_users.email,
    cas_users.note,
    ARRAY( SELECT cas_users_organizations.organization_id
           FROM cas_users_organizations
          WHERE cas_users_organizations.user_id = cas_users.id) AS organization_ids,
    cas_users.is_active,
    cas_users.created AS signup_date,
    cas_users.modified
   FROM cas_users;
