CREATE OR REPLACE VIEW utility.shared_library_solr_dih_all as
SELECT (COALESCE(org_item.organization_id, 0) || '_'::text) || item.id AS id,
    item.id AS item_id,
    item.item_type,
    org_item.organization_id AS org_id,
    org_item.quantity_available,
    org_item.modified,
    utility.item_name_by_language(item.*, 'eng'::text) AS item_name_en,
    utility.item_name_by_language(item.*, 'id'::text) AS item_name_id,
    brand.name AS brand_name,
    vendor.name AS vendor_name,
    utility.catalog_ids_for_item(item.id, org_item.organization_id) AS catalog_ids,
    utility.author_names_for_item(item.id) AS author_names,
    utility.category_names_eperpus(item.id, org_item.organization_id) AS category_names,
    item.languages AS item_languages,
    item.release_date AS item_release_date,
    'https://images.apps-foundry.com/magazine_static/'::text || item.thumb_image_highres::text AS item_thumb_highres,
    'https://images.apps-foundry.com/magazine_static/'::text || item.thumb_image_normal::text AS item_thumb_normal,
    item.is_internal_content AS is_internal_content
   FROM core_items item
     LEFT JOIN core_brands brand ON item.brand_id = brand.id
     LEFT JOIN core_vendors vendor ON brand.vendor_id = vendor.id
     JOIN core_organization_items org_item ON org_item.item_id = item.id;


-- CREATE OR REPLACE VIEW utility.shared_library_solr_dih_all as
-- SELECT (COALESCE(org_item.organization_id, 0) || '_'::text) || item.id AS id,
--     item.id AS item_id,
--     item.item_type,
--     org_item.organization_id AS org_id,
--     org_item.quantity_available,
--     org_item.modified,
--     utility.item_name_by_language(item.*, 'eng'::text) AS item_name_en,
--     utility.item_name_by_language(item.*, 'id'::text) AS item_name_id,
--     brand.name AS brand_name,
--     vendor.name AS vendor_name,
--     utility.catalog_ids_for_item(item.id, org_item.organization_id) AS catalog_ids,
--     utility.author_names_for_item(item.id) AS author_names,
--     utility.category_names_for_item(item.id) AS category_names,
--     item.languages AS item_languages,
--     item.release_date AS item_release_date,
--     'https://images.apps-foundry.com/magazine_static/'::text || item.thumb_image_highres::text AS item_thumb_highres,
--     'https://images.apps-foundry.com/magazine_static/'::text || item.thumb_image_normal::text AS item_thumb_normal
--    FROM core_items item
--      LEFT JOIN core_brands brand ON item.brand_id = brand.id
--      LEFT JOIN core_vendors vendor ON brand.vendor_id = vendor.id
--      JOIN core_organization_items org_item ON org_item.item_id = item.id;
