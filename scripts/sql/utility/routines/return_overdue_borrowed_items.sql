CREATE OR REPLACE FUNCTION utility.return_overdue_borrowed_items()
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
  DECLARE
    change_count INTEGER;
  BEGIN
    WITH temp_return_shared_lib AS (
        with tempo_tempo as (
            SELECT ubi.catalog_item_org_id as organization_id,
                ubi.catalog_item_item_id as item_id,
                ubi.user_id as user_id,
                itm.item_type,
                ubi.borrowing_start_time,
                org.magazine_borrowing_time_limit,
                org.book_borrowing_time_limit,
                org.newspaper_borrowing_time_limit
            FROM core_user_borrowed_items ubi
                LEFT JOIN cas_shared_libraries AS org ON ubi.catalog_item_org_id = org.id
                left join core_items as itm on ubi.catalog_item_item_id = itm.id
            WHERE
                org.borrowing_time_limit IS NOT NULL AND
                ubi.returned_time IS NULL
        )
        select
            organization_id,
            item_id,
            count(DISTINCT user_id) as qty_returned
        from
            tempo_tempo
        where
            (case when item_type = 'book' then (borrowing_start_time + book_borrowing_time_limit)
                  when item_type = 'magazine' then (borrowing_start_time + magazine_borrowing_time_limit)
                  when item_type = 'newspaper' then (borrowing_start_time + newspaper_borrowing_time_limit) end ) < now()
        group by organization_id, item_id
    )

    UPDATE core_organization_items SET
        quantity_available =
         CASE WHEN quantity_available + qty_returned > quantity THEN quantity
             ELSE quantity_available + qty_returned END
        , modified = now()
    FROM temp_return_shared_lib
    WHERE core_organization_items.organization_id = temp_return_shared_lib.organization_id
      AND core_organization_items.item_id = temp_return_shared_lib.item_id;

    UPDATE core_user_borrowed_items SET returned_time = now(), modified = now()
    WHERE id IN (
      SELECT user_borrowed_item.id
        FROM core_user_borrowed_items AS user_borrowed_item
        LEFT JOIN cas_shared_libraries AS organization ON user_borrowed_item.catalog_item_org_id = organization.id
        left join core_items as item on item.id = user_borrowed_item.catalog_item_item_id
        WHERE
          organization.borrowing_time_limit IS NOT NULL AND
          user_borrowed_item.returned_time IS NULL and
          (case when item.item_type = 'book' then (user_borrowed_item.borrowing_start_time + organization.book_borrowing_time_limit)
                when item.item_type = 'magazine' then (user_borrowed_item.borrowing_start_time + organization.magazine_borrowing_time_limit)
                when item.item_type = 'newspaper' then (user_borrowed_item.borrowing_start_time + organization.newspaper_borrowing_time_limit) end ) < now()
    );
    GET DIAGNOSTICS change_count = ROW_COUNT;
    RETURN change_count;
  END;
$function$
