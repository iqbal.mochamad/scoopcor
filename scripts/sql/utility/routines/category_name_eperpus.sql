CREATE OR REPLACE FUNCTION utility.category_names_eperpus(_item_id integer, _organization_id integer)
 RETURNS text[]
 LANGUAGE plpgsql
AS $function$
BEGIN
  RETURN ARRAY(
    SELECT
        name
    FROM
        core_eperpus_categories_organizations
    WHERE
        category_id in (
            SELECT
                category.id
            FROM
                core_categories category
            JOIN
                core_eperpus_items_categories item_category ON category.id = item_category.category_id
            WHERE
                item_category.item_id = _item_id AND
                item_category.organization_id = _organization_id
        )
        and organization_id=_organization_id
    );
END;
$function$
