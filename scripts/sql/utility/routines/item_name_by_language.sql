CREATE OR REPLACE FUNCTION utility.item_name_by_language(item public.core_items, lang_id text)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
  BEGIN
    if lang_id = 'eng' THEN
      if '{eng}'::character varying[] @> item.languages THEN
        return item.name;
      ELSE
        return NULL::character varying;
      END IF;
    ELSE
      if '{eng}'::character varying[] @> item.languages THEN
        return NULL::character varying;
      ELSE
        return item.name;
      END IF;
    END IF;
  END;
$function$
