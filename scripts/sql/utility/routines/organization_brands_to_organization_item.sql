CREATE OR REPLACE FUNCTION utility.organization_brands_to_organization_item()
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
  DECLARE
    change_count INTEGER;
  BEGIN
    CREATE TEMPORARY TABLE temp_shared_lib_new_item (
        organization_id int not null,
        item_id int not null,
        brand_id int not null,
        quantity int not null) on commit drop;

    -- get data of new items from org-brands that now yet exists in org-item
    --  with item id per brands is bigger than latest_added_item_id
    INSERT INTO temp_shared_lib_new_item (organization_id, item_id, brand_id, quantity)
    SELECT ob.organization_id as organization_id,
            i.id as item_id, ob.brand_id,
            coalesce(ob.quantity, 0) as quantity
    FROM core_organization_brands ob
        LEFT JOIN core_items i ON ob.brand_id = i.brand_id
            and i.id > coalesce(ob.latest_added_item_id, 0)
        LEFT JOIN core_organization_items oi on oi.organization_id = ob.organization_id
            and oi.item_id = i.id
    WHERE
        (ob.valid_from is null or ob.valid_from < now())
        and (ob.valid_to is null or ob.valid_to > now())
        and i.id is not null
        and i.is_active = true and i.item_status = 'ready for consume'
        and oi.item_id is null;

    -- insert new org item data
    INSERT INTO core_organization_items
    (organization_id, item_id, item_name_override, quantity, quantity_available, created, modified)
    SELECT organization_id, item_id, null as item_name_override,
        quantity, quantity as quantity_available, now() as created, now() as modified
    FROM temp_shared_lib_new_item;

    -- insert new eperpus categories with name 'kira'
    insert into core_eperpus_categories_organizations(organization_id, category_id, name, is_active)
    select
        temp_shared_lib_new_item.organization_id,
        core_items_categories.category_id,
        'kira',
        True
    from
        temp_shared_lib_new_item
        join core_items_categories on temp_shared_lib_new_item.item_id = core_items_categories.item_id
    where
        not exists(
            select category_id from core_eperpus_categories_organizations
            where category_id = core_items_categories.category_id and organization_id = temp_shared_lib_new_item.organization_id
        );

    -- update eperpus categories with name 'kira'
    with t as (
        select core_eperpus_categories_organizations.category_id, core_categories.name
        from core_eperpus_categories_organizations
            inner join core_categories on core_eperpus_categories_organizations.category_id = core_categories.id
        where core_eperpus_categories_organizations.name = 'kira')
    update core_eperpus_categories_organizations
    set name = t.name
    from t where core_eperpus_categories_organizations.category_id = t.category_id;

    -- insert new eperpus categories items'
    insert into core_eperpus_items_categories(category_id, item_id, organization_id)
    select
        core_items_categories.category_id,
        temp_shared_lib_new_item.item_id,
        temp_shared_lib_new_item.organization_id
    from
        temp_shared_lib_new_item
        join core_items_categories on temp_shared_lib_new_item.item_id = core_items_categories.item_id
    where
        not exists(
            select item_id from core_eperpus_items_categories
            where item_id = temp_shared_lib_new_item.item_id and
            organization_id = temp_shared_lib_new_item.organization_id and
            category_id = core_items_categories.category_id
        );

    GET DIAGNOSTICS change_count = ROW_COUNT;

    -- insert new catalog-items data
    INSERT INTO core_catalog_items (catalog_id, shared_library_item_org_id, shared_library_item_item_id)
    SELECT DISTINCT cb.catalog_id, s.organization_id, s.item_id
    FROM temp_shared_lib_new_item s
        LEFT JOIN core_catalog_brands cb on s.organization_id = cb.shared_library_brand_org_id
            AND s.brand_id = cb.shared_library_brand_brand_id
        LEFT JOIN core_catalog_items t on cb.catalog_id = t.catalog_id
            AND t.shared_library_item_org_id = s.organization_id
            AND t.shared_library_item_item_id = s.item_id
    WHERE cb.catalog_id IS NOT NULL and t.shared_library_item_item_id IS NULL
    ;

    -- update latest added item id to org-brands for the next update
    UPDATE core_organization_brands SET latest_added_item_id = s.max_item_id
        , modified = now()
    FROM ( SELECT organization_id, brand_id, MAX(item_id) as max_item_id
           FROM temp_shared_lib_new_item
           GROUP BY organization_id, brand_id
          ) s
    WHERE s.organization_id = core_organization_brands.organization_id
        AND s.brand_id = core_organization_brands.brand_id
        AND core_organization_brands.latest_added_item_id != s.max_item_id;

    RETURN change_count;
  END;
$function$
