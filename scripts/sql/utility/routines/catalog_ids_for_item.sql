CREATE OR REPLACE FUNCTION utility.catalog_ids_for_item(_item_id integer)
 RETURNS integer[]
 LANGUAGE plpgsql
AS $function$
BEGIN
  RETURN
    ARRAY(
        SELECT item_catalog.catalog_id
        FROM core_catalog_items item_catalog
        WHERE item_catalog.shared_library_item_item_id = _item_id);
END;
$function$

CREATE OR REPLACE FUNCTION utility.catalog_ids_for_item(_item_id integer, _org_id integer)
 RETURNS integer[]
 LANGUAGE plpgsql
AS $function$
  BEGIN
  RETURN
    ARRAY(
        SELECT item_catalog.catalog_id
        FROM core_catalog_items item_catalog
        WHERE item_catalog.shared_library_item_item_id = _item_id
              AND item_catalog.shared_library_item_org_id = _org_id
              AND item_catalog.is_active = true
    );
END;
$function$
