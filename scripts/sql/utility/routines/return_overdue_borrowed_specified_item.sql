CREATE OR REPLACE FUNCTION utility.return_overdue_borrowed_specified_items()
 RETURNS integer
 LANGUAGE plpgsql
AS $function$
  DECLARE
    change_count INTEGER;
  BEGIN
    WITH temp_return_shared_lib AS (
        SELECT ubi.catalog_item_org_id as organization_id,
            ubi.catalog_item_item_id as item_id,
            count(DISTINCT ubi.user_id) as qty_returned
        FROM core_user_borrowed_items ubi
            LEFT JOIN cas_shared_libraries_borrowing_items AS org ON
            ubi.catalog_item_org_id = org.organization_id and ubi.catalog_item_catalog_id = org.catalog_id and
            ubi.catalog_item_item_id = org.item_id
        WHERE
            org.borrowing_time_limit IS NOT NULL AND
            ubi.returned_time IS NULL and
            (ubi.borrowing_start_time + org.borrowing_time_limit) < now()
        GROUP BY ubi.catalog_item_org_id, ubi.catalog_item_item_id
    )
    UPDATE core_organization_items SET
        quantity_available =
         CASE WHEN quantity_available + qty_returned > quantity THEN quantity
             ELSE quantity_available + qty_returned END
        , modified = now()
    FROM temp_return_shared_lib
    WHERE core_organization_items.organization_id = temp_return_shared_lib.organization_id
      AND core_organization_items.item_id = temp_return_shared_lib.item_id;

    UPDATE core_user_borrowed_items SET returned_time = now(), modified = now()
    WHERE id IN (
      SELECT ubi.id
        FROM core_user_borrowed_items as ubi
            LEFT JOIN cas_shared_libraries_borrowing_items AS org ON
            ubi.catalog_item_org_id = org.organization_id and ubi.catalog_item_catalog_id = org.catalog_id and
            ubi.catalog_item_item_id = org.item_id
        WHERE
          org.borrowing_time_limit IS NOT NULL AND
          ubi.returned_time IS NULL AND
          (ubi.borrowing_start_time + org.borrowing_time_limit) < now()
    );
    GET DIAGNOSTICS change_count = ROW_COUNT;
    RETURN change_count;
  END;
$function$
