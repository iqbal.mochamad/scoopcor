CREATE OR REPLACE FUNCTION utility.student_save_eperpus(_item_id integer)
 RETURNS text[]
 LANGUAGE plpgsql
AS $function$
BEGIN
  RETURN ARRAY(
    SELECT
        studentsave_id
    FROM
        core_items_studentsaves
    WHERE
        item_id = _item_id);
END;
$function$
