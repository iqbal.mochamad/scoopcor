CREATE OR REPLACE FUNCTION utility.author_names_for_item(_item_id integer)
 RETURNS text[]
 LANGUAGE plpgsql
AS $function$
BEGIN
  RETURN ARRAY(
      SELECT
          core_authors.name
      FROM core_authors
          JOIN core_items_authors ON core_authors.id = core_items_authors.author_id
          WHERE core_items_authors.item_id = _item_id);
END;
$function$