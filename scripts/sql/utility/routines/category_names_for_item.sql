CREATE OR REPLACE FUNCTION utility.category_names_for_item(_item_id integer)
 RETURNS text[]
 LANGUAGE plpgsql
AS $function$
BEGIN
  RETURN ARRAY(
      SELECT
          category.name
     FROM core_categories category
     JOIN core_items_categories item_category ON category.id = item_category.category_id
     WHERE item_category.item_id = _item_id);
END;
$function$