import os
import sys

from app import app
from app.uploads.api import upload_webreader

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))


def main():
    ctx = app.test_request_context('/')
    ctx.push()

    upload_webreader()

    ctx.pop()


if __name__ == '__main__':
    main()
