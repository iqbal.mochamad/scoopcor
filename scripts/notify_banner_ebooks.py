import os, sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.notification.ebooks.helpers import send_banner_ebook_notification

def main():
    send_banner_ebook_notification()

if __name__ == '__main__':
    main()
