import os
import sys

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir))

from app.eperpus.scripts import ProcessingFiles


def main():
    ProcessingFiles().process()


if __name__ == '__main__':
    main()
