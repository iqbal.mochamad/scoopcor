#!/usr/bin/env python
"""
Web Reader File Processor
=========================

Constructs JPEGs from our Item PDF's suitable for viewing via our WebReader.
"""
from __future__ import unicode_literals

import argparse
import logging
import multiprocessing
import smtplib
from datetime import datetime
from time import sleep

import os
from sqlalchemy.orm import sessionmaker, scoped_session

from app import app, db
from app.items.choices import ProcessingMode, WebReaderTaskStatus, ItemTypes
from app.items.files.locations import has_original_locally, \
    has_mobile_bundle_locally, pdf_original_path
from app.items.files.mobile_bundles import MobileBundleExtractor, \
    mobile_bundle_factory
from app.items.files.pdfs import web_reader_batch_extractor_factory, \
    thumbnail_batch_extractor_factory
from app.items.files.s3 import s3_transfer_factory, S3Downloader, s3_downloader_factory
from app.items.helpers import get_all_latest_editions
from app.items.models import Item
from app.items.web_reader import ProcessingTaskManager, \
    get_web_reader_content_pages_directory
from app.messaging.email import get_smtp_server, HtmlEmail
from app.uploads.pdf.utils import PdfMetadata
from app.utils.loops import LoopRetarder


_log = logging.getLogger("scoopcor.script.web_reader_file_processor")


def main(maximum_parallel_tasks, processing_mode, max_limit_latest_edition, task_manager):
    """ Main worker loop for processing web reader files.

    :param `int` maximum_parallel_tasks: The number of tasks that can be
        run in parallel.
    :param processing_mode: high or normal (ProcessingMode)
    :param 'int' max_limit_latest_edition: limit/maximum top n record for selected latest edition at each loop
    :param task_manager: ProcessingTaskManager(app.kvs)
    :param session: database session
    :return: None
    """
    _log.info("About to process tasks: {} max / {} mode".format(
        maximum_parallel_tasks, processing_mode.name))

    active_task_threads = {}

    global tasks_exhausted

    # this is main loop, will continue forever, except for test
    continue_main_loop = True
    while continue_main_loop:

        _log.info("Sub loop started.")

        # force a new session and connection
        db.engine.dispose()
        session_factory = sessionmaker(bind=db.engine)
        main_scoped_session = scoped_session(session_factory)
        session = main_scoped_session()

        # prepare for sub loop: get latest edition of books and magazine, limit/top n = max_limit_latest_edition
        if not app.config.get('TESTING') and processing_mode == ProcessingMode.normal:
            get_or_create_task_for_all_latest_editions(task_manager, session, max_limit_latest_edition)

        # prepare for sub loop: get pending tasks (prepare in a generator)
        tasks_iterator = iterate_all_pending_tasks(task_manager, processing_mode)

        tasks_exhausted = False
        # sub loop for current list of latest edition and pending task
        while not tasks_exhausted:
            try:
                # get a task from tasks_iterator
                task = tasks_iterator.next()
                _log.info("Waiting available thread to run task for item_id: {}".format(task.item_id))
            except StopIteration:
                _log.info("Tasks exhausted.")
                tasks_exhausted = True
                break

            task_processed = False
            while not task_processed:

                # get latest task status and other info from task manager/redis
                latest_task_info = task_manager.get_by_item_id(task.item_id)
                if latest_task_info and not (latest_task_info.status == WebReaderTaskStatus.pending and
                        latest_task_info.is_high_priority == task.is_high_priority):
                    # task for the same item id already processed by another task processor with different priority.
                    #  for example, this is a normal processor, item 123 is pending with normal priority in queue.
                    #    at the same time, another user submitted 123 with high priority and finished
                    #    item 123 shouldn't be execute in this queue anymore
                    _log.info("Task for item_id: {} completed in another processor".format(task.item_id))
                    task_processed = True
                else:
                    if len(active_task_threads) < maximum_parallel_tasks:
                        # create new thread for current task
                        active_task_threads[task] = create_thread_for_task(task)
                        task_processed = True
                    else:
                        with LoopRetarder(minimum_iteration_seconds=15):
                            for t in active_task_threads.copy():
                                p = active_task_threads[t]
                                if not p.is_alive():
                                    # removed finished task from thread
                                    del active_task_threads[t]
                                    # create thread for current task
                                    active_task_threads[task] = create_thread_for_task(task)
                                    task_processed = True
                                    break

        session.expunge_all()
        session.close_all()
        main_scoped_session.remove()

        _log.info("Sub loop completed")

        if app.config.get('TESTING'):
            # stop main loop in Testing mode
            continue_main_loop = False
        else:
            sleep(15)


def create_thread_for_task(task):
    p = multiprocessing.Process(target=process_web_reader_files, args=(task, app.static_folder, ))
    p.start()
    _log.info("Creating thread to process item_id: {}".format(task.item_id))
    return p


def update_item_page_count(item, local_static_folder, thread_session):
    """ Does a little bit more than's on the tin..

    :param thread_session:
    :param local_static_folder:
    :param task:
    :return:
    """
    try:
        pdf_path = pdf_original_path(item, local_static_folder)

        pdf_meta = PdfMetadata(pdf_path)
        item.page_count = pdf_meta.page_count
        thread_session.commit()

        return item
    except Exception as ex:
        raise PdfPageCountFailedException('Failed to count pdf pages. Error: {}'.format(ex.message))


def flag_processing_started(task, mgr):
    task.processing_start_time = datetime.now()
    mgr.update(task)


def extract_web_reader_images(task, mgr, item, local_static_folder, high_res=False):
    try:
        task.status = WebReaderTaskStatus.creating_web_reader_images
        mgr.update(task)
        web_reader_batch_extractor_factory(item, local_static_folder, high_res).process()
    except Exception as ex:
        raise PdfToImageFailedException('Failed to extract image from pdf. Error: {}'.format(ex.message))


def extract_thumbnails(task, mgr, item, local_static_folder):
    try:
        task.status = WebReaderTaskStatus.creating_thumbnail_images
        mgr.update(task)
        thumbnail_batch_extractor_factory(item, local_static_folder).process()
    except Exception as ex:
        raise PdfToImageFailedException('Failed to extract thumbnail from pdf. Error: {}'.format(ex.message))


def flag_task_completed(task, mgr):
    # update the processing queue job
    task.processing_end_time = datetime.now()
    task.status = WebReaderTaskStatus.complete
    mgr.update(task)


def get_processing_thread_locals():
    from app import app as app2, db as db2
    return ProcessingTaskManager(app2.kvs), app.static_folder, db2.session()


def process_web_reader_files(task, static_folder):

    _log.info("Thread started for item {}".format(task.item_id))

    from app import app as app2
    mgr = ProcessingTaskManager(app2.kvs)

    local_static_folder = static_folder

    db.engine.dispose()
    session_factory = sessionmaker(bind=db.engine)
    thread_scoped_session = scoped_session(session_factory)
    thread_session = thread_scoped_session()

    try:
        flag_processing_started(task, mgr)

        item = thread_session.query(Item).get(task.item_id)
        # flag item as processing (page_count=0, if its stayed 0 ==> failed/error).
        # page_count=null ==> not yet processed, page_count>0 ==> processed and succeed
        if not item:
            _log.exception("Failed to query item {} in thread processor.".format(task.item_id))
        else:
            item.page_count = 0
            thread_session.commit()

            if not has_mobile_bundle_locally(item, local_static_folder):
                try:
                    downloader = s3_downloader_factory(S3Downloader, item=item)
                    downloader.download_mobile_bundle()
                except Exception as ex:
                    raise MobileBundleZipFileNotFound(
                        'Zip File not found locally and failed to download from amazon s3 too. '
                        'Time: {check_time}. '
                        'Brand id: {brand_id}, Edition Code: {edition_code}, File Name: {file_name}. '
                        'S3 error: {s3_error}'.format(
                            check_time=datetime.now(), s3_error=ex.message,
                            brand_id=item.brand_id, edition_code=item.edition_code, file_name=item.files[0].file_name)
                    )

            if not has_original_locally(item, local_static_folder):
                extractor = mobile_bundle_factory(MobileBundleExtractor, item=item)
                extractor.extract_original()

            update_item_page_count(item, local_static_folder, thread_session)

            # if newspaper: process pdf to high res PNG (150dpi then compressed again with PNGQuant)
            # else, process with regular jpeg 112dpi quality 60
            high_res = item.item_type == ItemTypes.newspaper.value

            _log.info("Extracting image for item {}".format(task.item_id))

            extract_web_reader_images(task, mgr, item, local_static_folder, high_res)

            extract_thumbnails(task, mgr, item, local_static_folder)

            flag_task_completed(task, mgr)

            _log.info("item_id: {} completed".format(item.id))

            notify_users_processing_complete(task, item)

    except Exception as ex:
        flag_task_as_failed(ex, mgr, task)
        _log.exception("Unexpected error processing web reader files for item {}.".format(task.item_id))

    finally:
        thread_session.expunge_all()
        thread_session.close_all()
        thread_scoped_session.remove()
        db.engine.dispose()


def flag_task_as_failed(ex, mgr, task):
    task.status = WebReaderTaskStatus.failed
    task.processing_end_time = datetime.now()
    task.error_message = ex.message if ex.message else str(ex)
    mgr.update(task)


def notify_users_processing_complete(task, item):
    smtp_server = get_smtp_server()
    for user in task.notify_users:
        try:
            msg = HtmlEmail(
                sender="SCOOP WEB READER <SCOOP@apps-foundry.com>",
                to=[user.username, ],
                subject="Scoop Web Reader - {0.name} Sudah Siap".format(item),
                html_template="email/web_reader/notify-user-processing-complete.html",
                plaintext_template="email/web_reader/notify-user-processing-complete.txt",
                item=item,
            )
            smtp_server.send_message(msg)

        except smtplib.SMTPException:
            _log.exception("Couldn't send e-mail to "
                           "user: {}".format(user.username))


def iterate_all_pending_tasks(mgr, processing_mode):
    for t in mgr.get_all_tasks():
        if t.status == WebReaderTaskStatus.pending:
            if processing_mode == ProcessingMode.high:
                if t.is_high_priority:
                    yield t
            else:
                if not t.is_high_priority:
                    yield t


def get_or_create_task_for_all_latest_editions(mgr, session, max_limit_latest_edition):
    i = 0

    # get latest processed item id from redis
    latest_processed_item_id = mgr.get_last_minimum_item_id()

    last_min_item_id = latest_processed_item_id

    # get_all_latest_editions will return item.id and item.brand_id only, if need anything else, change the query first.
    for item in get_all_latest_editions(session, max_results=max_limit_latest_edition):
        task = mgr.get_by_item_id(item.id)
        if not os.path.exists(get_web_reader_content_pages_directory(item.id, item.brand_id)) \
                or (task and task.status == WebReaderTaskStatus.failed):
            task = mgr.get_or_create_by_item_id(item_id=item.id, brand_id=item.brand_id, ignore_existing=True)
            # if it's failed, retry, set to pending again
            task.status = WebReaderTaskStatus.pending
            mgr.update(task)
            i += 1
            if not last_min_item_id or item.id < last_min_item_id:
                last_min_item_id = item.id

    # get items below latest processed item id
    if latest_processed_item_id and latest_processed_item_id > 0:
        for item in get_all_latest_editions(
                        session,
                        max_results=max_limit_latest_edition,
                        last_min_item_id=latest_processed_item_id):
            task = mgr.get_by_item_id(item.id)
            if not os.path.exists(get_web_reader_content_pages_directory(item.id, item.brand_id)) \
                    or (task and task.status == WebReaderTaskStatus.failed):
                task = mgr.get_or_create_by_item_id(item_id=item.id, brand_id=item.brand_id, ignore_existing=True)
                task.status = WebReaderTaskStatus.pending
                mgr.update(task)
                i += 1
                if not last_min_item_id or item.id < last_min_item_id:
                    last_min_item_id = item.id

    _log.info("Creating {} tasks for latest editions without web reader.".format(i))

    session.expunge_all()
    session.close_all()

    # save/set latest_processed_item_id to redis
    mgr.set_last_minimum_item_id(last_min_item_id)


class MobileBundleZipFileNotFound(Exception):
    pass


class PdfPageCountFailedException(Exception):
    pass


class PdfToImageFailedException(Exception):
    pass


def get_command_line_arguments():
    parser = argparse.ArgumentParser()

    parser.add_argument(
       "--max-parallel-tasks",
       dest="maximum_parallel_tasks",
       type=int,
       required=True,
       help="Number of processing jobs that can be executed in parallel")

    parser.add_argument(
       '--processing-mode',
       dest='processing_mode',
       type=lambda val: ProcessingMode[val],
       required=True,
       help='Controls whether this instance runs "high" or "normal" '
            'priority jobs')

    parser.add_argument(
       "--max-limit-latest-edition",
       dest="max_limit_latest_edition",
       type=int,
       required=True,
       help="Number of maximum top N/limit for selecting latest edition on each main loop.")

    command_line_args = vars(parser.parse_args())

    command_line_args.update({
        'task_manager': ProcessingTaskManager(redis=app.kvs)
    })

    return command_line_args


if __name__ == '__main__':

    try:

        with app.test_request_context('/'):
            args = get_command_line_arguments()
            main(**args)
            args['session'].close_all()
            sleep(60)

    except Exception as e:
        _log.exception("Script failed with an unknown error")
        raise




