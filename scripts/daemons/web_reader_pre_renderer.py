#!/usr/bin/env python
from __future__ import unicode_literals
from argparse import ArgumentParser
import os
import sys

# required so we can load from app, because it will not be on our PATH
from datetime import timedelta, datetime

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, os.path.join(BASE_DIR, os.pardir, os.pardir))

from app import app, db
from app.items.models import Item
from app.items.web_reader import assert_web_reader_files_ready, \
    get_or_create_processing_job
from app.items.exceptions import WebReaderFilesNotReady


def main(redis, book_min_age, magazine_min_age, newspaper_min_age):
    """

    :param `timedelta` book_min_age:
    :param `timedelta` magazine_min_age:
    :param `timedelta` newspaper_min_age:
    :return:
    """
    schedule_pdf_processing_jobs('book', datetime.now() - book_min_age, redis)
    schedule_pdf_processing_jobs('magazine', datetime.now() - magazine_min_age, redis)
    schedule_pdf_processing_jobs('newspaper', datetime.now() - newspaper_min_age, redis)


def schedule_pdf_processing_jobs(item_type, minimum_release_date, redis):
    """

    :param `string` item_type:
    :param `datetime` minimum_release_date:
    :return:
    """
    s = db.session()

    result_set = s.query(Item).filter(
        Item.item_type == item_type,
        Item.release_date >= minimum_release_date,
        Item.is_active == True,
        Item.item_status == 'ready for consume',
        Item.content_type == 'pdf')

    for item in result_set:
        try:
            assert_web_reader_files_ready(item.id, item.brand_id, redis)
        except WebReaderFilesNotReady:
            get_or_create_processing_job(item.id, redis)

        # get the item out of the session so it's memory can be released
        s.expunge(item)


if __name__ == "__main__":

    def timedelta_from_int(value):
        value = int(value)
        return timedelta(days=value)

    parser = ArgumentParser()
    parser.add_argument(
        '--book-min-age',
        dest='book_min_age',
        type=timedelta_from_int,
        required=False,
        default=timedelta(days=3650),  # lama lama yang lalu
    )
    parser.add_argument(
        '--magazine-min-age',
        dest='magazine_min_age',
        type=timedelta_from_int,
        required=True
    )
    parser.add_argument(
        '--newspaper-min-age',
        dest='newspaper_min_age',
        type=timedelta_from_int,
        required=True,
    )
    with app.test_request_context('/'):
        main(redis=app.kvs, **vars(parser.parse_args()))
