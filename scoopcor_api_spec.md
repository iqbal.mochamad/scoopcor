# SCOOP CORE API Specification
---

* [GENERAL](#general)

    * [Base URL and Versions](#base-url-and-versions)

    * [HTTP Header Fields](#http-header-fields)

    * [Authorization](#authorization)

    * [Common Error HTTP Response Codes](#common-error-http-response-codes)

---

* [ITEMS AND METADATA](#items-and-metadata)

    * [Items](#items)
        * [Item Properties](#item-properties)
        * [Create Item](#create-item)
        * [Retrieve Items](#retrieve-items)
        * [Retrieve Latest Items](#retrieve-latest-items)
        * [Retrieve Popular Items](#retrieve-popular-items)
        * [Retrieve Free Items](#retrieve-free-items)
        * [Search Items](#search-items)
        * [Search Suggestions for Items](#search-suggestions-for-items)
        * [Retrieve Individual Item](#retrieve-individual-item)
        * [Retrieve Item Recommendations](#retrieve-item-recommendations)
        * [Update Item](#update-item)
        * [Delete Item](#delete-item)

    * [Item Files](#item-files)
        * [Item File Properties](#item-file-properties)
        * [Create Item Files](#create-item-files)
        * [Retrieve Item Files](#retrieve-item-files)
        * [Retrieve Individual Item File](#retrieve-individual-item-file)
        * [Update Item File](#update-item-file)
        * [Delete Item File](#delete-item-file)

    * [Item Types](#item-types)
        * [Item Type Properties](#item-type-properties)
        * [Create Item Type](#create-item-type)
        * [Retrieve Item Types](#retrieve-item-types)
        * [Retrieve Individual Item Type](#retrieve-individual-item-type)
        * [Update Item Type](#update-item-type)
        * [Delete Item Type](#delete-item-type)

    * [Vendors](#vendors)
        * [Vendor Properties](#vendor-properties)
        * [Create Vendor](#create-vendor)
        * [Retrieve Vendors](#retrieve-vendors)
        * [Retrieve Individual Vendor](#retrieve-individual-vendor)
        * [Update Vendor](#update-vendor)
        * [Delete Vendor](#delete-vendor)
        * [Deactivate Vendor](#deactivate-vendor)

    * [Brands](#brands)
        * [Brand Properties](#brand-properties)
        * [Create Brand](#create-brand)
        * [Retrieve Brands](#retrieve-brands)
        * [Retrieve Individual Brand](#retrieve-individual-brand)
        * [Update Brand](#update-brand)
        * [Delete Brand](#delete-brand)

    * [Authors](#authors)
        * [Author Properties](#author-properties)
        * [Create Author](#create-author)
        * [Retrieve Authors](#retrieve-authors)
        * [Retrieve Individual Author](#retrieve-individual-author)
        * [Update Author](#update-author)
        * [Delete Author](#delete-author)

    * [Countries](#countries)
        * [Country Properties](#country-properties)
        * [Create Country](#create-country)
        * [Retrieve Countries](#retrieve-countries)
        * [Retrieve Individual Country](#retrieve-individual-country)
        * [Update Country](#update-country)
        * [Delete Country](#delete-country)

    * [Languages](#languages)
        * [Language Properties](#language-properties)
        * [Create Language](#create-language)
        * [Retrieve Languages](#retrieve-languages)
        * [Retrieve Individual Language](#retrieve-individual-language)
        * [Update Language](#update-language)
        * [Delete Language](#delete-language)

    * [Categories](#categories)
        * [Category Properties](#category-properties)
        * [Create Category](#create-category)
        * [Retrieve Categories](#retrieve-categories)
        * [Retrieve Individual Category](#retrieve-individual-category)
        * [Update Category](#update-category)
        * [Delete Category](#delete-category)

    * [Parental Controls](#parental-controls)
        * [Parental Control Properties](#parental-control-properties)
        * [Create Parental Control](#create-parental-control)
        * [Retrieve Parental Controls](#retrieve-parental-controls)
        * [Retrieve Individual Parental Control](#retrieve-individual-parental-control)
        * [Update Parental Controls](#update-parental-control)
        * [Delete Parental Controls](#delete-parental-control)

    * [Bonuses](#bonuses)
        * [Bonuses Properties](#gifts-properties)
        * [Create Bonuses](#create-bonuses)
        * [Retrieve Bonuses](#retrieve-bonuses)
        * [Retrieve Individual Gift](#retrieve-individual-gift)
        * [Update Bonuses](#update-bonuses)

    * [Claim Bonuses](#claim-bonuses)
        * [Claim Bonuses Properties](#claim-bonuses-properties)
        * [Update Claim Bonuses](#update-claim-bonuses)

---

* [OFFERS, PLATFORMS, TIERS, CAMPAIGNS, DISCOUNTS, and BANNERS](#offers-platforms-tiers-campaigns-discounts-and-banners)

    * [Offers](#offers)
        * [Offer Properties](#offer-properties)
        * [Create Offer](#create-offer)
        * [Retrieve Offers](#retrieve-offers)
        * [Retrieve Individual Offer](#retrieve-individual-offer)
        * [Update Offer](#update-offer)
        * [Delete Offer](#delete-offer)

    * [Buffets](#buffets)
        * [Buffet Properties](#buffet-properties)
        * [Create Buffet](#create-buffet)
        * [Retrieve Buffet](#retrieve-buffet)
        * [Retrieve Individual Buffet](#retrieve-individual-buffet)
        * [Update Buffet](#update-buffet)
        * [Delete Buffet](#delete-buffet)

    * [Offer Types](#offer-types)
        * [Offer Type Properties](#offer-type-properties)
        * [Create Offer Type](#create-offer-type)
        * [Retrieve Offer Types](#retrieve-offer-types)
        * [Retrieve Individual Offer Type](#retrieve-individual-offer-type)
        * [Update Offer Type](#update-offer-type)
        * [Delete Offer Type](#delete-offer-type)

    * [Platforms](#platforms)
        * [Platform Properties](#platform-properties)
        * [Create Platform](#platform-type)
        * [Retrieve Platforms](#retrieve-platforms)
        * [Retrieve Individual Platform](#retrieve-individual-platform)
        * [Update Platform](#update-platform)
        * [Delete Platform](#delete-platform)

    * [iOS Tiers](#ios-tiers)
        * [iOS Tier Properties](#ios-tier-properties)
        * [Create iOS Tier](#create-ios-tier)
        * [Update iOS Tier](#update-ios-tier)
        * [Retrieve iOS Tiers](#retrieve-ios-tiers)
        * [Retrieve Individual iOS Tier](#retrieve-individual-ios-tier)
        * [Delete iOS Tier](#delete-ios-tier)

    * [Android Tiers](#android-tiers)
        * [Android Tier Properties](#android-tier-properties)
        * [Create Android Tier](#create-android-tier)
        * [Update Android Tier](#update-android-tier)
        * [Retrieve Android Tiers](#retrieve-android-tiers)
        * [Retrieve Individual Android Tier](#retrieve-individual-android-tier)
        * [Delete Android Tier](#delete-android-tier)

    * [Windows Phone Tiers](#windows-phone-tiers)
        * [Windows Phone Tier Properties](#windows-phone-tier-properties)
        * [Create Windows Phone Tier](#create-windows-phone-tier)
        * [Update Windows Phone Tier](#update-windows-phone-tier)
        * [Retrieve Windows Phone Tiers](#retrieve-windows-phone-tiers)
        * [Retrieve Individual Windows Phone Tier](#retrieve-individual-windows-phone-tier)
        * [Delete Windows Phone Tier](#delete-windows-phone-tier)

    * [Currencies](#currencies)
        * [Currency Properties](#currency-properties)
        * [Create Currency](#create-currency)
        * [Retrieve Currencies](#retrieve-currencies)
        * [Retrieve Individual Currency](#retrieve-individual-currency)
        * [Update Currency](#update-currency)
        * [Delete Currency](#delete-currency)

    * [Payment Gateways](#payment-gateways)
        * [Payment Gateway Properties](#payment-gateway-properties)
        * [Create Payment Gateway](#create-payment-gateway)
        * [Retrieve Payment Gateways](#retrieve-payment-gateways)
        * [Retrieve Individual Payment Gateway](#retrieve-individual-payment-gateway)
        * [Update Payment Gateway](#update-payment-gateway)
        * [Delete Payment Gateway](#delete-payment-gateway)

    * [Campaigns](#campaigns)
        * [Campaign Properties](#campaign-properties)
        * [Create Campaign](#create-campaign)
        * [Retrieve Campaigns](#retrieve-campaigns)
        * [Retrieve Individual Campaign](#retrieve-individual-campaign)
        * [Retrieve Campaign's Items](#retrieve-campaigns-items)
        * [Update Campaign](#update-campaign)
        * [Delete Campaign](#delete-campaign)

    * [Discounts](#discounts) **TODO: UPDATE**
        * [Discount Properties](#discount-properties)
        * [Create Discount](#create-discount)
        * [Retrieve Discounts](#retrieve-discounts)
        * [Retrieve Individual Discount](#retrieve-individual-discount)
        * [Update Discount](#update-discount)
        * [Delete Discount](#delete-discount)

    * [Discount Codes](#discount-codes)
        * [Discount Codes Properties](#discount-codes-properties)
        * [Create Discount Codes](#create-discount-codes)
        * [Retrieve Discount Codes](#retrieve-discount-codes)
        * [Retrieve Individual Discount Code](#retrieve-individual-discount-code)
        * [Update Discount Code](#update-discount-code)
        * [Delete Discount Code](#delete-discount-code)

    * [Partners](#partners)
        * [Partner Properties](#partner-properties)
        * [Create Partner](#create-partner)
        * [Retrieve Partners](#retrieve-partners)
        * [Retrieve Individual Partner](#retrieve-individual-partner)
        * [Update Partner](#update-partner)
        * [Delete Partner](#delete-partner)

    * [Banners](#banners)
        * [Banner Properties](#banner-properties)
        * [Create Banner](#create-banner)
        * [Retrieve Banner](#retrieve-banners)
        * [Retrieve Individual Banner](#retrieve-individual-banner)
        * [Update Banner](#update-banner)
        * [Delete Banner](#delete-banner)

    * [Item Distribution Country Group](#item-distribution-country-group)
        * [Item Distribution Country Group Properties](#item-distribution-country-group-properties)
        * [Create Item Distribution Country Group](#create-item-distribution-country-group)
        * [Retrieve Item Distribution Country Groups](#retrieve-item-distribution-country-group)
        * [Retrieve Individual Item Distribution Country Group](#retrieve-individual-item-distribution-country-group)
        * [Update Item Distribution Country Group](#update-item-distribution-country-group)
        * [Delete Item Distribution Country Group](#delete-item-distribution-country-group)

    * [Revenue Share](#banners)
        * [Revenue Share Properties](#revenue-share-properties)
        * [Create Revenue Share](#create-revenue-share)
        * [Retrieve Revenue Share](#retrieve-revenue-shares)
        * [Retrieve Individual Revenue Share](#retrieve-individual-revenue-share)
        * [Update Revenue Share](#update-revenue-share)
        * [Delete Revenue Share](#delete-revenue-share)

    * [Payment Gateway Cost](#payment-gateway-cost)
        * [Payment Gateway Cost Properties](#payment-gateway-cost-properties)
        * [Create Payment Gateway Cost](#create-payment-gateway-cost)
        * [Retrieve Payment Gateway Cost](#retrieve-payment-gateway-costs)
        * [Retrieve Individual Payment Gateway Cost](#retrieve-payment-gateway-cost)
        * [Update Payment Gateway Cost](#update-payment-gateway-cost)
        * [Delete Payment Gateway Cost](#delete-payment-gateway-cost)

    * [Vendor Payment Gateway Cost](#banners)
        * [Vendor Payment Gateway Cost Properties](#vendor-payment-gateway-cost-properties)
        * [Create Vendor Payment Gateway Cost](#create-vendor-payment-gateway-cost)
        * [Retrieve Vendor Payment Gateway Cost](#retrieve-vendor-payment-gateway-costs)
        * [Retrieve Individual Vendor Payment Gateway Cost](#retrieve-vendor-payment-gateway-cost)
        * [Update Vendor Payment Gateway Cost](#update-vendor-payment-gateway-cost)
        * [Delete Vendor Payment Gateway Cost](#delete-vendor-payment-gateway-cost)

---

* [ORDERS, REFUNDS, AND PAYMENTS](#orders-refunds-and-payments)

    * [Orders](#orders)
        * [Order Properties](#order-properties)
        * [Retrieve Individual Temporarily Order](#retrieve-individual-temporarily-order)
        * [Retrieve Individual Order](#retrieve-individual-order)
        * [Checkout Order](#checkout-order)
        * [Confirm Order](#confirm-order)
        * [Order Transaction Histories](#order-transaction-histories)

    * [Refunds](#refunds)
        * [Refund Properties](#refund-properties)
        * [Retrieve Refunds](#retrieve-refunds)
        * [Retrieve Individual Refund](#retrieve-individual-refund)
        * [Update Refund](#update-refund)
        * [Delete Refund](#delete-refund)

    * [Payments](#payments)
        * [Payment Signature](#payment-signature)
        * [Payment Properties](#payment-properties)

    * [Free Payment Flow](#free-payment-flow)
        * [Free Payment Charge](#free-payment-charge)

    * [SCOOP Point Payment Flow](#scoop-point-payment-flow)
        * [SCOOP Point Payment Charge](#scoop-point-payment-charge)

    * [mandiri clickpay Payment Flow](#mandiri-clickpay-payment-flow)
        * [mandiri clickpay Payment Properties](#mandiri-clickpay-payment-properties)
        * [mandiri clickpay Payment Capture](#mandiri-clickpay-payment-capture)

    * [Paypal Payment Flow](#paypal-payment-flow)
        * [Paypal Payment Properties](#paypal-payment-properties)
        * [Paypal Payment Authorize](#paypal-payment-authorize)
        * [Paypal Payment Capture](#paypal-payment-capture)

    * [mandiri e-cash Payment Flow](#mandiri-e-cash-payment-flow)
        * [mandiri e-cash Properties](#mandiri-e-cash-payment-properties)
        * [mandiri e-cash Payment Authorize](#mandiri-e-cash-payment-authorize)
        * [mandiri e-cash Payment Validate](#mandiri-e-cash-payment-validate)

    * [Veritrans Payment Flow](#veritrans-payment-flow)
        * [Veritrans Payment Properties](#veritrans-payment-properties)
        * [Veritrans Payment Capture](#veritrans-payment-capture)

    * [Payment Tokens](#payment-tokens)
        * [Payment token Properties](#payment-token-properties)
        * [Retrieve Payment Tokens](#retrieve-payment-tokens)
        * [Retrieve Individual Payment Token](#retrieve-individual-payment-token)
        * [Update Payment Token](#update-payment-token)
        * [Delete Payment Token](#delete-payment-token)

    * [Pending Payment](#pending-payment)
        * [Pending Payment Properties](#pending-payment-properties)
        * [Retrieve Pending Payment(s)](#retrieve-pending-payment)

---

* [SCOOP POINTS](#scoop-points)

    * [Points](#points) **TODO: update implementation**
        * [Retrieve Individual User Point Summary](#retrieve-individual-user-point-summary)
        * [Acquire Points](#acquire-points)
        * [Use Points](#use-points)

---

* [USER RELATED](#user-related)

    * [User Owned Items](#user-owned-items)
        * [User Owned Item Properties](#user-owned-item-properties)
        * [Add New User Owned Items](#add-new-user-owned-items)
        * [Retrieve User Owned Items](#retrieve-user-owned-items)
        * [Update User Owned Item](#update-user-owned-item)
        * [Delete User Owned Item](#delete-user-owned-item)

    * [User Owned Subscriptions](#user-owned-subscriptions)
        * [User Owned Subscription Properties](#user-owned-subscription-properties)
        * [Add New User Owned Subscriptions](#add-new-user-owned-subscriptions)
        * [Retrieve User Owned Subscriptions](#retrieve-user-owned-subscriptions)
        * [Update User Owned Subscription](#update-user-owned-subscription)
        * [Delete User Owned Subscription](#delete-user-owned-subscription)

    * [Downloads](#downloads)
        * [Download Signature](#download-signature)
        * [Download Properties](#download-properties)
        * [Authorize and Create Download](#authorize-and-create-download)

    * [Emails](#emails) **TODO: Implementation**
        * [Email Properties](#email-properties)
        * [Send Email](#send-email)

    * [User Buffets](#user-buffets)
        * [User Buffets Properties](#user-buffets-properties)
        * [Retrieve User Buffets](#retrieve-user-buffets)

    * [Publisher Reminder](#banners)
        * [Publisher Reminder Properties](#publisher-reminder-properties)
        * [Create Publisher Reminder](#create-publisher-reminder)
        * [Retrieve Publisher Reminder](#retrieve-publisher-reminders)
        * [Retrieve Individual Publisher Reminder](#retrieve-individual-publisher-reminder)
        * [Update Publisher Reminder](#update-publisher-reminder)
        * [Delete Publisher Reminder](#delete-publisher-reminder)

---

* [MISC](#misc)

    * [Geolocations](#geolocations)
        * [Geolocation Properties](#geolocation-properties)
        * [Retrieve Individual Geolocation by IP Address](#retrieve-individual-geolocation-by-ip-address)

    * [Server Timestamp](#server-timestamp)
        * [Server Timestamp Properties](#server-timestamp-properties)
        * [Retrieve Server Timestamp]

---

* [LOGS](#logs)

    * [Analytic Log](#analytic-log)
        * [Analytic Log Properties](#analytic-log-properties)
        * [Create Analytic Logs](#create-analytic-logs)

    * [Pageviews](#pageviews)
        * [Analytic Log Pageview Properties](#analytic-log-pageview-properties)
        * [Create Pageview Analytic Log](#create-pageview-analytic-log)
        * [Retrieve Analytic Log Pageviews](#retrieve-analytic-log-pageviews)

    * [Downloads](#download)
        * [Analytic Log Download Properties](#analytic-log-download-properties)
        * [Create Download Analytic Log](#create-download-analytic-log)
        * [Retrieve Analytic Log Download](#retrieve-analytic-log-download)

    * [Open App](#open-app)
        * [Open App Log Properties](#open-app-log-properties)
        * [Create Open App Log](#create-open-app-log)
        * [Retrieve Open App Log](#retrieve-open-app-log)

    * [Facebook Log](#facebook-log)
        * [Create Facebook Log](#create-facebook-log)

---

* [What the Heck is All of This?? What Should I Do?](#what-the-heck-is-all-of-this-what-should-i-do)

---

## GENERAL

### Base URL and Versions

**Base URL**

Production HTTPS base URL: https://scoopadm.apps-foundry.com/scoopcor/api

Production HTTP base URL: http://scoopadm.apps-foundry.com/scoopcor/api

Test HTTP base URL: http://dev2.apps-foundry.com/scoopcor/api

**Major Version**

Major version is placed right after base URL with "v" prefix, e.g., https://scoopadm.apps-foundry.com/scoopcor/api/v2

### HTTP Header Fields

Unless for being specified in certain end-point, all end-point needs HTTP header fields as below.

* Authorization

    * `Authorization` - Authorization information

* No Cache for all POST and PUT, excepts POST that used to override GET

    * `Cache-Control` - Set the value to "no-cache"

* Request with JSON content

    * `Content-Type` - Set the value to "application/json"

* Minor Version

    * `Api-Version` - Set the value to

### Authorization

Most of the end-points will need HTTP header `Authorization` parameter to be provided with `Bearer` as the authorization type.

Each request will need to generate base 64 encoded string from valid `user_id` and `access_token` with colon ( : ) delimited:

value = base64encode(<user_id>:<access_token>)

**Example:**

```
user_id = 337373

access_token = a2435791637bca5be27b14877b01d69dcdc41e5e531a40d7a82417bb9e79e6a6e6b9e17b79682926

Base 64 Encoded value = MzM3MzczOjQxNGU2ZGMwODY2ZWQ0ZmYwZmE0NGE0OTY2OTNiYjgy

Authorization: Bearer MzM3MzczOjQxNGU2ZGMwODY2ZWQ0ZmYwZmE0NGE0OTY2OTNiYjgy
```

For more details about the authorization of the system, please refer to SCOOP CAS API specifications.

### Common Error HTTP Response Codes

* `400` - Bad Request

    * `400101` - Missing required parameter

    * `400112` - Invalid parameter value

    * `400301` - Invalid parameter format

* `401` - Unauthorized; Invalid access_token, invalid credentials, invalid username or password

    * `401103` - The resource owner credentials is invalid, access_token is invalid, expired, or revoked

* `403` - Forbidden; Token is correct, but the scope has no permission to do requested action

    * `403102` - Access denied

* `404` - Not Found; Requested resource not found

* `409` - Conflict

    * `409101` - Request parameter conflicted with existing one on server-side

* `500` - Internal Server Error; Unexpected server-side errors

---

## ITEMS AND METADATA

### Items

Item resource is the main resource that used to render store front. Item is related to metadata like [item type](#item-types), [country](#countries), [language](#languages), [category](#categories), [author](#authors), [brand](#brands), [vendor](#vendors), [parental control](#parental-controls), pricing.

The item's type define metadata and detail properties owned by an item. The current system supported 3 types:

1. Magazine

    Magazine is the main type of digital magazine, which metadata consists of : country, language, category, brand, vendor, parental control, and pricing. Author metadata is not available for this type.

2. Book

    Book is the main type of digital book, which metadata consists of: country, language, category, brand, vendor, author, parental control, and pricing.

3. Newspaper

    Newspaper is the main type of digital newspaper, which metadata consists of: country, language, category, brand, vendor, parental control, and pricing. Author metadata is not available for this type.

#### Item Properties

* `id` - Number; Instance identifier.

* `name` - String; Item name.

* `display_until` - String; Datetime to determine to when this item displayed on store. There's a cron to check hourly, if the datetime passed, the `item_status` will set to NOT FOR CONSUME.

* `edition_code` - String; Unique edition code from previous version, don't use this property anymore excepts for translation layer. **TODO: deprecated after all migrated**

* `sort_priority` - Number; Sort priority use to defining order. Higher number means higher priority.

* `is_active` - Boolean; Instance's status, used to mark if the instance is active or soft-deleted. Default to true.

* `content_type` - Number; Item's content type in general. For specific file type info, see [Item Files](#item-files).

    * 1: PDF

    * 2: ePub

* `item_status` - Number; Item's status

    * 1: NEW - New item in preparation; this is the default value when an item is new created with no specified status.

    * 2: READY FOR UPLOAD - The item is ready to be uploaded after the metadata is completed.

    * 3: UPLOADED - The item has already uploaded and ready for the next process.

    * 4: WAITING FOR REVIEW - The file is already completed and so is the metadata; waiting to be reviewed. This status is used when any item uploaded by the vendor needs review from our internal reviewer; shown to vendor to keep them posted about the status.

    * 5: IN REVIEW - The item is being reviewed by the reviewer(s). This status is shown to vendor to keep them posted about the status.

    * 6: REJECTED - The item is rejected. This status is shown to vendor to keep them posted about the status.

    * 7: APPROVED - The item is approved by the reviewer. This status is shown to vendor to keep them posted about the status.

    * 8: PREPARE FOR CONSUME - The item is in preparation to be placed on store, server side will proceed with needed processes if any before set the status to READY FOR CONSUME. This status is shown to vendor to keep them posted about the status.

    * 9: READY FOR CONSUME - The Item is already on store and ready for consumption.

    * 10: NOT FOR CONSUME - The item is taken down from store for some reasons. Users who owned the item is still able to see and download the item files. To disable the item completely, refer to `is_active` property

* `parental_control_id` - Number; Parental control identifier of the item.

* `parental_control` - Object; Parental control of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "parental_control"; otherwise, only `parental_control_id` is provided.

    * `id` - Number; Parental control identifier.

    * `name` - String; Parental control name.

* `item_type_id` - Number; Item type identifier of the item.

* `item_type` - Object; Item type of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "item_type"; otherwise, only `item_type_id` is provided.

    * `id` - Number; Item type identifier.

    * `name` - String; Item type name.

* `release_date` - String; Release date of the item in yyyy-MM-dd format.

* `brand_id` - Number; Brand identifier of the item.

* `brand` - Object; Brand of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "brand"; otherwise, only `brand_id` is provided.

    * `id` - Number; Brand identifier.

    * `name` - String; Brand name.

* `is_featured` - Boolean; Flag to define if the item featured in query. The default value is false.

* `is_extra` - Boolean; Flag to define if the item is an extra item. The default value is false.

* `is_latest` - Boolean; Flag to define if the item is the latest item in a brand.

* `authors` - Array; Authors of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "authors".

    * `id` - Number; Author identifier.

    * `name` - String; Author name.

* `categories` - Array; Categories of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "categories".

    * `id` - Number; Category identifier.

    * `name` - String; Category name.

* `languages` - Array; Languages of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "languages".

    * `id` - Number; Language identifier.

    * `name` - String; Language name.

* `countries` - Array; Countries of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "countries".

    * `id` - Number; Country identifier.

    * `name` - String; Country name.

* `vendor` - Object; Vendor of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "vendor".

    * `id` - Number; Parental control identifier.

    * `name` - String; Parental control name.

* `item_distribution_country_group_id` - Number; Group identifier where the item is distributed

* `reading_direction` - Number; The direction to read the item

    * 1: Left to Right

    * 2: Right to Left

* `issue_number` - String; Issue number of item, e.g., JUN-2013, ED 23, etc. This property is only available on magazine and newspaper.

* `revision_number` - String; Issue number of item, e.g., Revision 2, 2013, etc. This property is only available on book type.

* `slug` - String, Unique; Item slug for SEO.

* `media_base_url` - String; Base URL for image files, this string must appended to image URI.

* `thumb_image_normal` - String; URI for the item thumbnail in normal resolution.

* `thumb_image_highres` - String; URI for the item thumbnail in high resolution.

* `extra_items` - Array; List of extra item identifiers bind to this item, on delivery to user all of the extra items will be included

* `subs_weight` - Number; Weight of subscription for this item. Normally the value will be 1, means the item
will deducts 1 from user owned subscription quantity on every new item downloaded. On some special issue for magazine, this may set to 0 so it won't deduct user owned subscription. Default to 1

**Exception:** Extra items will deliver along with the primary item, subscription quantity will only deducted by primary item's `subs_weight`

* `meta` - String; Item meta for SEO.

* `description` - String; Item description.
.
* `image_normal` - String; Item thumb image URI

* `image_highres` - String; Item thumb image in highres URI.

* `gtin8` - String; [GTIN8](#http://en.wikipedia.org/wiki/Global_Trade_Item_Number). This property is only available on magazine type and book type.

* `gtin13` - String; [GTIN13](#http://en.wikipedia.org/wiki/Global_Trade_Item_Number). This property is only available on magazine type and book type.

* `gtin14` - String; [GTIN14](#http://en.wikipedia.org/wiki/Global_Trade_Item_Number). This property is only available on magazine type and book type.

* `previews` - Array; The preview of the item.

* `release_schedule` - String; Release schedule-date of the item in UTC format.

* `display_offers` - Displayed offers to show pricing and discount informations.

    * `offer_id` - Number; Offer identifier

    * `offer_name` - String; Offer's name

    * `offer_type_id` - Number; Offer's type identifier. See [Offer Properties](#offer-properties)

    * `price_usd` - String; Tag normal price to show to UI side in USD

    * `price_idr` - String; Tag normal price to show to UI side in IDR

    * `price_point` - String; Tag normal price to show to UI side in SCOOP Point

    * `tier_code` - String; Tier code of the price

    * `discount_price_usd` - String; Tag discount price to show to UI side in USD. Read-only, the value will auto-populated by the system

    * `discount_price_idr` - String; Tag discount price to show to UI side in IDR. Read-only, the value will auto-populated by the system

    * `discount_price_point` - String; Tag discount price to show to UI side in SCOOP Point. Read-only, the value will auto-populated by the system

    * `discount_tier_code` - String; Tier code of the price when discount. Read-only, the value will auto-populated by the system

    * `aggr_price_usd` - String; Tag aggregator price to show to UI side in USD

    * `aggr_price_usd` - String; Tag aggregator price to show to UI side in IDR

    * `aggr_price_usd` - String; Tag aggregator price to show to UI side in SCOOP Point

    * `discount_tag` - String; Discount tag, very short text to describe the discount. Read-only, the value will auto-populated by the system

    * `discount_name` - String; Discount name. Read-only, the value will auto-populated by the system

#### Create Item

This service use to create and return new created item on response.

On creation or update, if the item status set to PREPARE FOR CONSUME, the delivery of new item will be triggered and queued for later (in short) process. After the process is completed, server-side will automatically set the item status to READY FOR CONSUME.

**URL:** /items

**Method:** POST

**Request Data Params:**

Required: `name`, `item_type_id`, `sort_priority`, `is_active`, `slug`, `parental_control_id`, `release_date`, `brand_id`, `is_featured`, `is_extra`, `slug`, `subs_weight`, `thumb_image_normal`, `thumb_image_highres`, `image_normal`, `image_highres`, `reading_direction`

depends on `item_type_id`: `issue_number`, `revision_number`

Optional: `authors`, `categories`, `languages`, `countries`,  `extra_items`,  `meta`, `description`, `previews`, `item_distribution_country_group_id`

depends on `item_type_id`: `gtin8`, `gtin13`, `gtin14`

**Response HTTP Code:**

* `201` - New item created

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

Create new magazine:

```
POST /items HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Tempo ED 27 2014",
    "item_type_id": 1,
    "display_until": "2014-05-12T10:00:00.000Z",
    "content_type": 1,
    "sort_priority": 1,
    "is_active": true,
    "edition_code": "ID_TEMED272014", **TODO: deprecated after all migrated**
    "brand_id": 20,
    "is_featured": false,
    "is_extra": false,
    "release_date": "2014-01-20",
    "release_schedule":"2014-01-20T00:00:00",
    "parental_control_id": 3,
    "issue_no": "ED 27 2014",
    "slug": "ed-27-2014",
    "thumb_image_normal": "items/tempo/ed-27-2014/thumb_cover_normal.png",
    "thumb_image_highres": "items/tempo/ed-27-2014/thumb_cover_highres.png",
    "subs_weight": 1
    "description": "Jokowi efek gak ngefek",
    "meta": "Tempo ED 27 2014 Jokowi efek gak ngefek"
    "image_normal": "items/tempo/ed-27-2014/cover_normal.png",
    "image_highres": "items/tempo/ed-27-2014/cover_highres.png",
    "item_distribution_country_group_id" : 1,
    "restricted_countries" : ["UK", "NZ"],
    "categories": [1, 5, 6],
    "languages": [1],
    "countries": [1]
}
```

Create new book:

```
POST /items HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Harry Potter and the Winter is Coming",
    "item_type_id": 2,
    "content_type": 1,
    "display_until": "2014-05-12T10:00:00.000Z",
    "sort_priority": 1,
    "is_active": true,
    "edition_code": "ID_HPANDTWIC", **TODO: deprecated after all migrated**
    "brand_id": 345,
    "is_featured": false,
    "is_extra": false,
    "parental_control_id": 2,
    "release_date": "2014-01-20",
    "release_schedule":"2014-01-20T00:00:00",
    "revision_number": "2014",
    "slug": "harry-potter-and-the-winter-is-coming",
    "thumb_image_normal": "items/bip/harry-potter-and-the-winter-is-coming/thumb_cover_normal.png",
    "thumb_image_highres": "items/bip/harry-potter-and-the-winter-is-coming/thumb_cover_highres.png",
    "subs_weight": 1,
    "description": "Harry Potter sucked by giant sandworm and find himself in Westeros",
    "meta": "Harry Potter and the Winter is Coming sucked by giant sandworm and find himself in Westeros"
    "image_normal": "items/bip/harry-potter-and-the-winter-is-coming/cover_normal.png",
    "image_highres": "items/bip/harry-potter-and-the-winter-is-coming/cover_highres.png",
    "reading_directions" : 1,
    "item_distribution_country_group_id" : 1,
    "categories": [23, 55, 64],
    "languages": [2],
    "countries": [9]
}
```

Create new newspaper:

```
POST /items HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Kompas 14 APR 2014 (Siang)",
    "item_type_id": 3,
    "content_type": 1,
    "display_until": "2014-05-12T10:00:00.000Z",
    "sort_priority": 1,
    "is_active": true,
    "edition_code": "ID_KOM14APR2014S", **TODO: deprecated after all migrated**
    "brand_id": 305,
    "is_featured": false,
    "is_extra": false,
    "parental_control_id": 1,
    "release_date": "2014-01-20T19:00:000Z",
    "release_schedule":"2014-01-20T00:00:00",
    "issue_no": "14 APR Siang",
    "slug": "14-apr-2014-siang",
    "thumb_image_normal": "items/kompas/14-apr-2014-siang/thumb_cover_normal.png",
    "thumb_image_highres": "items/kompas/14-apr-2014-siang/thumb_cover_highres.png",
    "subs_weight": 1
    "description": "Pohon tumbang di Slipi",
    "meta": "Kompas 14 APR 2014 (Siang) Pohon tumbang di Slipi"
    "image_normal": "items/kompas/14-apr-2014-siang/cover_normal.png",
    "image_normal": "items/kompas/14-apr-2014-siang/cover_highres.png",
    "reading_directions" : 1,
    "item_distribution_country_group_id" : 1,
    "categories": [40, 41],
    "languages": [1],
    "countries": [1]
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/items/45

{
    "id": 45,
    "name": "Tempo ED 27 2014",
    "item_type_id": 1,
    "content_type": 1,
    "display_until": "2014-05-12T10:00:00.000Z",
    "sort_priority": 1,
    "is_active": true,
    "edition_code": "ID_TEMED272014", **TODO: deprecated after all migrated**
    "brand_id": 20,
    "is_featured": false,
    "is_extra": false,
    "parental_control_id": 3,
    "release_date": "2014-01-20",
    "release_schedule":"2014-01-20T00:00:00",
    "issue_no": "ED 27 2014",
    "slug": "ed-27-2014",
    "thumb_image_normal": "items/tempo/ed-27-2014/thumb_cover_normal.png",
    "thumb_image_highres": "items/tempo/ed-27-2014/thumb_cover_highres.png",
    "subs_weight": 1,
    "exclusive_countries": null
    "description": "Jokowi efek gak ngefek",
    "meta": "Tempo ED 27 2014 Jokowi efek gak ngefek"
    "image_normal": "items/tempo/ed-27-2014/cover_normal.png",
    "image_highres": "items/tempo/ed-27-2014/cover_highres.png",
    "reading_directions" : 1,
    "item_distribution_country_group_id" : 1,
    "gtin8": null,
    "gtin13": null,
    "gtin14": null,
    "categories": [1, 5, 6],
    "languages": [1],
    "countries": [1]
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409101,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Items

Retrieve items.

End-user client side only showing items that can be consumed by end-users. For store front, the retrieval should always query for `is_active` equals true, `is_extra` equals false, and `item_status` in *READY FOR CONSUME* or *NOT FOR CONSUME*. For User's cloud library, end-user client side should only show active items.

By default the `order` is ascending by id, End-user client side will need to specify `order` as *-release_date* if need to show the list as latest

**URL:** /items

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Item Properties](#item-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `expand` - Expand the result details. Default to null, the supported expand for items are as below

    * ext: get all related filters as object or array of objects contains identifier and name. See [Item Properties](#item-properties)

    * display_offers: get display offers as array and also offer's meta informations (this expand value is covered in "ext")

    * parental_control: get related parental_control as object (this expand value is covered in "ext")

    * item_type: get related item_type as object (this expand value is covered in "ext")

    * brand: get related brand as object (this expand value is covered in "ext")

    * vendor: get related vendor as object (this expand value is covered in "ext")

    * authors: get all related authors as array of objects (this expand value is covered in "ext")

    * categories: get all related categories as array of objects (this expand value is covered in "ext")

    * languages: get all related languages as array of objects (this expand value is covered in "ext")

    * countries: get all related countries as array of objects (this expand value is covered in "ext")

* `item_id` - Provided item identifier(s) to retrieve. Response will only returns if any matched

* `item_type_id` - Filter by item type identifier. Response will returns if matched

* `category_id` - Filter by category identifier(s). Response will returns if any matched

* `brand_id` - Filter by brand identifier. Response will returns if matched

* `vendor_id` - Filter by vendor identifier. Response will returns if matched

* `language_id` - Filter by language identifier(s). Response will returns if any matched

* `country_id` - Filter by country identifier(s). Response will returns if any matched

* `author_id` - Filter by author identifier(s). Response will returns if any matched

* `q` - Simple string query to records' string properties

* `created__lt`, `created__lte`, `created__gt`, `created__gte` - Filter `created` with operators

* `release_date__lt`, `release_date__lte`, `release_date__gt`, `release_date__gte` - Filter `release_date` with operators

* `item_status` - Filter by item's status(es). Response will only returns if any matched

* `display_offer_plid` - Filter result's offer for specific platform identifier. By default if this parameter is not provided, base offers will be returned

* `allowed_countries` - Array; Countries where the item(s) are allowed to be distributed, all the country codes are listed in ISO 3166-1 alpha-2 format. Set to empty if the item is allowed in every country.

* `restricted_countries` - Array; Countries where the item(s) are restricted to be distributed, all the country codes are listed in ISO 3166-1 alpha-2 format.

* `reading_direction` - Number; The direction to read the item

    * 1: Left to Right

    * 2: Right to Left

**Method:** GET

**Alternative Method:** **TODO: to be implemented**

To avoid maximum URL char in some user-agent, this resource also provided POST override. Set the HTTP Method to POST, include X-HTTP-Method-Override: GET in HTTP Header, and md5 the body content and place it after the resource alias

**Response HTTP Code:**

* `200` - Items retrieved and returned in body content

* `204` - No matched item

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

Retrieve magazine published by specific vendor, after specific release_date, and from offest 10 with limit 25

```
GET /items?offset=10&limit=25&vendor_id=2&release_date__gt=2014-01-01 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Retrieve magazine published by specific brand with expand "ext"

```
GET /items?brand_id=10&expand=ext HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response with default properties:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    "items": [{
        "is_featured": false,
        "extra_items": [],
        "revision_number": "by Walter Isaacson",
        "subs_weight": 1,
        "parental_control_id": 1,
        "item_type_id": 2,
        "is_active": true,
        "item_status": 10,
        "gtin8": "",
        "brand_id": 1034,
        "thumb_image_normal": "images/1/1034/general_small_covers/ID_MIZ2012MTH12SJ_S.jpg",
        "meta": "",
        "content_type": 1,
        "filenames": ["images/1/1034/ID_MIZ2012MTH12SJ.zip"],
        "id": 8175,
        "edition_code": "ID_MIZ2012MTH12SJ",
        "image_highres": "images/1/1034/big_covers/ID_MIZ2012MTH12SJ_B.jpg",
        "sort_priority": 1,
        "image_normal": "images/1/1034/general_big_covers/ID_MIZ2012MTH12SJ_B.jpg",
        "name": "Steve Jobs",
        "release_date": "2012-12-10T00:00:00",
        "release_schedule":"2014-01-20T00:00:00",
        "is_extra": false,
        "slug": "steve-jobs",
        "media_base_url": "http://images.getscoop.com/magazine_static/",
        "gtin14": "",
        "gtin13": "",
        "previews": [],
        "reading_directions" : 1,
        "allowed_countries" : ["ID", "AU", "MY"],
        "restricted_countries" : ["UK", "NZ"],
        "thumb_image_highres": "images/1/1034/small_covers/ID_MIZ2012MTH12SJ_S.jpg"
    },
    ...
    {
        "is_featured": false,
        "extra_items": [],
        "subs_weight": 1,
        "parental_control_id": 1,
        "item_type_id": 1,
        "is_active": true,
        "item_status": 9,
        "gtin8": "",
        "brand_id": 24,
        "thumb_image_normal": "None",
        "meta": "",
        "content_type": 1,
        "filenames": ["images/1/24/ID_CAL2011ED13.zip"],
        "id": 79,
        "edition_code": "ID_CAL2011ED13",
        "image_highres": "images/1/24/big_covers/big_cover__.png",
        "sort_priority": 1,
        "image_normal": "None",
        "name": "Charlie and Lola / JAN 2011",
        "release_date": "2010-12-29T00:00:00",
        "release_schedule":"2014-01-20T00:00:00",
        "is_extra": false,
        "slug": "charlie-and-lola-jan-2011",
        "media_base_url": "http://images.getscoop.com/magazine_static/",
        "gtin14": "None",
        "issue_number": "JAN 2011",
        "gtin13": "None",
        "previews": [],
        "reading_directions" : 1,
        "allowed_countries" : ["ID", "AU", "MY"],
        "restricted_countries" : ["UK", "NZ"],
        "thumb_image_highres": "images/1/24/small_covers/small_cover__.png"
    }],
    "metadata": {
        "resultset": {
            "count": 48140,
            "limit": 20,
            "offset": 0
        }
    }
}
```

Success response with "ext,display_offers" as expand value:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "items": [{
        "is_featured": false,
        "extra_items": [],
        "revision_number": "Yoris Sebastian",
        "subs_weight": 1,
        "parental_control_id": 1,
        "item_type_id": 2,
        "brand": {
            "id": 4848,
            "name": "Oh My Goodness - Creative Junkies"
        },
        "is_active": true,
        "item_status": 10,
        "gtin8": "",
        "brand_id": 4848,
        "thumb_image_normal": "images/1/4848/general_small_covers/ID_GPU2013MTH05OMGCJ_S.jpg",
        "meta": "",
        "content_type": 1,
        "authors": [{
            "id": 1739,
            "name": "Yoris Sebastian"
        }],
        "vendor": {
            "id": 251,
            "name": "Gramedia Pustaka Utama"
        },
        "display_offers": [],
        "filenames": ["images/1/4848/ID_GPU2013MTH05OMGCJ.zip"],
        "id": 18068,
        "edition_code": "ID_GPU2013MTH05OMGCJ",
        "image_highres": "images/1/4848/big_covers/ID_GPU2013MTH05OMGCJ_B.jpg",
        "languages": [{
            "id": 2,
            "name": "Indonesian"
        }],
        "sort_priority": 1,
        "image_normal": "images/1/4848/general_covers/ID_GPU2013MTH05OMGCJ_C.jpg",
        "name": "Oh My Goodness - Creative Junkies",
        "countries": [{
            "id": 1,
            "name": "Indonesia"
        }],
        "release_date": "2013-05-21T00:00:00",
        "release_schedule":"2014-01-20T00:00:00",
        "is_extra": false,
        "item_type": {
            "id": 2,
            "name": "Book"
        },
        "slug": "oh-my-goodness-creative-junkies",
        "media_base_url": "http://images.getscoop.com/magazine_static/",
        "gtin14": "9789792255263",
        "categories": [{
            "id": 74,
            "name": "Motivation & Self-Help"
        }],
        "gtin13": "",
        "parental_control": {
            "id": 1,
            "name": "4+"
        },
        "previews": [],
        "reading_directions" : 1,
        "allowed_countries" : ["ID", "AU", "MY"],
        "restricted_countries" : ["UK", "NZ"],
        "thumb_image_highres": "images/1/4848/small_covers/ID_GPU2013MTH05OMGCJ_S.jpg"
    },
    ...
    {
        "is_featured": false,
        "extra_items": [],
        "subs_weight": 1,
        "parental_control_id": 1,
        "item_type_id": 1,
        "brand": {
            "id": 24,
            "name": "Charlie and Lola"
        },
        "is_active": true,
        "item_status": 9,
        "gtin8": "",
        "brand_id": 24,
        "thumb_image_normal": "None",
        "meta": "",
        "content_type": 1,
        "authors": [],
        "vendor": {
            "id": 2,
            "name": "MRA"
        },
        "display_offers": [{
            "price_usd": "0.99",
            "offer_id": 940,
            "offer_type_id": 1,
            "price_idr": "9000.00",
            "price_point": 95,
            "is_free": false,
            "offer_name": "Single Edition"
        }],
        "filenames": ["images/1/24/ID_CAL2011ED13.zip"],
        "id": 79,
        "edition_code": "ID_CAL2011ED13",
        "image_highres": "images/1/24/big_covers/big_cover__.png",
        "languages": [{
            "id": 2,
            "name": "Indonesian"
        }],
        "sort_priority": 1,
        "image_normal": "None",
        "name": "Charlie and Lola / JAN 2011",
        "countries": [{
            "id": 1,
            "name": "Indonesia"
        }],
        "release_date": "2010-12-29T00:00:00",
        "release_schedule":"2014-01-20T00:00:00",
        "is_extra": false,
        "item_type": {
            "id": 1,
            "name": "Magazine"
        },
        "slug": "charlie-and-lola-jan-2011",
        "media_base_url": "http://images.getscoop.com/magazine_static/",
        "gtin14": "None",
        "issue_number": "JAN 2011",
        "categories": [{
            "id": 1,
            "name": "Kids"
        }],
        "gtin13": "None",
        "parental_control": {
            "id": 1,
            "name": "4+"
        },
        "previews": [],
        "reading_directions" : 1,
        "allowed_countries" : ["ID", "AU", "MY"],
        "restricted_countries" : ["UK", "NZ"],
        "thumb_image_highres": "images/1/24/small_covers/small_cover__.png"
    }],
    "metadata": {
        "resultset": {
            "count": 48140,
            "limit": 20,
            "offset": 0
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Latest Items

Retrieve latest items.

**URL:** /items/latest

Optional:

See [Retrieve Items](#retrieve-items), except `order` will be ignored

**Method:** GET

**Response HTTP Code:**

* `200` - Items retrieved and returned in body content

* `204` - No matched item

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

Retrieve latest magazines published by specific vendor, after specific release_date, and from offest 10 with limit 25

```
GET /items/latest?offset=10&limit=25&vendor_id=2&release_date__gt=2014-01-01 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Retrieve latest magazines published by specific brands with expand "ext" and "display_offers"

```
GET /items/latest?brand_id=10,11,12&expand=ext,display_offers HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

#### Retrieve Popular Items

Retrieve popular items.

**URL:** /items/popular

Optional:

See [Retrieve Items](#retrieve-items), except `order` and `item_id` will be ignored

**Method:** GET

**Response HTTP Code:**

* `200` - Items retrieved and returned in body content

* `204` - No matched item

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

Retrieve popular magazines published by specific vendor, after specific release_date, and from offest 10 with limit 25

```
GET /items/popular?offset=10&limit=25&vendor_id=2&release_date__gt=2014-01-01 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Retrieve popular magazines published by specific brand with expand "ext" and "display_offers"

```
GET /items/popular?brand_id=10&expand=ext,display_offers HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

#### Retrieve Free Items

Retrieve free items, sorted by the latest item.

**URL:** /items?is_free=True

Optional:

See [Retrieve Items](#retrieve-items); but at `display_offers` parameter, the prices (`price_usd`, `price_idr`, `price_point`) are set to "0.00"/"0" and the value of `is_free` is `true`

**Method:** GET

**Response HTTP Code:**

* `200` - Items retrieved and returned in body content

* `204` - No matched item

**Response Params:** See [Item Properties](#item-properties); but at `display_offers` parameter, the prices (`price_usd`, `price_idr`, `price_point`) are set to "0.00"/"0" and the value of `is_free` is `true`

**Examples:**

Request:

Retrieve free items, and from offest 10 with limit 25

```
GET /items?is_free=True
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

#### Search Items

Test based search resource for items. This service will seek for items and it's metadatas, then returns set of items.

The differences between this resource and GET `/items` are:

1. Seek more deep into items related metadata: authors, categories, brands, vendors, languages, countries

2. `q` URL parameter is compulsory, if none provided HTTP 400 will be returned

**URL:** /items/searches?q={query}

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Item Properties](#item-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `expand` - Expand the result details. Default to null, the supported expand for items are as below

    * ext: get all related filters as object or array of objects contains identifier and name. See [Item Properties](#item-properties)

    * display_offers: get display offers as array and also offer's meta informations (this expand value is covered in "ext")

    * parental_control: get related parental_control as object (this expand value is covered in "ext")

    * item_type: get related item_type as object (this expand value is covered in "ext")

    * brand: get related brand as object (this expand value is covered in "ext")

    * vendor: get related vendor as object (this expand value is covered in "ext")

    * authors: get all related authors as array of objects (this expand value is covered in "ext")

    * categories: get all related categories as array of objects (this expand value is covered in "ext")

    * languages: get all related languages as array of objects (this expand value is covered in "ext")

    * countries: get all related countries as array of objects (this expand value is covered in "ext")

* `item_type_id` - Filter by item type identifier(s). Response will returns if any matched

* `category_id` - Filter by category identifier(s). Response will returns if any matched

* `brand_id` - Filter by brand identifier(s). Response will returns if any matched

* `vendor_id` - Filter by vendor identifier(s). Response will returns if any matched

* `language_id` - Filter by language identifier(s). Response will returns if any matched

* `country_id` - Filter by country identifier(s). Response will returns if any matched

* `author_id` - Filter by author identifier(s). Response will returns if any matched

* `created__lt`, `created__lte`, `created__gt`, `created__gte` - Filter created with operators

* `release_date__lt`, `release_date__lte`, `release_date__gt`, `release_date__gte` - Filter release_date with operators

* `item_status` - Filter by item's status(es). Response will only returns if any matched

* `display_offer_plid` - Filter result's offer for specific platform identifier. By default if this parameter is not provided, base offers will be returned

Note: consider the max URL length on some system

**Method:** GET

**TODO: Alternative Method:**

To avoid maximum URL char in some user-agent, this resource also provided POST override. Set the HTTP Method to POST, include X-HTTP-Method-Override: GET in HTTP Header, and md5 the body content and place it after the resource alias

**Response HTTP Code:**

* `200` - Items retrieved and returned in body content

* `204` - No matched item

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

Retrieve items contains "rowling" string (most likely will hit by author name) from offest 10 with limit 25

```
GET /items/searches?q=rowling&offset=10&limit=25 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

#### Search Suggestions for Items

Search suggestions for Items.

This service query for brand's names and returns max 10 suggested names

**URL:** /items/searches/suggestions/{query}

**Method:** GET

**Response HTTP Code:**

* `200` - Suggestions retrieved and returned in body content

* `204` - No matched suggestion

**Response Params:**

`Suggestions` - Array; Array of suggestions from the system

Note: `metadata` property will be static for now, this reserved for future use

**Examples:**

Request:

```
GET /items/searches/suggestions/auto HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "suggestions": [
        "Autocar",
        "Automotif",
        "Auto-repair",
        "Auto Robotics"
    ],
    "metadata": {
        "resultset": {
            "count": 10,
            "offset": 0,
            "limit": 10
        }
    }
}
```

#### Retrieve Individual Item

Retrieve individual item using identifier.

**URL:** /items/{item_id}

**Alternative URL:** /items/edition_code/{edition_code} -- **TODO: Deprecated after all migrated**

Optional:

* `fields` - Retrieve specific properties. See [Item Properties](#item-properties). Default to ALL

* `expand` - Expand the result details. Default to null, the supported expand for items are as below

    * display_offers: get display offers

* `display_offer_plid` - Filter result's offer for specific platform identifier. By default if this parameter is not provided, base offers will be returned

**Method:** GET

**Response HTTP Code:**

* `200` - Item found and returned

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

```
GET /items/45?expand=display_offers HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    "is_featured": false,
    "extra_items": [],
    "description": "",
    "subs_weight": 1,
    "parental_control_id": 1,
    "item_type_id": 1,
    "brand": {
        "id": 72,
        "name": "Bloomberg Businessweek"
    },
    "is_active": true,
    "item_status": 9,
    "gtin8": "",
    "brand_id": 72,
    "thumb_image_normal": "None",
    "meta": "",
    "content_type": 1,
    "authors": [],
    "vendor": {
        "id": 27,
        "name": "Indomedia Dinamika"
    },
    "display_offers": [{
        "price_usd": "2.99",
        "offer_id": 1093,
        "offer_type_id": 1,
        "price_idr": "27000.00",
        "price_point": 290,
        "is_free": false,
        "offer_name": "Single Edition"
    }],
    "filenames": ["images/1/72/ID_BLO2430MAR2011.zip"],
    "id": 232,
    "edition_code": "ID_BLO2430MAR2011",
    "image_highres": "images/1/72/big_covers/b_.png",
    "languages": [{
        "id": 2,
        "name": "Indonesian"
    }],
    "sort_priority": 1,
    "image_normal": "None",
    "name": "Bloomberg Businessweek / 24\u201330 MAR 2011",
    "countries": [{
        "id": 1,
        "name": "Indonesia"
    }],
    "release_date": "2011-03-22T00:00:00",
    "release_schedule":"2014-01-20T00:00:00",
    "is_extra": false,
    "item_type": {
        "id": 1,
        "name": "Magazine"
    },
    "slug": "bloomberg-businessweek-24-30-mar-2011",
    "media_base_url": "http://images.getscoop.com/magazine_static/",
    "gtin14": "None",
    "issue_number": "24\u201330 MAR 2011",
    "categories": [{
        "id": 6,
        "name": "Business & Finance"
    }],
    "gtin13": "None",
    "parental_control": {
        "id": 1,
        "name": "4+"
    },
    "previews": [],
    "reading_directions" : 1,
    "allowed_countries" : ["ID", "AU", "MY"],
    "restricted_countries" : ["UK", "NZ"],
    "thumb_image_highres": "images/1/72/small_covers/s_.png"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Retrieve Item Recommendations

Retrieve recommendation for specific item.

**URL:** /items/{item_id}/recommendations

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Item Properties](#item-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `expand` - Expand the result details. Default to null, the supported expand for items are as below

    * ext: get all related filters as object or array of objects contains identifier and name. See [Item Properties](#item-properties)

    * display_offers: get display offer array and also offer's meta informations (this expand value is covered in "ext")

    * parental_control: get related parental control as object (this expand value is covered in "ext")

    * item_type: get related item type as object (this expand value is covered in "ext")

    * brand: get related brand as object (this expand value is covered in "ext")

    * vendor: get related vendor as object (this expand value is covered in "ext")

    * authors: get all related authors as array of objects (this expand value is covered in "ext")

    * categories: get all related categories as array of objects (this expand value is covered in "ext")

    * languages: get all related languages as array of objects (this expand value is covered in "ext")

    * countries: get all related countries as array of objects (this expand value is covered in "ext")

* `display_offer_plid` - Filter result's offer for specific platform identifier. By default if this parameter is not provided, base offers will be returned

**Method:** GET

**Response HTTP Code:**

* `200` - Item found and returned

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

```
GET /items/45/recommendations?expand=ext HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

#### Update Item

Update item.

**URL:** /items/{item_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `item_type_id`, `sort_priority`, `is_active`, `slug`, `parental_control_id`, `release_date`, `brand_id`, `is_featured`, `is_extra`, `slug`, `subs_weight`, `thumb_image_normal`, `thumb_image_highres`, `image_normal`, `image_highres`,

depends on `item_type_id`: `issue_number`, `revision_number`

Optional: `authors`, `categories`, `languages`, `countries`,  `extra_items`,  `meta`, `description`, `previews`

depends on `item_type_id`: `gtin8`, `gtin13`, `gtin14`

**Response HTTP Code:**

* `200` - Item updated

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

```
PUT /items/45 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Tempo ED 27 2014",
    "item_type_id": 1,
    "content_type": 1,
    "display_until": "2014-05-12T10:00:00.000Z",
    "sort_priority": 2,
    "is_active": true,
    "edition_code": "ID_TEMED272014", **TODO: deprecated after all migrated**
    "brand_id": 20,
    "is_featured": false,
    "is_extra": false,
    "parental_control_id": 3,
    "release_date": "2014-01-20",
    "release_schedule":"2014-01-20T00:00:00",
    "issue_no": "ED 27 2014",
    "slug": "ed-27-2014",
    "thumb_image_normal": "items/tempo/ed-27-2014/thumb_cover_normal.png",
    "thumb_image_highres": "items/tempo/ed-27-2014/thumb_cover_highres.png",
    "subs_weight": 1,
    "exclusive_countries": null,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "meta": "Tempo ED 27 2014 Jokowi efek gak ngefek"
    "image_normal": "items/tempo/ed-27-2014/cover_normal.png",
    "image_highres": "items/tempo/ed-27-2014/cover_highres.png",
    "gtin8": null,
    "gtin13": null,
    "gtin14": null,
    "reading_directions" : 1,
    "allowed_countries" : ["ID", "AU", "MY"],
    "restricted_countries" : ["UK", "NZ"],
    "categories": [1, 5, 6],
    "languages": [1],
    "countries": [1]
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 45,
    "name": "Tempo ED 27 2014",
    "item_type_id": 1,
    "content_type": 1,
    "display_until": "2014-05-12T10:00:00.000Z",
    "sort_priority": 2,
    "is_active": true,
    "edition_code": "ID_TEMED272014", **TODO: deprecated after all migrated**
    "brand_id": 20,
    "is_featured": false,
    "is_extra": false,
    "parental_control_id": 3,
    "release_date": "2014-01-20",
    "release_schedule":"2014-01-20T00:00:00",
    "issue_no": "ED 27 2014",
    "slug": "ed-27-2014",
    "thumb_image_normal": "items/tempo/ed-27-2014/thumb_cover_normal.png",
    "thumb_image_highres": "items/tempo/ed-27-2014/thumb_cover_highres.png",
    "subs_weight": 1,
    "exclusive_countries": null,
    "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    "meta": "Tempo ED 27 2014 Jokowi efek gak ngefek"
    "image_normal": "items/tempo/ed-27-2014/cover_normal.png",
    "image_highres": "items/tempo/ed-27-2014/cover_highres.png",
    "gtin8": null,
    "gtin13": null,
    "gtin14": null,
    "reading_directions" : 1,
    "allowed_countries" : ["ID", "AU", "MY"],
    "restricted_countries" : ["UK", "NZ"],
    "categories": [1, 5, 6],
    "languages": [1],
    "countries": [1]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Item

Delete item. This service will set `is_active` value to false.

**URL:** /items/{item_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Item deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /items/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Item Files

Item file resource used to get which files are linked to an item. This resource should only used by the server side, for client side to get file for download, see [Downloads](#downloads).

If there's new kind of type supported (e.g., support to open HTML file), along with logic implementation, file type must be added.

#### Item File Properties

* `id` - Number; Instance identifier

* `item_id` - Number; Item's identifier which this file linked to

* `file_name` - String; File name

* `file_type` - Number; File type

    * 1: PDF Bulk ZIP Encrypted, PDF document in a single file, one item one file

    * 2: ePub Bulk ZIP Encrypted, PDF file in a bulk, one item one file

    * 3: Image File RAW Encrypted, single file of image, mostly for alternative image version or ads

* `file_order` - Number; Order number defining file order starts from 1, ascending. For single file item, the value will be set to 1

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `file_status` - Number; File status. Default to NEW

    * 1: NEW - New added file meta, for upload later; State: not active

    * 2: UPLOADING - File uploading. for later use if the system supports pause-and-resume in file upload progress; State: not active

    * 3: UPLOADED - File uploaded to temporarily location, ready for process; State: not active

    * 4: PROCESSING - Processing the file; State: not active

    * 5: READY - File ready for consume by user; State: active

    * 6: NOT ACTIVE - File inactivated due to some pending issues; State: not active

    * 7: DELETED - File deleted, the psychical file deleted, later if want to reupload, file_status must set to NEW; State: not active

* `file_version` - String; Version of the file, in timestamp, for client side to check if there's new version available. Note: the reason we don't use `modified` property directly for this needs because sometimes we just reupload the same version. This field related to `md5_checksum`, when the version changed, the checksum need to be updated too

* `key` - String; Global encryption key of the file. In JSON representation, this key must be passed in encrypted data

* `md5_checksum` - String; MD5 checksum of the file, use to compare after client side downloaded the file. If null provided, the client side will ignore the check

#### Create Item Files

Create new item files.

**URL:** /item_files

**Method:** POST

**Request Data Params:**

Required: `item_id `, `file_name `, `file_type `, `sort_priority `, `is_active `, `file_status `, `key`

Optional: `file_version `, `md5_checksum `

**Response HTTP Code:**

* `207` - Multiple status of each item file

    * `201` -  New item file created

**Response Params:** See [Item File Properties](#item-file-properties)

**Examples:**

Request:

```
POST /item_files HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "item_files": [
        {
            "item_id": 876,
            "file_name": "/tempo/ed-25-2014/page1.zip",
            "file_type": 3,
            "file_order" : 1,
            "is_active": True,
            "file_status": 1,
            "key": ""
        },
        {
            "item_id": 876,
            "file_name": "/tempo/ed-25-2014/page2.zip",
            "file_type": 3,
            "file_order" : 2,
            "is_active": True,
            "file_status": 1,
            "key": ""
        }
    ]
}
```

Success response:

```
HTTP/1.1 207 Multi-Status
Content-Type: application/json; charset=utf-8

{
    "results":[
        {
            "status": 201,
            "id": 1233,
            "item_id": 876,
            "file_name": "/tempo/ed-25-2014/page1.zip",
            "file_type": 3,
            "file_order" : 1,
            "is_active": True,
            "file_status": 1,
            "key": "",
            "file_version": null,
            "md5_checksum": null
        },
        {
            "status": 201,
            "id": 1234,
            "item_id": 876,
            "file_name": "/tempo/ed-25-2014/page2.zip",
            "file_type": 3,
            "file_order" : 2,
            "is_active": True,
            "file_status": 1,
            "key": "",
            "file_version": null,
            "md5_checksum": null
        }
    ]
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

#### Retrieve Item Files

Retrieve item files.

**URL:** /item_files

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Item File Properties](#item-file-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

* `item_id` - Filter by item identifier(s). Response will returns if any matched

**Method:** GET

**Response HTTP Code:**

* `200` - Item files retrieved and returned in body content

* `204` - No matched item file

**Response Params:** See [Item File Properties](#item-file-properties)

**Examples:**

Request:

```
GET /item_files?order=file_order&item_id=223 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "item_files": [
        {
            "id": 12341,
            "item_id": 432,
            "file_name": "/tempo/ed-45-2014/magazine.zip",
            "file_type": 1,
            "file_order" : 1,
            "is_active": True,
            "file_status": 4,
            "key": "ew@*G94YB>0BEc$bD#VOcVR:751Ama",
            "file_version": "2014-02-13T23:34:34.231Z",
            "md5_checksum": "7815696ecbf1c96e6894b779456d330e"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 1,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Item File

Retrieve individual item file using identifier.

**URL:** /item_files/{item_file_id}

**Alternative URL:** /items/{item_id}/files/file_order/{file_order}

Optional:

* `fields` - Retrieve specific properties. See [Item File Properties](#item-file-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Item file found and returned

**Response Params:** See [Item File Properties](#item-file-properties)

**Examples:**

Request:

```
GET /items/12/files/file_order/3 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 234,
    "item_id": 12,
    "file_name": "/tempo/ed-23-2014/page3.zip",
    "file_type": 3,
    "file_order" : 3,
    "is_active": True,
    "file_status": 4,
    "key": "3aYq{w62l?pU9,X`xm$p*M#16sl8I7",
    "file_version": "2013-01-23T12:34:34.231Z",
    "md5_checksum": "1a604da008c5b755ad064d9e7c8682c3"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Item File

Update item file.

**URL:** /item_files/{item_file_id}

**Method:** PUT

**Request Data Params:**

Required: `item_id `, `file_name `, `file_type `, `sort_priority `, `is_active `, `file_status `, `key`

Optional: `file_version `, `md5_checksum `

**Response HTTP Code:**

* `200` - Item file updated

**Response Params:** See [Item File Properties](#item-file-properties)

**Examples:**

Request:

```
PUT /item_files/234 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "item_id": 12,
    "file_name": "/tempo/ed-23-2014/page3.zip",
    "file_type": 3,
    "file_order" : 3,
    "is_active": True,
    "file_status": 4,
    "key": "3aYq{w62l?pU9,X`xm$p*M#16sl8I7",
    "file_version": "2014-04-25T11:04:34.221Z",
    "md5_checksum": "12a9b051b58b8925c790ccae3f02941c"
}

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 234,
    "item_id": 12,
    "file_name": "/tempo/ed-23-2014/page3.zip",
    "file_type": 3,
    "file_order" : 3,
    "is_active": True,
    "file_status": 4,
    "key": "3aYq{w62l?pU9,X`xm$p*M#16sl8I7",
    "file_version": "2014-04-25T11:04:34.221Z",
    "md5_checksum": "12a9b051b58b8925c790ccae3f02941c"
}

```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Item File

Delete item file. This service will set `is_active` value to false.

**URL:** /item_files/{item_file_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Item file deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /item_files/234 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Item Types

Item type resource used to identify system's supported item types.

Current existing item types are:

1. Magazine: digital magazines

2. Book: digital books

3. Newspaper: digital newspapers

Note: New item type mostly will has specific use-cases come along with it. So this resource records only for metadata only, the use-cases implementation will be vary.

#### Item Type Properties

* `id` - Number; Instance identifier

* `name` - String; Item type name

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `slug` - String, Unique; Item type slug for SEO

* `meta` - String; Item type meta for SEO

* `description` - String; Item type description

**Note:**

**Each item type has it own unique use cases:**

**1. Different properties, e.g., book type got author, magazine type not**

**2. Different offers, e.g., newspaper type has no single type of offer**

**In worst case, if need to switch the item type, please consult with the developer team.**

#### Create Item Type

Create new item type.

**URL:** /item_types

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `201` - New item type created

**Response Params:** See [Item Type Properties](#item-type-properties)

**Examples:**

Request:

```
POST /item_types HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Magazine",
    "sort_priority" : 1,
    "is_active": True,
    "slug": "magazine",
    "meta": "Digital magazine type",
    "description": "Digital magazine type"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/item_types/1

{
    "id": 1
    "name": "Magazine",
    "sort_priority": 1,
    "is_active": True,
    "slug": "magazine",
    "meta": "Digital magazine type",
    "description": "Digital magazine type"
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Item Types

Retrieve item types.

**URL:** /item_types

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Item Type Properties](#item-type-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Item types retrieved and returned in body content

* `204` - No matched item type

**Response Params:** See [Item Type Properties](#item-type-properties)

**Examples:**

Request:

```
GET /item_types?fields=id,name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "item_types": [
        {
            "id": 1,
            "name": "Magazine"
        },
        {
            "id": 2,
            "name": "Book"
        },
        {
            "id": 3,
            "name": "Newspaper"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 3,
            "offset": 0,
            "limit": 3
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Item Type

Retrieve individual item type using identifier.

**URL:** /item_types/{item_type_id}

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific properties. See [Item Type Properties](#item-type-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Item type found and returned

**Response Params:** See [Item Type Properties](#item-type-properties)

**Examples:**

Request:

```
GET /item_types/1?fields=id,name,sort_priority,slug,meta,description,is_active HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Magazine",
    "sort_priority": 1,
    "slug": "magazine",
    "meta": "Digital magazine type",
    "description": "Digital magazine type",
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Item Type

Update item type.

**URL:** /item_types/{item_type_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `200` - Item type updated

**Response Params:** See [Item Type Properties](#item-type-properties)

**Examples:**

Request:

```
PUT /item_types/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Magz",
    "sort_priority": 2,
    "slug": "magazine",
    "meta": "Digital magazine type",
    "description": "Digital magazine type",
    "is_active": true
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Magz",
    "sort_priority": 2,
    "slug": "magazine",
    "meta": "Digital magazine type",
    "description": "Digital magazine type",
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Item Type

Delete item type. This service will set `is_active` value to false.

**URL:** /item_types/{item_type_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Item type deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /item_types/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Vendors

Vendor resource contains all items' providers, which in current business model is what we called publishers.

#### Vendor Properties

* `id` - Number; Instance identifier

* `name` - String; Vendor name

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority

* `vendor_status` - Number; Vendor status in the system

    * 1: NEW - New created vendor

    * 2: WAITING FOR REVIEW - Waiting for business side review for any kind of partnership

    * 3: IN REVIEW - In review

    * 4: REJECTED - Rejected due to some reasons

    * 5: APPROVED - Aproved by business side

    * 6: CLEAR - Clear for any action

    * 7: BANNED - Banned due to some reasons

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `slug` - String, Unique; Vendor slug for SEO

* `meta` - String; Vendor meta for SEO

* `description` - String; Vendor description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URI.

* `icon_image_normal` - String; Vendor icon image URI.

* `icon_image_highres` - String; Vendor icon image in highres URI

* `organization_id` - Number; Unique, organization identifier that this instance bind to. A vendor can only bind to an organization

* `links` - Array; List of the vendor's social media account(s)

* `link_type` - Number; Type of the link, based on the social media account

    * 1: Facebook account

    * 2: Twitter account

* `url` - String; The url of the social media account owned by the vendor

* `accounting_identifier` - String; Accounting identifier

* `iso4217_code` - String; The format of the currency in ISO 4217 standard

* `report_header_logo` - Number; Predefined type of header logo format, e.g:

    * 1: Apps Foundry PT logo

    * 2: PT Aplikasi Prima Persada logo

* `report_summary_format` - Number; Predefined type of report summary format:

    * 1: Summary with platforms

    * 2: Summmary with platforms and payment gateway

* `report_summary_as_invoice` - String; Set to true if summary is reported as invoice

* `show_preferred_currency_rates` - String; Set to true if the preferred currency rate is shown

* `report_detail_columns` - List of report data to be shown on report summary; all the values are in String (boolean), True (shown on report) or False (not shown on report):

    * `title`

    * `product_name`

    * `price`

    * `revenue`

    * `sales_or_return`

    * `payment_gateway`

    * `platform`

    * `application`

    * `coupon_code`

    * `author`

    * `isbn_or_gtin`

    * `content`

    * `purchase_plan_type`

    * `orderline_id`

#### Create Vendor

Create new vendor.

**URL:** /vendors

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`, `cut_rule_id`, `organization_id`, `vendor_status`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `links`

**Response HTTP Code:**

* `201` - New vendor created

**Response Params:** See [Vendor Properties](#vendor-properties)

**Examples:**

Request:

```
POST /vendors HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "MRA",
    "slug": "mra",
    "sort_priority": 1,
    "vendor_status": 6,
    "is_active": true,
    "description": "MRA",
    "meta": "MRA",
    "icon_image_normal": "vendors/mra/icon.png",
    "icon_image_highres": "vendors/mra/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 3,
    "accounting_identifier": "P-0047",
    "iso4217_code": "USD",
    "report_summary_as_invoice": "true",
    "show_preferred_currency_rates": "true"
    "report_header_logo": 1,
    "report_summary_format": 2,
    "report_detail_columns" [
        "title": "true",
        "product_name": "true",
        "price": "true",
        "revenue": "true",
        "sales_or_return": "true",
        "payment_gateway": "true",
        "platform": "true",
        "application": "true",
        "coupon_code": "true",
        "author": "true",
        "isbn_or_gtin": "true",
        "content": "true",
        "purchase_plan_type": "true",
        "orderline_id": "true"
    ],
    "links": [
        {
            "link_type": 1,
            "url": "www.facebook.com/lorem"
        },
        {
            "link_type": 2,
            "url": "www.twitter.com/lorem"
        }
    ]
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/vendors/5

{
    "id": 5,
    "name": "MRA",
    "slug": "mra",
    "sort_priority": 1,
    "vendor_status": 6,
    "is_active": true,
    "description": "MRA",
    "meta": "MRA",
    "icon_image_normal": "vendors/mra/icon.png",
    "icon_image_highres": "vendors/mra/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 3,
    "accounting_identifier": "P-0047",
    "iso4217_code": "USD",
    "report_summary_as_invoice": "true",
    "show_preferred_currency_rates": "true"
    "report_header_logo": 1,
    "report_summary_format": 2,
    "report_detail_columns" [
        "title": "true",
        "product_name": "true",
        "price": "true",
        "revenue": "true",
        "sales_or_return": "true",
        "payment_gateway": "true",
        "platform": "true",
        "application": "true",
        "coupon_code": "true",
        "author": "true",
        "isbn_or_gtin": "true",
        "content": "true",
        "purchase_plan_type": "true",
        "orderline_id": "true"
    ],
    "links": [
        {
            "link_type": 1,
            "url": "www.facebook.com/lorem"
        },
        {
            "link_type": 2,
            "url": "www.twitter.com/lorem"
        }
    ]
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Vendors

Retrieve vendors.

**URL:** /vendors

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Vendor Properties](#vendor-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

* `organization_id` - Filter by organization identifier(s). Response will returns if any matched

**Method:** GET

**Response HTTP Code:**

* `200` - Vendor retrieved and returned in body content

* `204` - No matched vendor

**Response Params:** See [Vendor Properties](#vendor-properties)

**Examples:**

Request:

```
GET /vendors?fields=id,name&offset=0&limit=1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "vendors": [
        {
            "id": 1,
            "name": "TEMPO"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 357,
            "offset": 0,
            "limit": 1
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Vendor

Retrieve individual vendor using identifier.

**URL:** /vendors/{vendor_id}

**URL:** /vendors/organization/{organization_id}

Optional:

* `fields` - Retrieve specific properties. See [Vendor Properties](#vendor-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Vendor found and returned

**Response Params:** See [Vendor Properties](#vendor-properties)

**Examples:**

Request:

```
GET /vendors/5 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 5,
    "name": "MRA",
    "slug": "mra",
    "sort_priority": 1,
    "vendor_status": 6,
    "is_active": true,
    "description": "MRA",
    "meta": "MRA",
    "icon_image_normal": "vendors/mra/icon.png",
    "icon_image_highres": "vendors/mra/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 3,
    "accounting_identifier": "P-0047",
    "iso4217_code": "USD",
    "report_summary_as_invoice": "true",
    "show_preferred_currency_rates": "true"
    "report_header_logo": 1,
    "report_summary_format": 2,
    "report_detail_columns" [
        "title": "true",
        "product_name": "true",
        "price": "true",
        "revenue": "true",
        "sales_or_return": "true",
        "payment_gateway": "true",
        "platform": "true",
        "application": "true",
        "coupon_code": "true",
        "author": "true",
        "isbn_or_gtin": "true",
        "content": "true",
        "purchase_plan_type": "true",
        "orderline_id": "true"
    ],
    "links": [
        {
            "link_type": 1,
            "url": "www.facebook.com/lorem"
        },
        {
            "link_type": 2,
            "url": "www.twitter.com/lorem"
        }
    ]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Vendor

Update vendor.

**URL:** /vendors/{vendor_id}

**Alternative URL:** /vendors/organization/{organization_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`, `cut_rule_id`, `organization_id`, `vendor_status`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `200` - Vendor updated

**Response Params:** See [Vendor Properties](#vendor-properties)

**Examples:**

Request:

```
PUT /vendors/5 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "MRA",
    "slug": "mra",
    "sort_priority": 1,
    "vendor_status": 6,
    "is_active": true,
    "description": "MRA",
    "meta": "MRA",
    "icon_image_normal": "vendors/mra/icon.png",
    "icon_image_highres": "vendors/mra/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 3,
    "accounting_identifier": "P-0047",
    "iso4217_code": "USD",
    "report_summary_as_invoice": "true",
    "show_preferred_currency_rates": "true"
    "report_header_logo": 1,
    "report_summary_format": 2,
    "report_detail_columns" [
        "title": "true",
        "product_name": "true",
        "price": "true",
        "revenue": "true",
        "sales_or_return": "true",
        "payment_gateway": "true",
        "platform": "true",
        "application": "true",
        "coupon_code": "true",
        "author": "true",
        "isbn_or_gtin": "true",
        "content": "true",
        "purchase_plan_type": "true",
        "orderline_id": "true"
    ],
    "links": [
        {
            "link_type": 1,
            "url": "www.facebook.com/lorem"
        },
        {
            "link_type": 2,
            "url": "www.twitter.com/lorem"
        }
    ]
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 5,
    "name": "MRA",
    "slug": "mra",
    "sort_priority": 1,
    "vendor_status": 6,
    "is_active": true,
    "description": "MRA",
    "meta": "MRA",
    "icon_image_normal": "vendors/mra/icon.png",
    "icon_image_highres": "vendors/mra/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 3,
    "accounting_identifier": "P-0047",
    "iso4217_code": "USD",
    "report_summary_as_invoice": "true",
    "show_preferred_currency_rates": "true"
    "report_header_logo": 1,
    "report_summary_format": 2,
    "report_detail_columns" [
        "title": "true",
        "product_name": "true",
        "price": "true",
        "revenue": "true",
        "sales_or_return": "true",
        "payment_gateway": "true",
        "platform": "true",
        "application": "true",
        "coupon_code": "true",
        "author": "true",
        "isbn_or_gtin": "true",
        "content": "true",
        "purchase_plan_type": "true",
        "orderline_id": "true"
    ],
    "links": [
        {
            "link_type": 1,
            "url": "www.facebook.com/lorem"
        },
        {
            "link_type": 2,
            "url": "www.twitter.com/lorem"
        }
    ]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Vendor

Deletes vendor. This service will set `is_active` value to false.

**URL:** /vendors/{vendor_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Vendor deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /vendors/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Deactivate Vendor

Deactivates vendor. Sets the vendor and it's items to be hidden.

**URL:** /vendors/{vendor_id}/deactivate

**Method:** PUT

**Response HTTP Code:**

* `200` - Vendor Deactivated

**Response Params:** -

**Examples:**

Request:

```
PUT /vendors/1/deactivate HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 200 Ok
Content-Type: application/json; charset=utf-8

{
    "name" : "Media Pangan Indonesia"
    "deactivate": [
        {
            "items": [1,2,3,4,5,6,7,8,9]
            "name": "FOOD REVIEW Indonesia"
        },
        {
            "items": [1,2,3,4,5,6,7,8,9]
            "name": "KULINOLOGI INDONESIA"
        },
    ]

}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Brands

Brand resource contains vendor item's brand.

On publishing company, brand is magazine title, book series title, or newspaper name.

#### Brand Properties

* `id` - Number; Instance identifier.

* `name` - String; Brand name.

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority.

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true.

* `slug` - String, Unique; Brand slug for SEO.

* `meta` - String; Brand meta for SEO.

* `description` - String; Brand description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URI.

* `icon_image_normal` - String; Brand icon image URI.

* `icon_image_highres` - String; Brand icon image in highres URI.

* `vendor_id` - Number; Brand's vendor identifier

* `links` - Array; List of the vendor's social media account(s)

* `link_type` - Number; Type of the link, based on the social media account

    * 1: Facebook account

    * 2: Twitter account

* `url` - String; The url of the social media account owned by the vendor

* `daily_release_quota` - Number; Daily maximum quota to upload items

* `default_categories` -  Number; List of the category ID where the brand is included in

* `default_languages` - Number; List of the language ID where the brand is included in

* `default_countries` - Number; List of the country ID where the brand is included in

* `default_parental_control_id` - Number; Parental ID where the brand is included in

* `default_item_type_id` - Number; Type of the item (book, magazine, or newspaper)

* `default_authors` - Number; List of the author ID where the brand is included in

* `default_distribution_groups` - Number; List of the distribution ID where the brand is included in

#### Create Brand

Create new brand.

**URL:** /brands

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `vendor_id`, `links`

**Response HTTP Code:**

* `201` - New brand created

**Response Params:** See [Brand Properties](#brand-properties)

**Examples:**

Request:

```
POST /brands HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "FHM Indonesia",
    "slug": "fhm-indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "FHM Indonesia bring men lifestyle to the next level",
    "meta": "FHM Indonesia majalah pria metroseksual",
    "icon_image_normal": "brands/fhm-indonesia/icon.png",
    "icon_image_highres": "brands/fhm-indonesia/icon_highres.png",
    "vendor_id": 2,
    "daily_release_quota": 2,
    "default_categories": [1,2,3],
    "default_languages": [1,2,3],
    "default_countries": [1,2,3],
    "default_parental_control_id": 1,
    "default_item_type_id": 1,
    "default_authors": [1,2,3],
    "default_distribution_groups": [1,2,3],
    "links": [
        {
            "link_type": 1,
            "url": "www.facebook.com/lorem"
        },
        {
            "link_type": 2,
            "url": "www.twitter.com/lorem"
        }
    ]
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/brands/25

{
    "id": 25,
    "name": "FHM Indonesia",
    "slug": "fhm-indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "FHM Indonesia bring men lifestyle to the next level",
    "meta": "FHM Indonesia majalah pria metroseksual",
    "icon_image_normal": "brands/fhm-indonesia/icon.png",
    "icon_image_highres": "brands/fhm-indonesia/icon_highres.png",
    "vendor_id": 2,
    "daily_release_quota": 2,
    "default_categories": [1,2,3],
    "default_languages": [1,2,3],
    "default_countries": [1,2,3],
    "default_parental_control_id": 1,
    "default_item_type_id": 1,
    "default_authors": [1,2,3],
    "default_distribution_groups": [1,2,3],
    "links": [
        {
            "link_type": 1,
            "url": "www.facebook.com/lorem"
        },
        {
            "link_type": 2,
            "url": "www.twitter.com/lorem"
        }
    ]
}

```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Brands

Retrieve brands.

**URL:** /brands

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Brand Properties](#brand-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

* `vendor_id` - Vendor identifier filter for brands

**Method:** GET

**Response HTTP Code:**

* `200` - Brands retrieved and returned in body content

* `204` - No matched brand

**Response Params:** See [Brand Properties](#brand-properties)

**Examples:**

Request:

```
GET /brands?fields=id,name,vendor_id&offset=0&limit=10&vendor_id=2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "brands": [
        {
            "id": 24,
            "name": "Autocar",
            "vendor_id": 2
        },
        {
            "id": 25,
            "name": "FHM-Indonesia",
            "vendor_id": 2
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Brand

Retrieve individual brand using identifier.

**URL:** /brands/{brand_id}

Optional:

* `fields` - Retrieve specific properties. See [Brand Properties](#brand-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Brand found and returned

**Response Params:** See [Brand Properties](#brand-properties)

**Examples:**

Request:

```
GET /brands/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Tempo",
    "slug": "tempo",
    "sort_priority": 1,
    "is_active": true,
    "description": "Tempo deliver what matters",
    "meta": "Tempo berita keuangan politik",
    "icon_image_normal": "brands/tempo/icon.png",
    "icon_image_highres": "brands/tempo/icon_highres.png",
    "vendor_id": 1,
    "daily_release_quota": 2,
    "default_categories": [1,2,3],
    "default_languages": [1,2,3],
    "default_countries": [1,2,3],
    "default_parental_control_id": 1,
    "default_item_type_id": 1,
    "default_authors": [1,2,3],
    "default_distribution_groups": [1,2,3],
    "links": [
        {
            "link_type": 1,
            "url": "www.facebook.com/lorem"
        },
        {
            "link_type": 2,
            "url": "www.twitter.com/lorem"
        }
    ]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Brand

Update brand.

**URL:** /brands/{brand_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `vendor_id`

**Response HTTP Code:**

* `200` - Brand updated

**Response Params:** See [Brand Properties](#brand-properties)

**Examples:**

Request:

```
PUT /brands/25 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "FHM",
    "slug": "fhm-indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "FHM Indonesia bring men lifestyle to the next level",
    "meta": "FHM Indonesia majalah pria metroseksual",
    "icon_image_normal": "brands/fhm-indonesia/icon.png",
    "icon_image_highres": "brands/fhm-indonesia/icon_highres.png",
    "vendor_id": 2,
    "daily_release_quota": 2,
    "default_categories": [1,2,3],
    "default_languages": [1,2,3],
    "default_countries": [1,2,3],
    "default_parental_control_id": 1,
    "default_item_type_id": 1,
    "default_authors": [1,2,3],
    "default_distribution_groups": [1,2,3],
    "links": [
        {
            "link_type": 1,
            "url": "www.facebook.com/lorem"
        },
        {
            "link_type": 2,
            "url": "www.twitter.com/lorem"
        }
    ]
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 25,
    "name": "FHM",
    "slug": "fhm-indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "FHM Indonesia bring men lifestyle to the next level",
    "meta": "FHM Indonesia majalah pria metroseksual",
    "icon_image_normal": "brands/fhm-indonesia/icon.png",
    "icon_image_highres": "brands/fhm-indonesia/icon_highres.png",
    "vendor_id": 2,
    "daily_release_quota": 2,
    "default_categories": [1,2,3],
    "default_languages": [1,2,3],
    "default_countries": [1,2,3],
    "default_parental_control_id": 1,
    "default_item_type_id": 1,
    "default_authors": [1,2,3],
    "default_distribution_groups": [1,2,3],
    "links": [
        {
            "link_type": 1,
            "url": "www.facebook.com/lorem"
        },
        {
            "link_type": 2,
            "url": "www.twitter.com/lorem"
        }
    ]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Brand

Delete brand. This service will set `is_active` value to false.

**URL:** /brands/{brand_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Brand deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /brands/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Authors

Author resource contains items' authors.

#### Author Properties

* `id` - Number; Instance identifier

* `name` - String; Author name

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `slug` - String, Unique; Author slug for SEO

* `first_name` - String; First name

* `last_name` - String; Last name

* `title` - Number; Author's title

* `academic_title` - String; Author's academic title

* `birthdate` - String; Author's birthdate

* `meta` - String; Author meta for SEO


#### Create Author

Create new author.

**URL:** /authors

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `first_name`, `last_name`, `title`, `academic_title`, `birthdate`, `meta`

**Response HTTP Code:**

* `201` - New author created

**Response Params:** See [Author Properties](#author-properties)

**Examples:**

Request:

```
POST /authors HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%


{
    "name": "Lorem ipsum",
    "is_active": True,
    "sort_priority": 1,
    "first_name": "Lorem ipsum",
    "last_name": "takeda",
    "title": "Mr.",
    "academic_title": "Thunderbird",
    "birthdate": "2014-03-12"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/authors/231

{
    "id": 231,
    "name": "Lorem ipsum",
    "is_active": True,
    "sort_priority": 1,
    "first_name": "Lorem ipsum",
    "last_name": "takeda",
    "title": "Mr.",
    "academic_title": "Thunderbird",
    "birthdate": "2014-03-12"
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

#### Retrieve Authors

Retrieve authors.

**URL:** /authors

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Author Properties](#author-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Author retrieved and returned in body content

* `204` - No matched author

**Response Params:** See [Author Properties](#author-properties)

**Examples:**

Request:

```
GET /authors?fields=id,name&offset=0&limit=2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "authors": [
        {
            "id": 1,
            "name": "RL Stained"
        },
        {
            "id": 2,
            "name": "John Greenshame"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 127,
            "offset": 0,
            "limit": 2
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Author

Retrieve individual author using identifier.

**URL:** /authors/{author_id}

Optional:

* `fields` - Retrieve specific properties. See [Author Properties](#author-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Author found and returned

**Response Params:** See [Author Properties](#author-properties)

**Examples:**

Request:

```
GET /authors/231 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 231,
    "name": "JK RAWLING",
    "slug": "jk-rawling",
    "sort_priority": 1,
    "is_active": true,
    "first_name": "Jusup Kolak",
    "last_name": "Rawon Keling",
    "title": "Mr.",
    "academic_title": "Bachelor of Writing",
    "birthdate": "1956-03-12",
    "meta": "Top British author"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Author

Update author.

**URL:** /authors/{author_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `profile`

**Response HTTP Code:**

* `200` - Author updated

**Response Params:** See [Author Properties](#author-properties)

**Examples:**

Request:

```
PUT /authors/231 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "id": 231,
    "name": "JK RAWLING",
    "slug": "jk-rawling",
    "sort_priority": 1,
    "is_active": true
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 231,
    "name": "JK RAWLING",
    "slug": "jk-rawling",
    "sort_priority": 1,
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Author

Delete author. This service will set `is_active` value to false.

**URL:** /authors/{author_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Author deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /authors/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Countries

Country resource contains item's country.

#### Country Properties

* `id` - Number; Instance identifier.

* `code_alpha2` - String; Unique; Country code in [ISO 3166-1 alpha-2](#http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2).

* `code_alpha3` - String; Unique; Country code in [ISO 3166-1 alpha-3](#http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3)

* `idd_code` - String; Unique; International direct dialing code.

* `name` - String; Country name.

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority.

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true.

* `slug` - String, Unique; Country slug for SEO.

* `meta` - String; Country meta for SEO.

* `description` - String; Country description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URI.

* `icon_image_normal` - String; Country icon image URI.

* `icon_image_highres` - String; Country icon image in highres URI.

#### Create Country

Create new country.

**URL:** /countries

**Method:** POST

**Request Data Params:**

Required: `code_alpha2`, `code_alpha3`, `idd_code`, `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `201` - New country created

**Response Params:** See [Country Properties](#country-properties)

**Examples:**

Request:

```
POST /countries HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "code_alpha2": "ID",
    "code_alpha3": "IDN",
    "idd_code": "+62",
    "name": "Indonesia",
    "slug": "indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "Indonesia",
    "meta": "Indonesia",
    "icon_image_normal": "countries/indonesia/icon.png",
    "icon_image_highres": "countries/indonesia/icon_highres.png"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/countries/1

{
    "id": 1,
    "code_alpha2": "ID",
    "code_alpha3": "IDN",
    "idd_code": "+62",
    "name": "Indonesia",
    "slug": "indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "Indonesia",
    "meta": "Indonesia",
    "icon_image_normal": "countries/indonesia/icon.png",
    "icon_image_highres": "countries/indonesia/icon_highres.png""
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Countries

Retrieve countries.

**URL:** /countries

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Country Properties](#country-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Countries retrieved and returned in body content

* `204` - No matched brand

**Response Params:** See [Country Properties](#country-properties)

**Examples:**

Request:

```
GET /countries?fields=id,code HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "countries": [
        {
            "id": 1,
            "code": "IDN"
        },
        {
            "id": 2,
            "code": "SGP"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Country

Retrieve individual country using system internal identifier, ISO 3166-1 alpha-2, or ISO 3166-1 alpha-3.

**URL:** /countries/{country_id}

**Alternative URL:** /countries/code_alpha2/{code_alpha2}

**Alternative URL:** /countries/code_alpha3/{code_alpha3}

Optional:

* `fields` - Retrieve specific properties. See [Country Properties](#country-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Country found and returned

**Response Params:** See [Country Properties](#country-properties)

**Examples:**

Request:

```
GET /countries/code_alpha2/ID HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "code_alpha2": "ID",
    "code_alpha3": "IDN",
    "idd_code": "+62",
    "name": "Indonesia",
    "slug": "indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "Indonesia",
    "meta": "Indonesia",
    "icon_image_normal": "countries/indonesia/icon.png",
    "icon_image_highres": "countries/indonesia/icon_highres.png"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Country

Update country.

**URL:** /countries/{country_id}

**Method:** PUT

**Request Data Params:**

Required: `code_alpha2`, `code_alpha3`, `idd_code`, `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `200` - Country updated

**Response Params:** See [Country Properties](#country-properties)

**Examples:**

Request:

```
PUT /countries/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "code_alpha2": "ID",
    "code_alpha3": "IDN",
    "idd_code": "+62",
    "name": "Indonesia",
    "slug": "indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "Indonesia",
    "meta": "Indonesia",
    "icon_image_normal": "countries/indonesia/icon.png",
    "icon_image_highres": "countries/indonesia/icon_highres.png"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "code_alpha2": "ID",
    "code_alpha3": "IDN",
    "idd_code": "+62",
    "name": "Indonesia",
    "slug": "indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "Indonesia",
    "meta": "Indonesia",
    "icon_image_normal": "countries/indonesia/icon.png",
    "icon_image_highres": "countries/indonesia/icon_highres.png"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Country

Delete country. This service will set `is_active` value to false.

**URL:** /countries/{country_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Country deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /countries/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Languages

Language resource contains item's language.

#### Language Properties

* `id` - Number; Instance identifier.

* `name` - String; Language name.

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority.

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true.

* `slug` - String, Unique; Language slug for SEO.

* `meta` - String; Language meta for SEO.

* `description` - String; Language description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URI.

* `icon_image_normal` - String; Language icon image URI.

* `icon_image_highres` - String; Language icon image in highres URI.

#### Create Language

Create new language.

**URL:** /languages

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `201` - New language created

**Response Params:** See [Language Properties](#language-properties)

**Examples:**

Request:

```
POST /languages HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Indonesia",
    "slug": "indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "Indonesia",
    "meta": "Indonesia",
    "icon_image_normal": "languages/indonesia/icon.png",
    "icon_image_highres": "languages/indonesia/icon_highres.png"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/languages/1

{
    "id": 1,
    "name": "Indonesia",
    "slug": "indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "Indonesia",
    "meta": "Indonesia",
    "icon_image_normal": "languages/indonesia/icon.png",
    "icon_image_highres": "languages/indonesia/icon_highres.png"
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Languages

Retrieve languages.

**URL:** /languages

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Language Properties](#language-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Languages retrieved and returned in body content

* `204` - No matched language

**Response Params:** See [Language Properties](#language-properties)

**Examples:**

Request:

```
GET /languages?fields=id,name HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "languages": [
        {
            "id": 1,
            "name": "Indonesia"
        },
        {
            "id": 2,
            "code": "English"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Language

Retrieve individual language

**URL:** /languages/{language_id}

Optional:

* `fields` - Retrieve specific properties. See [Language Properties](#language-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Language found and returned

**Response Params:** See [Language Properties](#language-properties)

**Examples:**

Request:

```
GET /languages/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Indonesia",
    "slug": "indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "Indonesia",
    "meta": "Indonesia",
    "icon_image_normal": "languages/indonesia/icon.png",
    "icon_image_highres": "languages/indonesia/icon_highres.png"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Language

Update language.

**URL:** /languages/{language_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `200` - Language updated

**Response Params:** See [Language Properties](#language-properties)

**Examples:**

Request:

```
PUT /languages/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Bahasa Indonesia",
    "slug": "indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "Indonesia",
    "meta": "Indonesia",
    "icon_image_normal": "languages/indonesia/icon.png",
    "icon_image_highres": "languages/indonesia/icon_highres.png"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Bahasa Indonesia",
    "slug": "indonesia",
    "sort_priority": 1,
    "is_active": true,
    "description": "Indonesia",
    "meta": "Indonesia",
    "icon_image_normal": "languages/indonesia/icon.png",
    "icon_image_highres": "languages/indonesia/icon_highres.png"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Language

Delete language. This service will set `is_active` value to false.

**URL:** /languages/{language_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Language deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /languages/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Categories

Category resource contains item's category.

#### Category Properties

* `id` - Number; Instance identifier.

* `item_type_id` - Number; Item type identifier of this category.

* `parent_category_id` - Number; Parent category of this category, use for multi-leveled category.

* `name` - String; Category name.

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority.

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true.

* `slug` - String, Unique; Category slug for SEO.

* `meta` - String; Category meta for SEO.

* `description` - String; Category description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URI.

* `icon_image_normal` - String; Category icon image URI.

* `icon_image_highres` - String; Category icon image in highres URI.

* `countries` - Array; Category's countries. If null, the category is eligible for all countries.

#### Create Category

Create new category.

**URL:** /categories

**Method:** POST

**Request Data Params:**

Required: `item_type_id`, `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `parent_category_id`, `countries`

**Response HTTP Code:**

* `201` - New category created

**Response Params:** See [Category Properties](#category-properties)

**Examples:**

Request:

```
POST /categories HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "item_type_id": 1
    "name": "Men's",
    "slug": "mens",
    "sort_priority": 1,
    "is_active": true,
    "description": "Men's category",
    "meta": "men's category",
    "icon_image_normal": "categories/indonesia/icon.png",
    "icon_image_highres": "categories/indonesia/icon_highres.png",
    "parent_category_id": null,
    "countries": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/categories/23

{
    "id": 23
    "item_type_id": 1
    "name": "Men's",
    "slug": "mens",
    "sort_priority": 1,
    "is_active": true,
    "description": "Men's category",
    "meta": "men's category",
    "icon_image_normal": "categories/indonesia/icon.png",
    "icon_image_highres": "categories/indonesia/icon_highres.png",
    "parent_category_id": null,
    "countries": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Categories

Retrieve categories.

**URL:** /categories

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Category Properties](#category-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

* `item_type_id` - Item type identifier of categories to be retrieved

* `parent_category_id` - Parent category identifier of categories to be retrieved.

    * If not provided, the default results will be all categories

    * If provided with null value, the results will be root categories (the categories without parent)

* `countries` - List of country identifiers of categories to be retrieved. Any category having one of provided country will be returned

**Method:** GET

**Response HTTP Code:**

* `200` - Categories retrieved and returned in body content

* `204` - No matched brand

**Response Params:** See [Category Properties](#category-properties)

**Examples:**

Request:

```
GET /categories?fields=id,name,countries&countries_id=1,2&item_type_id=3 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "categories": [
        {
            "id": 1,
            "name": "Jawa Barat",
            "countries":[1]
        },
        {
            "id": 2,
            "code": "Jawa Timur",
            "countries":[1]
        },
        {
            "id": 3,
            "name": "Singapore",
            "countries":[2]
        },
        {
            "id": 3,
            "name": "Men's",
            "countries":[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        }
    ],
    "metadata": {
        "resultset": {
            "count": 3,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Category

Retrieve individual category

**URL:** /categories/{category_id}

Optional:

* `fields` - Retrieve specific properties. See [Category Properties](#category-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Category found and returned

**Response Params:** See [Category Properties](#category-properties)

**Examples:**

Request:

```
GET /categories/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 23,
    "item_type_id": 1,
    "name": "Men's",
    "slug": "mens",
    "sort_priority": 1,
    "is_active": true,
    "description": "Men's category",
    "meta": "men's category",
    "icon_image_normal": "categories/indonesia/icon.png",
    "icon_image_highres": "categories/indonesia/icon_highres.png",
    "parent_category_id": null,
    "countries": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Category

Update category.

**URL:** /categories/{category_id}

**Method:** PUT

**Request Data Params:**

Required: `item_type_id`, `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `parent_category_id`, `countries`

**Response HTTP Code:**

* `200` - Category updated

**Response Params:** See [Category Properties](#category-properties)

**Examples:**

Request:

```
PUT /categories/23 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "item_type_id": 1,
    "name": "Men's",
    "slug": "mens",
    "sort_priority": 1,
    "is_active": true,
    "description": "Men's global category",
    "meta": "men's global category",
    "icon_image_normal": "categories/indonesia/icon.png",
    "icon_image_highres": "categories/indonesia/icon_highres.png",
    "parent_category_id": null,
    "countries": null
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 23,
    "item_type_id": 1,
    "name": "Men's",
    "slug": "mens",
    "sort_priority": 1,
    "is_active": true,
    "description": "Men's global category",
    "meta": "men's global category",
    "icon_image_normal": "categories/indonesia/icon.png",
    "icon_image_highres": "categories/indonesia/icon_highres.png",
    "parent_category_id": null,
    "countries": null
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Category

Delete category. This service will set `is_active` value to false.

**URL:** /categories/{category_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Category deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /categories/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Parental Controls

Parental control resource.

Current parental control list:

* **4+**

    * Item in this category contain no objectionable material.

* **9+**

    * Item in this category may contain mild or infrequent occurrences of cartoon, fantasy, or realistic violence, and infrequent or mild mature, suggestive, or horror-themed content which may not be suitable for children under the age of 9.

* **12+**

    * Item in this category may also contain infrequent mild language, frequent or intense cartoon, fantasy, or realistic violence, and mild or infrequent mature or suggestive themes, and simulated gambling, which may not be suitable for children under the age of 12.

* **17+**

    * You must be at least 17 years old to consume this item. Items in this category may also contain frequent and intense offensive language; frequent and intense cartoon, fantasy, or realistic violence; and frequent and intense mature, horror, and suggestive themes; plus sexual content, nudity, alcohol, tobacco, and drugs which may not be suitable for children under the age of 17.

Note: Client side implementation might has restriction password recovery. To send password recovery email, refer to [Send Email](#send-email)

#### Parental Control Properties

* `id` - Number; Instance identifier.

* `name` - String; Parental control name.

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority.

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true.

* `slug` - String, Unique; Parental control slug for SEO.

* `meta` - String; Parental control meta for SEO.

* `description` - String; Parental control description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URI.

* `icon_image_normal` - String; Parental control icon image URI.

* `icon_image_highres` - String; Parental control icon image in highres URI.

#### Create Parental Control

Create new parental control.

**URL:** /parental_controls

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `201` - New parental control created

**Response Params:** See [Parental Control Properties](#parental-control-properties)

**Examples:**

Request:

```
POST /parental_controls HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "12+",
    "slug": "twelve-plus",
    "sort_priority": 1,
    "is_active": true,
    "description": "For age 12 or more",
    "meta": "For age 12 or more",
    "icon_image_normal": "parental_controls/twelve_plus/icon.png",
    "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/parental_controls/3

{
    "id": 3,
    "name": "12+",
    "slug": "twelve-plus",
    "sort_priority": 1,
    "is_active": true,
    "description": "For age 12 or more",
    "meta": "For age 12 or more",
    "icon_image_normal": "parental_controls/twelve_plus/icon.png",
    "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Parental Controls

Retrieve parental controls.

**URL:** /parental_controls

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Parental Control Properties](#parental-control-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Parental Controls retrieved and returned in body content

* `204` - No matched parental control

**Response Params:** See [Parental Control Properties](#parental control-properties)

**Examples:**

Request:

```
GET /parental_controls?fields=id,name HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "parental_controls": [
        {
            "id": 1,
            "name": "4+"
        },
        {
            "id": 2,
            "code": "7+"
        },
        {
            "id": 3,
            "name": "12+"
        },
        {
            "id": 4,
            "code": "17+"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Parental Control

Retrieve individual parental control

**URL:** /parental_controls/{parental_control_id}

Optional:

* `fields` - Retrieve specific properties. See [Parental Control Properties](#parental-control-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Parental control found and returned

**Response Params:** See [Parental Control Properties](#parental-control-properties)

**Examples:**

Request:

```
GET /parental_controls/3 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 3,
    "name": "12+",
    "slug": "twelve-plus",
    "sort_priority": 1,
    "is_active": true,
    "description": "For age 12 or more",
    "meta": "For age 12 or more",
    "icon_image_normal": "parental_controls/twelve_plus/icon.png",
    "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Parental Control

Update parental control.

**URL:** /parental_controls/{parental_control_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `200` - Parental control updated

**Response Params:** See [Parental Control Properties](#parental-control-properties)

**Examples:**

Request:

```
PUT /parental_controls/3 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "12+",
    "slug": "twelve-plus",
    "sort_priority": 1,
    "is_active": true,
    "description": "For age 12+",
    "meta": "For age 12+",
    "icon_image_normal": "parental_controls/twelve_plus/icon.png",
    "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 3,
    "name": "12+",
    "slug": "twelve-plus",
    "sort_priority": 1,
    "is_active": true,
    "description": "For age 12+",
    "meta": "For age 12+",
    "icon_image_normal": "parental_controls/twelve_plus/icon.png",
    "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Parental Control

Delete parental control. This service will set `is_active` value to false.

**URL:** /parental_controls/{parental_control_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Parental control deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /parental_controls/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Bonuses

Bonus resource. This API Allows the system to send 5 free items and give free SCOOP points to new users.

#### Bonuses Properties

* `id` - Number; Gift identifier

* `name` - String; The name of the gift

* `items` - Number; List of the item id that included within the gift

* `brands` - Number; List of the brand id that included within the gift

* `valid_to` - String; Datetime in UTC when the gift validity is due on

* `valid_from` - String; Datetime in UTC when the gift is valid from

* `description` - String; Gift description

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. The default value is true.

* `point` - Number; Amount of SCOOP points the user get from new account registration

* `point_referral` - Number; Amount of SCOOOP points the user get by entering referral code while creating new account


#### Create Bonuses

Makes new Bonuses.

**URL:** /bonuses

**Method:** POST

**Request Data Params:**

Required: `name`, `valid_to`, `valid_from`

Optional: `point`, `point_referral`, `brands`, `items`

**Response HTTP Code:**

* `201` - New Bonuses is created

**Response Params:** See [Bonuses Properties](#bonuses-properties)

**Examples:**

Request:

```
POST /bonuses HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Register users",
    "items": [1,2,3],
    "brands": [17,18],
    "valid_to": "2015-02-22T00:00:00Z",
    "valid_from": "2015-02-20T00:00:00Z",
    "description": "lorem ipsum dolor siamet",
    "is_active": True,
    "point": 50,
    "point_referral": 100
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/bonuses/randombonus

{
    "id": 1,
    "name": "Register users",
    "items": [1,2,3],
    "brands": [17,18],
    "valid_to": "2015-02-22T00:00:00Z",
    "valid_from": "2015-02-20T00:00:00Z",
    "description": "lorem ipsum dolor siamet",
    "is_active": True,
    "point": 50,
    "point_referral": 100
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Bonuses

Retrieves all of the Bonuses.

**URL:** /bonuses

**Method:** GET

**Response HTTP Code:**

* `200` - Bonuses are retrieved and returned in body content

* `204` - No matched Bonuses

**Response Params:** See [Bonuses Properties](#bonuses-properties)

**Examples:**

Request:

```
GET /bonuses HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "bonuses": [
        {
            "id": 1,
            "name": "Register users",
            "items": [1,2,3],
            "brands": [17,18],
            "valid_to": "2015-02-22T00:00:00Z",
            "valid_from": "2015-02-20T00:00:00Z",
            "description": "lorem ipsum dolor siamet",
            "is_active": True,
            "point": 50,
            "point_referral": 100
        },
        {
            "id": 2,
            "name": "Facebook login",
            "items": [1,2,3],
            "brands": [17,18],
            "valid_to": "2015-02-22T00:00:00Z",
            "valid_from": "2015-02-20T00:00:00Z",
            "description": "lorem ipsum dolor siamet",
            "is_active": True,
            "point": 50,
            "point_referral": 100
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 20
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Gift

Retrieves individual Gift by using gift identifier.

**URL:** /bonuses/{gift_name}

**Response HTTP Code:**

* `200` - Bonuses data is found and returned

**Response Params:** See [Bonuses Properties](#bonuses-properties)

**Examples:**

Request:

```
GET /bonuses/randombonus HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "name": "Randombonus",
    "items": [1,2,3],
    "brands": [17,18],
    "valid_to": "2015-02-22T00:00:00Z",
    "valid_from": "2015-02-20T00:00:00Z",
    "description": "lorem ipsum dolor siamet",
    "is_active": True,
    "point": 50,
    "point_referral": 100
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Bonuses

Updates Bonuses data.

**URL:** /bonuses/{gift_name}

**Method:** PUT

**Request Data Params:**

Required: `name`, `valid_to`, `valid_from`

Optional: `point`, `point_referral`, `brands`, `items`

**Response HTTP Code:**

* `200` - Bonuses is updated

**Response Params:** See [Bonuses properties](#bonuses-properties)

**Examples:**

Request:

```
PUT /bonuses/randombonus HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "id": 2,
    "name": "Randombonus",
    "items": [1,2,3],
    "brands": [17,18],
    "valid_to": "2015-02-22T00:00:00Z",
    "valid_from": "2015-02-20T00:00:00Z",
    "description": "lorem ipsum dolor siamet",
    "is_active": True,
    "point": 50,
    "point_referral": 100
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "name": "Randombonus",
    "items": [1,2,3],
    "brands": [17,18],
    "valid_to": "2015-02-22T00:00:00Z",
    "valid_from": "2015-02-20T00:00:00Z",
    "description": "lorem ipsum dolor siamet",
    "is_active": True,
    "point": 50,
    "point_referral": 100
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Bonuses

Deletes Gift. This service will set `is_active` value to false.

**URL:** /bonuses/{gift_name}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Gift is deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /bonuses/randombonuses HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Claim Bonuses Bonuses

API to claim the gifts

#### ACCESS API

**HTTP** (token : can_read_write_public)

**HTTPS** (token : can_read_write_public_ext)

#### Claim Bonuses Properties

* `name` - String; The name of the gift

* `items` - Number; List of the items that included within the gift. The combination between items and brands.

            * If the brands (from gifts API) is left blank, all the items are retrieved based on the items (from gifts API)

            * If the items (from gifts API) is left blank, all the items are retrieved based on the last item by the brands

#### Update Claim Bonuses

Updates the claim based on the name.

**URL:** /bonuses/claim/{name}

e.g:

* /bonuses/claim/facebook-register

* /bonuses/claim/guest-register

**Method:** PUT

**Request Data Params:**

Required: `user_id`

Optional: -

**Response HTTP Code:**

* `201`

**Response Params:** See [Claim Bonuses properties](#claim-properties)

**Examples:**

Request:

```
PUT /bonuses/claim/facebook-register HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "user_id": 11
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "name": "Register users",
    "items":
        [
           {
            "id": 12,
            "name": "FHM01",
            "brand_id": 17,
            ....
            },
           {
            "id": 114,
            "name": "FHM0114",
            "brand_id": 17,
            ....
            }
        ]
}
```

### Item Distribution Country Group

Item Distribution Country Group resource describes where any certain item is distributed.

#### Item Distribution Country Group Properties

* `id` - Number; Instance identifier

* `name` - String; Name of the group

* `group_type` - Number; Type of the group

* `countries` - List; List of the country codes where the item is distributed, in ISO_3166-2 format

* `vendor_id` - Vendor identifier

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. The default value is true.

#### Create Item Distribution Country Group

Makes new Item distribution country group.

**URL:** /item_distribution_country_groups

**Method:** POST

**Request Data Params:**

Required: `name`, `group_type`, `countries`, `vendor_id`, `is_active`

Optional: -

**Response HTTP Code:**

* `201` - New Item Distribution Country Group data is created

**Response Params:** See [Item Distribution Country Group Properties](#item-distribution-country-group-properties)

**Examples:**

Request:

```
POST /item_distribution_country_groups HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Harper collins restricted group",
    "group_type": 1,
    "countries": ["US","CA"],
    "vendor_id": 167,
    "is_active": true
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/item_distribution_country_groups/1

{
    "id": 1,
    "name": "Harper collins restricted group",
    "group_type": 1,
    "countries": ["US","CA"],
    "vendor_id": 167,
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Item Distribution Country Group

Retrieves all of the item distribution country Groups.

**URL:** /item_distribution_country_groups

Optional:

* `vendor_id` - Number; Filter by vendor identifier

* `group_type` - Number; Filter by the group type

* `q` - Simple string query to records' string properties

* `fields` - Retrieve specific fields. See [Item Distribution Country Group](#item-distribution-country-group-properties). Default to ALL

* `expand` - Expand the result details. Default to null, the supported expands for Item Distribution Country Group are described below

    * `ext` - get all related links as object or array of objects contains identifier and name. See [Item Distribution Country Group](#item-distribution-country-group-properties)

    * `vendors` - get all related groups based on the vendor

* `is_active` - Filter by is_active status(es).

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

**Method:** GET

**Response HTTP Code:**

* `200` - Item Distribution Country Group data are retrieved and returned in body content

* `204` - No matched Item Distribution Country Group

**Response Params:** See [Item Distribution Country Group](#item-distribution-country-group-properties)

**Examples:**

Request:

```
GET /item_distribution_country_groups?is_active=true&vendor_id=2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "item_distribution_country_groups": [
        {
            "id": 3,
            "name": "Gramedia restricted group",
            "group_type": 1,
            "countries": ["MY","SG"],
            "vendor_id": 2,
            "is_active": true
        },
        {
            "id": 7,
            "name": "Some random restricted group",
            "group_type": 1,
            "countries": ["UK","CA"],
            "vendor_id": 2,
            "is_active": true
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Item Distribution Country Group

Retrieves individual group by using identifier.

**URL:** /item_distribution_country_groups/{item_distribution_country_groups_id}

Optional:

* `fields` - Retrieve specific properties. See [Item Distribution Country Group](#item-distribution-country-group-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Item Distribution Country Group data is found and returned

**Response Params:** See [Item Distribution Country Group](#item-distribution-country-group-properties)

**Examples:**

Request:

```
GET /item_distribution_country_groups/2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "name": "Something distribution group",
    "group_type": 1,
    "countries": ["ID","SG", "MY"],
    "vendor_id": 2,
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Item Distribution Country Group

Updates Item distribution country group data.

**URL:** /item_distribution_country_groups/{item_distribution_country_groups_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `group_type`, `countries`, `vendor_id`, `is_active`

Optional: -

**Response HTTP Code:**

* `200` - Item Distribution Country Group is updated

**Response Params:** See [Item Distribution Country Group](#item-distribution-country-group-properties)

**Examples:**

Request:

```
PUT /item_distribution_country_groups/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Harper collins restricted group",
    "group_type": 1,
    "countries": ["US","CA"],
    "vendor_id": 167,
    "is_active": true
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Harper collins restricted group",
    "group_type": 1,
    "countries": ["US","CA"],
    "vendor_id": 167,
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Item Distribution Country Group

Deletes Item distribution country group. This service will set `is_active` value to false.

**URL:** /item_distribution_country_groups/{item_distribution_country_groups_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Item Distribution Country Group is deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /item_distribution_country_groups/2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Revenue Share

This resource will be used to manage revenue share calculation data

#### Revenue Share Properties

* `id` - Number; Revenue share identifier

* `name` - String; Revenue share name, usually brief description of the calculation

* `description` - String; The description of how the revenue share is calculated, in some circumstances may provide some simple calculations

* `default_fixed_amount` - Number; The default amount of how the revenue share is calculated

* `default_percentage_amount` - Number; The default percentage of how the revenue share is calculated

* `fixed_amount` - Number; Fixed amount of revenue share for any related platforms

* `percentage_amount` - Number; Fixed percentage of revenue share for any related platforms

* `platform_id` - Number; Platform identifier

* `formulas` - List of the formulas; consist of `platform_id`, `fixed_amount`, `percentage_amount`

#### Create Revenue Share

**URL:** /revenue_share

**Method:** POST

**Request Data Params:**

Required: `name`

Optional: `description`, `default_fixed_amount`, `default_percentage_amount`

**Response HTTP Code:**

* `201` - New Revenue Share is Created

**Response Params:** See [Revenue Share Properties](#revenue-share-properties)

**Examples:**

Request:

```
POST /revenue_share HTTP/1.1
Accept: application/json
Content-Type: application/json
Revenue Shareization: Bearer ajksfhiau12371894^&*@&^$%

{
    "is_active": true,
    "name": "lorem ipsum",
    "description" : "lorem ipsum",
    "default_fixed_amount" : "10000",
    "default_percentage_amount" : "20",
    "formulas": [
        {
            "platform_id" : 1,
            "fixed_amount" : "200000",
            "percentage_amount" : "30"
        },
        {
            "platform_id" : 2,
            "fixed_amount" : "500000",
            "percentage_amount" : "70"
        },
        {
            "platform_id" : 3,
            "fixed_amount" : "100000",
            "percentage_amount" : "30"
        }
    ]
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "is_active": true,
    "name": "lorem ipsum",
    "description" : "lorem ipsum",
    "default_fixed_amount" : "10000",
    "default_percentage_amount" : "20",
    "formulas": [
        {
            "platform_id" : 1,
            "fixed_amount" : "200000",
            "percentage_amount" : "30"
        },
        {
            "platform_id" : 2,
            "fixed_amount" : "500000",
            "percentage_amount" : "70"
        },
        {
            "platform_id" : 3,
            "fixed_amount" : "100000",
            "percentage_amount" : "30"
        }
    ]
}

```

#### Retrieve Revenue Shares

Retrieves revenue share logs

**URL:** /revenue_share

**Method:** GET

**Response HTTP Code:**

* `200` -  Revenue Share retrieved and returned in body content

**Response Params:** See [Revenue Share Properties](#revenue-share-properties)

**Examples:**

Request:

```
GET /revenue_share HTTP/1.1
Revenue Shareization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "revenue_share": [
        {
            "id": 1,
            "is_active": true,
		    "name": "lorem ipsum",
    		"description" : "lorem ipsum",
		    "default_fixed_amount" : "10000",
    		"default_percentage_amount" : "20",
    		"formulas": [
        	{
            	"platform_id" : 1,
            	"fixed_amount" : "200000",
            	"percentage_amount" : "30"
	        },
    	    {
        	    "platform_id" : 2,
            	"fixed_amount" : "500000",
            	"percentage_amount" : "70"
        	},
        	{
            	"platform_id" : 3,
            	"fixed_amount" : "100000",
            	"percentage_amount" : "30"
        },
        {
            "id": 2,
            "is_active": true,
		    "name": "lorem ipsum",
    		"description" : "lorem ipsum",
		    "default_fixed_amount" : "10000",
    		"default_percentage_amount" : "20",
    		"formulas": [
        	{
            	"platform_id" : 1,
            	"fixed_amount" : "200000",
            	"percentage_amount" : "30"
	        },
    	    {
        	    "platform_id" : 2,
            	"fixed_amount" : "500000",
            	"percentage_amount" : "70"
        	},
        	{
            	"platform_id" : 3,
            	"fixed_amount" : "100000",
            	"percentage_amount" : "30"
        }
    ]
}
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 20
        }
    }
}

```
Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Retrieve Individual Revenue Shares

Retrieves revenue share logs

**URL:** /revenue_share/{revenue_share_id}

**Method:** GET

**Response HTTP Code:**

* `200` -  Revenue Share retrieved and returned in body content

**Response Params:** See [Revenue Share Properties](#revenue-share-properties)

**Examples:**

Request:

```
GET /revenue_share/1 HTTP/1.1
Revenue Shareization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "is_active": true,
    "name": "lorem ipsum",
    "description" : "lorem ipsum",
    "default_fixed_amount" : "10000",
    "default_percentage_amount" : "20",
    "formulas": [
        {
            "platform_id" : 1,
            "fixed_amount" : "200000",
            "percentage_amount" : "30"
        },
        {
            "platform_id" : 2,
            "fixed_amount" : "500000",
            "percentage_amount" : "70"
        },
        {
            "platform_id" : 3,
            "fixed_amount" : "100000",
            "percentage_amount" : "30"
        }
    ]
}

```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Update Revenue Share

Updates revenue share.

**URL:** /revenue_share/{revenue_share_id}

**Method:** PUT

**Request Data Params:**

Required: `name`

Optional: `description`, `default_fixed_amount`, `default_percentage_amount`

**Response HTTP Code:**

* `200` - Revenue Share updated

**Response Params:** See [Revenue Share Properties](#revenue-share-properties)

**Examples:**

Request:

```
PUT /revenue shares/1 HTTP/1.1
Content-Type: application/json
Revenue Shareization: Bearer ajksfhiau12371894^&*@&^$%

{
    "is_active": true,
    "name": "lorem ipsum",
    "description" : "lorem ipsum",
    "default_fixed_amount" : "10000",
    "default_percentage_amount" : "20",
    "formulas": [
        {
            "platform_id" : 1,
            "fixed_amount" : "200000",
            "percentage_amount" : "30"
        },
        {
            "platform_id" : 2,
            "fixed_amount" : "500000",
            "percentage_amount" : "70"
        },
        {
            "platform_id" : 3,
            "fixed_amount" : "100000",
            "percentage_amount" : "30"
        }
    ]
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "is_active": true,
    "name": "lorem ipsum",
    "description" : "lorem ipsum",
    "default_fixed_amount" : "10000",
    "default_percentage_amount" : "20",
    "formulas": [
        {
            "platform_id" : 1,
            "fixed_amount" : "200000",
            "percentage_amount" : "30"
        },
        {
            "platform_id" : 2,
            "fixed_amount" : "500000",
            "percentage_amount" : "70"
        },
        {
            "platform_id" : 3,
            "fixed_amount" : "100000",
            "percentage_amount" : "30"
        }
    ]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Revenue Share

Delete revenue share. This service will set `is_active` value to false.

**URL:** /revenue_share/{revenue_share_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Revenue Share deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /revenue_share/1 HTTP/1.1
Revenue Shareization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Payment Gateway Cost

This resource will be used to manage payment gateway cost in publisher revenue calculation

#### Payment Gateway Cost Properties

* `id` - Number; Payment gateway cost identifier

* `name` - String; Payment gateway cost name, usually brief description of the calculation

* `description` - String; The description of how the payment gateway cost is calculated, in some circumstances may provide some simple calculations

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default value is set to true

* `default_fixed_amount` - Number; The default amount of how the payment gateway cost is calculated

* `default_percentage_amount` - Number; The default percentage of how the payment gateway cost is calculated

* `default_min_amount` - Number; The default minimal amount of PG Cost

* `default_max_amount` - Number; The default maximal amount of PG Cost

* `fixed_amount` - Number; Fixed amount of payment gateway cost for any related platforms

* `percentage_amount` - Number; Fixed percentage of payment gateway cost for any related platforms

* `payment_gateway_id` - Number, Payment gateway identifier

* `max_amount` - Number, Maximal amount of PG Cost

* `min_amount` - Number, Minimal amount of PG Cost

* `formulas` - List of the formulas; consist of `payment_gateway_id`, `fixed_amount`, `percentage_amount`, `max_amount`, `min_amount`

#### Create Payment Gateway Cost

**URL:** /paymentgatewaycosts

**Method:** POST

**Request Data Params:**

Required: `payment_gateway_id`

Optional: all

**Response HTTP Code:**

* `201` - New Payment Gateway Cost is Created

**Response Params:** See [Payment Gateway Cost Properties](#payment-gateway-cost-properties)

**Examples:**

Request:

```
POST /paymentgatewaycosts HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"default_fixed_amount": 10000,
  	"default_max_amount": 9,
  	"default_min_amount": 0,
  	"default_percentage_amount": 30,
  	"description": "lorem ipsum",
  	"formulas": [
    	{
      		"fixed_amount": 10000,
      		"max_amount": 9,
    	  	"min_amount": 0,
      		"payment_gateway_id": 1,
      		"percentage_amount": 30
    	},
    	{
      		"fixed_amount": 10000,
      		"max_amount": 9,
      		"min_amount": 0,
      		"payment_gateway_id": 2,
      		"percentage_amount": 30
    	}
  	],
	"is_active": true,
  	"name": "lorem ipsum"
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
	"id": 1,
	"default_fixed_amount": 10000,
  	"default_max_amount": 9,
  	"default_min_amount": 0,
  	"default_percentage_amount": 30,
  	"description": "lorem ipsum",
  	"formulas": [
    	{
      		"fixed_amount": 10000,
      		"max_amount": 9,
    	  	"min_amount": 0,
      		"payment_gateway_id": 1,
      		"percentage_amount": 30
    	},
    	{
      		"fixed_amount": 10000,
      		"max_amount": 9,
      		"min_amount": 0,
      		"payment_gateway_id": 2,
      		"percentage_amount": 30
    	}
  	],
	"is_active": true,
  	"name": "lorem ipsum"
}

```

#### Retrieve Payment Gateway Costs

Retrieves payment gateway cost logs

**URL:** /paymentgatewaycosts

**Method:** GET

**Response HTTP Code:**

* `200` -  Payment Gateway Cost retrieved and returned in body content

**Response Params:** See [Payment Gateway Cost Properties](#payment-gateway-cost-properties)

**Examples:**

Request:

```
GET /paymentgatewaycosts HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "paymentgatewaycosts": [
        {
			"id": 1,
			"default_fixed_amount": 10000,
			"default_max_amount": 9,
			"default_min_amount": 0,
			"default_percentage_amount": 30,
			"description": "lorem ipsum",
			"formulas": [
				{
					"fixed_amount": 10000,
					"max_amount": 9,
					"min_amount": 0,
					"payment_gateway_id": 1,
					"percentage_amount": 30
				},
				{
					"fixed_amount": 10000,
					"max_amount": 9,
					"min_amount": 0,
					"payment_gateway_id": 2,
					"percentage_amount": 30
				}
			],
			"is_active": true,
			"name": "lorem ipsum"
		},
		{
			"id": 2,
			"default_fixed_amount": 10000,
			"default_max_amount": 9,
			"default_min_amount": 0,
			"default_percentage_amount": 30,
			"description": "lorem ipsum",
			"formulas": [
				{
					"fixed_amount": 10000,
					"max_amount": 9,
					"min_amount": 0,
					"payment_gateway_id": 1,
					"percentage_amount": 30
				},
				{
					"fixed_amount": 10000,
					"max_amount": 9,
					"min_amount": 0,
					"payment_gateway_id": 2,
					"percentage_amount": 30
				}
			],
			"is_active": true,
			"name": "lorem ipsum"
		}
    ]
}
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 20
        }
    }
}

```
Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Retrieve Individual Payment Gateway Costs

Retrieves payment gateway cost logs

**URL:** /paymentgatewaycosts/{paymentgatewaycosts_id}

**Method:** GET

**Response HTTP Code:**

* `200` -  Payment Gateway Cost retrieved and returned in body content

**Response Params:** See [Payment Gateway Cost Properties](#payment-gateway-cost-properties)

**Examples:**

Request:

```
GET /paymentgatewaycosts/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"id": 1,
	"default_fixed_amount": 10000,
	"default_max_amount": 9,
	"default_min_amount": 0,
	"default_percentage_amount": 30,
	"description": "lorem ipsum",
	"formulas": [
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 1,
			"percentage_amount": 30
		},
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 2,
			"percentage_amount": 30
		}
	],
	"is_active": true,
	"name": "lorem ipsum"
}

```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Update Payment Gateway Cost

Updates payment gateway cost.

**URL:** /paymentgatewaycosts/{paymentgatewaycost_id}

**Method:** PUT

**Request Data Params:**

Required: `payment_gateway_id`

Optional: all

**Response HTTP Code:**

* `200` - Payment Gateway Cost updated

**Response Params:** See [Payment Gateway Cost Properties](#payment-gateway-cost-properties)

**Examples:**

Request:

```
PUT /payment gateway costs/1 HTTP/1.1
Content-Type: application/json
Authorizatioin: Bearer ajksfhiau12371894^&*@&^$%

{
	"default_fixed_amount": 10000,
	"default_max_amount": 9,
	"default_min_amount": 0,
	"default_percentage_amount": 30,
	"description": "lorem ipsum",
	"formulas": [
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 1,
			"percentage_amount": 30
		},
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 2,
			"percentage_amount": 30
		}
	],
	"is_active": true,
	"name": "lorem ipsum"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"id": 1,
	"default_fixed_amount": 10000,
	"default_max_amount": 9,
	"default_min_amount": 0,
	"default_percentage_amount": 30,
	"description": "lorem ipsum",
	"formulas": [
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 1,
			"percentage_amount": 30
		},
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 2,
			"percentage_amount": 30
		}
	],
	"is_active": true,
	"name": "lorem ipsum"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Payment Gateway Cost

Delete payment gateway cost. This service will set `is_active` value to false.

**URL:** /paymentgatewaycosts/{paymentgatewaycosts_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Payment Gateway Cost deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /paymentgatewaycosts/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Vendor Payment Gateway Cost

This resource will be used to manage vendor payment gateway cost in publisher revenue calculation

#### Vendor Payment Gateway Cost Properties

* `id` - Number; Vendor PG cost identifier

* `vendor_id` - Number; Vendor identifier

* `paymentgatewaycost_id` - Number; PG Cost identifier

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default value is set to true

* `valid_from` - String; Datetime in UTC format, the start date of PG Cost validity

* `valid_to` - String; Datetime in UTC format, the end date of PG Cost validity

* `created` - String; Datetime in UTC the banner is created

* `modified` - String; Datetime in UTC the banner is last modified

#### Create Vendor Payment Gateway Cost

**URL:** /vendor_payment_gateway_costs

**Method:** POST

**Request Data Params:**

Required: all

Optional: -

**Response HTTP Code:**

* `201` - New Vendor Payment Gateway Cost is Created

**Response Params:** See [Vendor Payment Gateway Cost Properties](#vendor-payment-gateway-cost-properties)

**Examples:**

Request:

```
POST /vendor_payment_gateway_costs HTTP/1.1
Accept: application/json
Content-Type: application/json
Vendor Payment Gateway Costization: Bearer ajksfhiau12371894^&*@&^$%

{
	"default_fixed_amount": 10000,
  	"default_max_amount": 9,
  	"default_min_amount": 0,
  	"default_percentage_amount": 30,
  	"description": "lorem ipsum",
  	"formulas": [
    	{
      		"fixed_amount": 10000,
      		"max_amount": 9,
    	  	"min_amount": 0,
      		"payment_gateway_id": 1,
      		"percentage_amount": 30
    	},
    	{
      		"fixed_amount": 10000,
      		"max_amount": 9,
      		"min_amount": 0,
      		"payment_gateway_id": 2,
      		"percentage_amount": 30
    	}
  	],
	"is_active": true,
  	"name": "lorem ipsum"
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
	"id": 1,
	"default_fixed_amount": 10000,
  	"default_max_amount": 9,
  	"default_min_amount": 0,
  	"default_percentage_amount": 30,
  	"description": "lorem ipsum",
  	"formulas": [
    	{
      		"fixed_amount": 10000,
      		"max_amount": 9,
    	  	"min_amount": 0,
      		"payment_gateway_id": 1,
      		"percentage_amount": 30
    	},
    	{
      		"fixed_amount": 10000,
      		"max_amount": 9,
      		"min_amount": 0,
      		"payment_gateway_id": 2,
      		"percentage_amount": 30
    	}
  	],
	"is_active": true,
  	"name": "lorem ipsum"
}

```

#### Retrieve Vendor Payment Gateway Costs

Retrieves vendor payment gateway cost logs

**URL:** /vendor_payment_gateway_costs

**Method:** GET

**Response HTTP Code:**

* `200` -  Vendor Payment Gateway Cost retrieved and returned in body content

**Response Params:** See [Vendor Payment Gateway Cost Properties](#vendor-payment-gateway-cost-properties)

**Examples:**

Request:

```
GET /vendor_payment_gateway_costs HTTP/1.1
Vendor Payment Gateway Costization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "vendor_payment_gateway_costs": [
        {
			"id": 1,
			"default_fixed_amount": 10000,
			"default_max_amount": 9,
			"default_min_amount": 0,
			"default_percentage_amount": 30,
			"description": "lorem ipsum",
			"formulas": [
				{
					"fixed_amount": 10000,
					"max_amount": 9,
					"min_amount": 0,
					"payment_gateway_id": 1,
					"percentage_amount": 30
				},
				{
					"fixed_amount": 10000,
					"max_amount": 9,
					"min_amount": 0,
					"payment_gateway_id": 2,
					"percentage_amount": 30
				}
			],
			"is_active": true,
			"name": "lorem ipsum"
		},
		{
			"id": 2,
			"default_fixed_amount": 10000,
			"default_max_amount": 9,
			"default_min_amount": 0,
			"default_percentage_amount": 30,
			"description": "lorem ipsum",
			"formulas": [
				{
					"fixed_amount": 10000,
					"max_amount": 9,
					"min_amount": 0,
					"payment_gateway_id": 1,
					"percentage_amount": 30
				},
				{
					"fixed_amount": 10000,
					"max_amount": 9,
					"min_amount": 0,
					"payment_gateway_id": 2,
					"percentage_amount": 30
				}
			],
			"is_active": true,
			"name": "lorem ipsum"
		}
    ]
}
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 20
        }
    }
}

```
Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Retrieve Individual Vendor Payment Gateway Costs

Retrieves vendor payment gateway cost logs

**URL:** /vendor_payment_gateway_costs/{vendor_payment_gateway_costs_id}

**Method:** GET

**Response HTTP Code:**

* `200` -  Vendor Payment Gateway Cost retrieved and returned in body content

**Response Params:** See [Vendor Payment Gateway Cost Properties](#payment-gateway-cost-properties)

**Examples:**

Request:

```
GET /vendor_payment_gateway_costs/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"id": 1,
	"default_fixed_amount": 10000,
	"default_max_amount": 9,
	"default_min_amount": 0,
	"default_percentage_amount": 30,
	"description": "lorem ipsum",
	"formulas": [
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 1,
			"percentage_amount": 30
		},
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 2,
			"percentage_amount": 30
		}
	],
	"is_active": true,
	"name": "lorem ipsum"
}

```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Update Vendor Payment Gateway Cost

Updates vendor payment gateway cost.

**URL:** /vendor_payment_gateway_costs/{paymentgatewaycost_id}

**Method:** PUT

**Request Data Params:**

Required: all

Optional: -

**Response HTTP Code:**

* `200` - Vendor Payment Gateway Cost updated

**Response Params:** See [Vendor Payment Gateway Cost Properties](#payment-gateway-cost-properties)

**Examples:**

Request:

```
PUT /vendor payment gateway costs/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"default_fixed_amount": 10000,
	"default_max_amount": 9,
	"default_min_amount": 0,
	"default_percentage_amount": 30,
	"description": "lorem ipsum",
	"formulas": [
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 1,
			"percentage_amount": 30
		},
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 2,
			"percentage_amount": 30
		}
	],
	"is_active": true,
	"name": "lorem ipsum"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"id": 1,
	"default_fixed_amount": 10000,
	"default_max_amount": 9,
	"default_min_amount": 0,
	"default_percentage_amount": 30,
	"description": "lorem ipsum",
	"formulas": [
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 1,
			"percentage_amount": 30
		},
		{
			"fixed_amount": 10000,
			"max_amount": 9,
			"min_amount": 0,
			"payment_gateway_id": 2,
			"percentage_amount": 30
		}
	],
	"is_active": true,
	"name": "lorem ipsum"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Vendor Payment Gateway Cost

Delete vendor payment gateway cost. This service will set `is_active` value to false.

**URL:** /vendor_payment_gateway_costs/{vendor_payment_gateway_costs_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Vendor Payment Gateway Cost deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /vendor_payment_gateway_costs/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

---

## OFFERS, PLATFORMS, TIERS, CAMPAIGNS, AND DISCOUNTS

### Offers

Offer resource contains base pricings, relation to tier-based pricings (See [iOS Tiers](#offer-properties), [Android Tiers](#android-tiers), [Windows Phone Tiers](#windows-phone-tiers)), and relation to items and brands.

There's 3 types of existing offers:

1. Single

    Single type of offer, this offer only linked to an item. The linked item, and its related extra item(s), will delivered to user when proceed with this offer.

2. Subscription

    Subscription type of offer, linked to the list of brands. If purchased this offer, user will eligible for forward issue of the subscribed brand(s). Latest existing item of the brand will be delivered to user. New latest item of the brand will be delivered to user as long as the subcription still valid based on unit or time.

    Subcription has 2 required properties, quantity and quantity_unit. This properties will define the validity of the owned subscription later.

3. Bundle

    Bundle type of offer, linked to a list of items that will delivered to user when purchased this offer. Note: Further implementation needed to automate the earning and revenue shares calculation.

Note: There's a list to overrides platform specific's offers. This platform's offers can be not available for some platforms, means it didn't sell on that platform, excepts for platform that retrieve from base offers.

#### Offer Properties

* `id` - Number; Instance identifier

* `name` - String; Offer name See [Offer Name Format](#offer-name-format)

* `long_name` - String; Offer complete name to identify the item name

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `description` - String; Offer description

* `offer_type_id` - Number; Offer type identifier. Refer to [Offer Types](#offer-types). Unchangeable.

* `offer_status` - Number; Offer status

    * 1: NEW - New created offer; State: not active

    * 2: WAITING FOR REVIEW - Offer waiting for reviewer's review. This status used to shows to vendor's users; State: not active

    * 3: IN REVIEW - Offer in review by reviewer. This status used to shows to vendor's users; State: not active

    * 4: REJECTED - Offer in rejected by reviewer. This status used to shows to vendor's users; State: not active

    * 5: APPROVED - Offer in approved by reviewer. This status used to shows to vendor's users; State: not active

    * 6: PREPARE FOR SALE - Offer preparing to store. This status used to shows to vendor's users; State: not active

    * 7: READY FOR SALE - Offer ready for sale on store; State: active

    * 8: NOT FOR SALE - Offer taken down from store. All purchased offer will still valid, but all auto-renewal by internal system will be invalidated (For products that must removed from payment gateway side, please refer to each payment gateway's details); State: not active

* `quantity` - Number; subcribed quantity, for unit-based subcription type. For unit-based with multiple brands, this quantity will applied to each brands.

* `quantity_unit` - Number; Offer quantity unit type for subcription, related to `quantity` property

    * 1: UNIT - For unit-based subscription, the total unit will be stored in user's owned subscription. Most of the magazines use this type of unit

    * 2: DAY - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. This unit rarely used

    * 3: WEEK - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. This unit rarely used

    * 4: MONTH - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. Most of the newspaper use this type of unit

* `is_free` - Boolean; Flag for free offer. If true, other prices information will be overriden. Default to false

* `point_reward` - Number; Point rewarded to user (acquired by user) if purchased this offer

* `items` - Array; Array of items related to this offer, used for single or bundle type. Single type offer will has single item, bundle type will has list of items

* `brands` - Array; Array of brands related to this offer, used for subscription type. For current supported business case, the subscription only has single brand to deliver

* `exclusive_clients` - Array; Array of clients exclusively offered this offer

* `offer_code` - String; Offer code, for single type corresponding to edition_code, for subscription/bundle type corresponding to bundle_subscription_detail_code of previous system **TODO: will be dropped after translation layer dropped. should not be used in new apps. correspondent to edition_code, bundle_subscription_detail_code**

* `price_usd` - String; Float value for base price in USD. Must be set to 0 for free offers

* `price_idr` - String; Float value for base price in IDR. Must be set to 0 for free offers

* `price_point` - String; Float value for base price in PTS (SCOOP Point). Must be set to 0 for free offers

* `aggr_price_usd` - String; Float value for aggregator price in USD. Must be greater than `base_usd_price`

* `aggr_price_idr` - String; Float value for aggregator price in IDR. Must be greater than `base_idr_price`

* `aggr_price_point` - String; Float value for aggregator price in PTS (SCOOP Point). Must be greater than `price_point`

* `vendor_price_usd` - String; Float value for retail price in USD. Must be set to 0 for free offers

* `vendor_price_idr` - String; Float value for retail price in IDR. Must be set to 0 for free offers

* `vendor_price_point` - String; Float value for retail price in PTS (SCOOP Point). Must be set to 0 for free offers

* `discount_usd` - String; Float value for base discount in USD. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

* `discount_idr` - String; Float value for base discount in IDR. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

* `discount_point` - String; Float value for base discount in PTS (SCOOP Point). Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

* `discount_tag` - String; Discount short tag. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

* `discount_name` - String; Discount name. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

* `printed_price` - String; The printed-version price

* `printed_currency_id` - Number; Currency used in printed-version price. 1 for USD, 2 for IDR

* `platforms_offers` - Array; List of the override offer for the platforms. On specific platform, this object's properties wil overrides offer's (base) prices

    * `platform_id` - Number; Platform identifier of this platform specific offer. See [Platforms](#platforms)

    * `price_usd` - String; Float value for base price in USD. Must set to 0 for free offer

    * `price_idr` - String; Float value for base price in IDR. Must set to 0 for free offer

    * `price_point` - String; Float value for base price in PTS (SCOOP Point). Must set to 0 for free offer

    * `tier_id` - Number; Tier identifier of the price

    * `tier_code` - String; Tier code of the price

    * `discount_usd` - String; Float value for base discount in USD. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

    * `discount_idr` - String; Float value for base discount in IDR. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

    * `discount_point` - String; Float value for base discount in PTS (SCOOP Point). Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

    * `discount_tier_id` - Number; Tier identifier of the price when discount

    * `discount_tier_code` - String; Tier code of the price when discount

    * `discount_tag` - String; Discount short tag. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

    * `discount_name` - String; Discount name. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

####Offer Name Format

Offer name format is different for each offer type, in general as below:

1. Single type name: {item_name} + " - " + {info}, e.g., "Tempo Ed-34 - Single Edition"

2. Subscription type name: {brand_name} + " - " + {info}, e.g., "Tempo - 53 Editions / 12 Months"

3. Bundle type name: custom as the bundle name, e.g., "Special Bundle 2013 Autocar Magazines"

Note:

* Offer's name will be passed directly as order line's name

* Client side needs to remove item_name or brand_name, then trim it, depends on how the client want to shows it. e.g., "Tempo Ed-34 - Single Edition" shows as "Single Edition" on UI side

#### Create Offer

Create new offer.

**URL:** /offers

**Method:** POST

Note: Each offer and platform can only has a single `platforms_offers` record

**Request Data Params:**

Required: `offer_type_id`, `offer_status`, `name`, `sort_priority`, `is_active`, `price_usd`, `price_idr`, `price_point`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `exclusive_clients`, `aggr_price_usd`, `aggr_price_idr`, `aggr_price_point`

For single and bundle type, the required params are: `items`

For subscription type, the required params are: `quantity`, `quantity_unit`, `brands`

For `platforms_offers`, the required params are: `platform_id`, `price_usd`, `price_idr`, `price_point`

**Response HTTP Code:**

* `201` - New vendor created

**Response Params:** See [Offer Properties](#offer-properties)

**Examples:**

Request:

```
POST /offers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Single issue",
    "long_name": "Judul Majalah / ED 12",
    "sort_priority": 1,
    "is_active": true,
    "offer_type_id": 1,
    "offer_status": 7,
    "is_free": false,
    "point_reward": 99,
    "printed_price": "100000",
    "printed_currency_id: 2,
    "items": [
        2345
    ],
    "exclusive_clients": [],
    "offer_code": "ID_TEMP2014ED452",
    "price_usd": "3.20",
    "price_idr": "32000",
    "price_point": "320",
    "aggr_price_usd": "5.99",
    "aggr_price_idr": "59000",
    "aggr_price_point": "590",
    "vendor_price_usd": "3.20",
    "vendor_price_idr": "32000",
    "vendor_price_point": "320",
    "platforms_offers": [
        {
            "platform_id": 1,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 123
        },
        {
            "platform_id": 2,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 124
        },
        {
            "platform_id": 3,
            "price_usd": "3.49",
            "price_idr": "35000",
            "price_point": "320",
            "tier_id": 125
        }
    ]
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/offers/1232

{
    "id": 1232,
    "name": "Single issue",
    "long_name": "Judul Majalah / ED 12",
    "sort_priority": 1,
    "is_active": true,
    "offer_type_id": 1,
    "offer_status": 7,
    "is_free": false,
    "point_reward": 99,
    "printed_price": "100000",
    "printed_currency_id: 2,
    "items": [
        2345
    ],
    "exclusive_clients": [],
    "offer_code": "ID_TEMP2014ED452",
    "price_usd": "3.20",
    "price_idr": "32000",
    "price_point": "320",
    "aggr_price_usd": "5.99",
    "aggr_price_idr": "59000",
    "aggr_price_point": "590",
    "vendor_price_usd": "3.20",
    "vendor_price_idr": "32000",
    "vendor_price_point": "320",
    "discount_price_usd": null,
    "discount_price_idr": null,
    "discount_price_point": null,
    "discount_tag": null,
    "discount_name": null,
    "platforms_offers": [
        {
            "offer_id": 1232,
            "platform_id": 1,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 123,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        },
        {
            "offer_id": 1232,
            "platform_id": 2,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 124,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        },
        {
            "offer_id": 1232,
            "platform_id": 3,
            "price_usd": "3.49",
            "price_idr": "35000",
            "price_point": "320",
            "tier_id": 125,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        }
    ]
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

#### Retrieve Offers

Retrieve offers.

**URL:** /offers

Optional:

* `offer_id` - Provides offer identifier(s) to be retrieved. Response will only be returned if the ID is available in the system

* `is_active` - Filter by the status. Response will only returns if there is any status that match

* `fields` - Retrieve specific fields. See [Offer Properties](#vendor-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `offer_type_id` - Provided offer type identifier(s). Response will returns results (if any matched)

* `related_item_id` - Provided item identifier(s) to relates. Response will returns offers related to items

* `related_brand_id` - Provided brand identifier(s) to relates. Response will returns offers related to brands

* `expand` - Expand the result details. Default to null, the supported expand for offers are as below

    * ext: get all related links as object or array of objects contains identifier and name. See [Offer Properties](#offer-properties)

    * offer_type: get related offer_type as object (this expand value is covered in "ext")

    * platforms_offers: get platform's offers as an array (this expand value is covered in "ext")

    * buffet_details: If the offer is for a buffet (offer_type_id = 4), this will include the relevant buffet information (this expand value is covered in "ext")

* `platform_id` - Provided platform identifier(s). Response will returns platform tier information (if any matched). To get this in results, `expand` must contains platforms_offers.

**Method:** GET

**Response HTTP Code:**

* `200` - Offers retrieved and returned in body content

* `204` - No matched offer

**Response Params:** See [Offer Properties](#offer-properties)

**Examples:**

Request:

```
GET /offers?related_item_id=345&related_brand_id=56&platform_id=1,2&offer_id=1,2&is_active=true&expand=ext HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response without expand:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "offers": [
        {
            "id": 1232,
            "name": "Single issue",
            "long_name": "Judul Majalah / ED 12",
            "sort_priority": 1,
            "is_active": true,
            "offer_type_id": 1,
            "printed_price": "100000",
            "printed_currency_id: 2,
            "items": [
                2345
            ],
            "offer_status": 7,
            "is_free": false,
            "point_reward": 99,
            "exclusive_clients": [],
            "offer_code": "ID_TEMP2014ED452",
            "price_usd": "3.20",
            "price_idr": "32000",
            "price_point": "320",
            "aggr_price_usd": "5.99",
            "aggr_price_idr": "59000",
            "aggr_price_point": "590",
            "vendor_price_usd": "3.20",
            "vendor_price_idr": "32000",
            "vendor_price_point": "320",
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tag": null,
            "discount_name": null,
        },
        {
            "id": 231,
            "name": "3 Months Subscription",
            "long_name": "Judul Majalah 3 Months Subscription",
            "sort_priority": 1,
            "is_active": true,
            "offer_type_id": 2,
            "offer_status": 7,
            "quantity": 14,
            "quantity_unit": 1,
            "is_free": false,
            "point_reward": 99,
            "printed_price": "100000",
            "printed_currency_id: 2,
            "brands": [
                56
            ],
            "exclusive_clients": [],
            "offer_code": "ID_SUBTEMP14ED3MTH",
            "price_usd": "24.99",
            "price_idr": "250000",
            "price_point": "2500",
            "aggr_price_usd": "49.99",
            "aggr_price_idr": "500000",
            "aggr_price_point": "5000",
            "vendor_price_usd": "3.20",
            "vendor_price_idr": "32000",
            "vendor_price_point": "320",
            "discount_price_usd": "5.00",
            "discount_price_idr": "50000",
            "discount_price_point": "50",
            "discount_tag": "25% OFF",
            "discount_name": "Special promo 25% Off",
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 20
        }
    }
}
```

Success response with expand=ext:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "offers": [
        {
            "id": 1232,
            "name": "May buffet",
            "long_name": "SCOOP Premium",
            "sort_priority": 1,
            "is_active": true,
            "offer_type_id": 4,
            "offer_type": {
                "id": 4,
                "name": "Buffet"
            }
            "offer_status": 7,
            "is_free": false,
            "point_reward": 99,
            "printed_price": "100000",
            "printed_currency_id: 2,
            "items": [
                {
                    "id": 2345,
                    "name": "Tempo Ed 32"
                }
            ],
            "buffet_details": {
                "id": 66,
                "available_from": "2015-05-01T00:00:00+00:00",
                "available_until": null,
                "buffet_duration": "P30D0S"
            }
            "exclusive_clients": [],
            "offer_code": "ID_TEMP2014ED452",
            "price_usd": "3.20",
            "price_idr": "32000",
            "price_point": "320",
            "aggr_price_usd": "5.99",
            "aggr_price_idr": "59000",
            "aggr_price_point": "590",
            "vendor_price_usd": "3.20",
            "vendor_price_idr": "32000",
            "vendor_price_point": "320",
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tag": null,
            "discount_name": null,
            "platforms_offers": [
                {
                    "offer_id": 1232,
                    "platform_id": 1,
                    "price_usd": "3.99",
                    "price_idr": "39000",
                    "price_point": "320",
                    "tier_id": 123,
                    "tier_code": null,
                    "discount_price_usd": null,
                    "discount_price_idr": null,
                    "discount_price_point": null,
                    "discount_tier_id": null,
                    "discount_tier_code": null,
                    "discount_tag": null,
                    "discount_name": null
                },
                {
                    "offer_id": 1232,
                    "platform_id": 2,
                    "price_usd": "3.99",
                    "price_idr": "39000",
                    "price_point": "320",
                    "tier_id": 124,
                    "tier_code": null,
                    "discount_price_usd": null,
                    "discount_price_idr": null,
                    "discount_price_point": null,
                    "discount_tier_id": null,
                    "discount_tier_code": null,
                    "discount_tag": null,
                    "discount_name": null
                }
            ]
        },
        {
            "id": 231,
            "name": "3 Months Subscription",
            "long_name": "Judul Majalah 3 Months Subscription",
            "sort_priority": 1,
            "is_active": true,
            "offer_type_id": 2,
            "offer_type": {
                "id": 1,
                "name": "Subscription"
            }
            "offer_status": 7,
            "quantity": 14,
            "quantity_unit": 1,
            "is_free": false,
            "point_reward": 99,
            "printed_price": "100000",
            "printed_currency_id: 2,
            "brands": [
                {
                    "id": 56,
                    "name": "Tempo"
                }
            ],
            "exclusive_clients": [],
            "offer_code": "ID_SUBTEMP14ED3MTH",
            "price_usd": "24.99",
            "price_idr": "250000",
            "price_point": "2500",
            "aggr_price_usd": "49.99",
            "aggr_price_idr": "500000",
            "aggr_price_point": "5000",
            "vendor_price_usd": "3.20",
            "vendor_price_idr": "32000",
            "vendor_price_point": "320",
            "discount_price_usd": "5.00",
            "discount_price_idr": "50000",
            "discount_price_point": "50",
            "discount_tag": "25% OFF",
            "discount_name": "Special promo 25% Off",
            "platforms_offers": [
                {
                    "offer_id": 1232,
                    "platform_id": 1,
                    "price_usd": "3.99",
                    "price_idr": "39000",
                    "price_point": "320",
                    "tier_id": 123,
                    "tier_code": null,
                    "discount_price_usd": "2.99",
                    "discount_price_idr": "29000",
                    "discount_price_point": "290",
                    "discount_tag": "25% OFF",
                    "discount_name": "Special promo 25% Off",
                    "discount_tier_id": null,
                    "discount_tier_code": null
                },
                {
                    "offer_id": 1232,
                    "platform_id": 2,
                    "price_usd": "3.99",
                    "price_idr": "39000",
                    "price_point": "320",
                    "tier_id": 124,
                    "tier_code": null,
                    "discount_price_usd": "2.99",
                    "discount_price_idr": "29000",
                    "discount_price_point": "290",
                    "discount_tag": "25% OFF",
                    "discount_name": "Special promo 25% Off",
                    "discount_tier_id": null,
                    "discount_tier_code": null
                }
            ]
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 20
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Offer

Retrieve individual offer using identifier.

**URL:** /offers/{offer_id}

Optional:

* `fields` - Retrieve specific properties. See [Offer Properties](#offer-properties). Default to ALL

* `platform_id` - Provided platform identifier(s). Response will returns platform tier information (if any matched)

**Method:** GET

**Response HTTP Code:**

* `200` - Offer found and returned

**Response Params:** See [Offer Properties](#offer-properties)

**Examples:**

Request:

```
GET /offers/5?platform_id=1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 5,
    "name": "Travelounge 2013 Bundle",
    "sort_priority": 1,
    "is_active": true,
    "offer_type_id": 3,
    "offer_status": 7,
    "is_free": false,
    "point_reward": 59,
    "printed_price": "100000",
    "printed_currency_id: 2,
    "items": [
        {
            "id": 345,
            "name": "Travelounge Jan 2013"
        },
        {
            "id": 457,
            "name": "Travelounge Apr 2013"
        },
        {
            "id": 521,
            "name": "Travelounge Jul 2013"
        },
        {
            "id": 631,
            "name": "Travelounge Oct 2013"
        }
    ],
    "exclusive_clients": [],
    "offer_code": "ID_TRVLNG2013BUNDL",
    "price_usd": "4.99",
    "price_idr": "49000",
    "price_point": "490",
    "aggr_price_usd": "9.99",
    "aggr_price_idr": "99000",
    "aggr_price_point": "990",
    "vendor_price_usd": "3.20",
    "vendor_price_idr": "32000",
    "vendor_price_point": "320",
    "discount_price_usd": "2.49",
    "discount_price_idr": "24500",
    "discount_price_point": null,
    "discount_tag": "50% OFF",
    "discount_name": "Special promo 50% Off",
    "platforms_offers": [
        {
            "offer_id": 5,
            "platform_id": 1,
            "price_usd": "4.99",
            "price_idr": "49000",
            "price_point": "490",
            "tier_id": 123,
            "tier_code": ".c.USD.4.99",
            "discount_price_usd": "2.99",
            "discount_price_idr": "29000",
            "discount_price_point": null,
            "discount_tag": "50% OFF",
            "discount_name": "Special promo 50% Off",
            "discount_tier_id": 764,
            "discount_tier_code": ".c.USD.2.99"
        }
    ]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Offer

Update offer.

**URL:** /offers/{offer_id}

**Method:** PUT

**Request Data Params:**

Required: `offer_type_id`, `offer_status`, `name`, `sort_priority`, `is_active`, `price_usd`, `price_idr`, `price_point`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `exclusive_clients`, `aggr_price_usd`, `aggr_price_idr`, `aggr_price_point`

For single and bundle type, the required params are: `items`

For subscription type, the required params are: `quantity`, `quantity_unit`, `brands`

For `platforms_offers`, the required params are: `platform_id`, `price_usd`, `price_idr`, `price_point`

**Response HTTP Code:**

* `200` - Offer updated with no new platforms_offers record created

* `201` - Offer updated with new platforms_offers record created

**Response Params:** See [Offer Properties](#offer-properties)

**Examples:**

Request:

```
PUT /offers/5 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Single issue",
    "sort_priority": 1,
    "is_active": true,
    "offer_type_id": 1,
    "offer_status": 7,
    "is_free": false,
    "point_reward": 99,
    "printed_price": "100000",
    "printed_currency_id: 2,
    "items": [
        2345
    ],
    "exclusive_clients": [],
    "offer_code": "ID_TEMP2014ED452",
    "price_usd": "3.20",
    "price_idr": "32000",
    "price_point": "320",
    "aggr_price_usd": "5.99",
    "aggr_price_idr": "59000",
    "aggr_price_point": "590",
    "vendor_price_usd": "3.20",
    "vendor_price_idr": "32000",
    "vendor_price_point": "320",
    "platforms_offers": [
        {
            "platform_id": 1,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 123
        },
        {
            "platform_id": 2,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 124
        },
        {
            "platform_id": 3,
            "price_usd": "3.49",
            "price_idr": "35000",
            "price_point": "320",
            "tier_id": 125
        }
    ]
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 5,
    "name": "Single issue",
    "sort_priority": 1,
    "is_active": true,
    "offer_type_id": 1,
    "offer_status": 7,
    "is_free": false,
    "point_reward": 99,
    "printed_price": "100000",
    "printed_currency_id: 2,
    "items": [
        2345
    ],
    "exclusive_clients": [],
    "offer_code": "ID_TEMP2014ED452",
    "price_usd": "3.20",
    "price_idr": "32000",
    "price_point": "320",
    "aggr_price_usd": "5.99",
    "aggr_price_idr": "59000",
    "aggr_price_point": "590",
    "vendor_price_usd": "3.20",
    "vendor_price_idr": "32000",
    "vendor_price_point": "320",
    "discount_price_usd": null,
    "discount_price_idr": null,
    "discount_price_point": null,
    "discount_tag": null,
    "discount_name": null,
    "platforms_offers": [
        {
            "offer_id": 5,
            "platform_id": 1,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 123,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        },
        {
            "offer_id": 5,
            "platform_id": 2,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 124,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        },
        {
            "offer_id": 5,
            "platform_id": 3,
            "price_usd": "3.49",
            "price_idr": "35000",
            "price_point": "320",
            "tier_id": 125,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        }
    ]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Offer

Delete offer. This service will set `is_active` value to false.

**URL:** /offers/{offer_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Offer deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /offers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Buffets

Buffets are a type of offer where the user has access to a set of Items,
or all Items within a given Brand, for a limited amount of time.  After
that amount of time expires for a User, their access to those Items will be
revoked (this is different to a Subscription where a user will always have
access to the Items acquired as part of their subscription, even when that
subscription has expired).


#### Buffet Properties

Buffets have only 4 properties.

* `available_from` - ISO 8601 Date/Time;  The earliest date/time that a user may puchase this buffet.  If null, the buffet is available immediately.
* `available_until` - ISO 8601 Date/Time;  The last date/time that users may purchase access to this buffet.  If null, the buffet has no limit to it's purchase availability.
* `buffet_duration` - ISO 8601 Duration **(required)**;  The amount of time, after the date of purchase, that buffet items will be available to a user.
* `offer` - A single offer, with all it's properties (include offer_platforms), nested within this buffet (buffets only ever have a single offer).  See Offer Properties for the exact details.

#### Create Buffet

**URL**: /v1/offers/buffets

**Method**: POST

**Success Response Codes**:

* HTTP 201 CREATED

**Success Response Data**: See *Retrieve Individual Buffet* below.

**Error Response Codes**:

* HTTP 400 BAD REQUEST - Invalid or missing data in JSON request body
* HTTP 401 UNAUTHORIZED - Bad Authentication header

```
Content-type: application/json
Accept: application/json
Authorization: Bearer {your-scoopcas-token}
```

``` json
{
    "available_from": "2015-07-01T00:00:00Z",
    "available_until": "2015-07-31T23:59:59Z",
    "buffet_duration": "P1M",
    "offer": {
        "name": "Scoop July Buffet",
        "offer_status": 7,
        "sort_priority": 1,
        "is_active": true,
        "offer_type_id": 4,
        "price_usd": 14.44,
        "price_idr": 144440,
        "price_point": 0,
        "brands": [5,12,36],
        "items": [20051, 20052],
        "platforms_offers": [
            {
                "platform_id": 3,
                "tier_id": 3,
                "currency": "USD",
                "price_usd": 7.99,
                "price_idr": 79000,
                "price_point": 0
            }
        ]
    }
}
```

#### Retrieve Buffet

**URL**: /v1/offers/buffets

**Method**: GET

**Success Response Codes**:

* HTTP 200 OK

**Error Response Codes**:

* HTTP 400 BAD REQUEST - Invalid request data
* HTTP 401 UNAUTHORIZED - Bad Authentication header

```
Accept: application/json
Authorization: Bearer {your-scoopcas-token}
```

``` json
{
    "buffets": [
        {
            "available_from": "2015-05-01T00:00:00+00:00",
            "available_until": "2015-05-01T00:00:00+00:00",
            "buffet_duration": "P30DT0S",
            "id": 1,
            "offer": {
                "aggr_price_idr": 0,
                "aggr_price_point": 0,
                "aggr_price_usd": null,
                "brands": [
                    12,
                    5,
                    36
                ],
                "discount_id": null,
                "discount_name": null,
                "discount_price_idr": 0,
                "discount_price_point": 0,
                "discount_price_usd": null,
                "discount_tag": null,
                "exclusive_clients": null,
                "id": 41975,
                "image_highres": null,
                "image_normal": null,
                "is_active": true,
                "is_discount": false,
                "is_free": false,
                "item_code": null,
                "items": [
                    20051,
                    20052
                ],
                "name": "MAY BUFFET OFFER",
                "offer_code": null,
                "offer_status": 7,
                "offer_type_id": 4,
                "platforms_offers": [],
                "price_idr": 144440,
                "price_point": 0,
                "price_usd": 14.44,
                "sort_priority": 1,
                "vendor_price_idr": 0,
                "vendor_price_point": 0,
                "vendor_price_usd": null
            }
        },
        " ... Additional objects with identical format to above ..."
    ],
    "metadata": {
        "resultset": {
            "count": 5,
            "limit": 20,
            "offset": 0
        }
    }
}

```

#### Retrieve Individual Buffet

**URL**: /v1/offers/buffets/{buffet-id}

Similar to an individual JSON object from the response above, with idential
request headers.

#### Update Buffet

**URL**: /v1/offers/buffets/{buffet-id}

**Method**: PUT

All other properties are idential to Create Buffet

#### Delete Buffet

This performs a soft-delete on a buffet object by setting it's 'is_active'
property to false:

**URL**: /v1/offers/buffets/{buffet-id}

**Method**: DELETE

**Success Response Codes**:

* HTTP 204 NO CONTENT

```
Authorization: Bearer {your-scoopcas-token}
```

This method does not accept an HTTP body.


### Offer Types

Offer type resource used to identify system's supported offer types.

Current existing offer types are:

1. Single: Single item offer, this offer contains item that already existing in the system

2. Subscription: Subscription offer, this offer contains forward contract of certain brand existing in the system

3. Bundle: Bundled items offer, this offer contains items that already existing in the system

Note: New offer type mostly will has specific use-cases come along with it. So this resource records only for metadata only, the use-cases implementation will be vary.

#### Offer Type Properties

* `id` - Number; Instance identifier

* `name` - String; Unique offer type name

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `slug` - String, Unique; Offer type slug for SEO

* `meta` - String; Offer type meta for SEO

* `description` - String; Offer type description

#### Create Offer Type

Create new offer type.

**URL:** /offer_types

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `201` - New offer type created

**Response Params:** See [Offer Type Properties](#offer-type-properties)

**Examples:**

Request:

```
POST /offer_types HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Single",
    "sort_priority" : 1,
    "is_active": true,
    "slug": "single"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/offer_types/1

{
    "id": 1,
    "name": "Single",
    "sort_priority" : 1,
    "is_active": true,
    "slug": "single",
    "meta": null,
    "description": null
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Offer Types

Retrieve offer types.

**URL:** /offer_types

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Item Type Properties](#item-type-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Offer types retrieved and returned in body content

* `204` - No matched offer type

**Response Params:** See [Offer Type Properties](#offer-type-properties)

**Examples:**

Request:

```
GET /offer_types?fields=id,name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "offer_types": [
        {
            "id": 1,
            "name": "Single"
        },
        {
            "id": 2,
            "name": "Subscription"
        },
        {
            "id": 3,
            "name": "Bundle"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 3,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Offer Type

Retrieve individual offer type using identifier.

**URL:** /offer_types/{offer_type_id}

Optional:

* `fields` - Retrieve specific properties. See [Offer Type Properties](#offer-type-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Offer type found and returned

**Response Params:** See [Offer Type Properties](#offer-type-properties)

**Examples:**

Request:

```
GET /offer_types/1?fields=id,name,sort_priority,slug,meta,description,is_active HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Single",
    "sort_priority": 1,
    "slug": "single",
    "meta": null,
    "description": null,
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Offer Type

Update offer type.

**URL:** /offer_types/{offer_type_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `200` - Offer type updated

**Response Params:** See [Offer Type Properties](#offer-type-properties)

**Examples:**

Request:

```
PUT /offer_types/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Single",
    "sort_priority": 9,
    "slug": "single",
    "is_active": true
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "sort_priority": 9,
    "slug": "single",
    "meta": null,
    "description": null,
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Offer Type

Delete offer type. This service will set `is_active` value to false.

**URL:** /offer_types/{offer_type_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Offer type deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /offer_types/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Platforms

Platform resource to list platform supported by the system.

Current existing platforms are:

1. iOS

2. Android

3. Windows Phone

4. Web

Note: When new platform with tiered pricings added, [Offer](#offer) will also need to be populated with the platform's offers

#### Platform Properties

* `id` - Number; Instance identifier

* `name` - String; Unique offer type name

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `slug` - String, Unique; Platform slug for SEO

* `meta` - String; Platform meta for SEO

* `description` - String; Platform description

#### Create Platform

Create new platform.

**URL:** /platforms

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `201` - New platform created

**Response Params:** See [Platform Properties](#platform-properties)

**Examples:**

Request:

```
POST /platforms HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "iOS",
    "sort_priority" : 1,
    "is_active": true,
    "slug": "ios"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/platforms/1

{
    "id": 1,
    "name": "iOS",
    "sort_priority" : 1,
    "is_active": true,
    "slug": "ios",
    "meta": null,
    "description": null
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Platforms

Retrieve platforms.

**URL:** /platforms

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Item Type Properties](#item-type-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Platforms retrieved and returned in body content

* `204` - No matched platform

**Response Params:** See [Platform Properties](#platform-properties)

**Examples:**

Request:

```
GET /platforms?fields=id,name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "platforms": [
        {
            "id": 1,
            "name": "iOS",
            "sort_priority" : 1,
            "is_active": true,
            "slug": "ios",
            "meta": null,
            "description": null
        },
        {
            "id": 2,
            "name": "Android",
            "sort_priority" : 1,
            "is_active": true,
            "slug": "android",
            "meta": null,
            "description": null
        },
        {
            "id": 3,
            "name": "Windows Phone",
            "sort_priority" : 1,
            "is_active": true,
            "slug": "windows-phone",
            "meta": null,
            "description": null
        },
        {
            "id": 4,
            "name": "Web",
            "sort_priority" : 1,
            "is_active": true,
            "slug": "web",
            "meta": null,
            "description": null
        }
    ],
    "metadata": {
        "resultset": {
            "count": 4,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Platform

Retrieve individual platform using identifier.

**URL:** /platforms/{platform_id}

Optional:

* `fields` - Retrieve specific properties. See [Platform Properties](#platform-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Platform found and returned

**Response Params:** See [Platform Properties](#platform-properties)

**Examples:**

Request:

```
GET /platforms/1?fields=id,name HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "iOS"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Platform

Update platform.

**URL:** /platforms/{platform_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `200` - Platform updated

**Response Params:** See [Platform Properties](#platform-properties)

**Examples:**

Request:

```
PUT /platforms/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "iOS",
    "sort_priority": 9,
    "slug": "ios",
    "is_active": true
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "iOS"
    "sort_priority": 9,
    "slug": "ios",
    "meta": null,
    "description": null,
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Platform

Delete platform. This service will set `is_active` value to false.

**URL:** /platforms/{platform_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Platform deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /platforms/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### iOS Tiers

iOS Tiers resource contains list of tiers associated with iOS platform's tiered payment gateways. Same unique product identifier must available on the payment gateway side.

iOS platform tiered payment gateway: Apple In-App Purchase (iTunes)

#### iOS Tier Properties

* `tier_price` - String; Tier's price. On tiered patment gateway, this price information only for showing and base price for reporting, the actual price determined by the value on payment gateway side.

* `tier_order` - Number; Tier's order. The price order, e.g., 1 - USD 0.99, 2 = USD 1.99, 3 = USD 2.99, and soon

* `currency_id` - Number; Currency's identifier, refer to Currencies resource.

* `currency` - Number; Currency code. **TODO: FIX INCONSISTENT NAMING**

* `tier_type` - Number; Tier type

    * 1: NON-CONSUMABLE - Tier that corresponded an offer as a tier (iTunes' non-consumable and auto-renewal subscription).

    * 2: CONSUMABLE - Tier that corresponded more than an offer as a tier (iTunes' consumable, Google IAB unmanaged, WP consumable).

* `is_active` - Boolean; Instance’s active status, used to be marked if the instance is active or soft-deleted. The default value is true.

* `tier_code` - String; Tier unique identifier to associates with payment gateway side. The value will be used cross clients for the identical offer.

    ```
    e.g.,

    Tempo Issue #34

    Sample of iTunes In-App Purchase's product codes:
    1. SCOOP: com.appsfoundry.scoop.nc.tempo-issue-34
    2. Tempo Whitelabel: com.appsfoundry.id.tempo.nc.tempo-issue-34
    3. SCOOP Business Magz Whitelabel: com.appsfoundry.biz.nc.tempo-issue-34


    The tier will be recorded as below:
    tier_type: 1
    tier_code: .nc.tempo-issue-34

    ```

#### Create iOS Tier

Creates iOS tier

**URL:** /ios_tiers

**Method:** POST

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code`

**Response HTTP Code:**

* `201` - New iOS Tier created

**Response Params:** See [iOS Tier Properties](#ios-tier-properties)

**Examples:**

Request:

```
POST /ios_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	“tier_price": “0.99",
	“tier_order": 1,
	“currency_id": 1,
	“tier_type": 1,
	“is_active": true,
	“tier_code": “com.IOS.c.99"
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
    "id": 1,
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": "com.IOS.c.99"
}

```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}


```

#### Update iOS Tier

Updates existing iOS tier

**URL:** /ios_tiers/{ios_tier_id}

**Method:** PUT

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code`

**Response HTTP Code:**

* `200` - iOS Tier updated

**Response Params:** See [iOS Tier Properties](#ios-tier-properties)

**Examples:**

Request:

```
PUT /ios_tiers/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	“tier_price": “0.99",
	“tier_order": 1,
	“currency_id": 1,
	“tier_type": 1,
	“is_active": true,
	“tier_code": “com.IOS.c.99"
}

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": "com.IOS.c.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Retrieve iOS Tiers

Retrieves list of the iOS Tiers

**URL:** /ios_tiers

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Tiers IOS Properties](#tiers-ios-properties). Default to ALL.

* `offset` - The offset of the records. The default value is 0

* `limit` - The limit of the records. The default value is 9999

* `order` - Order the records. The default mode is ascending order by primary identifier or ascending by created timestamp

* `q` - Simple string query to record’s string properties

**Method:** GET

**Response HTTP Code:**

* `200` - iOS tiers retrieved and returned in body content
* `204` - No matched iOS tier

**Response Params:** See [iOS Tier Properties](#ios-tier-properties)

**Examples:**

Request:

```
GET /ios_tiers?fields=id,name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    ios_tiers: [
        {
            "id": 1,
            "tier_price": "0.99",
            "tier_order": 1,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": "com.IOS.c.99",
        },
        {
            "id": 2,
            "tier_price": "1.99",
            "tier_order": 2,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": "com.IOS.c.1.99",
        },
        ...
        {
            "id": 20,
            "tier_price": "19.99",
            "tier_order": 20,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": "com.IOS.c.19.99",
        }
    ],
    "metadata": {
        "count": 367,
        "limit": 20,
        "offset": 0
    }
}

```

Fail response:

```
HTTP/1.1 500 Internal Server Error

```

#### Retrieve Individual iOS Tier

Retrieves specific iOS tier

**URL:** /ios_tiers/{ios_tier_id}

**Alternative URL:** /ios_tiers/tier_code/.nc.tempo-issue-34 ** TODO: IMPLEMENTATION **

Optional:

* `fields` - Retrieve specific fields. See [iOS Tier Properties](#tiers-ios-properties). Default to ALL.

**Method:** GET

**Response HTTP Code:**

* `200` - iOS Tier is retrieved and returned

**Response Params:** See [iOS Tier Properties](#ios-tier-properties)

**Examples:**

Request:

```
GET /ios_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
	"tier_price": "0.99",
	"tier_order": 1,
    "currency": "USD",
	"currency_id": 1,
	"tier_type": 1,
	"is_active": true,
	"tier_code": "com.IOS.c.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found

```

#### Delete iOS Tier

Deletes iOS tier by using identifier.

**URL:** /ios_tiers/{ios_tier_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - iOS Tier is deleted

**Response Params:** See [iOS Tier Properties](#ios-tier-properties)

**Examples:**

Request:

```
DELETE /ios_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


```

Success response:

```
HTTP/1.1 204 No Content

```

Fail response:

```
HTTP/1.1 404 Not Found

```

### Android Tiers

Android Tiers resource contains list of tiers associated with Android platform's tiered payment gateways. Same unique product identifier must available on the payment gateway side.

Android platform tiered payment gateway: Google In-App Billing

#### Android Tier Properties

* `tier_price` - String; Tier's price. On tiered patment gateway, this price information only for showing and base price for reporting, the actual price determined by the value on payment gateway side.

* `tier_order` - Number; Tier's order. The price order, e.g., 1 - USD 0.99, 2 = USD 1.99, 3 = USD 2.99, and so on

* `currency_id` - Number; Currency's identifier, refer to Currencies resource.

* `currency` - Number; Currency code. **TODO: FIX INCONSISTENT NAMING**

* `tier_type` - Number; Tier type

    * 1: NON-CONSUMABLE - Tier that corresponded an offer as a tier (iTunes' non-consumable and auto-renewal subscription).

    * 2: CONSUMABLE - Tier that corresponded more than an offer as a tier (iTunes' consumable, Google IAB unmanaged, WP consumable).

* `is_active` - Boolean; Instance’s active status, used to be marked if the instance is active or soft-deleted. The default value is true.

* `tier_code` - String; Tier unique identifier to associates with payment gateway side. The value will be used cross clients for the identical offer.

    ```
    e.g.,

    Tempo Issue #34

    Sample of iTunes In-App Purchase's product codes:
    1. SCOOP: com.appsfoundry.scoop.nc.tempo-issue-34
    2. Tempo Whitelabel: com.appsfoundry.id.tempo.nc.tempo-issue-34
    3. SCOOP Business Magz Whitelabel: com.appsfoundry.biz.nc.tempo-issue-34


    The tier will be recorded as below:
    tier_type: 1
    tier_code: .nc.tempo-issue-34

    ```

#### Create Android Tier

Creates Android tier

**URL:** /android_tiers

**Method:** POST

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code`

**Response HTTP Code:**

* `201` - New Android tier created

**Response Params:** See [Android Tier Properties](#android-tier-properties)

**Examples:**

Request:

```
POST /android_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"tier_price": “0.99",
	"tier_order": 1,
	"currency_id": 1,
	"tier_type": 1,
	"is_active": true,
	"tier_code": “.c.usd.0.99"
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
    "id": 1,
	“tier_price": “0.99",
	“tier_order": 1,
	“currency_id": 1,
	“tier_type": 1,
	“is_active": true,
	“tier_code": “.c.usd.0.99"
}

```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}


```

#### Update Android Tier

Updates existing android tier

**URL:** /android_tiers/{android_tier_id}

**Method:** PUT

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code` (only required if the value in tier_type is 1 or 2)

**Response HTTP Code:**

* `200` - Android tier updated

**Response Params:** See [Android Tier Properties](#android-tier-properties)

**Examples:**

Request:

```
PUT /android_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"tier_price": “0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": “.c.usd.0.99"
}

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	“tier_price": “0.99",
	“tier_order": 1,
	“currency_id": 1,
    "currency": "USD",
	“tier_type": 1,
	“is_active": true,
	“tier_code": “.c.usd.0.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Retrieve Android Tiers

Retrieves all of the android tiers

**URL:** /android_tiers

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Android Tier Properties](#android-tier-properties). Default to ALL.

* `offset` - The offset of the records. The default value is 0

* `Limit` - The limit of the records. The default value is 9999

* `order` - Order the records. The default mode is ascending order by primary identifier or ascending by created timestamp

* `q` - Simple string query to record’s string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Android tiers retrieved and returned in body content
* `204` - No matched Android tier

**Response Params:** See [Android Tier Properties](#android-tier-properties)

**Examples:**

Request:

```
GET /android_tiers?fields=id, name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    android_tiers: [
        {
            "id": 1,
            "tier_price": "0.99",
            "tier_order": 1,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.0.99",
        },
        {
            "id": 2,
            "tier_price": "1.99",
            "tier_order": 2,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.1.99",
        },
        ...
        {
            "id": 20,
            "tier_price": "19.99",
            "tier_order": 20,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.19.99",
        }
    ],
    "metadata": {
        "count": 87,
        "limit": 20,
        "offset": 0
    }
}


```

Fail response:

```
HTTP/1.1 500 Internal Server Error

```

#### Retrieve Individual android Tier

Retrieves android tier per item

**URL:** /android_tiers/{android_tier_id}

Optional:

* `fields` - Retrieve specific fields. See [Android Tier Properties](#android-tier-properties). Default to ALL.

**Method:** GET

**Response HTTP Code:**

* `200` - Android tier is retrieved and returned

**Response Params:** See [Android Tier Properties](#android-tier-properties)

**Examples:**

Request:

```
GET /android_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": "com.ANDROID.c.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found

```

#### Delete android Tier

Deletes android tier by using identifier.

**URL:** /android_tiers/{android_tier_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Android tier is deleted

**Response Params:** See [Android Tier Properties](#android-tier-properties)

**Examples:**

Request:

```
DELETE /android_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


```

Success response:

```
HTTP/1.1 204 No Content

```

Fail response:

```
HTTP/1.1 404 Not Found


```

### Windows Phone Tiers

Windows phone tiers contains bundled price for Windows Phone platform.

#### Windows Phone Tier Properties

* `tier_price` - String; Tier's price. On tiered patment gateway, this price information only for showing and base price for reporting, the actual price determined by the value on payment gateway side.

* `tier_order` - Number; Tier's order. The price order, e.g., 1 - USD 0.99, 2 = USD 1.29, 3 = USD 1.49, and so on

* `currency_id` - Number; Currency's identifier, refer to Currencies resource.

* `currency` - Number; Currency code. **TODO: FIX INCONSISTENT NAMING**

* `tier_type` - Number; Tier type

    * 1: NON-CONSUMABLE - Tier that corresponded an offer as a tier (iTunes' non-consumable and auto-renewal subscription).

    * 2: CONSUMABLE - Tier that corresponded more than an offer as a tier (iTunes' consumable, Google IAB unmanaged, WP consumable).

* `is_active` - Boolean; Instance’s active status, used to be marked if the instance is active or soft-deleted. The default value is true.

* `tier_code` - String; Tier unique identifier to associates with payment gateway side. The value will be used cross clients for the identical offer.

    ```
    e.g.,

    Tempo Issue #34

    Sample of iTunes In-App Purchase's product codes:
    1. SCOOP: com.appsfoundry.scoop.nc.tempo-issue-34
    2. Tempo Whitelabel: com.appsfoundry.id.tempo.nc.tempo-issue-34
    3. SCOOP Business Magz Whitelabel: com.appsfoundry.biz.nc.tempo-issue-34


    The tier will be recorded as below:
    tier_type: 1
    tier_code: .nc.tempo-issue-34

    ```

#### Create Windows Phone Tier

Creates windows phone tier.

**URL:** /wp_tiers

**Method:** POST

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code` (only required if the value in tier_type is 1 or 2)

**Response HTTP Code:**

* `201` - New Windows Phone Tier created

**Response Params:** See [Windowns Phone Tier Properties](#windows-phone-tier-properties)

**Examples:**

Request:

```
POST /wp_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": ".c.usd.0.99"
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
    "id" = 1.
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": ".c.usd.0.99"
}

```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}


```

#### Update Windows Phone Tier
Updates existing windows phone tier

**URL:** /wp_tiers/{windowsphone_tier_id}

**Method:** PUT

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code` (only required if the value in tier_type is 1 or 2)

**Response HTTP Code:**

* `200` - Windows Phone tier updated

**Response Params:** See [Windows Phone Tier Properties](#windows-phone-tier-properties)

**Examples:**

Request:

```
PUT /wp_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": ".c.usd.0.99"
}

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": ".c.usd.0.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Retrieve Windows Phone Tiers
Retrieves all of the windows phone tiers.

**URL:** /wp_tiers

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Windows Phone Tier Properties](#windows-phone-tier-properties). Default to ALL.

* `offset` - The offset of the records. The default value is 0

* `Limit` - The limit of the records. The default value is 9999

* `order` - Order the records. The default mode is ascending order by primary identifier or ascending by created timestamp

* `q` - Simple string query to record’s string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Windows Phone tiers retrieved and returned in body content

* `204` - No matched Windows Phone tier

**Response Params:** See [Windows Phone Tier Properties](#windows-phone-tier-properties)

**Examples:**

Request:

```
GET /wp_tiers?fields=id, name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    wp_tiers: [
        {
            "id": 1,
            "tier_price": "0.99",
            "tier_order": 1,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.0.99",
        },
        {
            "id": 2,
            "tier_price": "1.29",
            "tier_order": 2,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.1.29",
        },
        ...
        {
            "id": 20,
            "tier_price": "14.99",
            "tier_order": 20,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.14.99",
        }
    ],
    "metadata": {
        "count": 81,
        "limit": 20,
        "offset": 0
    }
}



```

Fail response:

```
HTTP/1.1 500 Internal Server Error

```

#### Retrieve Individual android Tier
Retrieves android tier per item

**URL:** /wp_tiers/{windowsphone_tier_id}

Optional:

* `fields` - Retrieve specific fields. See [Windows Phone Tier Properties](#windows-phone-tier-properties). Default to ALL.

**Method:** GET

**Response HTTP Code:**

* `200` - Android tier is retrieved and returned

**Response Params:** See [Windows Phone Tier Properties](#windows-phone-tier-properties)

**Examples:**

Request:

```
GET /wp_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	“tier_price": “0.99",
	“tier_order": 1,
	“currency_id": 1,
    "currency": "USD",
	“tier_type": 1,
	“is_active": true,
	“tier_code": “com.WP.c.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found

```

#### Delete Windows Phone Tier
Deletes windows phone tier by using identifier.

**URL:** /wp_tiers/{windowsphone_tier_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Windows Phone tier is deleted

**Response Params:** See [Windows Phone Tier Properties](#windows-phone-tier-properties)

**Examples:**

Request:

```
DELETE /wp_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%

```

Success response:

```
HTTP/1.1 204 No Content

```

Fail response:

```
HTTP/1.1 404 Not Found

```

### Currencies

Currency resource.

#### Currency Properties

* `id` - Number; Currency identifier

* `iso4217_code` - String; Unique ISO 4217 code. eg: USD, IDR, GBP. This property can be used as alternative identifier

* `right_symbol` - String; Currency right symbol

* `left_symbol` - String; Currency left symbol

* `one_usd_equals` - Float String; 1 USD comparison to the currency value

* `group_separator` - String; Thousand group separator. Default to comma ( , )

* `decimal_separator` - String; Decimal separator. Default to dot ( . )

#### Create Currency

Create new currency.

**URL:** /currencies

**Method:** POST

**Request Data Params:**

Required: `iso4217_code`, `right_symbol`, `left_symbol`, `one_usd_equals`

Optional: `group_separator`, `decimal_separator`

**Response HTTP Code:**

* `201` - New currency created

**Response Params:** See [Currency Properties](#currency-properties)

**Examples:**

Request:

```
POST /currencies HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "iso4217_code": "USD",
    "right_symbol": "",
    "one_usd_equals": "1.0",
    "left_symbol": "$",
    "group_separator": ".",
    "decimal_separator": ","
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/currencies/1

{
    "id": 1,
    "iso4217_code": "USD",
    "right_symbol": "",
    "one_usd_equals": "1.0",
    "left_symbol": "$",
    "group_separator": ".",
    "decimal_separator": ","
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409101,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Currencies

Retrieve currencies.

**URL:** /currencies

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Currency Properties](#currency-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Currencies retrieved and returned in body content

* `204` - No matched currency

**Response Params:** See [Currency Properties](#currency-properties)

**Examples:**

Request:

```
GET /currencies HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "currencies": [
        {
            "id": 1,
            "iso4217_code": "USD",
            "right_symbol": "",
            "one_usd_equals": "1.0",
            "left_symbol": "$",
            "group_separator": ",",
            "decimal_separator": "."
        },
        {
            "id": 2,
            "iso4217_code": "IDR",
            "right_symbol": "",
            "one_usd_equals": "11000",
            "left_symbol": "Rp",
            "group_separator": ".",
            "decimal_separator": ","
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 2
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Currency

Retrieve individual currency with identifier.

**URL:** /currencies/{currency_id}

Optional:

* `fields` - Retrieve specific properties. See [Currency Properties](#currency-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Currency found and returned

**Response Params:** See [Currency Properties](#currency-properties)

**Examples:**

Request:

```
GET /currencies/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "iso4217_code": "USD",
    "right_symbol": "",
    "one_usd_equals": "1.0",
    "left_symbol": "$",
    "group_separator": ",",
    "decimal_separator": "."
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Currency

Update currency.

**URL:** /currencies/{currency_id}

**Method:** PUT

**Request Data Params:**

Required: `iso4217_code`, `right_symbol`, `left_symbol`, `one_usd_equals`

Optional: `group_separator`, `decimal_separator`

**Response HTTP Code:**

* `200` - Currency updated

**Response Params:** See [Currency Properties](#currency-properties)

**Examples:**

Request:

```
PUT /currencies/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "iso4217_code": "USD",
    "right_symbol": "",
    "one_usd_equals": "1.0",
    "left_symbol": "$"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "iso4217_code": "USD",
    "right_symbol": "",
    "one_usd_equals": "1.0",
    "left_symbol": "$",
    "group_separator": null,
    "decimal_separator": null
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Delete Currency

Delete currency. This service will set `is_active` value to false.

**URL:** /currencies/{currency_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Currency deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /currencies/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Payment Gateways

Payment gateway resource.

#### Payment Gateway Properties

* `id` - Number; Instance identifier

* `name` - String; Vendor name

* `payment_flow_type` - Number; Payment flow type as below.

    1: N/A; Set to this when payment flow is not yet clear or not yet normalized

    2: AUTHORIZE - VALIDATE; 2 step payment flow with server side to authorize and later validate, e.g., mandiri e-cash, finnet

    3: CHARGE; 1 step payment flow with server side to trigger charge, e.g., mandiri clickpay, SCOOP Point, FREE

    4: VALIDATE; 1 step payment flow with server side receipt validation, e.g., Apple In-App Purchase, Google In-App Billing, Windows Phone In-App Purchase

    5: AUTHORIZE - CAPTURE; 2 step payment flow with server side to authorize and capture, e.g., Paypal, Veritrans' VT-Direct

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority.

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true.

* `slug` - String, Unique; Payment gateway slug for SEO.

* `meta` - String; Payment gateway meta for SEO.

* `description` - String; Payment gateway description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URL.

* `icon_image_normal` - String; Payment gateway icon image URI.

* `icon_image_highres` - String; Payment gateway icon image in highres URI.

* `base_currency_id` - Number; Currency identifier which this payment gateway used as base currency.

* `mnc` - Number; Mobile Network Code, use on payment gateway in relation with telco.

* `mcc` - Number; Mobile Country Code, use on payment gateway in relation with telco.

* `sms_number` - Number; Short Message Service number to send, use on payment gateway in relation with telco.

* `organization_id` - Number; Unique, organization identifier that this instance bind to. A payment gateway can only bind to an organization.

* `clients` - Array; List of clients this payment gateway already implemented.

* `minimal_amount` - String; The minimal amount of purchase. Set to NULL if one has no minimal amount.

#### Create Payment Gateway

Create new payment gateway.

**URL:** /payment_gateways

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`, `cut_rule_id`, `base_currency_id`, `organization_id`, `payment_flow_type`, `clients`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `mnc`, `mcc`, `sms_number`

**Response HTTP Code:**

* `201` - New payment gateway created

**Response Params:** See [Payment Gateway Properties](#payment-gateway-properties)

**Examples:**

Request:

```
POST /payment_gateways HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Apple In-App Purchase",
    "slug": "apple-in-app-purchase",
    "sort_priority": 1,
    "payment_flow_type": 4,
    "is_active": true,
    "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
    "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
    "cut_rule_id": 5,
    "base_currency_id": 1,
    "organization_id": 3421,
    "clients": [],
    "minimal_amount": "25000"

}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/payment_gateways/1

{
    "id": 1,
    "name": "Apple In-App Purchase",
    "slug": "apple-in-app-purchase",
    "sort_priority": 1,
    "payment_flow_type": 4,
    "is_active": true,
    "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
    "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
    "cut_rule_id": 5,
    "base_currency_id": 1,
    "description": null,
    "mnc": null,
    "mcc": null,
    "sms_number": null,
    "organization_id": 3421,
    "clients": [],
    "minimal_amount": "25000"
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

#### Retrieve Payment Gateways

Retrieve payment gateways.

**URL:** /payment_gateways

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Payment Gateway Properties](#payment-gateway-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

* `organization_id` - Filter by organization identifier(s). Response will returns if any matched

**Method:** GET

**Response HTTP Code:**

* `200` - Payment gateway retrieved and returned in body content

* `204` - No matched payment gateway

**Response Params:** See [Payment Gateway Properties](#payment-gateway-properties)

**Examples:**

Request:

```
GET /payment_gateways HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "payment_gateways": [
        {
            "id": 1,
            "name": "Apple In-App Purchase",
            "slug": "apple-in-app-purchase",
            "sort_priority": 1,
            "payment_flow_type": 4,
            "is_active": true,
            "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
            "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
            "cut_rule_id": 5,
            "base_currency_id": 1,
            "meta": null,
            "description": null,
            "mnc": null,
            "mcc": null,
            "sms_number": null,
            "organization_id": 3421,
            "clients": [13, 14],
            "minimal_amount": "25000"
        },
        {
            "id": 2,
            "name": "Paypal",
            "slug": "paypal",
            "sort_priority": 1,
            "payment_flow_type": 5,
            "is_active": true,
            "icon_image_normal": "payment_gateways/paypal/icon.png",
            "icon_image_highres": "payment_gateways/paypal/icon_highres.png",
            "cut_rule_id": 7,
            "base_currency_id": 2,
            "meta": null,
            "description": null,
            "mnc": null,
            "mcc": null,
            "sms_number": null,
            "organization_id": 3424,
            "clients": [13, 14],
            "minimal_amount": NULL
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Payment Gateway

Retrieve individual payment gateway using identifier.

**URL:** /payment_gateways/{payment_gateway_id}

**Alternative URL:** /payment_gateways/organization/{organization_id}

Optional:

* `fields` - Retrieve specific properties. See [Payment Gateway Properties](#payment-gateway-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Payment gateway found and returned

**Response Params:** See [Payment Gateway Properties](#payment-gateway-properties)

**Examples:**

Request:

```
GET /payment_gateways/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Apple In-App Purchase",
    "slug": "apple-in-app-purchase",
    "sort_priority": 1,
    "payment_flow_type": 4,
    "is_active": true,
    "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
    "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
    "cut_rule_id": 5,
    "base_currency_id": 1,
    "meta": null,
    "description": null,
    "mnc": null,
    "mcc": null,
    "sms_number": null,
    "organization_id": 3421,
    "clients": [13, 14]m
    "minimal_amount": "25000"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Payment Gateway

Update payment gateway.

**URL:** /payment_gateways/{payment_gateway_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`, `cut_rule_id`, `base_currency_id`, `organization_id`, `payment_flow_type`, `clients`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `mnc`, `mcc`, `sms_number`

**Response HTTP Code:**

* `200` - Payment gateway updated

**Response Params:** See [Payment Gateway Properties](#payment-gateway-properties)

**Examples:**

Request:

```
PUT /payment_gateways/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Apple In-App Purchase",
    "slug": "apple-in-app-purchase",
    "sort_priority": 1,
    "payment_flow_type": 4,
    "is_active": true,
    "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
    "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
    "cut_rule_id": 5,
    "base_currency_id": 1,
    "meta": "Apple In-App Purchase iTunes IAP,
    "organization_id": 3421,
    "clients": [13, 14, 15],
    "minimal_amount": "25000"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Apple In-App Purchase",
    "slug": "apple-in-app-purchase",
    "sort_priority": 1,
    "payment_flow_type": 4,
    "is_active": true,
    "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
    "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
    "cut_rule_id": 5,
    "base_currency_id": 1,
    "meta": "Apple In-App Purchase iTunes IAP,
    "description": null,
    "mnc": null,
    "mcc": null,
    "sms_number": null,
    "organization_id": 3421,
    "clients": [13, 14, 15],
    "minimal_amount": "25000"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Payment Gateway

Delete payment gateway. This service will set `is_active` value to false.

**URL:** /payment_gateways/{payment_gateway_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Payment gateway deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /payment_gateways/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Payment Gateway Tiers

Payment gateway tier resource used to get which files are linked to an payment gateway

#### Payment Gateway Tier Properties

* `id` - Number; Instance identifier

* `payment_gateway_id` - Number; Payment gateway's identifier which this tier linked to

* `currency_id` - Number; Currency's identifier of the tier

* `tier_order` - Number; Tier order, this will used to define which tier the discount fell into

* `tier_price` - String; Tier price in 2 digit precision float, this will used to define which tier the discount fell into

* `tier_code` - String; Tier code, payment gateway side identifier of consumable type (Apple In-App Purchase, Google In-App Billing, etc)

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

#### Create Payment Gateway Tiers

Create new payment gateway tiers

**URL:** /payment_gateway_tiers

**Method:** POST

**Request Data Params:**

Required: `payment_gateway_id `, `currency_id `, `tier_order `, `tier_price `, `tier_code `, `is_active `

**Response HTTP Code:**

* `207` - Multiple status of each payment gateway tier

    * `201` -  New payment gateway tier created

**Response Params:** See [Payment Gateway Tier Properties](#payment-gateway-tier-properties)

**Examples:**

Request:

```
POST /payment_gateway_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "payment_gateway_tiers": [
        {
            "payment_gateway_id": 1,
            "currency_id": 1,
            "tier_order": 1,
            "tier_price" : "0.99",
            "tier_code": "c.usd.0.99",
            "is_active": true,
        },
        {
            "payment_gateway_id": 1,
            "currency_id": 1,
            "tier_order": 2,
            "tier_price" : "1.99",
            "tier_code": "c.usd.1.99",
            "is_active": true,
        }
    ]
}
```

Success response:

```
HTTP/1.1 207 Multi-Status
Content-Type: application/json; charset=utf-8

{
    "results":[
        {
            "status": 201,
            "id": 1,
            "payment_gateway_id": 1,
            "currency_id": 1,
            "tier_order": 1,
            "tier_price" : "0.99",
            "tier_code": "c.usd.0.99",
            "is_active": true,
        },
        {
            "status": 201,
            "id": 2,
            "payment_gateway_id": 1,
            "currency_id": 1,
            "tier_order": 2,
            "tier_price" : "1.99",
            "tier_code": "c.usd.1.99",
            "is_active": true,
        }
    ]
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

### Campaigns

Campaign resource.

#### Campaign Properties

* `id` - Number; Currency identifier

* `name` - String; Campaign name

* `description` - String; Campaign description

#### Create Campaign

Create new campaign.

**URL:** /campaigns

**Method:** POST

**Request Data Params:**

Required: `name`

Optional: `description`

**Response HTTP Code:**

* `201` - New campaign created

**Response Params:** See [Campaign Properties](#campaign-properties)

**Examples:**

Request:

```
POST /campaigns HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Christmas 2014 Sale",
    "description": "Christmas 2014 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/campaigns/1

{
    "id": 1,
    "name": "Christmas 2014 Sale",
    "description": "Christmas 2014 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

#### Retrieve Campaigns

Retrieve campaigns.

**URL:** /campaigns

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Campaign Properties](#campaign-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Campaign retrieved and returned in body content

* `204` - No matched campaign

**Response Params:** See [Campaign Properties](#campaign-properties)

**Examples:**

Request:

```
GET /campaigns HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "campaigns": [
        {
            "id": 1,
            "name": "Christmas 2014 Sale",
            "description": "Christmas 2014 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 1,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Campaign

Retrieve individual campaign.

**URL:** /campaigns/{campaign_id}

Optional:

* `fields` - Retrieve specific properties. See [Campaign Properties](#campaign-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Campaign found and returned

**Response Params:** See [Campaign Properties](#campaign-properties)

**Examples:**

Full request:

```
GET /campaigns/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Christmas 2014 Sale",
    "description": "Christmas 2014 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Retrieve Campaign's Items

Retrieve items that are included to any campaigns.

**URL:** /campaigns/{campaign_id}/items

Required:

* `platform_id` - Retrieve specific item in platform id within any campaigns

**Method:** GET

**Response HTTP Code:**

* `200` - Campaign retrieved and returned in body content

* `204` - No matched campaign

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

```
GET /campaigns/654/items?platform_id=4 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "items": [
        {
            "is_featured": false,
            "extra_items": [],
            "gtin13": null,
            "subs_weight": 1,
            "parental_control_id": 1,
            "is_active": true,
            "brand":
            {
                        "id": 142,
                        "name": "POPULAR"
            },
            "item_type_id": 1,
            "item_status": 9,
            "gtin8": null,
            "brand_id": 142,
            "thumb_image_normal": "images/1/142/general_small_covers/ID_POPLR2013MTH12_S_.jpg",
            "meta": null,
            "content_type": 1,
            "authors": [],
            "display_offers":
            [
                {
                    "discount_tag": "Reduce Price for POPULAR 2013",
                    "discount_name": "Reduce Price for POPULAR 2013",
                    "price_usd": "3.99",
                    "discount_id": [
                        "697"
                    ],
                    "discount_paymentgateway_id": [
                        2,
                        5,
                        11,
                        12,
                        14,
                        6
                    ],
                    "offer_id": 18927,
                    "offer_type_id": 1,
                    "discount_price_idr": "20000.00",
                    "price_idr": "39000.00",
                    "price_point": 390,
                    "discount_price_usd": "1.99",
                    "is_free": false,
                    "discount_price_point": 0,
                    "offer_name": "Single Edition",
                    "aggr_price_usd": null,
                    "aggr_price_point": null,
                    "aggr_price_idr": null
              }
            ],
            "is_latest": false,
            "id": 30669,
            "edition_code": "ID_POPLR2013MTH12",
            "image_highres": "images/1/142/big_covers/ID_POPLR2013MTH12_B_.jpg",
            "restricted_countries": [],
            "sort_priority": 1,
            "image_normal": "images/1/142/general_big_covers/ID_POPLR2013MTH12_B_.jpg",
            "name": "POPULAR / DEC 2013",
            "allowed_countries": [],
            "release_date": "2013-12-01T00:00:00",
            "is_extra": false,
            "slug": "popular-dec-2013",
            "media_base_url": "http://images.getscoop.com/magazine_static/",
            "gtin14": null,
            "reading_direction": 1,
            "filenames":
            [
                "images/1/142/ID_POPLR2013MTH12.zip"
            ],
            "previews": [],
            "thumb_image_highres": "images/1/142/small_covers/ID_POPLR2013MTH12_S_.jpg",
            "issue_number": "DEC 2013"
            },
            {...}
        ],
        "name": "Reduce Price for POPULAR",
        "description": "Reduce Price for POPULAR magazine"
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Update Campaign

Update campaign.

**URL:** /campaigns/{campaign_id}

**Method:** PUT

**Request Data Params:**

Required: `name`

Optional: `description`

**Response HTTP Code:**

* `200` - Campaign updated

**Response Params:** See [Campaign Properties](#campaign-properties)

**Examples:**

Full request:

```
PUT /campaigns/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "0.99 Sale",
    "description": "0.99 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "0.99 Sale",
    "description": "0.99 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Fail response:

```
HTTP/1.1 400 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

#### Delete Campaign

Delete campaign. This service will set `is_active` value to false.

**URL:** /campaigns/{campaign_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Campaign deleted

**Response Params:** -

**Examples:**

Full request:

```
DELETE /campaigns/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Discounts **TODO: UPDATE**

Discount resource to set and manage various discount case.

Action on this resource may affects offers' `discount_price_...` to be recalculated.

#### Discount Properties

* `name` - String; Discount name

* `tag_name` - String; Discount tag name, will be shown

* `valid_from` - String; When the discount is started in datetime format

* `valid_to` - String; When the discount is expired in datetime format

* `campaign_id` - Number; Campaign identifier, to which campaign the discount is attached to

* `discount_rule` - Number; How the discount will affect the price:

    * 1: By amount - the price will be reduced by some certain amount of price

    * 2: By percentage - the price will be reduced based on the percentage

    * 3: To amount - the price will be reduced to certain amount of price

* `discount_type` - Number; Type of the discount, how the discount will be executed

    * 1: Offer Discount - The discount is based on some offers

    * 2: Order Discount - The discount is based by order

    * 3: Offer Payment gateway Discount - The discount is based on the payment method

    * 4: Order Payment gateway Discount - The discount is based by the order in certain payment method

    * 5: Discount Code - The discount is based on discount codes

* `discount_status` - Number; Status of the discount

    * 1: New - The discount is just made

    * 2: Ongoing - The discount is within its due date

    * 3: Expired - The discount is past its due date

* `discount_schedule_type` - Number; How the discount code is scheduled

    * 1: Immediate - The discount is available immediately

    * 2: Scheduled - The discount is availabe on a certain date

* `is_active` - String; The status of the discount, whether active or not in Boolean format

* `discount_usd` - String; The value/amount of the discount in USD

* `discount_idr` - String; The value/amount of the discount in IDR

* `discount_point` - Number; The value/amount of the discount in PTS

* `offers` - List of the offers/items in which the discount is available for

    * `offer_id` - Number; Offer identifier

    * `offer_name` - String; The item name

* `predefined_group` - Number; Explains which items are available for any discounts

    * 1: All Magazines - The discount is available for every purchase (single and subscription) of magazines

    * 2: All Books - The discount is available for single purchase of books

    * 3: All Newspapers - The discount is available for subscription purchase of newspapers

    * 4: All Offers - The discount is available for both single or subscription purchasing

    * 5: All Single - The discount is available for every single purchase (of magazines and/or books)

    * 6: All Subscriptions - The discount is available for every subscription purchase (of magazines and/or newspapers)

    * 7: Magazine Single - The discount is *only* available for every single purchase of magazines

    * 8: Magazine Subscriptions - The discount is *only* available for every subscription purchase of magazines

    * 9: All Magazines + All Newspapers - The discount is available for every single and subscription purchase of magazines and subscription purchase of newspapers

* `platforms` - Numbers; List of `platform_id` in which platform the discount is avaliable

* `min_usd_order_price` - String; The minimal amount of purchasing in USD so that the discount is available to use

* `max_usd_order_price` - String; The maximal amount of purchasing in USD so that the discount is available to use

* `min_idr_order_price` - String; The minimal amount of purchasing in IDR so that the discount is available to use

* `max_idr_order_price` - String; The minimal amount of purchasing in IDR so that the discount is available to use

* `paymentgateways` - Number; list of 'paymentgateway_id' in which payment method the discount is available

* `description` - String; Discount description

#### Create Discount

Create new discount.

**URL:** /discounts

**Method:** POST

**Response HTTP Code:**

* `201` - New discount created

**Response Params:** See [Discount Properties](#discount-properties)

**Example:**

Request (without predefined group):

```
POST /discounts HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Promo Mayday",
    "tag_name": "Promo",
    "valid_from": "2015-05-01 09:59:59",
    "valid_to": "2015-05-07 23:59:59",
    "campaign_id": 697,
    "discount_rule": 2,
    "discount_type": 1,
    "discount_status": 1,
    "discount_schedule_type": 1,
    "is_active": true,
    "discount_usd": "5",
    "discount_idr": "5",
    "discount_point": 0,
    "min_usd_order_price": "25000",
    "max_usd_order_price": "55000",
    "min_idr_order_price": "1000",
    "max_idr_order_price": "2000",
    "offers": [12,13,14],
    "platforms": [1,2,4],
    "paymentgateways": [1,2,5,6,11,12,14,15,16],
    "description": "Promo Mayday"
}
```

Success response (without predefined group) :

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/campaigns/1

{
    "id": 1,
    "name": "Promo Mayday",
    "tag_name": "Promo",
    "valid_from": "2015-05-01 09:59:59",
    "valid_to": "2015-05-07 23:59:59",
    "campaign_id": 697,
    "discount_rule": 2,
    "discount_type": 1,
    "discount_status": 1,
    "discount_schedule_type": 1,
    "is_active": true,
    "discount_usd": "5",
    "discount_idr": "5",
    "discount_point": 0,
    "offers": [
        {
            "offer_id": 12,
            "offer_name": "edition6
        },
        {
            "offer_id": 13,
            "offer_name": "edition7
        },
        {
            "offer_id": 14,
            "offer_name": "edition8
        },
    ],
    "predefined_group": None,
    "platforms": [1,2,4],
    "min_usd_order_price": "25000",
    "max_usd_order_price": "55000",
    "min_idr_order_price": "1000",
    "max_idr_order_price": "2000",
    "paymentgateways": [1,2,5,6,11,12,14,15,16],
    "description": "Promo Mayday"
}
```
Request (with predefined group):

```
POST /discounts HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Promo Mayday",
    "tag_name": "Promo",
    "valid_from": "2015-05-01 09:59:59",
    "valid_to": "2015-05-07 23:59:59",
    "campaign_id": 697,
    "discount_rule": 2,
    "discount_type": 1,
    "discount_status": 1,
    "discount_schedule_type": 1,
    "is_active": true,
    "discount_usd": "5",
    "discount_idr": "5",
    "discount_point": 0,
    "predefined_group": 1,
    "min_usd_order_price": "25000",
    "max_usd_order_price": "55000",
    "min_idr_order_price": "1000",
    "max_idr_order_price": "2000",
    "offers": [12,13,14],
    "platforms": [1,2,4],
    "paymentgateways": [1,2,5,6,11,12,14,15,16],
    "description": "Promo Mayday"
}
```

Success response (with predefined group) :

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/campaigns/1

{
    "id": 2,
    "name": "Promo Mayday",
    "tag_name": "Promo",
    "valid_from": "2015-05-01 09:59:59",
    "valid_to": "2015-05-07 23:59:59",
    "campaign_id": 697,
    "discount_rule": 2,
    "discount_type": 1,
    "discount_status": 1,
    "discount_schedule_type": 1,
    "is_active": true,
    "discount_usd": "5",
    "discount_idr": "5",
    "discount_point": 0,
    "predefined_group": 1,
    "offers": [
        {
            "offer_id": 12,
            "offer_name": "edition6
        },
        {
            "offer_id": 13,
            "offer_name": "edition7
        },
        {
            "offer_id": 14,
            "offer_name": "edition8
        },
    ],
    "predefined_group": None,
    "platforms": [1,2,4],
    "min_usd_order_price": "25000",
    "max_usd_order_price": "55000",
    "min_idr_order_price": "1000",
    "max_idr_order_price": "2000",
    "paymentgateways": [1,2,5,6,11,12,14,15,16],
    "description": "Promo Mayday"
}
```

Failed response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

#### Retrieve Discounts

Retrieves discounts.

**URL:** /discounts

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Discount Properties](#discount-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Discount is retrieved and returned in body content

* `204` - No matched discount

**Response Params:** See [Discount Properties](#discount-properties)

**Examples:**

Request:

```
GET /discounts HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "discounts": [
        {
            "id": 1,
            "name": "Promo Mayday",
            "tag_name": "Promo",
            "valid_from": "2015-05-01 09:59:59",
            "valid_to": "2015-05-07 23:59:59",
            "campaign_id": 697,
            "discount_rule": 2,
            "discount_type": 1,
            "discount_status": 1,
            "discount_schedule_type": 1,
            "is_active": true,
            "discount_usd": "5",
            "discount_idr": "5",
            "discount_point": 0,
            "min_usd_order_price": "25000",
            "max_usd_order_price": "55000",
            "min_idr_order_price": "1000",
            "max_idr_order_price": "2000",
            "offers": [
                {
                    "offer_id": 12,
                    "offer_name": "edition6
                },
                {
                    "offer_id": 13,
                    "offer_name": "edition7
                },
                {
                    "offer_id": 14,
                    "offer_name": "edition8
                },
            ],
            "predefined_group": 1,
            "platforms": [1,2,4],
            "paymentgateways": [1,2,5,6,11,12,14,15,16],
            "description": "Promo Mayday"
        }
    ]
    "metadata": {
        "resultset":{
            "count": 12,
            "limit": 1,
            "offset": 0
        }
    }
}

```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Discount

Retrieve individual campaign.

**URL:** /discounts/{discount_id}

Optional:

* `fields` - Retrieve specific properties. See [Discount Properties](#discount-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Discount found and returned

**Response Params:** See [Discount Properties](#discount-properties)

**Examples:**

Full request:

```
GET /discounts/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Promo Mayday",
    "tag_name": "Promo",
    "valid_from": "2015-05-01 09:59:59",
    "valid_to": "2015-05-07 23:59:59",
    "campaign_id": 697,
    "discount_rule": 2,
    "discount_type": 1,
    "discount_status": 1,
    "discount_schedule_type": 1,
    "is_active": true,
    "discount_usd": "5",
    "discount_idr": "5",
    "discount_point": 0,
    "offers": [
        {
            "offer_id": 12,
            "offer_name": "edition6
        },
        {
            "offer_id": 13,
            "offer_name": "edition7
        },
        {
            "offer_id": 14,
            "offer_name": "edition8
        },
    ],
    "predefined_group": None,
    "platforms": [1,2,4],
    "min_usd_order_price": "25000",
    "max_usd_order_price": "55000",
    "min_idr_order_price": "1000",
    "max_idr_order_price": "2000",
    "paymentgateways": [1,2,5,6,11,12,14,15,16],
    "description": "Promo Mayday"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Discounts

Updates discount.

**URL:** /discounts/{campaign_id}

**Method:** PUT

**Response HTTP Code:**

* `200` - Discount is updated

**Response Params:** See [Discount Properties](#discount-properties)

**Examples:**

Full request:

```
PUT /campaigns/2 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Promo Mayday",
    "tag_name": "Promo",
    "valid_from": "2015-05-01 09:59:59",
    "valid_to": "2015-05-07 23:59:59",
    "campaign_id": 697,
    "discount_rule": 2,
    "discount_type": 1,
    "discount_status": 1,
    "discount_schedule_type": 1,
    "is_active": true,
    "discount_usd": "5",
    "discount_idr": "5",
    "discount_point": 0,
    "predefined_group": 1,
    "min_usd_order_price": "25000",
    "max_usd_order_price": "55000",
    "min_idr_order_price": "1000",
    "max_idr_order_price": "2000",
    "offers": [12,13,14],
    "platforms": [1,2,4],
    "paymentgateways": [1,2,5,6,11,12,14,15,16],
    "description": "Promo Mayday"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "name": "Promo Mayday",
    "tag_name": "Promo",
    "valid_from": "2015-05-01 09:59:59",
    "valid_to": "2015-05-07 23:59:59",
    "campaign_id": 697,
    "discount_rule": 2,
    "discount_type": 1,
    "discount_status": 1,
    "discount_schedule_type": 1,
    "is_active": true,
    "discount_usd": "5",
    "discount_idr": "5",
    "discount_point": 0,
    "predefined_group": 1,
    "min_usd_order_price": "25000",
    "max_usd_order_price": "55000",
    "min_idr_order_price": "1000",
    "max_idr_order_price": "2000",
    "offers": [12,13,14],
    "platforms": [1,2,4],
    "paymentgateways": [1,2,5,6,11,12,14,15,16],
    "description": "Promo Mayday"
}
```

Fail response:

```
HTTP/1.1 400 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

#### Delete Discount

Deletes discount. This service will set `is_active` value to false.

**URL:** /discounts/{discount_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Discount deleted

**Response Params:** -

**Examples:**

Full request:

```
DELETE /discounts/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Discount Codes

This API is used to manage every discount codes made regarding to any campaigns

#### Discount Codes Properties

* `discount_id` - Number; discount code identifier.

* `is_active` - Boolean; Instance's active status, used to be marked if the instance is active or soft-deleted. The default value is true.

* `discount_type` - Number; to determine whether the discount overrides the previous discount:

    * 1 - OVERIDE OFFER DISCOUNT

    * 2 - CAN'T OVERIDE OFFER DISCOUNT

* `disocunt_codes` - list of codes, consists of codes and the maximum number of use:

    * `code` - Number; the discount code used to apply any kind of discount.

    * `max_uses` - Number; the maximum number of discount availability.

#### Create Discount Codes

Creates discount code

**URL:** /discountcodes

**Method:** POST

**Request Data Params:**

Required: `discount_id`, `is_active`, `discount_type`, `discount_codes`, `code`, `max_uses`

Optional: -

**Response HTTP Code:**

* `201` - New discount code created

**Response Params:** See [Discount Codes Properties](#discount-codes-properties)

**Examples:**

Request:

```
POST /discountcodes HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "discount_id": 1,
    "is_active": true,
    "discount_type": 1,
    "discount_codes": [
        {
            "code": "XX1",
            "max_uses": 10
        },
        {
            "code": "XX2",
            "max_uses": 10
        }
    ]
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
	"discount_id": 1,
	"is_active": true,
	"discount_type": 1,
	"coupon_codes": [
        {
            "code": "XX1",
            "max_uses": 10
        },
        {
            "code": "XX2",
            "max_uses": 10
        }
    ]
}


```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}

```

#### Retrieve Discount Codes

Retrieves discount codes

**URL:** /discountcodes

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Owned Item Properties](#discount-codes-properties). Default to ALL.

* `offset` - The offset of the records. The default value is 0

* `Limit` - The limit of the records. The default value is 9999

* `order` - Order the records. The default mode is ascending order by primary identifier or ascending by created timestamp

* `q` - Simple string query to record’s string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Discount codes retrieved and returned in body content

* `204` - No matched discount codes

**Response Params:** See [Discount Codes Properties](#discount-codes-properties)

**Examples:**

Request:

```
GET /discountcodes HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "discount_codes": [
        {
            "discount_id": 1,
            "is_active": true,
            "discount_type": 1,
            "discount_codes": [
                {
                    "code": "XX1",
                    "max_uses": 10
                },
                {
                    "code": "XX2",
                    "max_uses": 10
                }
            ],
            [
        {
            "discount_id": 2,
            "is_active": true,
            "discount_type": 1,
            "discount_codes": [
                {
                    "code": "XXY1",
                    "max_uses": 10
                },
                {
                    "code": "XXY2",
                    "max_uses": 10
                }
            ]

        }
    ]
    "metadata":{
        "resultset":{
            "count": 20,
            "limit": 1,
            "offset": 0
        }
    }
}


```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Discount Code

Retrieves individual discount code by using identifier

**URL:** /discountcodes/{discount_id}

Optional:

* `fields` - Retrieve specific fields. See [Discount Code Properties](#discount-code-properties). Default to ALL.

**Method:** GET

**Response HTTP Code:**

* `200` - Discount code is retrieved and returned

**Response Params:** See [Discount Codes Properties](#discount-codes-properties)

**Examples:**

Request:

```
GET /discountcodes/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "discount_codes": [
        {
            "discount_id": 1,
            "is_active": true,
            "discount_type": 1,
            "discount_codes": [
                {
                    "code": "XX1",
                    "max_uses": 10
                },
                {
                    "code": "XX2",
                    "max_uses": 10
                }
            ]
        }
    ]
    "metadata":{
        "resultset":{
            "count": 20,
            "limit": 1,
            "offset": 0
        }
    }
}


```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Partners

Partner resource contains all associated partner. Currently this contains associated partner to preload app and profit sharing.

#### Partner Properties

* `id` - Number; Instance identifier.

* `name` - String; Partner name.

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority.

* `partner_status` - Number; Partner status in the system.

    * 1: NEW - New created vendor

    * 2: WAITING FOR REVIEW - Waiting for business side review for any kind of partnership

    * 3: IN REVIEW - In review

    * 4: REJECTED - Rejected due to some reasons

    * 5: APPROVED - Aproved by business side

    * 6: CLEAR - Clear for any action

    * 7: BANNED - Banner due to some reasons

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true.

* `slug` - String, Unique; Partner slug for SEO.

* `meta` - String; Partner meta for SEO.

* `description` - String; Partner description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URL.

* `icon_image_normal` - String; Partner icon image URI.

* `icon_image_highres` - String; Partner icon image in highres URI.

* `organization_id` - Number; Unique, organization identifier that this instance bind to. A partner can only bind to an organization.

#### Create Partner

Create new partner.

**URL:** /partners

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`, `cut_rule_id`, `organization_id`, `partner_status`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `201` - New partner created

**Response Params:** See [Partner Properties](#partner-properties)

**Examples:**

Request:

```
POST /partners HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "SONY",
    "slug": "sony",
    "sort_priority": 1,
    "partner_status": 6,
    "is_active": true,
    "description": "Sony Indonesia",
    "meta": "Sony Indonesia",
    "icon_image_normal": "partners/sony/icon.png",
    "icon_image_highres": "partners/sony/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 765
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/partners/2

{
    "id": 2,
    "name": "SONY",
    "slug": "sony",
    "sort_priority": 1,
    "partner_status": 6,
    "is_active": true,
    "description": "Sony Indonesia",
    "meta": "Sony Indonesia",
    "icon_image_normal": "partners/sony/icon.png",
    "icon_image_highres": "partners/sony/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 765
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Partners

Retrieve partners.

**URL:** /partners

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Partner Properties](#partner-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

* `organization_id` - Filter by organization identifier(s). Response will returns if any matched

**Method:** GET

**Response HTTP Code:**

* `200` - Partners retrieved and returned in body content

* `204` - No matched partner

**Response Params:** See [Partner Properties](#partner-properties)

**Examples:**

Request:

```
GET /partners?fields=id,name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "partners": [
        {
            "id": 1,
            "name": "LENOVO"
        },
        {
            "id": 2,
            "name": "SONY"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Partner

Retrieve individual partner using identifier.

**URL:** /partners/{partner_id}

**Alternative URL:** /partners/organization/{organization_id}

Optional:

* `fields` - Retrieve specific properties. See [Partner Properties](#partner-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Partner found and returned

**Response Params:** See [Partner Properties](#partner-properties)

**Examples:**

Request:

```
GET /partners/2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "name": "SONY",
    "slug": "sony",
    "sort_priority": 1,
    "partner_status": 6,
    "is_active": true,
    "description": "Sony Indonesia",
    "meta": "Sony Indonesia",
    "icon_image_normal": "partners/sony/icon.png",
    "icon_image_highres": "partners/sony/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 765
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Partner

Update partner.

**URL:** /partners/{partner_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`, `cut_rule_id`, `organization_id`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `200` - Partner updated

**Response Params:** See [Partner Properties](#partner-properties)

**Examples:**

Request:

```
PUT /partners/2 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "SONY",
    "slug": "sony",
    "sort_priority": 5,
    "partner_status": 6,
    "is_active": true,
    "description": "Sony Mobile",
    "meta": "Sony Mobile",
    "icon_image_normal": "partners/sony/icon.png",
    "icon_image_highres": "partners/sony/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 765
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "name": "SONY",
    "slug": "sony",
    "sort_priority": 5,
    "partner_status": 6,
    "is_active": true,
    "description": "Sony Mobile",
    "meta": "Sony Mobile",
    "icon_image_normal": "partners/sony/icon.png",
    "icon_image_highres": "partners/sony/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 765
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Partner

Delete partner. This service will set `is_active` value to false.

**URL:** /partners/{partner_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Partner deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /partners/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```
### Banners

Banner resource contains all the properties within the banners shown on the SCOOP webstore/applications.

#### Banner Properties

* `id` - Number; Instance identifier.

* `media_base_url` - String; The Base URL for image files, this string must be appended to image URI.

* `image_highres` - String; The URL where the image file (in high resolution) of the banner is stored

* `image_normal` - String; The URL where the image file (in normal resolution) of the banner is stored

* `countries` - Array; Countries where the banner is shown, all the country codes are listed in ISO 3166-1 alpha-2 format.

* `banner_type` - Number; Type of the banner that determines the value of `payload`

    * 1: LANDING PAGE

    * 2: OPEN URL

    * 3: STATIC

    * 4: BRAND

* `payload` - String; What lies behind the banner. Payload depends on the banner type. e.g:

    * `banner_type` = 1 : "567"; The banner refers to any campaign which id is 567

    * `banner_type` = 2 : "http://www.getscoop.com/id/majalah/kategori/teen"; The banner refers to the aforementioned URL

    * `banner_type` = 3 : NULL; The banner does nothing, just a plain banner

    * `banner_type` = 4 : "17"; The banner refers to all items which brand id is 17

* `valid_from` -  String; Datetime in UTC the banner is valid from

* `valid_to` -  String; Datetime in UTC the banner is valid to

* `clients` - Number; List of client identifier(s). Used to identify on which client(s) the banner will be shown.

* `zone` - Strings; Where the banner will be shown, taken from the following

    * `homepage` (default)

    * `magazines page`

    * `books page`

    * `newspapers page`

    * `publisher page`

    * `promo page`

* `sort_priority` - Number; Used to sort the sequence of the banners. The higher the number, the higher the priority. The banners are sorted based on the priority.

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. The default value is true.

* `created` - String; Datetime in UTC the banner is created

* `modified` - String; Datetime in UTC the banner is modified

* `description` - String; Detailed explanation about the banner

#### Create Banner

Creates new banner.

**URL:** /banners

**Method:** POST

**Request Data Params:**

Required: `banner_type`, `is_active`, `clients`, `image_highres`, `image_normal`

Optional: `payload`, `valid_from`, `valid_to`, `sort_priority`, `description`

**Response HTTP Code:**

* `201` - New banner is created

**Response Params:** See [Banner Properties](#banner-properties)

**Examples:**

Request:

```
POST /banners HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "banner_type": 1,
    "payload": "567",
    "valid_from": "2014-12-01T00:00:00",
    "valid_to": "2015-01-01T16:59:59",
    "is_active": true,
    "sort_priority": 1,
    "countries": ["NZ", "UK"],
    "clients": [1,2,3],
    "zone": "homepage",
    "image_highres": "iqwioq43oy834y5kj2423n4jk2i42i34g2j3g4j2f34yf23jh42.......",
    "image_normal": "iqwioq43oy834y5kj2423n4jk2i42i34g2j3g4j2f34yf23jh42.......",
    "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/banner/2

{
    "banner_type" : 1,
    "payload" : "567",
    "valid_from" : "2014-12-01T17:00:00",
    "valid_to" : "2015-01-01T16:59:59",
    "is_active": true,
    "sort_priority": 1,
    "countries" : ["NZ", "UK"],
    "clients": [1,2,3],
    "zone": "homepage",
    "created": "2014-12-01T05:12:12",
    "modified": "2014-12-01T05:12:12",
    "id": 2,
    "image_highres": "../images/xxx_hres.png",
    "image_normal": "../images/xxx.png",
    "media_base_url": "http://images.getscoop.com/magazine_static/",
    "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Banners

Retrieves all of the banners.

**URL:** /banners

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `client_id` - Number; List of client identifier(s). Used to identify on which client(s) the banner will be shown.

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

**Method:** GET

**Response HTTP Code:**

* `200` - Banners are retrieved and returned in body content

* `204` - No matched banner

**Response Params:** See [Banner Properties](#banner-properties)

**Examples:**

Request:

```
GET /banners?is_active=true&client_id=1,2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "banners": [
        {
            "banner_type": 1,
            "payload": "567",
            "valid_from": "2014-12-01T17:00:00",
            "valid_to": "2015-01-01T16:59:59",
            "is_active": true,
            "sort_priority": 1,
            "countries": ["NZ", "UK"],
            "clients": [1,2],
            "zone": "homepage",
            "created": "2014-12-01T05:12:12",
            "modified": "2014-12-01T05:12:12",
            "id": 3,
            "image_highres": "../images/xxx_hres.png",
            "image_normal": "../images/xxx.png",
            "media_base_url": "http://images.getscoop.com/magazine_static/",
            "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
        },
        {
            "banner_type": 2,
            "payload": "http://www.getscoop.com/id/majalah/kategori/teen",
            "valid_from": "2014-12-01T17:00:00",
            "valid_to": "2015-01-01T16:59:59",
            "is_active": true,
            "sort_priority": 1,
            "countries": ["ID", "MY"],
            "clients": [1],
            "zone": "homepage",
            "created": "2014-12-01T05:12:12",
            "modified": "2014-12-01T05:12:12",
            "id": 4,
            "image_highres": "../images/xcx_hres.png",
            "image_normal": "../images/xcx.png",
            "media_base_url": "http://images.getscoop.com/magazine_static/",
            "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Banner

Retrieves individual banner by using identifier.

**URL:** /banners/{banner_id}

Optional:

* `fields` - Retrieve specific properties. See [Banner Properties](#banner-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Banner is found and returned

**Response Params:** See [Banner Properties](#banner-properties)

**Examples:**

Request:

```
GET /banner/2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "banner_type": 1,
    "payload": "567",
    "valid_from": "2014-12-01T17:00:00",
    "valid_to": "2015-01-01T16:59:59",
    "is_active": true,
    "sort_priority": 1,
    "countries": ["NZ", "UK"],
    "clients": [1,2,3],
    "zone": "homepage",
    "created": "2014-12-01T05:12:12",
    "modified": "2014-12-01T05:12:12",
    "id": 2,
    "image_highres": "../images/xxx_hres.png",
    "image_normal": "../images/xxx.png",
    "media_base_url": "http://images.getscoop.com/magazine_static/",
    "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Banner

Updates banner.

**URL:** /banners/{banner_id}

**Method:** PUT

**Request Data Params:**

Required: `banner_type`, `is_active`, `clients`

Optional: `payload`, `valid_from`, `valid_to`, `sort_priority`

**Response HTTP Code:**

* `200` - Banner updated

**Response Params:** See [banner Properties](#banner-properties)

**Examples:**

Request:

```
PUT /banners/2 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "banner_type": 1,
    "payload": "567",
    "valid_from": "2014-12-01T17:00:00",
    "valid_to": "2015-01-01T16:59:59",
    "is_active": true,
    "sort_priority": 1,
    "countries": ["NZ", "UK"],
    "clients": [1,2,3],
    "zone": "homepage",
    "created": "2014-12-01T05:12:12",
    "modified": "2014-12-01T05:12:12",
    "id": 2,
    "image_highres": "../images/xxx_hres.png",
    "image_normal": "../images/xxx.png",
    "media_base_url": "http://images.getscoop.com/magazine_static/",
    "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "banner_type": 1,
    "payload": "567",
    "valid_from": "2014-12-01T17:00:00",
    "valid_to": "2015-01-01T16:59:59",
    "is_active": true,
    "sort_priority": 1,
    "countries": ["NZ", "UK"],
    "clients": [1,2,3],
    "zone": "homepage",
    "created": "2014-12-01T05:12:12",
    "modified": "2014-12-01T05:12:12",
    "id": 2,
    "image_highres": "../images/xxx_hres.png",
    "image_normal": "../images/xxx.png",
    "media_base_url": "http://images.getscoop.com/magazine_static/",
    "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Banner

Delete banner. This service will set `is_active` value to false.

**URL:** /banners/{banner_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Banner deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /banners/2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

---

## ORDERS, REFUNDS, AND PAYMENTS

### Orders

Order resource, used to order the offers. For consumer clients, [Checkout Order](#checkout-order) and [Confirm Order](#confirm-order) must be called to confirm the order before continue to payment process.

General consumer client order use-case flow:
* 1. [Checkout Order](#confirm-order)
* 2. [Confirm Order](#checkout-order)
* ...continue to payment process

#### Order Properties

* `order_id` - Number; Instance identifier.

* `temp_order_id` - Number; Temporarily instance identifier. Please note this temporarily identifier returned on checkout process will be required on order confirmation.

* `user_id` - Number; User identifier that will paid and eligible for the completed order later.

* `total_amount` - String; Order base total amount before any discount applied, this property is only available if payment gateway already specified in request.

* `final_amount` - String; Order final amount after all discounts applied, this property is only available if payment gateway already specified in request.

* `total_amount_point` - String; Order total amount in SCOOP Point before any discount applied, this property is only available if no payment gateway already specified in request.

* `final_amount_point` - String; Order final amount in SCOOP Point after all discounts applied, this property is only available if no payment gateway already specified in request.

* `total_amount_usd` - String; Order total amount in USD before any discount applied, this property is only available if no payment gateway already specified in request.

* `final_amount_usd` - String; Order final amount in USD after all discounts applied, this property is only available if no payment gateway already specified in request.

* `total_amount_idr` - String; Order total amount in IDR before any discount applied, this property is only available if no payment gateway already specified in request.

* `final_amount_idr` - String; Order final amount in IDR after all discounts applied, this property is only available if no payment gateway already specified in request.

* `tier_code` - String; Tier code to use in payment process, this property is only for tiered payment with specified platform.

* `is_active` - Boolean; Instance's status, used to mark if the instance is active or soft-deleted. Default to true.

* `payment_gateway_id` - Number; Payment gateway identifier. Payment gateway when checkout and when confirmed must be the same, later, payment method must match this specified payment gateway.

* `payment_gateway_name` - String; Payment gateway name.

* `platform_id` - Number; Platform identifier. Used to identify from which platform this order come from, also used to calculate the platform specific discount.

* `client_id` - Number; Client identifier. Used to identify from which client this order come from.

* `order_status` - Number; Order status. Showing order statuses in order, payment, delivery, refund, and cancellation states.

    * 10000: TEMP - Temporarily order, this status marks the order on checking out state and before confirmation.

    * 10001: NEW - New confirmed order, this status marks the order is confirmed and ready to go into payment flow.

    * 20001: WAITING FOR PAYMENT - Order ready for payment, this status marked by payment process on authorization request and before call to payment (charge) process, can be skipped for payment that has no authorization flow or payment with charging process called on front-end.

    * 20002: PAYMENT IN PROCESS - Payment on going, this status marked right before call for payment charging, can be skipped for payment with charging process called on front-end.

    * 20003: PAYMENT BILLED - Payment already charged, this status marked right away after the payment gateway returns success charging process.

    * 30001: WAITING FOR DELIVERY - Marked right as the delivery function starts.

    * 30002: DELIVERY IN PROCRESS - Status to marks delivery process is on going.

    * 30003: DELIVERED - Status showing that eligible items delivered to user, this status merked right away after all save process to user completed

    * 40001: WAITING FOR REFUND - Order waiting for refund process. This only can occure to order that already billed. Only for internal use.

    * 40002: REFUND IN PROCESS - Order in process of refund.

    * 40003: REFUNDED - Refund completed.

    * 50000: CANCELLED - Order cancelled by consumer user side before payment flow. This only included if the payment gateway provide cancellation service.

    * 50001: PAYMENT ERROR - Payment error status, for detected fraud payment, payment gateway server issue, unmatched charged amount, retrieve or save data error (DB's table level fail), etc. Details appended to `note` property.

    * 50002: DELIVERY ERROR - Delivery error status, for missing items, item not for sale anymore, retrieve or save data error (DB's table level fail), etc. Details appended to `note` property.

    * 50003: REFUND ERROR - Refund error status, retrieve or save data error (DB's table level fail), etc. Details appended to `note` property.

    * 90000: COMPLETE - All order process completed.

* `point_reward` - Number; SCOOP Points that rewarded on this order completion. This property only available after order confirmed.

* `order_number` - Number; Order alternative identifier for consumer-user communication purpose.

* `partner_id` - Number; Partner identifier to identify from which preloaded-partnership client this order come from.

* `currency_code` - String; Currency code of total_amount and final_amount.

* `user_email` - String; User email of this order.

* `user_name` - String; User full name of this order.

* `user_street_address` - String; User street address of this order.

* `user_city` - String; User city of this order.

* `user_zipcode` - String; User zipcode of this order.

* `user_state` - String; User state of this order.

* `user_country` - String; User country of this order.

* `latitude` - String; User current latitude of this order.

* `longitude` - String; User current longitude of this order.

* `note` - String; Note for this order.

* `ip_address` - String; Client IP address of this order.

* `os_version` - String; Client OS version of this order.

* `client_version` - String; Client version of this order.

* `device_model` - String; Client device model of this order.

* `order_lines` - Array; Lines of the order.

    * `id` - Number; Order line identifier.

    * `name` - String; Order line name, equals to offer's name See [Offer Properties](#offer-properties).

    * `offer_id` - Number; Offer identifier.

    * `quantity` - Number; Quantity of the ordered offer, currently this parameter will only contains 1 as value.

    * `raw_price` - String; Order line raw price before any discount applied, this property is only available if payment gateway already specified in request.

    * `final_price` - String; Order line final price after all discounts applied, this property is only available if payment gateway already specified in request.

    * `raw_price_point` - String; Order line raw price in SCOOP Point before any discount applied, this property is only available if no payment gateway already specified in request.

    * `final_price_point` - String; Order line final price in SCOOP Point after all discounts applied, this property is only available if no payment gateway already specified in request.

    * `raw_price_usd` - String; Order line raw price in USD before any discount applied, this property is only available if no payment gateway already specified in request.

    * `final_price_usd` - String; Order line final price in USD after all discounts applied, this property is only available if no payment gateway already specified in request.

    * `raw_price_idr` - String; Order line raw price in IDR before any discount applied, this property is only available if no payment gateway already specified in request.

    * `final_price_idr` - String; Order line final price in IDR after all discounts applied, this property is only available if no payment gateway already specified in request.

    * `currency_code` - String; Currency code of raw_price and final_price.

    * `order_line_status` - Number; Order line status. Showing order line statuses in order, delivery, and refund states. This status only used as supplement to Order's statuses.

        * 10000: TEMP - Temporarily order line, this status marks the order line on checking out state and before confirmation.

        * 10001: NEW - New confirmed order line, this status marks the order line is confirmed and ready to go into payment flow.

        * 30003: DELIVERED - Status showing that eligible items delivered to user, this status merked right away after all save process to user completed

        * 40003: REFUNDED - Refund completed.

        * 50000: CANCELLED - Order cancelled by consumer user side before payment flow. This only included if the payment gateway provide cancellation service.

        * 50002: DELIVERY ERROR - Delivery error status, for missing items, item not for sale anymore, retrieve or save data error (DB's table level fail), etc.

        * 50003: REFUND ERROR - Refund error status, retrieve or save data error (DB's table level fail), etc.

        * 90000: COMPLETE - All order line process completed.

#### Retrieve Individual Order

Retrieve individual order with identifier or order number. Only confirmed order can be retrieved from this resource, to retrieve temporarily order, see [Retrieve Individual Temporarily Order](#retrieve-individual-temporarily-order)

Clients are encouraged to use order_id for this retrieval purpose, order_number is prepared for end-user and customer support communication purpose.

**URL:** /orders/{order_id}

**Alternative URL:** /orders/order_number/{order_number}

**Method:** GET

**Response HTTP Code:**

* `200` - OK

**Examples:**

Request:

```
GET /orders/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    "payment_gateways": {
        "id": 14,
        "name": "mandiri e-cash"
    },
    "user_id": 290411,
    "total_amount": "9000.00",
    "tier_code": null,
    "order_id": 2,
    "partner_id": null,
    "is_active": true,
    "final_amount": "9000.00",
    "platform_id": null,
    "payment_gateway_id": 14,
    "temp_order_id": 2,
    "platform": null,
    "client_id": 7,
    "point_reward": 9,
    "order_status": 1,
    "order_number": 48391064664108104,
    "currency_code": "IDR",
    "user_email": "brijap@apps-foundry.com",
    "user_name": "Brian Japutra",
    "user_street_address": "Kolong Langit St. #322-12",
    "user_city": "Jakarta Timur Laut",
    "user_zipcode": "14450",
    "user_state": "Indonesia",
    "user_country": "Indonesia",
    "latitude": "1.214112",
    "longitude": "94.102362",
    "note": null,
    "ip_address": "192.168.0.1",
    "os_version": "8.1.2",
    "client_version": "4.1.0",
    "device_model": "iPad6,1",
    "order_lines": [{
        "user_id": 290411,
        "name": "Single Edition",
        "discounts": false,
        "order_id": 2,
        "offer_id": 47892,
        "is_active": true,
        "campaign_id": null,
        "is_free": false,
        "order_line_status": 1,
        "price": "9000.00",
        "payment_gateway_id": 14,
        "final_price": "9000.00",
        "id": 2,
        "currency_code": "IDR",
        "quantity": 1
    }]
}
```

#### Retrieve Individual Temporarily Order

Retrieve individual temporarily order with temporarily identifier or order number. Older records will be removed periodically.

Clients are encouraged to use temp_order_id for this retrieval purpose, order_number is prepared for end-user and customer support communication purpose.

**URL:** /temp_orders/{temp_order_id}

**Alternative URL:** /temp_orders/order_number/{order_number}

**Method:** GET

**Response HTTP Code:**

* `200` - OK

**Examples:**

Request:

```
GET /temp_orders/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json

{
    "user_id": 112,
    "payment_gateway_id": 1,
    "client_id": 1,
    "currency": "USD",
    "final_price": "13.98",
    "order_number": 296774314105186,
    "order_id": 20443
}

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "temp_order_id": 55,
    "user_id": 112,
    "payment_gateway_id": 1,
    "total_amount": "13.98",
    "currency_code": "USD",
    "platform_id": 1,
    "tier_code": ".c.usd.13.99",
    "order_number": 296774314105186,
    "order_lines": [
        {
            "name": "GOLFMAGZ - 6 Editions / 6 Months",
            "items": [
                {
                    "is_featured": false,
                    "name": "GOLFMAGZ / NOV 2012",
                    "countries": [
                        {
                            "id": 1,
                            "name": "Indonesia"
                        }
                    ],
                    "release_date": "2012-11-01T00:00:00",
                    "item_status": 9,
                    "languages": [
                        {
                            "id": 2,
                            "name": "Indonesian"
                        }
                    ],
                    "vendor": {
                        "id": 13,
                        "name": "IMP"
                    },
                    "authors": [],
                    "edition_code": "ID_GLM2012MTH11"
                }
            ],
            "offer_id": 1,
            "is_free": false,
            "currency_code": "USD",
            "final_price": "4.99",
            "raw_price": "4.99"
        },
        {
            "name": "GOLFMAGZ - 12 Editions / 12 Months",
            "items": [
                {
                    "is_featured": false,
                    "name": "GOLFMAGZ / NOV 2012",
                    "countries": [
                        {
                            "id": 1,
                            "name": "Indonesia"
                        }
                    ],
                    "release_date": "2012-11-01T00:00:00",
                    "item_status": 9,
                    "languages": [
                        {
                            "id": 2,
                            "name": "Indonesian"
                        }
                    ],
                    "vendor": {
                        "id": 13,
                        "name": "IMP"
                    },
                    "authors": [],
                    "edition_code": "ID_GLM2012MTH11"
                }
            ],
            "offer_id": 2,
            "is_free": false,
            "currency_code": "USD",
            "final_price": "8.99",
            "raw_price": "8.99"
        }
    ],
    "payment_gateway_name": "Apple In-App Purchase",
    "final_amount": "13.98",
    "user_email": "brijap@apps-foundry.com",
    "user_name": "Brian Japutra",
    "user_street_address": "Kolong Langit St. #322-12",
    "user_city": "Jakarta Timur Laut",
    "user_zipcode": "14450",
    "user_state": "Indonesia",
    "user_country": "Indonesia",
    "latitude": "1.214112",
    "longitude": "94.102362",
    "note": null,
    "ip_address": "192.168.0.1",
    "os_version": "8.1.2",
    "client_version": "4.1.0",
    "device_model": "iPad6,1"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Checkout Order

Checkout order called to check the order details before confirmed. At backend, this order is recorded in temporarily table, separated from main order records.

**URL:** /orders/checkout

**Method:** POST

**Request Data Params:**

Required: `user_id`, `client_id`, `platform_id`, `order_lines`

Optional: `payment_gateway_id`, `partner_id`, `user_email`, `user_name`, `user_street_address`, `user_city`, `user_zipcode`, `user_state`, `user_country`, `latitude`, `longitude`,`note`,`ip_address`,`os_version`,`client_version`,`device_model`, `recipient_email`

Required in `order_line`: `offer_id`, `name`, `quantity`

Optional in `order_line`: `discount_code`

**Response HTTP Code:**

* `200` - OK

**Examples:**

Request:

```
POST /orders/checkout HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "user_id": 112,
    "client_id": 1,
    "platform_id": 1,
    "user_email": "brijap@apps-foundry.com",
    "user_name": "Brian Japutra",
    "user_street_address": "Kolong Langit St. #322-12",
    "user_city": "Jakarta Timur Laut",
    "user_zipcode": "14450",
    "user_state": "Indonesia",
    "user_country": "Indonesia",
    "note": null,
    "os_version": "8.1.2",
    "client_version": "4.1.0",
    "device_model": "iPad6,1",
    "recipient_email" : "bj@apps-foundry.com",
    "order_lines": [
        {
            "offer_id": 1,
            "name": "GOLFMAGZ / NOV 2012 - Single Edition",
        },
        {
            "offer_id": 2,
            "name": "GOLFMAGZ / DEC 2012 - Single Edition",
            "discount_code": "MH-10101"
        }
    ]
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "partner_id": null,
    "total_amount_idr": "126000.00",
    "user_id": 112,
    "client_id": 1,
    "order_number": 66655510271053102,
    "is_active": true,
    "final_amount_idr": "126000.00",
    "platform_id": 1,
    "tier_code": ".c.usd.13.99",
    "total_amount_point": 1380,
    "final_amount_usd": "13.98",
    "order_status": 10000,
    "temp_order_id": 82,
    "total_amount_usd": "13.98",
    "final_amount_point": 1380,
    "user_email": "brijap@apps-foundry.com",
    "user_name": "Brian Japutra",
    "user_street_address": "Kolong Langit St. #322-12",
    "user_city": "Jakarta Timur Laut",
    "user_zipcode": "14450",
    "user_state": "Indonesia",
    "user_country": "Indonesia",
    "latitude": null,
    "longitude": null,
    "note": null,
    "ip_address": null,
    "os_version": "8.1.2",
    "client_version": "4.1.0",
    "device_model": "iPad6,1"
    "order_lines": [{
        "total_amount_idr": "45000.00",
        "name": "GOLFMAGZ - 6 Editions / 6 Months",
        "items": [{
            "is_featured": false,
            "vendor": {
                "id": 13,
                "name": "IMP"
            },
            "name": "GOLFMAGZ / NOV 2012",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2012-11-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "edition_code": "ID_GLM2012MTH11"
        }],
        "offer_id": 1,
        "final_amount_idr": "45000.00",
        "is_free": false,
        "total_amount_point": 490,
        "final_amount_usd": "4.99",
        "total_amount_usd": "4.99",
        "final_amount_point": 490
    }, {
        "total_amount_idr": "81000.00",
        "name": "GOLFMAGZ - 12 Editions / 12 Months",
        "items": [{
            "is_featured": false,
            "vendor": {
                "id": 13,
                "name": "IMP"
            },
            "name": "GOLFMAGZ / NOV 2012",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2012-11-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "edition_code": "ID_GLM2012MTH11"
        }],
        "offer_id": 2,
        "final_amount_idr": "81000.00",
        "is_free": false,
        "total_amount_point": 890,
        "final_amount_usd": "8.99",
        "total_amount_usd": "8.99",
        "final_amount_point": 890
    }]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Confirm Order

Confirms any checked out order from [Checkout Order](#checkout-order) before proceeding to payment process. Note: all passed parameter in request must conform to the last value of checkout response's parameters.

**URL:** /orders/confirm

**Method:** POST

**Request Data Params:**

Required: `user_id`, `payment_gateway_id`, `client_id`, `temp_order_id`, `final_amount`, `order_number`, `currency_code`

**Response HTTP Code:**

* `200` - OK

**Examples:**

Request:

```
POST /orders/confirm HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "user_id": 682,
    "payment_gateway_id": 9,
    "client_id": 1,
    "currency_code": "PTS",
    "final_amount": "95.0",
    "order_number": 97749913159435,
    "temp_order_id": 59
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "payment_gateway_name": "SCOOP Point",
    "user_id": 682,
    "total_amount": "95.00",
    "order_id": 43,
    "partner_id": null,
    "is_active": true,
    "final_amount": "95.00",
    "payment_gateway_id": 9,
    "temp_order_id": 59,
    "client_id": 1,
    "point_reward": 0,
    "order_status": 90000,
    "order_number": 97749913159435,
    "currency_code": "PTS",
    "user_email": "brijap@apps-foundry.com",
    "user_name": "Brian Japutra",
    "user_street_address": "Kolong Langit St. #322-12",
    "user_city": "Jakarta Timur Laut",
    "user_zipcode": "14450",
    "user_state": "Indonesia",
    "user_country": "Indonesia",
    "latitude": null,
    "longitude": null,
    "note": null,
    "ip_address": null,
    "os_version": "8.1.2",
    "client_version": "4.1.0",
    "device_model": "iPad6,1"
    "order_lines": [{
        "name": "PANORAMA / NOV\u2013DEC 2011 - Single Edition",
        "items": [{
            "is_featured": false,
            "vendor": {
                "id": 10,
                "name": "Panorama Multi Media"
            },
            "name": "PANORAMA / NOV\u2013DEC 2011",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2011-11-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "id": 1260,
            "edition_code": "ID_PAN2011MTH1112ED26"
        }],
        "offer_id": 2122,
        "is_free": false,
        "final_price": "95.00",
        "currency_code": "PTS",
        "raw_price": "95.00"
    }]
}
```

#### Order Transaction Histories

Retrieves transaction histories based on certain user

**URL:** /orders/transactions/{user_id}

**Method:** GET

**Response HTTP Code:**

* `200` - OK

**Examples:**

Request:

```
GET /orders/transactions/401857 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    "transactions": [
        {
        "user_id": 401857,
        "total_amount": "590.00",
        "created": "2015-02-09T04:26:04.256443",
        "order_id": 97435,
        "final_amount": "590.00",
        "platform_id": 4,
        "payment_gateway_id": 9,
        "point_reward": 0,
        "order_status": 90000,
        "order_number": 104269439731296,
        "currency_code": "PTS",
        "order_lines": [
            {
            "offer_quantity": 5,
            "item_ids": [
                59001,
                59002,
                59000,
                59003,
                58999
            ],
            "price": "590.00",
            "offer_quantity_unit": 1,
            "offer_type_id": 3,
            "id": 108565,
            "on_going": 0,
            "final_price": "590.00",
            "offer_name": "POPULAR Babes From Net - Bundle 5in1 - Goddess",
            "offer_id": 38255
            }
        ]
      }
    ],
    "metadata": {
        "resultset": {
            "count": 1,
            "limit": 20,
            "offset": 0
        }
    },
}
```

### Refunds

Refunds resource contains all the properties within the transaction refunds.

#### Refund Properties

* `id` - Number; Instance identifier

* `pic_id` - Number; User identifier

* `origin_payment_id` - Number; Payment identifier

* `origin_order_id` -  Number; Order identifier

* `origin_order_line_id` -  Number; Order line identifier

* `refund_amount` -  String; Amount of the transaction refund in float format

* `refund_currency_id` - Number; Currency identifier

* `refund_reason` - Number; Reasons why the transaction is refunded

    * 1: Duplicate Payment

    * 2: Fraud Payment

    * 3: System Error

    * 4: Wrong Purchase

    * 5: Subcription is Discontinued

    * 6: Marketing Campaign Price Issues

    * 99: Others

* `refund_datetime` -  String; Datetime in UTC when the refund takes place

* `finance_status` - Number; Status of the transaction refund

    * 1: REFUND PENDING

    * 2: REFUND IN PROGRESS

    * 3: REFUND IS COMPLETED

* `ticket_number` -  String; JIRA ticket where the transaction refund is requested

* `note` -  String; Additional description

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. The default value is true.

#### Create Refund

Makes new transaction refund.

**URL:** /refunds

**Method:** POST

**Request Data Params:**

Required: `pic_id`, `origin_payment_id`, `origin_order_id`, `origin_order_line_id`, `refund_amount`, `refund_currency_id`, `refund_reason`, `refund_datetime`, `finance_status`

Optional: `ticket_number`, `note`, `is_active`

**Response HTTP Code:**

* `201` - New refund data is created

**Response Params:** See [Refund Properties](#refund-properties)

**Examples:**

Request:

```
POST /refunds HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "pic_id": 379890,
    "origin_payment_id": 45715,
    "origin_order_id": 46661,
    "origin_order_line_id": 55835,
    "refund_amount": "0.99",
    "refund_currency_id": 1,
    "refund_reason": 1,
    "refund_datetime": "2014-06-01T00:00:00Z",
    "finance_status": 1,
    "ticket_number": "JIRA-9000",
    "note": "loremipsum",
    "is_active": true
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/refund/2

{
    "id": 1,
    "pic_id": 379890,
    "origin_payment_id": 45715,
    "origin_order_id": 46661,
    "origin_order_line_id": 55835,
    "refund_amount": "0.99",
    "refund_currency_id": 1,
    "refund_reason": 1,
    "refund_datetime": "2014-06-01T00:00:00Z",
    "finance_status": 1,
    "ticket_number": "JIRA-9000",
    "note": "loremipsum",
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Refunds

Retrieves all of the transaction refunds.

**URL:** /refunds

Optional:

* `pic_id` - Number; Filter by user identifier

* `origin_payment_id` - Number; Filter by payment identifier

* `origin_order_id` -  Number; Filter by order identifier

* `origin_order_line_id` -  Number; Filter by order line identifier

* `refund_amount` -  String; Filter by amount of the transaction refund in float format

* `refund_currency_id` - Number; Filter by currency identifier

* `refund_reason` - Number; Reasons why the transaction is refunded

    * 1: Duplicate Payment

    * 2: Fraud Payment

    * 3: System Error

    * 4: Wrong Purchase

    * 5: Subcription is Discontinued

    * 6: Marketing Campaign Price Issues

    * 99: Others

* `refund_datetime_lt`, `refund_datetime_lte`, `refund_datetime_gt`, `refund_datetime_gte` -  String; Datetime in UTC when the refund takes place

* `finance_status` - Number; Status of the transaction refund

    * 1: REFUND PENDING

    * 2: REFUND IN PROGRESS

    * 3: REFUND IS COMPLETED

* `expand` - Expand the result details. Default to null, the supported expand for items are as below

    * ext: get all related filters as object or array of objects contains identifier and name. See [Item Properties](#item-properties)

    * display_offers: get display offers as array and also offer's meta informations (this expand value is covered in "ext")

    * parental_control: get related parental_control as object (this expand value is covered in "ext")

    * item_type: get related item_type as object (this expand value is covered in "ext")

    * brand: get related brand as object (this expand value is covered in "ext")

    * vendor: get related vendor as object (this expand value is covered in "ext")

    * authors: get all related authors as array of objects (this expand value is covered in "ext")

    * categories: get all related categories as array of objects (this expand value is covered in "ext")

    * languages: get all related languages as array of objects (this expand value is covered in "ext")

    * countries: get all related countries as array of objects (this expand value is covered in "ext")

* `is_active` - Filter by is_active status(es).

* `client_id` - Number; List of client identifier(s). Used to identify on which client(s) the refund will be shown.

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

**Method:** GET

**Response HTTP Code:**

* `200` - Refund data are retrieved and returned in body content

* `204` - No matched refund

**Response Params:** See [Refund Properties](#refund-properties)

**Examples:**

Request:

Retrieve refund based on the status and the currency id

```
GET /refunds?is_active=true&refund_currency_id=2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Retrieve refund with expand = ext

```
GET /refunds?id=2&expand=ext&limit=1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response (default properties):

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "refunds": [
        {
            "id": 9,
            "pic_id": 379890,
            "origin_payment_id": 45715,
            "origin_order_id": 46661,
            "origin_order_line_id": 55835,
            "refund_amount": "0.99",
            "refund_currency_id": 2,
            "refund_reason": 1,
            "refund_datetime": "2014-06-01T00:00:00Z",
            "finance_status": 1,
            "ticket_number": "JIRA-9000",
            "note": "loremipsum",
            "is_active": true
        },
        {
            "id": 10,
            "pic_id": 379890,
            "origin_payment_id": 45715,
            "origin_order_id": 46661,
            "origin_order_line_id": 55835,
            "refund_amount": "0.99",
            "refund_currency_id": 2,
            "refund_reason": 1,
            "refund_datetime": "2014-06-01T00:00:00Z",
            "finance_status": 1,
            "ticket_number": "JIRA-9000",
            "note": "loremipsum",
            "is_active": true
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Success response with "ext" as expand value:

```
{
    "refunds": [
        {
            "origin_orderline_id": 55835,
            "finance_status": 1,
            "order_line": {
                "is_discount": false,
                "user_id": 379890,
                "name": "Nyata / ED 2267 2014",
                "order_id": 46661,
                "offer_id": 36056,
                "is_active": true,
                "campaign_id": null,
                "is_free": false,
                "order_line_status": 90000,
                "price": "0.99",
                "payment_gateway_id": 15,
                "final_price": "0.99",
                "id": 55835,
                "currency_code": "USD",
                "quantity": 1
                },
            "is_active": true,
            "refund_amount": "0.99",
            "payment": {
                "payment_datetime": "2014-12-16T09:05:05.372397",
                "financial_archive_date": null,
                "user_id": 379890,
                "created": "2014-12-16T09:04:29.460938",
                "order_id": 46661,
                "payment_status": 20003,
                "modified": "2014-12-16T09:05:06.406177",
                "amount": "0.99",
                "paymentgateway_id": 15,
                "id": 45715,
                "currency_code": "USD"
                },
            "note": "loremipsum",
            "refund_datetime": "2014-06-01T00:00:00",
            "origin_payment_id": 45715,
            "pic_id": 379890,
            "ticket_number": "JIRA-9000",
            "origin_order_id": 46661,
            "order": {
                "payment_id": 45715,
                "user_id": 379890,
                "total_amount": "0.99",
                "temporder_id": 54681,
                "tier_code": ".c.usd.0.99",
                "order_id": 46661,
                "partner_id": null,
                "is_active": true,
                "final_amount": "0.99",
                "platform_id": 2,
                "payment_gateway_id": 15,
                "client_id": 2,
                "point_reward": 9,
                "order_status": 90000,
                "order_number": 637123472741109,
                "currency_code": "USD"
                },
            "refund_reason": 1,
            "id": 2
        }
    ],
    "metadata": {
        "resultset": {
            "count": 7,
            "limit": 1,
            "offset": 0
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Refund

Retrieves individual refund by using identifier.

**URL:** /refunds/{refund_id}

Optional:

* `fields` - Retrieve specific properties. See [Refund Properties](#refund-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Refund data is found and returned

**Response Params:** See [Refund Properties](#refund-properties)

**Examples:**

Request:

```
GET /refund/2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "pic_id": 379890,
    "origin_payment_id": 45715,
    "origin_order_id": 46661,
    "origin_order_line_id": 55835,
    "refund_amount": "0.99",
    "refund_currency_id": 2,
    "refund_reason": 1,
    "refund_datetime": "2014-06-01T00:00:00Z",
    "finance_status": 1,
    "ticket_number": "JIRA-9000",
    "note": "loremipsum",
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Refund

Updates refund data.

**URL:** /refunds/{refund_id}

**Method:** PUT

**Request Data Params:**

Required: `pic_id`, `origin_payment_id`, `origin_order_id`, `origin_order_line_id`, `refund_amount`, `refund_currency_id`, `refund_reason`, `refund_datetime`, `finance_status`

Optional: `ticket_number`, `note`, `is_active`

**Response HTTP Code:**

* `200` - Refund is updated

**Response Params:** See [Refund Properties](#refund-properties)

**Examples:**

Request:

```
PUT /refunds/2 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "pic_id": 379890,
    "origin_payment_id": 45715,
    "origin_order_id": 46661,
    "origin_order_line_id": 55835,
    "refund_amount": "0.99",
    "refund_currency_id": 2,
    "refund_reason": 99,
    "refund_datetime": "2014-06-01T00:00:00Z",
    "finance_status": 1,
    "ticket_number": "JIRA-9000",
    "note": "loremipsum",
    "is_active": true
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "pic_id": 379890,
    "origin_payment_id": 45715,
    "origin_order_id": 46661,
    "origin_order_line_id": 55835,
    "refund_amount": "0.99",
    "refund_currency_id": 2,
    "refund_reason": 99,
    "refund_datetime": "2014-06-01T00:00:00Z",
    "finance_status": 1,
    "ticket_number": "JIRA-9000",
    "note": "loremipsum",
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Refund

Delete refund. This service will set `is_active` value to false.

**URL:** /refunds/{refund_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Refund is deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /refunds/2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Payments

Payment resource use for order and payment use cases.

There're 4 end-points to call, depends on payment gateway's payment flow type:

* Authorize
Authorize the requested payment. This end-points must continued with **Capture** or **Validate**. In this stage the server side will check if the user account and payment request are eligible or not. If eligible, server will returns needed properties to continue the flow.

* Capture
Capture the requested payment. This end-points must started with **Authorize**. In this stage the server side will capture the requested payment.

* Charge
Direct charge the requested payment from the server side. This is a single call for payment without authorize process.

* Validate
Validate the completed payment, for the payment gateway with client side charge/capture process. This process may started with **Authorize**


#### Payment Signature

Client needs to send **signature** parameter on request to payment resource, the value generated by SHA256 hash N + 1 times from the concat'ed order_id and SHARED SECRET, with N = order_id with base-100 system, e.g,

```
order_id = 3

N = 3

SHARED_SECRET = thisisthesampleofsharedsecret

1st hash: sha256(concat(order_id, SHARED_SECRET))

2nd hash: sha256(concat(1st hash result, SHARED_SECRET))

3rd hash: sha256(concat(2nd hash result, SHARED_SECRET))

4th hash: sha256(concat(3rd hash result, SHARED_SECRET))

signature = 0381471033c3d6e4fcf353463ce3d11a2d8eac61bef84baf02645734cdd30d61
```

#### Payment Properties

Payment properties are vary, depends on choosen payment gateway. But the main properties consists:

* `payment_id` - Number; Payment identifier.

* `user_id` - Number; User identifier, must conform with order's user identifier.

* `order_id` - Number; Order identifier, must conform with order's identifier.

* `payment_gateway_id` - Number; Payment gateway identifier, must conform with order's payment gateway identifier.

* `signature` - String; Signature for this payment. See [Payment Signature](#payment-signature) for detail how to generate this value.

### Free Payment Flow

Free payment flow (`payment_gateway_id = 3`) used when the order's `final_price` equals to 0 (no charge needed). Only a single call to charge needed in this flow.

The order and payment service calls in sequence:
* 1. [Checkout Order](#checkout-order)
* 2. [Confirm Order](#confirm-order)
* 3. [Free Payment Charge](#free-payment-charge)

#### Free Payment Charge

**URL:** /payments/charge

**Method:** POST

**Request Data Params:**

Required: `user_id`, `order_id`, `payment_gateway_id`, `signature`

**Response HTTP Code:**

* `200` -  Payment accepted successfully

**Response Params:** See [Order Properties](#order-properties)

**Examples:**

Request:

```
POST /payments/charge HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "order_id": 11,
    "signature": "37c1f5399b0d6461b00f15b61adac9480f6eea7c8f272c6e17be07c7b8fe22d1",
    "user_id": 112,
    "payment_gateway_id": 3
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    "user_id": 112,
    "order_id": 11,
    "temp_order_id": 39,
    "platform_id": 1,
    "partner_id": null,
    "is_active": true,
    "client_id": 1,
    "point_reward": 0,
    "payment_gateway_id": 3,
    "final_amount": "0.00",
    "order_status": 90000,
    "order_lines": [{
        "name": "PROVOKE! / AUG 2014 - Single Edition",
        "items": [{
            "id": 42,
            "is_featured": false,
            "vendor": {
                "id": 42,
                "name": "Simple Media"
            },
            "name": "PROVOKE! / AUG 2014",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2014-08-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "edition_code": "ID_PROV2014MTH08"
        }],
        "offer_id": 47341,
        "is_free": true,
        "final_price": "0.00",
        "currency_code": "USD",
        "raw_price": "0.00"
    }],
    "order_number": 21717867218328,
    "payment_gateway_name": "Free Purchase",
    "currency_code": "USD",
    "total_amount": "0.00"
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Payment Invalid, Invalid signature: 37c1f5399b0d6461b00f15b61adac9480f6eea7c8f272c6e17be07c7b8fe22d1"
    "user_message": "Payment Invalid"
}
```

### SCOOP Point Payment Flow

SCOOP Point payment flow (`payment_gateway_id = 9`). Only a single call to charge needed in this flow.

The order and payment service calls in sequence:
* 1. [Checkout Order](#checkout-order)
* 2. [Confirm Order](#confirm-order)
* 3. [SCOOP Point Payment Charge](#scoop-point-payment-charge)

#### SCOOP Point Payment Charge

**URL:** /payments/charge

**Method:** POST

**Request Data Params:**

Required: `user_id`, `order_id`, `payment_gateway_id`, `signature`

**Response HTTP Code:**

* `200` -  Payment accepted successfully

**Response Params:** See [Order Properties](#order-properties)

**Examples:**

Request:

```
POST /payments/charge HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"order_id": 43,
	"signature": "a1b901d0211fa3c259a67a4bf1427ed9f865f81363d08651b677c03ddc7036bf",
	"user_id": 682,
	"payment_gateway_id": 9
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    "payment_gateway_name": "SCOOP Point",
    "user_id": 682,
    "total_amount": "95.00",
    "order_id": 43,
    "partner_id": null,
    "is_active": true,
    "final_amount": "95.00",
    "payment_gateway_id": 9,
    "temp_order_id": 59,
    "client_id": 1,
    "point_reward": 0,
    "order_status": 90000,
    "order_number": 97749913159435,
    "currency_code": "PTS",
    "order_lines": [{
        "name": "PANORAMA / NOV\u2013DEC 2011 - Single Edition",
        "items": [{
            "is_featured": false,
            "vendor": {
                "id": 10,
                "name": "Panorama Multi Media"
            },
            "name": "PANORAMA / NOV\u2013DEC 2011",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2011-11-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "id": 1260,
            "edition_code": "ID_PAN2011MTH1112ED26"
        }],
        "offer_id": 2122,
        "is_free": false,
        "final_price": "95.00",
        "currency_code": "PTS",
        "raw_price": "95.00"
    }]
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Payment Invalid, Invalid signature: 37c1f5399b0d6461b00f15b61adac9480f6eea7c8f272c6e17be07c7b8fe22d1"
    "user_message": "Payment Invalid"
}
```

### mandiri clickpay Payment Flow

mandiri clickpay payment flow (`payment_gateway_id = 11`). The payment flow consists physical authorization and service call to capture.

The order and payment service calls in sequence:
* 1. [Checkout Order](#checkout-order)
* 2. [Confirm Order](#confirm-order)
* 3. Physical token device authorization
* 4. [mandiri clickpay Payment Capture](#mandiri-clickpay-payment-capture)

#### mandiri clickpay Payment Properties

* `response_token` - String; mandiri internet banking token response.

* `card_no` - String; mandiri ATM Card number.

#### mandiri clickpay Payment Capture

**URL:** /payments/capture

**Method:** POST

**Request Data Params:**

Required: `user_id`, `order_id`, `payment_gateway_id`, `signature`, `card_no`, `response_token`

**Response HTTP Code:**

* `200` -  Payment accepted successfully

**Response Params:** See [Order Properties](#order-properties)

**Examples:**

Request:

```
POST /payments/capture HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"order_id": 51,
	"signature": "9f1adffc60cc10920823410fb08108e22e6749230ed37a39fe061f27e7c8d6fb",
	"user_id": 682,
	"payment_gateway_id": 11,
    "response_token": "000000",
    "card_no": "4617007700000039"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    "payment_gateway_name": "SCOOP Point",
    "user_id": 682,
    "total_amount": "95.00",
    "order_id": 43,
    "partner_id": null,
    "is_active": true,
    "final_amount": "95.00",
    "payment_gateway_id": 9,
    "temp_order_id": 59,
    "client_id": 1,
    "point_reward": 0,
    "order_status": 90000,
    "order_number": 97749913159435,
    "currency_code": "PTS",
    "order_lines": [{
        "name": "PANORAMA / NOV\u2013DEC 2011 - Single Edition",
        "items": [{
            "is_featured": false,
            "vendor": {
                "id": 10,
                "name": "Panorama Multi Media"
            },
            "name": "PANORAMA / NOV\u2013DEC 2011",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2011-11-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "id": 1260,
            "edition_code": "ID_PAN2011MTH1112ED26"
        }],
        "offer_id": 2122,
        "is_free": false,
        "final_price": "95.00",
        "currency_code": "PTS",
        "raw_price": "95.00"
    }]
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Payment Invalid, Invalid signature: 37c1f5399b0d6461b00f15b61adac9480f6eea7c8f272c6e17be07c7b8fe22d1"
    "user_message": "Payment Invalid"
}
```

### Paypal Payment Flow

Paypal payment flow (`payment_gateway_id = 2`). The payment flow consists authorize and capture process.

The order and payment service calls in sequence:
* 1. [Checkout Order](#checkout-order)
* 2. [Confirm Order](#confirm-order)
* 3. [Paypal Payment Authorize](#paypal-payment-authorize)
* 4. Paypal web verification
* 5. [Paypal Payment Capture](#paypal-payment-capture)

#### Paypal Payment Properties

* `return_url` - URL; URL that will be called when the Paypal verification process success.

* `cancel_url` - URL; URL that will be called when the Paypal verification process cancelled.

* `payer_id` - String; Payer identifier from Paypal. This must be passed for capture process.

#### Paypal Payment Authorize

**URL:** /payments/authorize

**Method:** POST

**Request Data Params:**

Required: `user_id`, `order_id`, `payment_gateway_id`, `signature`, `return_url`, `cancel_url`

**Response HTTP Code:**

* `200` -  Payment accepted successfully

**Response Params:** See [Order Properties](#order-properties)

**Examples:**

Request:

```
POST /payments/authorize HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"order_id": 50,
	"signature": "59e4661d728e6da9090e51d7e05cfbfbd3f6863a97c10f952f4013f98857387b",
	"user_id": 682,
	"payment_gateway_id": 2,
    "return_url": "http://ap.apps-foundry.com/scoopwebstore2/confirm/paypal/?x=50",
    "cancel_url": "http://ap.apps-foundry.com/scoopwebstore2/cancel/paypal/?x=50"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "payment_id": 46,
    "approval_url": "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=EC-0SX60190D7927533C"
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Payment Invalid, Invalid signature: 37c1f5399b0d6461b00f15b61adac9480f6eea7c8f272c6e17be07c7b8fe22d1"
    "user_message": "Payment Invalid"
}
```

#### Paypal Payment Capture

**URL:** /payments/capture

**Method:** POST

**Request Data Params:**

Required: `user_id`, `order_id`, `payment_gateway_id`, `signature`, `payer_id`, `payment_id`

**Response HTTP Code:**

* `200` -  Payment accepted successfully

**Response Params:** See [Order Properties](#order-properties)

**Examples:**

Request:

```
POST /payments/capture HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"order_id": 50,
	"signature": "59e4661d728e6da9090e51d7e05cfbfbd3f6863a97c10f952f4013f98857387b",
	"user_id": 682,
	"payment_gateway_id": 2,
 	"payer_id": "S8JVR3RNVU86Q",
    "payment_id": 46
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "payment_gateway_name": "Paypal",
    "user_id": 682,
    "total_amount": "0.99",
    "order_id": 50,
    "partner_id": null,
    "is_active": true,
    "final_amount": "0.99",
    "payment_gateway_id": 2,
    "temp_order_id": 84,
    "client_id": 1,
    "point_reward": 9,
    "order_status": 90000,
    "order_number": 9109863892910722,
    "currency_code": "USD",
    "order_lines": [{
        "name": "PANORAMA / NOV\u2013DEC 2011 - Single Edition",
        "items": [{
            "is_featured": false,
            "vendor": {
                "id": 10,
                "name": "Panorama Multi Media"
            },
            "name": "PANORAMA / NOV\u2013DEC 2011",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2011-11-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "id": 1260,
            "edition_code": "ID_PAN2011MTH1112ED26"
        }],
        "offer_id": 2122,
        "is_free": false,
        "final_price": "0.99",
        "currency_code": "USD",
        "raw_price": "0.99"
    }]
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Payment Invalid, Invalid signature: 37c1f5399b0d6461b00f15b61adac9480f6eea7c8f272c6e17be07c7b8fe22d1"
    "user_message": "Payment Invalid"
}
```

### mandiri e-cash Payment Flow

mandiri e-cash payment flow (`payment_gateway_id = 14`). The payment flow consists authorize and validate process.

The order and payment service calls in sequence:
* 1. [Checkout Order](#checkout-order)
* 2. [Confirm Order](#confirm-order)
* 3. [mandiri e-cash Payment Authorize](#mandiri-e-cash-payment-authorize)
* 4. mandiri e-cash web OTP verification and capture
* 5. [mandiri e-cash Payment Validate](#mandiri-e-cash-payment-validate)

#### mandiri e-cash Payment Properties

* `return_url` - URL; URL that will be called when the Paypal verification process success.

* `client_ip` - String; Merchant's IP address

* `ecash_trx_id` - String; mandiri e-cash transaction identifier.

#### mandiri e-cash Payment Authorize

**URL:** /payments/authorize

**Method:** POST

**Request Data Params:**

Required: `user_id`, `order_id`, `payment_gateway_id`, `signature`, `return_url`, `client_ip`

**Response HTTP Code:**

* `200` -  Payment accepted successfully

**Response Params:** See [Order Properties](#order-properties)

**Examples:**

Request:

```
POST /payments/authorize HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"order_id": 52,
	"signature": "1ea15b703afcc90445c94f144c742bf50b0fd2c8982de7231e89084f6d298141",
	"user_id": 682,
	"payment_gateway_id": 14,
    "return_url": "http://127.0.0.1/getscoop/confirm/ecash",
    "client_ip": "182.253.203.91"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "payment_id": 47,
    "ecash_trx_id": "OHCHXQHTOWD317XXYYUHYM9VAEEKESRG"
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Payment Invalid, Invalid signature: 37c1f5399b0d6461b00f15b61adac9480f6eea7c8f272c6e17be07c7b8fe22d1"
    "user_message": "Payment Invalid"
}
```

#### mandiri e-cash Payment Validate

**URL:** /payments/validate

**Method:** POST

**Request Data Params:**

Required: `user_id`, `order_id`, `payment_gateway_id`, `signature`, `payer_id`, `payment_id`

**Response HTTP Code:**

* `200` -  Payment accepted successfully

**Response Params:** See [Order Properties](#order-properties)

**Examples:**

Request:

```
POST /payments/validate HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "ecash_trx_id": "GHKR5ZWK45GII93X9CMN939A5EYRUVVM",
    "order_id": 52,
    "signature": "1ea15b703afcc90445c94f144c742bf50b0fd2c8982de7231e89084f6d298141",
    "user_id": 682,
    "payment_gateway_id": 14,
    "payment_id": 48
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "payment_gateway_name": "mandiri e-cash",
    "user_id": 682,
    "total_amount": "10000.00",
    "order_id": 52,
    "partner_id": null,
    "is_active": true,
    "final_amount": "10000.00",
    "payment_gateway_id": 14,
    "temp_order_id": 89,
    "client_id": 1,
    "point_reward": 10,
    "order_status": 90000,
    "order_number": 910887179696433,
    "currency_code": "IDR",
    "order_lines": [{
        "name": "PANORAMA / NOV\u2013DEC 2011 - Single Edition",
        "items": [{
            "is_featured": false,
            "vendor": {
                "id": 10,
                "name": "Panorama Multi Media"
            },
            "name": "PANORAMA / NOV\u2013DEC 2011",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2011-11-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "id": 1260,
            "edition_code": "ID_PAN2011MTH1112ED26"
        }],
        "offer_id": 2122,
        "is_free": false,
        "final_price": "10000.00",
        "currency_code": "IDR",
        "raw_price": "10000.00"
    }]
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Payment Invalid, Invalid signature: 37c1f5399b0d6461b00f15b61adac9480f6eea7c8f272c6e17be07c7b8fe22d1"
    "user_message": "Payment Invalid"
}
```

### Veritrans Payment Flow

Veritrans VT-Direct payment flow (`payment_gateway_id = 12`). The flow consists authorize from client side and capture process.

The order and payment service calls in sequence:
* 1. [Checkout Order](#checkout-order)
* 2. [Confirm Order](#confirm-order)
* 3. Client side authorize with Veritrans API
* 4. [Veritrans Payment Capture](#veritrans-payment-capture)

#### Veritrans Payment Properties

* `payer_id` - String; User email.

* `payment_code` - String; Authorization payment code from Veritrans.

* `billing_address` - Object; Billing address of the credit card

    * `first_name` - String; First name as in credit card information.

    * `last_name` - String; Last name as in credit card information.

    * `address1` - String; Address as in credit card information.

    * `address2` - String; Address (continuation) as in credit card information.

    * `city` - String; City as in credit card information.

    * `postal_code` - String; Postal code as in credit card information.

    * `phone` - String; Phone number as in credit card information, e.g, 081599912345.

#### Veritrans Payment Capture

**URL:** /payments/capture

**Method:** POST

**Request Data Params:**

Required: `user_id`, `order_id`, `payment_gateway_id`, `signature`, `payer_id`, `payment_code`, `billing_address`

Required in `billing_address`: `first_name`, `last_name`, `address1`, `address2`, `city`, `postal_code`, `phone`

**Response HTTP Code:**

* `200` -  Payment accepted successfully

**Response Params:** See [Order Properties](#order-properties)

**Examples:**

Request:

```
POST /payments/capture HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "order_id": 53,
    "payment_code": "91f3ab85-accf-488e-9fa2-425382752f21-411111-1111",
    "payer_id": "vt-testing@veritrans.co.id",
    "signature": "3a940347789512bce39bd285307d15054771cda1fdb49fefad99e2dbccb7bd2d",
    "user_id": 682,
    "payment_gateway_id": 12,
    "billing_address": {
        "first_name":"rassel",
        "last_name":"pratomo",
        "address1":"jl lauser",
        "address2":"jl lauser",
        "city":"jakarta",
        "postal_code":"12120",
        "phone":"087878667171"
    }
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "payment_gateway_name": "Veritrans",
    "user_id": 682,
    "total_amount": "10000.00",
    "order_id": 53,
    "partner_id": null,
    "is_active": true,
    "final_amount": "10000.00",
    "payment_gateway_id": 12,
    "temp_order_id": 90,
    "client_id": 1,
    "point_reward": 10,
    "order_status": 90000,
    "order_number": 61079275621021067,
    "currency_code": "IDR",
    "order_lines": [{
        "name": "PANORAMA / NOV\u2013DEC 2011 - Single Edition",
        "items": [{
            "is_featured": false,
            "vendor": {
                "id": 10,
                "name": "Panorama Multi Media"
            },
            "name": "PANORAMA / NOV\u2013DEC 2011",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2011-11-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "id": 1260,
            "edition_code": "ID_PAN2011MTH1112ED26"
        }],
        "offer_id": 2122,
        "is_free": false,
        "final_price": "10000.00",
        "currency_code": "IDR",
        "raw_price": "10000.00"
    }]
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Payment Invalid, Invalid signature: 37c1f5399b0d6461b00f15b61adac9480f6eea7c8f272c6e17be07c7b8fe22d1"
    "user_message": "Payment Invalid"
}
```

### Payment Tokens

Payment token resource describes the tokens from any payment gateways. This API Allows storing and retrieving payment tokens retrieved from a payment gateway for submitting additional orders (at a later time), without requiring the user to re-enter their credit card information.

#### Payment Token Properties

* `id` - Number; Instance identifier

* `user_id` - Number; User (card holder) identifier

* `billing_address` - Nested JSON objects describe the billing address. Consists of :

    * `address1` - String; Billing address 1

    * `address2` - String; Billing address 2; alternate address

    * `city` - String; City where the billing address at

    * `country_code` - String; Country code in ISO 3166

    * `postal_code` - String; Postal code of the billing address

    * `province` - String; Province name of the billing address

* `shipping_address` - Nested JSON objects describe the shipping address. Consists of :

    * `address1` - String; Shipping address 1

    * `address2` - String; Shipping address 2; alternate address

    * `city` - String; City where the shipping address at

    * `country_code` - Number; Country code of the shipping address

    * `postal_code` - Number; Postal code of the shipping address

    * `province` - String; Province name of the shipping address

* `expiration` - String; Datetime in UTC, Credit card expiration date

* `gateway` - String; Gateway this token belongs to. Default : veritrans (note: only available for veritrans for the current time)

* `masked_credit_card` - String; Partial credit card number (used for displaying to the user so they know which token they want to use)

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. The default value is true.

* `token` - String; The key used to authenticate the user

#### Create Payment Token

Makes new Payment token.

**URL:** /payment/tokens

**Method:** POST

**Request Data Params:**

Required: `gateway`, `user_id`

Optional: -

**Response HTTP Code:**

* `201` - New Payment Token is created

**Response Params:** See [Payment Token Properties](#payment-token-properties)

**Examples:**

Request:

```
POST /payment/tokens HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "token": "ABCDEFGHIJKLMNOP",
    "user_id": 1,
    "billing_address":
        {
            "address1": "Jl. Menuju Terang",
            "address2": null,
            "city": "Jakarta",
            "country_code": "ID",
            "postal_code": "12790",
            "province": "DKI Jakarta"
        },
    "expiration": "2015-09-01T00:00:00+00:00",
    "gateway": "veritrans",
    "is_active": true,
    "masked_credit_card": "4967xxxxxxxx4198",
    "shipping_address":
        {
            "address1": "Jl. Menuju Terang",
            "address2": null,
            "city": "Jakarta",
            "country_code": "ID",
            "postal_code": "12790",
            "province": "DKI Jakarta"
        }
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/payment_token/1

{
    "token": "ABCDEFGHIJKLMNOP",
    "user_id": 1,
    "id": 1,
    "billing_address":
        {
            "address1": "Jl. Menuju Terang",
            "address2": null,
            "city": "Jakarta",
            "country_code": "ID",
            "postal_code": "12790",
            "province": "DKI Jakarta"
        },
    "expiration": "2015-09-01T00:00:00+00:00",
    "gateway": "veritrans",
    "is_active": true,
    "masked_credit_card": "4967xxxxxxxx4198",
    "shipping_address":
        {
            "address1": "Jl. Menuju Terang",
            "address2": null,
            "city": "Jakarta",
            "country_code": "ID",
            "postal_code": "12790",
            "province": "DKI Jakarta"
        }
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Payment Tokens

Retrieves all of the payment tokens.

**URL:** /payment/tokens

Optional:

* `q` - Simple string query to records' string properties

* `fields` - Retrieve specific fields. See [Payment Token properties](#payment-token-properties). Default to ALL

* `is_active` - Filter by is_active status(es).

* `offset` - Offset of the records. Default : 0

* `limit` - Limit of the records. Default : 20

**Method:** GET

**Response HTTP Code:**

* `200` - Payment Token data are retrieved and returned in body content

* `204` - No matched Payment Token

**Response Params:** See [Payment Token Properties](#payment-token-properties)

**Examples:**

Request:

```
GET /payment/tokens?is_active=true HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "tokens": [
        {
            "token": "ABCDEFGHIJKLMNOP",
            "user_id": 1,
            "id": 1,
            "billing_address":
                {
                    "address1": "Jl. Menuju Terang",
                    "address2": null,
                    "city": "Jakarta",
                    "country_code": "ID",
                    "postal_code": "12790",
                    "province": "DKI Jakarta"
                },
            "expiration": "2015-09-01T00:00:00+00:00",
            "gateway": "veritrans",
            "is_active": true,
            "masked_credit_card": "4967xxxxxxxx4198",
            "shipping_address":
                {
                    "address1": "Jl. Menuju Terang",
                    "address2": null,
                    "city": "Jakarta",
                    "country_code": "ID",
                    "postal_code": "12790",
                    "province": "DKI Jakarta"
                }
        },
        {
            "token": "ABCDEFGHIJKLMNOP",
            "user_id": 2,
            "id": 6,
            "billing_address":
                {
                    "address1": "Jl. Jalan",
                    "address2": null,
                    "city": "Jakarta",
                    "country_code": "ID",
                    "postal_code": "12790",
                    "province": "DKI Jakarta"
                },
            "expiration": "2015-09-01T00:00:00+00:00",
            "gateway": "veritrans",
            "is_active": true,
            "masked_credit_card": "4967xxxxxxxx4198",
            "shipping_address":
                {
                    "address1": "Jl. Jalan",
                    "address2": null,
                    "city": "Jakarta",
                    "country_code": "ID",
                    "postal_code": "12790",
                    "province": "DKI Jakarta"
                }
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 20
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Payment Token

Retrieves individual Payment token by using identifier.

**URL:** /payment/tokens/{token_id}

Optional:

* `fields` - Retrieve specific properties. See [Payment Token properties](#payment-token-properties). Default : ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Payment Token data is found and returned

**Response Params:** See [Payment Token properties](#payment-token-properties)

**Examples:**

Request:

```
GET /payment/tokens/2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "token": "ABCDEFGHIJKLMNOP",
    "user_id": 1,
    "id": 2,
    "billing_address":
        {
            "address1": "Jl. Menuju Terang",
            "address2": null,
            "city": "Jakarta",
            "country_code": "ID",
            "postal_code": "12790",
            "province": "DKI Jakarta"
        },
    "expiration": "2015-09-01T00:00:00+00:00",
    "gateway": "veritrans",
    "is_active": true,
    "masked_credit_card": "4967xxxxxxxx4198",
    "shipping_address":
        {
            "address1": "Jl. Menuju Terang",
            "address2": null,
            "city": "Jakarta",
            "country_code": "ID",
            "postal_code": "12790",
            "province": "DKI Jakarta"
        }
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Payment Token

Updates payment token data.

**URL:** /payment/tokens/{token_id}

**Method:** PUT

**Request Data Params:**

Required: `gateway`, `user_id`

Optional: -

**Response HTTP Code:**

* `200` - Payment Token is updated

**Response Params:** See [Payment Token properties](#payment-token-properties)

**Examples:**

Request:

```
PUT /payment/tokens/2 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "token": "ABCDEFGHIJKLMNOP",
    "user_id": 1,
    "id": 2,
    "billing_address":
        {
            "address1": "Jl. Menuju Terang",
            "address2": null,
            "city": "Jakarta",
            "country_code": "ID",
            "postal_code": "12790",
            "province": "DKI Jakarta"
        },
    "expiration": "2015-09-01T00:00:00+00:00",
    "gateway": "veritrans",
    "is_active": true,
    "masked_credit_card": "4967xxxxxxxx4198",
    "shipping_address":
        {
            "address1": "Jl. Menuju Terang",
            "address2": null,
            "city": "Jakarta",
            "country_code": "ID",
            "postal_code": "12790",
            "province": "DKI Jakarta"
        }
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "token": "ABCDEFGHIJKLMNOP",
    "user_id": 1,
    "id": 2,
    "billing_address":
        {
            "address1": "Jl. Menuju Terang",
            "address2": null,
            "city": "Jakarta",
            "country_code": "ID",
            "postal_code": "12790",
            "province": "DKI Jakarta"
        },
    "expiration": "2015-09-01T00:00:00+00:00",
    "gateway": "veritrans",
    "is_active": true,
    "masked_credit_card": "4967xxxxxxxx4198",
    "shipping_address":
        {
            "address1": "Jl. Menuju Terang",
            "address2": null,
            "city": "Jakarta",
            "country_code": "ID",
            "postal_code": "12790",
            "province": "DKI Jakarta"
        }
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Payment Token

Deletes Payment token. This service will set `is_active` value to false.

**URL:** /payment/tokens/{token_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Payment Token is deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /payment/tokens/5 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Pending Payment

A "Pending" payment is a payment that is not committed yet, and waiting for the payment API to validate.

#### Pending Payment Properties

* `order_id` - Number; Order identifier

* `payment_code` - String; Code generated by the payment API, this code will be used to commit and validate the payment

* `payment_gateway_id` - Number; Payment gateway identifier

* `payment_id` - Number; Payment identifier

* `payment_status` - Number; Current status of the payment

* `transaction_date` - String; Datetime in UTC, captured when the transaction took place

#### Retrieve Pending Payment

Retrieves pending payment (status on database is 20001) of any certain user

**URL:** /payments/pending

Required :

* `user_id` - User identifier

Optional:

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `payment_gateway` - Payment gateway identifier. The response can be retrieved based on certain payment method.

**Method:** GET

**Response HTTP Code:**

* `200` - Payment gateway is retrieved and returned in body content

* `204` - No matched payment gateway

**Response Params:** See [Pending Payment Properties](#pending-payment-properties)

**Examples:**

Request:

```
GET payments/pending?limit=100&offset=0&user_id=132543&payment_gateway=5,6 HTTP/1.1
Authorization: Bearer  MjY4ODYwOmNmZjhj^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "metadata": {
        "limit": 100,
        "offset": 0,
        "total": 13
    },
        "pending_transactions": [
            {
                "order_id": 6473,
                "payment_code": null,
                "payment_gateway_id": 6,
                "payment_id": 6392,
                "payment_status": 20001,
                "transaction_date": "2014-10-31T10:17:27.296008"
            },
            {
                "order_id": 6347,
                "payment_code": null,
                "payment_gateway_id": 6,
                "payment_id": 6286,
                "payment_status": 20001,
                "transaction_date": "2014-10-30T10:37:29.257960"
            },
            {
                ...
            },
            {
                "order_id": 1353,
                "payment_code": "0195750596485",
                "payment_gateway_id": 5,
                "payment_id": 1157,
                "payment_status": 20001,
                "transaction_date": "2014-09-24T12:21:35.633522"
            }
        ]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

---

## SCOOP POINTS

### Points

Point resource contains use case for user's point summary, acquire, and use

**Point expiring**

Point expires datetime set to 12 months and rounded up to last seconds of the expiring month, with UTC timezone

#### Retrieve Individual User Point Summary

Retrieve individual user point summary

**Point Summary Properties**

* `used_points` - Number; Number of used points

* `available_points` - Number; Number of points available to use

* `user_id` - Number; User identifier

**URL:** /points/summary/{user_id}

**Method:** GET

**Response HTTP Code:**

* `200` -  Points summary retrieved and returned in body content

**Response Params:** See [Point Summary Properties](#point-summary-properties)

**Examples:**

Request:

```
GET /points/summary/5 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    used_points: 475
    available_points: 0
    user_id: 5
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Retrieve All User Point Summary

Retrieves all user point summary

**Point Summary Properties**

* `points_used` - Number; Number of used points

* `point_amount` - Number; Number of points available to use

* `user_id` - Number; User identifier

* `is_active` - Boolean; Instance's status, used to mark if the instance is active or not. Default value is true

* `expired_datetime` - String; Datetime to which the points are available to use. See **point expiring**

* `client_id` - Number; Client identifier

* `acquire_datetime` - String; Datetime from which the acquired points are available to use

* `id` - Number; Instance identifier

**URL:** /points

**Method:** GET

**Response HTTP Code:**

* `200` -  Points summary retrieved and returned in body content

**Response Params:** See [Point Summary Properties](#point-summary-properties)

**Examples:**

Request:

```
GET /points HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "points": [
        {
            "user_id": 5
            "is_active": true
            "expired_datetime": "2015-08-21T16:55:44.286315"
            "point_used": 26665
            "client_id": 7
            "point_amount": 999985440
            "acquire_datetime": "2014-08-21T16:55:44.288066"
            "id": 3
        },
        {
            "user_id": 329438
            "is_active": true
            "expired_datetime": "2014-08-02T00:00:00"
            "point_used": 909999
            "client_id": 80
            "point_amount": 909999
            "acquire_datetime": "2014-08-01T00:00:00"
            "id": 12
        },
        {...}
    ],
    "metadata": {
        "resultset": {
            "count": 7,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Retrieve Individual Point Summary

Retrieves point summary by point id

**Point Summary Properties**

* `points_used` - Number; Number of used points

* `point_amount` - Number; Number of points available to use

* `user_id` - Number; User identifier

* `is_active` - Boolean; Instance's status, used to mark if the instance is active or not. Default value is true

* `expired_datetime` - String; Datetime to which the points are available to use. See **point expiring**

* `client_id` - Number; Client identifier

* `acquire_datetime` - String; Datetime from which the acquired points are available to use

* `id` - Number; Instance identifier

**URL:** /points/{point_id}

**Method:** GET

**Response HTTP Code:**

* `200` -  Points summary retrieved and returned in body content

**Response Params:** See [Point Summary Properties](#point-summary-properties)

**Examples:**

Request:

```
GET /points/99 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "user_id": 777,
    "is_active": true,
    "expired_datetime": "2020-12-31T23:59:59",
    "point_used": 0,
    "client_id": 0,
    "point_amount": 27,
    "acquire_datetime": "2012-12-22T04:50:13",
    "id": 99
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Add Points

Adds points, add some amount of points to certain user id

**URL:** /points

**Method:** POST

**Request Data Params:**

Required:

* `user_id` - Number; User identifier who acquiring points

* `is_active` - Boolean; Instance's status, used to mark if the instance is active or not. Default value is true

* `expired_datetime` - String; Datetime to which the points are available to use. See **point expiring**

* `point_used` - Number; The amount of used point

* `client_id` - Number; Client identifier that performed a request to acquire points

* `point_amount` - Number; Amount of points to be acquired

* `acquired_datetime` - String; Datetime from which the acquired points are available to use

Optional:

* `details` - String; Details of this acquire action

**Response HTTP Code:**

* `200` - Points successfully acquired by user

**Response Params:** -

**Examples:**

Request:

```
POST /points HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "user_id": 290411,
    "is_active": true,
    "expired_datetime": "2015-08-20T12:09:05.835515",
    "point_used": 290,
    "client_id": 2,
    "point_amount": 510,
    "acquire_datetime": "2014-08-20T12:09:05.835496"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "user_id": 290411,
    "is_active": true,
    "expired_datetime": "2015-08-20T12:09:05.835515",
    "point_used": 290,
    "client_id": 2,
    "point_amount": 510,
    "acquire_datetime": "2014-08-20T12:09:05.835496"
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

---

## USER RELATED

### User Owned Items

User owned item resource.

#### User Owned Item Properties

* `id` - Number; Instance identifier

* `user_id` - Number; Owner user identifier

* `item_id` - Number; Owned item identifier

* `edition_code` - Number; Owned edition code **TODO: deprecated after migration**

* `order_line_id` - Number; Related order line identifier from which this owned item added from

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

#### Add New User Owned Items

Add new items to specific user. This action only can be called by internal system or trusted CMS to deliver the item to user.

Add new items is called on item delivery (post-payment) use case or when administrator want to give complimentary items to user.

**URL:** /owned_items

**Method:** POST

**Request Data Params:**

Required: `user_id`, `item_id`, `is_active`

Optional: `order_line_id`

**Response HTTP Code:**

* `207` - Multiple status of each item

    * `200` -  Item already owned by user, detail updated

    * `201` -  New item added to user

**Response Params:** See [User Owned Item Properties](#user-owned-item-properties)

**Examples:**

Request:

```
POST /owned_items HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "owned_items": [
        {
            "user_id": 23,
            "item_id": 23456,
            "is_active": true,
            "order_line_id": 528263
        },
        {
            "user_id": 23,
            "item_id": 243432,
            "is_active": true,
            "order_line_id": 8263
        },
        {
            "user_id": 23,
            "item_id": 12,
            "is_active": true,
            "order_line_id": 5283
        }
    ]
}
```

Success response:

```
HTTP/1.1 207 Multi-Status
Content-Type: application/json; charset=utf-8

{
    "results":[
        {
            "status": 201,
            "id": 213,
            "user_id": 23,
            "item_id": 23456,
            "is_active": true,
            "order_line_id": 528263,
            "brand_id": 17,
            "vendor_id": 12,
            "itemtype_id": 1
        },
        {
            "status": 400,
            "error_code": 400771,
            "developer_message": "Bad Request. Invalid item_id",
            "user_message": "Invalid item_id",
            "brand_id": 17,
            "vendor_id": 12,
            "itemtype_id": 1
        },
        {
            "status": 200,
            "id": 214,
            "user_id": 23,
            "item_id": 12,
            "is_active": true,
            "order_line_id": null,
            "brand_id": 17,
            "vendor_id": 12,
            "itemtype_id": 1
        }
    ]
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Retrieve User Owned Items

Retrieve user owned items, this service to support below use cases:

1. Retrieve items owned by user to show user's cloud items

2. Retrieve items owned by user with provided items, to show user item's statuses on store front

Client side must passed true as `is_active` value to show active owned items

Note: For the response's expanded representation, please refers to [Item Properties](#item-properties)

**URL:** /owned_items

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Item Properties](#item-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

* `user_id` - Provided user identifier(s) to retrieve. Response will only returns if any matched; If alternative URL provided the user_id, this parameter will be ignored

* `item_id` - Provided item identifier(s) to retrieve. Response will only returns if any matched

* `brand_id` - Provided brand identifier to retrieve

* `vendor_id` - Provided vendor identifier to retrieve

* `itemtype_id` - Provided item type identifier to retrieve

* `expand` - Expand the result details. Default to null, the supported expand for owned items are as below

    * item: get related item as object. See [Item Properties](#item-properties)

**Method:** GET

**Response HTTP Code:**

* `200` - Items retrieved and returned in body content

* `204` - No matched item

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

Retrieve item's statuses for store front:

```
GET /owned_items?offset=0&limit=10&user_id=36&fields=id&is_active=true&item_id=4876,23,234,1129,5087 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Retrieve user owned items:

```
GET /owned_items?offset=0&limit=20&user_id=36&is_active=true HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Retrieve user owned items for CMS:

```
GET /owned_items?offset=0&limit=100&expand=item HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "owned_items": [
        {
            "id": 213,
            "item": {
                "id": 45,
                "name": "Tempo ED 27 2014",
                "item_type_id": 1,
                "sort_priority": 1,
                "display_until": "2014-05-12T10:00:00.000Z",
                "content_type": 1,
                "is_active": true,
                "edition_code": "ID_TEMED272014", **TODO: deprecated after all migrated**
                "brand_id": 20,
                "is_featured": false,
                "is_extra": false,
                "parental_control_id": 3,
                "release_date": "2014-01-20",
                "issue_no": "ED 27 2014",
                "slug": "ed-27-2014",
                "thumb_image_normal": "items/tempo/ed-27-2014/thumb_cover_normal.png",
                "thumb_image_highres": "items/tempo/ed-27-2014/thumb_cover_highres.png",
                "subs_weight": 1
            }
            "user_id": 36,
            "order_line_id": 1231,
            "is_active": true,
            "brand_id": 17,
            "vendor_id": 12,
            "itemtype_id": 1
        },
         {
            "id": 214,
            "item": {
                "id": 452,
                "name": "Tempo ED 44 2014",
                "item_type_id": 1,
                "sort_priority": 1,
                "display_until": "2014-05-12T10:00:00.000Z",
                "content_type": 1,
                "is_active": true,
                "edition_code": "ID_TEMED442014", **TODO: deprecated after all migrated**
                "brand_id": 20,
                "is_featured": false,
                "is_extra": false,
                "parental_control_id": 3,
                "release_date": "2014-03-20",
                "issue_no": "ED 44 2014",
                "slug": "ed-44-2014",
                "thumb_image_normal": "items/tempo/ed-44-2014/thumb_cover_normal.png",
                "thumb_image_highres": "items/tempo/ed-44-2014/thumb_cover_highres.png",
                "subs_weight": 1
            }
            "user_id": 36,
            "order_line_id": 1231,
            "is_active": true,
            "brand_id": 17,
            "vendor_id": 12,
            "itemtype_id": 1
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Update User Owned Item

Update user owned item.

**URL:** /owned_items/{owned_item_id}

**Method:** PUT

**Response HTTP Code:**

* `200` - User owned item updated

**Response Params:** -

**Examples:**

Request:

```
PUT /owned_items/2316 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "user_id": 37,
    "item_id": 452,
    "is_active": false,
    "order_line_id": 528263
}
```

Success response:

```
HTTP/1.1 200 OK

{
    "id": 2316,
    "item_id": 452,
    "user_id": 37,
    "order_line_id": 1231,
    "is_active": true,
    "brand_id": 17,
    "vendor_id": 12,
    "itemtype_id": 1
}

```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete User Owned Item

Delete user owned item. This service will set `is_active` value to false.

**URL:** /owned_items/{owned_item_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - User owned item deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /owned_items/23161 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### User Owned Subscriptions

User owned subscription resource support the use cases of user owned subscription. The service representation will vary depends on use case needs.

#### User Owned Subscription Properties

* `id` - Number; Instance identifier

* `user_id` - Number; Owner user identifier

* `brand_id` - Number; Owned subscription's brand identifier

* `subscription_code` - Number; Owned subscription code **TODO: deprecated after migration**

* `order_line_id` - Number; Related order line identifier from which this owned item added from

* `allow_backward` - Boolean; Flag if the subscription allow backward (back issue in publication) for items in the same brand, this only works on time-based subscription

* `quantity` - Number; Quantity for unit-based subscription, if this value available `valid_to` value are ignored

* `current_quantity` - Number; Current left over quantity for unit-based subscription. On new owned subscription, this value will be exactly same as `quantity` and will reduced to 0 when the subscription already fully-used.

* `quantity_unit` - Number; Quantity unit type for subcription, related to `quantity` property

    * 1: UNIT - For unit-based subscription, the total unit will be stored in user's owned subscription. Most of the magazines use this type of unit

    * 2: DAY - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. This unit rarely used

    * 3: WEEK - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. This unit rarely used

    * 4: MONTH - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. Most of the newspaper use this type of unit

* `valid_from` - Number; Valid from timestamps for time-based subcription, this value will be used if `quantity` not provided

* `valid_to` - Number; Valid to timestamps for time-based subcription, this value will be used if `quantity` not provided

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `brand` - Object; Brand of the subscription, this returned on subcriptions retrieval

#### Add New User Owned Subcriptions

Add new subscriptions to specific user. This action only can be called by internal system or trusted CMS to deliver the subscriptions to user.

Add new subscriptions is called on subscription delivery (post-payment) use case or when administrator want to give complimentary subscriptions to user.

**URL:** /owned_subscriptions

**Method:** POST

**Request Data Params:**

Required: `subscriptions`

**Response HTTP Code:**

* `207` - Multiple status of each subcription

    * `200` -  Subscription already owned by user, detail updated

    * `201` -  New subscription added to user

**Response Params:** See [User Owned Subscription Properties](#user-owned-subscription-properties)

**Examples:**

Request:

```
POST /owned_subscriptions HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "owned_subscriptions":[
        {
            "valid_from": "2014-06-16 15:13:29",
            "user_id": 639,
            "quantity_unit": 1,
            "order_line_id": 1647,
            "allow_backward": false,
            "is_active": true,
            "valid_to": "2014-07-16 15:13:29",
            "brand_id": 7709,
            "current_quantity": 8,
            "quantity": 8
        },
        {
            "valid_from": "2014-06-16 15:13:29",
            "user_id": 639,
            "quantity_unit": 1,
            "order_line_id": 1648,
            "allow_backward": false,
            "is_active": true,
            "valid_to": "2014-07-16 15:13:29",
            "brand_id": 7710,
            "current_quantity": 8,
            "quantity": 8
        }
    ]
}
```

Success response:

```
HTTP/1.1 207 Multi-Status
Content-Type: application/json; charset=utf-8

{
    "results": [{
        "status": 201,
        "valid_from": "2014-06-16T15:13:29",
        "user_id": 639,
        "quantity_unit": 1,
        "order_line_id": 1647,
        "allow_backward": false,
        "is_active": true,
        "valid_to": "2014-08-29T12:06:51.379137",
        "id": 24,
        "brand_id": 7709,
        "subscription_code": "SUB_STU06ED06MTH",
        "current_quantity": 8,
        "quantity": 8
    }, {
        "status": 201,
        "valid_from": "2014-06-16T15:13:29",
        "user_id": 639,
        "quantity_unit": 1,
        "order_line_id": 1648,
        "allow_backward": false,
        "is_active": true,
        "valid_to": "2014-08-29T12:06:51.422306",
        "id": 25,
        "brand_id": 7710,
        "subscription_code": "SUB_STU06ED06MTH",
        "current_quantity": 8,
        "quantity": 8
    }]
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Retrieve User Owned Subscriptions

Retrieve user owned subscriptions, this service to support below use cases:

1. Retrieve subscriptions owned by user to show to user's subcription list

2. Retrieve subscriptions owned by user with provided brands, to show user item's statuses on store front

Note: The returned representation refers to [User Subscription Properties](#user—subscription-properties)

**URL:** /owned_subscriptions

**Alternative URL:** /owned_subscriptions/{owned_subscription_id}

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Subscription Properties](#subscription-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

* `user_id` - Provided list of user identifiers to retrieve. Response will only returns owned subscriptions’ with matched user; If alternative URL provided the user_id, this parameter will be ignored

* `brand_id` - Provided list of brand identifiers to retrieve. Response will only returns owned subscriptions’ with matched brand

* `expand` - Expand the result details. Default to null, the supported expand for owned subscriptions are as below

    * brand: get related brand as object. See [Brand Properties](#brand-properties)

    * ext: get all sub-object or array, including brand and referred user's owned items from the owned subscription. Note: All referred items of the owned subscription will be returned, since the data will be huge, use this only when really needed.

**Method:** GET

**Response HTTP Code:**

* `200` - Subscriptions retrieved and returned in body content

* `204` - No matched Subscription

**Response Params:** See [User Owned Subscription Properties](#user—owned-subscription-properties)

**Examples:**

Request:

Retrieve item's statuses for store front:

```
GET /owned_subscriptions?offset=0&limit=10&brand_id=1,2,3,4 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Retrieve user owned subscriptions:

```
GET /owned_subscriptions?offset=0&limit=10&fields=id HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Retrieve user subscriptions for CMS:

```
GET /owned_subscriptions?offset=0&limit=10&fields=id,user_id,brand_id&expand=ext HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    "owned_subscriptions": [{
        "valid_from": "2014-08-28T05:10:26",
        "user_id": 116301,
        "quantity_unit": 1,
        "items": [{
            "user_id": 116301,
            "order_line_id": null,
            "is_active": true,
            "item_id": 51646,
            "id": 43,
            "edition_code": "ID_TBOLA2014MTH08ED2577"
        }],
        "order_line_id": 0,
        "allow_backward": false,
        "is_active": true,
        "valid_to": "2014-08-28T12:10:26.936511",
        "id": 4,
        "brand_id": 5366,
        "brand": {
            "icon_image_normal": "",
            "sort_priority": 1,
            "description": "",
            "vendor_id": 156,
            "is_active": true,
            "slug": "tabloid-bola",
            "meta": "",
            "icon_image_highres": "",
            "id": 5366,
            "name": "Tabloid Bola"
        },
        "subscription_code": "SUB_TBOLA03MTH",
        "current_quantity": 14,
        "quantity": 14
    }, {
        "valid_from": "2014-08-28T05:31:29",
        "user_id": 370587,
        "quantity_unit": 4,
        "items": [{
            "user_id": 370587,
            "order_line_id": null,
            "is_active": true,
            "item_id": 51637,
            "id": 73,
            "edition_code": "ID_BOLAH2014MTH08DT28"
        }],
        "order_line_id": 0,
        "allow_backward": true,
        "is_active": true,
        "valid_to": "2014-11-28T12:31:29.638729",
        "id": 5,
        "brand_id": 5267,
        "brand": {
            "icon_image_normal": "",
            "sort_priority": 1,
            "description": "",
            "vendor_id": 156,
            "is_active": true,
            "slug": "bola-harian",
            "meta": "",
            "icon_image_highres": "",
            "id": 5267,
            "name": "BOLA Harian"
        },
        "subscription_code": "SUB_BOLAH03MTH",
        "current_quantity": 3,
        "quantity": 3
    }, {
        "valid_from": "2014-08-28T05:56:46",
        "user_id": 48035,
        "quantity_unit": 4,
        "items": [{
            "user_id": 48035,
            "order_line_id": null,
            "is_active": true,
            "item_id": 51637,
            "id": 114,
            "edition_code": "ID_BOLAH2014MTH08DT28"
        }],
        "order_line_id": 0,
        "allow_backward": true,
        "is_active": true,
        "valid_to": "2014-11-28T12:56:46.215202",
        "id": 6,
        "brand_id": 5267,
        "brand": {
            "icon_image_normal": "",
            "sort_priority": 1,
            "description": "",
            "vendor_id": 156,
            "is_active": true,
            "slug": "bola-harian",
            "meta": "",
            "icon_image_highres": "",
            "id": 5267,
            "name": "BOLA Harian"
        },
        "subscription_code": "SUB_BOLAH03MTH",
        "current_quantity": 3,
        "quantity": 3
    }],
    "metadata": {
        "resultset": {
            "count": 23,
            "limit": 3,
            "offset": 0
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Update User Owned Subscription

Update user owned subscription.

**URL:** /owned_subscriptions/{owned_subscription_id}

**Method:** PUT

**Response HTTP Code:**

* `200` - User owned subscription updated

**Response Params:** -

**Examples:**

Request:

```
PUT /owned_subscriptions/3 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "valid_from": "2014-06-16 15:13:29",
    "user_id": 298106,
    "quantity_unit": 1,
    "order_line_id": 1647,
    "allow_backward": false,
    "is_active": true,
    "valid_to": "2014-07-16 15:13:29",
    "current_quantity": 8,
    "brand_id": 5267,
    "quantity": 8
}
```

Success response:

```
HTTP/1.1 200 OK

{
    "valid_from": "2014-06-16T15:13:29",
    "user_id": 298106,
    "quantity_unit": 1,
    "order_line_id": 1647,
    "allow_backward": false,
    "is_active": true,
    "valid_to": "2014-07-16T15:13:29",
    "id": 3,
    "brand_id": 5267,
    "subscription_code": "SUB_STU06ED06MTH",
    "current_quantity": 3,
    "quantity": 8
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete User Owned Subscription

Delete user owned Subscription. This service will set `is_active` value to false.

**URL:** /owned_subscriptions/{owned_subscription_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - User owned subscription deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /owned_subscriptions/5 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Downloads

Download resource, used to authorize and create the download for client side. The returned download's URL(s) only active for a very short period and must be used to connect to the downloads immediately.

This resource will only returns success response if the requested user_id and item_id is matched on user's owned items resource.

#### Generate Signature

Client needs to send **signature** parameter on request to download resource. To obtain the signature, some keys are created by looping the hash function within some data. The number of key is determined based on the last two digits of timestamp_unix + 1. The signature is taken from the latest key, which is generated by crypting DOWNLOAD_SALT and the predecessor key with SHA-256 hash function. The first key is generated by crypting DOWNLOAD_SALT and Format_data with SHA-256 hash function.

```
timestamp_unix: 192461648711

Keys = 11 + 1 = 12

DOWNLOAD_SALT :  36yjfhjgy86o75i6urkyfhjgvbcftdfgnbccnhu756yjthmfgjgk876uky76iyjtd

FORM1 = Integer(<item_id> x 2)

FORM2 = Integer(<user_id> + <timestamp_unix>)

Format_data = String(<timestamp_unix> + FORM1 + FORM2 + <DOWNLOAD_SALT>)

Key1 = sha256(<DOWNLOAD_SALT> + Format_data)

Key2 = sha256(<DOWNLOAD_SALT> + Key1)

Key3 = sha256(<DOWNLOAD_SALT> + Key2)

...

Key12 = sha256(<DOWNLOAD_SALT> + Key11)

SIGNATURE = key12
```

#### Key Encryption

AES Mode = CBC

SHARED KEY = "tuyjsue93iruydgkui7u6ryejtdghfjg"

**Key Encription:**
```
ENCODE_KEY = BASE64.ENCODE(<AES_KEY>)

KEY = <IV>+ENCODE_KEY
```

#### Download Properties

* `url` - String; Encrypted location that refers to the item

* `is_active` - Boolean; Download status

* `md5_checksum` - String; The hash encryption key used within the download

* `file_type` - Number; The type of the file that will be downloaded

* `file_status` - Number; The status of the file that will be downloaded

* `file_order` - Number; The number of file order

* `key` - String; Combination of IV 16 digits and AES key

* `item_id` - Number; Item identifier

#### Authorize and Create Download

Authorize and create download. The use case for this end-point call is to check if the user eligible to download specific item files. If eligible, the detail of eligible files will be returned; otherwise, "403 Forbidden" will be returned.

Created download instance will only active for 15 minutes.

**URL:** /downloads

**Method:** POST

**Request Data Params:**

Required: `user_id`, `item_id`, `timestamp_unix`, `signature`

Optional: `country_code`, `ip_address`, `file_orders` - can be filtered with operator `__in`, `__lte`, or `__gte` to get specified files

albeit `country_code` and `ip_address` are optional, one of them must be filled otherwise the request will be rejected.

**Response HTTP Code:**

* `201` - Download created and returned in body content

**Response Params:** See [Downloads Properties](#downloads-properties)

**Examples:**

Request with file orders specified in each:
```
POST /downloads HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "user_id": 307019,
    "item_id": 50377,
    "timestamp_unix": 192461648711,
    "signature": "e30838dc8400660940d2418e3c1abf55ec97876e69e9d9f73bdd0974dccdc2f6",
    "file_order__in": [1, 2, 3, 4, 5],
    "file_order__gte": 1,
    "file_order__lte": 20,
    "country_code": "ID",
    "ip_address": "139.0.18.154"
}
```
Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "downloads": [
        {
            "url": "http://182.253.226.134/Xkr6jVqvsydbhV2E8uuGpI1H1eVi1ubDo4xUTkD2T34FMI66tBATVfmMhfVP",
            "file_version": "1",
            "is_active": true,
            "md5_checksum": null,
            "file_type": 1,
            "file_status": 1,
            "file_order": 1,
            "key": "gAwkLuU4x5R6lcFEmT2EP6ynfDvLewCScaDlVxgvG6Q9KvtUmRxiZ3nsx4mu6eZOsW7SA038ZrFtDLTdIhHkIXfQArOuEbcVJ8VaNw==",
            "item_id": 27335
        }
    ],
    "metadata": {
        "resultset": {
            "count": 1,
            "offset": 0,
            "limit": 0
        }
    }
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

### Emails

**TODO: Implementation**

Email resource, to manage email from client side

#### Email Properties

* `from` - String; Sender

* `to` - Array; List of main recipients

* `cc` - Array; List of CC recipients

* `bcc` - Array; List of BCC recipients

* `subject` - String; Subject of email

* `body` - String; Body content of email

#### Send Email

Create new email and sednd.

**URL:** /emails/send

**Method:** POST

**Request Data Params:**

Required: `from`, `to`, `subject`, `body`

Optional: `cc`, `bcc`

**Response HTTP Code:**

* `201` - New email created and to be send

**Response Params:** See [Email Properties](#email-properties)

**Examples:**

Request:

```
POST /emails/send HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "to": ["js@apps-foundry.com"],
    "from": "no-reply@apps-foundry.copm",
    "subject": "Lorem ipsum dolor sit amet",
    "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "to": ["js@apps-foundry.com"],
    "from": "no-reply@apps-foundry.copm",
    "subject": "Lorem ipsum dolor sit amet",
    "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

### User Buffets

User buffets, like owned items, are objects that contain a list of all buffets
that have been purchased.

This endpoint is a read-only endpoint.

#### User Buffets Properties

* `id` - Number; User buffet identifier

* `user_id` - Number; User identifier of any SCOOP ID that purchased this buffet

* `orderline_id` - Number; Represents the 'id' of the line item, which this buffet was purchased through.

* `valid_to` - String; (in ISO 8601 DateTime Format) Represents the date and time that this buffet will no longer be available for purchase.

* `offerbuffet` - Object; (see OfferBuffet) Buffet data, exactly as what is present in the OfferBuffet get details API.

#### Retrieve User Buffets

Retrieves all of the User who purchased Buffets.

**URL:** /users/buffets

Optional:

* `id` - Filters by UserBuffet id, e.g :

    * id=10 - Filters any UserBuffet data with id equal to 10

    * id__lt=10 - Filters any UserBuffet data with id less than 10

    * id__gt=10 - Filters any UserBuffet data with id more than 10

* `user_id` - Filters any UserBuffet data by user_id

* `orderline_id` - Filters any UserBuffet data by orderlines_id

* `offerbuffet_id` - Filters any UserBuffet data by offerbuffet_id

* `valid_to` - Filters any UserBuffet data by valid_to. REQUIRES a full ISO 8601 datetime, which argument must be in URL-encoded format.

**Method:** GET

**Response HTTP Code:**

* `200` - User Buffets data are retrieved and returned in body content

**Response Params:** See [User Buffets Properties](#user-buffets-properties)

**Examples:**

Request:

```
GET /user/buffets?user_id=1234 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"id": 1234,
	"user_id": 1234,
	"orderline_id": 1234,
	"offerbuffet_id": 1,
	"valid_to": "2015-10-10T00:00:00+00:00",
	"offerbuffet": {
		{
			"available_from": "2015-05-01T00:00:00+00:00",
			"available_until": "2016-05-01T00:00:00+00:00",
			"buffet_duration": "P30DT0S",
			"id": 1,
			"offer": {
				"aggr_price_idr": 0,
				"aggr_price_point": 0,
				"aggr_price_usd": null,
				"brands": [
					12,
					5,
					36
				],
				"discount_id": null,
				"discount_name": null,
				"discount_price_idr": 0,
				"discount_price_point": 0,
				"discount_price_usd": null,
				"discount_tag": null,
				"exclusive_clients": null,
				"id": 41975,
				"image_highres": null,
				"image_normal": null,
				"is_active": true,
				"is_discount": false,
				"is_free": false,
				"item_code": null,
				"items": [
					20051,
					20052
				],
				"name": "MAY BUFFET OFFER",
				"offer_code": null,
				"offer_status": 7,
				"offer_type_id": 4,
				"platforms_offers": [],
				"price_idr": 144440,
				"price_point": 0,
				"price_usd": 14.44,
				"sort_priority": 1,
				"vendor_price_idr": 0,
				"vendor_price_point": 0,
				"vendor_price_usd": null
			}
		}
	}
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Publisher Reminder

This resource will be used to notify the publisher about the content schedule

#### Publisher Reminder Properties

* `id` - Number; Publisher reminder identifier

* `brand_id` - Number; Brand identifier, the notification is based on the brand which publisher issues

* `due_date` - Number; The due date/day the publisher upload the content(s), depends on the reminder type:

    * weekly : due date is set in 1 (monday), 2 (tuesday), ... 7 (sunday)

    * monthly : due date represents the date

* `due_time` - String; Datetime in UTC, due time of the reminder

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. The default value is true

* `last_reminder_date` - String; Datetime in UTC, the last date of the reminder

* `reminder_date` - Number; List of number at what date/day the reminder will notify publisher

* `reminder_time` - String; Time in UTC, at what time the reminder will notify publisher

* `reminder_type` - String; The pattern of reminder frequency: weekly, monthly


#### Create Publisher Reminder

**URL:** /publisher_reminder_settings

**Method:** POST

**Request Data Params:**

Required: `brand_id`, `reminder_type`, `due_date`, `due_time`, `reminder_date`, `reminder_time`, `last_reminder_date`, `is_active`

**Response HTTP Code:**

* `201` - New Publisher Reminder is Created

**Response Params:** See [Publisher Reminder Properties](#publisher-reminder-properties)

**Examples:**

Request:

```
POST /publisher_reminder_settings HTTP/1.1
Accept: application/json
Content-Type: application/json
Publisher Reminderization: Bearer ajksfhiau12371894^&*@&^$%

{
    "brand_id": 457,
    "reminder_type": "daily",
    "due_date": 1,
    "due_time": "12:12:12",
    "reminder_date": [
        1,
        2,
        3,
        4
      ],
    "reminder_time": "11:11:11",
    "last_reminder_date": "2015-08-05T14:32:28.481223",
    "is_active": true
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
    "id"
    "brand_id": 457,
    "reminder_type": "daily",
    "due_date": 1,
    "due_time": "12:12:12",
    "reminder_date": [
        1,
        2,
        3,
        4
      ],
    "reminder_time": "11:11:11",
    "last_reminder_date": "2015-08-05T14:32:28.481223",
    "is_active": true
}

```

#### Retrieve Publisher Reminders

Retrieves publisher reminder logs

**URL:** /publisher_reminder_settings

**Method:** GET

**Response HTTP Code:**

* `200` -  Publisher Reminder retrieved and returned in body content

**Response Params:** See [Publisher Reminder Properties](#publisher-reminder-properties)

**Examples:**

Request:

```
GET /publisher_reminder_settings HTTP/1.1
Publisher Reminderization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "publisher_reminder_settings": [
        {
        "id": 1,
        "brand_id": 457,
        "reminder_type": "daily",
        "due_date": 1,
        "due_time": "12:12:12",
        "reminder_date": [
            1,
            2,
            3,
            4
        ],
        "reminder_time": "11:11:11",
        "last_reminder_date": "2015-08-05T14:32:28.481223",
        "is_active": true
        },
        {
        "id": 2,
        "brand_id": 459,
        "reminder_type": "weekly",
        "due_date": 1,
        "due_time": "12:12:12",
        "reminder_date": [
            1,
            2,
            3,
            4
        ],
        "reminder_time": "11:11:11",
        "last_reminder_date": "2015-08-05T14:32:28.481223",
        "is_active": true
        }
    ],
    "metadata": {
        "resultset": {
            "count": 1,
            "offset": 0,
            "limit": 20
        }
    }
}

```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```
#### Retrieve Individual Publisher Reminders

Retrieves publisher reminder logs

**URL:** /publisher_reminder_settings/{publisher_reminder_settings_id}

**Method:** GET

**Response HTTP Code:**

* `200` -  Publisher Reminder retrieved and returned in body content

**Response Params:** See [Publisher Reminder Properties](#publisher-reminder-properties)

**Examples:**

Request:

```
GET /publisher_reminder_settings/1 HTTP/1.1
Publisher Reminderization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "brand_id": 457,
    "reminder_type": "daily",
    "due_date": 1,
    "due_time": "12:12:12",
    "reminder_date": [
        1,
        2,
        3,
        4
    ],
    "reminder_time": "11:11:11",
    "last_reminder_date": "2015-08-05T14:32:28.481223",
    "is_active": true
}

```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Update Publisher Reminder

Updates publisher reminder.

**URL:** /publisher_reminder_settings/{publisher reminder_id}

**Method:** PUT

**Request Data Params:**

Required: `brand_id`, `reminder_type`, `due_date`, `due_time`, `reminder_date`, `reminder_time`, `last_reminder_date`, `is_active`

**Response HTTP Code:**

* `200` - Publisher Reminder updated

**Response Params:** See [Publisher Reminder Properties](#publisher-reminder-properties)

**Examples:**

Request:

```
PUT /publisher reminders/1 HTTP/1.1
Content-Type: application/json
Publisher Reminderization: Bearer ajksfhiau12371894^&*@&^$%

{
    "brand_id": 457,
    "reminder_type": "daily",
    "due_date": 1,
    "due_time": "12:12:12",
    "reminder_date": [
        1,
        2,
        3,
        4
    ],
    "reminder_time": "11:11:11",
    "last_reminder_date": "2015-08-05T14:32:28.481223",
    "is_active": true
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "brand_id": 457,
    "reminder_type": "daily",
    "due_date": 1,
    "due_time": "12:12:12",
    "reminder_date": [
        1,
        2,
        3,
        4
    ],
    "reminder_time": "11:11:11",
    "last_reminder_date": "2015-08-05T14:32:28.481223",
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Publisher Reminder

Delete publisher reminder. This service will set `is_active` value to false.

**URL:** /publisher_reminder_settings/{publisher_reminder_settings_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Publisher Reminder deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /publisher_reminder_settings/1 HTTP/1.1
Publisher Reminderization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

---

## MISC

### Geolocations

This resource is used to retrieve geolocation informations. Currently the supported input is IP Address.

#### Geolocation Properties

* `city` - Object; City of the geolocation.

    * `name` - String; City's name.

* `postal` - Object; Postal of the geolocation.

    * `code` - Number; Postal code.

* `subdivision` - Object; Subdivision of the country.

    * `iso_code` - String; Subdivision's ISO code.

    * `name` - String; Subdivision's name.

* `location` - Object; Location of the geolocation.

    * `latitude` - Number; Location's latitude.

    * `longitude` - Number; Location's longitude.

    * `accuracy_radius` - Number; Location's accuracy radius.

    * `time_zone` - String; Location's time zone based on [IANA Time Zone Database](#http://en.wikipedia.org/wiki/List_of_IANA_time_zones).

    * `metro_code` - Number; Location's metro code.

* `traits` - Object; Traits of the network.

    * `domain` - String; Traits' domain.

    * `isp` - String; Internet Service Provider information.

    * `user_type` - Number; User type.

    * `organization` - String; Time zone based on [IANA Time Zone Database](#http://en.wikipedia.org/wiki/List_of_IANA_time_zones).

    * `ip_address` - String; Internet Protocol address.

    * `is_anonymous_proxy` - Boolean; Anonymous proxy status, used to mark if the proxy is anonymous or not.

    * `is_satellite_provider` - Boolean; Satellite provider status, used to mark if the provider is satellite based or not.

* `country` - Object; Country of the geolocation.

    * `code_alpha2` - String; Country code in [ISO 3166-1 alpha-2](#http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2).

    * `name` - String; Country name.

* `represented_country` - Object; Country represented by the user when that country is different than the country in country. For instance, the country represented by an overseas military base.

    * `code_alpha2` - String; Repesented country code in [ISO 3166-1 alpha-2](#http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2).

    * `name` - String; Repesented country name.

    * `confidence` - String; Confidence.

    * `represented_country` - String; Repesented country.

* `continent` - Object; Continent of the geolocation.

    * `code` - String; Continent's code.

    * `name` - String; Continent's name.

#### Retrieve Individual Geolocation by IP Address

Retrieve location information from provided IP address

**URL:** /geolocations/ip_address/{ipv4_address or ipv6_address}

Optional:

* `fields` - Retrieve specific fields. See [Geolocation Properties](#geolocation-properties). Default to ALL

**Method:** GET

**Request Data Params:** -

**Response HTTP Code:**

* `200` - Location found and returned

**Response Params:** See [Geolocation Properties](#geolocation-properties)

**Examples:**

Request:

```
GET /geolocations/ip_address/217.33.193.179?fields=code_alpha3 HTTP/1.1
Authorization: Bearer MzM3MzczOjQxNGU2ZGMwODY2ZWQ0ZmYwZmE0NGE0OTY2OTNiYjgy
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "city":{
        "name":null
    },
    "postal":{
        "code":null
    },
    "subdivision":{
        "iso_code":null,
        "name":null
    },
    "location":{
        "latitude":39.76,
        "accuracy_radius":null,
        "time_zone":null,
        "longitude":-98.5,
        "metro_code":null
    },
    "traits":{
        "domain":null,
        "isp":null,
        "user_type":null,
        "organization":null,
        "ip_address":"2607:f0d0:1002:51::4",
        "is_anonymous_proxy":false,
        "is_satellite_provider":false
    },
    "country":{
        "code_alpha2":"US",
        "name":"United States"
    },
    "represented_country":{
        "code_alpha2":null,
        "confidence":null,
        "represented_country":null,
        "name":null
    },
    "continent":{
        "code":"NA",
        "name":"North America"
    }
}
```

Fail response:

Below response is returned if couldn't found any matched geolocation for the provided IP address

```
HTTP/1.1 404 Not Found
```

### Server Timestamp

This resource is used to retrieve current server's timestamp

#### Server Timestamp Properties

* `api_status` - String; The status of API while being accessed

* `server_timestamp` - String; Datetime in UTC when the server is accessed

#### Retrieve Server Timestamp

Retrieves the server timestamp and API status

**URL:** http://scoopadm.apps-foundry.com/scoopcor/api/v1

**Method:** GET

**Request Data Params:** -

**Response HTTP Code:**

* `200` - Timestamp returned

**Response Params:** See [Server Timestamp Properties](#server-timestamp-properties)

**Examples:**

Request:

```
GET http://scoopadm.apps-foundry.com/scoopcor/api/v1 HTTP/1.1
Authorization: Bearer MzM3MzczOjQxNGU2ZGMwODY2ZWQ0ZmYwZmE0NGE0OTY2OTNiYjgy
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "api_status": "Normal",
    "server_timestamp": "2015-05-21T07:35:30+00:00"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```
---

## Logs

### Analytic Log

This resource is used to record user activity in SCOOP application: item downloads and page view time-lapse. There are two logs: [Pageviews](#pageviews) and [Downloads](#download)

#### Analytic Log Pageview Properties

* `id` - Number; Pageview identifier

* `client_version` - String; The application version the user used

* `datetime` - String; Datetime in UTC user attempted the download or performed the pageview

* `session_name` - String; Name of the session

* `device_id` - String; Device identifier

* `device_model` - String; Device model

* `duration` - Number; The amount of time (in seconds) the user is viewing the page.

* `ip_address` - Number; The IP Address of the user accessing the pageview

* `item_id` - Number; Item identifier

* `location` - String; Location where the pageview is accessed. Shown based on Latitude and Logitude, separated by space. It must be +/- numbers and not include N/S W/E suffixes

* `online_status` - String; Whether the pageview is accessed in "online" or "offline" mode (cached data). If the pageview is accessed via getSCOOP.com, it will always be "online"

* `os_version` - String; The version of operating system the user used

* `page_number` - Number; The list is based on the page orientation, consist of 1 value if the orientation is potrait and 2 values if landscape

* `page_orientation` - String; How the pageview is displayed, whether it is "landscape" or "potrait"

* `user_id` - Number; User identifier

#### Create Analytic Logs

Bulk creates analytic logs that consist of pageviews and downloads

**URL:** /analytics

**Method:** POST

**Request Data Params:**

Required: Required: `device_id`, `user_id`, `item_id`, `session_name`, `ip_address`, `datetime`, `page_orientation`, `page_number`, `duration`, `online_status`

Optional: `location`, `client_version`, `os_version`, `device_model`

**Response HTTP Code:**

* `201` - Download created and returned in body content

**Response Params:** See [Analytic Log Properties](#analytic-log-properties)

**Examples:**

Request with file orders specified in each:
```
POST /analytics HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "pageviews":
    [   {
        "device_id": "123",
    	"user_id": 1,
    	"item_id": 1,
	    "session_name": "some session",
	    "ip_address": "127.0.0.1",
    	"page_orientation": "landscape",
    	"page_number": [5, 6],
    	"duration": 123,
    	"online_status": "online",
    	"location": "64.54 32.64",
    	"client_version": "1.2.3",
    	"os_version": "abc",
    	"device_model": "abc",
    	"datetime": "2015-03-23T10:11:30.121365"
    	},
    	{
    	"device_id": "345",
    	"user_id": 1,
    	"item_id": 1,
	    "session_name": "some session",
	    "ip_address": "127.0.0.1",
    	"page_orientation": "potrait",
    	"page_number": [123],
    	"duration": 123,
    	"online_status": "online",
    	"location": "64.54 32.64",
    	"client_version": "1.2.3",
    	"os_version": "abc",
    	"device_model": "abc",
    	"datetime": "2015-03-23T10:11:30.121365"
    	}
    ],
    "downloads":
    [   {
        "device_id": "123",
	    "user_id": 1,
	    "item_id": 1,
	    "session_name": "some session",
	    "ip_address": "127.0.0.1",
	    "download_status": "success",
	    "location": "64.54 32.64",
	    "client_version": "1.2.3",
	    "os_version": "abc",
	    "device_model": "abc",
    	"datetime": "2015-03-23T10:11:30.121365"
    	},
    	{
    	"device_id": "123",
	    "user_id": 1,
	    "item_id": 1,
	    "session_name": "some session",
	    "ip_address": "127.0.0.1",
	    "download_status": "success",
	    "location": "64.54 32.64",
	    "client_version": "1.2.3",
	    "os_version": "abc",
	    "device_model": "abc",
    	"datetime": "2015-03-23T10:11:30.121365"
    	}
    ]

}
```
Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "pageviews":
    [   {
        "id": 1,
        "device_id": "123",
    	"user_id": 1,
    	"item_id": 1,
	    "session_name": "some session",
	    "ip_address": "127.0.0.1",
    	"page_orientation": "landscape",
    	"page_number": [5, 6],
    	"duration": 123,
    	"online_status": "online",
    	"location": "64.54 32.64",
    	"client_version": "1.2.3",
    	"os_version": "abc",
    	"device_model": "abc",
    	"datetime": "2015-03-23T10:11:30.121365"
    	},
    	{
    	"id": 2,
    	"device_id": "345",
    	"user_id": 1,
    	"item_id": 1,
	    "session_name": "some session",
	    "ip_address": "127.0.0.1",
    	"page_orientation": "potrait",
    	"page_number": [123],
    	"duration": 123,
    	"online_status": "online",
    	"location": "64.54 32.64",
    	"client_version": "1.2.3",
    	"os_version": "abc",
    	"device_model": "abc",
    	"datetime": "2015-03-23T10:11:30.121365"
    	}
    ],
    "downloads":
    [   {
        "id": 1,
        "device_id": "123",
	    "user_id": 1,
	    "item_id": 1,
	    "session_name": "some session",
	    "ip_address": "127.0.0.1",
	    "download_status": "success",
	    "location": "64.54 32.64",
	    "client_version": "1.2.3",
	    "os_version": "abc",
	    "device_model": "abc",
    	"datetime": "2015-03-23T10:11:30.121365"
    	},
    	{
    	"id": 2,
    	"device_id": "123",
	    "user_id": 1,
	    "item_id": 1,
	    "session_name": "some session",
	    "ip_address": "127.0.0.1",
	    "download_status": "success",
	    "location": "64.54 32.64",
	    "client_version": "1.2.3",
	    "os_version": "abc",
	    "device_model": "abc",
    	"datetime": "2015-03-23T10:11:30.121365"
    	}
    ]

}
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Pageviews

**Test URL:** dev2.apps-foundry.com/scoopcor/v1/pageview

**Production URL:** scoopadm.apps-foundry.com/scoopcor/v1/pageview

#### Analytic Log Pageview Properties

* `id` - Number; Pageview identifier

* `client_version` - String; The application version the user used

* `datetime` - String; Datetime in UTC user attempted the download or performed the pageview

* `session_name` - String; Name of the session

* `device_id` - String; Device identifier

* `device_model` - String; Device model

* `duration` - Number; The amount of time (in seconds) the user is viewing the page

* `ip_address` - Number; The IP Address of the user accessing the pageview

* `item_id` - Number; Item identifier

* `location` - String; Location where the pageview is accessed. Shown based on Latitude and Logitude, separated by space. It must be +/- numbers and not include N/S W/E suffixes

* `online_status` - String; Whether the pageview is accessed in "online" or "offline" mode (cached data). If the pageview is accessed via getSCOOP.com, it will always be "online"

* `os_version` - String; The version of operating system the user used

* `page_number` - Number;

* `page_orientation` - String; How the pageview is displayed, whether it is "landscape" or "potrait"

* `user_id` - Number; User identifier

#### Create Pageview Analytic Log

**URL:** /pageviews

**Method:** POST

**Request Data Params:**

Required: `device_id`, `user_id`, `item_id`, `session_name`, `ip_address`, `datetime`, `page_orientation`, `page_number`, `duration`, `online_status`

Optional: `location`, `client_version`, `os_version`, `device_model`

**Response HTTP Code:**

* `201` - Created

**Response Params:** See [Analytic Log Pageview Properties](#analytic-log-pageview-properties)

**Examples:**

Request:

```
POST scoopadm.apps-foundry.com/scoopcor/v1/pageviews HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "device_id": "123",
    "user_id": 1,
    "item_id": 1,
    "session_name": "some session",
    "ip_address": "127.0.0.1",
    "page_orientation": "landscape",
    "page_number": 123,
    "duration": 123,
    "online_status": "online",
    "location": "64.54 32.64",
    "client_version": "1.2.3",
    "os_version": "abc",
    "device_model": "abc",
    "datetime": "2015-03-23T10:11:30.121365"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "device_id": "123",
    "user_id": 1,
    "item_id": 1,
    "session_name": "some session",
    "ip_address": "127.0.0.1",
    "page_orientation": "landscape",
    "page_number": 123,
    "duration": 123,
    "online_status": "online",
    "location": "64.54 32.64",
    "client_version": "1.2.3",
    "os_version": "abc",
    "device_model": "abc",
    "datetime": "2015-03-23T10:11:30.121365"
}
```

#### Retrieve Analytic Log Pageviews

Retrieves pageviews analytic logs

**URL:** /pageviews

**Method:** GET

**Response HTTP Code:**

* `200` -  Analytic Log Pageviews retrieved and returned in body content

**Response Params:** See [Analytic Log Pageview Properties](#analytic-log-pageview-properties)

**Examples:**

Request:

```
GET /pageviews HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "pageviews": [
        {
            "id": 1,
            "device_id": "123",
            "user_id": 1,
            "item_id": 1,
    		"session_name": "some session",
    		"ip_address": "127.0.0.1",
    		"page_orientation": "landscape",
    		"page_number": 123,
    		"duration": 123,
    		"online_status": "online",
    		"location": "64.54 32.64",
    		"client_version": "1.2.3",
    		"os_version": "abc",
    		"device_model": "abc",
    		"datetime": "2015-03-23T10:11:30.121365"
        },
        {
            "id":2,
            "device_id": "123",
            "user_id": 1,
            "item_id": 1,
    		"session_name": "some session",
    		"ip_address": "127.0.0.1",
    		"page_orientation": "landscape",
    		"page_number": 123,
    		"duration": 123,
    		"online_status": "online",
    		"location": "64.54 32.64",
    		"client_version": "1.2.3",
    		"os_version": "abc",
    		"device_model": "abc",
    		"datetime": "2015-03-23T10:11:30.121365"
        },
        {...}
    ],
    "metadata": {
        "resultset": {
            "count": 16,
            "offset": 0,
            "limit": 20
        }
    }
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Download

**Test URL:** dev2.apps-foundry.com/scoopcor/v1/download

**Production URL:** scoopadm.apps-foundry.com/scoopcor/v1/download


#### Analytic Log Download Properties

* `id` - Number; Download log identifier

* `client_version` - String; The application version the user used

* `session_name` - String; Name of the session

* `datetime` - String; Datetime in UTC user attempted the download or performed the download

* `device_id` - String; Device identifier

* `device_model` - String; Device model

* `ip_address` - Number; The IP Address of the user accessing the download

* `item_id` - Number; Item identifier

* `location` - String; Location where the download is accessed. Shown based on Latitude and Logitude, separated by space. It must be +/- numbers and not include N/S W/E suffixes

* `download_status` - String; Whether the download is "success" or "fail"

* `os_version` - String; The version of operating system the user used

* `user_id` - Number; User identifier


#### Record Analytic Log Download

**URL:** /download

**Method:** POST

**Request Data Params:**

Required: `device_id`, `user_id`, `item_id`, `session_name`, `ip_address`, `datetime`, `download_status`

Optional: `location`, `client_version`, `os_version`, `device_model`

**Response HTTP Code:**

* `201` - Created

**Response Params:** See [Analytic Log Download Properties](#analytic-log-download-properties)

**Examples:**

Request:

```
POST scoopadm.apps-foundry.com/scoopcor/v1/downloads HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "device_id": "123",
    "user_id": 1,
    "item_id": 1,
    "session_name": "some session",
    "ip_address": "127.0.0.1",
    "download_status": "success",
    "location": "64.54 32.64",
    "client_version": "1.2.3",
    "os_version": "abc",
    "device_model": "abc",
    "datetime": "2015-03-23T10:11:30.121365"
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
    "id": 3,
    "device_id": "123",
    "user_id": 1,
    "item_id": 1,
    "session_name": "some session",
    "ip_address": "127.0.0.1",
    "download_status": "success",
    "location": "64.54 32.64",
    "client_version": "1.2.3",
    "os_version": "abc",
    "device_model": "abc",
    "datetime": "2015-03-23T10:11:30.121365"
}
```

#### Retrieve Analytic Log Downloads

Retrieves downloads analytic logs

**Downloads Properties**

* `id` - Number; Download log identifier

* `client_version` - String; The application version the user used

* `session_name` - String; Name of the session

* `datetime` - String; Datetime in UTC user attempted the download or performed the download

* `device_id` - String; Device identifier

* `device_model` - String; Device model

* `ip_address` - Number; The IP Address of the user accessing the download

* `item_id` - Number; Item identifier

* `location` - String; Location where the download is accessed. Shown based on Latitude and Logitude, separated by space. It must be +/- numbers and not include N/S W/E suffixes

* `download_status` - String; Whether the download is "success" or "fail"

* `os_version` - String; The version of operating system the user used

* `user_id` - Number; User identifier

**URL:** /downloads

**Method:** GET

**Response HTTP Code:**

* `200` -  Analytic Log Downloads retrieved and returned in body content

**Response Params:** See [Analytic Log Download Properties](#analytic-log-download-properties)

**Examples:**

Request:

```
GET /downloads HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "downloads": [
        {
            "id": 1,
            "device_id": "123",
            "user_id": 1,
    		"item_id": 1,
    		"session_name": "some session",
    		"ip_address": "127.0.0.1",
    		"download_status": "success",
    		"location": "64.54 32.64",
    		"client_version": "1.2.3",
    		"os_version": "abc",
    		"device_model": "abc",
    		"datetime": "2015-03-23T10:11:30.121365"
        },
        {
            "id": 2,
            "device_id": "123",
            "user_id": 1,
    		"item_id": 1,
    		"session_name": "some session",
    		"ip_address": "127.0.0.1",
    		"download_status": "success",
    		"location": "64.54 32.64",
    		"client_version": "1.2.3",
    		"os_version": "abc",
    		"device_model": "abc",
    		"datetime": "2015-03-23T10:11:30.121365"
        },
        {...}
    ],
    "metadata": {
        "resultset": {
            "count": 16,
            "offset": 0,
            "limit": 20
        }
    }
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Open App

This resource will be used to record user activity (accessing the application)

**Test URL:** dev5.apps-foundry.com/scoopcor/v1/analytics/app_open

**Production URL:** scoopadm.apps-foundry.com/scoopcor/v1/analytics/app_open


#### Open App Log Properties

* `id` - Number; Download log identifier

* `client_version` - String; The application version the user used

* `session_name` - String; Name of the session

* `datetime` - String; Datetime in UTC user attempted the download or performed the download

* `device_id` - String; Device identifier

* `device_model` - String; Device model

* `ip_address` - Number; The IP Address of the user accessing the download

* `location` - String; Location where the download is accessed. Shown based on Latitude and Logitude, separated by space. It must be +/- numbers and not include N/S W/E suffixes

* `os_version` - String; The version of operating system the user used

* `user_id` - Number; User identifier

* `client_id` - Number; User's client identifier

* `activity_type` - String; Type of the activity, the default value is "app_open"


#### Record Open App Log

**URL:** /app_open

**Method:** POST

**Request Data Params:**

Required: `device_id`, `user_id`, `item_id`, `session_name`, `ip_address`

Optional: `location`, `client_version`, `client_id`, ``os_version`, `device_model`, `datetime`, `activity_type`

**Response HTTP Code:**

* `201` - New Open App Log is Created

**Response Params:** See [Open App Log Properties](#open-app-log-properties)

**Examples:**

Request:

```
POST /app_open HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "activity_type": "app_open",
	"device_id": "oaly2Uijsn8Ikxnl0lakmnGtsjklu44772b47aca5f6d5743218af7e08aacb",
	"user_id": 359942,
	"session_name": "20150504 17117 63989",
	"ip_address": "120.184.95.185",
	"location": null,
	"client_id": 1,
	"client_version": "4.2.1",
	"os_version": "8.1.3",
	"datetime": "2015-05-04T17:27:43+00:00",
	"device_model": "iPad4,5"
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "activity_type": "app_open",
	"device_id": "oaly2Uijsn8Ikxnl0lakmnGtsjklu44772b47aca5f6d5743218af7e08aacb",
	"user_id": 359942,
	"session_name": "20150504 17117 63989",
	"ip_address": "120.184.95.185",
	"location": null,
	"client_id": 1,
	"client_version": "4.2.1",
	"os_version": "8.1.3",
	"datetime": "2015-05-04T17:27:43+00:00",
	"device_model": "iPad4,5"
}

```

#### Retrieve Open App Log

Retrieves downloads analytic logs

**URL:** /app_open

**Method:** GET

**Response HTTP Code:**

* `200` -  Open App Log retrieved and returned in body content

**Response Params:** See [Open App Log Properties](#open-app-log-properties)

**Examples:**

Request:

```
GET /app_open HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "downloads": [
        {
        "id": 1,
	    "activity_type": "app_open",
		"device_id": "oaly2Uijsn8Ikxnl0lakmnGtsjklu44772b47aca5f6d5743218af7e08aacb",
		"user_id": 359942,
		"session_name": "20150504 17117 63989",
		"ip_address": "120.184.95.185",
		"location": null,
		"client_id": 1,
		"client_version": "4.2.1",
		"os_version": "8.1.3",
		"datetime": "2015-05-04T17:27:43+00:00",
		"device_model": "iPad4,5"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 1,
            "offset": 0,
            "limit": 20
        }
    }
}

```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Facebook Log

This resource will be used to manage facebook log in publisher revenue calculation

#### Facebook Log Properties

* `id` - Number; Facebook log identifier

* `facebook_data` - JSON data retrieved from facebook API

* `location`- String; Location where the user access application

* `client_version` - String;

* `device_id` - String; Current device identifier

* `old_device_id` - String; Device identifier

* `session_name` - String; How the system recognise the session

* `datetime` - String; Datetime in UTC format whenever the user access application

* `os_version` - String; OS version of user's device while accessing application

* `device_model` - String; User's device model while accessing application

* `client_id` - String; Client identifier

* `ip_address` - String; IP Address while accessing application

#### Create Facebook Log

**URL:** /analytics?migration_guest_login

**Method:** POST

**Request Data Params:**

Required: `payment_gateway_id`

Optional: all

**Response HTTP Code:**

* `201` - New Facebook Log is Created

**Response Params:** See [Facebook Log Properties](#facebook-log-properties)

**Examples:**

Request:

```
POST /analytics?migration_guest_login HTTP/1.1
Accept: application/json
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"id": 1,
  	"facebook_data":
  	{
    	"id": "1039775249",
    	"link": "http://www.facebook.com/1039775249",
    	"verified": true,
    	"locale": "en_US",
    	"bio": "Our lives improve only when we take chances and the first and the most difficult risk we can take is to be honest with ourselves.",
    	"updated_time": "2015-06-05T21:05:41+0000",
    	"location":
    	{
			"id": "102173726491792",
			"name": "Jakarta, Indonesia"
    	},
		"birthday": "06/25/1989",
		"first_name": "Harry",
		"timezone": 7,
		"last_name": "Ersan",
		"email": "harry_25689@yahoo.co.id",
		"gender": "male",
		"name": "Harry Ersan"
  	},
	"location": "-6.1744 106.8294",
	"client_version": "4.4.2",
	"device_id": "oaly2Uijsn8Ikxnl0lakmnGtsjkludac0b708776b3e839e3a381d3019503c",
	"old_device_id": "oaly2Uijsn8Ikxnl0lakmnGtsjkludac0b708776b3e839e3a381d3019503c",
	"session_name": "2o4tulekfcx090djknladhfgdlfhglfdbvncxzmneoeiyu",
	"datetime": "2015-07-28T10:08:03Z",
	"os_version": "8.0.2",
	"device_model": "iPad4,1",
	"client_id": 1,
	"ip_address": "139.0.18.154"
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
	"id": 1,
  	"facebook_data":
  	{
    	"id": "1039775249",
    	"link": "http://www.facebook.com/1039775249",
    	"verified": true,
    	"locale": "en_US",
    	"bio": "Our lives improve only when we take chances and the first and the most difficult risk we can take is to be honest with ourselves.",
    	"updated_time": "2015-06-05T21:05:41+0000",
    	"location":
    	{
			"id": "102173726491792",
			"name": "Jakarta, Indonesia"
    	},
		"birthday": "06/25/1989",
		"first_name": "Harry",
		"timezone": 7,
		"last_name": "Ersan",
		"email": "harry_25689@yahoo.co.id",
		"gender": "male",
		"name": "Harry Ersan"
  	},
	"location": "-6.1744 106.8294",
	"client_version": "4.4.2",
	"device_id": "oaly2Uijsn8Ikxnl0lakmnGtsjkludac0b708776b3e839e3a381d3019503c",
	"datetime": "2015-07-28T10:08:03Z",
	"os_version": "8.0.2",
	"device_model": "iPad4,1",
	"client_id": 1,
	"ip_address": "139.0.18.154"
}

```
---

## What the Heck is All of This?? What Should I Do?

Relax.
