"""
Application Configuration
=========================

ScoopCOR settings come from environmental variables.  Anything that isn't truly dynamic, should not
be present in this configuration file, unless there is a 3rd party library requirement for it to be
set here.

Ideally, all your setting should exist somewhere close to where they are used (perhaps in 'constant'
variables), unless they need set differently on dev/local/staging/production.

Ask yourself if it's seriously required before adding anything here.

The idea behind this configuration file is that it always defaults to 'safe' settings.. not dev or production.
Ask yourself, "If we deploy this into production, and miss this setting, what would cause no harm".  Silently
running and dumping data to the database on a dev server IS EXTREMELY harmful =).  Ask yourself the reverse --
If I forget a setting in dev, will this cause problems (like writing to production).  Crashing the application
isn't good, but it's FAR FAR FAR better than not knowing a problem exists for weeks.

Table of Contents
-----------------

 .. note::

    These comments are temporary until we can get this guy cleaned up sufficiently

1. Runtime App Configuration
2. Credential Files / Local Filesystem Paths
3. Scoop Service URLS
4. 3rd-Party Service URLs
5. Other Static Files / WebReader & Previews
6. Static Settings (these things need to be removed ASAP)
7. Logging Configuration
"""
import base64, os

from config_helpers import _bool, _int, _str

# from elasticapm.contrib.flask import ElasticAPM
# from elasticapm.handlers.logging import LoggingHandler

_basedir = os.path.abspath(os.path.dirname(__file__))
_credentials_dir = os.path.abspath(os.path.join(_basedir, os.pardir))

# -----------------------------------------------------------------------------
# 1. Runtime App Configuration
DEBUG = _bool("DEBUG", False)
TESTING = _bool("TESTING", False)
PROPAGATE_EXCEPTIONS = True

# These 2 looks like it's only used by the e-mail sender
ADMIN_SERVER = 'LIVE SERVER'
LIVE_SERVER = True
SERVER_MOD = 'LIVE'

SQLALCHEMY_DATABASE_URI = 'postgresql://{user}:{password}@{host}:{port}/{db_name}'.format(
    user=_str("DB_USER", "scoopcor"),
    password=_str("DB_PASS", "hobbes"),
    host=_str("DB_HOST", "localhost"),
    port=_str("DB_PORT", "5432"),
    db_name=_str("DB_NAME", "scoopcor_db"))

GRAMEDIA_DATABASE_URI = 'postgresql://{user}:{password}@{host}:{port}/{db_name}'.format(
    user=_str("GM_DB_USER", "gramedia"),
    password=_str("GM_DB_PASS", "p@ssw0rd24"),
    host=_str("GM_DB_HOST", "localhost"),
    port=_str("GM_DB_PORT", "5432"),
    db_name=_str("GM_DB_NAME", "gramedia"))

SQLALCHEMY_TRACK_MODIFICATIONS = False

REDIS_HOST = _str('REDIS_HOST', 'localhost')
REDIS_PORT = _int('REDIS_PORT', 6379)
REDIS_DB = _int('REDIS_DB', 0)
REDIS_DB_PASS = _str('REDIS_DB_PASS', "ce5cfd4e2b81a9df584cf1e4892ac254d1ac8f57")

EMAIL_LOGIN = _int('EMAIL_LOGIN', False)
EMAIL_HOST = _str('EMAIL_HOST', 'email-smtp.us-west-2.amazonaws.com')
EMAIL_USER = _str('EMAIL_USER', 'AKIAJJLG6VE6WG6XOEXA')
EMAIL_PASS = _str('EMAIL_PASS', 'Am2/VNn1aBqgZF3gplH+evf0mYecVtQFgSg9Wv79GdIh')
EMAIL_PORT = _str('EMAIL_PORT', '587')
EMAIL_USE_TLS = _bool('EMAIL_USE_TLS', True)

ADMIN_EMAILS = ['ah@staff.gramedia.com']
OPERATOR_EMAILS = ['fa@staff.gramedia.com']
OPERATIONAL_EMAILS = ['ah@staff.gramedia.com']
MONITORING_EMAILS = ['ah@staff.gramedia.com', 'hendra@gramedia.digital', 'henry.limada@gramedia.digital',
                     'jessica.lim@gramedia.digital']

RESTORE_BUFFET_LIMIT = 5

JWT_SECRET = '3023jdkjfklfjlkdjfsalkj1231231kl23jljlkf231235099dfs23dkljKKAZS12'

SECRET_GENERATE_TOKEN_KEY = "e8wiy5hn34tnlw8s9eiythln534trws89iyrth5n34wtrw8s9iu"

# GENERAL FOR EXPIRED POINTS
POINT_RULE_BY = 'MONTH'  # this is point expired per YEAR
POINT_RULE_EXPIRED = 11  # duration 1 Year, 1 Month, 1 Day
POINT_CUT_DURATION = POINT_RULE_EXPIRED + 1
POINT_CUT = 50

PAYMENT_SALT = '347546a8bade6cda047081d51555a77da104ad7751c35961ed77f6ea10f2750d'

# Web/Reader -- used for an nginx-specific feature to auth downloads
NGINX_SECURE_MEDIA_BASE = '/media-secure'

# Flask Restful
DEFAULT_PAGE_SIZE = 20
# Turn off flask-restful's auto-generated help messages on 404s.
# These suggest possible alternate URLs, which we don't want.
# This is not documented in their API
# https://github.com/flask-restful/flask-restful/blob/master/flask_restful/__init__.py#L295
ERROR_404_HELP = False

CONSUMABLE_MARK = ".c."

POPULAR_REDIS_TIME = 86400
CAMPAIGNS_REDIS_TIME = 3600
LATEST_ITEM_REDIS_TIME = 1800
ITEM_DETAILS_REDIS_TIME = 3600 #1hours after consult with PM.

MAXIMUM_PAGE = 5
MAXIMUM_ALLOWED_PAGE_SIZE = 100

# -----------------------------------------------------------------------------
# 2. Credential Files


# -----------------------------------------------------------------------------
# 3. Scoop Service URLS
LOGIN_HEADERS = {'Content-Type': 'application/x-www-form-urlencoded'}
JSON_HEADERS = {'Content-Type': 'application/json'}
AUTH_HEADER = 'Bearer %s' % base64.b64encode('337374:eb9306a9f6dd8ac8c30771679d5df97c')

SERVER_SCOOPCAS = "https://scoopadm.apps-foundry.com/scoopcas/api/v1/"
SERVER_SCOOPCOR = "https://scoopadm.apps-foundry.com/scoopcor/api/v1/"
VERIFY_TOKEN = '%sauth/verify_token' % (SERVER_SCOOPCAS)
SCOOPCAS_USERS = '%susers' % (SERVER_SCOOPCAS)

OFFERS_BASE_URL = _str('MEDIA_OFFERS_BASE_URL', 'https://s3-ap-southeast-1.amazonaws.com/ebook-statics/')
MEDIA_BASE_URL = _str('MEDIA_BASE_URL', 'https://scoopadm.apps-foundry.com/ebook-covers/')
BANNER_BASE_URL = _str('MEDIA_BANNER_BASE_URL', 'https://scoopadm.apps-foundry.com/ebook-statics/banners/')

DIGITAL_MAGAZINE = "http://images.getscoop.com/magazine_static/"
SERVER_SCOOPCOR1 = "http://scoopadm.apps-foundry.com/magazine/"
REPORT_SERVICE_SC1_SINGLE = "services/9da129cfafbe3fe41694adfb432177cf/save_purchase_transaction_view_report_translation/"
REPORT_SERVICE_SC1_SUBS = "services/9da129cfafbe3fe41694adfb432177cf/save_bundle_subscription_purchase_transaction_view_report_translation/"

BASE_URL = "https://scoopadm.apps-foundry.com/scoopcor/api/v1/"
INT_BASE_URL = _str('INT_BASE_URL', 'http://localhost/scoopinterface')

SC1_ACTIVATE_ITEM = "http://scoopadm.apps-foundry.com/magazine/services/335b8657093b34f3hwdb48dj48f279fe56d12e62e5a3b0f7f_v2/item/activate/"
SC1_COUPON_PURCHASE = "http://scoopadm.apps-foundry.com/magazine/services/9da129cfafbe3fe41694adfb432177cf/update_coupon_history/"
SCOOPGS_SERVER = 'http://scoopadm.apps-foundry.com/scoopgs/geolocations/ip_address/'
UPLOAD_BASE_DIR = os.path.join(_basedir, 'static')
OFFER_UPLOAD_DIR = '/var/python/www/scoopcor/static/offers'

# Apache Solr
SOLR_URL = _str('SOLR_URL', 'http://dev-search.apps-foundry.com:8983/solr/scoop-main')
SOLR_BASE_URL = _str('SOLR_BASE_URL', 'http://dev-search.apps-foundry.com:8983/solr')
SOLR_SHAREDLIB_CORE = 'scoop-sharedlib-all'
# -----------------------------------------------------------------------------
# 4. 3rd-Party Service URLs

PAYPAL_MODE = "live"

PAYPAL_CLIENT_ID = "AUa1ohChuaacKJXHY2_bk2d0qZqKtxm4ZuCj-4ETQ7P3cOipetP3FJ56ipBE"
PAYPAL_SECRET = "ELzAFRD_wwhIvEJjLGaEVpBnjqSNHNSeMwhrAeQ5G5IyIHNEDTiEEHgeqWSe"

VERITRANS_SERVER_KEY = "ac93cd6b-225b-472c-95f9-501748d73994"
VERITRANS_CHARGE_ENDPOINT = "https://payments.veritrans.co.id/vtdirect/v1/charges"
VERITRANS_V2_SERVER_KEY = 'VT-server-lqN9L7i6_MWH8cUm-nUGtAS0'
VERITRANS_ACQUIRING_BANK = 'cimb'

ELEVENIA_API_KEY = "be07151aaee7f4bc38445586586a3330"
ELEVENIA_SANDBOX_MODE = False

GRAMEDIA_COM_API_URL = _str('GRAMEDIA_URL', '')
GRAMEDIA_COM_API_USER = _str('GRAMEDIA_USER', '')
GRAMEDIA_COM_API_PASSWORD = _str('GRAMEDIA_PASS', '')
GRAMEDIA_COM_AUTH_API_PATH = _str('GRAMEDIA_COM_AUTH_API', 'auth/api-token-auth/')

GRAMEDIA_SECRET_KEY = _str('GRAMEDIA_SECRET', 'om@0_^@!_hp7!3fjp(mr!r+el0iy^y=^bgt4q=2_j+=f39*w5l')

ECASH_NEW_TRANSACTION_URL = '/ecommgateway/services/ecommgwws'
ECASH_VALIDATION_URL = '/ecommgateway/validation.html'

ECASH_SITE = 'https://mandiriecash.com'
ECASH_USERNAME = '49801000012'
ECASH_PASSWORD = '2EFF62B1D102C20AC0082E0E7C0AE7EA'  # Token IPG
SERVER_ADDRESS = '182.253.226.138'

MANDIRI_CLICKPAY_USER = 'user'
MANDIRI_CLICKPAY_PASSWORD = 'pwd'
MANDIRI_SITE = 'http://scoopadm.apps-foundry.com:22334/velispayment/'

GOOGLE_TOKEN_INFO_API = "https://www.googleapis.com/oauth2/v1/tokeninfo"
GOOGLE_OAUTH_CREDENTIALS_FILE = os.path.join(_credentials_dir, 'google-iab-credentials.json')

GOOGLE_TOKEN_AUTH = "https://accounts.google.com/o/oauth2/token"
GOOGLE_API_PURCHASE = 'https://www.googleapis.com/androidpublisher/v3/applications/{}/purchases/products/{}/tokens/{}?access_token={}'
GOOGLE_TOKEN = _str('GOOGLE_TOKEN', 'unknown-token')
GOOGLE_CLIENT_ID = _str('GOOGLE_CLIENT_ID', 'unknown-client-id')
GOOGLE_CLIENT_SECRET = _str('GOOGLE_CLIENT_SECRET', 'unknown-client-secret')

APPLE_IAP_SHARED_SECRET = 'cc34aa7f178042d4aa8879e91a52e3c0'
URL_VERIFY_RECEIPT = '/verifyReceipt'
APPLE_IAP_HOST_LIVE = 'https://buy.itunes.apple.com'
APPLE_IAP_HOST_SANDBOX = 'https://sandbox.itunes.apple.com'

FINNET195_HOST_URL = 'https://billhosting.finnet-indonesia.com/prepaidsystem/195/'
FINNET195_MERCHANT_ID = 'APPF026'
FINNET195_PASSWORD = 'APPF#369'
FINNET195_RETURN_URL = 'https://scoopadm.apps-foundry.com/scoopcor/api/v1/payments/finnet195/return'
FINNET195_REQUEST_URL = 'response-insert.php'
FINNET195_CHECK_STATUS_URL = 'check-status.php'
FINNET195_TIMEOUT = 3600  # 1 Day

FASPAY_SANDBOX_MODE = False
FASPAY_MERCHANT_ID = 31140
FASPAY_MERCHANT = "SCOOPONLINE"
FASPAY_USER_ID = "bot31140"
FASPAY_PASSWORD = "p@ssw0rd"

KLIKPAYCODE = "07APLI0372"
CLEAR_KEY = "0j4y3KPLIy4P0elr"
KEY_ID = "306A3479334B504C4979345030656C72"

BCA_CALLBACK = "https://www.getscoop.com/id/confirm/faspay"

MIDTRANS_API = 'https://api.sandbox.midtrans.com' if DEBUG else 'https://api.midtrans.com'
MIDTRANS_API_CHARGE = '{}/{}'.format(MIDTRANS_API, 'v2/charge')
MIDTRANS_API_BIN = '{}/{}'.format(MIDTRANS_API, 'v1/bins')
MIDTRANS_SERVER_KEY = _str('MIDTRANS_SERVER_KEY', 'somekeyunknown')



# -----------------------------------------------------------------------------
# 5. Static Settings
# The vast majority of these need removed from our application config.

# Upload Items Publisher
UPLOAD_ITEMS_BASE = '/var/scoopcor/upload/'
UPLOAD_ITEM_TEMP = '/tmp/'
COVER_ITEM_BASE = '/tmp/covers/'
PREVIEW_ITEM_BASE = '/tmp/previews/'

# COVER PIXEL
GEN_SM_COVER = 178  # pixel
GEN_COV = 356  # pixel
GEN_BIG_COV = 380  # pixel
BIG_COV = 760  # pixel
DENSITY_DPI = 24  # dpi

GARAMZIP = "!@#$Gnjiolkuy44567890-yHUIkjhtrfgHY&0pl/09876`wdfBNJK"
GARAMPDF = "pou56^YHNyfvbn@#$%^YJM./-[)12efvb%tgHJkL@#RghjI()P:?!][98yh"
GARAMAUDIO = 'lMa9l1YasiqwpRqp129asqFaN2;!!@~*ja58a22q3asa:;kajq1osxAns1'

PRIVATE_KEY = os.path.join(_credentials_dir, 'scoop62_id_rsa')  # '/var/python/www/.ssh/id_rsa'
SSH_USER_NAME = 'scoopadm'
SSH_PASSWORD = 'p@ssw0rd24'

SERVER_62001 = "62001.apps-foundry.com"  # di pake / IP baru utk di biznet
SERVER_62001_PORT = 220
SERVER_62001_PATH = "/var/scoop62/img/1/"
SERVER_62001_DOWNLOAD_PATH = "/var/scoop62/downloads/"

SERVER_65001 = "65001.apps-foundry.com"
SERVER_65001_PORT = 2200
SERVER_65001_PATH = "/var/scoop65/img/1/"
SERVER_65001_DOWNLOAD_PATH = "/var/scoop65/downloads/"

SERVER_SCOOPADM = "scoopadm.apps-foundry.com"
SERVER_SCOOPADM_PORT = 2200
SERVER_SCOOPADM_PATH = "/var/python/www/scoopcore/magazine_static/images/1"

SERVER_STATIC = "static.getscoop.com"
SERVER_STATIC_PORT = 22000
SERVER_STATIC_PATH = "/var/scoop62/images/1"

SALT_LINK = 'tuyjsue93iruydgkui7u6ryejtdghfjg'
DOWNLOAD_SALT = '36yjfhjgy86o75i6urkyfhjgvbcftdfgnbccnhu756yjthmfgjgk876uky76iyjtd'


# *** ==========================================================================
#                               GRAMEDIA AWS
# *** ==========================================================================

AWS_STORAGE_BUCKET_NAME = _str('AWS_STORAGE_BUCKET_NAME', '')
AWS_SECRET_ACCESS_KEY = _str('AWS_SECRET_ACCESS_KEY', '')
AWS_S3_CUSTOM_DOMAIN = _str('AWS_S3_CUSTOM_DOMAIN', '')
AWS_ACCESS_KEY_ID = _str('AWS_ACCESS_KEY_ID', '')
AWS_DEFAULT_ACL = _str('AWS_DEFAULT_ACL', '')

BOTO3_GRAMEDIA_APP_ID = 'AKIAI2ZPKZILU2IRTCRQ'
BOTO3_GRAMEDIA_APP_SECRET = '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'

AWS_PUBLIC = 'https://s3-ap-southeast-1.amazonaws.com/'
BOTO3_NGINX = 'https://{}.s3.amazonaws.com/'

BOTO3_GRAMEDIA_MOBILE_BUCKET = 'ebook-mobilebundles'
BOTO3_BIZNET_MOBILE_BUNDLE = '/srv/scoopcor/www/app/static/item_content_files/processed'
BOTO3_NGINX_MOBILE_BUCKET = BOTO3_NGINX.format(BOTO3_GRAMEDIA_MOBILE_BUCKET)
NGINX_S3_MOBILE_BUNDLES = '/{}'.format(BOTO3_GRAMEDIA_MOBILE_BUCKET)

BOTO3_GRAMEDIA_ORIGINAL_BUCKET = 'ebook-originals'
BOTO3_BIZNET_ORIGINAL = '/srv/scoopcor/www/app/static/original_local'
BOTO3_NGINX_ORIGINAL_BUCKET = BOTO3_NGINX.format(BOTO3_BIZNET_ORIGINAL)
NGINX_S3_ORIGINAL_BUNDLES = '/{}'.format(BOTO3_GRAMEDIA_ORIGINAL_BUCKET)

BOTO3_GRAMEDIA_WEBREADER_BUCKET = 'ebook-webreaders'
BOTO3_BIZNET_WEBREADER = '/srv/scoopcor/www/app/static/item_content_files/web_reader'
BOTO3_NGINX_WEB_READER_BUCKET = BOTO3_NGINX.format(BOTO3_GRAMEDIA_WEBREADER_BUCKET)
NGINX_S3_WEB_READER = '/{}'.format(BOTO3_GRAMEDIA_WEBREADER_BUCKET)

BOTO3_GRAMEDIA_COVERS_BUCKET = 'ebook-covers'
BOTO3_BIZNET_COVERS = '/var/python/www/scoopcore/magazine_static/images/1'
BOTO3_NGINX_COVERS = BOTO3_NGINX.format(BOTO3_GRAMEDIA_COVERS_BUCKET)
NGINX_S3_COVERS = '/{}'.format(BOTO3_GRAMEDIA_COVERS_BUCKET)

BOTO3_GRAMEDIA_AUDIO_MOBILE_BUCKET = 'ebook-audiobook-bundle'
BOTO3_NGINX_AUDIO_MOBILE_BUCKET = BOTO3_NGINX.format(BOTO3_GRAMEDIA_AUDIO_MOBILE_BUCKET)
NGINX_S3_AUDIO_MOBILE_BUNDLES = '/{}'.format(BOTO3_GRAMEDIA_AUDIO_MOBILE_BUCKET)

BOTO3_GRAMEDIA_PREVIEW_BUCKET = 'ebook-previews'

BOTO3_GRAMEDIA_THUMBNAILS_BUCKET = 'ebook-thumbnails'
BOTO3_BIZNET_THUMBNAIL = '/srv/scoopcor/www/app/static/item_content_files/thumbnails'
NGINX_S3_THUMBNAILS = '/{}'.format(BOTO3_GRAMEDIA_THUMBNAILS_BUCKET)

BOTO3_GRAMEDIA_STATIC_BUCKET = 'ebook-statics'
BOTO3_NGINX_STATICS = BOTO3_NGINX.format(BOTO3_GRAMEDIA_STATIC_BUCKET)
NGINX_S3_STATICS = '/{}'.format(BOTO3_GRAMEDIA_STATIC_BUCKET)

BOTO3_LINK_EXPIRED = 3600  # second --> 1 hours

MAX_DEVICE_ALLOWED = 5
MAX_DEVICE_ALLOWED_FOR_ELIBRARY = 1
MAX_DEVICE_ALLOWED_FOR_CAFE = 10
MAX_DEVICE_WEB_READER = 1

# *** ==========================================================================
#                       EPUB UPLOADER SETTINGS
# *** ==========================================================================

EPUB_SANTIANG = 'santiang'
EPUB_SALTKEY = '4d0470542b1e034b3386dd19236189ff'

EPUB_IV = '0000000000000000'
EPUB_EXTENSIONS = ['.xhtml', '.xml', '.html', '.htm']

# *** ==========================================================================
#                       PARTNER_RELATION SETTINGS
# *** ==========================================================================
KOMPASID_API_SUBS = 'https://apiner.kompas.id/v1/membership/show'
KOMPASID_AUTH_KEY = _str('KOMPASID_AUTH_KEY', '33cde44a-7a13-11ea-8390-000d3aa2b8f9')
KOMPAS_BRAND = 1498

# *** ==========================================================================
#                       SC1 UPLOADER MIGRATION SETTINGS
# *** ==========================================================================

SC1_METADATA = 'http://scoopadm.apps-foundry.com/magazine/services/post_item/'
SC1_FILE_METADATA = 'http://scoopadm.apps-foundry.com/magazine/services/put_item/'
SC1_SALT = 'eb9306a9f6dd8ac8c30771679d5df97c'

WHITELABEL_CLIENT_ID = [45]

# NOTIFICATION KEY API
FIREBASE_KEY_API = 'loremipsum'  # firebase key already move to cas_clients

DW_API_USER = _str('DW_API_USER_ID', 'appadmin')
DW_API_PASSWORD = _str('DW_API_PWD', 'aplikasi')
# data warehouse api url (for eperpur dashboard report, etc)
DW_API_BASE_URL = _str('DW_API_BASE_URL',  'http://dw-prod.gramedia.io:8173')

ANALYTICS_API_BASE_URL = _str('ANALYTICS_API_BASE_URL', 'http://localhost:5003/analytics')

DW_JASPER_URL = _str('DW_JASPER_URL', 'http://report.apps-foundry.com:8080/jasperserver/flow.html?')
DW_JASPER_USER = _str('DW_JASPER_USER', 'client_eperpus')
DW_JASPER_PASS = _str('DW_JASPER_PASS', '3peRPusj4ya')

SCREENSHOT_VIOLATION_UNLOCK = _int('SCREENSHOT_VIOLATION_UNLOCK', 5)
SCREENSHOT_VIOLATION_EXPIRED_DURATION = _int('SCREENSHOT_VIOLATION_EXPIRED_DURATION', 30)
SCREENSHOT_VIOLATION_REPORT = _int('SCREENSHOT_VIOLATION_REPORT', 5)
SCREENSHOT_VIOLATION_ACTIVE = _bool('SCREENSHOT_VIOLATION_ACTIVE', True)

# *** ==========================================================================
#                       APM SETTINGS
# *** ==========================================================================
ELASTIC_APM = {
    'SERVICE_NAME': os.getenv('COR_APM_NAME', 'flask-app'),
    'SERVER_URL' : os.getenv('COR_APM_URL', ''),
    'SECRET_TOKEN': os.getenv('COR_APM_PASS', ''),
    'DEBUG': 'True',
}

# *** ==========================================================================
#                       EPERPUS SETTINGS
# *** ==========================================================================
EPERPUS_URL_BASE = _str('EPERPUS_URL_BASE', 'https://staging.eperpus.com')
EBOOKS_URL_BASE = _str('EBOOKS_URL_BASE', 'https://dev-portal.gramedia.io')

BULK_DIR = '/tmp/members'
BULK_TRANSACTION = '/tmp/transaction'
BULK_DELETE_MAX = 1000
BULK_DELETE_CSV_HEADER = ['username']
AWS_BUCKET = 'https://s3-ap-southeast-1.amazonaws.com/ebook-statics/eperpus'

BULK_ADD_MAX = 500
BULK_ADD_CSV_HEADER = ['username', 'email', 'password', 'name', 'job']
BULK_ADD_CSV_TRANSACTION_SINGLE_ISSUE = ['item id', 'harga', 'total harga', 'eksemplar']
BULK_ADD_CSV_TRANSACTION_SUBSCRIPTION_PURCHASE = ['offer id', 'eksemplar', 'tanggal mulai (yyyy-mm-dd hh:mm:ss)']

PASSWORD_HASH = _str('PASSWORD_HASH', '5gvi-ojhgf%^&YGBNeds1{')
RESTRICTED_VENDORS = _str('RESTRICTED_VENDORS', [])

# *** ==========================================================================
#                       E2PAY SETTINGS
# *** ==========================================================================
E2PAY_API = 'https://sandbox.e2pay.co.id' if DEBUG else 'https://payment.e2pay.co.id'
E2PAY_API_CHARGE = '{}/{}'.format(E2PAY_API, 'epayment/entry.asp')
E2PAY_API_RE_QUERY = '{}/{}'.format(E2PAY_API, 'epayment/enquiry.asp')

E2PAY_MERCHANT_CODE = _str('E2PAY_MERCHANT_CODE', "unknown")
E2PAY_MERCHANT_KEY = _str('E2PAY_MERCHANT_KEY', "unknown")


# -----------------------------------------------------------------------------
# 7. Logging Configuration

# 7.1 Graylog config
GRAYLOG_HOST = _str('GRAYLOG_HOST', 'logs.gramedia.io')
GRAYLOG_PORT = _int('GRAYLOG_PORT', 13303)
GRAYLOG_HANDLER = _str('GRAYLOG_HANDLER', 'graypy.GELFHandler')
USE_GRAYLOG = _bool('USE_GRAYLOG', False)

LOG_BASE_DIR = os.path.join(_basedir, 'logs')
LOGGER_NAME = 'scoopcor'
LOGGING_CONFIG = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(asctime)s - %(levelname)s - %(message)s - %(name)s'
        },
        'email': {
            'format': "Message type:       %(levelname)s\n"
                      "Location:           %(pathname)s:%(lineno)d\n"
                      "Module:             %(module)s\n"
                      "Function:           %(funcName)s\n"
                      "Time:               %(asctime)s\n\n"
                      "Message:\n\n"
                      "%(message)s"
        },
        'script_format': {
            'format': '%(asctime)s - %(levelname)s - %(message)s - %(name)s'
        },
        'json': {
            '()': 'pythonjsonlogger.jsonlogger.JsonFormatter',
        },
        'verbose': {
            'format': '%(process)-5d %(thread)d %(name)-50s %(levelname)-8s %(message)s'
        },
    },
    'filters': {
        'api_format_filter': {
            '()': 'app.applog.ScoopLogFormatFilter',
        },
        'auth_failure_filter': {
            '()': 'app.applog.AuthFailureFilter',
        },
        'request_data_filter': {
            '()': 'app.applog.RequestDataFilter'
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'default',
        },
        'app_log_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'default',
            'filters': ['api_format_filter', ],
            'filename': os.path.join(LOG_BASE_DIR, 'app.log'),
        },
        'audit_log_file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'formatter': 'default',
            'filters': ['api_format_filter', ],
            'filename': os.path.join(LOG_BASE_DIR, 'audit.log'),
        },
        'uploads_log_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'default',
            'filters': ['api_format_filter', ],
            'filename': os.path.join(LOG_BASE_DIR, 'uploads.log'),
        },
        'remote_checkout': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'formatter': 'default',
            'filters': ['api_format_filter', ],
            'filename': os.path.join(LOG_BASE_DIR, 'remote_checkout_audit.log'),
        },
        'graylog': {
            'level': 'DEBUG' if DEBUG else 'INFO',
            'class': GRAYLOG_HANDLER,
            'host': GRAYLOG_HOST,
            'port': GRAYLOG_PORT,
            'filters': ['api_format_filter', 'request_data_filter'],
            'formatter': 'json'
        },
        'elasticapm': {
            'level': 'WARNING',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'scoopcor.uploads': {
            'handlers': ['uploads_log_file', ] + ['graylog'] if USE_GRAYLOG else [],
            'level': 'DEBUG',
        },
        'scoopcor': {
            'handlers': ['app_log_file', 'console', ] + ['graylog'] if USE_GRAYLOG else [],
            'propagate': False,
            'level': 'DEBUG',
        },
        'scoopcor.remote_checkout': {
            'handlers': ['remote_checkout', ] + ['graylog'] if USE_GRAYLOG else [],
            'propagate': False,
            'level': 'INFO',
        },
        'scoopcor.audit': {
            'handlers': [],
            'level': 'INFO',
        },
        'eperpus': {
            'handlers': ['graylog'] if USE_GRAYLOG else [],
            'propagate': False,
            'level': 'INFO',
        },
        'elasticapm.errors': {
            'level': 'ERROR',
            'handlers': ['elasticapm'],
            'propagate': False,
        },
    }
}

KREDIVO_SERVER_KEY = 'WqzHqeCna9zxYTg3CdXRfBNpQ75Qej'
# # Allow overrides from local_config
# try:
#     from local_config import *
# except ImportError:
#     pass


FONTS_BASE_DIR = os.path.join(_basedir, 'app', 'static', 'fonts')
