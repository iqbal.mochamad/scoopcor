#!/usr/bin/env python
# coding: utf-8

from flask.ext.runner import Manager
from app import app, db

manager = Manager(app)


@manager.command
def syncdb():
    '''
        Create a new db. python manage.py createdb
    '''
    db.create_all()

@manager.command
def createdb():
    '''
    Create a new db.
    `python manage.py createdb`
    '''
    print('Creating tables... Stand by...')
    db.create_all()


@manager.command
def help():
    print ("===============================")
    print ("Command Line List by kiratakada")
    print ("===============================")

    print ("$ syncdb : Untuk nge-sync database baru, tanpa mendestroy database yg lama")
    print ("$ createdb : Untuk nge-sync database baru. ( New database) ")

    print ("=============================== arigato")


if __name__ == '__main__':
    manager.run()
