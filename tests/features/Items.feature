Feature: Items

Scenario: Request Item List Options
Given I am a Superuser
When I request Item List OPTIONS
Then I am allowed to LIST, CREATE, UPDATE, DELETE

Given I am a Content Operator
When I request Item OPTIONS
Then I am allowed to LIST, UPDATE, DELETE

Given I am a Publisher User
When I request Item List OPTIONS
Then I am allowed to LIST

Given I am a Public User
When I request Brand List OPTIONS
Then I am allowed to LIST


Scenario: Request Item Options
Given I am a Superuser
When I request Item OPTIONS
Then I am allowed to GET, CREATE, UPDATE, DELETE

Given I am a Content Operator
When I request Item OPTIONS
Then I am allowed to GET, UPDATE, DELETE

Given I am a Public User
When I request Item OPTIONS
Then I am only allowed to GET

Given I am a Publisher User
When I request Item OPTIONS
And the Item is owned by my Organization
Then I am allowed to GET, UPDATE, DELETE

Given I am a Publisher User
When I request Item OPTIONS
And the Item is NOT owned by my Organization
Then I am only allowed to GET
