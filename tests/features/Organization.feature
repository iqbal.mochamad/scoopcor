Feature: Organization (CAS)

Scenario: Request Organization List Options
Given I am a Superuser
When I request Organization List OPTIONS
Then I am allowed to LIST, CREATE, UPDATE, DELETE

Given I am a Public User
When I request Organization List OPTIONS
Then I am NOT allowed to LIST, CREATE, UPDATE, DELETE

Given I am a Publisher User
When I request Organization List OPTIONS
Then I am NOT allowed to LIST, CREATE, UPDATE, DELETE

Scenario: Request Organization Options
Given I am a Superuser
When I request Organization OPTIONS
Then I am allowed to GET, CREATE, UPDATE, DELETE

Given I am a Public User
When I request Organization OPTIONS
Then I am NOT allowed to GET, CREATE, UPDATE, DELETE

Given I am a Publisher User
When I request Organization OPTIONS
Then I am NOT allowed to GET, CREATE, UPDATE, DELETE
