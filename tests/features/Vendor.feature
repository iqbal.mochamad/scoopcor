Feature: Vendor (COR)

Scenario: Request Vendor List Options
Given I am a Superuser
When I request Vendor List OPTIONS
Then I am allowed to LIST, CREATE, UPDATE, DELETE

Given I am a Content Operator
When I request Vendor List OPTIONS
Then I am allowed to LIST, CREATE, UPDATE, DELETE

Given I am a Public User
When I request Vendor List OPTIONS
Then I am NOT allowed to LIST, CREATE, UPDATE, DELETE

Given I am a Publisher User
When I request Vendor List OPTIONS
Then I am NOT allowed to LIST, CREATE, UPDATE, DELETE

Scenario: Request Vendor Item Options
Given I am a Superuser
When I request Vendor Item OPTIONS
Then I am allowed to GET, CREATE, UPDATE, DELETE

Given I am a Content Operator
When I request Vendor Item OPTIONS
Then I am allowed to GET, CREATE, UPDATE, DELETE

Given I am a Public User
When I request Vendor Item OPTIONS
Then I am NOT allowed to GET, CREATE, UPDATE, DELETE

Given I am a Publisher User
When I request Vendor Item OPTIONS and it is NOT my own Vendor Item
Then I am NOT allowed to GET, CREATE, UPDATE, DELETE

Given I am a Publisher User
When I request Vendor Item OPTIONS and it is my own Vendor Item
Then I am allowed to GET, UPDATE and NOT allowed to CREATE, DELETE
