Feature: Brands Auth

Scenario: Request Brand List
Given I am a SuperUser
When I request Brand List OPTIONS
Then I am allowed to LIST, CREATE

Given I am a Publisher User
When I request Brand List OPTIONS
Then I am allowed to LIST

Given I am a Regular User
When I request Brand List OPTIONS
Then I am allowed to LIST

Scenario: Request Brand Item
Given I am a SuperUser
When I request Brand Item OPTIONS
Then I am allowed to GET, UPDATE, DELETE

Given I am a Publisher User
When I request Brand Item OPTIONS
And the Item is Owned by my Organization
Then I am allowed to GET, UPDATE, DELETE

Given I am a Publisher User
When I request Brand Item OPTIONS
And the Item is not Owned by my Organization
Then I am allowed to GET and not allowed to UPDATE, DELETE

Given I am a Regular User
When I request Brand Item OPTIONS
Then I am allowed to GET and not allowed to UPDATE, DELETE
