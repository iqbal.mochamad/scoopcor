Feature: Shared-Library (for Pustaka Digital)

Scenario: Request ITEM List
Given I am a Shared-Library Administrator
When I request my own ITEM List OPTIONS
Then I am allowed to LIST, CREATE, UPDATE

Given I am a Shared-Library User
When I request ITEM List OPTIONS
Then I am NOT allowed to LIST, CREATE, UPDATE

Given I am a SuperUser
When I request ITEM List OPTIONS
Then I am allowed to LIST, CREATE, UPDATE

Scenario: Request CATALOG List
Given I am a Shared-Library Administrator
When I request CATALOG List OPTIONS
Then I am allowed to LIST, CREATE, UPDATE

Given I am a Shared-Library User
When I request my own CATALOG List OPTIONS
Then I am allowed to LIST

Given I am a Shared-Library User
When I request CATALOG List OPTIONS that is not my own
Then I am NOT allowed to LIST, CREATE, UPDATE

Given I am a SuperUser
When I request CATALOG List OPTIONS
Then I am allowed to LIST, CREATE, UPDATE

Scenario: Request BORROWED ITEM List
Given I am a Shared-Library User
When I Request BORROWED ITEM List Options and I am NOT Logged On other device
Then I am not allowed to LIST, UPDATE, GET


Scenario: Shared-Library SECURITY
Given I am a Shared-Library User
When I Request BORROWED ITEM List Options and I have Logged On other device
Then I am not allowed to LIST, UPDATE, GET
