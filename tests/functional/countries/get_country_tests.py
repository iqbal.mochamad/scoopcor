import unittest

from nose.tools import nottest

from app import app
from faker import Factory
import random

from app.depreciated.api import CountryDetailApi

import json

fake = Factory.create()


# deprecated
@nottest
class GetCountryTests(unittest.TestCase):

    def test_fetch_random_country(self):
        client = app.test_client()

        random_country = random.choice(CountryDetailApi().data)

        response = client.get('/v1/countries/{}'.format(random_country['id']),
                               headers={'accept': 'application/json'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'application/json')

        data = json.loads(response.data)

        self.assertEqual(data["code"], random_country["code"])
        self.assertEqual(data["description"], random_country["description"])
        self.assertEqual(data["icon_image_highres"], random_country["icon_image_highres"])
        self.assertEqual(data["icon_image_normal"], random_country["icon_image_normal"])
        self.assertEqual(data["id"], random_country["id"])
        self.assertEqual(data["meta"], random_country["meta"])
        self.assertEqual(data["name"], random_country["name"])
        self.assertEqual(data["slug"], random_country["slug"])
        self.assertEqual(data["sort_priority"], random_country["sort_priority"])

        self.assertEqual(data["media_base_url"], "http://images.getscoop.com/magazine_static/")
