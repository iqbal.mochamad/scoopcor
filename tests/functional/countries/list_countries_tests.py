import unittest

from nose.tools import nottest

from app import app
import random
from app.depreciated.api import CountryDetailApi
import json


# deprecated
@nottest
class ListCountriesTests(unittest.TestCase):

    def test_fetch_random_country(self):
        client = app.test_client()

        response = client.get('/v1/countries', headers={'accept': 'application/json'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content_type, 'application/json')

        list_data = json.loads(response.data)

        for data in list_data['countries']:
            country = filter(lambda e: e['id'] == data['id'], CountryDetailApi().data)
            country = country[0]
            self.assertEqual(data["code"], country["code"])
            self.assertEqual(data["description"], country["description"])
            self.assertEqual(data["icon_image_highres"], country["icon_image_highres"])
            self.assertEqual(data["icon_image_normal"], country["icon_image_normal"])
            self.assertEqual(data["id"], country["id"])
            self.assertEqual(data["meta"], country["meta"])
            self.assertEqual(data["name"], country["name"])
            self.assertEqual(data["slug"], country["slug"])
            self.assertEqual(data["sort_priority"], country["sort_priority"])
            self.assertEqual(data["media_base_url"], "http://images.getscoop.com/magazine_static/")
