import unittest
import random

import httplib

from nose.tools import nottest

from app import app
from app.depreciated.api import CountryDetailApi


# deprecated
@nottest
class DeleteCountryTests(unittest.TestCase):

    def test_returns_501(self):
        client = app.test_client()
        random_country = random.choice(CountryDetailApi().data)
        response = client.delete('/v1/countries/{}'.format(random_country['id']))
        self.assertEqual(response.status_code, httplib.NOT_IMPLEMENTED)
