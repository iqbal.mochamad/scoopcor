import unittest

from nose.tools import nottest

from app import app
import datetime
from faker import Factory
import random
from app.depreciated.api import CountryDetailApi

fake = Factory.create()


# deprecated
@nottest
class CreateCountryTests(unittest.TestCase):

    def test_returns_501(self):
        client = app.test_client()

        random_country = random.choice(CountryDetailApi().data)

        response = client.put('/v1/countries/{}'.format(random_country['id']),
                               data={
                                  "name": fake.country(),
                                  "description": fake.bs(),
                                  "code": fake.country_code(),
                                  "slug": fake.country().lower(),
                                  "meta": fake.bs(),
                                  "icon_image_normal": fake.uri(),
                                  "icon_image_highres": fake.uri(),
                                  "sort_priority": fake.pyint(),
                                  "is_active": random.choice([True, False])
                                 },
                               headers={'content-type': 'application/json',
                                        'accept': 'application/json'})
        self.assertEqual(response.status_code, 501)
