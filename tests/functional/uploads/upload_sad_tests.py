from __future__ import unicode_literals

from httplib import CONFLICT
from faker import Factory
from nose.tools import istest

from app import db
from app.items.choices import ItemUploadProcessStatus
from tests.fixtures.sqlalchemy.items import ItemUploadProcessFactory
from tests import testcases


@istest
class CreateUploadConflictTests(testcases.CreateTestCase):
    """ If there is currently one file processing for an item, we don't want
    to allow a second file to be uploaded.  If we do, there would essentially
    be a race to see which one would be used.
    """
    request_url = '/v1/items/{0.item_id}/upload'
    fixture_factory = ItemUploadProcessFactory

    @classmethod
    def set_up_fixtures(cls):
        return ItemUploadProcessFactory(
            status=ItemUploadProcessStatus.new.value)

    @classmethod
    def get_api_request_body(cls):
        return {
            'file_name': _fake.file_name(extension='pdf'),
            'file_dir': '/srv/scoopcor/file/path1',
            'role_id': _fake.pyint(),
            'pic_id': _fake.pyint(),
            'file_size': _fake.pyint(),
            'pic_name': _fake.email(),
        }

    def test_response_body(self):
        self.assertScoopErrorResponse(self.response)

    def test_response_code(self):
        self.assertEqual(self.response.status_code, CONFLICT,
                         msg="If there is already a EPUB/PDF file currently "
                             "processing for an Item, any subsequent request "
                             "to upload new files should return HTTP 409. "
                             "Got {}".format(self.response.status_code))


_fake = Factory.create()


def setup_module():
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
