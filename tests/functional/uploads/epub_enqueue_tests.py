from __future__ import unicode_literals

import json
from httplib import ACCEPTED
import os

from faker import Factory
import jsonschema
from nose.tools import istest

from app import db, app, set_request_id
from app.auth.models import UserInfo
from app.items.choices import ItemUploadProcessStatus
from app.items.models import ItemUploadProcess
from app.users.users import User
from tests.fixtures.sqlalchemy.items import (
    ItemFactory, ItemUploadProcessFactory)
from tests import testcases


@istest
class CreateEpubUploadTests(testcases.CreateTestCase):
    """ Tests that when a EPUB is uploaded, it is properly recorded in the
    processing queue.
    """
    request_url = '/v1/items/{0.id}/upload'
    fixture_factory = ItemUploadProcessFactory
    maxDiff = None

    @classmethod
    def set_up_fixtures(cls):
        return ItemFactory(content_type='epub')

    @classmethod
    def get_api_request_body(cls):
        return {
            'file_name': _fake.file_name(extension='epub'),
            'file_dir': '/srv/scoopcor/file/path1',
            'file_size': _fake.pyint(),
            'pic_id': _fake.pyint(),
            'pic_name': _fake.email(),
            'role_id': _fake.pyint(),
        }

    def test_response_body(self):
        session = db.session
        item_upload_file = session.query(ItemUploadProcess)\
            .filter_by(item_id=self.fixture.id)
        if item_upload_file.count() != 1:
            self.fail("This test expects only a single ItemUploadProcess "
                      "to exist for it's Item fixture.")
        item_upload_file = item_upload_file.first()

        self.assertDictEqual(
            {
                'file_name': item_upload_file.file_name,
                'file_size': item_upload_file.file_size,
                'pic_id': item_upload_file.pic_id,
                'pic_name': item_upload_file.pic_name,
                'role_id': item_upload_file.role_id,
                'processing_start_time': None,
                'processing_complete_time': None,
                'status': ItemUploadProcessStatus.new.value,
                'error': None
             },
            json.loads(self.response.data))

    def test_request_schema(self):
        req_schema_path = os.path.join(
            _base_dir,
            'app',
            'items',
            'schemas',
            'item-upload.request.json',
        )
        jsonschema.validate(
            self.request_body,
            json.load(open(req_schema_path)))

    def test_response_schema(self):
        resp_schema_path = os.path.join(
            _base_dir,
            'app',
            'items',
            'schemas',
            'item-upload.response.json',
        )
        jsonschema.validate(
            json.loads(self.response.data),
            json.load(open(resp_schema_path)))

    def test_response_code(self):
        self.assertEqual(self.response.status_code, ACCEPTED,
                         msg="Expecting HTTP 202 ACCEPTED on successful POST. "
                             "Got {}".format(self.response.status_code))

    def test_database_model(self):

        item_fixture = self.fixture
        session = db.session()
        entity = session.query(ItemUploadProcess).filter_by(
            item_id=item_fixture.id).first()

        self.assertIsNone(entity.start_process_time)
        self.assertIsNone(entity.end_process_time)
        self.assertIsNone(entity.error)

        self.assertEqual(self.request_body['file_name'], entity.file_name)
        self.assertEqual(entity.status, ItemUploadProcessStatus.new)


_fake = Factory.create()

_base_dir = os.path.join(
    os.path.dirname(__file__),
    os.pardir,
    os.pardir,
    os.pardir
)

# original_before_req_funcs = app.before_request_funcs


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()
    # app.before_request_funcs = {
    #     None: [
    #         set_request_id,
    #         init_g_current_user,
    #     ]
    # }


def teardown_module():
    db.session().close_all()
    db.drop_all()
    # app.before_request_funcs = original_before_req_funcs

#
# def init_g_current_user():
#     g.user = UserInfo(username='dfdsafsdf', user_id=12345, token='abcd', perm=['can_read_write_global_all',])
#     g.current_user = None
#     # session = db.session()
#     # g.current_user = session.query(User).get(12345)
#     # session.close()
