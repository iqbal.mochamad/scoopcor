from __future__ import unicode_literals

import json
from httplib import ACCEPTED
import os

from faker import Factory
from nose.tools import istest
import jsonschema

from app import db
from app.items.choices import ItemUploadProcessStatus
from app.items.models import ItemUploadProcess
from tests.fixtures.sqlalchemy.items import (
    ItemFactory, ItemUploadProcessFactory)
from tests import testcases


@istest
class CreatePdfUploadTests(testcases.CreateTestCase):
    """ Tests that when a PDF is uploaded, it is properly recorded in the
    processing queue.
    """
    request_url = '/v1/items/{0.id}/upload'
    fixture_factory = ItemUploadProcessFactory
    maxDiff = None

    @classmethod
    def set_up_fixtures(cls):
        return ItemFactory(content_type='pdf')

    @classmethod
    def get_api_request_body(cls):
        return {
            'file_name': _fake.file_name(extension='pdf'),
            'file_dir': '/srv/scoopcor/file/path1',
            'role_id': _fake.pyint(),
            'pic_id': _fake.pyint(),
            'file_size': _fake.pyint(),
            'pic_name': _fake.email(),
        }

    def test_response_body(self):
        session = db.session
        item_upload_file = session.query(ItemUploadProcess)\
            .filter_by(item_id=self.fixture.id)
        if item_upload_file.count() != 1:
            self.fail("This test expects only a single ItemUploadProcess "
                      "to exist for it's Item fixture.")
        item_upload_file = item_upload_file.first()

        self.assertDictEqual(
            {
                'file_name': item_upload_file.file_name,
                'file_size': item_upload_file.file_size,
                'pic_id': item_upload_file.pic_id,
                'pic_name': item_upload_file.pic_name,
                'role_id': item_upload_file.role_id,
                'processing_start_time': None,
                'processing_complete_time': None,
                'status': ItemUploadProcessStatus.new.value,
                'error': None
             },
            json.loads(self.response.data))

    def test_request_schema(self):
        req_schema_path = os.path.join(
            _base_dir,
            'app',
            'items',
            'schemas',
            'item-upload.request.json',
        )
        jsonschema.validate(
            self.request_body,
            json.load(open(req_schema_path)))

    def test_response_schema(self):
        resp_schema_path = os.path.join(
            _base_dir,
            'app',
            'items',
            'schemas',
            'item-upload.response.json',
        )
        jsonschema.validate(
            json.loads(self.response.data),
            json.load(open(resp_schema_path)))

    def test_response_code(self):
        self.assertEqual(self.response.status_code, ACCEPTED)

    def test_item_processing_file_created(self):

        item_fixture = self.fixture
        session = db.session()
        entity = session.query(ItemUploadProcess).filter_by(
            item_id=item_fixture.id).first()

        self.assertIsNone(entity.start_process_time)
        self.assertIsNone(entity.end_process_time)
        self.assertIsNone(entity.error)

        self.assertEqual(self.request_body['file_name'], entity.file_name)
        self.assertEqual(entity.status, ItemUploadProcessStatus.new)


_fake = Factory.create()

_base_dir = os.path.join(
    os.path.dirname(__file__),
    os.pardir,
    os.pardir,
    os.pardir
)


def setup_module():
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
