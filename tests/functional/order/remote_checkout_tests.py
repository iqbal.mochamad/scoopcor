from __future__ import unicode_literals
import json
from httplib import CREATED, CONFLICT
from unittest import TestCase, SkipTest

from datetime import timedelta

from flask import g
from nose.tools import istest, nottest

from app import app, set_request_id, db
from app.auth.models import UserInfo
from app.discounts.choices.predefined_type import HARPER_COLLINS
from app.items.choices import STATUS_TYPES, STATUS_READY_FOR_CONSUME
from app.offers.choices.offer_type import SINGLE, SUBSCRIPTIONS
from app.offers.models import OfferType
from app.orders.choices import CANCELLED, COMPLETE
from app.orders.models import Order, OrderLine, OrderLineDiscount
from app.remote_publishing.choices import ELEVENIA, GRAMEDIA
from app.services.elevenia.helpers import OFFER_READY_FOR_SALE
from app.services.gramedia.constants import BOOK_GENERAL_DISCOUNT_ID
from app.users.users import User
from app.utils.datetimes import get_local
from app.users.tests.fixtures import UserFactory
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy.discounts import DiscountFactory
from tests.fixtures.sqlalchemy.items import ItemFactory
from tests.fixtures.sqlalchemy.master import VendorFactory, BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory


@istest
class RemoteCheckoutApiEleveniaTests(TestCase):

    db = db
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        cls.original_before_request_funcs = app.before_request_funcs

    @classmethod
    def tearDownClass(cls):
        app.before_request_funcs = cls.original_before_request_funcs

    def test_post_elevenia(self):
        # set user context to user elevenia
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user_elevenia,
            ]
        }
        json_body = {
            "sub_total": 30000,
            "grand_total": 30000,
            "sender_email": "test1@test.com",
            "receiver_email": "test1@test.com",
            "remote_service_name": ELEVENIA,
            "currency_code": "idr",
            "items": [{"id": "SC00P121",
                       "sub_total": 10000,
                       "grand_total": 10000
                       },
                      {"id": "SC00P122",
                       "sub_total": 20000,
                       "grand_total": 20000
                       },
                      ],
            "user_message": ""
        }
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/remote-checkout/elevenia',
                               data=json.dumps(json_body),
                               headers={
                                    'Accept': 'application/json',
                                    'Accept-Charset': 'utf-8',
                                    'Content-Type': 'application/json',
                                })
        if response.status_code == CREATED:
            actual = json.loads(response.data)
            self.assertEqual(actual.get("final_amount"), 30000)
            self.assertEqual(len(actual.get("order_lines")), 2)
            self.assertEqual(actual.get("order_status"), COMPLETE)
            self.assertEqual(actual.get("user_id", None), 1000)
        self.assertEqual(response.status_code, CREATED)


class RemoteCheckoutApiGramediaTests(TestCase):
    db = db
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        # set user context to user gramedia
        cls.original_before_request_funcs = app.before_request_funcs
        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user_gramedia,
            ]
        }

    @classmethod
    def tearDownClass(cls):
        app.before_request_funcs = cls.original_before_request_funcs

    def test_post_gramedia(self):
        json_body = {
            "sub_total": 24000,
            "grand_total": 24000,
            "sender_email": "test2@test.com",
            "receiver_email": "test2@test.com",
            "user_id": 2000,
            "remote_service_name": GRAMEDIA,
            "currency_code": "idr",
            "items": [{"id": "SC00P121",
                       "discount_id": None,
                       "sub_total": 8000,
                       "grand_total": 8000
                       },
                      {"id": "SC00P122",
                       "discount_id": BOOK_GENERAL_DISCOUNT_ID,
                       "sub_total": 16000,
                       "grand_total": 16000
                       },
                      ],
            "user_message": "",
            "remote_order_number": "order12345abc7"
        }
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/remote-checkout/gramedia',
                               data=json.dumps(json_body),
                               headers={
                                   'Accept': 'application/json',
                                   'Accept-Charset': 'utf-8',
                                   'Content-Type': 'application/json',
                               })
        if response.status_code == CREATED:
            actual = json.loads(response.data)
            self.assertEqual(actual.get("final_amount"), 24000)
            self.assertEqual(len(actual.get("order_lines")), 2)
            session = db.session()
            saved_order = session.query(Order).get(actual.get('order_id', None))
            self.assertIsNotNone(saved_order)
            self.assertEqual(saved_order.remote_order_number, "order12345abc7")
            self.assertEqual(saved_order.user_id, 2000)
            self.assertEqual(actual.get("order_status"), COMPLETE)
            order_line = session.query(OrderLine).filter(OrderLine.order_id == saved_order.id)
            self.assertIsNotNone(order_line)
            self.assertEqual([121, 122, ], [line.offer_id for line in order_line])
            expected_total_base_price = 0
            for line in order_line:
                self.assertEqual(line.is_discount, True)
                self.assertEqual(line.localized_currency_code, 'IDR')
                expected_total_base_price += line.offer.price_idr
                self.assertEqual(line.price, line.offer.price_idr)
                if line.offer_id == 121:
                    self.assertEqual(line.final_price, 8000)
                    self.assertEqual(line.localized_final_price, 8000)
                elif line.offer_id == 122:
                    self.assertEqual(line.final_price, 16000)
                    self.assertEqual(line.localized_final_price, 16000)

            self.assertEqual(saved_order.total_amount, expected_total_base_price)

            order_line_discount = session.query(OrderLineDiscount).filter(
                OrderLineDiscount.order_id == saved_order.id)
            self.assertIsNotNone(order_line_discount)
            for line_disc in order_line_discount:
                # both of line discount must be BOOK_GENERAL_DISCOUNT_ID (cause BOOK_GENERAL_DISCOUNT_ID id default discount for books)
                self.assertEqual(BOOK_GENERAL_DISCOUNT_ID, line_disc.discount_id)
        self.assertEqual(response.status_code, CREATED)

        # post the same remote order twice will raise error http CONFLICT (already exists)
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/remote-checkout/gramedia',
                               data=json.dumps(json_body),
                               headers={
                                   'Accept': 'application/json',
                                   'Accept-Charset': 'utf-8',
                                   'Content-Type': 'application/json',
                               })
        self.assertEqual(response.status_code, CONFLICT)

    def test_post_gramedia_with_invalid_discount_id(self):
        json_body = {
            "sub_total": 40000,
            "grand_total": 40000,
            "sender_email": "test2@test.com",
            "receiver_email": "test2@test.com",
            "user_id": 2000,
            "remote_service_name": GRAMEDIA,
            "currency_code": "idr",
            "items": [{"id": "SC00P122",
                       "discount_id": BOOK_GENERAL_DISCOUNT_ID,
                       "sub_total": 16000,
                       "grand_total": 16000
                       },
                      {"id": "SC00P123",
                       "discount_id": BOOK_GENERAL_DISCOUNT_ID,
                       "sub_total": 24000,
                       "grand_total": 24000
                       },
                      ],
            "user_message": "",
            "remote_order_number": "order12345INVALID"
        }
        # items SC00P123 == Offer id 123 --> this offer is invalid for discount id BOOK_GENERAL_DISCOUNT_ID
        # discount id BOOK_GENERAL_DISCOUNT_ID didn't have vendor for this offer
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/remote-checkout/gramedia',
                               data=json.dumps(json_body),
                               headers={
                                   'Accept': 'application/json',
                                   'Accept-Charset': 'utf-8',
                                   'Content-Type': 'application/json',
                               })
        if response.status_code == CREATED:
            actual = json.loads(response.data)
            self.assertEqual(len(actual.get("order_lines")), 2)
            self.assertEqual(actual.get("order_status"), CANCELLED)
            session = db.session()
            saved_order = session.query(Order).get(actual.get('order_id', None))
            self.assertIsNotNone(saved_order)
            self.assertEqual(saved_order.remote_order_number, "order12345INVALID")
            self.assertEqual(saved_order.order_status, CANCELLED)
        self.assertEqual(response.status_code, CREATED)


USER_ID_GRAMEDIA_DEV = 523862
USER_ID_ELEVENIA_DEV = 436786


def init_g_current_user_gramedia():
    g.user = UserInfo(username='dfdsafsdf', user_id=USER_ID_GRAMEDIA_DEV, token='abcd', perm=['can_read_write_global_all', ])
    session = db.session()
    g.current_user = session.query(User).get(USER_ID_GRAMEDIA_DEV)
    session.close()


def init_g_current_user_elevenia():
    g.user = UserInfo(username='dfdsafsdf', user_id=USER_ID_ELEVENIA_DEV,
                      token='abcd', perm=['can_read_write_global_all', ])

    g.user = UserInfo(username='dfdsafsdf', user_id=USER_ID_ELEVENIA_DEV, token='abcd',
                      perm=['can_read_write_global_all', ])
    session = db.session()
    g.current_user = session.query(User).get(USER_ID_ELEVENIA_DEV)
    session.close()


def setup_module():
    # make sure the test mode flag is set, or bail out
    if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
        raise SkipTest("COR is not in testing mode.  Cannot continue.")

    db.session().close_all()
    db.drop_all()
    db.create_all()

    create_init_data()


def teardown_module():
    db.session().close_all()
    db.drop_all()


def create_init_data():
    session = db.session()
    generate_static_sql_fixtures(session)

    # offer_type_single = session.query(OfferType).filter(OfferType.id == 1).first()
    # offer_type_subscription = session.query(OfferType).filter(OfferType.id == 2).first()
    offer_type_single = session.query(OfferType).get(SINGLE)
    offer_type_subscription = session.query(OfferType).get(SUBSCRIPTIONS)

    vendor_harper = VendorFactory(id=HARPER_COLLINS)
    vendor_books = VendorFactory(id=1000)
    vendor_without_disc = VendorFactory(id=1005)
    brand_harper = BrandFactory(id=1000, vendor=vendor_harper)
    brand_normal = BrandFactory(id=1002, vendor=vendor_books)
    brand_without_disc = BrandFactory(id=1005, vendor=vendor_without_disc)

    item1 = ItemFactory(
        id=1,
        name='Book test 1',
        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
        item_type='book',
        is_active=True,
        content_type='pdf',
        parentalcontrol_id=1,
        brand=brand_normal,
    )

    offer1 = OfferFactory(
        id=121,
        name='test offer book 1',
        offer_type=offer_type_single,
        offer_status=OFFER_READY_FOR_SALE,
        price_idr=10000
    )
    offer1.items.append(item1)

    item2 = ItemFactory(
        id=2,
        name='Magazine test 2',
        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
        item_type='magazine',
        is_active=True,
        content_type='pdf',
        parentalcontrol_id=1,
        brand=brand_normal,
    )

    offer2 = OfferFactory(
        id=122,
        name='test offer magazine 2',
        offer_type=offer_type_single,
        offer_status=OFFER_READY_FOR_SALE,
        price_idr=20000
    )
    offer2.items.append(item2)

    item3 = ItemFactory(
        id=3,
        name='item newspaper 3',
        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
        item_type='newspaper',
        is_active=True,
        content_type='pdf',
        parentalcontrol_id=1,
        brand=brand_without_disc,
    )

    offer3 = OfferFactory(
        id=123,
        name='test offer newspaper 3',
        offer_type=offer_type_single,
        offer_status=OFFER_READY_FOR_SALE,
        is_free=False,
        price_idr=30000
    )
    # offer 3
    offer3.items.append(item3)

    item4 = ItemFactory(
        id=4,
        name='item book 4',
        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
        item_type='book',
        is_active=True,
        content_type='pdf',
        parentalcontrol_id=1,
        brand=brand_harper
    )
    offer_harper = OfferFactory(
        id=124,
        name='test offer book 4',
        offer_type=offer_type_single,
        offer_status=OFFER_READY_FOR_SALE,
        is_free=False,
        price_idr=40000
    )

    # offer 4 - offer_harper
    offer_harper.items.append(item4)

    item_god = ItemFactory(
        id=91097,
        name='God in the Dock',
        item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
        item_type=u'book',
        is_active=True,
        content_type=u'pdf',
        image_normal='images/1/28711/general_big_covers/ID_HCO2015MTH11GITD.jpeg',
        countries=['ID', ],
        parentalcontrol_id=1,
        brand=brand_normal,
    )
    offer_god = OfferFactory(
        id=50385,
        name='test offer 1',
        long_name='God in the Dock long name',
        offer_type=offer_type_single,
        offer_status=OFFER_READY_FOR_SALE,
        is_free=False,
        is_active=True,
        price_idr=10000
    )
    offer_god.items.append(item_god)

    discount_harper_collins = DiscountFactory(
        id=741, name='general discount harper collins',
        valid_to=get_local() + timedelta(days=7), valid_from=get_local() - timedelta(days=7),
        discount_rule=2, discount_type=1, discount_status=2, discount_schedule_type=1,
        is_active=True, discount_usd=5, discount_idr=5, discount_point=5,
        predefined_group=2)
    discount_harper_collins.vendors.append(vendor_harper)
    discount_books = DiscountFactory(
        id=BOOK_GENERAL_DISCOUNT_ID, name='general discount books',
        valid_to=get_local() + timedelta(days=7), valid_from=get_local() - timedelta(days=7),
        discount_rule=2, discount_type=1, discount_status=2, discount_schedule_type=1,
        is_active=True, discount_usd=20, discount_idr=20, discount_point=20,
        predefined_group=5)
    discount_books.vendors.append(vendor_books)

    user1 = UserFactory(id=1000,email='test1@test.com')
    user2 = UserFactory(id=2000, email='test2@test.com')
    user_elevenia = UserFactory(id=USER_ID_ELEVENIA_DEV, email='testelevenia@test.com')
    user_gramedia = UserFactory(id=USER_ID_GRAMEDIA_DEV, email='testgramedia@test.com')
    session.commit()
