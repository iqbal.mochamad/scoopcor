import json
from unittest import SkipTest
from unittest import TestCase

from mock import patch

from app import app, db
from app.offers.models import OfferPlatform
from app.orders.order_checkout_platform import OrderCheckoutConstructPlatform
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferPlatformFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory

USER_ID = 12345


class OrderCheckoutTests(TestCase):

    def setUp(self):
        s = db.session()
        generate_static_sql_fixtures(s)
        args = {
            "user_id": USER_ID,
            "order_id": 1122,
        }
        self.order_checkout = OrderCheckoutConstructPlatform(
            args=args,
            method='POST'
        )
        self.order_checkout.master_order = OrderFactory(id=1122)
        s.commit()

    @patch('app.offers.models.OfferPlatform.get_offer')
    @patch('app.helpers.log_api')
    @patch('app.orders.models.OrderLineTemp.save')
    def test_insert_order_line_quantity_0(self, mocked_order_line_save, mocked_log_api, mocked_get_offer):
        s = db.session()
        offer_platform = OfferPlatformFactory()

        name = 'Nama dummy product offer'

        mocked_get_offer.return_value = offer_platform.offer
        mocked_order_line_save.return_value = {'invalid': False, 'message': "OrderLineTemp success add"}

        with app.test_request_context('/'):
            quantity = 0
            actual = self.order_checkout.insert_order_line(offer_platform, quantity, name)
            self.assertEqual(actual[0], 'FAILED')
            self.assertEqual('Quantity order <= 0 not allowed', json.loads(actual[1].data).get('developer_message'))

            # for quantity = None, it should convert to 1 (for old device/mobile device that didn't send quantity)
            quantity = None
            actual = self.order_checkout.insert_order_line(offer_platform, quantity, name)
            self.assertEqual(actual[0], 'OK', "{} != 'OK', {}".format(actual[0], actual[1]))
            self.assertEqual(actual[1].quantity, 1)

            # for quantity > 0
            quantity = 2
            actual = self.order_checkout.insert_order_line(offer_platform, quantity, name)
            self.assertEqual(actual[0], 'OK', "{} != 'OK', {}".format(actual[0], actual[1]))
            self.assertEqual(actual[1].quantity, 2)


def setup_module():
    # make sure the test mode flag is set, or bail out
    if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
        raise SkipTest("COR is not in testing mode.  Cannot continue.")

    db.session().close_all()
    db.drop_all()
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
