from __future__ import unicode_literals, absolute_import
from unittest import TestCase, SkipTest

from datetime import timedelta
from flask import current_app, g
from nose.tools import istest, nottest
from werkzeug.exceptions import BadRequest, Forbidden, Conflict, UnprocessableEntity

from app import app, db
from app.auth.models import UserInfo
from app.constants import RoleType
from app.items.choices import BOOK
from app.items.choices import ITEM_TYPES
from app.master.models import Currencies
from app.offers.choices.offer_type import BUFFET, SINGLE
from app.offers.models import OfferType, Offer
from app.orders.choices import COMPLETE
from app.orders.helpers import assert_user_doesnt_already_own, get_client_id_for_remote_service, \
    get_paymentgateway_for_remote_service, assert_remote_ordering_allowed, \
    get_discount_for_remote_offer, assert_discount_from_remote_checkout, \
    construct_order_from_remote_request, assert_remote_order_not_exists, DiscountNotValidForOfferInRemoteCheckout, \
    user_allowed_to_get_point
from app.payment_gateways.models import PaymentGateway
from app.remote_publishing.choices import ELEVENIA, GRAMEDIA
from app.payments.choices.gateways import ELEVENIA as ELEVENIA_PAYMENT_GATEWAY, \
    GRAMEDIA as GRAMEDIA_PAYMENT_GATEWAY, FREE, POINT, OLD_VERITRANS
from app.services.gramedia.constants import BOOK_GENERAL_DISCOUNT_ID
from app.users.users import User, Role
from app.utils.datetimes import get_local
from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.fixtures.sqlalchemy.discounts import DiscountFactory
from tests.fixtures.sqlalchemy.items import ItemFactory, UserItemFactory
from tests.fixtures.sqlalchemy.master import VendorFactory, BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferBuffetFactory
from tests.fixtures.sqlalchemy.orders import OrderLineFactory, OrderFactory
from app.users.tests.fixtures import OrganizationFactory, UserFactory
from app.users.tests.fixtures_user_buffet import UserBuffetFactory


@istest
class OrderHelperTests(TestCase):

    db = db
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        # make sure the test mode flag is set, or bail out
        if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
            raise SkipTest("COR is not in testing mode.  Cannot continue.")

        db.session().close_all()
        db.drop_all()
        db.create_all()
        cls.init_data()

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.drop_all()

    def test_assert_remote_ordering_allowed(self):
        with app.app_context():
            user_elevenia = 436786 if current_app.debug else 436785
            actual_assert_user_elevenia = assert_remote_ordering_allowed(
                remote_service=ELEVENIA, user_id=user_elevenia
            )
            user_gramedia = 523862 if current_app.debug else 523861
            actual_assert_user_gramedia = assert_remote_ordering_allowed(
                remote_service=GRAMEDIA, user_id=user_gramedia
            )
            self.assertIsNone(actual_assert_user_elevenia)
            self.assertIsNone(actual_assert_user_gramedia)

    def test_assert_remote_ordering_allowed_error_badrequest(self):
        with app.app_context():
            self.assertRaises(
                BadRequest,
                assert_remote_ordering_allowed,
                remote_service='NOT_VALID_SERVICE',
                user_id=123,
            )

    def test_assert_remote_ordering_allowed_error_forbidden(self):
        with app.app_context():
            g.user = UserInfo(username="randomusertest@apps-foundry.com",
                              user_id=12349,
                              token='abcd',
                              perm=['a', 'b', 'c', ])
            self.assertRaises(
                Forbidden,
                assert_remote_ordering_allowed,
                remote_service=GRAMEDIA,
                user_id=12349,
            )

    def test_assert_remote_ordering_allowed_allow_for_superuser(self):
        with app.app_context():
            g.user = UserInfo(username="zzzz@apps-foundry.com",
                              user_id=12345,
                              token='abcd',
                              perm=['can_read_write_global_all', 'b', 'c', ])
            actual = assert_remote_ordering_allowed(
                remote_service=GRAMEDIA,
                user_id=12345,
            )
            self.assertIsNone(actual)

    def test_get_paymentgateway_for_remote_service(self):
        actual_pg_elevenia = get_paymentgateway_for_remote_service(ELEVENIA)
        actual_pg_gramedia = get_paymentgateway_for_remote_service(GRAMEDIA)
        self.assertEqual(actual_pg_elevenia.id, ELEVENIA_PAYMENT_GATEWAY)
        self.assertEqual(actual_pg_gramedia.id, GRAMEDIA_PAYMENT_GATEWAY)

    def test_get_paymentgateway_for_remote_service_forbidden(self):
        self.assertRaises(BadRequest, get_paymentgateway_for_remote_service, 'INVALIDSERVICE')

    def test_get_client_id_for_remote_service(self):
        actual_elevenia = get_client_id_for_remote_service(ELEVENIA)
        actual_gramedia = get_client_id_for_remote_service(GRAMEDIA)
        self.assertEqual(actual_elevenia, 80)
        self.assertEqual(actual_gramedia, 82)

    def test_assert_user_doesnt_already_own(self):
        """
            initial data:
             user 100 item 1, without buffet id
             user 100 item 2, with buffet id

            test assert_user_doesnt_already_own:
             item 1, elevenia --> error UnprocessableEntity
             item 1, gramedia --> False : already owned, don't add
             item 2, elevenia --> True : not owned, can add
             item 2, gramedia --> True : not owned, can add
             item 3 (elevenia dan gramedia --> True : not owned, can add
        """
        actual1g = assert_user_doesnt_already_own(user_id=100, item_id=1, remote_service_name=GRAMEDIA)
        self.assertEqual(actual1g, False, msg='user 100-item 1-gramedia: should return False')
        actual2e = assert_user_doesnt_already_own(user_id=100, item_id=2, remote_service_name=ELEVENIA)
        self.assertEqual(actual2e, True, msg='user 100-item 2-elevenia: should return True')
        actual2g = assert_user_doesnt_already_own(user_id=100, item_id=2, remote_service_name=GRAMEDIA)
        self.assertEqual(actual2g, True, msg='user 100-item 2-gramedia: should return True')
        actual3e = assert_user_doesnt_already_own(user_id=100, item_id=3, remote_service_name=ELEVENIA)
        self.assertEqual(actual3e, True, msg='user 100-item 3-elevenia: should return True')
        actual3g = assert_user_doesnt_already_own(user_id=100, item_id=3, remote_service_name=GRAMEDIA)
        self.assertEqual(actual3g, True, msg='user 100-item 3-gramedia: should return True')

        self.assertRaises(UnprocessableEntity,
                          assert_user_doesnt_already_own,
                          user_id=100, item_id=1, remote_service_name=ELEVENIA)
        pass

    def test_get_discount_for_remote_offer(self):
        session = db.session()
        session.add(self.offer_single1)
        remote_service_name = GRAMEDIA
        remote_order_number = '12345'
        # pass valid discount id to method, and assert if it's return Discount object with the same id
        line_info = {"discount_id": BOOK_GENERAL_DISCOUNT_ID}
        discount = get_discount_for_remote_offer(session, self.offer_single1, line_info,
                                                 remote_service_name, remote_order_number)
        self.assertIsNotNone(discount)
        self.assertEqual(discount.id, BOOK_GENERAL_DISCOUNT_ID)

        # pass empty discount id to method,
        # and assert if it's return Default discount with id = BOOK_GENERAL_DISCOUNT_ID too (hardcoded for books)
        line_info = {"discount_id": None}
        discount = get_discount_for_remote_offer(session, self.offer_single1, line_info,
                                                 remote_service_name, remote_order_number)
        self.assertIsNotNone(discount)
        self.assertEqual(discount.id, BOOK_GENERAL_DISCOUNT_ID)

    def test_assert_discount_from_remote_checkout(self):
        s = db.session()
        s.add(self.discount_books)
        s.add(self.offer_buffet)
        s.add(self.offer_single1)
        s.add(self.offer_single2)
        s.add(self.offer_single3)
        s.add(self.offer_single_vendor_not_in_disc)
        remote_service_name = 'gramedia'
        remote_order_number = '12345'
        # discount is invalid for offer buffet
        #  (because we setup BOOK_GENERAL_DISCOUNT_ID for single offer only - in predefined_group)
        self.assertRaises(DiscountNotValidForOfferInRemoteCheckout,
                          assert_discount_from_remote_checkout,
                          self.discount_books, self.offer_buffet, remote_service_name, remote_order_number)
        # discount is invalid for offer_single1 (valid vendor, valid offer type)
        self.assertIsNone(assert_discount_from_remote_checkout(
            self.discount_books, self.offer_single1, remote_service_name, remote_order_number))
        # validate discount.vendors vs offer.items[0].brand.vendor_id, invalid because this offer's vendor not in disc
        self.assertRaises(DiscountNotValidForOfferInRemoteCheckout,
                          assert_discount_from_remote_checkout,
                          self.discount_books, self.offer_single_vendor_not_in_disc,
                          remote_service_name, remote_order_number)

        # validate discount.offers vs offer -- start
        # add offers limitation for a discount
        self.discount_books.offers.append(self.offer_single1)
        self.discount_books.offers.append(self.offer_single2)
        # offer 2 is valid, bcause vendors is correct and offer exists in discount.offers and type is single
        self.assertIsNone(assert_discount_from_remote_checkout(
            self.discount_books, self.offer_single2,remote_service_name, remote_order_number))
        # offer 3 invalid because not in the list of discount.offers (even tho offer type and vendor is correct)
        self.assertRaises(DiscountNotValidForOfferInRemoteCheckout,
                          assert_discount_from_remote_checkout,
                          self.discount_books, self.offer_single3,
                          remote_service_name, remote_order_number)
        # validate discount.offers vs offer -- end

    def test_assert_remote_order_not_exists(self):
        session = db.session()
        pg = session.query(PaymentGateway).get(GRAMEDIA_PAYMENT_GATEWAY)
        order = OrderFactory(remote_order_number='123abc', paymentgateway=pg)
        session.commit()
        self.assertRaises(Conflict, assert_remote_order_not_exists,
                          '123abc', session)

        actual_no_error = assert_remote_order_not_exists('123abc890', session)
        self.assertIsNone(actual_no_error)

    def test_construct_order_from_remote_request(self):
        session = db.session()
        user = session.query(User).get(102)
        args = {
            "sub_total": 29000,
            "grand_total": 29000,
            "sender_email": "buyer2@test.com",
            "receiver_email": "buyer2@test.com",
            "user_id": 102,
            "remote_service_name": "gramedia",
            "currency_code": "idr",
            "items": [{"id": "SC00P101",
                       "discount_id": None,
                       "sub_total": 15000,
                       "grand_total": 15000
                       },
                      {"id": "SC00P102",
                       "discount_id": BOOK_GENERAL_DISCOUNT_ID,
                       "sub_total": 14000,
                       "grand_total": 14000
                       }
                      ],
            "user_message": "",
            "remote_order_number": "order12345abc6"
            }
        order = construct_order_from_remote_request(args=args, session=session, user=user)
        self.assertEqual(order.order_status, COMPLETE)
        self.assertEqual(order.remote_order_number, "order12345abc6")
        self.assertEqual(order.user_id, 102)
        self.assertEqual(order.final_amount, 29000)
        order_lines = order.order_lines
        expected_total_base_price = 0
        for line in order_lines:
            expected_total_base_price += line.offer.price_idr
            self.assertIn(line.offer_id, [101, 102])
            offer = session.query(Offer).get(line.offer_id)
            self.assertEqual(line.price, line.offer.price_idr)
            if line.offer_id == 101:
                self.assertEqual(line.final_price, 15000)
            else:
                self.assertEqual(line.final_price, 14000)
        self.assertEqual(order.total_amount, expected_total_base_price)
        order_line_discounts = order.order_line_discounts
        self.assertIsNotNone(order_line_discounts)
        self.assertEqual(order_line_discounts[0].discount_id, BOOK_GENERAL_DISCOUNT_ID)
        session.expunge_all()

    def test_user_allowed_to_get_point(self):
        s = db.session()
        role_guest = s.query(Role).get(RoleType.guest_user.value)
        role_verified = s.query(Role).get(RoleType.verified_user.value)
        user_guest = UserFactory(id=889911, roles=[role_guest, ])
        user_verified = UserFactory(id=889912, roles=[role_verified, ])
        organization = OrganizationFactory(id=889910)

        # payment gateway FREE not allowed to get point on purchase
        actual = user_allowed_to_get_point(
            user_id=user_verified.id, payment_gateway_id=FREE, is_top_up_wallet_transaction=False
        )
        self.assertEqual(False, actual)
        # payment gateway POINT not allowed to get point on purchase
        actual = user_allowed_to_get_point(
            user_id=user_verified.id, payment_gateway_id=POINT, is_top_up_wallet_transaction=False
        )
        self.assertEqual(False, actual)
        # Purchase for wallet top up not allowed to get point on purchase
        actual = user_allowed_to_get_point(
            user_id=user_verified.id, payment_gateway_id=OLD_VERITRANS, is_top_up_wallet_transaction=True
        )
        self.assertEqual(False, actual)
        # Organization Purchase not allowed to get point on purchase
        actual = user_allowed_to_get_point(
            user_id=organization.id, payment_gateway_id=OLD_VERITRANS, is_top_up_wallet_transaction=False
        )
        self.assertEqual(False, actual)
        # User Guest not allowed to get point on purchase
        actual = user_allowed_to_get_point(
            user_id=user_guest.id, payment_gateway_id=OLD_VERITRANS, is_top_up_wallet_transaction=False
        )
        self.assertEqual(False, actual)

        # other than above condition: allowed to get point on purchase
        actual = user_allowed_to_get_point(
            user_id=user_verified.id, payment_gateway_id=OLD_VERITRANS, is_top_up_wallet_transaction=False
        )
        self.assertEqual(True, actual)

    @classmethod
    def init_data(cls):
        session = db.session()
        generate_static_sql_fixtures(session)
        currency = session.query(Currencies).get(2)

        pg_elevenia = session.query(PaymentGateway).get(ELEVENIA_PAYMENT_GATEWAY)
        pg_gramedia = session.query(PaymentGateway).get(GRAMEDIA_PAYMENT_GATEWAY)

        offer_type_buffet = session.query(OfferType).get(BUFFET)
        offer_type_single = session.query(OfferType).get(SINGLE)

        cls.offer_buffet = OfferFactory(id=1, offer_type=offer_type_buffet)
        obf = OfferBuffetFactory(id=1, offer=cls.offer_buffet)

        cls.offer_single1 = OfferFactory(id=101, offer_type=offer_type_single, price_idr=16000)
        cls.offer_single2 = OfferFactory(id=102, offer_type=offer_type_single, price_idr=20000)
        cls.offer_single3 = OfferFactory(id=103, offer_type=offer_type_single)
        cls.offer_single_vendor_not_in_disc = OfferFactory(id=104, offer_type=offer_type_single)

        vendor_books = VendorFactory(id=1000)
        vendor_not_in_discount = VendorFactory(id=1001)
        brand_normal = BrandFactory(id=1002, vendor=vendor_books)
        brand_vendor_not_in_disc = BrandFactory(id=1003, vendor=vendor_not_in_discount)

        item1 = ItemFactory(id=1, item_type=ITEM_TYPES[BOOK], brand=brand_normal)
        item2 = ItemFactory(id=2, item_type=ITEM_TYPES[BOOK], brand=brand_normal)
        cls.offer_buffet.items.append(item2)
        item3 = ItemFactory(id=3, item_type=ITEM_TYPES[BOOK], brand=brand_normal)
        item4 = ItemFactory(id=4, item_type=ITEM_TYPES[BOOK], brand=brand_vendor_not_in_disc)

        cls.offer_single1.items.append(item1)
        cls.offer_single2.items.append(item2)
        cls.offer_single3.items.append(item3)
        cls.offer_single_vendor_not_in_disc.items.append(item4)

        order1 = OrderFactory(paymentgateway=pg_elevenia)
        order_line1 = OrderLineFactory(offer=cls.offer_buffet, order=order1)
        order_line2 = OrderLineFactory(offer=cls.offer_buffet, order=order1)

        user12345 = UserFactory(id=12345, email='buyer12345@test.com')
        user1 = UserFactory(id=100, email='buyer1@test.com')
        user2 = UserFactory(id=102, email='buyer2@test.com')
        ubf = UserBuffetFactory(id=1, user_id=100, orderline=order_line2, offerbuffet=obf)
        user_item1 = UserItemFactory(user_id=100, item=item1, user_buffet_id=None, orderline=order_line1)
        user_item2 = UserItemFactory(user_id=100, item=item2, user_buffet_id=1, orderline=order_line2)

        cls.discount_books = DiscountFactory(
            id=BOOK_GENERAL_DISCOUNT_ID, name='general discount books',
            valid_to=get_local() + timedelta(days=7), valid_from=get_local() - timedelta(days=7),
            discount_rule=2, discount_type=1, discount_status=2, discount_schedule_type=1,
            is_active=True, discount_usd=20, discount_idr=20, discount_point=20,
            predefined_group=5)
        cls.discount_books.vendors.append(vendor_books)

        session.commit()
