from datetime import datetime, timedelta
from unittest import TestCase

from nose.tools import istest

from tests.fixtures.helpers import generate_static_sql_fixtures
from tests.testcases import SATestCaseMixin
from tests.fixtures.sqlalchemy.items import ItemFactory, UserItemFactory

from app import db

from scripts.archive_to_s3 import find_items_to_archive


@istest
class ReadyForArchiveDetectionTests(SATestCaseMixin, TestCase):
    fixture_factory = UserItemFactory

    @classmethod
    def set_up_fixtures(cls):

        s = db.session()
        generate_static_sql_fixtures(s)
        now = datetime.utcnow()
        start_time = now - timedelta(days=15)

        for i in range(20):
            item = ItemFactory(release_date=start_time)
            start_time += timedelta(days=1)
            s.add(item)

            if i % 2:
                useritem = UserItemFactory(item=item)
                s.add(useritem)

        s.commit()
        return useritem

    def test_find_items_to_archive(self):
        items = find_items_to_archive(
            grace_period=timedelta(days=5)
        )

        item_count = 0
        for _ in items:
            item_count += 1

        # because the slight time difference that exists between now and when
        # the tests were started .. we want one more item than we expected
        self.assertEqual(item_count, 6)


def setup_module():
    db.drop_all()
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
