from unittest import TestCase

from nose.tools import istest

from app import db
from app.items.models import PopularItem

from tests.testcases import SATestCaseMixin

import sys
import os

BASE_DIR = os.path.abspath(
    os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, os.pardir))
sys.path.insert(0, os.path.join(BASE_DIR, 'scripts'))

from scripts.popular_generator import main as popular_generator_main


@istest
class PopularGeneratorTests(TestCase):
    """ Makes sure that our PopularItems view is generated properly.
    """

    def test_view_created(self):
        """ Make sure that our view is created.
        """
        self.assertFalse(view_exists(db.engine, 'item_popular', 'utility'))

        popular_generator_main()

        self.assertTrue(view_exists(db.engine, 'item_popular', 'utility'))

    @classmethod
    def setUpClass(cls):
        db.session().close_all()
        cls.drop_popular_view()
        db.drop_all()
        db.create_all()
        # table item popular automatically created when db.create_all
        #  here we drop it cause it will be replaced with materialized view in popular_generator.main method
        PopularItem.__table__.drop(db.engine, checkfirst=True)

    @classmethod
    def tearDownClass(cls):
        cls.drop_popular_view()
        db.session().close_all()
        db.drop_all()

    @classmethod
    def drop_popular_view(cls):
        try:
            db.engine.execute('DROP MATERIALIZED VIEW utility.item_popular;')
        except:
            pass
        try:
            db.engine.execute('DROP MATERIALIZED VIEW utility.item_later_editions;')
        except:
            pass
        try:
            db.engine.execute('DROP MATERIALIZED VIEW utility.item_downloads;')
        except:
            pass
        try:
            db.engine.execute('DROP VIEW utility.non_free_item RESTRICT;')
        except:
            pass


def view_exists(engine, viewname, schema):
    """ Does a table/view with viewname exist in a schema?

    :param engine:
    :param viewname:
    :param schema:
    :return:
    """
    query_text = ("""
    Select EXISTS (select 1 from pg_catalog.pg_class c inner join pg_catalog.pg_namespace n on c.relnamespace=n.oid
    where n.nspname = '{schema_name}' and c.relname='{table_or_view_name}')
    """.format(
        schema_name=schema, table_or_view_name=viewname))

    return engine.execute(query_text).scalar()
