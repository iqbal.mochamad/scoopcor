from datetime import timedelta
from unittest import TestCase

import boto3
from mockredis import mock_strict_redis_client
from mock import patch
from nose.tools import nottest, istest

from app import app, db
from app.items.files import s3_transfer_factory, S3Downloader, s3_downloader_factory, S3Uploader
from app.items.models import Item
from app.items.web_reader import ProcessingTask, ProcessingTaskManager
from app.items.choices import ProcessingMode, WebReaderTaskStatus
from app.utils.datetimes import get_utc_nieve

from scripts.daemons.web_reader_file_processor import main, get_or_create_task_for_all_latest_editions
from tests.fixtures.sqlalchemy import ItemFactory


processed_item_ids = []


def fake_processor(task):
    global processed_item_ids
    print("Running fake processor for Task {}".format(task.item_id))
    processed_item_ids.append(task.item_id)


@istest
class HighPriorityTests(TestCase):

    def setUp(self):

        self.high_priority_item_ids = []
        self.low_priority_item_ids = []

        global processed_item_ids
        processed_item_ids = []

        self.redis = mock_strict_redis_client()
        self.mgr = ProcessingTaskManager(self.redis)

        for fake_item_id in range(1, 100):
            task = ProcessingTask(fake_item_id)
            if fake_item_id % 2:
                task.is_high_priority = True
                self.high_priority_item_ids.append(fake_item_id)
            else:
                task.is_high_priority = False
                self.low_priority_item_ids.append(fake_item_id)

            self.mgr.enqueue(task)

    @patch('scripts.daemons.web_reader_file_processor.process_web_reader_files')
    def test_processing_loop(self, mock):
        with app.app_context():
            main(maximum_parallel_tasks=5,
                 processing_mode=ProcessingMode.high,
                 max_limit_latest_edition=10,
                 task_manager=self.mgr,
                 )

            global processed_item_ids

            high_completed_task = self.mgr.get_tasks(high_priority_only=True)
            processed_item_ids = [i.item_id for i in high_completed_task]

            self.assertEqual(
                len(processed_item_ids),
                len(self.high_priority_item_ids)
            )
            self.assertItemsEqual(
                processed_item_ids,
                self.high_priority_item_ids
            )


@istest
class GetLatestEditionTests(TestCase):

    @classmethod
    def setUpClass(cls):
        db.create_all()
        cls.create_items()

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.drop_all()

    def test_get_or_create_task_for_all_latest_editions_with_no_last_item_id(self):
        with app.app_context():
            session = db.session()
            redis = mock_strict_redis_client()
            mgr = ProcessingTaskManager(redis)
            mgr.set_last_minimum_item_id(0)

            get_or_create_task_for_all_latest_editions(
                mgr=mgr,
                session=session,
                max_limit_latest_edition=5,
            )
            actual = len(mgr.get_all_tasks())
            self.assertEqual(actual, 5)

    def test_get_or_create_task_for_all_latest_editions_with_last_item_id(self):
        with app.app_context():
            session = db.session()
            redis = mock_strict_redis_client()
            mgr = ProcessingTaskManager(redis)
            mgr.set_last_minimum_item_id(5002)

            get_or_create_task_for_all_latest_editions(
                mgr=mgr,
                session=session,
                max_limit_latest_edition=2,
            )
            actual = len(mgr.get_all_tasks())
            # actual = 2 item from latest editions, + (2 item with item id < 2)
            self.assertEqual(actual, 3)

    @staticmethod
    def create_items():
        modified = get_utc_nieve() - timedelta(days=1)
        session = db.session()
        item_id = 5000
        for i in range(1, 4):
            item_id += 1
            item = ItemFactory(
                id=item_id,
                is_active=True,
                item_status='ready for consume',
                item_type='magazine',
                content_type='pdf',
                page_count=None,
                modified=modified)

        for i in range(1, 4):
            item_id += 1
            item = ItemFactory(
                id=item_id,
                is_active=True,
                item_status='ready for consume',
                item_type='book',
                content_type='pdf',
                page_count=None,
                modified=modified)

        for i in range(1, 4):
            item_id += 1
            item = ItemFactory(
                id=item_id,
                is_active=True,
                item_status='ready for consume',
                item_type='newspaper',
                content_type='pdf',
                page_count=None,
                modified=modified)

        # create item with page count = 0 (process but failed)
        item_id += 1
        item = ItemFactory(
            id=item_id,
            is_active=True,
            item_status='ready for consume',
            item_type='magazine',
            content_type='pdf',
            page_count=0)

        # create item with page count > 0 (process and succeed)
        item_id += 1
        item = ItemFactory(
            id=item_id,
            is_active=True,
            item_status='ready for consume',
            item_type='magazine',
            content_type='pdf',
            page_count=99,
            modified=modified)

        session.commit()
        session.expunge_all()


@nottest
class S3UtilsTests(TestCase):

    def s3_downloader_tests(self):
        with app.app_context():
            item = Item.query.get(2545)

            c = app.test_client(use_cookies=False)

            get_url = '/v1/items/{}/check-files'.format(item.id)
            response = c.get(get_url)

            s3 = boto3.client(
                's3',
                app.config['BOTO3_AWS_REGION'],
                aws_access_key_id=app.config['BOTO3_AWS_APP_ID'],
                aws_secret_access_key=app.config['BOTO3_AWS_APP_SECRET'],
            )
            file_name = item.files[0].file_name
            s3_info = s3.head_object(Bucket='item-file-archives', Key=file_name)

            # s3.download_file('item-file-archives', file_name, '/tmp/123.zip')
            downloader = s3_downloader_factory(S3Downloader, item=item)
            downloader.download_mobile_bundle()
