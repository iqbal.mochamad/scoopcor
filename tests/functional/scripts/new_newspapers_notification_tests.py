from __future__ import unicode_literals, absolute_import

from datetime import timedelta

from app import db, app
from app.items.choices import ItemTypes, STATUS_TYPES, STATUS_READY_FOR_CONSUME, STATUS_NEW
from app.utils.datetimes import get_local
from app.utils.testcases import TestBase
from scripts.new_newspapers_notification import get_item_count, send_email_notifications, construct_email
from tests.fixtures.sqlalchemy import ItemFactory


class NewsPaperTests(TestBase):

    @classmethod
    def setUpClass(cls):
        # create db and some initial data
        super(NewsPaperTests, cls).setUpClass()


        # generate valid items
        for k in range(3):
            item = ItemFactory(
                item_type=ItemTypes.newspaper.value,
                is_active=True,
                item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
                release_date=get_local().replace(hour=0, minute=0, second=0, microsecond=0)
            )

        # create non active item, should not selected
        item = ItemFactory(
            item_type=ItemTypes.newspaper.value,
            is_active=False,
            item_status=STATUS_TYPES[STATUS_NEW],
            release_date=get_local()
        )
        # create book item, should not selected
        item = ItemFactory(
            item_type=ItemTypes.book.value,
            is_active=True,
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            release_date=get_local()
        )
        # create old newspaper item, should not selected
        item = ItemFactory(
            item_type=ItemTypes.newspaper.value,
            is_active=True,
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            release_date=get_local() - timedelta(days=1)
        )
        cls.session.commit()

    def test_get_item_count(self):
        actual = get_item_count(self.session)
        self.assertEqual(actual, 3, 'Active newspaper actual {} != 3'.format(actual))

    def test_construct_email(self):
        expected_to = ['dummy@test.com', ]
        actual = construct_email(expected_to, item_count=0)
        self.assertIn('CRITICAL', actual.subject)
        self.assertEqual(expected_to, actual.to)
        actual = construct_email(expected_to, item_count=10)
        self.assertIn('INFO', actual.subject)

    def test_send_email_notifications(self):
        with app.test_request_context('/'):
            actual = send_email_notifications(['dummy@test.com', ], item_count=0)
            self.assertIsNone(actual)
            actual = send_email_notifications(['dummy@test.com', ], item_count=10)
            self.assertIsNone(actual)
