from __future__ import unicode_literals
from datetime import timedelta
import json
from faker import Factory
from flask import g

from app import db, app, set_request_id
from app.auth.models import UserInfo
from app.master.models import VendorRevenueShare
from app.users.users import User
from tests.fixtures.sqlalchemy.revenue_shares import RevenueShareFactory
from tests.helpers import gen_api_safe_iso8601_datetime
from tests import testcases
from tests.fixtures.sqlalchemy.master import VendorRevenueShareFactory, \
    VendorFactory


class GetTests(testcases.DetailTestCase):
    request_url = '/v1/vendors/{0.vendor_id}/revenue_shares/{0.id}'
    fixture_factory = VendorRevenueShareFactory

    @classmethod
    def set_up_fixtures(cls):
        vendor = VendorFactory()
        rev_share = RevenueShareFactory()
        vr = VendorRevenueShareFactory(vendor=vendor, revenue_share=rev_share)
        return vr

    def test_response_body(self):
        self.assertDictEqual(
            json.loads(self.response.data),
            {
                'valid_from': gen_api_safe_iso8601_datetime(self.fixture.valid_from),
                'vendor_id': self.fixture.vendor_id,
                'valid_to': gen_api_safe_iso8601_datetime(self.fixture.valid_to),
                'revenue_id': self.fixture.revenue_id,
                'is_active': self.fixture.is_active,
                'id': self.fixture.id,
            })


class CreateTests(testcases.CreateTestCase):
    request_url = '/v1/vendors/{0.id}/revenue_shares'
    fixture_factory = VendorRevenueShareFactory

    VENDOR_FIXTURE_INDEX = 0
    REV_SHARE_FIXTURE_INDEX = 1

    @classmethod
    def set_up_fixtures(cls):
        vendor = VendorFactory()
        rev_share = RevenueShareFactory()
        return [vendor, rev_share, ]

    @classmethod
    def get_api_request_body(cls):
        vendor_fixture = cls.fixtures[cls.VENDOR_FIXTURE_INDEX]
        revenue_fixture = cls.fixtures[cls.REV_SHARE_FIXTURE_INDEX]
        # TODO: the apoin endpoint should accept timezone info in ISO8601
        valid_from = _fake.date_time()
        valid_from = valid_from.replace(tzinfo=None)
        return {
            'valid_from': valid_from.isoformat(),
            'vendor_id': vendor_fixture.id,
            'valid_to': (valid_from + timedelta(days=30)).isoformat(),
            'revenue_id': revenue_fixture.id,
            # 'is_active': _fake.pybool(),
        }

    def test_response_body(self):
        self.assertDictContainsSubset(
            expected=self.request_body,
            actual=json.loads(self.response.data),
            msg="Actual response body: {0.data}".format(self.response)
        )


class UpdateTest(testcases.UpdateTestCase):
    request_url = '/v1/vendors/{0.vendor_id}/revenue_shares/{0.id}'
    fixture_factory = VendorRevenueShareFactory

    @classmethod
    def get_api_request_body(cls):
        session = db.session()
        rev_share = session.query(VendorRevenueShare).get(
            cls.fixture.id)
        valid_from = rev_share.valid_from
        return {
            'valid_from': gen_api_safe_iso8601_datetime(
                valid_from),
            'vendor_id': cls.fixture.vendor_id,
            'valid_to': gen_api_safe_iso8601_datetime(
                valid_from + timedelta(days=30)),
            'revenue_id': cls.fixture.revenue_id,
        }

    def test_response_body(self):
        self.assertDictContainsSubset(
            expected=self.request_body,
            actual=json.loads(self.response.data))


class DeleteTest(testcases.HardDeleteTestCase):
    request_url = '/v1/vendors/{0.vendor_id}/revenue_shares/{0.id}'
    fixture_factory = VendorRevenueShareFactory


_fake = Factory.create()
original_before_req_funcs = app.before_request_funcs


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()
    app.before_request_funcs = {
        None: [
            set_request_id,
            init_g_current_user,
        ]
    }


def teardown_module():
    db.session().close_all()
    db.drop_all()
    app.before_request_funcs = original_before_req_funcs


def init_g_current_user():
    g.user = UserInfo(username='dfdsafsdf', user_id=12345, token='abcd', perm=['can_read_write_global_all',])
    session = db.session()
    g.current_user = session.query(User).get(12345)
    session.close()
