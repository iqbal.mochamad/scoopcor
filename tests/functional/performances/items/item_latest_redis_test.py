import unittest
import json
import time
from urllib import urlencode

from nose.tools import nottest

from app import app, db
from tests.fixtures.sqlalchemy.items import ItemFactory


@nottest
class ItemLatestRedisTests(unittest.TestCase):
    """This tests will load 10.000 item to database, run on demand only

    """

    maxDiff = None
    client = app.test_client(use_cookies=False)
    headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Authorization': 'Bearer MjY4ODYwOmNmZjhjMjg0M'
                         'WVmYzAxZDZmYzEwODU0YjhjMzAzO'
                         'DE5NjNlODA2MjIzY2RmNzJiNTFmZ'
                         'mViNTYzZTkxNDU4MzI5NTEyNDQ5M'
                         'DA4YzcxZDM3'
    }

    @classmethod
    def setUpClass(cls):
        db.create_all()
        s = db.session()

        for _ in range(10000):
            ItemFactory()

        s.commit()

        cls.args = {'offset': 0, 'limit': 10}

        cls.resp = cls.client.get('/v1/items/latest?{params}'.format(params=urlencode(cls.args)), headers=cls.headers)
        cls.result = json.loads(cls.resp.data)


    @classmethod
    def tearDownClass(cls):
        db.session.close_all()
        db.drop_all()

    def test_time_re_run_item_latest(self):
        start = time.time()

        args = {'offset': 10, 'limit': 10}
        self.client.get('/v1/items/latest?{params}'.format(params=urlencode(args)), headers=self.headers)

        end = time.time()
        total_time = (end - start) * 1000

        self.assertLessEqual(total_time, 100)
