from __future__ import unicode_literals, absolute_import
from datetime import datetime
from decimal import Decimal
import json
from unittest import TestCase
import httplib

from app import app, db
from app.logs.models import IabErrorLog


class GetTests(TestCase):
    client = app.test_client(use_cookies=False)
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.create_all()

        # create model in the database!
        iab_error_logs = IabErrorLog(
                developer_payload="80b71aedd37cf6173b19bd7c063b2e42b58479308cae5474ab8e2d8ec59ae08c",
                iab_order_id="GPA.1397-8485-4932-13239",
                package_name="com.appsfoundry.scoop",
                product_id="com.appsfoundry.scoop.c.usd.1.99",
                purchase_state=0,
                purchase_time="1452068448406",
                purchase_token="dkdajogmkajkeoccokikohhn.AO-J1OyI_pA2HjyvFBKYCL5K7X3uOEkcnDHRAQrO-Uf_UA2vgpR4A1kK9v0cwM016rRQjT30HdMdnpnfqzMksvooM8Xif5Le1iEsIP9OwJvLnu1qSdw92VOLCbh7z-Hpnfo9xyc6666u_W-ynNXwKKEZkM7lz-O0ew",
                status="new",
                user_id=3124,
        )

        s = db.session()
        s.add(iab_error_logs)
        s.commit()

        cls.response = cls.client.get('/v1/logs/iab_errors/1', headers={
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
        })

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.session.remove()
        db.drop_all()

    def test_response_body(self):
        self.assertDictContainsSubset({
            "developer_payload": "80b71aedd37cf6173b19bd7c063b2e42b58479308cae5474ab8e2d8ec59ae08c",
            "iab_order_id": "GPA.1397-8485-4932-13239",
            "id": 1,
            "is_active": True,
            "package_name": "com.appsfoundry.scoop",
            "product_id": "com.appsfoundry.scoop.c.usd.1.99",
            "purchase_state": 0,
            "purchase_time": "1452068448406",
            "purchase_token": "dkdajogmkajkeoccokikohhn.AO-J1OyI_pA2HjyvFBKYCL5K7X3uOEkcnDHRAQrO-Uf_UA2vgpR4A1kK9v0cwM016rRQjT30HdMdnpnfqzMksvooM8Xif5Le1iEsIP9OwJvLnu1qSdw92VOLCbh7z-Hpnfo9xyc6666u_W-ynNXwKKEZkM7lz-O0ew",
            "status": "new",
            "user_id": "3124"
                }, json.loads(self.response.data))

    def test_status_code(self):
        self.assertEqual(200, self.response.status_code)

    def test_response_content_type(self):
        self.assertEqual('application/json', self.response.content_type)



class ListTests(TestCase):
    client = app.test_client(use_cookies=False)
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.create_all()
        iab_error_logs_1 = IabErrorLog(
                developer_payload="80b71aedd37cf6173b19bd7c063b2e42b58479308cae5474ab8e2d8ec59ae08c",
                iab_order_id="GPA.1397-8485-4932-13239",
                package_name="com.appsfoundry.scoop",
                product_id="com.appsfoundry.scoop.c.usd.1.99",
                purchase_state=2,
                purchase_time="1452068448406",
                purchase_token="dkdajogmkajkeoccokikohhn.AO-J1OyI_pA2HjyvFBKYCL5K7X3uOEkcnDHRAQrO-Uf_UA2vgpR4A1kK9v0cwM016rRQjT30HdMdnpnfqzMksvooM8Xif5Le1iEsIP9OwJvLnu1qSdw92VOLCbh7z-Hpnfo9xyc6666u_W-ynNXwKKEZkM7lz-O0ew",
                status="new",
                user_id=3124,
        )

        iab_error_logs_2 = IabErrorLog(
                developer_payload="80b71aedd37cf6173b19bd7c063b2e42b58479308cae5474ab8e2d8ec59ae08c",
                iab_order_id="GPA.1111-4321-1234-45567",
                package_name="com.appsfoundry.scoop",
                product_id="com.appsfoundry.scoop.c.usd.1.99",
                purchase_state=2,
                purchase_time="1452068449029",
                purchase_token="dkdajogmkajkeoccokikohhn.AO-J1OyI_pA2HjyvFBKYCL5K7X3uOEkcnDHRAQrO-Uf_UA2vgpR4A1kK9v0cwM016rRQjT30HdMdnpnfqzMksvooM8Xif5Le1iEsIP9OwJvLnu1qSdw92VOLCbh7z-Hpnfo9xyc6666u_W-ynNXwKKEZkM7lz-O0ew",
                status="new",
                user_id=3234,
        )

        s = db.session()
        s.add(iab_error_logs_1)
        s.add(iab_error_logs_2)
        s.commit()

        cls.response = cls.client.get('/v1/logs/iab_errors',
                                       headers={
                                           'Accept': 'application/json',
                                           'Accept-Charset': 'utf-8',

                                       })

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.session.remove()
        db.drop_all()

    def test_response_body(self):
        iab_error = json.loads(self.response.data)

        self.assertEqual(iab_error['metadata']['resultset']['count'], 2)

    def test_content_type(self):
        self.assertEqual(self.response.content_type, 'application/json')

    def test_status_code(self):
        self.assertEqual(httplib.OK, self.response.status_code)


class CreateTests(TestCase):
    client = app.test_client(use_cookies=False)
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.create_all()
        cls.response = cls.client.post('/v1/logs/iab_errors',
                                       data=json.dumps({
                                              "user_id": "3124",
                                              "iab_order_id": "GPA.1397-8485-4932-13239",
                                              "package_name": "com.appsfoundry.scoop",
                                              "product_id": "com.appsfoundry.scoop.c.usd.1.99",
                                              "purchase_time": 1452068448406,
                                              "purchase_state": 0,
                                              "developer_payload": "80b71aedd37cf6173b19bd7c063b2e42b58479308cae5474ab8e2d8ec59ae08c",
                                              "purchase_token": "dkdajogmkajkeoccokikohhn.AO-J1OyI_pA2HjyvFBKYCL5K7X3uOEkcnDHRAQrO-Uf_UA2vgpR4A1kK9v0cwM016rRQjT30HdMdnpnfqzMksvooM8Xif5Le1iEsIP9OwJvLnu1qSdw92VOLCbh7z-Hpnfo9xyc6666u_W-ynNXwKKEZkM7lz-O0ew"
                                       }),
                                       headers={
                                           'Accept': 'application/json',
                                           'Accept-Charset': 'utf-8',
                                           'Content-Type': 'application/json'
                                       })

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.session.remove()
        db.drop_all()

    def test_response_body(self):
        expect = {
              "developer_payload": "80b71aedd37cf6173b19bd7c063b2e42b58479308cae5474ab8e2d8ec59ae08c",
              "iab_order_id": "GPA.1397-8485-4932-13239",
              "id": 1,
              "is_active": True,
              "package_name": "com.appsfoundry.scoop",
              "product_id": "com.appsfoundry.scoop.c.usd.1.99",
              "purchase_state": 0,
              "purchase_time": "1452068448406",
              "purchase_token": "dkdajogmkajkeoccokikohhn.AO-J1OyI_pA2HjyvFBKYCL5K7X3uOEkcnDHRAQrO-Uf_UA2vgpR4A1kK9v0cwM016rRQjT30HdMdnpnfqzMksvooM8Xif5Le1iEsIP9OwJvLnu1qSdw92VOLCbh7z-Hpnfo9xyc6666u_W-ynNXwKKEZkM7lz-O0ew",
              "status": "new",
              "user_id": "3124"}

        self.assertDictContainsSubset(expect, json.loads(self.response.data))


    def test_status_code(self):
        self.assertEqual(self.response.status_code, httplib.CREATED)

    def test_content_type(self):
        self.assertEqual(self.response.content_type, 'application/json')

    def test_object_exists_in_db(self):
        iab_error = json.loads(self.response.data)
        data = IabErrorLog.query.get(iab_error['id'])
        self.assertEqual(data.is_active, iab_error['is_active'])


class DeleteTests(TestCase):
    client = app.test_client(use_cookies=False)
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.create_all()

        # create model in the database!
        cls.iab_error = IabErrorLog(
                developer_payload="80b71aedd37cf6173b19bd7c063b2e42b58479308cae5474ab8e2d8ec59ae08c",
                iab_order_id="GPA.1397-8485-4932-13239",
                package_name="com.appsfoundry.scoop",
                product_id="com.appsfoundry.scoop.c.usd.1.99",
                purchase_state=0,
                purchase_time="1452068448406",
                purchase_token="dkdajogmkajkeoccokikohhn.AO-J1OyI_pA2HjyvFBKYCL5K7X3uOEkcnDHRAQrO-Uf_UA2vgpR4A1kK9v0cwM016rRQjT30HdMdnpnfqzMksvooM8Xif5Le1iEsIP9OwJvLnu1qSdw92VOLCbh7z-Hpnfo9xyc6666u_W-ynNXwKKEZkM7lz-O0ew",
                status="new",
                user_id=3124,
        )

        s = db.session()
        s.add(cls.iab_error)
        s.commit()

        cls.response = cls.client.delete('/v1/logs/iab_errors/{}'.format(
                cls.iab_error.id),
                                       headers={
                                           'Accept': 'application/json',
                                           'Accept-Charset': 'utf-8',
                                           'Content-Type': 'application/json'
                                       })

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.session.remove()
        db.drop_all()

    def test_response_body(self):
        self.assertEqual(self.response.data, "")


    def test_status_code(self):
        self.assertEqual(self.response.status_code, httplib.NO_CONTENT)

    def test_content_type(self):
        """
        .. note::

            We're just testing that the response type ISN'T JSON.
            Flask is automatically assigning a content/type to all
            responses no matter what ;_;
        """
        #self.assertNotEqual(self.response.content_type, 'application/json')
        self.assertEqual(self.response.content_type, 'application/json')

    def test_object_inactive_in_db(self):
        data = IabErrorLog.query.get(1)
        self.assertEqual(data.is_active, False)


class PutTests(TestCase):
    client = app.test_client(use_cookies=False)
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.create_all()

        # create model in the database!
        cls.iab_errors = IabErrorLog(
                developer_payload="80b71aedd37cf6173b19bd7c063b2e42b58479308cae5474ab8e2d8ec59ae08c",
                iab_order_id="GPA.1397-8485-4932-13239",
                package_name="com.appsfoundry.scoop",
                product_id="com.appsfoundry.scoop.c.usd.1.99",
                purchase_state=0,
                purchase_time="1452068448406",
                purchase_token="dkdajogmkajkeoccokikohhn.AO-J1OyI_pA2HjyvFBKYCL5K7X3uOEkcnDHRAQrO-Uf_UA2vgpR4A1kK9v0cwM016rRQjT30HdMdnpnfqzMksvooM8Xif5Le1iEsIP9OwJvLnu1qSdw92VOLCbh7z-Hpnfo9xyc6666u_W-ynNXwKKEZkM7lz-O0ew",
                status="new",
                user_id=3124,
        )

        s = db.session()
        s.add(cls.iab_errors)
        s.commit()

        cls.response = cls.client.put('/v1/logs/iab_errors/{}'.format(
                cls.iab_errors.id),
                                      data=json.dumps({
                                             "user_id": "3124",
                                              "iab_order_id": "GPA.1397-8485-4932-13239",
                                              "package_name": "com.appsfoundry.scoop",
                                              "product_id": "com.appsfoundry.scoop.c.usd.1.99",
                                              "purchase_time": 1452068448406,
                                              "purchase_state": 0,
                                              "developer_payload": "80b71aedd37cf6173b19bd7c063b2e42b58479308cae5474ab8e2d8ec59ae08c",
                                              "purchase_token":
                                                  "dkdajogmkajkeoccokikohhn.AO-J1OyI_pA2HjyvFBKYCL5K7X3uOEkcnDHRAQrO-Uf_UA2vgpR4A1kK9v0cwM016rRQjT30HdMdnpnfqzMksvooM8Xif5Le1iEsIP9OwJvLnu1qSdw92VOLCbh7z-Hpnfo9xyc6666u_W-ynNXwKKEZkM7lz-O0ew",
                                          "status": "processing",

                                       }),
                                      headers={
                                           'Accept': 'application/json',
                                           'Accept-Charset': 'utf-8',
                                           'Content-Type': 'application/json'
                                       })


    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.session.remove()
        db.drop_all()

    def test_response_body(self):
        iab_errors = json.loads(self.response.data)
        data = IabErrorLog.query.get(int(iab_errors['id']))
        self.assertEqual(iab_errors['id'], data.id)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, httplib.OK)

    def test_content_type(self):
        self.assertEqual(self.response.content_type, 'application/json')

    def test_database_object_updated(self):
        session = db.session()
        data = session.query(IabErrorLog).get(1)
        self.assertNotEqual(data.status, 'new')
        self.assertEqual(data.status, 'processing')

