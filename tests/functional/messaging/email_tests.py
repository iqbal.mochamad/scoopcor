from distutils.util import strtobool
from unittest import TestCase, SkipTest

from app import app, db
from app.items.models import Item
from app.messaging.email import HtmlEmail, SmtpServer, get_smtp_server
from nose.tools import istest
from tests.fixtures.sqlalchemy import ItemFactory


@istest
class SignupApiTests(TestCase):

    db = db
    MaxDiff = None

    def setUp(self):
        # make sure the test mode flag is set, or bail out
        if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
            raise SkipTest("COR is not in testing mode.  Cannot continue.")

        self.db.drop_all()
        self.db.create_all()

        item = ItemFactory()

    def tearDown(self):
        self.db.session.rollback()
        self.db.drop_all()

    def test_send_email(self):

        item = Item.query.first()

        message = HtmlEmail(
            sender="web-reader@getscoop.com",
            to=['test@getscoop.com', ],
            subject="Scoop Web Reader - {0.name} Sudah Siap".format(item),
            html_template="email/web_reader/notify-user-processing-complete.html",
            plaintext_template="email/web_reader/notify-user-processing-complete.txt",
            item=item,
        )

        with app.app_context():
            smtp = get_smtp_server()
            smtp.send_message(message)
        pass
