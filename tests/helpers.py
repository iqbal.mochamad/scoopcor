from __future__ import unicode_literals

from base64 import b64encode
from datetime import datetime
import os
from jsonschema import RefResolver

from pytz import UTC


def gen_api_safe_iso8601_datetime(input_datetime):
    """ Strips milliseconds off ISO8601 formatted dates and ensures a timezone.

    :param `datetime.datetime` input_datetime: Any datetime instance.
    :return: ISO8601-encoded DATETIME string.
    """
    return input_datetime.replace(microsecond=0, tzinfo=UTC).isoformat()


def gen_api_safe_iso8601_date(input_date_or_datetime):
    """ Strips milliseconds off ISO8601 formatted dates and ensures a timezone.

    :param `datetime.date` input_date_or_datetime:
        Any date or datetime instance.
    :return: ISO8601-encoded DATE string.
    """
    if isinstance(input_date_or_datetime, datetime):
        input_date_or_datetime = input_date_or_datetime.date()
    return input_date_or_datetime.isoformat()


def bearer_string(user, token):
    return b64encode("{}:{}".format(user.id, token.token))


def get_app_base_dir():
    return os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))




        # scheme = urlsplit(uri).scheme
        #
        # if scheme in self.handlers:
        #     result = self.handlers[scheme](uri)
        # elif (
        #                 scheme in [u"http", u"https"] and
        #             requests and
        #             getattr(requests.Response, "json", None) is not None
        # ):
        #     # Requests has support for detecting the correct encoding of
        #     # json over http
        #     if callable(requests.Response.json):
        #         result = requests.get(uri).json()
        #     else:
        #         result = requests.get(uri).json
        # else:
        #     # Otherwise, pass off to urllib and assume utf-8
        #     result = json.loads(urlopen(uri).read().decode("utf-8"))
        #
        # if self.cache_remote:
        #     self.store[uri] = result
        # return result
