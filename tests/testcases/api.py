"""
API Test Cases
==============

Base test classes for any functional tests that run against SCOOPCOR API and
it's database.
"""
from __future__ import unicode_literals

from abc import ABCMeta, abstractmethod
from httplib import OK, CREATED, NO_CONTENT
import json
import random
from unittest import TestCase

from six import string_types

from app import app, db

from .database import SATestCaseMixin
from .util import abstractclassmethod

__all__ = ['ApiTestCaseMixin', 'DetailTestCase', 'UpdateTestCase',
           'ListTestCase', 'SoftDeleteTestCase', 'CreateTestCase',
           'HardDeleteTestCase',
           ]


class ApiTestCaseMixin(object):
    """ Simple mixin that just adds a preconfigured client to talk to scoopcor.
    """
    __metaclass__ = ABCMeta

    client = app.test_client(use_cookies=False)

    request_url = ""
    request_headers = {}

    @abstractclassmethod
    def get_api_response(cls):
        pass

    def assertScoopErrorResponse(self, response):
        try:
            response_json = json.loads(response.data)
            self.assertItemsEqual(
                ['user_message', 'developer_message', 'status', 'error_code'],
                response_json.keys())
            self.assertIsInstance(response_json['user_message'], string_types)
            self.assertIsInstance(response_json['developer_message'], string_types)
            self.assertIsInstance(response_json['status'], int)
            self.assertIsInstance(response_json['error_code'], int)

        except ValueError:
            self.fail("Failed to parse JSON.")


class ListTestCase(SATestCaseMixin, ApiTestCaseMixin, TestCase):

    resultset_max_size = 19
    resultset_min_size = 3
    max_response_size = 20
    list_key = 'objects'

    __metaclass__ = ABCMeta

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
    }

    @classmethod
    def setUpClass(cls):
        super(ListTestCase, cls).setUpClass()
        cls.response = cls.get_api_response()

    def test_response_code(self):
        self.assertEqual(OK, self.response.status_code)

    def test_response_content_type(self):
        self.assertEqual('application/json', self.response.content_type)

    def test_response_metadata(self):
        """
        .. note::

            This needs improvement.  It only tests for the existence
            of the proper metadata keys.
        """
        d = json.loads(self.response.data)
        metadata = d['metadata']['resultset']
        self.assertItemsEqual(
            expected_seq=['offset', 'limit', 'count'],
            actual_seq=metadata.keys()
        )

    @abstractmethod
    def assert_entity_detail_expectation(self, fixture, fixture_json):
        pass

    def test_response_body(self):
        response_json = json.loads(self.response.data)
        for entity_json in response_json[self.list_key]:
            entity = filter(lambda e: e.id == entity_json['id'],
                            self.fixtures)[0]
            self.assert_entity_detail_expectation(entity, entity_json)

    @classmethod
    def set_up_fixtures(cls):
        return [cls.fixture_factory() for _
                in range(random.randint(cls.resultset_min_size,
                                        cls.resultset_max_size))]

    @classmethod
    def get_api_response(cls):
        return cls.client.get(cls.request_url, headers=cls.request_headers)


class DetailTestCase(SATestCaseMixin, ApiTestCaseMixin, TestCase):

    __metaclass__ = ABCMeta

    fixture = None

    request_headers = {
        'Accept': "application/json",
        'Accept-Charset': 'utf-8',
    }

    @classmethod
    def setUpClass(cls):
        super(DetailTestCase, cls).setUpClass()
        cls.fixture = cls.fixtures[0] if cls.fixtures else None
        cls.response = cls.get_api_response()

    @classmethod
    def get_api_response(cls):
        return cls.client.get(cls.request_url.format(cls.fixture),
                              headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(
            OK, self.response.status_code,
            'response status code is not OK, response data: {}'.format(self.response.data))

    def test_response_content_type(self):
        self.assertEqual('application/json', self.response.content_type,
                         msg="JSON endpoints set Content-Type response header "
                             "to application/json.  "
                             "Got: {0.content_type}".format(self.response))

    @classmethod
    def set_up_fixtures(cls):
        return cls.fixture_factory()

    @abstractmethod
    def test_response_body(self):
        """  Derived classes must override this method and check the returned
        JSON matches expectations.
        """
        pass


class UpdateTestCase(DetailTestCase):

    __metaclass__ = ABCMeta

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Content-Type': 'application/json',
    }

    request_body = {}

    @abstractclassmethod
    def get_api_request_body(self):
        pass

    @classmethod
    def get_api_response(cls):
        cls.request_body = cls.get_api_request_body()
        return cls.client.put(cls.request_url.format(cls.fixture),
                              data=json.dumps(cls.request_body),
                              headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(OK, self.response.status_code,
                         msg="Updating existing objects returns HTTP 200 OK. "
                             "Actual response code was: {0.status_code} \n "
                             "{0.data}".format(
                                self.response))

    def test_get_returns_same_data(self):
        pass


class CreateTestCase(DetailTestCase):

    __metaclass__ = ABCMeta

    request_headers = {
        'Accept': 'application/json',
        'Accept-Charset': 'utf-8',
        'Content-Type': 'application/json',
    }

    request_body = {}

    @abstractclassmethod
    def get_api_request_body(self):
        pass

    @classmethod
    def get_api_response(cls):
        cls.request_body = cls.get_api_request_body()
        return cls.client.post(cls.request_url.format(cls.fixture),
                               data=json.dumps(cls.request_body),
                               headers=cls.request_headers)

    @classmethod
    def set_up_fixtures(cls):
        """ Create any required fixtures for the test.  On CREATE tests,
        this usually doesn't make any sense.
        """
        return []

    def test_response_code(self):
        self.assertEqual(CREATED, self.response.status_code,
                         msg="Creating new objects returns HTTP 201.  "
                             "Got {0.status_code}. Response: {0.data}".format(self.response))


class SoftDeleteTestCase(DetailTestCase):

    __metaclass__ = ABCMeta

    request_headers = {}

    @classmethod
    def get_api_response(cls):
        return cls.client.delete(cls.request_url.format(cls.fixture),
                                 headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(NO_CONTENT, self.response.status_code,
                         msg="Soft-deleting objects simulates an actual delete "
                             "and returns HTTP 204 NO CONTENT.  "
                             "Actual response code: {0.status_code}".format(
                                self.response))

    def test_response_body(self):
        self.assertEqual("", self.response.data,
                         msg="Soft-deleting objects simulates an actual delete "
                             "and return an empty HTTP body.  "
                             "Actual response body: {0.data}\n".format(
                                self.response))

    def test_response_content_type(self):
        self.assertTrue(True, msg="We don't care the return type here.  "
                                  "Werkzeug always forces a content/type, "
                                  "even on deletes with no body.")

    def test_entity_is_inactive(self):
        session = db.session()
        model = session.query(
            self.fixture_factory._get_model_class()).get(self.fixture.id)

        if model is None:
            self.fail("Could not find model in database")

        self.assertFalse(model.is_active, msg="Model is_active should be false "
                                              "after a soft-delete.")

    @classmethod
    def set_up_fixtures(cls):
        return cls.fixture_factory(is_active=True)


class HardDeleteTestCase(DetailTestCase):

    __metaclass__ = ABCMeta

    request_headers = {}

    @classmethod
    def get_api_response(cls):
        return cls.client.delete(cls.request_url.format(cls.fixture),
                                 headers=cls.request_headers)

    def test_response_code(self):
        self.assertEqual(NO_CONTENT, self.response.status_code,
                         msg="Soft-deleting objects simulates an actual delete "
                             "and returns HTTP 204 NO CONTENT.  "
                             "Actual response code: {0.status_code}".format(
                                self.response))

    def test_response_body(self):
        self.assertEqual("", self.response.data,
                         msg="Soft-deleting objects simulates an actual delete "
                             "and return an empty HTTP body.  "
                             "Actual response body: {0.data}\n".format(
                                self.response))

    def test_response_content_type(self):
        self.assertTrue(True, msg="We don't care the return type here.  "
                                  "Werkzeug always forces a content/type, "
                                  "even on deletes with no body.")

    def test_entity_is_deleted(self):
        session = db.session()
        model = session.query(
            self.fixture_factory._get_model_class()).get(self.fixture.id)

        self.assertIsNone(model, msg="Model should be None "
                                     "after a hard-delete.")

    @classmethod
    def set_up_fixtures(cls):
        return cls.fixture_factory(is_active=True)
