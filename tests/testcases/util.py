class abstractclassmethod(classmethod):
    """ SHIM since python2 doesn't support python3's ABC.abstractclassmethod
    """
    __isabstractmethod__ = True

    def __init__(self, callable):
        callable.__isabstractmethod__ = True
        super(abstractclassmethod, self).__init__(callable)
