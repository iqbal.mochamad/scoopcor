from unittest import TestCase

from flask import Flask, Request
from mock import MagicMock
from werkzeug.exceptions import BadRequest

from .util import abstractclassmethod


class RequestContextTestCase(TestCase):
    @classmethod
    def get_app(cls):
        return Flask(__name__)

    @classmethod
    def setUpClass(cls):
        cls.request_context = cls.get_app().test_request_context('/')
        cls.request_context.push()

    @classmethod
    def tearDownClass(cls):
        cls.request_context.pop()


class ParserTestBase(RequestContextTestCase):
    """ Base test class for UnitTests that check the output of a parser.

    Ordinarily, you won't need to provide any of your own functionality
    however, if you do override setUpClass, you need to make sure that
    you call super().
    """
    parser_class = None
    parsed_data = {}
    request_data = {}

    @classmethod
    def validate_class_setup(cls):
        if cls.parser_class is None:
            raise RuntimeError(".parser_class attribute MUST be set "
                               "on instances of ParserTestBase")

    @classmethod
    def setUpClass(cls):
        super(ParserTestBase, cls).setUpClass()

        cls.validate_class_setup()
        cls.request_data = cls.get_request_data()
        try:
            cls.parsed_data = cls.parse_request_data()
        except BadRequest as e:
            raise AssertionError("Couldn't parse!  Dev Msg: {} | User Msg: {}".format(
                e.data.get('developer_message'),
                e.data.get('user_message')))

    @abstractclassmethod
    def get_request_data(cls):
        pass

    @classmethod
    def parse_request_data(cls):
        mock_req = MagicMock(spec=Request)
        mock_req.json = lambda: cls.request_data
        return cls.parser_class().parse_args(req=mock_req)

    def assertParsedFieldMatchesInput(
            self, field_name, input_converter=lambda json_data: json_data,
            msg=None):
        """ Checks that a field from the request input matches parser output.

        :param `basestring` field_name:
            The name of the field in the input and the output dictionary.
            This CANNOT be used if the input and output field names are different.
        :param `callable` input_converter:
            A single parameter function that will be applied to the input data
            to convert it to a type that can be compared to the output data.
        :param `basestring` msg:
            Some message that will be displayed to the user if the test fails.
        :return:
        """
        actual = self.parsed_data[field_name]
        expected = input_converter(self.request_data[field_name])
        self.assertEqual(actual, expected, msg=msg)
