from app import db
from tests.testcases.util import abstractclassmethod

__all__ = ('SATestCaseMixin', )


class SATestCaseMixin(object):
    """ Base test case for any test cases which involve hitting the database.

    .. warning::

        TAKE EXTRA CARE that you are not using your development database, or
        doing something super crazy, like running this against live.  These
        tests are DATA DESTRUCTIVE by nature.
    """
    fixtures = []
    fixture_factory = None

    @classmethod
    def setUpClass(cls):
        db.session.rollback()
        cls.fixtures = cls.set_up_fixtures()

        # just a little simple stop-gap to make sure that fixtures always
        # returns a list, to avoid confusion with its name.
        if not isinstance(cls.fixtures, list):
            cls.fixtures = [cls.fixtures, ]

        cls.remove_fixtures_from_session()

        super(SATestCaseMixin, cls).setUpClass()

    @classmethod
    def remove_fixtures_from_session(cls):
        """ Removes any of our test fixtures from a SQLAlchemy session.

         If they're not removed from the session, we will later have issues
         when trying to reference their properties during a test.

         .. warning::

            If you have any deferred or relation properties, those WILL cause
            errors when they are references.  To negate this problem, you
            should override this method and FORCE loading of those properties
            before expunging.
        """
        session = db.session
        session.commit()
        for fixture in cls.fixtures:
            session.refresh(fixture)
        session.expunge_all()

    @classmethod
    def tearDownClass(cls):
        cls.clean_database()
        super(SATestCaseMixin, cls).tearDownClass()

    @classmethod
    def clean_database(cls):
        """ Remove any test fixtures committed to the database after test.

        By default, it will simply grab the model class created by factory-boy
        and then remove all the entities in the database for that class.

        .. warning::

            FRAGILE!  This relies on a protected method in Factory-Boy.
            Best solution for the time being, but if these all break, look here!
        """
        if cls.fixture_factory is not None:
            session = db.session
            session.rollback()
            for entity in session.query(cls.fixture_factory._get_model_class()):
                session.delete(entity)
            session.commit()

    @abstractclassmethod
    def set_up_fixtures(cls):
        """ Override this method to generate any fixtures required for your test.

        :return:
            Anything you like.  Generally this should be a single SQLAlchemy
            model OR a list of SQLAlchemy models.
        :rtype: Any
        """

