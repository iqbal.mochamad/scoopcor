from .api import *
from .database import *
from .redis import *


def load_tests(loader=None, tests=None, pattern=None):
    # don't load any tests from this module, as they're all base tests
    import unittest
    suite = unittest.TestSuite()
    return suite
