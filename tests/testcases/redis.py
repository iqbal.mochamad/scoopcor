from unittest import TestCase

__all__ = ('RedisTestCase', 'RedisUnitTestCase', )


class RedisTestCase(TestCase):
    """ This mixin should be used with any test cases that utilize redis.

    It simply dumps all keys from the KVS before each test
    """
    redis_instance = None

    def setUp(self):
        clear_redis(self.redis_instance)
        super(RedisTestCase, self).setUp()

    def tearDown(self):
        clear_redis(self.redis_instance)
        super(RedisTestCase, self).setUp()


class RedisUnitTestCase(TestCase):
    """ Similar to the RedisTestCase, but the redis cache is cleared before
    and after each individual test.
    """
    redis_instance = None

    def setUp(self):
        clear_redis(self.redis_instance)
        super(RedisUnitTestCase, self).setUp()

    def tearDown(self):
        clear_redis(self.redis_instance)
        super(RedisUnitTestCase, self).tearDown()


def clear_redis(redis_instance):
    for k in redis_instance.keys("*"):
        redis_instance.delete(k)
