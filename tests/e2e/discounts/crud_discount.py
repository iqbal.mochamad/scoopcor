import json
import httplib

from datetime import datetime
from decimal import Decimal
from unittest import TestCase

from app import app, db
from app.discounts.models import Discount


class GetTests(TestCase):
    client = app.test_client(use_cookies=False)
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.create_all()

        # create model in the database!
        discount = Discount(
            name="this is a name",
            description="description must be here",
            start_date=datetime(year=2015, month=8, day=17),
            end_date=datetime(year=2015, month=8, day=18),
            total_cost=Decimal(20.8832),
            is_active=True
        )

        s = db.session()
        s.add(discount)
        s.commit()

        cls.response = cls.client.get('/v1/discounts/1', headers={
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
        })

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.session.remove()
        db.drop_all()

    def test_response_body(self):
        self.assertDictEqual({
            'id': 1,
            'name': 'this is a name',
            'description': 'description must be here',
            'start_date': '2015-08-17T00:00:00+00:00',
            'end_date': '2015-08-18T00:00:00+00:00',
            'total_cost': 20.8832,
            'is_active': True
        }, json.loads(self.response.data))

    def test_status_code(self):
        self.assertEqual(200, self.response.status_code)

    def test_response_content_type(self):
        self.assertEqual('application/json', self.response.content_type)


class ListTests(TestCase):
    client = app.test_client(use_cookies=False)
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.create_all()
        discount_1 = Discount(
            name="this is a name one",
            description="description must be here",
            start_date=datetime(year=2015, month=8, day=17),
            end_date=datetime(year=2015, month=8, day=18),
            total_cost=Decimal(20.8832),
            is_active=True
        )

        discount_2 = Discount(
            name="this is a name two",
            description="description must be here",
            start_date=datetime(year=2015, month=8, day=17),
            end_date=datetime(year=2015, month=8, day=18),
            total_cost=Decimal(20.8832),
            is_active=True
        )

        s = db.session()
        s.add(discount_1)
        s.add(discount_2)
        s.commit()

        cls.response = cls.client.get('/v1/discounts',
                                      headers={
                                          'Accept': 'application/json',
                                          'Accept-Charset': 'utf-8',

                                      })

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.session.remove()
        db.drop_all()

    def test_response_body(self):
        campaign = json.loads(self.response.data)

        self.assertEqual(campaign['metadata']['resultset']['count'], 2)

    def test_content_type(self):
        self.assertEqual(self.response.content_type, 'application/json')

    def test_status_code(self):
        self.assertEqual(httplib.OK, self.response.status_code)


class CreateTests(TestCase):
    client = app.test_client(use_cookies=False)
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.create_all()
        cls.response = cls.client.post('/v1/discounts',
                                       data=json.dumps({
                                           'name': 'this is a name',
                                           'description': 'description must be here',
                                           'start_date': '2015-08-17T00:00:00+00:00',
                                           'end_date': '2015-08-18T00:00:00+00:00',
                                           'total_cost': 20.8832,
                                           'is_active': True
                                       }),
                                       headers={
                                           'Accept': 'application/json',
                                           'Accept-Charset': 'utf-8',
                                           'Content-Type': 'application/json'
                                       })

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.session.remove()
        db.drop_all()

    def test_response_body(self):
        expect = {
            'id': 1,
            'name': 'this is a name',
            'description': 'description must be here',
            'start_date': '2015-08-17T00:00:00+00:00',
            'end_date': '2015-08-18T00:00:00+00:00',
            'total_cost': 20.8832,
            'is_active': True}

        self.assertDictEqual(expect, json.loads(self.response.data))

    def test_status_code(self):
        self.assertEqual(self.response.status_code, httplib.CREATED)

    def test_content_type(self):
        self.assertEqual(self.response.content_type, 'application/json')

    def test_object_exists_in_db(self):
        discount = json.loads(self.response.data)
        data = Discount.query.get(discount['id'])
        self.assertEqual(data.is_active, discount['is_active'])


class DeleteTests(TestCase):
    client = app.test_client(use_cookies=False)
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.create_all()

        # create model in the database!
        cls.discount = Discount(
            name="this is a name",
            description="description must be here",
            start_date=datetime(year=2015, month=8, day=17),
            end_date=datetime(year=2015, month=8, day=18),
            total_cost=Decimal(20.8832),
            is_active=True
        )
        s = db.session()
        s.add(cls.discount)
        s.commit()

        cls.response = cls.client.delete('/v1/discounts/{}'.format(cls.campaign.id),
                                         headers={
                                             'Accept': 'application/json',
                                             'Accept-Charset': 'utf-8',
                                             'Content-Type': 'application/json'
                                         })

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.session.remove()
        db.drop_all()

    def test_response_body(self):
        self.assertEqual(self.response.data, "")

    def test_status_code(self):
        self.assertEqual(self.response.status_code, httplib.NO_CONTENT)

    def test_content_type(self):
        """
        .. note::

            We're just testing that the response type ISN'T JSON.
            Flask is automatically assigning a content/type to all
            responses no matter what ;_;
        """
        # self.assertNotEqual(self.response.content_type, 'application/json')
        self.assertEqual(self.response.content_type, 'application/json')

    def test_object_inactive_in_db(self):
        data = Discount.query.get(1)
        self.assertEqual(data.is_active, False)


class PutTests(TestCase):
    client = app.test_client(use_cookies=False)
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.create_all()

        # create model in the database!
        cls.campaign = Discount(
            name="this is a name",
            description="description must be here",
            start_date=datetime(year=2015, month=8, day=17),
            end_date=datetime(year=2015, month=8, day=18),
            total_cost=Decimal(20.8832),
            is_active=True
        )

        s = db.session()
        s.add(cls.campaign)
        s.commit()

        cls.response = cls.client.put('/v1/discounts/{}'.format(cls.campaign.id),
                                      data=json.dumps({
                                          'name': 'this is a name changed',
                                          'description': 'description must be here',
                                          'start_date': '2015-08-17T00:00:00+00:00',
                                          'end_date': '2015-08-18T00:00:00+00:00',
                                          'total_cost': 20.8832,
                                          'is_active': True
                                      }),
                                      headers={
                                          'Accept': 'application/json',
                                          'Accept-Charset': 'utf-8',
                                          'Content-Type': 'application/json'
                                      })

    @classmethod
    def tearDownClass(cls):
        db.session().close_all()
        db.session.remove()
        db.drop_all()

    def test_response_body(self):
        discount = json.loads(self.response.data)
        data = Discount.query.get(int(discount['id']))
        self.assertEqual(discount['id'], data.id)

    def test_status_code(self):
        self.assertEqual(self.response.status_code, httplib.OK)

    def test_content_type(self):
        self.assertEqual(self.response.content_type, 'application/json')

    def test_database_object_updated(self):
        data = Discount.query.get(1)
        self.assertNotEqual(data.name, self.discount.name)

