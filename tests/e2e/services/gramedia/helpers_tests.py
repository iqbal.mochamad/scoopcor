# -*- coding: utf-8 -*-
from datetime import timedelta

from decimal import Decimal
from nose.tools import istest, nottest
from unittest import TestCase, SkipTest

from app import app, db
from app.discounts.choices.predefined_type import HARPER_COLLINS
from app.items.choices import STATUS_TYPES, STATUS_READY_FOR_CONSUME, ItemTypes
from app.items.models import Item
from app.parental_controls.choices import ParentalControlType
from app.parental_controls.models import ParentalControl
from app.services.elevenia.exceptions import NoMappableCategoriesError

from app.offers.models import OfferType, Offer
from app.remote_publishing.choices import GRAMEDIA
from app.remote_publishing.models import RemoteOffer
from app.services.elevenia.helpers import OFFER_READY_FOR_SALE
from app.services.gramedia.constants import BOOK_GENERAL_DISCOUNT_ID, HARPER_COLLINS_BOOK_DISCOUNT_ID
from app.services.gramedia.gramedia_gateway import GramediaGateway
from app.services.gramedia.helpers import get_single_offers, get_remote_category, get_formatted_description, \
    build_gramedia_product_from_scoop_offer, get_item, record_offer_published_to_gramedia, get_remote_category_id, \
    publish_offer, NoAvailableItemsForOffer, get_discounted_price, get_general_discount_for_gramedia
from app.utils.datetimes import get_local

from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.discounts import DiscountFactory
from tests.fixtures.sqlalchemy.master import CategoryFactory, BrandFactory, VendorFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, RemoteOfferFactory, RemoteCategoryFactory, OfferTypeFactory


@istest
class HelpersTests(TestCase):

    db = db
    MaxDiff = None

    @classmethod
    def setUpClass(cls):

        # make sure the test mode flag is set, or bail out
        if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
            raise SkipTest("COR is not in testing mode.  Cannot continue.")

        cls.db.drop_all()
        cls.db.create_all()

        cls.create_init_data()

    @classmethod
    def tearDownClass(cls):
        cls.session.rollback()
        cls.db.session.rollback()
        cls.db.drop_all()

    @classmethod
    def create_init_data(cls):
        cls.session = db.session()
        cls.expire_on_commit = False

        parental_control1 = ParentalControl(id=1, name='abc', is_active=True, sort_priority=1)
        parental_control2 = ParentalControl(id=2, name='+17', is_active=True, sort_priority=1)
        cls.session.add(parental_control1)
        cls.session.add(parental_control2)

        # offer_type_single = cls.session.query(OfferType).filter(OfferType.id == 1).first()
        # offer_type_subscription = cls.session.query(OfferType).filter(OfferType.id == 2).first()
        cls.offer_type_single = OfferTypeFactory(id=1, name='single offer')
        cls.offer_type_subscription = OfferTypeFactory(id=2, name='subscription offer')

        category = CategoryFactory(id=1, name='Magazine abc')
        cls.remote_category = RemoteCategoryFactory(
            category_id=category.id,
            remote_service=GRAMEDIA,
            remote_id='categories/books.html')
        category_not_mapped = CategoryFactory(id=2, name='Magazine abc not yet mapped')

        vendor_harper = VendorFactory(id=HARPER_COLLINS)
        vendor_books = VendorFactory(id=1000)
        brand_harper = BrandFactory(id=1000, vendor=vendor_harper)
        brand_normal = BrandFactory(id=1002, vendor=vendor_books)

        cls.item_valid_2 = ItemFactory(
            id=2,
            name='Magazine test 2',
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            item_type=ItemTypes.magazine.value,
            is_active=True,
            content_type='pdf',
            parentalcontrol_id=ParentalControlType.parental_level_1.value,
        )
        cls.item_valid_2.categories.append(category)

        offer_type_2 = OfferFactory(
            id=2,
            name='test offer magazine 2',
            offer_type=cls.offer_type_subscription,
            offer_status=OFFER_READY_FOR_SALE,
        )

        # offer_type_2, wrong offer type, correct item, -- should not selected
        offer_type_2.items.append(cls.item_valid_2)

        cls.item_category_not_mapped = ItemFactory(
            id=3,
            name='item newspaper 3',
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            item_type='newspaper',
            is_active=True,
            content_type='pdf'
        )
        cls.item_category_not_mapped.categories.append(category_not_mapped)
        offer_with_invalid_item = OfferFactory(
            id=123,
            name='test offer newspaper 3',
            offer_type=cls.offer_type_single,
            offer_status=OFFER_READY_FOR_SALE,
            is_free=False,
        )
        # offer 3, correct offer, wrong item, -- should not selected
        offer_with_invalid_item.items.append(cls.item_category_not_mapped)
        cls.offer_id_with_invalid_item = offer_with_invalid_item.id

        item4 = ItemFactory(
            id=4,
            name='item book 4',
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            item_type='book',
            is_active=True,
            content_type='pdf',
            brand=brand_harper
        )
        cls.offer_harper = OfferFactory(
            id=124,
            name='test offer book 4',
            offer_type=cls.offer_type_single,
            offer_status=OFFER_READY_FOR_SALE,
            is_free=False,
            price_idr=100000
        )

        remote_offer4 = RemoteOfferFactory(
            offer_id=cls.offer_harper.id, remote_service=GRAMEDIA,
            remote_id='SC00P'+str(cls.offer_harper.id))

        # offer 4 - offer_harper, correct offer, correct item, already published, -- should not selected
        cls.offer_harper.items.append(item4)

        cls.item_valid_1 = ItemFactory(
            id=91097,
            name='God in the Dock',
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            item_type=u'book',
            is_active=True,
            content_type=u'pdf',
            image_normal='images/1/28711/general_big_covers/ID_HCO2015MTH11GITD.jpeg',
            countries=['ID', ],
            parentalcontrol_id=1,
            brand=brand_normal,
        )
        cls.item_valid_1.categories.append(category)

        cls.offer_valid_1 = OfferFactory(
            id=50385,
            name='test offer 1',
            long_name='God in the Dock long name',
            offer_type=cls.offer_type_single,
            offer_status=OFFER_READY_FOR_SALE,
            is_free=False,
            is_active=True,
            price_idr=50000
        )
        # offer 1, correct offer, correct item, -- should selected
        cls.offer_valid_1.items.append(cls.item_valid_1)

        # magazine, adult, should not selected
        item_magazine_adult = ItemFactory(
            id=5500,
            name='FHM bro',
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            item_type=ItemTypes.magazine.value,
            is_active=True,
            content_type=u'pdf',
            image_normal='images/1/28711/general_big_covers/ID_HCO2015MTH11GITD1.jpeg',
            countries=['ID', ],
            parentalcontrol_id=ParentalControlType.parental_level_2.value,
            brand=brand_normal,
        )
        offer_magazine_adult = OfferFactory(
            id=5500,
            name='FHM Bro',
            long_name='FHM Bro',
            offer_type=cls.offer_type_single,
            offer_status=OFFER_READY_FOR_SALE,
            is_free=False,
            is_active=True,
            price_idr=50000
        )
        offer_magazine_adult.items.append(item_magazine_adult)

        item_magazine_valid = ItemFactory(
            id=5502,
            name='Magazine bro',
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            item_type=ItemTypes.magazine.value,
            is_active=True,
            content_type=u'pdf',
            image_normal='images/1/28711/general_big_covers/ID_HCO2015MTH11GITD1.jpeg',
            countries=['ID', ],
            parentalcontrol_id=ParentalControlType.parental_level_1.value,
            brand=brand_normal,
        )
        offer_magazine_valid = OfferFactory(
            id=5501,
            name='Magazine bro',
            long_name='Magazine Bro',
            offer_type=cls.offer_type_single,
            offer_status=OFFER_READY_FOR_SALE,
            is_free=False,
            is_active=True,
            price_idr=50000
        )
        offer_magazine_valid.items.append(item_magazine_valid)

        cls.discount_harper_collins = DiscountFactory(
            id=HARPER_COLLINS_BOOK_DISCOUNT_ID, name='general discount harper collins',
            valid_to=get_local() + timedelta(days=7), valid_from=get_local() - timedelta(days=7),
            discount_rule=2, discount_type=1, discount_status=2, discount_schedule_type=1,
            is_active=True, discount_usd=5, discount_idr=5, discount_point=5,
            predefined_group=2)
        cls.discount_books = DiscountFactory(
            id=BOOK_GENERAL_DISCOUNT_ID, name='general discount books',
            valid_to=get_local() + timedelta(days=7), valid_from=get_local() - timedelta(days=7),
            discount_rule=2, discount_type=1, discount_status=2, discount_schedule_type=1,
            is_active=True, discount_usd=20, discount_idr=20, discount_point=20,
            predefined_group=2)
        cls.session.add(cls.discount_books)
        cls.session.add(cls.discount_harper_collins)
        cls.session.add(cls.item_valid_2)
        cls.session.add(cls.item_category_not_mapped)
        cls.session.add(cls.item_valid_1)
        cls.session.commit()

    def test_get_single_offers(self):
        with app.app_context():

            number_of_record = 5
            offers = get_single_offers(self.session, number_of_record)

            self.assertIsNotNone(offers)

            for offer in offers:
                self.assertIsNotNone(offer)

                item = offer.items.first()
                self.assertIsNotNone(item)

                self.assertEqual(offer.offer_type_id, 1)
                self.assertEqual(offer.offer_status, OFFER_READY_FOR_SALE)
                self.assertEqual(item.item_status, STATUS_TYPES[STATUS_READY_FOR_CONSUME])
                self.assertIn(item.item_type, [u'magazine', u'book'])

                # adult magazine should not selected
                if item.item_type == ItemTypes.magazine.value:
                    self.assertEqual(item.parentalcontrol_id, ParentalControlType.parental_level_1.value)

                self.assertEqual(item.is_active, True)
                self.assertEqual(item.content_type, 'pdf')

    def test_build_gramedia_product_from_scoop_offer(self):
        with app.app_context():
            session = db.session()

            offers = get_single_offers(session, 1)

            self.assertIsNotNone(offers)

            for offer in offers:
                item = offer.items.first()

                expected_remote_category = get_remote_category(item)

                expected_author_name = item.authors.first().name if item.authors.first() else ""

                expected_publisher = item.brand.vendor.name if item.brand and item.brand.vendor else ""

                expected_description = get_formatted_description(item.description)

                # get actual objecs -- start

                # prepare a mocked gramedia gateway
                url_host = 'http://getfrom_app_config'
                gramedia_gateway = GramediaGateway(url_host=url_host, user='123', password='123', is_mock_mode=True)

                # media_base_url = app.config['MEDIA_BASE_URL']
                media_base_url = 'http://getscoop.com/'

                # get gramedia product instance to test
                gramedia_products = build_gramedia_product_from_scoop_offer(
                    gramedia_gateway=gramedia_gateway,
                    offer=offer,
                    media_base_url=media_base_url)

                # gramedia_products = GramediaProductEntity(gramedia_gateway)
                # get actual objecs -- end

                net_price = get_discounted_price(offer, offer.price_idr, session)
                self.assertEqual(gramedia_products.price, str(net_price))
                self.assertEqual(gramedia_products.name, item.name)
                self.assertEqual(gramedia_products.sku, "SC00P" + str(offer.id))
                self.assertEqual(gramedia_products.vpn, "SC00P" + str(offer.id))
                self.assertEqual(gramedia_products.isbn, item.gtin14)
                self.assertEqual(gramedia_products.country_of_manufacture, item.countries[0])
                self.assertEqual(gramedia_products.author, expected_author_name)
                self.assertEqual(gramedia_products.publisher, expected_publisher)
                self.assertEqual(gramedia_products.supplier, "SCOOP")
                self.assertEqual(gramedia_products.publish_date, item.release_date.strftime('%Y-%m-%d %H:%M:%S'))
                self.assertEqual(gramedia_products.width, 0)
                self.assertEqual(gramedia_products.length, 0)
                self.assertEqual(gramedia_products.image, u"{base}{img}".format(
                    base=media_base_url,
                    img=item.image_normal))
                self.assertEqual(gramedia_products.url_path_category, expected_remote_category.remote_id)
                self.assertEqual(gramedia_products.short_description, offer.long_name)
                self.assertEqual(gramedia_products.description, expected_description)

    def test_get_general_discount_for_gramedia(self):
        session = db.session()
        session.add(self.offer_harper)
        discount_harper = get_general_discount_for_gramedia(session, self.offer_harper)
        self.assertEquals(discount_harper.id, HARPER_COLLINS_BOOK_DISCOUNT_ID)

        session.add(self.offer_valid_1)
        discount = get_general_discount_for_gramedia(session, self.offer_valid_1)
        self.assertEquals(discount.id, BOOK_GENERAL_DISCOUNT_ID)

    def test_get_discounted_price(self):
        # harper collins discount
        session = db.session()
        session.add(self.discount_books)
        session.add(self.discount_harper_collins)
        session.add(self.offer_harper)
        actual_net_price_harper = get_discounted_price(self.offer_harper, self.offer_harper.price_idr, session)
        expected_net_price_harper = float(
            self.offer_harper.price_idr -
            (self.discount_harper_collins.discount_idr * self.offer_harper.price_idr / Decimal(100.0)))
        self.assertEquals(actual_net_price_harper, expected_net_price_harper)

        session.add(self.offer_valid_1)
        actual_net_price = get_discounted_price(self.offer_valid_1, self.offer_valid_1.price_idr, session)
        expected_net_price = float(self.offer_valid_1.price_idr -
                                   (self.discount_books.discount_idr * self.offer_valid_1.price_idr / Decimal(100.0)))
        self.assertEquals(actual_net_price, expected_net_price)

    def test_get_item(self):
        session = db.session()
        session.add(self.offer_valid_1)
        offer = session.query(Offer).get(self.offer_valid_1.id)
        offer_invalid_item = session.query(Offer).get(self.offer_id_with_invalid_item)
        actual_item = get_item(offer)
        self.assertIsNotNone(actual_item)
        self.assertRaises(NoAvailableItemsForOffer, get_item, offer_invalid_item)

    def test_get_formatted_description(self):

        description = u'kfjslkfjalsk –fsafsdfl \nklfj lafjdlkf asklf "doublequote1" “dq2”'

        expected = u'kfjslkfjalsk -fsafsdfl <br/>klfj lafjdlkf asklf "doublequote1" "dq2"'

        actual = get_formatted_description(description)

        self.assertEqual(expected, actual)

    def test_get_remote_category(self):

        session = db.session()
        item = session.query(Item).get(self.item_valid_1.id)
        remote_category = get_remote_category(item)

        self.assertIsNotNone(remote_category)
        self.assertEqual(remote_category.remote_id, self.remote_category.remote_id)

    def test_get_remote_category_id(self):
        session = db.session()
        item = session.query(Item).get(2)
        item.parentalcontrol_id = 1
        item.item_type = 'magazine'
        session.commit()
        remote_category_id = get_remote_category_id(item)
        # if its not in parental control, return it's original remote_id
        self.assertEqual(remote_category_id, self.remote_category.remote_id)

        # change item to parental control 2
        item.parentalcontrol_id = 2
        item.item_type = 'magazine'
        session.commit()
        remote_category_parental = get_remote_category_id(item)
        self.assertEqual(remote_category_parental, 'categories/lifestyle/magazine/adult-magazine.html')

    def test_get_remote_category_not_mapped(self):
        session = db.session()
        item = session.query(Item).get(3)

        self.assertRaises(NoMappableCategoriesError, get_remote_category, item)

    def test_record_offer_published_to_gramedia(self):
        session = db.session()
        offer = OfferFactory(id=999, name='test offer', is_active=False, offer_type=self.offer_type_single)
        remote_id = "SC00P" + str(offer.id)
        record_offer_published_to_gramedia(
                        session=session,
                        scoop_offer=offer,
                        remote_id=remote_id,
                        log=None,
                    )

        k = session.query(RemoteOffer).filter(
            RemoteOffer.offer_id == offer.id,
            RemoteOffer.remote_service == GRAMEDIA,
            RemoteOffer.remote_id == remote_id
        )
        self.assertIsNotNone(k)

    @nottest
    def test_publish_offers(self):
        """ This test depend on remote service

        :return:
        """

        # prepare gramedia gateway
        url_host = 'http://dev.gramedia.com/api/?wsdl'
        gramedia_gateway = GramediaGateway(
            url_host=url_host,
            user='tomo',
            password='123456',
            is_mock_mode=True)

        media_base_url = 'https://images.apps-foundry.com/magazine_static/'

        max_records = 100

        actual = publish_offer(
            gramedia_gateway=gramedia_gateway,
            max_records=max_records,
            media_base_url=media_base_url,
            log=None)

        self.assertIsNotNone(actual)
        self.assertEqual(actual.get("total_count"), actual.get("success_count"))
        self.assertEqual(actual.get("failed_count"), 0)
