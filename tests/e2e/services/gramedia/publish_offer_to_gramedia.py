from unittest import TestCase

from nose.tools import istest, nottest

from app.services.gramedia.gramedia_gateway import GramediaGateway
from app.services.gramedia.helpers import publish_offer


@nottest
class PublishOfferToGramediaTests(TestCase):

    @nottest
    def test_publish_offers(self):
        """
        This is to test sending Offer data to Gramedia.com dev server.
          this method is run manually / on demand.

        make sure to configure the url host + user + pwd correctly and gramedia.com dev server is ready
        """

        # prepare gramedia gateway
        url_host = 'http://dev.gramedia.com/api/?wsdl'
        gramedia_gateway = GramediaGateway(
            url_host=url_host,
            user='tomo',
            password='123456',
            is_mock_mode=False)

        media_base_url = 'https://images.apps-foundry.com/magazine_static/'

        max_records = 1

        actual = publish_offer(
            gramedia_gateway=gramedia_gateway,
            max_records=max_records,
            media_base_url=media_base_url,
            log=None,
            resend=True
        )
        self.assertIsNotNone(actual)
        self.assertEqual(actual.get("total_count"), actual.get("success_count"))
        self.assertEqual(actual.get("failed_count"), 0)
