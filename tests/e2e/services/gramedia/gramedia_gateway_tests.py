import re
import json
from nose.tools import istest, nottest
from unittest import TestCase, SkipTest

from app import app, db
from app.items.choices import STATUS_TYPES, STATUS_READY_FOR_CONSUME
from app.parental_controls.models import ParentalControl
from app.remote_publishing.choices import GRAMEDIA
from app.services.elevenia.helpers import OFFER_READY_FOR_SALE
from app.services.gramedia.gramedia_gateway import GramediaGateway, GramediaProductEntity
from app.services.gramedia.helpers import get_single_offers, build_gramedia_product_from_scoop_offer

from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.master import CategoryFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, RemoteOfferFactory, RemoteCategoryFactory, OfferTypeFactory


@istest
class GramediaProductEntityTests(TestCase):

    db = db
    MaxDiff = None

    @classmethod
    def setUpClass(cls):

        # make sure the test mode flag is set, or bail out
        if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
            raise SkipTest("COR is not in testing mode.  Cannot continue.")

        cls.db.drop_all()
        cls.db.create_all()

        cls.create_offer_data()

    @classmethod
    def tearDownClass(cls):
        cls.session.rollback()
        cls.db.session.rollback()
        cls.db.drop_all()

    @classmethod
    def create_offer_data(cls):
        cls.session = db.session()
        cls.session.expire_on_commit = False

        parental_control1 = ParentalControl(id=1, name='abc', is_active=True, sort_priority=1)
        parental_control2 = ParentalControl(id=2, name='+17', is_active=True, sort_priority=1)
        cls.session.add(parental_control1)
        cls.session.add(parental_control2)

        # offer_type_single = cls.session.query(OfferType).filter(OfferType.id == 1).first()
        # offer_type_subscription = cls.session.query(OfferType).filter(OfferType.id == 2).first()
        cls.offer_type_single = OfferTypeFactory(id=1, name='single offer')
        cls.offer_type_subscription = OfferTypeFactory(id=2, name='subscription offer')

        category = CategoryFactory(id=1, name='Magazine abc')
        cls.remote_category = RemoteCategoryFactory(
            category_id=category.id,
            remote_service=GRAMEDIA,
            remote_id='categories/books.html')
        category_not_mapped = CategoryFactory(id=2, name='Magazine abc not yet mapped')

        item_valid2 = ItemFactory(
            id=2,
            name='Magazine test 2',
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            item_type='magazine',
            is_active=True,
            content_type='pdf',
        )
        cls.session.commit()
        item_valid2.categories.append(category_not_mapped)

        offer_type_2 = OfferFactory(
            id=2,
            name='test offer magazine 2',
            offer_type=cls.offer_type_subscription,
            offer_status=OFFER_READY_FOR_SALE
        )

        # offer_type_2, wrong offer type, correct item, -- should not selected
        offer_type_2.items.append(item_valid2)

        item3 = ItemFactory(
            id=3,
            name='item newspaper 3',
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            item_type='newspaper',
            is_active=True,
            content_type='pdf'
        )
        cls.session.commit()
        offer3 = OfferFactory(
            id=123,
            name='test offer newspaper 3',
            offer_type=cls.offer_type_single,
            offer_status=OFFER_READY_FOR_SALE,
            is_free=False,
        )
        # offer 3, correct offer, wrong item, -- should not selected
        offer3.items.append(item3)

        item4 = ItemFactory(
            id=4,
            name='item magazine 4',
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            item_type='magazine',
            is_active=True,
            content_type='pdf'
        )
        cls.session.commit()
        offer4 = OfferFactory(
            id=124,
            name='test offer magazine 4',
            offer_type=cls.offer_type_single,
            offer_status=OFFER_READY_FOR_SALE,
            is_free=False,
        )

        remote_offer4 = RemoteOfferFactory(offer_id=offer4.id, remote_service=GRAMEDIA, remote_id='SC00P'+str(offer4.id))

        # offer 4, correct offer, correct item, already published, -- should not selected
        offer4.items.append(item4)

        cls.item_valid_1 = ItemFactory(
            id=91097,
            name='God in the Dock',
            item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME],
            item_type=u'book',
            is_active=True,
            content_type=u'pdf',
            image_normal='images/1/28711/general_big_covers/ID_HCO2015MTH11GITD.jpeg',
            countries=['ID', ],
            parentalcontrol_id=1,
        )
        cls.session.commit()
        cls.item_valid_1.categories.append(category)

        offer_valid_1 = OfferFactory(
            id=50385,
            name='test offer 1',
            long_name='God in the Dock long name',
            offer_type=cls.offer_type_single,
            offer_status=OFFER_READY_FOR_SALE,
            is_free=False,
            is_active=True,
        )
        # offer 1, correct offer, correct item, -- should selected
        offer_valid_1.items.append(cls.item_valid_1)
        cls.session.add(cls.item_valid_1)
        cls.session.add(offer_valid_1)
        cls.session.commit()

    def test_get_json_data(self):

        self.prepare_gramedia_products(is_mock_mode=True)
        data = self.gramedia_products.__dict__.copy()
        data.pop('gateway')
        data_expected = json.dumps({"items": [data, ]})

        actual = self.gramedia_products.get_json_data()

        self.assertEqual(data_expected, actual)

    def test_send_offer_to_gramedia_in_mock_mode(self):
        self.prepare_gramedia_products(is_mock_mode=True)

        # send product/offer to gramedia.com
        actual = self.gramedia_products.send_to_gramedia()

        self.assertIsNone(actual)

    @nottest
    def test_send_offer_to_gramedia_dev_server(self):
        # run this test on demand
        # make sure to configure the url host + user + pwd correctly and gramed.com dev server is ready
        self.prepare_gramedia_products(is_mock_mode=False)

        # send product/offer to gramedia.com
        actual = self.gramedia_products.send_to_gramedia()

        self.assertIsNotNone(actual)

    @nottest
    def prepare_gramedia_products(self, is_mock_mode=True):
        with app.app_context():
            session = db.session()

            offers = get_single_offers(session, 1)

            self.assertIsNotNone(offers)

            for offer in offers:

                # prepare gramedia gateway
                url_host = 'http://dev.gramedia.com/api/?wsdl'
                gramedia_gateway = GramediaGateway(
                    url_host=url_host,
                    user='tomo',
                    password='123456',
                    is_mock_mode=is_mock_mode)

                # media_base_url = app.config['MEDIA_BASE_URL']
                media_base_url = 'https://images.apps-foundry.com/magazine_static/'
                # https://images.apps-foundry.com/magazine_static/images/1/631/general_big_covers/ID_SERID2015MTH04DT23_B.jpg

                # get gramedia product instance to test
                self.gramedia_products = build_gramedia_product_from_scoop_offer(
                    gramedia_gateway=gramedia_gateway,
                    offer=offer,
                    media_base_url=media_base_url)

