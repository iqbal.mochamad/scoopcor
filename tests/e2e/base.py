from collections import namedtuple

from app import app, db
from app.constants import RoleType

from tests.fixtures.helpers import generate_static_sql_fixtures, create_user_and_token, bearer_string


class DbTestMixin(object):
    """ Mixin for tests that need to automatically build and drop a database.
    """
    @property
    def session(self):
        return db.session()

    def setUp(self):
        db.drop_all()
        db.create_all()
        generate_static_sql_fixtures(self.session)
        self.api_client = app.test_client(use_cookies=False)

    def tearDown(self):
        self.session.close_all()
        db.drop_all()


SuperuserMeta = namedtuple("SuperuserMeta", ['user', 'token', 'bearer'])


class SuMixin(object):
    """ Mixin for any tests that need to authenticate as a superuser
    The superuser is assigned to self.su and is detatched from the session.
    .. note::
        This should only be for Version 2 endpoint tests.. Version 3
        should always perform actions with the User's authorization,
        not with an 'App Token'.
    """
    def setUp(self):
        super(SuMixin, self).setUp()
        # create superuser
        user, token = create_user_and_token(self.session, RoleType.super_admin)
        self.su = SuperuserMeta(user=user, token=token, bearer=bearer_string(user, token))
