import json
from httplib import OK, CREATED
from unittest import TestCase

from datetime import timedelta
from flask import g
from nose.tools import nottest

from app import db, app, set_request_id
from app.auth.models import UserInfo
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.orders.choices import COMPLETE
from app.payments.choices.gateways import FREE, POINT
from app.payments.signature_generator import GenerateSignature
from app.users.users import User
from app.payments.choices import CHARGED
from tests.fixtures.sqlalchemy import ItemFactory
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.master import CurrencyFactory, BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferTypeFactory
from tests.fixtures.sqlalchemy.payments.gateways import PaymentGatewayFactory


class OrderFreeTests(TestCase):

    maxDiff = None
    original_before_request_funcs = app.before_request_funcs

    @classmethod
    def setUpClass(cls):
        db.session().close_all()
        db.drop_all()
        db.create_all()

        cls.init_data()

        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }

    @classmethod
    def init_data(cls):
        s = db.session()
        s.expire_on_commit = False

        cur_idr = CurrencyFactory(id=2, iso4217_code='IDR')
        cur_pts = CurrencyFactory(id=3, iso4217_code='PTS')
        pg_free = PaymentGatewayFactory(id=FREE, base_currency=cur_idr, payment_flow_type=CHARGED,
                                        minimal_amount=0)
        pg_point = PaymentGatewayFactory(id=POINT, base_currency=cur_pts, payment_flow_type=CHARGED,
                                         minimal_amount=1)

        offer_type = OfferTypeFactory(id=1)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=True, price_idr=0,
                                  price_usd=10, price_point=10)
        brand = BrandFactory()
        item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)
        cls.user = UserFactory(id=12345)
        s.commit()

    @classmethod
    def tearDownClass(cls):
        app.before_request_funcs = cls.original_before_request_funcs
        db.session().close_all()
        db.drop_all()

    def test_order_payment_with_free_payment_gateway(self):
        session = db.session()
        session.add(self.user)
        session.add(self.offer1)

        header_data = {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'Content-Type': 'application/json',
        }
        request_body = self.get_request_body_checkout()
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/checkout',
                                 data=json.dumps(request_body),
                                 headers=header_data)
        self.assertEqual(response.status_code, CREATED)
        if response.status_code == CREATED:
            response_checkout = json.loads(response.data)
            self.assertIsNotNone(response_checkout.get('order_number', None))
            self.assertIsNotNone(response_checkout.get('temp_order_id', None))
            request_body = self.get_request_body_confirm(response_checkout)
            response = client.post('/v1/orders/confirm',
                                   data=json.dumps(request_body),
                                   headers=header_data)
            self.assertEqual(response.status_code, CREATED)
            if response.status_code == CREATED:
                response_confirm = json.loads(response.data)
                order_id = response_confirm.get('order_id', None)
                self.assertIsNotNone(order_id)
                request_body = self.get_request_body_payment(response_confirm)
                response = client.post('/v1/payments/charge',
                                       data=json.dumps(request_body),
                                       headers=header_data)
                self.assertEqual(response.status_code, OK)
                if response.status_code == OK:
                    response_charge = json.loads(response.data)
                    self.assertIsNotNone(response_charge)
                    self.assertEqual(order_id, response_charge.get('order_id', None))
                    self.assertEqual(COMPLETE, response_charge.get('order_status', None))

    @staticmethod
    def get_request_body_payment(response_confirm):
        return {
            "order_id": response_confirm.get('order_id', None),
            "signature": GenerateSignature(order_id=response_confirm.get('order_id', None)).construct(),
            "user_id": response_confirm.get('user_id', None),
            "payment_gateway_id": response_confirm.get('payment_gateway_id', None)
        }

    @staticmethod
    def get_request_body_confirm(response_checkout):
        return {
            "user_id": response_checkout.get('user_id', None),
            "payment_gateway_id": response_checkout.get('payment_gateway_id', None),
            "client_id": response_checkout.get('client_id', None),
            "currency_code": response_checkout.get('currency_code', None),
            "final_amount": response_checkout.get('final_amount', None),
            "order_number": response_checkout.get('order_number', None),
            "temp_order_id": response_checkout.get('temp_order_id', None),
        }

    def get_request_body_checkout(self):
        return {
            "user_id": self.user.id,
            "payment_gateway_id": FREE,
            "client_id": 7,
            "platform_id": 4,
            "order_lines": [
                {
                    "offer_id": self.offer1.id,
                    "quantity": 1,
                    "name": self.offer1.name
                },
            ],
            "user_email": "ahmad.syafrudin@apps-foundry.com",
            "user_name": "",
            "user_street_address": "",
            "user_city": "Jakarta",
            "user_zipcode": "",
            "user_state": "",
            "user_country": "Indonesia",
            "latitude": -6.1744,
            "longitude": 106.8294,
            "ip_address": "202.179.188.106",
            "os_version": "Mac OS X",
            "device_model": "Chrome 51.0.2704.84",
            "client_version": "3.10.1"
        }

    @staticmethod
    def expected_checkout_response_example():
        return {
            "payment_gateway_name": "Free Purchase",
            "user_id": 11,
            "total_amount": "0.00",
            "partner_id": None,
            "is_active": True,
            "final_amount": "0.00",
            "payment_gateway_id": 3,
            "temp_order_id": 1,
            "client_id": 7,
            "order_status": 10000,
            "order_number": 78171068314826,
            "currency_code": "IDR",
            "order_lines": [
                {
                    "name": "streamline ubiquitous metrics",
                    "items": [
                        {
                            "is_featured": False,
                            "vendor": {
                                "slug": "amet-unde-odio",
                                "id": 1,
                                "name": "strategize cross-media e-commerce"
                            },
                            "name": "re-intermediate turn-key users",
                            "countries": [
                                {
                                    "name": "Indonesia",
                                    "id": 1
                                }
                            ],
                            "release_date": "1990-05-03T20:20:39",
                            "item_status": 9,
                            "languages": [
                                {
                                    "id": 1,
                                    "name": "English"
                                }
                            ],
                            "authors": [],
                            "id": 1,
                            "edition_code": "provident-repellat"
                        }
                    ],
                    "offer_id": 1,
                    "is_free": True,
                    "raw_price": "0.00",
                    "currency_code": "IDR",
                    "final_price": "0.00"
                }
            ]
        }


def init_g_current_user():
    g.user = UserInfo(username='dfdsafsdf', user_id=12345, token='abcd', perm=['can_read_write_global_all',])
    session = db.session()
    g.current_user = session.query(User).get(12345)
    session.close()
