import json
from httplib import OK, CREATED
from unittest import TestCase

from datetime import timedelta, datetime
from flask import g
from nose.tools import nottest

from app import db, app, set_request_id
from app.auth.models import UserInfo
from app.items.choices import STATUS_READY_FOR_CONSUME, STATUS_TYPES
from app.payments.choices.gateways import FREE, POINT, INDOMARET
from app.payments.signature_generator import GenerateSignature
from app.users.users import User
from app.payments.choices import CHARGED
from tests.fixtures.sqlalchemy import ItemFactory
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.master import CurrencyFactory, BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferTypeFactory
from tests.fixtures.sqlalchemy.payments.gateways import PaymentGatewayFactory


class OrderIndomaretPaymentTests(TestCase):
    """End to End test from Order Checkout until Payment with Indomaret (Veritrans)

    make sure app.config['VERITRANS_V2_SERVER_KEY'] contain key from vertirans sandbox
    """

    original_before_req_funcs = None
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        db.drop_all()
        db.create_all()

        cls.init_data()

        # set last sequence id for Order to unique number
        # to prevent error 406 already exists from veritrans
        last_order_id = int((datetime.now() - datetime(1970, 1, 1)).total_seconds())
        db.engine.execute("select setval('core_orders_id_seq', {}, true)".format(last_order_id))
        db.engine.execute("select setval('core_payments_id_seq', {}, true)".format(last_order_id))

        cls.original_before_req_funcs = app.before_request_funcs

        app.before_request_funcs = {
            None: [
                set_request_id,
                init_g_current_user,
            ]
        }

    @classmethod
    def init_data(cls):
        s = db.session()
        s.expire_on_commit = False

        cur_idr = CurrencyFactory(id=2, iso4217_code='IDR')
        cur_pts = CurrencyFactory(id=3, iso4217_code='PTS')
        pg_indomaret = PaymentGatewayFactory(id=INDOMARET, base_currency=cur_idr, payment_flow_type=CHARGED,
                                        minimal_amount=1)
        pg_point = PaymentGatewayFactory(id=POINT, base_currency=cur_pts, payment_flow_type=CHARGED,
                                         minimal_amount=1)

        offer_type = OfferTypeFactory(id=1)
        cls.offer1 = OfferFactory(id=1, offer_type=offer_type, is_free=False, price_idr=10000, price_usd=10,
                                  price_point=10)
        brand = BrandFactory()
        item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand)
        cls.offer1.items.append(item1)
        cls.user = UserFactory(id=USER_ID, username='cortesting@apps-foundry.com', email='cortesting@apps-foundry.com')
        s.commit()

    @classmethod
    def tearDownClass(cls):
        app.before_request_funcs = cls.original_before_req_funcs
        db.session().close_all()
        db.drop_all()

    def test_order_indomaret_payment(self):
        session = db.session()
        session.add(self.user)
        session.add(self.offer1)

        header_data = {
            'Accept': 'application/json',
            'Accept-Charset': 'utf-8',
            'Content-Type': 'application/json',
        }
        request_body = self.get_request_body_checkout()
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/checkout',
                                 data=json.dumps(request_body),
                                 headers=header_data)
        self.assertEqual(response.status_code, CREATED)
        if response.status_code == CREATED:
            response_checkout = json.loads(response.data)
            self.assertIsNotNone(response_checkout.get('order_number', None))
            self.assertIsNotNone(response_checkout.get('temp_order_id', None))
            request_body = self.get_request_body_confirm(response_checkout)
            response = client.post('/v1/orders/confirm',
                                   data=json.dumps(request_body),
                                   headers=header_data)
            self.assertEqual(response.status_code, CREATED)
            if response.status_code == CREATED:
                response_confirm = json.loads(response.data)
                order_id = response_confirm.get('order_id', None)
                self.assertIsNotNone(order_id)
                request_body = self.get_request_body_payment(response_confirm)
                response = client.post('/v1/payments/authorize',
                                       data=json.dumps(request_body),
                                       headers=header_data)
                self.assertEqual(response.status_code, OK)
                if response.status_code == OK:
                    response_charge = json.loads(response.data)
                    self.assertIsNotNone(response_charge)
                    self.assertEqual(order_id, response_charge.get('order_id', None))
                    self.assertIsNotNone(response_charge.get('payment_code', None))

    @staticmethod
    def get_request_body_payment(response_confirm):
        return {
            "order_id": response_confirm.get('order_id', None),
            "signature": GenerateSignature(order_id=response_confirm.get('order_id', None)).construct(),
            "user_id": response_confirm.get('user_id', None),
            "payment_gateway_id": response_confirm.get('payment_gateway_id', None)
        }

    @staticmethod
    def get_request_body_confirm(response_checkout):
        return {
            "user_id": response_checkout.get('user_id', None),
            "payment_gateway_id": response_checkout.get('payment_gateway_id', None),
            "client_id": response_checkout.get('client_id', None),
            "currency_code": response_checkout.get('currency_code', None),
            "final_amount": response_checkout.get('final_amount', None),
            "order_number": response_checkout.get('order_number', None),
            "temp_order_id": response_checkout.get('temp_order_id', None),
        }

    def get_request_body_checkout(self):
        return {
            "user_id": self.user.id,
            "payment_gateway_id": INDOMARET,
            "client_id": 7,
            "platform_id": 4,
            "order_lines": [
                {
                    "offer_id": self.offer1.id,
                    "quantity": 1,
                    "name": self.offer1.name
                },
            ],
            "user_email": "ahmad.syafrudin@apps-foundry.com",
            "user_name": "",
            "user_street_address": "",
            "user_city": "Jakarta",
            "user_zipcode": "",
            "user_state": "",
            "user_country": "Indonesia",
            "latitude": -6.1744,
            "longitude": 106.8294,
            "ip_address": "202.179.188.106",
            "os_version": "Mac OS X",
            "device_model": "Chrome 51.0.2704.84",
            "client_version": "3.10.1"
        }


USER_ID = 12345


def init_g_current_user():
    session = db.session()
    g.current_user = session.query(User).get(USER_ID)
    g.user = UserInfo(username=g.current_user.username, user_id=USER_ID, token='abcd', perm=['can_read_write_global_all',])
    session.close()

