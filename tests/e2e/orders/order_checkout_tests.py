from __future__ import unicode_literals, absolute_import

import json
from unittest import TestCase, SkipTest

from httplib import CREATED, BAD_REQUEST

from datetime import datetime, timedelta
from nose.tools import istest
from flask import g

from app import app, db, set_request_id
from app.items.choices import STATUS_TYPES, STATUS_READY_FOR_CONSUME
from app.master.choices import IOS, GETSCOOP
from app.offers.choices.offer_type import SINGLE, BUFFET
from app.offers.models import Offer
from app.orders.models import OrderTemp
from app.payments.choices import CHARGED
from app.users.users import User
from app.auth.models import UserInfo
from tests.fixtures.sqlalchemy import ItemFactory
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.master import BrandFactory, CurrencyFactory, PlatformFactory
from tests.fixtures.sqlalchemy.offers import OfferTypeFactory, OfferFactory, OfferPlatformFactory, OfferBuffetFactory
from tests.fixtures.sqlalchemy.orders import PaymentGatewayFactory


@istest
class OrderCheckoutOfferSingleTests(TestCase):
    db = db
    maxDiff = None

    def test_post_order_IOS(self):
        session = db.session()
        offer = session.query(Offer).get(SINGLE)
        json_body = get_request_body(offer, payment_gateway_id=25, platform_id=IOS)
        session.close()
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/checkout',
                               data=json.dumps(json_body),
                               headers={
                                   'Accept': 'application/json',
                                   'Accept-Charset': 'utf-8',
                                   'Content-Type': 'application/json',
                               })

        self.assertEqual(response.status_code, CREATED)

    def test_post_order_IOS_quantity_0(self):
        session = db.session()
        offer = session.query(Offer).get(SINGLE)
        json_body = get_request_body(offer, payment_gateway_id=25, platform_id=IOS)
        session.close()
        lines = json_body.get('order_lines')
        lines[0]['quantity'] = 0
        json_body['order_lines'] = lines
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/checkout',
                               data=json.dumps(json_body),
                               headers={
                                   'Accept': 'application/json',
                                   'Accept-Charset': 'utf-8',
                                   'Content-Type': 'application/json',
                               })

        self.assertEqual(response.status_code, BAD_REQUEST,
                         'Response status code is not {}. {}'.format(BAD_REQUEST, response.data))

    def test_post_order_web_getscoop(self):
        session = db.session()
        offer = session.query(Offer).get(SINGLE)
        json_body = get_request_body(offer, payment_gateway_id=25, platform_id=GETSCOOP)
        session.close()
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/checkout',
                               data=json.dumps(json_body),
                               headers={
                                   'Accept': 'application/json',
                                   'Accept-Charset': 'utf-8',
                                   'Content-Type': 'application/json',
                               })

        self.assertEqual(response.status_code, CREATED)

    def test_post_order_web_getscoop_with_quantity(self):
        session = db.session()
        offer = session.query(Offer).get(BUFFET)
        json_body = get_request_body(offer, payment_gateway_id=25, platform_id=GETSCOOP)
        lines = json_body.get('order_lines')
        quantity = 2
        lines[0]['quantity'] = quantity
        json_body['order_lines'] = lines
        session.close()
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/checkout',
                               data=json.dumps(json_body),
                               headers={
                                   'Accept': 'application/json',
                                   'Accept-Charset': 'utf-8',
                                   'Content-Type': 'application/json',
                               })

        self.assertEqual(response.status_code, CREATED,
                         'Response status code is not 201. {}'.format(response.data))
        result = json.loads(response.data)
        order = OrderTemp.query.filter_by(id=result.get('temp_order_id', 0)).first()
        self.assertEqual(order.price_idr, offer.price_idr * quantity)
        self.assertEqual(order.final_price_idr, offer.price_idr * quantity)

    def test_post_order_web_getscoop_with_quantity_0(self):
        session = db.session()
        offer = session.query(Offer).get(BUFFET)
        json_body = get_request_body(offer, payment_gateway_id=25, platform_id=GETSCOOP)
        lines = json_body.get('order_lines')
        lines[0]['quantity'] = 0
        json_body['order_lines'] = lines
        session.close()
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/checkout',
                               data=json.dumps(json_body),
                               headers={
                                   'Accept': 'application/json',
                                   'Accept-Charset': 'utf-8',
                                   'Content-Type': 'application/json',
                               })

        self.assertEqual(response.status_code, BAD_REQUEST,
                         'Response status code is not {}. {}'.format(BAD_REQUEST, response.data))


class OrderCheckoutBuffetPremiumTests(TestCase):
    db = db
    maxDiff = None

    def test_post_order_IOS(self):

        session = db.session()
        offer = session.query(Offer).get(BUFFET)
        json_body = get_request_body(offer, payment_gateway_id=25, platform_id=IOS)
        session.close()
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/checkout',
                               data=json.dumps(json_body),
                               headers={
                                   'Accept': 'application/json',
                                   'Accept-Charset': 'utf-8',
                                   'Content-Type': 'application/json',
                               })

        self.assertEqual(response.status_code, CREATED,
                         'Response status code is not 201. {}'.format(response.data))

    def test_post_order_web_getscoop(self):
        session = db.session()
        offer = session.query(Offer).get(BUFFET)
        json_body = get_request_body(offer, payment_gateway_id=25, platform_id=GETSCOOP)
        session.close()
        client = app.test_client(use_cookies=False)
        response = client.post('/v1/orders/checkout',
                               data=json.dumps(json_body),
                               headers={
                                   'Accept': 'application/json',
                                   'Accept-Charset': 'utf-8',
                                   'Content-Type': 'application/json',
                               })

        self.assertEqual(response.status_code, CREATED,
                         'Response status code is not 201. {}'.format(response.data))


def get_request_body(offer, payment_gateway_id, platform_id):
    return {
        "os_version": "9.3",
        "order_lines": [
            {
                "offer_id": offer.id,
                "name": offer.long_name,
                "localized_currency_code": "IDR",
                "localized_final_price": str(offer.price_idr)
            }
        ],
        "longitude": "106.8294",
        "user_country": "Indonesia",
        "latitude": "-6.1744",
        "ip_address": "120.164.44.125",
        "device_model": "x86_64",
        "client_version": "4.7.1.0",
        "client_id": "1",
        "user_id": SUPER_USER_ID,
        "user_city": "Jakarta",
        "platform_id": platform_id,
        "payment_gateway_id": payment_gateway_id
    }


def create_init_data():
    session = db.session()

    cur_idr = CurrencyFactory(id=2, iso4217_code='IDR')
    cur_pts = CurrencyFactory(id=3, iso4217_code='PTS')
    pg_direct = PaymentGatewayFactory(id=25, base_currency=cur_idr, payment_flow_type=CHARGED,
                                      minimal_amount=0.001)

    offer_type_single = OfferTypeFactory(id=SINGLE)
    offer_type_buffet = OfferTypeFactory(id=BUFFET)
    offer_single = OfferFactory(id=SINGLE, offer_type=offer_type_single, is_free=False,
                                price_idr=10000, price_usd=10, price_point=10)
    offer_buffet_premium = OfferFactory(id=BUFFET, offer_type=offer_type_buffet, is_free=False,
                                        price_idr=10000, price_usd=10, price_point=10)
    platform_ios = PlatformFactory(id=IOS)
    platform_web = PlatformFactory(id=GETSCOOP)
    offer_single_platform_ios = OfferPlatformFactory(offer=offer_single, platform=platform_ios, price_idr=1000,
                                                     discount_price_usd=1000)
    offer_buffer_platform_ios = OfferPlatformFactory(offer=offer_buffet_premium,
                                                     platform=platform_ios, price_idr=1000, discount_price_usd=1000)
    brand = BrandFactory()
    item1 = ItemFactory(id=1, item_status=STATUS_TYPES[STATUS_READY_FOR_CONSUME], brand=brand,
                        languages=['ind'],
                        countries=['id'])
    offer_single.items.append(item1)
    offer_buffet_premium.brands.append(brand)
    offer_buffet = OfferBuffetFactory(offer=offer_buffet_premium,
                                      available_from=datetime.now(),
                                      available_until=datetime.now() + timedelta(days=2))
    user = UserFactory(id=SUPER_USER_ID)
    session.commit()


original_before_request_funcs = None


def setup_module():
    # make sure the test mode flag is set, or bail out
    if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
        raise SkipTest("COR is not in testing mode.  Cannot continue.")

    global original_before_request_funcs
    original_before_request_funcs = app.before_request_funcs

    db.drop_all()
    db.create_all()

    app.before_request_funcs = {
        None: [
            set_request_id,
            init_g_current_user,
        ]
    }

    create_init_data()


def teardown_module():
    app.before_request_funcs = original_before_request_funcs
    db.session().close_all()
    db.drop_all()


SUPER_USER_ID = 430450


def init_g_current_user():
    session = db.session()
    g.current_user = session.query(User).get(SUPER_USER_ID)
    g.user = UserInfo(username=g.current_user.username, user_id=SUPER_USER_ID, token='abcd',
                      perm=['can_read_write_global_all', ])
    session.close()

