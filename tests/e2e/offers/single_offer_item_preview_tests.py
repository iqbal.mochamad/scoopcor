from __future__ import unicode_literals
import json

from faker import Factory
from nose.tools import istest, nottest

from app import db, app
from app.items.previews import PreviewUrlBuilder
from app.offers.choices.offer_type import SINGLE
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory, OfferTypeFactory
from tests import testcases


@istest
class GetDetailTest(testcases.DetailTestCase):

    request_url = '/v1/offers/10/previews'
    fixture_factory = OfferFactory
    maxDiff = None

    @classmethod
    def set_up_fixtures(cls):
        session = db.session()
        item = ItemFactory(id=1, name='item_name1', page_count=100, item_type='book')
        offer_type = OfferTypeFactory(id=SINGLE)
        offer = OfferFactory(id=10, offer_type=offer_type)
        offer.items.append(item)
        session.commit()
        return offer

    def test_response_body(self):
        actual = json.loads(self.response.data)
        session = db.session()
        session.add(self.fixture)

        preview_url_builder = PreviewUrlBuilder(app.config['BASE_URL'])
        previews = preview_url_builder.make_urls(self.fixture.items[0])

        expected = {
            'id': self.fixture.id,
            'item_name': self.fixture.items[0].name,
            'previews': previews
        }
        self.assertDictEqual(
            actual,
            expected
        )


_fake = Factory.create()


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()

