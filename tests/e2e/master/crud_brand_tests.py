from __future__ import unicode_literals

import json
from unittest import TestCase

import iso3166
import iso639
from faker import Factory
from nose.tools import istest, nottest

from app import db
from app.master.models import Brand, BrandLink
from app.parental_controls.models import ParentalControl
from app.utils.shims import countries, languages, item_types
from tests.fixtures.sqlalchemy import DistributionCountryGroupFactory, AuthorFactory
from tests.fixtures.sqlalchemy.master import VendorFactory, CategoryFactory, BrandFactory
from tests import testcases


class BaseBrandApiMixin(TestCase):

    @classmethod
    def setup_required_object(cls, session):
        cls.author = AuthorFactory(id=1, name="authortest")
        cls.category = CategoryFactory(id=1, name='categorytest')
        cls.dist_group = DistributionCountryGroupFactory(id=1, name='distcountry1')
        cls.vendor = VendorFactory()
        cls.parental_control = ParentalControl(id=1, name='parental control 1', sort_priority=1, is_active=True)
        session.add(cls.parental_control)

    @classmethod
    def clean_data(cls):
        session = db.session()
        for entity in session.query(BrandLink):
            session.delete(entity)
        for entity in session.query(Brand):
            session.delete(entity)
        session.delete(cls.author)
        session.delete(cls.category)
        session.delete(cls.dist_group)
        session.delete(cls.vendor)
        session.delete(cls.parental_control)
        session.commit()

    @classmethod
    def get_request_body(cls):

        return {
            'buffet_offers': [],
            "daily_release_quota": _fake.pyint(),
            "default_authors": [
                cls.author.id,
            ],
            "default_categories": [
                cls.category.id,
            ],
            "default_countries": [1, 2, 3],
            "default_distribution_groups": [
                cls.dist_group.id,
            ],
            "default_parental_control_id": cls.parental_control.id,
            "vendor_id": cls.vendor.id,
            "default_item_type_id": 1,
            "default_languages": [
                1,
            ],
            "description": _fake.bs(),
            "icon_image_highres": _fake.url(),
            "icon_image_normal": _fake.url(),
            "is_active": True,
            "links": [
                {
                    "link_type": 1,
                    "url": "www.facebook.com/lorem"
                },
                {
                    "link_type": 2,
                    "url": "www.twitter.com/lorem"
                }
            ]
            ,
            "meta": _fake.bs(),
            "name": _fake.bs(),
            "slug": _fake.slug(),
            "sort_priority": _fake.pyint(),
        }

    def get_expected_response(self, brand_id):
        s = db.session()
        brand = s.query(Brand).filter(Brand.id == brand_id).first()
        expected = {
            'id': brand.id,
            'buffet_offers': [],
            "daily_release_quota": brand.daily_release_quota,
            "default_item_type_id": item_types.enum_to_id(brand.default_item_type)
            if brand.default_item_type else None,
            "description": brand.description,
            "icon_image_highres": brand.icon_image_highres,
            "icon_image_normal": brand.icon_image_normal,
            "is_active": brand.is_active,
            "links": brand.get_links(),
            "meta": brand.meta,
            "name": brand.name,
            "slug": brand.slug,
            "sort_priority": brand.sort_priority,
            "vendor_id": brand.vendor_id,
            "default_languages": [
                {
                    'id': languages.iso639_to_id(lang_code),
                    'name': iso639.find(lang_code)['name']
                }
                for lang_code in brand.default_languages],
            "default_authors": [{'name': i.name, 'id': i.id }
                for i in brand.default_author ],
            "default_categories":[{'name': i.name, 'id': i.id }
                for i in brand.default_categories ],
            "default_countries": [
                {
                    'name': iso3166.countries.get(country_code).name,
                    'id': countries.iso3166_to_id(country_code),
                }
                for country_code in brand.default_countries],
            "default_distribution_groups":[{'name': i.name, 'id': i.id }
                for i in brand.default_distributions ],
            "default_parental_control_id": brand.default_parentalcontrol_id,
        }
        s.rollback()
        s.expunge(brand)

        return expected

    def reformat_expected_values(self, expected):
        expected['default_languages'] = [
            {
                "id": 1,
                "name": "English"
            },
        ]

        expected["default_countries"] = [
            {
                "id": 1,
                "name": "Indonesia"
            },
            {
                "id": 2,
                "name": "Malaysia"
            },
            {
                "id": 3,
                "name": "Singapore"
            }
        ]
        expected["default_distribution_groups"] = [
            {
                "id": 1,
                "name": "distcountry1"
            },
        ]
        expected["default_authors"] = [
            {
                "id": 1,
                "name": "authortest"
            },
        ]
        expected["default_categories"] = [
            {
                "id": 1,
                "name": "categorytest"
            },
        ]

        return expected


@istest
class CreateTests(testcases.CreateTestCase, BaseBrandApiMixin):

    request_url = '/v1/brands'
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        s = db.session()
        cls.setup_required_object(s)
        s.commit()

        # super(CreateTestCase, cls).setUpClass()
        cls.fixture = cls.fixtures[0] if cls.fixtures else None
        cls.response = cls.get_api_response()

    @classmethod
    def clean_database(cls):
        cls.clean_data()

    @classmethod
    def get_api_request_body(cls):
        return cls.get_request_body()

    def test_response_body(self):
        response_json = json.loads(self.response.data)

        if 'developer_message' in response_json:
            self.fail(msg=response_json['developer_message'])

        # we can't compare id, because id created in database, not send from request, so remove it
        response_json.pop('id')

        expected = self.request_body.copy()
        expected = self.reformat_expected_values(expected)

        self.assertDictEqual(response_json, expected)


@istest
class UpdateTests(testcases.UpdateTestCase, BaseBrandApiMixin):
    request_url = '/v1/brands/{0.id}'
    fixture_factory = BrandFactory
    maxDiff = None

    @classmethod
    def setUpClass(cls):

        super(UpdateTests, cls).setUpClass()
        # cls.fixture = cls.fixtures[0] if cls.fixtures else None
        # cls.response = cls.get_api_response()

    @classmethod
    def set_up_fixtures(cls):
        s = db.session()
        cls.setup_required_object(s)
        # cls.fixture_factory = BrandFactory()
        s.commit()
        return cls.fixture_factory()

    @classmethod
    def remove_fixtures_from_session(cls):
        session = db.session()
        session.commit()
        session.refresh(cls.author)
        session.refresh(cls.category)
        session.refresh(cls.dist_group)
        session.refresh(cls.vendor)
        session.refresh(cls.parental_control)
        for fixture in cls.fixtures:
            session.refresh(fixture)
        session.expunge_all()

    @classmethod
    def clean_database(cls):
        cls.clean_data()
        # if cls.fixture_factory is not None:
        #     session = db.session()
        #     for entity in session.query(cls.fixture_factory._get_model_class()):
        #         session.delete(entity)
        #     session.commit()

    @classmethod
    def get_api_request_body(cls):
        return cls.get_request_body()

    def test_response_body(self):
        """ Make sure the returned message meets our expectations.

        .. note::

            We have to do a heavy amount of data massaging because of some
            peculiarities of this API (posting base64 strings and receiving
            back filenames for images)
        """
        response_json = json.loads(self.response.data)

        if 'developer_message' in response_json:
            self.fail(msg=response_json['developer_message'])

        expected = self.request_body.copy()
        expected = self.reformat_expected_values(expected)
        expected['id'] = self.fixtures[0].id

        self.assertDictEqual(response_json, expected)


@istest
class DeleteTests(testcases.SoftDeleteTestCase):
    request_url = '/v1/brands/{0.id}'
    fixture_factory = BrandFactory


@istest
class GetSingleTests(testcases.DetailTestCase, BaseBrandApiMixin):
    request_url = '/v1/brands/{0.id}'
    fixture_factory = BrandFactory
    maxDiff = None

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = self.get_expected_response(brand_id=self.fixture.id)
        self.assertDictEqual(actual, expected)


@istest
class ListTests(testcases.ListTestCase, BaseBrandApiMixin):

    request_url = '/v1/brands'
    list_key = 'brands'
    fixture_factory = BrandFactory
    maxDiff = None

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        expected = self.get_expected_response(fixture.id)

        # in lists API, default_author is different then others API of brands (which use: default_authors)
        expected['default_author'] = expected.pop('default_authors')
        expected.pop('buffet_offers', None)

        fixture_json.pop('modified')
        self.assertDictEqual(fixture_json, expected)


_fake = Factory.create()


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()
