from __future__ import unicode_literals
import json

from faker import Factory
from nose.tools import istest, nottest

from app import db
from tests.fixtures.sqlalchemy.master import PlatformFactory
from tests import testcases


@istest
class CreateTest(testcases.CreateTestCase):

    request_url = '/v1/platforms'
    fixture_factory = PlatformFactory
    maxDiff = None

    @classmethod
    def get_api_request_body(cls):
        return {
            'name': _fake.name(),
            'description': _fake.bs(),
            'is_active': _fake.pybool(),
        }

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = self.request_body
        self.assertDictContainsSubset(
            expected=expected,
            actual=actual)


@istest
class UpdateTest(testcases.UpdateTestCase):

    request_url = '/v1/platforms/{0.id}'
    fixture_factory = PlatformFactory

    @classmethod
    def get_api_request_body(cls):
        return {
            'name': _fake.name(),
            'description': _fake.bs(),
            'is_active': _fake.pybool(),
        }

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = self.request_body
        self.assertDictContainsSubset(
            expected=expected,
            actual=actual)


@istest
class DeleteTest(testcases.SoftDeleteTestCase):

    request_url = '/v1/platforms/{0.id}'
    fixture_factory = PlatformFactory


@istest
class GetDetailTest(testcases.DetailTestCase):

    request_url = '/v1/platforms/{0.id}'
    fixture_factory = PlatformFactory
    maxDiff = None

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = {
            'id': self.fixture.id,
            'name': self.fixture.name,
            'description': self.fixture.description,
            'is_active': self.fixture.is_active,
        }
        self.assertDictEqual(
            actual,
            expected
        )


@istest
class ListTest(testcases.ListTestCase):

    request_url = '/v1/platforms'
    list_key = 'platforms'
    fixture_factory = PlatformFactory

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        expected = {
            'id': fixture.id,
            'name': fixture.name,
            'description': fixture.description,
            'is_active': fixture.is_active,
        }
        self.assertDictEqual(fixture_json, expected)


_fake = Factory.create()


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()

