from __future__ import unicode_literals
import json

from faker import Factory
from nose.tools import istest, nottest

from app import db
from tests.fixtures.sqlalchemy.master import PartnerFactory
from tests import testcases


@istest
class CreateTest(testcases.CreateTestCase):

    request_url = '/v1/partners'
    fixture_factory = PartnerFactory
    maxDiff = None

    @classmethod
    def get_api_request_body(cls):
        return {
            'name': _fake.name(),
            'sort_priority': _fake.pyint(),
            'partner_status': 1,
            'is_active': _fake.pybool(),
            'slug': _fake.slug(),
            'meta': _fake.bs(),
            'description': _fake.bs(),
            'icon_image_normal': _fake.url(),
            'icon_image_highres': _fake.url(),
            'organization_id': 2,
        }

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = self.request_body
        self.assertDictContainsSubset(
            expected=expected,
            actual=actual)


@istest
class UpdateTest(testcases.UpdateTestCase):

    request_url = '/v1/partners/{0.id}'
    fixture_factory = PartnerFactory

    @classmethod
    def get_api_request_body(cls):
        return {
            'name': _fake.name(),
            'sort_priority': _fake.pyint(),
            'partner_status': 2,
            'is_active': _fake.pybool(),
            'slug': _fake.slug(),
            'meta': _fake.bs(),
            'description': _fake.bs(),
            'icon_image_normal': _fake.url(),
            'icon_image_highres': _fake.url(),
            'organization_id': 1,
        }

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = self.request_body
        self.assertDictContainsSubset(
            expected=expected,
            actual=actual)


@istest
class DeleteTest(testcases.SoftDeleteTestCase):

    request_url = '/v1/partners/{0.id}'
    fixture_factory = PartnerFactory


@istest
class GetDetailTest(testcases.DetailTestCase):

    request_url = '/v1/partners/{0.id}'
    fixture_factory = PartnerFactory
    maxDiff = None

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = {
            'id': self.fixture.id,
            'name': self.fixture.name,
            'partner_status': self.fixture.partner_status,
            'is_active': self.fixture.is_active,
            'slug': self.fixture.slug,
            'meta': self.fixture.meta,
            'description': self.fixture.description,
            'icon_image_normal': self.fixture.icon_image_normal,
            'icon_image_highres': self.fixture.icon_image_highres,
            'sort_priority': self.fixture.sort_priority,
            'organization_id': self.fixture.organization_id,
        }
        self.assertDictEqual(
            actual,
            expected
        )


@istest
class ListTest(testcases.ListTestCase):

    request_url = '/v1/partners'
    list_key = 'partners'
    fixture_factory = PartnerFactory

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        expected = {
            'id': fixture.id,
            'name': fixture.name,
            'partner_status': fixture.partner_status,
            'is_active': fixture.is_active,
            'slug': fixture.slug,
            'meta': fixture.meta,
            'description': fixture.description,
            'icon_image_normal': fixture.icon_image_normal,
            'icon_image_highres': fixture.icon_image_highres,
            'sort_priority': fixture.sort_priority,
            'organization_id': fixture.organization_id,
        }
        self.assertDictEqual(fixture_json, expected)


_fake = Factory.create()


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()

