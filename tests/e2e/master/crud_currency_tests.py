from __future__ import unicode_literals
import json
from random import choice

from faker import Factory
from nose.tools import istest, nottest

from app import db
from tests.fixtures.sqlalchemy.master import CurrencyFactory
from tests import testcases


@istest
class CreateTest(testcases.CreateTestCase):

    request_url = '/v1/currencies'
    fixture_factory = CurrencyFactory
    maxDiff = None

    @classmethod
    def get_api_request_body(cls):
        return {
            'iso4217_code': _fake.currency_code(),
            'left_symbol': choice(['$', 'Rp.', '']),
            'right_symbol': choice(['', 'Pts']),
            'group_separator': '.',
            'decimal_separator': ',',
            'one_usd_equals': 1234.56,
            'is_active': _fake.pybool(),
        }

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = self.request_body
        expected['one_usd_equals'] = str(expected.get('one_usd_equals'))
        self.assertDictContainsSubset(
            expected=expected,
            actual=actual)


@istest
class UpdateTest(testcases.UpdateTestCase):

    request_url = '/v1/currencies/{0.id}'
    fixture_factory = CurrencyFactory

    @classmethod
    def get_api_request_body(cls):
        return {
            'iso4217_code': _fake.currency_code(),
            'left_symbol': choice(['$', 'Rp.', '']),
            'right_symbol': choice(['', 'Pts']),
            'group_separator': '.',
            'decimal_separator': ',',
            'one_usd_equals': 4567.89,
            'is_active': _fake.pybool(),
        }

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = self.request_body
        expected['one_usd_equals'] = str(expected.get('one_usd_equals'))
        self.assertDictContainsSubset(
            expected=expected,
            actual=actual)


@istest
class DeleteTest(testcases.SoftDeleteTestCase):

    request_url = '/v1/currencies/{0.id}'
    fixture_factory = CurrencyFactory


@istest
class GetDetailTest(testcases.DetailTestCase):

    request_url = '/v1/currencies/{0.id}'
    fixture_factory = CurrencyFactory
    maxDiff = None

    def test_response_body(self):
        actual = json.loads(self.response.data)
        expected = {
            'id': self.fixture.id,
            'iso4217_code': self.fixture.iso4217_code,
            'left_symbol': self.fixture.left_symbol,
            'right_symbol': self.fixture.right_symbol,
            'group_separator': self.fixture.group_separator,
            'decimal_separator': self.fixture.decimal_separator,
            'one_usd_equals': str(self.fixture.one_usd_equals),
            'is_active': self.fixture.is_active,
        }
        self.assertDictEqual(
            actual,
            expected
        )


@istest
class ListTest(testcases.ListTestCase):

    request_url = '/v1/currencies'
    list_key = 'currencies'
    fixture_factory = CurrencyFactory

    def assert_entity_detail_expectation(self, fixture, fixture_json):
        expected = {
            'id': fixture.id,
            'iso4217_code': fixture.iso4217_code,
            'left_symbol': fixture.left_symbol,
            'right_symbol': fixture.right_symbol,
            'group_separator': fixture.group_separator,
            'decimal_separator': fixture.decimal_separator,
            'one_usd_equals': str(fixture.one_usd_equals),
            'is_active': fixture.is_active,
        }
        self.assertDictEqual(fixture_json, expected)


_fake = Factory.create()


def setup_module():
    db.session().close_all()
    db.drop_all()
    db.create_all()


def teardown_module():
    db.session().close_all()
    db.drop_all()

