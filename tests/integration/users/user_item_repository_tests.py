from unittest import TestCase

from datetime import datetime, timedelta

from app import app, db
from app.auth.models import UserInfo
from app.items.helpers import UserItemRepository
from app.offers.choices.status_type import READY_FOR_SALE
from app.utils.testcases import TestBase
from tests.fixtures.sqlalchemy import ItemFactory
from tests.fixtures.sqlalchemy import UserItemFactory
from tests.fixtures.sqlalchemy.offers import standard_offer_types, \
    OfferFactory, OfferBuffetFactory, OfferSingleFactory, offer_of_type
from tests.fixtures.sqlalchemy.orders import OrderLineFactory
from app.users.tests.fixtures_user_buffet import UserBuffetFactory


class UserItemRepositoryTests(TestBase):

    def test_get_permenantly_owned_item(self):

        user = UserInfo(username="dj@scoop.com",
                        user_id=9999,
                        token="abcd",
                        perm=[])

        item = ItemFactory()

        single_offer = OfferFactory(
                offer_status=READY_FOR_SALE,
                offer_type=standard_offer_types()['single'],
                items=[item, ]
        )

        order_line_item = OrderLineFactory(user_id=user.user_id, quantity=1, offer=single_offer)

        self.session.commit()

        repo = UserItemRepository(user, self.session)

        user_item = repo.get_or_new_user_item(item, order_line_item)

        self.assertIsNone(user_item.id)

        db.session.add(user_item)
        db.session.commit()

        self.assertIsNotNone(user_item.id)

        self.assertEqual(user_item.user_id, user.user_id)
        self.assertEqual(user_item.item.id, item.id)
        self.assertIsNone(user_item.user_buffet)

