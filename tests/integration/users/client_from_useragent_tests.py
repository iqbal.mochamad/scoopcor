from __future__ import unicode_literals, absolute_import
from unittest import TestCase

from app import app, db

from app.auth.models import Client
from app.utils.testcases import TestBase


class ClientFromUseragentTests(TestBase):

    @classmethod
    def setUpClass(cls):
        super(ClientFromUseragentTests, cls).setUpClass()

        perpusnas_client = Client(
            id=1234,
            client_id=1234,
            app_name='perpusnas1_android',
            version='1.0.0',
            slug='perpusnas-android',
            is_active=True
        )
        cls.session.add(perpusnas_client)
        cls.session.add(Client(id=1235,client_id=1235,
                               app_name='not_the_right_one',
                               version='1.0.0',
                               slug='per-pus-nas',
                               is_active=True))

        cls.session.add(Client(id=1236,client_id=1236,
                               app_name='not_the_right_one_2',
                               version='1.0.0',
                               slug='per-pus-nas2',
                               is_active=True))

        cls.session.add(Client(id=1237,client_id=1237,
                               app_name='not_the_right_one_3',
                               version='1.0.0',
                               slug='per-pus-nas3',
                               is_active=True))

        cls.session.commit()

    def test_valid_client(self):

        with app.test_request_context('/', headers={'User-Agent': 'PerPusNas1 Android/1.0.0'}) as ctx:

            # run the before-request-functions manually.
            # check the state of 'g'
            for before_req_func in app.before_request_funcs.get(None):
                before_req_func()

            self.assertIsNotNone(ctx.g.current_client)

            self.assertEqual(ctx.g.current_client.client_id, 1234)

            self.assertNotEqual(ctx.g.current_client.client_id, 1235, 1236)
            self.assertIsInstance(ctx.g.current_client, Client)

    def test_invalid_client(self):
        with app.test_request_context('/', headers={'User-Agent': 'SomeUnknownUserAgent/1.0.0'}) as ctx:
            # run the before-request-functions manually.
            # check the state of 'g'
            for before_req_func in app.before_request_funcs.get(None):
                before_req_func()

            self.assertIsNone(ctx.g.current_client)
