import os
import boto3

from unittest import TestCase, SkipTest
from moto import mock_s3
from nose.tools import nottest, istest

from app import app, db
from app.items.api import ItemFileUploadToServerApi
from app.items.file_server_gateway import AmazonS3ServerGateway, InternalFileServerGateway
from tests.fixtures.sqlalchemy.items import ItemFactory, ItemFileFactory
from tests.fixtures.sqlalchemy.master import BrandFactory


class ItemFileUploadToServerApiTests(TestCase):

    db = db
    MaxDiff = None

    def setUp(self):

        # make sure the test mode flag is set, or bail out
        if not app.config.get('TESTING') or db.engine.url.database == 'scoopcor_db':
            raise SkipTest("COR is not in testing mode.  Cannot continue.")

        self.db.drop_all()
        self.db.create_all()

        self.moto = mock_s3()
        # We enter "moto" mode using this
        self.moto.start()

        self.bucket_name = 'abc'

        s3_resource = boto3.resource('s3')
        s3_resource.create_bucket(Bucket=self.bucket_name)

        s3 = boto3.client(
            's3',
            app.config['BOTO3_AWS_REGION'],
            aws_access_key_id='12',
            aws_secret_access_key='a123',
        )

        self.server = AmazonS3ServerGateway(
            s3,
            aws_server=s3._endpoint.host,
            aws_bucket_name=self.bucket_name,
        )

        self.create_item_file_data_for_test()

        # self.file_name = 'testfile.txt'
        #
        # s3_resource.Object(self.bucket_name, self.file_name).put(Body='file content abc')

    def tearDown(self):
        self.moto.stop()
        self.db.session.rollback()
        self.db.drop_all()

    @nottest
    def create_item_file_data_for_test(self):
        s = db.session()
        items_to_upload = [
            {'id': 96031},
            {'id': 96030},
            {'id': 96029},
        ]
        brand = BrandFactory(id=8, name='Sample1')

        for item in items_to_upload:

            item_id = item.get('id')
            file_name = 'file_id_{}.txt'.format(item_id)

            item = ItemFactory(
                id=item_id,
                name='test item check files {}'.format(item_id),
                brand=brand
            )
            itemfile = ItemFileFactory(
                item=item,
                file_name=file_name,
            )

            item_file_full_path = os.path.join('/tmp', file_name)
            with open(item_file_full_path, 'w') as f:
                f.write('dlkfjalfkjdf slkfjsdlk fdsklfj dlksfjlskd fskldfjklsd fjsdlkfj{}'.format(file_name))
                f.close()

        s.commit()

    def test_upload_item(self):

        items_to_upload = [
            {'id': 96031},
            {'id': 96030},
            {'id': 96029},
        ]

        internal_server_path = '/tmp'

        expected = {'results': [
            {'item_id': 96031,
             'item_file': 'file_id_96031.txt',
             'upload_status': 'File uploaded to s3 with key=file_id_96031.txt'
            },
            {'item_id': 96030,
             'item_file': 'file_id_96030.txt',
             'upload_status': 'File uploaded to s3 with key=file_id_96030.txt'
            },
            {'item_id': 96029,
             'item_file': 'file_id_96029.txt',
             'upload_status': 'File uploaded to s3 with key=file_id_96029.txt'
            }
        ]}

        upload_api = ItemFileUploadToServerApi()
        actual = upload_api.upload_item(
            items_to_upload=items_to_upload,
            amazon_s3_server_gateway=self.server,
            internal_server_path=internal_server_path
        )

        self.assertDictEqual(actual, expected)

    def test_post_item_to_s3_from_script(self):

        expected = {'results': [
            {'item_id': 96031,
             'item_file': 'file_id_96031.txt',
             'upload_status': 'File uploaded to s3 with key=file_id_96031.txt'
            }
        ]
        }

        internal_server_path = '/tmp'
        items_to_upload = [{'id': 96031}]

        upload_api = ItemFileUploadToServerApi()
        actual = upload_api.upload_item(
            items_to_upload=items_to_upload,
            amazon_s3_server_gateway=self.server,
            internal_server_path=internal_server_path
        )

        self.assertDictEqual(actual, expected)


