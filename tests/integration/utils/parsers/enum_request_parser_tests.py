from __future__ import unicode_literals, absolute_import

from unittest import TestCase

from enum import Enum
from flask import Flask, make_response
from flask_appsfoundry.parsers import SqlAlchemyFilterParser, IntegerFilter
from sqlalchemy import MetaData, Column, Integer
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

from app import db
from app.layouts.banners import EnumStringFilter
from app.utils.models import Py3EnumType

fake_app = Flask(__name__)

conn = db.session.bind
meta = MetaData(bind=conn)
Base = declarative_base(metadata=meta, bind=conn)
Session = sessionmaker(bind=conn)

class Negara(Enum):
    indonesia = 'indonesia'
    amerika_serikat = 'amerika serikat'


class Orang(Base):
    __tablename__ = 'orang'
    id = Column(Integer, primary_key=True)
    negara = Column(Py3EnumType(Negara, name='negara_type'))


class NegaraParser(SqlAlchemyFilterParser):
    __model__ = Orang
    id = IntegerFilter()
    negara = EnumStringFilter(Negara)


@fake_app.route('/')
def some_view_route():
    return make_response('Sakitnya tuh di sini')


class EnumRequestParserTests(TestCase):

    def setUp(self):
        meta.create_all()
        self.session = Session()

    def tearDown(self):
        self.session.close_all()
        meta.drop_all()

    def test_enum_request(self):

        with fake_app.test_request_context("/?negara=amerika+serikat") as ctx:
            filters = NegaraParser().parse_args(req=ctx.request)['filters']
            self.assertEqual(1, len(filters))

            self.assertEqual(filters[0].right.value, Negara.amerika_serikat)
            self.assertEqual(filters[0].left.name, 'negara')

