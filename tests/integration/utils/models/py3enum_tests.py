from __future__ import unicode_literals, absolute_import
from unittest import TestCase

from enum import Enum
from sqlalchemy import create_engine, MetaData, Column, Integer
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from app import db
from app.utils.models import Py3EnumType

conn = db.session.bind
meta = MetaData(bind=conn)
Base = declarative_base(metadata=meta, bind=conn)
Session = sessionmaker(bind=conn)

class HatType(Enum):
    cap = 'cap'
    baseball_hat = 'baseball hat'
    peci = 'peci'


class Person(Base):
    __tablename__ = 'people'
    id = Column(Integer, primary_key=True)
    favorite_hat_type = Column(Py3EnumType(HatType, name='hat_type'))


class Py3EnumTests(TestCase):

    def setUp(self):
        meta.create_all()
        self.session = Session()


    def tearDown(self):
        self.session.close_all()
        meta.drop_all()

    def test_insert_same_name_value(self):
        p = Person(favorite_hat_type=HatType.peci)
        self.session.add(p)
        self.session.commit()
        self.assertEqual(p.favorite_hat_type, HatType.peci)


    def test_insert_differing_name_value(self):
        p = Person(favorite_hat_type=HatType.baseball_hat)
        self.session.add(p)
        self.session.commit()
        self.assertEqual(p.favorite_hat_type, HatType.baseball_hat)

    def test_querying(self):
        for _ in range(3):
            p = Person(favorite_hat_type=HatType.peci)
            self.session.add(p)
        for _ in range(6):
            p = Person(favorite_hat_type=HatType.baseball_hat)
            self.session.add(p)
        self.session.commit()


        self.assertEqual(
            self.session.query(Person).filter_by(favorite_hat_type=HatType.baseball_hat).count(),
            6)

        self.assertEqual(
            self.session.query(Person).filter_by(favorite_hat_type=HatType.peci).count(),
            3)

    def test_adv_querying(self):
        for _ in range(3):
            p = Person(favorite_hat_type=HatType.peci)
            self.session.add(p)
        for _ in range(6):
            p = Person(favorite_hat_type=HatType.baseball_hat)
            self.session.add(p)
        self.session.commit()

        self.assertEqual(
            self.session.query(Person).filter(Person.favorite_hat_type != HatType.peci).count(),
            6
        )

    def test_returned_enum_is_proper_type(self):
        for _ in range(3):
            p = Person(favorite_hat_type=HatType.peci)
            self.session.add(p)

        self.assertIsInstance(
            self.session.query(Person).first().favorite_hat_type,
            HatType
        )
