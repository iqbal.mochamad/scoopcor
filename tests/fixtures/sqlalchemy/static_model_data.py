"""
Static Model Data
=================

Some of our database models are permanent or semi-permanent.
In the event of needing some of this model data, DO NOT generate factory objects to utilize them!
"""
from random import randint

from factory import Faker, faker, LazyAttribute
from factory.alchemy import SQLAlchemyModelFactory
from faker import Factory
from six import integer_types
from sqlalchemy import func

from app import db
from app.items.models import DistributionCountryGroup
from app.master.models import Category, Currencies, Platform
from app.parental_controls.models import ParentalControl
from app.users.users import Role
from app.auth.models import Perm

__all__ = ('PermFactory', 'RoleFactory', 'CategoryFactory', 'CurrencyFactory',
           'DistributionCountryGroupFactory', 'ParentalControlFactory', 'PlatformFactory', )


class StaticModelFixture(object):

    @classmethod
    def get_random(cls):
        """ Randomly returns a entity from the database.

        :return:
        """
        s = cls.Meta.sqlalchemy_session()
        min_id, max_id = s.query(func.min(cls.Meta.model.id)).scalar(), s.query(func.max(cls.Meta.model.id)).scalar()

        random_entity = None
        while not random_entity:
            random_entity = cls(randint(min_id, max_id))
        return random_entity

    def __new__(cls, database_id=None):
        """
        :param `string_types|integer_types` database_id:
        :return:
        """
        if isinstance(database_id, integer_types):
            return cls.Meta.sqlalchemy_session().query(cls.Meta.model).get(database_id)
        else:
            return cls.Meta.sqlalchemy_session().query(cls.Meta.model).filter_by(name=database_id).first()


class PermFactory(StaticModelFixture):

    class Meta:
        sqlalchemy_session = db.session
        model = Perm


class RoleFactory(StaticModelFixture):

    def __new__(cls, role_type):
        return cls.Meta.sqlalchemy_session().query(Role).get(int(role_type))

    class Meta:
        sqlalchemy_session = db.session
        model = Role


class CategoryFactory(StaticModelFixture):

    class Meta:
        sqlalchemy_session = db.session
        model = Category


class CurrencyFactory(StaticModelFixture):
    # TODO: this doesn't have a name property so will break.. implement similar to roles
    class Meta:
        sqlalchemy_session = db.session
        model = Currencies


class DistributionCountryGroupFactory(StaticModelFixture):

    class Meta:
        sqlalchemy_session = db.session
        model = DistributionCountryGroup


class ParentalControlFactory(SQLAlchemyModelFactory):

    class Meta:
        sqlalchemy_session = db.session
        model = ParentalControl

    name = LazyAttribute(lambda _: _fake.bs()[1:40])
    sort_priority = Faker('pyint')
    is_active = True


class PlatformFactory(StaticModelFixture):

    class Meta:
        sqlalchemy_session = db.session
        model = Platform


_fake = Factory.create()
