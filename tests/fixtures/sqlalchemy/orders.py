from random import randint

from factory import Faker, SubFactory
from factory import LazyAttribute
from factory.alchemy import SQLAlchemyModelFactory

from app import db
from app.orders.models import Order, OrderLine
from app.orders.choices import ORDER_LINE_STATUS_TYPES, COMPLETE
from app.payment_gateways.models import PaymentGateway
from app.users.tests.fixtures import UserFactory
from tests.fixtures.sqlalchemy.master import CurrencyFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory


class PaymentGatewayFactory(SQLAlchemyModelFactory):
    name = Faker('bs')
    payment_flow_type = 1
    organization_id = 1
    base_currency_id = 2
    is_active = True
    sort_priority = 1

    class Meta:
        model = PaymentGateway
        sqlalchemy_session = db.session


class OrderFactory(SQLAlchemyModelFactory):

    order_number = Faker('pyint')
    total_amount = Faker('pyfloat', left_digits=8, right_digits=2, positive=True)
    final_amount = Faker('pydecimal', left_digits=8, right_digits=2, positive=True)

    # user_id = Faker('pyint')
    party = SubFactory(UserFactory)
    client_id = Faker('pyint') # unenforcable constraint from CAS
    partner_id = Faker('pyint') # unenforcable constraint from CAS

    order_status = COMPLETE
    point_reward = Faker('pyint')
    currency_code = Faker('currency_code')
    # careful, no payment gateway with id = 4 (see fixtures/sql/payment-gateways.sql)
    paymentgateway = LazyAttribute(lambda *_: db.session.query(PaymentGateway).get(randint(1, 3)))
    is_active = True

    platform_id = Faker('pyint') # unenforcable constraint from CAS
    temporder_id = Faker('pyint')

    class Meta:
        model = Order
        sqlalchemy_session = db.session


class OrderLineFactory(SQLAlchemyModelFactory):

    name = Faker('bs')
    offer = SubFactory(OfferFactory)

    is_active = True
    is_free = False
    is_discount = False

    user_id = Faker('pyint')
    campaign_id = Faker('pyint')
    order = SubFactory(OrderFactory)
    quantity = 1
    orderline_status = COMPLETE

    currency_code = Faker('random_element', elements=('USD', 'IDR'))
    price = Faker('pydecimal', left_digits=2, right_digits=2, positive=True)
    final_price = Faker('pydecimal', left_digits=2, right_digits=2, positive=True)

    class Meta:
        model = OrderLine
        sqlalchemy_session = db.session
