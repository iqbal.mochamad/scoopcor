from factory import Faker
from factory.alchemy import SQLAlchemyModelFactory

from app import db
from app.revenue_shares.models import RevenueFormulas, RevenueShares


class RevenueFormulaFactory(SQLAlchemyModelFactory):
    platform_id = Faker('pyint')
    fixed_amount = Faker('pydecimal', left_digits=4, right_digits=2, positive=True)
    percentage_amount = Faker('pydecimal', left_digits=4, right_digits=2, positive=True)

    class Meta:
        sqlalchemy_session = db.session
        model = RevenueFormulas


class RevenueShareFactory(SQLAlchemyModelFactory):
    name = Faker('bs')
    description = Faker('text')
    is_active = Faker('pybool')
    default_fixed_amount = Faker('pydecimal', left_digits=4, right_digits=2, positive=True)
    default_percentage_amount = Faker('pydecimal', left_digits=4, right_digits=2, positive=True)

    class Meta:
        sqlalchemy_session = db.session
        model = RevenueShares