from __future__ import unicode_literals, absolute_import

import random

from factory import Faker, LazyAttribute, SubFactory, Sequence
from factory import post_generation
from factory.alchemy import SQLAlchemyModelFactory
from faker import Factory

from app import db
from app.items.choices import ITEM_TYPES, STATUS_TYPES, FILE_TYPES, READING_DIRECTIONS
from app.items.models import (
    Item, UserItem, UserSubscription, ItemUploadProcess, ItemFile, DistributionCountryGroup, Author)
from app.layouts.banners import BannerActions, BannerZone, Banner
from app.parental_controls.models import ParentalControl
from tests.fixtures.sqlalchemy.static_model_data import ParentalControlFactory
from .master import BrandFactory
from .orders import OrderLineFactory

__all__ = ('AuthorFactory', 'ItemFactory', 'UserItemFactory', 'UserSubscriptionFactory', 'ItemUploadProcessFactory',
           'BannerFactory', 'DistributionCountryGroupFactory', )


class AuthorFactory(SQLAlchemyModelFactory):
    name = Sequence(lambda n: '{}-{}'.format(_fake.name(), n))
    sort_priority = Faker('pyint')
    is_active = Faker('pybool')
    slug = Sequence(lambda n: '{}-{}'.format(_fake.slug(), n))

    first_name = Faker('first_name')
    last_name = Faker('last_name')
    title = LazyAttribute(lambda _: random.choice(["MR", "MRS", "MISS", None]))
    academic_title = LazyAttribute(lambda _: random.choice(["DR", "PHD", None]))
    birthdate = Faker('date_time')
    meta = Faker('text')
    profile_pic_url = Faker('url')

    class Meta:
        model = Author
        sqlalchemy_session = db.session


class ItemFactory(SQLAlchemyModelFactory):
    name = Faker('bs')
    slug = Sequence(lambda n: '{}-{}'.format(_fake.slug(), n))
    description = Faker('text')
    display_until = Faker('date_time')
    release_date = Faker('date_time')
    release_schedule = Faker('date_time')

    is_featured = Faker('pybool')
    is_extra = Faker('pybool')

    edition_code = Sequence(lambda n: '{}-{}'.format(_fake.slug(), n))
    issue_number = Sequence(lambda n: '{}-{}'.format(_fake.slug(), n))

    subs_weight = Faker('pyint')
    sort_priority = Faker('pyint')

    thumb_image_normal = Faker('url')
    thumb_image_highres = Faker('url')
    image_normal = Faker('url')
    image_highres = Faker('url')

    gtin13 = Faker('ean13')
    gtin14 = Faker('ean13')
    gtin8 = Faker('ean8')

    # countries = LazyAttribute(lambda _: [_fake.country_code() for _ in range(5)])
    countries = LazyAttribute(lambda _: [random.choice(['id', 'my', ]), ])
    # languages = LazyAttribute(lambda _: [_fake.language_code() for _ in range(5)])
    languages = LazyAttribute(lambda _: [random.choice(['ind', 'eng', ]), ])

    item_status = Faker('random_element', elements=STATUS_TYPES.values())
    content_type = Faker('random_element', elements=FILE_TYPES.values())
    item_type = Faker('random_element', elements=ITEM_TYPES.values())
    reading_direction = Faker('random_element', elements=READING_DIRECTIONS.values())

    # parentalcontrol = LazyAttribute(lambda _: ParentalControlFactory.get_random())
    parentalcontrol_id = LazyAttribute(lambda *_: get_parental_control())

    brand = SubFactory(BrandFactory)

    printed_price = Faker('pydecimal', left_digits=2, right_digits=2)
    printed_currency_code = Faker('random_element', elements=('usd', 'idr',))

    @post_generation
    def authors(self, create, extracted, **kwargs):
        if not create:
            return

        if not extracted:
            for i in range(2):
                self.authors.append(AuthorFactory())
        else:
            for author in extracted:
                self.authors.append(author)

    class Meta:
        sqlalchemy_session = db.session
        model = Item


def get_parental_control():

    parental_control_id = random.choice([1, 2, ])

    parental_control = db.session.query(ParentalControl).get(parental_control_id)
    if not parental_control:
        parental_control = ParentalControlFactory(id=parental_control_id)
        db.session.commit()

    return parental_control_id


class UserItemFactory(SQLAlchemyModelFactory):

    user_id = Faker('pyint')
    item = SubFactory(ItemFactory)
    orderline = SubFactory(OrderLineFactory)

    is_active = True
    is_restore = False

    class Meta:
        sqlalchemy_session = db.session
        model = UserItem


class UserSubscriptionFactory(SQLAlchemyModelFactory):

    user_id = Faker('pyint')
    quantity = Faker('pyint')
    quantity_unit = Faker('pyint')
    current_quantity = Faker('pyint')
    valid_from = Faker('date_time_this_month')
    valid_to = Faker('date_time_this_month')
    brand = SubFactory(BrandFactory)
    subscription_code = Faker('pystr')
    orderline_id = Faker('pyint')
    allow_backward = Faker('pybool')
    backward_quantity = Faker('pyint')
    backward_quantity_unit = Faker('pyint')
    is_active = Faker('pybool')

    class Meta:
        sqlalchemy_session = db.session
        model = UserSubscription


class ItemUploadProcessFactory(SQLAlchemyModelFactory):
    pic_id = Faker('pyint')
    pic_name = Faker('name')
    role_id = Faker('pyint')

    item = SubFactory(ItemFactory)
    start_process_time = Faker('date_time')
    end_process_time = Faker('date_time')

    file_name = Faker('file_name')
    file_size = Faker('pyint')
    file_dir = Faker('file_name')
    status = LazyAttribute(lambda _: random.choice(range(1, 12)))
    error = Faker('text')

    class Meta:
        sqlalchemy_session = db.session
        model = ItemUploadProcess


class ItemFileFactory(SQLAlchemyModelFactory):

    item = SubFactory(ItemFactory)
    file_type = LazyAttribute(lambda _: random.choice(range(1, 3)))
    file_name = Faker('file_name')
    file_order = Faker('pyint')
    file_status = LazyAttribute(lambda _: random.choice(range(1, 7)))
    is_active = True

    class Meta:
        sqlalchemy_session = db.session
        model = ItemFile


class DistributionCountryGroupFactory(SQLAlchemyModelFactory):
    name = Faker('bs')
    group_type = LazyAttribute(lambda _: random.choice(range(1, 2)))
    countries = LazyAttribute(lambda _: [_fake.country_code() for _ in range(5)])
    vendor_id = None
    is_active = True

    class Meta:
        sqlalchemy_session = db.session
        model = DistributionCountryGroup


class BannerFactory(SQLAlchemyModelFactory):

    name = Faker('bs')
    description = Faker('text')
    image_normal = Faker('file_name')
    image_highres = Faker('file_name')
    banner_type = Faker('random_element', elements=BannerActions)
    payload = Faker('text')
    valid_from = Faker('date_time')
    valid_to = Faker('date_time')
    clients = LazyAttribute(lambda _: [])

    #clients = Faker('pylist', None, 10, True, int)
    sort_priority = Faker('pyint')
    # countries = Faker('pylist', None, 10, True, _fake.country_code)
    countries = LazyAttribute(lambda _: [])
    zone = Faker('random_element', elements=BannerZone)

    is_active = Faker('pybool')

    class Meta:
        model = Banner
        sqlalchemy_session = db.session



_fake = Factory.create()
