from datetime import datetime, timedelta

from factory import alchemy, Faker, LazyAttribute, SubFactory

from app import db
from app.auth.models import Client, Token, ClientVersion

__all__ = ('ClientFactory',
           'TokenFactory',)


# TODO: remove this and replace with a static fixture getter =)


class ClientFactory(alchemy.SQLAlchemyModelFactory):
    client_id = Faker('random_number')
    name = Faker('ean', length=8)
    slug = Faker('slug')

    class Meta:
        sqlalchemy_session = db.session
        model = Client


class ClientVersionFactory(alchemy.SQLAlchemyModelFactory):
    client = SubFactory(ClientFactory)
    version = Faker('ean', length=8)
    is_active = True
    enable_scoop_point = Faker('boolean')
    enable_referral = Faker('boolean')

    class Meta:
        sqlalchemy_session = db.session
        model = ClientVersion


class TokenFactory(alchemy.SQLAlchemyModelFactory):
    """ Generate a token which will be valid from one-month ago until 3 months in the future.
    """
    token = Faker('md5')
    refresh_token = Faker('md5')
    valid_from = LazyAttribute(lambda _: datetime.now() - timedelta(days=1))
    valid_to = LazyAttribute(lambda _: datetime.now() + timedelta(days=3))
    is_active = True

    class Meta:
        sqlalchemy_session = db.session
        model = Token


