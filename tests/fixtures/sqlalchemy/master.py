from random import choice
from factory import Faker, LazyAttribute, SubFactory, Sequence
from factory.alchemy import SQLAlchemyModelFactory
from faker import Factory

from app import db
from app.items.choices import ITEM_TYPES
from app.master.models import Currencies, Vendor, Brand, VendorRevenueShare, Category, Partner, Platform
from .revenue_shares import RevenueShareFactory

__all__ = (
    'CurrencyFactory', 'VendorFactory', 'VendorRevenueShareFactory', 'BrandFactory',
    'CategoryFactory', 'PartnerFactory', 'PlatformFactory', )


class CurrencyFactory(SQLAlchemyModelFactory):
    iso4217_code = Faker('currency_code')

    # The 4 fields below are based on our production data
    left_symbol = LazyAttribute(lambda _: choice(['$', 'Rp.', '']))
    right_symbol = LazyAttribute(lambda _: choice(['', 'Pts']))
    group_separator = LazyAttribute(lambda _: '.')
    decimal_separator = LazyAttribute(lambda _: ',')

    one_usd_equals = Faker('pydecimal', left_digits=4, right_digits=2, positive=True)
    is_active = Faker('pybool')

    class Meta:
        model = Currencies
        sqlalchemy_session = db.session


class VendorFactory(SQLAlchemyModelFactory):
    name = Faker('bs')
    description = Faker('text')
    slug = Sequence(lambda n: '{}-{}'.format(_fake.slug(), n))
    sort_priority = Faker('pyint')
    is_active = Faker('pybool')
    organization_id = Faker('pyint')
    vendor_status = Faker('pyint') # this is an limited set of values, but we don't need it for now.
    meta = Faker('text')
    icon_image_normal = Faker('url')
    icon_image_highres = Faker('url')
    accounting_identifier = Faker('word')
    iso4217_code = Faker('currency_code')

    class Meta:
        model = Vendor
        sqlalchemy_session = db.session


class VendorRevenueShareFactory(SQLAlchemyModelFactory):
    vendor = SubFactory(VendorFactory)
    # missing revenue relationship, so we need to manually define!
    valid_to = Faker('date_time')
    valid_from = Faker('date_time')
    is_active = Faker('pybool')
    revenue_share = SubFactory(RevenueShareFactory)


    class Meta:
        model = VendorRevenueShare
        sqlalchemy_session = db.session


class BrandFactory(SQLAlchemyModelFactory):
    name = Faker('bs')
    description = Faker('text')
    slug = Sequence(lambda n: '{}-{}'.format(_fake.slug(), n))
    meta = Faker('text')
    icon_image_normal = Faker('url')
    icon_image_highres = Faker('url')
    sort_priority = Faker('pyint')
    is_active = Faker('pybool')
    daily_release_quota = Faker('pyint')
    vendor = SubFactory(VendorFactory)

    class Meta:
        model = Brand
        sqlalchemy_session = db.session


class CategoryFactory(SQLAlchemyModelFactory):
    name = Faker('name')
    description = Faker('text')
    slug = Sequence(lambda n: '{}-{}'.format(_fake.slug(), n))
    meta = Faker('text')
    icon_image_normal = Faker('url')
    icon_image_highres = Faker('url')
    sort_priority = Faker('pyint')
    parent_category_id = None
    # parent_category_id = SubFactory(CategoryFactory)
    item_type = Faker('random_element', elements=ITEM_TYPES.values())
    countries = ['id', 'my']
    # cannot use _fake.country_code() cause as of now, category use app.utils.shims.countries.id_to_iso3166 mapping
    #   this dict mapping is less than all country available in _fake.country_code
    # LazyAttribute(lambda _: [_fake.country_code() for _ in range(5)])

    class Meta:
        model = Category
        sqlalchemy_session = db.session


class PartnerFactory(SQLAlchemyModelFactory):
    name = Faker('name')
    sort_priority = Faker('pyint')
    partner_status = Faker('random_element', elements=range(1, 8))
    is_active = Faker('pybool')
    slug = Sequence(lambda n: '{}-{}'.format(_fake.slug(), n))
    meta = Faker('text')
    description = Faker('text')
    icon_image_normal = Faker('url')
    icon_image_highres = Faker('url')
    organization_id = Faker('pyint')
    # organization_d mapped to Scoop CAS organization_id, here we can just fake it

    class Meta:
        model = Partner
        sqlalchemy_session = db.session


class PlatformFactory(SQLAlchemyModelFactory):
    name = Faker('name')
    description = Faker('text')
    is_active = Faker('pybool')

    class Meta:
        model = Platform
        sqlalchemy_session = db.session


_fake = Factory.create()
