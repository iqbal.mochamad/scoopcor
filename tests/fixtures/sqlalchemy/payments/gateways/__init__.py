from decimal import Decimal
from random import randint
from factory import Faker, LazyAttribute
from factory.alchemy import SQLAlchemyModelFactory

from app.master.models import Currencies
from app.payments.choices import PAYMENT_FLOW_TYPES

from app import db
from app.payment_gateways.models import PaymentGateway


class PaymentGatewayFactory(SQLAlchemyModelFactory):
    name = Faker('bs')
    mnc = Faker('password')
    mcc = Faker('password')
    sms_number = LazyAttribute(lambda _: "".join(str(randint(0,9)) for _ in range(10)))
    payment_flow_type = Faker('random_element', elements=PAYMENT_FLOW_TYPES)

    description = Faker('text')
    slug = Faker('slug')
    meta = Faker('text')
    icon_image_normal = Faker('url')
    icon_image_highres = Faker('url')

    sort_priority = Faker('pyint')
    is_active = Faker('pybool')

    # this is directly setting the ID because the relationship is unenforceable
    # the referenced object lives in CAS
    organization_id = Faker('pyint')

    # clients?? .. skipping this because it's unclear how we could
    # create usable test data

    # base_currency = SubFactory(CurrencyFactory)
    base_currency = LazyAttribute(lambda *_: db.session.query(Currencies).get(randint(1, 2)))
    minimal_amount = LazyAttribute(lambda _: Decimal(float(randint(0, 10))))

    class Meta:
        sqlalchemy_session = db.session
        model = PaymentGateway
