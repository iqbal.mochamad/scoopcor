from factory import SubFactory, Faker
from factory.alchemy import SQLAlchemyModelFactory
from app import db
from app.paymentgateway_connectors.faspay.faspaypg.choices import \
    FASPAY_PAYMENT_STATUSES, PAYMENT_CARD_TYPES
from app.paymentgateway_connectors.faspay.models import PaymentBCAKlikpay
from tests.fixtures.sqlalchemy.payments import PaymentFactory
from app.paymentgateway_connectors.faspay.faspaypg.response_status import FASPAY_PAYMENT_RESPONSE_STATUSES

class PaymentBCAKlikpayFactory(SQLAlchemyModelFactory):

    payment = SubFactory(PaymentFactory)
    transaction_no = Faker('pystr')
    mi_trx_id = Faker('pystr', max_chars=16)
    mi_signature = Faker('pystr')
    pay_type = Faker('random_element', elements=['01', '00', '02']) # I have literally no idea what the hell the choices are for this.
    bca_signature = Faker('pystr', max_chars=11)
    bca_authkey = Faker('pystr', max_chars=32)
    response_code = Faker('random_element', elements=FASPAY_PAYMENT_RESPONSE_STATUSES)
    payment_status_code = Faker('random_element', elements=FASPAY_PAYMENT_STATUSES)
    purchase_date = Faker('date_time')
    card_type = Faker('random_element', elements=PAYMENT_CARD_TYPES)

    # the other 4 properties, I think are just logging.. so I'm skipping

    class Meta:
        model = PaymentBCAKlikpay
        sqlalchemy_session = db.session
