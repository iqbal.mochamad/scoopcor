from factory.alchemy import SQLAlchemyModelFactory

from factory import Faker, SubFactory

from app import db
from app.paymentgateway_connectors.creditcard.models import PaymentVeritrans
from app.payments.choices import PAYMENT_STATUSES
from app.payments.models import Payment

from tests.fixtures.sqlalchemy.orders import OrderFactory


class PaymentFactory(SQLAlchemyModelFactory):

    order = SubFactory(OrderFactory)
    user_id = Faker('pyint') # unenforcable constraint from CAS
    # paymentgateway = SubFactory(PaymentGatewayFactory)
    currency_code = Faker('currency_code')
    amount = Faker('pydecimal', left_digits=2, right_digits=2, positive=True)
    payment_status = Faker('random_element', elements=PAYMENT_STATUSES)
    is_active = Faker('pybool')
    is_test_payment = Faker('pybool')
    payment_datetime = Faker('date_time')
    financial_archive_date = Faker('date_time')

    class Meta:
        model = Payment
        sqlalchemy_session = db.session


class PaymentVeritransFactory(SQLAlchemyModelFactory):

    payment = SubFactory(PaymentFactory)
    transaction_id = Faker('pyint')
    payment_code = Faker('bs')

    class Meta:
        model = PaymentVeritrans
        sqlalchemy_session = db.session

