import random

from factory import alchemy, Faker, LazyAttribute, SubFactory

from app import db
from app.users.base import Party, PartyType
from app.users.wallet import Wallet, WalletTransaction
from app.users.tests.fixtures import OrganizationFactory, UserFactory
from tests.fixtures.sqlalchemy.orders import OrderFactory


class PartyFactory(alchemy.SQLAlchemyModelFactory):
    id = SubFactory(OrganizationFactory)
    party_type = LazyAttribute(lambda _: random.choice([v.value for v in PartyType]))

    class Meta:
        sqlalchemy_session = db.session
        model = Party


class OrganizationWalletFactory(alchemy.SQLAlchemyModelFactory):
    party = SubFactory(OrganizationFactory)
    balance = Faker('pyint')
    is_active = Faker('pybool')

    class Meta:
        sqlalchemy_session = db.session
        model = Wallet


class OrganizationWalletTransactionFactory(alchemy.SQLAlchemyModelFactory):
    wallet = SubFactory(OrganizationWalletFactory)
    amount = Faker('pyint')
    order = SubFactory(OrderFactory)

    class Meta:
        sqlalchemy_session = db.session
        model = WalletTransaction


class UserWalletFactory(alchemy.SQLAlchemyModelFactory):
    party = SubFactory(UserFactory)
    balance = Faker('pyint')
    is_active = Faker('pybool')

    class Meta:
        sqlalchemy_session = db.session
        model = Wallet


class UserWalletTransactionFactory(alchemy.SQLAlchemyModelFactory):
    wallet = SubFactory(UserWalletFactory)
    amount = Faker('pyint')
    order = SubFactory(OrderFactory)

    class Meta:
        sqlalchemy_session = db.session
        model = WalletTransaction
