from datetime import timedelta
from factory import Faker, SubFactory, LazyAttribute, PostGeneration
from factory.alchemy import SQLAlchemyModelFactory
from random import randint

from app import db
from app.master.models import Platform
from app.offers.choices import OFFER_STATUS
from app.offers.models import OfferType, Offer, OfferBuffet, OfferPlatform, OfferSubscription
from app.remote_publishing.choices import REMOTE_DATA_LOCATIONS
from app.remote_publishing.models import RemoteOffer, RemoteCategory
from tests.fixtures.sqlalchemy.master import CategoryFactory, PlatformFactory
from app.offers.choices.status_type import READY_FOR_SALE
from app.offers.models import OfferType, Offer, OfferBuffet, OfferSingle


class OfferTypeFactory(SQLAlchemyModelFactory):
    name = Faker('bs')
    description = Faker('text')
    slug = Faker('slug')
    meta = Faker('text')
    sort_priority = 1
    is_active = True

    class Meta:
        sqlalchemy_session = db.session
        model = OfferType


def standard_offer_types():

    # global __offer_types
    if not db.session.query(OfferType).count():
        __offer_types = {
            'single': OfferTypeFactory(id=1, name='Single', slug='single'),
            'subscription': OfferTypeFactory(id=2, name='Subscription', slug='subscription'),
            'bundle': OfferTypeFactory(id=3, name='Bundle', slug='bundle'),
            'buffet': OfferTypeFactory(id=4, name='Buffet', slug='buffet'),
        }
        db.session.commit()
    else:
        __offer_types = {
            'single': db.session.query(OfferType).get(1),
            'subscription': db.session.query(OfferType).get(2),
            'bundle': db.session.query(OfferType).get(3),
            'buffet': db.session.query(OfferType).get(4),
        }

    return __offer_types


class OfferFactory(SQLAlchemyModelFactory):
    name = Faker('bs')
    long_name = Faker('bs')
    offer_status = READY_FOR_SALE
    sort_priority = 1
    is_active = True

    offer_type = LazyAttribute(lambda *_: standard_offer_types()['single'])
    # offer_type = SubFactory(OfferTypeFactory)
    is_free = False

    price_usd = Faker('pydecimal', left_digits=2, right_digits=2, positive=True)
    price_idr = Faker('pydecimal', left_digits=5, right_digits=0, positive=True)
    price_point = Faker('pyint')

    class Meta:
        sqlalchemy_session = db.session
        model = Offer


class OfferSubscriptionFactory(SQLAlchemyModelFactory):
    description = Faker('bs')
    quantity = Faker('pyint')
    quantity_unit = 4

    offer = LazyAttribute(lambda *_: offer_of_type('subscription'))

    class Meta:
        sqlalchemy_session = db.session
        model = OfferSubscription


def offer_of_type(type_name):
    offer_type = standard_offer_types()[type_name]
    offer = OfferFactory(offer_type=offer_type)
    return offer


class OfferSingleFactory(SQLAlchemyModelFactory):
    offer = LazyAttribute(lambda *_: offer_of_type('single'))

    class Meta:
        sqlalchemy_session = db.session
        model = OfferSingle


class OfferBuffetFactory(SQLAlchemyModelFactory):
    available_from = Faker('date_time')
    available_until = Faker('date_time')
    buffet_duration = LazyAttribute(lambda _: timedelta(days=randint(10, 20)))
    offer = LazyAttribute(lambda *_: offer_of_type('buffet'))

    class Meta:
        sqlalchemy_session = db.session
        model = OfferBuffet


class RemoteOfferFactory(SQLAlchemyModelFactory):
    offer_id = SubFactory(OfferFactory)
    remote_service = Faker('random_element', elements=list(REMOTE_DATA_LOCATIONS))
    remote_id = None
    last_checked = Faker('date_time')
    last_posted = Faker('date_time')

    class Meta:
        sqlalchemy_session = db.session
        model = RemoteOffer


class RemoteCategoryFactory(SQLAlchemyModelFactory):
    category_id = SubFactory(CategoryFactory)
    remote_service = Faker('random_element', elements=list(REMOTE_DATA_LOCATIONS))
    remote_id = None

    class Meta:
        sqlalchemy_session = db.session
        model = RemoteCategory


class OfferPlatformFactory(SQLAlchemyModelFactory):
    offer = SubFactory(OfferFactory)
    platform = LazyAttribute(lambda *_: db.session.query(Platform).get(randint(1, 4)) or PlatformFactory())
    tier_id = Faker('pyint')
    tier_code = Faker('bs')
    currency = 'idr'
    price_usd = Faker('pydecimal', left_digits=2, right_digits=2, positive=True)
    price_idr = Faker('pydecimal', left_digits=5, right_digits=0, positive=True)
    price_point = Faker('pyint')

    discount_price_usd = Faker('pydecimal', left_digits=2, right_digits=2, positive=True)
    discount_price_idr = Faker('pydecimal', left_digits=2, right_digits=2, positive=True)
    discount_price_point = Faker('pyint')

    class Meta:
        sqlalchemy_session = db.session
        model = OfferPlatform





