from __future__ import unicode_literals, absolute_import

from factory import Faker, SubFactory, LazyAttribute
from factory.alchemy import SQLAlchemyModelFactory

from app import db
from app.layouts.layout_type import LayoutTypes
from app.layouts.models import LayoutControl, LayoutOffer, LayoutBanner, LayoutIntro

from tests.fixtures.sqlalchemy.master import BrandFactory
from tests.fixtures.sqlalchemy.offers import OfferFactory


class LayoutFactory(SQLAlchemyModelFactory):
    name = Faker('name')
    sort_priority = Faker('pyint')
    layout_type = Faker('random_element', elements=[LayoutTypes.latest_book,
                                                    LayoutTypes.latest_magazine,
                                                    LayoutTypes.latest_newspaper,
                                                    LayoutTypes.popular_book,
                                                    LayoutTypes.popular_magazine,
                                                    LayoutTypes.popular_newspaper,
                                                    LayoutTypes.editor_pick_banners,
                                                    LayoutTypes.editor_pick_offers])

    class Meta:
        sqlalchemy_session = db.session
        model = LayoutControl


class LayoutOfferFactory(SQLAlchemyModelFactory):
    layout = SubFactory(LayoutFactory)
    offer = SubFactory(OfferFactory)
    sort_priority = Faker('pyint')

    class Meta:
        sqlalchemy_session = db.session
        model = LayoutOffer


class LayoutBannerFactory(SQLAlchemyModelFactory):
    layout = SubFactory(LayoutFactory)
    # banner = SubFactory(BannerFactory)
    sort_priority = Faker('pyint')

    class Meta:
        sqlalchemy_session = db.session
        model = LayoutBanner


class LayoutIntroFactory(SQLAlchemyModelFactory):
    brand = SubFactory(BrandFactory)
    sort_priority = Faker('pyint')

    class Meta:
        sqlalchemy_session = db.session
        model = LayoutIntro

