from datetime import timedelta
from factory import Faker, LazyAttribute, SubFactory
from factory.alchemy import SQLAlchemyModelFactory
from faker import Factory

from app import db
from app.campaigns.models import Campaign
from app.discounts.models import Discount
from app.utils.datetimes import get_local

_fake = Factory.create()


class CampaignFactory(SQLAlchemyModelFactory):
    name = Faker('bs')
    description = Faker('bs')
    start_date = LazyAttribute(lambda _: get_local() - timedelta(days=7))
    end_date = LazyAttribute(lambda _: get_local() + timedelta(days=7))
    total_cost = Faker('pydecimal', left_digits=8, right_digits=2, positive=True)

    class Meta:
        model = Campaign
        sqlalchemy_session = db.session


class DiscountFactory(SQLAlchemyModelFactory):
    name = Faker('bs')

    tag_name = Faker('bs')
    description = Faker('text')
    campaign = SubFactory(CampaignFactory)

    valid_to = LazyAttribute(lambda _: get_local() - timedelta(days=7))
    valid_from = LazyAttribute(lambda _: get_local() + timedelta(days=7))

    # status-status-an
    discount_rule = Faker('random_element', elements=[1, 2, 3])  # READ LEGEND : OFFER DISCOUNT RULE
    discount_type = Faker('random_element', elements=[1, 2, 3, 4, 5])  # READ LEGEND : DISCOUNT TYPE
    discount_status = Faker('random_element', elements=[1, 2, 3])  # READ LEGEND : DISCOUNT STATUS
    discount_schedule_type = Faker('random_element', elements=[1, 2])  # READ LEGEND : DISCOUNT SCHEDULE TYPE
    is_active = Faker('pybool')

    discount_usd = 10.0
    discount_idr = 10.0
    discount_point = 10.0
    #
    # min_usd_order_price = db.Column(db.Numeric(10, 2))
    # max_usd_order_price = db.Column(db.Numeric(10, 2))
    # min_idr_order_price = db.Column(db.Numeric(10, 2))
    # max_idr_order_price = db.Column(db.Numeric(10, 2))

    class Meta:
        model = Discount
        sqlalchemy_session = db.session
