from __future__ import unicode_literals

import smtplib
from base64 import b64encode
import os

from minimock import Mock

from app.auth.helpers import prehash_user_password
from tests.fixtures.sqlalchemy import TokenFactory
from app.users.tests.fixtures import UserFactory

FIXTURES_BASE_DIR = os.path.dirname(__file__)
SQL_FIXTURES_DIR = os.path.join(FIXTURES_BASE_DIR, 'sql')


def mock_smtplib():
    smtplib.SMTP = Mock('smtplib.SMTP')
    smtplib.SMTP.mock_returns = Mock('smtp_connection')


def generate_static_sql_fixtures(session):
    """ Inserts all the static SQL fixtures into the database.

    This are all the unchanging, or rarely changing, SQL data (so after major updates to any of these tables,
    it would be a good idea to refresh all of their data).

    :param `sqlalchemy.orm.session.Session` session:
    :return: Nothing
    """
    for fixture_sql in [
        # need to be created in this order
        'perms.sql',
        'roles.sql',
        'perms_roles.sql',
        'currencies.sql',
        'payment-gateways.sql',

        # the following can be generated in any order
        'offer-types.sql',
        'categories.sql',
        'distribution_countries.sql',
        'parental_controls.sql',
        'platforms.sql',
        'clients.sql'
    ]:

        with open(os.path.join(SQL_FIXTURES_DIR, fixture_sql)) as fh:
            for insert_statement in fh:
                session.execute(insert_statement)

    session.commit()


def get_role(role_type, session):
    """ Returns a `Role` database model matching the provided type, which is still attached to the session.

    :param `app.auth.choices.RoleType|int` role_type:
    :param `sqlalchemy.orm.session.Session` session: sqlalchemy session object
    """
    from app.users.users import Role
    role = session.query(Role).get(int(role_type))
    return role


def create_user_and_token(session, *role_types, **user_attributes):
    """ Creates a `User`, in the specified `Role`s with the password 'we4scoop'

    The user and token will both be returned in a tuple, after they're expunged from the current session.

    :param `sqlalchemy.orm.session.Session` session:
    :rtype: tuple
    """
    roles = [get_role(role_type, session) for role_type in role_types]

    user = UserFactory(roles=roles, **user_attributes)
    user.set_password(prehash_user_password('we4scoop'))

    all_perms = []
    for role in roles:
        all_perms.extend([perm.name for perm in role.perms])

    token = TokenFactory(
        user=user,
        perm_list=all_perms
    )

    session.add(user)
    session.commit()

    session.refresh(user)
    session.refresh(token)

    session.expunge(user)
    session.expunge(token)

    return user, token


def bearer_string(user, token):
    """ Returns a string that can be used to authenticate an api user in the Bearer realm.

    Eg, given the following header -> Authorization: Bearer abcd

    'abcd' is what would be returned by this function given a valid `User` and `Token`

    :param `app.users.users.User` user:
    :param `app.auth.models.Token` token:
    :return:
    :rtype: six.string_types
    """
    return b64encode("{}:{}".format(user.id, token.token))


