from nose.tools import eq_
from mockredis import MockRedis
from flask.ext.testing import TestCase
import app as application
from flask import abort

import ast
import scrypt
import base64
import requests
import json
import redis

from app import app, db
from app.auth import constants as AUTH

from datetime import datetime

from app.items.choices import ItemTypes
from app.master.models import Vendor


def check_content_type(headers):
    '''
        Utility helper function used everwhere in the test case
    '''
    eq_(headers['Content-Type'], 'application/json')


def get_or_default(args, keyname, default):
    '''
    Where
    args is a reqparser Namespace instance (this is why we can't use getattr),
    keyname is the name of the key in our object, and
    default is the default value should args[keyname] be None

    Returns the value of args[keyname]

    Usage example:
    u.username = get_or_default(args, 'username', u.username)
    '''
    if args[keyname] is None:
        return default
    return args[keyname]


class SC2TestCase(TestCase):

    def create_app(self):
        app = application.app
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://scoopadm_usr:hobbes@localhost:5432/sc2_testing_db'
        app.kvs = MockRedis(strict=True)
        self.kvs = app.kvs
        return app

    def setUp(self):

        def generate_valid_token():
            USERNAME_VALID = 'kiratakada@gmail.com'
            USER_ID = 1

            token = scrypt.hash(USERNAME_VALID, str(datetime.utcnow()),
                buflen=AUTH.TOKEN_BYTES).encode('hex')

            jsonform = {'token': token, 'perm': ['can_read_write_global_all']}
            app.kvs.set(AUTH.TOKEN_PREFIX + str(USER_ID), jsonform, ex=AUTH.TOKEN_TTL)

        def generate_invalid_token():
            USERNAME_INVALID = 'ga_ada_gitu@gmail.com'
            USER_ID = 2

            token = scrypt.hash(USERNAME_INVALID, str(datetime.utcnow()),
                buflen=AUTH.TOKEN_BYTES).encode('hex')

            jsonform = {'token': token, 'perm': ['can_bom_bom_bom']}
            app.kvs.set(AUTH.TOKEN_PREFIX + str(USER_ID), jsonform, ex=AUTH.TOKEN_TTL)

        db.create_all()

        app.kvs = redis.StrictRedis(
            host=app.config['REDIS_HOST'],
            port=app.config['REDIS_PORT'],
            db=app.config['REDIS_DB'],
        )

        vendor = Vendor(
            name = "Apps-Foundry",
            description = "Hap Hap",
            slug = "Apps-Foundry.com",
            meta = "Hap Hap",
            icon_image_normal = "Hap Hap",
            icon_image_highres = "Hap Hap",
            cut_rule_id = 1,
            sort_priority = 1,
            is_active = True
        )

        item_types = ItemTypes(
            name = "Majalah Keren",
            description = "Majalah Keren",
            slug = "Majalah Keren",
            meta = "Majalah Keren",
            sort_priority = 1,
            is_active = True

        )

        generate_valid_token()
        generate_invalid_token()

        db.session.add(vendor)
        db.session.add(item_types)
        db.session.commit()

    def get_valid_token_header(self):
        token = ast.literal_eval(app.kvs.get('tok:1'))
        username_token = '%s:%s' % (1, token['token'])
        b64val = base64.b64encode(username_token)

        # Generate headers
        headers = {}
        headers['Authorization'] = 'Bearer %s' % b64val
        headers['Content-Type'] = 'application/json'
        return headers

    def get_invalid_token_header(self):
        token = ast.literal_eval(app.kvs.get('tok:2'))
        username_token = '%s:%s' % (2, token['token'])

        b64val = base64.b64encode(username_token)

        # Generate headers
        headers = {}
        headers['Authorization'] = 'Bearer %s' % b64val
        headers['Content-Type'] = 'application/json'
        return headers

    def tearDown(self):
        db.session.remove()
        app.kvs.delete('tok:1')
        app.kvs.delete('tok:2')
        db.drop_all()

