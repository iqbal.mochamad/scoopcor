from __future__ import unicode_literals, absolute_import

import os
import json

def solr_response_fixture(name, schema_type):
    """

    :param `SchemaType` schema_type:
    :param `six.string_types` name:
    :return:
    :rtype: dict
    """
    curr_dir = os.path.abspath(os.path.dirname(__file__))
    return json.load(open(os.path.join(curr_dir, schema_type.value, '{}.json'.format(name))))
