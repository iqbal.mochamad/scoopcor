from __future__ import unicode_literals

from random import choice

from faker import Factory

from app.analytics import choices



class PageviewReqBodyFactory(object):
    def __new__(cls, **kwargs):
        fake_dict = {
            'device_id': kwargs.pop("device_id", _fake.word()),
            'user_id': kwargs.pop("user_id", _fake.pyint()),
            'item_id': kwargs.pop("item_id", _fake.pyint()),
            'session_name': kwargs.pop("session_name",_fake.word()),
            'ip_address': kwargs.pop("ip_address",_fake.ipv4()),
            'location': kwargs.pop("location", "{} {}".format(_fake.geo_coordinate(), _fake.geo_coordinate())),
            'client_version': kwargs.pop("client_version", "{}.{}.{}".format(_fake.pyint(), _fake.pyint(), _fake.pyint())),
            'os_version': kwargs.pop("os_version", "{}.{}.{}".format(_fake.pyint(), _fake.pyint(), _fake.pyint())),
            'device_model': kwargs.pop("device_model", _fake.bs()),
            'client_id': kwargs.pop("client_id", _fake.pyint()),
            'datetime': kwargs.pop("datetype", _fake.iso8601()),
            'activity_type': kwargs.pop("activity_type", 'pageview'),

            'page_orientation': kwargs.pop("page_orientation", choice(choices.ORIENTATION_TYPES)),
            'page_number': kwargs.pop("page_number", _fake.pylist(5, True, int)),
            'duration': kwargs.pop("duration", str(_fake.pydecimal(left_digits=6, right_digits=4, positive=True))),
            'online_status': kwargs.pop("online_status", choice(choices.ONLINE_TYPES))
        }
        if kwargs:
            raise ValueError("Invalid kwargs provided: {}".format(
                ",".join(kwargs)))
        return fake_dict


def generate_pageview_input():
    return {
        'device_id': _fake.word(),
        'user_id': _fake.pyint(),
        'item_id': _fake.pyint(),
        'session_name': _fake.word(),
        'ip_address': _fake.ipv4(),
        'location': u"{} {}".format(_fake.geo_coordinate(), _fake.geo_coordinate()),
        'client_version': u"{}.{}.{}".format(_fake.pyint(), _fake.pyint(), _fake.pyint()),
        'os_version': u"{}.{}.{}".format(_fake.pyint(), _fake.pyint(), _fake.pyint()),
        'device_model': _fake.bs(),
        'client_id': _fake.pyint(),
        'datetime': _fake.iso8601(),
        'activity_type': u'pageview',

        'page_orientation': choice(choices.ORIENTATION_TYPES),
        'page_number': _fake.pylist(5, True, int),
        'duration': _fake.pydecimal(),
        'online_status': choice(choices.ONLINE_TYPES)
    }


def generate_download_input():
    return {
        'device_id': _fake.word(),
        'user_id': _fake.pyint(),
        'item_id': _fake.pyint(),
        'session_name': _fake.word(),
        'ip_address': _fake.ipv4(),
        'location': u"{} {}".format(_fake.geo_coordinate(), _fake.geo_coordinate()),
        'client_version': u"{}.{}.{}".format(_fake.pyint(), _fake.pyint(), _fake.pyint()),
        'os_version': u"{}.{}.{}".format(_fake.pyint(), _fake.pyint(), _fake.pyint()),
        'device_model': _fake.bs(),
        'client_id': _fake.pyint(),
        'datetime': _fake.iso8601(),
        'activity_type': u'download',
        'download_status': choice(choices.SUCCESS_TYPES),
    }


_fake = Factory.create()
