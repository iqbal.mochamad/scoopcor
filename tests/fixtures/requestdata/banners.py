import random
from faker import Factory
from app.banners.choices import ACTIONS, DISPLAY_ZONES
from app.banners.choices.actions import LANDING_PAGE, STATIC, OPEN_URL

fake = Factory.create()

def get_create_data(client_ids, img_highres, img_lowres, validity_type="now"):

    banner_type = random.choice(ACTIONS)

    if validity_type == "now":
        valid_from = fake.date_time_between(start_date="-30d", end_date="-1d")
        valid_to = fake.date_time_between(start_date="+1d", end_date="+30d")
    elif validity_type == "future":
        valid_from = fake.date_time_between(start_date="-30d", end_date="-1d")
        valid_to = fake.date_time_between(start_date="+1d", end_date="+30d")
    elif validity_type == "past":
        valid_from = fake.date_time_between(start_date="-30d", end_date="-20d")
        valid_to = fake.date_time_between(start_date="+1d", end_date="-1h")


    data = {
        "banner_type": banner_type,
        "payload": generate_payload(banner_type),
        "valid_from": valid_from,
        "valid_to": valid_to,
        "is_active": random.choice([True, False]),
        "sort_priority": random.randint(1, 10),
        "countries": generate_country_code_list(),
        "clients": client_ids,
        "zone": random.choice(DISPLAY_ZONES),
        "image_highres": img_highres,
        "image_normal": img_lowres
    }

    return data

def generate_country_code_list(min=1, max=10):
    pass

def generate_client_id_list(min=1, max=10):
    pass

def generate_payload(banner_type):
    if banner_type == LANDING_PAGE:
        return ""
    elif banner_type == STATIC:
        return ""
    elif banner_type == OPEN_URL:
        return fake.url()
