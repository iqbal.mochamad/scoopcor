from __future__ import unicode_literals, absolute_import
from unittest import TestCase

from flask_sqlalchemy import SQLAlchemy
from flask import Flask

from app.utils.models import url_mapped, get_url_for_model, get_mapped_attribute

fake_app = Flask(__name__)
fake_app.config.update({
    'SQLALCHEMY_DATABASE_URL': 'sqlite://'
})
fake_db = SQLAlchemy(fake_app)


@fake_app.route('/')
def index():
    pass


@fake_app.route('/teacher/<int:db_id>', endpoint='teacher_detail')
def teacher_detail(db_id):
    pass


@fake_app.route('/student-general/<int:db_id>', endpoint='student_detail')
def student_detail(db_id):
    pass


@fake_app.route('/student-smp/<int:db_id>', endpoint='student_smp_detail')
def student_detail(db_id):
    pass


@url_mapped('teacher_detail', (('db_id', 'id'), ))
class Teacher(fake_db.Model):
    __tablename__ = 'teachers'
    id = fake_db.Column(fake_db.Integer, primary_key=True)
    name = fake_db.Column(fake_db.Text)


@url_mapped('student_detail', (('db_id', 'id'), ))
class Student(fake_db.Model):
    __tablename__ = 'students'
    id = fake_db.Column(fake_db.Integer, primary_key=True)
    name = fake_db.Column(fake_db.Text)
    level = fake_db.Column(fake_db.String, nullable=False)
    __mapper_args__ = {
        'polymorphic_on': 'level',
        'polymorphic_identity': None
    }


class StudentSMA(Student):
    __mapper_args__ = {'polymorphic_identity': 'sma'}


@url_mapped('student_smp_detail', (('db_id', 'id'), ))
class StudentSMP(Student):
    __mapper_args__ = {'polymorphic_identity': 'smp'}


class Lesson(fake_db.Model):
    __tablename__ = 'lessons'
    id = fake_db.Column(fake_db.Integer, primary_key=True)
    name = fake_db.Column(fake_db.Text)


class Organization(fake_db.Model):
    __tablename__ = 'organizations'
    id = fake_db.Column(fake_db.Integer, primary_key=True)
    name = fake_db.Column(fake_db.Text)


class SharedLibrary(Organization):
    __mapper_args__ = {'polymorphic_identity': 'shared_library'}
    extend_existing = True
    user_concurrent_borrowing_limit = fake_db.Column(
        fake_db.Integer, doc="Number of items that can be borrowed by a User at the same time.  "
                        "When `None`, Users may borrow an unlimited number of Items.")


class Item(fake_db.Model):
    __tablename__ = 'items'
    id = fake_db.Column(fake_db.Integer, primary_key=True)
    name = fake_db.Column(fake_db.Text)


class SharedLibraryItem(fake_db.Model):
    __tablename__ = 'shared_libary_items'
    id = fake_db.Column(fake_db.Integer, primary_key=True)
    shared_library_id = fake_db.Column(fake_db.Integer, fake_db.ForeignKey('organizations.id'))
    shared_library = fake_db.relationship('SharedLibrary', backref='shared_library_items')
    item_id = fake_db.Column(fake_db.Integer, fake_db.ForeignKey('items.id'))
    item = fake_db.relationship('Item', backref='shared_library_items')
    total_qty = fake_db.Column(fake_db.Integer)
    available_qty = fake_db.Column(fake_db.Integer)


class Catalog(fake_db.Model):
    __tablename__ = 'catalogs'
    id = fake_db.Column(fake_db.Integer, primary_key=True)
    name = fake_db.Column(fake_db.Text)
    shared_library_id = fake_db.Column(fake_db.Integer, fake_db.ForeignKey('organizations.id'))
    shared_library = fake_db.relationship('SharedLibrary', backref='catalogs')


@fake_app.route('/organization/<int:org_id>/catalog/<int:catalog_id>/<int:item_id>',
                endpoint='catalog_item_detail')
def catalog_item_detail(org_id, catalog_id, item_id):
    pass


@url_mapped('catalog_item_detail', (('org_id', 'catalog.shared_library.id'),
                                    ('catalog_id', 'catalog.id'),
                                    ('item_id', 'shared_library_item.item.id'), ))
class CatalogItem(fake_db.Model):
    __tablename__ = 'catalog_items'
    id = fake_db.Column(fake_db.Integer, primary_key=True)
    catalog_id = fake_db.Column(fake_db.Integer, fake_db.ForeignKey('catalogs.id'))
    catalog = fake_db.relationship('Catalog', backref='items')
    shared_library_item_id = fake_db.Column(fake_db.Integer, fake_db.ForeignKey('shared_libary_items.id'))
    shared_library_item = fake_db.relationship('SharedLibraryItem', backref='catalog_items')


class UrlMappingRegistryTests(TestCase):

    def setUp(self):
        self.req_ctx = fake_app.test_request_context('/')
        self.req_ctx.push()

    def tearDown(self):
        self.req_ctx.pop()

    def test_direct_class_matching(self):
        t = Teacher(id=123, name='Monty')
        self.assertEqual('http://localhost/teacher/123', get_url_for_model(t))

    def test_child_class_without_direct_mapping(self):
        s = StudentSMA(id=222, name='Monty')
        self.assertEqual('http://localhost/student-general/222', get_url_for_model(s))

    def test_child_class_mapping(self):
        s = StudentSMP(id=333, name='Monty')
        self.assertEqual('http://localhost/student-smp/333', get_url_for_model(s))

    def test_unmapped_class(self):
        lesson = Lesson(id=444, name='Biology')
        self.assertIsNone(get_url_for_model(lesson))

    def test_get_mapped_attribute(self):
        shared_lib = SharedLibrary(id=222, name='Shared Lib Org A')
        item1 = Item(id=112, name='Item abcd')
        shared_lib_item = SharedLibraryItem(id=999, shared_library=shared_lib,
                                            item=item1, total_qty=10, available_qty=10)
        catalog1 = Catalog(id=11, name='Catalog 123', shared_library=shared_lib)
        catalog_item = CatalogItem(id=1111, catalog=catalog1, shared_library_item=shared_lib_item)

        actual = get_mapped_attribute(catalog_item, 'catalog.shared_library.id')
        self.assertEqual(222, actual)
        actual2 = get_mapped_attribute(catalog_item, 'shared_library_item.item.id')
        self.assertEqual(112, actual2)

    def test_url_with_relationship(self):
        shared_lib = SharedLibrary(id=222, name='Shared Lib Org A')
        item1 = Item(id=112, name='Item abcd')
        shared_lib_item = SharedLibraryItem(id=999, shared_library=shared_lib,
                                            item=item1, total_qty=10, available_qty=10)
        catalog1 = Catalog(id=11, name='Catalog 123', shared_library=shared_lib)
        catalog_item = CatalogItem(id=1111, catalog=catalog1, shared_library_item=shared_lib_item)

        self.assertEqual('http://localhost/organization/222/catalog/11/112',
                         get_url_for_model(catalog_item))


