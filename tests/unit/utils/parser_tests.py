from unittest import TestCase

from flask import Flask
from flask_appsfoundry.parsers import SqlAlchemyFilterParser
from sqlalchemy import Column, String, Integer
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.ext.declarative import declarative_base

from app.utils.parsers import CountryIdFilter, LanguageIdFilter


class LanguageIdFilterTests(TestCase):
    """ Legacy filter. Currently front end submits requests with:
    ?language_id=1,2,3 which used identity foreign keys in databases.
    Currently, country codes are stored in the database with the

    """

    def test_single_languages(self):
        with fake_app.test_request_context('/?language_id=2'):
            result = FakeFilterParser().parse_args()

            self.assertEqual(len(result['filters']), 1)

            sql_expression = result['filters'][0]

            self.assertEqual(sql_expression.left.name, 'languages')
            self.assertEqual(sql_expression.right.value, ['ind', ])

    def test_single_languages(self):
        with fake_app.test_request_context('/?language_id=1,2'):
            result = FakeFilterParser().parse_args()

            self.assertEqual(len(result['filters']), 1)

            sql_expression = result['filters'][0]

            self.assertEqual(sql_expression.left.name, 'languages')
            self.assertEqual(sql_expression.right.value, ['eng', 'ind', ])


class CountryIdFilterTests(TestCase):
    """ Legacy filter.  Currently front end submits requests with:
    ?country_id=1,2,3, which used to identify foreign keys in the database.
    Currently, country codes are stored in the database with the ISO3166 code.
    """

    def test_single_countries(self):
        with fake_app.test_request_context('/?country_id=1'):

            result = FakeFilterParser().parse_args()

            self.assertEqual(len(result['filters']), 1)

            sql_expression = result['filters'][0]

            self.assertEqual(sql_expression.left.name, 'countries')
            self.assertEqual(sql_expression.right.value, ['id', ])

    def test_multiple_countries(self):
        with fake_app.test_request_context('/?country_id=1,2,3'):

            result = FakeFilterParser().parse_args()

            self.assertEqual(len(result['filters']), 1)

            sql_expression = result['filters'][0]

            self.assertEqual(sql_expression.left.name, 'countries')
            self.assertEqual(sql_expression.right.value, ['id', 'my', 'sg'])


fake_app = Flask(__name__)


class FakeOrmEntity(declarative_base()):
    __tablename__ = 'fake_orm_entity'
    id = Column(Integer, primary_key=True)
    countries = Column(ARRAY(String(3)))
    languages = Column(ARRAY(String(3)))


class FakeFilterParser(SqlAlchemyFilterParser):
    __model__ = FakeOrmEntity
    country_id = CountryIdFilter(dest='countries')
    language_id = LanguageIdFilter(dest='languages')
