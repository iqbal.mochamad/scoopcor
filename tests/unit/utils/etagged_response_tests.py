from __future__ import unicode_literals, absolute_import
from hashlib import md5
from unittest import TestCase

from flask import Flask, jsonify
from app.utils.responses import append_weak_etag

fake_app = Flask(__name__)


@fake_app.route('/', methods=['GET', 'POST', 'PUT', 'DELETE', 'PATCH', ])
def some_list():
    return jsonify({
          "categories": [
              {
                  "icon_image_normal": "images/1/category_icons/KID_1.png",
                  "sort_priority": 1,
                  "description": "",
                  "countries": None,
                  "item_type_id": 1,
                  "is_active": True,
                  "slug": "kids",
                  "media_base_url": "https://images.apps-foundry.com/magazine_static/",
                  "meta": "",
                  "parent_category_id": None,
                  "icon_image_highres": "",
                  "id": 1,
                  "name": "Kids"
              },
              {
                  "icon_image_normal": "images/1/category_icons/MEN_3.png",
                  "sort_priority": 1,
                  "description": "",
                  "countries": None,
                  "item_type_id": 1,
                  "is_active": True,
                  "slug": "mens",
                  "media_base_url": "https://images.apps-foundry.com/magazine_static/",
                  "meta": "",
                  "parent_category_id": None,
                  "icon_image_highres": "",
                  "id": 2,
                  "name": "Men's"
              },
              {
                  "icon_image_normal": "images/1/category_icons/LIF_1.png",
                  "sort_priority": 1,
                  "description": "",
                  "countries": None,
                  "item_type_id": 1,
                  "is_active": True,
                  "slug": "lifestyle",
                  "media_base_url": "https://images.apps-foundry.com/magazine_static/",
                  "meta": "",
                  "parent_category_id": None,
                  "icon_image_highres": "",
                  "id": 3,
                  "name": "Lifestyle"
              },
          ],
          "metadata": {
            "resultset": {
              "count": 93,
              "limit": 3,
              "offset": 0
            }
          }})


@fake_app.after_request
def after_req(response):
    append_weak_etag(response)
    return response


class WeakEtaggedResponseTests(TestCase):

    def setUp(self):
        self.api_client = fake_app.test_client(use_cookies=False)

    def test_weak_etag_on_response(self):

        EXPECTED_ETAG = 'w/"{weak_etag}"'.format(weak_etag=md5('1,2,3,4,5').hexdigest())

        response = self.api_client.get('/')

        self.assertEqual(
            EXPECTED_ETAG,
            response.headers.get('ETag')
        )

    def test_head_etag_on_response(self):

        EXPECTED_ETAG = 'w/"{weak_etag}"'.format(weak_etag=md5('1,2,3,4,5').hexdigest())

        response = self.api_client.head('/')

        self.assertEqual(
            EXPECTED_ETAG,
            response.headers.get('ETag')
        )


    def test_only_applies_to_http_get_or_head(self):

        non_etagged_methods = ['post', 'put', 'options', 'delete', 'patch', ]

        for method_name in non_etagged_methods:
            meth = getattr(self.api_client, method_name)
            resp = meth('/')
            if 300 < resp.status_code < 599:
                self.fail('unexpected status code: {} for method {}'.format(resp.status_code, method_name))
            self.assertNotIn('ETag', resp.headers)
