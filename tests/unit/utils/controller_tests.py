from __future__ import unicode_literals, absolute_import

import json
from unittest import TestCase

from flask import Flask, Blueprint
from flask_babel import Babel
from flask_appsfoundry import Api, Resource
from flask_appsfoundry.controllers import ErrorHandlingApiMixin

from flask_appsfoundry.exceptions import Conflict
from app.utils.controllers import CorApiErrorHandlingMixin


fake_app = Flask(__name__)
babel = Babel(fake_app)


class FakeController(CorApiErrorHandlingMixin, ErrorHandlingApiMixin, Resource):
    def get(self):
        raise Conflict

bp = Blueprint('blueprints_bro', __name__)
bp.add_url_rule('/some-url', view_func=FakeController.as_view(b'some_view'))
fake_app.register_blueprint(bp)


class CorErrorHandlingMixinTests(TestCase):

    def setUp(self):
        self.client = fake_app.test_client(use_cookies=False)

    def test_correct_status_with_blueprint(self):
        """ Proves initial regression in 500 responses where Conflict is raised.
        """
        resp = self.client.get('/some-url')
        self.assertEqual(resp.status_code, 409)

    def test_blueprint_registered_body(self):
        """ Make sure that our response body is formatted properly.
        """
        resp = self.client.get('/some-url')
        self.assertDictEqual(
            json.loads(resp.data),
            {
                'status': 409,
                'error_code': 409,
                'user_message': 'The request conflicts with the server.',
                'developer_message': 'The request conflicts with the server.',
            })
