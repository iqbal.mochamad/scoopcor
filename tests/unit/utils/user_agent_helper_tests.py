from __future__ import absolute_import, unicode_literals
from unittest import TestCase

from app.utils.useragent_helpers import construct_useragent_meta, UnparsableUserAgent
from nose.tools import raises


class UserAgentHelperTests(TestCase):

    def test_convert_simple_useragent_to_appname(self):

        user_agent = "PerPusNas iOS/1.0.0"

        user_agent_meta = construct_useragent_meta(user_agent)

        self.assertEqual(user_agent_meta.app_name, "perpusnas_ios")
        self.assertEqual(user_agent_meta.version, "1.0.0")
        self.assertIsNone(user_agent_meta.metadata)

    def test_convert_useragent_with_meta_to_appname(self):

        user_agent = "PerPusNas iOS/1.0.0 (iOS 1.2.3; iPhone 6s)"

        ua_meta = construct_useragent_meta(user_agent)

        self.assertEqual(ua_meta.app_name, "perpusnas_ios")
        self.assertEqual(ua_meta.version, "1.0.0")
        self.assertEqual(ua_meta.metadata, "iOS 1.2.3; iPhone 6s")

    @raises(UnparsableUserAgent)
    def test_unparsable_user_agent(self):
        construct_useragent_meta("Something That I Can't Parse")
