import unittest

from flask.ext.appsfoundry import marshal
from faker import Factory

from app.general.serializers import GeneralSerializer


faker = Factory.create()


class SerializerTests(unittest.TestCase):
    def setUp(self):
        self.general_info = {
            'api_status': faker.pystr(),
            'server_timestamp': faker.date_time()
        }

        self.result = marshal(self.general_info, GeneralSerializer())

    def test_api_status(self):
        self.assertEquals(
            self.general_info.get('api_status'),
            self.result.get('api_status'))

    def test_server_timestamp(self):
        self.assertEquals(
            self.general_info.get('server_timestamp').isoformat()+'+00:00',
            self.result.get('server_timestamp'))
