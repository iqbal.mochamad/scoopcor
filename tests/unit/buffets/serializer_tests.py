import unittest

from dateutil.relativedelta import relativedelta
from faker import Factory
from flask_appsfoundry import marshal

from app.offers.models import OfferBuffet, Offer
from app.offers.serializers import BuffetSerializer
from app.offers.parsers import relativedelta_from_iso8601
from app.offers.viewmodels import OfferBuffetViewmodel
from app.items.models import Item
from app.master.models import Brand
from app.offers.choices.offer_type import BUFFET
from app.offers.choices.status_type import READY_FOR_SALE


fake = Factory.create()


class BuffetSerializerTests(unittest.TestCase):

    def setUp(self):
        self.entity = OfferBuffet(
            id=fake.pyint(),
            available_from=fake.date_time(),
            available_until=fake.date_time(),
            buffet_duration=relativedelta(days=fake.pyint()),
            offer=Offer(
                name=fake.name(),
                offer_status=READY_FOR_SALE,
                offer_type_id=BUFFET,
                sort_priority=fake.pyint(),
                is_active=fake.pybool(),
                price_usd=fake.pyfloat(),
                price_idr=fake.pyint(),
                price_point=fake.pyint(),
                brands=[Brand(id=fake.pyint()) for _ in range(5)],
                items=[Item(id=fake.pyint()) for _ in range(5)]
            )
        )

        vm = OfferBuffetViewmodel(self.entity)
        self.result = marshal(vm, BuffetSerializer())

    def test_id(self):
        self.assertEquals(self.entity.id, self.result['id'])

    def test_available_from(self):
        expected = self.entity.available_from.isoformat()+"+00:00"
        self.assertEquals(expected,
                          self.result['available_from'])

    def test_available_until(self):
        expected = self.entity.available_until.isoformat()+"+00:00"
        self.assertEquals(expected,
                          self.result['available_until'])

    def test_buffet_duration(self):
        expected = relativedelta_from_iso8601(self.result['buffet_duration'])
        self.assertEquals(expected, self.entity.buffet_duration)

    def test_offer_brands(self):
        self.assertListEqual([brand.id for brand in self.entity.offer.brands],
                             self.result['offer']['brands'])

    def test_offer_items(self):
        self.assertListEqual([item.id for item in self.entity.offer.items],
                             self.result['offer']['items'])
