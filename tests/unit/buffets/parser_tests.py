import random
import unittest

from dateutil.relativedelta import relativedelta
from faker import Factory
from flask import Request
from flask.ext.appsfoundry.parsers.converters import naive_datetime_from_iso8601
from mock import MagicMock

from app.offers.parsers import OfferBuffetArgsParser
from app.offers.choices import OFFER_STATUS
from app.offers.choices.offer_type import BUFFET

fake = Factory.create()


class BuffetArgsParserTests(unittest.TestCase):

    def setUp(self):
        self.input = {
            'available_from': fake.iso8601(),
            'available_until': fake.iso8601(),
            'buffet_duration': "P10DT40S",
            'offer': {
                'name': fake.name(),
                'offer_status': random.choice(OFFER_STATUS.keys()),
                'sort_priority': fake.pyint(),
                'is_active': fake.pybool(),
                'offer_type_id': BUFFET,
                'price_usd': fake.pyfloat(),
                'price_idr': fake.pyint(),
                'price_point': fake.pyint(),
                'items': fake.pylist(10, True, int),
                'brands': fake.pylist(10, True, int),
            }
        }

        req = MagicMock(spec=Request)
        req.json = lambda: self.input

        parser = OfferBuffetArgsParser()
        self.results = parser.parse_args(req=req)

    def test_available_from(self):
        expected = naive_datetime_from_iso8601(self.input['available_from'])
        self.assertEquals(expected, self.results['available_from'])

    def test_available_to(self):
        expected = naive_datetime_from_iso8601(self.input['available_until'])
        self.assertEquals(expected, self.results['available_until'])

    def test_duration(self):
        expected = relativedelta(days=10, seconds=40)
        self.assertEquals(expected, self.results['buffet_duration'])

    def test_offer_name(self):
        self.assertEquals(self.input['offer']['name'],
                          self.results['offer']['name'])

    def test_offer_status(self):
        self.assertEquals(self.input['offer']['offer_status'],
                          self.results['offer']['offer_status'])

    def test_offer_sort_priority(self):
        self.assertEquals(self.input['offer']['sort_priority'],
                          self.results['offer']['sort_priority'])

    def test_offer_is_active(self):
        self.assertEquals(self.input['offer']['is_active'],
                          self.results['offer']['is_active'])

    def test_offer_type_id(self):
        self.assertEquals(BUFFET,
                          self.results['offer']['offer_type_id'])

    def test_offer_price_usd(self):
        self.assertEquals(self.input['offer']['price_usd'],
                          self.results['offer']['price_usd'])

    def test_offer_price_idr(self):
        self.assertEquals(self.input['offer']['price_idr'],
                          self.results['offer']['price_idr'])

    def test_offer_price_point(self):
        self.assertEquals(self.input['offer']['price_point'],
                          self.results['offer']['price_point'])

    def test_items(self):
        self.assertListEqual(self.input['offer']['items'],
                             self.results['offer']['items'])

    def test_brands(self):
        self.assertListEqual(self.input['offer']['brands'],
                             self.results['offer']['brands'])
