from __future__ import unicode_literals, absolute_import
import unittest

from app import db
from app.analytics.models import UserDownload


class UserActivityTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        cls.session = db.session()
        db.create_all()

        user_download = UserDownload(
            user_activity={
                "device_id": "123",
                "user_id": 1,
                "item_id": 1,
                "session_name": "some session",
                "ip_address": "127.0.0.1",
                "download_status": "success",
                "location": "64.54 32.64",
                "client_version": "1.2.3",
                "os_version": "abc",
                "device_model": "abc",
                "datetime": "2015-03-23T10:11:30.121365"
            }
        )
        cls.session.add(user_download)
        cls.session.commit()

        cls.args = {
            "client_id" :1,
            "client_version" :"4.7.0",
            "datetime" :"2016-04-07T07:35:15Z",
            "device_id" :"6shd93hd83h8fjdoeuejfueijdidi535fdd2c2c8de4be8c23f32df1ad976b",
            "device_model" :"iPad2,1",
            "duration" :"3.632206916809082",
            "ip_address" :"202.179.188.106",
            "item_id" :101926,
            "location" :"-6.1744 106.8294",
            "online_status" :"online",
            "os_version" :"7.1.2",
            "page_number" :[1],
            "page_orientation" :"portrait",
            "session_name" :"ecf8327442921b6a16c20215e8950993f274bbfe647bf05315001977c31402fe",
            "user_id" :442006,
            "activity_type": "pageview",
            "datetime_epoch": 1465284915.0
            }

