from datetime import datetime
import unittest
from random import choice

from mock import MagicMock
from flask import Request
from faker import Factory

from app.analytics import parsers, choices
from app.analytics.choices.activity import APPOPEN


fake = Factory.create()

class BulkParserTests(unittest.TestCase):

    def test_minimal_data_pageviews(self):
        input = {
            'pageviews': [
                {
                    'device_id': fake.word(),
                    'user_id': fake.pyint(),
                    'item_id': fake.pyint(),
                    'session_name': fake.word(),
                    'ip_address': fake.ipv4(),
                    'page_orientation': choice(choices.ORIENTATION_TYPES),
                    'page_number': fake.pylist(5, True, int),
                    'duration': fake.pydecimal(),
                    'online_status': choice(choices.ONLINE_TYPES)
                },
                {
                    'device_id': fake.word(),
                    'user_id': fake.pyint(),
                    'item_id': fake.pyint(),
                    'session_name': fake.word(),
                    'ip_address': fake.ipv4(),
                    'page_orientation': choice(choices.ORIENTATION_TYPES),
                    'page_number': fake.pylist(5, True, int),
                    'duration': fake.pydecimal(),
                    'online_status': choice(choices.ONLINE_TYPES)
                },
                {
                    'device_id': fake.word(),
                    'user_id': fake.pyint(),
                    'item_id': fake.pyint(),
                    'session_name': fake.word(),
                    'ip_address': fake.ipv4(),
                    'page_orientation': choice(choices.ORIENTATION_TYPES),
                    'page_number': fake.pylist(5, True, int),
                    'duration': fake.pydecimal(),
                    'online_status': choice(choices.ONLINE_TYPES)
                }
            ]
        }

        parser = parsers.BulkAnalyticsParser()

        req = MagicMock(spec=Request)
        req.json = lambda: input

        args = parser.parse_args(req=req)

        for actual, expected in zip(args['pageviews'], input['pageviews']):
            self.assertEqual(actual['device_id'], expected['device_id'])
            self.assertEqual(actual['user_id'], expected['user_id'])
            self.assertEqual(actual['item_id'], expected['item_id'])
            self.assertEqual(actual['session_name'], expected['session_name'])
            self.assertEqual(actual['ip_address'], expected['ip_address'])

            self.assertEqual(actual['location'], None)
            self.assertEqual(actual['client_version'], None)
            self.assertEqual(actual['os_version'], None)
            self.assertEqual(actual['device_model'], None)

            self.assertIsInstance(actual['datetime'], datetime)

            self.assertEqual(actual['activity_type'], 'pageview')

            self.assertEqual(actual['page_orientation'], expected['page_orientation'])
            self.assertEqual(actual['page_number'], expected['page_number'])
            self.assertEqual(actual['duration'], float(expected['duration']))
            self.assertEqual(actual['online_status'], expected['online_status'])

    def test_minimal_data_downloads(self):
        input = {
            'downloads': [
                {
                    'device_id': fake.word(),
                    'user_id': fake.pyint(),
                    'item_id': fake.pyint(),
                    'session_name': fake.word(),
                    'ip_address': fake.ipv4(),
                    'download_status': choice(choices.SUCCESS_TYPES)
                },
                {
                    'device_id': fake.word(),
                    'user_id': fake.pyint(),
                    'item_id': fake.pyint(),
                    'session_name': fake.word(),
                    'ip_address': fake.ipv4(),
                    'download_status': choice(choices.SUCCESS_TYPES)
                },
                {
                    'device_id': fake.word(),
                    'user_id': fake.pyint(),
                    'item_id': fake.pyint(),
                    'session_name': fake.word(),
                    'ip_address': fake.ipv4(),
                    'download_status': choice(choices.SUCCESS_TYPES)
                }
            ]
        }

        parser = parsers.BulkAnalyticsParser()

        req = MagicMock(spec=Request)
        req.json = lambda: input

        args = parser.parse_args(req=req)

        for actual, expected in zip(args['downloads'], input['downloads']):
            self.assertEqual(actual['device_id'], expected['device_id'])
            self.assertEqual(actual['user_id'], expected['user_id'])
            self.assertEqual(actual['item_id'], expected['item_id'])
            self.assertEqual(actual['session_name'], expected['session_name'])
            self.assertEqual(actual['ip_address'], expected['ip_address'])
            self.assertIsInstance(actual['datetime'], datetime)
            self.assertEqual(actual['activity_type'], 'download')
            self.assertEqual(actual['download_status'], expected['download_status'])

    def test_minimal_data_appopens(self):
        input = {
            'app_opens': [
                {
                    'device_id': fake.bs(),
                    'user_id': fake.pyint(),
                    'session_name': fake.bs(),
                    'ip_address': fake.ipv4(),
                    'datetime': fake.iso8601(),
                    'activity_type': APPOPEN
                },
                {
                    'device_id': fake.bs(),
                    'user_id': fake.pyint(),
                    'session_name': fake.bs(),
                    'ip_address': fake.ipv4(),
                    'datetime': fake.iso8601(),
                    'activity_type': APPOPEN
                },
                {
                    'device_id': fake.bs(),
                    'user_id': fake.pyint(),
                    'session_name': fake.bs(),
                    'ip_address': fake.ipv4(),
                    'datetime': fake.iso8601(),
                    'activity_type': APPOPEN
                }
            ]
        }

        parser = parsers.BulkAnalyticsParser()

        req = MagicMock(spec=Request)
        req.json = lambda: input

        args = parser.parse_args(req=req)

        for actual, expected in zip(args['app_opens'], input['app_opens']):
            self.assertEqual(actual['device_id'], expected['device_id'])
            self.assertEqual(actual['user_id'], expected['user_id'])
            self.assertEqual(actual['session_name'], expected['session_name'])
            self.assertEqual(actual['ip_address'], expected['ip_address'])

            self.assertIsInstance(actual['datetime'], datetime)
            self.assertEqual(actual['activity_type'], APPOPEN)
