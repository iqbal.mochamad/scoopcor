import unittest
from faker import Faker


class Test(unittest.TestCase):


    def test_report_allows_import(self):
        '''
        Importing the report script should not cause it to run.
        '''
        pass

    def test_report_sends_to_expected(self):
        '''
        Upon e-mailing, we should hit the addresses we expect.
        '''
        pass

    def test_queries_skip_expected(self):
        '''
        We should skip the tests that we don't expect to run.
        '''
        pass


if __name__ == "__main__":
    unittest.main()
