import json
from unittest import TestCase

import datetime

import requests
from app.utils.solr_gateway import EmailLog, EmailLogEntity
from mock import MagicMock


class EmailOutgoingTests(TestCase):

    def test_post_data(self):
        current_date = datetime.datetime.utcnow().isoformat() + 'Z'
        data = {
            'sender': 'ram@apps-foundry.com',
            'target': ['anwari@apps-foundry.com'],
            'html_template': 'Salam sayang',
            'is_error': False,
            'log_info': 'gagal kirim email',
            'send_date': current_date
        }

        BASE_URL = 'http://fake-solr/solr'
        mock_request = MagicMock(spec=requests)
        sg = EmailLog(BASE_URL, http=mock_request)

        email_log_entity = EmailLogEntity(**data)
        sg.add(email_log_entity)

        args = mock_request.post.call_args_list[0][0]
        kwargs = mock_request.post.call_args_list[0][1]
        actual_json_arg = json.loads(kwargs['data']).get('add').get('doc')

        expected_json = {
            'sender': 'ram@apps-foundry.com',
            'target': ['anwari@apps-foundry.com'],
            'html_template': 'Salam sayang',
            'is_error': False,
            'log_info': 'gagal kirim email',
            'send_date': current_date,
        }

        self.assertDictEqual(expected_json, actual_json_arg)
        self.assertEqual(len(mock_request.post.call_args_list), 1)
        self.assertEqual(args[0], "http://fake-solr/solr/email-log/update?wt=json")

