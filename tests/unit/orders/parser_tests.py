from __future__ import unicode_literals
import random
import unittest

from faker import Factory
from flask import Request
from mock import MagicMock

from app.orders.parsers import RemoteCheckoutArgsParser
from app.remote_publishing.choices import REMOTE_DATA_LOCATIONS

fake = Factory.create()


class RemoteCheckoutArgsParserTests(unittest.TestCase):

    maxDiff = None

    def setUp(self):

        self.input = {
            'sub_total': fake.pydecimal(),
            'grand_total': fake.pydecimal(),
            'sender_email': fake.email(),
            'receiver_email': fake.email(),
            'remote_service_name': random.choice(REMOTE_DATA_LOCATIONS),
            'currency_code': random.choice([u'idr', u'usd']),
            'items': [{'id': fake.pyint(),
                       'sub_total': fake.pydecimal(),
                       'grand_total': fake.pydecimal()} for x in range(3)],
            'user_message': fake.text(),
            'user_id': fake.pyint(),
            'remote_order_number': u"ORDER123",
        }

        req = MagicMock(spec=Request)
        req.json = lambda: self.input

        parser = RemoteCheckoutArgsParser()
        self.results = parser.parse_args(req=req)

    def test_sub_total(self):
        self.assertEqual(self.input['sub_total'], self.results['sub_total'])

    def test_grand_total(self):
        self.assertEqual(self.input['grand_total'], self.results['grand_total'])

    def test_sender_email(self):
        self.assertEqual(self.input['sender_email'], self.results['sender_email'])

    def test_receiver_email(self):
        self.assertEqual(self.input['receiver_email'], self.results['receiver_email'])

    def test_remote_service_name(self):
        self.assertEqual(self.input['remote_service_name'], self.results['remote_service_name'])

    def test_currency_code(self):
        self.assertEqual(self.input['currency_code'], self.results['currency_code'])

    def test_item_id(self):
        for item in self.results['items']:
            input_item = [input_line for input_line in self.input['items'] if input_line['id'] == item['id']]
            self.assertDictContainsSubset(input_item[0], item)

    def test_user_message(self):
        self.assertEqual(self.input['user_message'], self.results['user_message'])
