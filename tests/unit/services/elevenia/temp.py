# -*- coding: utf-8 -*-
from app.services.elevenia.gateway import Elevenia
from app.services.elevenia.entities import Category, Product, DeliveryTemplate

from app.services.elevenia.entities.choices.sale_types import READY_STOCK
from app.services.elevenia.entities.choices.item_conditions import NEW
from app.services.elevenia.entities.choices.tax_codes import NOT_TAXED
from app.services.elevenia.entities.exceptions import BaseApiResponseError, ValidationError

from app import app

# -------------------------------------------
# create ourselves a gateway
# we use this to communicate with the API
gateway = Elevenia(app.config['ELEVENIA_API_KEY'])


# -------------------------------------------
# get a list of our categories
cats = gateway.resource(Category).list()
print(len(cats))
cat = cats[0]

# ..then this delivery template thing
# that isn't actually useful for us
delivery_templates = gateway.resource(DeliveryTemplate).list()
print(len(delivery_templates))
delivery_template = delivery_templates[0]

# ..then products, just for interest!
products = gateway.resource(Product).list(page_num=1)
print(len(products))


# ---------------------------------------
# create a new product
p = gateway.resource(Product).new(
    name=u"Der Besüch den Alten Damme",
    #name="Derek's Second Book",
    nickname=u"Der Besüch den Alten Damme",
    product_code=2345,
    sale_type=READY_STOCK,
    weight_kilograms=0.1,
    quantity_in_stock=99999,
    item_condition=NEW,
    vat_tax_code=NOT_TAXED,
    is_sellable_to_minors=True,
    uses_sales_period=False,
    price=99000,
    after_service_details="please contact support@getscoop.com",
    return_or_exchange_details="You may exchange your copy on only Mondays at Tanah Abang",
    has_delivery_guarantee=True,
    primary_image="http://images.getscoop.com/magazine_static/images/1/21266/general_small_covers/ID_MINE2015MTH04ED14_S_.jpg",
    html_detail="<h1>LOTS OF BIG TEXT.  PLZ BUY.  K THAX</h1>",
    delivery_template_id=1,
    display_category_id=520 # religion
)


try:
    # then this automatically uploads it,
    # using the gateway
    p.save()
    print("Great!  We uploaded our product")
    print("Product ID: {}".format(p.id))
except ValidationError as e:
    print("You need to check your data")
    print(e.message)
except BaseApiResponseError as e:
    print("Elevenia sent back a bad response")
    print(e.message)
