import random
import unittest
from xml.etree import ElementTree

import os
from faker import Factory

from app.services.elevenia.entities import Product
from app.services.elevenia.entities.choices import \
    SALE_TYPES, POINT_CODE_UNITS, ITEM_CONDITIONS, TAX_CHOICES

from . import FIXTURE_DIR


fake = Factory.create()


def can_set(obj, property_name):
    return SetterTest(obj, property_name)

class SetterTest(object):

    def __init__(self, obj, property_name):
        self.obj = obj
        self.property_name = property_name

    def to(self, value):
        if callable(value):
            value = value()
        setattr(self.obj, self.property_name, value)

        try:
            assert getattr(self.obj, self.property_name) == value
        except AssertionError:
            print u"Failed Property {}: {} != {}".format(
                self.property_name,
                getattr(self.obj, self.property_name),
                value)
            raise


with open(os.path.join(FIXTURE_DIR, 'product.xml')) as f:
    product_xml = ElementTree.fromstring(f.read())



def given(entity):

    if callable(entity):
        product = Product(None, ElementTree.Element('Product'))
    else:
        product = entity

    class SetterTestCondition(object):

        def __init__(self, entity):
            self.entity = entity
            self.attribute_name = None
            self.new_value = None

        def when(self, attribute_name):
            self.attribute_name = attribute_name
            return self

        def is_set_to(self, new_value):
            setattr(self.entity, self.attribute_name, new_value)
            self.new_value = new_value
            return self

        def then_getter_returns_new_value(self):
            assert getattr(self.entity, self.attribute_name) == self.new_value
            return self

    return SetterTestCondition(product)


class ProductAttributeTests(unittest.TestCase, object):

    def setUp(self):
        self.product = Product(None, product_xml)

    def test_id(self):
        self.assertEquals(self.product.id, 1234)

    def test_nickname(self):
        self.assertEquals(self.product.nickname,
                          "Scoop's Great Product")

    def test_sale_type(self):
        self.assertEquals(self.product.sale_type,
                          '01')

    def test_display_category_number(self):
        self.assertEquals(self.product.display_category_id,
                         53)

    def test_service_type(self):
        self.assertEquals(self.product.service_type, 25)

    def test_product_name(self):
        self.assertEquals(self.product.name,
                          "Test Product - Do Not Buy This Items")

    def test_advertising_statement(self):
        self.assertEquals(self.product.advertising_statement,
                          "Hot Items Of The Month !!")

    def test_country_of_origin(self):
        self.assertEquals(self.product.country_of_origin, "03")

    def test_regional_code(self):
        self.assertEquals(self.product.regional_code, "Other: China")

    def test_product_code(self):
        self.assertEquals(self.product.scoop_item_id, "SKU-00001")

    def test_vat_tax_code(self):
        self.assertEquals(self.product.vat_tax_code, "01")

    def test_item_condition(self):
        self.assertEquals(self.product.item_condition, "01")

    def test_is_sellable_to_minors(self):
        self.assertEquals(self.product.is_sellable_to_minors, True)

    def test_product_image_1(self):
        self.assertEquals(self.product.primary_image, "http://soffice.11st.co.kr/img/layout/logo.gif")

    def test_html_detail(self):
        expected = """<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Suspendisse pulvinar vestibulum diam elementum tempus.</p>"""
        self.assertEquals(self.product.html_detail, expected)

    def test_uses_sales_period(self):
        self.assertEquals(self.product.uses_sales_period, False)

    def test_price(self):
        self.assertEquals(self.product.price, 50000.0)

    def test_quantity_in_stock(self):
        self.assertEquals(self.product.quantity_in_stock, 150)

    def test_point_unit_code(self):
        self.assertEquals(self.product.point_unit_code, "01")

    def test_after_service_details(self):
        self.assertEquals(self.product.after_service_details, "Sorry. No after sales service for this product.")

    def test_return_or_exchange_details(self):
        self.assertEquals(self.product.return_or_exchange_details, "Exchange Returns Information 070-7400-3719")

    def test_weight_kilograms(self):
        self.assertEquals(self.product.weight_kilograms, 12.1)

    def test_has_delivery_guarantee(self):
        self.assertEquals(self.product.has_delivery_guarantee, True)


class ProductWriteAttrTests(unittest.TestCase):

    TEST_ENTITY_NAME = "product"

    def __init__(self, aduh):
        super(ProductWriteAttrTests, self).__init__(aduh)

    def setUp(self):
        self.product = Product(None, product_xml)


    def test_id(self):
        can_set(self.product, "id").to(fake.pyint)

    def test_nickname(self):

        given(Product).\
            when("nickname").\
            is_set_to(fake.name()).\
            then_getter_returns_new_value()

    def test_sale_type(self):

        given(Product).\
            when("sale_type").\
            is_set_to(random.choice(SALE_TYPES)).\
            then_getter_returns_new_value()

    def test_display_category_number(self):
        # ...idk if I like this.
        # playing with making this very readable.
        given(Product).\
            when("display_category_id").\
            is_set_to(fake.pyint()).\
            then_getter_returns_new_value()

    def test_service_type(self):
        given(Product).\
            when("service_type").\
            is_set_to(26).\
            then_getter_returns_new_value()

    def test_display_category_number(self):

        given(Product).\
            when("display_category_id").\
            is_set_to(fake.pyint()).\
            then_getter_returns_new_value()

    def test_product_name(self):
        can_set(self.product, "name").to(fake.bs)

    def test_advertising_statement(self):
        can_set(self.product, "advertising_statement").to(fake.bs)

    def test_country_of_origin(self):
        can_set(self.product, "country_of_origin").to(str(fake.pyint()))

    def test_regional_code(self):
        can_set(self.product, "regional_code").to(str(fake.pyint()))

    def test_product_code(self):
        can_set(self.product, "scoop_item_id").to(fake.pystr)

    def test_vat_tax_code(self):
        new = random.choice(TAX_CHOICES)
        self.product.vat_tax_code = new
        self.assertEquals(new, self.product.vat_tax_code)

    def test_sellable_to_minors(self):
        new = not self.product.is_sellable_to_minors
        self.product.is_sellable_to_minors = new
        self.assertEquals(new, self.product.is_sellable_to_minors)

    def test_uses_sales_period(self):
        new = not self.product.uses_sales_period
        self.product.uses_sales_period = new
        self.assertEquals(new, self.product.uses_sales_period)

    def test_price(self):
        new = fake.pyint()
        self.product.price = new
        self.assertEquals(new, self.product.price)

    def test_quantity_in_stock(self):
        new = fake.pyint()
        self.product.quantity_in_stock = new
        self.assertEquals(new, self.product.quantity_in_stock)

    def test_after_service_details(self):
        new = fake.bs()
        self.product.after_service_details = new
        self.assertEquals(new, self.product.after_service_details)

    def test_return_or_exchange_details(self):
        new = fake.bs()
        self.product.return_or_exchange_details = new
        self.assertEquals(new, self.product.return_or_exchange_details)

    def test_weight_kilograms(self):
        new = fake.pyint()
        self.product.weight_kilograms = new
        self.assertEquals(new, self.product.weight_kilograms)

    def test_has_delivery_guarantee(self):
        new = not self.product.has_delivery_guarantee
        self.product.has_delivery_guarantee = new
        self.assertEquals(new, self.product.has_delivery_guarantee)

    def test_point_unit_code(self):

        def generate_new():
            candidate = random.choice(POINT_CODE_UNITS)
            return generate_new() if candidate == self.product.point_unit_code \
                else candidate

        new = generate_new()
        self.product.point_unit_code = new
        self.assertEquals(new, self.product.point_unit_code)

    def test_item_condition(self):

        def generate_new():
            candidate = random.choice(ITEM_CONDITIONS)
            return generate_new() if candidate == self.product.item_condition \
                else candidate

        new = generate_new()
        self.product.item_condition = new
        self.assertEquals(new, self.product.item_condition)


