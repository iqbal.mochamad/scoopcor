import unittest
from xml.etree import ElementTree

from app.services.elevenia.entities import Category

top_level = ElementTree.fromstring(
    """<ns2:categorys xmlns:ns2="http://skt.tmall.business.openapi.spring.service.client.domain/">
        <ns2:category>
            <depth>1</depth>
            <dispNm>Laptop / Desktop</dispNm>
            <dispNo>123</dispNo>
            <gblDlvYn>Y</gblDlvYn>
            <parentDispNo>0</parentDispNo>
        </ns2:category>
    </ns2:categorys>
    """)[0]

child_doc = ElementTree.fromstring("""<ns2:categorys xmlns:ns2="http://skt.tmall.business.openapi.spring.service.client.domain/">
    <ns2:category>
        <depth>3</depth>
        <dispNm>Atom</dispNm>
        <dispNo>7</dispNo>
        <gblDlvYn>N</gblDlvYn>
        <parentDispNo>3</parentDispNo>
    </ns2:category>
</ns2:categorys>""")[0]


class ToplevelCategoryReadAttrTests(unittest.TestCase):

    def test_depth_attribute(self):
        c = Category(None, top_level)
        self.assertEqual(c.depth, 1)

    def test_display_name_attribute(self):
        c = Category(None, top_level)
        self.assertEqual(c.name, "Laptop / Desktop")

    def test_display_number_attribute(self):
        c = Category(None, top_level)
        self.assertEqual(c.id, 123)

    def test_globally_delivery_attribute(self):
        c = Category(None, top_level)
        self.assertEqual(c.is_globally_deliverable, True)

    def test_no_parent_display_number(self):
        c = Category(None, top_level)
        self.assertEquals(0, c.parent_id)  # NOTE: zero indicates no parent id

    def test_parent_display_number(self):
        c = Category(None, child_doc)
        self.assertEqual(3, c.parent_id)
