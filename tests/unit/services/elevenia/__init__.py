import os


FIXTURE_DIR = os.path.join(
    os.path.dirname(__file__),  # /scoopcor/tests/unit/services/elevenia/
    os.pardir,  # /scoopcor/tests/unit/services/
    os.pardir,  # /scoopcor/tests/unit/
    os.pardir,  # /scoopcor/tests/
    'fixtures',
    'elevenia')
