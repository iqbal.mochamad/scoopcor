from app.services.elevenia.entities import DeliveryTemplate

import unittest
from xml.etree import ElementTree


delivery_template = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<DeliveryTemplates>
    <template>
        <dlvTmpltNm>TIKI &amp; JNE (regular service)</dlvTmpltNm>
        <dlvTmpltSeq>231437</dlvTmpltSeq>
    </template>
</DeliveryTemplates>"""


class DeliveryTemplateReadAttrTests(unittest.TestCase):

    def setUp(self):
        xml = ElementTree.fromstring(delivery_template).find('.//template')
        self.delivery_template = DeliveryTemplate(None, xml)

    def test_name(self):
        self.assertEqual(self.delivery_template.name, "TIKI & JNE (regular service)")

    def test_template_sequence(self):
        self.assertEqual(self.delivery_template.id, 231437)
