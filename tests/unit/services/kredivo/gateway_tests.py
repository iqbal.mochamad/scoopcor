from __future__ import unicode_literals
import json
import os
from unittest import TestCase

import requests
from faker import Factory
from httplib import OK
from mock import patch
from requests import HTTPError

from app.services.kredivo.kredivo_gateway import Kredivo, KredivoStatus, KredivoCheckoutResponse, \
    KredivoCheckoutError, KredivoUnexpectedError, KredivoConfirmResponse

# def mock_response():


# return MockResponse(expected, OK)
from app.services.kredivo.models import KredivoCheckoutEntity, ItemDetail, TransactionDetail, CustomerDetail


class MockResponse(object):
    def __init__(self, status_code, json_data):
        self.status_code = status_code
        self.json_data = json_data

    def json(self):
        return self.json_data


class MockRequest(object):
    def __init__(self, response):
        self.response = response

    def post(self, url, data=None, headers=None):
        return MockResponse(OK, self.response)

    def get(self, url, headers=None):
        return MockResponse(OK, self.response)


class UnexpectedError(object):
    def __init__(self, response):
        self.response = response

    def post(self, url, data=None, headers=None):
        return MockResponse(HTTPError, self.response)

    def get(self, url, data=None, headers=None):
        return MockResponse(HTTPError, self.response)


class GatewayCheckoutToKredivoTests(TestCase):

    def setUp(self):
        self.expected ={
            "status": "OK",
            "jojon": 12313,
            "message": "Checkout URL is created successfully.",
            "redirect_url": "http://dev.kredivo.com/kredivo/v2/signin"
                            "?tk=IWL5lQDsjHlArDm7LS8hucno6SDa/YNfE527CuBTw"
                            "4qG9mRWSoyFbXHgklpFqARcLvMvMFN4JVQHWqAeyziY7/uZbsxaGWKEysBobDGM1+4="
        }
        self.checkout_url = "/v2/checkout_url"
        items = [ItemDetail(
            id="BB12345678",
            name="iPhone 5S",
            price=100000,
            type="newspaper",
            url="",
            quantity=1
        ), ]
        transaction_details = TransactionDetail(
            amount=100000,
            order_id=7004,
            items=items
        )
        customer_details = CustomerDetail(
            first_name="Oemang",
            last_name="Tandra",
            email="ahmad.syafrudin@apps-foundry.com",
            phone="08971370647"
        )

        self.checkout_data = KredivoCheckoutEntity(
            transaction_details=transaction_details,
            customer_details=customer_details,
            push_uri="http://dev.apps-foundry.com/scoopcor/api/v1/payments/veritrans/cstore/validation",
            back_to_store_uri="https://getscoop.com")

    def test_success_checkout_request(self):
        gateway = Kredivo(server_key=_fake.name(), development_mode=True,
                          http_client=MockRequest(self.expected))
        response_data = gateway.checkout(self.checkout_data.serialize())
        # #check success checkout response
        self.assertEqual(KredivoStatus.ok, response_data.status)
        self.assertEquals(self.expected['message'], response_data.message)
        self.assertEquals(self.expected['redirect_url'], response_data.redirect_url)
        self.assertIsInstance(response_data, KredivoCheckoutResponse)

    def test_failed_response(self):
        with open(
            os.path.abspath(os.path.join(os.path.dirname(__file__),
                                         'fixtures',
                                         'failed-checkout.resp.json'
            ))) as fh:
            gateway = Kredivo(server_key=_fake.name(), development_mode=True,
                              http_client=MockRequest(json.load(fh)))

            self.assertRaises(
                KredivoCheckoutError,
                lambda: gateway.checkout(self.checkout_data.serialize())
            )

    def test_unexpected_error(self):
        gateway = Kredivo(server_key=_fake.name(), development_mode=True,
                          http_client=UnexpectedError("Unexpected Error Happen"))
        # #check success checkout response
        self.assertRaises(
            KredivoUnexpectedError,
            lambda: gateway.checkout(self.checkout_data.serialize())
        )


class GatewayGetStatusTests(TestCase):

    def setUp(self):
        self.mocked_response_ok = {
            "status": "OK",
            "message": "Confirmed order status.",
            "payment_type": "30_days",
            "transaction_id": "bb0380d7-2688-4e73-a668-997f3fc5cbec",
            "transaction_status": "SETTLEMENT",
            "transaction_time": 1469613243,
            "order_id": "KD125262",
            "amount": 125000,
            "fraud_status": "ACCEPT"
        }

    def test_get_payment_status_ok(self):
        gateway = Kredivo(server_key=_fake.name(), development_mode=True,
                          http_client=MockRequest(self.mocked_response_ok))
        response_data = gateway.get_payment_status(transaction_id="bb0380d7-2688-4e73-a668-997f3fc5cbec",
                                                   signature_key="asdlkfsa-dfasfs")
        # check success response
        self.assertEqual(KredivoStatus.ok.value, response_data.status)
        self.assertEquals(self.mocked_response_ok['transaction_status'], response_data.transaction_status)
        self.assertEquals(self.mocked_response_ok['transaction_id'], response_data.transaction_id)
        self.assertEquals(self.mocked_response_ok['transaction_status'], response_data.transaction_status)
        self.assertEquals(self.mocked_response_ok['fraud_status'], response_data.fraud_status)
        self.assertIsInstance(response_data, KredivoConfirmResponse)

    def test_unexpected_error(self):
        gateway = Kredivo(server_key=_fake.name(), development_mode=True,
                          http_client=UnexpectedError("Unexpected Error Happen"))
        # #check success checkout response
        self.assertRaises(
            HTTPError,
            lambda: gateway.get_payment_status(transaction_id="bb0380d7-2688-4e73-a668-997f3fc5cbec",
                                               signature_key="asdlkfsa-dfasfs")
        )


_fake = Factory.create()



