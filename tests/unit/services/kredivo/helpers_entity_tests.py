from unittest import TestCase
from flask import g
from app import app
from app.helpers import load_json_schema
from app.services.kredivo.helpers import build_kredivo_entity, build_kredivo_gateway
from app.services.kredivo.models import KredivoCheckoutEntity, ItemDetail, TransactionDetail, CustomerDetail
from app.utils.json_schema import validate_scoop_jsonschema


SERVER_KEY_DEV = app.config['KREDIVO_SERVER_KEY']


class EntityAttributeTests(TestCase, object):
    """ Test KredivoCheckoutEntity class to build kredivo entity
    """

    maxDiff = None

    def setUp(self):
        items = [ItemDetail(
            id=123,
            name="abcde",
            price=12311,
            type="newspaper",
            url="",
            quantity=123
        ), ]
        transaction_details = TransactionDetail(
            amount=9959000,
            order_id=12311,
            items=items
        )
        customer_details = CustomerDetail(
            first_name="Oemang",
            last_name="Tandra",
            email="alie@satuduatiga.com",
            phone="081513114262"
        )
        self.kredivo_entity = KredivoCheckoutEntity(
            transaction_details=transaction_details,
            customer_details=customer_details,
            push_uri="http://dev.apps-foundry.com/scoopcor/api/v1/payments/veritrans/cstore/validation",
            back_to_store_uri="https://getscoop.com")

    def test_valid_entity(self):
        schema = load_json_schema("services/kredivo", "kredivo-post")
        entity = self.kredivo_entity.serialize()
        entity['server_key'] = SERVER_KEY_DEV
        entity['payment_type'] = "30_days"
        validate_scoop_jsonschema(entity, schema)


class BuildEntityTest(TestCase, object):
    """ Test build_kredivo_entity method with data from an order api
    """

    maxDiff = None

    def setUp(self):
        with app.test_request_context('/'):
            g.current_user = None
            order_items = [{
                "offer_id": 123,
                "name": "abcde",
                "price": 12311,
                "quantity": 1
            }]

            self.kredivo_entity = build_kredivo_entity(order_id=12345, order_items=order_items,
                                                       amount=2012000, back_to_store_uri="http://www.testscoop.com")

    def test_valid_entity(self):
        schema = load_json_schema("services/kredivo", "kredivo-post")
        entity = self.kredivo_entity.serialize()
        entity['server_key'] = SERVER_KEY_DEV
        entity['payment_type'] = "30_days"
        validate_scoop_jsonschema(entity, schema)


class BuildGatewayTests(TestCase):

    def test_build_kredivo_gateway(self):
        with app.test_request_context('/'):
            kredivo_gateway = build_kredivo_gateway()
            self.assertIsNotNone(kredivo_gateway)
            self.assertEqual(kredivo_gateway.server_key, SERVER_KEY_DEV)
