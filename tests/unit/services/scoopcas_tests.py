from mock import patch, MagicMock
import unittest

from faker import Faker

from app.services import scoopcas


@patch('app.services.scoopcas.gateway.requests.get')
class ScoopCas_get_client_Tests(unittest.TestCase):

    FAKE = Faker()

    def test_expected_http_call_made(self, mock_get):
        """
        Test that the expected headers are sent with our request, and that we're
        hitting the expected URL.
        """
        mock_get.return_value.status_code = 200

        url = self.FAKE.url()
        token = self.FAKE.md5()
        client_id = self.FAKE.random_int()

        expected_headers = {
            'Authorization': token,
            'Accept': 'application/json'
        }
        expected_url = "{base_url}/clients/{id}".format(
            base_url=url, id=client_id)

        service = scoopcas.ScoopCas(url, token)
        service.get_client(client_id)

        mock_get.assert_called_once_with(expected_url, headers=expected_headers)

    def test_get_client_returns_client_info(self, mock_get):
        """
        Test that we're returning whatever JSON values were sent back from
        the server.
        """
        expected = self.FAKE.pydict()

        mock_get.return_value.status_code = 200
        mock_get.return_value.attach_mock(MagicMock(return_value=expected), 'json')

        service = scoopcas.ScoopCas(self.FAKE.url(), self.FAKE.md5())
        resp = service.get_client(self.FAKE.random_int())

        self.assertEqual(resp, expected)

    def test_non_200_response_codes_raise_ValueError(self, mock_get):
        """
        Any non-ok HTTP status codes should raise a ValueError.
        """

        mock_get.return_value.status_code = self.FAKE.random_int(201, 599)

        service = scoopcas.ScoopCas(self.FAKE.url(), self.FAKE.md5())
        self.assertRaises(ValueError,
                          lambda: service.get_client(self.FAKE.random_int()))


