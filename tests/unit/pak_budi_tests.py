from __future__ import unicode_literals, absolute_import

import json
import unittest
from httplib import BAD_REQUEST

from flask import jsonify
from six import string_types, integer_types

from app import app, db, hack_ios_payment_validate_error_format, detect_client
from tests.fixtures.helpers import generate_static_sql_fixtures


class IosPaymentValidateHack(unittest.TestCase):
    """ IOS version 4.8.0 has a bug causing a crash on payment failure.

    This is due to a bug where IOS is assuming status_code should be a STRING, instead of a number.
    """
    def setUp(self):
        db.drop_all()
        db.create_all()
        self.session = db.session()
        generate_static_sql_fixtures(self.session)

    def tearDown(self):
        self.session.close_all()
        db.drop_all()

    def test_rewrite_ios_response(self):

        with app.test_request_context(
                '/v1/payments/validate',
                headers={'User-Agent': 'scoop ios/4.8.0 (ipad4,2; iOS 10.0.2)'}
                ) as ctx:

            detect_client()

            original_response = jsonify({
                    'status': 400,
                    'error_code': 400,
                    'user_message': 'Waduh masbro',
                    'developer_message': 'Sumpfin done messed up, yo.'
                })
            original_response.status_code = BAD_REQUEST

            response = hack_ios_payment_validate_error_format(original_response)

            response_json = json.loads(response.data)

            self.assertIsInstance(response_json['status'], integer_types)
            self.assertIsInstance(response_json['error_code'], string_types)

    def test_others_not_rewritten(self):

        with app.test_request_context(
                '/v1/payments/validate',
                headers={'User-Agent': 'Literally/Anything-Else'}
                ) as ctx:

            detect_client()

            original_response = jsonify({
                'status': 400,
                'error_code': 400,
                'user_message': 'Waduh masbro',
                'developer_message': 'Sumpfin done messed up, yo.'
            })
            original_response.status_code = BAD_REQUEST

            response = hack_ios_payment_validate_error_format(original_response)

            response_json = json.loads(response.data)

            self.assertIsInstance(response_json['status'], integer_types)
            self.assertIsInstance(response_json['error_code'], integer_types)

if __name__ == '__main__':
    unittest.main()
