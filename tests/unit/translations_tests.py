from __future__ import unicode_literals, absolute_import
import json

from httplib import NOT_FOUND
from unittest import TestCase
from flask_appsfoundry.exceptions import Unauthorized, NotFound

from app import app, babel


class TranslationsTests(TestCase):

    def test_bahasa(self):
        with app.test_request_context('/') as ctx:
            original_selector_func = app.babel_instance.locale_selector_func
            # temporary set local selector function to a function that return 'id' as locale language
            app.babel_instance.locale_selector_func = lambda: 'id'
            # app.config['BABEL_DEFAULT_LOCALE'] = 'id'
            # ctx.push()
            # refresh()
            try:
                error1 = Unauthorized()

                self.assertEqual(
                    error1.user_message,
                    'Unauthorized, anda tidak bisa mengakses dengan mandat yang tersedia')
            finally:
                # restore original locale selector function
                app.babel_instance.locale_selector_func = original_selector_func

    def test_english(self):

        with app.test_request_context('/') as ctx:
            # get_locale already set to 'en' as default
            error1 = Unauthorized()

            self.assertEqual(
                error1.user_message,
                'Unauthorized, You cannot access this resource with the provided credentials.')

    def test_bahasa_localization(self):
        """ BASIC: Make sure the language can be switched to bahasa indonesia
        """
        c = localized_app.test_client(use_cookies=False)

        indonesian_response = c.get(
            '/not-found-in-bahasa', headers=[("Accept-Language", "id")])

        self.assertEqual(
            indonesian_response.status_code,
            NOT_FOUND
        )

        self.assertEqual(
            json.loads(indonesian_response.data)['user_message'],
            'Data tidak ditemukan.'
        )

    def test_english_localization_as_default(self):
        """ BASIC: Make sure the language to english as default
        """
        c = localized_app.test_client(use_cookies=False)
        # since default accept language in headers not defined, default should be english
        english_response = c.get('/not-found-in-english')

        self.assertEqual(
            english_response.status_code,
            NOT_FOUND
        )

        self.assertEqual(
            json.loads(english_response.data)['user_message'],
            'The requested resource could not be found.'
        )

    def test_english_via_header(self):
        """ BASIC: Make sure the language can be switched to english
        """
        c = localized_app.test_client(use_cookies=False)
        english_response = c.get(
            '/not-found-in-english-via-header',
            headers=[("Accept-Language", "da, en-gb;q=0.8, en;q=0.7")])

        self.assertEqual(
            english_response.status_code,
            NOT_FOUND
        )

        self.assertEqual(
            json.loads(english_response.data)['user_message'],
            'The requested resource could not be found.'
        )

localized_app = app


@localized_app.route('/not-found-in-bahasa')
def not_found_indonesian():
    raise NotFound


@localized_app.route('/not-found-in-english')
def not_found_english():
    raise NotFound


@localized_app.route('/not-found-in-english-via-header')
def not_found_english2():
    raise NotFound


""""
# generate pot file
pybabel extract -F babel.cfg -k lazy_gettext -o messages.pot .
# generate po file -- generate translations files
pybabel init -i messages.pot -d app/translations -l id
pybabel init -i messages.pot -d app/translations -l en
# generate mo files (compile po)
pybabel compile -d app/translations

# if there'r any changes or additions: -- START
# # generate pot files again
pybabel extract -F babel.cfg -k lazy_gettext -o messages.pot .
# # merge translations
pybabel update -i messages.pot -d app/translations
# # generate mo files (compile po)
pybabel compile -d app/translations
# if there'r any changes or additions: -- END
"""
