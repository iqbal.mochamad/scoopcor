from decimal import Decimal
from unittest import TestCase
from nose.tools import raises, istest, nottest
from app.campaigns.serializer import Number


@nottest
class SerializerTestBase(TestCase):

    field = None

    def test_input_none(self):
        actual = self.field.format(None)

        self.assertEqual(actual, None)


@istest
class NumberFieldTests(SerializerTestBase):
    """

    """

    field = Number()
    field.__doc__ = "OKE"

    def test_input_integer(self):
        """

        :return:
        """
        actual = self.field.format(2)

        self.assertEqual(actual, 2.0)
        self.assertIsInstance(actual, float)

    test_input_integer.__test__ = False

    def test_input_decimal(self):
        actual = self.field.format(Decimal(2))

        self.assertEqual(actual, 2.0)
        self.assertIsInstance(actual, float)

    def test_input_string(self):
        actual = self.field.format("2")

        self.assertEqual(actual, 2.0)
        self.assertIsInstance(actual, float)

    @raises(ValueError)
    def test_bad_input_string(self):
        self.field.format("abcd")

    @raises(TypeError)
    def test_bad_input_object(self):
        self.field.format(object())

