fb_data = '''{
"facebook_data":{
  "location": {
    "id": "102173726491792",
    "name": "Jakarta, Indonesia"
  },
  "birthday": "06/25/1989",
  "link": "http://www.facebook.com/1039775249",
  "last_name": "Ersan",
  "locale": "en_US",
  "name": "Harry Ersan",
  "verified": true,

  "id": "1039775249",
  "gender": "male",

  "email": "harry_25689@yahoo.co.id",
  "timezone": 7,

  "first_name": "Harry",
  "updated_time": "2015-06-05T21:05:41+0000"
},

  "client_version": "4.4.2",
  "client_id": 1,
  "os_version": "8.0.2",
  "device_model": "iPad4,1",
  "datetime": "2015-07-28T09:33:28Z",
  "ip_address": "139.0.18.154",
  "device_id": "oaly2Uijsn8Ikxnl0lakmnGtsjkludac0b708776b3e839e3a381d3019503c",
  "location" : ""
}'''

import json
import unittest
from mock import MagicMock
from flask import Request

from app.facebook_login_data.parsers import FacebookLoginDataArgsParser


class FacebookLoginParserTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        fake_req = MagicMock(spec=Request)
        fake_req.json = lambda: json.loads(fb_data)
        parser = FacebookLoginDataArgsParser()
        cls.result = parser.parse_args(req=fake_req)

    def test_facebook_data(self):
        self.assertDictEqual(self.result['facebook_data']['location'],
                         {"id": "102173726491792",
                          "name": "Jakarta, Indonesia"})
