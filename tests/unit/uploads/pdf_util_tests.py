from unittest import TestCase

from app.uploads.pdf.utils import PdfMetadata

from . import TEST_PDF_PATH


class PdfMetadataTests(TestCase):

    def test_page_count(self):
        meta = PdfMetadata(TEST_PDF_PATH)
        self.assertEqual(meta.page_count, 144)

