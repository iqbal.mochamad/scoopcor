import os
from tempfile import mkdtemp
from shutil import rmtree
from unittest import TestCase

from nose.plugins.attrib import attr
from faker import Factory

from . import TEST_PDF_PATH
from app.uploads.pdf.thumbnails import PdfBatchThumbnailExtractor


@attr('slow')
class BatchTests(TestCase):
    """ Checks the thumbnail batch generation process.

    For batch thumbnail generation, a single image should be generated for
    every single page in the input PDF.

    They should be sequentially numbered (starting at 1.png) for each page.
    """
    @classmethod
    def setUpClass(cls):

        processor = PdfBatchThumbnailExtractor(
            output_base_dir=temp_dir,
            input_pdf_path=TEST_PDF_PATH
        )

        cls.output_files = processor.process()
        cls.current_iteration = 0

    def test_thumbnails_count(self):
        """ Make sure a thumbnail was generated for each page of our PDF.
        """
        self.assertEqual(len(self.output_files), 144)

    def test_individual_thumbnails(self):
        """ Check each individual thumbnail on disk that was generated.
        """
        for i in range(1, len(self.output_files)):
            expected_output_file = os.path.join(temp_dir, '{}.png'.format(i))
            self.assertEqual(self.output_files[i], expected_output_file)
            self.assertTrue(os.path.exists(expected_output_file))
            self.assertTrue(os.path.isfile(expected_output_file))


_fake = Factory.create()

temp_dir = None

PILLOW_WIDTH_SENTINEL = 0


def setUpModule():
    global temp_dir
    temp_dir = mkdtemp()


def tearDownModule():
    if os.path.exists(temp_dir):
        rmtree(temp_dir)


class FakeItem(object):
    """ Mock item interface required by cover file processor. """
    def __init__(self, edition_code, brand_id):
        self.edition_code = edition_code
        self.brand_id = brand_id
