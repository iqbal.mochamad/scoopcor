from __future__ import unicode_literals

from faker import Factory
from nose.tools import istest

from app.items.parsers import ItemPdfContentUploadParser, \
    ItemEpubContentUploadParser
from tests.testcases.parsing import ParserTestBase


@istest
class ItemUploadPdfParserTests(ParserTestBase):

    parser_class = ItemPdfContentUploadParser

    @classmethod
    def get_request_data(cls):
        return {
            'file_name': _fake.file_name(extension='pdf'),
            'file_size': _fake.pyint(),
            'pic_id': _fake.pyint(),
            'pic_name': _fake.name(),
            'role_id': _fake.pyint(),
        }

    def test_file_name(self):
        self.assertParsedFieldMatchesInput('file_name')

    def test_file_size(self):
        self.assertParsedFieldMatchesInput('file_size')

    def test_pic_id(self):
        self.assertParsedFieldMatchesInput('pic_id')

    def test_pic_name(self):
        self.assertParsedFieldMatchesInput('pic_name')

    def test_role_id(self):
        self.assertParsedFieldMatchesInput('role_id')


@istest
class ItemUploadPdfParserTests(ParserTestBase):

    parser_class = ItemEpubContentUploadParser

    @classmethod
    def get_request_data(cls):
        return {
            'file_name': _fake.file_name(extension='epub'),
            'cover_image_file_name': _fake.file_name(extension='png'),
            'file_size': _fake.pyint(),
            'pic_id': _fake.pyint(),
            'pic_name': _fake.name(),
            'role_id': _fake.pyint(),
        }

    def test_file_name(self):
        self.assertParsedFieldMatchesInput('file_name')

    def test_cover_image_file_name(self):
        self.assertParsedFieldMatchesInput('cover_image_file_name')

    def test_file_size(self):
        self.assertParsedFieldMatchesInput('file_size')

    def test_pic_id(self):
        self.assertParsedFieldMatchesInput('pic_id')

    def test_pic_name(self):
        self.assertParsedFieldMatchesInput('pic_name')

    def test_role_id(self):
        self.assertParsedFieldMatchesInput('role_id')


_fake = Factory.create()
