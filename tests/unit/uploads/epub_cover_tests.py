"""
Runs our PDF cover image extraction with a known PDF to ensure all files
are generated properly.
"""
from unittest import TestCase
import os
from tempfile import mkdtemp
from shutil import rmtree

from faker import Factory
from PIL import Image

from app.uploads.image.covers import ImageCoverExtractorCommand
from app.uploads.choices import CoverTypes
from . import TEST_EPUB_COVER_IMAGE_PATH


class CoverImageProcessorMixin(object):

    cover_type = None
    image = None

    @classmethod
    def setUpClass(cls):
        processor = ImageCoverExtractorCommand(
            item=FakeItem(edition_code="ID_TRB2015MTH04DT23", brand_id=123),
            output_base_dir=temp_dir,
            cover_type=cls.cover_type,
            input_file_path=TEST_EPUB_COVER_IMAGE_PATH
        )
        processor.process()

        cls.image = Image.open(processor.output_file_full_path)

    @classmethod
    def tearDownClass(cls):
        cls.image.close()


class BigCoverImageGeneratorTests(CoverImageProcessorMixin, TestCase):

    cover_type = CoverTypes.cover_big

    def test_is_jpeg(self):
        self.assertEqual(self.image.format, 'JPEG')

    def test_output_width(self):
        actual_width = self.image.size[PILLOW_WIDTH_SENTINEL]
        self.assertEqual(actual_width, 760)


class GeneralSmallCoverImageGeneratorTests(CoverImageProcessorMixin, TestCase):

    cover_type = CoverTypes.cover_general_small

    def test_is_jpeg(self):
        self.assertEqual(self.image.format, 'JPEG')

    def test_output_width(self):
        actual_width = self.image.size[PILLOW_WIDTH_SENTINEL]
        self.assertEqual(actual_width, 178)


class GeneralCoverImageGeneratorTests(CoverImageProcessorMixin, TestCase):

    cover_type = CoverTypes.cover_general

    def test_is_jpeg(self):
        self.assertEqual(self.image.format, 'JPEG')

    def test_output_width(self):
        actual_width = self.image.size[PILLOW_WIDTH_SENTINEL]
        self.assertEqual(actual_width, 356)


class GeneralBigCoverImageGeneratorTests(CoverImageProcessorMixin, TestCase):

    cover_type = CoverTypes.cover_general_big

    def test_is_jpeg(self):
        self.assertEqual(self.image.format, 'JPEG')

    def test_output_width(self):
        actual_width = self.image.size[PILLOW_WIDTH_SENTINEL]
        self.assertEqual(actual_width, 380)


class ItemCoverFileProcessorTests(TestCase):

    def test_output_filename(self):
        """ Check the output file path is correctly generated.

        The format for this should be:
        <some_base_path>/<brand_id>/<cover_type>/<edition_code>.jpg
        """
        fake_item = FakeItem(edition_code="ID_TRB2015MTH04DT23", brand_id=123)

        processor = ImageCoverExtractorCommand(
            item=fake_item,
            output_base_dir='/some/random/dir',
            cover_type=CoverTypes.cover_big,
            input_file_path=None  # irrelevant to this test, but required param
        )

        self.assertEqual(
            "/some/random/dir/123/big_covers/ID_TRB2015MTH04DT23.jpg",
            processor.output_file_full_path)


_fake = Factory.create()

temp_dir = None

PILLOW_WIDTH_SENTINEL = 0


def setup_module():
    global temp_dir
    temp_dir = mkdtemp()


def teardown_module():
    if os.path.exists(temp_dir):
        rmtree(temp_dir)


class FakeItem(object):
    """ Mock item interface required by cover file processor. """
    def __init__(self, edition_code, brand_id):
        self.edition_code = edition_code
        self.brand_id = brand_id
