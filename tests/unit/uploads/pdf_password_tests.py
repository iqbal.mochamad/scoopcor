from datetime import datetime
import hashlib
from unittest import TestCase


class PdfEncryptorTests(TestCase):

    def test_file_version_generation(self):
        pass

    def test_password_generation(self):
        pass


ORIGINAL_SALT = "pou56^YHNyfvbn@#$%^YJM./-[)12efvb%tgHJkL@#RghjI()P:?!][98yh"


def original_password_gen_function(item, salt):
    """
    This is almost a straight dump of the original PDF password generator
    code that we've been using in SC1.
    """
    file_version1 = '%s%s%s%s' % (
        item.id, item.edition_code,
        datetime.now().strftime('%Y-%m-%d %H:%M:%S'), item.release_date)
    version1 = hashlib.md5(file_version1).hexdigest()

    file_version2 = '%s%s%s' % (
        item.brand_id, '0.99',
        datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    version2 = hashlib.md5(file_version2).hexdigest()

    file_version = '%s%s' % (version1, version2)

    pass_pdf = "%s%s%s%s%s%s%s" % (
        item.id, '~#%&', app.config['GARAMPDF'],
        '*&#@!', item.brand_id, '&%^)', file_version)
    return hashlib.sha256(pass_pdf).hexdigest()


class FakeItem(object):
    def __init__(self, id, edition_code, release_date, brand_id):
        self.id = id
        self.edition_code = edition_code
        self.release_date = release_date
        self.brand_id = brand_id


