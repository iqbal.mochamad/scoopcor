from unittest import SkipTest

import os


TEST_PDF_PATH = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), 'fixtures', 'poweredge.pdf')

TEST_EPUB_COVER_IMAGE_PATH = os.path.join(
    os.path.dirname(os.path.abspath(__file__)), 'fixtures', 'GormetMilo.png')

def setup_package():
    raise SkipTest
