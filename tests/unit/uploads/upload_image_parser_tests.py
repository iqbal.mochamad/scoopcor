from __future__ import unicode_literals
from faker import Factory
from flask import Request
from mock import MagicMock
from nose.tools import raises, istest
from werkzeug.exceptions import BadRequest

from app.uploads.choices import ImageResourceType
from app.uploads.parsers import UploadImageParser
from tests.testcases.parsing import ParserTestBase, RequestContextTestCase


@istest
class UploadItemImageParserTests(ParserTestBase):

    parser_class = UploadImageParser

    @classmethod
    def get_request_data(cls):
        return {
            'id': _fake.pyint(),
            'resource': 1,
            'image': _fake.file_name(extension='jpg'),
        }

    def test_id(self):
        self.assertParsedFieldMatchesInput('id')

    def test_resource(self):
        self.assertEqual(self.parsed_data['resource'], ImageResourceType.item)

    def test_image(self):
        self.assertParsedFieldMatchesInput('image')


@istest
class UploadOfferImageParserTests(ParserTestBase):

    parser_class = UploadImageParser

    @classmethod
    def get_request_data(cls):
        return {
            'id': _fake.pyint(),
            'resource': 2,
            'image': _fake.file_name(extension='jpg'),
        }

    def test_id(self):
        self.assertParsedFieldMatchesInput('id')

    def test_resource(self):
        self.assertEqual(self.parsed_data['resource'], ImageResourceType.offer)

    def test_image(self):
        self.assertParsedFieldMatchesInput('image')


@istest
class UploadImageParserSadTests(RequestContextTestCase):

    @raises(BadRequest)
    def test_invalid_input(self):
        mock_req = MagicMock(spec=Request)
        mock_req.json = lambda: {
            'id': _fake.pyint(),
            'resource': 1000,
            'image': _fake.file_name(extension='pdf')
        }

        UploadImageParser().parse_args(req=mock_req)


_fake = Factory.create()