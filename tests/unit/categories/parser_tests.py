from random import choice, randint
import unittest

from faker import Factory
from flask import Request, Flask
from mock import MagicMock

from app.items.choices import ITEM_TYPES
from app.master.parsers import CategoryArgsParser, CategoryListArgsParser
from app.utils.shims import countries

fake = Factory.create()


class CategoryArgsParserJSONTests(unittest.TestCase):
    """ Given some JSON input, validates that
    :py:meth:`app.master.parsers.CategoryArgsParser.parse_args`
    transforms our request into the expected data types
    """
    def setUp(self):

        self.input = {
            'name': fake.name(),
            'slug': fake.slug(),
            'sort_priority': fake.pyint(),
            'is_active': fake.pybool(),
            'item_type_id': choice(ITEM_TYPES.keys()),

            'description': fake.bs(),
            'meta': fake.bs(),
            'icon_image_normal': fake.word() + ".jpg",
            'icon_image_highres': fake.word() + '.jpg',

            'parent_category_id': fake.pyint(),
            'countries': list(set([randint(1, 12)
                                   for _ in range(randint(1, 5))]))
        }

        req = MagicMock(spec=Request)
        req.json = lambda: self.input

        parser = CategoryArgsParser()
        self.result = parser.parse_args(req=req)

    def test_name(self):
        self.assertEqual(self.result['name'],
                         self.input['name'])

    def test_slug(self):
        self.assertEqual(self.result['slug'],
                         self.input['slug'])

    def test_sort_priority(self):
        self.assertEqual(self.result['sort_priority'],
                         self.input['sort_priority'])

    def test_is_active(self):
        self.assertEqual(self.result['is_active'],
                         self.input['is_active'])

    def test_item_type_id(self):
        self.assertEqual(self.result['item_type'],
                         ITEM_TYPES[self.input['item_type_id']])

    def test_description(self):
        self.assertEqual(self.result['description'],
                         self.input['description'])

    def test_meta(self):
        self.assertEqual(self.result['meta'],
                         self.input['meta'])

    def test_icon_image_normal(self):
        self.assertEqual(self.result['icon_image_normal'],
                         self.input['icon_image_normal'])

    def test_icon_image_highres(self):
        self.assertEqual(self.result['icon_image_highres'],
                         self.input['icon_image_highres'])

    def test_parent_category_id(self):
        self.assertEqual(self.result['parent_category_id'],
                         self.input['parent_category_id'])

    def test_countries(self):

        expected_countries = [countries.id_to_iso3166(db_id)
                              for db_id in self.input['countries']]

        self.assertEquals(self.result['countries'],
                          expected_countries)


fake_app = Flask(__name__)


class CategoryListArgsParserJSONTests(unittest.TestCase):
    """ Filter category list with some parameter
    :py:meth:`app.master.parsers.CategoryListArgsParser.parse_args`
    """

    def test_filter_id(self):
        with fake_app.test_request_context('/?id=1'):
            result = CategoryListArgsParser().parse_args()
            self.check_assertion(result, 'id', 1)

    def test_filter_name(self):
        with fake_app.test_request_context('/?name=marzuki'):
            result = CategoryListArgsParser().parse_args()
            self.check_assertion(result, 'name', 'marzuki')

    def test_filter_slug(self):
        with fake_app.test_request_context('/?slug=what-happen-with-you'):
            result = CategoryListArgsParser().parse_args()
            self.check_assertion(result, 'slug', 'what-happen-with-you')

    def test_filter_sort_priority(self):
        with fake_app.test_request_context('/?sort_priority=1'):
            result = CategoryListArgsParser().parse_args()
            self.check_assertion(result, 'sort_priority', 1)

    def test_filter_item_type_id(self):
        item_type_id = choice(ITEM_TYPES.keys())
        with fake_app.test_request_context('/?item_type_id={}'.format(item_type_id)):
            result = CategoryListArgsParser().parse_args()
            self.check_assertion(result, 'item_type', ITEM_TYPES[item_type_id])

    def check_assertion(self, result, field_name, value):
        self.assertEqual(len(result['filters']), 1)

        sql_expression = result['filters'][0]

        self.assertEqual(sql_expression.left.name, field_name)
        self.assertEqual(sql_expression.right.value, value)
