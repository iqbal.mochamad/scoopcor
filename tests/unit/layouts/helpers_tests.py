from unittest import TestCase

from app import app
from app.layouts.banners import BannerActions
from app.layouts.helpers import get_href_for_banners


class HelpersTests(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.base_url = 'http://localhost'
        cls.layout_id = 10

    def test_get_href_for_banners(self):
        with app.test_request_context('/'):
            expected = 'http://abc/123'
            actual = get_href_for_banners(banner_type=BannerActions.open_url, payload=expected)
            self.assertEqual(actual, expected,
                             'Banner open url test failed, expected: {expected} vs actual: {actual}'.format(
                                 expected=expected, actual=actual
                             ))

            expected = None
            actual = get_href_for_banners(banner_type=BannerActions.static, payload=expected)
            self.assertEqual(actual, expected,
                             'Banner static test failed, expected: {expected} vs actual: {actual}'.format(
                                 expected=expected, actual=actual
                             ))

            expected = 'http://localhost/v1/campaigns/123/items'
            actual = get_href_for_banners(banner_type=BannerActions.promo, payload=123)
            self.assertEqual(actual, expected,
                             'Banner promo test failed, expected: {expected} vs actual: {actual}'.format(
                                 expected=expected, actual=actual
                             ))

            expected = 'http://localhost/v1/items?expand=ext&brand_id=123'
            actual = get_href_for_banners(banner_type=BannerActions.brand, payload=123)
            self.assertEqual(actual, expected,
                             'Banner brand test failed, expected: {expected} vs actual: {actual}'.format(
                                 expected=expected, actual=actual
                             ))

            expected = 'http://localhost/v1/campaigns/244/items'
            actual = get_href_for_banners(banner_type=BannerActions.buffet, payload=244)
            self.assertEqual(actual, expected,
                             'Banner buffet test failed, expected: {expected} vs actual: {actual}'.format(
                                 expected=expected, actual=actual
                             ))

            expected = 'http://localhost/v1/campaigns/123/items'
            actual = get_href_for_banners(banner_type=BannerActions.landing_page, payload=123)
            self.assertEqual(actual, expected,
                             'Banner landing page test failed, expected: {expected} vs actual: {actual}'.format(
                                 expected=expected, actual=actual
                             ))
