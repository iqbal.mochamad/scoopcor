from __future__ import unicode_literals
import random

from dateutil.parser import parse
from faker import Factory
from nose.tools import istest

from app.items.parsers import AuthorsArgsParser
from tests.testcases.parsing import ParserTestBase
from tests.helpers import (gen_api_safe_iso8601_date,
    gen_api_safe_iso8601_datetime)


@istest
class AuthorParserTests(ParserTestBase):

    parser_class = AuthorsArgsParser

    @classmethod
    def get_request_data(cls):
        return {
            'name': _fake.name(),
            'is_active': _fake.pybool(),
            'sort_priority': _fake.pyint(),
            'slug': _fake.slug(),
            'first_name': _fake.first_name(),
            'last_name': _fake.last_name(),
            'title': random.choice(['Mr.', 'Mrs', 'Miss', None]),
            'academic_title': random.choice(['Dr.', 'Phd.', None]),
            'birthdate': random.choice([
                gen_api_safe_iso8601_date,
                gen_api_safe_iso8601_datetime])(_fake.date_time()),
            'meta': _fake.text(),
        }

    def test_name(self):
        self.assertParsedFieldMatchesInput('name')

    def test_is_active(self):
        self.assertParsedFieldMatchesInput('is_active')

    def test_sort_priority(self):
        self.assertParsedFieldMatchesInput('sort_priority')

    def test_slug(self):
        self.assertParsedFieldMatchesInput('slug')

    def test_first_name(self):
        self.assertParsedFieldMatchesInput('first_name')

    def test_last_name(self):
        self.assertParsedFieldMatchesInput('last_name')

    def test_title(self):
        self.assertParsedFieldMatchesInput('title')

    def test_academic_title(self):
        self.assertParsedFieldMatchesInput('academic_title')

    def test_birthdate(self):
        self.assertParsedFieldMatchesInput(
            'birthdate',
            input_converter=lambda json_data: parse(json_data).replace(tzinfo=None))

    def test_meta(self):
        self.assertParsedFieldMatchesInput('meta')




_fake = Factory.create()
