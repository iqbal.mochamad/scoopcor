import unittest

from flask.ext.restful import marshal

from app.items.models import Author
from app.items.serializers import AuthorSerializer

from faker import Factory

fake = Factory.create()


class AuthorSerializerTests(unittest.TestCase):
    def setUp(self):
        self.datamodel = Author(
            id=fake.pyint(),
            name=fake.name(),
            sort_priority=fake.pyint(),
            slug=fake.slug(),
            is_active=fake.pybool(),
            first_name=fake.first_name(),
            last_name=fake.last_name(),
            title=fake.prefix(),
            academic_title=fake.prefix(),
            birthdate=fake.date_time(),
            meta=fake.bs())

        self.serialized = marshal(self.datamodel, AuthorSerializer())

    def test_id(self):
        self.assertEquals(self.datamodel.id, self.serialized['id'])

    def test_name(self):
        self.assertEquals(self.datamodel.name,
                          self.serialized['name'])

    def test_sort_priority(self):
        self.assertEquals(self.datamodel.sort_priority,
                          self.serialized['sort_priority'])

    def test_slug(self):
        self.assertEquals(self.datamodel.slug,
                          self.serialized['slug'])

    def test_first_name(self):
        self.assertEquals(self.datamodel.first_name,
                          self.serialized['first_name'])

    def test_last_name(self):
        self.assertEquals(self.datamodel.last_name,
                          self.serialized['last_name'])

    def test_title(self):
        self.assertEquals(self.datamodel.title,
                          self.serialized['title'])

    def test_academic_title(self):
        self.assertEquals(self.datamodel.academic_title,
                          self.serialized['academic_title'])

    def test_birthdate(self):
        self.assertEquals(self.datamodel.birthdate.date().isoformat(),
                          self.serialized['birthdate'])

    def test_meta(self):
        self.assertEquals(self.datamodel.meta,
                          self.serialized['meta'])

