from __future__ import with_statement
from alembic import context
from sqlalchemy import pool
import os
import sys
from sqlalchemy.engine import create_engine

from logging.config import fileConfig


THIS_DIR = os.path.dirname(os.path.abspath(__file__))

fileConfig(os.path.join(THIS_DIR, os.pardir, 'alembic.ini'))

sys.path.insert(0, os.path.join(THIS_DIR, os.pardir))

from app import db, app
target_metadata = db.Model.metadata


def run_migrations_offline():
    '''Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    '''
    url = app.config['SQLALCHEMY_DATABASE_URI']
    context.configure(url=url)

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    '''Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    '''
    url = app.config['SQLALCHEMY_DATABASE_URI']

    engine = create_engine(url, poolclass=pool.NullPool, echo=False)
    connection = engine.connect()
    context.configure(
        connection=connection,
        target_metadata=target_metadata
    )

    try:
        with context.begin_transaction():
            context.run_migrations()
    finally:
        connection.close()

if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
