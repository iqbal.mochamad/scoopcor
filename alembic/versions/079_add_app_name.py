"""
    079 added app name cas_organizations

"""

# revision identifiers, used by Alembic.
revision = 'lkhg724kasfm'
down_revision = '9qa53wklg9or'

from alembic import op
import sqlalchemy as sa

def upgrade():
    op.add_column('cas_organizations', sa.Column('app_name', sa.String(225)))


def downgrade():
    op.drop_column('cas_organizations', 'app_name')
