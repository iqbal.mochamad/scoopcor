"""publisher paymentgatewaycost

Revision ID: 6e392222f61
Revises: 123a239e084f
Create Date: 2015-08-06 16:10:24.160493

"""

# revision identifiers, used by Alembic.
revision = '6e392222f61'
down_revision = '123a239e084f'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'core_vendors_paymentgatewaycosts',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('vendor_id', sa.Integer(), nullable=False),
        sa.Column('paymentgatewaycost_id', sa.Integer(), nullable=False),
        sa.Column('valid_from', sa.DateTime(), nullable=False),
        sa.Column('valid_to', sa.DateTime(), nullable=False),
        sa.Column('created', sa.DateTime(), nullable=False),
        sa.Column('modified', sa.DateTime(), nullable=False),
        sa.Column('is_active', sa.Boolean(), nullable=False),
        sa.ForeignKeyConstraint(['vendor_id'], ['core_vendors.id'], ),
        sa.ForeignKeyConstraint(['paymentgatewaycost_id'], ['core_paymentgatewaycosts.id'], )
    )


def downgrade():
    op.drop_table('core_vendors_paymentgatewaycosts')
