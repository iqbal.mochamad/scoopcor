"""059b_add_platform_to_client

Revision ID: 1b9a2dea01c6
Revises: 439cb078402
Create Date: 2016-09-08 13:50:29.751812

"""

# revision identifiers, used by Alembic.
revision = '1b9a2dea01c6'
down_revision = '439cb078402'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('cas_clients',
                  sa.Column('platform_id', sa.Integer(),
                            sa.ForeignKey('core_platforms.id'),
                            nullable=False,
                            server_default=sa.literal(4)))

    op.get_bind().execute(sa.text('UPDATE cas_clients set platform_id = 4'))
    op.get_bind().execute(sa.text("UPDATE cas_clients set platform_id = 2 where name ilike '%(Android)%'"))
    op.get_bind().execute(sa.text("UPDATE cas_clients set platform_id = 1 where name ilike '%(iOS)%'"))
    op.get_bind().execute(sa.text("UPDATE cas_clients set platform_id = 3 where name ilike '%(Windows%'"))



def downgrade():
    pass
