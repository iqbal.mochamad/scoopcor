"""orderline_localized_currency

Revision ID: c87a0b416de
Revises: 2a9b8e1de2a2
Create Date: 2015-09-17 17:00:45.836841

"""

# revision identifiers, used by Alembic.
revision = 'c87a0b416de'
down_revision = '2a9b8e1de2a2'

from alembic import op
from sqlalchemy.dialects import postgresql

import sqlalchemy as sa

def upgrade():

    # create table logs renewal, record all case errors
    op.create_table(
        'core_matrixtiers',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('paymentgateway_id', sa.Integer()),
        sa.Column('tier_price', sa.DECIMAL(10, 2)),
        sa.Column('rate_price', postgresql.JSON),
        sa.Column('is_alternate', sa.Boolean()),
        sa.Column('localize_tier', sa.Text())
    )

    # adding new columns
    op.add_column('core_orderlines', sa.Column('localized_currency_code', sa.Text()))
    op.add_column('core_orderlines', sa.Column('localized_final_price', sa.DECIMAL(10, 2)))
    op.add_column('core_logs_orderstransactions', sa.Column('api', sa.Text()))

def downgrade():

    op.drop_column('core_orderlines', 'localized_currency_code')
    op.drop_column('core_orderlines', 'localized_final_price')
    op.drop_column('core_logs_orderstransactions', 'api')

