"""
    add image_banner in core_offers

    Revision ID: 11cab7ec79a9
    Revises: a3ae4167594
    Create Date: 2019-03-06 12:02:34.863000

"""

# revision identifiers, used by Alembic.
revision = 'ioa2klo6ql94'
down_revision = 'e3ef97c9076'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('core_offers', sa.Column('image_banner', sa.JSON(), nullable=True))

def downgrade():
    op.drop_column('core_offers', 'image_banner')
