"""
Add new column core_paymentvirtualaccounts

Revision ID: po20ow391jn
Revises: kidja1219ara
Create Date: 2018-12-17 13:46:06.921000

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'po20ow391jn'
down_revision = '33ab20b84e56'


midtrans_bank_account = postgresql.ENUM(u'permata', u'bca', u'mandiri', u'bni', name='midtrans_bank_account', create_type=False)
midtrans_status = postgresql.ENUM(u'pending', u'expire', u'deny', u'cancel', u'refund', u'capture', u'settlement',
                                  name='midtrans_status', create_type=False)

def upgrade():
    midtrans_bank_account.create(op.get_bind())
    midtrans_status.create(op.get_bind())

    op.create_table(
        'core_paymentvirtualaccounts',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('order_id', sa.Integer()),
        sa.Column('virtual_number', sa.String(150)),
        sa.Column('bank_account', midtrans_bank_account),
        sa.Column('status', midtrans_status),
        sa.Column('data_request', postgresql.JSONB()),
        sa.Column('data_response', postgresql.JSONB())
    )

def downgrade():
    op.drop_table('core_paymentvirtualaccounts')
