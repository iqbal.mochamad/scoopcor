"""061_buffet_offer_by_vendors

Revision ID: 3cdc05838e1e
Revises: 575666c45241
Create Date: 2016-09-30 17:44:32.548233

"""

# revision identifiers, used by Alembic.
revision = '3cdc05838e1e'
down_revision = '315d20417f4b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('core_offers_vendors',
                    sa.Column('offer_id', sa.Integer(), sa.ForeignKey('core_offers.id'), primary_key=True),
                    sa.Column('vendor_id', sa.Integer(), sa.ForeignKey('core_vendors.id'), primary_key=True)
                    )


def downgrade():
    op.drop_table('core_offers_vendors')
