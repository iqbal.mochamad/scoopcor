"""empty message

Revision ID: 3c25ffcd83d6
Revises: 48929462f016
Create Date: 2015-08-11 14:20:41.668036

"""

# revision identifiers, used by Alembic.
revision = '3c25ffcd83d6'
down_revision = '123a239e084f'

from alembic import op
import sqlalchemy as sa

def upgrade():
    op.add_column('core_userbuffets', sa.Column('is_trial', sa.Boolean(), default=False))
    op.add_column('core_applereceiptidentifiers', sa.Column('is_trial', sa.Boolean(), default=False))
    op.add_column('core_payments', sa.Column('is_trial', sa.Boolean(), default=False))

def downgrade():
    op.drop_column('core_userbuffets', 'is_trial')
    op.drop_column('core_applereceiptidentifiers', 'is_trial')
    op.drop_column('core_payments', 'is_trial')
