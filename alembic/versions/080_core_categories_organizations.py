# revision identifiers, used by Alembic.
revision = 'hakkjkk45jh1jG'
down_revision = 'lkhg724kasfm'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'core_eperpus_categories_organizations',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('organization_id', sa.Integer(), sa.ForeignKey('cas_organizations.id')),
        sa.Column('category_id', sa.Integer(), sa.ForeignKey('core_categories.id')),
        sa.Column('name', sa.String(250)),
        sa.Column('is_active', sa.Boolean(), default=True)
    )

    op.create_table(
        'core_eperpus_items_categories',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('category_id', sa.Integer(), sa.ForeignKey('core_categories.id')),
        sa.Column('item_id', sa.Integer(), sa.ForeignKey('core_items.id')),
        sa.Column('organization_id', sa.Integer(), sa.ForeignKey('cas_organizations.id')),
    )


def downgrade():
    op.drop_table('core_eperpus_items_categories')
    op.drop_table('core_eperpus_categories_organizations')
