"""Fix duplicate useritems

Revision ID: 511d992af16c
Revises: 5c26fdce8wc6
Create Date: 2015-08-14 09:13:10.019468

"""

# revision identifiers, used by Alembic.
revision = '511d992af16c'
down_revision = '5c26fdce8wc6'

from alembic import op
import sqlalchemy as sa


def upgrade():
    conn = op.get_bind()
    result = conn.execute(sa.select([core_useritems.c.user_id,
                                     core_useritems.c.item_id,
                                     core_useritems.c.user_buffet_id])
                          .group_by(
                                core_useritems.c.user_id,
                                core_useritems.c.item_id,
                                core_useritems.c.user_buffet_id)
                          .having(sa.func.count() > 1)).fetchall()

    for row in result:
        conflict_rows = conn.execute(
            core_useritems
                .select()
                .where(
                    sa.and_(
                        core_useritems.c.user_id==row.user_id,
                        core_useritems.c.item_id==row.item_id,
                        core_useritems.c.user_buffet_id==row.user_buffet_id))
            ).fetchall()

        # keep one of our conflicting rows
        conflict_rows.pop()

        # then delete all the rest
        for conflict_row in conflict_rows:
            print("removing row (user_id={0.user_id}, item_id={0.item_id}, user_buffet_id={0.user_buffet_id})".format(conflict_row))
            conn.execute(core_useritems.delete().where(core_useritems.c.id == conflict_row.id))


    #op.create_unique_constraint('unique_user_item_buffet', 'core_useritems', ['item_id', 'user_buffet_id', 'user_id'])


def downgrade():
    #op.drop_constraint('unique_user_item_buffet', 'core_useritems')
    pass


meta = sa.MetaData()

core_useritems = sa.Table(
    'core_useritems',
    meta,
    sa.Column('id', sa.Integer(), primary_key=True),
    sa.Column('item_id', sa.Integer()),
    sa.Column('user_id', sa.Integer()),
    sa.Column('user_buffet_id', sa.Integer())
)
