"""Added images to User and Organizations

Revision ID: 552ca805062
Revises: 4ef10d8d2c8
Create Date: 2016-05-28 13:30:47.127765
"""
revision = '552ca805062'
down_revision = '4ef10d8d2c8'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('cas_users', sa.Column('profile_pic_url', sa.String(length=255)))
    op.add_column('cas_organizations', sa.Column('logo_url', sa.String(length=255)))
    op.execute('alter table cas_clients add allow_age_restricted_content boolean default(true);')


def downgrade():
    op.drop_column('cas_users', 'profile_pic_url')
    op.drop_column('cas_organizations', 'logo_url')
    op.execute('alter table cas_clients drop COLUMN if exists allow_age_restricted_content')
