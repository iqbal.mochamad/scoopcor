""" Drop core_logs_buffetpage (never used)

Revision ID: 23e81126ae01
Revises: ('580d447b2201', '342f5385bf17', '302b7deffe07')
Create Date: 2016-06-23 18:54:16.915876

"""
revision = '23e81126ae01'
down_revision = ('580d447b2201', '342f5385bf17', '302b7deffe07')

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_table('core_logs_buffetpage')


def downgrade():
    op.create_table(
        'core_logs_buffetpage',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('item_id', sa.Integer),
        sa.Column('total_page', sa.Integer),
        sa.Column('status', sa.Integer),
        sa.Column('error', sa.Text),
        sa.Column('created', sa.DateTime, nullable=False),
        sa.Column('modified', sa.DateTime, nullable=False),
    )
