"""056_author_add_pic_url

Revision ID: 329903c4443f
Revises: 4f34cbc597a2
Create Date: 2016-08-01 10:38:38.418688

"""

# revision identifiers, used by Alembic.
revision = '329903c4443f'
down_revision = '4f34cbc597a2'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('core_authors', sa.Column('profile_pic_url', sa.String(length=255)))


def downgrade():
    op.drop_column('core_authors', 'profile_pic_url')
