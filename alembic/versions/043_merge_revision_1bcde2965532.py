"""Merge revision

Revision ID: 1bcde2965532
Revises: ('2c789ba6f45e', '42f80822fd33')
Create Date: 2015-12-15 16:44:00.324627

"""

# revision identifiers, used by Alembic.
revision = '1bcde2965532'
down_revision = ('2c789ba6f45e', '42f80822fd33')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
