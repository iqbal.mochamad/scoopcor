"""Merge revision

Revision ID: 1adab9664c43
Revises: ('faeebe69671', '57c07fcc746d')
Create Date: 2015-11-03 15:38:15.508582

"""

# revision identifiers, used by Alembic.
revision = '1adab9664c43'
down_revision = ('faeebe69671', '57c07fcc746d')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
