"""Apply initial Elevenia category Mappings

This migration does not include any Schema migration code.  It only
populates some initial data into the 'core_remote_categories' table.

Revision ID: 4b83b28b27db
Revises: 2d8640d2b4ea
Create Date: 2015-06-01 11:46:03.426865

"""
revision = '4b83b28b27db'
down_revision = '2d8640d2b4ea'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import ENUM
from collections import namedtuple


def upgrade():
    conn = op.get_bind()
    for cat_map in mappings:
        conn.execute(core_remote_categories.insert().values(
            category_id=cat_map.scoop_category_id,
            remote_service=ELEVENIA,
            remote_id=str(cat_map.elevenia_category_id)
        ))


def downgrade():
    conn = op.get_bind()
    conn.execute(
        core_remote_categories
        .delete()
        .where(remote_service=ELEVENIA))


ELEVENIA = u'elevenia'

REMOTE_DATA_LOCATIONS = [
    ELEVENIA,
]

# Frozen Schema
meta = sa.MetaData()
core_remote_categories = sa.Table(
    'core_remote_categories',
    meta,
    sa.Column('category_id',
              sa.Integer,
               nullable=False),
    sa.Column('remote_service',
              ENUM(*REMOTE_DATA_LOCATIONS, name='remote_data_location'),
              nullable=False),
    sa.Column('remote_id', sa.Integer, nullable=False),
    sa.PrimaryKeyConstraint('category_id', 'remote_service'),
    sa.ForeignKeyConstraint(['category_id'], ['core_categories.id'])
)

# Hardcoded mapping of the
# scoop categories id's<->elevenia category identifiers.
# This data was provided by marketing.
CategoryMapping = namedtuple('CategoryMapping', ['scoop_category_id',
                                                 'elevenia_category_id', ])
mappings = [
    CategoryMapping(
        scoop_category_id=t[0],
        elevenia_category_id=t[1])
    for t in [
        # BOOKS ONLY
        (17, 526),
        (18, 521),
        (19, 524),
        (20, 509),
        (21, 510),
        (22, 525),
        (23, 522),
        (24, 526),
        (25, 526),
        (26, 526),
        (27, 522),
        (28, 526),
        (29, 526),
        (30, 515),
        (31, 526),
        (32, 511),
        (33, 526),
        (34, 524),
        (35, 526),
        (36, 520),
        (37, 514),
        (38, 526),
        (39, 523),
        (40, 523),
        (74, 526),
        (78, 520),
        (84, 510),
        (85, 514),
        (86, 517),
        (87, 526),

        # MAGAZINES
        (1, 3526),
        (2, 3506),
        (3, 3523),
        (4, 3512),
        (5, 3515),
        (6, 3510),
        (7, 3531),
        (8, 3518),
        (9, 3508),
        (10, 3513),
        (11, 3517),
        (12, 3521),
        (13, 3519),
        (16, 3509),
        (41, 3507),
        (49, 3520),
        (50, 3529),
        (51, 3528),
        (52, 3525),
        (53, 3516),
        (54, 3514),
        (55, 3524),
        (58, 3527),
        (83, 3511),
    ]
]
