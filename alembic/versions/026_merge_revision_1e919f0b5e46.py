"""Merge revision

Revision ID: 1e919f0b5e46
Revises: ('c87a0b416de', '2be971531911', u'4174e8cca59d')
Create Date: 2015-10-28 14:57:25.406589

"""

# revision identifiers, used by Alembic.
revision = '1e919f0b5e46'
down_revision = ('c87a0b416de', '2be971531911', u'4174e8cca59d')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
