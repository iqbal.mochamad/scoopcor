"""
    Add new table core_report_data_modified

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'kioqllso12si'
down_revision = 'koqioiyrs12'

def upgrade():

    op.create_table(
        'core_report_data_modified',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_id', sa.Integer()),
        sa.Column('data_id', sa.Integer()),
        sa.Column('data_url', sa.Text),
        sa.Column('data_method', sa.String(50)),
        sa.Column('data_request', postgresql.JSONB())
    )

def downgrade():
    op.drop_table('core_report_data_modified')

