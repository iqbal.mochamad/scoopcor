"""066b_add_wish_list_table

Revision ID: 22d0a3d97ef1
Revises: 21e47d85f6cf
Create Date: 2016-11-15 15:44:49.109963

"""

# revision identifiers, used by Alembic.
revision = '22d0a3d97ef1'
down_revision = '21e47d85f6cf'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'wish_list',
        sa.Column('user_id', sa.Integer(), sa.ForeignKey('cas_users.id')),
        sa.Column('item_id', sa.Integer(), sa.ForeignKey('core_items.id')),
        sa.Column('created', sa.DateTime(timezone=True))
    )
    op.get_bind().execute(sa.text('ALTER TABLE wish_list ALTER created SET DEFAULT now()'))
    op.execute("CREATE UNIQUE INDEX wish_list_user_item_unique_idx "
               "ON wish_list (user_id, item_id) ")


def downgrade():
    op.drop_table('wish_list')
