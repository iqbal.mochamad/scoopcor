"""
    Add new table core_report_data_eperpus
"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'logeperpus01'
down_revision = 'landingpage01'


def upgrade():
    op.create_table(
        'core_report_data_eperpus',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_id', sa.Integer()),
        sa.Column('item_id', sa.Integer()),
        sa.Column('latest_quantity_available', sa.Integer()),
        sa.Column('organization_id', sa.Integer())
    )


def downgrade():
    op.drop_table('core_report_data_eperpus')
