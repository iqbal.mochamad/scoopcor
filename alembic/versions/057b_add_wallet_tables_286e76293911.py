"""052_add_wallet_tables

Revision ID: 286e76293911
Revises: 333545ef8c9c
Create Date: 2016-06-22 16:50:33.474330

"""

# revision identifiers, used by Alembic.
revision = '286e76293911'
down_revision = '333545ef8c9c'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'wallets',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('party_id', sa.Integer(), sa.ForeignKey('parties.id')),
        sa.Column('balance', sa.Numeric(12, 2)),
        sa.Column('created', sa.DateTime(timezone=True)),
        sa.Column('modified', sa.DateTime(timezone=True)),
        sa.Column('is_active', sa.Boolean)
    )
    op.execute('alter table wallets alter balance set default 0')

    op.execute('alter table wallets alter is_active set default true')

    op.create_table(
        'wallet_transactions',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('wallet_id', sa.Integer(), sa.ForeignKey('wallets.id')),
        sa.Column('amount', sa.Numeric(12, 2)),
        sa.Column('created_by', sa.Integer(), sa.ForeignKey('cas_users.id')),
        sa.Column('order_id', sa.Integer(), sa.ForeignKey('core_orders.id')),
        sa.Column('is_active', sa.Boolean),
        sa.Column('created', sa.DateTime(timezone=True)),
        sa.Column('modified', sa.DateTime(timezone=True))
    )
    op.execute('alter table wallet_transactions alter is_active set default true')


def downgrade():
    op.drop_table('wallet_transactions')
    op.drop_table('wallets')
