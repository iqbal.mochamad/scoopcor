"""merge alembic migration

Revision ID: 2c789ba6f45e
Revises: ('fd3d6b2ab10', 'a3020dc9e80')
Create Date: 2015-12-10 13:17:49.593047

"""

# revision identifiers, used by Alembic.
revision = '2c789ba6f45e'
down_revision = ('fd3d6b2ab10', 'a3020dc9e80')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
