"""059_add_layout_control

Revision ID: 439cb078402
Revises: 4037ce706844
Create Date: 2016-08-22 13:19:24.789906

"""

# revision identifiers, used by Alembic.
from sqlalchemy.dialects import postgresql

revision = '439cb078402'
down_revision = '4037ce706844'

from alembic import op
import sqlalchemy as sa


layout_types_enum = postgresql.ENUM(
    u'latest all', u'latest book', u'latest magazine', u'latest newspaper',
    u'popular all', u'popular book', u'popular magazine', u'popular newspaper',
    u'editor pick banner', u'editor pick offers',
    name='layout_type', create_type=False)


def upgrade():
    layout_types_enum.create(op.get_bind())

    op.create_table('layouts',
                    sa.Column('id', sa.Integer(), primary_key=True),
                    sa.Column('name', sa.Text()),
                    sa.Column('sort_priority', sa.Integer()),
                    sa.Column('layout_type', layout_types_enum),
                    sa.Column('created', sa.DateTime(timezone=True)),
                    sa.Column('modified', sa.DateTime(timezone=True)),
                    sa.Column('is_active', sa.Boolean)
                    )
    op.execute('ALTER TABLE layouts ALTER is_active SET DEFAULT TRUE')

    op.create_table('layout_offers',
                    sa.Column('layout_id', sa.Integer(), sa.ForeignKey('layouts.id'), primary_key=True),
                    sa.Column('offer_id', sa.Integer(), sa.ForeignKey('core_offers.id'), primary_key=True),
                    sa.Column('sort_priority', sa.Integer()),
                    sa.Column('created', sa.DateTime(timezone=True))
                    )

    op.create_table('layout_banners',
                    sa.Column('layout_id', sa.Integer(), sa.ForeignKey('layouts.id'), primary_key=True),
                    sa.Column('banner_id', sa.Integer(), sa.ForeignKey('core_banners.id'), primary_key=True),
                    sa.Column('sort_priority', sa.Integer()),
                    sa.Column('created', sa.DateTime(timezone=True))
                    )

    op.create_table('layout_intro',
                    sa.Column('brand_id', sa.Integer(), sa.ForeignKey('core_brands.id'), primary_key=True),
                    sa.Column('sort_priority', sa.Integer()),
                    sa.Column('created', sa.DateTime(timezone=True))
                    )


def downgrade():
    op.drop_table('layout_offers')
    op.drop_table('layout_banners')
    op.drop_table('layouts')
    op.drop_table('layout_intro')

