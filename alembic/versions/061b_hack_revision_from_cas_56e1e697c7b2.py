"""061b_hack_revision_from_cas

Revision ID: 56e1e697c7b2
Revises: 575666c45241
Create Date: 2016-10-07 13:06:23.303598

"""

# revision identifiers, used by Alembic.
revision = '315d20417f4b'
down_revision = '575666c45241'

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
