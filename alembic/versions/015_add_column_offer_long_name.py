"""Add column offer long name

Revision ID: 4b8f8fbfecdd
Revises: c87a0b416de
Create Date: 2015-10-27 12:57:57.227556

"""

# revision identifiers, used by Alembic.
revision = '4b8f8fbfecdd'
down_revision = 'c87a0b416de'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('core_offers', sa.Column('long_name', sa.Text()))


def downgrade():
    op.drop_column('core_offers', 'long_name')
