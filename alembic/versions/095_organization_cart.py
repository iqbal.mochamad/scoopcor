"""Organization Cart

Revision ID: 401fd7190336
Revises: 3f259c6cdcd6
Create Date: 2019-03-25 15:25:43.719000

"""

# revision identifiers, used by Alembic.
revision = '401fd7190336'
down_revision = 'k9ty4121anw4'

import sqlalchemy as sa
from alembic import op


def upgrade():
    op.create_table(
        'core_organization_cart',
        sa.Column('id', sa.INTEGER, primary_key=True),
        sa.Column('created', sa.DateTime(timezone=True)),
        sa.Column('modified', sa.DateTime(timezone=True)),
        sa.Column('organization_id', sa.Integer(), sa.ForeignKey('cas_organizations.id')),
        sa.Column('is_active', sa.Boolean(), default=True)
    )
    op.create_table(
        'core_organization_cart_offer',
        sa.Column('id', sa.INTEGER, primary_key=True),
        sa.Column('created', sa.DateTime(timezone=True)),
        sa.Column('modified', sa.DateTime(timezone=True)),
        sa.Column('cart_id', sa.Integer(), sa.ForeignKey('core_organization_cart.id')),
        sa.Column('offer_id', sa.Integer(), sa.ForeignKey('core_offers.id')),
        sa.Column('quantity', sa.SmallInteger()),
    )


def downgrade():
    op.drop_table('core_organization_cart')
    op.drop_table('core_organization_cart_offer')
