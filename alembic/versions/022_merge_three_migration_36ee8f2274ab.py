"""merge three migration

Revision ID: 36ee8f2274ab
Revises: (u'4174e8cca59d', '33bb98c9e370', '2be971531911')
Create Date: 2015-10-28 16:14:15.513800

"""

# revision identifiers, used by Alembic.
revision = '36ee8f2274ab'
down_revision = (u'4174e8cca59d', '33bb98c9e370', '2be971531911')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
