"""Merge

Revision ID: 575666c45241
Revises: ('1b9a2dea01c6', '33d46026e1c')
Create Date: 2016-09-19 08:44:45.456693

"""

# revision identifiers, used by Alembic.
revision = '575666c45241'
down_revision = ('1b9a2dea01c6', '33d46026e1c')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
