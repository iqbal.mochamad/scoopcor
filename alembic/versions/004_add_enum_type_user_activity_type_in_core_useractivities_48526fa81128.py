"""add enum type user_activity_type in core_useractivities

Revision ID: 48526fa81128
Revises: 391e4138e9fe
Create Date: 2015-07-03 12:54:23.499087

"""

# revision identifiers, used by Alembic.
revision = '48526fa81128'
down_revision = '4b83b28b27db'

from alembic import op
import sqlalchemy as sa



def upgrade():

    conn = op.get_bind()
    conn.execute("COMMIT;")
    conn.execute("ALTER TYPE user_activity_type ADD VALUE IF NOT EXISTS 'migration_guest_login';")
    conn.execute("BEGIN READ WRITE;")

    op.add_column('core_useractivities',
                  sa.Column('old_device_id', sa.Text())
                  )


def downgrade():
    raise NotImplementedError("Cannot downgrade past this revision.")
    op.alter_column("core_useractivities", "item_id", nullable=False)
