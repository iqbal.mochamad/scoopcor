"""
    076 added shared library item

"""

# revision identifiers, used by Alembic.
revision = 'ijs487wwopja'
down_revision = 'c1sk429hldsn'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'cas_shared_libraries_borrowing_items',
        sa.Column('id', sa.INTEGER, primary_key=True),
        sa.Column('organization_id', sa.Integer(), sa.ForeignKey('cas_organizations.id')),
        sa.Column('catalog_id', sa.Integer(), sa.ForeignKey('core_catalogs.id')),
        sa.Column('item_id', sa.Integer(), sa.ForeignKey('core_items.id')),
        sa.Column('borrowing_time_limit', sa.Interval())
    )

def downgrade():
    op.drop_table('cas_shared_libraries_borrowing_items')
