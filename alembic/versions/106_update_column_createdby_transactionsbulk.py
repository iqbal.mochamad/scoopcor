"""
    added created_by for core_eperpus_transactions
"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'createdby01'
down_revision = 'logeperpus01'


def upgrade():
    op.add_column('core_eperpus_transactions', sa.Column('created_by', sa.Integer()))


def downgrade():
    op.drop_column('core_eperpus_transactions', 'created_by')
