"""067_apple_ios7_receipt_logs

Revision ID: 27862c4c3cdd
Revises: 22d0a3d97ef1
Create Date: 2017-02-01 15:02:01.225936

"""

# revision identifiers, used by Alembic.

revision = '27862c4c3cdd'
down_revision = '22d0a3d97ef1'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

log_receipt_status_enum = postgresql.ENUM(u'new', u'in progress', u'completed', u'error',
                                          name='log_receipt_status', create_type=False)

log_receipt_activity_type_enum = postgresql.ENUM(
    u'purchase', u'renewal', u'restore',
    name='log_receipt_activity', create_type=False)


def upgrade():
    op.create_table(
        'log_receipts',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_id', sa.Integer(), sa.ForeignKey('cas_users.id')),
        sa.Column('activity_type', log_receipt_activity_type_enum),
        sa.Column('status', log_receipt_status_enum),
        sa.Column('created', sa.DateTime(timezone=True)),
        sa.Column('device_registration_id', sa.String()),
        sa.Column('error', sa.TEXT()),
        sa.Column('receipt_data', sa.TEXT()),
        sa.Column('receipt_json', postgresql.JSONB()),
        sa.Column('modified', sa.DateTime(timezone=True)),
    )
    op.get_bind().execute(sa.text('ALTER TABLE log_receipts ALTER created SET DEFAULT now()'))
    op.get_bind().execute(sa.text('ALTER TABLE log_receipts ALTER modified SET DEFAULT now()'))
    op.execute("CREATE INDEX log_receipts_user_id_idx "
               "ON log_receipts (user_id, activity_type, status) ")

    op.add_column('core_paymentappleiaps',
                  sa.Column('receipt_version', sa.Integer(), nullable=True,
                            server_default=sa.literal(6)))
    # op.get_bind().execute(sa.text('Update core_paymentappleiaps set receipt_version=6'))


def downgrade():
    op.drop_table('log_receipts')
