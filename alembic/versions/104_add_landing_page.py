"""
    add new table core_landingpage
"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

revision = 'landingpage01'
down_revision = 'kioqllso12si'


def upgrade():
    op.create_table(
        'core_landingpage',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('organization_id', sa.Integer()),
        sa.Column('slug', sa.String()),
        sa.Column('data', postgresql.JSONB()),
    )


def downgrade():
    op.drop_table('core_landingpage')
