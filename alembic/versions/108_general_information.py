"""
    Add new table core_informations

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

revision = 'general01'
down_revision = 'orgcatalog01'

information_types = postgresql.ENUM(u'tnc', u'privacy', name='information_types', create_type=False)


def upgrade():
    information_types.create(op.get_bind())

    op.create_table(
        'core_informations',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('title', sa.String(250)),
        sa.Column('content', sa.Text()),
        sa.Column('information_type', information_types),
        sa.Column('language', sa.String(2))
    )

def downgrade():
    op.drop_table('core_informations')

