"""add is_internal_content in core_items

Revision ID: 11cab7ec79a9
Revises: a3ae4167594
Create Date: 2019-02-20 12:02:34.863000

"""

# revision identifiers, used by Alembic.
revision = '11cab7ec79a9'
down_revision = 'a3ae4167594'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("core_items",
                  sa.Column('is_internal_content', sa.Boolean(),
                            default=False))
    op.get_bind().execute(
        sa.text('update core_items set is_internal_content = false where is_internal_content is null;'))


def downgrade():
    op.drop_column('core_items', 'is_internal_content')
