"""Merge revision

Revision ID: 981a31e9f01
Revises: ('388b389dc923', '23399b54c4e6')
Create Date: 2015-11-12 13:00:49.446342

"""

# revision identifiers, used by Alembic.
revision = '981a31e9f01'
down_revision = ('388b389dc923', '23399b54c4e6')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
