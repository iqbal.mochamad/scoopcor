"""060_add_kredivo_related_fields

Revision ID: 33d46026e1c
Revises: 439cb078402
Create Date: 2016-09-09 17:34:05.447180

"""

# revision identifiers, used by Alembic.
revision = '33d46026e1c'
down_revision = '439cb078402'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('core_payments', sa.Column('merchant_params', sa.JSON(),
                                             nullable=True))

def downgrade():
    op.drop_column('core_payments', 'merchant_params')
