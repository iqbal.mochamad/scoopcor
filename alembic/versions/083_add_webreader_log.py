"""Add new table web_reader_processed

Revision ID: 3f02309aa60b
Revises: CvHGoi7hbhF
Create Date: 2018-09-10 16:00:06.921000

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '789opql2xmaksl'
down_revision = '3f02309aa60b'


def upgrade():
    op.create_table(
        'web_reader_processed',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('item_id', sa.Integer(), sa.ForeignKey('core_items.id')),
        sa.Column('is_processed', sa.Boolean(), default=True),
        sa.Column('is_web_reader', sa.Boolean(), default=True),
        sa.Column('error', sa.String(250))
    )

def downgrade():
    op.drop_table('web_reader_processed')
