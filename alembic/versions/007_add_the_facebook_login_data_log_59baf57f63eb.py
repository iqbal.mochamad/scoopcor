"""add the facebook login data log

Revision ID: 59baf57f63eb
Revises: 5356fa66515b
Create Date: 2015-07-27 16:40:09.118538

"""

# revision identifiers, used by Alembic.
revision = '59baf57f63eb'
down_revision = '5356fa66515b'

from datetime import datetime

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql
from geoalchemy2 import Geometry


def upgrade():
    op.create_table(
        'core_logs_facebooklogindata',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('facebook_data', postgresql.JSON, nullable=False),
        sa.Column('location', Geometry('POINT')),
        sa.Column('client_version', sa.Text()),
        sa.Column('device_id', sa.Text()),
        sa.Column('datetime', sa.DateTime()),
        sa.Column('os_version', sa.Text()),
        sa.Column('device_model', sa.Text()),
        sa.Column('client_id', sa.Integer()),
        sa.Column('ip_address', sa.Text()),
        sa.Column('is_active', sa.Boolean()),
        sa.Column('created', sa.DateTime(), default=datetime.utcnow)
    )

def downgrade():
    op.drop_table('core_logs_facebooklogindata')
