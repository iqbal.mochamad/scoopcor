"""Add Banner.is_age_restricted

Revision ID: 4ef10d8d2c8
Revises: 1e2aa4927d24
Create Date: 2016-05-17 06:54:52.262516

"""

# revision identifiers, used by Alembic.
revision = '4ef10d8d2c8'
down_revision = '1e2aa4927d24'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('core_banners', sa.Column('is_age_restricted', sa.Boolean))

    op.get_bind().execute('UPDATE core_banners SET is_age_restricted = false')

    op.alter_column('core_banners', 'is_age_restricted', nullable=False) #, default=False)


def downgrade():
    op.drop_column('core_banners', 'is_age_restricted')
