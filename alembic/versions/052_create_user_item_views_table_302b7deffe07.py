"""create user item views table

Revision ID: 302b7deffe07
Revises: 4f7f99d1dae0
Create Date: 2016-06-16 15:05:25.164558

"""

# revision identifiers, used by Alembic.
from datetime import datetime

revision = '302b7deffe07'
down_revision = '4f7f99d1dae0'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import JSONB


def upgrade():
    op.create_table(
        'user_item_view',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_activity', JSONB(), nullable=False),
        schema='analytics'
    )


def downgrade():
    op.drop_table('user_item_view')
