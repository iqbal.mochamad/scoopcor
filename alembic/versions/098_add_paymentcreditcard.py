"""
Add new table core_paymentcreditcards & core_paymentcreditcardtokens

Revision ID: po20ow391jn
Revises: kidja1219ara
Create Date: 2018-12-17 13:46:06.921000

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'katw278ols69k'
down_revision = 'hjiquq32241'

midtrans_status = postgresql.ENUM(u'pending', u'expire', u'deny', u'cancel', u'refund', u'capture', u'settlement',
                                  name='midtrans_status', create_type=False)

def upgrade():
    midtrans_status.create(op.get_bind())

    op.create_table(
        'core_paymentcreditcards',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('order_id', sa.Integer()),
        sa.Column('save_token', sa.String(250)),
        sa.Column('status', midtrans_status),
        sa.Column('data_request', postgresql.JSONB()),
        sa.Column('data_response', postgresql.JSONB())
    )

    op.create_table(
        'core_paymentcreditcardtokens',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_id', sa.Integer()),
        sa.Column('masked_credit_card', sa.String(250)),
        sa.Column('expiration', sa.DateTime()),
        sa.Column('billing_address', postgresql.JSONB()),
        sa.Column('first_name', sa.String(100)),
        sa.Column('last_name', sa.String(100)),
        sa.Column('is_active', sa.Boolean()),
        sa.Column('bin_bank', sa.String(50)),
        sa.Column('bin_bank_code', sa.String(50)),
        sa.Column('bin_class', sa.String(50)),
        sa.Column('bin_types', sa.String(50)),
        sa.Column('bin_brand', sa.String(50)),
        sa.Column('bin_country_code', sa.String(50))
    )

def downgrade():
    op.drop_table('core_paymentcreditcards')
    op.drop_table('core_paymentcreditcardtokens')

