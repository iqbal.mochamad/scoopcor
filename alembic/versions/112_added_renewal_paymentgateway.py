"""
    Add new column is_renewal for core_paymentgateways

"""
import sqlalchemy as sa
from alembic import op

revision = 'payrenewal01'
down_revision = 'mcgrahill01'


def upgrade():
    op.add_column("core_paymentgateways", sa.Column('is_renewal', sa.Boolean(), default=False))


def downgrade():
    op.drop_column('core_paymentgateways', 'is_renewal')

