"""add column card_type for core_paymentgatewaycosts

Revision ID: 3b5f76ef5ed8
Revises: 123a239e084f
Create Date: 2015-08-05 14:36:43.789425

"""

# revision identifiers, used by Alembic.
revision = '3b5f76ef5ed8'
down_revision = '123a239e084f'

from alembic import op
from sqlalchemy.dialects import postgresql as pg
import sqlalchemy as sa


def upgrade():
    op.add_column('core_paymentgatewayformulas', sa.Column(
        'card_type',
        pg.ENUM('debit', 'credit', name='payment_card_types'),
        nullable=True
    ))
    op.drop_column('core_paymentgatewaycosts', 'default_max_amount')
    op.drop_column('core_paymentgatewaycosts', 'default_min_amount')


def downgrade():
    op.drop_column('core_paymentgatewaycosts', 'card_type')
    op.add_column('core_paymentgatewaycosts', sa.Column(
        'default_max_amount', sa.DECIMAL(10, 4), nullable=False))
    op.add_column('core_paymentgatewaycosts', sa.Column(
        'default_min_amount', sa.DECIMAL(10, 4), nullable=False))

CREDIT_CARD = 'credit'
DEBIT_CARD = 'debit'
PAYMENT_CARD_TYPES = (
    CREDIT_CARD,
    DEBIT_CARD,
)
