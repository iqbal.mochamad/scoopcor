"""create log for google iab 

Revision ID: 187c22028ca4
Revises: ae163b95ed0
Create Date: 2016-01-21 17:39:52.438586

"""

# revision identifiers, used by Alembic.
revision = '187c22028ca4'
down_revision = 'ae163b95ed0'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


def upgrade():
    op.create_table(
        'core_logs_iab',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_id', sa.Integer(), nullable=False),
        sa.Column('status', postgresql.ENUM('new', 'processing', 'error',
                                            'complete',
                                            name='scoop_iab_status')),
        sa.Column('iab_order_id', sa.String()),
        sa.Column('package_name', sa.String()),
        sa.Column('product_id', sa.String()),
        sa.Column('purchase_time', sa.String()),
        sa.Column('purchase_state', sa.Integer()),
        sa.Column('developer_payload', sa.String()),
        sa.Column('purchase_token', sa.String()),
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('is_active', sa.Boolean()),
    )
    op.add_column('core_paymentinappbillings',
                  sa.Column('iab_order_id', sa.String()),)


def downgrade():
    raise NotImplemented

