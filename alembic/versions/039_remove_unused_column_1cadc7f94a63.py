"""remove_unused_column

Revision ID: 1cadc7f94a63
Revises: fd3d6b2ab10
Create Date: 2015-12-14 13:44:48.952589

"""

# revision identifiers, used by Alembic.
revision = '1cadc7f94a63'
down_revision = 'fd3d6b2ab10'

from alembic import op
import sqlalchemy as sa


def upgrade():
    # core_offers
    op.drop_column('core_offers', 'aggr_price_usd')
    op.drop_column('core_offers', 'aggr_price_idr')
    op.drop_column('core_offers', 'aggr_price_point')

    # core_orderlines
    op.drop_column('core_orderlines', 'paymentgateway_id')
    op.drop_column('core_orderlinetemps', 'paymentgateway_id')

def downgrade():
    pass
