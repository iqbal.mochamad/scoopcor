"""Adding remote publishing tables

Revision ID: 1ecaa84c94b7
Revises: 25ce07082204
Create Date: 2015-04-29 15:30:21.271485

"""

# revision identifiers, used by Alembic.
revision = '1ecaa84c94b7'
down_revision = None #'43978ada710e'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql


def upgrade():

    op.create_table(
        'core_remote_categories',

        sa.Column('category_id', sa.Integer(), nullable=False),
        sa.Column('remote_service',
                  postgresql.ENUM('elevenia', name='remote_data_location'),
                  nullable=False),
        sa.Column('remote_id', sa.String(), nullable=False),

        sa.ForeignKeyConstraint(['category_id'], ['core_categories.id'], ),
        sa.PrimaryKeyConstraint('category_id', 'remote_service')
    )
    op.create_table(
        'core_remote_offers',

        sa.Column('offer_id', sa.Integer(), nullable=False),
        sa.Column('remote_service',
                  postgresql.ENUM('elevenia', name='remote_data_location'),
                  nullable=False),
        sa.Column('remote_id', sa.String(), nullable=False),
        sa.Column('last_checked', sa.DateTime(), nullable=True),
        sa.Column('last_posted', sa.DateTime(), nullable=True),

        sa.ForeignKeyConstraint(['offer_id'], ['core_offers.id'], ),
        sa.PrimaryKeyConstraint('offer_id', 'remote_service')
    )



def downgrade():
    op.drop_table('core_remote_offers ')
    op.drop_table('core_remote_categories')

