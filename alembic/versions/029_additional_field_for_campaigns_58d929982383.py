"""additional field for campaigns

Revision ID: 58d929982383
Revises: 46fb9d41cee7
Create Date: 2015-08-27 16:54:09.728628

"""

# revision identifiers, used by Alembic.
revision = '58d929982383'
down_revision = '6911e5ac75c'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('core_campaigns',sa.Column('start_date', sa.DateTime()))
    op.add_column('core_campaigns', sa.Column('end_date', sa.DateTime()))
    op.add_column('core_campaigns', sa.Column('total_cost', sa.DECIMAL(13, 4)))


def downgrade():
    op.drop_column('core_campaigns', 'start_date')
    op.drop_column('core_campaigns', 'end_date')
    op.drop_column('core_campaigns', 'total_cost')

