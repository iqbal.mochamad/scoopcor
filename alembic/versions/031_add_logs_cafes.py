"""cafe_logs

Revision ID: 388b389dc923
Revises: 31967442f94f
Create Date: 2015-11-10 15:16:43.400520

"""

# revision identifiers, used by Alembic.
revision = '388b389dc923'
down_revision = '34922a25c504'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'core_logs_cafes',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('client_id', sa.Integer()),
        sa.Column('cafe_email', sa.Text()),
        sa.Column('user_email', sa.Text())
    )

def downgrade():
    op.drop_table('core_logs_cafes')
