"""add file_size in core_items

Revision ID: a3ae4167594
Revises: po20ow391jn
Create Date: 2019-02-13 10:16:11.043000

"""

# revision identifiers, used by Alembic.
revision = 'a3ae4167594'
down_revision = 'po20ow985'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("core_items",
                  sa.Column('file_size', sa.Integer(),
                            nullable=True))


def downgrade():
    op.drop_column('core_items', 'file_size')
