"""055_add_item_new_fields_for_vendor

Revision ID: 4f34cbc597a2
Revises: 2ea0e7b3950b
Create Date: 2016-07-19 15:34:35.232885

"""

# revision identifiers, used by Alembic.
revision = '4f34cbc597a2'
down_revision = '2ea0e7b3950b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('core_items', sa.Column('vendor_product_id_print', sa.String(length=15)))


def downgrade():
    op.drop_column('core_items', 'vendor_product_id_print')
