"""Add IPhone resolution to Banner

Revision ID: 27837fa7a12a
Revises: 981a31e9f01
Create Date: 2015-11-24 01:23:45.490938

"""

# revision identifiers, used by Alembic.
revision = '27837fa7a12a'
down_revision = '981a31e9f01'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('core_banners', sa.Column('image_iphone', sa.String))


def downgrade():
    op.drop_column('core_banners', 'image_iphone')
