"""renewal_updates

Revision ID: 2a9b8e1de2a2
Revises: 46fb9d41cee7
Create Date: 2015-09-15 12:03:55.052998

"""

# revision identifiers, used by Alembic.
revision = '2a9b8e1de2a2'
down_revision = '46fb9d41cee7'

from alembic import op
import sqlalchemy as sa


def upgrade():

    # create table logs renewal, record all case errors
    op.create_table(
        'core_logs_iaprenewal',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('original_transaction_id', sa.BIGINT()),
        sa.Column('web_order_line_id', sa.BIGINT()),
        sa.Column('start_process', sa.DateTime()),
        sa.Column('end_process', sa.DateTime()),
        sa.Column('error', sa.Text()),
        sa.Column('apple_receipt', sa.Text()),
        sa.Column('apple_response', sa.Text()),
        sa.Column('is_patch', sa.Boolean()),
        sa.Column('is_notice', sa.Boolean())
    )

    # adding new columns
    op.add_column('core_paymentappleiaps', sa.Column('is_processed', sa.Boolean(), default=False))
    op.add_column('core_paymentappleiaps', sa.Column('renewal_status', sa.Integer(), default=1))

def downgrade():
    op.drop_table('core_logs_iaprenewal')

    op.drop_column('core_paymentappleiaps', 'is_processed')
    op.drop_column('core_paymentappleiaps', 'renewal_status')

