"""merge revision heads

Revision ID: 34922a25c504
Revises: ('1adab9664c43', '58d929982383')
Create Date: 2015-11-03 16:54:49.741039

"""

# revision identifiers, used by Alembic.
revision = '34922a25c504'
down_revision = ('1adab9664c43', '58d929982383')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
