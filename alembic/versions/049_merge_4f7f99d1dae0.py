""" Merge revision

Revision ID: 4f7f99d1dae0
Revises: ('552ca805062', '1e2aa4927d24')
Create Date: 2016-05-30 20:04:45.255351

"""

# revision identifiers, used by Alembic.
revision = '4f7f99d1dae0'
down_revision = ('552ca805062', '1e2aa4927d24')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
