"""058_shared_library_brands

Revision ID: 4037ce706844
Revises: 25faf87ea892
Create Date: 2016-08-16 12:05:18.510758

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '4037ce706844'
down_revision = '25faf87ea892'


def upgrade():
    conn = op.get_bind()

    op.create_table(
        'core_organization_brands',
        sa.Column('organization_id', sa.INTEGER, sa.ForeignKey('cas_organizations.id')),
        sa.Column('brand_id', sa.INTEGER, sa.ForeignKey('core_brands.id')),
        sa.Column('quantity', sa.INTEGER, nullable=False),
        sa.Column('latest_added_item_id', sa.INTEGER, sa.ForeignKey('core_items.id')),
        sa.Column('created', sa.DateTime(timezone=True)),
        sa.Column('modified', sa.DateTime(timezone=True)),
        sa.PrimaryKeyConstraint('organization_id', 'brand_id')
    )

    op.execute('ALTER TABLE core_organization_brands ALTER created SET DEFAULT now()')
    op.execute('ALTER TABLE core_organization_brands ALTER modified SET DEFAULT now()')

    op.create_table(
        'core_catalog_brands',
        sa.Column('catalog_id', sa.INTEGER, sa.ForeignKey('core_catalogs.id')),
        sa.Column('shared_library_brand_org_id', sa.INTEGER, sa.ForeignKey('cas_organizations.id')),
        sa.Column('shared_library_brand_brand_id', sa.INTEGER, sa.ForeignKey('core_brands.id')),
        sa.PrimaryKeyConstraint('catalog_id', 'shared_library_brand_org_id', 'shared_library_brand_brand_id')
    )
    op.create_foreign_key(
        'core_catalog_brands_organization_brand_fkey',
        'core_catalog_brands', 'core_organization_brands',
        ['shared_library_brand_org_id', 'shared_library_brand_brand_id'],
        ['organization_id', 'brand_id']
    )

    _create_function_auto_add_organization_items(conn)


def downgrade():
    op.drop_table('core_catalog_brands')
    op.drop_table('core_organization_brands')


def _create_function_auto_add_organization_items(conn):
    conn.execute("""CREATE OR REPLACE FUNCTION utility.organization_brands_to_organization_item()
  RETURNS int4
AS
$$
  DECLARE
    change_count INTEGER;
  BEGIN
    CREATE TEMPORARY TABLE temp_shared_lib_new_item (
        organization_id int not null,
        item_id int not null,
        brand_id int not null,
        quantity int not null) on commit drop;

    -- get data of new items from org-brands that now yet exists in org-item
    --  with item id per brands is bigger than latest_added_item_id
    INSERT INTO temp_shared_lib_new_item (organization_id, item_id, brand_id, quantity)
    SELECT ob.organization_id as organization_id,
            i.id as item_id, ob.brand_id,
            coalesce(ob.quantity, 0) as quantity
    FROM core_organization_brands ob
        LEFT JOIN core_items i ON ob.brand_id = i.brand_id
            and i.id > coalesce(ob.latest_added_item_id, 0)
        LEFT JOIN core_organization_items oi on oi.organization_id = ob.organization_id
            and oi.item_id = i.id
    WHERE i.id is not null
    and i.is_active = true and i.item_status = 'ready for consume'
    and oi.item_id is null;

    -- insert new org item data
    INSERT INTO core_organization_items
    (organization_id, item_id, item_name_override, quantity, quantity_available, created, modified)
    SELECT organization_id, item_id, null as item_name_override,
        quantity, quantity as quantity_available, now() as created, now() as modified
    FROM temp_shared_lib_new_item;

    GET DIAGNOSTICS change_count = ROW_COUNT;

    -- insert new catalog-items data
    INSERT INTO core_catalog_items (catalog_id, shared_library_item_org_id, shared_library_item_item_id)
    SELECT DISTINCT cb.catalog_id, s.organization_id, s.item_id
    FROM temp_shared_lib_new_item s
        LEFT JOIN core_catalog_brands cb on s.organization_id = cb.shared_library_brand_org_id
            AND s.brand_id = cb.shared_library_brand_brand_id
        LEFT JOIN core_catalog_items t on cb.catalog_id = t.catalog_id
            AND t.shared_library_item_org_id = s.organization_id
            AND t.shared_library_item_item_id = s.item_id
    WHERE cb.catalog_id IS NOT NULL and t.shared_library_item_item_id IS NULL
    ;

    -- update latest added item id to org-brands for the next update
    UPDATE core_organization_brands SET latest_added_item_id = s.max_item_id
        , modified = now()
    FROM ( SELECT organization_id, brand_id, MAX(item_id) as max_item_id
           FROM temp_shared_lib_new_item
           GROUP BY organization_id, brand_id
          ) s
    WHERE s.organization_id = core_organization_brands.organization_id
        AND s.brand_id = core_organization_brands.brand_id
        AND core_organization_brands.latest_added_item_id != s.max_item_id;

    RETURN change_count;
  END;
$$
LANGUAGE plpgsql VOLATILE;
    """)
