"""066_add_parental_control_flag_on_user

Revision ID: 21e47d85f6cf
Revises: 483a2148de7f
Create Date: 2016-11-14 13:28:48.085175

"""

# revision identifiers, used by Alembic.
revision = '21e47d85f6cf'
down_revision = '5331cc3cb63a'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('cas_users', sa.Column('allow_age_restricted_content', sa.Boolean(), nullable=True,
                                         server_default=sa.literal(True)))
    op.get_bind().execute(sa.text('UPDATE cas_users SET allow_age_restricted_content = True'))


def downgrade():
    op.drop_column('cas_users', 'allow_age_restricted_content')
