"""Add cas_users_organizations.is_manager

Revision ID: 25faf87ea892
Revises: 286e76293911
Create Date: 2016-07-15 12:31:48.411426

"""

# revision identifiers, used by Alembic.
revision = '25faf87ea892'
down_revision = '286e76293911'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('cas_users_organizations', sa.Column('is_manager', sa.Boolean(),
                                                       nullable=False,
                                                       server_default=sa.literal(False)))

    op.get_bind().execute('UPDATE cas_users_organizations set is_manager = False')

def downgrade():
    op.drop_column('cas_users_organizations', 'is_manager')
