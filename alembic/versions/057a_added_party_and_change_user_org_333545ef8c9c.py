"""009_added_party_and_change_user_org

Revision ID: 333545ef8c9c
Revises: 4f7f99d1dae0
Create Date: 2016-06-14 16:15:56.895125

"""

# revision identifiers, used by Alembic.
revision = '333545ef8c9c'
down_revision = '329903c4443f'

import logging

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

_log = logging.getLogger('alembic.party_user_org_migration')

party_type_enum = postgresql.ENUM(u'user', u'organization', u'shared library',
                                  u'partner', u'payment gateway', u'vendor',
                                  name='party_type', create_type=False)


def upgrade():

    conn = op.get_bind()

    _log.info('creating parties table')

    party_type_enum.create(op.get_bind())

    op.create_table(
        'parties',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('party_type', party_type_enum),
    )

    # increase sequence
    conn.execute("select setval('parties_id_seq', 1110000, true)")

    _log.info('creating cas_shared_libraries table')

    op.create_table(
        'cas_shared_libraries',
        sa.Column('id', sa.Integer(), sa.ForeignKey('parties.id')),
        sa.Column('user_concurrent_borrowing_limit', sa.INTEGER),
        sa.Column('reborrowing_cooldown', sa.Interval),
        sa.Column('borrowing_time_limit', sa.Interval)
    )

    _log.info('migrate user with id 711 to 1090711')
    _migrate_user(conn, 711)
    _log.info('migrate user with id 723 to 1090723')
    _migrate_user(conn, 723)

    _log.info('Migrate organization to parties')
    _migrate_cas_organizations_to_parties(conn)

    _log.info('Migrate user to parties')
    conn.execute("insert into parties (id, party_type) "
                 "select id, 'user'::party_type from cas_users")
    conn.execute(
        "alter table cas_users add CONSTRAINT cas_users_id_parties_fkey "
        "FOREIGN KEY (id) REFERENCES parties(id);")
    conn.execute("alter table cas_users alter id drop default;")

    _log.info('Migrate shared library data to cas_shared_libraries')
    _migrate_shared_library_to_cas_shared_libraries(conn)

    _update_function_return_overdue_borrowed_items(conn)

    _log.info('Party-User-Organization migration Done')


def _migrate_user(conn, user_id):
    # insert user 1090711 first with the same data as old 711, to prevent error of constraint
    # insert user 1090723 first with the same data as old 723, to prevent error of constraint
    conn.execute("update cas_users set email = 'removed-' || email, "
                 "username = 'removed-' ||username where id = {};".format(user_id))
    conn.execute("INSERT INTO cas_users (created, modified, id, username, email, first_name, "
                 "last_name, last_login, password, salt, is_active, is_verified, origin_client_id, "
                 "reset_password_token, key_verify, fb_id, note, referral_code, referral_id, "
                 "excluded_from_reporting, profile_pic_url) "
                 "select created, modified, id+1090000, replace(username, 'removed-', '')"
                 ", replace(email, 'removed-', ''), first_name, "
                 "last_name, last_login, password, salt, is_active, is_verified, origin_client_id, "
                 "reset_password_token, key_verify, fb_id, note, referral_code, referral_id, "
                 "excluded_from_reporting, profile_pic_url from cas_users "
                 "where id = {user_id};".format(user_id=user_id))

    # change user id in other tables
    conn.execute("UPDATE core_useractivities SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_userbuffets SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_paymenttokens SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE cas_clients SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_useritems SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_payments SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_paymentinappbillings SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_refunds SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_pointprofiles SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_userpoints SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_pointuses SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE cas_log_apilog SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_logs SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_itemlogs SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE cas_log_deauthorized SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE cas_users_roles SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE cas_profiles SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_logs_iab SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE cas_tokens SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_orderlinetemps SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_logs_orders SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_discountcodes_permitted_users SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_buffetpageviews SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_applereceiptidentifiers SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_orderlines SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_orderdetails SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_logs_orderstransactions SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_logs_payments SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE cas_refreshtokens SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE cas_userdevices SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE cas_log_devicehistory SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE cas_users_organizations SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_usersubscriptions_items SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_usersubscriptions SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_user_borrowed_items SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_iapreceiptdata SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_itemdownloads SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_ordertemps SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE cas_cafes_passwords SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))
    conn.execute("UPDATE core_orders SET user_id = user_id+1090000 WHERE user_id = {};".format(user_id))

    # remove old user data 711 and 723
    conn.execute("delete from cas_users where id = {};".format(user_id))


def _migrate_shared_library_to_cas_shared_libraries(conn):
    conn.execute("insert into cas_shared_libraries "
                 "select o.id, coalesce(o.user_concurrent_borrowing_limit, 0), "
                 "coalesce(reborrowing_cooldown, '0D'), "
                 "coalesce(borrowing_time_limit, '7D') "
                 "from cas_organizations o where o.type = 'shared library'")
    # add foreign key from cas_shared_libraries.id to parties.id
    conn.execute(
        "alter table cas_shared_libraries "
        "add CONSTRAINT cas_shared_libraries_id_cas_organizations_fkey "
        "FOREIGN KEY (id) REFERENCES cas_organizations(id);")
    op.alter_column('cas_shared_libraries', 'user_concurrent_borrowing_limit',
                    existing_type=sa.INTEGER, nullable=False)
    op.alter_column('cas_shared_libraries', 'reborrowing_cooldown',
                    existing_type=sa.Interval, nullable=False)
    op.alter_column('cas_shared_libraries', 'borrowing_time_limit',
                    existing_type=sa.Interval, nullable=False)
    op.drop_column('cas_organizations', 'user_concurrent_borrowing_limit')
    op.drop_column('cas_organizations', 'reborrowing_cooldown')
    op.drop_column('cas_organizations', 'borrowing_time_limit')


def _migrate_cas_organizations_to_parties(conn):

    _log.info('change org id')
    _insert_copied_data_with_new_id(conn)
    _update_organization_id(conn)
    _cleanup_unused_org_data(conn)
    _log.info('insert org into parties')

    conn.execute("insert into parties (id, party_type) "
                 "select id, case when type = 'shared library'::organization_type then 'shared library'::party_type "
                 "else 'organization'::party_type end from cas_organizations")

    # add foreign key from cas_organizations.id to parties.id
    conn.execute(
        "alter table cas_organizations add CONSTRAINT cas_organizations_id_parties_fkey "
        "FOREIGN KEY (id) REFERENCES parties(id);")
    conn.execute("alter table cas_organizations alter id drop default;")


def _insert_copied_data_with_new_id(conn):
    conn.execute(
        "INSERT INTO cas_organizations (created, modified, id, name, legal_name, slug, sort_priority, "
        "is_active, parent_organization_id, accounting_id, billing_address_id, mailing_address_id, status, "
        "type, contact_email, phone_alternate, phone_primary, user_concurrent_borrowing_limit, "
        "reborrowing_cooldown, borrowing_time_limit, logo_url) "
        " select created, modified, id+1100000, name, legal_name, 'org-' || slug, sort_priority, is_active, "
        "parent_organization_id, accounting_id, billing_address_id, mailing_address_id, status, type, "
        "contact_email, phone_alternate, phone_primary, user_concurrent_borrowing_limit, reborrowing_cooldown, "
        "borrowing_time_limit, logo_url "
        " from cas_organizations where id not in (711, 723);")


def _cleanup_unused_org_data(conn):
    conn.execute("delete from cas_organizations where id not in (711, 723) and id < 10000;")
    # conn.execute("update cas_organizations set slug = replace(slug, 'org-', '') where slug like '1100000%';")


def _update_organization_id(conn):
    _log.info('updating organization_id')
    # Update organization id before merge with user in parties
    # we don't change org id 711 and 723, because user id with this id already changed to 1090711 and 1090723

    conn.execute("UPDATE cas_organizations SET "
                 "parent_organization_id = parent_organization_id + 1100000 "
                 "where id not in (711, 723)")

    conn.execute("update cas_bankaccounts set organization_id = organization_id + 1100000"
                 " where organization_id not in (711, 723)")
    conn.execute("update cas_organizations_clients set organization_id = organization_id + 1100000"
                 " where organization_id not in (711, 723)")
    conn.execute("update cas_roles_organizations set organization_id = organization_id + 1100000"
                 " where organization_id not in (711, 723)")
    conn.execute("update cas_users_organizations set organization_id = organization_id + 1100000"
                 " where organization_id not in (711, 723)")
    conn.execute("update cas_users_publishers set organization_id = organization_id + 1100000"
                 " where organization_id not in (711, 723)")
    conn.execute("update core_partners set organization_id = organization_id + 1100000"
                 " where organization_id not in (711, 723)")
    conn.execute("update core_paymentgateways set organization_id = organization_id + 1100000"
                 " where organization_id not in (711, 723)")
    conn.execute("update core_vendors set organization_id = organization_id + 1100000"
                 " where organization_id not in (711, 723)")

    # conn.execute("update core_catalogs set organization_id = organization_id + 1100000"
    #              " where organization_id not in (711, 723)")
    # conn.execute("update core_organization_items set organization_id = organization_id + 1100000"
    #              " where organization_id not in (711, 723)")
    # conn.execute("update core_catalog_items set shared_library_item_org_id = shared_library_item_org_id + 1100000"
    #              " where shared_library_item_org_id not in (711, 723)")
    # conn.execute("update core_user_borrowed_items set catalog_item_org_id = catalog_item_org_id + 1100000"
    #              " where catalog_item_org_id not in (711, 723)")


def _update_function_return_overdue_borrowed_items(conn):
    conn.execute("""CREATE OR REPLACE FUNCTION utility.return_overdue_borrowed_items()
  RETURNS int4
AS
$$
  DECLARE
    change_count INTEGER;
  BEGIN
    WITH temp_return_shared_lib AS (
        SELECT ubi.catalog_item_org_id as organization_id,
            ubi.catalog_item_item_id as item_id,
            count(DISTINCT ubi.user_id) as qty_returned
        FROM core_user_borrowed_items ubi
            LEFT JOIN cas_shared_libraries AS org ON ubi.catalog_item_org_id = org.id
        WHERE
            org.borrowing_time_limit IS NOT NULL AND
            ubi.returned_time IS NULL AND
            (ubi.borrowing_start_time + org.borrowing_time_limit) < now()
        GROUP BY ubi.catalog_item_org_id, ubi.catalog_item_item_id
    )
        UPDATE core_organization_items SET
            quantity_available =
             CASE WHEN quantity_available + qty_returned > quantity THEN quantity
                 ELSE quantity_available + qty_returned END
            , modified = now()
        FROM temp_return_shared_lib
        WHERE core_organization_items.organization_id = temp_return_shared_lib.organization_id
          AND core_organization_items.item_id = temp_return_shared_lib.item_id;

    UPDATE core_user_borrowed_items SET returned_time = now() WHERE id IN (
      SELECT user_borrowed_item.id
        FROM core_user_borrowed_items AS user_borrowed_item
        LEFT JOIN cas_shared_libraries AS organization ON user_borrowed_item.catalog_item_org_id = organization.id
        WHERE
          organization.borrowing_time_limit IS NOT NULL AND
          user_borrowed_item.returned_time IS NULL AND
          (user_borrowed_item.borrowing_start_time + organization.borrowing_time_limit) < now()
    );

    GET DIAGNOSTICS change_count = ROW_COUNT;
    RETURN change_count;
  END;
$$
LANGUAGE plpgsql VOLATILE;
    """)


def downgrade():
    pass
