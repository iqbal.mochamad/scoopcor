"""Merge revision

Revision ID: 6911e5ac75c
Revises: ('36ee8f2274ab', '1e919f0b5e46')
Create Date: 2015-10-28 18:37:23.597050

"""

# revision identifiers, used by Alembic.
revision = '6911e5ac75c'
down_revision = ('36ee8f2274ab', '1e919f0b5e46')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
