"""Add created_by in UserBulkUpload

Revision ID: 3f02309aa60b
Revises: CvHGoi7hbhF
Create Date: 2018-09-10 16:00:06.921000

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '3f02309aa60b'
down_revision = 'CvHGoi7hbhF'


def upgrade():
    op.add_column('core_eperpus_upload', sa.Column('created_by', sa.Integer(), sa.ForeignKey('cas_users.id')))


def downgrade():
    op.drop_column('core_eperpus_upload', 'created_by')
