"""add foreign key support on vendor revenue share

Revision ID: 24d2691a08bc
Revises: 981a31e9f01
Create Date: 2015-11-19 14:40:36.685234

"""

# revision identifiers, used by Alembic.
revision = '24d2691a08bc'
down_revision = '981a31e9f01'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_foreign_key("fk_vendorrevenueshares_revenue_id", "core_vendorrevenueshares", "core_revenueshares", ["revenue_id"], ["id"])


def downgrade():
    op.drop_constraint("fk_vendorrevenueshares_revenue_id", "core_vendorrevenueshares")
