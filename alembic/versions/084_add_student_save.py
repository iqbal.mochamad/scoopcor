"""Add new table core_eperpus_studentsaves

Revision ID: 3f02309aa60b
Revises: 789opql2xmaksl
Create Date: 2018-09-10 16:00:06.921000

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'koaslquemxk12'
down_revision = '789opql2xmaksl'


def upgrade():
    op.create_table(
        'core_eperpus_studentsaves',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('name', sa.String(150), nullable=False),
        sa.Column('description', sa.Text),
        sa.Column('is_active', sa.Boolean(), default=True)
    )

    op.create_table(
        'core_items_studentsaves',
        sa.Column('item_id', sa.Integer(), sa.ForeignKey('core_items.id')),
        sa.Column('studentsave_id', sa.Integer(), sa.ForeignKey('core_eperpus_studentsaves.id'))
    )

def downgrade():
    op.drop_table('core_eperpus_studentsaves')
    op.drop_table('core_items_studentsaves')
