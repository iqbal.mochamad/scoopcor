"""
    075 added is_internal_catalog on core_catalogs

"""

# revision identifiers, used by Alembic.
revision = 'c1sk429hldsn'
down_revision = '111k228skqro'

from alembic import op
import sqlalchemy as sa

def upgrade():
    op.add_column('core_catalogs', sa.Column('is_internal_catalog', sa.Boolean(), default=False))
    op.get_bind().execute(sa.text('update core_catalogs set is_internal_catalog=False;'))

def downgrade():
    op.drop_column('core_catalogs', 'is_internal_catalog')
