"""
Add new column web_reader_processed

Revision ID: 3f02309aa60b
Revises: 789opql2xmaksl
Create Date: 2018-09-10 16:00:06.921000

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'kidja1219ara'
down_revision = 'koaslquemxk12'


def upgrade():
    op.get_bind().execute(sa.text('alter table web_reader_processed alter column created type timestamp with time zone;'))
    op.get_bind().execute(sa.text('alter table web_reader_processed alter column modified type timestamp with time zone;'))


def downgrade():
    op.get_bind().execute(sa.text('alter table web_reader_processed alter column created type timestamp without time zone;'))
    op.get_bind().execute(sa.text('alter table web_reader_processed alter column modified type timestamp without time zone;'))
