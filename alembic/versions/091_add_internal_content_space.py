"""Add internal content disk space in cas_shared_libraries

Revision ID: e3ef97c9076
Revises: a3ae4167594
Create Date: 2019-02-27 09:57:57.446000

"""

# revision identifiers, used by Alembic.
revision = 'e3ef97c9076'
down_revision = '11cab7ec79a9'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column("cas_shared_libraries",
                  sa.Column('disk_space', sa.BigInteger(),
                            nullable=True))


def downgrade():
    op.drop_column('cas_shared_libraries', 'disk_space')
