"""
    Add new status mcgrawhill content
"""


from alembic import op

revision = 'mcgrahill01'
down_revision = 'partner012'


def upgrade():
    op.execute("INSERT INTO pg_enum (enumtypid, enumlabel, enumsortorder)"
               " SELECT 'item_status'::regtype::oid, 'mcgrawhill content', "
               " ( SELECT MAX(enumsortorder) + 1 FROM pg_enum WHERE enumtypid = 'item_status'::regtype )")


def downgrade():
    pass

