"""Merge revision

Revision ID: 57c07fcc746d
Revises: ('6911e5ac75c', '517eaa2b816d')
Create Date: 2015-10-29 17:42:30.832138

"""

# revision identifiers, used by Alembic.
revision = '57c07fcc746d'
down_revision = ('6911e5ac75c', '517eaa2b816d')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
