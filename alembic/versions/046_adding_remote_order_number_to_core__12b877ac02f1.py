"""adding remote_order_number to core_orders

Revision ID: 12b877ac02f1
Revises: 187c22028ca4
Create Date: 2016-03-11 16:33:23.797773

"""

# revision identifiers, used by Alembic.
revision = '12b877ac02f1'
down_revision = '2c14545c0d7c'

from alembic import op
import sqlalchemy as sa


def upgrade():
    conn = op.get_bind()

    op.add_column('core_orders',
                  sa.Column('remote_order_number', sa.String(50)),)

    # alter table core_orders add remote_order_number character varying(50);
    # conn.execute("ALTER TYPE public.remote_data_location ADD VALUE 'gramedia'")
    # select enum_range(null::public.remote_data_location)


def downgrade():
    op.drop_column('core_orders', 'remote_order_number')
