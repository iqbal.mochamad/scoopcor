"""
    added expired_date for core_report_userblacklists
    added is_for_new_user for core_discountcodes
    add core_discountcodes_users table
"""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'bannedexpired'
down_revision = 'paye2pay'


def upgrade():
    op.add_column('core_report_userblacklists', sa.Column('expired_date', sa.DateTime()))

    # boolean for new user
    op.add_column('core_discountcodes', sa.Column('is_for_new_user', sa.Boolean()))

    # junction table for core_discountcodes_users
    op.create_table(
        'core_discountcodes_users',
        sa.Column('discountcode_id', sa.Integer(), sa.ForeignKey('core_discountcodes.id'), primary_key=True),
        sa.Column('user_id', sa.Integer(), sa.ForeignKey('cas_users.id'), primary_key=True)
    )


def downgrade():
    op.drop_column('core_report_userblacklists', 'expired_date')
    op.drop_column('core_discountcodes', 'is_for_new_user')
    op.drop_table('core_discountcodes_users')
