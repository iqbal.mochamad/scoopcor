# revision identifiers, used by Alembic.
revision = '1a1a9811c0tk'
down_revision = '9a120641c0kc'

from alembic import op
import sqlalchemy as sa

def upgrade():
    op.add_column('cas_organizations', sa.Column('username_prefix', sa.String(10)))

    op.get_bind().execute(sa.text("update cas_organizations set username_prefix=NULL"))
    op.get_bind().execute(sa.text("update cas_organizations set username_prefix='BPK' where id in (1129637,1129636)"))
    op.get_bind().execute(sa.text("update cas_organizations set username_prefix='FIF' where id in (1100749)"))

    op.add_column('cas_users', sa.Column('level', sa.String(50)))

def downgrade():
    op.drop_column('cas_organizations', 'username_prefix')
    op.drop_column('cas_users', 'level_id')

