"""051_add_revision_id_in_user_borrowed_item

Revision ID: 342f5385bf17
Revises: 4f7f99d1dae0
Create Date: 2016-06-16 18:20:45.353720

"""

# revision identifiers, used by Alembic.
revision = '342f5385bf17'
down_revision = '4f7f99d1dae0'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('core_user_borrowed_items', sa.Column('modified', sa.DateTime))

    op.get_bind().execute('UPDATE core_user_borrowed_items SET modified = borrowing_start_time WHERE modified is null')

    op.alter_column('core_user_borrowed_items', 'modified', nullable=False)


def downgrade():
    op.drop_column('core_user_borrowed_items', 'modified')
