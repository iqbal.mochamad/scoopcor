"""
    Add new table core_partners_clients

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

revision = 'partner012'
down_revision = 'partner01'


def upgrade():
    op.create_table(
        'core_partners_clients',
        sa.Column('partner_id', sa.Integer()),
        sa.Column('client_id', sa.Integer())
    )

    op.add_column("core_partners", sa.Column('reward_offers', postgresql.JSONB()))
    op.add_column("core_partners", sa.Column('display_offers', sa.ARRAY(sa.Text)))


def downgrade():
    op.drop_table('core_partners_clients')

