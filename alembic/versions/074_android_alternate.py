"""
    074_android_alternate

"""

# revision identifiers, used by Alembic.
revision = '111k228skqro'
down_revision = '38492aqgk16n'

from alembic import op
import sqlalchemy as sa

def upgrade():
    op.get_bind().execute(sa.text('ALTER TABLE core_iostiers ADD COLUMN "price_idr" Numeric( 10, 2 );'))
    op.get_bind().execute(sa.text('ALTER TABLE core_androidtiers ADD COLUMN "price_idr" Numeric( 10, 2 );'))
    op.get_bind().execute(sa.text('ALTER TABLE core_windowsphonetiers ADD COLUMN "price_idr" Numeric( 10, 2 );'))

def downgrade():
    op.get_bind().execute(sa.text('ALTER TABLE core_iostiers DROP COLUMN "price_idr";'))
    op.get_bind().execute(sa.text('ALTER TABLE core_androidtiers DROP COLUMN "price_idr";'))
    op.get_bind().execute(sa.text('ALTER TABLE core_windowsphonetiers DROP COLUMN "price_idr";'))
