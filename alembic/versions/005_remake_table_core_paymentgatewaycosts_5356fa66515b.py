"""change table structure core_paymentgatewaycosts

Revision ID: 5356fa66515b
Revises: 4b83b28b27db
Create Date: 2015-07-09 12:51:53.059127

"""

# revision identifiers, used by Alembic.
revision = '5356fa66515b'
down_revision = '48526fa81128'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_column('core_paymentgatewayformulas', 'default_fixed_amount')
    op.drop_column('core_paymentgatewayformulas', 'default_percentage_amount')

    op.add_column('core_paymentgatewaycosts', sa.Column('default_max_amount', sa.DECIMAL(10, 4)))
    op.add_column('core_paymentgatewaycosts',sa.Column('default_min_amount', sa.DECIMAL(10, 4)))
    op.add_column('core_paymentgatewaycosts',sa.Column('default_fixed_amount', sa.DECIMAL(10, 4)))
    op.add_column('core_paymentgatewaycosts',sa.Column('default_percentage_amount', sa.DECIMAL(10, 4)))

    op.add_column('core_paymentgatewayformulas', sa.Column('fixed_amount', sa.DECIMAL(10, 4)))
    op.add_column('core_paymentgatewayformulas', sa.Column('percentage_amount', sa.DECIMAL(10, 4)))

def downgrade():
    op.drop_column('core_paymentgatewaycosts', 'default_fixed_amount')
    op.drop_column('core_paymentgatewaycosts', 'default_percentage_amount')
    op.drop_column('core_paymentgatewaycosts', 'default_max_amount')
    op.drop_column('core_paymentgatewaycosts', 'default_min_amount')

    op.drop_column('core_paymentgatewayformulas', 'fixed_amount')
    op.drop_column('core_paymentgatewayformulas', 'percentage_amount')

    op.add_column('core_paymentgatewayformulas', sa.Column('default_fixed_amount', sa.DECIMAL(10, 4)))
    op.add_column('core_paymentgatewayformulas', sa.Column('default_percentage_amount', sa.DECIMAL(10, 4)))