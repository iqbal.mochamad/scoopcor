"""
update core_paymentcreditcardtokens add token saved
add new table core_paymentcreditcardrenewals

Revision ID: po20ow391jn
Revises: kidja1219ara
Create Date: 2018-12-17 13:46:06.921000

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'imawa69kio'
down_revision = 'katw278ols69k'

renewal_status = postgresql.ENUM(u'new', u'processing', u'error', u'complete', u'expired', u'cancel',
                                 name='midtrans_renewal_status', create_type=False)

def upgrade():
    renewal_status.create(op.get_bind())
    conn = op.get_bind()

    op.create_table(
        'core_paymentcreditcardrenewals',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('masked_credit_card', sa.String(250)),
        sa.Column('renewal_status', renewal_status),
        sa.Column('user_id', sa.Integer()),
        sa.Column('offer_id', sa.Integer()),
        sa.Column('order_id', sa.Integer()),
        sa.Column('orderline_id', sa.Integer()),
        sa.Column('start_date', sa.DateTime(timezone=True)),
        sa.Column('expired_date', sa.DateTime(timezone=True)),
        sa.Column('note', sa.String(250))
    )
    op.add_column('core_paymentcreditcardtokens', sa.Column('token', sa.String(250), nullable=True))

def downgrade():
    op.drop_table('core_paymentcreditcards')
    op.drop_column('core_paymentcreditcardtokens', 'token')
