"""Merge revision

Revision ID: 42f80822fd33
Revises: ('4942b8ce882d', '24d2691a08bc')
Create Date: 2015-12-02 20:08:17.119809

"""

# revision identifiers, used by Alembic.
revision = '42f80822fd33'
down_revision = ('4942b8ce882d', '24d2691a08bc')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
