# revision identifiers, used by Alembic.
revision = '9a120641c0kc'
down_revision = '3c190544908c'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'notifications',
        sa.Column('id', sa.INTEGER, primary_key=True),
        sa.Column('created', sa.DateTime(timezone=True)),
        sa.Column('modified', sa.DateTime(timezone=True)),
        sa.Column('user_id', sa.Integer(), sa.ForeignKey('cas_users.id')),
        sa.Column('client_id', sa.Integer(), sa.ForeignKey('cas_clients.id')),
        sa.Column('device_register_key', sa.Text()),
        sa.Column('is_allow_notification', sa.Boolean()),
        sa.Column('error_deactivate', sa.Text())
    )
    op.add_column('cas_clients', sa.Column('firebase_server_key', sa.TEXT()))
    op.get_bind().execute(sa.text('ALTER TABLE notifications ALTER is_allow_notification SET DEFAULT true'))

def downgrade():
    op.drop_table('notifications')
    op.drop_column('cas_clients', 'firebase_server_key')
