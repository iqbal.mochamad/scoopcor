"""064_add_field_locked_message_to_shared_lib

Revision ID: 483a2148de7f
Revises: 47e7a6aa4ec2
Create Date: 2016-10-24 12:51:53.840088

"""

# revision identifiers, used by Alembic.
revision = '483a2148de7f'
down_revision = '47e7a6aa4ec2'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('cas_shared_libraries', sa.Column('locked_message', sa.Text(), nullable=True))


def downgrade():
    op.drop_column('cas_shared_libraries', 'locked_message')
