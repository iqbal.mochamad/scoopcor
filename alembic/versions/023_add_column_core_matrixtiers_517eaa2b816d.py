"""add column core matrixtiers

Revision ID: 517eaa2b816d
Revises: 36ee8f2274ab
Create Date: 2015-10-29 11:54:45.629321

"""

# revision identifiers, used by Alembic.
revision = '517eaa2b816d'
down_revision = '36ee8f2274ab'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column(
        'core_matrixtiers', sa.Column(
            'valid_from', sa.DateTime
        )
    )
    op.add_column(
        'core_matrixtiers', sa.Column(
            'valid_to', sa.DateTime
        )
    )


def downgrade():
    op.drop_column('core_matrixtiers', 'valid_from')
    op.drop_column('core_matrixtiers', 'valid_to')
