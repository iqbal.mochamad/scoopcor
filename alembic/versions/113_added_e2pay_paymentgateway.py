"""
    Add new table group, merchant_code, merchant_key

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

revision = 'paye2pay'
down_revision = 'payrenewal01'

paymentgateway_group = postgresql.ENUM(u'midtrans', u'e2pay', u'others', name='paymentgateway_group', create_type=False)
e2pay_status = postgresql.ENUM(u'success', u'pending', u'fail', name='e2pay_status', create_type=False)


def upgrade():
    paymentgateway_group.create(op.get_bind())
    e2pay_status.create(op.get_bind())

    op.create_table(
        'core_paymente2pay',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('paymentgateway_id', sa.Integer()),
        sa.Column('order_id', sa.Integer()),
        sa.Column('status', e2pay_status),
        sa.Column('status_url', sa.Text()),
        sa.Column('data_request', postgresql.JSONB()),
        sa.Column('data_response', postgresql.JSONB())
    )

    op.add_column("core_paymentgateways", sa.Column('payment_group', paymentgateway_group))
    op.add_column("core_paymentgateways", sa.Column('merchant_code', sa.String(250)))
    op.add_column("core_paymentgateways", sa.Column('merchant_key', sa.String(250)))


def downgrade():
    op.drop_column('core_paymentgateways', 'group')
    op.drop_column('core_paymentgateways', 'merchant_code')
    op.drop_column('core_paymentgateways', 'merchant_key')


