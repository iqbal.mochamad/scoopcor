"""
    Add new column is_trial, trial_time for core_orderlines, core_orderlinetemps, core_discounts

"""
import sqlalchemy as sa
from alembic import op

revision = 'trialpromo'
down_revision = 'bannedexpired'


def upgrade():
    op.add_column("core_orderlines", sa.Column('is_trial', sa.Boolean(), default=False))
    op.add_column("core_orderlinetemps", sa.Column('is_trial', sa.Boolean(), default=False))
    op.add_column("core_discounts", sa.Column('trial_time', sa.Interval(), nullable=True))

    op.execute("update core_discounts set trial_time='00:00:00';")


def downgrade():
    op.drop_column("core_orderlines", 'is_trial')
    op.drop_column("core_orderlinetemps", 'is_trial')
    op.drop_column("core_discounts", 'trial_time')

