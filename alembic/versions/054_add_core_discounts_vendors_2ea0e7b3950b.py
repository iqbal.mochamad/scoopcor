"""050_add_core_discounts_vendors

Revision ID: 2ea0e7b3950b
Revises: 4f7f99d1dae0
Create Date: 2016-06-17 15:31:05.015288

"""

# revision identifiers, used by Alembic.
revision = '2ea0e7b3950b'
down_revision = '23e81126ae01'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'core_discounts_vendors',
        sa.Column('discount_id', sa.INTEGER, sa.ForeignKey('core_discounts.id')),
        sa.Column('vendor_id', sa.INTEGER, sa.ForeignKey('core_vendors.id'))
    )
    op.execute("CREATE UNIQUE INDEX core_discounts_vendors_discount_vendor_unique_idx "
              "ON core_discounts_vendors (discount_id, vendor_id) ")


def downgrade():
    op.drop_table('core_discounts_vendors')
