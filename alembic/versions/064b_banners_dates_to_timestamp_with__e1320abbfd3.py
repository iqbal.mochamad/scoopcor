"""065_banners_dates_to_timestamp_with_time_zone

Revision ID: e1320abbfd3
Revises: 483a2148de7f
Create Date: 2016-11-25 15:44:36.635015

"""

# revision identifiers, used by Alembic.
revision = 'e1320abbfd3'
down_revision = '483a2148de7f'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.get_bind().execute(sa.text('alter table core_banners alter column valid_from type timestamp with time zone;'))
    op.get_bind().execute(sa.text('alter table core_banners alter column valid_to type timestamp with time zone;'))
    op.get_bind().execute(sa.text('alter table core_banners alter column created type timestamp with time zone;'))
    op.get_bind().execute(sa.text('alter table core_banners alter column modified type timestamp with time zone;'))


def downgrade():
    op.get_bind().execute(sa.text('alter table core_banners alter column valid_from type timestamp without time zone;'))
    op.get_bind().execute(sa.text('alter table core_banners alter column valid_to type timestamp without time zone;'))
    op.get_bind().execute(sa.text('alter table core_banners alter column created type timestamp without time zone;'))
    op.get_bind().execute(sa.text('alter table core_banners alter column modified type timestamp without time zone;'))

