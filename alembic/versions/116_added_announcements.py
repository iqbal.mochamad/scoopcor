"""
    Add new table core_announcements

"""

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

revision = 'announcements'
down_revision = 'trialpromo'


renewal_status_eperpus = postgresql.ENUM(u'new', u'complete', u'fail', u'cancel',
                                         name='renewal_status_eperpus', create_type=False)


def upgrade():

    op.create_table(
        'core_announcements',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('title', sa.String(250)),
        sa.Column('publisher_id', sa.Integer()),
        sa.Column('content', sa.Text()),
        sa.Column('is_active', sa.Boolean(), default=False)
    )

    renewal_status_eperpus.create(op.get_bind())

    op.add_column("core_organization_brands", sa.Column('order_id', sa.Integer()))
    op.add_column("core_organization_brands", sa.Column('is_renewal', sa.Boolean(), default=False))
    op.add_column("core_organization_brands", sa.Column('renewal_note', postgresql.JSONB()))
    op.add_column("core_organization_brands", sa.Column('is_active', sa.Boolean(), default=True))
    op.add_column("core_organization_brands", sa.Column('renewal_status', renewal_status_eperpus))

    op.execute("""
        UPDATE core_organization_brands SET renewal_note ='{}', is_renewal=false, is_active=true, renewal_status='new' 
    """)


def downgrade():
    op.drop_table('core_announcements')

    op.drop_column('core_organization_brands', 'order_id')
    op.drop_column('core_organization_brands', 'is_renewal')
    op.drop_column('core_organization_brands', 'renewal_note')
    op.drop_column('core_organization_brands', 'is_active')
    op.drop_column('core_organization_brands', 'renewal_status')

