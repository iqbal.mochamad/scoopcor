"""Dropping cut_rules

Revision ID: 580d447b2201
Revises: 4f7f99d1dae0
Create Date: 2016-06-19 13:27:47.503479

"""

# revision identifiers, used by Alembic.
revision = '580d447b2201'
down_revision = '4f7f99d1dae0'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_column('core_paymentgateways', 'cutrule_id')
    op.drop_column('core_vendors', 'cut_rule_id')
    op.drop_column('core_partners', 'cut_rule_id')
    op.drop_table('core_cutrules')


def downgrade():

    op.create_table(
        'core_cutrules',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(250), nullable=False),
        sa.Column('description', sa.Text),
        sa.Column('cut_rule', sa.Integer, nullable=False),
        sa.Column('is_active', sa.Boolean(), default=False),
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime())
    )

    # payment_gateways have cut_rules enforced as a non-nullable foreign key
    op.execute('''
        INSERT INTO core_cutrules (name, description, cut_rule, is_active, created, modified)
        VALUES ('NA', 'NA', 1, true, NOW(), NOW())''')
    op.add_column(
        'core_paymentgateways',
        sa.Column('cutrule_id', sa.Integer, sa.ForeignKey('core_cutrules.id'), nullable=True)
    )
    op.execute('''UPDATE core_paymentgateways SET cutrule_id = 1;''')
    op.alter_column('core_paymentgateways', 'cutrule_id', nullable=False)


    # vendors don't have any enforcement of cut_rules relationship.. also spelling is different =|
    op.add_column('core_vendors', sa.Column('cut_rule_id', sa.Integer))

    # same is true of 'partners'
    op.add_column('core_partners', sa.Column('cut_rule_id', sa.Integer))
