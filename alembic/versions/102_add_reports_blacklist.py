"""
    Add new table core_report_screenshots & core_report_userblacklists

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'koqioiyrs12'
down_revision = 'audiojksml12'

violation_reports = postgresql.ENUM(u'screenshots violation', u'hackers violation', u'spamming violation',
    u'sara violation', u'referral violation', name='violation_reports', create_type=False)

def upgrade():
    violation_reports.create(op.get_bind())

    op.create_table(
        'core_report_screenshots',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_id', sa.Integer()),
        sa.Column('item_id', sa.Integer()),
        sa.Column('screenshot_date', sa.DateTime(timezone=True)),
        sa.Column('is_active', sa.Boolean(), default=True))

    op.create_table(
        'core_report_userblacklists',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_id', sa.Integer()),
        sa.Column('violation_status', violation_reports),
        sa.Column('is_active', sa.Boolean(), default=True)
    )

def downgrade():
    op.drop_table('core_reports_screenshots')
    op.drop_table('core_reports_userblacklist')

