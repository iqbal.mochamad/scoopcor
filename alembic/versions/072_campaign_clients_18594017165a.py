"""072_campaign_clients

Revision ID: 18594017165a
Revises: 1a1a9811c0tk
Create Date: 2017-06-14 13:44:44.253326

"""

# revision identifiers, used by Alembic.
revision = '18594017165a'
down_revision = '1a1a9811c0tk'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.get_bind().execute(sa.text('alter table core_campaigns alter column start_date type timestamp with time zone;'))
    op.get_bind().execute(sa.text('alter table core_campaigns alter column end_date type timestamp with time zone;'))
    op.get_bind().execute(sa.text('alter table core_campaigns alter column created type timestamp with time zone;'))
    op.get_bind().execute(sa.text('alter table core_campaigns alter column modified type timestamp with time zone;'))


def downgrade():
    op.get_bind().execute(sa.text('alter table core_campaigns alter column start_date type timestamp without time zone;'))
    op.get_bind().execute(sa.text('alter table core_campaigns alter column end_date type timestamp without time zone;'))
    op.get_bind().execute(sa.text('alter table core_campaigns alter column created type timestamp without time zone;'))
    op.get_bind().execute(sa.text('alter table core_campaigns alter column modified type timestamp without time zone;'))

