"""
    Add new table core_partners_users

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

revision = 'partner01'
down_revision = 'general01'


def upgrade():
    op.create_table(
        'core_partners_users',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_id', sa.Integer()),
        sa.Column('partner_id', sa.Integer()),
        sa.Column('partner_account', sa.String(250)),
        sa.Column('expired_date', sa.DateTime()),
        sa.Column('response_data', postgresql.JSONB()),
        sa.Column('order_id', sa.Integer()),
        sa.Column('client_id', sa.Integer())
    )

    op.add_column("core_partners", sa.Column('is_displayed', sa.Boolean(), default=False))
    op.add_column("core_partners", sa.Column('auth_url', sa.String(250)))
    op.add_column("core_partners", sa.Column('auth_key', sa.String(250)))

    op.add_column("core_partners", sa.Column('web_oauth_page', sa.String(250)))
    op.add_column("core_partners", sa.Column('web_success_page', sa.String(250)))


def downgrade():
    op.drop_table('core_partners_users')

