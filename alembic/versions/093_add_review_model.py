"""add review model

Revision ID: 3f259c6cdcd6
Revises: 11cab7ec79a9
Create Date: 2019-03-08 09:47:14.782000

"""

# revision identifiers, used by Alembic.
revision = '3f259c6cdcd6'
down_revision = 'ioa2klo6ql94'

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

report_status = postgresql.ENUM(u'new', u'on_process', u'processed', u'revoked', name='report_status',
                                create_type=False)
report_reason = postgresql.ENUM(u'sara', u'harsh_word', u'review_unrelated', u'other', name='report_reason',
                                create_type=False)


def upgrade():
    op.create_table(
        'core_reviews',
        sa.Column('id', sa.INTEGER, primary_key=True),
        sa.Column('created', sa.DateTime(timezone=True)),
        sa.Column('modified', sa.DateTime(timezone=True)),
        sa.Column('user_id', sa.Integer(), sa.ForeignKey('cas_users.id')),
        sa.Column('item_id', sa.Integer(), sa.ForeignKey('core_items.id')),
        sa.Column('rating', sa.SmallInteger()),
        sa.Column('review', sa.Text(), nullable=True),
        sa.Column('is_active', sa.Boolean(), default=True)
    )
    report_status.create(op.get_bind())
    report_reason.create(op.get_bind())
    op.create_table(
        'core_report_review',
        sa.Column('created', sa.DateTime(timezone=True)),
        sa.Column('modified', sa.DateTime(timezone=True)),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('review_id', sa.Integer(), sa.ForeignKey('core_reviews.id')),
        sa.Column('user_id', sa.Integer(), sa.ForeignKey('cas_users.id')),
        sa.Column('status', report_status),
        sa.Column('reason', report_reason),
        sa.Column('notes', sa.Text(), nullable=True),
        sa.Column('is_active', sa.Boolean(), default=False)
    )


def downgrade():
    op.drop_table('core_reviews')
    op.drop_table('core_report_review')
