"""
    Add new column colors on core_offers
"""
import sqlalchemy as sa
from alembic import op
revision = 'po20ow985'
down_revision = 'po20ow391jn'

def upgrade():
    op.add_column('core_offers', sa.Column('colors', sa.JSON(), nullable=True))
    op.add_column('core_platforms_offers', sa.Column('colors', sa.JSON(), nullable=True))

def downgrade():
    op.drop_column('core_offers', 'colors')
    op.drop_column('core_platforms_offers', 'colors')
