"""add page count to items

Revision ID: 4942b8ce882d
Revises: 27837fa7a12a
Create Date: 2015-12-01 09:32:53.885997

"""

# revision identifiers, used by Alembic.
revision = '4942b8ce882d'
down_revision = '27837fa7a12a'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('core_items', sa.Column('page_count', sa.Integer))


def downgrade():
    op.drop_column('core_items', 'page_count')
