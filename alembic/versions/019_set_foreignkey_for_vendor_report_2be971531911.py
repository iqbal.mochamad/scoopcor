"""set foreignkey for vendor report

Revision ID: 2be971531911
Revises: dde54d72aff
Create Date: 2015-09-28 14:11:51.209299

"""

# revision identifiers, used by Alembic.
revision = '2be971531911'
down_revision = 'dde54d72aff'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_foreign_key("fk_vendor_report_id", "core_vendorreports", "core_vendors",["vendor_id"], ["id"])


def downgrade():
    op.drop_constraint("fk_vendor_report_id", "core_vendorreports")
