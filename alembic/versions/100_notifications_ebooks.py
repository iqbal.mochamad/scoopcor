"""
add new table notifications_ebooks

Revision ID: po20ow391jn
Revises: kidja1219ara
Create Date: 2018-12-17 13:46:06.921000

"""
import sqlalchemy as sa
from alembic import op

revision = 'ui1928skqjw'
down_revision = 'imawa69kio'

def upgrade():
    op.create_table(
        'notifications_ebooks',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_id', sa.Integer()),
        sa.Column('client_id', sa.Integer()),
        sa.Column('device_register_key', sa.Text),
        sa.Column('device_model', sa.String(250)),
        sa.Column('is_active', sa.Boolean()))

def downgrade():
    op.drop_table('notifications_ebooks')
