"""
    add new table cas_organizations_catalogs
"""

import sqlalchemy as sa
from alembic import op


revision = 'orgcatalog01'
down_revision = 'createdby01'


def upgrade():
    op.create_table(
        'cas_organizations_catalogs',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('organization_id', sa.Integer()),
        sa.Column('catalog_id', sa.Integer())
    )
    op.get_bind().execute(
        sa.text('insert into cas_organizations_catalogs (created, modified, catalog_id, organization_id) '
                'select now(), now(), id, organization_id from core_catalogs;'))

    # insert 1 new catalog for BTPN.
    op.get_bind().execute(
        sa.text('insert into cas_organizations_catalogs (created, modified, catalog_id, organization_id) '
                'select now(), now(), 195, 1100797'))

    # move syariah to BTPN catalogs.
    op.get_bind().execute(
        sa.text('update cas_organizations_catalogs set catalog_id=16 where organization_id=1611713'))


def downgrade():
    op.drop_table('cas_organizations_catalogs')
