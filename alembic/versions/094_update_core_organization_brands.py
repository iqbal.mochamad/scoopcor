"""
    update core_organization_brands
"""

# revision identifiers, used by Alembic.
revision = 'k9ty4121anw4'
down_revision = '3f259c6cdcd6'

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql


core_organization_brands_type = postgresql.ENUM(u'edition', u'duration', name='core_organization_brands_type', create_type=False)


def upgrade():
    core_organization_brands_type.create(op.get_bind())

    op.create_table(
        'core_organization_brands_temp',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('organization_id', sa.Integer()),
        sa.Column('brand_id', sa.Integer()),
        sa.Column('quantity', sa.Integer()),
        sa.Column('valid_from', sa.DateTime()),
        sa.Column('valid_to', sa.DateTime()),
        sa.Column('latest_added_item_id', sa.Integer()),

        sa.Column('current_offer_type', core_organization_brands_type),
        sa.Column('offer_stock', sa.Integer()),
        sa.Column('offer_id', sa.Integer()),
        sa.Column('current_offer_stock', sa.Integer()),
        sa.Column('note', sa.String(250))
    )

    op.get_bind().execute(sa.text(
        '''
            insert into core_organization_brands_temp (created, modified, organization_id, brand_id, quantity, valid_from, 
            valid_to, latest_added_item_id, current_offer_type, offer_stock, offer_id, current_offer_stock, note)

            select created, modified, organization_id, brand_id, quantity, valid_from, valid_to, latest_added_item_id,
            'duration', 0, NULL, 0, 'added before release edition subs' from core_organization_brands
        '''
    ))

    op.get_bind().execute(sa.text('ALTER TABLE core_organization_brands RENAME TO core_organization_brands_temp2;'))
    op.get_bind().execute(sa.text('ALTER TABLE core_organization_brands_temp RENAME TO core_organization_brands;'))

    op.get_bind().execute(sa.text(
    ''' 
        ALTER SEQUENCE "core_organization_brands_temp_id_seq" RENAME TO "core_organization_brands_id_seq";
        SELECT setval('core_organization_brands_id_seq', (select max(id) from core_organization_brands));
    '''))

def downgrade():
    op.get_bind().execute(sa.text('ALTER TABLE core_organization_brands RENAME TO core_organization_brands_temp;'))
    op.get_bind().execute(sa.text('ALTER TABLE core_organization_brands_temp2 RENAME TO core_organization_brands;'))
