"""067_add_public_flag_to_shared_lib

Revision ID: 55f708b63c18
Revises: 22d0a3d97ef1
Create Date: 2017-02-27 13:45:35.487470

"""

# revision identifiers, used by Alembic.
revision = '55f708b63c18'
down_revision = '27862c4c3cdd'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('cas_shared_libraries',
                  sa.Column('public_library', sa.Boolean(), nullable=True, server_default=sa.literal(False)))
    op.get_bind().execute(sa.text('UPDATE cas_shared_libraries SET public_library = false;'))
    op.get_bind().execute(sa.text('ALTER TABLE cas_shared_libraries ALTER COLUMN public_library DROP NOT NULL;'))


def downgrade():
    op.drop_column('cas_shared_libraries', 'public_library')
