"""072_campaign_clients

Revision ID: 18594017165a
Revises: 1a1a9811c0tk
Create Date: 2017-06-14 13:44:44.253326

"""

# revision identifiers, used by Alembic.
revision = '38492aqgk16n'
down_revision = '18594017165a'

from alembic import op
import sqlalchemy as sa


def upgrade():

    op.create_table(
        'core_pointcutexpires',
        sa.Column('id', sa.INTEGER, primary_key=True),
        sa.Column('user_id', sa.Integer()),
        sa.Column('cut_point', sa.Integer()),
        sa.Column('cut_datetime', sa.DateTime())
    )

    op.add_column('core_userpoints', sa.Column('cut_datetime', sa.DateTime()))
    op.add_column('core_userpoints', sa.Column('notify_expired', sa.BOOLEAN))

def downgrade():
    op.drop_table('core_pointcutexpires')

    op.drop_column('core_userpoints', 'cut_datetime')
    op.drop_column('core_userpoints', 'nofity_expired')
