"""empty message

Revision ID: 5c26fdce8wc6
Revises: 3c25ffcd83d6
Create Date: 2015-08-13 14:20:41.668036

"""

# revision identifiers, used by Alembic.
revision = '5c26fdce8wc6'
down_revision = '3c25ffcd83d6'

from alembic import op
import sqlalchemy as sa

def upgrade():
    op.add_column('core_discounts', sa.Column('vendor_participation', sa.Integer()))
    op.add_column('core_discounts', sa.Column('partner_participation', sa.Integer()))
    op.add_column('core_discounts', sa.Column('sales_recognition', sa.Integer()))

def downgrade():
    op.drop_column('core_discounts', 'vendor_participation')
    op.drop_column('core_discounts', 'partner_participation')
    op.drop_column('core_discounts', 'sales_recognition')
