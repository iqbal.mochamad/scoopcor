"""set foreignkey for vendor revenue share

Revision ID: dde54d72aff
Revises: 2a9b8e1de2a2
Create Date: 2015-09-28 13:46:25.332142

"""

# revision identifiers, used by Alembic.
revision = 'dde54d72aff'
down_revision = '2a9b8e1de2a2'

from alembic import op


def upgrade():
    op.create_foreign_key("fk_vendorrevenueshares_vendor_id", "core_vendorrevenueshares", "core_vendors", ["vendor_id"], ["id"])
    op.create_foreign_key("fk_vendorlinks_vendor_id", "core_vendorlinks", "core_vendors", ["vendor_id"], ["id"])


def downgrade():
    op.drop_constraint("fk_vendorrevenueshares_vendor_id", "core_vendorrevenueshares")
    op.drop_constraint("fk_vendorlinks_vendor_id", "core_vendorlinks")