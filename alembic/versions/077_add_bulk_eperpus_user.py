"""
    077 added bulk eperpus user

"""

# revision identifiers, used by Alembic.
revision = 'lqa427vvsipr'
down_revision = 'ijs487wwopja'

from alembic import op
import sqlalchemy as sa

def upgrade():
    op.create_table(
        'core_eperpus_upload',
        sa.Column('id', sa.INTEGER, primary_key=True),
        sa.Column('organization_id', sa.Integer(), sa.ForeignKey('cas_organizations.id')),
        sa.Column('upload_date', sa.DateTime(timezone=True)),
        sa.Column('upload_file', sa.String(250)),
        sa.Column('upload_type', sa.String(20)),
        sa.Column('complete_file', sa.String(250)),
        sa.Column('status', sa.Integer()),
        sa.Column('error_status', sa.String(250))
    )

def downgrade():
    op.drop_table('core_eperpus_upload')
