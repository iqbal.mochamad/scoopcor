"""rename wrong name models

Revision ID: faeebe69671
Revises: 517eaa2b816d
Create Date: 2015-11-03 10:54:05.003829

"""

# revision identifiers, used by Alembic.
revision = 'faeebe69671'
down_revision = '517eaa2b816d'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.alter_column('core_paymentgatewayformulas', column_name='payment_gateway_id',
                     new_column_name='paymentgateway_id')
    op.alter_column('core_paymentgatewaycosts_formula',
                    column_name='payment_id',
                     new_column_name='paymentgatewaycost_id')
    op.alter_column('core_paymentgatewaycosts_formula',
                    column_name='formula_id',
                     new_column_name='paymentgatewayformula_id')
    op.alter_column('core_revenueshareformula',
                    column_name='revenue_id',
                    new_column_name='revenueshare_id')
    op.alter_column('core_revenueshareformula',
                    column_name='formula_id',
                    new_column_name='revenueformula_id')

    op.rename_table('core_revenueshareformula', 'core_revenueshares_formulas')
    op.rename_table('core_revenueformulas', 'core_revenuesformulas')
    op.rename_table('core_paymentgatewaycosts_formula',
                    'core_paymentgatewaycosts_formulas')



def downgrade():
    pass
