"""Merge revision

Revision ID: 4145980e597
Revises: ('4942b8ce882d', '24d2691a08bc')
Create Date: 2015-12-04 00:12:24.172089

"""

# revision identifiers, used by Alembic.
revision = '4145980e597'
down_revision = ('4942b8ce882d', '24d2691a08bc')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
