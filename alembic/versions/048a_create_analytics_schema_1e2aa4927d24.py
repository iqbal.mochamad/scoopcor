"""create analytics schema

Revision ID: 1e2aa4927d24
Revises: e5eede664de
Create Date: 2016-05-30 14:01:49.201675

"""

# revision identifiers, used by Alembic.
revision = '1e2aa4927d24'
down_revision = 'e5eede664de'

from alembic import op
import sqlalchemy as sa

from sqlalchemy.dialects.postgresql import JSONB


def upgrade():

    op.execute("CREATE SCHEMA IF NOT EXISTS  analytics;")
    op.create_table(
        'user_download',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_activity', JSONB(), nullable=False),
        schema='analytics'
    )

    op.create_table(
        'user_page_view',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_activity', JSONB(), nullable=False),
        schema='analytics'
    )

    op.create_table(
        'user_search',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_activity', JSONB(), nullable=False),
        schema='analytics'
    )

    op.create_table(
        'user_guest_login',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_activity', JSONB(), nullable=False),
        schema='analytics'
    )

    op.create_table(
        'user_app_open',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_activity', JSONB(), nullable=False),
        schema='analytics'
    )

    op.execute("CREATE INDEX user_page_view_user_activity_gin "
                 "ON analytics.user_page_view USING GIN (user_activity)")
    op.execute('CREATE INDEX user_page_view_datetime_epoch_idx '
                 'ON analytics.user_page_view ((user_activity #> \'{\"user_id\"}\'), '
                 'cast(user_activity #>> \'{datetime_epoch}\' AS FLOAT))')
    op.execute("CREATE INDEX user_page_view_item_idx "
                 "ON analytics.user_page_view ((user_activity #> \'{\"item_id\"}\'))")
    op.execute("CREATE INDEX user_page_view_client_id_idx "
                 "ON analytics.user_page_view ((user_activity #> \'{\"client_id\"}\'))")

    op.execute("CREATE INDEX user_download_user_activity_gin "
                 "ON analytics.user_download USING GIN (user_activity)")
    op.execute('CREATE INDEX user_download_datetime_epoch_idx '
                 'ON analytics.user_download ((user_activity #> \'{\"user_id\"}\'), '
                 'cast(user_activity #>> \'{datetime_epoch}\' AS FLOAT))')
    op.execute("CREATE INDEX user_download_item_idx "
                 "ON analytics.user_download ((user_activity #> \'{\"item_id\"}\'))")
    op.execute("CREATE INDEX user_download_client_id_idx "
                 "ON analytics.user_download ((user_activity #> \'{\"client_id\"}\'))")

    op.execute("CREATE INDEX user_search_user_activity_gin "
                 "ON analytics.user_search USING GIN (user_activity)")
    op.execute('CREATE INDEX user_search_datetime_epoch_idx '
                 'ON analytics.user_search ((user_activity #> \'{\"user_id\"}\'), '
                 'cast(user_activity #>> \'{datetime_epoch}\' AS FLOAT))')
    op.execute("CREATE INDEX user_search_client_id_idx "
                 "ON analytics.user_search ((user_activity #> \'{\"client_id\"}\'))")

    op.execute("CREATE INDEX user_app_open_user_activity_gin "
                 "ON analytics.user_app_open USING GIN (user_activity)")
    op.execute('CREATE INDEX user_app_open_datetime_epoch_idx '
                 'ON analytics.user_app_open ((user_activity #> \'{\"user_id\"}\'), '
                 'cast(user_activity #>> \'{datetime_epoch}\' AS FLOAT))')
    op.execute("CREATE INDEX user_app_open_client_id_idx "
                 "ON analytics.user_app_open ((user_activity #> \'{\"client_id\"}\'))")

    op.execute("CREATE INDEX user_guest_login_user_activity_gin "
                 "ON analytics.user_guest_login USING GIN (user_activity)")
    op.execute('CREATE INDEX user_guest_login_datetime_epoch_idx '
                 'ON analytics.user_guest_login ((user_activity #> \'{\"user_id\"}\'), '
                 'cast(user_activity #>> \'{datetime_epoch}\' AS FLOAT))')
    op.execute("CREATE INDEX user_guest_login_client_id_idx "
                 "ON analytics.user_guest_login ((user_activity #> \'{\"client_id\"}\'))")


def downgrade():
    pass
