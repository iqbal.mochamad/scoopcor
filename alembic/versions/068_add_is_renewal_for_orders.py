"""068_add_is_renewal_for_orders

Revision ID: 32f70ub62c10
Revises: 22d0a3d97ef1
Create Date: 2017-03-22 13:45:35.487470

"""

# revision identifiers, used by Alembic.
revision = '32f70ub62c10'
down_revision = '55f708b63c18'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('core_orders', sa.Column('is_renewal', sa.Boolean(), nullable=True, server_default=sa.literal(False)))
    op.add_column('core_ordertemps', sa.Column('is_renewal', sa.Boolean(), nullable=True, server_default=sa.literal(False)))

def downgrade():
    op.drop_column('core_ordertemps', 'is_renewal')
    op.drop_column('core_orders', 'is_renewal')
