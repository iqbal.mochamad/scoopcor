# Revision identifier, used by Alembic


from alembic import op
import sqlalchemy as sa

revision = 'CvHGoi7hbhF'
down_revision = 'hakkjkk45jh1jG'


def upgrade():
    op.create_table(
        'core_eperpus_transactions',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('organization_id', sa.Integer(), sa.ForeignKey('cas_organizations.id')),
        sa.Column('catalog_id', sa.Integer(), sa.ForeignKey('core_catalogs.id')),
        sa.Column('upload_date', sa.DateTime(timezone=True)),
        sa.Column('upload_file', sa.String(250)),
        sa.Column('upload_type', sa.String(20)),
        sa.Column('complete_file', sa.String(250)),
        sa.Column('status', sa.Integer()),
        sa.Column('error_status', sa.String(250))
    )


def downgrade():
    op.drop_table('core_eperpus_transactions')
