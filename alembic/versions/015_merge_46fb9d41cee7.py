"""empty message

Revision ID: 46fb9d41cee7
Revises: ('3b5f76ef5ed8', '6e392222f61', '14dcacf41a5e', '511d992af16c')
Create Date: 2015-08-14 13:13:12.938842

"""

# revision identifiers, used by Alembic.
revision = '46fb9d41cee7'
down_revision = ('3b5f76ef5ed8', '6e392222f61', '14dcacf41a5e', '511d992af16c')

from alembic import op
import sqlalchemy as sa


def upgrade():
    pass


def downgrade():
    pass
