"""add shared library table attribute

Revision ID: e5eede664de
Revises: 2c14545c0d7c
Create Date: 2016-04-27 15:19:02.632144

"""
import logging

from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'e5eede664de'
down_revision = '12b877ac02f1'

_log = logging.getLogger('alembic.shared_library_migration')


def upgrade():
    _log.info('creating core_catalogs table')

    conn = op.get_bind()

    op.add_column('cas_organizations',
                  sa.Column('user_concurrent_borrowing_limit', sa.INTEGER))
    op.add_column('cas_organizations',
                  sa.Column('reborrowing_cooldown', sa.Interval))
    op.add_column('cas_organizations',
                  sa.Column('borrowing_time_limit', sa.Interval))

    conn.execute("update cas_organizations set "
                 "user_concurrent_borrowing_limit=coalesce(user_concurrent_borrowing_limit, 0), "
                 "reborrowing_cooldown=coalesce(reborrowing_cooldown, '0D'), "
                 "borrowing_time_limit=coalesce(borrowing_time_limit, '7D')")

    op.alter_column('cas_organizations', 'user_concurrent_borrowing_limit',
                    existing_type=sa.INTEGER, nullable=False)
    op.alter_column('cas_organizations', 'reborrowing_cooldown',
                    existing_type=sa.Interval, nullable=False)
    op.alter_column('cas_organizations', 'borrowing_time_limit',
                    existing_type=sa.Interval, nullable=False)


    _log.info('creating core_organization_items table')
    op.create_table(
        'core_organization_items',
        sa.Column('organization_id', sa.INTEGER, sa.ForeignKey('cas_organizations.id')),
        sa.Column('item_id', sa.INTEGER, sa.ForeignKey('core_items.id')),
        sa.Column('quantity', sa.INTEGER, nullable=False),
        sa.Column('quantity_available', sa.INTEGER, nullable=False),
        sa.Column('item_name_override', sa.String(250)),
        sa.Column('created', sa.DateTime),
        sa.Column('modified', sa.DateTime),
        sa.PrimaryKeyConstraint('organization_id', 'item_id')
    )
    op.alter_column('core_organization_items', 'quantity',
                    existing_type=sa.INTEGER, nullable=False)
    op.alter_column('core_organization_items', 'quantity_available',
                    existing_type=sa.INTEGER, nullable=False)

    op.create_table(
        'core_catalogs',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(50), nullable=False),
        sa.Column('organization_id', sa.INTEGER, sa.ForeignKey('cas_organizations.id')),
        sa.Column('created', sa.DateTime),
        sa.Column('modified', sa.DateTime),
    )

    _log.info('creating core_catalog_items table')
    op.create_table(
        'core_catalog_items',
        sa.Column('catalog_id', sa.INTEGER, sa.ForeignKey('core_catalogs.id')),
        sa.Column('shared_library_item_org_id', sa.INTEGER, sa.ForeignKey('cas_organizations.id')),
        sa.Column('shared_library_item_item_id', sa.INTEGER, sa.ForeignKey('core_items.id')),
        sa.PrimaryKeyConstraint('catalog_id', 'shared_library_item_org_id', 'shared_library_item_item_id')
    )
    op.create_foreign_key(
        'core_catalog_items_organization_item_fkey',
        'core_catalog_items', 'core_organization_items',
        ['shared_library_item_org_id', 'shared_library_item_item_id'],
        ['organization_id', 'item_id']
    )

    _log.info('creating core_user_borrowed_items table')
    op.create_table(
        'core_user_borrowed_items',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id', sa.INTEGER, sa.ForeignKey('cas_users.id')),
        sa.Column('catalog_item_catalog_id', sa.INTEGER, sa.ForeignKey('core_catalogs.id')),
        sa.Column('catalog_item_org_id', sa.INTEGER, sa.ForeignKey('cas_organizations.id')),
        sa.Column('catalog_item_item_id', sa.INTEGER, sa.ForeignKey('core_items.id')),
        sa.Column('borrowing_start_time', sa.DateTime, nullable=False),
        sa.Column('returned_time', sa.DateTime, nullable=True),
    )
    op.create_foreign_key(
        'core_user_borrowed_items_catalog_items_organization_item_fkey',
        'core_user_borrowed_items', 'core_catalog_items',
        ['catalog_item_catalog_id', 'catalog_item_org_id', 'catalog_item_item_id'],
        ['catalog_id',
         'shared_library_item_org_id',
         'shared_library_item_item_id']
    )

    # should be run manually in progress, cannot from alembic (ALTER TYPE ... ADD cannot run inside a transaction block)
    # conn.execute("ALTER TYPE public.organization_type ADD VALUE 'shared library'")


def downgrade():
    op.drop_table('core_user_borrowed_items')
    op.drop_table('core_organization_items')
    op.drop_table('core_catalog_items')
    op.drop_table('core_catalogs')
