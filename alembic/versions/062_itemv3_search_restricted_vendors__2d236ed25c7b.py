"""062_itemv3_search_restricted_vendors_for_kg_smart

Revision ID: 2d236ed25c7b
Revises: 3cdc05838e1e
Create Date: 2016-10-18 18:36:18.385329

"""

# revision identifiers, used by Alembic.
revision = '2d236ed25c7b'
down_revision = '3cdc05838e1e'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table('cas_shared_libraries_restricted_vendors',
                    sa.Column('organization_id', sa.Integer(),
                              sa.ForeignKey('cas_organizations.id'), primary_key=True),
                    sa.Column('vendor_id', sa.Integer(), sa.ForeignKey('core_vendors.id'), primary_key=True)
                    )


def downgrade():
    op.drop_table('cas_shared_libraries_restricted_vendors')
