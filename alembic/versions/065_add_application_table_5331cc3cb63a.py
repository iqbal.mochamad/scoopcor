"""065_add_application_table

Revision ID: 5331cc3cb63a
Revises: 483a2148de7f
Create Date: 2016-11-02 10:48:15.205890

"""

# revision identifiers, used by Alembic.
revision = '5331cc3cb63a'
down_revision = 'e1320abbfd3'

from alembic import op
import sqlalchemy as sa


def upgrade():
    conn = op.get_bind()

    op.create_table(
        'applications',
        sa.Column('id', sa.INTEGER, primary_key=True),
        sa.Column('name', sa.String(250)),
        sa.Column('max_devices', sa.INTEGER),
        sa.Column('enable_unlimited_devices', sa.Boolean()),
        sa.Column('created', sa.DateTime(timezone=True)),
        sa.Column('modified', sa.DateTime(timezone=True)),
        sa.Column('is_active', sa.BOOLEAN)
    )
    conn.execute(sa.text('ALTER TABLE applications ALTER enable_unlimited_devices SET DEFAULT false'))
    conn.execute(sa.text('ALTER TABLE applications ALTER created SET DEFAULT now()'))
    conn.execute(sa.text('ALTER TABLE applications ALTER modified SET DEFAULT now()'))
    conn.execute(sa.text('ALTER TABLE applications ALTER is_active SET DEFAULT true'))

    op.add_column('cas_clients',
                  sa.Column('application_id', sa.INTEGER,
                            sa.ForeignKey('applications.id'),
                            nullable=True))

    # CREATE application for scoop mobile apps ios and android
    conn.execute(sa.text("""INSERT INTO applications (id, name, max_devices, enable_unlimited_devices)
        VALUES (1, 'SCOOP mobile apps', 5, false)"""))
    # CREATE application for getscoop.com and scoop portal web (angular)
    conn.execute(sa.text("""INSERT INTO applications (id, name, max_devices, enable_unlimited_devices)
        VALUES (2, 'SCOOP Web and Portal', 0, true)"""))
    conn.execute(sa.text("""INSERT INTO applications (id, name, max_devices, enable_unlimited_devices)
        VALUES (3, 'KG Smart', 2, false)"""))
    conn.execute(sa.text("""INSERT INTO applications (id, name, max_devices, enable_unlimited_devices)
        VALUES (4, 'ePerpus Apps', 1, false)"""))
    conn.execute(sa.text("""INSERT INTO applications (id, name, max_devices, enable_unlimited_devices)
        VALUES (5, 'Other Apps', 5, false)"""))
    conn.execute(sa.text("""SELECT setval('applications_id_seq', 6, true)"""))

    # update scoop mobile apps ios and android
    conn.execute(sa.text("""UPDATE cas_clients SET application_id = 1 WHERE id IN (1, 2, 3)"""))
    # update getscoop.com/7 and scoop portal web
    conn.execute(sa.text("""UPDATE cas_clients SET application_id = 2 WHERE id IN (7, 67)"""))
    # eperpus
    conn.execute(sa.text("""UPDATE cas_clients SET application_id = 3 WHERE id IN (85, 86)"""))
    conn.execute(sa.text("""UPDATE cas_clients SET application_id = 4 WHERE id IN (83, 84)"""))
    # other apps
    conn.execute(sa.text("""UPDATE cas_clients SET application_id = 5 WHERE application_id is null"""))


def downgrade():
    op.drop_column('cas_clients', 'application_id')
    op.drop_table('applications')
