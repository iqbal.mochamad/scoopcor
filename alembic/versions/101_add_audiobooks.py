"""
    add new table core_items_audiobooks and core_audiobooks
"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

revision = 'audiojksml12'
down_revision = 'ui1928skqjw'


def upgrade():

    # already tested in staging, this line must comment out.
    # everytime otomatic pipe line execute alembic upgrade head, this script will make exclusive lock db to core_items.
    # dont know why alembic not raise column already exist if column already created.., before insert new column. need research to handle this case.
    # op.add_column('core_items', sa.Column('parent_item_id', sa.Integer()))

    op.create_table(
        'core_audiobooks',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('title', sa.String(250)),
        sa.Column('subtitle', sa.String(100)),
        sa.Column('chapter', sa.Integer()),
        sa.Column('file_name', sa.String(100)),
        sa.Column('file_size', sa.Integer()),
        sa.Column('file_time', sa.String(25)),
        sa.Column('file_pattern', sa.ARRAY(sa.Text)),
        sa.Column('item_id', sa.Integer()),
        sa.Column('description', sa.Text),
        sa.Column('hash_key', sa.String(250)),
        sa.Column('is_active', sa.Boolean()),
        sa.Column('metainfo', postgresql.JSONB()),
    )

    op.create_table(
        'core_items_audiobooks',
        sa.Column('item_id', sa.Integer()),
        sa.Column('audiobook_id', sa.Integer()))

def downgrade():
    op.drop_table('core_audiobooks')
    op.drop_table('core_items_audiobooks')
