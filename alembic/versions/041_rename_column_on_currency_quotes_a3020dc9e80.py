"""rename column on currency quotes

Revision ID: a3020dc9e80
Revises: 981a31e9f01
Create Date: 2015-12-07 14:47:48.391121

"""

# revision identifiers, used by Alembic.
revision = 'a3020dc9e80'
down_revision = '981a31e9f01'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.drop_column('core_currencyquotes', 'time_frame')
    op.drop_column('core_currencyquotes', 'timestamp')
    op.add_column('core_currencyquotes',
                  sa.Column(
                      'valid_from', sa.DateTime,
                  ))
    op.add_column('core_currencyquotes',
                  sa.Column(
                      'valid_to', sa.DateTime,
                  ))


def downgrade():
    pass
