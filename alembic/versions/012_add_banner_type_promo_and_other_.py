"""add banner type promo and other attributes

Revision ID: 14dcacf41a5e
Revises: 123a239e084f
Create Date: 2015-08-11 13:52:37.381364

"""

# revision identifiers, used by Alembic.
revision = '14dcacf41a5e'
down_revision = '123a239e084f'

from alembic import op
from sqlalchemy.dialects import postgresql as pg
import sqlalchemy as sa


def upgrade():

    conn = op.get_bind()
    conn.execute("COMMIT;")
    conn.execute("ALTER TYPE scoop_banner_type ADD VALUE IF NOT EXISTS 'promo';")
    conn.execute("CREATE TYPE scoop_item_price AS ENUM ('single', 'subscription');")
    conn.execute("BEGIN READ WRITE;")

    op.add_column('core_banners', sa.Column('term_and_condition', sa.Text()))
    op.add_column('core_banners', sa.Column('image_social_media', sa.Text()))
    op.add_column('core_banners', sa.Column('slug', sa.Text()))
    op.add_column('core_banners', sa.Column('product_per_page', sa.Integer()))
    op.add_column('core_banners', sa.Column('show_title', sa.Boolean()))

    op.add_column('core_banners', sa.Column('show_price', pg.ENUM('single', 'subscription', name='scoop_item_price'),
                                            nullable=True)
                  )



def downgrade():

    op.drop_column('core_banners', 'term_and_condition')
    op.drop_column('core_banners', 'image_social_media')
    op.drop_column('core_banners', 'slug')
    op.drop_column('core_banners', 'product_per_page')
    op.drop_column('core_banners', 'show_title')
    conn = op.get_bind()
    conn.execute("DROP TYPE IF EXISTS scoop_item_price;")

