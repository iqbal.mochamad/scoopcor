"""
    add core_paymentgopays

    Revision ID: ioa2klo6ql94
    Revises: a3ae4167594
    Create Date: 2019-03-06 12:02:34.863000

"""

# revision identifiers, used by Alembic.
revision = 'nmsque12942'
down_revision = '401fd7190336'

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql

midtrans_status = postgresql.ENUM(u'pending', u'expire', u'deny', u'cancel', u'refund', u'capture', u'settlement',
                                  name='midtrans_status', create_type=False)

def upgrade():
    midtrans_status.create(op.get_bind())

    op.create_table(
        'core_paymentgopays',
        sa.Column('created', sa.DateTime()),
        sa.Column('modified', sa.DateTime()),
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('order_id', sa.Integer()),
        sa.Column('status', midtrans_status),
        sa.Column('status_url', sa.Text()),
        sa.Column('data_request', postgresql.JSONB()),
        sa.Column('data_response', postgresql.JSONB())
    )

def downgrade():
    op.drop_table('core_paymentgopays')



