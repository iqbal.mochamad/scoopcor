"""Add 'precedence' field to item_category m2m table

Revision ID: 2d8640d2b4ea
Revises: 43978ada710e
Create Date: 2015-05-21 10:40:18.829126

"""

# revision identifiers, used by Alembic.
revision = '2d8640d2b4ea'
down_revision = '1ecaa84c94b7'

from collections import namedtuple

from alembic import op
import sqlalchemy as sa


def upgrade():

    conn = op.get_bind()

    # we currently have no primary key on this m2m in the database
    # and have lots of duplicate data in the database.
    # remove those duplicates.
    remove_duplicate_item_categories(conn)

    op.add_column("core_brands",
                  sa.Column("primary_category_id",
                            sa.Integer(),
                            sa.ForeignKey("core_categories.id")))

    assign_brand_primary_categories(conn)

    # apply new primary key and set all the fields to non-nullable
    op.create_primary_key(
        name="core_items_categories_pk",
        table_name="core_items_categories",
        cols=["item_id", "category_id",]
    )
    op.alter_column("core_items_categories", "item_id", nullable=False)
    op.alter_column("core_items_categories", "category_id", nullable=False)


def downgrade():

    op.drop_constraint(
        name='core_items_categories_pk',
        table_name='core_items_categories',
        type_='primary'
    )

    op.drop_column('core_brands', 'primary_category_id')


def remove_duplicate_item_categories(conn):

    # find all the duplicates
    duplicates = conn.execute(
        sa.select([core_items_categories.c.item_id,
                   core_items_categories.c.category_id])
            .group_by(
                core_items_categories.c.item_id,
                core_items_categories.c.category_id)
            .having(sa.func.count() > 1))\
        .fetchall()

    # we have to delete ALL the duplicates and reinsert because
    # we have no way to uniquely-identify a row without a primary key
    for row in duplicates:

        conn.execute(
            core_items_categories
                .delete()
                .where(
                    (core_items_categories.c.item_id == row.item_id) &
                    (core_items_categories.c.category_id == row.category_id)
        ))

        conn.execute(
            core_items_categories
                .insert()
                .values(
                    item_id=row.item_id,
                    category_id=row.category_id
        ))


def assign_brand_primary_categories(conn):

    for primary_category in primary_categories:
        conn.execute(
            core_brands
                .update()
                .where(core_brands.c.id == primary_category.brand_id)
                .values(primary_category_id=primary_category.category_id))


# Frozen Schema
meta = sa.MetaData()
core_brands = sa.Table(
    'core_brands',
    meta,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('primary_category_id', sa.Integer, sa.ForeignKey('core_categories.id'))
)

core_items = sa.Table(
    'core_items',
    meta,
    sa.Column('id', sa.Integer, primary_key=True),
    sa.Column('brand_id', sa.Integer, sa.ForeignKey('core_brands.id'))
)

core_items_categories = sa.Table(
    'core_items_categories',
    meta,
    sa.Column('item_id', sa.Integer, sa.ForeignKey('core_items.id')),
    sa.Column('category_id', sa.Integer)
)


# Marketing-specified list of brands -> primary_categories
BrandPrimaryCategory = namedtuple("BrandPrimaryCategory", ['brand_id', 'category_id'])
primary_categories = [BrandPrimaryCategory(brand_id=t[0], category_id=t[1]) for t in (

    # BOOKS
    (3427,85),
    (3668,85),
    (3765,85),
    (3767,85),
    (3771,85),
    (3775,85),
    (4954,85),
    (4956,85),
    (4964,85),
    (4979,85),
    (4980,85),
    (4981,85),
    (4996,85),
    (4998,85),
    (6166,85),
    (6545,85),
    (6599,85),
    (8030,85),
    (9569,85),
    (9659,85),
    (14975,85),
    (15822,85),
    (15836,85),
    (17567,85),
    (18790,85),
    (18791,85),
    (18792,85),
    (283,17),
    (284,17),
    (1364,18),
    (5369,18),
    (257,19),
    (258,19),
    (259,19),
    (322,19),
    (323,19),
    (324,19),
    (983,19),
    (1177,19),
    (2458,19),
    (3011,19),
    (3875,19),
    (4196,19),
    (4982,19),
    (5330,19),
    (5332,19),
    (5343,19),
    (5363,19),
    (5365,19),
    (5423,19),
    (5440,19),
    (5642,19),
    (5699,19),
    (5710,19),
    (5775,19),
    (5785,19),
    (20716,19),
    (21978,19),
    (22371,19),
    (23259,19),
    (23260,19),
    (7711,72),
    (260,20),
    (419,20),
    (420,20),
    (421,20),
    (423,20),
    (542,20),
    (1059,20),
    (1065,20),
    (3385,20),
    (3386,20),
    (3387,20),
    (17163,20),
    (18826,20),
    (18827,20),
    (18828,20),
    (18829,20),
    (18830,20),
    (18831,20),
    (18832,20),
    (20361,20),
    (21117,20),
    (21174,20),
    (21175,20),
    (20489,78),
    (20491,78),
    (20492,78),
    (21196,78),
    (21197,78),
    (22131,78),
    (22137,78),
    (22138,78),
    (23221,78),
    (5381,21),
    (7711,5),
    (333,22),
    (371,22),
    (372,22),
    (373,22),
    (375,22),
    (3694,22),
    (5176,22),
    (5405,22),
    (5422,22),
    (5427,22),
    (5449,22),
    (5791,22),
    (21622,22),
    (21887,22),
    (22209,22),
    (22210,22),
    (22211,22),
    (22614,22),
    (233,24),
    (234,24),
    (238,24),
    (244,24),
    (246,24),
    (248,24),
    (250,24),
    (261,24),
    (278,24),
    (279,24),
    (280,24),
    (281,24),
    (282,24),
    (350,24),
    (351,24),
    (352,24),
    (426,24),
    (1062,24),
    (1063,24),
    (1911,24),
    (5374,24),
    (5377,24),
    (5379,24),
    (5545,24),
    (262,23),
    (300,23),
    (337,23),
    (339,23),
    (343,23),
    (1061,23),
    (1070,23),
    (1248,23),
    (1585,23),
    (1958,23),
    (1959,23),
    (3878,23),
    (4079,23),
    (4807,23),
    (4808,23),
    (4931,23),
    (247,25),
    (418,25),
    (1261,25),
    (4173,25),
    (4765,25),
    (4778,25),
    (5164,25),
    (9127,25),
    (407,26),
    (408,26),
    (1981,26),
    (1982,26),
    (1983,26),
    (2411,26),
    (9255,26),
    (9353,26),
    (331,27),
    (1572,27),
    (4654,27),
    (4656,27),
    (4669,27),
    (4671,27),
    (4672,27),
    (4677,27),
    (4682,27),
    (263,28),
    (23258,28),
    (1064,29),
    (1573,29),
    (3231,29),
    (3736,29),
    (4762,29),
    (8298,29),
    (980,74),
    (982,74),
    (1210,74),
    (1333,74),
    (1339,74),
    (1351,74),
    (3034,74),
    (5238,74),
    (5239,74),
    (21112,74),
    (21881,74),
    (243,31),
    (264,31),
    (265,31),
    (347,31),
    (1922,31),
    (9494,31),
    (9495,31),
    (9496,31),
    (18837,31),
    (23145,31),
    (23255,31),
    (329,32),
    (340,32),
    (345,32),
    (348,32),
    (413,32),
    (414,32),
    (415,32),
    (416,32),
    (417,32),
    (424,32),
    (425,32),
    (2496,32),
    (2553,32),
    (2554,32),
    (2555,32),
    (2557,32),
    (2558,32),
    (5424,32),
    (19026,32),
    (19027,32),
    (19406,32),
    (240,33),
    (252,33),
    (253,33),
    (266,33),
    (267,33),
    (294,33),
    (295,33),
    (296,33),
    (297,33),
    (298,33),
    (299,33),
    (312,33),
    (313,33),
    (330,33),
    (1357,33),
    (21888,33),
    (239,34),
    (268,34),
    (269,34),
    (327,34),
    (328,34),
    (332,34),
    (1060,34),
    (1060,34),
    (4933,34),
    (5147,34),
    (5527,34),
    (5558,34),
    (9064,34),
    (20362,34),
    (22864,34),
    (23102,34),
    (235,35),
    (236,35),
    (241,35),
    (270,35),
    (271,35),
    (346,35),
    (430,35),
    (1005,35),
    (1066,35),
    (1067,35),
    (1068,35),
    (1069,35),
    (1213,35),
    (1319,35),
    (3246,35),
    (4104,35),
    (4858,35),
    (5296,35),
    (22129,35),
    (23256,35),
    (272,36),
    (325,36),
    (338,36),
    (344,36),
    (1058,36),
    (3291,36),
    (3343,36),
    (3729,36),
    (4594,36),
    (5001,36),
    (5305,36),
    (5373,36),
    (5504,36),
    (5622,36),
    (5695,36),
    (5806,36),
    (5833,36),
    (5834,36),
    (8807,36),
    (8868,36),
    (9313,36),
    (9427,36),
    (9634,36),
    (9804,36),
    (18931,36),
    (18933,36),
    (19324,36),
    (20717,36),
    (245,37),
    (249,37),
    (1546,37),
    (273,38),
    (285,38),
    (422,38),
    (427,38),
    (428,38),
    (2104,38),
    (3327,38),
    (5303,38),
    (22810,39),
    (1315,40),
    (21116,40),
    (21774,40),
    (21775,40),

    # OTHERS
    (2432,49),
    (4557,49),
    (4619,49),
    (808,6),
    (1409,6),
    (4645,6),
    (12216,6),
    (13504,6),
    (21137,6),
    (941,72),
    (4044,72),
    (56,5),
    (7711,5),
    (8263,5),
    (10148,5),
    (16997,5),
    (1172,51),
    (5615,51),
    (17314,51),
    (17576,51),
    (2373,52),
    (2591,52),
    (16,10),
    (35,10),
    (2600,10),
    (2702,10),
    (7515,10),
    (7523,10),
    (2706,11),
    (2921,11),
    (20478,11),
    (1515,56),
    (671,3),
    (3403,3),
    (6447,3),
    (8773,3),
    (16064,3),
    (18076,3),
    (21482,3),
    (23585,3),
    (1367,58),
    (5567,58),
    (7522,58),
    (676,2),
    (22760,2),
    (675,12),
    (1661,12),
    (4582,12),
    (7693,12),
    (23586,12),
    (879,34),
    (807,56),
    (942,56),
    (2080,56),
    (370,40),
    (921,55),
    (922,55),
    (1190,55),
    (2778,55),
    (2862,55),
    (3039,55),
    (3040,55),
    (6275,55),
    (8867,55),
    (2588,8),
    (2726,8),
    (5851,8),
    (2703,4),
)]
