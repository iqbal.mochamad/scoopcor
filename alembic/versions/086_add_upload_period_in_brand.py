"""add release period in Brand

Revision ID: 37268130a15b
Revises: koaslquemxk12
Create Date: 2018-11-21 09:55:15.022000

"""

# revision identifiers, used by Alembic.
revision = '33ab20b84e56'
down_revision = 'kidja1219ara'

import sqlalchemy as sa
from alembic import op
from sqlalchemy.dialects import postgresql as pg

# Daily, Weekly, Biweekly, Monthly, Bimonthly, Quarterly, Special
release_period_types = pg.ENUM(u'daily', u'weekly', u'biweekly',
                               u'monthly', u'bimonthly', u'quarterly', u'special',
                               name='release_period_types', create_type=False)


def upgrade():
    release_period_types.create(op.get_bind())

    op.add_column("core_brands",
                  sa.Column('release_period', release_period_types,
                            nullable=True,
                            default="daily"))


def downgrade():
    op.drop_column('core_brands', 'release_period')
    release_period_types.drop(op.get_bind())
