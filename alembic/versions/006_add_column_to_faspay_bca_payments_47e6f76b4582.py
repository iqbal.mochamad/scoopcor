"""Add column to faspay BCA payments

Revision ID: 47e6f76b4582
Revises: 5356fa66515b
Create Date: 2015-07-24 15:14:03.946888

"""

# revision identifiers, used by Alembic.
revision = '47e6f76b4582'
down_revision = '5356fa66515b'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql as pg


def upgrade():
    op.add_column('core_paymentbcaklikpays', sa.Column(
        'card_type',
        pg.ENUM(*PAYMENT_CARD_TYPES, name='payment_card_types'),
        nullable=False
    ))


def downgrade():
    op.drop_column('core_paymentbcaklikpays', 'card_type')


CREDIT_CARD = 'credit'
DEBIT_CARD = 'debit'
PAYMENT_CARD_TYPES = (
    CREDIT_CARD,
    DEBIT_CARD,
)
