"""
    add lowest_supported_version

    Revision ID: ioa2klo6ql94
    Revises: a3ae4167594
    Create Date: 2019-03-06 12:02:34.863000

"""

# revision identifiers, used by Alembic.
revision = 'hjiquq32241'
down_revision = 'nmsque12942'

import sqlalchemy as sa
from alembic import op


def upgrade():
    op.add_column('core_paymentgateways', sa.Column('lowest_supported_version', sa.String(20), nullable=True))

def downgrade():
    op.drop_column('core_paymentgateways', 'lowest_supported_version')
