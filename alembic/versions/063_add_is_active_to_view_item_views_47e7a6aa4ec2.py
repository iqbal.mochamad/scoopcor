"""063_add_is_active_to_view_item_views

Revision ID: 47e7a6aa4ec2
Revises: 2d236ed25c7b
Create Date: 2016-10-20 16:19:47.264482

"""

# revision identifiers, used by Alembic.
revision = '47e7a6aa4ec2'
down_revision = '2d236ed25c7b'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.get_bind().execute(sa.text("""
CREATE OR REPLACE VIEW public.view_items AS
  SELECT DISTINCT item.id,
    item.name AS item_name,
    utility.item_name_by_language(item.*, 'eng'::text) AS item_name_en,
    utility.item_name_by_language(item.*, 'id'::text) AS item_name_id,
    item.languages,
    ib.name AS brand_name,
    v.name AS vendor_name,
    item.countries,
    utility.author_names_for_item(item.id) AS author_names,
    utility.category_names_for_item(item.id) AS category_names,
    item.item_type,
    COALESCE(po_ios.price_usd, 0::numeric) AS price_ios_usd,
    COALESCE(po_ios.price_idr, 0::numeric) AS price_ios_idr,
    COALESCE(po_android.price_usd, 0::numeric) AS price_android_usd,
    COALESCE(po_android.price_idr, 0::numeric) AS price_android_idr,
    COALESCE(o.price_usd, 0::numeric) AS price_web_usd,
    COALESCE(o.price_idr, 0::numeric) AS price_web_idr,
    item.release_date AS item_release_date,
        CASE
            WHEN item.parentalcontrol_id = 2 THEN true
            ELSE false
        END AS is_age_restricted,
    'https://images.apps-foundry.com/magazine_static/'::text || item.thumb_image_highres::text AS item_thumb_highres,
    'https://images.apps-foundry.com/magazine_static/'::text || item.thumb_image_normal::text AS item_thumb_normal,
    ((('/mnt/nfs/nas/scoopcor_static/item_content_files/original/'::text || item.brand_id::text) || '/'::text) || item.edition_code::text) || '.pdf'::text AS file_path,
    item.modified,
    COALESCE(( SELECT ido.download_count
           FROM utility.item_downloads ido
          WHERE ido.brand_id = item.brand_id), 0::bigint) AS download_count,
    case when item.is_active = true and item.item_status = 'ready for consume'::item_status
        then true
      else false end as is_active
   FROM core_items item
     LEFT JOIN core_brands ib ON item.brand_id = ib.id
     LEFT JOIN core_vendors v ON v.id = ib.vendor_id
     LEFT JOIN core_offers_items oi ON oi.item_id = item.id
     LEFT JOIN core_offers o ON o.id = oi.offer_id AND o.offer_type_id = 1 and o.is_active = true
     LEFT JOIN core_platforms_offers po_ios ON po_ios.offer_id = o.id AND po_ios.platform_id = 1
     LEFT JOIN core_platforms_offers po_android ON po_android.offer_id = o.id AND po_android.platform_id = 2
  ORDER BY item.id DESC;
    """))


def downgrade():
    pass
