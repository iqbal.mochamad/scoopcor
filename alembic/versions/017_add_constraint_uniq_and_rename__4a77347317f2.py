""" rename the slug, regenerate new slug and add indexing for slug column

Revision ID: 4174e8cca59d
Revises: 2a9b8e1de2a2
Create Date: 2015-09-16 14:03:25.064662

"""
from __future__ import unicode_literals

revision = '4174e8cca59d'
down_revision = '2a9b8e1de2a2'

from alembic import op
import sqlalchemy as sa
from sqlalchemy import func, select

#from app.helpers import generate_slug
import roman
from slugify import slugify as new_slugify


def upgrade():
    conn = op.get_bind()

    for table in existing_tables:
        for bad_slug in [None, 'ii-', '']:
            regenerate_bad_slug(conn, table, bad_slug)

        rename_duplicates(conn, table, find_duplicate_slugs(conn, table))

        op.create_unique_constraint(table.name + '_unique_' + "slug", table.name, ['slug'])


def downgrade():
    for t in existing_tables:
        op.drop_constraint(t.name + '_unique_' + 'slug', t.name, 'unique')



meta = sa.MetaData()

# List of tables that we want to check the duplicates slugs.
existing_tables = [
    sa.Table('core_vendors',
             meta,
             sa.Column('id', sa.Integer),
             sa.Column('name', sa.String),
             sa.Column('slug', sa.String)
             ),

    sa.Table('core_brands',
             meta,
             sa.Column('id', sa.Integer),
             sa.Column('name', sa.String),
             sa.Column('slug', sa.String)
             ),

    sa.Table('core_partners',
             meta,
             sa.Column('id', sa.Integer),
             sa.Column('name', sa.String),
             sa.Column('slug', sa.String)
             ),

    sa.Table('core_authors',
             meta,
             sa.Column('id', sa.Integer),
             sa.Column('name', sa.String),
             sa.Column('slug', sa.String)
             ),

    sa.Table('core_banners',
             meta,
             sa.Column('id', sa.Integer),
             sa.Column('name', sa.String),
             sa.Column('slug', sa.String)
             ),

    sa.Table('core_items',
             meta,
             sa.Column('id', sa.Integer),
             sa.Column('name', sa.String),
             sa.Column('slug', sa.String)
             ),
]

def generate_slug(entity_name, conflicting_slug_count=0):
    """
    :param `basestring` entity_name:
    :param `int` conflicting_slug_count:
    :return: returning new slug
    :rtype: `unicode`
    """
    try:
        new_slug = new_slugify(entity_name)
        if conflicting_slug_count:
            new_slug += '-{suffix}'.format(
                suffix=roman.toRoman(conflicting_slug_count))

        return new_slug.lower()
    except Exception:
        print "Error generating slug for: ".format(entity_name)
        raise


def find_duplicate_slugs(conn, t):
    dup_slugs = conn.execute(select([t.c.slug, func.count()])
                             .where(t.c.name != None)
                             .group_by(t.c.slug)
                             .having(func.count() > 1))\
        .fetchall()
    return [row.slug for row in dup_slugs]


def rename_duplicates(conn, table, duplicate_slugs):

    if duplicate_slugs:
        for dup_slug in duplicate_slugs:
            slug_suffix = 0
            for entity in conn.execute(
                    table.select().where(table.c.slug == dup_slug).where(table.c.name != None))\
                    .fetchall():

                if entity.name:
                    if slug_suffix > 0:
                        new_slug = generate_slug(entity.slug if entity.slug is not None else entity.name, slug_suffix)
                        conn.execute(table.update()
                                     .where(table.c.id == entity.id)
                                     .values(slug=new_slug))
                    slug_suffix += 1

        rename_duplicates(conn, table, find_duplicate_slugs(conn, table))

    else:
        return None


def regenerate_bad_slug(conn, table, bad_slug):
    """ Find slugs in the database that were incorrectly generated.

    The values passed to bad_slug were generated from manually searching
    the database and finding them by eye.

    :param conn:
    :param `sqlalchemy.Table` table:
    :param `unicode` bad_slug:
    :return:
    """
    results = conn.execute(table.select()
                           .where(table.c.slug == bad_slug)
                           .where(table.c.name != None)
                           ).fetchall()
    for row in results:
        conn.execute(table.update()
                     .where(table.c.id == row.id)
                     .values(slug=generate_slug(row.name)))
