"""
    078 added pic marketing

"""

# revision identifiers, used by Alembic.
revision = '9qa53wklg9or'
down_revision = 'lqa427vvsipr'

from alembic import op
import sqlalchemy as sa

def upgrade():
    #op.add_column('cas_users', sa.Column('phone_number', sa.String(100)))
    op.add_column('cas_organizations', sa.Column('marketing_pic_id', sa.Integer()))
    op.add_column('cas_organizations', sa.Column('pic_client', sa.String(100)))


def downgrade():
    op.drop_column('cas_users', 'phone_number')
    op.drop_column('cas_organizations', 'marketing_pic_id')
    op.drop_column('cas_organizations', 'pic_client')
