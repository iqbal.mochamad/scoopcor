"""069_add_eperpus_watch_list

Revision ID: 3c190544908c
Revises: 32f70ub62c10
Create Date: 2017-04-05 14:19:26.649372

"""

# revision identifiers, used by Alembic.
revision = '3c190544908c'
down_revision = '32f70ub62c10'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'watch_list',
        sa.Column('id', sa.INTEGER, primary_key=True),
        sa.Column('user_id', sa.Integer(), sa.ForeignKey('cas_users.id')),
        sa.Column('stock_org_id', sa.Integer(), sa.ForeignKey('cas_organizations.id')),
        sa.Column('item_id', sa.Integer(), sa.ForeignKey('core_items.id')),
        sa.Column('catalog_id', sa.Integer(), sa.ForeignKey('core_catalogs.id')),
        sa.Column('start_date', sa.DateTime(timezone=True)),
        sa.Column('end_date', sa.DateTime(timezone=True)),
        sa.Column('is_borrowed', sa.Boolean())
    )
    op.get_bind().execute(sa.text('ALTER TABLE watch_list ALTER is_borrowed SET DEFAULT true'))
    op.get_bind().execute(
        "CREATE UNIQUE INDEX watch_list_user_org_item_unique_idx "
        "ON watch_list (user_id, stock_org_id, item_id, end_date, is_borrowed) ")

    op.add_column('core_organization_items', sa.Column('is_active', sa.BOOLEAN))
    op.get_bind().execute(sa.text('ALTER TABLE core_organization_items ALTER is_active SET DEFAULT true'))
    op.get_bind().execute(sa.text('UPDATE core_organization_items SET is_active = true'))

    op.add_column('core_catalog_items', sa.Column('is_active', sa.BOOLEAN))
    op.get_bind().execute(sa.text('ALTER TABLE core_catalog_items ALTER is_active SET DEFAULT true'))
    op.get_bind().execute(sa.text('UPDATE core_catalog_items SET is_active = TRUE'))

    op.add_column('core_organization_brands', sa.Column('valid_from', sa.DateTime(timezone=True)))
    op.add_column('core_organization_brands', sa.Column('valid_to', sa.DateTime(timezone=True)))

    op.get_bind().execute(sa.text("""CREATE OR REPLACE FUNCTION utility.catalog_ids_for_item(_item_id integer, _org_id integer)
 RETURNS integer[]
 LANGUAGE plpgsql
AS $function$
  BEGIN
  RETURN
    ARRAY(
        SELECT item_catalog.catalog_id
        FROM core_catalog_items item_catalog
        WHERE item_catalog.shared_library_item_item_id = _item_id
              AND item_catalog.shared_library_item_org_id = _org_id
              AND item_catalog.is_active = true
    );
END;
$function$"""))


def downgrade():
    op.drop_column('core_organization_items', 'is_active')
    op.drop_column('core_catalog_items', 'is_active')
    op.drop_table('watch_list')
