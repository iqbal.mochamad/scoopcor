Eperpus Reports
===============

The reports here exist in different levels of granularity (organization, user, and item level).

Furthermore, there are 3 different types of reporting data return

micro
    Only returns a single value, meant for displaying directly though a widget.
report
    Standard report that returns detailed information for a range of values.
chart
    Meant for displaying in a graphical chart.


Organization-Level Reports
--------------------------

User Reading Statistics
^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/user-read-statistics



User Count Reading Activity Above Duration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/users-count-over-reading-duration

    Counts the Users that read for more than (x) minutes, since a given time period.

    **Example request**:

    .. sourcecode:: http

        GET /reports/eperpus/123/user-read-count HTTP/1.1
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "count": 999
        }

    :query duration: Minimum time a User must have read to be counted (ISO8601 duration format; default PT15M).
    :query since: Timespan (subtracted from now) to include in the report (ISO8601 duration format; default P30D).

Total Borrowed Items (micro)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/current-borrowed-items

    Counts the Items that are currently borrowed from this Organizations's Catalogs.

    **Example requst**:

    .. sourcecode:: http

        GET /reports/eperpus/123/current-borrowed-items
        Accept: application/json

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: application/json

        {
            "count": 999
        }

    This endpoint accepts no query parameters.

New User Count (micro)
^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/new-user-count

    Counts the new users that have created an account within (x) amount of time from now.

    .. note::

        Users that first signed up for scoop, and then signed up for a shared library
        may not be accurately counted


Most-active User Count (micro)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/current-active-users

.. warning::

    Unimplemented;  This doesn't make sense without additional parameters.

Reading Statistics (chart)
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/user-read-count

Most Active Users (report)
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/most-active-users

Most Popular Items (report)
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/most-downloaded-items


Individual User Reports
-----------------------

Daily Reading Time Average (micro)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/user-average-daily-reading-time

Total Borrowing Count (micro)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/user-total-borrowing-count

Reading Rate (micro)
^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/user-reading-rate

.. warning::

    Unimplemented;  This is extremely expensive to calculate.


Individual Item Reports
-----------------------

Weekly Borrowing Count
^^^^^^^^^^^^^^^^^^^^^^

.. http:get:: /reports/eperpus/(int:organization_id)/item-borrowing-count
