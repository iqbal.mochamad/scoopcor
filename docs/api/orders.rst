### Orders

Order resource, used to order the offers. For consumer clients, [Checkout Order](#checkout-order) and [Confirm Order](#confirm-order) must be called to confirm the order before continue to payment process.

General consumer client order use-case flow:
* 1. [Checkout Order](#confirm-order)
* 2. [Confirm Order](#checkout-order)
* ...continue to payment process

#### Order Properties

* `order_id` - Number; Instance identifier.

* `temp_order_id` - Number; Temporarily instance identifier. Please note this temporarily identifier returned on checkout process will be required on order confirmation.

* `user_id` - Number; User identifier that will paid and eligible for the completed order later.

* `total_amount` - String; Order base total amount before any discount applied, this property is only available if payment gateway already specified in request.

* `final_amount` - String; Order final amount after all discounts applied, this property is only available if payment gateway already specified in request.

* `total_amount_point` - String; Order total amount in SCOOP Point before any discount applied, this property is only available if no payment gateway already specified in request.

* `final_amount_point` - String; Order final amount in SCOOP Point after all discounts applied, this property is only available if no payment gateway already specified in request.

* `total_amount_usd` - String; Order total amount in USD before any discount applied, this property is only available if no payment gateway already specified in request.

* `final_amount_usd` - String; Order final amount in USD after all discounts applied, this property is only available if no payment gateway already specified in request.

* `total_amount_idr` - String; Order total amount in IDR before any discount applied, this property is only available if no payment gateway already specified in request.

* `final_amount_idr` - String; Order final amount in IDR after all discounts applied, this property is only available if no payment gateway already specified in request.

* `tier_code` - String; Tier code to use in payment process, this property is only for tiered payment with specified platform.

* `is_active` - Boolean; Instance's status, used to mark if the instance is active or soft-deleted. Default to true.

* `payment_gateway_id` - Number; Payment gateway identifier. Payment gateway when checkout and when confirmed must be the same, later, payment method must match this specified payment gateway.

* `payment_gateway_name` - String; Payment gateway name.

* `platform_id` - Number; Platform identifier. Used to identify from which platform this order come from, also used to calculate the platform specific discount.

* `client_id` - Number; Client identifier. Used to identify from which client this order come from.

* `order_status` - Number; Order status. Showing order statuses in order, payment, delivery, refund, and cancellation states.

    * 10000: TEMP - Temporarily order, this status marks the order on checking out state and before confirmation.

    * 10001: NEW - New confirmed order, this status marks the order is confirmed and ready to go into payment flow.

    * 20001: WAITING FOR PAYMENT - Order ready for payment, this status marked by payment process on authorization request and before call to payment (charge) process, can be skipped for payment that has no authorization flow or payment with charging process called on front-end.

    * 20002: PAYMENT IN PROCESS - Payment on going, this status marked right before call for payment charging, can be skipped for payment with charging process called on front-end.

    * 20003: PAYMENT BILLED - Payment already charged, this status marked right away after the payment gateway returns success charging process.

    * 30001: WAITING FOR DELIVERY - Marked right as the delivery function starts.

    * 30002: DELIVERY IN PROCRESS - Status to marks delivery process is on going.

    * 30003: DELIVERED - Status showing that eligible items delivered to user, this status merked right away after all save process to user completed

    * 40001: WAITING FOR REFUND - Order waiting for refund process. This only can occure to order that already billed. Only for internal use.

    * 40002: REFUND IN PROCESS - Order in process of refund.

    * 40003: REFUNDED - Refund completed.

    * 50000: CANCELLED - Order cancelled by consumer user side before payment flow. This only included if the payment gateway provide cancellation service.

    * 50001: PAYMENT ERROR - Payment error status, for detected fraud payment, payment gateway server issue, unmatched charged amount, retrieve or save data error (DB's table level fail), etc. Details appended to `note` property.

    * 50002: DELIVERY ERROR - Delivery error status, for missing items, item not for sale anymore, retrieve or save data error (DB's table level fail), etc. Details appended to `note` property.

    * 50003: REFUND ERROR - Refund error status, retrieve or save data error (DB's table level fail), etc. Details appended to `note` property.

    * 90000: COMPLETE - All order process completed.

* `point_reward` - Number; SCOOP Points that rewarded on this order completion. This property only available after order confirmed.

* `order_number` - Number; Order alternative identifier for consumer-user communication purpose.

* `partner_id` - Number; Partner identifier to identify from which preloaded-partnership client this order come from.

* `currency_code` - String; Currency code of total_amount and final_amount.

* `user_email` - String; User email of this order.

* `user_name` - String; User full name of this order.

* `user_street_address` - String; User street address of this order.

* `user_city` - String; User city of this order.

* `user_zipcode` - String; User zipcode of this order.

* `user_state` - String; User state of this order.

* `user_country` - String; User country of this order.

* `user_latitude` - String; User current latitude of this order.

* `user_longitude` - String; User current longitude of this order.

* `note` - String; Note for this order.

* `ip_address` - String; Client IP address of this order.

* `os_version` - String; Client OS version of this order.

* `client_version` - String; Client version of this order.

* `device_model` - String; Client device model of this order.

* `order_lines` - Array; Lines of the order.

    * `id` - Number; Order line identifier.

    * `name` - String; Order line name, equals to offer's name See [Offer Properties](#offer-properties).

    * `offer_id` - Number; Offer identifier.

    * `quantity` - Number; Quantity of the ordered offer, currently this parameter will only contains 1 as value.

    * `raw_price` - String; Order line raw price before any discount applied, this property is only available if payment gateway already specified in request.

    * `final_price` - String; Order line final price after all discounts applied, this property is only available if payment gateway already specified in request.

    * `raw_price_point` - String; Order line raw price in SCOOP Point before any discount applied, this property is only available if no payment gateway already specified in request.

    * `final_price_point` - String; Order line final price in SCOOP Point after all discounts applied, this property is only available if no payment gateway already specified in request.

    * `raw_price_usd` - String; Order line raw price in USD before any discount applied, this property is only available if no payment gateway already specified in request.

    * `final_price_usd` - String; Order line final price in USD after all discounts applied, this property is only available if no payment gateway already specified in request.

    * `raw_price_idr` - String; Order line raw price in IDR before any discount applied, this property is only available if no payment gateway already specified in request.

    * `final_price_idr` - String; Order line final price in IDR after all discounts applied, this property is only available if no payment gateway already specified in request.

    * `currency_code` - String; Currency code of raw_price and final_price.

    * `order_line_status` - Number; Order line status. Showing order line statuses in order, delivery, and refund states. This status only used as supplement to Order's statuses.

        * 10000: TEMP - Temporarily order line, this status marks the order line on checking out state and before confirmation.

        * 10001: NEW - New confirmed order line, this status marks the order line is confirmed and ready to go into payment flow.

        * 30003: DELIVERED - Status showing that eligible items delivered to user, this status merked right away after all save process to user completed

        * 40003: REFUNDED - Refund completed.

        * 50000: CANCELLED - Order cancelled by consumer user side before payment flow. This only included if the payment gateway provide cancellation service.

        * 50002: DELIVERY ERROR - Delivery error status, for missing items, item not for sale anymore, retrieve or save data error (DB's table level fail), etc.

        * 50003: REFUND ERROR - Refund error status, retrieve or save data error (DB's table level fail), etc.

        * 90000: COMPLETE - All order line process completed.

#### Retrieve Individual Order

Retrieve individual order with identifier or order number. Only confirmed order can be retrieved from this resource, to retrieve temporarily order, see [Retrieve Individual Temporarily Order](#retrieve-individual-temporarily-order)

Clients are encouraged to use order_id for this retrieval purpose, order_number is prepared for end-user and customer support communication purpose.

**URL:** /orders/{order_id}

**Alternative URL:** /orders/order_number/{order_number}

**Method:** GET

**Response HTTP Code:**

* `200` - OK

**Examples:**

Request:

```
GET /orders/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    "payment_gateways": {
        "id": 14,
        "name": "mandiri e-cash"
    },
    "user_id": 290411,
    "total_amount": "9000.00",
    "tier_code": null,
    "order_id": 2,
    "partner_id": null,
    "is_active": true,
    "final_amount": "9000.00",
    "platform_id": null,
    "payment_gateway_id": 14,
    "temp_order_id": 2,
    "platform": null,
    "client_id": 7,
    "point_reward": 9,
    "order_status": 1,
    "order_number": 48391064664108104,
    "currency_code": "IDR",
    "user_email": "brijap@apps-foundry.com",
    "user_name": "Brian Japutra",
    "user_street_address": "Kolong Langit St. #322-12",
    "user_city": "Jakarta Timur Laut",
    "user_zipcode": "14450",
    "user_state": "Indonesia",
    "user_country": "Indonesia",
    "user_latitude": "1.214112",
    "user_longitude": "94.102362",
    "note": null,
    "ip_address": "192.168.0.1",
    "os_version": "8.1.2",
    "client_version": "4.1.0",
    "device_model": "iPad6,1",
    "order_lines": [{
        "user_id": 290411,
        "name": "Single Edition",
        "discounts": false,
        "order_id": 2,
        "offer_id": 47892,
        "is_active": true,
        "campaign_id": null,
        "is_free": false,
        "order_line_status": 1,
        "price": "9000.00",
        "payment_gateway_id": 14,
        "final_price": "9000.00",
        "id": 2,
        "currency_code": "IDR",
        "quantity": 1
    }]
}
```

#### Retrieve Individual Temporarily Order

Retrieve individual temporarily order with temporarily identifier or order number. Older records will be removed periodically.

Clients are encouraged to use temp_order_id for this retrieval purpose, order_number is prepared for end-user and customer support communication purpose.

**URL:** /temp_orders/{temp_order_id}

**Alternative URL:** /temp_orders/order_number/{order_number}

**Method:** GET

**Response HTTP Code:**

* `200` - OK

**Examples:**

Request:

```
GET /temp_orders/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json

{
    "user_id": 112,
    "payment_gateway_id": 1,
    "client_id": 1,
    "currency": "USD",
    "final_price": "13.98",
    "order_number": 296774314105186,
    "order_id": 20443
}

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "temp_order_id": 55,
    "user_id": 112,
    "payment_gateway_id": 1,
    "total_amount": "13.98",
    "currency_code": "USD",
    "platform_id": 1,
    "tier_code": ".c.usd.13.99",
    "order_number": 296774314105186,
    "order_lines": [
        {
            "name": "GOLFMAGZ - 6 Editions / 6 Months",
            "items": [
                {
                    "is_featured": false,
                    "name": "GOLFMAGZ / NOV 2012",
                    "countries": [
                        {
                            "id": 1,
                            "name": "Indonesia"
                        }
                    ],
                    "release_date": "2012-11-01T00:00:00",
                    "item_status": 9,
                    "languages": [
                        {
                            "id": 2,
                            "name": "Indonesian"
                        }
                    ],
                    "vendor": {
                        "id": 13,
                        "name": "IMP"
                    },
                    "authors": [],
                    "edition_code": "ID_GLM2012MTH11"
                }
            ],
            "offer_id": 1,
            "is_free": false,
            "currency_code": "USD",
            "final_price": "4.99",
            "raw_price": "4.99"
        },
        {
            "name": "GOLFMAGZ - 12 Editions / 12 Months",
            "items": [
                {
                    "is_featured": false,
                    "name": "GOLFMAGZ / NOV 2012",
                    "countries": [
                        {
                            "id": 1,
                            "name": "Indonesia"
                        }
                    ],
                    "release_date": "2012-11-01T00:00:00",
                    "item_status": 9,
                    "languages": [
                        {
                            "id": 2,
                            "name": "Indonesian"
                        }
                    ],
                    "vendor": {
                        "id": 13,
                        "name": "IMP"
                    },
                    "authors": [],
                    "edition_code": "ID_GLM2012MTH11"
                }
            ],
            "offer_id": 2,
            "is_free": false,
            "currency_code": "USD",
            "final_price": "8.99",
            "raw_price": "8.99"
        }
    ],
    "payment_gateway_name": "Apple In-App Purchase",
    "final_amount": "13.98",
    "user_email": "brijap@apps-foundry.com",
    "user_name": "Brian Japutra",
    "user_street_address": "Kolong Langit St. #322-12",
    "user_city": "Jakarta Timur Laut",
    "user_zipcode": "14450",
    "user_state": "Indonesia",
    "user_country": "Indonesia",
    "user_latitude": "1.214112",
    "user_longitude": "94.102362",
    "note": null,
    "ip_address": "192.168.0.1",
    "os_version": "8.1.2",
    "client_version": "4.1.0",
    "device_model": "iPad6,1"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Checkout Order

Checkout order called to check the order details before confirmed. At backend, this order is recorded in temporarily table, separated from main order records.

**URL:** /orders/checkout

**Method:** POST

**Request Data Params:**

Required: `user_id`, `client_id`, `platform_id`, `order_lines`

Optional: `payment_gateway_id`, `partner_id`, `user_email`, `user_name`, `user_street_address`, `user_city`, `user_zipcode`, `user_state`, `user_country`, `user_latitude`, `user_longitude`,`note`,`ip_address`,`os_version`,`client_version`,`device_model`, `recipient_email`

Required in `order_line`: `offer_id`, `name`, `quantity`

Optional in `order_line`: `discount_code`

**Response HTTP Code:**

* `200` - OK

**Examples:**

Request:

```
POST /orders/checkout HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "user_id": 112,
    "client_id": 1,
    "platform_id": 1,
    "user_email": "brijap@apps-foundry.com",
    "user_name": "Brian Japutra",
    "user_street_address": "Kolong Langit St. #322-12",
    "user_city": "Jakarta Timur Laut",
    "user_zipcode": "14450",
    "user_state": "Indonesia",
    "user_country": "Indonesia",
    "note": null,
    "os_version": "8.1.2",
    "client_version": "4.1.0",
    "device_model": "iPad6,1",
    "recipient_email" : "bj@apps-foundry.com",
    "order_lines": [
        {
            "offer_id": 1,
            "name": "GOLFMAGZ / NOV 2012 - Single Edition",
        },
        {
            "offer_id": 2,
            "name": "GOLFMAGZ / DEC 2012 - Single Edition",
            "discount_code": "MH-10101"
        }
    ]
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "partner_id": null,
    "total_amount_idr": "126000.00",
    "user_id": 112,
    "client_id": 1,
    "order_number": 66655510271053102,
    "is_active": true,
    "final_amount_idr": "126000.00",
    "platform_id": 1,
    "tier_code": ".c.usd.13.99",
    "total_amount_point": 1380,
    "final_amount_usd": "13.98",
    "order_status": 10000,
    "temp_order_id": 82,
    "total_amount_usd": "13.98",
    "final_amount_point": 1380,
    "user_email": "brijap@apps-foundry.com",
    "user_name": "Brian Japutra",
    "user_street_address": "Kolong Langit St. #322-12",
    "user_city": "Jakarta Timur Laut",
    "user_zipcode": "14450",
    "user_state": "Indonesia",
    "user_country": "Indonesia",
    "user_latitude": null,
    "user_longitude": null,
    "note": null,
    "ip_address": null,
    "os_version": "8.1.2",
    "client_version": "4.1.0",
    "device_model": "iPad6,1"
    "order_lines": [{
        "total_amount_idr": "45000.00",
        "name": "GOLFMAGZ - 6 Editions / 6 Months",
        "items": [{
            "is_featured": false,
            "vendor": {
                "id": 13,
                "name": "IMP"
            },
            "name": "GOLFMAGZ / NOV 2012",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2012-11-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "edition_code": "ID_GLM2012MTH11"
        }],
        "offer_id": 1,
        "final_amount_idr": "45000.00",
        "is_free": false,
        "total_amount_point": 490,
        "final_amount_usd": "4.99",
        "total_amount_usd": "4.99",
        "final_amount_point": 490
    }, {
        "total_amount_idr": "81000.00",
        "name": "GOLFMAGZ - 12 Editions / 12 Months",
        "items": [{
            "is_featured": false,
            "vendor": {
                "id": 13,
                "name": "IMP"
            },
            "name": "GOLFMAGZ / NOV 2012",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2012-11-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "edition_code": "ID_GLM2012MTH11"
        }],
        "offer_id": 2,
        "final_amount_idr": "81000.00",
        "is_free": false,
        "total_amount_point": 890,
        "final_amount_usd": "8.99",
        "total_amount_usd": "8.99",
        "final_amount_point": 890
    }]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Confirm Order

Confirms any checked out order from [Checkout Order](#checkout-order) before proceeding to payment process. Note: all passed parameter in request must conform to the last value of checkout response's parameters.

**URL:** /orders/confirm

**Method:** POST

**Request Data Params:**

Required: `user_id`, `payment_gateway_id`, `client_id`, `temp_order_id`, `final_amount`, `order_number`, `currency_code`

**Response HTTP Code:**

* `200` - OK

**Examples:**

Request:

```
POST /orders/confirm HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "user_id": 682,
    "payment_gateway_id": 9,
    "client_id": 1,
    "currency_code": "PTS",
    "final_amount": "95.0",
    "order_number": 97749913159435,
    "temp_order_id": 59
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "payment_gateway_name": "SCOOP Point",
    "user_id": 682,
    "total_amount": "95.00",
    "order_id": 43,
    "partner_id": null,
    "is_active": true,
    "final_amount": "95.00",
    "payment_gateway_id": 9,
    "temp_order_id": 59,
    "client_id": 1,
    "point_reward": 0,
    "order_status": 90000,
    "order_number": 97749913159435,
    "currency_code": "PTS",
    "user_email": "brijap@apps-foundry.com",
    "user_name": "Brian Japutra",
    "user_street_address": "Kolong Langit St. #322-12",
    "user_city": "Jakarta Timur Laut",
    "user_zipcode": "14450",
    "user_state": "Indonesia",
    "user_country": "Indonesia",
    "user_latitude": null,
    "user_longitude": null,
    "note": null,
    "ip_address": null,
    "os_version": "8.1.2",
    "client_version": "4.1.0",
    "device_model": "iPad6,1"
    "order_lines": [{
        "name": "PANORAMA / NOV\u2013DEC 2011 - Single Edition",
        "items": [{
            "is_featured": false,
            "vendor": {
                "id": 10,
                "name": "Panorama Multi Media"
            },
            "name": "PANORAMA / NOV\u2013DEC 2011",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2011-11-01T00:00:00",
            "item_status": 9,
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "authors": [],
            "id": 1260,
            "edition_code": "ID_PAN2011MTH1112ED26"
        }],
        "offer_id": 2122,
        "is_free": false,
        "final_price": "95.00",
        "currency_code": "PTS",
        "raw_price": "95.00"
    }]
}
```

#### Order Transaction Histories

Retrieves transaction histories based on certain user

**URL:** /orders/transactions/{user_id}

**Method:** GET

**Response HTTP Code:**

* `200` - OK

**Examples:**

Request:

```
GET /orders/transactions/401857 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    "transactions": [
        {
        "user_id": 401857,
        "total_amount": "590.00",
        "created": "2015-02-09T04:26:04.256443",
        "order_id": 97435,
        "final_amount": "590.00",
        "platform_id": 4,
        "payment_gateway_id": 9,
        "point_reward": 0,
        "order_status": 90000,
        "order_number": 104269439731296,
        "currency_code": "PTS",
        "order_lines": [
            {
            "offer_quantity": 5,
            "item_ids": [
                59001,
                59002,
                59000,
                59003,
                58999
            ],
            "price": "590.00",
            "offer_quantity_unit": 1,
            "offer_type_id": 3,
            "id": 108565,
            "on_going": 0,
            "final_price": "590.00",
            "offer_name": "POPULAR Babes From Net - Bundle 5in1 - Goddess",
            "offer_id": 38255
            }
        ]
      }
    ],
    "metadata": {
        "resultset": {
            "count": 1,
            "limit": 20,
            "offset": 0
        }
    },
}
```
