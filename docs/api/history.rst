History
=======


Version 1
---------

Originally,


Version 2
---------

This is the current version of our API.  It is a RESTful (and in some cases, actually REST) API.

The current verison of the API (available from https://scoopadm.apps-foundry.com/scoopcor/api/v1),
we will be referring to as SCOOPCOR V2.


The original version of this service was called "Scoop Login", and was a companion
service to scoopcore.  For historical purposes, we will be referring this to **SCOOP API V1**.



The next-generation of this service (using HATEOAS), will be referred to as SCOOPCAS V3 and will
be available from generally the same URLs.  Keeping with the HATEOAS and REST concepts,
these services will be differentiated based on their Accept header (known as versioning by
content-type negoation).

We also may make the /v1/ portion of the URL optional, as it will be confusing.
