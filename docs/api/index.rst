API Endpoints
=============

Some things right off the bat -- YOU MUST READ :doc:`/api/history`.  The naming
of certain services and URLs may be extremely confusing if you do not
otherwise read this.


.. toctree::
  :maxdepth: 1

  auth/index
  users/index
  items/index
  user_related/index
  shared-library/index
  content-access/index
  reports/index
  solr/index
  layout/index
  payments/index
