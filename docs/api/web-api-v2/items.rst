Items
=====

Searching
---------

Our search API endpoint is a thin proxy around Solr.  The entire query string
provided to the request, will be sent to the Solr /select handler *(With one
notable exception:  Responses are ALWAYS automatically set to JSON.  Although
Solr supports different response formats, we do not.)*

https://cwiki.apache.org/confluence/display/solr/Apache+Solr+Reference+Guide

http://wiki.apache.org/solr/ -- generally horribly outdated

**URL:** /v2/items/searches

HTTP GET
^^^^^^^^

Query String Parameters
"""""""""""""""""""""""
q *(query)*
  **REQUIRED:** Primary query string
fq *(filter query)*
  May be provided multiple times.  A field name and an EXACT value (caps
  count here!!).
sort
  The name of a field, and it's sorting order.  GENERALLY YOU DO NOT WANT
  TO TOUCH THIS, because if you're just getting search results, you want
  Solr to handle ordering by query scoring.
start, rows
  Offset, and maximum records to return, respectively.  It's pagination
  support.
fl *(field list)*
  A list of all the fields to return.
df *(default query field)*
  If the query field is not explicitly specified, the name of the field
  to search.
facet
  Set to 'true' to enable faceted searching.
facet.field
  **MULTIPLE ALLOWED:**
  The name of a field to provide faceting results for.
  Note: the facet parameter MUST be set to true, or facet.field
  will be ignored!


Schema Fields
"""""""""""""




Response HTTP Code
""""""""""""""""""

* `200` - Items retrieved and returned in body content
* `204` - No matched item


Examples
""""""""

GET /v2/items/searches?
  - q=*&
  - indent=true&
  - facet=true&
  - facet.field=author_name&
  - facet.field=item_type&
  - rows=2&offset=100&
  - df=text&
  - rows=2&
  - offset=100

.. code-block:: json

  {
    "responseHeader":{
      "status":0,
      "QTime":2,
      "params":{
        "facet":"true",
        "df":"text",
        "indent":"true",
        "q":"*",
        "offset":"100",
        "facet.field":["author_name",
          "item_type"],
        "wt":"json",
        "rows":"2"}},
    "response":{"numFound":61066,"start":0,"docs":[
        {
          "item_created_date":"2014-12-22T11:28:06.481Z",
          "item_is_extra":false,
          "item_modified_date":"2014-12-22T11:28:06.481Z",
          "item_is_active":true,
          "item_reading_direction":"left-to-right",
          "item_status":"ready for consume",
          "brand_name":"Banjarmasin Post",
          "id":14834,
          "item_is_featured":false,
          "item_name":"Banjarmasin Post / 11 APR 2013",
          "item_release_date":"2013-04-11T00:00:00Z",
          "item_slug":"banjarmasin-post-11-apr-2013",
          "item_thumb_highres":"images/1/628/small_covers/ID_BJMP2013MTH04DT11_S_.jpg",
          "item_thumb_normal":"images/1/628/general_small_covers/ID_BJMP2013MTH04DT11_S_.jpg",
          "vendor_name":"Banjarmasin Post",
          "item_type":"newspaper",
          "item_language":["Indonesian"],
          "item_country":["Indonesia"],
          "_version_":1489632131826057216},
        {
          "item_created_date":"2014-12-22T11:28:06.481Z",
          "item_is_extra":false,
          "item_modified_date":"2014-12-22T11:28:06.481Z",
          "item_is_active":true,
          "item_reading_direction":"left-to-right",
          "item_status":"ready for consume",
          "brand_name":"WARTA KOTA",
          "id":14851,
          "item_is_featured":false,
          "item_name":"Tribun Jakarta / [Pagi] 12 APR 2013",
          "item_release_date":"2013-04-12T00:00:00Z",
          "item_slug":"tribun-jakarta-pagi-12-apr-2013",
          "item_thumb_highres":"images/1/368/small_covers/ID_TRIB2013MTH04DT12M_S.jpg",
          "item_thumb_normal":"images/1/368/general_small_covers/ID_TRIB2013MTH04DT12M_S.jpg",
          "vendor_name":"WARTA KOTA",
          "item_type":"newspaper",
          "item_language":["Indonesian"],
          "item_country":["Indonesia"],
          "_version_":1489632131832348672}]
    },
    "facet_counts":{
      "facet_queries":{},
      "facet_fields":{
        "author_name":[
          "Buddy Setianto",637,
          "Jubilee Enterprise",341,
          "Lilly T. Erwin",122,
          "Agatha Christie",93,
          "Ide Masak",91,
          "Dapur Kirana",81,
          "Imelda Akmal Architecture Writer Studio",76,
          "ASMARAMAN S. KHO PING HOO",70,
          "Sisca Soewitomo",60,
          "Meg Cabot",57,
          "Wahana Komputer",52,
          "Indriani",49,
          "Iva Hardiana",48],
        "item_type":[
          "newspaper",26342,
          "book",18612,
          "magazine",16112]},
      "facet_dates":{},
      "facet_ranges":{},
      "facet_intervals":{}}}


Retrieve items contains "rowling" string (most likely will hit by author name) from offest 10 with limit 25

```
GET /items/searches?q=rowling&offset=10&limit=25 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

#### Search Suggestions for Items

Search suggestions for Items.

This service query for brand's names and returns max 10 suggested names

**URL:** /items/searches/suggestions/{query}

**Method:** GET

**Response HTTP Code:**

* `200` - Suggestions retrieved and returned in body content

* `204` - No matched suggestion

**Response Params:**

`Suggestions` - Array; Array of suggestions from the system

Note: `metadata` property will be static for now, this reserved for future use

**Examples:**

Request:

```
GET /items/searches/suggestions/auto HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

.. code-block:: json

  {
      "suggestions": [
          "Autocar",
          "Automotif",
          "Auto-repair",
          "Auto Robotics"
      ],
      "metadata": {
          "resultset": {
              "count": 10,
              "offset": 0,
              "limit": 10
          }
      }
  }

