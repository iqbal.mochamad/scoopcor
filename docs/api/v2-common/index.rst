Version 2 Common Request/Response Information
=============================================

.. toctree::
    :maxdepth: 1

    errors
    requests
