### Partners

Partner resource contains all associated partner. Currently this contains associated partner to preload app and profit sharing.

#### Partner Properties

* `id` - Number; Instance identifier.

* `name` - String; Partner name.

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority.

* `partner_status` - Number; Partner status in the system.

    * 1: NEW - New created vendor

    * 2: WAITING FOR REVIEW - Waiting for business side review for any kind of partnership

    * 3: IN REVIEW - In review

    * 4: REJECTED - Rejected due to some reasons

    * 5: APPROVED - Aproved by business side

    * 6: CLEAR - Clear for any action

    * 7: BANNED - Banner due to some reasons

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true.

* `slug` - String, Unique; Partner slug for SEO.

* `meta` - String; Partner meta for SEO.

* `description` - String; Partner description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URL.

* `icon_image_normal` - String; Partner icon image URI.

* `icon_image_highres` - String; Partner icon image in highres URI.

* `organization_id` - Number; Unique, organization identifier that this instance bind to. A partner can only bind to an organization.

#### Create Partner

Create new partner.

**URL:** /partners

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`, `cut_rule_id`, `organization_id`, `partner_status`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `201` - New partner created

**Response Params:** See [Partner Properties](#partner-properties)

**Examples:**

Request:

```
POST /partners HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "SONY",
    "slug": "sony",
    "sort_priority": 1,
    "partner_status": 6,
    "is_active": true,
    "description": "Sony Indonesia",
    "meta": "Sony Indonesia",
    "icon_image_normal": "partners/sony/icon.png",
    "icon_image_highres": "partners/sony/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 765
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/partners/2

{
    "id": 2,
    "name": "SONY",
    "slug": "sony",
    "sort_priority": 1,
    "partner_status": 6,
    "is_active": true,
    "description": "Sony Indonesia",
    "meta": "Sony Indonesia",
    "icon_image_normal": "partners/sony/icon.png",
    "icon_image_highres": "partners/sony/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 765
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Partners

Retrieve partners.

**URL:** /partners

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Partner Properties](#partner-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

* `organization_id` - Filter by organization identifier(s). Response will returns if any matched

**Method:** GET

**Response HTTP Code:**

* `200` - Partners retrieved and returned in body content

* `204` - No matched partner

**Response Params:** See [Partner Properties](#partner-properties)

**Examples:**

Request:

```
GET /partners?fields=id,name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "partners": [
        {
            "id": 1,
            "name": "LENOVO"
        },
        {
            "id": 2,
            "name": "SONY"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Partner

Retrieve individual partner using identifier.

**URL:** /partners/{partner_id}

**Alternative URL:** /partners/organization/{organization_id}

Optional:

* `fields` - Retrieve specific properties. See [Partner Properties](#partner-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Partner found and returned

**Response Params:** See [Partner Properties](#partner-properties)

**Examples:**

Request:

```
GET /partners/2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "name": "SONY",
    "slug": "sony",
    "sort_priority": 1,
    "partner_status": 6,
    "is_active": true,
    "description": "Sony Indonesia",
    "meta": "Sony Indonesia",
    "icon_image_normal": "partners/sony/icon.png",
    "icon_image_highres": "partners/sony/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 765
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Partner

Update partner.

**URL:** /partners/{partner_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`, `cut_rule_id`, `organization_id`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `200` - Partner updated

**Response Params:** See [Partner Properties](#partner-properties)

**Examples:**

Request:

```
PUT /partners/2 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "SONY",
    "slug": "sony",
    "sort_priority": 5,
    "partner_status": 6,
    "is_active": true,
    "description": "Sony Mobile",
    "meta": "Sony Mobile",
    "icon_image_normal": "partners/sony/icon.png",
    "icon_image_highres": "partners/sony/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 765
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "name": "SONY",
    "slug": "sony",
    "sort_priority": 5,
    "partner_status": 6,
    "is_active": true,
    "description": "Sony Mobile",
    "meta": "Sony Mobile",
    "icon_image_normal": "partners/sony/icon.png",
    "icon_image_highres": "partners/sony/icon_highres.png",
    "cut_rule_id": 2,
    "organization_id": 765
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Partner

Delete partner. This service will set `is_active` value to false.

**URL:** /partners/{partner_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Partner deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /partners/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

