Layout Offers
=============

Custom pick offers for layout web-store.


Layout offers properties
------------------------

**offer**
id
    Number; Offer instance identifier.
title
    String; Offer long name.
href
    String; url link to offer"s item instance info.
subtitle
    String; Offer"s item authors for books or item edition code for magazine and newspaper
image_url
    String; Offer"s item image high-res url
price;
    Object; Contain: tier id, tier code, base price and net price for IDR, USD, and POINT.
    Currently None for banners


Create or update Layout Offer
-----------------------------

.. http:put:: /v1/layouts/(int:layout_id)

    Add or update a custom list of offers for existing layout.
    "id" is an offer id.

    **Example Request**

    .. sourcecode:: http

        PUT v1/layouts/101 HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        Content-Length: 780
        Authorization: Bearer ajksfhiau12371894^&*@&^$%
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop Portal/3.0

        {
            "items": [
                {
                    "id": 1000,
                    "title": "Offer Book ABC"
                },
                {
                    "id": 1001,
                    "title": "Offer Magazine B"
                }
            ]
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
          "items": [
              {
                "id": 1000,
                "href": "http://scoopcor/v1/offers/1000",
                "image_url": "http://img.scoop.com/v1/item/1000.png",
                "title": "Offer Book  ABC",
                "type": "Single",
                "subtitle": "author inem",
                "price": {
                    "idr": {"base": 50000, "net": 40000},
                    "usd": {"base": 50, "net": 40},
                    "point": {"base": 50, "net": 40}
                }
              },
              {
                "id": 1001,
                "href": "http://scoopcor/v1/offers/1001",
                "image_url": "http://img.scoop.com/v1/item/1001.png",
                "title": "Offer Magazine B",
                "type": "landing page",
                "subtitle": "ED MAR30 2016",
                "price": {
                    "platform_id": 2,
                    "tier_id": 1,
                    "tier_code": "c.usd.0.99"
                    "idr": {"base": 15000, "net": 10000},
                    "usd": {"base": 1, "net": 1},
                    "point": {"base": 1, "net": 1}
                }
              }
          ],
          "metadata": {
                "resultset": {
                    "count": 3,
                    "limit": 20,
                    "offset": 0
                }
          }
        }

List layout offer
-----------------

.. http:get:: /v1/layouts/(int:db_id)

    List offer for a layout

    **Example Request**

    .. sourcecode:: http

        GET v1/101/layouts/101  HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Authorization: Bearer ajksfhiau12371894^&*@&^$%
        User-Agent: Scoop Android/5.0

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
          "items": [
              {
                "id": 1000,
                "href": "http://scoopcor/v1/offers/1000",
                "image_url": "http://img.scoop.com/v1/item/1000.png",
                "title": "Offer Book  ABC",
                "type": "Single",
                "subtitle": "author inem",
                "price": {
                    "platform_id": 2,
                    "tier_id": 1,
                    "tier_code": "c.usd.1.99"
                    "idr": {"base": 30000, "net": 25000},
                    "usd": {"base": 3, "net": 2.5},
                    "point": {"base": 3, "net": 2}
                }
              },
              {
                "id": 1001,
                "href": "http://scoopcor/v1/offers/1001",
                "image_url": "http://img.scoop.com/v1/item/1001.png",
                "title": "Offer Magazine B",
                "type": "landing page",
                "subtitle": "ED MAR30 2016",
                "price": {
                    "platform_id": 2,
                    "tier_id": 1,
                    "tier_code": "c.usd.0.99"
                    "idr": {"base": 15000, "net": 10000},
                    "usd": {"base": 1.5, "net": 1},
                    "point": {"base": 1, "net": 1}
                }
              }
          ],
          "metadata": {
                "resultset": {
                    "count": 2,
                    "limit": 20,
                    "offset": 0
                }
          }
        }

