Dynamic Layouts
===============

Allows for StoreFront and Intro pages for Mobile Apps

Resource for web-store layout.

.. toctree::

    intro
    details
    offers
    banners
    clear-cache

Create or update list of layout for web-store
-----------------------------------------------

id
    Number; Instance identifier.

name
    String; Layout name.

sort_priority
    Number; Sort priority use to defining order. The larger the number,
    the higher the priority.

href
    String; url link to layout's detail info.

layout_type
    String; Layout type for web-store
    could be : custom offers, custom banners, latest items, popular items

payload
    String; url link to layout content info. (This value related to layout_type)

.. http:put:: /v1/layouts

    Add or update a list of layout for web-store.

    **Example Request**

    .. sourcecode:: http

        PUT v1/layouts HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        Authorization: Bearer ajksfhiau12371894^&*@&^$%
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop Portal/3.0

        {
            "layouts": [
                {
                    "id": 1,
                    "title": "abc",
                    "layout_type": "popular book"
                },
                {
                    "id": 2,
                    "title": "abc2",
                    "layout_type": "popular magazine",
                }
            ]
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
            "layouts":[
                {
                    "title": "abc1",
                    "id": 1,
                    "href": "http://localhost/v1/layouts/1",
                    "layout_type": "popular book"
                },
                {
                    "title": "abc2",
                    "id": 2,
                    "href": "http://localhost/v1/layouts/2",
                    "layout_type": "popular magazine"
                }
            ]
        }


List Layout
-----------

.. http:get:: /v1/layouts

    List all layout for web-store

    **Example Response**

    .. sourcecode:: http

        GET v1/layouts  HTTP/1.1
        Content-Type: application/json
        Authorization: Bearer ajksfhiau12371894^&*@&^$%

        {
            "layouts": [
                {
                    "title": "Ashlyn Farrell",
                    "id": 1000,
                    "href": "http://localhost/v1/layouts/1000",
                    "layout_type": "latest book",
                }
            ],
            "metadata": {
                "resultset": {
                    "count": 1,
                    "limit": 20,
                    "offset": 0
                }
            }
        }
