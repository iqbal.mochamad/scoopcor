Clear Layout Cache in Redis
===========================


Layout API and Layout Details API have cache in redis, in case we need to clear out these cache, use this API.


Clear cache/Purge
-----------------

.. http:purge:: /v1/layouts

    **Example Request**

    .. sourcecode:: http

        PURGE v1/101/layouts  HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Authorization: Bearer ajksfhiau12371894^&*@&^$%
        User-Agent: Scoop Android/5.0

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {"success_message": "Cached data for layout api and layout details api successfully purged"}

