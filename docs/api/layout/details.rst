Layout Details
==============


Based on layout type, could be:
- Latest Items (latest book, latest magazine, latest newspaper, or latest all)
- Popular Items (Latest book, latest magazine, latest newspaper, or latest all)


Layout details properties
-------------------------

**item**
id
    Number; Item instance identifier.
title
    String; Item name.
href
    String; url link to item instance info.
subtitle
    String; item authors for books or item edition code for magazine and newspaper
image_url
    String; item image high-res url
price;
    Object; Contain: tier id, tier code, base price and net price for IDR, USD, and POINT
    (all from the single offers of the item for the current client platform (IOS/Android/Web)).


List layout Items
-----------------

.. http:get:: /v1/layouts/(int:db_id)

    List latest/popular item for a layout

    **Example Request**

    .. sourcecode:: http

        GET v1/101/layouts/2  HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Authorization: Bearer ajksfhiau12371894^&*@&^$%
        User-Agent: Scoop Android/5.0

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
          "items": [
              {
                "id": 1000,
                "href": "http://scoopcor/v1/offers/1000",
                "image_url": "http://img.scoop.com/v1/item/1000.png",
                "title": "Book  ABC",
                "type": "Single",
                "subtitle": "author item",
                "price": {
                    "platform_id": 2,
                    "tier_id": 1,
                    "tier_code": "c.usd.1.99"
                    "idr": {"base": 30000, "net": 25000},
                    "usd": {"base": 3, "net": 2.5},
                    "point": {"base": 3, "net": 2}
                }
              },
              {
                "id": 1001,
                "href": "http://scoopcor/v1/offers/1001",
                "image_url": "http://img.scoop.com/v1/item/1001.png",
                "title": "Magazine B",
                "type": "landing page",
                "subtitle": "ED MAR30 2016",
                "price": {
                    "platform_id": 2,
                    "tier_id": 1,
                    "tier_code": "c.usd.0.99"
                    "idr": {"base": 15000, "net": 10000},
                    "usd": {"base": 1.5, "net": 1},
                    "point": {"base": 1, "net": 1}
                }
              }
          ],
          "metadata": {
                "resultset": {
                    "count": 2,
                    "limit": 20,
                    "offset": 0
                }
          }
        }

