Layout for App Intro
=====================

Resources for mobile apps introduction page layout.


Create or update Custom list of Brands for Intro Page
=====================================================

.. http:put:: /v1/layouts/intro

    Add or update a custom list of brands for introduction pages in apps.

    **Example Request**

    .. sourcecode:: http

        PUT v1/layouts/intro HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        Content-Length: 780
        Authorization: Bearer ajksfhiau12371894^&*@&^$%
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop Portal/3.0

        {
            "items": [
                {
                    "id": 1000,
                    "title": "Ashlyn Farrell",
                    "href": "http://img.scoop.com/v1/brands/1000.png"
                },
                {
                    "id": 1001,
                    "title": "Magazine ABC",
                    "href": "http://img.scoop.com/v1/brands/1001.png"
                }
            ]
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
            "items": [
                {
                    "id": 1000,
                    "title": "Ashlyn Farrell",
                    "href": "http://img.scoop.com/v1/brands/1000.png",
                },
                {
                    "id": 1001,
                    "title": "Magazine ABC",
                    "href": "http://img.scoop.com/v1/brands/1001.png",
                }
            ],
            "metadata": {
                "resultset": {
                    "count": 2,
                    "limit": 20,
                    "offset": 0
                }
            }
        }


List of brands for introduction page
====================================

.. http::get:: /v1/layouts/intro

    List of brands for introduction pages in mobile apps.

    **Example Request**

    .. sourcecode:: http

        GET v1/layouts/intro  HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Authorization: Bearer ajksfhiau12371894^&*@&^$%
        User-Agent: Scoop Android/5.0

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
            "items": [
                {
                    "id": 1000
                    "title": "Ashlyn Farrell",
                    "href": "http://image.localhost/brands/1000"

                },
                {
                    "id": 1001,
                    "title": "Magazine ABC",
                    "href": "http://image.localhost/brands/1001"
                }
            ],
            "metadata": {
                "resultset": {
                    "count": 2,
                    "limit": 20,
                    "offset": 0
                }
            }
        }


