Layout Banners
==============

Custom pick banners for a layout web-store.


Layout banners properties
-------------------------

**banners**
id
    Number; Banner instance identifier.
title
    String; Banner Name.
type
    String; Banner type.
href
    String; url link to banner's payload. ex: list of campaign items, landing page, or anything
subtitle
    String; Currently None
image_url
    String; Banner's item image high-res url
price;
    Object; Contain base price and net price for IDR, USD, and POINT. Currently None for banners


Create or update Layout Banners
-------------------------------

.. http:put:: /v1/layouts/(int:layout_id)

    Add or update a custom list of banners for an existing layout.

    **Example Request**

    .. sourcecode:: http

        PUT v1/layouts/101 HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        Content-Length: 780
        Authorization: Bearer ajksfhiau12371894^&*@&^$%
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop Portal/3.0

        {
            "items": [
                {
                    "id": 1000,
                    "title": "Banner ABC"
                },
                {
                    "id": 1001,
                    "title": "Banner Promo September"
                }
            ]
        }


    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
          "items": [
              {
                "id": 1000,
                "href": "http://getscoop.com/landingpage123",
                "image_url": "http://img.scoop.com/v1/banners/1000.png",
                "title": "Banner ABC",
                "type": "open url",
                "subtitle": null,
                "price": {
                    "idr": {"base": null, "net": null},
                    "usd": {"base": null, "net": null},
                    "point": {"base": null, "net": null}
                }
              },
              {
                "id": 1001,
                "href": "http://scoopcor/v1/campaigns/234/items",
                "image_url": "http://img.scoop.com/v1/banners/1000.png",
                "title": "Banner Promo September",
                "type": "landing page",
                "subtitle": null,
                "price": {
                    "idr": {"base": null, "net": null},
                    "usd": {"base": null, "net": null},
                    "point": {"base": null, "net": null}
                }
              }
          ],
          "metadata": {
                "resultset": {
                    "count": 3,
                    "limit": 20,
                    "offset": 0
                }
          }
        }

List of banners in a custom layout banners
------------------------------------------

.. http:get:: /v1/layouts/(int:layout_id)

    List of banners for a layout.

    **Example Request**

    .. sourcecode:: http

        GET v1/layouts/1  HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Authorization: Bearer ajksfhiau12371894^&*@&^$%
        User-Agent: Scoop Android/5.0

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
          "items": [
              {
                "id": 1000,
                "href": "http://getscoop.com/landingpage123",
                "image_url": "http://img.scoop.com/v1/banners/1000.png",
                "title": "Banner ABC",
                "type": "open url",
                "subtitle": null,
                "price": {
                    "idr": {"base": null, "net": null},
                    "usd": {"base": null, "net": null},
                    "point": {"base": null, "net": null}
                }
              },
              {
                "id": 1001,
                "href": "http://scoopcor/v1/campaigns/234/items",
                "image_url": "http://img.scoop.com/v1/banners/1000.png",
                "title": "Banner Promo September",
                "type": "landing page",
                "subtitle": null,
                "price": {
                    "idr": {"base": null, "net": null},
                    "usd": {"base": null, "net": null},
                    "point": {"base": null, "net": null}
                }
              }
          ],
          "metadata": {
                "resultset": {
                    "count": 3,
                    "limit": 20,
                    "offset": 0
                }
          }
        }
