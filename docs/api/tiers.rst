### iOS Tiers

iOS Tiers resource contains list of tiers associated with iOS platform's tiered payment gateways. Same unique product identifier must available on the payment gateway side.

iOS platform tiered payment gateway: Apple In-App Purchase (iTunes)

#### iOS Tier Properties

* `tier_price` - String; Tier's price. On tiered patment gateway, this price information only for showing and base price for reporting, the actual price determined by the value on payment gateway side.

* `tier_order` - Number; Tier's order. The price order, e.g., 1 - USD 0.99, 2 = USD 1.99, 3 = USD 2.99, and soon

* `currency_id` - Number; Currency's identifier, refer to Currencies resource.

* `currency` - Number; Currency code. **TODO: FIX INCONSISTENT NAMING**

* `tier_type` - Number; Tier type

    * 1: NON-CONSUMABLE - Tier that corresponded an offer as a tier (iTunes' non-consumable and auto-renewal subscription).

    * 2: CONSUMABLE - Tier that corresponded more than an offer as a tier (iTunes' consumable, Google IAB unmanaged, WP consumable).

* `is_active` - Boolean; Instance’s active status, used to be marked if the instance is active or soft-deleted. The default value is true.

* `tier_code` - String; Tier unique identifier to associates with payment gateway side. The value will be used cross clients for the identical offer.

    ```
    e.g.,

    Tempo Issue #34

    Sample of iTunes In-App Purchase's product codes:
    1. SCOOP: com.appsfoundry.scoop.nc.tempo-issue-34
    2. Tempo Whitelabel: com.appsfoundry.id.tempo.nc.tempo-issue-34
    3. SCOOP Business Magz Whitelabel: com.appsfoundry.biz.nc.tempo-issue-34


    The tier will be recorded as below:
    tier_type: 1
    tier_code: .nc.tempo-issue-34

    ```

#### Create iOS Tier

Creates iOS tier

**URL:** /ios_tiers

**Method:** POST

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code`

**Response HTTP Code:**

* `201` - New iOS Tier created

**Response Params:** See [iOS Tier Properties](#ios-tier-properties)

**Examples:**

Request:

```
POST /ios_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	“tier_price": “0.99",
	“tier_order": 1,
	“currency_id": 1,
	“tier_type": 1,
	“is_active": true,
	“tier_code": “com.IOS.c.99"
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
    "id": 1,
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": "com.IOS.c.99"
}

```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}


```

#### Update iOS Tier

Updates existing iOS tier

**URL:** /ios_tiers/{ios_tier_id}

**Method:** PUT

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code`

**Response HTTP Code:**

* `200` - iOS Tier updated

**Response Params:** See [iOS Tier Properties](#ios-tier-properties)

**Examples:**

Request:

```
PUT /ios_tiers/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	“tier_price": “0.99",
	“tier_order": 1,
	“currency_id": 1,
	“tier_type": 1,
	“is_active": true,
	“tier_code": “com.IOS.c.99"
}

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": "com.IOS.c.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Retrieve iOS Tiers

Retrieves list of the iOS Tiers

**URL:** /ios_tiers

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Tiers IOS Properties](#tiers-ios-properties). Default to ALL.

* `offset` - The offset of the records. The default value is 0

* `limit` - The limit of the records. The default value is 9999

* `order` - Order the records. The default mode is ascending order by primary identifier or ascending by created timestamp

* `q` - Simple string query to record’s string properties

**Method:** GET

**Response HTTP Code:**

* `200` - iOS tiers retrieved and returned in body content
* `204` - No matched iOS tier

**Response Params:** See [iOS Tier Properties](#ios-tier-properties)

**Examples:**

Request:

```
GET /ios_tiers?fields=id,name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    ios_tiers: [
        {
            "id": 1,
            "tier_price": "0.99",
            "tier_order": 1,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": "com.IOS.c.99",
        },
        {
            "id": 2,
            "tier_price": "1.99",
            "tier_order": 2,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": "com.IOS.c.1.99",
        },
        ...
        {
            "id": 20,
            "tier_price": "19.99",
            "tier_order": 20,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": "com.IOS.c.19.99",
        }
    ],
    "metadata": {
        "count": 367,
        "limit": 20,
        "offset": 0
    }
}

```

Fail response:

```
HTTP/1.1 500 Internal Server Error

```

#### Retrieve Individual iOS Tier

Retrieves specific iOS tier

**URL:** /ios_tiers/{ios_tier_id}

**Alternative URL:** /ios_tiers/tier_code/.nc.tempo-issue-34 ** TODO: IMPLEMENTATION **

Optional:

* `fields` - Retrieve specific fields. See [iOS Tier Properties](#tiers-ios-properties). Default to ALL.

**Method:** GET

**Response HTTP Code:**

* `200` - iOS Tier is retrieved and returned

**Response Params:** See [iOS Tier Properties](#ios-tier-properties)

**Examples:**

Request:

```
GET /ios_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
	"tier_price": "0.99",
	"tier_order": 1,
    "currency": "USD",
	"currency_id": 1,
	"tier_type": 1,
	"is_active": true,
	"tier_code": "com.IOS.c.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found

```

#### Delete iOS Tier

Deletes iOS tier by using identifier.

**URL:** /ios_tiers/{ios_tier_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - iOS Tier is deleted

**Response Params:** See [iOS Tier Properties](#ios-tier-properties)

**Examples:**

Request:

```
DELETE /ios_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


```

Success response:

```
HTTP/1.1 204 No Content

```

Fail response:

```
HTTP/1.1 404 Not Found

```

### Android Tiers

Android Tiers resource contains list of tiers associated with Android platform's tiered payment gateways. Same unique product identifier must available on the payment gateway side.

Android platform tiered payment gateway: Google In-App Billing

#### Android Tier Properties

* `tier_price` - String; Tier's price. On tiered patment gateway, this price information only for showing and base price for reporting, the actual price determined by the value on payment gateway side.

* `tier_order` - Number; Tier's order. The price order, e.g., 1 - USD 0.99, 2 = USD 1.99, 3 = USD 2.99, and so on

* `currency_id` - Number; Currency's identifier, refer to Currencies resource.

* `currency` - Number; Currency code. **TODO: FIX INCONSISTENT NAMING**

* `tier_type` - Number; Tier type

    * 1: NON-CONSUMABLE - Tier that corresponded an offer as a tier (iTunes' non-consumable and auto-renewal subscription).

    * 2: CONSUMABLE - Tier that corresponded more than an offer as a tier (iTunes' consumable, Google IAB unmanaged, WP consumable).

* `is_active` - Boolean; Instance’s active status, used to be marked if the instance is active or soft-deleted. The default value is true.

* `tier_code` - String; Tier unique identifier to associates with payment gateway side. The value will be used cross clients for the identical offer.

    ```
    e.g.,

    Tempo Issue #34

    Sample of iTunes In-App Purchase's product codes:
    1. SCOOP: com.appsfoundry.scoop.nc.tempo-issue-34
    2. Tempo Whitelabel: com.appsfoundry.id.tempo.nc.tempo-issue-34
    3. SCOOP Business Magz Whitelabel: com.appsfoundry.biz.nc.tempo-issue-34


    The tier will be recorded as below:
    tier_type: 1
    tier_code: .nc.tempo-issue-34

    ```

#### Create Android Tier

Creates Android tier

**URL:** /android_tiers

**Method:** POST

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code`

**Response HTTP Code:**

* `201` - New Android tier created

**Response Params:** See [Android Tier Properties](#android-tier-properties)

**Examples:**

Request:

```
POST /android_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"tier_price": “0.99",
	"tier_order": 1,
	"currency_id": 1,
	"tier_type": 1,
	"is_active": true,
	"tier_code": “.c.usd.0.99"
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
    "id": 1,
	“tier_price": “0.99",
	“tier_order": 1,
	“currency_id": 1,
	“tier_type": 1,
	“is_active": true,
	“tier_code": “.c.usd.0.99"
}

```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}


```

#### Update Android Tier

Updates existing android tier

**URL:** /android_tiers/{android_tier_id}

**Method:** PUT

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code` (only required if the value in tier_type is 1 or 2)

**Response HTTP Code:**

* `200` - Android tier updated

**Response Params:** See [Android Tier Properties](#android-tier-properties)

**Examples:**

Request:

```
PUT /android_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"tier_price": “0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": “.c.usd.0.99"
}

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	“tier_price": “0.99",
	“tier_order": 1,
	“currency_id": 1,
    "currency": "USD",
	“tier_type": 1,
	“is_active": true,
	“tier_code": “.c.usd.0.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Retrieve Android Tiers

Retrieves all of the android tiers

**URL:** /android_tiers

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Android Tier Properties](#android-tier-properties). Default to ALL.

* `offset` - The offset of the records. The default value is 0

* `Limit` - The limit of the records. The default value is 9999

* `order` - Order the records. The default mode is ascending order by primary identifier or ascending by created timestamp

* `q` - Simple string query to record’s string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Android tiers retrieved and returned in body content
* `204` - No matched Android tier

**Response Params:** See [Android Tier Properties](#android-tier-properties)

**Examples:**

Request:

```
GET /android_tiers?fields=id, name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    android_tiers: [
        {
            "id": 1,
            "tier_price": "0.99",
            "tier_order": 1,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.0.99",
        },
        {
            "id": 2,
            "tier_price": "1.99",
            "tier_order": 2,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.1.99",
        },
        ...
        {
            "id": 20,
            "tier_price": "19.99",
            "tier_order": 20,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.19.99",
        }
    ],
    "metadata": {
        "count": 87,
        "limit": 20,
        "offset": 0
    }
}


```

Fail response:

```
HTTP/1.1 500 Internal Server Error

```

#### Retrieve Individual android Tier

Retrieves android tier per item

**URL:** /android_tiers/{android_tier_id}

Optional:

* `fields` - Retrieve specific fields. See [Android Tier Properties](#android-tier-properties). Default to ALL.

**Method:** GET

**Response HTTP Code:**

* `200` - Android tier is retrieved and returned

**Response Params:** See [Android Tier Properties](#android-tier-properties)

**Examples:**

Request:

```
GET /android_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": "com.ANDROID.c.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found

```

#### Delete android Tier

Deletes android tier by using identifier.

**URL:** /android_tiers/{android_tier_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Android tier is deleted

**Response Params:** See [Android Tier Properties](#android-tier-properties)

**Examples:**

Request:

```
DELETE /android_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


```

Success response:

```
HTTP/1.1 204 No Content

```

Fail response:

```
HTTP/1.1 404 Not Found


```

### Windows Phone Tiers

Windows phone tiers contains bundled price for Windows Phone platform.

#### Windows Phone Tier Properties

* `tier_price` - String; Tier's price. On tiered patment gateway, this price information only for showing and base price for reporting, the actual price determined by the value on payment gateway side.

* `tier_order` - Number; Tier's order. The price order, e.g., 1 - USD 0.99, 2 = USD 1.29, 3 = USD 1.49, and so on

* `currency_id` - Number; Currency's identifier, refer to Currencies resource.

* `currency` - Number; Currency code. **TODO: FIX INCONSISTENT NAMING**

* `tier_type` - Number; Tier type

    * 1: NON-CONSUMABLE - Tier that corresponded an offer as a tier (iTunes' non-consumable and auto-renewal subscription).

    * 2: CONSUMABLE - Tier that corresponded more than an offer as a tier (iTunes' consumable, Google IAB unmanaged, WP consumable).

* `is_active` - Boolean; Instance’s active status, used to be marked if the instance is active or soft-deleted. The default value is true.

* `tier_code` - String; Tier unique identifier to associates with payment gateway side. The value will be used cross clients for the identical offer.

    ```
    e.g.,

    Tempo Issue #34

    Sample of iTunes In-App Purchase's product codes:
    1. SCOOP: com.appsfoundry.scoop.nc.tempo-issue-34
    2. Tempo Whitelabel: com.appsfoundry.id.tempo.nc.tempo-issue-34
    3. SCOOP Business Magz Whitelabel: com.appsfoundry.biz.nc.tempo-issue-34


    The tier will be recorded as below:
    tier_type: 1
    tier_code: .nc.tempo-issue-34

    ```

#### Create Windows Phone Tier

Creates windows phone tier.

**URL:** /wp_tiers

**Method:** POST

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code` (only required if the value in tier_type is 1 or 2)

**Response HTTP Code:**

* `201` - New Windows Phone Tier created

**Response Params:** See [Windowns Phone Tier Properties](#windows-phone-tier-properties)

**Examples:**

Request:

```
POST /wp_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": ".c.usd.0.99"
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
    "id" = 1.
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": ".c.usd.0.99"
}

```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}


```

#### Update Windows Phone Tier
Updates existing windows phone tier

**URL:** /wp_tiers/{windowsphone_tier_id}

**Method:** PUT

**Request Data Params:**

Required: `tier_price`, `tier_order`, `currency_id`, `tier_type`, `is_active`

Optional: `tier_code` (only required if the value in tier_type is 1 or 2)

**Response HTTP Code:**

* `200` - Windows Phone tier updated

**Response Params:** See [Windows Phone Tier Properties](#windows-phone-tier-properties)

**Examples:**

Request:

```
PUT /wp_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": ".c.usd.0.99"
}

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"tier_price": "0.99",
	"tier_order": 1,
	"currency_id": 1,
    "currency": "USD",
	"tier_type": 1,
	"is_active": true,
	"tier_code": ".c.usd.0.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Retrieve Windows Phone Tiers
Retrieves all of the windows phone tiers.

**URL:** /wp_tiers

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Windows Phone Tier Properties](#windows-phone-tier-properties). Default to ALL.

* `offset` - The offset of the records. The default value is 0

* `Limit` - The limit of the records. The default value is 9999

* `order` - Order the records. The default mode is ascending order by primary identifier or ascending by created timestamp

* `q` - Simple string query to record’s string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Windows Phone tiers retrieved and returned in body content

* `204` - No matched Windows Phone tier

**Response Params:** See [Windows Phone Tier Properties](#windows-phone-tier-properties)

**Examples:**

Request:

```
GET /wp_tiers?fields=id, name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8


{
    wp_tiers: [
        {
            "id": 1,
            "tier_price": "0.99",
            "tier_order": 1,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.0.99",
        },
        {
            "id": 2,
            "tier_price": "1.29",
            "tier_order": 2,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.1.29",
        },
        ...
        {
            "id": 20,
            "tier_price": "14.99",
            "tier_order": 20,
            "currency_id": 1,
            "currency": "USD",
            "tier_type": 1,
            "is_active": true,
            "tier_code": ".c.usd.14.99",
        }
    ],
    "metadata": {
        "count": 81,
        "limit": 20,
        "offset": 0
    }
}



```

Fail response:

```
HTTP/1.1 500 Internal Server Error

```

#### Retrieve Individual android Tier
Retrieves android tier per item

**URL:** /wp_tiers/{windowsphone_tier_id}

Optional:

* `fields` - Retrieve specific fields. See [Windows Phone Tier Properties](#windows-phone-tier-properties). Default to ALL.

**Method:** GET

**Response HTTP Code:**

* `200` - Android tier is retrieved and returned

**Response Params:** See [Windows Phone Tier Properties](#windows-phone-tier-properties)

**Examples:**

Request:

```
GET /wp_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	“tier_price": “0.99",
	“tier_order": 1,
	“currency_id": 1,
    "currency": "USD",
	“tier_type": 1,
	“is_active": true,
	“tier_code": “com.WP.c.99"
}

```

Fail response:

```
HTTP/1.1 404 Not Found

```

#### Delete Windows Phone Tier
Deletes windows phone tier by using identifier.

**URL:** /wp_tiers/{windowsphone_tier_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Windows Phone tier is deleted

**Response Params:** See [Windows Phone Tier Properties](#windows-phone-tier-properties)

**Examples:**

Request:

```
DELETE /wp_tiers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%

```

Success response:

```
HTTP/1.1 204 No Content

```

Fail response:

```
HTTP/1.1 404 Not Found

```
