Categories
==========

All the items on the storefront are categorized to ease the user to find items that suit them.


Properties
----------

id
    Number; Automatically-generated instance identifier.
item_type_id
    Number; Type of the item where the category is shown.
parent_category_id
    Number; Used for multi-level categorization, set to null if the category is not a sub-category.
name
    String; Name of the category that represents what segment the item is in.
sort_priority
    Number; Used to prioritize the order of the item in the list (default: 1).
is_active
    Boolean; Instance's active status, used to mark whether the instance is active or soft-deleted.
    The default value is true.
slug
    String; The slug of any category, used with permalinks as they help describing what the content and the URL is.
meta
    String; Metadata of the category, usually a short description of a certain category.
description
    String; Detailed explanation of the category.
media_base_url
    String; The Base URL for image files, this string must be appended to image URI and is a read-only property.
icon_image_normal
    String; On server responses, this will be the URL where the normal resolution image of the category is stored.
icon_image_highres
    String; Same with ``icon_image_normal``, but for the high-resolution image.
countries
    Number (list); The countries where the category is available. Set to ``null`` if the category is available
    for all countries.


Create
------

Adds a new category which will be eligible to be displayed on the SCOOP webpage/applications.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /categories                                          |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| item_type_id                      |   YES     |                              |
+-----------------------------------+-----------+------------------------------+
| parent_category_id                |           | null                         |
+-----------------------------------+-----------+------------------------------+
| name                              |           | null                         |
+-----------------------------------+-----------+------------------------------+
| sort_priority                     |    YES    | 1                            |
+-----------------------------------+-----------+------------------------------+
| is_active                         |    YES    | true                         |
+-----------------------------------+-----------+------------------------------+
| slug                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| meta                              |           | true                         |
+-----------------------------------+-----------+------------------------------+
| description                       |           | null                         |
+-----------------------------------+-----------+------------------------------+
| media_base_url                    |           | null                         |
+-----------------------------------+-----------+------------------------------+
| countries                         |           | []                           |
+-----------------------------------+-----------+------------------------------+
| icon_image_normal                 |           | null                         |
+-----------------------------------+-----------+------------------------------+
| icon_image_highres                |           | null                         |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /categories HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

    {
        "item_type_id": 1
        "name": "Men's",
        "slug": "mens",
        "sort_priority": 1,
        "is_active": true,
        "description": "Men's category",
        "meta": "men's category",
        "icon_image_normal": "categories/indonesia/icon.png",
        "icon_image_highres": "categories/indonesia/icon_highres.png",
        "parent_category_id": null,
        "countries": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/categories/23

    {
        "id": 23
        "item_type_id": 1
        "name": "Men's",
        "slug": "mens",
        "sort_priority": 1,
        "is_active": true,
        "description": "Men's category",
        "meta": "men's category",
        "icon_image_normal": "categories/indonesia/icon.png",
        "icon_image_highres": "categories/indonesia/icon_highres.png",
        "parent_category_id": null,
        "countries": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    }


List
----

Retrieves a paginated list of categories.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /categories                                          |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
| Query String Parameters                                                      |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 | Default   | Description                  |
+===================================+===========+==============================+
| offset                            | 0         | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| limit                             | 20        | Pagination option            |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /categories HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

    {
        "categories": [
            {
                "id": 1,
                "name": "Jawa Barat",
                "countries":[1]
            },
            {
                "id": 2,
                "code": "Jawa Timur",
                "countries":[1]
            },
            {
                "id": 3,
                "name": "Singapore",
                "countries":[2]
            },
            {
                "id": 3,
                "name": "Men's",
                "countries":[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
            }
        ],
        "metadata": {
            "resultset": {
                "count": 3,
                "offset": 0,
                "limit": 9999
            }
        }
    }


Detail
------

Retrieves individual category by using identifier.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /categories/{id}                                     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /categories/2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

    {
        "id": 23,
        "item_type_id": 1,
        "name": "Men's",
        "slug": "mens",
        "sort_priority": 1,
        "is_active": true,
        "description": "Men's category",
        "meta": "men's category",
        "icon_image_normal": "categories/indonesia/icon.png",
        "icon_image_highres": "categories/indonesia/icon_highres.png",
        "parent_category_id": null,
        "countries": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    }


Update
------

Updates the attribute of a single category, identified by the URL.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /categories/{id}                                     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

.. note::

    This requires exactly the same parameters as created.

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /categories/2 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

    {
        "item_type_id": 1,
        "name": "Men's",
        "slug": "mens",
        "sort_priority": 1,
        "is_active": true,
        "description": "Men's global category",
        "meta": "men's global category",
        "icon_image_normal": "categories/indonesia/icon.png",
        "icon_image_highres": "categories/indonesia/icon_highres.png",
        "parent_category_id": null,
        "countries": null
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

    {
        "id": 23,
        "item_type_id": 1,
        "name": "Men's",
        "slug": "mens",
        "sort_priority": 1,
        "is_active": true,
        "description": "Men's global category",
        "meta": "men's global category",
        "icon_image_normal": "categories/indonesia/icon.png",
        "icon_image_highres": "categories/indonesia/icon_highres.png",
        "parent_category_id": null,
        "countries": null
    }


Delete
------

Sets ``is_active`` value to ``false``. The API response will behave as if the resource were actually deleted.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /categories/{id}                                     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                               |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Record was deleted                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request (for delete, this would  |
|                       |               | typically be incorrect headers)      |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    DELETE /categories/2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 204 No Content
