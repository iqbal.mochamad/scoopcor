Languages
=========

Resource that contains metadata about languages that can be assigned to
:doc:`Item </web-api-v1/items/items>` resources.

.. warning::

    This resource have been depreciated and exist in a read-only
    to support existing applications that are reliant upon it.


Properties
----------

id
    Number; Instance identifier.

name
    String; Language name.

sort_priority
    Number; Sort priority use to defining order. Higher number, more priority.

is_active
    Boolean; Instance's active status, used to mark if the instance is
    active or soft-deleted. Default to true.

slug
    String, Unique; Language slug for SEO.

meta
    String; Language meta for SEO.

description
    String; Language description.

media_base_url
    String; Base URL for image files, this string must appended to image URI.

icon_image_normal
    String; Language icon image URI.

icon_image_highres
    String; Language icon image in highres URI.


Create
------

.. warning::

    This is no longer supported.


List
----

Retrieve languages.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/languages                                        |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Example Request
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /languages?fields=id,name HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Example Response
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "languages": [
            {
                "id": 1,
                "name": "Indonesia"
            },
            {
                "id": 2,
                "name": "English"
            }
        ],
        "metadata": {
            "resultset": {
                "count": 2,
                "offset": 0,
                "limit": 9999
            }
        }
    }

:download:`Schema <_schemas/languages/list-language-resp.json>`


Detail
------

Retrieve individual language

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | * /v1/languages/{id}                                 |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Example Request
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /languages/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Example Response
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 1,
        "name": "Indonesia",
        "slug": "indonesia",
        "sort_priority": 1,
        "is_active": true,
        "description": "Indonesia",
        "meta": "Indonesia",
        "icon_image_normal": "languages/indonesia/icon.png",
        "icon_image_highres": "languages/indonesia/icon_highres.png"
    }

:download:`Schema <_schemas/languages/detail-language-resp.json>`


Update
------

.. warning::

    This is no longer supported.


Delete
------

.. warning::

    This is no longer supported.
