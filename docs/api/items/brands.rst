Brands
======

"Brands" represents the title of the items; used as the title of magazines/newspapers and the series of books.


Properties
----------

id
    Number; Automatically-generated instance identifier.
name
    String; Name of the brand which will be displayed on storefront
sort_priority
    Number; Used to prioritize the order of the brands in the list
is_active
    Boolean; Instance's active status, used to mark whether the instance is active or soft-deleted. The default value is true.
slug
    String; The slug of any brand, used with permalinks as they help describing what the content and the URL is.
meta
    String; Metadata of the brand, usually a short description of a certain category.
description
    String; Detailed information about the brand.
media_base_url
    String; The Base URL for image files, this string must be appended to image URI and is a read-only property.
icon_image_normal
    String; On server responses, this will be the URL where the normal resolution image of the category is stored.
icon_image_highres
    String; Same with ``icon_image_normal``, but for the high-resolution image.
vendor_id
    Number; Vendor identifier to whom the brand belongs to.
links
    Social media links of the Brand
        link_type
            Number; Type of the social media account

            1. Facebook account
            2. Twitter account

        url
            String; The url of the aforementioned social media type.
daily_release_quota
    Number; Daily maximum quota to upload items.
default_categories
    Number (List); Category identifier(s) where the brand is included in.
default_languages
    Number (List); Language identifier(s) where the brand is included in.
default_countries
    Number (List); Country identifier(s) where the brand is included in.
default_parental_control_id
    Number (List); Parental control identifier(s) where the brand is included in.
default_item_type_id
    Number (List); Item type identifier(s) where the brand is included in (either book, magazine, or newspaper).
default_authors
    Number (List); Author identifier(s) where the brand is included in.
default_distribution_groups
    Number (List); Distribution group identifier(s) where the brand is included in.
buffet_offers (Object)
    Shows in which buffet offers the brand is included in. Contains: `id`, `name`, `offer_type`. If a brand is not included in any buffet plan, the response is NULL

Create
------

Adds a new brand

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/brands                                           |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| name                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| sort_priority                     |    YES    | 1                            |
+-----------------------------------+-----------+------------------------------+
| is_active                         |    YES    | true                         |
+-----------------------------------+-----------+------------------------------+
| slug                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| meta                              |           |                              |
+-----------------------------------+-----------+------------------------------+
| description                       |           |                              |
+-----------------------------------+-----------+------------------------------+
| vendor_id                         |           |                              |
+-----------------------------------+-----------+------------------------------+
| links                             |           |                              |
+-----------------------------------+-----------+------------------------------+
| link_type                         |           |                              |
+-----------------------------------+-----------+------------------------------+
| url                               |           |                              |
+-----------------------------------+-----------+------------------------------+
| media_base_url                    |           | null                         |
+-----------------------------------+-----------+------------------------------+
| icon_image_normal                 |           | null                         |
+-----------------------------------+-----------+------------------------------+
| icon_image_highres                |           | null                         |
+-----------------------------------+-----------+------------------------------+
| daily_release_quota               |           |                              |
+-----------------------------------+-----------+------------------------------+
| default_categories                |           |                              |
+-----------------------------------+-----------+------------------------------+
| default_languages                 |           |                              |
+-----------------------------------+-----------+------------------------------+
| default_countries                 |           |                              |
+-----------------------------------+-----------+------------------------------+
| default_parental_control_id       |           |                              |
+-----------------------------------+-----------+------------------------------+
| default_item_type_id              |           |                              |
+-----------------------------------+-----------+------------------------------+
| default_authors                   |           |                              |
+-----------------------------------+-----------+------------------------------+
| default_distribution_groups       |           |                              |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /brands HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "FHM Indonesia",
        "slug": "fhm-indonesia",
        "sort_priority": 1,
        "is_active": true,
        "description": "FHM Indonesia bring men lifestyle to the next level",
        "meta": "FHM Indonesia majalah pria metroseksual",
        "icon_image_normal": "brands/fhm-indonesia/icon.png",
        "icon_image_highres": "brands/fhm-indonesia/icon_highres.png",
        "vendor_id": 2,
        "daily_release_quota": 2,
        "default_categories": [1,2,3],
        "default_languages": [1,2,3],
        "default_countries": [1,2,3],
        "default_parental_control_id": 1,
        "default_item_type_id": 1,
        "default_authors": [1,2,3],
        "default_distribution_groups": [1,2,3],
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ],
        "buffet_offers": []
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/brands/2

.. code-block: json

    {
        "id": 2,
        "name": "FHM Indonesia",
        "slug": "fhm-indonesia",
        "sort_priority": 1,
        "is_active": true,
        "description": "FHM Indonesia bring men lifestyle to the next level",
        "meta": "FHM Indonesia majalah pria metroseksual",
        "icon_image_normal": "brands/fhm-indonesia/icon.png",
        "icon_image_highres": "brands/fhm-indonesia/icon_highres.png",
        "vendor_id": 2,
        "daily_release_quota": 2,
        "default_categories": [1,2,3],
        "default_languages": [1,2,3],
        "default_countries": [1,2,3],
        "default_parental_control_id": 1,
        "default_item_type_id": 1,
        "default_authors": [1,2,3],
        "default_distribution_groups": [1,2,3],
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ],
        "buffet_offers": [
            {
                "id": 43103,
                "name": "SCOOP Premium",
            }
        ]
    }


List
----

Retrieves a paginated list of brands.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/brands                                           |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
| Query String Parameters                                                      |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 | Default   | Description                  |
+===================================+===========+==============================+
| offset                            | 0         | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| limit                             | 20        | Pagination option            |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /brands HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "brands": [
            {
                "id": 24,
                "name": "Autocar",
                "vendor_id": 2,
                "buffet_offers": [
                    {
                        "id": 43103,
                        "name": "SCOOP Premium",
                    }
                ]
            },
            {
                "id": 25,
                "name": "FHM-Indonesia",
                "vendor_id": 2,
                "buffet_offers": []
            }
        ],
        "metadata": {
            "resultset": {
                "count": 2,
                "offset": 0,
                "limit": 10
            }
        }
    }


Detail
------

Retrieves individual brand by the identifier.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/brands/{id}                                      |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /brands/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 1,
        "name": "Tempo",
        "slug": "tempo",
        "sort_priority": 1,
        "is_active": true,
        "description": "Tempo deliver what matters",
        "meta": "Tempo berita keuangan politik",
        "icon_image_normal": "brands/tempo/icon.png",
        "icon_image_highres": "brands/tempo/icon_highres.png",
        "vendor_id": 1,
        "daily_release_quota": 2,
        "default_categories": [1,2,3],
        "default_languages": [1,2,3],
        "default_countries": [1,2,3],
        "default_parental_control_id": 1,
        "default_item_type_id": 1,
        "default_authors": [1,2,3],
        "default_distribution_groups": [1,2,3],
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ],
        "buffet_offers": [
            {
                "id": 43103,
                "name": "SCOOP Premium",
            }
        ]
    }


Update
------

Updates the attribute of any single brand.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/brands/{id}                                  |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

.. note::

    This requires exactly the same parameters as created.

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /brands/1 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "FHM",
        "slug": "fhm-indonesia",
        "sort_priority": 1,
        "is_active": true,
        "description": "FHM Indonesia bring men lifestyle to the next level",
        "meta": "FHM Indonesia majalah pria metroseksual",
        "icon_image_normal": "brands/fhm-indonesia/icon.png",
        "icon_image_highres": "brands/fhm-indonesia/icon_highres.png",
        "vendor_id": 2,
        "daily_release_quota": 2,
        "default_categories": [1,2,3],
        "default_languages": [1,2,3],
        "default_countries": [1,2,3],
        "default_parental_control_id": 1,
        "default_item_type_id": 1,
        "default_authors": [1,2,3],
        "default_distribution_groups": [1,2,3],
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ],
        "buffet_offers": [
            {
                "id": 43103,
              "name": "SCOOP Premium",
            }
        ]
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 1,
        "name": "FHM",
        "slug": "fhm-indonesia",
        "sort_priority": 1,
        "is_active": true,
        "description": "FHM Indonesia bring men lifestyle to the next level",
        "meta": "FHM Indonesia majalah pria metroseksual",
        "icon_image_normal": "brands/fhm-indonesia/icon.png",
        "icon_image_highres": "brands/fhm-indonesia/icon_highres.png",
        "vendor_id": 2,
        "daily_release_quota": 2,
        "default_categories": [1,2,3],
        "default_languages": [1,2,3],
        "default_countries": [1,2,3],
        "default_parental_control_id": 1,
        "default_item_type_id": 1,
        "default_authors": [1,2,3],
        "default_distribution_groups": [1,2,3],
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ],
        "buffet_offers": [
            {
                "id": 43103,
              "name": "SCOOP Premium",
            }
        ]
    }


Delete
------

Sets ``is_active`` value to ``false``. The API response will behave as if the resource were actually deleted.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/brands/{id}                                      |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                               |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Record was deleted                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request (for delete, this would  |
|                       |               | typically be incorrect headers)      |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    DELETE /brands/2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 204 No Content
