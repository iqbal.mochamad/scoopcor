Uploader
========


Don't get it wrong, though; "Uploader" has nothing to do with "uploading" thing. This API processes the file within the server, like, when somebody uploads a new file (and image), Portal sends "hey, there is a newly-uploaded file" notification to Scoopcor.


Processing the File
-------------------

Portal notifies scoopcor there is a new fle available for processing.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/items/<item-id>/upload                           |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Other Responses       | HTTP 202      | File is already accepted             |
|                       +---------------+--------------------------------------+
|                       | HTTP 102      | File is currently processed          |
+-----------------------+---------------+--------------------------------------+

.. attention::

    if the processing is failed, scoopcor will also return the OK response (HTTP 200), but with error code and the message indicating the status.


+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| name                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| cover_image_file_name             |    NO     |                              |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

**for ePub**

.. code-block:: http

    POST /items/3/upload  HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
      "file_name": "epubfile.ePub"
      "cover_image_file_name": "cover-of-the-item.jpg"
    }


**for PDF**

.. code-block:: http

    POST /items/3/upload  HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
      "file_name": "pdffile.pdf"
    }


Giving back signals
-------------------

Once the file is being processed, scoopcor will let the portal know whether the file is already processed (or nah) by sending some HTTP response:

* HTTP 202 - Accepted
* HTTP 102 - Processing
* HTTP 200 - OK (it could be a success or a failure ¯\\_(ツ)_/¯)

The *file processing status* given by scoopcor is triggered by ``GET`` request constantly sent by the portal.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/items/<item-id>/upload                           |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Other Responses       | HTTP 202      | File is already accepted             |
|                       +---------------+--------------------------------------+
|                       | HTTP 102      | File is currently processed          |
+-----------------------+---------------+--------------------------------------+

Response Example
^^^^^^^^^^^^^^^^

**ACCEPTED**

.. code-block:: http

    HTTP/1.1 202 ACCEPTED
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
      "status": null,
      "error_code": null,
      "user_message": "File is accepted",
      "developer_message": "File is accepted",
    }

**PROCESSING**

.. code-block:: http

    HTTP/1.1 102 PROCESSING
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
      "status": null,
      "error_code": null,
      "user_message": "File is currently processed",
      "developer_message": "File is currently processed",
    }

**COMPLETED**

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
      "status": null,
      "error_code": null,
      "user_message": "Upload successful",
      "developer_message": "Upload successful",
    }

**FAILED**

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
      "status": null,
      "error_code": null,
      "user_message": "Upload failed",
      "developer_message": "Some messages set up by the developer",
    }


to make it more obvious::

    portal: hey, i sent something
    scopcor: 202 roger that
    portal: is it already processed?
    scoopcor: 102 hold on
    portal: what about now?
    scoopcor: 102 one sec
    portal: what about now?
    scoopcor: 102 dude-_-
    portal: what about now?
    scoopcor: 200 lol OK done

::

    Deep down inside, this uploader guy really wants to do more humane,
    important thing in his brief life-span (like, *literally* uploading stuff).
    Godspeed, uploader guy :)
