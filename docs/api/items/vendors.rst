Vendors
=======

Vendor resource contains all items' providers, which in current business model is what we called publishers.


Properties
----------

id
	Number; Instance identifier

name
	String; Vendor name

sort_priority
	Number; Sort priority use to defining order. Higher number, more priority

vendor_status
	Number; Vendor status in the system
		    1. NEW: New created vendor
		    2. WAITING FOR REVIEW: Waiting for business side review for any kind of partnership
		    3. IN REVIEW: In review
		    4. REJECTED: Rejected due to some reasons
		    5. APPROVED: Approved by business side
		    6. CLEAR: Clear for any action
		    7. BANNED: Banned due to some reasons

is_active
	 Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

slug
	 String, Unique; Vendor slug for SEO

meta
	 String; Vendor meta for SEO

description
	 String; Vendor description.

media_base_url
	 String; Base URL for image files, this string must appended to image URI.

icon_image_normal
	 String; Vendor icon image URI.

icon_image_highres
	 String; Vendor icon image in highres URI.

organization_id
	  Number; Unique, organization identifier that this instance bind to. A vendor can only bind to an organization

links
    Number (list); List of the vendor's social media account(s)
				link_type
					Number; Type of the link, based on the social media account
		   			 1. Facebook account
		         2. Twitter account
		    url
		        String; The url of the social media account owned by the vendor

accounting_identifier
	 String; Accounting identifier

iso4217_code
	 String; The format of the currency in ISO 4217 standard

report_summary_as_invoice
    Boolean; Set to true if the summary will be shown as an invoice.

show_preferred_currency_rates
    Boolean; Create vendor report based on preferred currency rate

reports
    Object; consists of ``report_header_logo``, ``report_summary_format``, ``report_detail_columns``

    report_summary_as_invoice
		    Number; [explain this later]
    show_preferred_currency_rates
		    Boolean; whether the preffered currency rate is shown or not
		report_header_logo
		    Number (enum); Predefined type of header logo used for report
				    1. Apps Foundry logo
				    2. PT Aplikasi Prima Persada logo
		report_summary_format
		    Number; Which summary format to use
				    1. Summary with platforms
				    2. Summary with platforms and payment gateway
		report_detail_columns
		    Boolean (list); Data to be shown on report, to show any certain record(s), set the value to true:
				    * title
				    * product_name
				    * price
				    * revenue
				    * sales_or_return
				    * payment_gateway
				    * platform
				    * application
				    * coupon_code
				    * author
				    * isbn_or_gtin
				    * content
				    * purchase_plan_type
				    * orderline_id

revenue_shares
    Object; Shows which revenue share plan is used by the vendor

		    id
				    Number; Revenue share identifier
				is_active
				    Boolean; Instance's active status, used to mark if the instance is active or soft-deleted.
				revenue_id
				    Number; Revenue identifier, from which revenue plan this revenue share plan is based on
				valid_from
				    String; Datetime in UTC shows when the revenue share plan is valid from
				valid_to
				    String; Datetime in UTC shows when the revenue share plan is valid until


Create
------

Adds a new vendor.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/vendors                                          |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| name                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| sort_priority                     |    YES    | 1                            |
+-----------------------------------+-----------+------------------------------+
| is_active                         |    YES    | true                         |
+-----------------------------------+-----------+------------------------------+
| slug                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| meta                              |           |                              |
+-----------------------------------+-----------+------------------------------+
| description                       |           |                              |
+-----------------------------------+-----------+------------------------------+
| vendor_status                     |    YES    | 1                            |
+-----------------------------------+-----------+------------------------------+
| links                             |           |                              |
+-----------------------------------+-----------+------------------------------+
| link_type                         |           |                              |
+-----------------------------------+-----------+------------------------------+
| url                               |           |                              |
+-----------------------------------+-----------+------------------------------+
| media_base_url                    |           |                              |
+-----------------------------------+-----------+------------------------------+
| icon_image_normal                 |           |                              |
+-----------------------------------+-----------+------------------------------+
| icon_image_highres                |           |                              |
+-----------------------------------+-----------+------------------------------+
| organization_id                   |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| cut_rule_id                       |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| accounting_identifier             |           |                              |
+-----------------------------------+-----------+------------------------------+
| iso4217_code                      |           |                              |
+-----------------------------------+-----------+------------------------------+
| report_summary_as_invoice         |           |                              |
+-----------------------------------+-----------+------------------------------+
| show_preferred_currency_rates     |           |                              |
+-----------------------------------+-----------+------------------------------+
| report                            |           |                              |
+-----------------------------------+-----------+------------------------------+
| report_header_logo                |           |                              |
+-----------------------------------+-----------+------------------------------+
| report_summary_format             |           |                              |
+-----------------------------------+-----------+------------------------------+
| report_detail_columns             |           |                              |
+-----------------------------------+-----------+------------------------------+
| revenue_shares                    |           |                              |
+-----------------------------------+-----------+------------------------------+
| id                                |           |                              |
+-----------------------------------+-----------+------------------------------+
| is_active                         |           |                              |
+-----------------------------------+-----------+------------------------------+
| revenue_id                        |           |                              |
+-----------------------------------+-----------+------------------------------+
| valid_from                        |           |                              |
+-----------------------------------+-----------+------------------------------+
| valid_to                          |           |                              |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /vendors HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "MRA",
        "slug": "mra",
        "sort_priority": 1,
        "vendor_status": 6,
        "is_active": true,
        "description": "MRA",
        "meta": "MRA",
        "icon_image_normal": "vendors/mra/icon.png",
        "icon_image_highres": "vendors/mra/icon_highres.png",
        "cut_rule_id": 2,
        "organization_id": 3,
        "accounting_identifier": "P-0047",
        "iso4217_code": "USD",
				"report": {
					  "report_summary_as_invoice": "true",
		        "show_preferred_currency_rates": "true"
		        "report_header_logo": 1,
		        "report_summary_format": 2,
		        "report_detail_columns" [
		            "title": "true",
		            "product_name": "true",
		            "price": "true",
		            "revenue": "true",
		            "sales_or_return": "true",
		            "payment_gateway": "true",
		            "platform": "true",
		            "application": "true",
		            "coupon_code": "true",
		            "author": "true",
		            "isbn_or_gtin": "true",
		            "content": "true",
		            "purchase_plan_type": "true",
		            "orderline_id": "true"
	        ]
				}
        "revenue_shares": {
						"id": 481,
		        "is_active": true,
		        "revenue_id": 2,
		        "valid_from": "Fri, 01 Jan 2010 00:00:00 -0000",
		        "valid_to": "Tue, 31 Dec 2999 23:59:59 -0000",
				}
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ]
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/vendors/2

.. code-block: json

    {
        "id": 2,
        "name": "MRA",
        "slug": "mra",
        "sort_priority": 1,
        "vendor_status": 6,
        "is_active": true,
        "description": "MRA",
        "meta": "MRA",
        "icon_image_normal": "vendors/mra/icon.png",
        "icon_image_highres": "vendors/mra/icon_highres.png",
        "cut_rule_id": 2,
        "organization_id": 3,
        "accounting_identifier": "P-0047",
        "iso4217_code": "USD",
				"report": {
					  "report_summary_as_invoice": "true",
		        "show_preferred_currency_rates": "true"
		        "report_header_logo": 1,
		        "report_summary_format": 2,
		        "report_detail_columns" [
		            "title": "true",
		            "product_name": "true",
		            "price": "true",
		            "revenue": "true",
		            "sales_or_return": "true",
		            "payment_gateway": "true",
		            "platform": "true",
		            "application": "true",
		            "coupon_code": "true",
		            "author": "true",
		            "isbn_or_gtin": "true",
		            "content": "true",
		            "purchase_plan_type": "true",
		            "orderline_id": "true"
	        ]
				}
        "revenue_shares": {
						"id": 481,
		        "is_active": true,
		        "revenue_id": 2,
		        "valid_from": "Fri, 01 Jan 2010 00:00:00 -0000",
		        "valid_to": "Tue, 31 Dec 2999 23:59:59 -0000",
				}
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ]
    }


List
----

Retrieves a paginated list of vendors.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/vendors                                          |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
| Query String Parameters                                                      |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 | Default   | Description                  |
+===================================+===========+==============================+
| offset                            | 0         | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| limit                             | 20        | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| q                                 |           | String query                 |
+-----------------------------------+-----------+------------------------------+
| fields                            |           | Retrieves spesific fields    |
+-----------------------------------+-----------+------------------------------+
| expand                            | null      | Expands the result details   |
|                                   |           | supports: ext, vendors       |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /vendors?fields=id,name&offset=0&limit=1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "vendors": [
            {
                "id": 1,
                "name": "TEMPO"
            }
        ],
        "metadata": {
            "resultset": {
                "count": 357,
                "offset": 0,
                "limit": 1
            }
        }
    }


Detail
------

Retrieves individual vendor by the identifier.

+-----------------------+------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/vendors/{id}                                     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /vendors/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 2,
        "name": "MRA",
        "slug": "mra",
        "sort_priority": 1,
        "vendor_status": 6,
        "is_active": true,
        "description": "MRA",
        "meta": "MRA",
        "icon_image_normal": "vendors/mra/icon.png",
        "icon_image_highres": "vendors/mra/icon_highres.png",
        "cut_rule_id": 2,
        "organization_id": 3,
        "accounting_identifier": "P-0047",
        "iso4217_code": "USD",
				"report": {
					  "report_summary_as_invoice": "true",
		        "show_preferred_currency_rates": "true"
		        "report_header_logo": 1,
		        "report_summary_format": 2,
		        "report_detail_columns" [
		            "title": "true",
		            "product_name": "true",
		            "price": "true",
		            "revenue": "true",
		            "sales_or_return": "true",
		            "payment_gateway": "true",
		            "platform": "true",
		            "application": "true",
		            "coupon_code": "true",
		            "author": "true",
		            "isbn_or_gtin": "true",
		            "content": "true",
		            "purchase_plan_type": "true",
		            "orderline_id": "true"
	        ]
				}
        "revenue_shares": {
						"id": 481,
		        "is_active": true,
		        "revenue_id": 2,
		        "valid_from": "Fri, 01 Jan 2010 00:00:00 -0000",
		        "valid_to": "Tue, 31 Dec 2999 23:59:59 -0000",
				}
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ]
    }


Update
------

Updates single vendor data by the identifier.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/brands/{id}                                  |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

.. note::

    This requires exactly the same parameters as created.

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /vendors/1 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "MRA",
        "slug": "mra",
        "sort_priority": 1,
        "vendor_status": 6,
        "is_active": true,
        "description": "MRA",
        "meta": "MRA",
        "icon_image_normal": "vendors/mra/icon.png",
        "icon_image_highres": "vendors/mra/icon_highres.png",
        "cut_rule_id": 2,
        "organization_id": 3,
        "accounting_identifier": "P-0047",
        "iso4217_code": "USD",
				"report": {
					  "report_summary_as_invoice": "true",
		        "show_preferred_currency_rates": "true"
		        "report_header_logo": 1,
		        "report_summary_format": 2,
		        "report_detail_columns" [
		            "title": "true",
		            "product_name": "true",
		            "price": "true",
		            "revenue": "true",
		            "sales_or_return": "true",
		            "payment_gateway": "true",
		            "platform": "true",
		            "application": "true",
		            "coupon_code": "true",
		            "author": "true",
		            "isbn_or_gtin": "true",
		            "content": "true",
		            "purchase_plan_type": "true",
		            "orderline_id": "true"
	        ]
				}
        "revenue_shares": {
						"id": 481,
		        "is_active": true,
		        "revenue_id": 2,
		        "valid_from": "Fri, 01 Jan 2010 00:00:00 -0000",
		        "valid_to": "Tue, 31 Dec 2999 23:59:59 -0000",
				}
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ]
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "name": "MRA",
        "slug": "mra",
        "sort_priority": 1,
        "vendor_status": 6,
        "is_active": true,
        "description": "MRA",
        "meta": "MRA",
        "icon_image_normal": "vendors/mra/icon.png",
        "icon_image_highres": "vendors/mra/icon_highres.png",
        "cut_rule_id": 2,
        "organization_id": 3,
        "accounting_identifier": "P-0047",
        "iso4217_code": "USD",
				"report": {
					  "report_summary_as_invoice": "true",
		        "show_preferred_currency_rates": "true"
		        "report_header_logo": 1,
		        "report_summary_format": 2,
		        "report_detail_columns" [
		            "title": "true",
		            "product_name": "true",
		            "price": "true",
		            "revenue": "true",
		            "sales_or_return": "true",
		            "payment_gateway": "true",
		            "platform": "true",
		            "application": "true",
		            "coupon_code": "true",
		            "author": "true",
		            "isbn_or_gtin": "true",
		            "content": "true",
		            "purchase_plan_type": "true",
		            "orderline_id": "true"
	        ]
				}
        "revenue_shares": {
						"id": 481,
		        "is_active": true,
		        "revenue_id": 2,
		        "valid_from": "Fri, 01 Jan 2010 00:00:00 -0000",
		        "valid_to": "Tue, 31 Dec 2999 23:59:59 -0000",
				}
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ]
    }


Delete
------

Sets ``is_active`` value to ``false``. The API response will behave as if the resource were actually deleted.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/vendors/{id}                                     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                               |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Record was deleted                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request (for delete, this would  |
|                       |               | typically be incorrect headers)      |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    DELETE /vendors/2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 204 No Content
