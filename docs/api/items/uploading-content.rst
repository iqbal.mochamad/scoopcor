Uploading Item Content
======================

After an Item has been added to ScoopCOR, it's content files must be uploaded.
At the current time, the only types of files that are supported as EPUB
and PDF.

