Items
==================

Item resource is the main resource that used to render store front. Item is related to metadata like [item type](#item-types), [country](#countries), [language](#languages), [category](#categories), [author](#authors), [brand](#brands), [vendor](#vendors), [parental control](#parental-controls), pricing.

The item's type define metadata and detail properties owned by an item. The current system supported 3 types:

1. Magazine
    Magazine is the main type of digital magazine, which metadata consists of : country, language, category, brand, vendor, parental control, and pricing. Author metadata is not available for this type.

2. Book
    Book is the main type of digital book, which metadata consists of: country, language, category, brand, vendor, author, parental control, and pricing.

3. Newspaper
    Newspaper is the main type of digital newspaper, which metadata consists of: country, language, category, brand, vendor, parental control, and pricing. Author metadata is not available for this type.


Item Properties
---------------

id
    Number; Instance identifier.

name
    String; Item name.

display_until
    String; Datetime to determine to when this item displayed on store.
    There's a cron to check hourly, if the datetime passed, the item_status
    will set to NOT FOR CONSUME.

edition_code
    String; Unique edition code from previous version,
    don't use this property anymore excepts for translation layer.
    **TODO: deprecated after all migrated**

sort_priority
    Number; Sort priority use to defining order. The larger the number,
    the higher the priority.

is_active
    Boolean; Instance's status, used to mark if the instance is active
    or soft-deleted. Default value is ``true``.

content_type
    Number; Item's content type in general. For specific file type info,
    see.

    1. PDF
    2. ePub
    3. ZIP

item_status
    Number; Item's status

    1. NEW - New item in preparation; this is the default value when an item is new created with no specified status.
    2. READY FOR UPLOAD - The item is ready to be uploaded after the metadata is completed.
    3. UPLOADED - The item has already uploaded and ready for the next process.
    4. WAITING FOR REVIEW - The file is already completed and so is the metadata; waiting to be reviewed. This status is used when any item uploaded by the vendor needs review from our internal reviewer; shown to vendor to keep them posted about the status.
    5. IN REVIEW - The item is being reviewed by the reviewer(s). This status is shown to vendor to keep them posted about the status.
    6. REJECTED - The item is rejected. This status is shown to vendor to keep them posted about the status.
    7. APPROVED - The item is approved by the reviewer. This status is shown to vendor to keep them posted about the status.
    8. PREPARE FOR CONSUME - The item is in preparation to be placed on store, server side will proceed with needed processes if any before set the status to READY FOR CONSUME. This status is shown to vendor to keep them posted about the status.
    9. READY FOR CONSUME - The Item is already on store and ready for consumption.
    10. NOT FOR CONSUME - The item is taken down from store for some reasons. Users who owned the item is still able to see and download the item files. To disable the item completely, refer to ``is_active`` property

parental_control_id
    Number; Parental control identifier of the item.

parental_control
    Object; Parental control of the item. On retrieval request, this property
    is provided only if `expand` parameter contains "ext" or
    "parental_control"; otherwise, only `parental_control_id` is provided.

        id
            Number; Parental control identifier.

        name
            String; Parental control name.

        item_type_id
            Number; Item type identifier of the item.

item_type
    Object; Item type of the item. On retrieval request, this property
is provided only if ```expand`` parameter contains "ext" or "item_type";
otherwise, only ``item_type_id`` is provided.

        id
            Number; Item type identifier.

        name
            String; Item type name.

release_date
    String; Release date of the item in yyyy-mm-dd format.

brand_id
    Number; Brand identifier of the item.

brand
    Object; Brand of the item. On retrieval request, this property is
    provided only if ``expand`` parameter contains "ext" or "brand";
    otherwise, only ``brand_id`` is provided.

    id Number; Brand identifier.

    name String; Brand name.

is_featured
    Boolean; Flag to define if the item featured in query. The default value is false.

is_extra
    Boolean; Flag to define if the item is an extra item. The default value is false.

is_latest
    Boolean; Flag to define if the item is the latest item in a brand.

authors
    Array; Authors of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "authors".

        id
            Number; Author identifier.

        name
            String; Author name.

categories
    Array; Categories of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "categories".

        id
            Number; Category identifier.

        name
            String; Category name.

languages
    Array; Languages of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "languages".

        id
            Number; Language identifier.

        name
            String; Language name.

countries
    Array; Countries of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "countries".

        id
            Number; Country identifier.

        name
            String; Country name.

vendor
    Object; Vendor of the item. On retrieval request, this property is provided only if `expand` parameter contains "ext" or "vendor".

        id
            Number; Parental control identifier.

        name
            String; Parental control name.

allowed_countries
    Array; Countries where the item(s) are allowed to be distributed, all the country codes are listed in ISO 3166-1 alpha-2 format. Set to empty if the item is allowed in every country.

restricted_countries
    Array; Countries where the item(s) are restricted to be distributed, all the country codes are listed in ISO 3166-1 alpha-2 format.

reading_direction
    Number; The direction to read the item

    1. Left to Right
    2. Right to Left

issue_number
    String; Issue number of item, e.g., JUN-2013, ED 23, etc. This property is only available on magazine and newspaper.

revision_number
    String; Issue number of item, e.g., Revision 2, 2013, etc. This property is only available on book type.

slug
    String, Unique; Item slug for SEO.

media_base_url
    String; Base URL for image files, this string must appended to image URI.

thumb_image_normal
    String; URI for the item thumbnail in normal resolution.

thumb_image_highres
    String; URI for the item thumbnail in high resolution.

extra_items
    Array; List of extra item identifiers bind to this item,
    on delivery to user all of the extra items will be included

subs_weight
    Number; Weight of subscription for this item.
    Normally the value will be 1, means the item
    will deducts 1 from user owned subscription quantity on every new item
    downloaded. On some special issue for magazine, this may set to 0 so
    it won't deduct user owned subscription. Default to 1.

.. warning::

    Extra items will deliver along with the primary item,
    subscription quantity will only deducted by primary item's `subs_weight`

meta
    String; Item meta for SEO.

description
    String; Item description.

image_normal
    String; Item thumb image URI

image_highres
    String; Item thumb image in highres URI.

gtin8
    String; [GTIN8](#http://en.wikipedia.org/wiki/Global_Trade_Item_Number). This property is only available on magazine type and book type.

gtin13
    String; [GTIN13](#http://en.wikipedia.org/wiki/Global_Trade_Item_Number). This property is only available on magazine type and book type.

gtin14
    String; [GTIN14](#http://en.wikipedia.org/wiki/Global_Trade_Item_Number). This property is only available on magazine type and book type.

previews
    Array; The preview of the item.

display_offers
    Displayed offers to show pricing and discount informations.

        offer_id
            Number; Offer identifier

        offer_name
            String; Offer's name

        offer_type_id
            Number; Offer's type identifier. See [Offer Properties](#offer-properties)

        price_usd
            String; Tag normal price to show to UI side in USD

        price_idr
            String; Tag normal price to show to UI side in IDR

        price_point
            String; Tag normal price to show to UI side in SCOOP Point

        tier_code
            String; Tier code of the price

        discount_price_usd
            String; Tag discount price to show to UI side in USD. Read-only, the value will auto-populated by the system

        discount_price_idr
            String; Tag discount price to show to UI side in IDR. Read-only, the value will auto-populated by the system

        discount_price_point
            String; Tag discount price to show to UI side in SCOOP Point. Read-only, the value will auto-populated by the system

        discount_tier_code
            String; Tier code of the price when discount. Read-only, the value will auto-populated by the system

        aggr_price_usd
            String; Tag aggregator price to show to UI side in USD

        aggr_price_usd
            String; Tag aggregator price to show to UI side in IDR

        aggr_price_usd
            String; Tag aggregator price to show to UI side in SCOOP Point

        discount_tag
            String; Discount tag, very short text to describe the discount. Read-only, the value will auto-populated by the system

        discount_name
            String; Discount name. Read-only, the value will auto-populated by the system


Create Item
-----------

This service use to create and return new created item on response.

On creation or update, if the item status set to PREPARE FOR CONSUME, the delivery of new item will be triggered and queued for later (in short) process. After the process is completed, server-side will automatically set the item status to READY FOR CONSUME.

**URL:** /items

**Method:** POST

**Request Data Params:**

Required: `name`, `item_type_id`, `sort_priority`, `is_active`, `slug`, `parental_control_id`, `release_date`, `brand_id`, `is_featured`, `is_extra`, `slug`, `subs_weight`, `thumb_image_normal`, `thumb_image_highres`, `image_normal`, `image_highres`, `reading_direction`

depends on `item_type_id`: `issue_number`, `revision_number`

Optional: `authors`, `categories`, `languages`, `countries`,  `extra_items`,  `meta`, `description`, `previews`, `allowed_countries`, `restricted_countries`

depends on `item_type_id`: `gtin8`, `gtin13`, `gtin14`

**Response HTTP Code:**

* `201` - New item created

**Response Params:** See [Item Properties](#item-properties)

Examples
^^^^^^^^

Create new magazine
"""""""""""""""""""

POST /items HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%


 **TODO: edition_code -- deprecated after all migrated**

.. code-block:: json

    {
        "name": "Tempo ED 27 2014",
        "item_type_id": 1,
        "display_until": "2014-05-12T10:00:00.000Z",
        "content_type": 1,
        "sort_priority": 1,
        "is_active": true,
        "edition_code": "ID_TEMED272014",
        "brand_id": 20,
        "is_featured": false,
        "is_extra": false,
        "release_date": "2014-01-20",
        "parental_control_id": 3,
        "issue_no": "ED 27 2014",
        "slug": "ed-27-2014",
        "thumb_image_normal": "items/tempo/ed-27-2014/thumb_cover_normal.png",
        "thumb_image_highres": "items/tempo/ed-27-2014/thumb_cover_highres.png",
        "subs_weight": 1,
        "description": "Jokowi efek gak ngefek",
        "meta": "Tempo ED 27 2014 Jokowi efek gak ngefek",
        "image_normal": "items/tempo/ed-27-2014/cover_normal.png",
        "image_highres": "items/tempo/ed-27-2014/cover_highres.png",
        "reading_directions" : 1,
        "allowed_countries" : ["ID", "AU", "MY"],
        "restricted_countries" : ["UK", "NZ"],
        "categories": [1, 5, 6],
        "languages": [1],
        "countries": [1]
    }


Create new book
"""""""""""""""

POST /items HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

 **TODO: edition_code -- deprecated after all migrated**

.. code-block:: json

    {
        "name": "Harry Potter and the Winter is Coming",
        "item_type_id": 2,
        "content_type": 1,
        "display_until": "2014-05-12T10:00:00.000Z",
        "sort_priority": 1,
        "is_active": true,
        "edition_code": "ID_HPANDTWIC",
        "brand_id": 345,
        "is_featured": false,
        "is_extra": false,
        "parental_control_id": 2,
        "release_date": "2014-01-20",
        "revision_number": "2014",
        "slug": "harry-potter-and-the-winter-is-coming",
        "thumb_image_normal": "items/bip/harry-potter-and-the-winter-is-coming/thumb_cover_normal.png",
        "thumb_image_highres": "items/bip/harry-potter-and-the-winter-is-coming/thumb_cover_highres.png",
        "subs_weight": 1,
        "description": "Harry Potter sucked by giant sandworm and find himself in Westeros",
        "meta": "Harry Potter and the Winter is Coming sucked by giant sandworm and find himself in Westeros",
        "image_normal": "items/bip/harry-potter-and-the-winter-is-coming/cover_normal.png",
        "image_highres": "items/bip/harry-potter-and-the-winter-is-coming/cover_highres.png",
        "reading_directions" : 1,
        "allowed_countries" : ["ID", "AU", "MY"],
        "restricted_countries" : ["UK", "NZ"],
        "categories": [23, 55, 64],
        "languages": [2],
        "countries": [9]
    }

Create new newspaper
""""""""""""""""""""

POST /items HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%


 **TODO: edition_code -- deprecated after all migrated**

.. code-block:: json

    {
        "name": "Kompas 14 APR 2014 (Siang)",
        "item_type_id": 3,
        "content_type": 1,
        "display_until": "2014-05-12T10:00:00.000Z",
        "sort_priority": 1,
        "is_active": true,
        "edition_code": "ID_KOM14APR2014S",
        "brand_id": 305,
        "is_featured": false,
        "is_extra": false,
        "parental_control_id": 1,
        "release_date": "2014-01-20T19:00:000Z",
        "issue_no": "14 APR Siang",
        "slug": "14-apr-2014-siang",
        "thumb_image_normal": "items/kompas/14-apr-2014-siang/thumb_cover_normal.png",
        "thumb_image_highres": "items/kompas/14-apr-2014-siang/thumb_cover_highres.png",
        "subs_weight": 1,
        "description": "Pohon tumbang di Slipi",
        "meta": "Kompas 14 APR 2014 (Siang) Pohon tumbang di Slipi",
        "image_normal": "items/kompas/14-apr-2014-siang/cover_normal.png",
        "image_normal": "items/kompas/14-apr-2014-siang/cover_highres.png",
        "reading_directions" : 1,
        "allowed_countries" : ["ID", "AU", "MY"],
        "restricted_countries" : ["UK", "NZ"],
        "categories": [40, 41],
        "languages": [1],
        "countries": [1]
    }


Success response
''''''''''''''''

HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/items/45


 **TODO: edition_code -- deprecated after all migrated**

.. code-block:: json

    {
        "id": 45,
        "name": "Tempo ED 27 2014",
        "item_type_id": 1,
        "content_type": 1,
        "display_until": "2014-05-12T10:00:00.000Z",
        "sort_priority": 1,
        "is_active": true,
        "edition_code": "ID_TEMED272014",
        "brand_id": 20,
        "is_featured": false,
        "is_extra": false,
        "parental_control_id": 3,
        "release_date": "2014-01-20",
        "issue_no": "ED 27 2014",
        "slug": "ed-27-2014",
        "thumb_image_normal": "items/tempo/ed-27-2014/thumb_cover_normal.png",
        "thumb_image_highres": "items/tempo/ed-27-2014/thumb_cover_highres.png",
        "subs_weight": 1,
        "exclusive_countries": null,
        "description": "Jokowi efek gak ngefek",
        "meta": "Tempo ED 27 2014 Jokowi efek gak ngefek",
        "image_normal": "items/tempo/ed-27-2014/cover_normal.png",
        "image_highres": "items/tempo/ed-27-2014/cover_highres.png",
        "reading_directions" : 1,
        "allowed_countries" : ["ID", "AU", "MY"],
        "restricted_countries" : ["UK", "NZ"],
        "gtin8": null,
        "gtin13": null,
        "gtin14": null,
        "categories": [1, 5, 6],
        "languages": [1],
        "countries": [1]
    }


Fail response
'''''''''''''

HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "status": 409,
        "error_code": 409101,
        "developer_message": "Conflict. Duplicate value on unique parameter",
        "user_message": "Duplicate value on unique parameter"
    }


Retrieve Items
--------------

Retrieve items.

End-user client side only showing items that can be consumed by end-users.
For store front, the retrieval should always query for `is_active`
equals true, `is_extra` equals false, and `item_status` in *READY FOR CONSUME*
or *NOT FOR CONSUME*. For User's cloud library, end-user client side should
only show active items.

By default the `order` is ascending by id, End-user client side will need
to specify `order` as *-release_date* if need to show the list as latest

**URL:** /items

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched
* `fields` - Retrieve specific fields. See [Item Properties](#item-properties). Default to ALL
* `offset` - Offset of the records. Default to 0
* `limit` - Limit of the records. Default to 20
* `order` - Order the records. Default to order ascending by primary identifier or created timestamp
* `expand` - Expand the result details. Default to null, the supported expand for items are as below
    * ext: get all related filters as object or array of objects contains identifier and name. See [Item Properties](#item-properties)
    * display_offers: get display offers as array and also offer's meta informations (this expand value is covered in "ext")
    * parental_control: get related parental_control as object (this expand value is covered in "ext")
    * item_type: get related item_type as object (this expand value is covered in "ext")
    * brand: get related brand as object (this expand value is covered in "ext")
    * vendor: get related vendor as object (this expand value is covered in "ext")
    * authors: get all related authors as array of objects (this expand value is covered in "ext")
    * categories: get all related categories as array of objects (this expand value is covered in "ext")
    * languages: get all related languages as array of objects (this expand value is covered in "ext")
    * countries: get all related countries as array of objects (this expand value is covered in "ext")
* `item_id` - Provided item identifier(s) to retrieve. Response will only returns if any matched
* `item_type_id` - Filter by item type identifier. Response will returns if matched
* `category_id` - Filter by category identifier(s). Response will returns if any matched
* `brand_id` - Filter by brand identifier. Response will returns if matched
* `vendor_id` - Filter by vendor identifier. Response will returns if matched
* `language_id` - Filter by language identifier(s). Response will returns if any matched
* `country_id` - Filter by country identifier(s). Response will returns if any matched
* `author_id` - Filter by author identifier(s). Response will returns if any matched
* `q` - Simple string query to records' string properties
* `created__lt`, `created__lte`, `created__gt`, `created__gte` - Filter `created` with operators
* `release_date__lt`, `release_date__lte`, `release_date__gt`, `release_date__gte` - Filter `release_date` with operators
* `item_status` - Filter by item's status(es). Response will only returns if any matched
* `display_offer_plid` - Filter result's offer for specific platform identifier. By default if this parameter is not provided, base offers will be returned
* `allowed_countries` - Array; Countries where the item(s) are allowed to be distributed, all the country codes are listed in ISO 3166-1 alpha-2 format. Set to empty if the item is allowed in every country.
* `restricted_countries` - Array; Countries where the item(s) are restricted to be distributed, all the country codes are listed in ISO 3166-1 alpha-2 format.
* `reading_direction` - Number; The direction to read the item
    * 1: Left to Right
    * 2: Right to Left

**Method:** GET

**Alternative Method:** **TODO: to be implemented**

To avoid maximum URL char in some user-agent, this resource also provided POST override. Set the HTTP Method to POST, include X-HTTP-Method-Override: GET in HTTP Header, and md5 the body content and place it after the resource alias

**Response HTTP Code:**

* `200` - Items retrieved and returned in body content
* `204` - No matched item

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

Retrieve magazine published by specific vendor, after specific release_date, and from offest 10 with limit 25


GET /items?offset=10&limit=25&vendor_id=2&release_date__gt=2014-01-01 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Retrieve magazine published by specific brand with expand "ext"


GET /items?brand_id=10&expand=ext HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Success response with default properties:


HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "items": [{
            "is_featured": false,
            "extra_items": [],
            "revision_number": "by Walter Isaacson",
            "subs_weight": 1,
            "parental_control_id": 1,
            "item_type_id": 2,
            "is_active": true,
            "item_status": 10,
            "gtin8": "",
            "brand_id": 1034,
            "thumb_image_normal": "images/1/1034/general_small_covers/ID_MIZ2012MTH12SJ_S.jpg",
            "meta": "",
            "content_type": 1,
            "filenames": ["images/1/1034/ID_MIZ2012MTH12SJ.zip"],
            "id": 8175,
            "edition_code": "ID_MIZ2012MTH12SJ",
            "image_highres": "images/1/1034/big_covers/ID_MIZ2012MTH12SJ_B.jpg",
            "sort_priority": 1,
            "image_normal": "images/1/1034/general_big_covers/ID_MIZ2012MTH12SJ_B.jpg",
            "name": "Steve Jobs",
            "release_date": "2012-12-10T00:00:00",
            "is_extra": false,
            "slug": "steve-jobs",
            "media_base_url": "http://images.getscoop.com/magazine_static/",
            "gtin14": "",
            "gtin13": "",
            "previews": [],
            "reading_directions" : 1,
            "allowed_countries" : ["ID", "AU", "MY"],
            "restricted_countries" : ["UK", "NZ"],
            "thumb_image_highres": "images/1/1034/small_covers/ID_MIZ2012MTH12SJ_S.jpg"
        },
        {
            "is_featured": false,
            "extra_items": [],
            "subs_weight": 1,
            "parental_control_id": 1,
            "item_type_id": 1,
            "is_active": true,
            "item_status": 9,
            "gtin8": "",
            "brand_id": 24,
            "thumb_image_normal": "None",
            "meta": "",
            "content_type": 1,
            "filenames": ["images/1/24/ID_CAL2011ED13.zip"],
            "id": 79,
            "edition_code": "ID_CAL2011ED13",
            "image_highres": "images/1/24/big_covers/big_cover__.png",
            "sort_priority": 1,
            "image_normal": "None",
            "name": "Charlie and Lola / JAN 2011",
            "release_date": "2010-12-29T00:00:00",
            "is_extra": false,
            "slug": "charlie-and-lola-jan-2011",
            "media_base_url": "http://images.getscoop.com/magazine_static/",
            "gtin14": "None",
            "issue_number": "JAN 2011",
            "gtin13": "None",
            "previews": [],
            "reading_directions" : 1,
            "allowed_countries" : ["ID", "AU", "MY"],
            "restricted_countries" : ["UK", "NZ"],
            "thumb_image_highres": "images/1/24/small_covers/small_cover__.png"
        }],
        "metadata": {
            "resultset": {
                "count": 48140,
                "limit": 20,
                "offset": 0
            }
        }
    }


Success response with "ext,display_offers" as expand value:


HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "items": [{
            "is_featured": false,
            "extra_items": [],
            "revision_number": "Yoris Sebastian",
            "subs_weight": 1,
            "parental_control_id": 1,
            "item_type_id": 2,
            "brand": {
                "id": 4848,
                "name": "Oh My Goodness - Creative Junkies"
            },
            "is_active": true,
            "item_status": 10,
            "gtin8": "",
            "brand_id": 4848,
            "thumb_image_normal": "images/1/4848/general_small_covers/ID_GPU2013MTH05OMGCJ_S.jpg",
            "meta": "",
            "content_type": 1,
            "authors": [{
                "id": 1739,
                "name": "Yoris Sebastian"
            }],
            "vendor": {
                "id": 251,
                "name": "Gramedia Pustaka Utama"
            },
            "display_offers": [],
            "filenames": ["images/1/4848/ID_GPU2013MTH05OMGCJ.zip"],
            "id": 18068,
            "edition_code": "ID_GPU2013MTH05OMGCJ",
            "image_highres": "images/1/4848/big_covers/ID_GPU2013MTH05OMGCJ_B.jpg",
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "sort_priority": 1,
            "image_normal": "images/1/4848/general_covers/ID_GPU2013MTH05OMGCJ_C.jpg",
            "name": "Oh My Goodness - Creative Junkies",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2013-05-21T00:00:00",
            "is_extra": false,
            "item_type": {
                "id": 2,
                "name": "Book"
            },
            "slug": "oh-my-goodness-creative-junkies",
            "media_base_url": "http://images.getscoop.com/magazine_static/",
            "gtin14": "9789792255263",
            "categories": [{
                "id": 74,
                "name": "Motivation & Self-Help"
            }],
            "gtin13": "",
            "parental_control": {
                "id": 1,
                "name": "4+"
            },
            "previews": [],
            "reading_directions" : 1,
            "allowed_countries" : ["ID", "AU", "MY"],
            "restricted_countries" : ["UK", "NZ"],
            "thumb_image_highres": "images/1/4848/small_covers/ID_GPU2013MTH05OMGCJ_S.jpg"
        },
        {
            "is_featured": false,
            "extra_items": [],
            "subs_weight": 1,
            "parental_control_id": 1,
            "item_type_id": 1,
            "brand": {
                "id": 24,
                "name": "Charlie and Lola"
            },
            "is_active": true,
            "item_status": 9,
            "gtin8": "",
            "brand_id": 24,
            "thumb_image_normal": "None",
            "meta": "",
            "content_type": 1,
            "authors": [],
            "vendor": {
                "id": 2,
                "name": "MRA"
            },
            "display_offers": [{
                "price_usd": "0.99",
                "offer_id": 940,
                "offer_type_id": 1,
                "price_idr": "9000.00",
                "price_point": 95,
                "is_free": false,
                "offer_name": "Single Edition"
            }],
            "filenames": ["images/1/24/ID_CAL2011ED13.zip"],
            "id": 79,
            "edition_code": "ID_CAL2011ED13",
            "image_highres": "images/1/24/big_covers/big_cover__.png",
            "languages": [{
                "id": 2,
                "name": "Indonesian"
            }],
            "sort_priority": 1,
            "image_normal": "None",
            "name": "Charlie and Lola / JAN 2011",
            "countries": [{
                "id": 1,
                "name": "Indonesia"
            }],
            "release_date": "2010-12-29T00:00:00",
            "is_extra": false,
            "item_type": {
                "id": 1,
                "name": "Magazine"
            },
            "slug": "charlie-and-lola-jan-2011",
            "media_base_url": "http://images.getscoop.com/magazine_static/",
            "gtin14": "None",
            "issue_number": "JAN 2011",
            "categories": [{
                "id": 1,
                "name": "Kids"
            }],
            "gtin13": "None",
            "parental_control": {
                "id": 1,
                "name": "4+"
            },
            "previews": [],
            "reading_directions" : 1,
            "allowed_countries" : ["ID", "AU", "MY"],
            "restricted_countries" : ["UK", "NZ"],
            "thumb_image_highres": "images/1/24/small_covers/small_cover__.png"
        }],
        "metadata": {
            "resultset": {
                "count": 48140,
                "limit": 20,
                "offset": 0
            }
        }
    }


Fail response:


HTTP/1.1 500 Internal Server Error


#### Retrieve Latest Items

Retrieve latest items.

**URL:** /items/latest

Optional:

See [Retrieve Items](#retrieve-items), except `order` will be ignored

**Method:** GET

**Response HTTP Code:**

* `200` - Items retrieved and returned in body content

* `204` - No matched item

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

Retrieve latest magazines published by specific vendor, after specific release_date, and from offest 10 with limit 25


GET /items/latest?offset=10&limit=25&vendor_id=2&release_date__gt=2014-01-01 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Retrieve latest magazines published by specific brand with expand "ext" and "display_offers"


GET /items/latest?brand_id=10&expand=ext,display_offers HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


#### Retrieve Popular Items

Retrieve popular items.

**URL:** /items/popular

Optional:

See [Retrieve Items](#retrieve-items), except `order` and `item_id` will be ignored

**Method:** GET

**Response HTTP Code:**

* `200` - Items retrieved and returned in body content

* `204` - No matched item

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

Retrieve popular magazines published by specific vendor, after specific release_date, and from offest 10 with limit 25


GET /items/popular?offset=10&limit=25&vendor_id=2&release_date__gt=2014-01-01 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Retrieve popular magazines published by specific brand with expand "ext" and "display_offers"


GET /items/popular?brand_id=10&expand=ext,display_offers HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


#### Retrieve Free Items

Retrieve free items, sorted by the latest item.

**URL:** /items?is_free=True

Optional:

See [Retrieve Items](#retrieve-items); but at `display_offers` parameter, the prices (`price_usd`, `price_idr`, `price_point`) are set to "0.00"/"0" and the value of `is_free` is `true`

**Method:** GET

**Response HTTP Code:**

* `200` - Items retrieved and returned in body content

* `204` - No matched item

**Response Params:** See [Item Properties](#item-properties); but at `display_offers` parameter, the prices (`price_usd`, `price_idr`, `price_point`) are set to "0.00"/"0" and the value of `is_free` is `true`

**Examples:**

Request:

Retrieve free items, and from offest 10 with limit 25


GET /items?is_free=True


#### Search Items

Test based search resource for items. This service will seek for items and it's metadatas, then returns set of items.

The differences between this resource and GET `/items` are:

1. Seek more deep into items related metadata: authors, categories, brands, vendors, languages, countries

2. `q` URL parameter is compulsory, if none provided HTTP 400 will be returned

**URL:** /items/searches?q={query}

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched
* `fields` - Retrieve specific fields. See [Item Properties](#item-properties). Default to ALL
* `offset` - Offset of the records. Default to 0
* `limit` - Limit of the records. Default to 20
* `order` - Order the records. Default to order ascending by primary identifier or created timestamp
* `expand` - Expand the result details. Default to null, the supported expand for items are as below
    * ext: get all related filters as object or array of objects contains identifier and name. See [Item Properties](#item-properties)
    * display_offers: get display offers as array and also offer's meta informations (this expand value is covered in "ext")
    * parental_control: get related parental_control as object (this expand value is covered in "ext")
    * item_type: get related item_type as object (this expand value is covered in "ext")
    * brand: get related brand as object (this expand value is covered in "ext")
    * vendor: get related vendor as object (this expand value is covered in "ext")
    * authors: get all related authors as array of objects (this expand value is covered in "ext")
    * categories: get all related categories as array of objects (this expand value is covered in "ext")
    * languages: get all related languages as array of objects (this expand value is covered in "ext")
    * countries: get all related countries as array of objects (this expand value is covered in "ext")
* `item_type_id` - Filter by item type identifier(s). Response will returns if any matched
* `category_id` - Filter by category identifier(s). Response will returns if any matched
* `brand_id` - Filter by brand identifier(s). Response will returns if any matched
* `vendor_id` - Filter by vendor identifier(s). Response will returns if any matched
* `language_id` - Filter by language identifier(s). Response will returns if any matched
* `country_id` - Filter by country identifier(s). Response will returns if any matched
* `author_id` - Filter by author identifier(s). Response will returns if any matched
* `created__lt`, `created__lte`, `created__gt`, `created__gte` - Filter created with operators
* `release_date__lt`, `release_date__lte`, `release_date__gt`, `release_date__gte` - Filter release_date with operators
* `item_status` - Filter by item's status(es). Response will only returns if any matched
* `display_offer_plid` - Filter result's offer for specific platform identifier. By default if this parameter is not provided, base offers will be returned

Note: consider the max URL length on some system

**Method:** GET

**TODO: Alternative Method:**

To avoid maximum URL char in some user-agent, this resource also provided POST override. Set the HTTP Method to POST, include X-HTTP-Method-Override: GET in HTTP Header, and md5 the body content and place it after the resource alias

**Response HTTP Code:**

* `200` - Items retrieved and returned in body content
* `204` - No matched item

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

Retrieve items contains "rowling" string (most likely will hit by author name) from offest 10 with limit 25


GET /items/searches?q=rowling&offset=10&limit=25 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


#### Search Suggestions for Items

Search suggestions for Items.

This service query for brand's names and returns max 10 suggested names

**URL:** /items/searches/suggestions/{query}

**Method:** GET

**Response HTTP Code:**

* `200` - Suggestions retrieved and returned in body content

* `204` - No matched suggestion

**Response Params:**

`Suggestions` - Array; Array of suggestions from the system

Note: `metadata` property will be static for now, this reserved for future use

**Examples:**



|   GET /items/searches/suggestions/auto HTTP/1.1
|   Authorization: Bearer ajksfhiau12371894^&*@&^$%


    |   HTTP/1.1 200 OK
    |   Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "suggestions": [
            "Autocar",
            "Automotif",
            "Auto-repair",
            "Auto Robotics"
        ],
        "metadata": {
            "resultset": {
                "count": 10,
                "offset": 0,
                "limit": 10
            }
        }
    }


#### Retrieve Individual Item

Retrieve individual item using identifier.

**URL:** /items/{item_id}

**Alternative URL:** /items/edition_code/{edition_code} -- **TODO: Deprecated after all migrated**

Optional:

* `fields` - Retrieve specific properties. See [Item Properties](#item-properties). Default to ALL
* `expand` - Expand the result details. Default to null, the supported expand for items are as below
    * display_offers: get display offers
* `display_offer_plid` - Filter result's offer for specific platform identifier. By default if this parameter is not provided, base offers will be returned

**Method:** GET

**Response HTTP Code:**

* `200` - Item found and returned

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:


GET /items/45?expand=display_offers HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Success response:


HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "is_featured": false,
        "extra_items": [],
        "description": "",
        "subs_weight": 1,
        "parental_control_id": 1,
        "item_type_id": 1,
        "brand": {
            "id": 72,
            "name": "Bloomberg Businessweek"
        },
        "is_active": true,
        "item_status": 9,
        "gtin8": "",
        "brand_id": 72,
        "thumb_image_normal": "None",
        "meta": "",
        "content_type": 1,
        "authors": [],
        "vendor": {
            "id": 27,
            "name": "Indomedia Dinamika"
        },
        "display_offers": [{
            "price_usd": "2.99",
            "offer_id": 1093,
            "offer_type_id": 1,
            "price_idr": "27000.00",
            "price_point": 290,
            "is_free": false,
            "offer_name": "Single Edition"
        }],
        "filenames": ["images/1/72/ID_BLO2430MAR2011.zip"],
        "id": 232,
        "edition_code": "ID_BLO2430MAR2011",
        "image_highres": "images/1/72/big_covers/b_.png",
        "languages": [{
            "id": 2,
            "name": "Indonesian"
        }],
        "sort_priority": 1,
        "image_normal": "None",
        "name": "Bloomberg Businessweek / 24\u201330 MAR 2011",
        "countries": [{
            "id": 1,
            "name": "Indonesia"
        }],
        "release_date": "2011-03-22T00:00:00",
        "is_extra": false,
        "item_type": {
            "id": 1,
            "name": "Magazine"
        },
        "slug": "bloomberg-businessweek-24-30-mar-2011",
        "media_base_url": "http://images.getscoop.com/magazine_static/",
        "gtin14": "None",
        "issue_number": "24\u201330 MAR 2011",
        "categories": [{
            "id": 6,
            "name": "Business & Finance"
        }],
        "gtin13": "None",
        "parental_control": {
            "id": 1,
            "name": "4+"
        },
        "previews": [],
        "reading_directions" : 1,
        "allowed_countries" : ["ID", "AU", "MY"],
        "restricted_countries" : ["UK", "NZ"],
        "thumb_image_highres": "images/1/72/small_covers/s_.png"
    }


Fail response:


HTTP/1.1 404 Not Found


#### Retrieve Item Recommendations

Retrieve recommendation for specific item.

**URL:** /items/{item_id}/recommendations

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched
* `fields` - Retrieve specific fields. See [Item Properties](#item-properties). Default to ALL
* `offset` - Offset of the records. Default to 0
* `limit` - Limit of the records. Default to 20
* `order` - Order the records. Default to order ascending by primary identifier or created timestamp
* `expand` - Expand the result details. Default to null, the supported expand for items are as below
    * ext: get all related filters as object or array of objects contains identifier and name. See [Item Properties](#item-properties)
    * display_offers: get display offer array and also offer's meta informations (this expand value is covered in "ext")
    * parental_control: get related parental control as object (this expand value is covered in "ext")
    * item_type: get related item type as object (this expand value is covered in "ext")
    * brand: get related brand as object (this expand value is covered in "ext")
    * vendor: get related vendor as object (this expand value is covered in "ext")
    * authors: get all related authors as array of objects (this expand value is covered in "ext")
    * categories: get all related categories as array of objects (this expand value is covered in "ext")
    * languages: get all related languages as array of objects (this expand value is covered in "ext")
    * countries: get all related countries as array of objects (this expand value is covered in "ext")
* `display_offer_plid` - Filter result's offer for specific platform identifier. By default if this parameter is not provided, base offers will be returned

**Method:** GET

**Response HTTP Code:**

* `200` - Item found and returned

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:


GET /items/45/recommendations?expand=ext HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


#### Update Item

Update item.

**URL:** /items/{item_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `item_type_id`, `sort_priority`, `is_active`, `slug`, `parental_control_id`, `release_date`, `brand_id`, `is_featured`, `is_extra`, `slug`, `subs_weight`, `thumb_image_normal`, `thumb_image_highres`, `image_normal`, `image_highres`,

depends on `item_type_id`: `issue_number`, `revision_number`

Optional: `authors`, `categories`, `languages`, `countries`,  `extra_items`,  `meta`, `description`, `previews`

depends on `item_type_id`: `gtin8`, `gtin13`, `gtin14`

**Response HTTP Code:**

* `200` - Item updated

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:


PUT /items/45 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

 **TODO: edition_code -- deprecated after all migrated**

.. code-block:: json

    {
        "name": "Tempo ED 27 2014",
        "item_type_id": 1,
        "content_type": 1,
        "display_until": "2014-05-12T10:00:00.000Z",
        "sort_priority": 2,
        "is_active": true,
        "edition_code": "ID_TEMED272014",
        "brand_id": 20,
        "is_featured": false,
        "is_extra": false,
        "parental_control_id": 3,
        "release_date": "2014-01-20",
        "issue_no": "ED 27 2014",
        "slug": "ed-27-2014",
        "thumb_image_normal": "items/tempo/ed-27-2014/thumb_cover_normal.png",
        "thumb_image_highres": "items/tempo/ed-27-2014/thumb_cover_highres.png",
        "subs_weight": 1,
        "exclusive_countries": null,
        "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "meta": "Tempo ED 27 2014 Jokowi efek gak ngefek",
        "image_normal": "items/tempo/ed-27-2014/cover_normal.png",
        "image_highres": "items/tempo/ed-27-2014/cover_highres.png",
        "gtin8": null,
        "gtin13": null,
        "gtin14": null,
        "reading_directions" : 1,
        "allowed_countries" : ["ID", "AU", "MY"],
        "restricted_countries" : ["UK", "NZ"],
        "categories": [1, 5, 6],
        "languages": [1],
        "countries": [1]
    }


Success response:


HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

 **TODO: edition_code deprecated after all migrated**

.. code-block:: json

    {
        "id": 45,
        "name": "Tempo ED 27 2014",
        "item_type_id": 1,
        "content_type": 1,
        "display_until": "2014-05-12T10:00:00.000Z",
        "sort_priority": 2,
        "is_active": true,
        "edition_code": "ID_TEMED272014",
        "brand_id": 20,
        "is_featured": false,
        "is_extra": false,
        "parental_control_id": 3,
        "release_date": "2014-01-20",
        "issue_no": "ED 27 2014",
        "slug": "ed-27-2014",
        "thumb_image_normal": "items/tempo/ed-27-2014/thumb_cover_normal.png",
        "thumb_image_highres": "items/tempo/ed-27-2014/thumb_cover_highres.png",
        "subs_weight": 1,
        "exclusive_countries": null,
        "description": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        "meta": "Tempo ED 27 2014 Jokowi efek gak ngefek",
        "image_normal": "items/tempo/ed-27-2014/cover_normal.png",
        "image_highres": "items/tempo/ed-27-2014/cover_highres.png",
        "gtin8": null,
        "gtin13": null,
        "gtin14": null,
        "reading_directions" : 1,
        "allowed_countries" : ["ID", "AU", "MY"],
        "restricted_countries" : ["UK", "NZ"],
        "categories": [1, 5, 6],
        "languages": [1],
        "countries": [1]
    }


Fail response:


HTTP/1.1 404 Not Found


#### Delete Item

Delete item. This service will set `is_active` value to false.

**URL:** /items/{item_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Item deleted

**Response Params:** -

**Examples:**

Request:


DELETE /items/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Success response:


HTTP/1.1 204 No Content


Fail response:


HTTP/1.1 404 Not Found


### Item Files

Item file resource used to get which files are linked to an item. This resource should only used by the server side, for client side to get file for download, see [Downloads](#downloads).

If there's new kind of type supported (e.g., support to open HTML file), along with logic implementation, file type must be added.

#### Item File Properties

* `id` - Number; Instance identifier

* `item_id` - Number; Item's identifier which this file linked to

* `file_name` - String; File name

* `file_type` - Number; File type

    * 1: PDF Bulk ZIP Encrypted, PDF document in a single file, one item one file

    * 2: ePub Bulk ZIP Encrypted, PDF file in a bulk, one item one file

    * 3: Image File RAW Encrypted, single file of image, mostly for alternative image version or ads

* `file_order` - Number; Order number defining file order starts from 1, ascending. For single file item, the value will be set to 1

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `file_status` - Number; File status. Default to NEW

    * 1: NEW - New added file, ready for upload; State: not active

    * 2: UPLOADING - File uploading. for later use if the system supports pause-and-resume in file upload progress; State: not active

    * 3: UPLOADED - File uploaded; State: not active

    * 4: READY FOR CONSUME - File ready for consume by user; State: active

    * 5: NOT ACTIVE - File inactivated due to some pending issues; State: not active

    * 6: DELETED - File deleted, the psychical file deleted, later if want to reupload, file_status must set to NEW; State: not active

* `file_version` - String; Version of the file, in timestamp, for client side to check if there's new version available. Note: the reason we don't use `modified` property directly for this needs because sometimes we just reupload the same version. This field related to `md5_checksum`, when the version changed, the checksum need to be updated too

* `key` - String; Global encryption key of the file. In JSON representation, this key must be passed in encrypted data

* `md5_checksum` - String; MD5 checksum of the file, use to compare after client side downloaded the file. If null provided, the client side will ignore the check

#### Create Item Files

Create new item files.

**URL:** /item_files

**Method:** POST

**Request Data Params:**

Required: `item_id`, `file_name`, `file_type`, `sort_priority`, `is_active`, `file_status`, `key`

Optional: `file_version`, `md5_checksum`

**Response HTTP Code:**

* `207` - Multiple status of each item file

    * `201` -  New item file created

**Response Params:** See [Item File Properties](#item-file-properties)

**Examples:**

Request:


POST /item_files HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "item_files": [
            {
                "item_id": 876,
                "file_name": "/tempo/ed-25-2014/page1.zip",
                "file_type": 3,
                "file_order" : 1,
                "is_active": True,
                "file_status": 1,
                "key": ""
            },
            {
                "item_id": 876,
                "file_name": "/tempo/ed-25-2014/page2.zip",
                "file_type": 3,
                "file_order" : 2,
                "is_active": True,
                "file_status": 1,
                "key": ""
            }
        ]
    }


Success response:


HTTP/1.1 207 Multi-Status
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "results":[
            {
                "status": 201,
                "id": 1233,
                "item_id": 876,
                "file_name": "/tempo/ed-25-2014/page1.zip",
                "file_type": 3,
                "file_order" : 1,
                "is_active": True,
                "file_status": 1,
                "key": "",
                "file_version": null,
                "md5_checksum": null
            },
            {
                "status": 201,
                "id": 1234,
                "item_id": 876,
                "file_name": "/tempo/ed-25-2014/page2.zip",
                "file_type": 3,
                "file_order" : 2,
                "is_active": True,
                "file_status": 1,
                "key": "",
                "file_version": null,
                "md5_checksum": null
            }
        ]
    }


Fail response:


HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "status": 400,
        "error_code": 400101,
        "developer_message": "Bad Request. Missing required parameter",
        "user_message": "Missing required parameter"
    }


#### Retrieve Item Files

Retrieve item files.

**URL:** /item_files

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Item File Properties](#item-file-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

* `item_id` - Filter by item identifier(s). Response will returns if any matched

**Method:** GET

**Response HTTP Code:**

* `200` - Item files retrieved and returned in body content

* `204` - No matched item file

**Response Params:** See [Item File Properties](#item-file-properties)

**Examples:**

Request:


GET /item_files?order=file_order&item_id=223 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Success response:


HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "item_files": [
            {
                "id": 12341,
                "item_id": 432,
                "file_name": "/tempo/ed-45-2014/magazine.zip",
                "file_type": 1,
                "file_order" : 1,
                "is_active": True,
                "file_status": 4,
                "key": "ew@*G94YB>0BEc$bD#VOcVR:751Ama",
                "file_version": "2014-02-13T23:34:34.231Z",
                "md5_checksum": "7815696ecbf1c96e6894b779456d330e"
            }
        ],
        "metadata": {
            "resultset": {
                "count": 1,
                "offset": 0,
                "limit": 9999
            }
        }
    }


Fail response:


HTTP/1.1 500 Internal Server Error


#### Retrieve Individual Item File

Retrieve individual item file using identifier.

**URL:** /item_files/{item_file_id}

**Alternative URL:** /items/{item_id}/files/file_order/{file_order}

Optional:

* `fields` - Retrieve specific properties. See [Item File Properties](#item-file-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Item file found and returned

**Response Params:** See [Item File Properties](#item-file-properties)

**Examples:**

Request:


GET /items/12/files/file_order/3 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Success response:


HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 234,
        "item_id": 12,
        "file_name": "/tempo/ed-23-2014/page3.zip",
        "file_type": 3,
        "file_order" : 3,
        "is_active": True,
        "file_status": 4,
        "key": "3aYq{w62l?pU9,X`xm$p*M#16sl8I7",
        "file_version": "2013-01-23T12:34:34.231Z",
        "md5_checksum": "1a604da008c5b755ad064d9e7c8682c3"
    }


Fail response:


HTTP/1.1 404 Not Found


#### Update Item File

Update item file.

**URL:** /item_files/{item_file_id}

**Method:** PUT

**Request Data Params:**

Required: `item_id`, `file_name`, `file_type`, `sort_priority`, `is_active`, `file_status`, `key`

Optional: `file_version`, `md5_checksum`

**Response HTTP Code:**

* `200` - Item file updated

**Response Params:** See [Item File Properties](#item-file-properties)

**Examples:**

Request:


PUT /item_files/234 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "item_id": 12,
        "file_name": "/tempo/ed-23-2014/page3.zip",
        "file_type": 3,
        "file_order" : 3,
        "is_active": True,
        "file_status": 4,
        "key": "3aYq{w62l?pU9,X`xm$p*M#16sl8I7",
        "file_version": "2014-04-25T11:04:34.221Z",
        "md5_checksum": "12a9b051b58b8925c790ccae3f02941c"
    }



Success response:


HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 234,
        "item_id": 12,
        "file_name": "/tempo/ed-23-2014/page3.zip",
        "file_type": 3,
        "file_order" : 3,
        "is_active": True,
        "file_status": 4,
        "key": "3aYq{w62l?pU9,X`xm$p*M#16sl8I7",
        "file_version": "2014-04-25T11:04:34.221Z",
        "md5_checksum": "12a9b051b58b8925c790ccae3f02941c"
    }



Fail response:


HTTP/1.1 404 Not Found


#### Delete Item File

Delete item file. This service will set `is_active` value to false.

**URL:** /item_files/{item_file_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Item file deleted

**Response Params:** -

**Examples:**

Request:


DELETE /item_files/234 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Success response:


HTTP/1.1 204 No Content


Fail response:


HTTP/1.1 404 Not Found


### Item Types

Item type resource used to identify system's supported item types.

Current existing item types are:

1. Magazine: digital magazines

2. Book: digital books

3. Newspaper: digital newspapers

Note: New item type mostly will has specific use-cases come along with it. So this resource records only for metadata only, the use-cases implementation will be vary.

#### Item Type Properties

* `id` - Number; Instance identifier

* `name` - String; Item type name

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `slug` - String, Unique; Item type slug for SEO

* `meta` - String; Item type meta for SEO

* `description` - String; Item type description

**Note:**

**Each item type has it own unique use cases:**

**1. Different properties, e.g., book type got author, magazine type not**

**2. Different offers, e.g., newspaper type has no single type of offer**

**In worst case, if need to switch the item type, please consult with the developer team.**

#### Create Item Type

Create new item type.

**URL:** /item_types

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `201` - New item type created

**Response Params:** See [Item Type Properties](#item-type-properties)

**Examples:**

Request:


POST /item_types HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "Magazine",
        "sort_priority" : 1,
        "is_active": True,
        "slug": "magazine",
        "meta": "Digital magazine type",
        "description": "Digital magazine type"
    }


Success response:


HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/item_types/1

.. code-block:: json

    {
        "id": 1
        "name": "Magazine",
        "sort_priority": 1,
        "is_active": True,
        "slug": "magazine",
        "meta": "Digital magazine type",
        "description": "Digital magazine type"
    }


Fail response:


HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "status": 409,
        "error_code": 409100,
        "developer_message": "Conflict. Duplicate value on unique parameter",
        "user_message": "Duplicate value on unique parameter"
    }


Retrieve Item Types
-------------------

Retrieve item types.

**URL:** /item_types

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched
* `fields` - Retrieve specific fields. See [Item Type Properties](#item-type-properties). Default to ALL
* `offset` - Offset of the records. Default to 0
* `limit` - Limit of the records. Default to 9999
* `order` - Order the records. Default to order ascending by primary identifier or created timestamp
* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Item types retrieved and returned in body content

* `204` - No matched item type

**Response Params:** See [Item Type Properties](#item-type-properties)

**Examples:**

Request:


GET /item_types?fields=id,name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Success response:


HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "item_types": [
            {
                "id": 1,
                "name": "Magazine"
            },
            {
                "id": 2,
                "name": "Book"
            },
            {
                "id": 3,
                "name": "Newspaper"
            }
        ],
        "metadata": {
            "resultset": {
                "count": 3,
                "offset": 0,
                "limit": 3
            }
        }
    }


Fail response:


HTTP/1.1 500 Internal Server Error


#### Retrieve Individual Item Type

Retrieve individual item type using identifier.

**URL:** /item_types/{item_type_id}

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific properties. See [Item Type Properties](#item-type-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Item type found and returned

**Response Params:** See [Item Type Properties](#item-type-properties)

**Examples:**

Request:


GET /item_types/1?fields=id,name,sort_priority,slug,meta,description,is_active HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Success response:


HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 1,
        "name": "Magazine",
        "sort_priority": 1,
        "slug": "magazine",
        "meta": "Digital magazine type",
        "description": "Digital magazine type",
        "is_active": true
    }


Fail response:


HTTP/1.1 404 Not Found


#### Update Item Type

Update item type.

**URL:** /item_types/{item_type_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `200` - Item type updated

**Response Params:** See [Item Type Properties](#item-type-properties)

**Examples:**

Request:


PUT /item_types/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "Magz",
        "sort_priority": 2,
        "slug": "magazine",
        "meta": "Digital magazine type",
        "description": "Digital magazine type",
        "is_active": true
    }


Success response:


HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 1,
        "name": "Magz",
        "sort_priority": 2,
        "slug": "magazine",
        "meta": "Digital magazine type",
        "description": "Digital magazine type",
        "is_active": true
    }


Fail response:


HTTP/1.1 404 Not Found


#### Delete Item Type

Delete item type. This service will set `is_active` value to false.

**URL:** /item_types/{item_type_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Item type deleted

**Response Params:** -

**Examples:**

Request:


DELETE /item_types/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


Success response:


HTTP/1.1 204 No Content


Fail response:


HTTP/1.1 404 Not Found
