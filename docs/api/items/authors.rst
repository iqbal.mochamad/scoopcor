Authors
=======

An "author" is broadly defined as "the person who originated or gave existence to anything". Narrowly defined within our context, an Author is the originator of any (digitally) written work and can also be described as a *writer*.


Properties
----------

id
    Number; Automatically-generated instance identifier.

name
		String; Name of the Author

sort_priority
		Number; Sort priority use to defining order. Higher number, more priority

is_active
		Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

slug
		String, Unique; Author slug for SEO

first_name
		String; First name

last_name
		String; Last name

title
		Number; Author's title

academic_title
		String; Author's academic title

birthdate
		String; Author's birthdate

meta
		String; Author meta for SEO

profile_pic_url
		String; url link to author's profile picture

Create
------

Adds new Author

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/authors                                          |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| name                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| sort_priority                     |    YES    | 1                            |
+-----------------------------------+-----------+------------------------------+
| is_active                         |    YES    | true                         |
+-----------------------------------+-----------+------------------------------+
| slug                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| meta                              |           |                              |
+-----------------------------------+-----------+------------------------------+
| first_name                        |           |                              |
+-----------------------------------+-----------+------------------------------+
| last_name                         |           |                              |
+-----------------------------------+-----------+------------------------------+
| title                             |           |                              |
+-----------------------------------+-----------+------------------------------+
| academic_title                    |           |                              |
+-----------------------------------+-----------+------------------------------+
| birthdate                         |           |                              |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /authors HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "Lorem ipsum",
        "is_active": True,
        "sort_priority": 1,
        "first_name": "Lorem ipsum",
        "last_name": "takeda",
        "title": "Mr.",
        "academic_title": "Thunderbird",
        "birthdate": "2014-03-12",
        "profile_pic_url": "http://image.url/pic1.jpg"
    }


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/authors/2

.. code-block: json

    {
        "id": 2,
        "name": "Lorem ipsum",
        "is_active": True,
        "sort_priority": 1,
        "first_name": "Lorem ipsum",
        "last_name": "takeda",
        "title": "Mr.",
        "academic_title": "Thunderbird",
        "birthdate": "2014-03-12",
        "profile_pic_url": "http://image.url/pic1.jpg"
    }


List
----

Retrieves a paginated list of Authors.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/authors                                           |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
| Query String Parameters                                                      |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 | Default   | Description                  |
+===================================+===========+==============================+
| offset                            | 0         | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| limit                             | 20        | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| is_active                         |           | Filter                       |
+-----------------------------------+-----------+------------------------------+
| fields                            | ALL       | Data option                  |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /authors HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

{
    "authors": [
        {
            "id": 1,
            "name": "RL Stained"
        },
        {
            "id": 2,
            "name": "John Greenshame"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 127,
            "offset": 0,
            "limit": 2
        }
    }
}


Detail
------

Retrieves individual Author by the identifier.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/authors/{id}                                      |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /authors/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 231,
        "name": "JK RAWLING",
        "slug": "jk-rawling",
        "sort_priority": 1,
        "is_active": true,
        "first_name": "Jusup Kolak",
        "last_name": "Rawon Keling",
        "title": "Mr.",
        "academic_title": "Bachelor of Writing",
        "birthdate": "1956-03-12",
        "meta": "Top British author",
        "profile_pic_url": "http://image.url/pic1.jpg"
    }


Update
------

Updates the attribute of any single Author.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/authors/{id}                                     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

.. note::

    This requires exactly the same parameters as created.

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /authors/2 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "id": 2,
        "name": "JK CRAWLING",
        "slug": "jk-crawling",
        "sort_priority": 1,
        "is_active": true,
        "profile_pic_url": "http://image.url/pic1new.jpg"
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 2,
        "name": "JK CRAWLING",
        "slug": "jk-crawling",
        "sort_priority": 1,
        "is_active": true
    }


Delete
------

Sets ``is_active`` value to ``false``. The API response will behave as if the resource were actually deleted.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/authors/{id}                                      |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                               |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Record was deleted                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request (for delete, this would  |
|                       |               | typically be incorrect headers)      |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    DELETE /authors/2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 204 No Content
