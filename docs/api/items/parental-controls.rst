Parental Control
================

Some items were family environment friendly, while some others were not. We got it covered, though. All the items were given parental control so y'all can create safe work environment.
Currently, we have 4 stages of parental control:

* 4+
    Items included to this category contain SFW material.
* 9+
    Items included to this category contain mild or infrequent occurences of cartoon, fantasy, or realistic violence; infrequent or mild mature, sugestive, or horror-themed materials which may not be suitable for children under the age of 9.
* 12+
    Items included to this category contain mild or infrequent occurences of cartoon, fantasy, or realistic violence; infrequent or mild mature, sugestive, or horror-themed materials; simulated gambling; which may not be suitable for children under the age of 12.
* 17+
    Items included to this category may also contain frequent and intense offensive language; frequent and intense cartoon, fantasy, or realistic violence; frequent and intense mature, horror, and suggestive themes; plus sexual content, nudity, alcohol, tobacco, and drugs which may not be suitable for children under the age of 17.


Properties
----------

id
    Number; Automatically-generated instance identifier.
name
    String; Name of the parental control
sort_priority
    Number; Used to prioritize the order of the parental control in the list
is_active
    Boolean; Instance's active status, used to mark whether the instance is active or soft-deleted. The default value is true.
slug
    String; The slug of any parental control, used with permalinks as they help describing what the parental control and the URL is.
meta
    String; Metadata of the parental control, usually a short description of a certain category.
description
    String; Detailed information about the parental control.
media_base_url
    String; The Base URL for image files, this string must be appended to image URI and is a read-only property.
icon_image_normal
    String; On server responses, this will be the URL where the normal resolution image of the category is stored.
icon_image_highres
    String; Same with ``icon_image_normal``, but for the high-resolution image.


Create
------

Adds a new parental control.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/parental_controls                                |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| name                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| sort_priority                     |    YES    | 1                            |
+-----------------------------------+-----------+------------------------------+
| is_active                         |    YES    | true                         |
+-----------------------------------+-----------+------------------------------+
| slug                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| meta                              |           |                              |
+-----------------------------------+-----------+------------------------------+
| description                       |           |                              |
+-----------------------------------+-----------+------------------------------+
| media_base_url                    |           | null                         |
+-----------------------------------+-----------+------------------------------+
| icon_image_normal                 |           | null                         |
+-----------------------------------+-----------+------------------------------+
| icon_image_highres                |           | null                         |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /parental_controls HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "12+",
        "slug": "twelve-plus",
        "sort_priority": 1,
        "is_active": true,
        "description": "For age 12 or more",
        "meta": "For age 12 or more",
        "icon_image_normal": "parental_controls/twelve_plus/icon.png",
        "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
    }

Response Example
^^^^^^^^^^^^^^^^

    .. code-block:: http

        HTTP/1.1 201 Created
        Content-Type: application/json; charset=utf-8
        Location: https://scoopadm.apps-foundry.com/scoopcor/api/brands/2

    .. code-block: json

    {
        "id": 2,
        "name": "12+",
        "slug": "twelve-plus",
        "sort_priority": 1,
        "is_active": true,
        "description": "For age 12 or more",
        "meta": "For age 12 or more",
        "icon_image_normal": "parental_controls/twelve_plus/icon.png",
        "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
    }


List
----

Retrieves a paginated list of parental controls.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/parental_controls                                |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
| Query String Parameters                                                      |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 | Default   | Description                  |
+===================================+===========+==============================+
| offset                            | 0         | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| limit                             | 20        | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| q                                 |           | String query                 |
+-----------------------------------+-----------+------------------------------+
| fields                            |           | Retrieves spesific fields    |
+-----------------------------------+-----------+------------------------------+
| expand                            | null      | Expands the result details   |
|                                   |           | supports: ext, vendors       |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET //parental_controls?fields=id,name HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "parental_controls": [
            {
                "id": 1,
                "name": "4+"
            },
            {
                "id": 2,
                "code": "7+"
            },
            {
                "id": 3,
                "name": "12+"
            },
            {
                "id": 4,
                "code": "17+"
            }
        ],
        "metadata": {
            "resultset": {
                "count": 2,
                "offset": 0,
                "limit": 9999
            }
        }
    }


Detail
------

Retrieves individual parental control by the identifier.

+-----------------------+------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/parental_controls/{id}                                     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /parental_controls/3 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 3,
        "name": "12+",
        "slug": "twelve-plus",
        "sort_priority": 1,
        "is_active": true,
        "description": "For age 12 or more",
        "meta": "For age 12 or more",
        "icon_image_normal": "parental_controls/twelve_plus/icon.png",
        "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
    }


Update
------

Updates single parental control data.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/parental_controls/{id}                                  |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

.. note::

    This requires exactly the same parameters as created.

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /parental_controls/1 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "12+",
        "slug": "twelve-plus",
        "sort_priority": 1,
        "is_active": true,
        "description": "For age 12+",
        "meta": "For age 12+",
        "icon_image_normal": "parental_controls/twelve_plus/icon.png",
        "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

{
    "id": 1,
    "name": "12+",
    "slug": "twelve-plus",
    "sort_priority": 1,
    "is_active": true,
    "description": "For age 12+",
    "meta": "For age 12+",
    "icon_image_normal": "parental_controls/twelve_plus/icon.png",
    "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
{


  Delete
  ------

  Sets ``is_active`` value to ``false``. The API response will behave as if the resource were actually deleted.

  +------------------------------------------------------------------------------+
  |                            Endpoint Information                              |
  +=======================+======================================================+
  | Url                   | /v1/parental_controls/{id}                           |
  +-----------------------+------------------------------------------------------+
  | HTTP Method           | DELETE                                               |
  +-----------------------+---------------+--------------------------------------+
  | Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
  +-----------------------+---------------+--------------------------------------+
  | Success Response      | HTTP 204      | Record was deleted                   |
  +-----------------------+---------------+--------------------------------------+
  | Error Response        | HTTP 400      | Bad request (for delete, this would  |
  |                       |               | typically be incorrect headers)      |
  |                       +---------------+--------------------------------------+
  |                       | HTTP 401      | Incorrect permissions or bad auth    |
  |                       |               | token                                |
  |                       +---------------+--------------------------------------+
  |                       | HTTP 404      | {id} does not exist                  |
  +-----------------------+---------------+--------------------------------------+

  Request Example
  ^^^^^^^^^^^^^^^

  .. code-block:: http

      DELETE /parental_controls/2 HTTP/1.1
      Authorization: Bearer ajksfhiau12371894^&*@&^$%

  Response Example
  ^^^^^^^^^^^^^^^^

  .. code-block:: http

      HTTP/1.1 204 No Content
