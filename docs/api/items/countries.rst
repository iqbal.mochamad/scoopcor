Countries
=========

Countries return metadata about countries that can be assigned to
:doc:`Item </web-api-v1/items/items>` resources.

.. warning::

    This resource have been depreciated and exist in a read-only
    to support existing applications that are reliant upon it.


Properties
----------

id
    Number; Instance identifier.

code
    String; Unique; Country code in
    `ISO 3166-1 alpha-2 <http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2>`_ or
    `ISO 3166-1 alpha-3 <http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3>`_.
    This varies based on the record, because they've been entered in both
    formats.  Also note, in this version of the API, not all the data has been
    entered properly, so some country codes are incorrect.

name
    String; Country name.

sort_priority
    Number; Sort priority use to defining order. Higher number, more priority.

is_active
    Boolean; Instance's active status, used to mark if the instance is active
    or soft-deleted. Default to true.

slug
    String, Unique; Country slug for SEO.

meta
    String; Country meta for SEO.

description
    String; Country description.

media_base_url
    String; Base URL for image files, this string must appended to image URI.

icon_image_normal
    String; Country icon image URI.

icon_image_highres
    String; Country icon image in highres URI.


Create
------

.. warning::

    This is no longer supported.


List
----

Retrieve countries.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/countries                                        |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Example Request
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /countries?fields=id,code HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Example Response
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "countries": [
            {
                "id": 1,
                "code": "IDN"
            },
            {
                "id": 2,
                "code": "SGP"
            }
        ],
        "metadata": {
            "resultset": {
                "count": 2,
                "offset": 0,
                "limit": 9999
            }
        }
    }

:download:`Schema <_schemas/countries/list-country-resp.json>`

Detail
------

Retrieve individual country using system internal identifier.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/countries/{id}                                   |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Example Request
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /countries/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Example Response
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "code": "ID",
        "description": "",
        "icon_image_highres": "",
        "icon_image_normal": "images/1/country_icons/ID_2.png",
        "id": 1,
        "is_active": true,
        "media_base_url": "http://images.getscoop.com/magazine_static/",
        "meta": "",
        "name": "Indonesia",
        "slug": "indonesia",
        "sort_priority": 1
    }

:download:`Schema <_schemas/countries/detail-country-resp.json>`


Update
------

.. warning::

    This is no longer supported.


Delete
------

.. warning::

    This is no longer supported.
