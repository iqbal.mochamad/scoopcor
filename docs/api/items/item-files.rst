Item Files
==========

Item file resource used to get which files are linked to an item. This resource should only used by the server side, for client side to get file for download, see [Downloads](#downloads).

If there's new kind of type supported (e.g., support to open HTML file), along with logic implementation, file type must be added.


Properties
----------

id
		Number; Instance identifier

item_id
		Number; Item's identifier which this file linked to

file_name
		String; File name

file_type
		Number; File type

    * 1: PDF Bulk ZIP Encrypted, PDF document in a single file, one item one file
    * 2: ePub Bulk ZIP Encrypted, PDF file in a bulk, one item one file
    * 3: Image File RAW Encrypted, single file of image, mostly for alternative image version or ads

file_order
		Number; Order number defining file order starts from 1, ascending. For single file item, the value will be set to 1

is_active
		Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

file_status
		Number; File status. Default to NEW

    * 1: NEW - New added file meta, for upload later; State: not active
    * 2: UPLOADING - File uploading. for later use if the system supports pause-and-resume in file upload progress; State: not active
    * 3: UPLOADED - File uploaded to temporarily location, ready for process; State: not active
    * 4: PROCESSING - Processing the file; State: not active
    * 5: READY - File ready for consume by user; State: active
    * 6: NOT ACTIVE - File inactivated due to some pending issues; State: not active
    * 7: DELETED - File deleted, the psychical file deleted, later if want to reupload, file_status must set to NEW; State: not active

file_version
		String; Version of the file, in timestamp, for client side to check if there's new version available. Note: the reason we don't use `modified` property directly for this needs because sometimes we just reupload the same version. This field related to `md5_checksum`, when the version changed, the checksum need to be updated too

key
		String; Global encryption key of the file. In JSON representation, this key must be passed in encrypted data

md5_checksum
		String; MD5 checksum of the file, use to compare after client side downloaded the file. If null provided, the client side will ignore the check


Create
------

Create new item files.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_files                                          |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| item_id                           |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| file_name		                      |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| file_type                         |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| file_order                        |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| is_active                         |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| file_status                       |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| file_version                      |           |                              |
+-----------------------------------+-----------+------------------------------+
| key                               |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| md5_checksum                      |           |                              |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /item_files HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

		{
		    "item_id": 876,
		    "file_name": "/tempo/ed-25-2014/page2.zip",
		    "file_type": 3,
		    "file_order" : 2,
		    "is_active": True,
		    "file_status": 1,
		    "key": ""
		}

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/item_files/2

.. code-block: json

		{
				"id": 2,
				"item_id": 876,
				"file_name": "/tempo/ed-25-2014/page2.zip",
				"file_type": 3,
				"file_order" : 2,
				"is_active": True,
				"file_status": 1,
				"key": ""
		}


List
----

Retrieves a paginated list of Item Files.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_files                                       |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
| Query String Parameters                                                      |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 | Default   | Description                  |
+===================================+===========+==============================+
| offset                            | 0         | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| limit                             | 20        | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| is_active                         |           | Filter                       |
+-----------------------------------+-----------+------------------------------+
| fields                            | ALL       | Data option                  |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /item_files HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

	{
		"item_files": [
				{
					"id": 2,
					"item_id": 432,
					"file_name": "/tempo/ed-45-2014/magazine.zip",
					"file_type": 1,
					"file_order" : 1,
					"is_active": True,
					"file_status": 4,
					"key": "ew@*G94YB>0BEc$bD#VOcVR:751Ama",
					"file_version": "2014-02-13T23:34:34.231Z",
					"md5_checksum": "7815696ecbf1c96e6894b779456d330e"
				}
			],
				"metadata": {
		      "resultset": {
		          "count": 127,
		          "offset": 0,
		          "limit": 1
	        }
	    }
	}


Detail
------

Retrieves individual Item File using identifier.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_files/{id}                                  |
+-----------------------+------------------------------------------------------+
| Alternative Url       | /v1/items/{item_id}/files/file_order/{file_order}    |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /item_files/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
			"id": 1,
			"item_id": 12,
			"file_name": "/tempo/ed-23-2014/page3.zip",
			"file_type": 3,
			"file_order" : 3,
			"is_active": True,
			"file_status": 4,
			"key": "3aYq{w62l?pU9,X`xm$p*M#16sl8I7",
			"file_version": "2013-01-23T12:34:34.231Z",
			"md5_checksum": "1a604da008c5b755ad064d9e7c8682c3"
    }


Update
------

Updates the attribute of any single Item File.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_files/{id}                                     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

.. note::

    This requires exactly the same parameters as created.

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /item_files/2 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
			"id": 2,
			"item_id": 12,
			"file_name": "/tempo/ed-23-2014/page3.zip",
			"file_type": 3,
			"file_order" : 3,
			"is_active": True,
			"file_status": 4,
			"key": "3aYq{w62l?pU9,X`xm$p*M#16sl8I7",
			"file_version": "2013-01-23T12:34:34.231Z",
			"md5_checksum": "1a604da008c5b755ad064d9e7c8682c3"
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
			"id": 1,
			"item_id": 12,
			"file_name": "/tempo/ed-23-2014/page3.zip",
			"file_type": 3,
			"file_order" : 3,
			"is_active": True,
			"file_status": 4,
			"key": "3aYq{w62l?pU9,X`xm$p*M#16sl8I7",
			"file_version": "2013-01-23T12:34:34.231Z",
			"md5_checksum": "1a604da008c5b755ad064d9e7c8682c3"
    }


Delete
------

Sets ``is_active`` value to ``false``. The API response will behave as if the resource were actually deleted.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_files/{id}                                      |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                               |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Record was deleted                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request (for delete, this would  |
|                       |               | typically be incorrect headers)      |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    DELETE /item_files/2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 204 No Content
