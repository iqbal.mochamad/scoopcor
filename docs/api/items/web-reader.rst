Web Reader
==========

The web reader API supports fetching PDF-based content and returning it
page-by-page as a series of individual images (served at 150 DPI, by publisher
request).

.. note::

    This API functions differently than many of our other API endpoints.
    We do not return JSON directly.


Retrieve Page
-------------
Retrieves the contents of an item in PDF format

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/items/<item_id>/web-reader/<page_number>.jpg     |
|                       +------------------------------------------------------+
|                       | /v1/items/<item_id>/thumbnails/<page_number>.png     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Returned a JPEG Image                |
|                       +---------------+--------------------------------------+
|                       | HTTP 100      | The content is available immediately |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 403      | User doesn't own the item            |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | item (PDF) does not exist            |
|                       +---------------+--------------------------------------+
|                       | HTTP 417      | The files are not available...YET    |
|                       +---------------+--------------------------------------+
|                       | HTTP 102      | The files are not available...YET    |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /items/<item_id>/web-reader/<page_number>.jpg HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: image/jpeg
    Expect: 100 (this is optional)


Retrieve Header
---------------
Retrieves the header only, without any entity-body. (kind of similar with ``GET`` but without the "main" data)

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/items/<item_id>/web-reader/<page_number>.jpg     |
|                       +------------------------------------------------------+
|                       | /v1/items/<item_id>/thumbnails/<page_number>.png     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | HEAD                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Returned a JPEG Image                |
|                       +---------------+--------------------------------------+
|                       | HTTP 100      | The content is available immediately |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 403      | User doesn't own the item            |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | item (PDF) does not exist            |
|                       +---------------+--------------------------------------+
|                       | HTTP 417      | The files are not available...YET    |
|                       +---------------+--------------------------------------+
|                       | HTTP 102      | The files are not available...YET    |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    HEAD /items/<item_id>/web-reader/<page_number>.jpg HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: image/jpeg
    Expect: 100 (this is optional)


OPTIONS
-------
Finds out the HTTP methods and other options supported by a web server.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/items/<item_id>/web-reader/<page_number>.jpg     |
|                       +------------------------------------------------------+
|                       | /v1/items/<item_id>/thumbnails/<page_number>.png     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | OPTIONS                                              |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Returned a JPEG Image                |
|                       +---------------+--------------------------------------+
|                       | HTTP 100      | The content is available immediately |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 403      | User doesn't own the item            |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | item (PDF) does not exist            |
|                       +---------------+--------------------------------------+
|                       | HTTP 417      | The files are not available...YET    |
|                       +---------------+--------------------------------------+
|                       | HTTP 102      | The files are not available...YET    |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    OPTIONS /items/<item_id>/web-reader/<page_number>.jpg HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: image/jpeg
    Expect: 100 (this is optional)

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Allow: GET,HEAD,POST,OPTIONS,TRACE
    Content-Type: image/jpeg; charset=utf-8
