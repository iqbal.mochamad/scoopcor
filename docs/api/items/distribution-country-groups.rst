Item Distribution Country Group
===============================

Some items were distributed only to spesific countries, some others were restricted to certain countries, while the rest were distributed worldwide.

Worry not, we hast got it all cover'd. This resource regulates item (publisher-based) distribution to some spesific countries.


Properties
----------

id
    Number; Automatically-generated instance identifier.

name
    String; Name of the distribution group which represents what the group stands for.

group_type
    Number (enum); Type of the group which determines how the distribution is regulated.

    1. RESTRICTED
    2. ALLOWED

countries
    String (list); List of the country codes where the item(s) is distributed, in ISO_3166-2 format.

vendor_id
    Number; Vendor identifier, non-technically known as "publisher". Shows which publisher regulates the distribution.

is_active
    Boolean; Instance's active status, used to mark whether the instance is active or soft-deleted. The default value is true.


Create
------

Makes new distribution regulation of any certain item.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_distribution_country_groups                 |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| name                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| group_type                        |    YES    | null                         |
+-----------------------------------+-----------+------------------------------+
| countries                         |    YES    | null                         |
+-----------------------------------+-----------+------------------------------+
| vendor_id                         |    YES    | 1                            |
+-----------------------------------+-----------+------------------------------+
| is_active                         |    YES    | true                         |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /item_distribution_country_groups HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "Harper collins restricted group",
        "group_type": 1,
        "countries": ["US","CA"],
        "vendor_id": 167,
        "is_active": true
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/item_distribution_country_groups/2

.. code-block: json

    {
        "id": 2,
        "name": "Harper collins restricted group",
        "group_type": 1,
        "countries": ["US","CA"],
        "vendor_id": 167,
        "is_active": true
    }


List
----

Retrieves all of the item distribution country groups.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_distribution_country_groups                 |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
| Query String Parameters                                                      |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 | Default   | Description                  |
+===================================+===========+==============================+
| offset                            | 0         | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| limit                             | 20        | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| q                                 |           | String query                 |
+-----------------------------------+-----------+------------------------------+
| fields                            |           | Retrieves spesific fields    |
+-----------------------------------+-----------+------------------------------+
| expand                            | null      | Expands the result details   |
|                                   |           | supports: ext, vendors       |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /item_distribution_country_groups HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "item_distribution_country_groups": [
            {
                "id": 3,
                "name": "Gramedia restricted group",
                "group_type": 1,
                "countries": ["MY","SG"],
                "vendor_id": 2,
                "is_active": true
            },
            {
                "id": 7,
                "name": "Some random restricted group",
                "group_type": 1,
                "countries": ["UK","CA"],
                "vendor_id": 2,
                "is_active": true
            }
        ],
        "metadata": {
            "resultset": {
                "count": 2,
                "offset": 0,
                "limit": 10
            }
        }
    }


Detail
------

Retrieves detailed, individual group by using identifier.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_distribution_country_groups/{id}            |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /item_distribution_country_groups/2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 2,
        "name": "Something distribution group",
        "group_type": 1,
        "countries": ["ID","SG", "MY"],
        "vendor_id": 2,
        "is_active": true
    }


Update
------

Updates Item distribution country group data.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_distribution_country_groups/{id}            |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

.. note::

    This requires exactly the same parameters as created.

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /item_distribution_country_groups/2 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

  {
      "name": "Harper collins restricted group",
      "group_type": 1,
      "countries": ["US","CA"],
      "vendor_id": 167,
      "is_active": true
  }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

  {
      "id": 2,
      "name": "Harper collins restricted group",
      "group_type": 1,
      "countries": ["US","CA"],
      "vendor_id": 167,
      "is_active": true
  }


Delete
------

Sets ``is_active`` value to ``false``. The API response will behave as if the resource were actually deleted.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_distribution_country_groups/{id}            |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    DELETE /item_distribution_country_groups/2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 204 No Content
