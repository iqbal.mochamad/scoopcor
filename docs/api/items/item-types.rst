Item Types
==========

Item type resource used to identify system's supported item types.

Current existing item types are:

* 1. Magazine: digital magazines

* 2. Book: digital books

* 3. Newspaper: digital newspapers

Note: New item type mostly will has specific use-cases come along with it. So this resource records only for metadata only, the use-cases implementation will be vary.


Properties
----------

id
		Number; Instance identifier

name
		String; Item type name

sort_priority
		Number; Sort priority use to defining order. Higher number, more priority

is_active
		Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

slug
		String, Unique; Item type slug for SEO

meta
		String; Item type meta for SEO

description
		String; Item type description

**Note:**

**Each Item Type has it own unique use cases:**

**1. Different properties, e.g., book type got author, magazine type not**

**2. Different offers, e.g., newspaper type has no single type of offer**

**In worst case, if need to switch the item type, please consult with the developer team.**


Create
------

Adds new item type

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_types                                       |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| name                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| sort_priority                     |    YES    | 1                            |
+-----------------------------------+-----------+------------------------------+
| is_active                         |    YES    | true                         |
+-----------------------------------+-----------+------------------------------+
| slug                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| meta                              |           |                              |
+-----------------------------------+-----------+------------------------------+
| description                       |           |                              |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /item_types HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

		{
        "name": "Magazine",
        "sort_priority" : 1,
        "is_active": True,
        "slug": "magazine",
        "meta": "Digital magazine type",
        "description": "Digital magazine type"
		}

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/item_types/2

.. code-block: json

		{
        "id": 2,
        "name": "Magazine",
        "sort_priority" : 1,
        "is_active": True,
        "slug": "magazine",
        "meta": "Digital magazine type",
        "description": "Digital magazine type"
		}


List
----

Retrieves paginated list of Item Types.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/iten_types                                       |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
| Query String Parameters                                                      |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 | Default   | Description                  |
+===================================+===========+==============================+
| offset                            | 0         | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| limit                             | 20        | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| is_active                         |           | Filter                       |
+-----------------------------------+-----------+------------------------------+
| fields                            | ALL       | Data option                  |
+-----------------------------------+-----------+------------------------------+

equest Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /item_types?fields=id,name&offset=0&limit=10 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "item_types": [
            {
                "id": 1,
                "name": "Magazine"
            },
            {
                "id": 2,
                "name": "Book"
            },
            {
                "id": 3,
                "name": "Newspaper"
            }
        ],
        "metadata": {
            "resultset": {
                "count": 3,
                "offset": 0,
                "limit": 3
            }
        }
    }


Detail
------

Retrieve individual Item Type by using identifier.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_types/{id}                                  |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /item_types/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 1,
        "name": "Magazine",
        "sort_priority": 1,
        "slug": "magazine",
        "meta": "Digital magazine type",
        "description": "Digital magazine type",
        "is_active": true
    }


Update
------

Updates the attribute of any single Item Type.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_types/{id}                                  |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

.. note::

    This requires exactly the same parameters as created.

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /item_types/2 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "Magz",
        "sort_priority": 2,
        "slug": "magazine",
        "meta": "Digital magazine type",
        "description": "Digital magazine type",
        "is_active": true
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "name": "Magz",
        "sort_priority": 2,
        "slug": "magazine",
        "meta": "Digital magazine type",
        "description": "Digital magazine type",
        "is_active": true
    }


Delete
------

Sets ``is_active`` value to ``false``. The API response will behave as if the resource were actually deleted.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/item_types/{id}                                  |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                               |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Record was deleted                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request (for delete, this would  |
|                       |               | typically be incorrect headers)      |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    DELETE /item_types/2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 204 No Content
