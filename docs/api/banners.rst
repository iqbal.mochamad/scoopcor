Banners
=======

Banners are special types of images that are displayed on the getscoop.com
and on the mobile applications.

They contain a link to a highres and normal image file, as well as additional
metadata (ex. some banners redirect to a certain webpage when clicked on).


Properties
----------

id
    Number; Automatically-generated instance identifier.
media_base_url
    String; The Base URL for image files, this string must be
    appended to image URI and is a read-only property.
image_highres
    String; On server responses, this will be the URL where the banner
    is located, HOWEVER, on requests to the API, this should be the image
    encoded in base64 format.
image_normal
    String; On server responses, this will be the URL where the banner
    is located, HOWEVER, on requests to the API, this should be the image
    encoded in base64 format, but the non-highres image.
image_iphone
    String; On server responses, this will be the URL where the banner
    is located, HOWEVER, on requests to the API, this should be the image
    encoded in base64 format.
countries
    Array; Countries where the banner is shown, all the country
    codes are listed in ISO 3166-1 alpha-2 format.
banner_type
    Number (enum); Type of the banner that determines the value of payload

    1. LANDING PAGE
    2. OPEN URL
    3. STATIC
    4. BRAND
    5. BUFFET
    6. PROMO

payload
    String; What lies behind the banner. Payload depends on the banner type. e.g:

    * banner_type = 1 : ``campaign_id`` "567"; The banner refers to any campaign which id is 567
    * banner_type = 2 : "http://www.getscoop.com/id/majalah/kategori/teen";
        The banner refers to the aforementioned URL
    * banner_type = 3 : NULL; The banner does nothing, just a plain banner
    * banner_type = 4 : ``brand_id`` "17"; The banner refers to all items which brand id is 17
    * banner_type = 5 : NULL; The banner does nothing, just a plain banner
    * banner_type = 6 : ``campaign_id`` "567"; The banner refers to any campaign which id is 567

valid_from
    String (ISO 8601 DateTime); Datetime that the banner should be displayed.
valid_to
    String (ISO 8601 DateTime); Datetime that the banner should be displayed
    until.
clients
    Number; List of client identifier(s). Used to identify on which
    client(s) the banner will be shown.
zone
    String (enum); Where the banner will be shown, taken from the following

    * homepage (default)
    * magazines page
    * books page
    * newspapers page
    * publisher page
    * promo page

sort_priority
    Number; Used to sort the sequence of the banners.
    The higher the number, the higher the priority.
    The banners are sorted based on the priority.
is_active
    Boolean; Instance's active status, used to mark whether the instance
    is active or soft-deleted. The default value is true.
created
    String (ISO 8601 DateTime); Date/Time this banner was created in Scoopcor.
modified
    String (ISO 8601 DateTime); Date/Time this banner was created in Scoopcor.
description
    String; Detailed explanation about the banner
name
    String; Actually not the name of the banner.  It's used
    with GETSCOOP to store it's data for the 'alt' image tag.  It's
    called 'name' at the request of front-end to keep consistency
    with apparently other places in /v1/ where we use this convention.


Create
------

Adds a new banner which will be eligible to be displayed on the
scoop webpage/applications.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /banners                                             |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| banner_type                       |   YES     |                              |
+-----------------------------------+-----------+------------------------------+
| payload                           |           | null                         |
+-----------------------------------+-----------+------------------------------+
| name                              |           | null                         |
+-----------------------------------+-----------+------------------------------+
| description                       |           | null                         |
+-----------------------------------+-----------+------------------------------+
| valid_from                        |           | null                         |
+-----------------------------------+-----------+------------------------------+
| valid_to                          |           | null                         |
+-----------------------------------+-----------+------------------------------+
| is_active                         |           | true                         |
+-----------------------------------+-----------+------------------------------+
| sort_priority                     |           | 0                            |
+-----------------------------------+-----------+------------------------------+
| clients                           |           | []                           |
+-----------------------------------+-----------+------------------------------+
| countries                         |           | []                           |
+-----------------------------------+-----------+------------------------------+
| image_highres                     |   YES     |                              |
+-----------------------------------+-----------+------------------------------+
| image_normal                      |   YES     |                              |
+-----------------------------------+-----------+------------------------------+
| image_iphone                      |   YES     |                              |
+-----------------------------------+-----------+------------------------------+
| zone                              |   YES     |                              |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /banners HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

    {
        "banner_type": 1,
        "payload": "567",
        "valid_from": "2014-12-01T00:00:00",
        "valid_to": "2015-01-01T16:59:59",
        "is_active": true,
        "sort_priority": 1,
        "countries": ["NZ", "UK"],
        "clients": [1,2,3],
        "zone": "homepage",
        "image_highres": "iqwioq43oy834y5kj2423n4jk2i42i34g2j3g4j2f34yf23jh42.......",
        "image_normal": "iqwioq43oy834y5kj2423n4jk2i42i34g2j3g4j2f34yf23jh42.......",
        "image_iphone": "iqwioq43oy834y5kj2423n4jk2i42i34g2j3g4j2f34yf23jh42.......",
        "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
    }

:download:`Schema <schemas/banners/create-banner-req.json>`

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/banner/2

    {
        "banner_type" : 1,
        "payload" : "567",
        "valid_from" : "2014-12-01T17:00:00",
        "valid_to" : "2015-01-01T16:59:59",
        "is_active": true,
        "sort_priority": 1,
        "countries" : ["NZ", "UK"],
        "clients": [1,2,3],
        "zone": "homepage",
        "created": "2014-12-01T05:12:12",
        "modified": "2014-12-01T05:12:12",
        "id": 2,
        "image_highres": "../images/xxx_hres.png",
        "image_normal": "../images/xxx.png",
        "image_iphone": "../images/xxxiPhone.png",
        "media_base_url": "http://images.getscoop.com/magazine_static/",
        "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
    }

:download:`Schema <_schemas/banners/create-banner-resp.json>`


List
----

Retrieves a paginated list of Banners.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /banners                                             |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
| Query String Parameters                                                      |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 | Default   | Description                  |
+===================================+===========+==============================+
| offset                            | 0         | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| limit                             | 20        | Pagination option            |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /banners?is_active=true&client_id=1,2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

    {
        "banners": [
            {
                "banner_type": 1,
                "payload": "567",
                "valid_from": "2014-12-01T17:00:00",
                "valid_to": "2015-01-01T16:59:59",
                "is_active": true,
                "sort_priority": 1,
                "countries": ["NZ", "UK"],
                "clients": [1,2],
                "zone": "homepage",
                "created": "2014-12-01T05:12:12",
                "modified": "2014-12-01T05:12:12",
                "id": 3,
                "image_highres": "../images/xxx_hres.png",
                "image_normal": "../images/xxx.png",
                "image_iphone": "../images/xxxiPhone.png",
                "media_base_url": "http://images.getscoop.com/magazine_static/",
                "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
            },
            {
                "banner_type": 2,
                "payload": "http://www.getscoop.com/id/majalah/kategori/teen",
                "valid_from": "2014-12-01T17:00:00",
                "valid_to": "2015-01-01T16:59:59",
                "is_active": true,
                "sort_priority": 1,
                "countries": ["ID", "MY"],
                "clients": [1],
                "zone": "homepage",
                "created": "2014-12-01T05:12:12",
                "modified": "2014-12-01T05:12:12",
                "id": 4,
                "image_highres": "../images/xcx_hres.png",
                "image_normal": "../images/xcx.png",
                "image_iphone": "../images/xxxiPhone.png",
                "media_base_url": "http://images.getscoop.com/magazine_static/",
                "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
            }
        ],
        "metadata": {
            "resultset": {
                "count": 2,
                "offset": 0,
                "limit": 10
            }
        }
    }

:download:`Schema <_schemas/banners/list-banner-resp.json>`


Detail
------

Retrieves individual banner by using identifier.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /banners/{id}                                        |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /banner/2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

    {
        "banner_type": 1,
        "payload": "567",
        "valid_from": "2014-12-01T17:00:00",
        "valid_to": "2015-01-01T16:59:59",
        "is_active": true,
        "sort_priority": 1,
        "countries": ["NZ", "UK"],
        "clients": [1,2,3],
        "zone": "homepage",
        "created": "2014-12-01T05:12:12",
        "modified": "2014-12-01T05:12:12",
        "id": 2,
        "image_highres": "../images/xxx_hres.png",
        "image_normal": "../images/xxx.png",
        "image_iphone": "../images/xxxiPhone.png",
        "media_base_url": "http://images.getscoop.com/magazine_static/",
        "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
    }

:download:`Schema <_schemas/banners/detail-banner-resp.json>`


Update
------

Updates the attributes of a single banner, identified in the URL.

.. warning::

    Although this uses the standard update verb of PUT,
    if you're familiar with HTTP, this endpoint performs
    more like a PATCH.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /banners/{id}                                        |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Record was updated                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers.                 |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

.. note::

    This requires exactly the same parameters as create, EXCEPT that
    image_highres and image_normal are both now OPTIONAL.  They will
    not be updated unless provided.

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /banners/2 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

    {
        "banner_type": 1,
        "payload": "567",
        "valid_from": "2014-12-01T17:00:00",
        "valid_to": "2015-01-01T16:59:59",
        "is_active": true,
        "sort_priority": 1,
        "countries": ["NZ", "UK"],
        "clients": [1,2,3],
        "zone": "homepage",
        "created": "2014-12-01T05:12:12",
        "modified": "2014-12-01T05:12:12",
        "id": 2,
        "image_highres": "../images/xxx_hres.png",
        "image_normal": "../images/xxx.png",
        "image_iphone": "../images/xxxiPhone.png",
        "media_base_url": "http://images.getscoop.com/magazine_static/",
        "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
    }

:download:`Schema <_schemas/banners/update-banner-req.json>`

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

    {
        "banner_type": 1,
        "payload": "567",
        "valid_from": "2014-12-01T17:00:00",
        "valid_to": "2015-01-01T16:59:59",
        "is_active": true,
        "sort_priority": 1,
        "countries": ["NZ", "UK"],
        "clients": [1,2,3],
        "zone": "homepage",
        "created": "2014-12-01T05:12:12",
        "modified": "2014-12-01T05:12:12",
        "id": 2,
        "image_highres": "../images/xxx_hres.png",
        "image_normal": "../images/xxx.png",
        "image_iphone": "../images/xxxiPhone.png",
        "media_base_url": "http://images.getscoop.com/magazine_static/",
        "description": "Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aenean sit amet tempor orci"
    }

:download:`Schema <_schemas/banners/update-banner-resp.json>`


Delete
------

Soft-deletes the banner specified in the URL, although the API response will
behave as if the resource were actually deleted.  In reality, this endpoint
will simply set the is_active property of the banner to false.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /banners/{id}                                        |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                               |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Record was deleted                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request (for delete, this would  |
|                       |               | typically be incorrect headers)      |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    DELETE /banners/2 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 204 No Content
