Offers
======

Offers represent the prices and ways in which an Item may be sold in the
Scoop News Stand.  It's important to understand that Items do not have a
single, fixed selling price.  Instead, to handle the many different ways
in which Items can be purchased, Item purchasing happens in terms of 'offers'.


Offer Types
-----------

**Single**
    Single offers are pretty much what they sound like -- an offer where the
    user may purchase a single book, magazine issue, or newspaper edition.
**Subscription**
    A subscription allows the user to access the latest issue of a magazine or
    newspaper, and then to receive new editions from that brand during a set
    period of time.  One the user's subscription expires, they retain all items
    that were acquired during the subscription, but do not have access to any
    of the new issues.
**Bundle**
    A user can acquire a collection of books, magazines, or newspapers that are
    sold together with a single selling price.
**Buffet**
    A user can access a set of book, magazines, or newspapers that are part of
    the Buffet for a limited amount of time.  Once the user's buffet expires,
    they will no longer have access to any of the items that were acquired as
    part of their buffet.


Offer Properties
----------------

id
    Number; Instance identifier
name
    String; Offer name See [Offer Name Format](#offer-name-format)
sort_priority
    Number; Sort priority use to defining order. Higher number, more priority
is_active
    Boolean; Instance's active status, used to mark if the instance
    is active or soft-deleted. Default to true
description
    String; Offer description
offer_type_id
    Number; Offer type identifier. Refer to [Offer Types](#offer-types). Unchangeable.
offer_status
    Number; Offer status

    1 NEW - New created offer; State: not active
    2 WAITING FOR REVIEW - Offer waiting for reviewer's review.
        This status used to shows to vendor's users; State: not active
    3 IN REVIEW - Offer in review by reviewer.
        This status used to shows to vendor's users; State: not active
    4 REJECTED - Offer in rejected by reviewer.
        This status used to shows to vendor's users; State: not active
    5 APPROVED - Offer in approved by reviewer.
        This status used to shows to vendor's users; State: not active
    6 PREPARE FOR SALE - Offer preparing to store.
        This status used to shows to vendor's users; State: not active
    7 READY FOR SALE - Offer ready for sale on store; State: active
    8 NOT FOR SALE - Offer taken down from store. All purchased offer
        will still valid, but all auto-renewal by internal system will be
        invalidated (For products that must removed from payment gateway
        side, please refer to each payment gateway's details);
        State: not active

quantity
    Number; subcribed quantity, for unit-based subcription type.
    For unit-based with multiple brands, this quantity will
    applied to each brands.
quantity_unit
    Number; Offer quantity unit type for subcription,
    related to `quantity` property

    * 1: UNIT - For unit-based subscription, the total unit will be stored in user's owned subscription. Most of the magazines use this type of unit
    * 2: DAY - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. This unit rarely used
    * 3: WEEK - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. This unit rarely used
    * 4: MONTH - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. Most of the newspaper use this type of unit

is_free
    Boolean; Flag for free offer. If true, other prices information will
    be overriden. Default to false
point_reward
    Number; Point rewarded to user (acquired by user) if purchased this offer
items
    Array; Array of items related to this offer, used for single or
    bundle type. Single type offer will has single item,
    bundle type will has list of items
brands
    Array; Array of brands related to this offer, used for subscription
    type. For current supported business case, the subscription only has
    single brand to deliver
exclusive_clients
    Array; Array of clients exclusively offered this offer
offer_code
    String; Offer code, for single type corresponding to edition_code,
    for subscription/bundle type corresponding to
    bundle_subscription_detail_code of previous system
    **TODO: will be dropped after translation layer dropped.**
    **should not be used in new apps. correspondent to edition_code,**
    **bundle_subscription_detail_code**
price_usd
    String; Float value for base price in USD. Must be set to 0 for free offers
price_idr
    String; Float value for base price in IDR. Must be set to 0 for free offers
price_point
    String; Float value for base price in PTS (SCOOP Point).
    Must be set to 0 for free offers
aggr_price_usd
    String; Float value for aggregator price in USD.
    Must be greater than `base_usd_price`
aggr_price_idr
    String; Float value for aggregator price in IDR.
    Must be greater than `base_idr_price`
aggr_price_point
    String; Float value for aggregator price in PTS
    (SCOOP Point). Must be greater than `price_point`
vendor_price_usd
    String; Float value for retail price in USD.
    Must be set to 0 for free offers
vendor_price_idr
    String; Float value for retail price in IDR.
    Must be set to 0 for free offers
vendor_price_point
    String; Float value for retail price in PTS (SCOOP Point).
    Must be set to 0 for free offers
discount_usd
    String; Float value for base discount in USD.
    Default to null if no discount, this value is auto-generated
    by the system when there's valid discount related discount
discount_idr
    String; Float value for base discount in IDR.
    Default to null if no discount, this value is auto-generated by
    the system when there's valid discount related discount
discount_point
    String; Float value for base discount in PTS (SCOOP Point).
    Default to null if no discount, this value is auto-generated by
    the system when there's valid discount related discount
discount_tag
    String; Discount short tag. Default to null if no discount,
    this value is auto-generated by the system when there's
    valid discount related discount
discount_name
    String; Discount name. Default to null if no discount,
    this value is auto-generated by the system when there's
    valid discount related discount
platforms_offers
    Array; List of the override offer for the platforms.
    On specific platform, this object's properties wil overrides
    offer's (base) prices
platform_id
    Number; Platform identifier of this platform specific offer.
    See [Platforms](#platforms)
price_usd
    String; Float value for base price in USD. Must set to 0 for free offer
price_idr
    String; Float value for base price in IDR. Must set to 0 for free offer
price_point
    String; Float value for base price in PTS (SCOOP Point). Must set to 0 for free offer
tier_id
    Number; Tier identifier of the price
tier_code
    String; Tier code of the price
discount_usd
    String; Float value for base discount in USD.
    Default to null if no discount, this value is auto-generated
    by the system when there's valid discount related discount
discount_idr
    String; Float value for base discount in IDR.
    Default to null if no discount, this value is auto-generated by the
    system when there's valid discount related discount
discount_point
    String; Float value for base discount in PTS (SCOOP Point).
    Default to null if no discount, this value is auto-generated by
    the system when there's valid discount related discount
discount_tier_id
    Number; Tier identifier of the price when discount
discount_tier_code
    String; Tier code of the price when discount
discount_tag
    String; Discount short tag. Default to null if no discount,
    this value is auto-generated by the system when there's valid
    discount related discount
discount_name
    String; Discount name. Default to null if no discount,
    this value is auto-generated by the system when there's
    valid discount related discount


Offer Name Format
-----------------

Offer name format is different for each offer type, in general as below:

    1. Single type name: {item_name} + " - " + {info},
        e.g., "Tempo Ed-34 - Single Edition"
    2. Subscription type name: {brand_name} + " - " + {info},
        e.g., "Tempo - 53 Editions / 12 Months"
    3. Bundle type name: custom as the bundle name,
        e.g., "Special Bundle 2013 Autocar Magazines"

.. note::

    * Offer's name will be passed directly as order line's name
    * Client side needs to remove item_name or brand_name, then trim it,
        depends on how the client want to shows it. e.g.,
        "Tempo Ed-34 - Single Edition" shows as "Single Edition" on UI side

.. toctree::

    single
    subscription
    bundle
    buffet







## OFFERS, PLATFORMS, TIERS, CAMPAIGNS, AND DISCOUNTS

### Offers

Offer resource contains base pricings, relation to tier-based pricings (See [iOS Tiers](#offer-properties), [Android Tiers](#android-tiers), [Windows Phone Tiers](#windows-phone-tiers)), and relation to items and brands.

There's 3 types of existing offers:

1. Single

    Single type of offer, this offer only linked to an item. The linked item, and its related extra item(s), will delivered to user when proceed with this offer.

2. Subscription

    Subscription type of offer, linked to the list of brands. If purchased this offer, user will eligible for forward issue of the subscribed brand(s). Latest existing item of the brand will be delivered to user. New latest item of the brand will be delivered to user as long as the subcription still valid based on unit or time.

    Subcription has 2 required properties, quantity and quantity_unit. This properties will define the validity of the owned subscription later.

3. Bundle

    Bundle type of offer, linked to a list of items that will delivered to user when purchased this offer. Note: Further implementation needed to automate the earning and revenue shares calculation.

Note: There's a list to overrides platform specific's offers. This platform's offers can be not available for some platforms, means it didn't sell on that platform, excepts for platform that retrieve from base offers.

#### Offer Properties

* `id` - Number; Instance identifier

* `name` - String; Offer name See [Offer Name Format](#offer-name-format)

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `description` - String; Offer description

* `offer_type_id` - Number; Offer type identifier. Refer to [Offer Types](#offer-types). Unchangeable.

* `offer_status` - Number; Offer status

    * 1: NEW - New created offer; State: not active

    * 2: WAITING FOR REVIEW - Offer waiting for reviewer's review. This status used to shows to vendor's users; State: not active

    * 3: IN REVIEW - Offer in review by reviewer. This status used to shows to vendor's users; State: not active

    * 4: REJECTED - Offer in rejected by reviewer. This status used to shows to vendor's users; State: not active

    * 5: APPROVED - Offer in approved by reviewer. This status used to shows to vendor's users; State: not active

    * 6: PREPARE FOR SALE - Offer preparing to store. This status used to shows to vendor's users; State: not active

    * 7: READY FOR SALE - Offer ready for sale on store; State: active

    * 8: NOT FOR SALE - Offer taken down from store. All purchased offer will still valid, but all auto-renewal by internal system will be invalidated (For products that must removed from payment gateway side, please refer to each payment gateway's details); State: not active

* `quantity` - Number; subcribed quantity, for unit-based subcription type. For unit-based with multiple brands, this quantity will applied to each brands.

* `quantity_unit` - Number; Offer quantity unit type for subcription, related to `quantity` property

    * 1: UNIT - For unit-based subscription, the total unit will be stored in user's owned subscription. Most of the magazines use this type of unit

    * 2: DAY - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. This unit rarely used

    * 3: WEEK - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. This unit rarely used

    * 4: MONTH - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. Most of the newspaper use this type of unit

* `is_free` - Boolean; Flag for free offer. If true, other prices information will be overriden. Default to false

* `point_reward` - Number; Point rewarded to user (acquired by user) if purchased this offer

* `items` - Array; Array of items related to this offer, used for single or bundle type. Single type offer will has single item, bundle type will has list of items

* `brands` - Array; Array of brands related to this offer, used for subscription type. For current supported business case, the subscription only has single brand to deliver

* `exclusive_clients` - Array; Array of clients exclusively offered this offer

* `offer_code` - String; Offer code, for single type corresponding to edition_code, for subscription/bundle type corresponding to bundle_subscription_detail_code of previous system **TODO: will be dropped after translation layer dropped. should not be used in new apps. correspondent to edition_code, bundle_subscription_detail_code**

* `price_usd` - String; Float value for base price in USD. Must be set to 0 for free offers

* `price_idr` - String; Float value for base price in IDR. Must be set to 0 for free offers

* `price_point` - String; Float value for base price in PTS (SCOOP Point). Must be set to 0 for free offers

* `aggr_price_usd` - String; Float value for aggregator price in USD. Must be greater than `base_usd_price`

* `aggr_price_idr` - String; Float value for aggregator price in IDR. Must be greater than `base_idr_price`

* `aggr_price_point` - String; Float value for aggregator price in PTS (SCOOP Point). Must be greater than `price_point`

* `vendor_price_usd` - String; Float value for retail price in USD. Must be set to 0 for free offers

* `vendor_price_idr` - String; Float value for retail price in IDR. Must be set to 0 for free offers

* `vendor_price_point` - String; Float value for retail price in PTS (SCOOP Point). Must be set to 0 for free offers

* `discount_usd` - String; Float value for base discount in USD. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

* `discount_idr` - String; Float value for base discount in IDR. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

* `discount_point` - String; Float value for base discount in PTS (SCOOP Point). Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

* `discount_tag` - String; Discount short tag. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

* `discount_name` - String; Discount name. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

* `platforms_offers` - Array; List of the override offer for the platforms. On specific platform, this object's properties wil overrides offer's (base) prices

    * `platform_id` - Number; Platform identifier of this platform specific offer. See [Platforms](#platforms)

    * `price_usd` - String; Float value for base price in USD. Must set to 0 for free offer

    * `price_idr` - String; Float value for base price in IDR. Must set to 0 for free offer

    * `price_point` - String; Float value for base price in PTS (SCOOP Point). Must set to 0 for free offer

    * `tier_id` - Number; Tier identifier of the price

    * `tier_code` - String; Tier code of the price

    * `discount_usd` - String; Float value for base discount in USD. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

    * `discount_idr` - String; Float value for base discount in IDR. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

    * `discount_point` - String; Float value for base discount in PTS (SCOOP Point). Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

    * `discount_tier_id` - Number; Tier identifier of the price when discount

    * `discount_tier_code` - String; Tier code of the price when discount

    * `discount_tag` - String; Discount short tag. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

    * `discount_name` - String; Discount name. Default to null if no discount, this value is auto-generated by the system when there's valid discount related discount

####Offer Name Format

Offer name format is different for each offer type, in general as below:

1. Single type name: {item_name} + " - " + {info}, e.g., "Tempo Ed-34 - Single Edition"

2. Subscription type name: {brand_name} + " - " + {info}, e.g., "Tempo - 53 Editions / 12 Months"

3. Bundle type name: custom as the bundle name, e.g., "Special Bundle 2013 Autocar Magazines"

Note:

* Offer's name will be passed directly as order line's name

* Client side needs to remove item_name or brand_name, then trim it, depends on how the client want to shows it. e.g., "Tempo Ed-34 - Single Edition" shows as "Single Edition" on UI side

#### Create Offer

Create new offer.

**URL:** /offers

**Method:** POST

Note: Each offer and platform can only has a single `platforms_offers` record

**Request Data Params:**

Required: `offer_type_id`, `offer_status`, `name`, `sort_priority`, `is_active`, `price_usd`, `price_idr`, `price_point`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `exclusive_clients`, `aggr_price_usd`, `aggr_price_idr`, `aggr_price_point`

For single and bundle type, the required params are: `items`

For subscription type, the required params are: `quantity`, `quantity_unit`, `brands`

For `platforms_offers`, the required params are: `platform_id`, `price_usd`, `price_idr`, `price_point`

**Response HTTP Code:**

* `201` - New vendor created

**Response Params:** See [Offer Properties](#offer-properties)

**Examples:**

Request:

```
POST /offers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Single issue",
    "sort_priority": 1,
    "is_active": true,
    "offer_type_id": 1,
    "offer_status": 7,
    "is_free": false,
    "point_reward": 99,
    "items": [
        2345
    ],
    "exclusive_clients": [],
    "offer_code": "ID_TEMP2014ED452",
    "price_usd": "3.20",
    "price_idr": "32000",
    "price_point": "320",
    "aggr_price_usd": "5.99",
    "aggr_price_idr": "59000",
    "aggr_price_point": "590",
    "vendor_price_usd": "3.20",
    "vendor_price_idr": "32000",
    "vendor_price_point": "320",
    "platforms_offers": [
        {
            "platform_id": 1,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 123
        },
        {
            "platform_id": 2,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 124
        },
        {
            "platform_id": 3,
            "price_usd": "3.49",
            "price_idr": "35000",
            "price_point": "320",
            "tier_id": 125
        }
    ]
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/offers/1232

{
    "id": 1232,
    "name": "Single issue",
    "sort_priority": 1,
    "is_active": true,
    "offer_type_id": 1,
    "offer_status": 7,
    "is_free": false,
    "point_reward": 99,
    "items": [
        2345
    ],
    "exclusive_clients": [],
    "offer_code": "ID_TEMP2014ED452",
    "price_usd": "3.20",
    "price_idr": "32000",
    "price_point": "320",
    "aggr_price_usd": "5.99",
    "aggr_price_idr": "59000",
    "aggr_price_point": "590",
    "vendor_price_usd": "3.20",
    "vendor_price_idr": "32000",
    "vendor_price_point": "320",
    "discount_price_usd": null,
    "discount_price_idr": null,
    "discount_price_point": null,
    "discount_tag": null,
    "discount_name": null,
    "platforms_offers": [
        {
            "offer_id": 1232,
            "platform_id": 1,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 123,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        },
        {
            "offer_id": 1232,
            "platform_id": 2,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 124,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        },
        {
            "offer_id": 1232,
            "platform_id": 3,
            "price_usd": "3.49",
            "price_idr": "35000",
            "price_point": "320",
            "tier_id": 125,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        }
    ]
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

#### Retrieve Offers

Retrieve offers.

**URL:** /offers

Optional:

* `offer_id` - Provides offer identifier(s) to be retrieved. Response will only be returned if the ID is available in the system

* `is_active` - Filter by the status. Response will only returns if there is any status that match

* `fields` - Retrieve specific fields. See [Offer Properties](#vendor-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 20

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `offer_type_id` - Provided offer type identifier(s). Response will returns results (if any matched)

* `related_item_id` - Provided item identifier(s) to relates. Response will returns offers related to items

* `related_brand_id` - Provided brand identifier(s) to relates. Response will returns offers related to brands

* `expand` - Expand the result details. Default to null, the supported expand for offers are as below

    * ext: get all related links as object or array of objects contains identifier and name. See [Offer Properties](#offer-properties)

    * offer_type: get related offer_type as object (this expand value is covered in "ext")

    * platforms_offers: get platform's offers as an array (this expand value is covered in "ext")

    * buffet_details: If the offer is for a buffet (offer_type_id = 4), this will include the relevant buffet information (this expand value is covered in "ext")

* `platform_id` - Provided platform identifier(s). Response will returns platform tier information (if any matched). To get this in results, `expand` must contains platforms_offers.

**Method:** GET

**Response HTTP Code:**

* `200` - Offers retrieved and returned in body content

* `204` - No matched offer

**Response Params:** See [Offer Properties](#offer-properties)

**Examples:**

Request:

```
GET /offers?related_item_id=345&related_brand_id=56&platform_id=1,2&offer_id=1,2&is_active=true&expand=ext HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response without expand:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "offers": [
        {
            "id": 1232,
            "name": "Single issue",
            "sort_priority": 1,
            "is_active": true,
            "offer_type_id": 1,
            "items": [
                2345
            ],
            "offer_status": 7,
            "is_free": false,
            "point_reward": 99,
            "exclusive_clients": [],
            "offer_code": "ID_TEMP2014ED452",
            "price_usd": "3.20",
            "price_idr": "32000",
            "price_point": "320",
            "aggr_price_usd": "5.99",
            "aggr_price_idr": "59000",
            "aggr_price_point": "590",
            "vendor_price_usd": "3.20",
            "vendor_price_idr": "32000",
            "vendor_price_point": "320",
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tag": null,
            "discount_name": null,
        },
        {
            "id": 231,
            "name": "3 Months Subscription",
            "sort_priority": 1,
            "is_active": true,
            "offer_type_id": 2,
            "offer_status": 7,
            "quantity": 14,
            "quantity_unit": 1,
            "is_free": false,
            "point_reward": 99,
            "brands": [
                56
            ],
            "exclusive_clients": [],
            "offer_code": "ID_SUBTEMP14ED3MTH",
            "price_usd": "24.99",
            "price_idr": "250000",
            "price_point": "2500",
            "aggr_price_usd": "49.99",
            "aggr_price_idr": "500000",
            "aggr_price_point": "5000",
            "vendor_price_usd": "3.20",
            "vendor_price_idr": "32000",
            "vendor_price_point": "320",
            "discount_price_usd": "5.00",
            "discount_price_idr": "50000",
            "discount_price_point": "50",
            "discount_tag": "25% OFF",
            "discount_name": "Special promo 25% Off",
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 20
        }
    }
}
```

Success response with expand=ext:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "offers": [
        {
            "id": 1232,
            "name": "May buffet",
            "sort_priority": 1,
            "is_active": true,
            "offer_type_id": 4,
            "offer_type": {
                "id": 4,
                "name": "Buffet"
            }
            "offer_status": 7,
            "is_free": false,
            "point_reward": 99,
            "items": [
                {
                    "id": 2345,
                    "name": "Tempo Ed 32"
                }
            ],
            "buffet_details": {
                "id": 66,
                "available_from": "2015-05-01T00:00:00+00:00",
                "available_until": null,
                "buffet_duration": "P30D0S"
            }
            "exclusive_clients": [],
            "offer_code": "ID_TEMP2014ED452",
            "price_usd": "3.20",
            "price_idr": "32000",
            "price_point": "320",
            "aggr_price_usd": "5.99",
            "aggr_price_idr": "59000",
            "aggr_price_point": "590",
            "vendor_price_usd": "3.20",
            "vendor_price_idr": "32000",
            "vendor_price_point": "320",
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tag": null,
            "discount_name": null,
            "platforms_offers": [
                {
                    "offer_id": 1232,
                    "platform_id": 1,
                    "price_usd": "3.99",
                    "price_idr": "39000",
                    "price_point": "320",
                    "tier_id": 123,
                    "tier_code": null,
                    "discount_price_usd": null,
                    "discount_price_idr": null,
                    "discount_price_point": null,
                    "discount_tier_id": null,
                    "discount_tier_code": null,
                    "discount_tag": null,
                    "discount_name": null
                },
                {
                    "offer_id": 1232,
                    "platform_id": 2,
                    "price_usd": "3.99",
                    "price_idr": "39000",
                    "price_point": "320",
                    "tier_id": 124,
                    "tier_code": null,
                    "discount_price_usd": null,
                    "discount_price_idr": null,
                    "discount_price_point": null,
                    "discount_tier_id": null,
                    "discount_tier_code": null,
                    "discount_tag": null,
                    "discount_name": null
                }
            ]
        },
        {
            "id": 231,
            "name": "3 Months Subscription",
            "sort_priority": 1,
            "is_active": true,
            "offer_type_id": 2,
            "offer_type": {
                "id": 1,
                "name": "Subscription"
            }
            "offer_status": 7,
            "quantity": 14,
            "quantity_unit": 1,
            "is_free": false,
            "point_reward": 99,
            "brands": [
                {
                    "id": 56,
                    "name": "Tempo"
                }
            ],
            "exclusive_clients": [],
            "offer_code": "ID_SUBTEMP14ED3MTH",
            "price_usd": "24.99",
            "price_idr": "250000",
            "price_point": "2500",
            "aggr_price_usd": "49.99",
            "aggr_price_idr": "500000",
            "aggr_price_point": "5000",
            "vendor_price_usd": "3.20",
            "vendor_price_idr": "32000",
            "vendor_price_point": "320",
            "discount_price_usd": "5.00",
            "discount_price_idr": "50000",
            "discount_price_point": "50",
            "discount_tag": "25% OFF",
            "discount_name": "Special promo 25% Off",
            "platforms_offers": [
                {
                    "offer_id": 1232,
                    "platform_id": 1,
                    "price_usd": "3.99",
                    "price_idr": "39000",
                    "price_point": "320",
                    "tier_id": 123,
                    "tier_code": null,
                    "discount_price_usd": "2.99",
                    "discount_price_idr": "29000",
                    "discount_price_point": "290",
                    "discount_tag": "25% OFF",
                    "discount_name": "Special promo 25% Off",
                    "discount_tier_id": null,
                    "discount_tier_code": null
                },
                {
                    "offer_id": 1232,
                    "platform_id": 2,
                    "price_usd": "3.99",
                    "price_idr": "39000",
                    "price_point": "320",
                    "tier_id": 124,
                    "tier_code": null,
                    "discount_price_usd": "2.99",
                    "discount_price_idr": "29000",
                    "discount_price_point": "290",
                    "discount_tag": "25% OFF",
                    "discount_name": "Special promo 25% Off",
                    "discount_tier_id": null,
                    "discount_tier_code": null
                }
            ]
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 20
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Offer

Retrieve individual offer using identifier.

**URL:** /offers/{offer_id}

Optional:

* `fields` - Retrieve specific properties. See [Offer Properties](#offer-properties). Default to ALL

* `platform_id` - Provided platform identifier(s). Response will returns platform tier information (if any matched)

**Method:** GET

**Response HTTP Code:**

* `200` - Offer found and returned

**Response Params:** See [Offer Properties](#offer-properties)

**Examples:**

Request:

```
GET /offers/5?platform_id=1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 5,
    "name": "Travelounge 2013 Bundle",
    "sort_priority": 1,
    "is_active": true,
    "offer_type_id": 3,
    "offer_status": 7,
    "is_free": false,
    "point_reward": 59,
    "items": [
        {
            "id": 345,
            "name": "Travelounge Jan 2013"
        },
        {
            "id": 457,
            "name": "Travelounge Apr 2013"
        },
        {
            "id": 521,
            "name": "Travelounge Jul 2013"
        },
        {
            "id": 631,
            "name": "Travelounge Oct 2013"
        }
    ],
    "exclusive_clients": [],
    "offer_code": "ID_TRVLNG2013BUNDL",
    "price_usd": "4.99",
    "price_idr": "49000",
    "price_point": "490",
    "aggr_price_usd": "9.99",
    "aggr_price_idr": "99000",
    "aggr_price_point": "990",
    "vendor_price_usd": "3.20",
    "vendor_price_idr": "32000",
    "vendor_price_point": "320",
    "discount_price_usd": "2.49",
    "discount_price_idr": "24500",
    "discount_price_point": null,
    "discount_tag": "50% OFF",
    "discount_name": "Special promo 50% Off",
    "platforms_offers": [
        {
            "offer_id": 5,
            "platform_id": 1,
            "price_usd": "4.99",
            "price_idr": "49000",
            "price_point": "490",
            "tier_id": 123,
            "tier_code": ".c.USD.4.99",
            "discount_price_usd": "2.99",
            "discount_price_idr": "29000",
            "discount_price_point": null,
            "discount_tag": "50% OFF",
            "discount_name": "Special promo 50% Off",
            "discount_tier_id": 764,
            "discount_tier_code": ".c.USD.2.99"
        }
    ]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Offer

Update offer.

**URL:** /offers/{offer_id}

**Method:** PUT

**Request Data Params:**

Required: `offer_type_id`, `offer_status`, `name`, `sort_priority`, `is_active`, `price_usd`, `price_idr`, `price_point`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `exclusive_clients`, `aggr_price_usd`, `aggr_price_idr`, `aggr_price_point`

For single and bundle type, the required params are: `items`

For subscription type, the required params are: `quantity`, `quantity_unit`, `brands`

For `platforms_offers`, the required params are: `platform_id`, `price_usd`, `price_idr`, `price_point`

**Response HTTP Code:**

* `200` - Offer updated with no new platforms_offers record created

* `201` - Offer updated with new platforms_offers record created

**Response Params:** See [Offer Properties](#offer-properties)

**Examples:**

Request:

```
PUT /offers/5 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Single issue",
    "sort_priority": 1,
    "is_active": true,
    "offer_type_id": 1,
    "offer_status": 7,
    "is_free": false,
    "point_reward": 99,
    "items": [
        2345
    ],
    "exclusive_clients": [],
    "offer_code": "ID_TEMP2014ED452",
    "price_usd": "3.20",
    "price_idr": "32000",
    "price_point": "320",
    "aggr_price_usd": "5.99",
    "aggr_price_idr": "59000",
    "aggr_price_point": "590",
    "vendor_price_usd": "3.20",
    "vendor_price_idr": "32000",
    "vendor_price_point": "320",
    "platforms_offers": [
        {
            "platform_id": 1,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 123
        },
        {
            "platform_id": 2,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 124
        },
        {
            "platform_id": 3,
            "price_usd": "3.49",
            "price_idr": "35000",
            "price_point": "320",
            "tier_id": 125
        }
    ]
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 5,
    "name": "Single issue",
    "sort_priority": 1,
    "is_active": true,
    "offer_type_id": 1,
    "offer_status": 7,
    "is_free": false,
    "point_reward": 99,
    "items": [
        2345
    ],
    "exclusive_clients": [],
    "offer_code": "ID_TEMP2014ED452",
    "price_usd": "3.20",
    "price_idr": "32000",
    "price_point": "320",
    "aggr_price_usd": "5.99",
    "aggr_price_idr": "59000",
    "aggr_price_point": "590",
    "vendor_price_usd": "3.20",
    "vendor_price_idr": "32000",
    "vendor_price_point": "320",
    "discount_price_usd": null,
    "discount_price_idr": null,
    "discount_price_point": null,
    "discount_tag": null,
    "discount_name": null,
    "platforms_offers": [
        {
            "offer_id": 5,
            "platform_id": 1,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 123,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        },
        {
            "offer_id": 5,
            "platform_id": 2,
            "price_usd": "3.99",
            "price_idr": "39000",
            "price_point": "320",
            "tier_id": 124,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        },
        {
            "offer_id": 5,
            "platform_id": 3,
            "price_usd": "3.49",
            "price_idr": "35000",
            "price_point": "320",
            "tier_id": 125,
            "tier_code": null,
            "discount_price_usd": null,
            "discount_price_idr": null,
            "discount_price_point": null,
            "discount_tier_id": null,
            "discount_tier_code": null,
            "discount_tag": null,
            "discount_name": null
        }
    ]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Offer

Delete offer. This service will set `is_active` value to false.

**URL:** /offers/{offer_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Offer deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /offers/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

