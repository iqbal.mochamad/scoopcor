
### Offer Types

Offer type resource used to identify system's supported offer types.

Current existing offer types are:

1. Single: Single item offer, this offer contains item that already existing in the system

2. Subscription: Subscription offer, this offer contains forward contract of certain brand existing in the system

3. Bundle: Bundled items offer, this offer contains items that already existing in the system

Note: New offer type mostly will has specific use-cases come along with it. So this resource records only for metadata only, the use-cases implementation will be vary.

#### Offer Type Properties

* `id` - Number; Instance identifier

* `name` - String; Unique offer type name

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `slug` - String, Unique; Offer type slug for SEO

* `meta` - String; Offer type meta for SEO

* `description` - String; Offer type description

#### Create Offer Type

Create new offer type.

**URL:** /offer_types

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `201` - New offer type created

**Response Params:** See [Offer Type Properties](#offer-type-properties)

**Examples:**

Request:

```
POST /offer_types HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Single",
    "sort_priority" : 1,
    "is_active": true,
    "slug": "single"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/offer_types/1

{
    "id": 1,
    "name": "Single",
    "sort_priority" : 1,
    "is_active": true,
    "slug": "single",
    "meta": null,
    "description": null
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Offer Types

Retrieve offer types.

**URL:** /offer_types

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Item Type Properties](#item-type-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Offer types retrieved and returned in body content

* `204` - No matched offer type

**Response Params:** See [Offer Type Properties](#offer-type-properties)

**Examples:**

Request:

```
GET /offer_types?fields=id,name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "offer_types": [
        {
            "id": 1,
            "name": "Single"
        },
        {
            "id": 2,
            "name": "Subscription"
        },
        {
            "id": 3,
            "name": "Bundle"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 3,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Offer Type

Retrieve individual offer type using identifier.

**URL:** /offer_types/{offer_type_id}

Optional:

* `fields` - Retrieve specific properties. See [Offer Type Properties](#offer-type-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Offer type found and returned

**Response Params:** See [Offer Type Properties](#offer-type-properties)

**Examples:**

Request:

```
GET /offer_types/1?fields=id,name,sort_priority,slug,meta,description,is_active HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Single",
    "sort_priority": 1,
    "slug": "single",
    "meta": null,
    "description": null,
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Offer Type

Update offer type.

**URL:** /offer_types/{offer_type_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `200` - Offer type updated

**Response Params:** See [Offer Type Properties](#offer-type-properties)

**Examples:**

Request:

```
PUT /offer_types/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Single",
    "sort_priority": 9,
    "slug": "single",
    "is_active": true
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "sort_priority": 9,
    "slug": "single",
    "meta": null,
    "description": null,
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Offer Type

Delete offer type. This service will set `is_active` value to false.

**URL:** /offer_types/{offer_type_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Offer type deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /offer_types/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

