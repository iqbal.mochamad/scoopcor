Buffet Offers
=============

### Buffets

Buffets are a type of offer where the user has access to a set of Items,
or all Items within a given Brand, for a limited amount of time.  After
that amount of time expires for a User, their access to those Items will be
revoked (this is different to a Subscription where a user will always have
access to the Items acquired as part of their subscription, even when that
subscription has expired).


#### Buffet Properties

Buffets have only 4 properties.

* `available_from` - ISO 8601 Date/Time;  The earliest date/time that a user may puchase this buffet.  If null, the buffet is available immediately.
* `available_until` - ISO 8601 Date/Time;  The last date/time that users may purchase access to this buffet.  If null, the buffet has no limit to it's purchase availability.
* `buffet_duration` - ISO 8601 Duration **(required)**;  The amount of time, after the date of purchase, that buffet items will be available to a user.
* `offer` - A single offer, with all it's properties (include offer_platforms), nested within this buffet (buffets only ever have a single offer).  See Offer Properties for the exact details.

#### Create Buffet

**URL**: /v1/offers/buffets

**Method**: POST

**Success Response Codes**:

* HTTP 201 CREATED

**Success Response Data**: See *Retrieve Individual Buffet* below.

**Error Response Codes**:

* HTTP 400 BAD REQUEST - Invalid or missing data in JSON request body
* HTTP 401 UNAUTHORIZED - Bad Authentication header

```
Content-type: application/json
Accept: application/json
Authorization: Bearer {your-scoopcas-token}
```

``` json
{
    "available_from": "2015-07-01T00:00:00Z",
    "available_until": "2015-07-31T23:59:59Z",
    "buffet_duration": "P1M",
    "offer": {
        "name": "Scoop July Buffet",
        "offer_status": 7,
        "sort_priority": 1,
        "is_active": true,
        "offer_type_id": 4,
        "price_usd": 14.44,
        "price_idr": 144440,
        "price_point": 0,
        "brands": [5,12,36],
        "items": [20051, 20052],
        "platforms_offers": [
            {
                "platform_id": 3,
                "tier_id": 3,
                "currency": "USD",
                "price_usd": 7.99,
                "price_idr": 79000,
                "price_point": 0
            }
        ]
    }
}
```

#### Retrieve Buffet

**URL**: /v1/offers/buffets

**Method**: GET

**Success Response Codes**:

* HTTP 200 OK

**Error Response Codes**:

* HTTP 400 BAD REQUEST - Invalid request data
* HTTP 401 UNAUTHORIZED - Bad Authentication header

```
Accept: application/json
Authorization: Bearer {your-scoopcas-token}
```

``` json
{
    "buffets": [
        {
            "available_from": "2015-05-01T00:00:00+00:00",
            "available_until": "2015-05-01T00:00:00+00:00",
            "buffet_duration": "P30DT0S",
            "id": 1,
            "offer": {
                "aggr_price_idr": 0,
                "aggr_price_point": 0,
                "aggr_price_usd": null,
                "brands": [
                    12,
                    5,
                    36
                ],
                "discount_id": null,
                "discount_name": null,
                "discount_price_idr": 0,
                "discount_price_point": 0,
                "discount_price_usd": null,
                "discount_tag": null,
                "exclusive_clients": null,
                "id": 41975,
                "image_highres": null,
                "image_normal": null,
                "is_active": true,
                "is_discount": false,
                "is_free": false,
                "item_code": null,
                "items": [
                    20051,
                    20052
                ],
                "name": "MAY BUFFET OFFER",
                "offer_code": null,
                "offer_status": 7,
                "offer_type_id": 4,
                "platforms_offers": [],
                "price_idr": 144440,
                "price_point": 0,
                "price_usd": 14.44,
                "sort_priority": 1,
                "vendor_price_idr": 0,
                "vendor_price_point": 0,
                "vendor_price_usd": null
            }
        },
        " ... Additional objects with identical format to above ..."
    ],
    "metadata": {
        "resultset": {
            "count": 5,
            "limit": 20,
            "offset": 0
        }
    }
}

```

#### Retrieve Individual Buffet

**URL**: /v1/offers/buffets/{buffet-id}

Similar to an individual JSON object from the response above, with idential
request headers.

#### Update Buffet

**URL**: /v1/offers/buffets/{buffet-id}

**Method**: PUT

All other properties are idential to Create Buffet

#### Delete Buffet

This performs a soft-delete on a buffet object by setting it's 'is_active'
property to false:

**URL**: /v1/offers/buffets/{buffet-id}

**Method**: DELETE

**Success Response Codes**:

* HTTP 204 NO CONTENT

```
Authorization: Bearer {your-scoopcas-token}
```

This method does not accept an HTTP body.

