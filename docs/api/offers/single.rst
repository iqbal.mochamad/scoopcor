Single Offers
=============

Single offers are pretty much what they sound like -- an offer where the
user may purchase a single book, magazine issue, or newspaper edition.


List
----

+-------------+------------+
| HTTP Method |  GET       |
+-------------+------------+
| URL         | /v1/offers |
+-------------+------------+




Details
-------

Create
------

Update
------

Delete
------
