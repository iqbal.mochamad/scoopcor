JSON Data Types
===============

JSON has a very limited number of data types that is can represent.

In addition to the set of JSON data types, we also define our own.
When needing to specify serialize data, beyond what JSON provides, we try to stick with strings, encoded in
well-known standards.


Standard Data Types
-------------------

String

Boolean

Null

Number

Array

Object


Scoop Extended Data Types
-------------------------

Date

DateTime

Timespan

Locations

Suggested: http://geojson.org/
