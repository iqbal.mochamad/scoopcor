Remote Publishing
=================

Provides mappings of Scoopcor Items/Offers and Categories, to identifiers
used by a remote system.

An example of this would be Elevenia.co.id's website.  We upload our
offers to their API (known as products), but we must include one of their
category ID's (which are not writable by us), and thesefore must map to
one of our products.


Properties
----------

id
     Number; Automatically-assigned scoop internal identifier.

category_id
     Number; Scoop Category ID

remote_service
     String Enum (possible values: 'elevenia'); Identifier for a given
     remote service.

remote_id
     String; An identifier automatically assigned by the remote_service.
     This should be unique per remote_service.


Create
------

Create new category mapping.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/remote-publishing/categories                     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| category_id                       |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| remote_service                    |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| remote_id                         |    YES    |                              |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /v1/remote-publishing/categories HTTP/1.1
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "category_id": 12345,
        "remote_service": "elevenia",
        "remote_id": "12313423234"
    }

:download:`Schema <remote-category.json>`

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 1,
        "category_id": 12345,
        "remote_service": "elevenia",
        "remote_id": "12313423234"
    }

:download:`Schema <remote-category.json>`


List
----

Retrieve a paginated list of remote categories.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/remote-publishing/categories                     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /v1/remote-publishing/categories HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "remote_categories": [
            {
                "id": 1,
                "category_id": 12345,
                "remote_service": "elevenia",
                "remote_id": "12313423234"
            },
            {
                "id": 2,
                "category_id": 12346,
                "remote_service": "elevenia",
                "remote_id": "12313423235"
            }
        ],
        "metadata": {
            "resultset": {
                "count": 2,
                "offset": 0,
                "limit": 2
            }
        }
    }

:download:`Schema <remote-category-list.json>`


Detail
------

Retrieve individual remote category with identifier.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/remote-publishing/categories/{id}                |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /v1/remote-publishing/categories/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 1,
        "category_id": 12345,
        "remote_service": "elevenia",
        "remote_id": "12313423234"
    }

:download:`Schema <remote-category.json>`


Update
------

Update currency.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/remote-publishing/categories/{id}                |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Record was updated                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers.                 |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

.. note::

    Object property requirements are exactly the same as for Create

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /v1/remote-publishing/categories/1 HTTP/1.1
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "category_id": 12345,
        "remote_service": "elevenia",
        "remote_id": "12313423234"
    }

:download:`Schema <remote-category.json>`

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 1,
        "category_id": 12345,
        "remote_service": "elevenia",
        "remote_id": "12313423234"
    }

:download:`Schema <remote-category.json>`


Delete
------

Soft-deletes the resource specified in the URL, although the API response will
behave as if the resource were actually deleted.  In reality, this endpoint
will simply set the is_active property of the object to false.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/remote-publishing/categories/{id}                |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                               |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Record was deleted                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request (for delete, this would  |
|                       |               | typically be incorrect headers)      |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    DELETE /v1/remote-publishing/categories/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 204 No Content
