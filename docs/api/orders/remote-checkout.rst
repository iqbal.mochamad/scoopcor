Remote Checkout
===============

Allows for posting of orders that have been completed on a remote/3rd-party
system.

.. note::

    To notify SCOOP that a purchase has occured in a remote system, you must
    submit a POST that includes information about your purchase.

    For 3rd party providers, your Authorization header will be provided to you.

    Also, note the request and response objects returned are not of the same
    type and therefore not Restful.
    It's important to notice the different property types.


Properties
----------

Checkout Request
^^^^^^^^^^^^^^^^

sub_total
    Number; Required;  The purchase amount of the order before any taxes,
    discounts.

grand_total
    Number; Required;  The total amount the user actually paid.

sender_email
    String; Required;  The e-mail address of the user account that should be
    credited for the purchase.  This must be an existing SCOOP User.

receiver_email
    String; Required;  The e-mail address of the user account that should
    have this item added to it.  If no user exists, the user will automatically
    be created and notified via e-mail.

remote_service_name
    String; Required; Enumerable: {<partner_name>}.  Identifier of the remote system
    which processed the purchase.

currency_code
    String; Required; Enumerable: {idr, usd}

items
    Array [of object];  Required; The ID of the items in the
    **remote system**, their sub_total and grand_totals.
    SCOOP maintains an internal mapping of our internal
    item id's to the item id's of the remote system.

user_message
    String; An optional message that will be included in the notification
    email sent to receiver_email.

remote_order_number
    String; Order number from remote system

Checkout Response
^^^^^^^^^^^^^^^^^

order_id
    Number; Instance identifier.
user_id
    Number; SCOOP internal user identifier.
total_amount
    Number;  The order sub total.
final_amount
    Number;  The order grand total.
client_id
    Number; Client identifier. Used to identify from which client this order
    come from.
order_status
    Number; Order status. Showing order statuses in order, payment, delivery,
    refund, and cancellation states.

    +--------+-----------------------------------------------------------------+
    | Code   | Description                                                     |
    +========+=================================================================+
    | 30001  | WAITING FOR DELIVERY - Marked right as the delivery function    |
    |        | starts.                                                         |
    +--------+-----------------------------------------------------------------+
    | 30002  | DELIVERY IN PROCRESS - Status to marks delivery process is on   |
    |        | going.                                                          |
    +--------+-----------------------------------------------------------------+
    | 30003  | DELIVERED - Status showing that eligible items delivered to     |
    |        | user, this status merked right away after all save process to   |
    |        | user completed.                                                 |
    +--------+-----------------------------------------------------------------+
    | 50002  | DELIVERY ERROR - Delivery error status, for missing items, item |
    |        | not for sale anymore, retrieve or save data error (DB's table   |
    |        | level fail), etc. Details appended to note property.            |
    +--------+-----------------------------------------------------------------+
    | 90000  | COMPLETE - All order process completed.                         |
    +--------+-----------------------------------------------------------------+

order_number
    Number; SCOOP Order alternative identifier for consumer-user communication
    purpose.
remote_order_number
    String; Order number from remote system
partner_id
    Number; Partner identifier to identify from which preloaded-partnership
    client this order come from.
currency_code
    String; Currency code of total_amount and final_amount.
note
    String; Note for this order.
order_lines
    Array; Lines of the order.

    id
        Number; Order line identifier.
    name
        String; Order line name, equals to offer's name See Offer Properties.
    offer_id
        Number; Offer identifier.
    quantity
        Number; Quantity of the ordered offer, currently this parameter
        will only contains 1 as value.
    final_price
        String; Order line final price after all discounts applied, this property
        is only available if payment gateway already specified in request.
    currency_code
        String; Currency code of raw_price and final_price.
    order_line_status
        Number; Order line status. Showing order line statuses in order,
        delivery, and refund states. This status only used as supplement
        to Order's statuses.

        +--------+-------------------------------------------------------------+
        | Code   | Description                                                 |
        +========+=============================================================+
        | 10000  | TEMP - Temporarily order line, this status marks the order  |
        |        | line on checking out state and before confirmation.         |
        +--------+-------------------------------------------------------------+
        | 10001  | NEW - New confirmed order line, this status marks the order |
        |        | line is confirmed and ready to go into payment flow.        |
        +--------+-------------------------------------------------------------+
        | 30003  | DELIVERED - Status showing that eligible items delivered to |
        |        | user, this status merked right away after all save process  |
        |        | to user completed                                           |
        +--------+-------------------------------------------------------------+
        | 40003  | REFUNDED - Refund completed.                                |
        +--------+-------------------------------------------------------------+
        | 50000  | CANCELLED - Order cancelled by consumer user side before    |
        |        | payment flow. This only included if the payment gateway     |
        |        | provide cancellation service.                               |
        +--------+-------------------------------------------------------------+
        | 50002  | DELIVERY ERROR - Delivery error status, for missing items,  |
        |        | item not for sale anymore, retrieve or save data error      |
        |        | (DB's table level fail), etc.                               |
        +--------+-------------------------------------------------------------+
        | 50003  | REFUND ERROR - Refund error status, retrieve or save data   |
        |        | error (DB's table level fail), etc.                         |
        +--------+-------------------------------------------------------------+
        | 90000  | COMPLETE - All order line process completed.                |
        +--------+-------------------------------------------------------------+


Submit Checkout
---------------

* Method: POST
* URL: /v1/orders/remote-checkout/<partner_name>

**Example Request:**

.. code-block:: http

    Accept: application/json
    Accept-charset: utf-8
    Content-type: application/json; charset=utf-8
    Authorization: Bearer {your-SCOOP-token}


.. code-block:: json

    {
        "sub_total": 200000,
        "grand_total": 200000,
        "sender_email": "someone@getscoop.com",
        "receiver_email": "someone.else@getscoop.com",
        "currency_code": "idr",
        "items": [
            {"id": 123456, "sub_total": 50000, "grand_total": 50000},
            {"id": 123457, "sub_total": 150000, "grand_total": 150000}
        ],
        "user_message": "Thanks for buying your digital books on <partner_name>!",
        "remote_order_number": "INV12345"
    }



**Properties**:




Response
^^^^^^^^

Once you have submitted the completed order data to SCOOP, you'll receive
information back about your order, but this time, all the details will
be the SCOOP-centric data.

If there is any problem with submitting the order, you'll need to notify
Apps-Foundry with this data.

.. code-block:: http

    Content-type: application/json; charset=utf-8

.. code-block:: json

    {
        "payment_gateway_name": "<partner_name>",
        "payment_gateway_id": 27,
        "user_id": 682,
        "total_amount": 200000,
        "final_amount": 200000,
        "order_id": 50,
        "order_status": 90000,
        "order_number": 9109863892910722,
        "remote_order_number": "INV12345",
        "currency_code": "idr",
        "order_lines": [{
            "name": "PANORAMA / NOV\u2013DEC 2011 - Single Edition",
            "items": [
                {
                    "name": "PANORAMA / NOV\u2013DEC 2011",
                    "id": 1260,
                },
                {
                    "name": "PANORAMA / NOV\u2013DEC 2012",
                    "id": 1261,
                },
            ],
            "offer_id": 2122,
            "final_price": 200000,
            "currency_code": "idr",
        }]
    }




+-----------------------------------------------------------------------------+
|                  Response HTTP Status Codes                                 |
+=====+=======================================================================+
| 201 | Purchase was successfully recorded                                    |
+-----+-----------------------------------------------------------------------+
| 400 | Invalid request (either headers invalid or bad request body data)     |
+-----+-----------------------------------------------------------------------+
| 401 | Invalid Authorization header token                                    |
+-----+-----------------------------------------------------------------------+
| 50x | Some other error                                                      |
+-----+-----------------------------------------------------------------------+
