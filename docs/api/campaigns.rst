
### Campaigns

Campaign resource.

#### Campaign Properties

* `id` - Number; Currency identifier

* `name` - String; Campaign name

* `description` - String; Campaign description

#### Create Campaign

Create new campaign.

**URL:** /campaigns

**Method:** POST

**Request Data Params:**

Required: `name`

Optional: `description`

**Response HTTP Code:**

* `201` - New campaign created

**Response Params:** See [Campaign Properties](#campaign-properties)

**Examples:**

Request:

```
POST /campaigns HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Christmas 2014 Sale",
    "description": "Christmas 2014 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/campaigns/1

{
    "id": 1,
    "name": "Christmas 2014 Sale",
    "description": "Christmas 2014 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

#### Retrieve Campaigns

Retrieve campaigns.

**URL:** /campaigns

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Campaign Properties](#campaign-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Campaign retrieved and returned in body content

* `204` - No matched campaign

**Response Params:** See [Campaign Properties](#campaign-properties)

**Examples:**

Request:

```
GET /campaigns HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "campaigns": [
        {
            "id": 1,
            "name": "Christmas 2014 Sale",
            "description": "Christmas 2014 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 1,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Campaign

Retrieve individual campaign.

**URL:** /campaigns/{campaign_id}

Optional:

* `fields` - Retrieve specific properties. See [Campaign Properties](#campaign-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Campaign found and returned

**Response Params:** See [Campaign Properties](#campaign-properties)

**Examples:**

Full request:

```
GET /campaigns/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Christmas 2014 Sale",
    "description": "Christmas 2014 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Retrieve Campaign's Items

Retrieve items that are included to any campaigns.

**URL:** /campaigns/{campaign_id}/items

Required:

* `platform_id` - Retrieve specific item in platform id within any campaigns

**Method:** GET

**Response HTTP Code:**

* `200` - Campaign retrieved and returned in body content

* `204` - No matched campaign

**Response Params:** See [Item Properties](#item-properties)

**Examples:**

Request:

```
GET /campaigns/654/items?platform_id=4 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "items": [
        {
            "is_featured": false,
            "extra_items": [],
            "gtin13": null,
            "subs_weight": 1,
            "parental_control_id": 1,
            "is_active": true,
            "brand":
            {
                        "id": 142,
                        "name": "POPULAR"
            },
            "item_type_id": 1,
            "item_status": 9,
            "gtin8": null,
            "brand_id": 142,
            "thumb_image_normal": "images/1/142/general_small_covers/ID_POPLR2013MTH12_S_.jpg",
            "meta": null,
            "content_type": 1,
            "authors": [],
            "display_offers":
            [
                {
                    "discount_tag": "Reduce Price for POPULAR 2013",
                    "discount_name": "Reduce Price for POPULAR 2013",
                    "price_usd": "3.99",
                    "discount_id": [
                        "697"
                    ],
                    "discount_paymentgateway_id": [
                        2,
                        5,
                        11,
                        12,
                        14,
                        6
                    ],
                    "offer_id": 18927,
                    "offer_type_id": 1,
                    "discount_price_idr": "20000.00",
                    "price_idr": "39000.00",
                    "price_point": 390,
                    "discount_price_usd": "1.99",
                    "is_free": false,
                    "discount_price_point": 0,
                    "offer_name": "Single Edition",
                    "aggr_price_usd": null,
                    "aggr_price_point": null,
                    "aggr_price_idr": null
              }
            ],
            "is_latest": false,
            "id": 30669,
            "edition_code": "ID_POPLR2013MTH12",
            "image_highres": "images/1/142/big_covers/ID_POPLR2013MTH12_B_.jpg",
            "restricted_countries": [],
            "sort_priority": 1,
            "image_normal": "images/1/142/general_big_covers/ID_POPLR2013MTH12_B_.jpg",
            "name": "POPULAR / DEC 2013",
            "allowed_countries": [],
            "release_date": "2013-12-01T00:00:00",
            "is_extra": false,
            "slug": "popular-dec-2013",
            "media_base_url": "http://images.getscoop.com/magazine_static/",
            "gtin14": null,
            "reading_direction": 1,
            "filenames":
            [
                "images/1/142/ID_POPLR2013MTH12.zip"
            ],
            "previews": [],
            "thumb_image_highres": "images/1/142/small_covers/ID_POPLR2013MTH12_S_.jpg",
            "issue_number": "DEC 2013"
            },
            {...}
        ],
        "name": "Reduce Price for POPULAR",
        "description": "Reduce Price for POPULAR magazine"
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Update Campaign

Update campaign.

**URL:** /campaigns/{campaign_id}

**Method:** PUT

**Request Data Params:**

Required: `name`

Optional: `description`

**Response HTTP Code:**

* `200` - Campaign updated

**Response Params:** See [Campaign Properties](#campaign-properties)

**Examples:**

Full request:

```
PUT /campaigns/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "0.99 Sale",
    "description": "0.99 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "0.99 Sale",
    "description": "0.99 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Fail response:

```
HTTP/1.1 400 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```

#### Delete Campaign

Delete campaign. This service will set `is_active` value to false.

**URL:** /campaigns/{campaign_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Campaign deleted

**Response Params:** -

**Examples:**

Full request:

```
DELETE /campaigns/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```
