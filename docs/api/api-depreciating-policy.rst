API Deprecation
===============

As much as we wish it wasn't the case, but business requirements change, and so do technologies.

APIs will eventually be depreciated and removed from the API (usually replaced with a new API or a new API format).

After depreciation, APIs will remain for at least 6 months, or for more fundamental APIs, they'll remain for 12
months.

Once they've been flagged as depreciated, they'll include a warning header:


.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json
    X-Deprecation-Notice: Available until <some-date>, Replaced with <some-url>

+-----------------------------------------------------------------------------+
| Deprecated APIs                                                             |
+=============================================================================+
| Item Types                                                                  |
+-----------------------------------------------------------------------------+
| Organization Types                                                          |
+-----------------------------------------------------------------------------+
| Download Authorization                                                      |
+-----------------------------------------------------------------------------+
