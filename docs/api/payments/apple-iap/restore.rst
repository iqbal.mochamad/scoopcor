Restore (IOS)
=============

Allows for FrontEnd Mobile Apps to restore owned item based on previous receipt data.
If success, it will return a list of owned items for that receipt data.


.. toctree::

    restore

.. http:post:: /v1/payments/apple-iap/restore

    Restore owned item based on a Receipt Data (Apple IOS 7+).
    Important: this api is new, it's different with previous restore api (depecrated): /v1/payments/appleiaps/restore

    This is an asynchronous process. POST only submit a thread. After async process finished, it will send email
    and push notification to user device (if device_registration_id provided).
    or use GET method below to get the latest status of a restore process.


    **Request Data Params**

    Required: `receipt_data`

    * `receipt_data` - String; an ecrypted receipt data from Apple IOS 7+

    * `device_registration_id` - String: device registration id from Firebase Cloud Messaging (for push notification)

    **Properties of Response**

    `id` - Number; restore process identifier.

    `title` - String; process name (restore in this case).

    `href` - String; url link to restore details info.

    **Example Request**

    .. sourcecode:: http

        POST /v1/payments/apple-iap/restore HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        Authorization: JWT ajksfhiau12371894^&*@&^$%.dflasfjsadfsdaf.dfdsalkfjasdlfjdsaf
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop Portal/3.0

        {
            "receipt_data": "kfjalkfjsalkjfksdaljfsdlajfsadklfjl12k312dfkljasldkfj123123",
            "device_registration_id": "abc12312Device123"
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
            "id": 1,
            "title": "restore",
            "status": "in progress",
            "href": "https://dev6.apps-foundry.com/v1/payments/apple-iap/restore/1"
        }

.. http:get:: /v1/payments/apple-iap/restore/<int:restore_id>

    Get latest status of a restore process

    **Example Request**

    .. sourcecode:: http

        GET /v1/payments/apple-iap/restore/1 HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Authorization: JWT ajksfhiau12371894^&*@&^$%.dflasfjsadfsdaf.dfdsalkfjasdlfjdsaf
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop Portal/3.0

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
            "id": 1,
            "title": "restore",
            "status": "completed",
            "href": "https://dev6.apps-foundry.com/v1/payments/apple-iap/restore/1"
        }

        if failed/error:

        {
            "id": 1,
            "title": "restore",
            "href": "https://dev6.apps-foundry.com/v1/payments/apple-iap/restore/1"
            "status": "error",
            "error": {
                "user_message": "Failed in restore process",
                "developer_message": "Receipt status = 21003 - The receipt could not be authenticated.",
            }
        }
