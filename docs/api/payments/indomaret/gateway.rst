=========================
Indomaret Payment Gateway
=========================

For more information, developer can take look at :

http://docs.veritrans.co.id/en/vtdirect/integration_indomrt.html



.. note::

   This Feature is using python veritranspay library written by Derek J. Curtis <derek.curtis@apps-foundry.com>.
   see details:
   http://veritranspay.readthedocs.io/en/latest/

create payment with payment status as PAYMENT_IN_PROCESS.

PAYMENT STATUS TABLE

+---------------------+------------+
| Payment Status      |    CODE    |
+=====================+============+
| WAITING_FOR_PAYMENT |   20001    |
+---------------------+------------+
| PAYMENT_IN_PROCESS  |   20002    |
+---------------------+------------+
| PAYMENT_BILLED      |   20003    |
+---------------------+------------+
| PAYMENT_RESTORED    |   20004    |
+---------------------+------------+
| PAYMENT_CANCELLED   |   50000    |
+---------------------+------------+
| PAYMENT_ERROR       |   50001    |
+---------------------+------------+

Param :
Required
       order_id : [integer], order id from core_orders
       signature : [string], run module app.payments.signature_generator and use order_id as parameter
       user_id : [integer], buyer user id
       payment_gateway_id : [integer], take a look core_paymentgateways table. And, Indomaret id is 26.
Optional
       payer_id : [string], buyer email address
       billing_address: [dictionary], data billing address
           city, first_name, last_name, postal_code, phone, address1, address2


ExampleRequest:

|   URL : v1/payments/authorize

    {
        "order_id": 817408,
        "signature": "b6eb7f2f836cc4318bdd7869c156b6f0cb79f225fd7ac9231af58751c1cfafd1",
        "user_id": 430450,
        "payment_gateway_id": 26,
        "payer_id": "ahmad.syafrudin@apps-foundry.com",
        "billing_address":
        {
              "city": "Jakarta",
              "first_name": "Nama Cust1",
              "last_name": "LastName",
              "postal_code": "12345",
              "phone": "1234567",
              "address1": "",
              "address2": ""
        }
    }


.. note::

    login to https://my.sandbox.veritrans.co.id/ with this credentials:
    Username : ahmad.syafrudin@apps-foundry.com
    Password : P@ssw0rd24


if you want to change status to settlement for indomaret payment gateway, follow this URL:

https://api.sandbox.veritrans.co.id:7676/indomaret/index

.. note::
    To get payment code from transaction details. Open Transaction page(https://my.sandbox.veritrans.co.id/payments).
    and choose which order id that you want to pay.
