Kredivo Payment Gateway
=======================

All documentation related to Kredivo Gateway

Authorize
---------

.. http:post:: /v1/payments/authorize

    Create order with Kredivo payment gateway as Temporary orders

    **Request Data Params**

    Required: `user_id`, `order_id`, `signature`, `payment_gateway_id`

    * `user_id` - User id

    * `order_id` - Order number

    * `signature` - String; Signature for this payment. See Payment Signature for detail how to generate this value.

    * `payment_gateway_id` - integer; the value should be 27 (kredivo)

    * `return_url` - URL; URL that will be called when the Kredivo verification process success.

    * `cancel_url` - URL; URL that will be called when the Kredivo verification process cancelled.

    **Example Request**

    .. sourcecode:: http

        Content-Type: application/json; charset=utf-8
        Authorization: Bearer ajksfhiau12371894^&*@&^$%

        {
            "order_id": 123,
            "signature": "12313hiuahdiuhiuhwqe",
            "user_id": 430450,
            "payment_gateway_id": 27,
            "return_url": "http://getscoop.com/offer/123",
            "cancel_url": "http://getscoop.com/offer/123/cancel"
        }


    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json; charset=utf-8
        Authorization: Bearer ajksfhiau12371894^&*@&^$%

        {
          "payment_id": 233,
          "redirect_url": "https://api.kredivo.com/kredivo/signin?tr_id=XXX"
        }


    **Properties of Response**

    `payment_id` - integer, id of payment

    `redirect_url` - url, url from kredivo response

Capture
-------

.. http:post:: /v1/payments/kredivo/capture

    Capture Post Notification with Status=SETTLEMENT
    After customer validated in kredivo, Kredivo will hit this API.

    **Request Data Params**

    Required: `transaction_id`, `order_id`, `signature_key`, `transaction_status`

    * `transaction_id` - String; Transaction Id given by Kredivo

    * `order_id` - int; Order number by SCOOP

    * `signature_key` - String; Signature key to validate if the notification is originated from Kredivo

    * `transaction_status` - String; Transaction status of a transaction.

        * CAPTURE if Transaction has been captured
        * SETTLEMENT mean Transaction has been settled
        * PENDING for Transaction has not been completed
        * DENY if Transaction has been denied
        * CANCEL if Transaction has been cancelled
        * EXPIRE if Transaction has been expired

    **Example Request**

    .. sourcecode:: http

        POST /v1/payments/kredivo/capture
        Content-Type: application/json; charset=utf-8

        {
            "transaction_id": "bb0380d7-2688-4e73-a668-997f3fc5cbec",
            "transaction_status": "SETTLEMENT",
            "order_id": 70003,
            "signature_key": "5f1fa4a524f5a8201a3a373d67b5796840bed1598b2bb816855d0e14d01cd09b988b4b861f39bed642b4db861108bbf0dd84ce8e17caf675d4bf2c81ab545397"
        }


    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json; charset=utf-8

        {
            "status": "OK",
            "message": "Message from merchant if any"
        }


    **Properties of Response**

    `status` - String, "OK" if success or "ERROR" if failed . see message for detail info

    `message` - String, message from scoopcor API, if any
