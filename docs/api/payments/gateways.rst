Payment gateways
================

Payment gateway resource.

#### Payment Gateway Properties

* `id` - Number; Instance identifier

* `name` - String; Vendor name

* `payment_flow_type` - Number; Payment flow type as below.

    1: N/A; Set to this when payment flow is not yet clear or not yet normalized

    2: AUTHORIZE - VALIDATE; 2 step payment flow with server side to authorize and later validate, e.g., mandiri e-cash, finnet

    3: CHARGE; 1 step payment flow with server side to trigger charge, e.g., mandiri clickpay, SCOOP Point, FREE

    4: VALIDATE; 1 step payment flow with server side receipt validation, e.g., Apple In-App Purchase, Google In-App Billing, Windows Phone In-App Purchase

    5: AUTHORIZE - CAPTURE; 2 step payment flow with server side to authorize and capture, e.g., Paypal, Veritrans' VT-Direct

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority.

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true.

* `slug` - String, Unique; Payment gateway slug for SEO.

* `meta` - String; Payment gateway meta for SEO.

* `description` - String; Payment gateway description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URL.

* `icon_image_normal` - String; Payment gateway icon image URI.

* `icon_image_highres` - String; Payment gateway icon image in highres URI.

* `base_currency_id` - Number; Currency identifier which this payment gateway used as base currency.

* `mnc` - Number; Mobile Network Code, use on payment gateway in relation with telco.

* `mcc` - Number; Mobile Country Code, use on payment gateway in relation with telco.

* `sms_number` - Number; Short Message Service number to send, use on payment gateway in relation with telco.

* `organization_id` - Number; Unique, organization identifier that this instance bind to. A payment gateway can only bind to an organization.

* `clients` - Array; List of clients this payment gateway already implemented.

* `minimal_amount` - String; The minimal amount of purchase. Set to NULL if one has no minimal amount.


Create Payment Gateway
----------------------

.. http:post:: /v1/payment_gateways

    Create new payment gateway.

    **Request Data Params:**

    Required: `name`, `sort_priority`, `is_active`, `slug`, `cut_rule_id`, `base_currency_id`, `organization_id`, `payment_flow_type`, `clients`

    Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `mnc`, `mcc`, `sms_number`

    **Response HTTP Code:**

    * `201` - New payment gateway created

    **Response Params:**

    See [Payment Gateway Properties](#payment-gateway-properties)

    **Examples Request**

    .. sourcecode:: http

    POST /v1/payment_gateways HTTP/1.1
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

    {
        "name": "Apple In-App Purchase",
        "slug": "apple-in-app-purchase",
        "sort_priority": 1,
        "payment_flow_type": 4,
        "is_active": true,
        "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
        "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
        "cut_rule_id": 5,
        "base_currency_id": 1,
        "organization_id": 3421,
        "clients": [],
        "minimal_amount": "25000"

    }

    **Example Success response**

    .. sourcecode:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/payment_gateways/1

    {
        "id": 1,
        "name": "Apple In-App Purchase",
        "slug": "apple-in-app-purchase",
        "sort_priority": 1,
        "payment_flow_type": 4,
        "is_active": true,
        "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
        "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
        "cut_rule_id": 5,
        "base_currency_id": 1,
        "description": null,
        "mnc": null,
        "mcc": null,
        "sms_number": null,
        "organization_id": 3421,
        "clients": [],
        "minimal_amount": "25000"
    }

    **Example Failed Response**

    .. sourcecode:: http

    HTTP/1.1 400 Bad Request
    Content-Type: application/json; charset=utf-8

    {
        "status": 400,
        "error_code": 400101,
        "developer_message": "Bad Request. Missing required parameter"
        "user_message": "Missing required parameter"
    }


Retrieve Payment Gateways
-------------------------

.. http:post:: /v1/payment_gateways

    Retrieve payment gateways.


    Optional:

    * `is_active` - Filter by is_active status(es). Response will only returns if any matched

    * `fields` - Retrieve specific fields. See [Payment Gateway Properties](#payment-gateway-properties). Default to ALL

    * `offset` - Offset of the records. Default to 0

    * `limit` - Limit of the records. Default to 9999

    * `order` - Order the records. Default to order ascending by primary identifier or created timestamp

    * `q` - Simple string query to records' string properties

    * `organization_id` - Filter by organization identifier(s). Response will returns if any matched

    **Method:** GET

    **Response HTTP Code:**

    * `200` - Payment gateway retrieved and returned in body content

    * `204` - No matched payment gateway

    **Response Params:** See [Payment Gateway Properties](#payment-gateway-properties)

    **Examples Response**

    .. sourcecode:: http

    GET /v1/payment_gateways HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json
    Content-Type: application/json; charset=utf-8

    {
        "payment_gateways": [
            {
                "id": 1,
                "name": "Apple In-App Purchase",
                "slug": "apple-in-app-purchase",
                "sort_priority": 1,
                "payment_flow_type": 4,
                "is_active": true,
                "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
                "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
                "cut_rule_id": 5,
                "base_currency_id": 1,
                "meta": null,
                "description": null,
                "mnc": null,
                "mcc": null,
                "sms_number": null,
                "organization_id": 3421,
                "clients": [13, 14],
                "minimal_amount": "25000"
            },
            {
                "id": 2,
                "name": "Paypal",
                "slug": "paypal",
                "sort_priority": 1,
                "payment_flow_type": 5,
                "is_active": true,
                "icon_image_normal": "payment_gateways/paypal/icon.png",
                "icon_image_highres": "payment_gateways/paypal/icon_highres.png",
                "cut_rule_id": 7,
                "base_currency_id": 2,
                "meta": null,
                "description": null,
                "mnc": null,
                "mcc": null,
                "sms_number": null,
                "organization_id": 3424,
                "clients": [13, 14],
                "minimal_amount": NULL
            }
        ],
        "metadata": {
            "resultset": {
                "count": 2,
                "offset": 0,
                "limit": 9999
            }
        }
    }

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Payment Gateway

Retrieve individual payment gateway using identifier.

**URL:** /payment_gateways/{payment_gateway_id}

**Alternative URL:** /payment_gateways/organization/{organization_id}

Optional:

* `fields` - Retrieve specific properties. See [Payment Gateway Properties](#payment-gateway-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Payment gateway found and returned

**Response Params:** See [Payment Gateway Properties](#payment-gateway-properties)

**Examples:**

Request:

```
GET /payment_gateways/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Apple In-App Purchase",
    "slug": "apple-in-app-purchase",
    "sort_priority": 1,
    "payment_flow_type": 4,
    "is_active": true,
    "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
    "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
    "cut_rule_id": 5,
    "base_currency_id": 1,
    "meta": null,
    "description": null,
    "mnc": null,
    "mcc": null,
    "sms_number": null,
    "organization_id": 3421,
    "clients": [13, 14]m
    "minimal_amount": "25000"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Payment Gateway

Update payment gateway.

**URL:** /payment_gateways/{payment_gateway_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`, `cut_rule_id`, `base_currency_id`, `organization_id`, `payment_flow_type`, `clients`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `mnc`, `mcc`, `sms_number`

**Response HTTP Code:**

* `200` - Payment gateway updated

**Response Params:** See [Payment Gateway Properties](#payment-gateway-properties)

**Examples:**

Request:

```
PUT /payment_gateways/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Apple In-App Purchase",
    "slug": "apple-in-app-purchase",
    "sort_priority": 1,
    "payment_flow_type": 4,
    "is_active": true,
    "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
    "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
    "cut_rule_id": 5,
    "base_currency_id": 1,
    "meta": "Apple In-App Purchase iTunes IAP,
    "organization_id": 3421,
    "clients": [13, 14, 15],
    "minimal_amount": "25000"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Apple In-App Purchase",
    "slug": "apple-in-app-purchase",
    "sort_priority": 1,
    "payment_flow_type": 4,
    "is_active": true,
    "icon_image_normal": "payment_gateways/apple-in-app-purchase/icon.png",
    "icon_image_highres": "payment_gateways/apple-in-app-purchase/icon_highres.png",
    "cut_rule_id": 5,
    "base_currency_id": 1,
    "meta": "Apple In-App Purchase iTunes IAP,
    "description": null,
    "mnc": null,
    "mcc": null,
    "sms_number": null,
    "organization_id": 3421,
    "clients": [13, 14, 15],
    "minimal_amount": "25000"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Payment Gateway

Delete payment gateway. This service will set `is_active` value to false.

**URL:** /payment_gateways/{payment_gateway_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Payment gateway deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /payment_gateways/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Payment Gateway Tiers

Payment gateway tier resource used to get which files are linked to an payment gateway

#### Payment Gateway Tier Properties

* `id` - Number; Instance identifier

* `payment_gateway_id` - Number; Payment gateway's identifier which this tier linked to

* `currency_id` - Number; Currency's identifier of the tier

* `tier_order` - Number; Tier order, this will used to define which tier the discount fell into

* `tier_price` - String; Tier price in 2 digit precision float, this will used to define which tier the discount fell into

* `tier_code` - String; Tier code, payment gateway side identifier of consumable type (Apple In-App Purchase, Google In-App Billing, etc)

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

#### Create Payment Gateway Tiers

Create new payment gateway tiers

**URL:** /payment_gateway_tiers

**Method:** POST

**Request Data Params:**

Required: `payment_gateway_id `, `currency_id `, `tier_order `, `tier_price `, `tier_code `, `is_active `

**Response HTTP Code:**

* `207` - Multiple status of each payment gateway tier

    * `201` -  New payment gateway tier created

**Response Params:** See [Payment Gateway Tier Properties](#payment-gateway-tier-properties)

**Examples:**

Request:

```
POST /payment_gateway_tiers HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "payment_gateway_tiers": [
        {
            "payment_gateway_id": 1,
            "currency_id": 1,
            "tier_order": 1,
            "tier_price" : "0.99",
            "tier_code": "c.usd.0.99",
            "is_active": true,
        },
        {
            "payment_gateway_id": 1,
            "currency_id": 1,
            "tier_order": 2,
            "tier_price" : "1.99",
            "tier_code": "c.usd.1.99",
            "is_active": true,
        }
    ]
}
```

Success response:

```
HTTP/1.1 207 Multi-Status
Content-Type: application/json; charset=utf-8

{
    "results":[
        {
            "status": 201,
            "id": 1,
            "payment_gateway_id": 1,
            "currency_id": 1,
            "tier_order": 1,
            "tier_price" : "0.99",
            "tier_code": "c.usd.0.99",
            "is_active": true,
        },
        {
            "status": 201,
            "id": 2,
            "payment_gateway_id": 1,
            "currency_id": 1,
            "tier_order": 2,
            "tier_price" : "1.99",
            "tier_code": "c.usd.1.99",
            "is_active": true,
        }
    ]
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```
