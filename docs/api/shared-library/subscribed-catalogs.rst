Subscribed Catalogs
===================

A collection of `Items` which a `User` may temporarily `borrow` and access.


List All Subscribed Catalogs
----------------------------

.. http:get:: /users/current-user/subscribed-catalogs

    Paginated list of all the user's currently-subscribed catalogs.

    **Example request**

    .. sourcecode:: http

        GET /users/current-user/subscribed-catalogs HTTP/1.1
        Accept: application/json
        Accept-Language: en-US
        Accept-Charset: utf-8
        Authorization: Bearer MTIzNDp3ZTRzY29vcA==
        User-Agent: Scoop IOS/4.7.0 (iOS 3.2.1; iPhone 6s)

    **Example response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        ETag: "737060cd8c284d8af7ad3082f209582d"
        Link: </users/current-user/subscribed-catalogs/123?limit=2>;rel=next;title=2, </users/current-user/subscribed-catalogs/123?limit=2>;rel=last;title=2

        {
            "catalogs" : [
                {
                    "id": 12,
                    "title": "Math Books",
                    "href" : "https://api.apps-foundry.com/organizations/1234/shared-catalogs/12"
                },
                {
                    "id": 13,
                    "title": "Geographic Books",
                    "href" : "https://api.apps-foundry.com/organizations/1234/shared-catalogs/13"
                }
            ],
            "metadata": {
                "resultset": {
                    "count": 3,
                    "limit": 2,
                    "offset": 0
                }
            }
        }


List Items Available within a Catalog
-------------------------------------

.. http:get:: /organizations/1234/shared-catalogs/(int:catalog_id)

    Searchable, Paginated List endpoint of Items available within a Catalog

    **Example request**

    .. sourcecode:: http

        GET /users/current-user/subscribed-catalogs/123?limit=2&q=Intro HTTP/1.1
        Accept: application/json
        Accept-Language: id-ID
        Accept-Charset: utf-8
        Authorization: Bearer MTIzNDp3ZTRzY29vcA==
        User-Agent: Scoop IOS/4.7.0 (iOS 3.2.1; iPhone 6s)

    **Example response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        ETag: "737060cd8c284d8af7ad3082f209582d"
        Link: </users/current-user/subscribed-catalogs/123?offset=20>;rel=next;title=2, </users/current-user/subscribed-catalogs/123?offset=20>;rel=last;title=2

        {
            "items" :[
                {
                    "id": 9998,
                    "title": "Intro to Algebra",
                    "href" : "https://api.apps-foundry.com/organizations/1234/shared-catalogs/12/9998",
                    "details": {
                        "href": "https://api.apps-foundry.com/items/9998",
                        "title": "Detil Item"
                    },
                    "borrow_item": {
                        "href": "https://api.apps-foundry.com/users/current-user/borrowed-items",
                        "title": "Pinjam Item",
                    },
                    "cover_image": {
                        "href": "https://images.getscoop.com/items/cover-images/9998.jpg",
                        "title": "Gambar Sampul"
                    },
                    "currently_available": 12,
                    "total_in_collection": 20,
                    "max_borrow_time": "P7D",
                    "next_available": null,
                    "authors": [
                        {
                          "href": "https://api.apps-foundry.com/authors/201",
                          "id": 201,
                          "title": "Imelda Akmal Architecture Writer Studio"
                        }
                    ],
                    "vendor": {
                        "href": "https://api.apps-foundry.com/vendors/251",
                        "title": "Penerbit Apa Saja"
                    },
                    "brand": {
                        "id": 123,
                        "title": "Brand ABC",
                        "href": "https://api.apps-foundry.com/brands/123"},
                    "description": "deskripsi dari item ini apa saja, isinya apa, description about this item zzz",
                    "watch_list": null,
                    "catalog": {
                        "id": 12,
                        "title": "Catalog KG",
                        "href": "https://api.apps-foundry.com/organizations/1234/shared-catalogs/12"}
                },
                {
                    "id": 9999,
                    "title": "Intro to Geometry",
                    "href" : "https://api.apps-foundry.com/organizations/1234/shared-catalogs/12/9999",
                    "borrow_item": {
                        "href": "https://api.apps-foundry.com/users/current-user/borrowed-items",
                        "title": "Pinjam Item",
                    },
                    "details": {
                        "href": "http://api.apps-foundry.com/items/9999",
                        "title": "Detil Item"
                    },
                    "cover_image": {
                        "href": "https://images.getscoop.com/items/cover-images/9999.jpg",
                        "title": "Gambar Sampul"
                    },
                    "currently_available": 0,
                    "total_in_collection": 20,
                    "max_borrow_time": "P10D",
                    "next_available": "2016-10-10T12:00:00Z",
                    "authors": [
                        {
                          "href": "https://api.apps-foundry.com/authors/203",
                          "id": 203,
                          "title": "Kera Sakti"
                        }
                    ],
                    "vendor": {
                        "href": "https://api.apps-foundry.com/vendors/253",
                        "title": "Penerbit Pusara"
                    },
                    "brand": {
                        "id": 123,
                        "title": "Brand ABC",
                        "href": "https://api.apps-foundry.com/brands/123"},
                    "description": "deskripsi dari item ini apa saja, isinya apa, description about this item",
                    "watch_list": {
                        "id": 11133,
                        "title": "watch list",
                        "href": "https://api.apps-foundry.com/watch-list/11133"},
                    "catalog": {
                        "id": 12,
                        "title": "Catalog KG",
                        "href": "https://api.apps-foundry.com/organizations/1234/shared-catalogs/12"}
                }
            ],
            "metadata": {
                "resultset": {
                    "count": 3,
                    "limit": 2,
                    "offset": 0
                },
                "facets": [
                    {
                        "field_name": "item_categories",
                        "values":[
                            {
                                "value": "Automotive",
                                "count": 2,
                            },
                            {
                                "value": "Crime & Thrillers",
                                "count": 1,
                            },
                        ]
                    },
                    {
                        "field_name": "item_authors",
                        "values": [
                            {
                                "value": "ramdani",
                                "count": 2,
                            }
                        ]
                    },
                    {
                        "field_name": "item_languages",
                        "values": [
                            {
                                "value": "id",
                                "count": 2,
                            },
                            {
                                "value": "en",
                                "count": 0,
                            }
                        ]
                    }
                ],

                "spelling_suggestions": [
                    {
                        "value": "sedekah",
                        "count": 8,
                    },
                    {
                        "value": "selekta",
                        "count": 1,
                    }
                ]
            }
        }

    :param catalog_id: A unique ID for a single catalog.  Can be discovered from `List All Subscribed Catalogs`

    :query q: Search term to be run against the API.
    :query facets: Key-value-pair to filter faceted results.  Each key-value-pair, is in
        the format of ``some_facet:Value``.  This query parameter may be defined multiple times.
    :query limit: The maximum number of results to return.
    :query offset: The number of beginning results to offset from the start.


    **Properties**

    id *(integer)*
        The item id
    title *(string)*
        Displayable text to represent the Item.
    href *(string-encoded URL)*
        A unique link to this Item.
    borrow_item *(hyperlink relationship object)*
        A relationship object.  Href is the URL where this object may be posted to **borrow** this item.
    details *(hyperlink relationship object)*
        The item detail information.
    currently_available *(integer)*
        The number of Items in this catalog that are available for borrowing.  If 0, the Item cannot be borrowed
        at the current time.
    total_in_collection *(integer)*
        The total number of items that can be borrowed from this collection.
    max_borrow_time *(string - iso8601 duration)*
        The maximum amount of time this Item is available to the user, after borrowing, before it must be renewed
        or returned.
    next_available *(string - iso8601 datetime)*
        The datetime this item will be avaialble.  If currently_available > 0, this value will be null.


    **Metadata Properties**

    resultset
        count
            The total number of objects matching your query.
        limit
            The maximum number of objects displayed per page.
        offset
            The number of objects skipped at the beginning of the list.

    facets
        List of available facets from this endpoint.
        Inside the list will be  objects that match the faceting.

    spelling_suggestions
        Object containing alternate spelling suggestions for 'q', along with the
        the number of matches for the alternate spelling.


List Suggestion of Brand Name Available within a Catalog
--------------------------------------------------------

.. http:get:: /organizations/<int:org_id>/shared-catalogs/<int:catalog_id>/suggest

    Searchable, Paginated List endpoint of Items available by Brand Name

    **Example request**

    .. sourcecode:: http

        GET /organizations/1234/shared-catalogss/1/suggest?q=<brand_name> HTTP/1.1
        Accept: application/json
        Accept-Language: id-ID
        Accept-Charset: utf-8
        Authorization: Bearer MTIzNDp3ZTRzY29vcA==
        User-Agent: Scoop IOS/4.7.0 (iOS 3.2.1; iPhone 6s)

    **Example response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        ETag: "737060cd8c284d8af7ad3082f209582d"

        [
           "<b>Tribun</b> Pekanbaru",
           "<b>Tribun</b> Sumsel",
           "<b>Tribun</b> Batam",
           "<b>Tribun</b> Pekanbaru",
           "<b>Tribun</b> Pontianak",
           "<b>Tribun</b> Bali",
           "<b>Tribun</b> Manado",
           "<b>Tribun</b> Timur",
           "<b>Tribun</b> Pekanbaru",
           "<b>Tribun</b> Jabar"
        ]


    :query q: Search term to be run against the API.

    **Properties**

    suggestions *(list - list of string)*
        Top 10 list available to suggest


