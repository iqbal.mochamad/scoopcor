Watch List
==========

A collection of `Items` which a `User` watch.


List all item in user watch list
--------------------------------

.. http:get:: /users/current-user/watch-list

    Paginated list of all item in user's watch list.

    **Example request**

    .. sourcecode:: http

        GET /users/current-user/watch-list HTTP/1.1
        Accept: application/json
        Accept-Language: en-US
        Accept-Charset: utf-8
        Authorization: JWT MTIzNDp3ZTRzY29vcA==
        User-Agent: eperpus kg ios/3.0.0 (iOS 3.2.1; iPhone 6s)
        X-Scoop-Client: eperpus kg ios/3.0.0 (iOS 3.2.1; iPhone 6s)

    **Example response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        ETag: "737060cd8c284d8af7ad3082f209582d"

        {
            "items" :[
                {
                    "id": 1,
                    "title": "Intro to Algebra",
                    "href" : "https://api.apps-foundry.com/organizations/1234/shared-catalogs/12/9998",
                    "details": {
                        "href": "https://api.apps-foundry.com/items/9998",
                        "title": "Detil Item"
                    },
                    "borrow_item": {
                        "href": "https://api.apps-foundry.com/users/current-user/borrowed-items",
                        "title": "Pinjam Item",
                    },
                    "cover_image": {
                        "href": "https://images.getscoop.com/items/cover-images/9998.jpg",
                        "title": "Gambar Sampul"
                    },
                    "currently_available": 12,
                    "total_in_collection": 20,
                    "max_borrow_time": "P7D",
                    "next_available": null,
                    "vendor": {
                        "href": "https://api.apps-foundry.com/vendors/251",
                        "title": "Penerbit Apa Saja"
                    }
                },
                {
                    "id": 2,
                    "title": "Intro to Geometry",
                    "href" : "https://api.apps-foundry.com/organizations/1234/shared-catalogs/12/9999",
                    "borrow_item": {
                        "href": "https://api.apps-foundry.com/users/current-user/borrowed-items",
                        "title": "Pinjam Item",
                    },
                    "details": {
                        "href": "http://api.apps-foundry.com/items/9999",
                        "title": "Detil Item"
                    },
                    "cover_image": {
                        "href": "https://images.getscoop.com/items/cover-images/9999.jpg",
                        "title": "Gambar Sampul"
                    },
                    "currently_available": 0,
                    "total_in_collection": 20,
                    "max_borrow_time": "P10D",
                    "next_available": "2016-10-10T12:00:00Z",
                    "vendor": {
                        "href": "https://api.apps-foundry.com/vendors/253",
                        "title": "Penerbit Pusara"
                    }
                }
            ],
            "metadata": {
                "resultset": {
                    "count": 100,
                    "limit": 2,
                    "offset": 0
                }
            }
        }

    :query limit: The maximum number of results to return.
    :query offset: The number of beginning results to offset from the start.


    **Properties**

    id *(integer)*
        The id of this watch list
    title *(string)*
        Displayable text to represent the Item.
    href *(string-encoded URL)*
        A unique link to this Item. Post this href in request body to Borrow this item (see borrow an item API)
    borrow_item *(hyperlink relationship object)*
        A relationship object.  Href is the URL where this object may be posted to **borrow** this item.
    details *(hyperlink relationship object)*
        The item detail information.
    currently_available *(integer)*
        The number of Items in this catalog that are available for borrowing.  If 0, the Item cannot be borrowed
        at the current time.
    total_in_collection *(integer)*
        The total number of items that can be borrowed from this collection.
    max_borrow_time *(string - iso8601 duration)*
        The maximum amount of time this Item is available to the user, after borrowing, before it must be renewed
        or returned.
    next_available *(string - iso8601 datetime)*
        The datetime this item will be avaialble.  If currently_available > 0, this value will be null.


    **Metadata Properties**

    resultset
        count
            The total number of objects matching your query.
        limit
            The maximum number of objects displayed per page.
        offset
            The number of objects skipped at the beginning of the list.


Add an item to user's watch list
--------------------------------

required in request body:
href
    Url; A unique link to an Item in a catalog (exists in catalog item response)
        see example bellow

.. http:post:: /v1/users/current-user/watch-list

    Add an item to user's watch list.

    **Example Request**

    .. sourcecode:: http

        POST /v1/users/current-user/watch-list HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        Authorization: JWT ajksfhiau12371894^&*@&^$%.asdfsafs.reerer
        User-Agent: eperpus kg ios/3.0.0 (iOS 3.2.1; iPhone 6s)
        X-Scoop-Client: eperpus kg ios/3.0.0 (iOS 3.2.1; iPhone 6s)

        {
            "href" : "https://api.apps-foundry.com/organizations/1234/shared-catalogs/12/9998"
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 204 NO CONTENT
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT



Remove an item from user's watch list
-------------------------------------

id
    Number; Watch list instance identifier (from watch list get list API response above).


.. http:delete:: /v1/users/current-user/watch-list/<int:id>

    remove an item from user's watch list.

    **Example Request**

    .. sourcecode:: http

        DELETE /v1/users/current-user/watch-list/123 HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Authorization: JWT ajksfhiau12371894^&*@&^$%.asdfsafs.reerer
        User-Agent: eperpus kg ios/3.0.0 (iOS 3.2.1; iPhone 6s)
        X-Scoop-Client: eperpus kg ios/3.0.0 (iOS 3.2.1; iPhone 6s)


    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 204 NO_CONTENT
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

