Shared-Library
==============

Shared library allows `Users` to temporarily access `Items` from a set of Shared Catalogs which have been
shared with them.

.. toctree::

    subscribed-catalogs
    ../users/borrowed-items
    ../wallet/organization-wallet


Key Concepts
------------

`Users` belong to zero-or-many `Organizations` of type *shared-library* (hereafter referred to simply as
**shared libraries**.

Shared Libraries have zero-or-more Catalogs.

Catalogs contain many Items that may be *borrowed* by a User.

Once an Item is borrowed, it is available to the User for a limited amount of time.
