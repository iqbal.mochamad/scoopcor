## SCOOP POINTS

### Points

Point resource contains use case for user's point summary, acquire, and use

**Point expiring**

Point expires datetime set to 12 months and rounded up to last seconds of the expiring month, with UTC timezone

#### Retrieve Individual User Point Summary

Retrieve individual user point summary

**Point Summary Properties**

* `used_points` - Number; Number of used points

* `available_points` - Number; Number of points available to use

* `user_id` - Number; User identifier

**URL:** /points/summary/{user_id}

**Method:** GET

**Response HTTP Code:**

* `200` -  Points summary retrieved and returned in body content

**Response Params:** See [Point Summary Properties](#point-summary-properties)

**Examples:**

Request:

```
GET /points/summary/5 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    used_points: 475
    available_points: 0
    user_id: 5
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Retrieve All User Point Summary

Retrieves all user point summary

**Point Summary Properties**

* `points_used` - Number; Number of used points

* `point_amount` - Number; Number of points available to use

* `user_id` - Number; User identifier

* `is_active` - Boolean; Instance's status, used to mark if the instance is active or not. Default value is true

* `expired_datetime` - String; Datetime to which the points are available to use. See **point expiring**

* `client_id` - Number; Client identifier

* `acquire_datetime` - String; Datetime from which the acquired points are available to use

* `id` - Number; Instance identifier

**URL:** /points

**Method:** GET

**Response HTTP Code:**

* `200` -  Points summary retrieved and returned in body content

**Response Params:** See [Point Summary Properties](#point-summary-properties)

**Examples:**

Request:

```
GET /points HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "points": [
        {
            "user_id": 5
            "is_active": true
            "expired_datetime": "2015-08-21T16:55:44.286315"
            "point_used": 26665
            "client_id": 7
            "point_amount": 999985440
            "acquire_datetime": "2014-08-21T16:55:44.288066"
            "id": 3
        },
        {
            "user_id": 329438
            "is_active": true
            "expired_datetime": "2014-08-02T00:00:00"
            "point_used": 909999
            "client_id": 80
            "point_amount": 909999
            "acquire_datetime": "2014-08-01T00:00:00"
            "id": 12
        },
        {...}
    ],
    "metadata": {
        "resultset": {
            "count": 7,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Retrieve Individual Point Summary

Retrieves point summary by point id

**Point Summary Properties**

* `points_used` - Number; Number of used points

* `point_amount` - Number; Number of points available to use

* `user_id` - Number; User identifier

* `is_active` - Boolean; Instance's status, used to mark if the instance is active or not. Default value is true

* `expired_datetime` - String; Datetime to which the points are available to use. See **point expiring**

* `client_id` - Number; Client identifier

* `acquire_datetime` - String; Datetime from which the acquired points are available to use

* `id` - Number; Instance identifier

**URL:** /points/{point_id}

**Method:** GET

**Response HTTP Code:**

* `200` -  Points summary retrieved and returned in body content

**Response Params:** See [Point Summary Properties](#point-summary-properties)

**Examples:**

Request:

```
GET /points/99 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "user_id": 777,
    "is_active": true,
    "expired_datetime": "2020-12-31T23:59:59",
    "point_used": 0,
    "client_id": 0,
    "point_amount": 27,
    "acquire_datetime": "2012-12-22T04:50:13",
    "id": 99
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```

#### Add Points

Adds points, add some amount of points to certain user id

**URL:** /points

**Method:** POST

**Request Data Params:**

Required:

* `user_id` - Number; User identifier who acquiring points

* `is_active` - Boolean; Instance's status, used to mark if the instance is active or not. Default value is true

* `expired_datetime` - String; Datetime to which the points are available to use. See **point expiring**

* `point_used` - Number; The amount of used point

* `client_id` - Number; Client identifier that performed a request to acquire points

* `point_amount` - Number; Amount of points to be acquired

* `acquired_datetime` - String; Datetime from which the acquired points are available to use

Optional:

* `details` - String; Details of this acquire action

**Response HTTP Code:**

* `200` - Points successfully acquired by user

**Response Params:** -

**Examples:**

Request:

```
POST /points HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "user_id": 290411,
    "is_active": true,
    "expired_datetime": "2015-08-20T12:09:05.835515",
    "point_used": 290,
    "client_id": 2,
    "point_amount": 510,
    "acquire_datetime": "2014-08-20T12:09:05.835496"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "user_id": 290411,
    "is_active": true,
    "expired_datetime": "2015-08-20T12:09:05.835515",
    "point_used": 290,
    "client_id": 2,
    "point_amount": 510,
    "acquire_datetime": "2014-08-20T12:09:05.835496"
}
```

Fail response:

```
HTTP/1.1 403 Forbidden
Content-Type: application/json; charset=utf-8

{
    "status": 403,
    "error_code": 403102,
    "developer_message": "Forbidden. Access denied"
    "user_message": "Access denied"
}
```
