Wallet
======

Resource for debit, credit and check balance of 'Shared Library' organization.



Credit / Add Wallet
-------------------

.. http:post:: /v1/orders/checkout

    Add new wallet or update existing wallet amount

    **Example Request**

    .. sourcecode:: http

        POST v1/orders/checkout HTTP/1.1
        Content-Type: application/json
        Authorization: Bearer ajksfhiau12371894^&*@&^$%
        {
            "user_id": 711,
            "payment_gateway_id": 25,
            "client_id": 7,
            "platform_id": 4,
            "order_lines": [
                {
                    "offer_id": 68522,
                    "quantity": 1,
                    "name": "Top Up Wallet",
                    "top_up_amount": 100000
                }
            ],
            "user_email": "ram@apps-foundry.com",
            "user_name": "",
            "user_street_address": "",
            "user_city": "Jakarta",
            "user_zipcode": "",
            "user_state": "",
            "user_country": "Indonesia",
            "latitude": -6.1744,
            "longitude": 106.8294,
            "ip_address": "202.179.188.106",
            "os_version": "Mac OS X",
            "device_model": "Chrome 51.0.2704.84",
            "client_version": "3.10.1"
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 201 Created
        Content-Type: application/json; charset=utf-8
        Location: https://scoopadm.apps-foundry.com/scoopcor/api/payment_gateways/1
        {
            "user_id": 711,
            "payment_gateway_id": 25,
            "client_id": 7,
            "platform_id": 4,
            "order_lines": [
                {
                    "offer_id": 68522,
                    "quantity": 1,
                    "name": "Top Up Wallet",
                    "top_up_amount": 100000
                }
            ],
            "user_email": "ram@apps-foundry.com",
            "user_name": "",
            "user_street_address": "",
            "user_city": "Jakarta",
            "user_zipcode": "",
            "user_state": "",
            "user_country": "Indonesia",
            "latitude": -6.1744,
            "longitude": 106.8294,
            "ip_address": "202.179.188.106",
            "os_version": "Mac OS X",
            "device_model": "Chrome 51.0.2704.84",
            "client_version": "3.10.1"
        }


Check Wallet Balance Amount
---------------------------

.. http:get:: /organizations/(int:org_id)/wallet

    Returns wallet information that is owned by organizaton.

    **Example Request**

    .. sourcecode:: http

        GET /organizations/711/wallet
        {
          "balance": 100000,
          "transactions": {
            "href": "/organizations/711/wallet/transactions",
            "title": "Transaction History"
          }
        }


Check Wallet Transaction List
-----------------------------

.. http:get:: /organizations/(int:org_id)/wallet/transactions

    Return wallet transaction list that is owned by organization

    **Example Request**

    .. sourcecode:: http

        GET /organizations/711/wallet/transactions
        {
          "metadata": {
            "resultset": {
              "count": 1,
              "limit": 20,
              "offset": 0
            }
          },
          "wallet_transactions": [
            {
              "amount": 100000,
              "date": "2016-07-19T05:59:34+00:00",
              "href": "http://dev5.apps-foundry.com:5000/v1/orders/819254",
              "title": "Summary Transaction"
            }
          ]
        }

    :query offset: The number of beginning results to offset from the start
    :query limit: The maximum number of result to return.


    **Properties**

    **Metadata Properties**

    resultset
        count
            The total number of objects matching your query.
        limit
            The maximum number of objects displayed per page.
        offset
            The number of objects skipped at the beginning of the list.
