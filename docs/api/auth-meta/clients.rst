Clients
=======

Client resource contains client application that accessing other resources.

Every single application that can access to resources must be registered as client,
including resource servers that need access to non-owned resources, e.g., SCOOP CORE server
need to access SCOOP CAS resource, then SCOOP CORE must registered as a client with proper
access scope.

This resource also part of OAuth2's client implementation.


**TL;DR**

Clients represent an 'Application' that accesses our APIs, such as 'SCOOP Newsstand iOS'.
Each client also maintains information about the Versions of that application that
are able to access our APIs.

Version 2
---------

.. deprecated::

    This API has been depracted.  Please switch to Version 3 as soon as
    practical.

+-----------------------------------------------------------------------------------+
|                               Endpoint Information                                |
+============================+======================================================+
| Url                        | /clients[/{user.id}]                                 |
+----------------------------+------------------------------------------------------+
| HTTP Method                | POST, PUT, DELETE, GET                               |
+----------------------------+------------------------------------------------------+
| Request Headers / Response | :doc:`/api/v2-common/requests`                       |
+----------------------------+------------------------------------------------------+
| Error Response             | :doc:`/api/v2-common/errors`                         |
+----------------------------+------------------------------------------------------+


**JSON Properties**

client_id (Number)
    Client identifier provided by user when creating the client

app_name (String; Required; Unique)
    Migration property from old system. On new clients don't use this as
    identifier anymore **(TODO: deprecate after old system dropped)**

name (String; Required; Unique)
    Client name

description (String)
    Client description

client_secret (String)
    Client secret to access resource **(TODO: not yet implemented)**

redirect_url (String)
    OAuth2 authorization redirection URL **(TODO: not yet implemented)**

user_id (Number)
    User identifier that bond to this client and  user's access token used
    to access resources **This is unused**

client_type (String) **This is unused**
    OAuth2 client type:

    * condifential: client that capable of maintaining the confidentiality of their credentials
        (e.g., client implemented on a secure server with restricted access to the client credentials),
        or capable of secure client authentication using other means
    * public:  client that incapable of maintaining the confidentiality of their
        credentials (e.g., clients executing on the device used by the
        resource owner, such as an installed native application or a web
        browser-based application), and incapable of secure client
        authentication via any other means

version (String)
    Client latest available version. On UI side, this property can be used to check if user
    installed version is latest one

lowest_supported_version (String)
    Client lowest supported version. On UI side, this property can be used to check if user
    installed version already outdated

scopes (Array)
    Client default scopes, this scopes will assigned to token when client auth'ed using
    **client_id** or **client_secret** or user auth'ed from this client without providing any scopes
    information. **(TODO: not yet implemented, currently there's only single global default scopes)**

external_bundle_name (String)
    External bundle/namespace/package name of the client, e.g., for Android app there's Google
    namespace like com.apps-foundry.scoop

external_identifier (Number)
    External identifier of the client, e.g., for iOS app there's iTunes application identifier

url (String)
    URL of the app, for web app, this will be the URL of production web, for mobile app this
    will be the app store link, for backend app this will be the host, etc

ip_address (String)
    IP Address of this client. For certain web-based app, this the main IP used for handshake the access

flurry_key (String)
    Client's Flurry analytic application key

gai_key (String)
    Client's Google analytic application key

is_active (Boolean; Default 'true')
    Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

slug (String; Unique; Required)
    Item type slug for SEO

meta (String)
    Item type slug for SEO


Examples
^^^^^^^^

Create
~~~~~~

**Request**

.. code-block:: http

    PUT /clients/1 HTTP/1.1
    Accept : application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "name": "SCOOP Newsstand iOS",
        "is_active": true,
        "slug": "scoop-newsstand-ios",
        "app_name": "scoop_ios",
        "client_secret": "Lorem ipsum",
        "redirect_url": null,
        "client_type": "confidential",
        "flurry_key": "ASD12SA3CGAA",
        "gai_key": "U-4137361-1",
        "version": "3.8.0",
        "lowest_supported_version": "3.5.0",
        "scopes": ["can_read_write_public", "can_read_write_public_ext"],
        "user_id": 1,
        "description": "SCOOP Newsstand iOS application",
        "external_bundle_name": "com.appsfoundry.scoop",
        "external_identifier": 402166944,
        "url": "https://itunes.apple.com/id/app/scoop-magazine-book-newspaper/id402166944?mt=8",
        "ip_address": null,
        "meta": "SCOOP Newsstand iOS application, magazines, books, newspapers, fhm, kompas, autocar, popular"
        "client_versions": [
            {
                "version": "4.3",
                "is_active": True,
                "enable_scoop_point": True
            },
            {
                "version": "4.2",
                "is_active": true,
                "enable_scoop_point": True
            }
        ]
    }


**Response**

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/clients/1

.. code-block:: json

    {
        "client_id": 1,
        "name": "SCOOP Newsstand iOS",
        "is_active": true,
        "slug": "scoop-newsstand-ios",
        "app_name": "scoop_ios",
        "client_secret": "Lorem ipsum",
        "redirect_url": null,
        "client_type": "confidential",
        "flurry_key": "ASD12SA3CGAA",
        "gai_key": "U-4137361-1",
        "version": "3.8.0",
        "lowest_supported_version": "3.5.0",
        "scopes": ["can_read_write_public", "can_read_write_public_ext"],
        "user_id": 1,
        "description": "SCOOP Newsstand iOS application",
        "external_bundle_name": "com.appsfoundry.scoop",
        "external_identifier": 402166944,
        "url": "https://itunes.apple.com/id/app/scoop-magazine-book-newspaper/id402166944?mt=8",
        "ip_address": null,
        "meta": "SCOOP Newsstand iOS application, magazines, books, newspapers, fhm, kompas, autocar, popular"
        "client_versions": [
            {
                "version": "4.3",
                "is_active": True,
                "enable_scoop_point": True
            },
            {
                "version": "4.2",
                "is_active": true,
                "enable_scoop_point": True
            }
        ]

    }




..
    #### Retrieve Clients

    Retrieve clients.

    This only accessible with administrator level of permission. Each client should only be able to access it owned details.

    **URL:** /clients

    Optional:

    * `fields` - Retrieve specific fields. See [Client Properties](#client-properties). Default to ALL

    * `offset` - Offset of the records. Default to 0

    * `limit` - Limit of the records. Default to 9999

    * `order` - Order the records. Default to order ascending by primary identifier or created timestamp

    * `q` - Simple string query to records' string properties

    **Method:** GET

    **Request Data Params:** -

    **Response Success HTTP Code:**

    * `200` - Client retrieved and returned in body content

    * `204` - No matched client

    **Response Params:** See [Client Properties](#client-properties)

    **Examples:**

    Request:

    ```
    GET /clients HTTP/1.1
    Accept : application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    ```

    Success response:

    ```
    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

    {
        "clients": [
            {
                "client_id": 1,
                "name": "SCOOP Newsstand iOS",
                "is_active": true,
                "slug": "scoop-newsstand-ios",
                "app_name": "scoop_ios",
                "client_secret": "Lorem ipsum",
                "redirect_url": null,
                "client_type": "confidential",
                "flurry_key": "ASD12SA3CGAA",
                "gai_key": "U-4137361-1",
                "version": "3.8.0",
                "lowest_supported_version": "3.5.0",
                "scopes": ["can_read_write_public", "can_read_write_public_ext"],
                "user_id": 1,
                "description": "SCOOP Newsstand iOS application",
                "external_bundle_name": "com.appsfoundry.scoop",
                "external_identifier": 402166944,
                "url": "https://itunes.apple.com/id/app/scoop-magazine-book-newspaper/id402166944?mt=8",
                "ip_address": null,
                "meta": "SCOOP Newsstand iOS application, magazines, books, newspapers, fhm, kompas, autocar, popular"
                "client_versions": [
                    {
                        "version": "4.3",
                        "is_active": True,
                        "enable_scoop_point": True
                    },
                    {
                        "version": "4.2",
                        "is_active": true,
                        "enable_scoop_point": True
                    }
                ]
            }
        ],
        "metadata": {
            "resultset": {
                "count": 1,
                "offset": 0,
                "limit": 9999
            }
        }
    }
    ```

    Fail response:

    ```
    HTTP/1.1 500 Internal Server Error
    ```

    #### Retrieve Individual Client

    Retrieve individual client.

    **URL:** /clients/{client_id}

    Optional:

    * `fields` - Retrieve specific properties. See [Client Properties](#client-properties). Default to ALL

    **Method:** GET

    **Request Data Params:** -

    **Response HTTP Code:**

    * `200` - Client found and returned

    **Response Params:** See [Client Properties](#client-properties)

    **Examples:**

    Request:

    ```
    GET /clients/1 HTTP/1.1
    Accept : application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    ```

    Success response:

    ```
    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

    {
        "client_id": 1,
        "name": "SCOOP Newsstand iOS",
        "is_active": true,
        "slug": "scoop-newsstand-ios",
        "app_name": "scoop_ios",
        "client_secret": "Lorem ipsum",
        "redirect_url": null,
        "client_type": "confidential",
        "flurry_key": "ASD12SA3CGAA",
        "gai_key": "U-4137361-1",
        "version": "3.8.0",
        "lowest_supported_version": "3.5.0",
        "scopes": ["can_read_write_public", "can_read_write_public_ext"],
        "user_id": 1,
        "description": "SCOOP Newsstand iOS application",
        "external_bundle_name": "com.appsfoundry.scoop",
        "external_identifier": 402166944,
        "url": "https://itunes.apple.com/id/app/scoop-magazine-book-newspaper/id402166944?mt=8",
        "ip_address": null,
        "meta": "SCOOP Newsstand iOS application, magazines, books, newspapers, fhm, kompas, autocar, popular"
        "client_versions": [
            {
                "version": "4.3",
                "is_active": True,
                "enable_scoop_point": True
            },
            {
                "version": "4.2",
                "is_active": true,
                "enable_scoop_point": True
            }
        ]
    }
    ```

    Fail response:

    ```
    HTTP/1.1 404 Not Found
    ```

    #### Delete Client

    Delete client. This service will set `is_active` value to false.

    **URL:** /clients/{client_id}

    **Method:** DELETE

    **Request Data Params:** -

    **Response HTTP Code:**

    * `204` - Client deleted

    **Response Params:** -

    **Examples:**

    Request:

    ```
    DELETE /clients/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    ```

    Success response:

    ```
    HTTP/1.1 204 No Content
    ```

    Fail response:

    ```
    HTTP/1.1 404 Not Found
    ```





