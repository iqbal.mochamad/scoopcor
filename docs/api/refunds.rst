### Refunds

Refunds resource contains all the properties within the transaction refunds.

#### Refund Properties

* `id` - Number; Instance identifier

* `pic_id` - Number; User identifier

* `origin_payment_id` - Number; Payment identifier

* `origin_order_id` -  Number; Order identifier

* `origin_order_line_id` -  Number; Order line identifier

* `refund_amount` -  String; Amount of the transaction refund in float format

* `refund_currency_id` - Number; Currency identifier

* `refund_reason` - Number; Reasons why the transaction is refunded

    * 1: Duplicate Payment

    * 2: Fraud Payment

    * 3: System Error

    * 4: Wrong Purchase

    * 5: Subcription is Discontinued

    * 6: Marketing Campaign Price Issues

    * 99: Others

* `refund_datetime` -  String; Datetime in UTC when the refund takes place

* `finance_status` - Number; Status of the transaction refund

    * 1: REFUND PENDING

    * 2: REFUND IN PROGRESS

    * 3: REFUND IS COMPLETED

* `ticket_number` -  String; JIRA ticket where the transaction refund is requested

* `note` -  String; Additional description

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. The default value is true.

#### Create Refund

Makes new transaction refund.

**URL:** /refunds

**Method:** POST

**Request Data Params:**

Required: `pic_id`, `origin_payment_id`, `origin_order_id`, `origin_order_line_id`, `refund_amount`, `refund_currency_id`, `refund_reason`, `refund_datetime`, `finance_status`

Optional: `ticket_number`, `note`, `is_active`

**Response HTTP Code:**

* `201` - New refund data is created

**Response Params:** See [Refund Properties](#refund-properties)

**Examples:**

Request:

```
POST /refunds HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "pic_id": 379890,
    "origin_payment_id": 45715,
    "origin_order_id": 46661,
    "origin_order_line_id": 55835,
    "refund_amount": "0.99",
    "refund_currency_id": 1,
    "refund_reason": 1,
    "refund_datetime": "2014-06-01T00:00:00Z",
    "finance_status": 1,
    "ticket_number": "JIRA-9000",
    "note": "loremipsum",
    "is_active": true
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/refund/2

{
    "id": 1,
    "pic_id": 379890,
    "origin_payment_id": 45715,
    "origin_order_id": 46661,
    "origin_order_line_id": 55835,
    "refund_amount": "0.99",
    "refund_currency_id": 1,
    "refund_reason": 1,
    "refund_datetime": "2014-06-01T00:00:00Z",
    "finance_status": 1,
    "ticket_number": "JIRA-9000",
    "note": "loremipsum",
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Refunds

Retrieves all of the transaction refunds.

**URL:** /refunds

Optional:

* `pic_id` - Number; Filter by user identifier

* `origin_payment_id` - Number; Filter by payment identifier

* `origin_order_id` -  Number; Filter by order identifier

* `origin_order_line_id` -  Number; Filter by order line identifier

* `refund_amount` -  String; Filter by amount of the transaction refund in float format

* `refund_currency_id` - Number; Filter by currency identifier

* `refund_reason` - Number; Reasons why the transaction is refunded

    * 1: Duplicate Payment

    * 2: Fraud Payment

    * 3: System Error

    * 4: Wrong Purchase

    * 5: Subcription is Discontinued

    * 6: Marketing Campaign Price Issues

    * 99: Others

* `refund_datetime_lt`, `refund_datetime_lte`, `refund_datetime_gt`, `refund_datetime_gte` -  String; Datetime in UTC when the refund takes place

* `finance_status` - Number; Status of the transaction refund

    * 1: REFUND PENDING

    * 2: REFUND IN PROGRESS

    * 3: REFUND IS COMPLETED

* `expand` - Expand the result details. Default to null, the supported expand for items are as below

    * ext: get all related filters as object or array of objects contains identifier and name. See [Item Properties](#item-properties)

    * display_offers: get display offers as array and also offer's meta informations (this expand value is covered in "ext")

    * parental_control: get related parental_control as object (this expand value is covered in "ext")

    * item_type: get related item_type as object (this expand value is covered in "ext")

    * brand: get related brand as object (this expand value is covered in "ext")

    * vendor: get related vendor as object (this expand value is covered in "ext")

    * authors: get all related authors as array of objects (this expand value is covered in "ext")

    * categories: get all related categories as array of objects (this expand value is covered in "ext")

    * languages: get all related languages as array of objects (this expand value is covered in "ext")

    * countries: get all related countries as array of objects (this expand value is covered in "ext")

* `is_active` - Filter by is_active status(es).

* `client_id` - Number; List of client identifier(s). Used to identify on which client(s) the refund will be shown.

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

**Method:** GET

**Response HTTP Code:**

* `200` - Refund data are retrieved and returned in body content

* `204` - No matched refund

**Response Params:** See [Refund Properties](#refund-properties)

**Examples:**

Request:

Retrieve refund based on the status and the currency id

```
GET /refunds?is_active=true&refund_currency_id=2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Retrieve refund with expand = ext

```
GET /refunds?id=2&expand=ext&limit=1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response (default properties):

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "refunds": [
        {
            "id": 9,
            "pic_id": 379890,
            "origin_payment_id": 45715,
            "origin_order_id": 46661,
            "origin_order_line_id": 55835,
            "refund_amount": "0.99",
            "refund_currency_id": 2,
            "refund_reason": 1,
            "refund_datetime": "2014-06-01T00:00:00Z",
            "finance_status": 1,
            "ticket_number": "JIRA-9000",
            "note": "loremipsum",
            "is_active": true
        },
        {
            "id": 10,
            "pic_id": 379890,
            "origin_payment_id": 45715,
            "origin_order_id": 46661,
            "origin_order_line_id": 55835,
            "refund_amount": "0.99",
            "refund_currency_id": 2,
            "refund_reason": 1,
            "refund_datetime": "2014-06-01T00:00:00Z",
            "finance_status": 1,
            "ticket_number": "JIRA-9000",
            "note": "loremipsum",
            "is_active": true
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 10
        }
    }
}
```

Success response with "ext" as expand value:

```
{
    "refunds": [
        {
            "origin_orderline_id": 55835,
            "finance_status": 1,
            "order_line": {
                "is_discount": false,
                "user_id": 379890,
                "name": "Nyata / ED 2267 2014",
                "order_id": 46661,
                "offer_id": 36056,
                "is_active": true,
                "campaign_id": null,
                "is_free": false,
                "order_line_status": 90000,
                "price": "0.99",
                "payment_gateway_id": 15,
                "final_price": "0.99",
                "id": 55835,
                "currency_code": "USD",
                "quantity": 1
                },
            "is_active": true,
            "refund_amount": "0.99",
            "payment": {
                "payment_datetime": "2014-12-16T09:05:05.372397",
                "financial_archive_date": null,
                "user_id": 379890,
                "created": "2014-12-16T09:04:29.460938",
                "order_id": 46661,
                "payment_status": 20003,
                "modified": "2014-12-16T09:05:06.406177",
                "amount": "0.99",
                "paymentgateway_id": 15,
                "id": 45715,
                "currency_code": "USD"
                },
            "note": "loremipsum",
            "refund_datetime": "2014-06-01T00:00:00",
            "origin_payment_id": 45715,
            "pic_id": 379890,
            "ticket_number": "JIRA-9000",
            "origin_order_id": 46661,
            "order": {
                "payment_id": 45715,
                "user_id": 379890,
                "total_amount": "0.99",
                "temporder_id": 54681,
                "tier_code": ".c.usd.0.99",
                "order_id": 46661,
                "partner_id": null,
                "is_active": true,
                "final_amount": "0.99",
                "platform_id": 2,
                "payment_gateway_id": 15,
                "client_id": 2,
                "point_reward": 9,
                "order_status": 90000,
                "order_number": 637123472741109,
                "currency_code": "USD"
                },
            "refund_reason": 1,
            "id": 2
        }
    ],
    "metadata": {
        "resultset": {
            "count": 7,
            "limit": 1,
            "offset": 0
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Refund

Retrieves individual refund by using identifier.

**URL:** /refunds/{refund_id}

Optional:

* `fields` - Retrieve specific properties. See [Refund Properties](#refund-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Refund data is found and returned

**Response Params:** See [Refund Properties](#refund-properties)

**Examples:**

Request:

```
GET /refund/2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "pic_id": 379890,
    "origin_payment_id": 45715,
    "origin_order_id": 46661,
    "origin_order_line_id": 55835,
    "refund_amount": "0.99",
    "refund_currency_id": 2,
    "refund_reason": 1,
    "refund_datetime": "2014-06-01T00:00:00Z",
    "finance_status": 1,
    "ticket_number": "JIRA-9000",
    "note": "loremipsum",
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Refund

Updates refund data.

**URL:** /refunds/{refund_id}

**Method:** PUT

**Request Data Params:**

Required: `pic_id`, `origin_payment_id`, `origin_order_id`, `origin_order_line_id`, `refund_amount`, `refund_currency_id`, `refund_reason`, `refund_datetime`, `finance_status`

Optional: `ticket_number`, `note`, `is_active`

**Response HTTP Code:**

* `200` - Refund is updated

**Response Params:** See [Refund Properties](#refund-properties)

**Examples:**

Request:

```
PUT /refunds/2 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "pic_id": 379890,
    "origin_payment_id": 45715,
    "origin_order_id": 46661,
    "origin_order_line_id": 55835,
    "refund_amount": "0.99",
    "refund_currency_id": 2,
    "refund_reason": 99,
    "refund_datetime": "2014-06-01T00:00:00Z",
    "finance_status": 1,
    "ticket_number": "JIRA-9000",
    "note": "loremipsum",
    "is_active": true
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "pic_id": 379890,
    "origin_payment_id": 45715,
    "origin_order_id": 46661,
    "origin_order_line_id": 55835,
    "refund_amount": "0.99",
    "refund_currency_id": 2,
    "refund_reason": 99,
    "refund_datetime": "2014-06-01T00:00:00Z",
    "finance_status": 1,
    "ticket_number": "JIRA-9000",
    "note": "loremipsum",
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Refund

Delete refund. This service will set `is_active` value to false.

**URL:** /refunds/{refund_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Refund is deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /refunds/2 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```
