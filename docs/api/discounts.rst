

### Discounts **TODO: UPDATE**

Discount resource to set and manage various discount case.

Action on this resource may affects offers' `discount_price_...` to be recalculated.

#### Discount Properties

* `id` - Number; Discount identifier

* `name` - String; Discount name

* `tag` - String; Discount tag, a very short string defining the discount, max 10 chars

* `description` - String; Discount description

* `valid_from` - String; Datetime in UTC when the discount is valid from

* `valid_to` - String; Datetime in UTC when the discount is valid to

* `campaign_id` - Number; Campaign identifier of this discount

* `discount_type` - Number; Type of discount as below

    * 1: DISCOUNT FOR AN OFFER BY AMOUNT; Discount that applies to an offer by fixed discounted amount, e.g.,

        ```
        Offer price = IDR 100,000

        Discount amount = IDR 10,000

        After discount price = IDR 90,000
        ```

    * 2: DISCOUNT FOR AN OFFER TO PRICE; Discount that applies to an offer by set the price to fixed amount, e.g.,

        ```
        Offer price = IDR 100,000

        Discount to price = IDR 30,000

        After discount price = IDR 30,000
        ```

    * 3: DISCOUNT FOR AN OFFER BY PERCENTAGE; Discount that applies to an offer by percentage, e.g.,

        ```
        Offer price = IDR 100,000

        Discount = 25%

        After discount price = IDR 75,000
        ```

    * 4: FULL DISCOUNT FOR AN OFFER; Discount that makes offer to become free, e.g,

        ```
        Offer price = IDR 1,000,000

        Discount = FREE

        After discount price = IDR 0
        ```

    * 5: DISCOUNT FOR AN ORDER BY AMOUNT; Discount that applies to order total by fixed discounted amount, e.g.,

        ```
        Total order = IDR 1,000,000

        Discount = IDR 200,000

        After discount price = IDR 800,000
        ```

    * 6: DISCOUNT FOR AN ORDER BY PERCENTAGE; Discount that applies to order total by percentage,, e.g.,

        ```
        Total order = IDR 1,000,000

        Discount = 35%

        After discount price = IDR 650,000
        ```

* `require_code` - Boolean; If true, the discount only applies when certain code provided; Default to false

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `valid_for_all_users` - Boolean; Flag to determine if this discount applies to all users. This will override `users`

* `users` - Array; Array of user's identifiers that eligible for this discount

* `valid_for_all_partners` - Boolean; Flag to determine if this discount applies to all partners. This will override `partners`

* `partners` - Array; Array of partner's identifiers that eligible for this discount

* `valid_for_all_offers` - Boolean; Flag to determine if this discount applies to all offers. This will override `offers`

* `offers` - Array; Array of offer's identifiers that eligible for this discount

* `broadcast` - Boolean; Broadcast flag. If true, the discount will be use to calculate display price of an item

* `vendor_cut_rule_id` - Number; Vendor cut rule identifier override for this discount

* `discount_prices` - Array; Array of price related to specific payment gateway that eligible for this discount

    * `min_order` - String; Float of minimum order for the discount to become eligible; Only used when `discount_type` is DISCOUNT FOR AN ORDER BY AMOUNT or DISCOUNT FOR AN ORDER BY PERCENTAGE

    * `max_eligible_price` - String; Float of maximum eligible price for the discount; Only used when `discount_type` is DISCOUNT FOR AN ORDER BY AMOUNT or DISCOUNT FOR AN ORDER BY PERCENTAGE

    * `discount_value` - String; Float of discount value, the value interprete as `discount_type`

    * `payment_gateway_id` - Number; Payment gateway identifier of this price

    * `payment_gateway_cut_rule_id` - Number; Payment gateway cut rule identifier override for this discount

#### Create Discount

Create new discount.

**URL:** /discounts

**Method:** POST

**Request Data Params:**

Required: `tag`, `valid_from`, `valid_to`, `campaign_id`, `discount_type`, `require_code`, `is_active`, `broadcast`, `valid_for_all_users`, `valid_for_all_partners`, `valid_for_all_offers`

Optional: `description`, `users`, `partners`, `offers`, `vendor_cut_rule_id`, `discount_prices`

Required in `discount_prices`: `min_order`, `discount_value`, `payment_gateway_id`

***********

    * `min_order` - String; Float of minimum order for the discount to become eligible; Only used when `discount_type` is DISCOUNT FOR AN ORDER BY AMOUNT or DISCOUNT FOR AN ORDER BY PERCENTAGE

    * `max_eligible_price` - String; Float of maximum eligible price for the discount; Only used when `discount_type` is DISCOUNT FOR AN ORDER BY AMOUNT or DISCOUNT FOR AN ORDER BY PERCENTAGE

    * `discount_value` - String; Float of discount value, the value interprete as `discount_type`

    * `payment_gateway_id` - Number; Payment gateway identifier of this price

    * `payment_gateway_cut_rule_id` - Number; Payment gateway cut rule identifier override for this discount

**Response HTTP Code:**

* `201` - New campaign created

**Response Params:** See [Campaign Properties](#campaign-properties)

**Examples:**

Request:

```
POST /campaigns HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Christmas 2014 Sale",
    "description": "Christmas 2014 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/campaigns/1

{
    "id": 1,
    "name": "Christmas 2014 Sale",
    "description": "Christmas 2014 Sale - discount to 0.99 for all single offers, discount 50% for Gramedia books"
}
```

Fail response:

```
HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

{
    "status": 400,
    "error_code": 400101,
    "developer_message": "Bad Request. Missing required parameter"
    "user_message": "Missing required parameter"
}
```


#### Delete Discount

Delete discount. This service will set `is_active` value to false.

**URL:** /discounts/{discount_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Discount deleted

**Response Params:** -

**Examples:**

Full request:

```
DELETE /discounts/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Discount Codes
Discount Code is obtained from any voucher

#### Discount Codes Properties
* `discount_id` - Number; discount code identifier.

* `is_active` - Boolean; Instance's active status, used to be marked if the instance is active or soft-deleted. The default value is true.

* `discount_type` - Number; identifies which type of discount is applied.

* `codes` - list of codes, consists of codes and the maximum number of use.

* `code` - Number; the discount code used to apply any kind of discount.

* `max_uses` - Number; the maximum number of discount availability.

#### Create Discount Codes
Creates discount code

**URL:** /discount_codes

**Method:** POST

**Request Data Params:**

Required: `discount_id`, `is_active`, `discount_type`, `discount_codes`, `code`, `max_uses`

Optional: -

**Response HTTP Code:**

* `201` - New discount code created

**Response Params:** See [Discount Code Properties](#discount-code-properties)

**Examples:**

Request:

```
POST /discount_codes HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
        "discount_id": 1,
        "is_active": true,
        "discount_type": 1,
        "discount_codes": [
            {
                "code": "XX1",
                "max_uses": 10
            },
            {
                "code": "XX2",
                "max_uses": 10
            },
            {
                "code": "XX3",
                "max_uses": 5
            },
            {
                "code": "XX4",
                "max_uses": 10
            },
            {
                "code": "XX5",
                "max_uses": 8
            }
        ]
}

```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8

{
	"discount_id": 1,
	"is_active": true,
	"discount_type": 1,
	"coupon_codes": [
            {
                "code": "XX1",
                "max_uses": 10
            },
            {
                "code": "XX2",
                "max_uses": 10
            },
            {
                "code": "XX3",
                "max_uses": 5
            },
            {
                "code": "XX4",
                "max_uses": 10
            },
            {
                "code": "XX5",
                "max_uses": 8
            }
        ]
}


```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}

```

#### Update Discount Code
Updates discount code

**URL:** /discount_codes/{discount_code_id}

**Method:** PUT

**Request Data Params:**

Required: `discount_id`, `is_active`, `discount_type`, `codes`, `code`, `max_uses`

Optional: -

**Response HTTP Code:**

* `200` - Discount code updated

**Response Params:** See [Discount code Properties](#discount-code-properties)

**Examples:**

Request:

```
PUT /discount_code/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
	"discount_id": 1",
	"is_active": true,
	"discount_type": 1,
	"code": "XX1",
	"max_uses": 10
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"discount_id": 1",
	"is_active": true,
	"discount_type": 1,
	"code": "XX1",
	"max_uses": 10
}

```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Retrieve Discount Codes
Retrieves discount codes

**URL:** /owned_items/

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Owned Item Properties](#discount-codes-properties). Default to ALL.

* `offset` - The offset of the records. The default value is 0

* `Limit` - The limit of the records. The default value is 9999

* `order` - Order the records. The default mode is ascending order by primary identifier or ascending by created timestamp

* `q` - Simple string query to record’s string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Owned item retrieved and returned in body content
* `204` - No matched owned item

**Response Params:** See [Discount Code Properties](#discount-code-properties)

**Examples:**

Request:

```
GET /discount_code?fields=id, name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json

```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"discount_id": 1",
	"is_active": true,
	"discount_type": 1,
	"code": "XX1",
	"max_uses": 10
}


```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Discount Code
Retrieves individual discount code by using identifier

**URL:** /discount_code/{discount_code_id}

Optional:

* `fields` - Retrieve specific fields. See [Discount Code Properties](#discount-code-properties). Default to ALL.

**Method:** GET

**Response HTTP Code:**

* `200` - Discount code is retrieved and returned

**Response Params:** See [Discount Codes Properties](#discount-codes-properties)

**Examples:**

Request:

```
GET /owned_items/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
	"discount_id": 1",
	"is_active": true,
	"discount_type": 1,
	"code": "XX1",
	"max_uses": 10
}


```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Discount Code
Deletes owned item by using identifier

**URL:** /discount_code/{discount_code_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Discount code is deleted

**Response Params:** See [Discount Code Properties](#dscount-code-properties)

**Examples:**

Request:

```
DELETE /discount_code/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%


```

Success response:

```
HTTP/1.1 204 No Content

```

Fail response:

```
HTTP/1.1 404 Not Found
```

