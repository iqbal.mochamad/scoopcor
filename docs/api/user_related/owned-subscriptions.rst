User Owned Subscriptions
========================

User owned subscription resource support the use cases of user owned
subscription. The service representation will vary depends on use case needs.

Properties
----------

id
    Number; Instance identifier

user_id
    Number; Owner user identifier

brand_id
    Number; Owned subscription's brand identifier

subscription_code
    Number; Owned subscription code **TODO: deprecated after migration**

order_line_id
    Number; Related order line identifier from which this owned item added from

allow_backward
    Boolean; Flag if the subscription allow backward (back issue in publication) for items in the same brand, this only works on time-based subscription

quantity
    Number; Quantity for unit-based subscription, if this value available valid_to value are ignored

current_quantity
    Number; Current left over quantity for unit-based subscription. On new owned subscription, this value will be exactly same as quantity and will reduced to 0 when the subscription already fully-used.

quantity_unit
    Number; Quantity unit type for subcription, related to quantity property

    * 1: UNIT - For unit-based subscription, the total unit will be stored in user's owned subscription. Most of the magazines use this type of unit
    * 2: DAY - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. This unit rarely used
    * 3: WEEK - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. This unit rarely used
    * 4: MONTH - For time-based subscription, the calculated valid_to will be stored in user's owned subscription. Most of the newspaper use this type of unit

valid_from
    Number; Valid from timestamps for time-based subcription, this value will be used if quantity not provided

valid_to
    Number; Valid to timestamps for time-based subcription, this value will be used if quantity not provided

is_active
    Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

brand
    Object; Brand of the subscription, this returned on subcriptions retrieval


Create
------

Adds new subscription plan(s) to specific user. This action can only be called by internal system or trusted CMS to deliver the subscription plan(s) to user.
This method will be called on subscription delivery (post-payment) use case or when administrator want to give complimentary subscriptions to user.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /owned_subscriptions                                         |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 207      | NOTE: Individual item status codes   |
|                       |               | are nested in the JSON response data.|
|                       |               |                                      |
|                       |               | * 200 - Item already owned by user   |
|                       |               |     details updated.                 |
|                       |               | * 201 - New item added to user.      |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request - probably bad json data |
|                       |               | in your request body                 |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Examples
^^^^^^^^

Request

.. code-block:: http

    POST /owned_subscriptions HTTP/1.1
    Content-Type: application/json
    Accept: application/json
    Authorization: Bearer ajksfhiau12371894-&*@&-$%

.. code-block:: json

    {
        "owned_subscriptions": [
            {
                  "valid_from": "2014-06-16 15:13:29",
                  "user_id": 639,
                  "quantity_unit": 1,
                  "order_line_id": 1647,
                  "allow_backward": false,
                  "is_active": true,
                  "valid_to": "2014-07-16 15:13:29",
                  "brand_id": 7709,
                  "current_quantity": 8,
                  "quantity": 8
            },
            {
                  "valid_from": "2014-06-16 15:13:29",
                  "user_id": 639,
                  "quantity_unit": 1,
                  "order_line_id": 1647,
                  "allow_backward": false,
                  "is_active": true,
                  "valid_to": "2014-07-16 15:13:29",
                  "brand_id": 7709,
                  "current_quantity": 8,
                  "quantity": 8
            }
        ]
    }


Success response:

.. code-block:: http

    HTTP/1.1 207 Multi-Status
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "results":[
        {
              "id": 1,
              "valid_from": "2014-06-16 15:13:29",
              "user_id": 639,
              "quantity_unit": 1,
              "order_line_id": 1647,
              "allow_backward": false,
              "is_active": true,
              "valid_to": "2014-07-16 15:13:29",
              "brand_id": 7709,
              "current_quantity": 8,
              "quantity": 8
        },
        {
              "id": 2,
              "valid_from": "2014-06-16 15:13:29",
              "user_id": 639,
              "quantity_unit": 1,
              "order_line_id": 1647,
              "allow_backward": false,
              "is_active": true,
              "valid_to": "2014-07-16 15:13:29",
              "brand_id": 7709,
              "current_quantity": 8,
              "quantity": 8
        }
        ]
    }


List
----

Retrieve user owned subscriptions, this service to support below use cases:

1. Retrieve subscriptions owned by user to show to user's subcription list
2. Retrieve subscriptions owned by user with provided brands, to show user item's statuses on store front

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /owned_subscriptions                                 |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Record was updated                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers.                 |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {owned_item_id} does not exist       |
+-----------------------+---------------+--------------------------------------+

Query String Parameters
^^^^^^^^^^^^^^^^^^^^^^^

is_active
    Filter by is_active status(es). Response will only returns if any matched
fields
    Retrieve specific fields. See [Item Properties](#item-properties).
    Default to ALL
offset
    Offset of the records. Default to 0
limit
    Limit of the records. Default to 20
order
    Order the records. Default to order ascending by primary identifier or
    created timestamp
q
    Simple string query to records' string properties
user_id
    Provided user identifier(s) to retrieve. Response will only returns if
    any matched; If alternative URL provided the user_id, this parameter will
    be ignored
brand_id
    Provided brand identifier to retrieve
expand
    Expand the result details. Default to null, the supported expand for
    owned items are as below
item
    get related item as object.


Request Example
^^^^^^^^^^^^^^^

Retrieve item's statuses for store front:

.. code-block:: http

    GET /owned_subscriptions?offset=0&limit=10&brand_id=1,2,3,4 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894-&*@&-$%
    Accept: application/json


Retrieve user subscriptions:

.. code-block:: http

    GET /owned_subscriptions?offset=0&limit=10&fields=id HTTP/1.1
    Authorization: Bearer ajksfhiau12371894-&*@&-$%
    Accept: application/json


Retrieve user owned items for CMS:

.. code-block:: http

    GET /owned_subscriptions?offset=0&limit=10&fields=id,user_id,brand_id&expand=ext HTTP/1.1
    Authorization: Bearer ajksfhiau12371894-&*@&-$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

{
    "owned_subscriptions": [{
        "valid_from": "2014-08-28T05:10:26",
        "user_id": 116301,
        "quantity_unit": 1,
        "items": [{
            "user_id": 116301,
            "order_line_id": null,
            "is_active": true,
            "item_id": 51646,
            "id": 43,
            "edition_code": "ID_TBOLA2014MTH08ED2577"
        }],
        "order_line_id": 0,
        "allow_backward": false,
        "is_active": true,
        "valid_to": "2014-08-28T12:10:26.936511",
        "id": 4,
        "brand_id": 5366,
        "brand": {
            "icon_image_normal": "",
            "sort_priority": 1,
            "description": "",
            "vendor_id": 156,
            "is_active": true,
            "slug": "tabloid-bola",
            "meta": "",
            "icon_image_highres": "",
            "id": 5366,
            "name": "Tabloid Bola"
        },
        "subscription_code": "SUB_TBOLA03MTH",
        "current_quantity": 14,
        "quantity": 14
    },
    {
        "valid_from": "2014-08-28T05:31:29",
        "user_id": 370587,
        "quantity_unit": 4,
        "items": [
        {
            "user_id": 370587,
            "order_line_id": null,
            "is_active": true,
            "item_id": 51637,
            "id": 73,
            "edition_code": "ID_BOLAH2014MTH08DT28"
        }],
        "order_line_id": 0,
        "allow_backward": true,
        "is_active": true,
        "valid_to": "2014-11-28T12:31:29.638729",
        "id": 5,
        "brand_id": 5267,
        "brand": {
            "icon_image_normal": "",
            "sort_priority": 1,
            "description": "",
            "vendor_id": 156,
            "is_active": true,
            "slug": "bola-harian",
            "meta": "",
            "icon_image_highres": "",
            "id": 5267,
            "name": "BOLA Harian"
        },
        "subscription_code": "SUB_BOLAH03MTH",
        "current_quantity": 3,
        "quantity": 3
    },
    {
        "valid_from": "2014-08-28T05:56:46",
        "user_id": 48035,
        "quantity_unit": 4,
        "items": [{
            "user_id": 48035,
            "order_line_id": null,
            "is_active": true,
            "item_id": 51637,
            "id": 114,
            "edition_code": "ID_BOLAH2014MTH08DT28"
        }],
        "order_line_id": 0,
        "allow_backward": true,
        "is_active": true,
        "valid_to": "2014-11-28T12:56:46.215202",
        "id": 6,
        "brand_id": 5267,
        "brand": {
            "icon_image_normal": "",
            "sort_priority": 1,
            "description": "",
            "vendor_id": 156,
            "is_active": true,
            "slug": "bola-harian",
            "meta": "",
            "icon_image_highres": "",
            "id": 5267,
            "name": "BOLA Harian"
        },
        "subscription_code": "SUB_BOLAH03MTH",
        "current_quantity": 3,
        "quantity": 3
    }],
    "metadata": {
        "resultset": {
            "count": 23,
            "limit": 3,
            "offset": 0
        }
    }
}


Update
------

Update user owned subscription.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /owned_subscriptions/{owned_subscriptions_id}        |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Record was updated                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers.                 |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {owned_item_id} does not exist       |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /owned_subscriptions/1234 HTTP/1.1
    Authorization: Bearer @#46lkjserlj
    Content-type: application/json
    Accept: application/json

.. code-block:: json

    {
      "valid_from": "2014-06-16 15:13:29",
      "user_id": 298106,
      "quantity_unit": 1,
      "order_line_id": 1647,
      "allow_backward": false,
      "is_active": true,
      "valid_to": "2014-07-16 15:13:29",
      "current_quantity": 8,
      "brand_id": 5267,
      "quantity": 8
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK

.. code-block:: json

    {
      "id": 1234,
      "valid_from": "2014-06-16 15:13:29",
      "user_id": 298106,
      "quantity_unit": 1,
      "order_line_id": 1647,
      "allow_backward": false,
      "is_active": true,
      "valid_to": "2014-07-16 15:13:29",
      "current_quantity": 8,
      "brand_id": 5267,
      "quantity": 8
    }


Delete
------

Soft-deletes a user-owned item.  It behaves like a standard delete request
(returns HTTP 204 and no content when successful), but the record is not
removed from our database (is_active is set to false).

Delete user owned item. This service will set is_active value to false.

This endpoint does not have a request body or a response body.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /owned_subscriptions/{owned_subscription_id}         |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                               |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Record was deleted                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request (for delete, this would  |
|                       |               | typically be incorrect headers)      |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {owned_item_id} does not exist       |
+-----------------------+---------------+--------------------------------------+
