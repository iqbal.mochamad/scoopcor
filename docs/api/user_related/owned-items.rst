User Owned Items
================

Information that describes which describes which Items are owned by which
Users.

.. warning::

    The schemas linked from this document are based upon the original
    documentation, plus the current underlying database structure
    (as currently defined).  This is going to definitely need verified as
    correct.

    29 May 15 - Derek


Properties
----------

id
    Number; Instance identifier
user_id
    Number; Owner user identifier
item_id
    Number; Owned item identifier
edition_code
    Number; Owned edition code **TODO: deprecated after migration**
order_line_id
    Number; Related order line identifier from which this owned item added from
is_active
    Boolean; Instance's active status, used to mark if the instance is active
    or soft-deleted. Default to true


current-user
------------

Add new items to specific user. This action only can be called by
internal system or trusted CMS to deliver the item to user.

Add new items is called on item delivery (post-payment) use case or when
administrator want to give complimentary items to user.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | v1/users/current-user/owned-items                                         |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 207      | NOTE: Individual item status codes   |
|                       |               | are nested in the JSON response data.|
|                       |               |                                      |
|                       |               | * 200 - Item already owned by user   |
|                       |               |     details updated.                 |
|                       |               | * 201 - New item added to user.      |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request - probably bad json data |
|                       |               | in your request body                 |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Examples
^^^^^^^^

Request

.. code-block:: http

    GET v1/users/current-user/owned-items HTTP/1.1
    Content-Type: application/json
    Accept: application/json
    Authorization: Bearer ajksfhiau12371894-&*@&-$%

Success response:

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "items": [
        {
            "authors": [
            {
              "href": "http://localhost/v1/authors/1",
              "id": 1,
              "title": "Brett Berge"
            },
            {
              "href": "http://localhost/v1/authors/2",
              "id": 2,
              "title": "Kristy Deckow DDS"
            }
            ],
            "brand": {
                "href": "http://localhost/v1/brands/1",
                "id": 1,
                "title": "deploy revolutionary communities",
                "vendor": {
                    "href": "http://localhost/v1/vendors/1",
                    "id": 1,
                    "title": "engineer value-added initiatives"
                }
            },
            "created": "2017-01-18T08:07:46.064791+00:00",
            "href": "http://localhost/v1/items/1",
            "id": 1,
            "image": {
                "href": "https://images.apps-foundry.com/magazine_static/http://heller.com/",
                "title": "Cover Image"
            },
            "is_age_restricted": false,
            "title": "disintermediate impactful experiences"
        },
        {
            "authors": [
            {
              "href": "http://localhost/v1/authors/3",
              "id": 3,
              "title": "Merry Koss"
            },
            {
              "href": "http://localhost/v1/authors/4",
              "id": 4,
              "title": "Brant Satterfield"
            }
            ],
            "brand": {
                "href": "http://localhost/v1/brands/2",
                "id": 2,
                "title": "matrix revolutionary partnerships",
                "vendor": {
                  "href": "http://localhost/v1/vendors/2",
                  "id": 2,
                  "title": "seize interactive interfaces"
                }
            },
            "created": "2017-01-18T08:07:46.436936+00:00",
            "href": "http://localhost/v1/items/2",
            "id": 2,
            "image": {
                "href": "https://images.apps-foundry.com/magazine_static/http://bednar.com/",
                "title": "Cover Image"
            },
            "is_age_restricted": false,
            "title": "optimize open-source synergies"
        }],
        "metadata": {
            "count": 2,
            "limit": 20,
            "offset": 0
        }
    }

:download:`Schema <_schemas/owned-item.list.json>`


by user id
----------

Retrieve user owned items, this service to support below use cases:

1. Retrieve items owned by user to show user's cloud items
2. Retrieve items owned by user with provided items, to show user
    item's statuses on store front

Client side must passed true as is_active value to show active owned items

Note: For the response's expanded representation, please refers to
[Item Properties](#item-properties)

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | v1/users/<user_id>/owned_items                       |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
|                       +---------------+--------------------------------------+
|                       | HTTP 204      | Request was OK, but no data found    |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {user_id} does not exist             |
+-----------------------+---------------+--------------------------------------+

Query String Parameters
^^^^^^^^^^^^^^^^^^^^^^^

offset
    Offset of the records. Default to 0
limit
    Limit of the records. Default to 20
user_id
    Provided user identifier(s) to retrieve. Response will only returns if
    any matched; If alternative URL provided the user_id, this parameter will
    be ignored



Request Example
^^^^^^^^^^^^^^^

Retrieve user owned items:

.. code-block:: http

    GET /owned_items?offset=0&limit=20 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894-&*@&-$%
    Accept: application/json



Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "items": [
        {
            "authors": [
            {
              "href": "http://localhost/v1/authors/1",
              "id": 1,
              "title": "Brett Berge"
            },
            {
              "href": "http://localhost/v1/authors/2",
              "id": 2,
              "title": "Kristy Deckow DDS"
            }
            ],
            "brand": {
                "href": "http://localhost/v1/brands/1",
                "id": 1,
                "title": "deploy revolutionary communities",
                "vendor": {
                    "href": "http://localhost/v1/vendors/1",
                    "id": 1,
                    "title": "engineer value-added initiatives"
                }
            },
            "created": "2017-01-18T08:07:46.064791+00:00",
            "href": "http://localhost/v1/items/1",
            "id": 1,
            "image": {
                "href": "https://images.apps-foundry.com/magazine_static/http://heller.com/",
                "title": "Cover Image"
            },
            "is_age_restricted": false,
            "title": "disintermediate impactful experiences"
        },
        {
            "authors": [
            {
              "href": "http://localhost/v1/authors/3",
              "id": 3,
              "title": "Merry Koss"
            },
            {
              "href": "http://localhost/v1/authors/4",
              "id": 4,
              "title": "Brant Satterfield"
            }
            ],
            "brand": {
                "href": "http://localhost/v1/brands/2",
                "id": 2,
                "title": "matrix revolutionary partnerships",
                "vendor": {
                  "href": "http://localhost/v1/vendors/2",
                  "id": 2,
                  "title": "seize interactive interfaces"
                }
            },
            "created": "2017-01-18T08:07:46.436936+00:00",
            "href": "http://localhost/v1/items/2",
            "id": 2,
            "image": {
                "href": "https://images.apps-foundry.com/magazine_static/http://bednar.com/",
                "title": "Cover Image"
            },
            "is_age_restricted": false,
            "title": "optimize open-source synergies"
        }],
        "metadata": {
            "count": 2,
            "limit": 20,
            "offset": 0
        }
    }


:download:`Schema <_schemas/owned-items/owned-item.list.json>`
