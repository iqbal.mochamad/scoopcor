Current User
============

Current User, are objects that contain an information of current user
that access the API.

Properties
----------

id
    Number; Uniquely-identifies this object/ user id.
first_name
    String; Current user first name.
last_name
    String; Current user last name.
username
    String; Current user username.
roles
    List; List of current user role.
organizations
    List; List of current user organization.
is_active
    Boolean; Current user status.
email
    String; Curent user email.
is_verified
    Boolean; Current user is already verified by system or not.
note
    String; Curent user additional info.
profile_pic
    Object; Contain current user profile picture url;
signup_date
    String (ISO 8601 DateTime Formatted);  Represents the date and time that
    current user first sign up.
active_buffet
    Object; Buffet status, it contain user expired time for their buffet plan.
    if expired the value is null.
allow_age_restricted_content
    Boolean; True if user prefer to allow age restricted content (default is True)

.. http:get:: /v1/users/current-user

    Get current user data

    **Example Request**

    .. sourcecode:: http

        GET v1/users/current-user HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Authorization: JWT ajksfhiau12371894^&*@&^$%.dkfsdlkfsfjlkdsf.dfklasjdflksajfsld
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop Portal/3.0

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
          "email": "example@somemail.com",
          "first_name": "new_first_name",
          "id": 427936,
          "is_active": true,
          "is_verified": true,
          "last_name": "new_last_name",
          "note": null,
          "organizations": [],
          "profile_pic":
                {
                    "href": null,
                    "id": 0,
                    "title": null
          },
          "roles": [
                    {
                      "href": null,
                      "id": 1,
                      "title": "super admin"
           }
          ],
          "signup_date": "2015-04-17T04:16:46+00:00",
          "user_owned_buffet": "2018-09-22T00:00:00+00:00",
          "username": "example",
          "active_buffet": {
                "href": "https://example.backend/v1/owned_buffets",
                "title": "Active Buffet Status",
                "offers": [
                    {
                        "id": 123,
                        "title": "SCOOP PREMIUM",
                        "valid_until": "2018-09-22T00:00:00+00:00",
                    },
                ]
          },
          "allow_age_restricted_content": true
        }

.. http:put:: /v1/users/current-user

    Update current user data.
    Currently allowed data to update personally by user: first_name, last_name, allow_age_restricted_content

    **Example Request**

    .. sourcecode:: http

        PUT v1/users/current-user HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        Authorization: JWT ajksfhiau12371894^&*@&^$%.dkfsdlkfsfjlkdsf.dfklasjdflksajfsld
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop Portal/3.0

        {
            "first_name": "new_first_name",
            "last_name": "new_last_name",
            "allow_age_restricted_content": true
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
          "email": "example@somemail.com",
          "first_name": "new_first_name",
          "id": 427936,
          "is_active": true,
          "is_verified": true,
          "last_name": "new_last_name",
          "note": null,
          "organizations": [],
          "profile_pic":
                {
                    "href": null,
                    "id": 0,
                    "title": null
          },
          "roles": [
                    {
                      "href": null,
                      "id": 1,
                      "title": "super admin"
           }
          ],
          "signup_date": "2015-04-17T04:16:46+00:00",
          "user_owned_buffet": "2018-09-22T00:00:00+00:00",
          "username": "example",
          "active_buffet":
                   {
                        "href": "https://example.backend/v1/owned_buffets",
                        "title": "Active Buffet Status",
                        "valid_until": "2018-09-22T00:00:00+00:00"
          },
          "allow_age_restricted_content": true
        }
