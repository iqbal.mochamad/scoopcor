Organization manageable by user
===============================

Resource for list of organization that user can manage (related to wallet)

List Organization manageable
----------------------------

.. http:get:: /v1/users/current-user/manageable-organizations

    Return list of organization that can be manage by user

    **Example Request**

    .. sourcecode:: http

        GET /v1/users/current-user/manageable-organizations
        Authorization:  Bearer Mzk3MjA2OmYwNzhiODg3ZTdiOWEwZmNhMTljMjdmZjdlNDhmYmE0YzA4NDc4ODZiYTU2YjQ1MTViNjk5ZjNjZDE5ODU5MzhkNzc2MTRhN2YxZWU0MTRh
        {
          "manageable_organizations": [
            {
              "href": "http://127.0.0.1:5000/v1/organizations/711",
              "id": 711,
              "organization_type": "shared library",
              "title": "Perpustakaan Nasional"
            }
          ],
          "metadata": {
            "resultset": {
              "count": 1,
              "limit": 20,
              "offset": 0
            }
          }
        }


    :query offset: The number of beginning results to offset from the start
    :query limit: The maximum number of result to return.


    **Properties**

    **Metadata Properties**

    resultset
        count
            The total number of objects matching your query.
        limit
            The maximum number of objects displayed per page.
        offset
            The number of objects skipped at the beginning of the list.
