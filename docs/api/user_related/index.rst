User-Related
============

User data is not stored or retrieved directly through scoopcor (see: scoopcas),
however there is some user-centric data that is handled by scoopcor.


.. toctree::

    buffets
    current-user
    owned-items
    owned-subscriptions
    organization-manageable-by-user

