User Buffets
============

User buffets, like owned items, are objects that contain a list of all buffets
that a given user has purchased.

This endpoint is a read-only endpoint.


Properties
----------

id
    Number; Uniquely-identifies this object.
user_id
    Number; Represents the 'id' of the User that purchased this buffet.
orderline_id
    Number; Represents the 'id' of the line item, which this buffet
    was purchased through.
expires_on
    String (ISO 8601 DateTime Formatted);  Represents the date and time that
    this buffet will no longer be available for purchase.
offerbuffet
    Object (see OfferBuffet);  Buffet data, exactly as what is present
    in the OfferBuffet get details API.

List
----

Fetches a list of User Buffets that match the input query string.


**Query String Parameters**

id
    id=123 - fetch any UserBuffet with id equal to 123
    id__lt=123 - fetch any UserBuffet with id less than 123
    id__gt=123 - fetch any UserBuffet with id greater than 123
user_id
    same options as id, but filters on user_id
orderline_id
    same options as id, but filters on orderline_id
offerbuffet_id
    same options as id but filters on offerbuffet_id
expires_on
    same options as id, but filters on expires_on
    REQUIRES a full ISO 8601 datetime
    ISO 8601 datetime argument must be URL-encoded.

URL: /v1/users/buffets
Method: HTTP GET

**Response**

.. code-block:: json

    {
        "id": 1234,
        "user_id": 1234,
        "orderline_id": 1234,
        "offerbuffet_id": 1,
        "expires_on": "2015-10-10T00:00:00+00:00",
        "offerbuffet": {
            {
                "available_from": "2015-05-01T00:00:00+00:00",
                "available_until": "2016-05-01T00:00:00+00:00",
                "buffet_duration": "P30DT0S",
                "id": 1,
                "offer": {
                    "aggr_price_idr": 0,
                    "aggr_price_point": 0,
                    "aggr_price_usd": null,
                    "brands": [
                        12,
                        5,
                        36
                    ],
                    "discount_id": null,
                    "discount_name": null,
                    "discount_price_idr": 0,
                    "discount_price_point": 0,
                    "discount_price_usd": null,
                    "discount_tag": null,
                    "exclusive_clients": null,
                    "id": 41975,
                    "image_highres": null,
                    "image_normal": null,
                    "is_active": true,
                    "is_discount": false,
                    "is_free": false,
                    "item_code": null,
                    "items": [
                        20051,
                        20052
                    ],
                    "name": "MAY BUFFET OFFER",
                    "offer_code": null,
                    "offer_status": 7,
                    "offer_type_id": 4,
                    "platforms_offers": [],
                    "price_idr": 144440,
                    "price_point": 0,
                    "price_usd": 14.44,
                    "sort_priority": 1,
                    "vendor_price_idr": 0,
                    "vendor_price_point": 0,
                    "vendor_price_usd": null
                }
            }
        }
    }
