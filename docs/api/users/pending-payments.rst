User Pending Payments
=====================

Returns a summary of all orders for a given user with pending payments.

As with all 'user-related' APIs, 'current-user' may be subsituted in the
URL in leu of the user id.

/v1/users/<user_id>/pending-transactions


Properties
----------

title
    String; Always 'Pending Order'
href
    String (URI); URL with full order details
order_id
    Number; Database identifier (ignore if possible).
date
    String (ISO 8601); Date the order was created.
order_number
    Number; Unique identifier for this order.  Use this when displaying
    the user's order id.
payment_gateway
    Object (Implicit HREF);  Points to the payment gateway details used for
    this order.
payment_code
    String - Optional; Depending on the payment gateway this value may or
    may not be present.  Please consult the individual payment gateway
    information.
price
    Object;  Contains the 'final' and 'currency' code used in the user's
    locale.
line_items
    Array of Object;
