Users
=====

User resource contains all users of the system.

Non-administrator users only can access owned informations.

#### User Properties

* `id` - Number; User identifier

* `user_id` - Number; Old user identifier. For new client **do not use this**, use `id` instead **TODO: Drop after translation layer dropped**

* `username` - String; Username in alphanumeric

* `password` - String; Hashed password

* `email` - String; User's email

* `origin_client_id` - Number; Origin client where user did register

* `first_name` - String; First name

* `last_name` - String; Last name

* `title` - Number; User title

    * 1: MR.

    * 2: MRS.

* `fb_id` - String; Facebook identifier this user last login with

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `status` - Number; User's status. Default to 1

    * 1: CLEAR - Clear for any actions. State: active

    * 2: BANNED - Banned due to some reason, see note for further information. State: not active

* `note` - String; Note for this user

* `banned_from` - String; Banned starts datetime

* `banned_to` - String; Banned ends datetime

* `is_verified` - Boolean; User's email verification status. Default to false

* `referral_code` - String; User's referral code

* `referral_id` - String; Referral code used to register account

* `profile` - Object; contains more detail profile of user as below:

    * `gender` - Number; User gender

        * 1: MALE

        * 2: FEMALE

    * `birthdate` - String; User's birthdate

    * `origin_latitude` - String; User's origin latitude

    * `origin_longitude` - String; User's origin longitude

    * `origin_country_code` - String; User's country code

    * `origin_device_model` - String; User's origin device model

    * `origin_partner_id` - String; User's origin device's partner identifier

    * `current_latitude` - String; User's current latitude

    * `current_longitude` - String; User's current longitude

Some user properties only accessible on administrator level: `fb_id`, `is_active`, `status`, `note`, `banned_from`, `is_verified`


Create/User registration
------------------------

.. http:post:: /v1/users

    Create/Add a new user.
    User can also register with a facebook access token (see facebook login for more details)

    Required: `username`, `password`, `email`, `origin_client_id`

    **Response HTTP Code:**

    * `201` - New user created.
    * `422` - Unprocessable Entity. Required parameter not provided correctly in json request body
    * `409` - Conflict. Duplicated value on unique parameter (email or username already exists)

    **Example Request**

    .. sourcecode:: http

        POST v1/users HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        Content-Length: 780
        User-Agent: Scoop IOS/3.0
        X-Scoop-Client: Scoop IOS/3.0

        {
            "first_name": "John",
            "last_name": "Walker",
            "username": "john@fakeemail.com",
            "email": "john@fakeemail.com",
            "password": "mypassword",
            "allow_age_restricted_content": true,
            "origin_client_id": 1,
            "profile": {
                "birthdate": '1990-12-01T00:00:00+00:00',
                "gender": "1",
                "origin_device_model": "ASUS_T00J"
            }
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 201 CREATED
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
            "id": 123,
            "first_name": "John",
            "last_name": "Walker",
            "username": "john@fakeemail.com",
            "email": "john@fakeemail.com",
            "allow_age_restricted_content": true,
            "origin_client_id": 1,
            "is_verified": false,
            "is_active": true,
            "profile": {
                "birthdate": '1990-12-01T00:00:00+00:00',
                "gender": "1",
                "origin_device_model": "ASUS_T00J"
            }
        }


Update Current User
-------------------

.. http:put:: /v1/users/current-user


    Update current user data

    **Example Request**

    .. sourcecode:: http

        PUT v1/users HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        Content-Length: 780
        User-Agent: Scoop IOS/3.0
        X-Scoop-Client: Scoop IOS/3.0

        {
            "first_name": "John",
            "last_name": "Walker",
            "allow_age_restricted_content": true,
            "origin_client_id": 1,
            "profile": {
                "birthdate": '1990-12-01T00:00:00+00:00',
                "gender": "1",
                "origin_device_model": "ASUS_T00J"
            }
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
            "id": 123,
            "first_name": "John",
            "last_name": "Walker",
            "username": "john@fakeemail.com",
            "email": "john@fakeemail.com",
            "allow_age_restricted_content": true,
            "origin_client_id": 1,
            "is_verified": false,
            "is_active": true,
            "profile": {
                "birthdate": '1990-12-01T00:00:00+00:00',
                "gender": "1",
                "origin_device_model": "ASUS_T00J"
            }
        }
