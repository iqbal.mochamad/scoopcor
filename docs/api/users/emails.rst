Emails
======

**TODO: Implementation**

Email resource, to manage email from client side


Properties
----------

from
    String; Sender
to
    Array; List of main recipients
cc
    Array; List of CC recipients
bcc
    Array; List of BCC recipients
subject
    String; Subject of email
body
    String; Body content of email


Send Email
----------

Create new email and sednd.

**URL:*/emails/send

**Method:*POST

**Request Data Params:**

Required: from, to, subject, body

Optional: cc, bcc

**Response HTTP Code:**

201 - New email created and to be send

**Response Params:*See [Email Properties](#email-properties)

**Examples:**

Request:


POST /emails/send HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "to": ["js@apps-foundry.com"],
        "from": "no-reply@apps-foundry.copm",
        "subject": "Lorem ipsum dolor sit amet",
        "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    }


Success response:


HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "to": ["js@apps-foundry.com"],
        "from": "no-reply@apps-foundry.copm",
        "subject": "Lorem ipsum dolor sit amet",
        "body": "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    }


Fail response:

HTTP/1.1 400 Bad Request
Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "status": 400,
        "error_code": 400101,
        "developer_message": "Bad Request. Missing required parameter"
        "user_message": "Missing required parameter"
    }


