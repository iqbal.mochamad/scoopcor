Organizations
=============

A separate company/business which we (Apps Foundry/Scoop) have some dealings with.
Certain types of organizations, such as 'vendors', may have users associated with them
which will grant those users certain rights (like uploading/managing items within
'brands' owned by that vendor).


Properties
----------

id
    Number (readonly); Unique instance identifier
name
    String;  Name
legal_name
    String;  Legal name
slug
    String (unique);  Slug for SEO
status
    String (enum); Organization status in the system
    * 'new' - New created organization. State: not active
    * 'waiting for review' - Waiting for business side review for any kind of partnership. State: not active
    * 'in review' - In review. State: not active
    * 'rejected' - Rejected due to some reasons. State: not active
    * 'approved' - Aproved by business side. State: not active
    * 'clear' - Clear for any action. State: active
    * 'banned' - Banner due to some reasons. State: not active
type
    String (enum)
    * 'vendor' - items owner, e.g., publisher, book store
    * 'partner' - device manufacturer partner, e.g., Lenovo, Sony
    * 'payment gateway' - payment gateway, e.g., Veritrans
phone_primary
    String; Free-format contact phone number.
phone_secondary
    String; Same as phone primary, but a backup contact number.
contact_email
    String; Primary contact e-mail address.
accounting_id
    String, Unique; Used internally by Apps Foundry accounting dept.
parent_organization
    [Object](#relation-summary);  Another organization that this organization is nested under (if appliciable)
mailing_address
    [Object](#address);
billing_address
    [Object](#address);
clients
    Array (of [Object](#relation-summary));
sort_priority
    Number; Sort priority use to defining order. Higher number, more priority
is_active
    Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true


Create
------

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/organizations                                    |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /organizations HTTP/1.1
    Accept : application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

    {
        "name": "Apps Foundry",
        "legal_name": "PT APLIKASI PRIMA PERSADA",
        "slug": "apps-foundry",

        "status": "clear",
        "type": "vendor",

        "phone_primary": "+62 (812) 1272-8059",
        "phone_secondary": null,
        "contact_email": "backend@apps-foundry.com",
        "accounting_id": "DURIAN1234",
        "parental_organization": {
            "id": 1234,
            "name": "Weather Corp",
            "href": "https://scoopadm.apps-foundry.com/scoopcas/api/v1/organizations/1234",
            "rel": "parent"
        },
        "mailing_address": {
            "street1": "Wisma 77",
            "street2": "Tower 2, Lanti 12, Ste 2",
            "city": "Jakarta",
            "province": "DKI Jakarta",
            "postal_code": "12345",
            "country_code": "ID"
        },
        "billing_adress": {
            "street1": "Wisma 77",
            "street2": "Tower 2, Lanti 12, Ste 2",
            "city": "Jakarta",
            "province": "DKI Jakarta",
            "postal_code": "12345",
            "country_code": "ID"
        },
        "clients": [
            {"id": 1, "name": "Some Client", "rel": "client", "href": "/scoopcas/api/v1/clients/1"}
        ],
        "sort_priority": 1,
        "is_active": true
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/organizations/1


.. code-block:: json

    {
        "id": 1,
        "name": "Apps Foundry",
        "legal_name": "PT APLIKASI PRIMA PERSADA",
        "slug": "apps-foundry",

        "status": "clear",
        "type": "vendor",

        "phone_primary": "+62 (812) 1272-8059",
        "phone_secondary": null,
        "contact_email": "backend@apps-foundry.com",
        "accounting_id": "DURIAN1234",
        "parental_organization": {
            "id": 1234,
            "name": "Weather Corp",
            "href": "https://scoopadm.apps-foundry.com/scoopcas/api/v1/organizations/1234",
            "rel": "parent"
        },
        "mailing_address": {
            "street1": "Wisma 77",
            "street2": "Tower 2, Lanti 12, Ste 2",
            "city": "Jakarta",
            "province": "DKI Jakarta",
            "postal_code": "12345",
            "country_code": "ID"
        },
        "billing_adress": {
            "street1": "Wisma 77",
            "street2": "Tower 2, Lanti 12, Ste 2",
            "city": "Jakarta",
            "province": "DKI Jakarta",
            "postal_code": "12345",
            "country_code": "ID"
        },
        "clients": [
            {"id": 1, "name": "Some Client", "rel": "client", "href": "/scoopcas/api/v1/clients/1"}
        ],
        "sort_priority": 1,
        "is_active": true
    }


List
----

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/organizations                                    |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | OK                                   |
+-----------------------+---------------+--------------------------------------+
| Query Parameters      | fields        | limit response to specific fields    |
|                       +---------------+--------------------------------------+
|                       | offset        | Pagination; start offset             |
|                       +---------------+--------------------------------------+
|                       | limit         | Pagination; maximum records to fetch |
|                       +---------------+--------------------------------------+
|                       | q             | String query                         |
+-----------------------+---------------+--------------------------------------+


Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /organizations?fields=id,name&offset=0&limit=10 HTTP/1.1
    Accept : application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8


    {
        "organizations": [
            {
                "id": 1,
                "name": "Apps Foundry"
            },
            {
                "id": 2,
                "name": "MRA"
            },
            {
                "id": 3,
                "name": "Tempo"
            }
        ],
        "metadata": {
            "resultset": {
                "count": 30,
                "offset": 0,
                "limit": 10
            }
        }
    }


Detail
------

Retrieve individual organization using identifier.

**URL:** /organizations/{organization_id}

Optional:

* `fields` - Retrieve specific properties. See [Organization Properties](#organization-properties). Default to ALL

**Method:** GET

**Request Data Params:** -

**Response HTTP Code:**

* `200` - Organization found and returned

**Response Params:** See [Organization Properties](#organization-properties)

**Examples:**

Request:

```
GET /organizations/1?fields=id,name,is_active HTTP/1.1
Accept : application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "Apps Foundry",
    "is_active": true
}
```


Update
------


**URL:** /organizations/{organization_id}

**Method:** PUT



Request and response bodies are identical to CREATE



Delete
------

Delete organization. This service will set `is_active` value to false.

**URL:** /organizations/{organization_id}

**Method:** DELETE

**Request Data Params:** -

**Response HTTP Code:**

* `204` - Organization deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /organizations/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```
