User - Wish List
================

list of items in user's wish list


List of items in current user's wish list
----------------------------------------

.. http:get:: /v1/users/current-user/wish-list

    List all item in current user's wish list

    **Example Response**

    .. sourcecode:: http

        GET /v1/users/current-user/wish-list  HTTP/1.1
        Content-Type: application/json
        Authorization: JWT ajksfhiau12371894^&*@&^$%.asdfsafs.reerer

        {
            "items": [
                {
                    "title": "Item ABC",
                    "id": 1000,
                    "href": "http://localhost/v1/items/1000"
                },
                {
                    "title": "Item CDE",
                    "id": 1002,
                    "href": "http://localhost/v1/items/1002"
                }
            ],
            "metadata": {
                "resultset": {
                    "count": 2,
                    "limit": 20,
                    "offset": 0
                }
            }
        }


Add an item to user's wish list
-------------------------------

id
    Number; Item instance identifier.


.. http:link:: /v1/users/current-user/wish-list

    Add an item to user's wish list.

    **Example Request**

    .. sourcecode:: http

        LINK /v1/users/current-user/wish-list HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        Authorization: JWT ajksfhiau12371894^&*@&^$%.asdfsafs.reerer
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop Portal/3.0

        {
            "id": 123
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 201 CREATED
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {
            "success_message": "Item added"
        }


Remove an item from user's wish list
------------------------------------

id
    Number; Item instance identifier.


.. http:unlink:: /v1/users/current-user/wish-list

    remove an item from user's wish list.

    **Example Request**

    .. sourcecode:: http

        UNLINK /v1/users/current-user/wish-list HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        Authorization: JWT ajksfhiau12371894^&*@&^$%.asdfsafs.reerer
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop Portal/3.0

        {
            "id": 123
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 204 NO_CONTENT
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT
        ETag: W/"123456789

        {}


