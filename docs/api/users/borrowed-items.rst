Borrowed Items
==============

Resource for viewing and manipulating the `Items` which the user has currently borrowed from a `Shared Library`.


List Borrowed Items
-------------------

.. http:get:: /users/current-user/borrowed-items

    Retrieves a paginated list of BorrowedItemSummary objects.

    **Request Example**

    .. sourcecode:: http

        GET /users/current-user/borrowed-items HTTP/1.1
        Accept: application/json
        Accept-Charset: utf-8
        Accept-Language: id-ID
        Authorization: Bearer MTIzNDp3ZTRzY29vcA==
        User-Agent: ScoopIOS/4.7.0; iOS 3.2.1 (iPhone 6s)

    **Response Example**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        ETag: "737060cd8c284d8af7ad3082f209582d"
        Location: https://api.apps-foundry.com/users/current-user/borrowed-items/745823

        {
            "items": [
                {
                    "id": 745823,
                    "title": "Into to Algebra",
                    "href" : "https://api.apps-foundry.com/users/current-user/borrowed-items/745823",
                    "details": {
                        "href": "https://api.apps-foundry.com/items/9998",
                        "title": "Detil Item"
                    },
                    "cover_image": {
                        "href": "https://images.getscoop.com/items/cover-images/9998.jpg",
                        "title": "Gambar Sampul"
                    },
                    "borrowing_start_time": "2016-10-03T13:28:34+00:00",
                    "expires": "2016-10-10T01:23:45Z",
                    "last_read_time": "2016-10-10T01:23:45Z",
                    "last_read_page": 12
                    "authors": [
                        {
                          "href": "https://api.apps-foundry.com/authors/201",
                          "id": 201,
                          "title": "Imelda Akmal Architecture Writer Studio"
                        }
                    ],
                    "vendor": {
                        "href": "https://api.apps-foundry.com/vendors/251",
                        "title": "Penerbit Apa Saja"
                    },
                    "download": {
                        "href": "https://api.apps-foundry.com/v1/items/9998/download",
                        "title": "Download buat Baca Offline"
                    },
                    "catalog": {
                        "href": "https://api.apps-foundry.com/v1/organizations/1234/shared-catalogs/12",
                        "rel": "catalog",
                        "title": "Math Books"
                    }
                }
            ],
            "metadata": {
                "resultset": {
                    "offset": 0,
                    "limit": 20,
                    "count": 1
                }
            }
        }


Borrow an Item from a Shared Library
------------------------------------

.. http:post:: /users/current-user/borrowed-items

    Borrow an Item

    **Example request**

    .. note::

        As a convenience, you DO NOT need to post the entire JSON content, retrieved from the shared
        catalog item.  You can simply post the "href" fields retrieved from the shared catalog item object.
        However, if desired, you may post the entire object (everything other than the HREF field will be
        ignored by backend).

    .. warning::

        This uses the somewhat obscure HTTP LINK verb.  It may be substituted with POST if your client
        does not support LINK, however LINK is the preferred verb, although POST is documented here.

    .. sourcecode:: http

        POST /users/current-user/borrowed-items HTTP/1.1
        Accept: application/json: id-ID
        Accept-Charset: utf-8
        Accept-Language: id-ID
        Authorization: Bearer MTIzNDp3ZTRzY29vcA==
        Content-Type: application/json
        User-Agent: ScoopIOS/4.7.0; iOS 3.2.1 (iPhone 6s)

        {
            "href" : "https://api.apps-foundry.com/organizations/1/shared-catalogs/12/9998"
        }


    **Example response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        ETag: "737060cd8c284d8af7ad3082f209582d"
        Location: https://api.apps-foundry.com/users/current-user/borrowed-items/745823

        **Same Response As Details**


Borrowed Item Details
---------------------

.. http:get:: /users/current-user/borrowed-items/(int:borrow_id)

    Returns detailed information about a single `Item` that is currently borrowed by
    the current User.

    **Example Request**

    .. sourcecode:: http

        GET /users/current-user/borrowed-items/745823 HTTP/1.1
        Accept: application/json
        Accept-Charset: utf-8
        Accept-Language: id-ID
        Authorization: Bearer MTIzNDp3ZTRzY29vcA==
        User-Agent: ScoopIOS/4.7.0; iOS 3.2.1 (iPhone 6s)

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json

        {
            "id": 745823,
            "title": "Into to Algebra",
            "details": {
                "href": "https://api.apps-foundry.com/items/9998",
                "title": "Detil Item"
            },
            "owner": {
                "href": "https://api.apps-foundry.com/organizations/1",
                "rel": "organization",
                "title": "Apps-Foundry Shared Library"
            },
            "catalog": {
                "href": "https://api.apps-foundry.com/organizations/1/shared-catalogs/1",
                "rel": "catalog",
                "title": "Math Books"
            },
            "cover_image": {
                "href": "https://images.getscoop.com/items/cover-images/9998.jpg",
                "title": "Gambar Sampul"
            },
            "download": {
                "href": "https://api.apps-foundry.com/items/9998/download",
                "title": "Download buat Baca Offline"
            },
            "streaming": {
                "href": "https://api.apps-foundry.com/items/9998/web-reader",
                "title": "Baca Sekarang"
            },
            "expires": "2016-10-10T01:23:45Z",
            "last_read_time": "2016-10-10T01:23:45Z",
            "last_read_page": 12
        }

Manually Return Item
--------------------

.. http:delete:: /users/current-user/borrowed-items/(int:borrow_id)

    Manually return an Item before it's 'expires' time.

    **Request Example**

    .. sourcecode:: http

        DELETE /users/current-user/borrowed-items/745823 HTTP/1.1
        Accept-Language: id-ID
        Accept-Charset: utf-8
        Authorization: Bearer MTIzNDp3ZTRzY29vcA==
        User-Agent: ScoopIOS/4.7.0; iOS 3.2.1 (iPhone 6s)

    **Response Example**

    .. sourcecode:: http

        HTTP/1.1 204 NO CONTENT

Extend Borrowing Time
---------------------

.. http:put:: /users/current-user/borrowed-items/(int:borrow_id)

    Allows the `User` to extend the length of time they have access to an `Item` they have already borrowed.

    :>json string href:
        A string-encoded link to the Shared-Catalog Item Details the User wishes to extend the
        borrowing time of.

    **Request Example**

    .. sourcecode:: http

        PUT /users/current-user/borrowed-items/745823 HTTP/1.1
        Accept: application/json
        Accept-Language: id-ID
        Accept-Charset: utf-8
        Authorization: Bearer MTIzNDp3ZTRzY29vcA==
        Content-Type: application/json
        User-Agent: ScoopIOS/4.7.0; iOS 3.2.1 (iPhone 6s)

        {
            "href" : "https://api.apps-foundry.com/organizations/1/shared-catalogs/12/9998"
        }

    **Response Example**

    .. sourcecode:: http

        HTTP/1.1 200 OK

        **SAME REPSONSE AS GET DETAILS**


List Borrowed Items History
---------------------------

.. http:get:: /users/(int:user_id)/borrowed-items-history

    Retrieves a paginated list of BorrowedItemSummary objects that already returned (history of borrowed item for a `user`).

    **Request Example**

    .. sourcecode:: http

        GET /users/(int:user_id)/borrowed-items-history HTTP/1.1
        Accept: application/json
        Accept-Charset: utf-8
        Accept-Language: id-ID
        Authorization: Bearer MTIzNDp3ZTRzY29vcA==
        User-Agent: ScoopIOS/4.7.0; iOS 3.2.1 (iPhone 6s)

    **Response Example**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        ETag: "737060cd8c284d8af7ad3082f209582d"
        Location: https://api.apps-foundry.com/users/337374/borrowed-items-history

        {
            "items": [
                {
                    "id": 745823,
                    "title": "Into to Algebra",
                    "href" : "https://api.apps-foundry.com/users/current-user/borrowed-items/745823",
                    "details": {
                        "href": "https://api.apps-foundry.com/items/9998",
                        "title": "Detil Item"
                    },
                    "cover_image": {
                        "href": "https://images.getscoop.com/items/cover-images/9998.jpg",
                        "title": "Gambar Sampul"
                    },
                    "borrowing_start_time": "2016-10-03T13:28:34+00:00",
                    "expires": "2016-10-10T01:23:45Z",
                    "authors": [
                        {
                          "href": "https://api.apps-foundry.com/authors/201",
                          "id": 201,
                          "title": "Imelda Akmal Architecture Writer Studio"
                        }
                    ],
                    "vendor": {
                        "href": "https://api.apps-foundry.com/vendors/251",
                        "title": "Penerbit Apa Saja"
                    },
                    "download": {
                        "href": "https://api.apps-foundry.com/v1/items/9998/download",
                        "title": "Download buat Baca Offline"
                    },
                    "catalog": {
                        "href": "https://api.apps-foundry.com/v1/organizations/1234/shared-catalogs/12",
                        "rel": "catalog",
                        "title": "Math Books"
                    }
                }
            ],
            "metadata": {
                "resultset": {
                    "offset": 0,
                    "limit": 20,
                    "count": 1
                }
            }
        }
