Refresh Token
===========================

Refresh Authorization Token.

**URL:** /auth/refresh-token

**Method:** GET

**Header Fields:**

Required:

* `Authorization` - Token (user credential) that will be refreshed. Ex: JWT old.token.to-be-refreshd

* `X-Scoop-Client` - set the value to related app name in Clients data, ex: 'scoop ios/1.0.0'


**Response HTTP Code:**

* `200` - Resource owner credentials successfully auth'ed

* `400` - Bad Request, Authorization header is empty/not defined

* `401` - Unauthorized, Token in Authorization header is invalid or already expired

**Response Params:**

* `token` - String; New Access token

* `realm` - String; Type of access token (ex: JWT for JSON Web Token)

* `expires` - Number; Token expiration in epoch timestamp (in seconds since 1970)


.. http:get:: /v1/auth/refresh-token

    Login

    **Example Request**

    .. sourcecode:: http

        GET v1/auth/refresh-token HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Authorization: JWT dksfjldsakfjsadf.fklsdfjsldfldsafjdsaklfjsdalkf.dfljsdafjdlasfdsf
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop IOS/3.0

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
          "expires": 1475654265,
          "realm": "JWT",
          "token": "esyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2fVyX2lkIjo1MTUxMTB9.8BqTwsTDEt3jh-ojEL3FJ8wFzQoF-7Whdjg3fnXl8Bdg"
        }



    **Example Fail Response**

    .. sourcecode:: http

        HTTP/1.1 400 Bad Request
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
            "code": 400,
            "error_code": 400,
            "developer_message": "Failed to get authorization from request header"
            "user_message": "Failed to get authorization from request header"
        }

    .. sourcecode:: http

        HTTP/1.1 401 Unauthorized
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
            "code": 401,
            "error_code": 401,
            "developer_message": "Token expired"
            "user_message": "Your session has expired"
        }

    .. sourcecode:: http

        HTTP/1.1 401 Unauthorized
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
            "code": 401,
            "error_code": 401,
            "developer_message": "Token invalid, decode error"
            "user_message": "Token invalid"
        }
