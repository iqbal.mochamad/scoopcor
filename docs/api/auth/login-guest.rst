Login Guest
===========

For iOS apps, Login as a guest

**URL:** /auth/login-guest

**Method:** POST

**Header Fields:**

Required:

* `Content-Type` - Set the value to "application/json"

* `X-Scoop-Client` - set the value to related app name in Clients data, ex: 'scoop ios/1.0.0'

**Request Data Params:**

Required:

* `unique_id` - String; Required; Resource owner username

* `device_id` - String; Unique identifier of the device. See [Register Device](#register-device) about how to get this unique identifier

**Response HTTP Code:**

* `200` - Resource owner credentials successfully auth'ed

* `400` - Bad Request, request body is invalid (ex: username or password is empty/not defined)

* `420` - User Devices exceed the limit count. Ex: 5 devices is allowed per user, if user try to login from the 6th device, it will throw this error

**Response Params:**

* `id` - Number; User identifier

* `token` - String; Access token

* `realm` - String; Type of access token (ex: 'JWT'), this value should used in HTTP Header Authorization param

* `first_name` - String; User first name

* `last_name` - String; User last name

* `email` - String; User email

* `href` - Uri; Http url to get details of current user

* `expire_timedelta` - Number: Token expiration in seconds (ex: 3600, Token will expire in 3600 seconds = 1 hour)

* `expires` - Number; Token expiration in epoch timestamp (in seconds since 1970)

* `is_verified` - Boolean; User's email verification status


.. http:post:: /v1/auth/login-guest

    Login

    **Example Request**

    .. sourcecode:: http

        POST v1/auth/login-guest HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop IOS/3.0

        {
            "unique_id": "4015038fa71ea4c712a417ae26e87a36fd3e63a51232",
            "device_id": "ios123fjdkdfdjfdfs"
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
          "email": "4015038fa71ea4c712a417ae26e87a36fd3e63a51232@guest.scoop",
          "expires": 1475654265,
          "expire_timedelta": 300,
          "first_name": null,
          "href": "http://dev.apps-foundry.com/scoopcor/api/v1/users/11234",
          "id": 11234,
          "last_name": null,
          "realm": "JWT",
          "token": "efyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmdghbml6YXRpb25zIjpbXSwidXNlcl9pZCI6NDQyMDA3LCJyb2xlcyI6WzEwXSwiZXhwIjoxNDc1NjU0MjY1LCJ1c2VyX25hbWUiOiJtYXJrZXRpbmdzY29vcGNvcmV0ZXN0QGdtYWlsLmNvbSIsImVtYWlsIjoibWFya2V0aW5nc2Nvb3Bjb3JldGVzdEBnbWFpbC5jb20ifQ.ypxmVgl-ukURyOaseQV6zCl8j2zMpd_4hLNbZ2Q8lKdE",
          "is_verified": true
        }


    **Example Fail Response**

    .. sourcecode:: http

        HTTP/1.1 400 Bad Request
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
            "code": 400,
            "error_code": 400,
            "developer_message": "Unique id is empty"
            "user_message": "Please define unique guest id for username"
        }

    .. sourcecode:: http

        HTTP/1.1 420
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
            "code": 420,
            "error_code": 420,
            "developer_message": "Maximum number of device for eLibrary is 1"
            "user_message": "You have reached maximum number of authorized devices. Please deauthorized first then re-login"
        }
