Facebook Deauthorize Device
===========================

Deauthorize all device (of a user) with facebook access token.

**URL:** /auth/facebook-deauthorize-all

Deauthorize the oldest device (of a user) with facebook access token.

**URL:** /auth/facebook-deauthorize

**Method:** POST

**Header Fields:**

Required:

* `Content-Type` - Set the value to "application/json"

* `X-Scoop-Client` or `User-Agent` - set the value to related app name in Clients data, ex: 'scoop ios/1.0.0'

Required:

* `fb_access_token` - String; Facebook access token

**Response HTTP Code:**

* `200` - Resource owner credentials successfully authenticated and device deauthorized

* `422` - Unprocessable Entity, request body is invalid (ex: Content-Type, fb_access_token or client_id is empty/not defined)

* `401` - Unauthorized, User is invalid (email not found for the given facebook access token)



.. http:post:: /v1/auth/facebook-deauthorize

    Deauthorize/remove the oldest device in user device list

    **Example Request**

    .. sourcecode:: http

        POST v1/auth/facebook-deauthorize HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        User-Agent: Scoop IOS/3.0
        X-Scoop-Client: Scoop IOS/3.0

        {
            "fb_access_token": "CAAKvgAX7BZCUBAAnqDP3mMh6iF4GZBMSJVTXHe5aCxa1eqVp7yxZBp91xFUZCShhHT4QZAK4ZBcq7BSSCDN9zlLC2Eq5VyHxDSkwnSMHxxYU6YFkecUI9j9EH8kdcPPgRcKSV3tZBPgJKKhZB0VcZA0PlmnF3llgSOuCwrIfXDN0FRwRsr3A4y9bHpRrcQYaSmp4ZD"
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT


.. http:post:: /v1/auth/facebook-deauthorize-all

    Deauthorize/remove all device in user device list

    **Example Request**

    .. sourcecode:: http

        POST v1/auth/facebook-deauthorize-all HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        User-Agent: Scoop IOS/3.0
        X-Scoop-Client: Scoop IOS/3.0

        {
            "fb_access_token": "CAAKvgAX7BZCUBAAnqDP3mMh6iF4GZBMSJVTXHe5aCxa1eqVp7yxZBp91xFUZCShhHT4QZAK4ZBcq7BSSCDN9zlLC2Eq5VyHxDSkwnSMHxxYU6YFkecUI9j9EH8kdcPPgRcKSV3tZBPgJKKhZB0VcZA0PlmnF3llgSOuCwrIfXDN0FRwRsr3A4y9bHpRrcQYaSmp4ZD"
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT


    **Example Fail Response**

    .. sourcecode:: http

        HTTP/1.1 422 Unprocessable Entity
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
          "status": 422,
          "developer_message": {
             "fb_access_token": ["Missing data for required field."]
             },
          "error_code": 422,
          "user_message": "Your request contained invalid data."
        }

    .. sourcecode:: http

        HTTP/1.1 401 Unauthorized
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
            "code": 401,
            "error_code": 401,
            "developer_message": "Email for this facebook token is not found in user data"
            "user_message": "The username or password you entered is incorrect."
        }
