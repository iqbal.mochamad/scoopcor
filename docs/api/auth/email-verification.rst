Email Verification
==================

Endpoints that allow a :doc:`user </api/users/users>` to verify their e-mail address in order
to receive free SCOOPOINTS.

.. note::

    Users do not have to verify their account in order to receive
    full access to SCOOP... this is only here to award them free
    SCOOPPOINTS.


Send Verification Email (v2)
----------------------------

This will send an e-mail to the User's registered e-mail address, which
contains a unique 'verification_code'.

.. note::

    This endpoint does not required authentication.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /users/{user.id}/email_verifications                 |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+------------------------------------------------------+
| Request Headers       | :doc:`/api/v2-common/requests`                       |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Email Sent Successfully              |
+-----------------------+---------------+--------------------------------------+
| Error Response        | :doc:`/api/v2-common/errors`                         |
+-----------------------+------------------------------------------------------+


Examples
^^^^^^^^

**Request**

.. code-block:: http

    POST /users/1/email_verifications
    Accept: application/json

**Response**

.. code-block:: http

    HTTP/1.1 204 No Content


Confirm Email Address (v2)
--------------------------

Once the verification_token is retrieved, the user can PUT that data back
to our server to confirm that they actually own the e-mail address they
registered with.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /users/{user.id}/email_verifications                 |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+------------------------------------------------------+
| Request Headers       | :doc:`/api/v2-common/requests`                       |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Email Address Confirmed              |
+-----------------------+---------------+--------------------------------------+
| Error Response        | :doc:`/api/v2-common/errors`                         |
+-----------------------+------------------------------------------------------+


**Request JSON Properties**

verification_token (Required; String)
    A unique token which the user can retrieve the verification e-mail they received.

.. note::

    In practice, the verification_token is automatically encoded as part of the
    callback URL that the user must click from their e-mail.

Examples
^^^^^^^^

**Request**

.. code-block:: http

    PUT /users/1/email_verifications HTTP/1.1
    Accept : application/json
    Content-Type: application/json; charset=utf-8

    {
        "verification_token": "ZGMwODY2ZW123adcewr99fdZGMwODY2ZWaf2"
    }

**Success response:**

.. code-block:: http

    HTTP/1.1 204 No Content
