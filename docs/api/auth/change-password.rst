Change Password
===============

Updates a :doc:`User's </api/users/users>` password.


Version 2
---------

.. deprecated::

    This endpoint is depreciated.  Please migrate to /auth/change-password

.. note::

    This API must authenticate with the the APPLICATION bearer, not with the
    USER's bearer.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /auth/change-password                                |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+------------------------------------------------------+
| Request Headers       | :doc:`/api/v2-common/requests`                       |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Changed Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | :doc:`/api/v2-common/errors`                         |
+-----------------------+------------------------------------------------------+


**Request JSON Properties**

password (Required; String)
    The user's current plaintext (non-hashed) password
new_password (Required; String)
    The user's desired new plaintext (non-hashed) password.


Examples
^^^^^^^^

**Request**

.. code-block:: http

    PUT /auth/change-password HTTP/1.1
    Accept : application/json
    Content-Type: application/json
    Authorization: JWT MzM3MzczOjQxNGU2ZGMwODY2ZWQ0ZmYwZmE0NGE0OTY2OTNiYjgy.dfsadfsafs.ereewrwrwerewrdfdsaff

    {
        "password": "we4scoop",
        "new_password": "we5scoop"
    }


**Response**

.. code-block:: http

    HTTP/1.1 204 No Content


**Response HTTP Code:**

* `204` - No Content, password changed successfully

* `422` - Unprocessable Entity, request body is invalid (ex: new password is empty/not defined)

* `401` - Unauthorized. JWT Token is invalid/expired or password is invalid
