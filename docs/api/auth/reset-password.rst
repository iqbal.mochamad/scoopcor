Reset Forgotten Password
========================

Generates a 'reset password' email and sends to a :doc:`User's </api/auth/users/users>`
registered e-mail address.

This is a two-step process.  Once a reset code is generated, it can be used as a
temporary authorization token (sent in the JSON body, not as a replacement for
the Authorization header.

.. note::

    Users in the following groups cannot access this endpoint:

    * Cafe Users
    * Banned Users
    * Guest Users
    * Users whose is_active attribute is false.

.. note::

    No authorization is required to access these endpoints.  Authorization
    is determined by the user's temporary reset token.


Generate Reset E-mail (v2)
--------------------------

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /auth/reset-password                                |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+------------------------------------------------------+
| Request Headers       | :doc:`/api/v2-common/requests`                       |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Reset e-mail sent                    |
+-----------------------+---------------+--------------------------------------+
| Error Response        | :doc:`/api/v2-common/errors`                         |
+-----------------------+------------------------------------------------------+

Examples
^^^^^^^^

**Request**

.. code-block:: http

    POST /auth/reset-password HTTP/1.1
    Accept : application/json
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "username": "js"
    }

**Response**

.. code-block:: http

    HTTP/1.1 200 OK


Reset Password (v2)
-------------------

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /auth/reset-password                                |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+------------------------------------------------------+
| Request Headers       | :doc:`/api/v2-common/requests`                       |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Changed Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | :doc:`/api/v2-common/errors`                         |
+-----------------------+------------------------------------------------------+

**Request JSON Properties**

reset_password_token (Required; String)
    Reset password token retreived from the reset e-mail.

new_password (Required; String)
    The user's chosen new plaintext password (non-hashed)

Examples
^^^^^^^^

**Request**

.. code-block:: http

    PUT /auth/reset-password HTTP/1.1
    Accept : application/json
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "reset_password_token": "jashd8273y7a6sf723f7gsad898dhao23jn09",
        "new_password": "NGU2ZGMwODY2ZWQ0ZmYwZmE0NGE0O"
    }

**response**

    HTTP/1.1 204 No Content

