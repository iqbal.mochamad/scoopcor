Verify User and Password
========================

Verify user is valid with the given password

**URL:** /auth/verify-password

**Method:** POST

**Header Fields:**

Required:

* `Content-Type` - Set the value to "application/json"

* `X-Scoop-Client` - set the value to related app name in Clients data, ex: 'scoop ios/1.0.0'

**Request Data Params:**

Required:

* `username` - String; Resource owner username

* `password` - String; Resource owner password (hashed)

**Response HTTP Code:**

* `204` - User + password is valid

* `400` - Bad Request, request body is invalid (ex: username or password is empty/not defined)

* `401` - Unauthorized, User or password is invalid

.. http:post:: /v1/auth/verify-password

    Verify user + password

    **Example Request**

    .. sourcecode:: http

        POST v1/auth/verify-password HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36
        X-Scoop-Client: Scoop IOS/3.0

        {
            "username": "username@gmail.com",
            "password": "4015038fa71ea4c712a417ae26e87a36fd3e63a51232"
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 204 NO CONTENT
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT


    **Example Fail Response**

    .. sourcecode:: http

        HTTP/1.1 400 Bad Request
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
            "code": 400,
            "error_code": 400,
            "developer_message": "username or password is empty"
            "user_message": "Please define username and password."
        }

    .. sourcecode:: http

        HTTP/1.1 401 Unauthorized
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
            "code": 401,
            "error_code": 401,
            "developer_message": "Username not found"
            "user_message": "The username or password you entered is incorrect."
        }
