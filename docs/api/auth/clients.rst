Clients
=======

A `Client` is an application that can access the ScoopCOR API.

The client is identified by the HTTP User-Agent header and should be in the format of:

{ClientName}/{ClientVersion} [(Optional information, such as OS Version, Hardware Info)]

For HTTP Clients that cannot directly set their User-Agent header (such as browser XHR requests),
you may use the header X-Scoop-Client.

Basically, the only purpose of this endpoint is to verify that your application's client detection
is functioning properly.


Retrieve the Current Client
---------------------------

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /clients/current-client                              |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+------------------------------------------------------+
| Request Headers       | :doc:`/api/v3-common/requests`                       |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Client information retrieved         |
+-----------------------+---------------+--------------------------------------+
| Error Response        | :doc:`/api/v3-common/errors`                         |
+-----------------------+------------------------------------------------------+


Examples
^^^^^^^^

**Request**

.. code-block:: http

    GET /clients/current-client
    Accept: application/vnd.scoop.v3+json
    User-Agent: Scoop IOS/1.2.3 (iPhone 6s; iOS 6.7.8)

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

    {
        "app_name": "scoop_ios"
    }
