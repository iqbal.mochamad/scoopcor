Facebook Login
==============

Account login with Resource Owner Password Credentials Grant.

**URL:** /auth/facebook

**Method:** POST

**Header Fields:**

Required:

* `Content-Type` - Set the value to "application/json"

* `X-Scoop-Client` or `User-Agent` - set the value to related app name in Clients data, ex: 'scoop ios/1.0.0'

**Request Data Params:**

Required:

* `fb_access_token` - String; Facebook access token

Other Request params:

* `device_id` - String; Unique identifier of the device. See [Register Device](#register-device) about how to get this unique identifier

* `origin_device_model` - String; User's origin device model

* `origin_partner_id` - Number; User's origin device partner identifier

**Response HTTP Code:**

* `200` - Resource owner credentials successfully authenticated

* `422` - Unprocessable Entity, request body is invalid (ex: Content-Type, fb_access_token or client_id is empty/not defined)

* `420` - User Devices exceed the limit count. Ex: 5 devices is allowed per user, if user try to login from the 6th device, it will throw this error

**Response Params:**

* `id` - Number; User identifier

* `token` - String; Access token

* `realm` - String; Type of access token (ex: 'JWT'), this value should used in HTTP Header Authorization param

* `first_name` - String; User first name

* `last_name` - String; User last name

* `email` - String; User email

* `href` - Uri; Http url to get details of current user

* `expire_timedelta` - Number: Token expiration in seconds (ex: 3600, Token will expire in 3600 seconds = 1 hour)

* `expires` - Number; Token expiration in epoch timestamp (in seconds since 1970)

* `is_verified` - Boolean; User's email verification status


.. http:post:: /v1/auth/login

    Login

    **Example Request**

    .. sourcecode:: http

        POST v1/auth/facebook HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        User-Agent: Scoop IOS/3.0
        X-Scoop-Client: Scoop IOS/3.0

        {
            "fb_access_token": "CAAKvgAX7BZCUBAAnqDP3mMh6iF4GZBMSJVTXHe5aCxa1eqVp7yxZBp91xFUZCShhHT4QZAK4ZBcq7BSSCDN9zlLC2Eq5VyHxDSkwnSMHxxYU6YFkecUI9j9EH8kdcPPgRcKSV3tZBPgJKKhZB0VcZA0PlmnF3llgSOuCwrIfXDN0FRwRsr3A4y9bHpRrcQYaSmp4ZD",
            "device_id": "XHe5aCxa1eqVp7yxZBp91xFUZCShhHT4QZAK4ZBcq7BSSCD",
            "origin_device_model": "HTC-X",
            "origin_partner_id": 2
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
          "id": 11234,
          "token": "efyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvcmdghbml6YXRpb25zIjpbXSwidXNlcl9pZCI6NDQyMDA3LCJyb2xlcyI6WzEwXSwiZXhwIjoxNDc1NjU0MjY1LCJ1c2VyX25hbWUiOiJtYXJrZXRpbmdzY29vcGNvcmV0ZXN0QGdtYWlsLmNvbSIsImVtYWlsIjoibWFya2V0aW5nc2Nvb3Bjb3JldGVzdEBnbWFpbC5jb20ifQ.ypxmVgl-ukURyOaseQV6zCl8j2zMpd_4hLNbZ2Q8lKdE",
          "realm": "JWT",
          "email": "username@gmail.com",
          "expires": 1475654265,
          "expire_timedelta": 300,
          "first_name": "marketing",
          "href": "http://dev.apps-foundry.com/scoopcor/api/v1/users/current-user",
          "last_name": "scoop",
          "is_verified": true
        }


    **Example Fail Response**

    .. sourcecode:: http

        HTTP/1.1 422 Unprocessable Entity
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
          "status": 422,
          "developer_message": {
             "fb_access_token": ["Missing data for required field."]
             },
          "error_code": 422,
          "user_message": "Your request contained invalid data."
        }


    .. sourcecode:: http

        HTTP/1.1 420
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
            "code": 420,
            "error_code": 420,
            "developer_message": "Maximum number of device for eLibrary is 1"
            "user_message": "You have reached maximum number of authorized devices. Please deauthorized first then re-login"
        }
