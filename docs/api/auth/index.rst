Auth Endpoints
==============


.. note::

    For information on registering a new account, please see: :doc:`/api/users/users`

.. toctree::
    :maxdepth: 1

    login
    facebook-login
    refresh-auth-token
    deauthorize
    facebook-deauthorize
    email-verification
    change-password
    reset-password
    verify-auth-token
    device

