Deauthorize Device
==================

Create unique id for a device of user.

**URL:** /auth/device

**Method:** POST

**Header Fields:**

Required:

* `Content-Type` - Set the value to "application/json"

* `X-Scoop-Client` - set the value to related app name in Clients data, ex: 'scoop ios/1.0.0'

Required:

* `device_imei` - String; Device's IMEI

* `device_mid` - String; Random string value from apps

* `device_model` - String; Device's model name


**Response HTTP Code:**

* `201` - user device is registered and unique id for that device will be returned

* `422` - 422 - Unprocessable Entity, required data not send correctly


.. http:post:: /v1/auth/device

    **Example Request**

    .. sourcecode:: http

        POST v1/auth/device HTTP/1.1
        Accept: application/vnd.scoop.v3+json
        Accept-Language: id-ID
        Content-Type: application/json
        X-Scoop-Client: Scoop IOS/3.0

        {
            "device_imei": "358860060103228",
            "device_mid": "24edfd99accad16",
            "device_model": "EVERCOSS A65"
        }

    **Example Response**

    .. sourcecode:: http

        HTTP/1.1 201 CREATED
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
            "unique_id": "12312faklfjdsfkljsadfldsafjsdlfj",
            "device_imei": "358860060103228",
            "device_mid": "24edfd99accad16",
            "device_model": "EVERCOSS A65"
        }


    **Example Fail Response**

    .. sourcecode:: http

        HTTP/1.1 422 Unprocessable Entity
        Content-Type: application/json
        Content-Length: 580
        Date: Wed, 07 Sep 2016 11:35:10 GMT
        Last-Modified: Wed, 07 Sep 2016 11:35:10 GMT

        {
            "code": 422,
            "error_code": 422,
            "developer_message": "Device imei more than 20 digits"
            "user_message": "Register device failed. Device imei more than 20 digits"
        }
