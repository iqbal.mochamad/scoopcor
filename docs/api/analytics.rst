Analytics
=========

API for posting and retrieving data about user's interactions with the
Scoop apps (ie, when the user views a page or attempts to download a file).

.. warning::

    This API is different than most of our other CRUD-APIs in that it
    doesn't allow for updating or deleting records, and does not directly
    support retrieving individual records.  Also, this endpoint makes
    it possible to post data in a bulk format (locking it in to JSON-only)
    for the post format of the bulk endpoints.

location - String; Location where the pageview is accessed. Shown based on Latitude and Logitude, separated by space. It must be +/- numbers and not include N/S W/E suffixes
online_status - String; Whether the pageview is accessed in "online" or "offline" mode (cached data). If the pageview is accessed via getSCOOP.com, it will always be "online"
os_version - String; The version of operating system the user used

Properties
----------

Analytic log entities are actually divided into several different resource
types, each having it's own unique attributes.


Common Properties (all entities)
    id
        Number; Automatically-generated instance identifier.

    client_version
        String; The application version the user is currently using.

    datetime
        String (ISO 8601-encoded Datetime); Datetime that the user
        performed the given action.

    device_id
        String; Device identifier **DESCRIBE BETTER**

    device_model
        String; Device model **DESCRIBE BETTER**

    user_id
        Number; User account 'id' property retrieved from Scoopcas.

    session_name
        String; Name of the session **DESCRIBE BETTER**

    ip_address
        String; The current IPv4 address of the user's device.

Download (User attempt to download an owned-item)
    item_id
        Number;  The id attribute of the :doc:`Item </web-api-v1/items/items>`
        that corresponds to the file that was downloaded.

    activity_type
        String (static value);  Always 'download'

Pageview (User views a page of an owned-item)
    item_id
        Number;  The id attribute of the :doc:`Item </web-api-v1/items/items>`
        that corresponds to the file that was currently being viewed on the
        user's device.

    page_orientation
        String (enum: ["landscape", "potrait"]);  The orientation mode of the
        user's device at the time that page was viewed.

    page_number
        Array (of int); The page(s) that were currently being viewed.  Even
        if it was a single page, this should be sent as an array.

    duration
        Number; The amount of time (in seconds) the user is viewing the page.

    online_status
        String (enum: ["online", "offline"]);  Indication of whether the user
        had web access at the time the page was viewed.

    activity_type
        String (static value);  Always 'pageview'

AppOpen (User opens application)
    activity_type
        String (static value);  Always 'app_open'

Bulk Create
-----------

Allows bulk posting of multiple pageviews and downloads simultaniously.


.. code-block:: json

    {
        "pageviews": [],
        "downloads": [],
        "app_opens": [],
    }

These take the same formats as their singular posting endpoints listed
below.

Create
------

List
----

Example POST Request

.. code-block:: json

    {
        "device_id": "my-device",
        "user_id": 123,
        "item_id": 123,
        "session_name": "my-session",
        "ip_address": "127.0.0.1",
        "location": "54.24 23.57",
        "client_version": "some-version",
        "os_version": "some-version",
        "device_model": "some-model",
        "datetime": "2015-10-10T12:00:00Z",
        "activity_type": "pageview",
        "page_orientation": "landscape",
        "page_number": 1,
        "duration": 60,
        "online_status": "online"
    }


Update
------

Detail
------


Example POST Request

.. code-block:: json

    {
        "device_id": "my-device",
        "user_id": 123,
        "item_id": 123,
        "session_name": "my-session",
        "ip_address": "127.0.0.1",
        "location": "54.24 23.57",
        "client_version": "some-version",
        "os_version": "some-version",
        "device_model": "some-model",
        "datetime": "2015-10-10T12:00:00Z",
        "activity_type": "download",
        "download_status": "success"
    }

Example GET Request

.. code-block:: json

    {
        "id": 123456,
        "device_id": "my-device",
        "user_id": 123,
        "item_id": 123,
        "session_name": "my-session",
        "ip_address": "127.0.0.1",
        "location": "54.24 23.57",
        "client_version": "some-version",
        "os_version": "some-version",
        "device_model": "some-model",
        "datetime": "2015-10-10T12:00:00.000+00:00",
        "activity_type": "download",
        "download_status": "success"
    }
