Geolocation
===========

This resource is used to retrieve geolocation informations.
Currently the supported input is IP Address.


Properties
----------

city
    Object; City of the geolocation.

    name
        String; City's name.

postal
    Object; Postal of the geolocation.

    code
        Number; Postal code.

subdivision
    Object; Subdivision of the country.

    iso_code
        String; Subdivision's ISO code.

    name
        String; Subdivision's name.

location
    Object; Location of the geolocation.

    latitude
        Number; Location's latitude.

    longitude
        Number; Location's longitude.

    accuracy_radius
        Number; Location's accuracy radius.

    time_zone
        String; Location's time zone based on
        `IANA Time Zone Database <http://en.wikipedia.org/wiki/List_of_IANA_time_zones>`_.

    metro_code
        Number; Location's metro code.

traits
    Object; Traits of the network.

    domain
        String; Traits' domain.

    isp
        String; Internet Service Provider information.

    user_type
        Number; User type.

    organization
        String; Time zone based on
        `IANA Time Zone Database <http://en.wikipedia.org/wiki/List_of_IANA_time_zones>`_.

    ip_address
        String; Internet Protocol address.

    is_anonymous_proxy
        Boolean; Anonymous proxy status, used to mark if the proxy is
        anonymous or not.

    is_satellite_provider
        Boolean; Satellite provider status, used to mark if the provider is satellite based or not.

country
    Object; Country of the geolocation.

    code_alpha2
        String; Country code in
        `ISO 3166-1 alpha-2 <http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2>`_.

    name
        String; Country name.

represented_country
    Object; Country represented by the user when that country is different
    than the country in country. For instance, the country represented by
    an overseas military base.

    code_alpha2
        String; Repesented country code in `
        ISO 3166-1 alpha-2 <http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2>`_.

    name
        String; Repesented country name.

    confidence
        String; Confidence.

    represented_country
        String; Repesented country.

continent
    Object; Continent of the geolocation.

    code
        String; Continent's code.

    name
        String; Continent's name.


Geolocate IP Address
--------------------

Retrieve location information from provided IP address.  This endpoint supports
use of both IPv4 and IPv6 addresses.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/geolocations/ip_address/{ipv4_or_ipv6_address}   |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | Could not resolve location           |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /geolocations/ip_address/217.33.193.179?fields=code_alpha3 HTTP/1.1
    Authorization: Bearer MzM3MzczOjQxNGU2ZGMwODY2ZWQ0ZmYwZmE0NGE0OTY2OTNiYjgy
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "city":{
            "name":null
        },
        "postal":{
            "code":null
        },
        "subdivision":{
            "iso_code":null,
            "name":null
        },
        "location":{
            "latitude":39.76,
            "accuracy_radius":null,
            "time_zone":null,
            "longitude":-98.5,
            "metro_code":null
        },
        "traits":{
            "domain":null,
            "isp":null,
            "user_type":null,
            "organization":null,
            "ip_address":"2607:f0d0:1002:51::4",
            "is_anonymous_proxy":false,
            "is_satellite_provider":false
        },
        "country":{
            "code_alpha2":"US",
            "name":"United States"
        },
        "represented_country":{
            "code_alpha2":null,
            "confidence":null,
            "represented_country":null,
            "name":null
        },
        "continent":{
            "code":"NA",
            "name":"North America"
        }
    }

:download:`Schema <_schemas/geolocation/detail-geolocation-resp.json>`
