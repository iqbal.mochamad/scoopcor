Server Metadata
===============

Retrieve metadata about the API, including it's current timestamp and the
API status.  This endpoint is the root of the entire V1 API.


Properties
----------

api_status
    String (enum: ['normal', 'maintenance']); The status of API while being
    accessed

server_timestamp
    String (ISO8601 DateTime); Datetime when the server is accessed


Retrieve Server Timestamp
-------------------------

Retrieves the server timestamp and API status

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1                                                  |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET http://scoopadm.apps-foundry.com/scoopcor/api/v1 HTTP/1.1
    Authorization: Bearer MzM3MzczOjQxNGU2ZGMwODY2ZWQ0ZmYwZmE0NGE0OTY2OTNiYjgy
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "api_status": "normal",
        "server_timestamp": "2015-05-21T07:35:30+00:00"
    }

:download:`Schema <_schemas/server-meta/detail-servermeta-resp.json>`
