Miscellaneous
=============

API resources that generally don't fit into other logical categories.

.. toctree::
    :maxdepth: 1

    countries
    currencies
    languages
    geolocation
    server-metadata
