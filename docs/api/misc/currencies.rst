Currencies
==========

Currency resource.


Properties
----------

id
     Number; Currency identifier

iso4217_code
     String; Unique ISO 4217 code. eg: USD, IDR, GBP.
     This property can be used as alternative identifier

right_symbol
     String; Currency right symbol

left_symbol
     String; Currency left symbol

one_usd_equals
     Float String; 1 USD comparison to the currency value

group_separator
     String; Thousand group separator. Default to comma ( , )

decimal_separator
     String; Decimal separator. Default to dot ( . )


Create
------

Create new currency.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/currencies                                       |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| iso4217_code                      |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| right_symbol                      |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| left_symbol                       |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| one_usd_equals                    |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| group_separator                   |           | ","                          |
+-----------------------------------+-----------+------------------------------+
| decimal_separator                 |           | "."                          |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /currencies HTTP/1.1
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "iso4217_code": "USD",
        "right_symbol": "",
        "one_usd_equals": "1.0",
        "left_symbol": "$",
        "group_separator": ".",
        "decimal_separator": ","
    }

:download:`Schema <_schemas/currencies/create-currency-req.json>`

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scoopadm.apps-foundry.com/scoopcor/api/currencies/1

.. code-block:: json

    {
        "id": 1,
        "iso4217_code": "USD",
        "right_symbol": "",
        "one_usd_equals": "1.0",
        "left_symbol": "$",
        "group_separator": ".",
        "decimal_separator": ","
    }

:download:`Schema <_schemas/currencies/create-currency-resp.json>`


List
----

Retrieve a paginated list of currencies.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/currencies                                       |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /currencies HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "currencies": [
            {
                "id": 1,
                "iso4217_code": "USD",
                "right_symbol": "",
                "one_usd_equals": "1.0",
                "left_symbol": "$",
                "group_separator": ",",
                "decimal_separator": "."
            },
            {
                "id": 2,
                "iso4217_code": "IDR",
                "right_symbol": "",
                "one_usd_equals": "11000",
                "left_symbol": "Rp",
                "group_separator": ".",
                "decimal_separator": ","
            }
        ],
        "metadata": {
            "resultset": {
                "count": 2,
                "offset": 0,
                "limit": 2
            }
        }
    }

:download:`Schema <_schemas/currencies/list-currency-req.json>`


Detail
------

Retrieve individual currency with identifier.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/currencies/{id}                                  |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Request was OK and has data          |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad headers                          |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /currencies/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "id": 1,
        "iso4217_code": "USD",
        "right_symbol": "",
        "one_usd_equals": "1.0",
        "left_symbol": "$",
        "group_separator": ",",
        "decimal_separator": "."
    }

:download:`Schema <_schemas/currencies/detail-currency-resp.json>`


Update
------

Update currency.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/currencies/{id}                                  |
+-----------------------+------------------------------------------------------+
| HTTP Method           | PUT                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 200      | Record was updated                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers.                 |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

.. note::

    Object property requirements are exactly the same as for Create

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    PUT /currencies/1 HTTP/1.1
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "iso4217_code": "USD",
        "right_symbol": "",
        "one_usd_equals": "1.0",
        "left_symbol": "$"
    }

:download:`Schema <_schemas/currencies/update-currency-req.json>`

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

    {
        "iso4217_code": "USD",
        "right_symbol": "",
        "one_usd_equals": "1.0",
        "left_symbol": "$",
        "group_separator": null,
        "decimal_separator": null
    }

:download:`Schema <_schemas/currencies/update-currency-resp.json>`


Delete
------

Soft-deletes the resource specified in the URL, although the API response will
behave as if the resource were actually deleted.  In reality, this endpoint
will simply set the is_active property of the object to false.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /v1/currencies/{id}                                  |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                               |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 204      | Record was deleted                   |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad request (for delete, this would  |
|                       |               | typically be incorrect headers)      |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
|                       +---------------+--------------------------------------+
|                       | HTTP 404      | {id} does not exist                  |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    DELETE /currencies/1 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 204 No Content
