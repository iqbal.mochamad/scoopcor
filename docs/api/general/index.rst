General Information
===================

The documentation in this section serves to document information which is
common API-wide (so we don't have to pollute the rest of the documentation
covering the same things over and over again).


API Base URL
------------

+------------------------------------------------------------------------------+
| Base URL and Versions                                                        |
+===========================+==================================================+
| Production HTTPS          | https://scoopadm.apps-foundry.com/scoopcor/api   |
+---------------------------+--------------------------------------------------+
| Production HTTP           | http://scoopadm.apps-foundry.com/scoopcor/api    |
+---------------------------+--------------------------------------------------+
| Development HTTP          | * http://dev.apps-foundry.com/scoopcor/api       |
|                           | * http://dev2.apps-foundry.com/scoopcor/api      |
|                           | * http://dev3.apps-foundry.com/scoopcor/api      |
|                           | * http://dev4.apps-foundry.com/scoopcor/api      |
|                           | * http://dev5.apps-foundry.com/scoopcor/api      |
+---------------------------+--------------------------------------------------+

Versioning
----------

Major version is placed right after base URL with "v" prefix, e.g., for a
request to a version 2 endpoint, the URL would be something like the following:

.. code-block:: http

    https://scoopadm.apps-foundry.com/scoopcor/api/v2


HTTP Header Fields
------------------

Unless for being specified in certain end-point, all end-point needs HTTP
header fields as below.

Authorization
    Authorization information.  This is generally going to be an auth-token
    prefixed with "Bearer" (see authorization section below).
Cache-Control
    Set to 'no-cache' for everything, except GET requests.
Content-Type
    Use ONLY in requests where you're sending JSON data scoopcor, that we
    need to read out of the HTTP body of your request.  For requests
    where you are sending JSON data, it should be set to "application/json",
    and never "text/json".  (As a rule of thumb, set this on POST, PUT, and
    PATCH requests, and never on GET or DELETE requests).
Accept
    Set to "application/json" on anything that you expect JSON data back.
    As a rule of thumb, this should be set on everything except for DELETE
    requests.
Minor Version
    Api-Version - Set the value to the minor version of an API endpoint.

.. note::

    Please review the Minor-Version header.  I do not believe this is used
    anywhere.  If it's not, please remove from documentation

    30 May 15 - Derek


Authorization
-------------

Most of the end-points will need HTTP header *Authorization* parameter to
be provided with *Bearer* as the authorization type.

Each request will need to generate base 64 encoded string from
valid *user_id* and *access_token* with colon ( : ) delimited:

value = base64encode(<user_id>:<access_token>)

**Example:**

.. code-block::

    user_id = 337373

    access_token = a2435791637bca5be27b14877b01d69dcdc41e5e531a40d7a82417bb9e79e6a6e6b9e17b79682926

    Base 64 Encoded value = MzM3MzczOjQxNGU2ZGMwODY2ZWQ0ZmYwZmE0NGE0OTY2OTNiYjgy

    Authorization: Bearer MzM3MzczOjQxNGU2ZGMwODY2ZWQ0ZmYwZmE0NGE0OTY2OTNiYjgy


For more details about the authorization of the system, please refer to SCOOP CAS API specifications.


HTTP Responses
--------------

It's expected you understand HTTP status codes, however here's a quick recap
of what codes you can expect scoopcor to return to you in plain english (along
with some scoopcor-specific notes.

+-----------+------------------------------------------------------------------+
| HTTP Code | Description                                                      |
+===========+==================================================================+
| 200       | GET/PUT/PATCH request was successful.  Expect JSON response body.|
+-----------+------------------------------------------------------------------+
| 201       | POST request was successful.  Expect a JSON response body.       |
+-----------+------------------------------------------------------------------+
| 204       | Your request was successful, but we have nothing to show         |
|           | By common conventions, this indicates side-effects (a DELETE).   |
+-----------+------------------------------------------------------------------+
| 400       | Your headers or your request body was incorrect.                 |
+-----------+------------------------------------------------------------------+
| 401       | Bad token.                                                       |
+-----------+------------------------------------------------------------------+
| 403       | Valid token, but insufficient permissions (unused currently).    |
+-----------+------------------------------------------------------------------+
| 404       | Good request but resource doesn't exist.                         |
+-----------+------------------------------------------------------------------+
| 407       | Rare -- used in a bulk posting endpoints where each individual   |
|           | object returns it's own response code (nested within the JSON    |
|           | content of the reponse).                                         |
+-----------+------------------------------------------------------------------+
| 409       | Generally some kind of constraint violation: Related object id   |
|           | is invalid, or there is a unique constraint violation.           |
+-----------+------------------------------------------------------------------+
| 50x       | Probably our fault.  Tell us!                                    |
+-----------+------------------------------------------------------------------+

**Embedded Response Codes**

* 401103 - The resource owner credentials is invalid,
    access_token is invalid, expired, or revoked
* 403102 - Access denied
* 409101 - Request parameter conflicted with existing one on server-side


Making Requests to List Resources
---------------------------------

Scoopcor does not directly return a JSON array of objects from List-type
endpoints.  Instead, list data is wrapped inside of an object that contains
metadata about the entire collection, which can be used for pagination.

.. code-block:: json

    {
        "some-collection-name": [
            { "id": 1, "name": "some object" },
            { "id": 2, "name": "some other object" }
        ],
        "metadata": {
            "resultset": {
                "count": 10000,
                "limit": 2,
                "offset": 0
            }
        }
    }

