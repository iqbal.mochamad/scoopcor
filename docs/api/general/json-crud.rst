JSON Crud Requests
==================

Even though most of our API endpoints deal with very different object types, they all generally perform the
same functionality.


Listing
-------

List endpoints can summarize a set of data, but cannot return the exact same properties as the
``Fetch Detail`` endpoints.




Fetching Details
----------------

Creating
--------

Deleting
--------

Full-Update
-----------

