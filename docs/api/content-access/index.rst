Content Access
==============

Once users have purchased content though GETSCOOP or one of the mobile applications, they'll want to read it!

There are currently 2 types of content: ``mobile-bundles``, which is an encrypted zip file, containing the a PDF,
along with a set of pre-rendered thumbnails for each page.  There is also a ``streaming`` content option, where the
user can download a pre-rendered image for each page of the content.

.. toctree::

    mobile-bundles
    page-streaming
