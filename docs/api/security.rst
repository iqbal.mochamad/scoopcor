Security
--------

This uses OAuth2, however the vast majority of the OAuth2 features are unimplemented,
or purposely bypassed by our 3rd party client applications.

In practices, our APIs function more like standard API authentication, where the user
logs in by first providing their credentials, which are then swapped for a temporary
'Token', which is used for authentication on subsequent requests.

.. note::

    The perms parameter may be completely ignored.  If ignored, the user will
    be able to perform the full range of functions of which their account is
    authorized for.

