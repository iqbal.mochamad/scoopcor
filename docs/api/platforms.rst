
### Platforms

Platform resource to list platform supported by the system.

Current existing platforms are:

1. iOS

2. Android

3. Windows Phone

4. Web

Note: When new platform with tiered pricings added, [Offer](#offer) will also need to be populated with the platform's offers

#### Platform Properties

* `id` - Number; Instance identifier

* `name` - String; Unique offer type name

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true

* `slug` - String, Unique; Platform slug for SEO

* `meta` - String; Platform meta for SEO

* `description` - String; Platform description

#### Create Platform

Create new platform.

**URL:** /platforms

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `201` - New platform created

**Response Params:** See [Platform Properties](#platform-properties)

**Examples:**

Request:

```
POST /platforms HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "iOS",
    "sort_priority" : 1,
    "is_active": true,
    "slug": "ios"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/platforms/1

{
    "id": 1,
    "name": "iOS",
    "sort_priority" : 1,
    "is_active": true,
    "slug": "ios",
    "meta": null,
    "description": null
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Platforms

Retrieve platforms.

**URL:** /platforms

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Item Type Properties](#item-type-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Platforms retrieved and returned in body content

* `204` - No matched platform

**Response Params:** See [Platform Properties](#platform-properties)

**Examples:**

Request:

```
GET /platforms?fields=id,name&offset=0&limit=10 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "platforms": [
        {
            "id": 1,
            "name": "iOS",
            "sort_priority" : 1,
            "is_active": true,
            "slug": "ios",
            "meta": null,
            "description": null
        },
        {
            "id": 2,
            "name": "Android",
            "sort_priority" : 1,
            "is_active": true,
            "slug": "android",
            "meta": null,
            "description": null
        },
        {
            "id": 3,
            "name": "Windows Phone",
            "sort_priority" : 1,
            "is_active": true,
            "slug": "windows-phone",
            "meta": null,
            "description": null
        },
        {
            "id": 4,
            "name": "Web",
            "sort_priority" : 1,
            "is_active": true,
            "slug": "web",
            "meta": null,
            "description": null
        }
    ],
    "metadata": {
        "resultset": {
            "count": 4,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Platform

Retrieve individual platform using identifier.

**URL:** /platforms/{platform_id}

Optional:

* `fields` - Retrieve specific properties. See [Platform Properties](#platform-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Platform found and returned

**Response Params:** See [Platform Properties](#platform-properties)

**Examples:**

Request:

```
GET /platforms/1?fields=id,name HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "iOS"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Platform

Update platform.

**URL:** /platforms/{platform_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`

**Response HTTP Code:**

* `200` - Platform updated

**Response Params:** See [Platform Properties](#platform-properties)

**Examples:**

Request:

```
PUT /platforms/1 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "iOS",
    "sort_priority": 9,
    "slug": "ios",
    "is_active": true
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 1,
    "name": "iOS"
    "sort_priority": 9,
    "slug": "ios",
    "meta": null,
    "description": null,
    "is_active": true
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Platform

Delete platform. This service will set `is_active` value to false.

**URL:** /platforms/{platform_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Platform deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /platforms/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```
