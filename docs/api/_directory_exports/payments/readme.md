Payment API Docs
===============

<b>All request to payment need auth header</b>
<pre><code>Authorization: Bearer MzUxNzI0OjQ3ZGQxZGU2NmU2ZGQ4ZmIwMDM4ZWJhODNlYWZiZDM5ZDE5YWRhNjA1MTEwYzdiNWEzOWJhNGM1ODE3M2E5M2Q1NDA0Njg4NThlMDk3MmU4</code></pre>
<br>

## Get Pending Transaction
 <p><b>url : </b>http://dev2.apps-foundry.com/scoopcor/api/v1/payments?limit=20&offset=0&user_id=132543&payment_gateway_id=5,6</p>

<p><b>response:</b></p>
    
    {
    "pending_transaction_list": [
    {
      "order_id": 1362,
      "status": 20001,
      "payment_code": "0195750008844"
    },
    {
      "order_id": 1361,
      "status": 20001,
      "payment_code": "0195750008841"
    },
    {
      "order_id": 1360,
      "status": 20001,
      "payment_code": "0195750008836"
    },
    {
      "order_id": 1359,
      "status": 50000,
      "payment_code": "0195750008835"
    }
      ],
      "metadata": {
    "resultset": {
      "count": 11,
      "limit": "20",
      "offset": "0"
        }
     }
     }

   



## VERITRANS


1. User insert the required data such as cc number etc2 in our web page.
2. Front-end calls payment capture api 
    <p><b>url : </b>http://dev2.apps-foundry.com/scoopcor/api/v1/payments/capture</p>
    <p><b>request:</b></p>
    <pre><code>{
        "order_id": 1, 
        "payment_code": "ba6b2352-1c55-404d-acf3-fc16a2e51350-411111-1111", 
        "payer_id": "vt-testing@veritrans.co.id", 
        "signature": "loremipsum", 
        "user_id": 112,
        "payment_gateway_id": 12
    }</code></pre>

    <p><b>response:</b></p>
    <pre><code>{
        "user_id": 112,
        "payment_gateway_id": 12,
        "order_id": 1,
        "orderlines": [
          {
            "name": "OFFER FREE",
            "items": [
              {
                "is_featured": true,
                "name": "Magazine_3",
                "countries": [
                  {
                    "id": 1,
                    "name": "Indonesia"
                  }
                ],
                "release_date": "2014-06-24T16:02:07.725602",
                "item_status": 9,
                "languages": [
                  {
                    "id": 1,
                    "name": "Bahasa Indonesia"
                  }
                ],
                "vendors": {
                  "id": 1,
                  "name": "Apps-foundry"
                },
                "authors": [
                  {
                    "id": 1,
                    "name": "Kiratakada"
                  }
                ],
                "edition_code": "sku.magazine.3"
              },
              {
                "is_featured": true,
                "name": "Magazine_4",
                "countries": [
                  {
                    "id": 1,
                    "name": "Indonesia"
                  }
                ],
                "release_date": "2014-06-24T16:02:07.727257",
                "item_status": 9,
                "languages": [
                  {
                    "id": 1,
                    "name": "Bahasa Indonesia"
                  }
                ],
                "vendors": {
                  "id": 1,
                  "name": "Apps-foundry"
                },
                "authors": [
                  {
                    "id": 1,
                    "name": "Kiratakada"
                  }
                ],
                "edition_code": "sku.magazine.4"
              }
            ],
            "offer_id": 1,
            "is_free": true,
            "currency": "IDR",
            "final_price": "0.00",
            "raw_price": "0.00"
          },
          {
            "name": "OFFER SUBSCRIPTIONS",
            "items": [
              {
                "is_featured": true,
                "name": "Magazine_4",
                "countries": [
                  {
                    "id": 1,
                    "name": "Indonesia"
                  }
                ],
                "release_date": "2014-06-24T16:02:07.727257",
                "item_status": 9,
                "languages": [
                  {
                    "id": 1,
                    "name": "Bahasa Indonesia"
                  }
                ],
                "vendors": {
                  "id": 1,
                  "name": "Apps-foundry"
                },
                "authors": [
                  {
                    "id": 1,
                    "name": "Kiratakada"
                  }
                ],
                "edition_code": "sku.magazine.4"
              }
            ],
            "offer_id": 2,
            "is_free": false,
            "currency": "IDR",
            "final_price": "39000.00",
            "raw_price": "39000.00"
          },
          {
            "name": "OFFER SINGLE",
            "items": [
              {
                "is_featured": true,
                "name": "Magazine_1",
                "countries": [
                  {
                    "id": 1,
                    "name": "Indonesia"
                  }
                ],
                "release_date": "2014-06-24T16:02:07.721412",
                "item_status": 9,
                "languages": [
                  {
                    "id": 1,
                    "name": "Bahasa Indonesia"
                  }
                ],
                "vendors": {
                  "id": 1,
                  "name": "Apps-foundry"
                },
                "authors": [
                  {
                    "id": 1,
                    "name": "Kiratakada"
                  }
                ],
                "edition_code": "sku.magazine.1"
              },
              {
                "is_featured": true,
                "name": "Magazine_2",
                "countries": [
                  {
                    "id": 1,
                    "name": "Indonesia"
                  }
                ],
                "release_date": "2014-06-24T16:02:07.723955",
                "item_status": 9,
                "languages": [
                  {
                    "id": 1,
                    "name": "Bahasa Indonesia"
                  }
                ],
                "vendors": {
                  "id": 1,
                  "name": "Apps-foundry"
                },
                "authors": [
                  {
                    "id": 1,
                    "name": "Kiratakada"
                  }
                ],
                "edition_code": "sku.magazine.2"
              }
            ],
            "offer_id": 3,
            "is_free": false,
            "currency": "IDR",
            "final_price": "30000.00",
            "raw_price": "30000.00"
          }
        ],
        "currency": "IDR",
        "tier_code": "com-apps.IOS_MRA05",
        "final_price": "69000.00",
        "order_number": 8106210365195379,
        "platform": 1,
        "payment_gateway": "Veritrans",
        "raw_price": "69000.00"
    }</code></pre>

<br>

##PAYPAL
1. Front-end will request paypal webpage to do transaction authorization using
this api 
    <p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/payments/authorize.<p>
    <p><b>request</b>:</p>
    <pre><code>{
        "order_id": 2,
        "return_url": "http://127.0.0.1",
        "cancel_url": "http://127.0.0.1",
        "user_id":112,
        "signature": "loremipsum",
        "payment_gateway_id": 2
    }</code></pre>

    <p><b>response:</b></p>
    <pre><code>{
        "approval_url": "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=EC-3HD93399YM912952N"
        "payment_id": 18
    }
    </code></pre>
    front end will redirect user to approval url.


2. Front-end will capture payment_id from response and pass it to request to
this api 
    <p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/payments/capture</p>

    <p><b>request:</b></p>
    <pre><code>{
        "order_id": 2,
        "payer_id": "XXXLHJQZNY6PL",
        "payment_id": "18",
        "signature": "loremipsum",
        "user_id": 112,
        "payment_gateway_id": 2
    }</code></pre>

    <p><b>response:</b></p>
    <pre><code>{
      "user_id": 112,
      "payment_gateway_id": 2,
      "order_id": 2,
      "orderlines": [
        {
          "name": "OFFER FREE",
          "items": [
            {
              "is_featured": true,
              "name": "Magazine_3",
              "countries": [
                {
                  "id": 1,
                  "name": "Indonesia"
                }
              ],
              "release_date": "2014-06-24T16:02:07.725602",
              "item_status": 9,
              "languages": [
                {
                  "id": 1,
                  "name": "Bahasa Indonesia"
                }
              ],
              "vendors": {
                "id": 1,
                "name": "Apps-foundry"
              },
              "authors": [
                {
                  "id": 1,
                  "name": "Kiratakada"
                }
              ],
              "edition_code": "sku.magazine.3"
            },
            {
              "is_featured": true,
              "name": "Magazine_4",
              "countries": [
                {
                  "id": 1,
                  "name": "Indonesia"
                }
              ],
              "release_date": "2014-06-24T16:02:07.727257",
              "item_status": 9,
              "languages": [
                {
                  "id": 1,
                  "name": "Bahasa Indonesia"
                }
              ],
              "vendors": {
                "id": 1,
                "name": "Apps-foundry"
              },
              "authors": [
                {
                  "id": 1,
                  "name": "Kiratakada"
                }
              ],
              "edition_code": "sku.magazine.4"
            }
          ],
          "offer_id": 1,
          "is_free": true,
          "currency": "USD",
          "final_price": "0.00",
          "raw_price": "0.00"
        },
        {
          "name": "OFFER SUBSCRIPTIONS",
          "items": [
            {
              "is_featured": true,
              "name": "Magazine_4",
              "countries": [
                {
                  "id": 1,
                  "name": "Indonesia"
                }
              ],
              "release_date": "2014-06-24T16:02:07.727257",
              "item_status": 9,
              "languages": [
                {
                  "id": 1,
                  "name": "Bahasa Indonesia"
                }
              ],
              "vendors": {
                "id": 1,
                "name": "Apps-foundry"
              },
              "authors": [
                {
                  "id": 1,
                  "name": "Kiratakada"
                }
              ],
              "edition_code": "sku.magazine.4"
            }
          ],
          "offer_id": 2,
          "is_free": false,
          "currency": "USD",
          "final_price": "3.99",
          "raw_price": "3.99"
        },
        {
          "name": "OFFER SINGLE",
          "items": [
            {
              "is_featured": true,
              "name": "Magazine_1",
              "countries": [
                {
                  "id": 1,
                  "name": "Indonesia"
                }
              ],
              "release_date": "2014-06-24T16:02:07.721412",
              "item_status": 9,
              "languages": [
                {
                  "id": 1,
                  "name": "Bahasa Indonesia"
                }
              ],
              "vendors": {
                "id": 1,
                "name": "Apps-foundry"
              },
              "authors": [
                {
                  "id": 1,
                  "name": "Kiratakada"
                }
              ],
              "edition_code": "sku.magazine.1"
            },
            {
              "is_featured": true,
              "name": "Magazine_2",
              "countries": [
                {
                  "id": 1,
                  "name": "Indonesia"
                }
              ],
              "release_date": "2014-06-24T16:02:07.723955",
              "item_status": 9,
              "languages": [
                {
                  "id": 1,
                  "name": "Bahasa Indonesia"
                }
              ],
              "vendors": {
                "id": 1,
                "name": "Apps-foundry"
              },
              "authors": [
                {
                  "id": 1,
                  "name": "Kiratakada"
                }
              ],
              "edition_code": "sku.magazine.2"
            }
          ],
          "offer_id": 3,
          "is_free": false,
          "currency": "USD",
          "final_price": "1.99",
          "raw_price": "1.99"
        }
      ],
      "currency": "USD",
      "tier_code": "com-apps.IOS_MRA05",
      "final_price": "5.98",
      "order_number": 645594710853262,
      "platform": 1,
      "payment_gateway": "Paypal",
      "raw_price": "5.98"
    }</code></pre>

<br>

##ECASH
1. Get authorization code for ecash 
<p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/payments/authorize</p>
<p><b>Authorization:</b> Bearer MzUxNzI0OjQ3ZGQxZGU2NmU2ZGQ4ZmIwMDM4ZWJhODNlYWZiZDM5ZDE5YWRhNjA1MTEwYzdiNWEzOWJhNGM1ODE3M2E5M2Q1NDA0Njg4NThlMDk3MmU4</p>

    <p><b>request:</b></p>
    <pre><code>{
        "order_id": 902,
        "client_ip": "182.253.203.91", "signature":"4a53dc3a1e694e4f8a0bc659986a996a660aeea4840deeb25b5b527ac968b8eb",
        "user_id": 112,
        "payment_gateway_id": 14,
        "return_url": "http://127.0.0.1/getscoop/confirm/ecash"
}</code></pre>

    <p><b>response:</b></p>
    <pre><code>{
        "payment_id": 76
        "ecash_trx_id": "HQYKEGHJ45GET2K9UDIQ5LAFJI0STO6W"
    }</code></pre>

2. Redirect user to ecash payment page
    <p><b>url:</b> http://182.253.203.90:7070/ecommgateway/payment.html?id=HQYKEGHJ45GET2K9UDIQ5LAFJI0STO6W</p>
    
3. Validating ecash transaction
<p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/payments/capture</p>
<p><b>request:</b></p>
<pre><code>{
  "ecash_trx_id": "HQYKEGHJ45GET2K9UDIQ5LAFJI0STO6W",
  "order_id": 902,
  "signature": "4a53dc3a1e694e4f8a0bc659986a996a660aeea4840deeb25b5b527ac968b8eb",
  "user_id": 112,
  "payment_gateway_id": 14,
  "payment_id": 76
}</code></pre>

     <p><b>response:</b></p>
<pre><code>{
  "user_id": 220,
  "payment_gateway_id": 14,
  "order_id": 900,
  "orderlines": [
    {
      "name": "Single Edition",
      "items": [
        {
          "is_featured": false,
          "name": "Banjarmasin Post / 13 MAY 2014",
          "countries": [
            {
              "id": 1,
              "name": "Indonesia"
            }
          ],
          "release_date": "2014-05-13T00:00:00",
          "item_status": 9,
          "languages": [
            {
              "id": 2,
              "name": "Indonesian"
            }
          ],
          "vendors": {
            "id": 181,
            "name": "Banjarmasin Post"
          },
          "authors": [],
          "edition_code": "ID_BJMP2014MTH05DT13"
        }
      ],
      "offer_id": 41021,
      "is_free": false,
      "currency": "IDR",
      "final_price": "9900.00",
      "raw_price": "9900.00"
    }
  ],
  "currency": "IDR",
  "raw_price": "9900.00",
  "order_number": 91229859299188,
  "payment_gateway": "mandiri e-cash",
  "final_price": "9900.00"
}</code></pre>

<br>

##Clickpay
<p><b>Note:</b> API 3rd party harus dijalankan dahulu di server side. untuk melakukan koneksi ken velispayment.
commandnya adalah: sh /scoopcor/app/paymentgateway_connectors/API/runAPI.sh</p>

1. User redirect ke page front end, lalu meng-input data yang diperlukan seperti card number dan response token.
<p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/payments/charge</p>
<p><b>request:</b></p>
<pre><code>{
  "order_id": 18,
  "signature": "62daf6b715ab01fd9e3518864683e37c8cf9bb2a5ab78a599584e14b5bee5bef",
  "user_id": 112,
  "payment_gateway_id": 11,
  "response_token": "000000",
  "card_no": "4617007700000039"
}</pre></code>

    <p><b>response:</b></p>
    <pre><code>{
  "user_id": 112,
  "payment_gateway_id": 11,
  "order_id": 18,
  "orderlines": [
    {
      "name": "6 Editions / 6 Months",
      "items": [
        {
          "is_featured": false,
          "name": "GOLFMAGZ / NOV 2012",
          "countries": [
            {
              "id": 1,
              "name": "Indonesia"
            }
          ],
          "release_date": "2012-11-01T00:00:00",
          "item_status": 9,
          "languages": [
            {
              "id": 2,
              "name": "Indonesian"
            }
          ],
          "vendors": {
            "id": 13,
            "name": "IMP"
          },
          "authors": [],
          "edition_code": "ID_GLM2012MTH11"
        }
      ],
      "offer_id": 1,
      "is_free": false,
      "currency": "IDR",
      "final_price": "45000.00",
      "raw_price": "45000.00"
    }
  ],
  "currency": "IDR",
  "raw_price": "45000.00",
  "order_number": 5191094354761022,
  "payment_gateway": "mandiri clickpay",
  "final_price": "45000.00"
}</code></pre>

<br>
##FREE
1. 
<p><b>Request:</b></p>

    {
         "order_id": 11,
         "signature": "37c1f5399b0d6461b00f15b61adac9480f6eea7c8f272c6e17be07c7b8fe22d1",
         "user_id": 112,
         "payment_gateway_id": 3
    } 

<p><b>Response:</b></p>
<pre><code>
{
  "payment_gateway_name": "Free Purchase",
  "user_id": 112,
  "total_amount": "0.00",
  "order_id": 11,
  "partner_id": null,
  "is_active": true,
  "final_amount": "0.00",
  "platform_id": 1,
  "payment_gateway_id": 3,
  "temp_order_id": 13,
  "client_id": 1,
  "point_reward": 0,
  "order_status": 90000,
  "order_number": 21717867218328,
  "currency_code": "USD",
  "order_lines": [
    {
      "name": "Single Edition",
      "items": [
        {
          "is_featured": false,
          "vendor": {
            "id": 42,
            "name": "Simple Media"
          },
          "name": "PROVOKE! / AUG 2014",
          "countries": [
            {
              "id": 1,
              "name": "Indonesia"
            }
          ],
          "release_date": "2014-08-01T00:00:00",
          "item_status": 9,
          "languages": [
            {
              "id": 2,
              "name": "Indonesian"
            }],
          "authors": [],
          "id": 51499,
          "edition_code": "ID_PROV2014MTH08"
        }
      ],
      "offer_id": 47341,
      "is_free": true,
      "final_price": "0.00",
      "currency_code": "USD",
      "raw_price": "0.00"
    }
  ]
}
</code></pre>

<br>
##POINT
1.

<br>
##FINNET
1. Authorization untuk mendapatkan payment code yang akan diberikan kepada user untuk melakukan transaksi.
<p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/payments/authorize</p>
<p><b>request:</b></p>
<pre><code>{
  "order_id": 1057,
  "signature": "32bc45350514bef29e854d26f31d624346e26b697b85eb01fdee5e1432209e19",
  "user_id": 39657,
  "payment_gateway_id": 5
}</code></pre>

       <p><b>response:</b></p>
       <pre><code>{
       "finnet_payment_code": "0195750008745"
       }</code></pre>

<br>
##IAB
1.
<p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/payments/authorize</p>
<p><b>request:</b></p>
<pre><code>{
  "order_id": 4340,
  "signature": "805f579d7ef462877a5d837e997fcb40c5c0eb7e2a2de0209ff829d87544435b",
  "user_id": 355555,
  "payment_gateway_id": 15
}</code></pre>

<p><b>response:</b></p>
<pre><code>{
"product_code": ".c.usd.1.99"
"developer_payload": "9f9b2409b94acefdaa12cbe652abb432eef2f8baea98ae1e8619a30308cc038c"
}</code></pre>

2.
<p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/payments/validate</p>
<p><b>request:</b></p>
<pre><code>{
  "user_id": 355555,
  "payment_id": 4314,
  "order_id": 4309,
  "signature": "53a94670ca7d5a21561af0140b563fde5249166f6960ec2848b4cc9d3015c98a",
  "payment_gateway_id": 15,
  "purchase_token": "eanhbkjmobckdkbdakcbneld.AO-J1Owsr0U01bUIKuou3xWx5-qlLkelEDob5f9KdQfpKJqWl6pcrG7ZZ6UV_Bf60nnxlhL5UG6zsAuF088qbBiQy8zTJxcIs_yXCEgv9f9ZNt4asWFRcAJRLUq7eSQBgKhD0CZcaH_pNqDyeXJ9qAg6slvObWkkjg",
  "developer_payload": "c448cddcc540793542848fc178a2ef96e303c51a66d06313ae67c1489f31fab7",
  "product_sku": "com.appsfoundry.scoop.c.usd.89.99"
}</code></pre>

<p><b>response:</b></p>
<pre><code>{

}</code></pre>
<br>

##TCASH
1. Authorize
<p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/payments/authorize</p>
<p><b>request:</b></p>
<pre><code>{
  "order_id": 4354,
  "signature": "5acef9a42be93c7aa6853d88b235b24f9fe2b146743c6bae45cf1746dddcfc52",
  "user_id": 132543,
  "payment_gateway_id": 6
}</code></pre>

<p><b>response:</b></p>
<pre><code>{
  "payment_id": 4850,
  "order_id": 4354,
  "message": "Payment tcash authorized"
}</code></pre>

2. Front end akan mengirimkan sms ke tcash, dengan format:
	PAY SCOOP AMOUNT PIN PAYMENT_ID#ORDER_ID
<br><br>

##APPLE IN APP PURCHASE

1. Validate. Front end will initiate purchase and pass the order id, signature, user_id, payment_gateway_id and receipt to backend. Backend will validate it and complete the purchase.

<p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/payments/validate</p>
<p><b>request:</b></p>
<pre><code>{
"order_id": 6780,
"signature": "aa30af19e19c6205e7a6b134d34a1aafd9a4349c2a0478ce78d8503ac5d5b00e",
"user_id": 290411,
"payment_gateway_id": 1,
"receipt_data": "ewoJInNpZ25hdHVyZSIgPSAiQW1YMmxYdjVTTkoxVG55ZDRNZkdZMnpENG5XdHdxVzBuakFuZitaUHdxdGk2cTZwKzNDcXJVdXBZYWxNbXZWQkZCdlhDS3ZkKzN5YjJ1Sm1udUNsUTA2VGJOMnlZM1NLOGcwN1A3TEJ3bHdRRjZHWWViZExyUUdHWHZ2RDJkSWZDRmJjLy90THBlbmpxakZ4b2pwWGtuMlRPWUJ6Qy9Lc1lXRHRsU21aNFRMMUFBQURWekNDQTFNd2dnSTdvQU1DQVFJQ0NCdXA0K1BBaG0vTE1BMEdDU3FHU0liM0RRRUJCUVVBTUg4eEN6QUpCZ05WQkFZVEFsVlRNUk13RVFZRFZRUUtEQXBCY0hCc1pTQkpibU11TVNZd0pBWURWUVFMREIxQmNIQnNaU0JEWlhKMGFXWnBZMkYwYVc5dUlFRjFkR2h2Y21sMGVURXpNREVHQTFVRUF3d3FRWEJ3YkdVZ2FWUjFibVZ6SUZOMGIzSmxJRU5sY25ScFptbGpZWFJwYjI0Z1FYVjBhRzl5YVhSNU1CNFhEVEUwTURZd056QXdNREl5TVZvWERURTJNRFV4T0RFNE16RXpNRm93WkRFak1DRUdBMVVFQXd3YVVIVnlZMmhoYzJWU1pXTmxhWEIwUTJWeWRHbG1hV05oZEdVeEd6QVpCZ05WQkFzTUVrRndjR3hsSUdsVWRXNWxjeUJUZEc5eVpURVRNQkVHQTFVRUNnd0tRWEJ3YkdVZ1NXNWpMakVMTUFrR0ExVUVCaE1DVlZNd2daOHdEUVlKS29aSWh2Y05BUUVCQlFBRGdZMEFNSUdKQW9HQkFNbVRFdUxnamltTHdSSnh5MW9FZjBlc1VORFZFSWU2d0Rzbm5hbDE0aE5CdDF2MTk1WDZuOTNZTzdnaTNvclBTdXg5RDU1NFNrTXArU2F5Zzg0bFRjMzYyVXRtWUxwV25iMzRucXlHeDlLQlZUeTVPR1Y0bGpFMU93QytvVG5STStRTFJDbWVOeE1iUFpoUzQ3VCtlWnRERWhWQjl1c2szK0pNMkNvZ2Z3bzdBZ01CQUFHamNqQndNQjBHQTFVZERnUVdCQlNKYUVlTnVxOURmNlpmTjY4RmUrSTJ1MjJzc0RBTUJnTlZIUk1CQWY4RUFqQUFNQjhHQTFVZEl3UVlNQmFBRkRZZDZPS2RndElCR0xVeWF3N1hRd3VSV0VNNk1BNEdBMVVkRHdFQi93UUVBd0lIZ0RBUUJnb3Foa2lHOTJOa0JnVUJCQUlGQURBTkJna3Foa2lHOXcwQkFRVUZBQU9DQVFFQWVhSlYyVTUxcnhmY3FBQWU1QzIvZkVXOEtVbDRpTzRsTXV0YTdONlh6UDFwWkl6MU5ra0N0SUl3ZXlOajVVUllISytIalJLU1U5UkxndU5sMG5rZnhxT2JpTWNrd1J1ZEtTcTY5Tkluclp5Q0Q2NlI0Szc3bmI5bE1UQUJTU1lsc0t0OG9OdGxoZ1IvMWtqU1NSUWNIa3RzRGNTaVFHS01ka1NscDRBeVhmN3ZuSFBCZTR5Q3dZVjJQcFNOMDRrYm9pSjNwQmx4c0d3Vi9abEwyNk0ydWVZSEtZQ3VYaGRxRnd4VmdtNTJoM29lSk9PdC92WTRFY1FxN2VxSG02bTAzWjliN1BSellNMktHWEhEbU9Nazd2RHBlTVZsTERQU0dZejErVTNzRHhKemViU3BiYUptVDdpbXpVS2ZnZ0VZN3h4ZjRjemZIMHlqNXdOelNHVE92UT09IjsKCSJwdXJjaGFzZS1pbmZvIiA9ICJld29KSW05eWFXZHBibUZzTFhCMWNtTm9ZWE5sTFdSaGRHVXRjSE4wSWlBOUlDSXlNREUwTFRFeExUSTJJREl3T2pVeE9qTXhJRUZ0WlhKcFkyRXZURzl6WDBGdVoyVnNaWE1pT3dvSkluVnVhWEYxWlMxcFpHVnVkR2xtYVdWeUlpQTlJQ0ptTWpVd1lUTTRZamt5TjJFNE5XWXpaamsxTkRZNVpEWmtZVEV4TVdFek1HSXhOMkkzT0RnMElqc0tDU0p2Y21sbmFXNWhiQzEwY21GdWMyRmpkR2x2YmkxcFpDSWdQU0FpTVRBd01EQXdNREV6TXpJNE1UYzVNeUk3Q2draVluWnljeUlnUFNBaU5DNHhMak1pT3dvSkluUnlZVzV6WVdOMGFXOXVMV2xrSWlBOUlDSXhNREF3TURBd01UTXpNamd4Tnpreklqc0tDU0p4ZFdGdWRHbDBlU0lnUFNBaU1TSTdDZ2tpYjNKcFoybHVZV3d0Y0hWeVkyaGhjMlV0WkdGMFpTMXRjeUlnUFNBaU1UUXhOekEyTXpnNU1URTBPU0k3Q2draWRXNXBjWFZsTFhabGJt\nUnZjaTFwWkdWdWRHbG1hV1Z5SWlBOUlDSTFSRUl5Tmprek9DMDVRVFl3TFRSR09FRXRPVEpHTkMweVFqVXhNMEV3TnprME1ETWlPd29KSW5CeWIyUjFZM1F0YVdRaUlEMGdJbU52YlM1aGNIQnpabTkxYm1SeWVTNXpZMjl2Y0M1SlJGOVVSVTFKUkRJd01UUkZSRFF5T1RFaU93b0pJbWwwWlcwdGFXUWlJRDBnSWpreU56ZzFPVEUyTnlJN0Nna2lZbWxrSWlBOUlDSmpiMjB1WVhCd2N5MW1iM1Z1WkhKNUxsTkRUMDlRSWpzS0NTSndkWEpqYUdGelpTMWtZWFJsTFcxeklpQTlJQ0l4TkRFM01EWXpPRGt4TVRRNUlqc0tDU0p3ZFhKamFHRnpaUzFrWVhSbElpQTlJQ0l5TURFMExURXhMVEkzSURBME9qVXhPak14SUVWMFl5OUhUVlFpT3dvSkluQjFjbU5vWVhObExXUmhkR1V0Y0hOMElpQTlJQ0l5TURFMExURXhMVEkySURJd09qVXhPak14SUVGdFpYSnBZMkV2VEc5elgwRnVaMlZzWlhNaU93b0pJbTl5YVdkcGJtRnNMWEIxY21Ob1lYTmxMV1JoZEdVaUlEMGdJakl3TVRRdE1URXRNamNnTURRNk5URTZNekVnUlhSakwwZE5WQ0k3Q24wPSI7CgkiZW52aXJvbm1lbnQiID0gIlNhbmRib3giOwoJInBvZCIgPSAiMTAwIjsKCSJzaWduaW5nLXN0YXR1cyIgPSAiMCI7Cn0="
}</code></pre>

<br>
<b>Cron IAP</b>

<p>This cron jobs is to do revalidation for every subscription using IAP in our database that will expired in 1 day.</p>

<p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/appleiaps/renewal</p>

<br>
<b>Restore IAP</b>

<p>This API will be used to do restore for each receipts that user has bought. Front-end will call this API and backend will deliver the items</p>

<p><b>url:</b> http://dev2.apps-foundry.com/scoopcor/api/v1/appleiaps/restore</p>
<p><b>request:</b></p>
<pre><code>{
	"user_id":,
	"platform_id":,
	"receipt_data":
}
</code></pre>