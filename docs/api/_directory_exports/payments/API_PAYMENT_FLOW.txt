
======================================
    IN-APP BILLING PAYMENT FLOW
======================================

ORDER:
    --> /v1/checkout
    --> /v1/confirm

PAYMENT:
    --> /v1/payment/authorize
    
        REQUEST :
            {
                "order_id": 1,
                "user_id": 1,
                "payment_gateway_id": 1,

                "payment_code": "loremipsum",
                "return_url": "loremipsum",
                "cancel_url": "loremipsum",
            }
        
        RESPONSE :
            {
                "order_id": 12,
                "developer_payload": "124jhjkasf",
                "product_id": "com.sku.co"
            }
    
    --> /v1/payment/validate
        REQUEST:
            {
                "order_id": 12,
                "developer_payload": "124jhjkasf",
                "purchase_token": "hohohoho" #purchase_token from google
            }