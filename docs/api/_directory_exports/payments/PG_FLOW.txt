all request to payment need auth header
Authorization: Bearer MzUxNzI0OjQ3ZGQxZGU2NmU2ZGQ4ZmIwMDM4ZWJhODNlYWZiZDM5ZDE5YWRhNjA1MTEwYzdiNWEzOWJhNGM1ODE3M2E5M2Q1NDA0Njg4NThlMDk3MmU4

=========
VERITRANS
=========
1. User insert the required data such as cc number etc2 in our web page.
2. front-end calls payment capture api --> http://127.0.0.1:5000/v1/payments/capture
    request:
    {
        "order_id": 1, 
        "payment_code": "ba6b2352-1c55-404d-acf3-fc16a2e51350-411111-1111", 
        "payer_id": "vt-testing@veritrans.co.id", 
        "signature": "loremipsum", 
        "user_id": 112,
        "payment_gateway_id": 12
    }

    response:
    {
        "user_id": 112,
        "payment_gateway_id": 12,
        "order_id": 1,
        "orderlines": [
          {
            "name": "OFFER FREE",
            "items": [
              {
                "is_featured": true,
                "name": "Magazine_3",
                "countries": [
                  {
                    "id": 1,
                    "name": "Indonesia"
                  }
                ],
                "release_date": "2014-06-24T16:02:07.725602",
                "item_status": 9,
                "languages": [
                  {
                    "id": 1,
                    "name": "Bahasa Indonesia"
                  }
                ],
                "vendors": {
                  "id": 1,
                  "name": "Apps-foundry"
                },
                "authors": [
                  {
                    "id": 1,
                    "name": "Kiratakada"
                  }
                ],
                "edition_code": "sku.magazine.3"
              },
              {
                "is_featured": true,
                "name": "Magazine_4",
                "countries": [
                  {
                    "id": 1,
                    "name": "Indonesia"
                  }
                ],
                "release_date": "2014-06-24T16:02:07.727257",
                "item_status": 9,
                "languages": [
                  {
                    "id": 1,
                    "name": "Bahasa Indonesia"
                  }
                ],
                "vendors": {
                  "id": 1,
                  "name": "Apps-foundry"
                },
                "authors": [
                  {
                    "id": 1,
                    "name": "Kiratakada"
                  }
                ],
                "edition_code": "sku.magazine.4"
              }
            ],
            "offer_id": 1,
            "is_free": true,
            "currency": "IDR",
            "final_price": "0.00",
            "raw_price": "0.00"
          },
          {
            "name": "OFFER SUBSCRIPTIONS",
            "items": [
              {
                "is_featured": true,
                "name": "Magazine_4",
                "countries": [
                  {
                    "id": 1,
                    "name": "Indonesia"
                  }
                ],
                "release_date": "2014-06-24T16:02:07.727257",
                "item_status": 9,
                "languages": [
                  {
                    "id": 1,
                    "name": "Bahasa Indonesia"
                  }
                ],
                "vendors": {
                  "id": 1,
                  "name": "Apps-foundry"
                },
                "authors": [
                  {
                    "id": 1,
                    "name": "Kiratakada"
                  }
                ],
                "edition_code": "sku.magazine.4"
              }
            ],
            "offer_id": 2,
            "is_free": false,
            "currency": "IDR",
            "final_price": "39000.00",
            "raw_price": "39000.00"
          },
          {
            "name": "OFFER SINGLE",
            "items": [
              {
                "is_featured": true,
                "name": "Magazine_1",
                "countries": [
                  {
                    "id": 1,
                    "name": "Indonesia"
                  }
                ],
                "release_date": "2014-06-24T16:02:07.721412",
                "item_status": 9,
                "languages": [
                  {
                    "id": 1,
                    "name": "Bahasa Indonesia"
                  }
                ],
                "vendors": {
                  "id": 1,
                  "name": "Apps-foundry"
                },
                "authors": [
                  {
                    "id": 1,
                    "name": "Kiratakada"
                  }
                ],
                "edition_code": "sku.magazine.1"
              },
              {
                "is_featured": true,
                "name": "Magazine_2",
                "countries": [
                  {
                    "id": 1,
                    "name": "Indonesia"
                  }
                ],
                "release_date": "2014-06-24T16:02:07.723955",
                "item_status": 9,
                "languages": [
                  {
                    "id": 1,
                    "name": "Bahasa Indonesia"
                  }
                ],
                "vendors": {
                  "id": 1,
                  "name": "Apps-foundry"
                },
                "authors": [
                  {
                    "id": 1,
                    "name": "Kiratakada"
                  }
                ],
                "edition_code": "sku.magazine.2"
              }
            ],
            "offer_id": 3,
            "is_free": false,
            "currency": "IDR",
            "final_price": "30000.00",
            "raw_price": "30000.00"
          }
        ],
        "currency": "IDR",
        "tier_code": "com-apps.IOS_MRA05",
        "final_price": "69000.00",
        "order_number": 8106210365195379,
        "platform": 1,
        "payment_gateway": "Veritrans",
        "raw_price": "69000.00"
    }


======
PAYPAL
======
1. Front-end will request paypal webpage to do transaction authorization using
this api --> http://127.0.0.1:5000/v1/payments/authorize.
    request:
    {
        "order_id": 2,
        "return_url": "http://127.0.0.1",
        "cancel_url": "http://127.0.0.1",
        "user_id":112,
        "signature": "loremipsum",
        "payment_gateway_id": 2
    }

    response:
    {
        "approval_url": "https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=EC-3HD93399YM912952N"
        "payment_id": 18
    }
    front end will redirect user to approval url.


2. Front-end will capture payment_id from response and pass it to request to
this api --> http://127.0.0.1:5000/v1/payments/capture

    request:
    {
        "order_id": 2,
        "payer_id": "XXXLHJQZNY6PL",
        "payment_id": "18",
        "signature": "loremipsum",
        "user_id": 112,
        "payment_gateway_id": 2
    }

    response:
    {
      "user_id": 112,
      "payment_gateway_id": 2,
      "order_id": 2,
      "orderlines": [
        {
          "name": "OFFER FREE",
          "items": [
            {
              "is_featured": true,
              "name": "Magazine_3",
              "countries": [
                {
                  "id": 1,
                  "name": "Indonesia"
                }
              ],
              "release_date": "2014-06-24T16:02:07.725602",
              "item_status": 9,
              "languages": [
                {
                  "id": 1,
                  "name": "Bahasa Indonesia"
                }
              ],
              "vendors": {
                "id": 1,
                "name": "Apps-foundry"
              },
              "authors": [
                {
                  "id": 1,
                  "name": "Kiratakada"
                }
              ],
              "edition_code": "sku.magazine.3"
            },
            {
              "is_featured": true,
              "name": "Magazine_4",
              "countries": [
                {
                  "id": 1,
                  "name": "Indonesia"
                }
              ],
              "release_date": "2014-06-24T16:02:07.727257",
              "item_status": 9,
              "languages": [
                {
                  "id": 1,
                  "name": "Bahasa Indonesia"
                }
              ],
              "vendors": {
                "id": 1,
                "name": "Apps-foundry"
              },
              "authors": [
                {
                  "id": 1,
                  "name": "Kiratakada"
                }
              ],
              "edition_code": "sku.magazine.4"
            }
          ],
          "offer_id": 1,
          "is_free": true,
          "currency": "USD",
          "final_price": "0.00",
          "raw_price": "0.00"
        },
        {
          "name": "OFFER SUBSCRIPTIONS",
          "items": [
            {
              "is_featured": true,
              "name": "Magazine_4",
              "countries": [
                {
                  "id": 1,
                  "name": "Indonesia"
                }
              ],
              "release_date": "2014-06-24T16:02:07.727257",
              "item_status": 9,
              "languages": [
                {
                  "id": 1,
                  "name": "Bahasa Indonesia"
                }
              ],
              "vendors": {
                "id": 1,
                "name": "Apps-foundry"
              },
              "authors": [
                {
                  "id": 1,
                  "name": "Kiratakada"
                }
              ],
              "edition_code": "sku.magazine.4"
            }
          ],
          "offer_id": 2,
          "is_free": false,
          "currency": "USD",
          "final_price": "3.99",
          "raw_price": "3.99"
        },
        {
          "name": "OFFER SINGLE",
          "items": [
            {
              "is_featured": true,
              "name": "Magazine_1",
              "countries": [
                {
                  "id": 1,
                  "name": "Indonesia"
                }
              ],
              "release_date": "2014-06-24T16:02:07.721412",
              "item_status": 9,
              "languages": [
                {
                  "id": 1,
                  "name": "Bahasa Indonesia"
                }
              ],
              "vendors": {
                "id": 1,
                "name": "Apps-foundry"
              },
              "authors": [
                {
                  "id": 1,
                  "name": "Kiratakada"
                }
              ],
              "edition_code": "sku.magazine.1"
            },
            {
              "is_featured": true,
              "name": "Magazine_2",
              "countries": [
                {
                  "id": 1,
                  "name": "Indonesia"
                }
              ],
              "release_date": "2014-06-24T16:02:07.723955",
              "item_status": 9,
              "languages": [
                {
                  "id": 1,
                  "name": "Bahasa Indonesia"
                }
              ],
              "vendors": {
                "id": 1,
                "name": "Apps-foundry"
              },
              "authors": [
                {
                  "id": 1,
                  "name": "Kiratakada"
                }
              ],
              "edition_code": "sku.magazine.2"
            }
          ],
          "offer_id": 3,
          "is_free": false,
          "currency": "USD",
          "final_price": "1.99",
          "raw_price": "1.99"
        }
      ],
      "currency": "USD",
      "tier_code": "com-apps.IOS_MRA05",
      "final_price": "5.98",
      "order_number": 645594710853262,
      "platform": 1,
      "payment_gateway": "Paypal",
      "raw_price": "5.98"
    }

======
ECASH
======
1. Get authorization code for ecash 
Authorization: Bearer MzUxNzI0OjQ3ZGQxZGU2NmU2ZGQ4ZmIwMDM4ZWJhODNlYWZiZDM5ZDE5YWRhNjA1MTEwYzdiNWEzOWJhNGM1ODE3M2E5M2Q1NDA0Njg4NThlMDk3MmU4

request:
{
  "order_id": 902,
  "client_ip": "182.253.203.91",
  "signature": "4a53dc3a1e694e4f8a0bc659986a996a660aeea4840deeb25b5b527ac968b8eb",
  "user_id": 112,
  "payment_gateway_id": 14,
  "return_url": "http://127.0.0.1/getscoop/confirm/ecash"
}

response:
{
    "payment_id": 76
    "ecash_trx_id": "HQYKEGHJ45GET2K9UDIQ5LAFJI0STO6W"
}

2. redirect user to ecash payment page
    http://182.253.203.90:7070/ecommgateway/payment.html?id=HQYKEGHJ45GET2K9UDIQ5LAFJI0STO6W
    
3. Validating ecash transaction
request:
{
  "ecash_trx_id": "HQYKEGHJ45GET2K9UDIQ5LAFJI0STO6W",
  "order_id": 902,
  "signature": "4a53dc3a1e694e4f8a0bc659986a996a660aeea4840deeb25b5b527ac968b8eb",
  "user_id": 112,
  "payment_gateway_id": 14,
  "payment_id": 76
}
response:
{
  "user_id": 220,
  "payment_gateway_id": 14,
  "order_id": 900,
  "orderlines": [
    {
      "name": "Single Edition",
      "items": [
        {
          "is_featured": false,
          "name": "Banjarmasin Post / 13 MAY 2014",
          "countries": [
            {
              "id": 1,
              "name": "Indonesia"
            }
          ],
          "release_date": "2014-05-13T00:00:00",
          "item_status": 9,
          "languages": [
            {
              "id": 2,
              "name": "Indonesian"
            }
          ],
          "vendors": {
            "id": 181,
            "name": "Banjarmasin Post"
          },
          "authors": [
            
          ],
          "edition_code": "ID_BJMP2014MTH05DT13"
        }
      ],
      "offer_id": 41021,
      "is_free": false,
      "currency": "IDR",
      "final_price": "9900.00",
      "raw_price": "9900.00"
    }
  ],
  "currency": "IDR",
  "raw_price": "9900.00",
  "order_number": 91229859299188,
  "payment_gateway": "mandiri e-cash",
  "final_price": "9900.00"
}


==========
CLickpay
==========
*Note: API 3rd party harus dijalankan dahulu di server side. untuk melakukan koneksi ke velispayment.
commandnya adalah: sh /scoopcor/app/paymentgateway_connectors/API/runAPI.sh

1. User redirect ke page front end, lalu meng-input data yang diperlukan seperti card number dan response token.
URL     : http://dev2.apps-foundry.com/scoopcor/api/v1/payments/charge
Request :
{
  "order_id": 18,
  "signature": "62daf6b715ab01fd9e3518864683e37c8cf9bb2a5ab78a599584e14b5bee5bef",
  "user_id": 112,
  "payment_gateway_id": 11,
  "response_token": "000000",
  "card_no": "4617007700000039"
}

Response:
{
  "user_id": 112,
  "payment_gateway_id": 11,
  "order_id": 18,
  "orderlines": [
    {
      "name": "6 Editions / 6 Months",
      "items": [
        {
          "is_featured": false,
          "name": "GOLFMAGZ / NOV 2012",
          "countries": [
            {
              "id": 1,
              "name": "Indonesia"
            }
          ],
          "release_date": "2012-11-01T00:00:00",
          "item_status": 9,
          "languages": [
            {
              "id": 2,
              "name": "Indonesian"
            }
          ],
          "vendors": {
            "id": 13,
            "name": "IMP"
          },
          "authors": [
            
          ],
          "edition_code": "ID_GLM2012MTH11"
        }
      ],
      "offer_id": 1,
      "is_free": false,
      "currency": "IDR",
      "final_price": "45000.00",
      "raw_price": "45000.00"
    }
  ],
  "currency": "IDR",
  "raw_price": "45000.00",
  "order_number": 5191094354761022,
  "payment_gateway": "mandiri clickpay",
  "final_price": "45000.00"
}


======
FREE
======
1. 

======
POINT
======
1.

======
FINNET
======
1.

======
IAB
======
1.

======
IAP
======
1.