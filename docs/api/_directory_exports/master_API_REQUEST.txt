
========================
    CURRENCIES
========================
    < ======================================================================= >
        NOTE : JSON CAN'T PARSE FLOAT VALUE, SOO "ONE_USD_EQUALS" BECOME STR.
    < ======================================================================= >

POST    http://127.0.0.1:5000/v1/currencies
PUT     http://127.0.0.1:5000/v1/currencies/1

REQUEST:
    {
        "iso4217_code": "USD",
        "right_symbol": "$",
        "left_symbol": "$",
        "group_separator": ".",
        "decimal_separator": ",",
        "one_usd_equals": "1.0",
        "is_active": True
    }


========================
    CAMPAIGN
========================

POST    http://127.0.0.1:5000/v1/campaigns
PUT     http://127.0.0.1:5000/v1/campaigns/1   

REQUEST :
    {
        "name": "Lorem ipsum",
        "descriptions": "Lorem ipsum",
        "is_active": True
    }


========================
    VENDORS
========================

POST    http://127.0.0.1:5000/v1/vendors
PUT     http://127.0.0.1:5000/v1/vendors/1   

REQUEST :
    {
        "name": "Lorem ipsum dolor",
        "description": "Lorem ipsum dolor siamet",
        "slug": "Lorem-Lorem",
        "vendor_status": 1,
        "meta": "Lorem-porem",
        "icon_image_normal": "Highimage",
        "icon_image_highres": "highrest",
        "cut_rule_id": 1,
        "sort_priority": 1,
        "is_active": True,
        "organization_id": 1,
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ]
    }


========================
    PARTNER
========================

POST    http://127.0.0.1:5000/v1/partners
PUT     http://127.0.0.1:5000/v1/partners/1   

REQUEST :
    {
        "name": "Lorem ipsum dolor",
        "description": "Lorem ipsum dolor siamet",
        "slug": "Lorem-Lorem",
        "meta": "Lorem-porem",
        "partner_status": 1,
        "icon_image_normal": "Highimage",
        "icon_image_highres": "highrest",
        "cut_rule_id": 1,
        "sort_priority": 1,
        "is_active": True,
        "organization_id": 1
    }


========================
    BRANDS
========================

POST    http://127.0.0.1:5000/v1/brands
PUT     http://127.0.0.1:5000/v1/brands/1

REQUEST :
    {
        "name" : "Lorem ipsum",
        "description" : "Lorem ipsum Description",
        "slug" : "Lorem-ipsum-dolor",
        "meta" : "Lorem lorem",
        "icon_image_normal" : "lorem go lorem",
        "icon_image_highres" : "lorem go lorem",
        "sort_priority" : 1,
        "is_active" : True,
        "vendor_id" : 2,
        "links": [
            {
                "link_type": 1,
                "url": "www.facebook.com/lorem"
            },
            {
                "link_type": 2,
                "url": "www.twitter.com/lorem"
            }
        ]
    }


========================
    COUNTRY
========================

POST    http://127.0.0.1:5000/v1/countries
PUT     http://127.0.0.1:5000/v1/countries/1

REQUEST :
    {
        "name" : "Indonesia",
        "description" : "Indonesia bersatu teguh",
        "code" : "INA",
        "slug" : "Indonesia-tanah-ku",
        "meta" : "Merdeka",
        "icon_image_normal" : "Indonesia.jpg",
        "icon_image_highres" : "Indonesia.jpg",
        "sort_priority" : 1,
        "is_active" : True
    }


========================
    LANGUAGES
========================

POST    http://127.0.0.1:5000/v1/languages
PUT     http://127.0.0.1:5000/v1/languages/1

REQUEST :
    {
        "name" : "Bahasa Indonesia",
        "description" : "Bahasa bahasa",
        "slug" : "Bahasa-indonesia",
        "meta" : "Bahasa ku indonesia",
        "icon_image_normal" : "Bahasa.jpg",
        "icon_image_highres" : "Bahasa.jpg",
        "sort_priority" : 1,
        "is_active" : True
    }
    
========================
    CATEGORY
========================

GET http://127.0.0.1:5000/v1/categories
RESPONSE:
    <display all value json category>


GET http://127.0.0.1:5000/v1/categories?country_id=1,2,3,4
RESPONSE:
    <display category value json with specific country>

RESPONSE FAILED:
    < No content > status_code : 204


POST    http://127.0.0.1:5000/v1/categories
PUT     http://127.0.0.1:5000/v1/categories/1

REQUEST :
    {
        "name" : "Majalah FHM",
        "description" : "Majalah keren hot mantap",
        "slug" : "majalah-fhm",
        "meta" : "Majalah keren lohh",
        "icon_image_normal" : "majalah.jpg",
        "icon_image_highres" : "majalah.jpg",
        "sort_priority" : 1,
        "is_active" : True,
        "parent_category_id" : 1,
        "item_type_id" : 1,
        "countries": [1,2]
    }
    

========================
    PLATFORM
========================

POST    http://127.0.0.1:5000/v1/platforms
PUT     http://127.0.0.1:5000/v1/platforms/1

REQUEST:
    {
        "name": "ANDROID",
        "is_active": true,
        "description": "Android go go"
    }
