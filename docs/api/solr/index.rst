Solr
====

Solr allows `Users` to temporarily access `Items` based on solr search.

.. toctree::

    item-search
