List items from solr search
===========================

.. http:get:: /items

    Searchable, Paginated List endpoint of Items from solr search results

    **Example request**

    .. sourcecode:: http

        GET /items HTTP/1.1
        Accept: application/json
        Accept-Language: id-ID
        Accept-Charset: utf-8
        Authorization: Bearer MTIzNDp3ZTRzY29vcA==
        User-Agent: Scoop IOS/4.7.0 (iOS 3.2.1; iPhone 6s)

    **Example response**

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Content-Type: application/json
        ETag: "737060cd8c284d8af7ad3082f209582d"

        {
            "items": [
                {
                      "item_name": "1000+ Kejayaan Sains Muslim",
                      "brand_name": "1000+ Kejayaan Sains Muslim",
                      "category_names": ["Politics & Current Affairs"],
                      "vendor_name": "Elex Media Komputindo",
                      "price_android_usd": 59000.0,
                      "price_android_idr": 3.99,
                      "price_ios_idr": 59000.0,
                      "price_ios_usd": 3.99,
                      "price_web_usd": 3.99,
                      "price_web_idr": 59000.0,
                      "countries": ["id"],
                      "languages": ["ind"],
                      "item_thumb_highres": "https://images.apps-foundry.com/magazine_static/images/1/32862/small_covers/ID_EMK2016MTH05KSM_S.jpg",
                      "is_age_restricted": true,
                      "highlighting": {
                            "content": "COLD WAR SHADOW United States Policy Toward <em>Indonesia</em>  1953-1963 Baskara T. Wardaya COLD"
                      }
                }
                ],
            "metadata": {
                "resultset": {
                    "count": 1,
                    "limit": 20,
                    "offset": 0
                },
                "facets": [
                    {
                        "field_name": "item_categories",
                        "values":[
                            {
                                "value": "Automotive",
                                "count": 2,
                            },
                            {
                                "value": "Crime & Thrillers",
                                "count": 1,
                            },
                        ]
                    },
                    {
                        "field_name": "item_authors",
                        "values": [
                            {
                                "value": "ramdani",
                                "count": 2,
                            }
                        ]
                    },
                    {
                        "field_name": "item_languages",
                        "values": [
                            {
                                "value": "id",
                                "count": 2,
                            },
                            {
                                "value": "en",
                                "count": 0,
                            }
                        ]
                    }
                ],
                "spelling_suggestions": [
                    {
                        "value": "sedekah",
                        "count": 8,
                    },
                    {
                        "value": "selekta",
                        "count": 1,
                    }
                ]
            }
        }


    :query q: Search term to be run against the API.
    :query offset: The number of beginning results to offset from the start.
    :query limit: The maximum number of results to return.
    :query with_content: This query parameter to allow search crawl into pdf content.
    :query is_age_restricted: Age restriction value for filter content related to age restriction.


    **Properties**

    **Metadata Properties**

    resultset
        count
            The total number of objects matching your query.
        limit
            The maximum number of objects displayed per page.
        offset
            The number of objects skipped at the beginning of the list.

    facets
        List of available facets from this endpoint.
        Inside the list will be  objects that match the faceting.

    spelling_suggestions
        Object containing alternate spelling suggestions for 'q', along with the
        the number of matches for the alternate spelling.
