
### Categories

Category resource contains item's category.

#### Category Properties

* `id` - Number; Instance identifier.

* `item_type_id` - Number; Item type identifier of this category.

* `parent_category_id` - Number; Parent category of this category, use for multi-leveled category.

* `name` - String; Category name.

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority.

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true.

* `slug` - String, Unique; Category slug for SEO.

* `meta` - String; Category meta for SEO.

* `description` - String; Category description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URI.

* `icon_image_normal` - String; Category icon image URI.

* `icon_image_highres` - String; Category icon image in highres URI.

* `countries` - Array; Category's countries. If null, the category is eligible for all countries.

#### Create Category

Create new category.

**URL:** /categories

**Method:** POST

**Request Data Params:**

Required: `item_type_id`, `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `parent_category_id`, `countries`

**Response HTTP Code:**

* `201` - New category created

**Response Params:** See [Category Properties](#category-properties)

**Examples:**

Request:

```
POST /categories HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "item_type_id": 1
    "name": "Men's",
    "slug": "mens",
    "sort_priority": 1,
    "is_active": true,
    "description": "Men's category",
    "meta": "men's category",
    "icon_image_normal": "categories/indonesia/icon.png",
    "icon_image_highres": "categories/indonesia/icon_highres.png",
    "parent_category_id": null,
    "countries": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/categories/23

{
    "id": 23
    "item_type_id": 1
    "name": "Men's",
    "slug": "mens",
    "sort_priority": 1,
    "is_active": true,
    "description": "Men's category",
    "meta": "men's category",
    "icon_image_normal": "categories/indonesia/icon.png",
    "icon_image_highres": "categories/indonesia/icon_highres.png",
    "parent_category_id": null,
    "countries": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Categories

Retrieve categories.

**URL:** /categories

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Category Properties](#category-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

* `item_type_id` - Item type identifier of categories to be retrieved

* `parent_category_id` - Parent category identifier of categories to be retrieved.

    * If not provided, the default results will be all categories
    
    * If provided with null value, the results will be root categories (the categories without parent)

* `countries` - List of country identifiers of categories to be retrieved. Any category having one of provided country will be returned

**Method:** GET

**Response HTTP Code:**

* `200` - Categories retrieved and returned in body content

* `204` - No matched brand

**Response Params:** See [Category Properties](#category-properties)

**Examples:**

Request:

```
GET /categories?fields=id,name,countries&countries_id=1,2&item_type_id=3 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "categories": [
        {
            "id": 1,
            "name": "Jawa Barat",
            "countries":[1]
        },
        {
            "id": 2,
            "code": "Jawa Timur",
            "countries":[1]
        },
        {
            "id": 3,
            "name": "Singapore",
            "countries":[2]
        },
        {
            "id": 3,
            "name": "Men's",
            "countries":[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        }
    ],
    "metadata": {
        "resultset": {
            "count": 3,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Category

Retrieve individual category

**URL:** /categories/{category_id}

Optional:

* `fields` - Retrieve specific properties. See [Category Properties](#category-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Category found and returned

**Response Params:** See [Category Properties](#category-properties)

**Examples:**

Request:

```
GET /categories/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 23,
    "item_type_id": 1,
    "name": "Men's",
    "slug": "mens",
    "sort_priority": 1,
    "is_active": true,
    "description": "Men's category",
    "meta": "men's category",
    "icon_image_normal": "categories/indonesia/icon.png",
    "icon_image_highres": "categories/indonesia/icon_highres.png",
    "parent_category_id": null,
    "countries": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Category

Update category.

**URL:** /categories/{category_id}

**Method:** PUT

**Request Data Params:**

Required: `item_type_id`, `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`, `parent_category_id`, `countries`

**Response HTTP Code:**

* `200` - Category updated

**Response Params:** See [Category Properties](#category-properties)

**Examples:**

Request:

```
PUT /categories/23 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "item_type_id": 1,
    "name": "Men's",
    "slug": "mens",
    "sort_priority": 1,
    "is_active": true,
    "description": "Men's global category",
    "meta": "men's global category",
    "icon_image_normal": "categories/indonesia/icon.png",
    "icon_image_highres": "categories/indonesia/icon_highres.png",
    "parent_category_id": null,
    "countries": null
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 23,
    "item_type_id": 1,
    "name": "Men's",
    "slug": "mens",
    "sort_priority": 1,
    "is_active": true,
    "description": "Men's global category",
    "meta": "men's global category",
    "icon_image_normal": "categories/indonesia/icon.png",
    "icon_image_highres": "categories/indonesia/icon_highres.png",
    "parent_category_id": null,
    "countries": null
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Category

Delete category. This service will set `is_active` value to false.

**URL:** /categories/{category_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Category deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /categories/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Parental Controls

Parental control resource.

Current parental control list:

* **4+**

    * Item in this category contain no objectionable material.

* **9+**

    * Item in this category may contain mild or infrequent occurrences of cartoon, fantasy, or realistic violence, and infrequent or mild mature, suggestive, or horror-themed content which may not be suitable for children under the age of 9.

* **12+**

    * Item in this category may also contain infrequent mild language, frequent or intense cartoon, fantasy, or realistic violence, and mild or infrequent mature or suggestive themes, and simulated gambling, which may not be suitable for children under the age of 12.

* **17+**

    * You must be at least 17 years old to consume this item. Items in this category may also contain frequent and intense offensive language; frequent and intense cartoon, fantasy, or realistic violence; and frequent and intense mature, horror, and suggestive themes; plus sexual content, nudity, alcohol, tobacco, and drugs which may not be suitable for children under the age of 17.

Note: Client side implementation might has restriction password recovery. To send password recovery email, refer to [Send Email](#send-email)

#### Parental Control Properties

* `id` - Number; Instance identifier.

* `name` - String; Parental control name.

* `sort_priority` - Number; Sort priority use to defining order. Higher number, more priority.

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. Default to true.

* `slug` - String, Unique; Parental control slug for SEO.

* `meta` - String; Parental control meta for SEO.

* `description` - String; Parental control description.

* `media_base_url` - String; Base URL for image files, this string must appended to image URI.

* `icon_image_normal` - String; Parental control icon image URI.

* `icon_image_highres` - String; Parental control icon image in highres URI.

#### Create Parental Control

Create new parental control.

**URL:** /parental_controls

**Method:** POST

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `201` - New parental control created

**Response Params:** See [Parental Control Properties](#parental-control-properties)

**Examples:**

Request:

```
POST /parental_controls HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "12+",
    "slug": "twelve-plus",
    "sort_priority": 1,
    "is_active": true,
    "description": "For age 12 or more",
    "meta": "For age 12 or more",
    "icon_image_normal": "parental_controls/twelve_plus/icon.png",
    "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/parental_controls/3

{
    "id": 3,
    "name": "12+",
    "slug": "twelve-plus",
    "sort_priority": 1,
    "is_active": true,
    "description": "For age 12 or more",
    "meta": "For age 12 or more",
    "icon_image_normal": "parental_controls/twelve_plus/icon.png",
    "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Parental Controls

Retrieve parental controls.

**URL:** /parental_controls

Optional:

* `is_active` - Filter by is_active status(es). Response will only returns if any matched

* `fields` - Retrieve specific fields. See [Parental Control Properties](#parental-control-properties). Default to ALL

* `offset` - Offset of the records. Default to 0

* `limit` - Limit of the records. Default to 9999

* `order` - Order the records. Default to order ascending by primary identifier or created timestamp

* `q` - Simple string query to records' string properties

**Method:** GET

**Response HTTP Code:**

* `200` - Parental Controls retrieved and returned in body content

* `204` - No matched parental control

**Response Params:** See [Parental Control Properties](#parental control-properties)

**Examples:**

Request:

```
GET /parental_controls?fields=id,name HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "parental_controls": [
        {
            "id": 1,
            "name": "4+"
        },
        {
            "id": 2,
            "code": "7+"
        },
        {
            "id": 3,
            "name": "12+"
        },
        {
            "id": 4,
            "code": "17+"
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 9999
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Parental Control

Retrieve individual parental control

**URL:** /parental_controls/{parental_control_id}

Optional:

* `fields` - Retrieve specific properties. See [Parental Control Properties](#parental-control-properties). Default to ALL

**Method:** GET

**Response HTTP Code:**

* `200` - Parental control found and returned

**Response Params:** See [Parental Control Properties](#parental-control-properties)

**Examples:**

Request:

```
GET /parental_controls/3 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 3,
    "name": "12+",
    "slug": "twelve-plus",
    "sort_priority": 1,
    "is_active": true,
    "description": "For age 12 or more",
    "meta": "For age 12 or more",
    "icon_image_normal": "parental_controls/twelve_plus/icon.png",
    "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Parental Control

Update parental control.

**URL:** /parental_controls/{parental_control_id}

**Method:** PUT

**Request Data Params:**

Required: `name`, `sort_priority`, `is_active`, `slug`

Optional: `meta`, `description`, `icon_image_normal`, `icon_image_highres`

**Response HTTP Code:**

* `200` - Parental control updated

**Response Params:** See [Parental Control Properties](#parental-control-properties)

**Examples:**

Request:

```
PUT /parental_controls/3 HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "12+",
    "slug": "twelve-plus",
    "sort_priority": 1,
    "is_active": true,
    "description": "For age 12+",
    "meta": "For age 12+",
    "icon_image_normal": "parental_controls/twelve_plus/icon.png",
    "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 3,
    "name": "12+",
    "slug": "twelve-plus",
    "sort_priority": 1,
    "is_active": true,
    "description": "For age 12+",
    "meta": "For age 12+",
    "icon_image_normal": "parental_controls/twelve_plus/icon.png",
    "icon_image_highres": "parental_controls/twelve_plus/icon_highres.png"
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Parental Control

Delete parental control. This service will set `is_active` value to false.

**URL:** /parental_controls/{parental_control_id}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Parental control deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /parental_controls/1 HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Bonuses

Bonus resource. This API Allows the system to send 5 free items and give free SCOOP points to new users.

#### Bonuses Properties

* `id` - Number; Gift identifier

* `name` - String; The name of the gift

* `items` - Number; List of the item id that included within the gift

* `brands` - Number; List of the brand id that included within the gift

* `valid_to` - String; Datetime in UTC when the gift validity is due on

* `valid_from` - String; Datetime in UTC when the gift is valid from

* `description` - String; Gift description

* `is_active` - Boolean; Instance's active status, used to mark if the instance is active or soft-deleted. The default value is true.

* `point` - Number; Amount of SCOOP points the user get from new account registration

* `point_referral` - Number; Amount of SCOOOP points the user get by entering referral code while creating new account


#### Create Bonuses

Makes new Bonuses.

**URL:** /bonuses

**Method:** POST

**Request Data Params:**

Required: `name`, `valid_to`, `valid_from`

Optional: `point`, `point_referral`, `brands`, `items`

**Response HTTP Code:**

* `201` - New Bonuses is created

**Response Params:** See [Bonuses Properties](#bonuses-properties)

**Examples:**

Request:

```
POST /bonuses HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "name": "Register users",
    "items": [1,2,3],
    "brands": [17,18],
    "valid_to": "2015-02-22T00:00:00Z",
    "valid_from": "2015-02-20T00:00:00Z",
    "description": "lorem ipsum dolor siamet",
    "is_active": True,
    "point": 50,
    "point_referral": 100
}
```

Success response:

```
HTTP/1.1 201 Created
Content-Type: application/json; charset=utf-8
Location: https://scoopadm.apps-foundry.com/scoopcor/api/bonuses/randombonus

{
    "id": 1,
    "name": "Register users",
    "items": [1,2,3],
    "brands": [17,18],
    "valid_to": "2015-02-22T00:00:00Z",
    "valid_from": "2015-02-20T00:00:00Z",
    "description": "lorem ipsum dolor siamet",
    "is_active": True,
    "point": 50,
    "point_referral": 100
}
```

Fail response:

```
HTTP/1.1 409 Conflict
Content-Type: application/json; charset=utf-8

{
    "status": 409,
    "error_code": 409100,
    "developer_message": "Conflict. Duplicate value on unique parameter"
    "user_message": "Duplicate value on unique parameter"
}
```

#### Retrieve Bonuses

Retrieves all of the Bonuses.

**URL:** /bonuses

**Method:** GET

**Response HTTP Code:**

* `200` - Bonuses Country Group data are retrieved and returned in body content

* `204` - No matched Bonuses Country Group

**Response Params:** See [Bonuses Properties](#bonuses-properties)

**Examples:**

Request:

```
GET /bonuses HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "bonuses": [
        {
            "id": 1,
            "name": "Register users",
            "items": [1,2,3],
            "brands": [17,18],
            "valid_to": "2015-02-22T00:00:00Z",
            "valid_from": "2015-02-20T00:00:00Z",
            "description": "lorem ipsum dolor siamet",
            "is_active": True,
            "point": 50,
            "point_referral": 100
        },
        {
            "id": 2,
            "name": "Facebook login",
            "items": [1,2,3],
            "brands": [17,18],
            "valid_to": "2015-02-22T00:00:00Z",
            "valid_from": "2015-02-20T00:00:00Z",
            "description": "lorem ipsum dolor siamet",
            "is_active": True,
            "point": 50,
            "point_referral": 100
        }
    ],
    "metadata": {
        "resultset": {
            "count": 2,
            "offset": 0,
            "limit": 20
        }
    }
}
```

Fail response:

```
HTTP/1.1 500 Internal Server Error
```

#### Retrieve Individual Gift

Retrieves individual Gift by using gift identifier.

**URL:** /bonuses/{gift_name}

**Response HTTP Code:**

* `200` - Bonuses data is found and returned

**Response Params:** See [Bonuses Properties](#bonuses-properties)

**Examples:**

Request:

```
GET /bonuses/randombonus HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
Accept: application/json
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "name": "Randombonus",
    "items": [1,2,3],
    "brands": [17,18],
    "valid_to": "2015-02-22T00:00:00Z",
    "valid_from": "2015-02-20T00:00:00Z",
    "description": "lorem ipsum dolor siamet",
    "is_active": True,
    "point": 50,
    "point_referral": 100
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Update Bonuses

Updates Bonuses data.

**URL:** /bonuses/{gift_name}

**Method:** PUT

**Request Data Params:**

Required: `name`, `valid_to`, `valid_from`

Optional: `point`, `point_referral`, `brands`, `items`

**Response HTTP Code:**

* `200` - Bonuses is updated

**Response Params:** See [Bonuses properties](#bonuses-properties)

**Examples:**

Request:

```
PUT /bonuses/randombonus HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "id": 2,
    "name": "Randombonus",
    "items": [1,2,3],
    "brands": [17,18],
    "valid_to": "2015-02-22T00:00:00Z",
    "valid_from": "2015-02-20T00:00:00Z",
    "description": "lorem ipsum dolor siamet",
    "is_active": True,
    "point": 50,
    "point_referral": 100
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "id": 2,
    "name": "Randombonus",
    "items": [1,2,3],
    "brands": [17,18],
    "valid_to": "2015-02-22T00:00:00Z",
    "valid_from": "2015-02-20T00:00:00Z",
    "description": "lorem ipsum dolor siamet",
    "is_active": True,
    "point": 50,
    "point_referral": 100
}
```

Fail response:

```
HTTP/1.1 404 Not Found
```

#### Delete Bonuses

Deletes Gift. This service will set `is_active` value to false.

**URL:** /bonuses/{gift_name}

**Method:** DELETE

**Response HTTP Code:**

* `204` - Gift is deleted

**Response Params:** -

**Examples:**

Request:

```
DELETE /bonuses/randombonuses HTTP/1.1
Authorization: Bearer ajksfhiau12371894^&*@&^$%
```

Success response:

```
HTTP/1.1 204 No Content
```

Fail response:

```
HTTP/1.1 404 Not Found
```

### Claim Bonuses Bonuses

API to claim the gifts

#### ACCESS API

**HTTP** (token : can_read_write_public)

**HTTPS** (token : can_read_write_public_ext)

#### Claim Bonuses Properties

* `name` - String; The name of the gift

* `items` - Number; List of the items that included within the gift. The combination between items and brands. 

            * If the brands (from gifts API) is left blank, all the items are retrieved based on the items (from gifts API)
            
            * If the items (from gifts API) is left blank, all the items are retrieved based on the last item by the brands

#### Update Claim Bonuses

Updates the claim based on the name.

**URL:** /bonuses/claim/{name}

e.g: 

* /bonuses/claim/facebook-register

* /bonuses/claim/guest-register

**Method:** PUT

**Request Data Params:**

Required: `user_id`

Optional: -

**Response HTTP Code:**

* `201`

**Response Params:** See [Claim Bonuses properties](#claim-properties)

**Examples:**

Request:

```
PUT /bonuses/claim/facebook-register HTTP/1.1
Content-Type: application/json
Authorization: Bearer ajksfhiau12371894^&*@&^$%

{
    "user_id": 11
}
```

Success response:

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8

{
    "name": "Register users",
    "items": 
        [
           {
            "id": 12,
            "name": "FHM01",
            "brand_id": 17,
            ....
            },
           {
            "id": 114,
            "name": "FHM0114",
            "brand_id": 17,
            ....
            }
        ]
}
```
