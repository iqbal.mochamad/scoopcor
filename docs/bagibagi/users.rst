User
====


Retrieves user details.


Properties
----------

phone_number
    String; MSISDN or Mobile Subscriber Integrated Services Digital Network Number or, literally, the Phone Number.
provider_id
    Number; Provider identifier.
created
    String; Created date.
is_active
    Boolean; Set to FALSE if the account is inactive.
modified
    String; Last edited date.
role_id
    Number;
referral_code
    String; Unique characters assigned to your account. You can use this code to refer your friends to get stuff.
last_login
    String; The last time user check into their account.
passcode
    String;
is_verified
    Boolean; Set to TRUE if the account is already verified.
id
    Number; User identifier.
phone_number_type
    Number;
response_code
    Number;


+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /users/5                                             |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+


Request Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    GET /users/5 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Accept: application/json; charset=utf-8
    Location: https://scooppulsa.apps-foundry.com/bagibagi/api/v1/users/5

.. code-block: json

    {
        "phone_number": "+6282136360755",
        "provider_id": 2,
        "created": "2016-03-04T07:33:26.317223",
        "is_active": true,
        "modified": "2016-04-14T07:43:48.252974",
        "role_id": 10,
        "referral_code": "V3NDZZ",
        "last_login": "2016-04-14T07:43:48.252616",
        "passcode": null,
        "is_verified": true,
        "id": 5,
        "phone_number_type": 1,
        "response_code": 200
    }
