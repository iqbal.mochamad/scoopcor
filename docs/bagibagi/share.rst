Share on Social Media
=====================


Retrieves credits (pulsa).


Properties
----------

offers
    Object;
response_code
    Number;


+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /offers                                              |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+


Request Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    GET /shares HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block: json

    {
        "user_id": 5,
        "social_type": 2
    }

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Accept: application/json; charset=utf-8
    Location: https://scooppulsa.apps-foundry.com/bagibagi/api/v1/shares

.. code-block: json

    {
        "name": "Twitter bonuses share",
        "total_point": 12166,
        "share_point": 200,
        "response_code": 200
    }
