Authentication
==============


Authenticates user. simple.


Properties
----------

phone_number
    String; MSISDN or Mobile Subscriber Integrated Services Digital Network Number or, literally, the Phone Number.
user_id
    Number; User identifier.
access_token
    String; A system object representing the subject of access control operations.
expires_in
    Number;
token_type
    String;
Provider
    Object that represents certain service provider
    id
        Number; Service provider identifier
    name
        String; Service provider name
referral_code
    String; Unique characters assigned to your account. You can use this code to refer your friends to BAGIBAGI and get stuff.
scope
    String;
is_verified
    Boolean;
refresh_token
    String;
response_token
    Number;


+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /auth/token                                          |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+


Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /auth/token/grant_type=password&phone_number=082136360755&scope=can_read_write_public_ext&client_id=2&device_mid=ff0e7f4e570e868a&device_imei=352957061683715&device_model=Samsung SM-G900H&device_mac_address=48:5A:3F:36:84:58 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scooppulsa.apps-foundry.com/bagibagi/api/v1/auth/token

.. code-block: json

    {
        "phone_number": "+6282136360755",
        "user_id": 5,
        "access_token": "e465a14d8b1f7ffc76cb945e097267bbbfb312b7c57d528994fb5b53e0308cdd60ef2745f4894ad1",
        "expires_in": 7776000,
        "token_type": "Bearer",
        "provider":
        {
            "id": 2,
            "name": "Telkomsel"
        },
        "referral_code": "V3NDZZ",
        "scope": "can_read_write_public_ext",
        "is_verified": true,
        "refresh_token": "2f866ca539024bf7c2e4e2a79548f1127daa39f39fcea7d6351483efaebee2b4ea09d78087da68fb",
        "response_code": 200
    }
