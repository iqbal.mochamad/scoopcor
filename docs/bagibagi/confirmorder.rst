Confirm Order
=============


Confirms order


Properties
----------

order_id
    Number; Order identifier.
payment_gateway_name
    String; MSISDN or Mobile Subscriber Integrated Services Digital Network Number or, literally, the Phone Number.
provider_id
    Number; Provider identifier.
total_amount
    Number; Total amount of credits.
final_amount
    Number; Final amount of credits.
platform_id
    Number; Platform identifier.
payment_gateway_id
    Number; Payment method identifier.
temp_order_id
    Number; Temporary order identifier.
client_id
    Number; Client identifier.
order_status
    Number; Order status
order_number
    Number; Order number
currency_code
    String; Currency code
response_code
    Number; Responce code returned.


+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /orders/confirm                                      |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+


Request Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    POST /orders/confirm   HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block: json

    {
        "user_id": 5,
        "payment_gateway_id": 1,
        "currency_code": "PTS",
        "client_id": 2,
        "order_number": 46167325294105,
        "temp_order_id": 12070
    }


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scooppulsa.apps-foundry.com/bagibagi/api/v1/orders/confirm

.. code-block: json

    {
        "payment_gateway_name": "Fastpay",
        "provider_id": 2,
        "total_amount": "20000.00",
        "order_id": 10324,
        "final_amount": "20000.00",
        "platform_id": 2,
        "payment_gateway_id": 1,
        "temp_order_id": 12070,
        "client_id": 2,
        "order_status": 10001,
        "order_number": 46167325294105,
        "currency_code": "PTS",
        "response_code": 201
    }
