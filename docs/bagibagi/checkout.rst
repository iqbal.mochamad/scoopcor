Checkout Order
==============


To ...


Properties
----------

payment_gateway_name
    String; MSISDN or Mobile Subscriber Integrated Services Digital Network Number or, literally, the Phone Number.
provider_id
    Number; Provider identifier.
total_amount
    Number; Total amount of credits.
final_amount
    Number; Final amount of credits.
platform_id
    Number; Platform identifier.
payment_gateway_id
    Number; Payment method identifier.
temp_order_id
    Number; Temporary order identifier.
client_id
    Number; Client identifier.
order_status
    Number; Order status
order_number
    Number; Order number
currency_code
    String; Currency code
response_code
    Number; Responce code returned.


+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /orders/checkout                                     |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+


Request Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    POST /orders/checkout HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block: json

    {
        "user_id": 5,
        "payment_gateway_id": 1,
        "platform_id": 2,
        "client_id": 2,
        "provider_id": 2,
        "order_lines": [
            {
                "offer_id": 15
            }
        ],
        "user_info": [
            {
                "user_country": "Indonesia",
                "ip_address": "202.179.188.106",
                "user_city": "Jakarta",
                "os_version": "5.0",
                "client_version": "1.1.1",
                "device_model": "Samsung SM-G900H",
                "user_state": "Jakarta",
                "latitude": -6.1744,
                "longitude": 106.8294
            }
        ]
    }


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scooppulsa.apps-foundry.com/bagibagi/api/v1/orders/checkout

.. code-block: json

    {
        "payment_gateway_name": "Fastpay",
        "provider_id": 2,
        "total_amount": 20000,
        "final_amount": 20000,
        "platform_id": 2,
        "payment_gateway_id": 1,
        "temp_order_id": 12070,
        "client_id": 2,
        "order_status": 10000,
        "order_number": 46167325294105,
        "currency_code": "PTS",
        "response_code": 201
    }
