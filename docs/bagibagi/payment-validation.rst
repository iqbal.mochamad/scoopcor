Payment Validation
==================


Validates order/payment


Properties
----------

status
    String; Payment status.
order_number
    Number;
total_point
    Number;
response_code
    Number;


+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /payments/validate                                   |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+


Request Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    POST /payments/validate    HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block: json

    {
        "order_id": 10324,
        "user_id": 5,
        "order_number": "46167325294105"
    }


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scooppulsa.apps-foundry.com/bagibagi/api/v1//payments/validate

.. code-block: json

    {
        "status": "success",
        "order_number": 46167325294105,
        "total_point": 12179,
        "response_code": 200
    }
