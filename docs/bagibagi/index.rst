BAGIBAGI API Endpoints
======================

Below is the documentation for BAGIBAGI API endpoints.

Base URL
--------

PROD : https://scooppulsa.apps-foundry.com/bagibagi/api/v1/


**Contents:**

.. toctree::
  :maxdepth: 1

  checkout
  checkreferralcode
  claim_periodic_point
  confirmation_code
  confirmorder
  getcredit
  login
  payment-validation
  periodic_check
  referal-status
  share
  summary
  users
  verify_code
