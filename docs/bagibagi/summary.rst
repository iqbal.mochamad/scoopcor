Retrieve Summary
================


Retrieves Summary. what do you expect.


Properties
----------

user_id
    Number; User identifier.
descriptions
    Number;
total_point
    Number; User's total point.
response_code
    Number;


+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /points/summary/{user_id}                            |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+


Request Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    GET points/summary/5 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Accept: application/json; charset=utf-8
    Location: https://scooppulsa.apps-foundry.com/bagibagi/api/v1/points/summary/5

.. code-block: json

    {
        "user_id": 5,
        "descriptions": null,
        "total_point": 11940,
        "response_code": 200
    }
