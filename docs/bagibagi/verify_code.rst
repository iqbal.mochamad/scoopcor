Verify Code
===========


Verifies the challenge code sent to user.


Properties
----------

phone_number
    String; MSISDN or Mobile Subscriber Integrated Services Digital Network Number or, literally, the Phone Number.
message
    String; Confirmation message "Successfully verified.".
user_id
    Number; User identifier.
response_code
    Number;


+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /auth/verify                                         |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+


Request Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    POST /auth/verify HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block: json

    {
        "phone_number": "+6282136360755",
        "passcode": "7110",
        "device_mid": "ff0e7f4e570e868a",
        "device_imei": "352957061683715",
        "device_model": "Samsung SM-G900H",
        "device_mac_address": "48:5A:3F:36:84:58"
    }


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: https://scooppulsa.apps-foundry.com/bagibagi/api/v1/auth/verify

.. code-block: json

    {
        "phone_number": "+6282136360755",
        "message": "Successfully verified.",
        "user_id": 5,
        "response_code": 200
    }
