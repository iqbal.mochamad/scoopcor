Database Migrations
===================

Changes to the schema (DDL changes), and alterations to existing (SQL changes)
are configured using `Alembic <http://alembic.readthedocs.org/en/latest/>`_.

This is an amazing tool, but there are a couple of got-ya's that we commonly
run into, so here's a quick rundown on things to watch out for (and solutions
to common problems we encounter with scoop apps).


Migrations with Enums
---------------------

Enums are created as custom user-defined types within Postgres.  We have great
support within SQLAlchemy for enums, but alembic support is noticibly lacking
in this area (although, as you'll see, it will actually create our enum
types for us).


Creating a New Enum
^^^^^^^^^^^^^^^^^^^

Alembic will automatically create an enum for you, the first time it sees
it in your migration upgrade, however it will **not** automatically drop it
for you on the migration downgrade (even if you've dropped the only
thing referencing the enum.. which is what it is supposed to do).

The easiest and most clear solution is to drop the enum by issuing SQL on
the downgrade.  Also note, it is 100% acceptable to issue raw sql from
within your migrations.


.. code-block:: python

    import alembic as op
    import sqlalchemy as sa
    from sqlalchemy import postgresql

    def upgrade():

        # During a normal create_table statement, if Alembic encounters
        # an ENUM type whose 'name' doesn't already exist in the database,
        # it will generate a CREATE TYPE statement for it, using the choices
        # you've specified (in this case, the letters 'a', 'b', and 'c')
        op.create_table(
            'some_table',
            sa.Column('some_column',
                      postgresql.ENUM('a',
                                      'b',
                                      'c',
                                      name='some_enum'),
                      nullable=False),
            # NOTE: Omitting all other columns!!
        )


    def downgrade():
        # We can drop our table normally, but our enum won't drop!
        op.drop_table('some_table')

        # We manually must grab the current database connection an issue
        # a 'drop type' statement to get rid of the enum.
        conn = op.get_bind()
        conn.execute("DROP TYPE some_enum;")


Modifying an Existing Enum
^^^^^^^^^^^^^^^^^^^^^^^^^^

When it comes time to modify an enum from within a migration, you have a few
issues that need dealt with.

1.  Alembic manages your transaction for you and you CANNOT modify a
    PostgreSQL type during a transaction.
2.  Values already assigned to an enum type cannot be removed (the type
    must be dropped and recreated..
    **MAKE SURE YOU READ ON -> I don't recommend doing this**)
3.  Values already assigned to an enum type will cause an error if you try
    to readd the same value.

Put together, it goes like this -- we're going to have to drop back to sql
again to accomplish our goals.

.. code-block:: python

    from alembic import op
    import sqlalchemy as sa


    def upgrade():

        # We really have to BS things here.. we've got to manually commit
        # the previous transaction, alter the type, and then restart a new
        # transaction.  This is relatively dangerous, and migrations that
        # alter enum types should COMPLETELY INDEPENDENT and run completely
        # independently of other migrations.
        conn = op.get_bind()
        conn.execute("COMMIT;")
        conn.execute("ALTER TYPE some_enum ADD VALUE IF NOT EXISTS 'd';")
        conn.execute("BEGIN READ WRITE;")

    def downgrade():
        # We can't drop the value, so we do NOTHING here!
        pass


**Why don't we drop the new value on the downgrade?**

A value cannot be dropped from an enum.  Instead, to remove a value from
an enum, it's a 4 step process

1.  Create a new enum type, with a new name, that doesn't contain the new
    value (in our example, it would be
    'ADD TYPE some_enum_1 ADD VALUES ('a', 'b', 'c');)
2.  Reassign all the columns using the old type to the new type (reassign
    'some_column' to be of type 'some_enum_1')
3.  Drop the old enum type.
4.  Rename the new enum type to the old.

Remember, the point of down migrations is to provide security in a failed
deployment, so we can return the database schema to the state it was in
previously before we roll our code back. With this in mind, it's almost
completely pointless to remove an enum that the old codebase won't know about.

You just need to remember to add the 'IF NOT EXISTS' clause to your upgrade,
so that when you run your database back up (after you've fixed whatever
critical bug in the production code caused you to do a rollback),
your migration won't fail.
