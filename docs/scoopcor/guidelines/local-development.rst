Local Development Setup
=======================

Vagrant
-------

Our development environment is




Recreating the PostgreSQL extensions
------------------------------------

You shouldn't have to often need this.. but in case you do:

.. code-block:: pgpl

    CREATE EXTENSION postgis;
    CREATE EXTENSION postgis_topology;
    CREATE EXTENSION fuzzystrmatch;
    CREATE EXTENSION postgis_tiger_geocoder;

    CREATE SCHEMA postgis;
    ALTER DATABASE scoopcor_test SET search_path="$user", public, postgis, topology;
    GRANT ALL ON SCHEMA postgis TO public;
    ALTER EXTENSION postgis SET SCHEMA postgis;

.. code-block:: bash

    pg_restore --host=local-docker --user=postgres --schema=public --no-owner --no-acl --verbose --dbname=scoopcor_db scoopcor_db.full.backup
