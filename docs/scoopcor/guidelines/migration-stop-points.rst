Migration Stop Points
=====================

Our database changes frequently, so periodically we will create a
migration stop point (where all previous migrations are removed and a
new base migration is created).

The process to creating a new stop point would be to grab (i'm tired,
finish explaining this later).

It is possible to migrate down beyond the stop-points (except for the initial
migration listed below), but that specific tag must be checked out, and the
database must be migrated down to the base revision (alembic downgrade base)


+------------------------------------------+----------+-----------------------------------+
|             Git SHA                      |   Tag    |   Notes                           |
+==========================================+==========+===================================+
| 6390ac68ca769af97bb47538db76e20a68be0193 | v2.6.10  | We were not using Alembic before  |
|                                          |          | this point, so you cannot migrate |
|                                          |          | past here                         |
+------------------------------------------+----------+-----------------------------------+
