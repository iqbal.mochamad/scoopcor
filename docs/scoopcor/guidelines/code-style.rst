Python Code Guidelines
======================


General
-------

.. code-block:: python

    import this

But also remember.. "A Foolish Consistency is the Hobgoblin of Little Minds"


#. Follow PEP8, but don't be stupid.
#. **EXCEPTION TO PEP8**.  Maximum line lengths should be 120 characters, not 80.  Screen sizes have changed a lot
   and 120 characters is more readable now.
#. Code is read far more often than it is written -- be extremely clear.
#. No one owns code, or features.
#. Don't unnecessarily make temporary variables.
#. Use dict/list initializer format, when possible.
#. Don't make methods that must be called sequentially from the caller of an object.
#. The bule is easily made sad;  don't make the bule sad.  Ask yourself, 'will this make the bule sad?' ... if so,
   maybe you shouldn't do it =P.
#. Use single-quotes for string literals.
#. Always include trailing commas in list/dictionary/set literals.

.. code-block:: python

    # unnecessary temp variables
    def sum(a, b):
        return_value = a + b
        return return_value

    def sum(a, b):
        return a + b


    # bad code API design (sequential method calls)
    my_inst = SequentialObject()
    my_inst.first_step()
    my_inst.second_step()
    my_inst.third_step()

    # note: in the above, first/second should be protected/private methods
    #       and then third_step should only need to be called externally.


    # bad dict/list initialization
    my_numbers = list()
    my_numbers.append(1)
    my_numbers.append(2)

    my_dict = dict()
    my_dict['a'] = 1
    my_dict['b'] = 2

    # better
    my_nubmers = [
        1,
        2,
    ]

    my_dict = {
        'a': 1,
        'b': 2,
    }


Design
------

#. Classes should be short.
#. Even if you think you're following the previous rule, your classes are probably still too long.
#. Functions should follow the same length rules as classes.
#. DON'T LIE. Your function and class names should describe EXACTLY what they do.  A well-named function is worth
   more than a paragraph of documentation.
#. Classes and functions should have 1 job.
#. Try to inject all your dependencies (without getting silly).. If you're doing this, object factories are your
   friends for production calls to your code.
#. Keep your code DRY
#. Don't over-design.  Favor getting it done first, then refactor. Large up-front design is slow and usually
   gets it wrong.
#. No magic numbers, EVER.  If you see a magic number create a well-named variable that tells me what status 21643 is
   (STATUS_21643 = 21643 tells me absolutely nothing.. PAYMENT_DECLINED_BY_BANK = 21643 does).
#. Keep all your code in a given function, method or class at a similar level of abstraction.
   This is a hard thing to master.
#. Inject dependencies wherever possible.  This makes your code actually testable, and won't break us as hard
   when moving to new libraries or versions of frameworks.
#. Follow the 'Principal of Least Astonishment'


Testing
-------

We have 3 different types of tests that currently must be written.  These are located under the ``test``
directory.

#. e2e - end to end tests.
    * These are generally what you'll see examples of online when you google 'how to test flask/django'
    * These simulate the full http request/response cycle and check the response of that.
    * These may check internal state of the database.
    * These may be slow and do not consistently need to run.
#. unit
    * Special Note:  These can be in the test.unit package OR be written as doctests inline with your code.
    * Simple tests against a single class/function.
    * These should never talk to the database and should be extremely fast.
#. integration
    * These live somewhere between unit and e2e tests.
    * They test the interaction between different layers of the application, but are more limited in scope than e2e.

Finally, we're experimenting with defining the e2e tests in a different section of the application using
'behavior-driven-testing', which is ideal because it allows QA to get involved with defining our tests.


Exception Handling
------------------

#. If an error condition is encountered, that caused an action not to be
   processed, **YOU MUST THROW AN EXCEPTION.**  Never return a special value like ``1``, ``success``, or ``failed``
   to indicate whether an operation succeeded.
#. That exception MUST be caught at the last possible moment (generally, in your controllers).
#. Generally, it's only acceptable to catch an exception further in the call stack if you want to reraise another
   exception type.
#. Don't be afraid to create your own exception types and to use them.


Comments
--------

#. The less, the better.  Give us descriptive function, variable, and class names.
#. If you can't, comment it.
#. Realize that your comments will eventually become lies.
#. Never comment out code, because you think you're going to use it later, or you wrote a new
   version of a function, and want to still have the old version. **If you see this, you must delete the code**.


Documentation
-------------

#. Our code has two different pieces of documentation.  User documentation (for API consumers, like mobile apps)
   and Devleoper Documentation (explaining what your code actually does).
#. Each piece of code should have some accompanying documentation, that at the bare minimum should specify
   the parameter types.
#. If a piece of code is clear enough, you may not need to document it.
#. Too much documentation just makes the code difficult to read.
#. Docstrings should use double-quotes, not single-quotes.
#. The first line of the docstring should short, and include a basic description.
#. Do not comment code with your name, date of modification, etc.  We've had version control systems
   for longer than you've been working as a programmer, that tell everyone exactly this information.

.. code-block:: python

    from __future__ import division
    from datetime import datetime

    get_user_age(user_id, db_session):
        """ Gets age of the `User`'s current age.

        If you need to add a lot of extra text, that could go down here.. but there needs
        to be a blank line from the first line, and any additional descriptive text.

        :param `int` user_id: The database identifier for the `User`.
        :param `sqlalchemy.orm.Session` db_session: Database session to user for querying.
        :returns: The given `User`'s age currently.
        :rtype: int
        """
        user = db_session.query(User).get(user_id)
        if not user:
            raise NotFound
        age = datetime.now().date() - user.profile.birthday
        return int(floor(age.days / 365))


Performance
-----------

#. Design solutions to be consistent with the rest of our code base first.
#. We're Python developers -- our performance is in our short release cycles.
#. Don't do crazy stuff because you think it'll get us better performance -- show there's a performance problem first,
   then do the crazy stuff.


Ideals
------

* Write the test cases before the code. When refactoring old code (with no unit tests), write unit tests before you
  refactor.
* Add new test cases before you start debugging
* Debugging is an AMAZING tool.  It's fundamentally better and faster than littering your code with "PRINT" statements.
* Unit testing is faster than debugging.


Naming
------

#. Do not lie.  A function called "make_url_secure()" should do what it says. If it varies based on whether or not the
   application is in debug mode, call it "make_url_secure_unless_debugging()" or something.  It's longer, and verges
   on line-noise, but it tells me exactly what it does.  I don't need to look at the code, or spend 5 minutes of my
   life trying to figure out why it's not changing my url to HTTPS on my dev machine.
#. Be aware of conventions and don't misuse them.
#. i and j are generally used for numeric iterators
#. args and kwargs are used for slurping positional arguments and keyword arguments into a function/method.
   **Don't use them for anything else.**
#. Avoid ambiguous words in names (e.g. Don't abbreviate "Number" to "No"; "Num" is a better choice.)
#. __varname for variables that are intended to be private
#. _varname for variables that are intended to be protected
#. Programming happens in English.  Your variable, function, class names, and comments should be in English.

.. code-block:: python

    # NO -- the name i, j, and k are conventions for ITERATOR variables.
    i = Item(name='abc')

    # YES
    for i in range(100):
        pass

    # NO -- kwargs is a python for variable keyword arguments to a function
    kwargs = {}

    # YES
    def do_stuff(**kwargs):
        pass


Required Reading
----------------

These books are required reading for all team members. The first two should be read immediately.

You do not need to purchase any of these books yourself.  They are provided by the office.

'*Design Patterns*' and '*Refactoring*' are not to be read cover-to-cover.  You should keep them handy and reference
them frequently while writing code.

* `Clean Code <http://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882>`_
* `The Clean Coder <http://www.amazon.com/Clean-Coder-Conduct-Professional-Programmers/dp/0137081073>`_
* `Test-Driven Development with Python <http://chimera.labs.oreilly.com/books/1234000000754>`_
* `Design Patterns: Elements of Reusable Object-Oriented Software <http://www.amazon.com/Design-Patterns-Elements-Reusable-Object-Oriented/dp/0201633612>`_
* `Refactoring: Improving the Design of Existing Code <http://www.amazon.com/Refactoring-Improving-Design-Existing-Code/dp/0201485672>`_


Further Reading
---------------

* `DRY (Don't repeat yourself) <http://en.wikipedia.org/wiki/Don%27t_repeat_yourself>`_
* `SRP (Single Responsibility Principle) <http://en.wikipedia.org/wiki/Single_responsibility_principle>`_
* `Code smell <http://en.wikipedia.org/wiki/Code_smell>`_
* `POLA (Principal of Least Astonishment) <https://en.wikipedia.org/wiki/Principle_of_least_astonishment>`_
* `Dependency Injection <https://en.wikipedia.org/wiki/Dependency_injection>`_
