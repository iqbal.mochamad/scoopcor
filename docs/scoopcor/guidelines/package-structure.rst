Standard Package Structure
==========================

ScoopCOR is grouped together with logically-similar models and apis existing in the same package.

Within that package, you'll find a similar layout.

.. note::

    Usually these will be modules, however they can also be packages themselves,
    with the important bits proxies out at the package level.


api
    `Controller`_ classes and functions.  Called API because most of the definitions in here are APIs.
    However, not all things defined in here are web APIs.  A more fitting name would be the 'controllers'.

blueprints
    Request routing definitions.  In Flask, they're called `blueprints`_, which is where the module
    name has come from.  A more fitting name would be 'routes' or 'routing'.

choices
    Simply constant values or enums.  Named as such, because these are generally options for multiple
    choice data.

models
    `SQLAlchemy`_ database model entity classes.  These should really
    be treated as `DTOs`_ as much as possible (even though they're a little more complex than that).
    **AVOID defining methods and properties on these as much as possible**.

parsers
    `Adapter`_ classes to turn the incoming request data into python.  These come in two, distinct flavors:

    * **BaseParsers** - these generally turn a request body into python.
    * **FilterParsers** - These take query string arguments and convert them into SqlAlchemy filter expressions.
      these are useful for list endpoints when allowing frontend to filter down the result set.

serializers
    `Adapter`_ classes that control the conversion of the outgoing response data.

viewmodels
    `Fascade`_ classes that are used to alter the appearance of the data presented to the serializer.


.. _Controller: https://en.wikipedia.org/wiki/Front_controller
.. _Adapter: https://en.wikipedia.org/wiki/Adapter_pattern
.. _Fascade: https://en.wikipedia.org/wiki/Adapter_pattern
.. _DTOs: https://en.wikipedia.org/wiki/Data_transfer_object
.. _blueprints: http://flask.pocoo.org/docs/0.10/blueprints/
.. _SQLAlchemy: http://www.sqlalchemy.org/

