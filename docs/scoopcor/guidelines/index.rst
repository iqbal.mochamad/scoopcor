Developer Guidelines/Reference
==============================

These pages are designed to be a how-to or reference for developers working
directly within the SCOOPCOR code base.


.. toctree::
    :maxdepth: 1

    code-style
    package-structure
    database-migrations
    migration-stop-points

