Remote Publishing
=================

Facilitates publishing of SCOOPCOR items, to a remote API.

The remote API is expected to maintain it's own Item (Offer) listing and
possibly it's own listing of categories.

We maintain a mapping of these for use within a special-purpose order checkout
API.

The code within this package is only concerned with maintaining those mappings
and support for updating those mappings, if they happen to change.


.. toctree::

    api
    models

