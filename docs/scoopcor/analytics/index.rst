Analytics Log
=============

The analytics log is used to record user activities while using the Scoop
mobile applications or GetScoop.com.

This feature utilizes the PostGIS extension for PostgreSQL for storing
the 'location' parameter (as a POINT).

http://postgis.net/docs/manual-1.3/ch06.html

Generally, when displaying the point data back out of the API, you're going
to be looking to use something like:

```
select ST_AsText('0101000000EC51B81E851B5040A4703D0AD7A32840')
```

```
ua = UserAction
# I don't know if we can use UserAction below for referecing the location paramer
# or if it must be done off the instance object.. investigate this.
session.scalar(ua.location.ST_AsText(ua.location.desc));
```


.. warning:: Mobile applications should be recommended to make calls to this
    API endpoint from a background thread.

.. info:: Although there are two different URL routes into this controller,
    all the logic is controlled by a single controller object.  This object
    uses the URL capture to determine which serializers, parsers, and query_sets
    should be used.

There are two different types of records
- pageview
- download

TODOS:
- Input parsers should wrap location in a POINT(LATITUDE LONGITUDE) string.
- Output serializers should convert
- Security tokens
