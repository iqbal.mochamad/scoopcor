Scoopcor Internals
==================

This documentation is for Python developers working on the back end code
base for scoopcor.

**Conventions**

- COR or SCOOPCOR both refer to this codebase.
- CORE or SCOOPCORE refers to an older version of this API utilizing Django.


**Table of Contents**

.. toctree::
    :maxdepth: 1

    guidelines/index
    remote_publishing/index
    items/index
