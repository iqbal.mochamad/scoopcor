Elevenia Service
================

Elevenia is an online store that can be accessed through
http://www.elevenia.co.id/

We are listing all of our single (non subscription) products available for sale
through their API.

.. warning::

    Elevenia will happily let you post the same product OVER and OVER again
    with no consequences.  You need to be VERY sure that when you're posting
    a product, it does not already exist in the Elevenia store.

    Elevenia also does not give us a mechanism for looking up products by OUR
    stock numbers.

.. warning::

    To access the elevenia testing sandbox, they do not have a separate domain.
    Instead you need to alter the hosts file on the machine you're testing
    from (See 'Modifying Hosts File for Testing' below).


Modifying Hosts File for Testing
--------------------------------

Elevenia does not have a separate sandbox domain -- instead, you need to set your
local hostfiles to point the REGULAR elevenia domain names, to the SANDBOX
server ip addresses.

.. code-block:: bash

    112.215.119.91 soffice.elevenia.co.id
    112.215.119.91 image.elevenia.co.id
    112.215.119.91 api.elevenia.co.id


Basic Usage
-----------

All communication with the Elevenia API happens by creating a 'gateway' object.

.. code-block:: python

    from app.services.elevenia import Elevenia

    gateway = Elevenia('YOUR-API-GOES-HERE')


Once you've done that, you can now fetch lists of product, categories,
and delivery templates..

.. code-block:: python

    from app.services.elevenia.entities import Product, Category, \
        DeliveryTemplate

    # all the delivery templates
    delivery_templates = gateway.resource(DeliveryTemplate).list()

    # all the categories, in a nested structure
    categories = gateway.resource(Category).list()

    # **PAGINATED** list of products
    product_pg_1 = gateway.resource(Product).list(page_num=1)


.. code-block:: python

    # create a new product
    p = gateway.resource(Product).new(
        name=u"Der Besüch den Alten Damme",
        nickname=u"Der Besüch den Alten Damme",
        scoop_item_id=2345, # This is OUR scoop database id
        sale_type=READY_STOCK,
        weight_kilograms=1,
        quantity_in_stock=99999,
        item_condition=NEW,
        vat_tax_code=NOT_TAXED,
        is_sellable_to_minors=True,
        uses_sales_period=False,
        price=99000,
        after_service_details="please contact support@getscoop.com",
        return_or_exchange_details="You may exchange your copy on only Mondays at Tanah Abang",
        has_delivery_guarantee=True,
        primary_image="http://images.getscoop.com/magazine_static/images/1/21266/general_small_covers/ID_MINE2015MTH04ED14_S_.jpg",
        html_detail="<h1>LOTS OF BIG TEXT.  PLZ BUY.  K THAX</h1>",
        delivery_template_id=delivery_templates[0].id,
        display_category_id=520 # religion
    )

    try:
        p.save()
    except ValidationError:
        # Client-side validation problem
        raise
    except BaseApiResponseError:
        # API-side error
        raise
