Depreciated
===========

This package contains API endpoints that have been depreciated.

They are maintained here to maintain interface compatibility, but are not
intended to be the focus of any further development effort.  They are
maintained only for backwards compatibility.

At the current time, this is only 3 endpoints, that are used for data that
we are no longer storing in the database after some schema changes (item_types
have been replaced with a postgresql enum, languages have been replaced with
a decision to store ISO 639 language codes directly, and countries by
 storing ISO 3166 country codes directly).

To maintain API interface compatibility, their data was dropped into JSON files
and it returned in the same format as previously documented.  We've of course
dropped support for creates, updates and deletes.

We've also dropped support for any filtering, sorting, and field limiting.
