Solr
====

Solr 4.10.2

This directory contains the configuration files for a single instance
of Solr and a single core of Solr.

It's currently setup to handle text in Bahasa Indonesia, but can also be
configured to handle English-language text if we need to do that at some
point in the future as well.


Installation
------------

This is based pretty much off the Solr 4.10.1 example project running
inside Jetty.  


Scoopcor required configuration
-------------------------------

    * /config.py 
        * set SOLR_URL to the URL of the scoop-main core, 
            not just the base solr url.

Solr required configuration
---------------------------

    * /solr/scoop-main/conf/solrconfig.xml
        * update the <lib> directive elements to point to where ever your solr
            contrib and dist libraries.
        * OR set the solr libDir attribute and dump everything in there
    * /solr/scoop-main/conf/data-import-handler.xml
        * setup the appropriate connection string information for dataimport
            handler.


Shortcuts for Solr configuration
--------------------------------

There are two scripts that will automatically symlink out the appropriate
configuration files for either dev2 or production (so, if your environment)
is setup like those, these will simply things for you.

* /solr/set-devel.bash
* /solr/set-prod.bash


Requirements
------------

* Java 1.7 or later
* PostgreSQL JDBC library somewhere on the class path


Plugins Used
------------

* Data Import Handler (DIH)


Supported Languages
-------------------

* Bahasa Indonesia
    * By default, this index is assuming that everything included is in
        Bahasa Indonesia.
* English
    * English language files are included, but are not being used
        currently by any fields.


Supported Currencies
--------------------

Currencies are specified within each Solr core. 
Within the scoopcor-main core, we're currently implementing 3 currencies.

**THERE IS A HUGE NOTE HERE**
All currency codes specified MUST be a part of the java.util.Currencies
that are specified on the local machine.

http://docs.oracle.com/javase/7/docs/api/java/util/Currency.html


* USD is currently the default.. But, we're not implementing this feature
    right now.. If we decide to use this for all search results, this is 
    something that we will likely want to implement