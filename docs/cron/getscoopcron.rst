GetScoop CRON Jobs
==================


*Populates new Vendor data based on Vendors API*
Populates new Vendor data based on ``Vendors`` API by last_vendor_populate_time recorded on GetScoop, then saves data to sitemap table

.. code-block:: bash

      *	22	*	*	*	/usr/bin/curl http://www.getscoop.com/cron802/seo_mapping/vendor	>	/tmp/cronmapping_vendor.log


*Populates new Author data based on Authors API*
Populates new Author data based on ``Authors`` API by last_author_populate_time recorded on GetScoop, then saves data to sitemap table

.. code-block:: bash

      *	23	*	*	*	/usr/bin/curl http://www.getscoop.com/cron802/seo_mapping/author	>	/tmp/cronmapping_author.log


*Populates new Brand data based on Brands API*
Populates new Brand data based on ``Brands`` API by last_brand_populate_time recorded on GetScoop, then saves data to sitemap table

.. code-block:: bash

     *	3	*	*	*	/usr/bin/curl http://www.getscoop.com/cron802/seo_mapping/brand	>	/tmp/cronmapping_brand.log


*Populates new Item data based on Item API*
Populates new Item data based on ``Item`` API by last_item_populate_time recorded on GetScoop, then saves data to sitemap table

.. code-block:: bash

      *	4	*	*	*	/usr/bin/curl http://www.getscoop.com/cron802/seo_mapping/item	>	/tmp/cronmapping_item.log


*Regenerates new sitemap XML files everyday*

.. code-block:: bash

      *	5	*	*	*	/usr/bin/curl http://www.getscoop.com/cron802/sitemap	>	/tmp/cronsitemap.log


*Displays items on GetScoop homepage*
Populates 12 items by calling ``items/latest`` API based on item type, and saves them to cache to be displayed on GetScoop home

.. code-block:: bash

      0	*/2	*	*	*	/usr/bin/curl http://www.getscoop.com/cron802/home	>	/tmp/cronsidebar_home.log


*Displays bestselling books*
Populates 5 items by calling ``item/popular`` API based on item type, and saves them to cache to be displayed on "Edisi Terpopuler" @ GetScoop

.. code-block:: bash

    0	*/2	*	*	*	/usr/bin/curl http://www.getscoop.com/cron802/sidebar	>	/tmp/cronsidebar.log


