Cron Job
========

Cron is a tool for configuring scheduled tasks on Unix systems, used to schedule commands or scripts to run periodically and at a fixed intervals. The layout for a cron entry is made up of six components: minute, houd, day of month, month of year, day of week, and the command to be executed.

as shown below::

    # m h dom moy dow command

.. code-block:: bash

    # * * * * * command to execute
    # ┬ ┬ ┬ ┬ ┬
    # │ │ │ │ │
    # │ │ │ │ │
    # │ │ │ │ └───── day of week (0 - 7) (0 to 6 are Sunday to Saturday,
    # │ │ │ │        or use names; 7 is Sunday, the same as 0)
    # │ │ │ └────────── month of year (1 - 12)
    # │ │ └─────────────── day of month (1 - 31)
    # │ └──────────────────── hour (0 - 23)
    # └───────────────────────── minutes (0 - 59)



+--------------------------------------------------------------------------+
|                          CRON expression                                 |
+--------------------+--------------+-------------------+------------------+
|    Field Name      |  Mandatory   |  Allowed Values   |  Special Chars   |
+====================+==============+===================+==================+
| minutes            |     YES      |  0 - 59           |  \* , -          |
+--------------------+--------------+-------------------+------------------+
| hours              |     YES      |  0 - 23  YES      |  \* , -          |
+--------------------+--------------+-------------------+------------------+
| day of month       |     YES      |  1 - 31           |  \* , - ? L W    |
+--------------------+--------------+-------------------+------------------+
| month              |     YES      |  1-12 or JAN-DEC  |  \* , -          |
+--------------------+--------------+-------------------+------------------+
| day of week        |     YES      |  0-6 or SUN-SAT   |  \* , - ? L #    |
+--------------------+--------------+-------------------+------------------+
| year               |     NO       |  1970-2099        |  \* , -          |
+--------------------+--------------+-------------------+------------------+

Asterisk (* )
    Asterisks defines "all"

Question mark (?)
    Used instead of '*' for leaving either day-of-month or day-of-week blank. Other cron implementations substitute "?" with the start-up time of the cron daemon, so that ? ? * * * * would be updated to 30 14 * * * * if cron started-up on 2:30pm, and would run at this time every day until restarted again.

Comma (,)
    Commas are used to separate items of a list. For example, using "1, 3, 6" in the 5rd field (day of week) means Mondays, Wednesdays, and Saturdays

Hyphen (-)
    Hyphens define ranges. For example, JAN-DEC indicates every month from January to December

Slash (/)
    Slashes are used to show intervals between command executions. For example, 0 12 2/5 * ? means "do command at 12 PM every 5 days every month, starting on the second day of the month". Note that frequencies in general cannot be expressed; only step values which evenly divide their range express accurate frequencies.

L
    L stands for "Last". When used in the day-of-week field, it allows you to specify constructs such as "The last friday" (5L) of a given month. In the day-of-month fiels, it specifies the last day of the month

W
    The 'W' character is allowed for the day-of-month field only. Used to specify the weekday (mon-fri) nearest the given day. For example, 15W means "the nearest weekday to the 15th of the month". if the 15th is a Saturday, the trigger fires on the previous day (14th).
    HOWEVER, if you specify 1W and the 1st is a Saturday, the trigger fires on Monday the 3rd. Can only be specified when the day-of-month is a single day.


**Contents:**

.. toctree::

    getscoopcron
    scoopcorcron
