ScoopCOR CRON Jobs
==================


some descriptions

.. code-block:: bash

      0	2	*	*	*	/home/alex.richo/clean_upload_tmp.sh	>	/home/alex.richo/cleanuploadtmp.log 2>&1	clean *.upload in tmp directory
      0	2	*	*	*	/home/alex.richo/clean_upload_media.sh	>	/home/alex.richo/cleanuploadmedia.log 2>&1	clean file inside media_temp_dir and contentupload_temp_dir with more 1 day
      0	*	*	*	*	/var/python/www/scoopcas/scripts/alert_unusual_traffic_users.py
      *	4	*	*	*	/var/python/www/scoopcor/scripts/popular_generator.py --command=refresh	>	/var/log/scoopcor/popular_generator.log
      0	0	*	*	*	/var/python/www/scoopcor/scripts/payment_alert_report.py	>	/var/log/app-log/cron_payment_alert_report.log


*Generate Subscription Renewals for iOS*

.. code-block:: bash

     */5	*	*	*	*	/usr/bin/curl http://scoopadm.apps-foundry.com/scoopcor/api/v1/payments/appleiaps/renewal/4	>	/tmp/auto-renewal.log


*Synchronize uploaded files from Scoop Portal to ScoopCOR*

.. code-block:: bash

      *	1	*	*	*	/home/script/retensi_upload_scoopportal.sh	>	/tmp/rsync_upload_file.log	clean /var/scoopportal and /var/scoopcor more 7 days


*Process uploaded items from operator*

.. code-block:: bash

      */2	*	*	*	*	/usr/bin/curl http://scoopadm.apps-foundry.com/scoopcor/api/v1/uploads/item_processing	>	/tmp/upload_processing.log


*Process uploaded items from operator*

.. code-block:: bash

      */2	*	*	*	*	/usr/bin/curl http://scoopadm.apps-foundry.com/scoopcor/api/v1/sc1deliver_files	>	/tmp/sc1_processing.log


*Reset password for cafè user*

.. code-block:: bash

      */60	*	*	*	*	/usr/bin/curl http://scoopadm.apps-foundry.com/scoopcas/api/v1/user/cafe_password >		/tmp/cafe_password.log


*Monitor Buffet Restore*

.. code-block:: bash

      0	23	*	*	*	/usr/bin/curl http://scoopadm.apps-foundry.com/scoopcor/api/v1/monitors/buffet_restore >		/tmp/buffet_restore.log


*Back-up Crontab*

.. code-block:: bash

      0	18	*	*	*	crontab -l	>	/baksvr2/cron.root.appsvr4


*Crontab for CORadm*

.. code-block:: bash

     0	0	*	*	*	sh /home/script/coradm_4_cron.sh /var/python/www/scoopcor/scripts/elevenia_items_publish.py	>	/var/log/scoopcor/elevenia_upload.log


*Delivers subscription plan to user library*

.. code-block:: bash

     */2	*	*	*	*	/usr/bin/curl http://scoopadm.apps-foundry.com/scoopcor/api/v1/items/deliver_subscriptions	>	/tmp/deliver_subscriptions.log
     */5	*	*	*	*	/usr/bin/curl http://scoopadm.apps-foundry.com/scoopcor/api/v1/monitors/items/deliver_subscriptions	>	/tmp/monitor-subscriptions.log


*ScoopAdm disable temporary run_remove_log.sh*

.. code-block:: bash

      5	0	*	*	*	/var/python/www/scoopcore/cronscript/run_auto_subscription.sh
      0	8	*	*	*	/var/python/www/scoopcore/cronscript/run_send_warning.sh
      0	*	*	*	*	cd /var/python/www/scooplogin && python2.6 manage.py run_remove_expired_link	>	/tmp/crontab-logs/scooplogin/run_remove_expired_link.log


*Clean up uploaded Newspaper*

.. code-block:: bash

      0	8	*	*	5	/var/python/www/scoopcore/magazine_static/media_temp_dir/zclean_media_temp_dir.sh -s	>	/tmp/zclean_media_temp_dir.log


*Clean up uploaded books*

.. code-block:: bash

      5	8	*	*	5	/var/python/www/scoopcore/magazine_static/contentupload_temp_dir/zclean_contentupload_temp_dir.sh -s	>	/tmp/zclean_contentupload_temp_dir.log


*Snort notification*

.. code-block:: bash

      */10 * *	*	*	/home/script/snortalert/znotifysnort.sh


*GetScoop notification*

.. code-block:: bash

      */10 * *	*	*	/home/script/getscpalert/znotifygetscp.sh


*Backs-up GetScoop blog*

.. code-block:: bash

      30 4 * *	*	/home/script/backup/zcompress.sh /appweb/getscoopblog /baksvr2/getscoopblog	>	/tmp/getscoopblog.log


*Backs-up nginx appsvr4*

.. code-block:: bash

      30 3 * *	*	/home/script/backup/zcompress.sh /etc/nginx /baksvr2/appsvr4_nginx	>	/tmp/backup_appsvr4_nginx.log
      30 3 * *	*	/home/script/backup/zcompress.sh /etc/nginx2 /baksvr2/appsvr4_nginx	>	/tmp/backup_appsvr4_nginx2.log


*Backs-up task*

.. code-block:: bash

      30 2 * *	*	/home/script/backup/zbackup_httpdall.sh
      35 2 * *	*	/home/script/backup/zbackup_scoop.sh
      40 2 * *	*	/home/script/backup/zbackup_ereader.sh
      45 2 * *	*	/home/script/backup/zbackup_getscoop.sh


*Ansible: Updates SOLR index*

.. code-block:: bash

      */15 * *	*	*	/opt/solr-4.10.2/server/solr/scoop-main/solr-index-update.bash


*GetScoop upload retention notification*

.. code-block:: bash

      *	20 * *	*	/home/script/retensi_upload_s3_backup.sh
