Review Item and Rating
======================

A review (in getSCOOP.com) is an evaluation of a publication, written by the user. On storefront, item reviews are shown under pricing plans. There are 3 parts of section within Review Item API:
* Reviews (``reviews``) contains all the reviews metadata the user made
* User review (``user_reviews``) contains the review metadata the currently-logged in user made. if she did not make any comment (yet), the resource is left blank.
* Pinned revies (``pinned_reviews``) contains pinned review metadata. Pinned review will always be shown on top of the review list.


Properties
----------

id
    Number; Automatically-generated instance identifier.

item_id
		Number; Item identifier in which the reviews addressed to

user_id
		Number; User identifier to whom the review belongs to

rating_val
		Number; The value of how good the book is, on a scale of 1 (less) to 5 (more).

name
		String; Reviewer's user name

message
		String; The content of the review. Be it good, be it bad, one should realize that one person's good might be another person's bad.

Created
		Number; the date and time the review was made

is_active
		Number; Instance's active status, used to mark if the instance is active (1) or soft-deleted(0). Set to 1 by default


Create
------

Adds new review

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /review/{item_id}                                    |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+


+------------------------------------------------------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| user_id                           |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| edition_id                        |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| name                              |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| rating                            |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| message                           |    YES    |                              |
+------------------------------------------------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /review/123 HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "user_id": 290411,
        "edition_id”: 802,
        "name": "Brian",
        “rating”: 4,
        “message”: “Test”
    }


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: http://api.getscoop.com/v1/review/123

.. code-block: json

    {
        “user_message”: “The review has been successfully saved.”
    }


List
----

Retrieves all the reviews within an item

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /review/{item_id}                                    |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
| Query String Parameters                                                      |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 | Default   | Description                  |
+===================================+===========+==============================+
| offset                            | 0         | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| limit                             | 10        | Pagination option            |
+-----------------------------------+-----------+------------------------------+
| is_active                         |           | Filter                       |
+-----------------------------------+-----------+------------------------------+
| fields                            | ALL       | Data option                  |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /review/802 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

{
    "metadata": {
        "resultset": {
            "count": 10,
            "offset": 0,
            "limit": 10
        }
    },
    "results": {
        "reviews": [{
            "id": "12",
            "item_id": "802",
            "user_id": "290411",
            "rating_val": "4",
            "name": "Brian",
            "message": "test",
            "created": "2014-03-31 00:25:15",
            "is_active": "1"
        }, {
            "id": "13",
            "item_id": "802",
            "user_id": "290411",
            "rating_val": "4",
            "name": "Brian",
            "message": "test",
            "created": "2014-03-31 00:25:15",
            "is_active": "1"
        }],
        "user_review": [ //only accessable if there's user_id
 	    *same as above*
        ],
        "pinned_reviews": [
 	    *same as above*
        ]
    }
}


Rating
------

Rating averages the value of accumulated ``rating_val`` of an item

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /rating/{item_id}                                    |
+-----------------------+------------------------------------------------------+
| HTTP Method           | GET                                                  |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    GET /rating/123 HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%
    Accept: application/json

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 200 OK
    Content-Type: application/json; charset=utf-8

.. code-block:: json

{
   “product_rating”: 4
}
