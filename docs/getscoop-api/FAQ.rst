FAQ
===

Frequently Asked Questions. List of answers to questions people frequently ask. List of (usually essential) things people want to comprehend but too indolent to open up :)


Properties
----------

faq_id
    Number; FAQ auto-generated identifier

message
    String; Notification whether the FAQ is posted or not.


Create
------

Adds new FAQ post

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /faq                                    |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /faq HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "user_id": 290411,
        “name”: “Brian”,
        “type”: “Majalah”,
        “email”: “bj@apps-foundry.com”,
        “subject”: “Problem at iOS”,
        “message”: “Test”
    }


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: http://api.getscoop.com/v1/faq

.. code-block: json

    {
       “faq_id”: “123”,
       “message”: “The FAQ has been successfully saved.”
    }
