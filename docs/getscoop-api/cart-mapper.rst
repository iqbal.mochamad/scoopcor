Cart Mapper
===========

Cart Mapper keeps all of the user's (uncompleted) order intact, as long as the cache is not cleared.


Properties
----------

user_id
    Number; User identifier

items
    Object; items properties, consists of

    id
        Number; Item identifier

    offer_id
        Number; Offer identifier

user_message
    String; Notification whether the request is success or failed


Create
------

Adds new cart mapper

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /cart_mappers                                        |
+-----------------------+------------------------------------------------------+
| HTTP Method           | POST                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

+-----------------------------------+-----------+------------------------------+
|                          Request Body Parameters                             |
+-----------------------------------+-----------+------------------------------+
|    Parameter Name                 |  Required |  Default                     |
+===================================+===========+==============================+
| user_id                           |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| items                             |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| id                                |    YES    |                              |
+-----------------------------------+-----------+------------------------------+
| offer_id                          |    YES    |                              |
+-----------------------------------+-----------+------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    POST /cart_mappers HTTP/1.1
    Accept: application/json
    Content-Type: application/json
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

.. code-block:: json

    {
        "user_id": 345286,
        "items": {
            "id": "802", < item id
            "offer_id": "1663"
        }
    }


Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 201 Created
    Content-Type: application/json; charset=utf-8
    Location: http://api.getscoop.com/v1/cart_mappers

.. code-block: json

    {
        “user_message”: “The Cart data has been successfully saved.”
    }


Delete
------

Destroys cart mapper after the transaction is committed.

+------------------------------------------------------------------------------+
|                            Endpoint Information                              |
+=======================+======================================================+
| Url                   | /cart_mappers                                        |
+-----------------------+------------------------------------------------------+
| HTTP Method           | DELETE                                                 |
+-----------------------+---------------+--------------------------------------+
| Request Headers       | Authorization | Bearer {your-scoopcas-token}         |
|                       +---------------+--------------------------------------+
|                       | Content-type  | application/json                     |
|                       +---------------+--------------------------------------+
|                       | Accept        | application/json                     |
+-----------------------+---------------+--------------------------------------+
| Success Response      | HTTP 201      | Created Successfully                 |
+-----------------------+---------------+--------------------------------------+
| Error Response        | HTTP 400      | Bad JSON or headers sent             |
|                       +---------------+--------------------------------------+
|                       | HTTP 401      | Incorrect permissions or bad auth    |
|                       |               | token                                |
+-----------------------+---------------+--------------------------------------+

Request Example
^^^^^^^^^^^^^^^

.. code-block:: http

    DELETE /cart_mappers HTTP/1.1
    Authorization: Bearer ajksfhiau12371894^&*@&^$%

Response Example
^^^^^^^^^^^^^^^^

.. code-block:: http

    HTTP/1.1 204 No Content
