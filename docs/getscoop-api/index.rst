GetSCOOP.com API Endpoints
==========================

GetSCOOP.com API is a REST-like JSON API.  Below is the documentation for any
API endpoints.

Base URL
--------

DEV : http://dev.apps-foundry.com/services

PROD : http://api.getscoop.com/v1


**Contents:**

.. toctree::

    FAQ
    review-item
    cart-mapper
    url-mapper
