SCOOP Geolocation Service
==========================


Usage
-----

Download MaxMind binary file (*.mmdb) from http://dev.maxmind.com/geoip/geoip2/geolite2, choose **GeoLite2 City**
and put the file in geodb folder inside your project workspace.


By IP Address
-------------

**Format URL in API:** ``<HOST>/geolocations/ip_address/<YOUR_IPV4_OR_IPV6_ADDRESS>``

**Examples:**

Request:

**DEV URL** ``http://dev2.apps-foundry.com/scoopgs/geolocations/ip_address/139.0.18.154``

**LIVE URL** ``http://scoopadm.apps-foundry.com/scoopgs/geolocations/ip_address/139.0.18.154``

Response:

If the location is retrieved by the IP:

```
{
    "city":{
        "name":null
    },
    "postal":{
        "code":null
    },
    "subdivision":{
        "iso_code":null,
        "name":null
    },
    "location":{
        "latitude":39.76,
        "accuracy_radius":null,
        "time_zone":null,
        "longitude":-98.5,
        "metro_code":null
    },
    "traits":{
        "domain":null,
        "isp":null,
        "user_type":null,
        "organization":null,
        "ip_address":"2607:f0d0:1002:51::4",
        "is_anonymous_proxy":false,
        "is_satellite_provider":false
    },
    "country":{
        "iso_code":"US",
        "name":"United States"
    },
    "represented_country":{
        "iso_code":null,
        "confidence":null,
        "represented_country":null,
        "name":null
    },
    "continent":{
        "code":"NA",
        "name":"North America"
    }
}
```
Otherwise, it returns ``404 Not Found``.


By Autodetect
-------------

**Format URL in API:** ``<HOST>/geolocations/autodetect``

This API will automatically detect your IP Address based on the request, either using proxy or not.

**Examples:**

Request:

**DEV URL** ``http://dev2.apps-foundry.com/scoopgs/geolocations/autodetect``

**LIVE URL** ``http://scoopadm.apps-foundry.com/scoopgs/geolocations/autodetect``


Application Configuration
-------------------------

The Following environmental variables can be set via environmental variables to change the configuration

* **SGS_DEBUG** (Default value: False) to enable/disable Flask/Werkzebug to debug features
* **SGS_LOGGING_CONFIG_PATH** (Default value: ``NULL``) to load the logging configuration defined in the value. If the value is ``NULL``, no logging will be loaded.
* **SGS_CSRF_ENABLED** (Default value: False) Non-functional configuration thingie. It changes the cross-site-request-forgery handling for forms.


Development History
-------------------

**v 1.0.1**

* Added Geolocation by autodetecting IP Address
* Change Readme
