Common Error
============

List of all the common error codes


401 Unauthorized
----------------

    Obtained when Authorization is used incorrectly

    **Error response**

    .. sourcecode:: http

        HTTPS 401 UNAUTHORIZED
        Content-Type: application/json

        {
            "status": 401,
            "developer_message": "You cannot access this resource with the provided credentials.",
            "error_code": 401,
            "user_message": "You cannot access this resource with the provided credentials."
        }

    :user_message: Error message to show to user
