Scripts
=======

List of all active Cron Job in e-Perpus server

Return borrowed items
---------------------
This script is use to automate returning borrowed eperpus items and sync the stock to SOLR (every 15 minutes)

.. code-block:: bash

    python scrips/auto_return_borrowed_items.py


Insert items to eperpus
-----------------------
This script is use to automate if there are new content for eperpus and sync to SOLR (every 1 hour)

.. code-block:: bash

    python scrips/auto_return_sharedlib.py


Renewal subscription
--------------------
This script is use to automate subscription renewal for iOS (every 30 minutes)

.. code-block:: bash

    python scrips/renewal_process.py


Release offer premium
---------------------
This script automate releasing items by vendor to offer premium (at 12am and 6pm everyday)

.. code-block:: bash

    python scrips/auto_insert_offer_vendors_to_offer_brands.py


Newspaper notification
----------------------
This script automate to push notification how many newspaper release per day (every 15 minutes)

.. code-block:: bash

    python scrips/new_newspapers_notification.py


Watchlist Notification
----------------------
This script automate to push notification about user's watchlist (every 10 minutes)

.. code-block:: bash

    python scrips/watchlist_notify.py


Transaction email
-----------------
This script will send email to eperpus admin if there's any purchased from eperpus' portal (every 4 minutes)

.. code-block:: bash

    python scrips/eperpus_async_email_sender.py


Update popular items
--------------------
This script is use to automate updating popular items to SOLR (at 5am, 11am, 4pm)

.. code-block:: bash

    python scrips/popular_generator.py


Bulk user add/delete
--------------------
This script is use to importing user from csv (every 3 minutes)

.. code-block:: bash

    python scrips/eperpus_bulk_user.py
