Group
=====

Child organization
-------------------

.. http:get:: v1/organizations/<ORGANIZATION_ID>/child-organizations?limit=<LIMIT>&offset=<OFFSET>&q=<QUERY>

    **Example request**

    .. sourcecode:: http

        GET /v1/organizations/1100827/child-organizations?limit=25&offset=0&q=

        HTTPS 200 OK
        Content-Type application/json
        Authorization JWT <USER_JWT_TOKEN>

    **Success response**

    .. sourcecode:: http


        HTTPS 200 OK
        Content-Type application/json

        {
            "entities": [],
            "metadata": {
                "resultset": {
                    "count": 0,
                    "limit": 25,
                    "offset": 0
                }
            }
        }

    :reqheader Authorization: JWT token
