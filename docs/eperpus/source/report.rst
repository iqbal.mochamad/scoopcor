Report
======

Book Loan History
-----------------

.. http:get:: v1/organizations/<ORGANIZATION_ID>/loan-book-history?end-date=<DATE>&start-date=<DATE>&limit=<LIMIT>&offset=<OFFSET>

    **Example request**

    .. sourcecode:: http

        GET /v1/organizations/1100827/loan-book-history?end_date=2018-08-28T16:59:59.999Z&start_date=2018-07-29T00:00:00.000Z&limit=25&offset=0
        Host https://dev6.apps-foundry.com/scoopcor/api
        Authorization JWT <USER_JWT_TOKEN>

    **Success response**

    .. sourcecode:: http


        HTTPS 200 OK
        Content-Type application/json

        {
            "items": [
                {
                    "username": "mimo123456",
                    "first_name": "suzumi",
                    "last_name": "suzu",
                    "borrowing_start_time": "2018-08-06T13:38:05.140841+07:00",
                    "returned_time": null,
                    "title": "(SIMPLE TIPS) BERPIKIR KREATIF"
                }
            ],
            "metadata": {
                "resultset": {
                    "count": 1,
                    "limit": 25,
                    "offset": 0
                }
            }
        }

    :reqheader Authorization: JWT token
