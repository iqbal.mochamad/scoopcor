Auth
====

Login
-----


.. http:post:: v1/auth/login

    Login Authentication.

    **Example request**:

    .. sourcecode:: http

        POST /v1/auth/login
        Host: https://dev6.apps-foundry.com/scoopcor/api
        Content-Type: application/json

        {
            "password":"123456",
            "username" : "admin3"
        }

    :<json str email: User's email
    :<json str password: User's password



    **Success response**:

    .. sourcecode:: http

        HTTPS 200 OK
        Allow: POST, OPTIONS
        Content-Type: application/vnd.gramedia.v3+json
        Transfer-Encoding: chunked
        Vary: Accept
        X-Total-Options: SAMEORIGIN

        {
            "email": "admin3@gmail.com",
            "expire_timedelta": 0,
            "expires": 1535684520,
            "first_name": null,
            "href": "https://dev6.apps-foundry.com/scoopcor/api/v1/users/1207959",
            "id": 1207959,
            "is_verified": true,
            "last_name": null,
            "realm": "JWT",
            "token": "eyJhbGciOiJIUzI1NiIsInR5c"
        }

    :>json str token: Login token.

    :statuscode 200: All's good.

    **Wrong email/password response**:

    .. sourcecode:: http

        HTTPS 401 UNAUTHORIZED
        Allow: POST, OPTIONS
        Vary: Accept
        X-Total-Options: SAMEORIGIN

        {
            "status": 401,
            "developer_message": "The password is incorrect",
            "error_code": 401,
            "user_message": "The username or password you entered is incorrect."
        }

    :>json str user_message: Contains error message to display to user
    :statuscode 401: Unauthorized.

    **Failed response**:

    .. sourcecode:: http

        HTTPS 500 Internal Server Error

    :statuscode 500: Internal Server Error.


Reset Password
---------------------------


.. http:post:: v1/auth/reset-password

    Reset Password

    **Example request**:

    .. sourcecode:: http

        POST /v1/auth/reset-password
        Host: https://dev6.apps-foundry.com/scoopcor/api
        Content-Type: application/json

        {
            "email": "admin@staff.gramedia.com"
        }

    :>json str email: User's email


Refresh Token
------------------


.. http:post:: v1/auth/refresh-token

    Refresh Token

    **Example request**:

    .. sourcecode:: http

        POST /v1/auth/refresh-token HTTPS/2.0
        Host: https://dev6.apps-foundry.com/scoopcor/api
        Content-Type: application/json
        Authorization: JWT <TOKEN>


    **Success response**:

    .. sourcecode:: http

        HTTPS 200 OK
        Allow: POST, OPTIONS
        Content-Type: application/vnd.gramedia.v3+json
        Transfer-Encoding: chunked
        Vary: Accept
        X-Total-Options: SAMEORIGIN

        {
            "expire_timedelta": [
                0
            ],
            "expires": 1535685668,
            "realm": "JWT",
            "token": "eyJhbGciOiJIUzI1Ni"
        }

    :>json str token: New user token.

    :statuscode 200: All's good.

    :reqheader Authorization: JWT authorization (login token).

    **Wrong token**:

    .. sourcecode:: http

        HTTPS 401 UNAUTHORIZED
        Allow: POST, OPTIONS
        Content-Type: application/vnd.gramedia.v3+json
        Transfer-Encoding: chunked
        Vary: Accept
        X-Total-Options: SAMEORIGIN

        {
            "status": 401,
            "developer_message": "Token invalid",
            "error_code": 401,
            "user_message": "Token invalid"
        }

    :statuscode 401: Unauthorized.
