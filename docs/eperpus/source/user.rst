User
====

Get current user
----------------


.. http:get:: v1/users/current-user

    Get current user

    **Example request**

    .. sourcecode:: http

        GET /v1/users/current-user
        Host https://dev6.apps-foundry.com/scoopcor/api
        Authorization JWT <USER_JWT_TOKEN>


    **Success response**

    .. sourcecode:: http

        HTTPS 200 OK
        Content-Type application/json


        {
            "active_buffet": null,
            "allow_age_restricted_content": true,
            "email": "admin3@gmail.com",
            "first_name": null,
            "id": 1207959,
            "is_active": true,
            "is_verified": true,
            "last_name": null,
            "level": null,
            "note": null,
            "organizations": [
                {
                    "app_name": "Smart Library",
                    "href": "https://dev6.apps-foundry.com/scoopcor/api/v1/organizations/1234824",
                    "id": 1234824,
                    "is_manager": true,
                    "logo_url": "img/masthead/logo-smart.png",
                    "maximum_user_allowed": 1,
                    "name": "Nama Klien 3",
                    "type": "shared library",
                    "username_prefix": ""
                }
            ],
            "origin_client_id": 2,
            "profile": {
                "birthdate": null,
                "gender": null,
                "origin_country_code": null,
                "origin_device_model": null,
                "origin_latitude": null,
                "origin_longitude": null,
                "origin_partner_id": null,
                "source_type": 1
            },
            "profile_pic": {
                "href": "https://images.apps-foundry.com/scoop/defaults/user-profile-pic.png",
                "title": "Gambar Profil"
            },
            "roles": [
                {
                    "href": null,
                    "id": 21,
                    "title": "Internal Admin Eperpus"
                },
                {
                    "href": null,
                    "id": 10,
                    "title": "verified user"
                }
            ],
            "signup_date": "2017-07-06T12:09:30.477062+00:00",
            "username": "admin3"
        }

    :>json str email: User's email.
    :>jsonarr object organizations: User's organizations
    :>jsonarr int organizations.id: User's orgs ID
    :>jsonarr boolean organizations.is_manager: Status if user is manager in this orgs
    :>jsonarr str organization.username_prefix: Orgs username prefix

    :reqheader Authorization: JWT token
    :statuscode 200: OK

    **Not provide credential**

    .. sourcecode:: http

        HTTPS 401 UNAUTHORIZED
        Content-Type application/json

        {
            "status": 401,
            "developer_message": "You cannot access this resource with the provided credentials.",
            "error_code": 401,
            "user_message": "You cannot access this resource with the provided credentials."
        }

    :statuscode 401: Unauthorized.

    **Invalid token**

    .. sourcecode:: http

        HTTPS 401 UNAUTHORIZED
        Content-Type application/json

        {
            "status": 401,
            "developer_message": "Token invalid",
            "error_code": 401,
            "user_message": "Token invalid"
        }

    :statuscode 401: Unauthorized


Get user list by role id
-------------------------


.. http:get:: v1/users?role_id=<ROLE_ID>

    **Example request**

    .. sourcecode:: http

        GET /v1/users?role_id=3
        Host https://dev6.apps-foundry.com/scoopcor/api
        Authorization JWT <USER_JWT_TOKEN>

    **Query string paramaters**

        role_id - Role id you want to filter

    **Success response**

    .. sourcecode:: http

        HTTPS 200 OK
        Content-Type application/json

        {
            "users": [
                {
                    "email": "anang@staff.gramedia.com",
                    "fullname": "Anang Chandra Wijaya",
                    "id": 1120656,
                    "phone_number": "081284277734",
                    "roles": [
                        {
                            "id": 3,
                            "name": "internal marketing"
                        }
                    ]
                }
            ]
        }

    :>jsonarr object users: List of users
    :>jsonarr str users.email: User's email
    :>jsonarr str users.fullname: User's fullname

    :reqheader Authorization: JWT token
    :statuscode 200: OK


Get users list in organizations
-------------------------------

.. http:get:: v1/organizations/<ORGANIZATION_ID>/users?offset=<OFFSET>&limit=<LIMIT>&q=

    **Example request**

    .. sourcecode:: http

        GET /v1/organizations/1100827/users?offset=0&limit=25&q=
        Host https://dev6.apps-foundry.com/scoopcor/api
        Authorization JWT <USER_JWT_TOKEN>

    **Query string paramaters**

        offset - Offset for pagination

        limit - Limit how many data will be return

        q - Query

    **Success response**

    .. sourcecode:: http


        HTTPS 200 OK
        Content-Type application/json

        {
            "metadata": {
                "facets": [
                    {
                        "field_name": "is_active",
                        "values": [
                            {
                                "count": 5,
                                "value": "true"
                            },
                            {
                                "count": 0,
                                "value": "false"
                            }
                        ]
                    }
                ],
                "resultset": {
                    "count": 5,
                    "limit": 25,
                    "offset": 0
                },
                "spelling_suggestions": []
            },
            "users": [
                {
                    "email": "monk_line@yahoo.com",
                    "first_name": "user",
                    "id": 1233873,
                    "is_active": true,
                    "last_name": "test1",
                    "level": null,
                    "note": null,
                    "organizations": [],
                    "organizations_name": "",
                    "signup_date": "2018-07-18T05:04:09.624904+00:00",
                    "username": "tsel123456"
                },
                {
                    "email": "email7@gmail.com",
                    "first_name": "user",
                    "id": 1233910,
                    "is_active": true,
                    "last_name": "test2",
                    "level": null,
                    "note": null,
                    "organizations": [],
                    "organizations_name": "",
                    "signup_date": "2018-07-26T07:36:06.826367+00:00",
                    "username": "cima123456"
                }
            ]
        }

    :>jsonarr object users: List of users
    :>jsonarr str users.email: User's email
    :>jsonarr str users.fullname: User's fullname

    :reqheader Authorization: JWT token
    :statuscode 200: OK


    **Not provide credential**

    .. sourcecode:: http

        HTTPS 403 FORBIDDEN
        Content-Type application/json

        {
            "status": 403,
            "developer_message": "You cannot access this resource.",
            "error_code": 403,
            "user_message": "You cannot access this resource."
        }

    :statuscode 403: Forbidden


Create new Client
-----------------

.. http:post:: v1/new-eperpus

    **Example request**

    .. sourcecode:: http

        POST /v1/new-client
        Host https://dev6.apps-foundry.com/scoopcor/api
        Authorization JWT <USER_JWT_TOKEN>
        Content-Type application/json

        {
           "name":"Nama Klien 2",
           "legal_name":"Nama Legal Klien 2",
           "maximum_user_allowed":1,
           "pic_client":"PIC Klien",
           "contact_email":"admin3@admin.com",
           "phone_primary":"1234567",
           "phone_alternate":"1234566",
           "mailing_address":"Jalan Blora",
           "mailing_city":"Jakarta",
           "mailing_province":"DKI Jakarta",
           "mailing_postal_code":"10235",
           "marketing_pic_id":1120656,
           "eperpus_type":"2",
           "app_name":"",
           "username_prefix":"",
           "max_device":1,
           "user_borrowing_limit":1,
           "general_borrowing_time":"7 Days",
           "book_borrowing_time":"1 Days",
           "magazine_borrowing_time":"1 Days",
           "newspaper_borrowing_time":"1 Days",
           "catalog_name":"Nama Katalog"
        }

    :>json str name: New client name
    :>json str legal_name: Client legal name
    :>json int maximum_user_allowed: Maximum user in this orgs
    :>json int eperpus_type: Eperpus type #TODO: Define eperpus_type

    :reqheader Authorization: JWT token

    **Success response**

    .. sourcecode:: http

        HTTPS 201 CREATED
        Content-Type application/json

        {
            "href": "",
            "is_manager": null,
            "logo_url": "img/masthead/logo-smart.png",
            "maximum_user_allowed": 1,
            "message": "success create new eperpus!",
            "name": "Test Client Docs",
            "organization_id": 1234830,
            "type": "shared library",
            "username_prefix": ""
        }

    :statuscode 201: Created

    **Conflict response**

    .. sourcecode:: http


        HTTPS 500 INTERNAL SERVER ERROR
        Content-Type application/json

        {
            "status": 500,
            "developer_message": "Error create_organization 409: Conflict",
            "error_code": 500,
            "user_message": "Error create_organization 409: Conflict"
        }

    :statuscode 500: Internal server error


Wallet
------

.. http:get:: v1/organizations/<ORGANIZATION_ID>/wallet

    **Example request**

    .. sourcecode:: http

        GET /v1/organizations/1100827/wallet
        Host https://dev6.apps-foundry.com/scoopcor/api
        Authorization JWT <USER_JWT_TOKEN>

    **Success response**

    .. sourcecode:: http


        HTTPS 200 OK
        Content-Type application/json

        {
            "balance": 0,
            "transactions": {
                "href": "https://dev6.apps-foundry.com/scoopcor/api/v1/organizations/1234813/wallet/transactions",
                "title": "Transaction History"
            }
        }

    :reqheader Authorization: JWT token


User borrowed items
-------------------

.. http:get:: v1/users/current-user/borrowed-items

    **Example request**

    .. sourcecode:: http

        GET /v1/users/current-user/borrowed-items
        Host https://dev6.apps-foundry.com/scoopcor/api
        Authorization JWT <USER_JWT_TOKEN>

    **Success response**

    .. sourcecode:: http

        HTTPS 200 OK
        Content-Type application/json

        {
            "items": [
                {
                    "authors": [],
                    "borrowing_start_time": "2018-03-21T09:59:13+00:00",
                    "catalog": {
                        "href": "https://dev6.apps-foundry.com/scoopcor/api/v1/organizations/1100821/shared-catalogs/28",
                        "id": 28,
                        "rel": "catalog",
                        "title": "Katalog Gramedia"
                    },
                    "catalog_item": {
                        "href": "https://dev6.apps-foundry.com/scoopcor/api/v1/organizations/1100821/shared-catalogs/28/119972",
                        "title": "Detil Item di Katalog"
                    },
                    "categories": [
                        {
                            "href": null,
                            "id": 5,
                            "title": "Technology, Games & Gadget"
                        }
                    ],
                    "cover_image": {
                        "href": "https://s3-ap-southeast-1.amazonaws.com/ebook-covers/738/general_big_covers/ID_GPG2016MTH11_B.jpg",
                        "title": "Gambar Sampul"
                    },
                    "currently_available": 0,
                    "details": {
                        "href": "https://dev6.apps-foundry.com/scoopcor/api/v1/items/119972",
                        "id": 119972,
                        "title": "Detil Item"
                    },
                    "download": {
                        "href": "https://dev6.apps-foundry.com/scoopcor/api/v1/items/119972/download",
                        "title": "Download buat Baca Offline"
                    },
                    "expires": "2018-03-26T09:59:13+00:00",
                    "first_name": "scoop",
                    "href": "https://dev6.apps-foundry.com/scoopcor/api/v1/users/current-user/borrowed-items/61784",
                    "id": 61784,
                    "last_name": "test",
                    "page_count": 47,
                    "returned_time": null,
                    "title": "gopego / NOV 2016",
                    "username": "sc",
                    "vendor": {
                        "href": "https://dev6.apps-foundry.com/scoopcor/api/v1/vendors/202",
                        "title": "gopego"
                    }
                }
            ],
            "metadata": {
                "resultset": {
                    "count": 18,
                    "limit": 20,
                    "offset": 0
                }
            }
        }

    :reqheader Authorization: JWT token


User owned items
----------------

.. http:get:: v1/users/current-user/owned-items

    **Example request**

    .. sourcecode:: http

        GET /v1/users/current-user/owned-items
        Host https://dev6.apps-foundry.com/scoopcor/api
        Authorization JWT <USER_JWT_TOKEN>

    **Success response**

    .. sourcecode:: http

        HTTPS 200 OK
        Content-Type application/json

        {
            "items": [
                {
                    "brand": {
                        "href": "http://localhost:5000/v1/brands/795",
                        "id": 795,
                        "title": "SCOOP Newspaper",
                        "vendor": {
                            "href": "http://localhost:5000/v1/vendors/77",
                            "id": 77,
                            "title": "FREE BRAND"
                        }
                    },
                    "content_type": "pdf",
                    "edition_code": "FREE_NEWS2018MTH04DT02",
                    "href": "http://localhost:5000/v1/items/135591",
                    "id": 135591,
                    "image": {
                        "href": "https://scoopadm.apps-foundry.com/ebook-covers/images/1/795/image_highres/FREE_NEWS2018MTH04DT02.jpg",
                        "title": "Cover Image"
                    },
                    "is_age_restricted": false,
                    "issue_number": "02 APR 2018",
                    "item_type": "newspaper",
                    "release_date": "2018-04-01T17:00:00+00:00",
                    "thumbnail": {
                        "href": "https://scoopadm.apps-foundry.com/ebook-covers/images/1/795/thumb_image_highres/FREE_NEWS2018MTH04DT02.jpg",
                        "title": "Thumbnail Image"
                    },
                    "title": "SCOOP Newspaper / 02 APR 2018"
                },
                {
                    "brand": {
                        "href": "http://localhost:5000/v1/brands/795",
                        "id": 795,
                        "title": "SCOOP Newspaper",
                        "vendor": {
                            "href": "http://localhost:5000/v1/vendors/77",
                            "id": 77,
                            "title": "FREE BRAND"
                        }
                    },
                    "content_type": "pdf",
                    "edition_code": "FREE_NEWS2018MTH03DT29",
                    "href": "http://localhost:5000/v1/items/135590",
                    "id": 135590,
                    "image": {
                        "href": "https://scoopadm.apps-foundry.com/ebook-covers/images/1/795/image_highres/FREE_NEWS2018MTH03DT29.jpg",
                        "title": "Cover Image"
                    },
                    "is_age_restricted": false,
                    "issue_number": "29 MAR 2018",
                    "item_type": "newspaper",
                    "release_date": "2018-03-28T17:00:00+00:00",
                    "thumbnail": {
                        "href": "https://scoopadm.apps-foundry.com/ebook-covers/images/1/795/thumb_image_highres/FREE_NEWS2018MTH03DT29.jpg",
                        "title": "Thumbnail Image"
                    },
                    "title": "SCOOP Newspaper / 29 MAR 2018"
                },
                {
                    "brand": {
                        "href": "http://localhost:5000/v1/brands/142",
                        "id": 142,
                        "title": "POPULAR",
                        "vendor": {
                            "href": "http://localhost:5000/v1/vendors/65",
                            "id": 65,
                            "title": "PAPILLON PENERBIT"
                        }
                    },
                    "content_type": "pdf",
                    "edition_code": "ID_POPLR2017MTH07",
                    "href": "http://localhost:5000/v1/items/135502",
                    "id": 135502,
                    "image": {
                        "href": "https://scoopadm.apps-foundry.com/ebook-covers/images/1/142/big_covers/ID_POPLR2017MTH07_B.jpg",
                        "title": "Cover Image"
                    },
                    "is_age_restricted": true,
                    "issue_number": "JUL 2017",
                    "item_type": "magazine",
                    "release_date": "2017-07-05T00:00:00+00:00",
                    "thumbnail": {
                        "href": "https://scoopadm.apps-foundry.com/ebook-covers/images/1/142/small_covers/ID_POPLR2017MTH07_S.jpg",
                        "title": "Thumbnail Image"
                    },
                    "title": "POPULAR / JUL 2017"
                }
            ],
            "metadata": {
                "resultset": {
                    "count": 10138,
                    "limit": 20,
                    "offset": 0
                }
            }
        }

    :reqheader Authorization: JWT token
