Dashboard
=========

Item borrowed top
-----------------

.. http:get:: v1/reports/eperpus/<ORGANIZATION_ID>/item-borrowed-top-n?end-date=<DATE>&end-date2=<DATE>&limit=<LIMIT>&start-date=<DATE>&start-date2=<DATE>

    **Example request**

    .. sourcecode:: http

        GET /v1/reports/eperpus/1100827/item-borrowed-top-n??end-date=%222018-08-26T16:59:59.000Z%22&end-date2=%222018-08-19T16:59:59.000Z%22&limit=10&start-date=%222018-08-20T00:00:00.000Z%22&start-date2=%222018-08-13T00:00:00.000Z%22

    **Query string parameters**

    **Success response**

    .. sourcecode:: http


        HTTPS 200 OK
        Content-Type application/json

        {
            "data": [
                {
                    "group1": "Agar Jantung Sehat ",
                    "id": 12013,
                    "value": 1
                }
            ]
        }
