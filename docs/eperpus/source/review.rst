Review
======

Add review
----------

.. http:post:: v1/reviews

    Add new review

    **Example request**:

    .. sourcecode:: http

        POST /scoopcor/api/v1/reviews
        Host: https://dev.apps-foundry.com
        Authorization JWT <USER_JWT_TOKEN>
        Content-Type: application/json

        {
            "borrowing_id": 182202,
            "rating": 5,
            "review": "balabalalabalabalwabakwan"
        }

    :<json integer borrowing_id: Borrowing id
    :<json integer rating: Rating for an item. Range from 1-5.
    :<json string review: review from user
