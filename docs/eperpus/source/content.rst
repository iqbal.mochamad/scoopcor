Content
=======

Get products
------------

.. http:get:: v1/organizations/<ORGANIZATION_ID>/items?limit=<LIMIT>&offset=<OFFSET>

    **Example request**

    .. sourcecode:: http

        GET /v1/organizations/1100827/items?limit=25&offset=0&q=
        Host https://dev6.apps-foundry.com/scoopcor/api
        Authorization JWT <USER_JWT_TOKEN>


    **Success response**

    .. sourcecode:: http


        HTTPS 200 OK
        Content-Type application/json

        {
            "items": [],
            "metadata": {
                "facets": [
                    {
                        "field_name": "item_authors",
                        "values": []
                    },
                    {
                        "field_name": "item_categories",
                        "values": []
                    },
                    {
                        "field_name": "item_languages",
                        "values": []
                    }
                ],
                "resultset": {
                    "count": 0,
                    "limit": 25,
                    "offset": 0
                },
                "spelling_suggestions": []
            }
        }

    :>jsonarr object items: List of items

    :reqheader Authorization: JWT token

