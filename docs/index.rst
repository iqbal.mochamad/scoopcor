Welcome to Scoopcor's documentation!
====================================

This documentation is is divided into a few major sections, each for different
target audicences.

A quick synopsis of what you can expect to find in any of the sections follows.

Api
    Intended for other teams that must communicate with the scoopcor API.
    This is a replacement for the old "scoopcor_api_spec.md" markdown document.
    It includes information about the various available API endpoints, along
    with JSON Schema (Draft 4) linked to each document to allow you to validate
    your data.
Scoopcor Internals
    Programmer documentation for any developers writing the code for Scoopcor
    itself.  It includes information about the purpose of various classes,
    helper methods, along with generated code API documentation.
3rd Party
    Sys Admin/Programmer documentation about the various 3rd party software
    packages used by scoopcor (such as Solr, Redis, and PostgreSQL).

**Conventions:**

* Terminology
    * An endpoint is an individual API url which can be called with a
        specific method.
    * A resource refers to an individual JSON document that is returned.
    * Resources have 'properties', which are also called 'attributes'.
        * I personally prefer the term 'attributes', but Google uses the term
            properties in their documentation, so I will as well.


**Contents:**

.. toctree::
    :maxdepth: 1

    api/index
    scoopcor/index
    cron/index
    3rd-party/index

**Other Documentation**

These documents are not related to scoopcor, and really need moved out into the respective projects.

getscoop-api/index
scoopgs/index
bagibagi/index


Current Issues
--------------

* I don't think our example requests/responses should include the use of
    the 'fields' parameter.
* I'd like to take all the repetitive documentation explaining error status
    codes and expected response codes out of the individual documentation,
    and place them in a single, api-wide document.  It's unnecessary and
    likely distracts from reading the rest of the documentation which
    is important to understand for that individual endpoint.
* Include default values
* Find some way to better document the properties of an individual resource
    * type should be documented a standard way
    * ..which should be different from constraints
    * ..which should be different from the description
    * Currently it's done through a CSV-style document, which makes it more
        difficult to visually parse.

* TESTS NEEDED
    * Need unit tests that will test the schemas, vs their linked-documentation
    * Need unit tests that will test parsers and serializers vs their
        linked-documentation.
    * Our documentation is out of date or contains typos in many situations
        (original version of languages list showed a "code" property, which
        is not present.


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
